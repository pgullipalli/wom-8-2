<cfsilent>
<cfparam name="URL.md" default="homepage">
<cfparam name="URL.tmp" default="home">
<cfparam name="URL.pnid" default="0">
<cfparam name="URL.nid" default="0">
<cfparam name="URL.tid" default="0"><!--- template id --->
<cfparam name="URL.nowrap" default="0">
<cfparam name="URL.printer" default="0">
<cfparam name="URL.popup" default="0">
<cfparam name="URL.pid" default="0">
<cfscript>

  //determine catid used for promotion ads
  if (Not IsDefined("URL.catid") Or Not Compare(URL.catid,"0")) {
    switch (URL.md) {
      case 'pagebuilder':
	    if (IsDefined("URL.pid")) {
	      PB=CreateObject("component","com.PageBuilder").init();
		  URL.catid=PB.getPageCategoryIDByPageID(URL.pid);
	    }
	    break;
		
	  case 'class':
	    if (IsDefined("URL.cid")) {
		  CL=CreateObject("component","com.Class").init();
		  URL.catid=CL.getCategoryIDByCourseID(URL.cid);
		} else if (IsDefined("URL.sid")) {
		  CL=CreateObject("component","com.Class").init();
		  URL.catid=CL.getCategoryIDByServiceID(URL.sid);
		}
	    break;
		
	  case 'newsroom':
	    if (IsDefined("URL.articleID")) {
		  NR=CreateObject("component","com.Newsroom").init();
		  URL.catid=NR.getCategoryIDByArticleID(URL.articleID);
		}
	    break;

	  default:
	    break;
    }
  }
  GL=CreateObject("component","com.Global").init();
  TM=CreateObject("component","com.Template").init();
  NV=CreateObject("component","com.Navigation").init();  
  
  Request.isHomePage=0;
  Request.isSubHomePage=0;
  Request.logo=StructNew();
  Request.logo.image="imgLogo.gif";
  Request.logo.AltText="Woman's - Exceptional Care, Centered On You";
  Request.logo.URL="#Application.siteURLRoot#";
  
  Request.globalProperties=GL.getGlobalProperties();
  /*
  Request.globalProperties (query)
  		.title
		.description
		.keywords
		.footer
		.primaryNavIDList (the nav IDs of the big pink navs on the left of the page
  */
  Request.template=TM.getTemplate(argumentCollection=URL);
  /*
  Request.template (structure)
  			.topperStyle
			.sectionTitle
			.topperImage
			.mainNavIDList
			.themeImage
			.topMostAscendantNavID
			.parentNavID
			.navID (navID of the targeted page)
  */
  
  /* hard-coded stuffs: overriding the properties defined in Request.template */
  if (Not Compare(URL.md,"class")) {
    //for all 'classes & programs' pages, always highlight the top nav 'classes & programs'
	if (ListFind('detail_fitness,schedule_fitness,searchresults_fitness',URL.tmp) GT 0) {
	  //fitness club class pages
	  Request.template.topMostAscendantNavID=Application.class.topNavigationID_Fitness;
	} else {
      Request.template.topMostAscendantNavID=Application.class.topNavigationID_Class;
	}
  }
  
  if (Not Compare(URL.md, "homepage") And Not Compare(URL.tmp, "home")) {
    Request.isHomePage=1;
	Request.printerFriendlyURL="#Application.siteURLRoot#/index.cfm?printer=1";
  } else {
    Request.printerFriendlyURL="#Application.siteURLRoot#/index.cfm?" & Replace(Replace(CGI.QUERY_STRING,"&amp;","&","All"),"&","&amp;","All") & "&amp;printer=1";
  }

  if (Not Compare(URL.md, "homepage") And Not Compare(URL.tmp, "subhome")) {
	Request.isSubHomePage=1;
  } 
  
  if (Not Compare(Application.template.templateID_FitnessClub,URL.tid) OR ListFind("schedule_fitness,detail_fitness,searchresults_fitness",URL.tmp) GT 0) {
    Request.logo.image="imgLogoFitnessClb.gif";
	Request.logo.AltText="Woman's Fitness Club - Exceptional Care, Centered On You";
	//Request.logo.URL="#Application.siteURLRoot#/fitnessclub/";
	Request.logo.URL="#Application.siteURLRoot#";
  } else if (Not Compare(Application.template.templateID_WomansFoundation,URL.tid) OR ListFind("donation1,donation2",URL.tmp) GT 0) {
    Request.logo.image="imgLogoFoundation.gif";
	Request.logo.AltText="Woman's Hospital Foundation - Exceptional Care, Centered On You";
	Request.logo.URL="#Application.siteURLRoot#/foundation/";
  }  
</cfscript>
</cfsilent>

<cfif URL.nowrap>
  <cfinclude template="modules/#URL.md#/_#URL.tmp#.cfm">
<cfelse>  
  <cfimport taglib="wrappers">
  <cfif URL.printer>
    <wrapper_printer>
    <cfinclude template="modules/#URL.md#/_#URL.tmp#.cfm">
    </wrapper_printer>
  <cfelseif Request.isHomePage>
    <wrapper_home>
	<cfinclude template="modules/homepage/_home.cfm">
	</wrapper_home>
  <cfelseif URL.popup>
  <cfoutput>URL.popup</cfoutput>
    <wrapper_popup template="#Request.template#">
	<cfinclude template="modules/#URL.md#/_#URL.tmp#.cfm">
	</wrapper_popup>
  <cfelse>
    <wrapper template="#Request.template#">
	<cfinclude template="modules/#URL.md#/_#URL.tmp#.cfm">
	</wrapper>
  </cfif>
</cfif>