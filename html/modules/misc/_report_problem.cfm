<cfhtmlhead text='<link rel="stylesheet" type="text/css" href="modules/form/form.css" />'>

<div class="head">Share Idea/Report a Problem</div>

<script type="text/javascript">
<!--
  function reportProblem(form) {
    if (form.comments.value=="") {
	  alert('Please enter your comment.');
	  form.comments.focus();
	  return;
	}
	
    form.submit();
  }
//-->
</script>

<div style="margin:5px 0px;">&nbsp;</div>
    
<form name="frmShareIdea" id="frmShareIdea" action="action.cfm?md=misc&task=reportProblem" method="post" onsubmit="return false;">
<cfoutput>
<input type="hidden" name="referralURL" value="#CGI.HTTP_REFERER#">
</cfoutput>
<div class="row">
  <div class="col-first-right">Your E-Mail Address:</div>
  <div class="col-second-left"><input type="text" name="email" style="width:300px;" maxlength="100"></div>
</div>

<div class="row">
  <div class="col-first-right">Comments:</div>
  <div class="col-second-left">
    <textarea name="comments" style="width:300px;height:150px;"></textarea>
  </div>
</div>
<div class="row">
  <div class="col-first-right">&nbsp;</div>
  <div class="col-second-left"><a href="javascript:reportProblem(document.frmShareIdea)" class="btnMed">Submit</a></div>
</div>
</form>
