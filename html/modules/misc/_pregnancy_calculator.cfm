<div id="topperHTMLCodes" style="display:none">Pregnancy Calculator</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtMomDaughterCouch.jpg" width="202" height="244" /></div>

<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
	if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());	
  });
</script>

<div class="contentWrapper">

<script src="/jsapis/jquery/js/jquery-ui-1.7.2.core_datepicker.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    //dynamically load datepicker stylesheet
    var cssURL="/jsapis/jquery/css/jquery-ui-1.7.2.datepicker.css";
    var cssRef=document.createElement("link")
    cssRef.setAttribute("rel", "stylesheet")
    cssRef.setAttribute("type", "text/css")
    cssRef.setAttribute("href", cssURL)
    if (typeof cssRef!="undefined") document.getElementsByTagName("head")[0].appendChild(cssRef);
   
	$("#lastMenstrualPeriod").datepicker();	
  });
  
  function checkDateFormat(dateStr) {
    var pattern=/^[0-9]+\/[0-9]+\/[0-9]{4}$/i;	
	return pattern.test(dateStr);
  }
  
  function dateObjToString(dateObj) {
    var monthAsString = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	var dayAsString = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];  
  
	var month = dateObj.getMonth()+1;
	month = (month < 10) ? "0" + month : month;
	 
	var dayOfMonth   = dateObj.getDate();
	dayOfMonth = (dayOfMonth < 10) ? "0" + dayOfMonth : dayOfMonth;
	 
	var year  = dateObj.getYear();
	if (year < 2000) year += 1900;
	
	var dayOfWeek = dateObj.getDay();
	
	return monthAsString[month-1] + " " + dayOfMonth + ", " + year;
		
	//return (dayAsString[dayOfWeek] + ", " + monthAsString[month-1] + " " + dayOfMonth + ", " + year);
  }
  
  function calculatePregnancy() {
    $("#resultPregnancyCalculation").hide(); 
	
	var form = document.frmPregnancyCalculator;	
	var today = new Date();
	var menstrualCycleLength = parseInt(form.menstrualCycleLength.options[form.menstrualCycleLength.selectedIndex].value);
	var lutealPhaseLength = 14; // defaults to 14	
	
	var lastMenstrualPeriodDate = new Date();
	if (checkDateFormat(form.lastMenstrualPeriod.value)) { // Validates menstual date entered
	  var dateTemp = new Date(form.lastMenstrualPeriod.value);
	  lastMenstrualPeriodDate.setTime(dateTemp.getTime());
	} else {
	  alert("Please enter the first day of your last menstrual period in a correct date format.");
	  return;
	}		
		 
	// sets ovulation date to menstrual date + cycle days - luteal days
	// the '*86400000' is necessary because date objects track time
	// in milliseconds;  86400000 milliseconds equals one day
	var conceptionDate = new Date();
	conceptionDate.setTime(lastMenstrualPeriodDate.getTime() + (menstrualCycleLength * 86400000) - (lutealPhaseLength * 86400000));
	$("#resultConceptionDate").html(dateObjToString(conceptionDate));
		 
	// sets due date to ovulation date plus 266 days
	var dueDate = new Date();
	dueDate.setTime(conceptionDate.getTime() + 266 * 86400000);
	$("#resultDueDate").html(dateObjToString(dueDate));
	 
	// sets fetal age to 14 + 266 (pregnancy time) - time left
	var fetalAge = 14 + 266 - ((dueDate - today) / 86400000);
	var numFetalWeeks = parseInt(fetalAge / 7); // sets weeks to whole number of weeks
	var numFetalDays = Math.floor(fetalAge % 7); // sets days to the whole number remainder
	 
	// fetal age message, automatically includes 's' on week and day if necessary
	var fetalAgeString = numFetalWeeks + " week" + (numFetalWeeks > 1 ? "s" : "") + ", " + numFetalDays + " days";
	$("#resultFetalAge").html(fetalAgeString);
	
	// 4 weeks pregnant
	var pregnancyTestDate = new Date();
	pregnancyTestDate.setTime(conceptionDate.getTime() + 14 * 86400000);
	$("#resultPregnancyTestDate").html(dateObjToString(pregnancyTestDate));
	
	var firstHeartBeatDate = new Date();
	firstHeartBeatDate.setTime(conceptionDate.getTime() + 56 * 86400000);
	$("#resultFirstHeartBeatDate").html(dateObjToString(firstHeartBeatDate));
	
	var riskiestPeriodOverDate = new Date();
	riskiestPeriodOverDate.setTime(conceptionDate.getTime() + 84 * 86400000);
	$("#resultRiskiestPeriodOverDate").html(dateObjToString(riskiestPeriodOverDate));
	
	var babySexKnownDate = new Date();
	babySexKnownDate.setTime(conceptionDate.getTime() + 98 * 86400000);
	$("#resultBabySexKnownDate").html(dateObjToString(babySexKnownDate));
	
	$("#resultPregnancyCalculation").show('slide');
  }
</script>

<style type="text/css">
/*form pages*/
#frmPregnancyCalculator .intro { font-size:12px; line-height:150%; border-bottom: none; margin:0px; padding:15px 20px 20px 20px; float:left; width:415px; }
#frmPregnancyCalculator {font-size:10px; color:#80858E; line-height:110%;}
#frmPregnancyCalculator .unit {float:left; width:100%; padding-bottom:16px;}
#frmPregnancyCalculator .section { font-size:14px; line-height:100%; color:#6f7876; background:#e8eceb; padding:6px; margin-bottom:15px; float:left; width:456px; }
#frmPregnancyCalculator label.title {font-size:12px; color: #666; font-weight:bold; line-height:110%; text-align:left; padding:0px 0px 12px 0px; float:left; width:100%;}
#frmPregnancyCalculator label.descrip {font-size:11px; color:#80858E; line-height:125%; text-align:right; padding-right:10px; float:left; width:200px;}
#frmPregnancyCalculator .singleField { float:left; width:100%; padding-bottom:15px; font-size:11px; line-height:125%;}
#frmPregnancyCalculator .info { font-size:12px; line-height:110%; float:left; width:100%; padding:15px 0px 8px 0px;}
#frmPregnancyCalculator input, #frmPregnancyCalculator select {font-size:13px; line-height:100%; color:#535a66; border:1px solid #CCC; height:20px;}
#frmPregnancyCalculator .divider {margin:5px 0px 15px 0px; width:415px; border-top:1px dotted #cccccc; float:left;}
#resultPregnancyCalculation {display:none;width:100%;float:left;}
</style>

<div class="head">Pregnancy Due Date Calculator</div>

<form id="frmPregnancyCalculator" name="frmPregnancyCalculator" onSubmit="return false;">  
  <div class="info">
  This interactive Due Date Calculator will help you estimate the date your baby will arrive. Pregnancy usually lasts 40 weeks (280 days) from the first day of your last menstrual period. This calculator is a general guide - every pregnancy is unique so consult with your doctor because some babies arrive sooner or later than expected.<br /><br />
  </div>

  <div class="divider"></div>
  <div class="unit">    
    <!--- <span class="section">Contact Information</span> --->
    <div class="singleField">
      <label for="lastMenstrualPeriod" class="descrip">What was the date of the first day of your last menstrual period?</label>
      <input type="text" name="lastMenstrualPeriod" id="lastMenstrualPeriod" style="width:100px;" /> (MM/DD/YYYY)
    </div>
    <div class="singleField">
      <label for="menstrualCycleLength" class="descrip">Average Length of Menstrual Cycles:</label>

      <select name="menstrualCycleLength" id="menstrualCycleLength" style="width:50px;">
        <option value="15">15</option>
        <option value="15">15</option>        
        <option value="16">16</option>        
        <option value="17">17</option>        
        <option value="18">18</option>        
        <option value="19">19</option>        
        <option value="20">20</option>        
        <option value="21">21</option>        
        <option value="22">22</option>        
        <option value="23">23</option>        
        <option value="24">24</option>        
        <option value="25">25</option>        
        <option value="26">26</option>        
        <option value="27">27</option>        
        <option value="28" selected>28</option>

        <option value="29">29</option>        
        <option value="30">30</option>        
        <option value="31">31</option>        
        <option value="32">32</option>        
        <option value="33">33</option>        
        <option value="34">34</option>        
        <option value="35">35</option>        
        <option value="36">36</option>        
        <option value="37">37</option>        
        <option value="38">38</option>        
        <option value="39">39</option>        
        <option value="40">40</option>        
        <option value="41">41</option>        
        <option value="42">42</option>        
        <option value="43">43</option>        
        <option value="44">44</option>        
        <option value="45">45</option>

      </select>
    </div>    
  </div>
  <div class="unit">
    <div class="singleField">
      <label class="descrip">&nbsp;</label>
      <a href="javascript:calculatePregnancy();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCalculate','','/images/btnFormCalculateo.gif',1)" ><img src="/images/btnFormCalculate.gif" alt="Calculate" id="btnCalculate" width="103" height="17" name="btnCalculate" border="0" /></a>
    </div>
  </div>

  
  <div id="resultPregnancyCalculation">
  
      <div class="divider"></div>
      
      <div class="unit">        
        <div class="singleField">
          <label class="descrip">Due Date:</label>

          <span id="resultDueDate" class="accent02"></span>
        </div>  
        <div class="singleField">
          <label class="descrip">Conception Date:</label>
          <span id="resultConceptionDate" class="accent02"></span>
        </div>
  
        <div class="singleField">
          <label class="descrip">Fetal Age:</label>
          <span id="resultFetalAge" class="accent02"></span>
        </div>
        
        <div class="singleField">
          <label class="descrip">Positive Pregnancy Test:</label>
          <span id="resultPregnancyTestDate" class="accent02"></span>
        </div>
        <div class="singleField">
          <label class="descrip">Baby's 1st Heartbeat:</label>
          <span id="resultFirstHeartBeatDate" class="accent02"></span>
        </div>
        
        <div class="singleField">
          <label class="descrip">Riskiest Period Was Over:</label>
          <span id="resultRiskiestPeriodOverDate" class="accent02"></span>
        </div>
        
        
        
        
        <div class="singleField">
          <label class="descrip">Having a boy or girl?</label>
          <span id="resultBabySexKnownDate" class="accent02"></span>
        </div>
        
        
        
        
        
        <div class="singleField"><br />
        Remember: This calculator is a general guide, every pregnancy is unique, and sometimes babies arrive sooner or later than expected. Always talk to your health care provider about your due date.
        </div>

      </div>
  
  </div>
</form>

</div>