<cfparam name="URL.task">

<cfswitch expression="#URL.task#">
  <cfcase value="reportProblem">
    <cfset recipientEmail="web_new@womans.org">
    <cfmail from="doNotReply@womans.org" to="#recipientEmail#" subject="Share Idea/Report a Problem - Woman's Hospital Website" type="html">
      <cfmailpart type="text" charset="utf-8">
The following comment has been received:

Page: #Form.referralURL#

Email: #Form.email#

Comments:

#Form.comments#
	  </cfmailpart>
      <cfmailpart type="html" charset="utf-8">
      <div style="font-family:'Trebuchet MS', Arial; font-size:11px; color:##333132; padding:20px;">
        <a href="#Application.siteURLRoot#" target="_blank"><img src="#Application.siteURLRoot#/images/imgLogoEmailNot.gif" alt="Woman's - exceptional care, centered on you" width="96" height="84" border="0" /></a>
        
        <br /><br /><br /> 
        
        <span style="font-family:'Trebuchet MS',Arial;font-size:19px;color:##eb44a7;">The following comment has been received:</span><br /><br />
        
        <span style="font-family:'Trebuchet MS',Arial;color:##627d79;font-size:14px;">Page</span><br />        
        <a href="#Form.referralURL#" style="font-size: 13px;font-family:'Trebuchet MS',Arial;color:##666666; text-decoration:none;">#Form.referralURL#</a><br /><br />
                
        <span style="font-family:'Trebuchet MS',Arial;color:##627d79; font-size: 14px;">Email</span><br />
        <a href="mailto:#Form.email#" style="font-size: 13px;font-family:'Trebuchet MS',Arial;color:##666666; text-decoration:none;">#Form.email#</a><br /><br />
        
        <span style="font-family:'Trebuchet MS',Arial;color:##627d79; font-size: 14px;">Comments</span><br />
        <span style="font-size: 13px;font-family:'Trebuchet MS',Arial;color:##666666;">#Replace(Form.comments,Chr(10),"<br />","All")#  </span>     
      </div>
      </cfmailpart>
    </cfmail> 
    <cflocation url="index.cfm?md=misc&tmp=report_problem_thankyou&popup=1">
  </cfcase>
</cfswitch>
