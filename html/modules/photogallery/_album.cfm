<cfsilent>
<cfparam name="URL.albumid">
<cfparam name="URL.catid" default="0">
<cfparam name="URL.startIndex" default="1">
<cfparam name="URL.numItemsPerPage" default="10">
<cfscript>
  PG=CreateObject("component","com.PhotoGallery").init();
  albumInfo=PG.getAlbum(URL.albumid);  
  photos=PG.getAllAlbumPhotos(albumID=URL.albumid);
</cfscript>
<cfif albumInfo.recordcount Is 0><cflocation url="/404error.cfm"></cfif>
</cfsilent>
<cfset htmlHeadText='<link type="text/css" rel="stylesheet" href="jsapis/flowplayer/css/slideshow.css">' />
<cfset htmlHeadText=htmlHeadText & Chr(13) & Chr(10) & '<script type="text/javascript" src="jsapis/flowplayer/jquery.tools.min.js"></script>'>
<cfhtmlhead text="#htmlHeadText#">

<div id="topperHTMLCodes" style="display:none">Photo Gallery</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_gallery.jpg" width="202" height="244" /></div>
<script type="text/javascript">
  var api;
  var numImages;
  var slideShow=false;//slideshow status
  var delay=5000;

  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "")  $("#titleBar").html($("#topperHTMLCodes").html()); //topper title
	//if (trim($("#thmemImage").html()) == "")
	$("#thmemImage").html($("#themeImageHTMLCodes").html()); //corner image

	/* chained call: scrollable().find("a").tooltip().overlay().gallery(); */	 
	$(".scrollable").scrollable().circular().find("a").overlay({	 
		// each trigger uses the same overlay with id "gallery"
		target: '#gallery',	 
		// optional exposing effect with custom color
		expose: '#111',	 
		// clicking outside the overlay does not close it
		closeOnClick: false,
		onBeforeClose: function(){ slideShow=false; }		 
	// gallery plugin
	}).gallery({	 
		// do not use the same disabled class name as scrollable		
		disabledClass: 'inactive',		
		//template: '<strong>${title}</strong> <span><i>Image ${index} of ${total}</i></span> <div style="text-align:right;"><a href="javascript:downloadHiRes()" style="color:#ffffff;text-decoration:none;"><b>Download Hi-Resolution Version &raquo;</b></a><div>'
		template: '<strong>${title}</strong>'
	});
	
	$(".scrollable img[title]").tooltip({
		tip: '#tooltip'		
	}); 	
	
	$(".next,.prev").click(function(e){ slideShow=false; });
	
	api=$(".scrollable").scrollable();
	
	numImages=$(".items a").length;
	
	startSlideshow();
  });
  
  function startSlideshow() {
    slideShow=true;
    $($(".items a")[api.getIndex()]).click();   
    window.setTimeout('playNext()', delay);
  }

  function continueSlideshow() {
    slideShow=true;
    window.setTimeout('playNext()', delay);
  }

  function playNext() {
    if (slideShow) {
      $(".next").click();	
	  slideShow=true;
      window.setTimeout('playNext()', delay);
    }
  }
</script>
       
<div class="contentWrapper" id="phtGallery">  
  <span class="head">
    <cfoutput>
    #albumInfo.albumTitle#
    <cfif Compare(URL.catid,"0")><a href="index.cfm?md=photogallery&tmp=category&catid=#URL.catid#" class="lnkGallSelect">Select Another Gallery &raquo;</a><cfelse><a href="index.cfm?md=photogallery&tmp=home" class="lnkGallSelect">Select Another Gallery &raquo;</a></cfif>
    <span class="gallDescrip">
      #albumInfo.description#
    </span>
    </cfoutput>
  </span>   
  
  <div style="margin:200px auto;">
    <!-- "previous page" action --> 
    <a class="prevPage browse left"></a>
        
    <!-- root element for scrollable --> 
    <div class="scrollable"> 
      <!-- root element for the items --> 
      <div class="items">
        <cfoutput query="photos">
          <cfif Compare(photoThumbnail,"")>
            <cfset photoDesc = title>
            <cfif Compare(caption,"")><cfset photoDesc = photoDesc & '<br /><small>' & caption & '</small>'></cfif>
            <cfif Compare(author,"")><cfset photoDesc = photoDesc & '<br /><small><i>CREDIT:' & author & '</i></small>'></cfif>             
            <a href="#photoFile#" title="#HTMLEditFormat(photoDesc)#"><img src="#photoThumbnail#" title="#HTMLEditFormat(title)#" /></a>
          </cfif>
        </cfoutput>
      </div> 
    </div> 
    
    <!-- one single tooltip element --> 
    <div id="tooltip"></div>
    
    <!-- "next page" action --> 
    <a class="nextPage browse right"></a>
  </div>
    
  <br clear="all" />
    
  <div style="text-align:center;">
    <a href="javascript:startSlideshow()" style="text-decoration:none;"><b>Start Slide Show &raquo;</b></a>
  </div>
    
  <!-- overlay element --> 
  <div class="simple_overlay" id="gallery"> 
    <!-- "previous image" action --> 
    <a class="prev">prev</a> 
    <!-- "next image" action --> 
     <a class="next">next</a>
       
    <!-- image information --> 
    <div class="info"></div> 
    <!-- load indicator (animated gif) --> 
    <img class="progress" src="images/loading.gif" />
  </div>
</div>
