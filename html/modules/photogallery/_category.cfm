<cfsilent>
<cfparam name="URL.catid">
<cfparam name="URL.startIndex" default="1">
<cfparam name="URL.numItemsPerPage" default="10">
<cfscript>
  UT=CreateObject("component","com.Utility").init();
  PG=CreateObject("component","com.PhotoGallery").init();
  categoryName=PG.getAlbumCategoryName(URL.catid);
  structAlbums=PG.getAlbumsByCategoryID(albumCategoryID=URL.catid, startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage);
</cfscript>
<cfif NOT Compare(categoryName,"")><cflocation url="/404error.cfm"></cfif>
</cfsilent>

<div id="topperHTMLCodes" style="display:none">Photo Gallery</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_gallery.jpg" width="202" height="244" /></div>
<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "")  $("#titleBar").html($("#topperHTMLCodes").html());
	//if (trim($("#thmemImage").html()) == "")
	$("#thmemImage").html($("#themeImageHTMLCodes").html());
  });
</script>

<cfif structAlbums.numAllItems GT URL.numItemsPerPage>
  <cfset navPanel=UT.displayNextPreviousLinks(pageURL="index.cfm?md=photogallery&tmp=category&catid=#URL.catid#", startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage, numDisplayedItems=structAlbums.numDisplayedItems, numAllItems=structAlbums.numAllItems, itemName="Results")>
  <cfoutput>#navPanel#</cfoutput>
</cfif>

<div class="contentWrapper" id="phtGallery">
  <span class="head"><cfoutput>#categoryName#</cfoutput>  <a href="index.cfm?md=photogallery&tmp=home" class="lnkGallSelect">Select Another Category &raquo;</a></span>
  
  <cfoutput query="structAlbums.albums">
  <div class="phtUnit">
    <div class="gallHead"><a href="index.cfm?md=photogallery&tmp=album_html&catid=#URL.catid#&albumID=#albumID#">BROWSE</a><a href="index.cfm?md=photogallery&tmp=album&catid=#URL.catid#&albumID=#albumID#">SLIDESHOW</a> <span>#albumTitle#</span></div>
    <cfif Compare(coverImage,"")>
    <img src="#coverImage#" width="160" height="138" class="floatLeft" />
    </cfif>
    <div class="content">
      <a href="index.cfm?md=photogallery&tmp=album&catid=#URL.catid#&albumID=#albumID#" class="lnkHead">#albumTitle#</a>
      #description#
    </div>
    <img src="images/imgGallRtCap.gif" width="9" height="138" class="floatLeft" />
  </div>
  </cfoutput>
</div>

<cfif structAlbums.numAllItems GT URL.numItemsPerPage>
  <cfoutput>#navPanel#</cfoutput>
</cfif>