<cfsilent>
<cfparam name="URL.albumid">
<cfparam name="URL.catid" default="0">
<cfparam name="URL.startIndex" default="1">
<cfparam name="URL.numItemsPerPage" default="10">
<cfscript>
  UT=CreateObject("component","com.Utility").init();
  PG=CreateObject("component","com.PhotoGallery").init();
  albumInfo=PG.getAlbum(URL.albumid);
  structPhotos=PG.getAlbumPhotos(albumID=URL.albumid, startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage);
</cfscript>
<cfif albumInfo.recordcount Is 0><cflocation url="/404error.cfm"></cfif>
</cfsilent>

<div id="topperHTMLCodes" style="display:none">Photo Gallery</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_gallery.jpg" width="202" height="244" /></div>
<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "")  $("#titleBar").html($("#topperHTMLCodes").html());
	//if (trim($("#thmemImage").html()) == "")
	$("#thmemImage").html($("#themeImageHTMLCodes").html());
  });
</script>

<cfif structPhotos.numAllItems GT URL.numItemsPerPage>
  <cfset navPanel=UT.displayNextPreviousLinks(pageURL="index.cfm?md=photogallery&tmp=album_html&catid=#URL.catid#&albumid=#URL.albumid#", startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage, numDisplayedItems=structPhotos.numDisplayedItems, numAllItems=structPhotos.numAllItems, itemName="Results")>
  <cfoutput>#navPanel#</cfoutput>
</cfif>
        
<div class="contentWrapper" id="phtGallery">  
  <span class="head">
    <cfoutput>
    #albumInfo.albumTitle#
    <cfif Compare(URL.catid,"0")><a href="index.cfm?md=photogallery&tmp=category&catid=#URL.catid#" class="lnkGallSelect">Select Another Gallery &raquo;</a><cfelse><a href="index.cfm?md=photogallery&tmp=home" class="lnkGallSelect">Select Another Gallery &raquo;</a></cfif>
    <span class="gallDescrip">
      #albumInfo.description#
    </span>
    </cfoutput>
  </span>  
  
  <cfoutput query="structPhotos.photos">
  <div class="phtFeatured"><span class="name">#title#</span>
  	<cfif Compare(caption,"")>
    <div class="caption">#caption#</div>
    </cfif>
    <cfif Compare(photoFile,"")>
    <div class="floatLeftFullWdth" align="center"><img src="#photoFile#" style="max-width:465px;max-height:465px;" /></div>
    </cfif>
    <cfif Compare(author,"")>
    <div class="credit">CREDIT: #author#</div>
    </cfif>
  </div>
  </cfoutput>
  
  <div class="head">
    <cfoutput><a href="index.cfm?md=photogallery&tmp=album&catid=#URL.catid#&albumid=#URL.albumid#" class="lnkGallSelect">View Slide Show &raquo;</a></cfoutput>
  </div>

</div>


<cfif structPhotos.numAllItems GT URL.numItemsPerPage>
  <cfoutput>#navPanel#</cfoutput>
</cfif>