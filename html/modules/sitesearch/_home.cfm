<cfprocessingdirective pageencoding="utf-8">

<cfparam name="URL.q" default="">
<cfparam name="URL.startIndex" default="1">
<cfparam name="URL.numItemsPerPage" default="20">
<cfsilent>
<cfscript>
  UT=CreateObject("component","com.Utility").init();
  SS=CreateObject("component","com.SiteSearch").init();
  structResults=SS.siteSearch(keyword=URL.q, startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage);  
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none">Search Results</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_search.jpg" width="202" height="244" /></div>
<script type="text/javascript">
<!--
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
	if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  });
//-->
</script>

<cfif structResults.numAllItems GT URL.numItemsPerPage>
  <cfset navPanel=UT.displayNextPreviousLinks(pageURL="index.cfm?md=sitesearch&tmp=home&q=#URLEncodedFormat(URL.q)#", startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage, numDisplayedItems=structResults.numDisplayedItems, numAllItems=structResults.numAllItems, itemName="Results")>
  <cfoutput>#navPanel#</cfoutput>
</cfif>
  
  <div class="contentWrapper" id="searchResults">
    <div class="introResults"><span class="label">Results For </span> <span class="accent02Lrg">&quot;<cfoutput>#URL.q#</cfoutput>&quot;</span></div>
    <!--RESULTS-->
    
    <cfif structResults.numAllItems GT 0>
    <ul>
      <cfoutput>
        <cfloop index="idx" from="1" to="#ArrayLen(structResults.id)#">
          <cfif NOT Compare(structResults.searchType[idx] , "File")>
            <li><div class="category">#structResults.category[idx]#</div><a href="#structResults.id[idx]#" target="_blank">#structResults.pageName[idx]#</a> (#structResults.pagetype[idx]#)</li>
          <cfelse>
            <li><div class="category">#structResults.category[idx]#</div>
            <cfswitch expression="#structResults.pagetype[idx]#">
              <cfcase value="Page Builder">
                <a href="index.cfm?md=pagebuilder&pid=#structResults.id[idx]#"<cfif structResults.popup[idx]> target="_blank"</cfif>>#structResults.pageName[idx]#</a>
              </cfcase>
              <cfcase value="Newsroom">
                <a href="index.cfm?md=newsroom&tmp=detail&articleID=#structResults.id[idx]#"<cfif structResults.popup[idx]> target="_blank"</cfif>>#structResults.pageName[idx]#</a>
              </cfcase>
              <cfcase value="Photo Gallery">
                <a href="index.cfm?md=photogallery&tmp=album_html&albumID=#structResults.id[idx]#"<cfif structResults.popup[idx]> target="_blank"</cfif>>#structResults.pageName[idx]#</a>
              </cfcase>
              <cfcase value="Directory">
                <a href="index.cfm?md=directory&tmp=detail&itemID=#structResults.id[idx]#"<cfif structResults.popup[idx]> target="_blank"</cfif>>#structResults.pageName[idx]#</a>
              </cfcase>
              <cfcase value="Doctor">
                <a href="index.cfm?md=doctor&tmp=detail&rn=#structResults.id[idx]#"<cfif structResults.popup[idx]> target="_blank"</cfif>>#structResults.pageName[idx]#</a>
              </cfcase>
              <cfcase value="Event">
                <a href="index.cfm?md=event&tmp=detail&eventID=#structResults.id[idx]#"<cfif structResults.popup[idx]> target="_blank"</cfif>>#structResults.pageName[idx]#</a>
              </cfcase>
              <cfcase value="Class">
                <a href="index.cfm?md=class&tmp=detail&cid=#structResults.id[idx]#"<cfif structResults.popup[idx]> target="_blank"</cfif>>#structResults.pageName[idx]#</a>
              </cfcase>
              <cfcase value="Fitness">
                <a href="index.cfm?md=class&tmp=detail_fitness&fcid=#structResults.id[idx]#"<cfif structResults.popup[idx]> target="_blank"</cfif>>#structResults.pageName[idx]#</a>
              </cfcase>              
            </cfswitch>            
            </li>
          </cfif>
        </cfloop>
      </cfoutput>
    </ul>
    
    <cfelse>
      <div class="accent03" align="center">Your search returns no results.</div>
    </cfif>
  </div>
  
<cfif structResults.numAllItems GT URL.numItemsPerPage>
  <cfoutput>#navPanel#</cfoutput>
</cfif>