<cfparam name="URL.task">
<cfparam name="URL.pnid" default="0">
<cfparam name="URL.nid" default="0">
<cfparam name="URL.tid" default="0">

<cfinclude template="_create_user_session.cfm">
<!--- <cfset Session.user=CreateObject("component","com.User").init()> --->

<cfscript>
  UT=CreateObject("component", "com.Utility").init();
  
  switch (URL.task) {
    case 'login':
	  if (Session.user.login(argumentCollection=Form)) {
	    if (IsDefined("Form.next") And Compare(Form.next,"")) {
		  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=#Form.next#");
		} else {
		  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=myschedule");
		}
	  } else {//failed
	    if (IsDefined("Form.next") And Compare(Form.next,"")) {
	      UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=login&&next=#Form.next#&failed=1");
		} else {
		  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=login&failed=1");
		}
	  }
	  break;
	  
	case 'logout':
	  StructDelete(Session, "user");
	  Session.user=CreateObject("component","com.User").init();
	  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=login");
	  break;
	  
	case 'loginAsUserByAdmin':
	  StructDelete(Session, "user");
	  Session.user=CreateObject("component","com.User").init();
	  if (Session.user.login(argumentCollection=Form)) {
	    UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=myschedule");
	  } else {
	    UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=login&failed=1");
	  }
	  break;
	  
	case 'updateProfile':
	  if (Session.user.verifyLogin(Form.email, Form.password)) {
	    if (Session.user.updateProfile(argumentCollection=Form)) {
	      UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=profile&success=1&code=#Form.change#");
	    } else {
	      UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=profile&er=1&code=#Form.change#");
	    }
	  } else {
	    UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=profile&er=1&code=login");
	  }
	  break;
	  
	case 'updateClassReminder':
	  Session.user.updateClassReminder(argumentCollection=Form);
	  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=myschedule");
	  break;
	    
    case 'addClassToCart':
	  Session.user.addClassToCart(URL.clid);
	  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=myschedule");
      break;
	  
	case 'addServiceToCart':
	  Session.user.addServiceToCart(URL.sid);
	  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=myschedule");
      break;
	  
	case 'removeClassesFromCart':
	  Session.user.removeClassesFromCart(argumentCollection=Form);
	  Session.user.removeServicesFromCart(argumentCollection=Form);
	  Session.user.updatePaymentPlanOption(argumentCollection=Form);
	  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=myschedule");
      break;
	  
	case 'addCurriculaClassesToCart':
	  Session.user.addCurriculaClassesToCart(argumentCollection=Form);
	  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=myschedule");
	  break;
	  
	case 'updateCartContent':
	  Session.user.removeClassesFromCart(argumentCollection=Form);
	  Session.user.removeServicesFromCart(argumentCollection=Form);
	  Session.user.removeDiscountCodesFromCart(argumentCollection=Form);
	  Session.user.removeDonationFromCart(argumentCollection=Form);	   
	  if (IsNumeric(Form.donationAmount)) {
	    Session.user.addDonationToCart(Form.donationAmount);
	  }
	  Session.user.updatePaymentPlanOption(argumentCollection=Form);	  
	  if (IsDefined("Form.discountCode") And Compare(Form.discountCode,"")) {
	    if (Session.user.addDiscountCodeToCart(Form.discountCode)) {
		  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout&update=1");  
		} else {
		  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout&er1=1&code=#URLEncodedFormat(Form.discountCode)#");
		}
	  } else {
	    UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout&update=1");  
	  }	  
      break;
	  
	case 'updateBillingInfo':
	  try {
	    Session.user.updateBillingInfo(argumentCollection=Form);
		UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout_confirmation");
	  } catch (any excpt) {
	    UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout&er2=1");
	  }
	  
	  break;
	  
	case 'completeRegistration':
	  result=Session.user.completeRegistration(signUpFormID=Application.communication.eNewsletterSignUpFormID, donationFormID=Application.customform.customFormID_Donation);
	  if (result.success) {
	    if (Not result.successDonation) {
	      UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout_complete&derr=1");//donation error
		} else {
		  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout_complete");
		}
	  } else {
	    Session.user.message=result.message;
		UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout_confirmation&er=1");
	  }
	  break; 
	  
	case 'submitReview':
	  CL=CreateObject("component", "com.Class").init();
	  CL.submitReview(argumentCollection=Form);
	  UT.location("index.cfm?md=class&tmp=submit_review_thankyou");
	  break;
	  
	case 'changeClassSchedule':
	  if (Session.user.changeClassSchedule(argumentCollection=Form)) {
	    UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=myschedule");
	  } else {
	    UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=myschedule&er=1");
	  }
	  break;
	
	case 'register':
	  if (FindNoCase("@womans.org" , Form.email) GT 0) {
        Form.studentType="E";
      }
	  CL=CreateObject("component", "com.Class").init();
	  if (Compare(Form.fitnessClubMemberID,"")) {
	    if (Not IsNumeric(Form.fitnessClubMemberID)) {		  
		  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=register&er=2&email=#URLEncodedFormat(Form.email)#&fid=#Form.fitnessClubMemberID#");
		} else {
		  memberStatus=CL.checkFitnessClubMembership(Form.fitnessClubMemberID);		  
		  if (memberStatus IS 2) {//invalid member ID
		    UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=register&er=2&email=#URLEncodedFormat(Form.email)#&fid=#Form.fitnessClubMemberID#");
		  } else if (memberStatus IS 3) {//member ID has been used
		    UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=register&er=3&email=#URLEncodedFormat(Form.email)#&fid=#Form.fitnessClubMemberID#");
		  } else {		    
		    Form.studentType="F";
		  }
		}
	  }
	  
	  if (Session.user.register(argumentCollection=Form)) {
	    if (ArrayLen(Session.user.penciledInClasses) Is 0 AND ArrayLen(Session.user.penciledInServices) Is 0) {
		  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=register_thankyou");
		} else {
		  UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=myschedule");
		}   
	  } else {
	    UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=register&er=1&email=#URLEncodedFormat(Form.email)#&fid=#Form.fitnessClubMemberID#");
	  }
	  break;
	  
	case 'retrievePassword':
	  if (Session.user.retrievePassword(argumentCollection=Form)) {
	    UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=forgotpassword_thankyou&email=#URLEncodedFormat(Form.email)#");
	  } else {
	    UT.location("#Application.siteURLRootSSL#/index.cfm?md=class&tmp=forgotpassword&er=1&email=#URLEncodedFormat(Form.email)#");
	  }
	  break;
	  
	default:
	  break;
  }
</cfscript>
