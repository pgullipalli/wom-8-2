<cfsilent>
<cfscript>
  CL=CreateObject("component","com.Class").init();
  classCategorySummary=CL.getClassCategorySummary();
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none;">Classes and Programs Categories</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes); 
});
</script>

<div class="contentWrapper" id="classCategory">

  <!--- <cfdump var="#classCategorySummary.test#"> --->

  <cfoutput>
    <cfloop index="idx1" from="1" to="#ArrayLen(classCategorySummary.category)#">
      <cfif ArrayLen(classCategorySummary.category[idx1].course) GT 0>
  	  <div class="floatLeftFullWdth"><span class="titleBarSm">#classCategorySummary.category[idx1].categoryName#</span></div>
  	  <ul>
        <cfloop index="idx2" from="1" to="#ArrayLen(classCategorySummary.category[idx1].course)#">
        <li>
          <a href="index.cfm?md=class&tmp=detail&cid=#classCategorySummary.category[idx1].course[idx2].courseID#" class="className">#classCategorySummary.category[idx1].course[idx2].courseTitle#</a>
          #classCategorySummary.category[idx1].course[idx2].shortDescription#
        </li>
        </cfloop>    	
  	  </ul>
  	  <div class="viewAllWrapper"> <a href="index.cfm?md=class&tmp=category&catid=#classCategorySummary.category[idx1].categoryID#" class="btnFinishReg">View All Classes �</a> </div>
      <cfelseif ArrayLen(classCategorySummary.category[idx1].service) GT 0>
      <div class="floatLeftFullWdth"><span class="titleBarSm">#classCategorySummary.category[idx1].categoryName#</span></div>
  	  <ul>
        <cfloop index="idx2" from="1" to="#ArrayLen(classCategorySummary.category[idx1].service)#">
        <li>
          <a href="index.cfm?md=class&tmp=detail_service&sid=#classCategorySummary.category[idx1].service[idx2].serviceID#" class="className">#classCategorySummary.category[idx1].service[idx2].serviceTitle#</a>
          #classCategorySummary.category[idx1].service[idx2].shortDescription#
        </li>
        </cfloop>    	
  	  </ul>
  	  <div class="viewAllWrapper"> <a href="index.cfm?md=class&tmp=category&catid=#classCategorySummary.category[idx1].categoryID#" class="btnFinishReg">View All Classes �</a> </div>
      </cfif>
    </cfloop>
  </cfoutput>
</div>


