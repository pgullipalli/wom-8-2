<cfinclude template="_create_user_session.cfm">
<cfparam name="URL.email" default="">
<cfparam name="URL.fid" default="">
<div id="topperHTMLCodes" style="display:none;">Classes &amp; Programs</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);  
});

function submitRegistration(form) {
  if (!checkEmail(form.email.value)) {
    alert("Please enter a valid e-mail address.");
	form.email.focus();
	return false;
  }
  
  if (form.email.value != form.email2.value) {
    alert('The email addresses you entered do not match.');
    form.email.focus();
	return false;
  }
  
  if (form.password.value.length < 6) {
    alert('The password you selected is too short. Please select a password with at least 6 characters.');
	form.password.focus();
	return false;
  }
  
  if (form.password.value != form.password2.value) {
    alert('The passwords you entered do not match.');
    form.password.focus();
	return false;
  }
  
  if (form.fitnessClubMemberID.value != "" && !checkDigits(form.fitnessClubMemberID.value)) {
    alert('The Fitness Club Member ID you entered is invalid.');
    form.fitnessClubMemberID.focus();
	return false;
  }
  
  return true;
}
</script>
<div class="contentWrapper" id="forms">
  <div class="floatLeftFullWdth">
    <cfoutput>
    <cfif IsDefined("URL.er")>    
    <div class="accent02" style="font-size:11px;background-color:##FFFF00;padding:5px 5px; margin:0px 0px 20px 0px;float:left;">
      <cfif URL.er Is "1">
      <i>It appears that the email address you entered has already been used.</i>
      <cfelseif URL.er Is "2">
      <i>Sorry, but the Fitness Club Member ID you entered can not be verified.</i>
      <cfelseif URL.er Is "3">
      <i>It appears that the Fitness Club Member ID you entered has already been used.</i>
      </cfif>
    </div>     
    </cfif>
    
    <form name="frmRegistration" id="frmRegistration" action="#Application.siteURLRootSSL#/action.cfm?md=class&task=register" method="post" onsubmit="return submitRegistration(this);">
      <div class="unit"> <span class="section">Your Information</span>
        <div class="singleField">
          <label for="email" class="descrip">E-Mail Address</label>
          <input type="text" name="email" id="email" value="#HTMLEditFormat(URL.email)#" />
        </div>
        <div class="singleField">
          <label for="email2" class="descrip">Re-type E-Mail Address</label>
          <input type="text" name="email2" id="email2" />
        </div>
        <div class="singleField">
          <label for="password" class="descrip">password</label>
          <input type="password" name="password" id="password" />
        </div>
        <div class="singleField">
          <label for="password2" class="descrip">re-type password</label>
          <input type="password" name="password2" id="password2" />
        </div>
        
        <div class="singleField">
          <span class="accent02" style="font-size:11px; font-weight:bold;">If you are a Fitness Club Member, please enter your member ID:</span>
        </div>
        <div class="singleField">
          <label for="fitnessClubMemberID" class="descrip">Fitness Club Member ID</label>
          <input type="text" name="fitnessClubMemberID" id="fitnessClubMemberID" value="#HTMLEditFormat(URL.fid)#" />
        </div>
      </div>
      <div class="unit">
        <input type="image" src="images/btnFormSubmit.gif" alt="Submit" name="btnFormSubmit" style="width:63px;height:17px; border:0px solid ##ffffff;" border="0" id="btnFormSubmit" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormSubmit','','images/btnFormSubmito.gif',1)" />&nbsp;&nbsp;
        <a href="javascript:document.frmRegistration.reset();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormClear','','images/btnFormClearo.gif',1)"><img src="images/btnFormClear.gif" alt="Clear" name="btnFormClear" width="63" height="17" border="0" id="btnFormClear" /></a>
      </div>
    </form>
    </cfoutput>
  </div>
</div>
