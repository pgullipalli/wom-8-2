<cfparam name="URL.w"><!--- keyword --->
<cfparam name="URL.startIndex" default="1">
<cfparam name="URL.numItemsPerPage" default="20">
<cfsilent>
<cffunction name="displayTimeRange" access="public" output="no" returntype="string">
  <cfargument name="sTime" type="any" required="yes">
  <cfargument name="eTime" type="any" required="yes">
  
  <cfif TimeFormat(Arguments.sTime,"h:mm tt") IS "12:00 PM">
    <cfreturn "Noon-" & LCase(TimeFormat(Arguments.eTime,"h:mm tt"))>
  <cfelse>
    <cfreturn TimeFormat(Arguments.sTime,"h:mm") & "-" & LCase(TimeFormat(Arguments.eTime,"h:mm tt"))>
  </cfif>
</cffunction>

<cfscript>
  UT=CreateObject("component","com.Utility").init();
  CL=CreateObject("component","com.Class").init();
  structClasses=CL.getFitnessClassesByKeyword(keyword=URL.w,startIndex=URL.startIndex,numItemsPerPage=URL.numItemsPerPage);  
</cfscript>
</cfsilent>
<div id="topperHTMLCodes" style="display:none;">Fitness Club</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtTowelAroundNeck.jpg" width="202" height="244" /></div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
});
</script>

<!--DISPLAY TOOLS-->
<cfif structClasses.numAllItems GT URL.numItemsPerPage>
  <cfset navPanel=UT.displayNextPreviousLinks(pageURL="index.cfm?md=class&tmp=searchresults_fitness&w=#URLEncodedFormat(URL.w)#&tid=#URL.tid#", startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage, numDisplayedItems=structClasses.numDisplayedItems, numAllItems=structClasses.numAllItems, itemName="Results")>
  <cfoutput>#navPanel#</cfoutput>
</cfif>

<div class="contentWrapper" id="searchResults">
  <!--INTRO-->
  <div class="introResults"><span class="label">Results For </span> <span class="accent02Lrg">&quot;<cfoutput>#URL.w#</cfoutput>&quot;</span></div>
  <div class="floatLeftFullWdth">
  	<table width="469" border="0" cellpadding="0" cellspacing="0" class="infoLib">
          <tr>
            <td width="38%" class="hdrBar">CLASS</td>
            <td width="20%" class="hdrBar">DAY</td>
            <td width="22%" class="hdrBar">TIME</td>
            <td width="20%" align="left" class="hdrBar">LOCATION</td>
          </tr>
          <cfoutput query="structClasses.classes">
          <tr>
            <td align="left" valign="top"><a href="index.cfm?md=class&tmp=detail_fitness&fcid=#fitnessClassID#&ref=search&nid=#URL.nid#&tid=#URL.tid#"><b>#classTitle#</b></a></td>
            <td align="left" valign="top">#DayOfWeekAsString(dayOfWeekAsNumber)#</td>
            <td align="left" valign="top">#displayTimeRange(startTime,endTime)#</td>
            <td align="left" valign="top">#location#</td>
          </tr>
          </cfoutput>
    </table>
  </div>
</div>
<!--DISPLAY TOOLS-->
<cfif structClasses.numAllItems GT URL.numItemsPerPage>
  <cfoutput>#navPanel#</cfoutput>
</cfif>