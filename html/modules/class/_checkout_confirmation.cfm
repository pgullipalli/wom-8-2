<cfinclude template="_create_user_session.cfm">
<cfif Not Session.user.isLoggedIn>
  <cflocation url="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=login&next=checkout">
</cfif>
<cfset UT=CreateObject("component", "com.Utility").init()>
<cfset penciledInClasses=Session.user.getPenciledInClasses()>
<cfset penciledInServices=Session.user.getPenciledInServices()>
<cfset donationAmount=Session.user.getDonationAmount()>
<cfset cartTotal=Session.user.getCartTotal()>
<cfset hasPaymentPlanEligibleClass=Session.user.hasPaymentPlanEligibleClass()>
<cfif ArrayLen(Session.user.penciledInClasses) Is 0 AND ArrayLen(Session.user.penciledInServices) Is 0>
  <!--- nothing to check out for --->
  <cflocation url="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=myschedule">
</cfif>

<div id="topperHTMLCodes" style="display:none;">Class Registration</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);    
});
</script>

<style type="text/css">
.section a, .section a:link, .section a:hover {font-size:11px;color:#e3369c; text-decoration:none;float:right;}
.section a:hover {font-size:11px;text-decoration:underline;float:right;}
.itemizedClass .date-range {color:#333132; font-size:11px;}
.class-title {color:#EC6CB8; margin:10px 0px 20px 0px; font-size:12px; padding:0px 15px; font-weight:bold;}
.singleField .formValue {font-size:11px; color:#e3369c; line-height:110%; text-align:left; padding-left:10px; float:left;}
</style>

<cfoutput>
<div class="contentWrapper" id="forms">
  <div class="head">Class Registration Confirmation</div>
  
  <div class="intro">
  Your class registration is not complete yet. Please verify the following information and click on "Submit" to complete the registration.
  </div>
  
  <cfif IsDefined("URL.er") And URL.er>
    <!--- credit card authorization failed --->
    <div class="accent01" style="background-color:##FFFF00; padding:10px 10px; margin:0px 20px 20px 20px; float:left">
      <cfif Not CompareNoCase(Session.user.paymentMethod,"Credit Card")>
        <i><b>Your credit card authorization was not approved. Please <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout">revise your credit card information</a> and try again. If you believe this is a system error, please contact our customer service at 225.924.8444. Thank you!</b></i>
      <cfelse>
        <i><b>Your online check authorization was not approved (#Session.user.message#). Please <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout">revise your bank account information</a> and try again. If you believe this is a system error, please contact our customer service at 225.924.8444. Thank you!</b></i>
      </cfif>
    </div>   
  </cfif>
  
  
  <span class="section">Selected Classes <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormEdit1','','images/btnFormEdito.gif',1)"><img src="images/btnFormEdit.gif" border="0" name="btnFormEdit1" id="btnFormEdit1" /></a></span>
  
  <cfloop index="idx" from="1" to="#ArrayLen(penciledInClasses)#">    
    <div class="itemizedContainer">
      <div class="itemizedClass">
        #penciledInClasses[idx].courseTitle#<br />
        <span class="date-range">
          #DateFormat(penciledInClasses[idx].classInstance[1].startDateTime,"mm.dd.yy")# |
          #LCase(TimeFormat(penciledInClasses[idx].classInstance[1].startDateTime,"h:mm tt"))# - #Lcase(TimeFormat(penciledInClasses[idx].classInstance[1].endDateTime,"h:mm tt"))#
        </span>
      </div>
      <div class="itemizedCost">
        <cfif penciledInClasses[idx].paymentPlanEligible>
		  <cfif penciledInClasses[idx].enrollPaymentPlan><!--- enroll payment plan --->
            #DollarFormat(penciledInClasses[idx].firstInstallment)#<br />
            <span class="accent01"><small>(Remaining Balance: #DollarFormat(penciledInClasses[idx].remainingPayment)#)</small></span>
          <cfelse>
            #DollarFormat(penciledInClasses[idx].cost)#*
          </cfif>
        <cfelse>
          #DollarFormat(penciledInClasses[idx].cost)#
        </cfif>        
      </div>
    </div>
    <cfif penciledInClasses[idx].discountApplied>
      <div class="itemizedContainer">
        <div class="itemizedClass">
          Coupon Code - #penciledInClasses[idx].discountCode#
        </div>
        <div class="itemizedCost">-#DollarFormat(penciledInClasses[idx].discountAmount)#</div>
      </div>
    </cfif>  
  </cfloop>
  
  <cfloop index="idx" from="1" to="#ArrayLen(penciledInServices)#">
    <div class="itemizedContainer">
      <div class="itemizedClass">#penciledInServices[idx].serviceTitle#</div>
      <div class="itemizedCost">#DollarFormat(penciledInServices[idx].cost)#</div>
    </div>
  </cfloop>
  
  <cfif donationAmount GT 0>
    <div class="itemizedContainer">
      <div class="itemizedClass">Donate to Woman's Hospital Foundation</div>
      <div class="itemizedCost">#DollarFormat(donationAmount)#</div>
    </div>
  </cfif>  
  
  <div class="itemizedHdr">
    <div class="itemizedLabTotal">Total:</div>
    <div class="itemizedGrandTotal">#DollarFormat(cartTotal)#</div>
  </div>
  
  <cfif hasPaymentPlanEligibleClass And Session.user.enrollPaymentPlan>
   <div class="itemizedHdr" style="margin:0px 0px 10px 0px;">
      <span class="accent03">
        * You have selected the Payment Plan.
          A class that costs between $400 and $750 qualifies for the 2-Installment Payment Plan. The initial payment will be 1/2 the cost of the class plus $10.
          A class that costs $750 or more qualifies for the 3-Installment Payment Plan. The initial payment will be 1/3 the cost of the class plus $10.
      </span>
   </div>
  </cfif>  

  <div class="floatLeftFullWdth">
  	<div class="unit">
      <span class="section">Attendee Information <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormEdit2','','images/btnFormEdito.gif',1)"><img src="images/btnFormEdit.gif" border="0" name="btnFormEdit2" id="btnFormEdit2" /></a></span>
      <!--- <div class="singleField">
        <label class="descrip">First Name </label>
        <span class="formValue">#Session.user.attendeeFirstName#</span>
      </div>
      <div class="singleField">
        <label class="descrip">Last Name </label>
        <span class="formValue">#Session.user.attendeeLastName#</span>
      </div> --->
      
      <!--- <cfif hasFeePerCoupleClasses>
        <cfset feePerCoupleClassIDList="">
        <cfloop index="idx" from="1" to="#ArrayLen(penciledInClasses)#">
          <cfif penciledInClasses[idx].feePerCouple>                   
            <div class="singleField">
              <label class="descrip">Guest(s) for class <span class="accent01">'#penciledInClasses[idx].courseTitle#'</label>
              <span class="formValue">#penciledInClasses[idx].guestNames#</span>
            </div>
          </cfif>
        </cfloop>        
      </cfif> --->

      <cfloop index="idx" from="1" to="#ArrayLen(penciledInClasses)#">
        <div class="class-title">#penciledInClasses[idx].courseTitle#</div>
        <div class="singleField">
          <label class="descrip">Attendee Name</label>
          <span class="formValue">#penciledInClasses[idx].attendeeName#</span>          
        </div>               
        <cfif penciledInClasses[idx].feePerCouple>            
          <div class="singleField">
            <label class="descrip">Guest(s)</label>
            <span class="formValue">#penciledInClasses[idx].guestNames#</span> 
          </div>           
        </cfif>
      </cfloop>  
      <cfloop index="idx" from="1" to="#ArrayLen(penciledInServices)#">
        <div class="class-title">#penciledInServices[idx].serviceTitle#</div>
        <div class="singleField">
          <label class="descrip">Attendee Name</label>
          <span class="formValue">#penciledInServices[idx].attendeeName#</span>     
        </div>               
        <cfif penciledInServices[idx].feePerCouple>            
          <div class="singleField">
            <label class="descrip">Guest(s)</label>
            <span class="formValue">#penciledInServices[idx].guestNames#</span> 
          </div>           
        </cfif>
      </cfloop>
    </div>    
          
    <div class="unit">
      <span class="section">Contact Information <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormEdit3','','images/btnFormEdito.gif',1)"><img src="images/btnFormEdit.gif" border="0" name="btnFormEdit3" id="btnFormEdit3" /></a></span>
      <div class="singleField">
        <label class="descrip">First Name </label>
        <span class="formValue">#Session.user.contactFirstName#</span>
      </div>
      <div class="singleField">
        <label class="descrip">Last Name </label>
        <span class="formValue">#Session.user.contactLastName#</span>
      </div>
      <div class="singleField">
        <label class="descrip">Address </label>
        <span class="formValue">
          #Session.user.contactAddress1#<br />
          <cfif Compare(Session.user.contactAddress2, "")>
          #Session.user.contactAddress2#<br />
          </cfif>
          #Session.user.contactCity#, #Session.user.contactState# #Session.user.contactZip#
        </span>
      </div>
      <div class="singleField">
        <label class="descrip">Daytime Phone </label>
        <span class="formValue">#Session.user.daytimePhone#</span>
      </div>
      <div class="singleField">
        <label class="descrip">Cell Phone </label>
        <span class="formValue">#Session.user.cellPhone#&nbsp;</span>
      </div>
    </div>
    
    <cfif cartTotal GT 0>
      <div class="unit">
        <span class="section">Payment Information <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormEdit4','','images/btnFormEdito.gif',1)"><img src="images/btnFormEdit.gif" border="0" name="btnFormEdit4" id="btnFormEdit4" /></a></span>        
        <cfif Not CompareNoCase(Session.user.paymentMethod,"Credit Card")>
          <div class="singleField">
            <label for="creditCard" class="descrip">Payment Method</label>
            <span class="formValue">Credit Card</span>
          </div>
          <div class="singleField">
            <label for="creditCard" class="descrip">Card Type</label>
            <span class="formValue">#Session.user.cardType#</span>
          </div>
          <div class="singleField">
            <label for="notifyNameFirst" class="descrip">Name as on Card</label>
            <span class="formValue">#Session.user.nameOnCard#</span>
          </div>
          <div class="singleField">
            <label for="textfield5" class="descrip">Card Number</label>
            <span class="formValue">#Session.user.cardNumber#</span>
          </div>
          <div class="singleField">
            <label for="textfield6" class="descrip">Card Expiration Date</label>
            <span class="formValue">#Session.user.cardExpirationDate#</span>
          </div>
          <div class="singleField">
            <label for="textfield7" class="descrip">Card ID <br />(CVV2/CID) Number</label>
            <span class="formValue">#Session.user.cardCVV2#</span>          
          </div>
        <cfelseif Not CompareNoCase(Session.user.paymentMethod,"Bank Account")>
          <div class="singleField">
            <label for="creditCard" class="descrip">Payment Method</label>
            <span class="formValue">Bank Check</span>
          </div>
          <div class="singleField">
            <label for="notifyNameFirst" class="descrip">Name on Account</label>
            <span class="formValue">#Session.user.nameOnAccount#</span>
          </div>
          <div class="singleField">
            <label for="textfield5" class="descrip">Checking Account Number</label>
            <span class="formValue">#Session.user.accountNumber#</span>
          </div>
          <div class="singleField">
            <label for="textfield6" class="descrip">Bank Routing Number</label>
            <span class="formValue">#Session.user.routingNumber#</span>
          </div>
        <cfelse>
          <div class="singleField"><label for="anonymousYes4" class="descrip">Pay with check or cash in person.</label></div>
        </cfif>  
      </div>
      
      <div class="unit">
        <span class="section">Billing Information <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormEdit5','','images/btnFormEdito.gif',1)"><img src="images/btnFormEdit.gif" border="0" name="btnFormEdit5" id="btnFormEdit5" /></a></span>        
        <div class="singleField">
          <label for="billingNameFirst" class="descrip">First Name</label>
          <span class="formValue">#Session.user.billingFirstName#</span>
        </div>
        <div class="singleField">
          <label for="billingNameLast" class="descrip">Last Name</label>
          <span class="formValue">#Session.user.billingLastName#</span>
        </div>
        <div class="singleField">
          <label for="billingAddress" class="descrip">Address</label>
          <span class="formValue">
            #Session.user.billingAddress1#<br />
            <cfif Compare(Session.user.billingAddress2, "")>
            #Session.user.billingAddress2#<br />
            </cfif>
            #Session.user.billingCity#, #Session.user.billingState# #Session.user.billingZip#
          </span>
        </div>
      </div>
      </cfif>      
      
      <div class="dottedDivider" style="margin-bottom:20px;"></div>
      <div class="unit" style="text-align:center;">
        <a href="#Application.siteURLRootSSL#/action.cfm?RequestTimeout=60&md=class&task=completeRegistration" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormSubmit','','images/btnFormSubmito.gif',1)"><img src="images/btnFormSubmit.gif" alt="Submit" name="btnFormSubmit" width="63" height="17" border="0" id="btnFormSubmit" /></a>
      </div>
  </div>
</div>
</cfoutput>