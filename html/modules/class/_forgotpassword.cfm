<cfinclude template="_create_user_session.cfm">
<cfparam name="URL.email" default="">
<div id="topperHTMLCodes" style="display:none;">Forgot Password?</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);   
});
</script>
<div class="contentWrapper" id="forms">
  <div class="floatLeftFullWdth">
  
    <div class="intro">
    Please enter the email address you used to register with Woman's below and click submit. Your password will be emailed to you.
    </div>
    <cfoutput>
    
    <cfif IsDefined("URL.er") And URL.er>    
    <div class="accent02" style="font-size:11px;background-color:##FFFF00;padding:5px 5px; margin:0px 0px 20px 0px;float:left;">
      <i>The email address you entered can not be found. Please make sure you have entered the correct email address.</i>
    </div> 
    </cfif>
    
    <form id="frmForgotPassword" name="frmForgotPassword" method="post" action="#Application.siteURLRoot#/action.cfm?md=class&task=retrievePassword">
      <div class="unit">
        <div class="singleField">
          <label for="email" class="descrip">E-Mail Address</label>
          <input type="text" name="email" id="email" value="#HTMLEditFormat(URL.email)#" />
        </div>        
      </div>
      <div class="unit">
        <input type="image" src="images/btnFormSubmit.gif" alt="Submit" name="btnFormSubmit" style="width:63px;height:17px; border:0px solid ##ffffff;" border="0" id="btnFormSubmit" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormSubmit','','images/btnFormSubmito.gif',1)" />
      </div>
    </form>
    </cfoutput>
  </div>
</div>
