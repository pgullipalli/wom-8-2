<cfparam name="URL.keyword" default="">
<div class="gryWrapper">
  <div class="intro">
    <p>
      <span class="title">Find a Class</span>
      Woman&rsquo;s offers a large variety of classes. To view classes, please select a method below:
    </p>
    <script type="text/javascript">
	  function submitClassSearch() {
	    var form=document.frmClassSearch; 		
	    if (form.keyword.value=="Search Classes...") {
		  form.keyword.value="";
		  displayInlineMessage('errorMsg','Enter a keyword to search.');
		  form.keyword.focus();
		  return;
		}
	    if (form.keyword.value.length < 3) {
		  displayInlineMessage('errorMsg','Please enter a keyword with at least three characters.');
		  form.keyword.focus();
		  return;
		}
		form.submit();
	  }
	</script>
    <cfoutput>    
    <p>
      <div id="errorMsg" style="margin:0px 10px 10px 0px;padding:5px 5px;color:##000000;background-color:##FFFF00;font-size:11px;display:none;"></div>
      <form name="frmClassSearch" id="frmClassSearch" action="#Application.siteURLRoot#/index.cfm" method="get" onsubmit="return false;" style="margin:0px 0px 5px 0px;">
      <input type="hidden" name="md" value="class" />
      <input type="hidden" name="tmp" value="search_results" />  
      <input type="text" name="keyword" style="width:80px;height:13px;" <cfif Compare(URL.keyword,"")>value="#HTMLEditFormat(URL.keyword)#"<cfelse>value="Search Classes..."</cfif> onclick="resetField(this, 'Search Classes...')" /> <a href="javascript:submitClassSearch()" class="btnMed" style="display:inline;padding:2px 14px 2px 13px;">Search</a><br />
      </form>
      
      <a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=curriculumindex&tid=#Application.template.templateID_Class#"><img src="images/btnClassSuggest.gif" alt="Suggest Classes To Me" width="147" height="18" border="0" /></a><br />
      <a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=categoryindex&tid=#Application.template.templateID_Class#"><img src="images/btnClassBrowse.gif" alt="Browse For Classes" width="147" height="18" border="0" /></a><br />
      <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=myschedule"><img src="images/btnClassView.gif" alt="View Your Classes" width="147" height="18" border="0" /></a><br />     
      
	  <cfif IsDefined("Session.user") And Session.user.isLoggedIn>
      <a href="#Application.siteURLRootSSL#/action.cfm?md=class&task=logout"><img src="images/btnClassViewLogOut.gif" alt="Log out" width="147" height="18" border="0" /></a>
      <cfelse>
      <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=login"><img src="images/btnClassViewLogIn.gif" alt="Log in" width="147" height="18" border="0" /></a>
      </cfif>      
    </p>    
    </cfoutput>
  </div>
</div>