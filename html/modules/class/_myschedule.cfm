<cfprocessingdirective pageencoding="utf-8">
<cfinclude template="_create_user_session.cfm">
<cfsilent>
<cfscript>
  penciledInClasses=Session.user.getPenciledInClasses();//array
  penciledInServices=Session.user.getPenciledInServices();//array
  upcomingClasses=Session.user.getUpcomingClasses();//array
  upcomingServices=Session.user.getUpcomingServices();//array
  completedClasses=Session.user.getCompletedClasses();//array
  completedServices=Session.user.getCompletedServices();//array
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none;">My Schedule</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);    
});

function confirmClassDeletion(obj) {
  var form=document.frmUnconfirmed;
  if (obj.checked) {
    if (confirm('Are you sure you wish to delete this class?')) {
	  form.submit();
	} else {
	  obj.checked = false;
	}
  }
}

function addToCalendar(classInstanceID) {
  window.location="/index.cfm?md=class&tmp=ical_class&nowrap=1&ciid=" + classInstanceID;
}
</script>
<style type="text/css">
a.itemizedClassLnk, a.itemizedClassLnk:link, a.itemizedClassLnk:hover {color:#ec3ca5;padding-top:3px;text-decoration:none;}
a.itemizedClassLnk:hover { text-decoration:underline; }
div.classInstance a {text-decoration:none;}
div.classInstance a:hover {text-decoration:underline;}
</style>

<div class="contentWrapper" id="classes">
  <!--Content-->
  <cfif ArrayLen(penciledInClasses) GT 0 OR ArrayLen(penciledInServices) GT 0 >
  <div>
    <p class="accent02Lrg">Unconfirmed</p>
  </div>
  <div class="scheduleLft">
    <p class="accent03">Below are the classes that you’ve begun the registration process for but haven’t completed.</p>
    
    <!--- <cfif ArrayLen(penciledInClasses) GT 0 OR ArrayLen(penciledInServices) GT 0 >  ---> 
    <cfoutput>
    <form name="frmUnconfirmed" action="#Application.siteURLRootSSL#/action.cfm?md=class&task=removeClassesFromCart" method="post" onsubmit="return false;">  
      <div class="itemizedHdr">
        <div class="itemizedLabDelete">Delete</div>
        <div class="itemizedLabClass">Class</div>
        <div class="itemizedLabCost">Cost</div>
      </div>
      
      <cfloop index="idx" from="1" to="#ArrayLen(penciledInClasses)#">
      <div class="itemizedContainer">
        <div class="itemizedDelete">
          <input type="checkbox" name="classIDList" class="checkbox" value="#penciledInClasses[idx].classID#" onclick="confirmClassDeletion(this);" />
        </div>
        <div class="itemizedClass">
          <a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail&cid=#penciledInClasses[idx].courseID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#penciledInClasses[idx].courseTitle#</a><br />
          <span class="dateRange">
            #DateFormat(penciledInClasses[idx].classInstance[1].startDateTime,"mm.dd.yy")# |
            #LCase(TimeFormat(penciledInClasses[idx].classInstance[1].startDateTime,"h:mmtt"))# -
            #Lcase(TimeFormat(penciledInClasses[idx].classInstance[1].endDateTime,"h:mmtt"))#            
          </span>
        </div>
        <div class="itemizedCost">
          #DollarFormat(penciledInClasses[idx].cost)#<cfif penciledInClasses[idx].paymentPlanEligible>*</cfif>
        </div>
      </div>
      </cfloop>
      
      <cfloop index="idx" from="1" to="#ArrayLen(penciledInServices)#">
      <div class="itemizedContainer">
        <div class="itemizedDelete">
          <input type="checkbox" name="serviceIDList" class="checkbox" value="#penciledInServices[idx].serviceID#" onclick="confirmClassDeletion(this);" />
        </div>
        <div class="itemizedClass"><a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail_service&sid=#penciledInServices[idx].serviceID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#penciledInServices[idx].serviceTitle#</a></div>
        <div class="itemizedCost">#DollarFormat(penciledInServices[idx].cost)#</div>
      </div>
      </cfloop>
      
      
      
      <div class="itemizedHdr">
        <div class="itemizedLabTotal">
          <a href="#Application.siteURLRoot#/index.cfm?md=class&amp;tmp=categoryindex" class="btnMedWhtWide">Find More Classes</a>
        </div>
        <div class="itemizedGrandTotal">#DollarFormat(Session.user.getCartTotal())#</div>
      </div>
      
      <div class="itemizedHdr"><a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout" class="btnFinishReg">Finish Registering</a> </div>
      
      <cfif Session.user.hasPaymentPlanEligibleClass()>
      <div class="itemizedHdr">
        <span class="accent03">
          * A class that costs between $400 and $750 qualifies for the 2-Installment Payment Plan. The initial payment will be 1/2 the cost of the class plus $10.
            A class that costs $750 or more qualifies for the 3-Installment Payment Plan. The initial payment will be 1/3 the cost of the class plus $10.       
        </span>
      </div>
      </cfif>      
    </form>
    </cfoutput>    
  </div>  
  <cfelse>
  <div class="scheduleLft">    
    <cfoutput><a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=home" class="lnkAccent01">VIEW MORE CLASSES &raquo;</a></cfoutput>
  </div>
  </cfif>
  
    
  <cfif Session.user.isLoggedIn>
    <div class="scheduleRt">
      <div><img src="images/imgClassStatus.gif" alt="Status" width="158" height="13" /></div>
      <cfoutput>
      <div class="content">
        <p class="accent02Med">You are currently logged in.</p>
        <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=profile" class="btnGry">Edit Profile</a><br />
        <a href="#Application.siteURLRootSSL#/action.cfm?md=class&task=logout" class="btnGry">Logout</a>        
      </div>
      <cfif Not Session.user.sendReminderEmail>
      <img src="images/imgClassReqRemind.gif" alt="Request Reminders" width="158" height="13" />
      <div class="content">
      <form name="frmClassReminder" id="frmClassReminder" action="#Application.siteURLRootSSL#/action.cfm?md=class&task=updateClassReminder" method="post">
        <p>
        <input type="checkbox" name="sendReminderEmail" id="sendReminderEmail" value="1" />
        Please send me a class reminder via e-mail two days before my class.
        </p>
        <a href="javascript:document.frmClassReminder.submit();" class="btnGry">Submit</a>
      </form>
      </div>
      </cfif>
      </cfoutput>
    </div>
  <cfelse>
    <div class="scheduleRt">
      <div><img src="images/imgClassLogin.gif" alt="Login" width="158" height="13" /></div>
      <div class="content">
        <cfoutput>
        <form name="frmLogin" id="login" action="#Application.siteURLRootSSL#/action.cfm?md=class&task=login" method="post">
          <input type="hidden" name="next" value="myschedule" />
          <label for="email">Email Address</label>
          <input type="text" name="email" id="email" />
          <label for="password">Password</label>
          <input type="password" name="password" id="password" />
          <a href="javascript:document.frmLogin.submit();" class="btnGry">Submit</a>
        </form>
        <p><a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=forgotpassword" class="createAccount">Forgot your password?</a></p>
        <p><a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=register" class="createAccount">Don’t have an account? Create one now »</a></p>
        </cfoutput>
      </div>
    </div>
  </cfif>
</div>

<cfif Session.user.isLoggedIn >
<div class="titleWrapper">
  <div class="floatLeftFullWdth">
    <div class="titleGry">Upcoming Classes</div>
  </div>
</div>

<div class="contentWrapperNoPad">
  <!--Class Instance Labels-->
  
  <cfif ArrayLen(upcomingClasses) GT 0 OR ArrayLen(upcomingServices) GT 0>  
  <div class="classInstanceLabels" align="center">
    <table border="0" cellpadding="4" cellspacing="0">
      <tr>
        <td width="18"><img src="images/imgSpacer.gif" width="18" height="1" /></td>
        <td width="135" align="left" valign="middle" class="accent02"><b>Date(s)</b></td>
        <td width="135" align="left" valign="middle" class="accent02"><b>Time(s)</b></td>
        <td width="78" align="left" valign="middle" class="accent02"><b>Location</b></td>
        <td><img src="images/imgSpacer.gif" width="103" height="1" /></td>
      </tr>
    </table>
  </div>
  <cfset currentTime=now()>
  <cfoutput>
  <cfloop index="idx" from="1" to="#ArrayLen(upcomingClasses)#">  <cfset Variables.classID=upcomingClasses[idx].classID>
  <div class="classInstance" align="center">
    <div class="upcomingTitle">
      <a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail&cid=#upcomingClasses[idx].courseID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#upcomingClasses[idx].courseTitle#</a>      
    </div>
    <cfif ArrayLen(upcomingClasses[idx].classInstance) GT 0>
    <table border="0" cellpadding="4" cellspacing="0" class="classInfo">
      <tr>
        <td width="18"><a href="javascript:toggle('classSchedule#Variables.classID#')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('expand#Variables.classID#','','images/btnClassPluso.gif',1)"><img src="images/btnClassPlus.gif" alt="Expand" name="expand#Variables.classID#" width="18" height="18" border="0" id="expand#Variables.classID#" /></a></td>
        <td width="135" align="left" class="small12"><b>#DateFormat(upcomingClasses[idx].classInstance[1].startDateTime,"mm.dd.yy")#</b></td>
        <td width="135" align="left" class="small12">
          <b>#LCase(TimeFormat(upcomingClasses[idx].classInstance[1].startDateTime,"h:mm tt"))# - #LCase(TimeFormat(upcomingClasses[idx].classInstance[1].endDateTime,"h:mm tt"))#</b>
        </td>
        <td width="78" align="left" class="small10">  
		  <cfif upcomingClasses[idx].classInstance[1].mapID>
            <a href="javascript:openWindow('index.cfm?md=class&tmp=class_location_map&popup=1&lid=#upcomingClasses[idx].classInstance[1].locationID#','map',900,600)">#upcomingClasses[idx].classInstance[1].locationName#</a>
          <cfelse>
            #upcomingClasses[idx].classInstance[1].locationName#
          </cfif>
          &nbsp;
        </td>
        <td width="103">
          <cfif DateCompare(upcomingClasses[idx].classInstance[1].startDateTime, currentTime) GT 0>
          <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=change_classschedule&cid=#upcomingClasses[idx].courseID#&clid=#upcomingClasses[idx].classID#&sclid=#upcomingClasses[idx].studentClassID#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnChange#Variables.classID#','','images/btnChangePinko.gif',1)"><img src="images/btnChangePink.gif" alt="Change schedule" name="btnChange#Variables.classID#" width="103" height="17" border="0" id="btnChange#Variables.classID#" /></a>
          <cfelse>
          &nbsp;
          </cfif>
        </td>
      </tr>
    </table>
    <!--Expanded Info Beginning-->
    <table border="0" cellpadding="4" cellspacing="0" class="classInfo" id="classSchedule#Variables.classID#" style="display:none;">
      <cfloop index="idx2" from="1" to="#ArrayLen(upcomingClasses[idx].classInstance)#">
      <tr>
        <td width="18">&nbsp;</td>
        <td width="135" align="left" class="detailsSm11"><table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td class="pdRt">#DayOfWeekAsString(upcomingClasses[idx].classInstance[idx2].dayOfWeekAsNumber)#</td>
              <td>#DateFormat(upcomingClasses[idx].classInstance[idx2].startDateTime,"mm.dd.yy")#</td>
            </tr>
          </table></td>
        <td width="135" align="left" class="detailsSm11">
          #LCase(TimeFormat(upcomingClasses[idx].classInstance[idx2].startDateTime,"h:mm tt"))# - #LCase(TimeFormat(upcomingClasses[idx].classInstance[idx2].endDateTime,"h:mm tt"))#
        </td>
        <td width="189" align="left" class="detailsSm10">
          <div class="scheduleLocation">
            <cfif upcomingClasses[idx].classInstance[idx2].mapID>
              <a href="javascript:openWindow('index.cfm?md=class&tmp=class_location_map&popup=1&lid=#upcomingClasses[idx].classInstance[idx2].locationID#','map',900,600)">#upcomingClasses[idx].classInstance[idx2].locationName#</a>
            <cfelse>
              #upcomingClasses[idx].classInstance[idx2].locationName#
            </cfif>
            &nbsp;          
          </div>
          <div class="calIcon"><a href="javascript:addToCalendar('#upcomingClasses[idx].classInstance[idx2].classInstanceID#');"><img src="images/imgAddToCalIcon.gif" alt="Add to Calendar" title="Add to Calendar" width="15" height="16" border="0" /></a></div></td>
      </tr>
      </cfloop>
      <tr valign="top">
        <td colspan="3">&nbsp;</td>
        <td class="detailsSm10" align="left">Attendee: #upcomingClasses[idx].attendeeName#</td>
      </tr>
    </table>
    </cfif>
    <!--Expanded Info End-->
  </div>
  </cfloop>
  <cfloop index="idx" from="1" to="#ArrayLen(upcomingServices)#">
  <div class="classInstance" align="center">
    <div class="upcomingTitle">
      <a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail_service&sid=#upcomingServices[idx].serviceID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#upcomingServices[idx].serviceTitle#</a>
    </div>
    
    <table border="0" cellpadding="4" cellspacing="0" class="classInfo">
      <tr>
        <td width="18">&nbsp;</td>
        <td width="135" align="left" class="small12">--</td>
        <td width="135" align="left" class="small12">--</td>
        <td width="78" align="left" class="small10">
          <cfif upcomingServices[idx].mapID>
            <a href="javascript:openWindow('index.cfm?md=class&tmp=class_location_map&popup=1&lid=#upcomingServices[idx].locationID#','map',900,600)">#upcomingServices[idx].locationName#</a>
          <cfelse>
            #upcomingServices[idx].locationName#
          </cfif>
          &nbsp;          
        </td>
        <td width="103">&nbsp;</td>
      </tr>
    </table>    
  </div>
  </cfloop>  
  </cfoutput>    
  <cfelse>
    <div style="padding:20px 0px; text-align:center;">
      <span class="accent01">No upcoming classes at this time.</span>
    </div> 
  </cfif>
  <!--Class Instance-->
  <div class="classInstance" align="center"></div>
</div>

<div class="contentWrapper" id="classes">
  <div class="floatLeftFullWdth">
    <div class="titleGry">Completed Classes</div>
    
    <cfif ArrayLen(completedClasses) GT 0 OR ArrayLen(completedServices) GT 0>
    <cfoutput>
    <cfloop index="idx" from="1" to="#ArrayLen(completedClasses)#">    
    <ul class="classSchedule">
      <li>
      <span class="accent02Med">
      <a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail&cid=#completedClasses[idx].courseID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#completedClasses[idx].courseTitle#</a>
      </span> <br />
      <cfif ArrayLen(completedClasses[idx].classInstance) GT 0>
      <b>
      #DateFormat(completedClasses[idx].classInstance[1].startDateTime,"mm.dd.yy")#  |
      #LCase(TimeFormat(completedClasses[idx].classInstance[1].startDateTime,"h:mm tt"))# - #LCase(TimeFormat(completedClasses[idx].classInstance[1].endDateTime,"h:mm tt"))#
      </b><br />      
      </cfif>
        
        <cfif Not completedClasses[idx].feedbackSent>
          <div class="btnWht">
            <div class="btnWhtCap"><img src="images/imgBtnWhtLft.gif" width="7" height="19" /></div>
            <a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=submit_review&type=course&stid=#Session.user.studentID#&id=#completedClasses[idx].classID#" class="btnWht">Give Feedback »</a>
            <div class="btnWhtCap"><img src="images/imgBtnWhtRt.gif" width="7" height="19" /></div>
          </div>
        <cfelse>
          <span class="feedbackThnks">Thanks for submitting feedback!</span>
        </cfif>        
        <br clear="left" />
      </li>
    </ul>
    </cfloop>
   
    <cfloop index="idx" from="1" to="#ArrayLen(completedServices)#">
    <ul class="classSchedule">
      <li>
      <span class="accent02Med">
      <a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail_service&sid=#completedServices[idx].serviceID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#completedServices[idx].serviceTitle#</a>
      </span> <br />        
       
      <cfif Not completedServices[idx].feedbackSent>
      <div class="btnWht">
        <div class="btnWhtCap"><img src="images/imgBtnWhtLft.gif" width="7" height="19" /></div>
          <a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=submit_review&type=service&stid=#Session.user.studentID#&id=#completedServices[idx].serviceID#" class="btnWht">Give Feedback »</a>
        <div class="btnWhtCap"><img src="images/imgBtnWhtRt.gif" width="7" height="19" /></div>
      </div>
      <cfelse>
      <span class="feedbackThnks">Thanks for submitting feedback!</span>
      </cfif>        
      <br clear="left" />
      </li>
    </ul>
    </cfloop>
    </cfoutput>
    <cfelse>
      <div style="padding:20px 0px; text-align:center;">
        <span class="accent01">No completed classes at this time.</span>
      </div>
    </cfif>
  </div>
</div>
</cfif>