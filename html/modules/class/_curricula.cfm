<cfparam name="URL.cuid">
<!--- curriculaID --->
<cfsilent>
<cfscript>
  CL=CreateObject("component","com.Class").init();
  curriculaInfo=CL.getCurriculaByID(URL.cuid);
  curriculaCourses=CL.getCurriculaCourses(URL.cuid);
  curriculaServices=CL.getCurriculaServices(URL.cuid);
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none;"><cfif curriculaInfo.recordcount GT 0><cfoutput>#curriculaInfo.curriculaTitle#</cfoutput><cfelse>Curriculum</cfif></div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);  
});

function addClassesToCart() {
  var form = document.frmCurricula;
  var found=false;
  for (var i=0; i < form.elements.length; i++) {
    if (form.elements[i].type == "radio") 
	  if (form.elements[i].checked && form.elements[i].value != "0") found=true;
	
  }
  if (found) form.submit();
  else alert("Please select at least one class. Thanks.");
}
</script>
<style type="text/css">
div.classInstanceCurric a {text-decoration:none;}
div.classInstanceCurric a:hover {text-decoration:underline;}
</style>

<cfif curriculaInfo.recordcount GT 0>
<div class="contentWrapper">  
  <!--INTRO-->
  <div class="introNoBottBorder"><cfoutput>#Replace(curriculaInfo.longDescription, Chr(13) & Chr(10), "<br />", "All")#</cfoutput></div>
</div>
<cfoutput>
<div class="contentWrapperNoPad">

  <div style="margin:20px 10px;" class="accent03">To view a description of the course, select the course name. To view instances of the course, select 'View Class Availability'.</div>

  <div class="availClasses">Suggested Classes</div>  
  <form name="frmCurricula" id="frmCurricula" action="#Application.siteURLRootSSL#/action.cfm?md=class&task=addCurriculaClassesToCart" method="post" onsubmit="return false;">
    <!--- BEGIN: COURSES --->
    <cfloop query="curriculaCourses"> <cfset classInfo=CL.getClassesByCourseID(courseID)>    
    <div class="classInstanceCurric" align="center">
      <div class="classHdr">
        <a href="index.cfm?md=class&tmp=detail&cid=#curriculaCourses.courseID#" class="className floatLeft">#courseTitle#</a>
        <a href="javascript:toggle('allClasses#curriculaCourses.courseID#')" class="floatRight lnkAccent01SmCaps">View Class Availability</a>
      </div>
      <div class="floatLeftFullWdth" id="allClasses#curriculaCourses.courseID#" style="display:none;">
        <cfif Not curriculaCourses.registerOnline>
          <table width="100%" border="0" cellpadding="4" cellspacing="0" class="classInfo">
            <tr>
              <td align="center" valign="middle">
                <cfif Compare(curriculaCourses.registrationURL,"")>
                  <a href="#curriculaCourses.registrationURL#" target="_blank">Register</a>
                <cfelse>
                  &nbsp;
                </cfif>
              </td>
            </tr>
          </table>
        <cfelseif classInfo.numClasses GT 0>
          <cfloop index="idx1" from="1" to="#classInfo.numClasses#"> <cfset Variables.classID=classInfo.class[idx1].classID>
            <table width="100%" border="0" cellpadding="4" cellspacing="0" class="classInfo">
              <tr>
                <td width="30" align="center" valign="middle">
                  <!--- available for registration only if it is: 1. registerOnline=1 2. numParticipants GTE regMaximum --->
                  <cfif curriculaCourses.callForPrice>
                    &nbsp;
                  <cfelseif classInfo.class[idx1].numParticipants GTE classInfo.class[idx1].regMaximum>
                    Full          
                  <cfelse>
                   <input type="radio" name="classID#curriculaCourses.courseID#" value="#classInfo.class[idx1].classID#" />
            	  </cfif>                  
                </td>
                <td width="135" align="left" class="small12"><b>#DateFormat(classInfo.class[idx1].classInstance[1].startDateTime,"mm.dd.yy")#</b></td>
                <td width="135" align="left" class="small12">
                  <b>#LCase(TimeFormat(classInfo.class[idx1].classInstance[1].startDateTime,"h:mm tt"))# - #LCase(TimeFormat(classInfo.class[idx1].classInstance[1].endDateTime,"h:mm tt"))#</b>
                </td>
                <td align="left" class="small10">
                  <cfif Compare(classInfo.class[idx1].classInstance[1].locationName,"")>
                    <cfif classInfo.class[idx1].classInstance[1].mapID>
                      <a href="javascript:openWindow('index.cfm?md=class&tmp=class_location_map&popup=1&lid=#classInfo.class[idx1].classInstance[1].locationID#','map',900,600)">#classInfo.class[idx1].classInstance[1].locationName#</a>
                    <cfelse>
                      #classInfo.class[idx1].classInstance[1].locationName#
                    </cfif>
				  <cfelse>
                    &nbsp;
				  </cfif>
                </td>
                <td width="18"><a href="javascript:toggle('classSchedule#Variables.classID#')" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('expand#Variables.classID#','','images/btnClassPluso.gif',1)"><img src="images/btnClassPlus.gif" alt="Expand" name="expand#Variables.classID#" width="18" height="18" border="0" id="expand#Variables.classID#" /></a></td>
              </tr>
            </table>
            <div id="classSchedule#Variables.classID#" style="display:none;">
            <table width="100%" border="0" cellpadding="4" cellspacing="0" class="classInfo">  
              <tr>
                <td width="30" align="center" valign="top">&nbsp;</td>
                <td align="left" valign="top">                  
                  <table width="100%" border="0" cellpadding="4" cellspacing="0" class="classInfo">
                    <cfloop index="idx2" from="1" to="#classInfo.class[idx1].numClassInstances#">
                    <tr>
                      <td width="135" align="left" class="detailsSm11"><table border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td class="pdRt">#DayOfWeekAsString(classInfo.class[idx1].classInstance[idx2].dayOfWeekAsNumber)#</td>
                            <td>#DateFormat(classInfo.class[idx1].classInstance[idx2].startDateTime,"mm.dd.yy")#</td>
                          </tr>
                        </table></td>
                      <td width="135" align="left" class="detailsSm11">
						#LCase(TimeFormat(classInfo.class[idx1].classInstance[idx2].startDateTime,"h:mm tt"))# - #LCase(TimeFormat(classInfo.class[idx1].classInstance[idx2].endDateTime,"h:mm tt"))#
                      </td>
                      <td align="left" class="detailsSm10">
                        <cfif Compare(classInfo.class[idx1].classInstance[idx2].locationName,"")>
                          <cfif classInfo.class[idx1].classInstance[idx2].mapID>
                            <a href="javascript:openWindow('index.cfm?md=class&tmp=class_location_map&popup=1&lid=#classInfo.class[idx1].classInstance[idx2].locationID#','map',900,600)">#classInfo.class[idx1].classInstance[idx2].locationName#</a>
                          <cfelse>
                            #classInfo.class[idx1].classInstance[idx2].locationName#
                          </cfif>
                        <cfelse>
                          &nbsp;
                        </cfif>
                      </td>
                    </tr>
                    </cfloop>                    
                  </table>                  
                </td>
              </tr>
            </table>  
            </div>              
          </cfloop>
          <table width="100%" border="0" cellpadding="4" cellspacing="0" class="classInfo">    
            <tr>
              <td width="30" align="center" valign="middle"><input type="radio" name="classID#curriculaCourses.courseID#" value="0" checked /></td>
              <td align="left" class="small12"><b>I do not wish to take this class</b></td>
            </tr>
          </table>
        <cfelse>
          <table width="100%" border="0" cellpadding="4" cellspacing="0" class="classInfo">   
            <tr>
              <td align="center" class="small12"><b>No classes available at this time.</b></td>
            </tr>
          </table>
        </cfif>        
      </div>
    </div>
    </cfloop>
    <!--- END: COURSES --->
    
    <!--- BEGIN: SERVICES --->
    <cfloop query="curriculaServices">  
    <div class="classInstanceCurric" align="center">
      <div class="classHdr">
        <a href="index.cfm?md=class&tmp=detail_service&sid=#curriculaServices.serviceID#" class="className floatLeft">#serviceTitle#</a>
        <a href="javascript:toggle('service#curriculaServices.serviceID#')" class="floatRight lnkAccent01SmCaps">View Class Availability</a>
      </div>
      <div class="floatLeftFullWdth" id="service#curriculaServices.serviceID#" style="display:none;">
        <table width="100%" border="0" cellpadding="4" cellspacing="0" class="classInfo">    
          <tr>
            <td width="30" align="center" valign="middle"><input type="radio" name="serviceID#curriculaServices.serviceID#" value="#curriculaServices.serviceID#" /></td>
            <td align="left" class="small12"><b>I do not wish to take this class</b></td>
          </tr>
        </table>            

        <table width="100%" border="0" cellpadding="4" cellspacing="0" class="classInfo">    
          <tr>
            <td width="30" align="center" valign="middle"><input type="radio" name="serviceID#curriculaServices.serviceID#" value="0" checked /></td>
            <td align="left" class="small12"><b>I do not wish to take this class</b></td>
          </tr>
        </table>       
      </div>
    </div>
    </cfloop>
    <!--- END: SERVICES --->
  </form>
</div>
<div class="contentWrapper">
  <a href="javascript:addClassesToCart()" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAddClasses','','images/btnAddClasseso.gif',1)"><img src="images/btnAddClasses.gif" alt="Add Selected Class(es) to my Schedule" name="btnAddClasses" width="252" height="17" border="0" id="btnAddClasses" /></a>
</div>
</cfoutput>
<cfelse>
<div class="contentWrapper">
  <p class="accent01" style="font-size:11px;text-align:center;">Sorry, but the curricula you are looking for is not available.</p>
</div>
</cfif>


