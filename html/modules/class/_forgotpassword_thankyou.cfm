<cfinclude template="_create_user_session.cfm">
<cfparam name="URL.email" default="">
<div id="topperHTMLCodes" style="display:none;">Forgot Password?</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);   
});
</script>
<div class="contentWrapper" id="forms">
  <div class="floatLeftFullWdth">
  
    <div class="accent03" style="font-size:11px;">
      <br /><b>Thank you! Your password has been sent to your email address: <br /><br />
      <span class="accent02"><cfoutput>#URL.email#</cfoutput>.</span></b>
    </div>
  </div>
</div>
