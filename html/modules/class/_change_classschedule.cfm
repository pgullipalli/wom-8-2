<cfinclude template="_create_user_session.cfm">
<cfparam name="URL.cid"><!--- courseID --->
<cfparam name="URL.clid"><!--- classID --->
<cfparam name="URL.sclid"><!--- studentClassID; primary key of 'cl_student_class' table --->
<cfsilent>
<cfscript>
  CL=CreateObject("component","com.Class").init();  
  courseInfo=CL.getCourseByID(URL.cid);  
  classInfo=CL.getClassesByCourseID(URL.cid);  
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none;">Change Class Schedule</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);   
});
</script>
<style type="text/css">
a.itemizedClassLnk, a.itemizedClassLnk:link, a.itemizedClassLnk:hover {color:#ec3ca5;padding-top:3px;text-decoration:none;}
a.itemizedClassLnk:hover { text-decoration:underline; }
</style>

<div class="contentWrapper" id="classes">
  <!--Content-->
  <span class="head"><cfoutput>#courseInfo.courseTitle#</cfoutput></span>
</div>

<div class="contentWrapperNoPad">
<cfif courseInfo.recordcount GT 0 And classInfo.numClasses GT 0>
  <div class="availClasses">Available Classes</div>
  <!--Class Instance Labels-->
  <div class="classInstanceLabels" align="center">
    <table border="0" cellpadding="4" cellspacing="0">
      <tr>
        <td width="18"><img src="images/imgSpacer.gif" width="18" height="1" /></td>
        <td width="135" align="left" valign="middle" class="accent02"><b>Date(s)</b></td>
        <td width="135" align="left" valign="middle" class="accent02"><b>Time(s)</b></td>
        <td width="78" align="left" valign="middle" class="accent02"><b>Location</b></td>
        <td><img src="images/imgSpacer.gif" width="103" height="1" /></td>
      </tr>
    </table>
  </div>
  
  <!--Class Instance-->
  <cfoutput>
  <form name="frmChangeSchedule" action="#Application.siteURLRootSSL#/action.cfm?md=class&task=changeClassSchedule" method="post" onsubmit="return false;">
  <input type="hidden" name="oldClassID" value="#URL.clid#">
  <input type="hidden" name="studentClassID" value="#URL.sclid#" />
  <cfloop index="idx1" from="1" to="#classInfo.numClasses#"> <cfset Variables.classID=classInfo.class[idx1].classID>
  <div class="classInstance" align="center">
    <table border="0" cellpadding="4" cellspacing="0" class="classInfo">
      <tr>
        <td width="18">
          <cfif classInfo.class[idx1].numParticipants GTE classInfo.class[idx1].regMaximum>
          <b>FULL</b>
          <cfelse>
          <input type="radio" name="classID" value="#Variables.classID#" <cfif Not Compare(URL.clid, Variables.classID)>checked</cfif> />
          </cfif>
        </td>
        <td width="135" align="left" class="small12"><b>#DateFormat(classInfo.class[idx1].classInstance[1].startDateTime,"mm.dd.yy")#</b></td>
        <td width="135" align="left" class="small12">
          <b>#LCase(TimeFormat(classInfo.class[idx1].classInstance[1].startDateTime,"h:mm tt"))# - #LCase(TimeFormat(classInfo.class[idx1].classInstance[1].endDateTime,"h:mm tt"))#</b>          
        </td>
        <td width="78" align="left" class="small10">
		  <cfif Compare(classInfo.class[idx1].classInstance[1].locationName,"")>
            <cfif classInfo.class[idx1].classInstance[1].mapID>
              <a href="javascript:openWindow('index.cfm?md=class&tmp=class_location_map&popup=1&lid=#classInfo.class[idx1].classInstance[1].locationID#','map',900,600)">#classInfo.class[idx1].classInstance[1].locationName#</a>
            <cfelse>
              #classInfo.class[idx1].classInstance[1].locationName#
            </cfif>
		  <cfelse>
            &nbsp;
		  </cfif></td>
        <td width="103">
          <a href="javascript:toggle('classSchedule#Variables.classID#')" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('expand#Variables.classID#','','images/btnClassPluso.gif',1)"><img src="images/btnClassPlus.gif" alt="Expand" name="expand#Variables.classID#" width="18" height="18" border="0" id="expand#Variables.classID#" /></a>
        </td>
      </tr>
    </table>
    <!--Expanded Info Beginning-->
    <div id="classSchedule#Variables.classID#" style="display:none;">
    <table border="0" cellpadding="4" cellspacing="0" class="classInfo">
      <cfloop index="idx2" from="1" to="#classInfo.class[idx1].numClassInstances#">
      <tr>
        <td width="18">&nbsp;</td>
        <td width="135" align="left" class="detailsSm11"><table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td class="pdRt">#DayOfWeekAsString(classInfo.class[idx1].classInstance[idx2].dayOfWeekAsNumber)#</td>
              <td>#DateFormat(classInfo.class[idx1].classInstance[idx2].startDateTime,"mm.dd.yy")#</td>
            </tr>
          </table></td>
        <td width="135" align="left" class="detailsSm11">
          #LCase(TimeFormat(classInfo.class[idx1].classInstance[idx2].startDateTime,"h:mm tt"))# - #LCase(TimeFormat(classInfo.class[idx1].classInstance[idx2].endDateTime,"h:mm tt"))#
        </td>
        <td width="189" align="left" class="detailsSm10">
          <cfif Compare(classInfo.class[idx1].classInstance[idx2].locationName,"")>            
            <cfif classInfo.class[idx1].classInstance[idx2].mapID>
              <a href="javascript:openWindow('index.cfm?md=class&tmp=class_location_map&popup=1&lid=#classInfo.class[idx1].classInstance[idx2].locationID#','map',900,600)">#classInfo.class[idx1].classInstance[idx2].locationName#</a>
            <cfelse>
              #classInfo.class[idx1].classInstance[idx2].locationName#
            </cfif>
          <cfelse>
            &nbsp;
          </cfif>
        </td>
      </tr>
      </cfloop>	  
      <tr>
        <td>&nbsp;</td>
        <td colspan="3" align="left" class="detailsSm11">
          <div class="notes">
          <cfif Compare(classInfo.class[idx1].notes,"")>
            <span class="labelAccent02">Notes:</span><br />
            #classInfo.class[idx1].notes#<br /><br />
          </cfif>
          </div>
        </td>
      </tr>
    </table>    
    </div>   
    <!--Expanded Info End-->    
  </div>
  </cfloop>
  <div align="center">
    <table border="0" cellpadding="4" cellspacing="0" class="classInfo">
      <tr valign="middle">
        <td align="center"><br />
          <a href="javascript:document.frmChangeSchedule.submit();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnChange','','images/btnChangePinko.gif',1)"><img src="images/btnChangePink.gif" alt="Change schedule" name="btnChange" width="103" height="17" border="0" id="btnChange" /></a>
        </td>
      </tr>
    </table>
  </div>
  </form>
  </cfoutput>
<cfelse>
    <div class="classInstance" align="center">
      <br />No classes available at this time.<br /><br />
    </div>
</cfif>
</div>