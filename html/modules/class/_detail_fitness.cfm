<cfparam name="URL.fcid"><!--- fitnessClassID --->
<cfsilent>
<cfscript>
  CL=CreateObject("component","com.Class").init();
  fitnessClassInfo=CL.getFitnessClassBasicInfo(URL.fcid);
  fitnessClassTimeInfo=CL.getFitnessClassTimeInfo(URL.fcid);
</cfscript>
</cfsilent>
<div id="topperHTMLCodes1" style="display:none;"><span>Fitness Club</span></div>
<div id="topperHTMLCodes2" style="display:none;"><cfoutput><a href="index.cfm?md=class&tmp=schedule_fitness&tid=#URL.tid#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnTpBarReturnSched','','images/btnTpBarReturnSchedo.gif',1)"><img src="images/btnTpBarReturnSched.gif" alt="Return To Schedule" name="btnTpBarReturnSched" width="145" height="24" border="0" id="btnTpBarReturnSched" /></a><span>Fitness Club</span></cfoutput></div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtTowelAroundNeck.jpg" width="202" height="244" /></div>
<script type="text/javascript">
  $(document).ready(function(){
    var url=window.location.href;
    if (url.indexOf("ref=schedule") != -1) {
      $("#titleBar").attr("id","titleBarWithBtnsText").html($("#topperHTMLCodes2").remove().html());
	} else {
	  $("#titleBar").attr("id","titleBarWithBtnsText").html($("#topperHTMLCodes1").remove().html());
	}

    $("#thmemImage").html($("#themeImageHTMLCodes").html());  
  });
  
  function addToCalendar(fitnessClassTimeID) {
	window.location="index.cfm?md=class&tmp=ical_fitness_class&nowrap=1&fctid=" + fitnessClassTimeID;
  }
</script>

<div class="contentWrapper" id="classes">  
  <!--Class Details-->
  <cfoutput query="fitnessClassInfo">
  <span class="head">#classTitle#</span>
  <div class="detailLft">
    <cfif Compare(fullsizeImage,"")>
    <div class="thmbnail"><div class="floatLeft"><img src="#fullsizeImage#" width="148" align="left" class="phtStroke" /></div><cfif Compare(imageCaption,"")><div class="caption">#imageCaption#</div></cfif></div>
    </cfif>
    
    #description#
    <cfif Compare(moreInfoURL,"")>
    <p><a href="#moreInfoURL#" class="lnkAccent03Caps"<cfif FindNoCase("index.cfm?md=",moreInfoURL) GT 0> target="_blank"</cfif>>MORE INFO...</a></p>
    </cfif>
  </div>
  <div class="detailRt">
    <div class="content">
      <cfif Compare(instructorName,"")>
      <p><span class="labelAccent02">Instructor</span> <span class="med14">#instructorName#</span></p>
      </cfif>
      <cfif Compare(location,"")>
      <p><span class="labelAccent02">Location</span> <span class="med14">#location#</span></p>
      </cfif>      
      <p><span class="labelAccent02">Dates / Times</span>
      <cfloop query="fitnessClassTimeInfo">
      <div class="addtoCalItem">
        <div class="floatLeft" style="width:100px;">
          <b>#DayOfWeekAsString(dayOfWeekAsNumber)#</b><br />
          #LCase(TimeFormat(startTime,"h:mm tt"))#
        </div>
        <div class="calIcon"><a href="javascript:addToCalendar('#fitnessClassTimeID#');"><img src="images/imgAddToCalIcon.gif" alt="Add to Calendar" title="Add to Calendar" width="15" height="16" border="0" /></a></div>
      </div>      
      </cfloop>
      </p>        
    </div>
  </div>
  <!--Additional Info-->
  <cfif Compare(specialInstructions,"")>
  <div class="additionalInfo">
    <p>
      <span class="labelAccent02">Special Instructions</span><br />
      #specialInstructions#
    </p>
  </div>
  </cfif>
  </cfoutput>
</div>