<cfparam name="URL.dn" default="#dayOfWeek(now())#"><!--- day of week: 1: Sun, 2: Mon, ... --->
<cfsilent>
<cffunction name="displayTimeRange" access="public" output="no" returntype="string">
  <cfargument name="sTime" type="any" required="yes">
  <cfargument name="eTime" type="any" required="yes">
  
  <cfif TimeFormat(Arguments.sTime,"h:mm tt") IS "12:00 PM">
    <cfreturn "Noon-" & LCase(TimeFormat(Arguments.eTime,"h:mm tt"))>
  <cfelse>
    <cfreturn TimeFormat(Arguments.sTime,"h:mm") & "-" & LCase(TimeFormat(Arguments.eTime,"h:mm tt"))>
  </cfif>
</cffunction>

<cfscript>
  CL=CreateObject("component","com.Class").init();
  fitnessClasses=CL.getFitnessClassesByDayNumber(URL.dn);  
</cfscript>
</cfsilent>
<div id="topperHTMLCodes" style="display:none;">Fitness Club</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtTowelAroundNeck.jpg" width="202" height="244" /></div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  $("#frmFitnessClassSearch select[name='dn']").change(function() {
	var dn=$("#frmFitnessClassSearch select[name='dn']").val();
	var nid=$("#frmFitnessClassSearch input[name='nid']").val();
	var tid=$("#frmFitnessClassSearch input[name='tid']").val();
	if (dn > 0) {
	  var url="index.cfm?md=class&tmp=schedule_fitness&dn=" + dn + "&nid=" + nid + "&tid=" + tid;
	  window.location = url;
	}
  });  
});

function searchFitnessClasses() {
  var form = document.frmFitnessClassSearch;
  var nid=form.nid.value;
  var tid=form.tid.value;
  if (form.keyword.value.length < 2) {
    alert("Please enter a keyword with at least two characters.");
	form.keyword.focus();
	return;
  }
  var url = "index.cfm?md=class&tmp=searchresults_fitness&w=" + escape(form.keyword.value) + "&nid=" + nid + "&tid=" + tid;
  window.location = url;
}

function resetKeywords(obj, str) {
  if (obj.value == str) obj.value = "";
}
</script>

<div class="contentWrapperIndent">
  <!--CALENDAR-->
  <div class="intro">Welcome to Woman's Fitness Club Members' class calendar. Click on a class to find out more!</div>
  <div class="weekDay">
    <div class="calSelect">
      <form id="frmFitnessClassSearch" name="frmFitnessClassSearch" onSubmit="return false;">
        <cfoutput>
        <input type="hidden" name="nid" value="#URL.nid#">	
        <input type="hidden" name="tid" value="#URL.tid#">		
		</cfoutput>
        <a href="javascript:searchFitnessClasses()" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnSmallGry','','images/btnSmallGryo.gif',1)"><img src="images/btnSmallGry.gif" alt="Submit" name="btnSmallGry" width="20" height="18" border="0" class="submit" id="btnSmallGry" /></a>
        
        <input name="keyword" type="text" id="keyword" value="Search All Classes..." size="20" onClick="resetKeywords(this,'Search All Classes...')" />
        <span>&nbsp; | or | &nbsp;</span>
        
        <select name="dn" id="dn">
          <option value="0">Select another day:</option>
          <option value="2"<cfif Not Compare(URL.dn,"2")> selected</cfif>>Monday</option>
          <option value="3"<cfif Not Compare(URL.dn,"3")> selected</cfif>>Tuesday</option>
          <option value="4"<cfif Not Compare(URL.dn,"4")> selected</cfif>>Wednesday</option>
          <option value="5"<cfif Not Compare(URL.dn,"5")> selected</cfif>>Thursday</option>
          <option value="6"<cfif Not Compare(URL.dn,"6")> selected</cfif>>Friday</option>
          <option value="7"<cfif Not Compare(URL.dn,"7")> selected</cfif>>Saturday</option>
          <option value="1"<cfif Not Compare(URL.dn,"1")> selected</cfif>>Sunday</option>
        </select>
      </form>
    </div>    
    <span><cfoutput>#DayOfWeekAsString(URL.dn)#</cfoutput></span>
  </div>
    
  <div class="floatLeftFullWdth">
    <table border="0" cellspacing="1" cellpadding="0" class="calendar">
      <tr>
        <th class="classCat">Aerobics <span class="location">( Studio 1 )</span></th>
        <th class="classCat">Gym Classes <span class="location">( Gym )</span></th>
        <th class="classCat">Mind/body <span class="location">( Studio 3 )</span></th>
        <th class="classCat">Aquatics <span class="location">( Pool )</span></th>
      </tr>
      <cfset classTypeList="Aerobics,Gym Classes,Mind/Body,Aquatics">
      <tr>
        <cfoutput>
        <cfloop index="idx" list="#classTypeList#">
        <td>
          <ul>
            <cfset cType=idx>
            <cfloop query="fitnessClasses">
              <cfif Not Compare(classType,cType)>
                <li><span class="time">#displayTimeRange(startTime,endTime)#</span><a href="index.cfm?md=class&tmp=detail_fitness&fcid=#fitnessClassID#&ref=schedule&nid=#URL.nid#&tid=#URL.tid#">#classTitle#</a></li>
              </cfif>
            </cfloop>
          </ul>
        </td>
        </cfloop>
        </cfoutput>
      </tr>
    </table>
  </div>
  <div class="calPrintSched"><a href="javascript:openWindow('http://www.womans.org/index.cfm?md=class&tmp=schedule_fitness&printer=1','printer',770,600)" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnPrintSchedule','','images/btnPrintScheduleo.gif',1)"><img src="images/btnPrintSchedule.gif" alt="Print This Schedule" name="btnPrintSchedule" width="147" height="17" border="0" id="btnPrintSchedule" /></a></div>
</div>
