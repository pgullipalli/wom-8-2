<cfprocessingdirective pageencoding="utf-8">
<cfparam name="URL.sid">
<cfparam name="URL.startIndex" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfsilent>
<cfscript>
  UT=CreateObject("component","com.Utility").init();
  CL=CreateObject("component","com.Class").init();
  serviceInfo=CL.getServiceByID(URL.sid);
  structReviews=CL.getReviewsByServiceID(serviceID=URL.sid, startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage);
  newline=Chr(13) & Chr(10);
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none;">Reviews</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);  
});
</script>

<cfif structReviews.numAllItems GT URL.numItemsPerPage>
  <cfset navPanel=UT.displayNextPreviousLinks(pageURL="index.cfm?md=class&tmp=servicereviews&sid=#URL.sid#", startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage, numDisplayedItems=structReviews.numDisplayedItems, numAllItems=structReviews.numAllItems, itemName="Results")>
  <cfoutput>#navPanel#</cfoutput>
</cfif>

<div class="contentWrapper" id="reviews">
  <div class="introNoBottBorder">Here’s what recent participants had to say about “<cfoutput>#serviceInfo.serviceTitle#</cfoutput>”.</div>
  <div class="floatLeftFullWdth">
    <ul class="smArrow">
      <cfoutput query="structReviews.reviews">   
      <li>
        “#Replace(review, newline, "<br />", "All")#”<br />
        <span class="small11 accent01">#fullName#</span>
      </li>
      </cfoutput>
    </ul>
  </div>
</div>
<!--DISPLAY TOOLS-->
<cfif structReviews.numAllItems GT URL.numItemsPerPage>
  <cfoutput>#navPanel#</cfoutput>
</cfif>
