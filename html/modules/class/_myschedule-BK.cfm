<cfinclude template="_create_user_session.cfm">

<cfset penciledInClasses=Session.user.getPenciledInClasses()><!--- array --->
<cfset penciledInServices=Session.user.getPenciledInServices()><!--- array --->
<cfset upcomingClasses=Session.user.getUpcomingClasses()><!--- array --->
<cfset upcomingServices=Session.user.getUpcomingServices()><!--- array --->
<cfset completedClasses=Session.user.getCompletedClasses()><!--- array --->
<cfset completedServices=Session.user.getCompletedServices()><!--- array --->

<div id="topperHTMLCodes" style="display:none;">Classes &amp; Programs</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtClasses.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);    
});

function confirmClassDeletion(obj) {
  var form=document.frmUnconfirmed;
  if (obj.checked) {
    if (confirm('Are you sure you wish to delete this class?')) {
	  form.submit();
	} else {
	  obj.checked = false;
	}
  }
}
</script>
<style type="text/css">
a.itemizedClassLnk, a.itemizedClassLnk:link, a.itemizedClassLnk:hover {color:#ec3ca5;padding-top:3px;text-decoration:none;}
a.itemizedClassLnk:hover { text-decoration:underline; }
</style>
<div class="contentWrapper" id="classes">

  <!--- <cfdump var="#Session.user.getPenciledInClasses()#"> --->
  
  <!--Content-->
  <div><p class="accent02Lrg">Unconfirmed</p></div>
  
  <div class="scheduleLft">   
    <p class="accent03">Below are the classes that you've begun the registration process for but haven't completed.</p>
    
    <cfif ArrayLen(penciledInClasses) GT 0 OR ArrayLen(penciledInServices) GT 0 >  
    <cfoutput>
    <form name="frmUnconfirmed" action="#Application.siteURLRootSSL#/action.cfm?md=class&task=removeClassesFromCart" method="post" onsubmit="return false;">      
      <div class="itemizedHdr">
        <div class="itemizedLabDelete">Delete</div>
        <div class="itemizedLabClass">Class</div>
        <div class="itemizedLabCost">Cost</div>
      </div>
      <cfloop index="idx" from="1" to="#ArrayLen(penciledInClasses)#">
      <div class="itemizedContainer">
        <div class="itemizedDelete">
          <input type="checkbox" name="classIDList" class="checkbox" value="#penciledInClasses[idx].classID#" onclick="confirmClassDeletion(this);" />
        </div>
        <div class="itemizedClass">
           <a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail&cid=#penciledInClasses[idx].courseID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#penciledInClasses[idx].courseTitle#</a><br />
          <span class="dateRange">
            #DateFormat(penciledInClasses[idx].classInstance[1].startDateTime,"mm.dd.yy")# |
            #LCase(TimeFormat(penciledInClasses[idx].classInstance[1].startDateTime,"h:mm tt"))# -
            #Lcase(TimeFormat(penciledInClasses[idx].classInstance[1].endDateTime,"h:mm tt"))#            
          </span>
        </div>
        <div class="itemizedCost">#DollarFormat(penciledInClasses[idx].cost)#</div>
      </div>
      </cfloop>
      
      <cfloop index="idx" from="1" to="#ArrayLen(penciledInServices)#">
      <div class="itemizedContainer">
        <div class="itemizedDelete">
          <input type="checkbox" name="serviceIDList" class="checkbox" value="#penciledInServices[idx].serviceID#" onclick="confirmClassDeletion(this);" />
        </div>
        <div class="itemizedClass"><a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail_service&sid=#penciledInServices[idx].serviceID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#penciledInServices[idx].serviceTitle#</a></div>
        <div class="itemizedCost">#DollarFormat(penciledInServices[idx].cost)#</div>
      </div>
      </cfloop>

      <div class="itemizedHdr">
        <div class="itemizedLabTotal">
          <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=checkout" class="btnFinishReg">Finish Registering</a>         
          <!--- <a href="javascript:document.frmUnconfirmed.submit();">update</a> --->
        </div>
        <div class="itemizedGrandTotal">#DollarFormat(Session.user.getCartTotal())#</div>
      </div>
    </form>
    </cfoutput>
    <cfelse>
      <div style="padding:20px 0px; text-align:center;">
        <span class="accent01">No unconfirmed classes at this time.</span>
      </div>    
    </cfif>
    
    <cfif Session.user.isLoggedIn >
    <div class="floatLeftFullWdth">
      <p>&nbsp;</p>
      <p class="accent02Lrg">My Schedule</p>     
      
      <div class="titleGry">Upcoming Classes</div>      
      
      <cfif ArrayLen(upcomingClasses) GT 0 OR ArrayLen(upcomingServices) GT 0>
      <cfoutput>
      <cfloop index="idx" from="1" to="#ArrayLen(upcomingClasses)#">
      <ul class="classSchedule">      
        <li><span class="accent02Med"><a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail&cid=#upcomingClasses[idx].courseID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#upcomingClasses[idx].courseTitle#</a></span> <br />
          
          <b>02.13.10 - 03.13.10  |  10:00am - 2:00pm</b> (need to rework this part)<br />
          
          <span class="accent01SmallCaps"><b>location:</b></span> Location A<br />
          <div class="btnWht">
            <div class="btnWhtCap"><img src="images/imgBtnWhtLft.gif" width="7" height="19" /></div>
            <a href="javascript:alert('not implemented yet');" class="btnWht">Change �</a>
            <div class="btnWhtCap"><img src="images/imgBtnWhtRt.gif" width="7" height="19" /></div>
          </div>
          <div class="btnWht">
            <div class="btnWhtCap"><img src="images/imgBtnWhtLft.gif" width="7" height="19" /></div>
            <a href="javascript:alert('not implemented yet');" class="btnWht">Add To Calendar �</a>
            <div class="btnWhtCap"><img src="images/imgBtnWhtRt.gif" width="7" height="19" /></div>
          </div>
          <br clear="left" />
        </li>        
      </ul>
      </cfloop>
      
      <cfloop index="idx" from="1" to="#ArrayLen(upcomingServices)#">
      <ul class="classSchedule">      
        <li><span class="accent02Med"><a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail_service&sid=#upcomingServices[idx].serviceID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#upcomingServices[idx].serviceTitle#</a></span> <br />
          <cfif Compare(upcomingServices[idx].locationName,"")>
          <span class="accent01SmallCaps"><b>location:</b></span> #upcomingServices[idx].locationName#<br />
          </cfif>
          <div class="btnWht">
            <div class="btnWhtCap"><img src="images/imgBtnWhtLft.gif" width="7" height="19" /></div>
            <a href="javascript:alert('not implemented yet');" class="btnWht">Change �</a>
            <div class="btnWhtCap"><img src="images/imgBtnWhtRt.gif" width="7" height="19" /></div>
          </div>

          <br clear="left" />
        </li>        
      </ul>
      </cfloop>
      </cfoutput>
      <cfelse>
      <div style="padding:20px 0px; text-align:center;">
        <span class="accent01">No upcoming classes at this time.</span>
      </div>
      </cfif>
      
      <div class="titleGry">Completed Classes</div>
      
      <cfif ArrayLen(completedClasses) GT 0 OR ArrayLen(completedServices) GT 0>
      <cfoutput>
      <cfloop index="idx" from="1" to="#ArrayLen(completedClasses)#">
      <ul class="classSchedule">
        <li><span class="accent02Med"><a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail&cid=#completedClasses[idx].courseID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#completedClasses[idx].courseTitle#</a></span><br />
        
          <b>02.13.10 - 03.13.10  |  10:00am - 2:00pm</b> (need to rework this part)<br />         
          
          <cfif Not completedClasses[idx].feedbackSent>
          <div class="btnWht">
            <div class="btnWhtCap"><img src="images/imgBtnWhtLft.gif" width="7" height="19" /></div>
            <a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=submit_review&cid=#completedClasses[idx].courseID#" class="btnWht">Give Feedback �</a>
            <div class="btnWhtCap"><img src="images/imgBtnWhtRt.gif" width="7" height="19" /></div>
          </div>
          <cfelse>
          <span class="feedbackThnks">Thanks for submitting feedback!</span>
          </cfif>
          <br clear="left" />          
        </li>
      </ul>
      </cfloop>
      
      <cfloop index="idx" from="1" to="#ArrayLen(completedServices)#">
      <ul class="classSchedule">
        <li><span class="accent02Med"><a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail_service&sid=#completedServices[idx].serviceID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#completedServices[idx].serviceTitle#</a></span><br />
          
          <cfif Not completedServices[idx].feedbackSent>
          <div class="btnWht">
            <div class="btnWhtCap"><img src="images/imgBtnWhtLft.gif" width="7" height="19" /></div>
            <a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=submit_review&sid=#completedServices[idx].serviceID#" class="btnWht">Give Feedback �</a>
            <div class="btnWhtCap"><img src="images/imgBtnWhtRt.gif" width="7" height="19" /></div>
          </div>
          <cfelse>
          <span class="feedbackThnks">Thanks for submitting feedback!</span>
          </cfif>
          <br clear="left" />          
        </li>
      </ul>
      </cfloop>
      </cfoutput>
      <cfelse>
      <div style="padding:20px 0px; text-align:center;">
        <span class="accent01">No completed classes at this time.</span>
      </div>
      </cfif>      
    </div>
    </cfif><!--- logged in --->
  </div>
  
  <cfif Session.user.isLoggedIn>
    <div class="scheduleRt">
      <div><img src="images/imgClassStatus.gif" alt="Status" width="158" height="13" /></div>
      <cfoutput>
      <div class="content">
        <p class="accent02Med">You are currently logged in.</p>
        <!--- <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=profile" class="btnGry">Edit Profile</a><br /> --->
        <a href="#Application.siteURLRootSSL#/action.cfm?md=class&task=logout" class="btnGry">Logout</a>        
      </div>
      <cfif Not Session.user.sendReminderEmail>
      <img src="images/imgClassReqRemind.gif" alt="Request Reminders" width="158" height="13" />
      <div class="content">
      <form name="frmClassReminder" id="frmClassReminder" action="#Application.siteURLRootSSL#/action.cfm?md=class&task=updateClassReminder" method="post">
        <p>
        <input type="checkbox" name="sendReminderEmail" id="sendReminderEmail" value="1" />
        Please send me a class reminder via e-mail two days before my class.
        </p>
        <a href="javascript:document.frmClassReminder.submit();" class="btnGry">Submit</a>
      </form>
      </div>
      </cfif>
      </cfoutput>
    </div>
  <cfelse>
    <div class="scheduleRt">
      <div><img src="images/imgClassLogin.gif" alt="Login" width="158" height="13" /></div>
      <div class="content">
        <cfoutput>
        <form name="frmLogin" id="login" action="#Application.siteURLRoot#/action.cfm?md=class&task=login" method="post">
          <input type="hidden" name="next" value="myschedule" />
          <label for="email">Email Address</label>
          <input type="text" name="email" id="email" />
          <label for="password">Password</label>
          <input type="password" name="password" id="password" />
          <a href="javascript:document.frmLogin.submit();" class="btnGry">Submit</a>
        </form>
        <p><a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=forgotpassword" class="createAccount">Forgot your password?</a></p>
        <p><a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=register" class="createAccount">Don�t have an account? Create one now �</a></p>
        </cfoutput>
      </div>
    </div>
  </cfif>
  
</div>
