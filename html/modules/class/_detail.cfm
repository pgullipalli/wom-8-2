<cfparam name="URL.cid"><!--- course ID --->
<cfsilent>
<cfscript>
  CL=CreateObject("component","com.Class").init();
  allCategories=CL.getCategoriesByCourseID(URL.cid);
  if (Find("/admin/index.cfm",CGI.HTTP_REFERER) GT 0) {//previewed by admin
    courseInfo=CL.getCourseByID(courseID=URL.cid, publishedOnly=0);
  } else {
    courseInfo=CL.getCourseByID(URL.cid);
  }
  courseReview=CL.getRandomCourseReview(URL.cid);
  classInfo=CL.getClassesByCourseID(URL.cid);
  //participantLimit=CL.getParticipantLimit(URL.cid);
  prerequisites=CL.getPrerequisitesByCourseID(URL.cid);
  associatedCourses=CL.getAssociatedCoursesByCourseID(URL.cid);
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none;">Classes &amp; Programs</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);  
});
</script>

<style type="text/css">
div.classInstance a {text-decoration:none;}
div.classInstance a:hover {text-decoration:underline;}
</style>

<div class="contentWrapper" id="classes">
  <cfoutput query="allCategories" maxrows="1">
  <div class="subSectHdwthCats">#categoryName#</div>
  </cfoutput>
  <cfif allCategories.recordcount GT 1>
  <span class="subSectCats">
    <cfoutput>
    <cfloop index="idx" from="2" to="#allCategories.recordcount#">
      <cfif idx GT 2>&nbsp;&nbsp;|&nbsp;&nbsp;</cfif>
      <a href="index.cfm?md=class&tmp=category&catid=#allCategories.categoryID[idx]#&tid=#Application.template.templateID_Class#">#allCategories.categoryName[idx]#</a> 
    </cfloop>
    </cfoutput>
  </span>
  </cfif>
  
  <!--Class Details-->
  <cfoutput query="courseInfo">
  <span class="head">#courseTitle#</span>
  <div class="detailLft">
    <cfif Compare(courseImage,"")>
    <div class="thmbnail">
      <div class="floatLeft"><img src="#courseImage#" width="148" height="148" align="left" class="phtStroke" /></div>
      <cfif Compare(courseImageCaption,"")>
      <div class="caption">#courseImageCaption#</div>
      </cfif>
    </div>
    </cfif>    
    #longDescription#      
    <!--- <a href="#" class="lnkAccent03Caps">MORE INFO...</a> --->      
    <cfif Compare(outsideEventURL,"")>
      <cfif FindNoCase("http://",outsideEventURL) IS 0 AND FindNoCase("https://",outsideEventURL) IS 0>
        <p><i>Event Website:</i> <a href="http://#outsideEventURL#" class="lnkAccent02">#outsideEventURL#</a></p>
      <cfelse>
        <p><i>Event Website:</i> <a href="#outsideEventURL#" class="lnkAccent02">#outsideEventURL#</a></p>
      </cfif>
    </cfif>
  </div>
  
  <div class="detailRt">
    <div><img src="images/imgClassCost.gif" alt="Cost" width="130" height="13" /></div>
    <div class="cost">
      <cfif courseInfo.callForPrice>
      <p><span class="med14"><b>Call for Pricing</b></span></p>
      <cfelse>
        <cfif feeRegular GT 0>
        <p><span class="price">#DollarFormat(feeRegular)#</span> <span class="descrip">{Regular Fee}</span></p>
          <cfif (feeRegular GT feeFitnessClub) OR (feeFitnessClub GT feeRegular)>
          <p>
		  <cfif feeFitnessClub GT 0><span class="price">#DollarFormat(feeFitnessClub)#</span><cfelse><span class="med14"><b>No Cost</b></cfif>        
          <span class="descrip">{Fitness Club Members}</span></p>
          </cfif>        
        <cfelse>
        <p><span class="med14"><b>No Cost</b></span></p>
        </cfif>
        
        <cfif IsDefined("Session.user")>
          <cfif Not Compare(Session.user.studentType,"E") AND ((feeRegular GT feeEmployee) OR (feeEmployee GT feeRegular))><!--- employee discount --->
        	<p>
            <cfif feeEmployee GT 0><span class="price">#DollarFormat(feeEmployee)#</span><cfelse><span class="med14"><b>No Cost</b></cfif>              
            <span class="descrip">{Emploees}</span></p>
          <cfelseif Not Compare(Session.user.studentType,"D") AND ((feeRegular GT feeDoctor) OR (feeDoctor GT feeRegular))><!--- doctor discount --->
            <p>
            <cfif feeDoctor GT 0><span class="price">#DollarFormat(feeDoctor)#</span><cfelse><span class="med14"><b>No Cost</b></cfif>              
            <span class="descrip">{Doctors}</span></p>
          </cfif>
        </cfif>
      </cfif>
    </div>
    <div class="content">
      <p>
        <span class="labelAccent02">Participant Limit</span>
        <span class="med14">
		  <cfif courseInfo.regLimit GT 1000>
            --
		  <cfelse>
            #courseInfo.regLimit#
		  </cfif>          
        </span>
      </p>
      <cfif Compare(contactName,"")>
        <p>
        <span class="labelAccent02">Contact Info</span> <span class="med14">#contactName#</span><br />
        <cfif Compare(contactEmail,"")><a href="mailto:#contactEmail#">#contactEmail#</a><br /></cfif>
        <cfif Compare(contactPhone,"")>#contactPhone#</cfif>
        </p>
      </cfif>
    </div>
  </div>
  </cfoutput>
  
  <cfif Compare(courseReview,"")>
  <!--Testimonial-->
  <div class="testimonialWrapper">
    <div class="testimonial">
      <div><img src="images/imgClassQuoteHdr.gif" width="420" height="6" /></div>
      <div class="content"><cfoutput>#courseReview#</cfoutput><br />
        <cfoutput><a href="index.cfm?md=class&tmp=coursereviews&cid=#URL.cid#" class="lnkAccent03Sm">&ndash;  Read More Reviews  &ndash; </a></cfoutput>
      </div>
      <div><img src="images/imgClassQuoteFtr.gif" width="420" height="6" /></div>
    </div>
  </div>
  </cfif>
</div>

<div class="contentWrapperNoPad">
<cfif courseInfo.recordcount GT 0 And classInfo.numClasses GT 0>
  <div class="availClasses">Available Classes</div>
  <!--Class Instance Labels-->
  <div class="classInstanceLabels" align="center">
    <table border="0" cellpadding="4" cellspacing="0">
      <tr>
        <td width="18"><img src="images/imgSpacer.gif" width="18" height="1" /></td>
        <td width="135" align="left" valign="middle" class="accent02"><b>Date(s)</b></td>
        <td width="135" align="left" valign="middle" class="accent02"><b>Time(s)</b></td>
        <td width="78" align="left" valign="middle" class="accent02"><b>Location</b></td>
        <td><img src="images/imgSpacer.gif" width="103" height="1" /></td>
      </tr>
    </table>
  </div>
  
  <!--Class Instance-->
  <cfoutput>
  <cfloop index="idx1" from="1" to="#classInfo.numClasses#"> <cfset Variables.classID=classInfo.class[idx1].classID>
  <div class="classInstance" align="center">
    <table border="0" cellpadding="4" cellspacing="0" class="classInfo">
      <tr>
        <td width="18">
          <a href="javascript:toggle('classSchedule#Variables.classID#')" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('expand#Variables.classID#','','images/btnClassPluso.gif',1)"><img src="images/btnClassPlus.gif" alt="Expand" name="expand#Variables.classID#" width="18" height="18" border="0" id="expand#Variables.classID#" /></a>
        </td>
        <td width="135" align="left" class="small12">
          <b>#DateFormat(classInfo.class[idx1].classInstance[1].startDateTime,"mm.dd.yy")#</b>  
        </td>
        <td width="135" align="left" class="small12">
          <b>#LCase(TimeFormat(classInfo.class[idx1].classInstance[1].startDateTime,"h:mm tt"))# - #LCase(TimeFormat(classInfo.class[idx1].classInstance[1].endDateTime,"h:mm tt"))#</b>          
        </td>
        <td width="78" align="left" class="small10">
		  <cfif Compare(classInfo.class[idx1].classInstance[1].locationName,"")>
            <cfif classInfo.class[idx1].classInstance[1].mapID>
              <a href="javascript:openWindow('index.cfm?md=class&tmp=class_location_map&popup=1&lid=#classInfo.class[idx1].classInstance[1].locationID#','map',900,600)">#classInfo.class[idx1].classInstance[1].locationName#</a>
            <cfelse>
              #classInfo.class[idx1].classInstance[1].locationName#
            </cfif>
		  <cfelse>
            &nbsp;
		  </cfif>
        </td>
        <td width="103">
          <cfif courseInfo.registerOnline And Not courseInfo.callForPrice>
            <cfif classInfo.class[idx1].numParticipants GTE classInfo.class[idx1].regMaximum>
              <div style="text-align:center;"><b>FULL</b></div>          
            <cfelse>
            <a href="#Application.siteURLRootSSL#/action.cfm?md=class&task=addClassToCart&clid=#Variables.classID#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('register#Variables.classID#','','images/btnClassRegisterNowo.gif',1)"><img src="images/btnClassRegisterNow.gif" alt="Register Now" name="register#Variables.classID#" width="103" height="17" border="0" id="register#Variables.classID#" /></a>
            </cfif>
          <cfelse>
            <cfif Compare(courseInfo.registrationURL,"")>
              <a href="#courseInfo.registrationURL#" target="_blank" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('register#Variables.classID#','','images/btnClassRegisterNowo.gif',1)"><img src="images/btnClassRegisterNow.gif" alt="Register Now" name="register#Variables.classID#" width="103" height="17" border="0" id="register#Variables.classID#" /></a>
            <cfelse>
              &nbsp;
            </cfif>
          </cfif>
        </td>
      </tr>
    </table>
    <!--Expanded Info Beginning-->
    <div id="classSchedule#Variables.classID#" style="display:none;">
    <table border="0" cellpadding="4" cellspacing="0" class="classInfo">
      <cfloop index="idx2" from="1" to="#classInfo.class[idx1].numClassInstances#">
      <tr>
        <td width="18">&nbsp;</td>
        <td width="135" align="left" class="detailsSm11"><table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td class="pdRt">#DayOfWeekAsString(classInfo.class[idx1].classInstance[idx2].dayOfWeekAsNumber)#</td>
              <td>#DateFormat(classInfo.class[idx1].classInstance[idx2].startDateTime,"mm.dd.yy")#</td>
            </tr>
          </table></td>
        <td width="135" align="left" class="detailsSm11">
          #LCase(TimeFormat(classInfo.class[idx1].classInstance[idx2].startDateTime,"h:mm tt"))# - #LCase(TimeFormat(classInfo.class[idx1].classInstance[idx2].endDateTime,"h:mm tt"))#
        </td>
        <td width="189" align="left" class="detailsSm10">
          <cfif Compare(classInfo.class[idx1].classInstance[idx2].locationName,"")>
            <cfif classInfo.class[idx1].classInstance[idx2].mapID>
              <a href="javascript:openWindow('index.cfm?md=class&tmp=class_location_map&popup=1&lid=#classInfo.class[idx1].classInstance[idx2].locationID#','map',900,600)">#classInfo.class[idx1].classInstance[idx2].locationName#</a>
            <cfelse>
              #classInfo.class[idx1].classInstance[idx2].locationName#
            </cfif>
          <cfelse>
            &nbsp;
          </cfif>
        </td>
      </tr>
      </cfloop>	  
      <tr>
        <td>&nbsp;</td>
        <td colspan="3" align="left" class="detailsSm11">
          <div class="notes">
          <cfif Compare(classInfo.class[idx1].notes,"")>
            <span class="labelAccent02">Notes:</span><br />
            #classInfo.class[idx1].notes#<br /><br />
          </cfif>
          </div>
        </td>
      </tr>
    </table>
    </div>
    <!--Expanded Info End-->
  </div>
  </cfloop>
  </cfoutput>
<cfelse>
    <div class="classInstance" align="center">
      <br />No classes available at this time.<br /><br />
    </div>
</cfif>
</div>

<cfif courseInfo.recordcount GT 0>
<!--Additional Info-->
<div class="contentWrapper" id="classes">
  <cfif Compare(courseInfo.specialInstructions,"")>
  <span class="labelAccent02">Special Instructions</span><br />
    <cfoutput>#courseInfo.specialInstructions#</cfoutput>
    <br /><br />
  </cfif>  
  
  <cfif prerequisites.recordcount GT 0>
  <span class="labelAccent02">Recommended Prerequisites</span>
  <ul class="classDetail">
    <cfoutput query="prerequisites">
    <li><a href="index.cfm?md=class&tmp=detail&cid=#courseID#">#courseTitle#</a></li>
    </cfoutput>
  </ul>
  </cfif>
  
  <cfif associatedCourses.recordcount GT 0>
  <span class="labelAccent02">Other classes to consider </span>
  <ul class="classDetail">
    <cfoutput query="associatedCourses">
    <li><a href="index.cfm?md=class&tmp=detail&cid=#courseID#">#courseTitle#</a></li>
    </cfoutput>
  </ul>
  </cfif>
</div>
</cfif>