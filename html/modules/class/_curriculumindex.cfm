<cfsilent>
<cfscript>
  CL=CreateObject("component","com.Class").init();
  introductionText=CL.getIntroductionText('curriculum');
  curriculum=CL.getAllCurriculum();
</cfscript>
</cfsilent>
<div id="topperHTMLCodes" style="display:none;">Curriculum</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);  
});
</script>
<div class="contentWrapper" id="classCategory">

  <div class="intro">
  <cfoutput>
  #Replace(introductionText,Chr(10), "<br />", "All")#
  </cfoutput>
  </div>
  
  <ul>
    <cfoutput query="curriculum">
    <li><a href="index.cfm?md=class&tmp=curricula&cuid=#curriculaID#" class="className">#curriculaTitle#</a> #shortDescription#</li>
    </cfoutput>    
  </ul>  
</div>