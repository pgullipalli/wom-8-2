<cfparam name="URL.next" default="">
<cfparam name="URL.failed" default="0">
<div id="topperHTMLCodes" style="display:none;">Classes &amp; Programs</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);  
});
</script>
<div class="contentWrapper" id="forms">
  <div class="floatLeftFullWdth">   
    <cfoutput>
    <form name="frmLogin" id="frmLogin" action="#Application.siteURLRootSSL#/action.cfm?md=class&task=login" method="post">
      <input type="hidden" name="next" value="#URL.next#">
      <div class="unit">
        <span class="section">Log In To Woman's</span>
      </div>
      <cfif URL.failed>
      <div class="unit">
        <div class="accent02"><b>Sorry, but your login information is not recognized. Please try again.</b><br /><br /></div>
      </div>
      </cfif>
      <div class="unit">
        <div class="singleField">
          <label for="email" class="descrip">E-Mail Address</label>
          <input type="text" name="email" id="email" />
        </div>        
      </div>
      <div class="unit">
        <div class="singleField">
          <label for="password" class="descrip">Password</label>
          <input type="password" name="password" id="password" />
        </div>        
      </div>
      <div class="unit">
        <input type="image" src="images/btnFormSubmit.gif" alt="Submit" name="btnFormSubmit" style="width:63px;height:17px; border:0px solid ##ffffff;" border="0" id="btnFormSubmit" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormSubmit','','images/btnFormSubmito.gif',1)" />
      </div>
    </form>

    <p align="center">
      <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=forgotpassword" class="lnkAccent01">Forgot your password?</a>    
      &nbsp;&nbsp;
      <a href="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=register" class="lnkAccent01">Don�t have an account? Create one now �</a>
    </p>    
    </cfoutput>
  </div>
</div>
