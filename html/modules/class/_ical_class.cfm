<cfparam name="URL.ciid"><!--- classInstanceID --->
<cfscript>
CL=CreateObject("component","com.Class").init();
UT=CreateObject("component","com.Utility").init();
classInstanceInfo=CL.getClassInstance(URL.ciid);

if (classInstanceInfo.recordcount GT 0) {
  newLine=Chr(13) & Chr(10);
  eventStr = StructNew();
  eventStr.name = classInstanceInfo.courseTitle;
  eventStr.location = classInstanceInfo.locationName;  
  eventStr.startDateTime=classInstanceInfo.startDateTime;
  eventStr.endDateTime=classInstanceInfo.endDateTime; 
  
  if (Compare(classInstanceInfo.contactName,"") OR Compare(classInstanceInfo.contactEmail,"") OR Compare(classInstanceInfo.contactPhone,"")) {
    eventStr.description="Contact Info:" & "\n\n" & classInstanceInfo.contactName &
						 "\n\n" & classInstanceInfo.contactEmail & "\n\n" & classInstanceInfo.contactPhone;
  } else {
    eventStr.description="";
  }
  
  iCalString=Trim(UT.createiCalendarString(eventStr));
  fileName="class-#URL.ciid#.ics";
} else {
  iCalString="Sorry, the class you are looking for is not available at this time.";
  fileName="class-#URL.ciid#.txt";
}
</cfscript>
<cfcontent type="text/calendar" reset="Yes"><cfheader name="Content-Disposition" value="inline; filename=#fileName#"><cfoutput>#iCalString#</cfoutput>