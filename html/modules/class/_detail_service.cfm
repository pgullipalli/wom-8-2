<cfparam name="URL.sid"><!--- serviceID --->
<cfsilent>
<cfscript>
  CL=CreateObject("component","com.Class").init();
  allCategories=CL.getCategoriesByServiceID(URL.sid);
  serviceInfo=CL.getServiceByID(URL.sid);
  serviceReview=CL.getRandomServiceReview(URL.sid);
  associatedCourses=CL.getAssociatedCoursesByServiceID(URL.sid);
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none;">Classes &amp; Programs</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);   
});
</script>

<div class="contentWrapper" id="classes">
  <cfoutput query="allCategories" maxrows="1">
  <div class="subSectHdwthCats">#categoryName#</div>
  </cfoutput>
  <cfif allCategories.recordcount GT 1>
  <span class="subSectCats">
    <cfoutput>
    <cfloop index="idx" from="2" to="#allCategories.recordcount#">
      <cfif idx GT 2>&nbsp;&nbsp;|&nbsp;&nbsp;</cfif>
      #allCategories.categoryName[idx]#
    </cfloop>
    </cfoutput>
  </span>
  </cfif>
  
  <!--Class Details-->
  <cfoutput query="serviceInfo">
  <span class="head">#serviceTitle#</span>
  <div class="detailLft">     
    #longDescription#
    
    <p><br />
    <a href="#Application.siteURLRootSSL#/action.cfm?md=class&task=addServiceToCart&sid=#URL.sid#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('register1','','images/btnClassRegisterNowo.gif',1)"><img src="images/btnClassRegisterNow.gif" alt="Register Now" name="register1" width="103" height="17" border="0" id="register1" /></a></p>
  </div>
  
  <div class="detailRt">
    <div><img src="images/imgClassCost.gif" alt="Cost" width="130" height="13" /></div>
    <div class="cost">
      <cfif feeRegular GT 0>
      <p><span class="price">#DollarFormat(feeRegular)#</span> <span class="descrip">{Regular Fee}</span></p>
        <cfif (feeRegular GT feeFitnessClub) OR (feeFitnessClub GT feeRegular)>
          <p><cfif feeFitnessClub GT 0><span class="price">#DollarFormat(feeFitnessClub)#</span><cfelse><span class="med14"><b>No Cost</b></cfif>        
          <span class="descrip">{Fitness Club Members}</span></p>
        </cfif>
      <cfelse>
      <p><span class="med14"><b>No Cost</b></span></p>
      </cfif>
      
      
      <cfif IsDefined("Session.user")>
        <cfif Not Compare(Session.user.studentType,"E") AND ((feeRegular GT feeEmployee) OR (feeEmployee GT feeRegular))><!--- employee discount --->
      	  <p>
          <cfif feeEmployee GT 0><span class="price">#DollarFormat(feeEmployee)#</span><cfelse><span class="med14"><b>No Cost</b></cfif>              
          <span class="descrip">{Emploees}</span></p>
        <cfelseif Not Compare(Session.user.studentType,"D") AND ((feeRegular GT feeDoctor) OR (feeDoctor GT feeRegular))><!--- doctor discount --->
          <p>
          <cfif feeDoctor GT 0><span class="price">#DollarFormat(feeDoctor)#</span><cfelse><span class="med14"><b>No Cost</b></cfif>              
          <span class="descrip">{Doctors}</span></p>
        </cfif>
      </cfif>
      
      
      
    </div>
    <div class="content">
      <cfif Compare(contactName,"") OR Compare(contactEmail,"") OR Compare(contactPhone,"")>
        <p>
        <span class="labelAccent02">Contact Info</span>
        <cfif Compare(contactName,"")><span class="med14">#contactName#</span><br /></cfif>
        <cfif Compare(contactEmail,"")><a href="mailto:#contactEmail#">#contactEmail#</a><br /></cfif>
        <cfif Compare(contactPhone,"")>#contactPhone#</cfif>
        </p>
      </cfif>
    </div>
  </div>
  </cfoutput>
  
  <cfif Compare(serviceReview,"")>
  <!--Testimonial-->
  <div class="testimonialWrapper">
    <div class="testimonial">
      <div><img src="images/imgClassQuoteHdr.gif" width="420" height="6" /></div>
      <div class="content"><cfoutput>#serviceReview#</cfoutput><br />
        <cfoutput><a href="index.cfm?md=class&tmp=servicereviews&sid=#URL.sid#" class="lnkAccent03Sm">&ndash;  Read More Reviews  &ndash; </a></cfoutput>
      </div>
      <div><img src="images/imgClassQuoteFtr.gif" width="420" height="6" /></div>
    </div>
  </div>
  </cfif>

</div>



<!--Additional Info-->
<div class="contentWrapper" id="classes">

  <cfif Compare(serviceInfo.specialInstructions,"")>
  <span class="labelAccent02">Special Instructions</span><br />
    <cfoutput>#serviceInfo.specialInstructions#</cfoutput>
    <br /><br />
  </cfif>
  
  <cfif associatedCourses.recordcount GT 0>
  <span class="labelAccent02">Other classes to consider </span>
  <ul class="classDetail">
    <cfoutput query="associatedCourses">
    <li><a href="index.cfm?md=class&tmp=detail&cid=#courseID#">#courseTitle#</a></li>
    </cfoutput>
  </ul>
  </cfif>
</div>