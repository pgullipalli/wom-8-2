<cfinclude template="_create_user_session.cfm">
<cfif Not Session.user.isLoggedIn>
  <cflocation url="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=login&next=checkout">
</cfif>
<cfset UT=CreateObject("component", "com.Utility").init()>
<cfset penciledInClasses=Session.user.getPenciledInClasses()>
<cfset classIDList="">
<cfset feePerCoupleClassIDList="">
<cfset penciledInServices=Session.user.getPenciledInServices()>
<cfset serviceIDList="">
<cfset feePerCoupleServiceIDList=""><!--- place holder only; for consistency this will always be empty string --->
<cfset donationAmount=Session.user.getDonationAmount()>
<cfset cartTotal=Session.user.getCartTotal()>
<cfset hasPaymentPlanEligibleClass=Session.user.hasPaymentPlanEligibleClass()>
<cfif ArrayLen(Session.user.penciledInClasses) Is 0 AND ArrayLen(Session.user.penciledInServices) Is 0>
  <!--- nothing to check out for --->
  <cflocation url="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=myschedule">
</cfif>

<div id="topperHTMLCodes" style="display:none;">Class Registration</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
<!--
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);    
  
  if (form.paymentMethod) {
    var form = document.frmBilling;  
    if (form.paymentMethod[0].checked) {
      $("#divCardInfo").show();
	  $("#divBankInfo").hide();
    } else if (form.paymentMethod[1].checked) {
      $("#divCardInfo").hide();
	  $("#divBankInfo").show();
    } else {
      $("#divCardInfo").hide();
	  $("#divBankInfo").hide();
    }
  }
});

function togglePaymentMethod(obj) {
  var paymentMethod=obj.value;
  if (paymentMethod=="Credit Card") {
    $("#divCardInfo").show();
	$("#divBankInfo").hide();
  } else if (paymentMethod=="Bank Account") {
    $("#divCardInfo").hide();
	$("#divBankInfo").show();
  } else {
    $("#divCardInfo").hide();
	$("#divBankInfo").hide();
  }
}

function toggleCardForm(obj) {
  if (obj.checked) {
    $("#divCardInfo").hide();
  } else {
    $("#divCardInfo").show();
  }
}


function confirmClassDeletion(obj) {
  var form=document.frmCart;
  if (obj.checked) {
    if (confirm('Are you sure you wish to remove this item?')) {
	  form.submit();
	} else {
	  obj.checked = false;
	}
  }
}

function submitCartUpdate(form) {
  if (form.donationAmount.value != "") {
	if (!checkNumber(form.donationAmount.value)) {
	  alert("Please enter your donation amount in a correct number format. Thanks!");
	  form.donationAmount.focus();
	  return false;
	}
  }
  return true;
}

function copyToBilling(obj) {
  var form = document.frmBilling;
  if (obj.checked) {
	form.billingFirstName.value=form.contactFirstName.value;
	form.billingLastName.value=form.contactLastName.value;
	form.billingAddress1.value=form.contactAddress1.value;	
	form.billingAddress2.value=form.contactAddress2.value;
	form.billingCity.value=form.contactCity.value;
	form.billingState.selectedIndex=form.contactState.selectedIndex;
	form.billingZip.value=form.contactZip.value;
  }
}

function submitBillingInfo(form) {
  for (var i=0; i < form.elements.length; i++) $('#' + form.elements[i].name).removeClass('error');
	
  var error = false;  
  //if (form.attendeeFirstName.value == "") {$('#attendeeFirstName').addClass("error"); error = true;}
  //if (form.attendeeLastName.value == "") {$('#attendeeLastName').addClass("error"); error = true;}  
  var classIDs=new Array();  
  var feePerCoupleClassIDs=new Array();
  if (form.classIDList.value != "") classIDs=form.classIDList.value.split(",");
  if (form.feePerCoupleClassIDList.value != "") feePerCoupleClassIDs=form.feePerCoupleClassIDList.value.split(",");
  for (var i=0; i < classIDs.length; i++) {//all classes
    var classIndex=i+1;
    var attName = eval("form.attendeeName" + classIDs[i] + "_" + classIndex + ".value");
	if (attName == "") {
	  $("#attendeeName" + classIDs[i] + "_" + classIndex).addClass("error"); error = true;
	}
    /*if (form.feePerCoupleClassIDList.value != "") {//has fee per couple classes
	  var found=false;
	  for (var j=0; j < feePerCoupleClassIDs.length; j++) if (classIDs[i] == feePerCoupleClassIDs[j]) found=true;
	  if (found) {
	    var gNames = eval("form.guestNames" + classIDs[i] + "_" + classIndex + ".value");
		if (gNames == "") {
	      $("#guestNames" + classIDs[i] + "_" + classIndex).addClass("error"); error = true;
	    }
	  }
	}*/
  }
  
  var serviceIDs=new Array();  
  var feePerCoupleServiceIDs=new Array();
  if (form.serviceIDList.value != "") serviceIDs=form.serviceIDList.value.split(",");
  if (form.feePerCoupleServiceIDList.value != "") feePerCoupleServiceIDs=form.feePerCoupleServiceIDList.value.split(",");
  for (var i=0; i < serviceIDs.length; i++) {//all services
    var serviceIndex=i+1;
    var attName = eval("form.serviceAttendeeName" + serviceIDs[i] + "_" + serviceIndex + ".value");
	if (attName == "") {
	  $("#serviceAttendeeName" + serviceIDs[i] + "_" + serviceIndex).addClass("error"); error = true;
	}
    if (form.feePerCoupleServiceIDList.value != "") {//has fee per couple services
	  var found=false;
	  for (var j=0; j < feePerCoupleServiceIDs.length; j++) if (serviceIDs[i] == feePerCoupleServiceIDs[j]) found=true;
	  if (found) {
	    var gNames = eval("form.serviceGuestNames" + serviceIDs[i] + "_" + serviceIndex + ".value");
		if (gNames == "") {
	      $("#serviceGuestNames" + serviceIDs[i] + "_" + serviceIndex).addClass("error"); error = true;
	    }
	  }
	}
  }
  
  if (form.contactFirstName.value == "") {$('#contactFirstName').addClass("error"); error = true;}
  if (form.contactLastName.value == "") {$('#contactLastName').addClass("error"); error = true;}
  if (form.contactAddress1.value == "") {$('#contactAddress1').addClass("error"); error = true;}
  if (form.contactCity.value == "") {$('#contactCity').addClass("error"); error = true;}
  if (form.contactState.selectedIndex < 1) {$('#contactState').addClass("error"); error = true;}
  if (form.contactZip.value == "") {$('#contactZip').addClass("error"); error = true;}
  if (form.daytimePhonePart1.value == "" || form.daytimePhonePart2.value == "" || form.daytimePhonePart3.value == "") {
    $('#daytimePhonePart1').addClass("error"); $('#daytimePhonePart2').addClass("error"); $('#daytimePhonePart3').addClass("error"); error = true;
  }
  
  if (form.billingFirstName) {//if this field is presented, balance > 0 and billing info is requred
    if (form.paymentMethod[0].checked) {//pay with Credit Card
	  if (form.cardType.selectedIndex < 1) {$('#cardType').addClass("error"); error = true;}
	  if (form.nameOnCard.value == "") {$('#nameOnCard').addClass("error"); error = true;}
	  if (form.cardNumber.value == "") {$('#cardNumber').addClass("error"); error = true;}
	  if (!checkExpirationDate(form.cardExpirationDate.value)) {
		$('#cardExpirationDate').addClass("error");
		error = true;
	  }	  
	  if (form.cardCVV2.value == "") {$('#cardCVV2').addClass("error"); error = true;}
	} else if (form.paymentMethod[1].checked) {//pay with Bank Account
	  if (form.nameOnAccount.value == "") {$('#nameOnAccount').addClass("error"); error = true;}
	  if (form.accountNumber.value == "") {$('#accountNumber').addClass("error"); error = true;}
	  if (form.routingNumber.value == "") {$('#routingNumber').addClass("error"); error = true;}
	} 
	
    if (form.billingFirstName.value == "") {$('#billingFirstName').addClass("error"); error = true;}
    if (form.billingLastName.value == "") {$('#billingLastName').addClass("error"); error = true;}
    if (form.billingAddress1.value == "") {$('#billingAddress1').addClass("error"); error = true;}
    if (form.billingCity.value == "") {$('#billingCity').addClass("error"); error = true;}
    if (form.billingState.selectedIndex < 1) {$('#billingState').addClass("error"); error = true;}
    if (form.billingZip.value == "") {$('#billingZip').addClass("error"); error = true;}  
  }

  if (error) {
    alert('The hilighted field(s) are required. Please make sure you have entered all required fields in correct format.');
	return false;
  } else {
    return true;
  }
}

function checkExpirationDate(str) {
  var datePattern = /[0-9]{2}\/20[0-9]{2}$/;
  return datePattern.test(str); 
}

function helpCVV() {
  var msg='The Card Security Code is located on the back of MasterCard, Visa and Discover credit or debit cards and is typically a separate group of 3 digits to the right of the signature strip.\n\n' +
  		  'On American Express cards, the Card Security Code is a printed (NOT embossed) group of four digits on the front towards the right.';
  alert(msg);
}
//-->
</script>
<style type="text/css">
a.itemizedClassLnk, a.itemizedClassLnk:link, a.itemizedClassLnk:hover {color:#ec3ca5; padding-top:3px;text-decoration:none;}
a.itemizedClassLnk:hover { text-decoration:underline; }
.date-range { color:#333132; font-size:11px; }
.class-title {color:#EC6CB8; margin:10px 0px 20px 0px; font-size:12px; padding:0px 15px; font-weight:bold;}
.error {background-color:#FFF2F9;}
#forms .radio-group { float:left; width:300px; font-size:13px;}
#forms .radio-group input {width:auto; height:13px;}

</style>
<cfoutput>
  <div class="contentWrapper" id="forms">
    <div class="head">Class Registration</div>   
    
    <form name="frmCart" action="#Application.siteURLRootSSL#/action.cfm?md=class&task=updateCartContent" method="post" onsubmit="return submitCartUpdate(this);">
      <cfif IsDefined("URL.update") And URL.update>
      <div class="accent03" style="text-align:center;margin:10px 0px; padding:5px 5px; background-color:##FFFFCC; float:left;">
        <i><b>Your total has been updated.</b></i>
      </div>    
    </cfif>
    
      <div class="itemizedHdr">
        <div class="itemizedLabDelete">Delete</div>
        <div class="itemizedLabClass">Class</div>
        <div class="itemizedLabCost">Cost</div>
      </div>
      <cfloop index="idx" from="1" to="#ArrayLen(penciledInClasses)#">        
        <cfset classIDList=ListAppend(classIDList, penciledInClasses[idx].classID)>
        <cfif penciledInClasses[idx].feePerCouple>
          <cfset feePerCoupleClassIDList=ListAppend(feePerCoupleClassIDList, penciledInClasses[idx].classID)>
		</cfif>
        <div class="itemizedContainer">
          <div class="itemizedDelete">
            <input type="checkbox" name="classIDList" class="checkbox" value="#penciledInClasses[idx].classID#" onclick="confirmClassDeletion(this);" />
          </div>
          <div class="itemizedClass"> <a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail&cid=#penciledInClasses[idx].courseID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#penciledInClasses[idx].courseTitle#</a><br />
            <span class="date-range">
              #DateFormat(penciledInClasses[idx].classInstance[1].startDateTime,"mm.dd.yy")# |
              #LCase(TimeFormat(penciledInClasses[idx].classInstance[1].startDateTime,"h:mm tt"))# - #Lcase(TimeFormat(penciledInClasses[idx].classInstance[1].endDateTime,"h:mm tt"))#
            </span>
          </div>
          <div class="itemizedCost">
            <cfif penciledInClasses[idx].paymentPlanEligible>
			  <cfif penciledInClasses[idx].enrollPaymentPlan><!--- enroll payment plan --->
                #DollarFormat(penciledInClasses[idx].firstInstallment)#<br />
                <span class="accent01"><small>(Remaining Balance: #DollarFormat(penciledInClasses[idx].remainingPayment)#)</small></span>
              <cfelse>
                #DollarFormat(penciledInClasses[idx].cost)#*
              </cfif>
            <cfelse>
              #DollarFormat(penciledInClasses[idx].cost)#
            </cfif>
          </div>
        </div>
        <cfif penciledInClasses[idx].discountApplied>
        <div class="itemizedContainer">
          <div class="itemizedDelete">
            <input type="checkbox" name="discountCodeList" class="checkbox" value="#penciledInClasses[idx].discountCode#" onclick="confirmClassDeletion(this);" />
          </div>
          <div class="itemizedClass">Coupon Code - #penciledInClasses[idx].discountCode#</div>
          <div class="itemizedCost">-#DollarFormat(penciledInClasses[idx].discountAmount)#</div>
        </div>
        </cfif>
      </cfloop>
      <cfloop index="idx" from="1" to="#ArrayLen(penciledInServices)#">
        <cfset serviceIDList=ListAppend(serviceIDList, penciledInServices[idx].serviceID)>
        <div class="itemizedContainer">
          <div class="itemizedDelete">
            <input type="checkbox" name="serviceIDList" class="checkbox" value="#penciledInServices[idx].serviceID#" onclick="confirmClassDeletion(this);" />
          </div>
          <div class="itemizedClass"><a href="#Application.siteURLRoot#/index.cfm?md=class&tmp=detail_service&sid=#penciledInServices[idx].serviceID#&tid=#Application.template.templateID_Class#" class="itemizedClassLnk">#penciledInServices[idx].serviceTitle#</a></div>
          <div class="itemizedCost">#DollarFormat(penciledInServices[idx].cost)#</div>
        </div>
      </cfloop>
      <cfif donationAmount GT 0>
        <div class="itemizedContainer">
          <div class="itemizedDelete">
            <input type="checkbox" name="removeDonation" class="checkbox" value="1" onclick="confirmClassDeletion(this);" />
          </div>
          <div class="itemizedClass">Donate to Woman's Hospital Foundation</div>
          <div class="itemizedCost">#DollarFormat(donationAmount)#</div>
        </div>
      </cfif>
      <div class="itemizedHdr">
        <div class="itemizedLabTotal">Total:</div>
        <div class="itemizedGrandTotal">#DollarFormat(cartTotal)#</div>
      </div>
      <!--- ask for coupon code only if total greater than zero --->
      <cfif cartTotal GT 0>
        <div class="dottedDivider"></div>
        <div class="itemizedHdr">
          <div class="itemizedLabTotal"> Do you have a Coupon Code? If so, enter it here:</div>
          <div class="itemizedGrandTotal">
            <input type="text" name="discountCode" style="width:50px;" />
          </div>
        </div>
        <cfif IsDefined("URL.er1") And URL.er1>
        <div class="itemizedHdr">
              <!--- <div class="accent03"><i>The coupon code "#URL.code#" you entered is either invalid or doesn't apply to the classes you have selected.</i></div> --->
              <div class="accent01" style="text-align:center;background-color:##FFFF00;padding:5px 5px; margin:0px 0px;float:left;">
                <i>The coupon code "#URL.code#" you entered is either invalid or doesn't apply to the classes you have selected.</i>
              </div> 
        </div>
        </cfif>
      </cfif>
      <div class="dottedDivider"></div>
      <div class="itemizedHdr">
        <div class="itemizedLabTotal">Donate to Woman's Hospital Foundation [OPTIONAL]:</div>
        <div class="itemizedGrandTotal">
          <input type="text" name="donationAmount" style="width:50px;" />
        </div>
      </div>
      
      <cfif hasPaymentPlanEligibleClass>
      <div class="dottedDivider"></div>
      <div class="itemizedHdr">
        <div class="itemizedLabTotal">Select Payment Plan [OPTIONAL]:</div>
        <div class="itemizedGrandTotal" style="text-align:center;">
          <input type="checkbox" name="paymentPlan" value="1" <cfif Session.user.enrollPaymentPlan>checked</cfif> class="checkbox" />        
        </div>
      </div>
      </cfif>
      
      <div class="dottedDivider"></div>
      <div class="itemizedHdr" style="text-align:right;">
        <input type="image" src="images/btnFormUpdate.gif" alt="Update Total" name="btnFormUpdate" style="width:108px;height:17px; border:0px solid ##ffffff;" border="0" id="btnFormUpdate" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormUpdate','','images/btnFormUpdateo.gif',1)" />
      </div>
      
      <cfif hasPaymentPlanEligibleClass>
      <div class="itemizedHdr" style="margin:0px 0px 10px 0px;">
        <span class="accent03">
          * A class that costs between $400 and $750 qualifies for the 2-Installment Payment Plan. The initial payment will be 1/2 the cost of the class plus $10.
            A class that costs $750 or more qualifies for the 3-Installment Payment Plan. The initial payment will be 1/3 the cost of the class plus $10.
        </span>
      </div>
      </cfif>
    </form>
    
    <div class="floatLeftFullWdth">
    <cfif IsDefined("URL.er2") AND URL.er2>
      <div class="accent01" style="text-align:center;background-color:##FFFF00; padding:5px 5px; margin:10px 30px 10px 30px;">
        <i><b>Error: please make sure all required fields are entered correctly.</b></i>
      </div>      
    </cfif>    
    
    <form name="frmBilling" id="frmBilling" action="#Application.siteURLRootSSL#/action.cfm?md=class&task=updateBillingInfo" method="post" onsubmit="return submitBillingInfo(this);">
      <input type="hidden" name="classIDList" value="#classIDList#" />
      <input type="hidden" name="feePerCoupleClassIDList" value="#feePerCoupleClassIDList#" />
      <input type="hidden" name="serviceIDList" value="#serviceIDList#" />
      <input type="hidden" name="feePerCoupleServiceIDList" value="#feePerCoupleServiceIDList#" />
      <div class="unit">
        <span class="section">Attendee Information</span>
        
        <!--- <div class="singleField">
          <label for="attendeeFirstName" class="descrip">First Name *</label>
          <input type="text" name="attendeeFirstName" id="attendeeFirstName" value="#HTMLEditFormat(Session.user.attendeeFirstName)#" />
        </div>
        <div class="singleField">
          <label for="attendeeLastName" class="descrip">Last Name *</label>
          <input type="text" name="attendeeLastName" id="attendeeLastName" value="#HTMLEditFormat(Session.user.attendeeLastName)#" />
        </div> --->
        
        <cfloop index="idx" from="1" to="#ArrayLen(penciledInClasses)#">
          <cfset clID=penciledInClasses[idx].classID>
          <cfif idx GT 1><div class="dottedDivider" style="margin-bottom:20px;"></div></cfif>
          <div class="class-title">#penciledInClasses[idx].courseTitle#</div>
          <div class="singleField">
            <cfif penciledInClasses[idx].feePerCouple And Compare(penciledInClasses[idx].attendeeNamelabel,"")>
            <label for="attendeeName#clID#_#idx#" class="descrip">#penciledInClasses[idx].attendeeNamelabel# *</label>
            <cfelse>
            <label for="attendeeName#clID#_#idx#" class="descrip">Attendee Name *</label>
            </cfif>
            
            <cfif Compare(penciledInClasses[idx].attendeeName,"")>
            <input type="text" name="attendeeName#clID#_#idx#" id="attendeeName#clID#_#idx#" value="#HTMLEditFormat(penciledInClasses[idx].attendeeName)#" />
            <cfelse>
            <input type="text" name="attendeeName#clID#_#idx#" id="attendeeName#clID#_#idx#" value="#HTMLEditFormat(Session.user.contactFirstName & " " & Session.user.contactLastName)#" />
            </cfif>
          </div>               
          <cfif penciledInClasses[idx].feePerCouple>            
            <div class="singleField">
              <cfif Compare(penciledInClasses[idx].guestNamelabel,"")>
              <label for="guestNames#clID#_#idx#" class="descrip">#penciledInClasses[idx].guestNamelabel# *</label>
              <cfelse>
              <label for="guestNames#clID#_#idx#" class="descrip">Guest(s) *</label>
              </cfif>
              <input type="text" name="guestNames#clID#_#idx#" id="guestNames#clID#_#idx#" value="#HTMLEditFormat(penciledInClasses[idx].guestNames)#" />
              (separate names using commas)
            </div>           
          <cfelse>
            <input type="hidden" name="guestNames#clID#_#idx#" id="guestNames#clID#_#idx#" value="" />
          </cfif>          
        </cfloop>  
        <cfloop index="idx" from="1" to="#ArrayLen(penciledInServices)#">
          <cfset sID=penciledInServices[idx].serviceID>
          <cfif ArrayLen(penciledInClasses) GT 0 OR idx GT 1><div class="dottedDivider" style="margin-bottom:20px;"></div></cfif>
          <div class="class-title">#penciledInServices[idx].serviceTitle#</div>
          <div class="singleField">
            <label for="serviceAttendeeName#sID#_#idx#" class="descrip">Attendee Name *</label>
            <cfif Compare(penciledInServices[idx].attendeeName,"")>
            <input type="text" name="serviceAttendeeName#sID#_#idx#" id="serviceAttendeeName#sID#_#idx#" value="#HTMLEditFormat(penciledInServices[idx].attendeeName)#" />
            <cfelse>
            <input type="text" name="serviceAttendeeName#sID#_#idx#" id="serviceAttendeeName#sID#_#idx#" value="#HTMLEditFormat(Session.user.contactFirstName & " " & Session.user.contactLastName)#" />
            </cfif>
          </div>               
          <cfif penciledInServices[idx].feePerCouple>            
            <div class="singleField">
              <label for="serviceGuestNames#sID#_#idx#" class="descrip">Guest(s) *</label>
              <input type="text" name="serviceGuestNames#sID#_#idx#" id="serviceGuestNames#sID#_#idx#" value="#HTMLEditFormat(penciledInServices[idx].guestNames)#" />
              (separate names using commas)
            </div>           
          <cfelse>
            <input type="hidden" name="serviceGuestNames#sID#_#idx#" id="serviceGuestNames#sID#_#idx#" value="" />
          </cfif>          
        </cfloop>
      </div>
      <div class="unit">
        <span class="section">Contact Information</span>
        <div class="singleField">
          <label for="contactFirstName" class="descrip">First Name *</label>
          <input type="text" name="contactFirstName" id="contactFirstName" value="#HTMLEditFormat(Session.user.contactFirstName)#" />
        </div>
        <div class="singleField">
          <label for="contactLastName" class="descrip">Last Name *</label>
          <input type="text" name="contactLastName" id="contactLastName" value="#HTMLEditFormat(Session.user.contactLastName)#" />
        </div>
        <div class="singleField">
          <label for="contactAddress1" class="descrip">Address *</label>
          <input name="contactAddress1" type="text" id="contactAddress1" value="#HTMLEditFormat(Session.user.contactAddress1)#" />
        </div>
        <div class="singleField">
          <label for="contactAddress2" class="descrip">&nbsp;</label>
          <input type="text" name="contactAddress2" id="contactAddress2" value="#HTMLEditFormat(Session.user.contactAddress2)#" />
        </div>
        <div class="singleField">
          <label for="contactCity" class="descrip">City *</label>
          <input type="text" name="contactCity" id="contactCity" value="#HTMLEditFormat(Session.user.contactCity)#" />
        </div>
        <div class="singleField">
          <label for="contactState" class="descrip">State *</label>
          <select name="contactState" id="contactState">
            <option value=""></option>
            #UT.getOptionList("States", Session.user.contactState)#
          </select>
        </div>
        <div class="singleField">
          <label for="contactZip" class="descrip">Zip *</label>
          <input name="contactZip" type="text" id="contactZip" size="5" value="#HTMLEditFormat(Session.user.contactZip)#" />
        </div>
        <div class="singleField">
          <label for="daytimePhonePart1" class="descrip">Daytime Phone *</label>
          <input name="daytimePhonePart1" type="text" class="phone" id="daytimePhonePart1" value="#HTMLEditFormat(Session.user.daytimePhonePart1)#" size="3" maxlength="3" onKeyUp="autotab(this, document.frmBilling.daytimePhonePart2, 3);" />
          <span style=" font-size:12px; line-height:102%;">-</span>
          <input name="daytimePhonePart2" type="text" class="phone" id="daytimePhonePart2" value="#HTMLEditFormat(Session.user.daytimePhonePart2)#" size="3" maxlength="3" onKeyUp="autotab(this, document.frmBilling.daytimePhonePart3, 3);" />
          <span style=" font-size:12px; line-height:102%;">-</span>
          <input name="daytimePhonePart3" type="text" class="phone" id="daytimePhonePart3" value="#HTMLEditFormat(Session.user.daytimePhonePart3)#" size="4" maxlength="4" />
        </div> 
        <div class="singleField">
          <label for="cellPhonePart1" class="descrip">Cell Phone</label>
          <input name="cellPhonePart1" type="text" class="phone" id="cellPhonePart1" value="#HTMLEditFormat(Session.user.cellPhonePart1)#" size="3" maxlength="3" onKeyUp="autotab(this, document.frmBilling.cellPhonePart2, 3);" />
          <span style=" font-size:12px; line-height:102%;">-</span>
          <input name="cellPhonePart2" type="text" class="phone" id="cellPhonePart2" value="#HTMLEditFormat(Session.user.cellPhonePart2)#" size="3" maxlength="3" onKeyUp="autotab(this, document.frmBilling.cellPhonePart3, 3);" />
          <span style=" font-size:12px; line-height:102%;">-</span>
          <input name="cellPhonePart3" type="text" class="phone" id="cellPhonePart3" value="#HTMLEditFormat(Session.user.cellPhonePart3)#" size="4" maxlength="4" />
        </div>      
      </div>
      <cfif cartTotal GT 0>
        <div class="unit">
          <span class="section">Payment Information</span>
          <div class="singleField">
            <label for="paymentMethod" class="descrip">Payment Method</label>
            <div class="radio-group">
              <input type="radio" name="paymentMethod" value="Credit Card" onchange="togglePaymentMethod(this)" <cfif Not CompareNoCase(Session.user.paymentMethod,"Credit Card")>checked</cfif> /> Credit Card
              <input type="radio" name="paymentMethod" value="Bank Account" onchange="togglePaymentMethod(this)" <cfif Not CompareNoCase(Session.user.paymentMethod,"Bank Account")>checked</cfif> /> Bank Check
              <!--- <br />
              <input type="radio" name="paymentMethod" value="Cash" onchange="togglePaymentMethod(this)" <cfif Not CompareNoCase(Session.user.paymentMethod,"Cash")>checked</cfif> /> Pay with check or cash in person --->
            </div>
          </div>
          <div id="divPaymentMethod" style="display:block;">
              <cfif Not CompareNoCase(Session.user.paymentMethod,"Credit Card")><cfset display="block"><cfelse><cfset display="none"></cfif>      
              <div id="divCardInfo" style="display:#display#;">
                <div class="singleField">
                  <label for="cardType" class="descrip">Card Type </label>
                  <select name="cardType" id="cardType">
                    <option value="">Please Select...</option>
                    <option value="MasterCard"<cfif Not CompareNoCase(Session.user.cardType, "MasterCard")> selected</cfif>>MasterCard</option>
                    <option value="Visa"<cfif Not CompareNoCase(Session.user.cardType, "Visa")> selected</cfif>>Visa</option>
                    <option value="American Express"<cfif Not CompareNoCase(Session.user.cardType, "American Express")> selected</cfif>>American Express</option>
                    <option value="Discover"<cfif Not CompareNoCase(Session.user.cardType, "Discover")> selected</cfif>>Discover</option>
                  </select>
                </div>
                <div class="singleField">
                  <label for="nameOnCard" class="descrip">Name as on Card</label>
                  <input type="text" name="nameOnCard" id="nameOnCard" value="#HTMLEditFormat(Session.user.nameOnCard)#" autocomplete="off" />
                </div>
                <div class="singleField">
                  <label for="cardNumber" class="descrip">Card Number</label>
                  <input type="text" name="cardNumber" id="cardNumber" value="#HTMLEditFormat(Session.user.cardNumber)#" autocomplete="off" />
                </div>
                <div class="singleField">
                  <label for="cardExpirationDate" class="descrip">Card Expiration Date</label>
                  <input type="text" name="cardExpirationDate" id="cardExpirationDate" value="#HTMLEditFormat(Session.user.cardExpirationDate)#" autocomplete="off" />
                  ex. 09/2010 </div>
                <div class="singleField">
                  <label for="cardCVV2" class="descrip">Card ID <br />
                  (CVV2/CID) Number</label>
                  <input type="text" name="cardCVV2" id="cardCVV2" value="#HTMLEditFormat(Session.user.cardCVV2)#" autocomplete="off" />
                  (<a href="javascript:helpCVV();">What is the Card ID?</a>)
                </div>
              </div>
              
              <cfif Not CompareNoCase(Session.user.paymentMethod,"Bank Account")><cfset display="block"><cfelse><cfset display="none"></cfif>  
              <div id="divBankInfo" style="display:#display#;">
                <!--- default to checking account for now
				<div class="singleField">
                  <label for="accountType" class="descrip">Account Type</label>
                  <select name="accountType" id="accountType">
                    <option value="">Please Select...</option>
                    <option value="Checking"<cfif Not CompareNoCase(Session.user.accountType, "Checking")> selected</cfif>>Checking</option>
                    <option value="Savings"<cfif Not CompareNoCase(Session.user.accountType, "Savings")> selected</cfif>>Savings</option>                    
                  </select>
                </div> --->
                <div class="singleField">
                  <label for="nameOnCard" class="descrip">Name on Account</label>
                  <input type="text" name="nameOnAccount" id="nameOnAccount" value="#HTMLEditFormat(Session.user.nameOnAccount)#" />
                </div>
                <div class="singleField">
                  <label for="accountNumber" class="descrip">Checking Account Number</label>
                  <input type="text" name="accountNumber" id="accountNumber" value="#HTMLEditFormat(Session.user.accountNumber)#" />
                </div>
                <div class="singleField">
                  <label for="routingNumber" class="descrip">Bank Routing Number</label>
                  <input type="text" name="routingNumber" id="routingNumber" value="#HTMLEditFormat(Session.user.routingNumber)#" />
                </div>
              </div>
          </div>        
          
          <!---
		  <div class="dottedDivider" style="margin-bottom:20px;"></div>
		  <div class="singleField">
            <label for="payWithCash" class="descrip">Pay with check or cash in person.</label>
            <input type="checkbox" name="payWithCash" id="payWithCash" class="checkbox" value="1" onclick="toggleCardForm(this)" <cfif Session.user.payWithCash>checked</cfif> />
          </div>
		  --->
        </div>
        <div class="unit">
          <span class="section">Billing Information</span>
          <div class="singleField">
            <label for="billingSameAsContact" class="descrip">Same as Above</label>
            <input type="checkbox" name="billingSameAsContact" id="billingSameAsContact" value="1" class="checkbox" onclick="copyToBilling(this)" />
          </div>
          <div class="singleField">
            <label for="billingFirstName" class="descrip">First Name</label>
            <input type="text" name="billingFirstName" id="billingFirstName" value="#HTMLEditFormat(Session.user.billingFirstName)#" />
          </div>
          <div class="singleField">
            <label for="billingLastName" class="descrip">Last Name</label>
            <input type="text" name="billingLastName" id="billingLastName" value="#HTMLEditFormat(Session.user.billingLastName)#" />
          </div>
          <div class="singleField">
            <label for="billingAddress1" class="descrip">Address</label>
            <input name="billingAddress1" type="text" id="billingAddress1" value="#HTMLEditFormat(Session.user.billingAddress1)#" />
          </div>
          <div class="singleField">
            <label for="billingAddress2" class="descrip">&nbsp;</label>
            <input type="text" name="billingAddress2" id="billingAddress2" value="#HTMLEditFormat(Session.user.billingAddress2)#" />
          </div>
          <div class="singleField">
            <label for="billingCity" class="descrip">City</label>
            <input type="text" name="billingCity" id="billingCity" value="#HTMLEditFormat(Session.user.billingCity)#" />
          </div>
          <div class="singleField">
            <label for="billingState" class="descrip">State</label>
            <select name="billingState" id="billingState">
              <option value=""></option>
              #UT.getOptionList("States", Session.user.billingState)#
            </select>
          </div>
          <div class="singleField">
            <label for="billingZip" class="descrip">Zip</label>
            <input name="billingZip" type="text" id="billingZip" size="5" value="#HTMLEditFormat(Session.user.billingZip)#" />
          </div>
      </div>
      </cfif>
      <div class="dottedDivider" style="margin-bottom:20px;"></div>
      <div class="unit" style="text-align:center;">
        <input type="image" src="images/btnFormSubmit.gif" alt="Submit" name="btnFormSubmit" style="width:63px;height:17px; border:0px solid ##ffffff;" border="0" id="btnFormSubmit" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormSubmit','','images/btnFormSubmito.gif',1)" />
      </div>
    </form>
    </div>
  </div>
</cfoutput>