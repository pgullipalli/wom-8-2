<cfinclude template="_create_user_session.cfm">
<cfif Not Session.user.isLoggedIn>
  <cflocation url="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=login&next=profile">
</cfif>
<div id="topperHTMLCodes" style="display:none;">Edit Profile</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);   
});

function submitChange(change) {
  var form=document.frmProfile;
  form.change.value=change;
  
  if (form.password.value=="") {
    alert("Please enter your password.");
	form.password.focus();
	return;
  }
  
  if (change == "email") {
    if (!checkEmail(form.newEmail.value)) {
	  alert("Please enter an valid email address.");
	  form.newEmail.focus();
	  return;
	} 
	
	if (form.newEmail.value != form.newEmail2.value) {
	  alert("The email addresses you enteres do not match.");
	  form.newEmail.focus();
	  return;
	}
  } else if (change == "password") {
    if (form.newPassword.value.length < 6) {
	  alert("Please select a password with at least 6 characters.");
	  form.newPassword.focus();
	  return;
	}
	if (form.newPassword.value != form.newPassword2.value) {
	  alert("The passwords you entered do not match.");
	  form.newPassword.focus();
	  return;
	}
  } else if (change == "fitness") {
    if (form.fitnessClubMemberID.value == "") {
	  alert("Please enter your Fitness Club Member number.");
	  form.fitnessClubMemberID.focus();
	  return;	  
	}
  }
  
  form.submit();
}
</script>
<div class="contentWrapper" id="forms">
  <div class="floatLeftFullWdth">
    <cfoutput>
    <form id="frmProfile" name="frmProfile" action="#Application.siteURLRootSSL#/action.cfm?md=class&task=updateProfile" method="post" onsubmit="return false;">
      <input type="hidden" name="change" value="" />
      
      <cfif IsDefined("URL.success") And URL.success>
      <div class="accent02">
        <b>
          <cfswitch expression="#URL.code#">
            <cfcase value="email">
              Your e-mail address has been updated.
            </cfcase>
            <cfcase value="password">
              Your password has been changed.
            </cfcase>
            <cfcase value="reminder">
              Your class reminder e-mail status has been updated.
            </cfcase>
            <cfcase value="fitness">
              Your Fitness Club Member number has been updated.
            </cfcase>
          </cfswitch>
        </b><br style="clear:both;" /><br />
      </div>
      </cfif>
      
      <div class="unit">
        <div class="section">Your Current Login Information</div>
        
        <cfif IsDefined("URL.er") And Not Compare(URL.code, "login")>   
        <div class="singleField">  
          <div class="accent02"><b>Sorry, but your login information is not recognized. Please try again.</b><br /></div>
        </div>
        </cfif>      
        
        <div class="singleField">
          <label for="email" class="descrip">E-Mail Address</label>
          <input type="text" name="email" id="email" value="#HTMLEditFormat(Session.user.email)#" />
        </div>        
        <div class="singleField">
          <label for="password" class="descrip">Password</label>
          <input type="password" name="password" id="password" />
         </div>
      </div>
      <div class="unit">
        <div class="section">New E-Mail Address</div>
        
        <cfif IsDefined("URL.er") And Not Compare(URL.code, "email")>   
        <div class="singleField">  
          <div class="accent02"><b>It appears that the e-mail address you entered has been used.</b><br /></div>
        </div>
        </cfif>        
        
        <div class="singleField">
          <label for="newEmail" class="descrip">E-Mail Address</label>
          <input type="text" name="newEmail" id="newEmail" />
        </div>  
        <div class="singleField">
          <label for="newEmail2" class="descrip">Re-Type E-Mail Address</label>
          <input type="text" name="newEmail2" id="newEmail2" />
        </div>    
        <a href="javascript:submitChange('email');" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnChangePink1','','images/btnChangePinko.gif',1)"><img src="images/btnChangePink.gif" alt="Submit" name="btnChangePink1" width="103" height="17" border="0" id="btnChangePink1" /></a>  
      </div>
      
      <div class="unit">
        <div class="section">New Password</div>
        <div class="singleField">
          <label for="newPassword" class="descrip">Password</label>
          <input type="password" name="newPassword" id="newPassword" />
        </div>    
        <div class="singleField">
          <label for="newPassword2" class="descrip">Re-Type Password</label>
          <input type="password" name="newPassword2" id="newPassword2" />
        </div>
        <a href="javascript:submitChange('password');" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnChangePink2','','images/btnChangePinko.gif',1)"><img src="images/btnChangePink.gif" alt="Submit" name="btnChangePink2" width="103" height="17" border="0" id="btnChangePink2" /></a>  
      </div>
      
      <div class="unit">
        <div class="section">Reminder E-Mail</div>        
        <div class="singleField">          
          <label class="descrip"><input type="checkbox" name="sendReminderEmail" id="sendReminderEmail" value="1" <cfif Session.user.sendReminderEmail>checked</cfif> style="border-width:0px;" /></label>          
          <span style="font-size:12px; line-height:125%;">&nbsp;&nbsp;&nbsp;Please send me a class reminder via e-mail two days<br />&nbsp;&nbsp;&nbsp;before my classes.</span>          
        </div>
        <a href="javascript:submitChange('reminder');" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnChangePink3','','images/btnChangePinko.gif',1)"><img src="images/btnChangePink.gif" alt="Submit" name="btnChangePink3" width="103" height="17" border="0" id="btnChangePink3" /></a>  
      </div>
      
      
      
      <div class="unit">        
        <span class="section">Fitness Club Member</span>
        <label class="title">For Discounts When Applicable:</label>
        <cfif Session.user.verifyFitnessClubMembership()><!--- is currently a member --->
        <div class="singleField">
          <span class="accent03" style="font-size:11px;">Your Number On File: #Session.user.fitnessClubMemberID# </span> 
        </div>
        </cfif>
        
        <cfif IsDefined("URL.er") And Not Compare(URL.code, "fitness")>
          <div class="singleField">  
            <div class="accent02"><b>Sorry, but the Fitness Club Member number you entered either has already been used or can not be verified at this time.</b><br /></div>
          </div>
        </cfif>
        <div class="singleField">          
          <label for="fitnessClubMemberID" class="descrip">Enter Number</label>
          <input type="text" name="fitnessClubMemberID" id="fitnessClubMemberID" />
        </div>
        <a href="javascript:submitChange('fitness');" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnChangePink4','','images/btnChangePinko.gif',1)"><img src="images/btnChangePink.gif" alt="Submit" name="btnChangePink4" width="103" height="17" border="0" id="btnChangePink4" /></a>
      </div>
      
    </form>
    </cfoutput>
  </div>
</div>
