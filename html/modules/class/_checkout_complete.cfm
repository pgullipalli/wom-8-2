<cfinclude template="_create_user_session.cfm">
<cfif Not Session.user.isLoggedIn>
  <cflocation url="#Application.siteURLRootSSL#/index.cfm?md=class&tmp=login&next=checkout">
</cfif>

<!--- <cfset UT=CreateObject("component", "com.Utility").init()>
<cfset penciledInClasses=Session.user.getPenciledInClasses()>
<cfset penciledInServices=Session.user.getPenciledInServices()>
<cfset donationAmount=Session.user.getDonationAmount()>
<cfset cartTotal=Session.user.getCartTotal()>
 --->
<div id="topperHTMLCodes" style="display:none;">Class Registration</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);   
});
</script>

<style type="text/css">
.section a, .section a:link, .section a:hover {font-size:11px;color:#e3369c; text-decoration:none;float:right;}
.section a:hover {font-size:11px;text-decoration:underline;float:right;}
.itemizedClass .date-range {color:#333132; font-size:11px;}
.class-title {color:#EC6CB8; margin:10px 0px 20px 0px; font-size:12px; padding:0px 15px; font-weight:bold;}
.singleField .formValue {font-size:11px; color:#e3369c; line-height:110%; text-align:left; padding-left:10px; float:left;}
</style>

<div class="contentWrapper" id="forms">  
  <cfif IsDefined("URL.derr") And URL.derr>
	<div class="accent01" style="background-color:##FFFF00; padding:10px 10px; margin:0px 20px 20px 20px; float:left">
	  You class registration is complete, but, due to some technical problem, we are unable to process your donation at this time. Please use our <a href="#Application.siteURLRootSSL#/index.cfm?md=customform&amp;tmp=donation1">donation form</a> to submit your donation. Thank you!
	</div>
  </cfif>

  <cfoutput>#Session.user.receiptHTML#</cfoutput>   
</div>

<cfset Session.user.emptyCart()>

<cfset Session.user.cardType="">
<cfset Session.user.nameOnCard="">
<cfset Session.user.cardNumber="">
<cfset Session.user.cardExpirationDate="">
<cfset Session.user.cardNumber="">
<cfset Session.user.cardCVV2="">
<cfset Session.user.nameOnAccount="">
<cfset Session.user.accountNumber="">
<cfset Session.user.routingNumber="">