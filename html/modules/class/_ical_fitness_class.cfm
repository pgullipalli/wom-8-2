<cfparam name="URL.fctid"><!--- fitnessClassTimeID --->
<cfscript>
CL=CreateObject("component","com.Class").init();
UT=CreateObject("component","com.Utility").init();
fitnessClassInfo=CL.getFitnessClassInfoByFitnessClassTimeID(URL.fctid);

if (fitnessClassInfo.recordcount GT 0) {
  newLine=Chr(13) & Chr(10);
  eventStr = StructNew();
  eventStr.name = fitnessClassInfo.classTitle;
  eventStr.location = fitnessClassInfo.location;
  
  currentDateTime=now();  
  currentDayNumber=DayOfWeek(currentDateTime);
  eventStr.startDateTime=ParseDateTime("#DateFormat(currentDateTime,"m/d/yyyy")# #TimeFormat(fitnessClassInfo.startTime,"H:mm")#");
  eventStr.endDateTime=ParseDateTime("#DateFormat(currentDateTime,"m/d/yyyy")# #TimeFormat(fitnessClassInfo.endTime,"H:mm")#");
  daysOffset=fitnessClassInfo.dayOfWeekAsNumber - currentDayNumber;
  if (daysOffset LT 0) daysOffset = daysOffset + 7;
  eventStr.startDateTime=DateAdd("d", daysOffset, eventStr.startDateTime);
  eventStr.endDateTime=DateAdd("d", daysOffset, eventStr.endDateTime);
  eventStr.recurring = 1;  
  
  if (Compare(fitnessClassInfo.instructorName,"")) {
    eventStr.description="Class: " & fitnessClassInfo.classTitle & "\n\nInstructor: " & fitnessClassInfo.instructorName;
  } else {
    eventStr.description="";
  }
  
  iCalString=Trim(UT.createiCalendarString(eventStr));
  fileName="fitness-class-#URL.fctid#.ics";
} else {
  iCalString="Sorry, the class you are looking for is not available at this time.";
  fileName="fitness-class-#URL.fctid#.txt";
}
</cfscript>
<cfcontent type="text/calendar" reset="Yes"><cfheader name="Content-Disposition" value="inline; filename=#fileName#"><cfoutput>#iCalString#</cfoutput>