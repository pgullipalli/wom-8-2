<cfsilent>
<cfscript>
  CL=CreateObject("component","com.Class").init();
  upcomingClasses=CL.getFeaturedUpComingClasses(2);
  allCategories=CL.getAllFeaturedCategories();
  featuredClass=CL.getFeaturedClassOnLandingPage();
</cfscript>
</cfsilent>
<div id="topperHTMLCodes" style="display:none;">Classes &amp; Programs</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);  
});
</script>

<div class="contentWrapper" id="classes">
  <!--INTRO-->
  <div class="intro">
    <p class="accent02">Woman&rsquo;s offers a large variety of classes &amp; programs. To view, please select a method below.</p>
    <p>Our wide selection of pregnancy and parenting classes is just the beginning - Woman's offers classes for women during all stages of life, including topics as such as health and wellness, nutrition, coping with stress and illness, and other topics vital to a healthy, fulfilling life. Most classes are held at Woman's Hospital or Woman's Center for Wellness.</p>
  </div> 
  
  <cfif upcomingClasses.recordcount GT 0>
  <!--Upcoming Classes and Programs-->
  <div class="twoColWrapper" id="upcoming">
    <div class="floatLeft"><img src="images/imgClassUpcomingHdr.gif" alt="Upcoming Classes &amp; Programs" width="456" height="24" /></div>
    <cfoutput>
    <div class="halfLeft">
      <div class="content">
        <div class="eventDate">
          <div class="monthAbbr">#DateFormat(upcomingClasses.startDateTime[1],"MMM")#</div>
          <div class="dayNumber">#DateFormat(upcomingClasses.startDateTime[1],"dd")#</div>
        </div>
        <div class="eventDescrip"><a href="index.cfm?md=class&tmp=detail&cid=#upcomingClasses.courseID[1]#&tid=#Application.template.templateID_Class#" class="lnkEventName">#upcomingClasses.courseTitle[1]#</a> #LCase(TimeFormat(upcomingClasses.startDateTime[1],"h:mm tt"))#</div>
      </div>
      <cfif Compare(upcomingClasses.courseThumbnail[1],"")>
      <div class="classPht"><img src="#upcomingClasses.courseThumbnail[1]#" width="211" height="75" /></div>
      </cfif>
      <div class="content"><span class="small10">#upcomingClasses.shortDescription[1]#</span><a href="index.cfm?md=class&tmp=detail&cid=#upcomingClasses.courseID[1]#&tid=#Application.template.templateID_Class#" class="lnkAccent01">REGISTER NOW &raquo;</a></div>
    </div>    
    <cfif upcomingClasses.recordcount GT 1>
    <div class="halfRt">
      <div class="content">
        <div class="eventDate">
          <div class="monthAbbr">#DateFormat(upcomingClasses.startDateTime[2],"MMM")#</div>
          <div class="dayNumber">#DateFormat(upcomingClasses.startDateTime[2],"dd")#</div>
        </div>
        <div class="eventDescrip"><a href="index.cfm?md=class&tmp=detail&cid=#upcomingClasses.courseID[2]#&tid=#Application.template.templateID_Class#" class="lnkEventName">#upcomingClasses.courseTitle[2]#</a> #LCase(TimeFormat(upcomingClasses.startDateTime[2],"h:mm tt"))#</div>
        <!--<div class="clearLeft"></div>-->
      </div>
      <cfif Compare(upcomingClasses.courseThumbnail[2],"")>
      <div class="classPht"><img src="#upcomingClasses.courseThumbnail[2]#" width="211" height="75" /></div>
      </cfif>
      <div class="content"><span class="small10">#upcomingClasses.shortDescription[2]#</span><a href="index.cfm?md=class&tmp=detail&cid=#upcomingClasses.courseID[2]#&tid=#Application.template.templateID_Class#" class="lnkAccent01">REGISTER NOW &raquo;</a></div>
    </div>
    </cfif>
    </cfoutput>
    <img src="images/imgClassUpcomingFtr.gif" width="456" height="24" /></div>
  </cfif>
  <!--Browse Categories-->
  <span class="subHead">Browse through the categories... </span>
  <div class="floatLeftFullWdth">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="categories">
      <cfset numRows=Ceiling(allCategories.recordcount/2)>
      <cfoutput>
      <cfloop index="r" from="1" to="#numRows#">
      <tr>
        <cfset idx=(r-1)*2+1>
        <td class="lftCol"><a href="index.cfm?md=class&tmp=category&catid=#allCategories.categoryID[idx]#">#allCategories.categoryName[idx]#</a></td>
        <cfset idx=idx+1>
        <cfif idx LTE allCategories.recordcount>
        <td class="rtCol"><a href="index.cfm?md=class&tmp=category&catid=#allCategories.categoryID[idx]#">#allCategories.categoryName[idx]#</a></td>
        <cfelse>
        <td class="rtCol">&nbsp;</td>
        </cfif>
      </tr>
      </cfloop>
      </cfoutput>
    </table>
  </div>
  
  <!--Featured Class-->
  <cfif featuredClass.recordcount GT 0>
  <div id="featured">
    <div class="floatLeft"><img src="images/imgClassFeatured.gif" alt="Featured" width="122" height="106" /></div>
    <cfoutput query="featuredClass">
    <div class="content">
      <a href="index.cfm?md=class&tmp=detail&cid=#courseID#&tid=#Application.template.templateID_Class#" class="lnkHead">#courseTitle#</a>
      #shortDescription#<br />
      <a href="index.cfm?md=class&tmp=detail&cid=#courseID#&tid=#Application.template.templateID_Class#"><img src="images/btnClassAvailDates.gif" alt="Click to see Available Dates" width="194" height="19" border="0" style="margin-top:10px;" /></a><br />
    </div>
    </cfoutput>
  </div>
  </cfif>
</div>
