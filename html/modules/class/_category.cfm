<!--- co-mingle Services and Courses --->
<cfparam name="URL.catid">
<cfparam name="URL.startIndex" default="1">
<cfparam name="URL.numItemsPerPage" default="20">
<cfscript>
  UT=CreateObject("component","com.Utility").init();
  CL=CreateObject("component","com.Class").init();
  categoryName=CL.getCategoryName(URL.catid);
  //structCourses=CL.getCoursesByCategoryID(categoryID=URL.catid, startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage);
  structCoursesAndServices=CL.getCoursesAndSerivcesByCategoryID(categoryID=URL.catid, startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage);
</cfscript>
<cfif Compare(categoryName,"")>
<div id="topperHTMLCodes" style="display:none;"><cfoutput>#categoryName#</cfoutput></div>
<cfelse>
<div id="topperHTMLCodes" style="display:none;">Classes &amp; Programs</div>
</cfif>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);  
});
</script>

<!--DISPLAY TOOLS-->
<cfif structCoursesAndServices.numAllItems GT URL.numItemsPerPage>
  <cfset navPanel=UT.displayNextPreviousLinks(pageURL="index.cfm?md=class&tmp=category&catid=#URL.catid#", startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage, numDisplayedItems=structCoursesAndServices.numDisplayedItems, numAllItems=structCoursesAndServices.numAllItems, itemName="Results")>
  <cfoutput>#navPanel#</cfoutput>
</cfif>
  
<div class="contentWrapper" id="classCategory">
  <ul>
  <cfoutput query="structCoursesAndServices.coursesAndServices">
    <li>
      <cfif Compare(type,"service")>
        <a href="index.cfm?md=class&tmp=detail&cid=#ID#&tid=#Application.template.templateID_Class#" class="className">#title#</a>
      <cfelse>
        <a href="index.cfm?md=class&tmp=detail_service&sid=#ID#&tid=#Application.template.templateID_Class#" class="className">#title#</a>
      </cfif> 
      #description#
    </li>
    </cfoutput>
  </ul>
</div>

<!--DISPLAY TOOLS-->
<cfif structCoursesAndServices.numAllItems GT URL.numItemsPerPage>
  <cfoutput>#navPanel#</cfoutput>
</cfif>