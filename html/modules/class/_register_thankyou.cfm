<div id="topperHTMLCodes" style="display:none;">Classes &amp; Programs</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);    
});
</script>


<div class="contentWrapper" id="classes">
  <!--Content-->
  <span class="head">Thank You for Registering</span>
  
  <div class="accent03" style="font-size:11px;"><br />
  <p>Thank you for registering with Woman's Classes and Programs registration system. You will receive an email with your username and password.</p>
  <p><cfoutput><a href="#Application.siteURLRootSSL#/index.cfm?md=class&amp;tmp=myschedule" class="lnkAccent02">View classes</a> that you may want to register for now.</cfoutput></p>
  <p>If you would like to receive Woman's email newsletters, you can <a href="/signup" class="lnkAccent02">sign up here</a>.</p>
  </p>
  </div>
</div>
