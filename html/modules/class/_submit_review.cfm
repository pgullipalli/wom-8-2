<cfparam name="URL.stid"><!--- studentID --->
<cfparam name="URL.id" default="0"><!--- classID/serviceID --->
<cfparam name="URL.type" default="course"><!--- course/service --->
<cfsilent>
<cfscript>
  CL=CreateObject("component","com.Class").init();
  if (URL.type Is "course") {
    classInfo=CL.getCourseByClassID(classID=URL.id, publishedOnly=0);
	if (classInfo.recordcount GT 0) {
	  className=classInfo.courseTitle;
	}
  } else {
    classInfo=CL.getServiceByID(URL.id);
	if (classInfo.recordcount GT 0) {
	  className=classInfo.serviceTitle;
	}
  }  
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none;">Classes &amp; Programs</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_feedback.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_nav_panel.cfm">
</div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
  $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
  $("#thmemImage").after(searchPanelHTMLCodes);  
});

function submitReview(form) {
  for (var i=0; i < form.elements.length; i++) $('#' + form.elements[i].name).removeClass('error');
	
  var error = false;
  
  if (form.fullName.value == "") { $('#fullName').addClass("error"); error = true; }
  
  if (form.programContentMetExpectation.selectedIndex < 1) { $('#programContentMetExpectation').addClass("error"); error = true; }
  if (form.programContentValuable.selectedIndex < 1) { $('#programContentValuable').addClass("error"); error = true; }
  if (form.presenterUnderstandable.selectedIndex < 1) { $('#presenterUnderstandable').addClass("error"); error = true; }
  if (form.presenterKnowledgable.selectedIndex < 1) { $('#presenterKnowledgable').addClass("error"); error = true; }
  if (form.overallSatisfied.selectedIndex < 1) { $('#overallSatisfied').addClass("error"); error = true; }
  if (form.overallRecommendToFriends.selectedIndex < 1) { $('#overallRecommendToFriends').addClass("error"); error = true; }  
  
  /*
  if (form.feedback.value == "") {
    alert("Please enter your feedback.");
    form.feedback.focus();
	return false;
  }*/
  
  if (error) {
    alert('The hilighted field(s) are required. Please make sure you have entered all required fields. Thank you!');
	return false;
  } else {
    return true;
  }

}
</script>

<style type="text/css">
.accent01 {color:#EC6CB8; font-size:11px;} /*light pink*/
#forms label.descrip {text-transform:none; font-size:11px; width:150px;}
.error {background-color:#FFF2F9;}
</style>

<div class="contentWrapper" id="forms">
  <div class="floatLeftFullWdth">
  <cfif classInfo.recordcount GT 0>
    <cfoutput>
    <form name="frmReview" id="frmReview" action="action.cfm?md=class&task=submitReview" method="post" onSubmit="return submitReview(this);">
      <input type="hidden" name="studentID" value="#URL.stid#" />
      <cfif URL.type IS "course">
      <input type="hidden" name="classType" value="course">
      <input type="hidden" name="classID" value="#URL.id#">
      <cfelse>
      <input type="hidden" name="classType" value="service">
      <input type="hidden" name="serviceID" value="#URL.id#">
      </cfif> 
      <input type="hidden" name="className" value="#HTMLEditFormat(className)#">
    
      <div class="unit">
        <span class="section">Submit a Review for '#className#'</span>
        <div class="singleField">
          <label for="fullName" class="descrip">Your Name</label>
          <input type="text" name="fullName" id="fullName" />
        </div>
        
        <div class="singleField accent01">For each of the following questions please select the response that best matches your opinion.</div>
        
        <div class="singleField">
          <label class="descrip"><span class="accent02">PROGRAM CONTENT</span></label>
        </div>                
        <div class="singleField">
          <label class="descrip">The program content met my expectations.</label>
          <select name="programContentMetExpectation" id="programContentMetExpectation">
            <option value=""></option>
            <option value="Strongly Agree" >Strongly Agree</option>
            <option value="Agree" >Agree</option>
            <option value="Undecided" >Undecided</option>
            <option value="Disagree" >Disagree</option>
            <option value="Strongly Disagree" >Strongly Disagree</option>
            <option value="Not Applicable" >Not Applicable</option>
          </select>
        </div>
        <div class="singleField">
          <label class="descrip">The handouts and presentation aids were valuable.</label>
          <select name="programContentValuable" id="programContentValuable">
            <option value=""></option>
            <option value="Strongly Agree" >Strongly Agree</option>
            <option value="Agree" >Agree</option>
            <option value="Undecided" >Undecided</option>
            <option value="Disagree" >Disagree</option>
            <option value="Strongly Disagree" >Strongly Disagree</option>
            <option value="Not Applicable" >Not Applicable</option>
          </select>
        </div>
        
        <div class="singleField">
          <label class="descrip"><span class="accent02">PRESENTER</span></label>
        </div>
        <div class="singleField">
          <label class="descrip">The presenter(s) was easy to follow and understand.</label>
          <select name="presenterUnderstandable" id="presenterUnderstandable">
            <option value=""></option>
            <option value="Strongly Agree" >Strongly Agree</option>
            <option value="Agree" >Agree</option>
            <option value="Undecided" >Undecided</option>
            <option value="Disagree" >Disagree</option>
            <option value="Strongly Disagree" >Strongly Disagree</option>
            <option value="Not Applicable" >Not Applicable</option>
          </select>
        </div>
        <div class="singleField">
          <label class="descrip">The presenter(s) was knowledgable.</label>
          <select name="presenterKnowledgable" id="presenterKnowledgable">
            <option value=""></option>
            <option value="Strongly Agree" >Strongly Agree</option>
            <option value="Agree" >Agree</option>
            <option value="Undecided" >Undecided</option>
            <option value="Disagree" >Disagree</option>
            <option value="Strongly Disagree" >Strongly Disagree</option>
            <option value="Not Applicable" >Not Applicable</option>
          </select>
        </div>
        
        <div class="singleField">
          <label class="descrip"><span class="accent02">OVERALL</span></label>
        </div>
        <div class="singleField">
          <label class="descrip">I was satisfied with the program.</label>
          <select name="overallSatisfied" id="overallSatisfied">
            <option value=""></option>
            <option value="Strongly Agree" >Strongly Agree</option>
            <option value="Agree" >Agree</option>
            <option value="Undecided" >Undecided</option>
            <option value="Disagree" >Disagree</option>
            <option value="Strongly Disagree" >Strongly Disagree</option>
            <option value="Not Applicable" >Not Applicable</option>
          </select>
        </div>
        <div class="singleField">
          <label class="descrip">I would recommend this program to a friend.</label>
          <select name="overallRecommendToFriends" id="overallRecommendToFriends">
            <option value=""></option>
            <option value="Strongly Agree" >Strongly Agree</option>
            <option value="Agree" >Agree</option>
            <option value="Undecided" >Undecided</option>
            <option value="Disagree" >Disagree</option>
            <option value="Strongly Disagree" >Strongly Disagree</option>
            <option value="Not Applicable" >Not Applicable</option>
          </select>
        </div>
        
        <div class="singleField">
          <label class="descrip"><span class="accent02">ADDITIONAL COMMENTS</span></label>
        </div>

        <div class="singleField">
          <label for="feedback"  class="descrip">&nbsp;</label>
          <textarea name="feedback" id="feedback" style="width:200px;height:100px;"></textarea>
        </div>
      </div>
      <div class="unit">
        <input type="image" src="images/btnFormSubmit.gif" alt="Submit" name="btnFormSubmit" style="width:63px;height:17px; border:0px solid ##ffffff;" border="0" id="btnFormSubmit" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormSubmit','','images/btnFormSubmito.gif',1)" /> &nbsp;&nbsp;
        <a href="javascript:document.frmReview.reset();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormClear','','images/btnFormClearo.gif',1)"><img src="images/btnFormClear.gif" alt="Clear" name="btnFormClear" width="63" height="17" border="0" id="btnFormClear" /></a>
      </div>
    </form>
    </cfoutput>
  <cfelse>
    <p class="accent01">
  	Sorry, but the class you are looking for doesn't exist.
    </p>
  </cfif>
  </div>
</div>