<cfparam name="URL.pid" default="0">
<cfsilent>
<cffunction name="getEmbeddedPlayerCodes" access="private" output="no" returntype="string">
  <cfargument name="movieFilePath" type="string" required="yes">
  <cfargument name="playerID" type="string" required="yes">
  <cfargument name="width" type="numeric" default="320">
  <cfargument name="height" type="numeric" default="240">
  <cfset mediaURL=arguments.movieFilePath>
  <cfif FindNoCase("http:", mediaURL) IS 0>
    <cfif Left(MediaURL,1) IS "/">
      <cfset mediaURL = "#APPLICATION.siteURLRoot##mediaURL#">
    <cfelse>
      <cfset mediaURL = "#APPLICATION.siteURLRoot#/#mediaURL#">
    </cfif>
  </cfif>
  
  <cfsavecontent variable="playerCodes">
  <cfoutput>
  <table border="0" cellpadding="0" cellspacing="0" width="#Arguments.width+20#" align="center">
    <tr>
      <td>
  		<div id="#Arguments.playerID#" mediaURL="#mediaURL#" style="display:block;width:#Arguments.width#px;height:#Arguments.height#px;margin:5px 5px;"></div>
      </td>
    </tr>
  </table>
  </cfoutput>
  </cfsavecontent>

  <cfreturn playerCodes>
</cffunction>

<cfscript>
  PB=CreateObject("component", "com.PageBuilder").init();
  pageName=PB.getPageName(pageID=URL.pid);
  if (Find("/admin/",CGI.HTTP_REFERER) GT 0) {
    pageContent=PB.getPageContent(pageID=URL.pid, publishedOnly=0);	
  } else {
    pageContent=PB.getPageContent(pageID=URL.pid);
  }

  pageCategoryName=PB.getPageCategoryNameByPageID(URL.pid);
    
  reg = "\{[a-zA-Z0-9_ \-\/]+\.flv[^\}]*\}";
  posFound = 1;
  posStart = 1;
  numItemsFound = 0;
  strLen = Len(pageContent);
  matchedString=ArrayNew(1);
  while (posFound) {
    matched = REFind(reg, pageContent, posStart, "TRUE");
    posFound = matched.pos[1]; 
    if (posFound GT 0) {
      numItemsFound = numItemsFound + 1;
	  matchedString[numItemsFound] = MID(pageContent, posFound, matched.len[1]);
      posStart = posFound + matched.len[1];
	  if (posStart GT strLen) {
	    posFound = 0;
	  }
    }  
  }
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none"><cfoutput>#pageCategoryName#</cfoutput></div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtMomDaughterCouch.jpg" width="202" height="244" /></div>
<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
	if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  });
</script>

<div class="contentWrapper">
<cfoutput>
<cfif ArrayLen(matchedString) GT 0><!--- has embedded videos --->
  <script src="jsapis/flowplayer/flowplayer-3.1.4.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {  
    var numPlayers=#ArrayLen(matchedString)#;
	for (var i = 1; i <= numPlayers; i++) {
	  var mediaURL=$("##flashPlayer" + i).attr('mediaURL');
	  flowplayer("flashPlayer" + i, "jsapis/flowplayer/flowplayer-3.1.4.swf", { 
		clip: { 
			url: mediaURL, 
			autoPlay: false, 
			autoBuffering: true 
		}
	  });
	}
  });
  </script>

  <cfloop index="idx" from="1" to="#ArrayLen(matchedString)#">
	<cfset movieFilePath = Replace(Replace(matchedString[idx], "{", ""), "}","")>
	<cfset movieFilePathParts=ListToArray(movieFilePath)>
	<cfif ArrayLen(movieFilePathParts) IS 3>
	  <cfset playerCodes = getEmbeddedPlayerCodes(Trim(movieFilePathParts[1]), "flashPlayer#idx#", Trim(movieFilePathParts[2]), Trim(movieFilePathParts[3]))>
	<cfelse>        
	  <cfset playerCodes = getEmbeddedPlayerCodes(Trim(movieFilePath), "flashPlayer#idx#")>
	</cfif>
	<cfset pageContent = Replace(pageContent, matchedString[idx], playerCodes)>  
  </cfloop>

  #pageContent#

  <!--<div style="clear:both;"></div>-->
<cfelse>
#pageContent#
</cfif>
</cfoutput>
</div>
