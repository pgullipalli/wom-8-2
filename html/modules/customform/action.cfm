<cfparam name="URL.task">
<cfparam name="URL.pnid" default="0">
<cfparam name="URL.nid" default="0">
<cfparam name="URL.tid" default="0">
<cfscript>
  CF=CreateObject("component", "com.CustomForm").init();
  UT=CreateObject("component", "com.Utility").init();
  
  switch (URL.task) {
    case 'submitDonation':
	  Form.formID=Application.customform.customFormID_Donation;
	  if (Compare(form.donationAmount, "Other")) {
	    Variables.amount=Form.donationAmount;
	  } else {
	    Variables.amount=Form.donationAmountOther;
	  }
	  expDateParts=ListToArray(Form.cardExpirationDate,"/");
	  Variables.expirationDate=expDateParts[1] & Right(expDateParts[2],2);
	  Variables.orderID=now().getTime();
	  Variables.clientIP=CGI.REMOTE_ADDR;
	  Variables.description="Woman's Hospital Foundation Online Donation - Donation ID: #orderID#";
	  
	  //credit card authorization
	  USAePay=CreateObject("component","com.USAePay").init();
	  ePayResponseStruct=USAePay.makeCreditCardPayment(
							command="cc:sale",
							USAePayKey="#Application.USAePayKeyForDonation#",
							nameOnCard="#Form.nameOnCard#",
							cardNumber="#Form.cardNumber#",
							expirationDate="#Variables.expirationDate#",
							CVV2="#Form.cardCVV#",
							amount="#Variables.amount#",
							address="#Form.billingAddress1#",
							zip="#Form.billingZip#",
							orderID="#Variables.orderID#",
							description="#Variables.description#",
							clientIP="#Variables.clientIP#",
							testMode="0");
	  
	  if (ePayResponseStruct.approved) {
	    Form.authCode=ePayResponseStruct.authCode;
		Form.orderID=Variables.orderID;
		Form.clientIP=Variables.clientIP;
		Form.donationAmount=Variables.amount;
		if (Not Compare(Form.contactTitle, "")) {
		  Form.contactTitle=Form.contactTitleOther;
		}
		if (Not Compare(Form.notifyTitle, "")) {
		  Form.notifyTitle=Form.notifyTitleOther;
		}
		
	    submissionID=CF.submitDonationForm(argumentCollection=Form);
		UT.location("index.cfm?md=customform&tmp=donation_thankyou&sid=" & submissionID  & "&pnid=" & URL.pnid & "&nid=" & URL.nid & "&tid=" & URL.tid);
	  } else {
	    if (ePayResponseStruct.serverError) {
		  UT.location("index.cfm?md=customform&tmp=donation_error&serverError=1&pnid=" & URL.pnid & "&nid=" & URL.nid & "&tid=" & URL.tid);
		} else {
		  UT.location("index.cfm?md=customform&tmp=donation_error&error=" & URLEncodedFormat(ePayResponseStruct.error) & "&pnid=" & URL.pnid & "&nid=" & URL.nid & "&tid=" & URL.tid);
		}  
	  }
      break;

	default:
	  break;
  }
</cfscript>
