<cfprocessingdirective pageencoding="utf-8">
<cfparam name="URL.serverError" default="0">
<cfparam name="URL.error" default="">

<div id="topperHTMLCodes" style="display:none;">Giving Opportunities</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtGivingOpp.jpg" width="202" height="244" /></div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
});
</script>
<div class="contentWrapper">
  <div class="head">Help Support Woman's Hospital Foundation</div>
  
  <p>&nbsp;</p>
  
  <div class="floatLeftFullWdth">
  
  
    <p>Sorry, but your credit card authorization was not approved.</p>
    
    <cfif Compare(URL.error, "")>
    <p style="color:#FF00CC; font-weight:bold;">
    <cfoutput>#URL.error#</cfoutput>
    </p>
    </cfif>
    <p>Please <a href="javascript:history.go(-1);">click here to go back to the previous page</a> and make sure your credit card information was entered correctly.</p>  
  </div>
</div>
