<cfprocessingdirective pageencoding="utf-8">
<cfset UT=CreateObject("component", "com.Utility").init()>
<div id="topperHTMLCodes" style="display:none;">Giving Opportunities</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtGivingOpp.jpg" width="202" height="244" /></div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
});

function toggleFields(radioObj, targetID, displayTarget) {
  var targetObj = document.getElementById(targetID);
  if (targetObj == null) return;
  if ((radioObj.checked && displayTarget) || (!radioObj.checked && !displayTarget) ) {
    targetObj.style.display="block";
  } else {
    targetObj.style.display="none";
  };
}

function submitDonation2(form) {
  for (var i=0; i < form.elements.length; i++) $('#' + form.elements[i].name).removeClass('error');
  
  var error = false;
  
  if (form.creditCardType.selectedIndex < 1) {$('#creditCardType').addClass("error"); error = true;}
  
  if (form.nameOnCard.value == "") {$('#nameOnCard').addClass("error"); error = true;}
  
  if (form.billingAddress1.value == "") {$('#billingAddress1').addClass("error"); error = true;}
  
  if (form.billingCity.value == "") {$('#billingCity').addClass("error"); error = true;}
  
  if (form.billingState.selectedIndex < 1) {$('#billingState').addClass("error"); error = true;}
  
  if (form.billingZip.value == "") {$('#billingZip').addClass("error"); error = true;}
  
  if (form.cardNumber.value == "") {$('#cardNumber').addClass("error"); error = true;}

  if (!checkExpirationDate(form.cardExpirationDate.value)) {
    $('#cardExpirationDate').addClass("error");
	error = true;
  }
  
  if (form.cardCVV.value == "") {$('#cardCVV').addClass("error"); error = true;}
  
  if (form.shippingFirstName.value == "") {$('#shippingFirstName').addClass("error"); error = true;}
  
  if (form.shippingLastName.value == "") {$('#shippingLastName').addClass("error"); error = true;}
  
  if (form.shippingAddress1.value == "") {$('#shippingAddress1').addClass("error"); error = true;}
  
  if (form.shippingCity.value == "") {$('#shippingCity').addClass("error"); error = true;}
  
  if (form.shippingState.value == "") {$('#shippingState').addClass("error"); error = true;}
  
  if (form.shippingZip.value == "") {$('#shippingZip').addClass("error"); error = true;}  
  
  if (form.shippingPhonePart1.value == "") {$('#shippingPhonePart1').addClass("error"); error = true;}
  if (form.shippingPhonePart2.value == "") {$('#shippingPhonePart2').addClass("error"); error = true;}
  if (form.shippingPhonePart3.value == "") {$('#shippingPhonePart3').addClass("error"); error = true;}  

  if (error) {
    alert('The hilighted field(s) are required. Please make sure you have entered all required fields in correct format.');
	return false;
  } else {
    return true;
  }
}

function copyBillingAddress (obj) {
  var form=document.frmDonation2;
  
  if (obj.checked) {
    var spaceIdx=form.nameOnCard.value.indexOf(" ");
	if (spaceIdx > 0 && form.nameOnCard.value.length > spaceIdx) {	
      form.shippingFirstName.value=trim(form.nameOnCard.value.substring(0,spaceIdx));
	  form.shippingLastName.value=trim(form.nameOnCard.value.substring(spaceIdx+1));
	}  
	
	form.shippingAddress1.value=form.billingAddress1.value;
	form.shippingAddress2.value=form.billingAddress2.value;
	form.shippingCity.value=form.billingCity.value;
	form.shippingState.selectedIndex=form.billingState.selectedIndex;
	form.shippingZip.value=form.billingZip.value;
  } 
}

function checkExpirationDate(str) {
  var datePattern = /[0-9]{2}\/20[0-9]{2}$/;
  return datePattern.test(str); 
}

function autotab(obj1, obj2, objLength) {
  if (obj1.value.length == objLength)
    obj2.focus();
}

function helpCVV() {
  var msg='The Card Security Code is located on the back of MasterCard, Visa and Discover credit or debit cards and is typically a separate group of 3 digits to the right of the signature strip.\n\n' +
  		  'On American Express cards, the Card Security Code is a printed (NOT embossed) group of four digits on the front towards the right.';
  alert(msg);
}
</script>
<style type="text/css">
.error {background-color:#FFF2F9;}
</style>
<div class="contentWrapper" id="forms">
  <div class="head">Help Support Woman's Hospital  Foundation</div>
  
  <p>&nbsp;</p>
  
  <div class="floatLeftFullWdth">
    <form id="frmDonation2" name="frmDonation2" method="post" <cfoutput>action="#Application.siteURLRootSSL#/action.cfm?md=customform&task=submitDonation&pnid=#URL.pnid#&nid=#URL.nid#&tid=#URL.tid#"</cfoutput> onSubmit="return submitDonation2(this);">
      <cfoutput>
      <cfloop index="field" list="#Form.fieldNames#">
        <input type="hidden" name="#field#" value="#HTMLEditFormat(Form["#field#"])#">
      </cfloop>
      </cfoutput>    
    
      <div class="unit">
        <span class="section">Credit Card Information</span>
        <div class="singleField">
          <label for="creditCardType" class="descrip">Card Type</label>
          <select name="creditCardType" id="creditCardType">
            <option value=""></option>
            <option value="MasterCard">MasterCard</option>
            <option value="Visa">Visa</option>
            <option value="American Express">American Express</option>
            <option value="Discover">Discover</option>
          </select>
        </div>
        <div class="singleField">
          <label for="nameOnCard" class="descrip">Name as on Card</label>
          <input type="text" name="nameOnCard" id="nameOnCard" />
        </div>
        <div class="singleField">
          <label for="billingAddress1" class="descrip">Card Billing Address</label>
          <input name="billingAddress1" type="text" id="billingAddress1" size="50" />
        </div>
        <div class="singleField">
          <label for="billingAddress2" class="descrip">&nbsp;</label>
          <input type="text" name="billingAddress2" id="billingAddress2" />
        </div>
        <div class="singleField">
          <label for="billingCity" class="descrip">City</label>
          <input type="text" name="billingCity" id="billingCity" />
        </div>
        <div class="singleField">
          <label for="billingState" class="descrip">State</label>
          <select name="billingState" id="billingState">
            <option value=""></option>
            <cfoutput>#UT.getOptionList("States")#</cfoutput>
          </select>
        </div>
        <div class="singleField">
          <label for="billingZip" class="descrip">Zip</label>
          <input name="billingZip" type="text" id="billingZip" size="5" />
        </div>
        <div class="singleField">
          <label for="cardNumber" class="descrip">Card Number</label>
          <input type="text" name="cardNumber" id="cardNumber" autocomplete="off" />
        </div>
        <div class="singleField">
          <label for="cardExpirationDate" class="descrip">Card Expiration Date</label>
          <input type="text" name="cardExpirationDate" id="cardExpirationDate" autocomplete="off" />
          ex. 09/2010 </div>
        <div class="singleField">
          <label for="cardCVV" class="descrip">Card ID <br />
          (CVV2/CID) Number</label>
          <input type="text" name="cardCVV" id="cardCVV" autocomplete="off" />
          (<a href="javascript:helpCVV()">What is the Card ID?</a>)</div>
      </div>
      
      <div class="unit"> <span class="section">Mailing Information</span>
        <div class="singleField">
          <label for="sameAsBilling" class="descrip">Same as billing information</label>
          <input type="checkbox" name="sameAsBilling" id="sameAsBilling" class="checkbox" onclick="copyBillingAddress(this)" />
        </div>
        <div class="singleField">
          <label for="shippingCompanyName" class="descrip">Company Name</label>
          <input type="text" name="shippingCompanyName" id="shippingCompanyName" />
        </div>
        <div class="singleField">
          <label for="shippingFirstName" class="descrip">First Name</label>
          <input type="text" name="shippingFirstName" id="shippingFirstName" />
        </div>
        <div class="singleField">
          <label for="shippingLastName" class="descrip">Last Name</label>
          <input type="text" name="shippingLastName" id="shippingLastName" />
        </div>
        <div class="singleField">
          <label for="shippingAddress1" class="descrip">Address</label>
          <input name="shippingAddress1" type="text" id="shippingAddress1" size="50" />
        </div>
        <div class="singleField">
          <label for="shippingAddress2" class="descrip">&nbsp;</label>
          <input type="text" name="shippingAddress2" id="shippingAddress2" />
        </div>
        <div class="singleField">
          <label for="shippingCity" class="descrip">City</label>
          <input type="text" name="shippingCity" id="shippingCity" />
        </div>
        <div class="singleField">
          <label for="shippingState" class="descrip">State</label>
          <select name="shippingState" id="shippingState">
            <option value=""></option>
            <cfoutput>#UT.getOptionList("States")#</cfoutput>
          </select>
        </div>
        <div class="singleField">
          <label for="shippingZip" class="descrip">Zip</label>
          <input name="shippingZip" type="text" id="shippingZip" size="5" />
        </div>
        <div class="singleField">
          <label for="shippingCountry" class="descrip">Country</label>
          <input type="text" name="shippingCountry" id="shippingCountry" />
        </div>
        <div class="singleField">
          <label for="notifyPhone" class="descrip">Phone</label>
          <input name="shippingPhonePart1" type="text" class="phone" id="shippingPhonePart1" size="3" maxlength="3" onKeyUp="autotab(this, document.frmDonation2.shippingPhonePart2, 3);"  />
          <span style=" font-size:12px; line-height:102%;">-</span>
          <input name="shippingPhonePart2" type="text" class="phone" id="shippingPhonePart2" size="3" maxlength="3" onKeyUp="autotab(this, document.frmDonation2.shippingPhonePart3, 3);"  />
          <span style=" font-size:12px; line-height:102%;">-</span>
          <input name="shippingPhonePart3" type="text" class="phone" id="shippingPhonePart3" size="4" maxlength="4" />
        </div>
      </div>
      <div class="unit">
        <input type="image" src="images/btnFormSubmit.gif" alt="Submit" name="btnFormSubmit" style="width:63px; height:17px;border:0px solid #CCC;" border="0" id="btnFormSubmit" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormSubmit','','images/btnFormSubmito.gif',1)" />
      </div>
    </form>
  </div>
</div>
