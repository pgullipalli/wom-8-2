<cfprocessingdirective pageencoding="utf-8">
<cfparam name="URL.src" default="">
<cfset UT=CreateObject("component", "com.Utility").init()>
<cfset CF=CreateObject("component", "com.CustomForm").init()>
<cfset formInfo=CF.getFormInfo(Application.customform.customFormID_Donation)>
<div id="topperHTMLCodes" style="display:none;">Giving Opportunities</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtGivingOpp.jpg" width="202" height="244" /></div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  
  var form=document.frmDonation1;
  if (form.dedicate[0].checked) $("#divDedicate").show();
  else $("#divDedicate").hide();
  
  if (form.notification[0].checked) $("#divNotification").show();
  else $("#divNotification").hide();
  
  if (form.matchGifts[0].checked) $("#divMatchGifts").show();  
  else $("#divMatchGifts").hide();  
  
  if (form.contactTitle.selectedIndex == 5) $("#divContactTitleOther").show();
  else $("#divContactTitleOther").hide(); 
});

function toggleFields(radioObj, targetID, displayTarget) {
  var targetObj = document.getElementById(targetID);
  if (targetObj == null) return;
  if ((radioObj.checked && displayTarget) || (!radioObj.checked && !displayTarget) ) {
    targetObj.style.display="block";
  } else {
    targetObj.style.display="none";
  };
}

function submitDonation1(form) {
  for (var i=0; i < form.elements.length; i++) $('#' + form.elements[i].name).removeClass('error');
	
  var error = false;
  if (form.contactTitle.selectedIndex < 1) {$('#contactTitle').addClass("error"); error = true;}
  if (form.contactTitle.selectedIndex ==5 && form.contactTitleOther.value == "") {$('#contactTitleOther').addClass("error"); error = true;}
  
  if (form.firstName.value == "") {$('#firstName').addClass("error"); error = true;}
  
  if (form.lastName.value == "") {$('#lastName').addClass("error"); error = true;}
  
  if (form.address1.value == "") {$('#address1').addClass("error"); error = true;}
  
  if (form.city.value == "") {$('#city').addClass("error"); error = true;}
  
  if (form.state.selectedIndex < 1) {$('#state').addClass("error"); error = true;}
  
  if (form.zip.value == "") {$('#zip').addClass("error"); error = true;}
  
  if (form.phonePart1.value == "") {$('#phonePart1').addClass("error"); error = true;}
  if (form.phonePart2.value == "") {$('#phonePart2').addClass("error"); error = true;}
  if (form.phonePart3.value == "") {$('#phonePart3').addClass("error"); error = true;}
  
  if (!checkEmail(form.emailAddress.value)) {$('#emailAddress').addClass("error"); error = true;}

  if (form.donationAmount.selectedIndex < 1) {
    $('#donationAmount').addClass("error");
	error = true;
  } else {
	if (form.donationAmount.options[form.donationAmount.selectedIndex].value == "Other") {//select other
	  var amount=form.donationAmountOther.value;
	  amount = amount.replace(/\$/g,"");
	  amount = amount.replace(/,/g,"");
		
	  if (amount.indexOf(".") == -1) {
		amount = amount + ".00";
	  }
		
	  if (!checkDollarFormat(amount)) {
	    alert("Please enter a donation amount in a correct number format.");
		$('#donationAmountOther').addClass("error");
		error = true;
	  } else {
		form.donationAmountOther.value=amount;
	  }	
	} else {//select a pre-defined amount
	  if (form.donationAmountOther.value != "") {
	    alert("Please either select a donation amount from the drop-down or enter your own amount.");
		$('#donationAmountOther').addClass("error");
		error = true;
	  }
	}
  }
  
  /*if (form.matchGifts[0].checked && form.matchCompany.value == "") {
    $('#matchCompany').addClass("error");
	error = true;
  }*/
  
  if (error) {
    alert('The hilighted field(s) are required. Please make sure you have entered all required fields in correct format.');
	return false;
  } else {
    return true;
  }
}

function toggleOtherTitle() {
  var form=document.frmDonation1;
  if (form.contactTitle.selectedIndex == 5) $("#divContactTitleOther").show();
  else $("#divContactTitleOther").hide(); 
}

function checkDollarFormat(amount) {
  var dollarPattern = /[0-9]+\.[0-9]{2}$/;
  return dollarPattern.test(amount); 
}
</script>
<style type="text/css">
.error {background-color:#FFF2F9;}
</style>
<div class="contentWrapper" id="forms">
  <div class="head">Help Support Woman's Hospital Foundation</div>
  <div class="intro">
    <cfoutput>#formInfo.introductionText#</cfoutput>
  </div>
  <div class="floatLeftFullWdth">
    <div class="floatRight" style=" font-style:italic; padding-bottom:10px;">* Indicates Required Field</div>
    
    <form id="frmDonation1" name="frmDonation1" method="post" <cfoutput>action="index.cfm?md=customform&tmp=donation2&pnid=#URL.pnid#&nid=#URL.nid#&tid=#URL.tid#"</cfoutput> onSubmit="return submitDonation1(this);">
      <cfoutput><input type="hidden" name="sourceCode" value="#URL.src#" /></cfoutput>
      <div class="unit"> <span class="section">Contact Information</span>
        <div class="singleField">
          <label for="contactTitle" class="descrip">Title *</label>
          <select name="contactTitle" id="contactTitle" onchange="toggleOtherTitle()">
            <option value=""></option>
            <option value="Ms.">Ms.</option>
            <option value="Mrs.">Mrs.</option>
            <option value="Mr.">Mr.</option>
            <option value="Dr.">Dr.</option>
            <option value="">Other</option>
          </select>
        </div>
        <div id="divContactTitleOther" class="singleField">
          <label for="contactTitleOther" class="descrip">Other Title *</label>
          <input type="text" name="contactTitleOther" id="contactTitleOther" />
        </div>
        <div class="singleField">
          <label for="firstName" class="descrip">First Name *</label>
          <input type="text" name="firstName" id="firstName" />
        </div>
        <div class="singleField">
          <label for="middleName" class="descrip">Middle Name/Initial</label>
          <input name="middleName" type="text" id="middleName" size="3" />
        </div>
        <div class="singleField">
          <label for="lastName" class="descrip">Last Name *</label>
          <input type="text" name="lastName" id="lastName" />
        </div>
        <div class="singleField">
          <label for="address1" class="descrip">Address *</label>
          <input name="address1" type="text" id="address1" size="50" />
        </div>
        <div class="singleField">
          <label for="address2" class="descrip">&nbsp;</label>
          <input type="text" name="address2" id="address2" />
        </div>
        <div class="singleField">
          <label for="city" class="descrip">City *</label>
          <input type="text" name="city" id="city" />
        </div>
        <div class="singleField">
          <label for="state" class="descrip">State *</label>
          <select name="state" id="state">
            <option value=""></option>                
            <cfoutput>#UT.getOptionList("States")#</cfoutput>
          </select>
        </div>
        <div class="singleField">
          <label for="zip" class="descrip">Zip *</label>
          <input name="zip" type="text" id="zip" size="5" />
        </div>
        
        <div class="singleField">
          <label for="phonePart1" class="descrip">Phone *</label>
          <input name="phonePart1" type="text" class="phone" id="phonePart1" size="3" maxlength="3" onKeyUp="autotab(this, document.frmDonation1.phonePart2, 3);" />
          <span style=" font-size:12px; line-height:102%;">-</span>
          <input name="phonePart2" type="text" class="phone" id="phonePart2" size="3" maxlength="3" onKeyUp="autotab(this, document.frmDonation1.phonePart3, 3);" />
          <span style=" font-size:12px; line-height:102%;">-</span>
          <input name="phonePart3" type="text" class="phone" id="phonePart3" size="4" maxlength="4" />
        </div>      
          
        <div class="singleField">
          <label for="emailAddress" class="descrip">E-Mail Address *</label>
          <input type="text" name="emailAddress" id="emailAddress" />
        </div>
        <div class="singleField">
          <label for="dateOfBirth" class="descrip">Birth Date</label>
          <!--- <input type="text" name="dateOfBirth" id="dateOfBirth" /> --->
          <input name="dateOfBirthPart1" type="text" class="phone" id="dateOfBirthPart1" size="3" maxlength="2" onKeyUp="autotab(this, document.frmDonation1.dateOfBirthPart2, 2);" />
          <span style=" font-size:12px; line-height:102%;">-</span>
          <input name="dateOfBirthPart2" type="text" class="phone" id="dateOfBirthPart2" size="3" maxlength="2" onKeyUp="autotab(this, document.frmDonation1.dateOfBirthPart3, 2);" />
          <span style=" font-size:12px; line-height:102%;">-</span>
          <input name="dateOfBirthPart3" type="text" class="phone" id="dateOfBirthPart3" size="4" maxlength="4" /> (MM-DD-YYYY)
          
          
          
        </div>
        <div class="singleField">
          <label for="displayName" class="descrip">Your name as you wish it to appear for donor recognition:</label>
          <input type="text" name="displayName" id="displayName" />
        </div>
        <div class="singleField">
          <label for="preferAnonymous" class="descrip">I prefer to remain anonymous</label>
          <input type="checkbox" name="preferAnonymous" id="preferAnonymous" class="checkbox" value="Yes" />
        </div>
      </div>
      <div class="unit"> <span class="section">Donation Amount</span>
        <label class="title">I wish to donate:</label>
        <div class="singleField">
          <label for="donationAmount" class="descrip">Please selct:</label>
          <select name="donationAmount" id="donationAmount" class="donateIncrmnt">
            <option value="0.00"></option>
            <option value="25.00">$25</option>
            <option value="50.00">$50</option>
            <option value="100.00">$100</option>
            <option value="250.00">$250</option>
            <option value="500.00">$500</option>
            <option value="1000.00">$1,000</option>
            <option value="Other">Other</option>
          </select>
        </div>
        <div class="singleField">
          <label for="donationAmountOther" class="descrip">Or enter your <br />
          own amount:</label>
          <input type="text" name="donationAmountOther" id="donationAmountOther" />
        </div>
      </div>
      
      <div class="unit">
        <span class="section">Honorarium/Memorium</span>
        <div class="singleField">
          <label class="descrip">Would you like to dedicate your gift in honor or memory of someone special?</label>
          <input type="radio" name="dedicate" id="dedicateYes" value="Yes" class="radio" onClick="toggleFields(this,'divDedicate',true)" />
          <label for="dedicateYes">Yes</label>
          <input type="radio" name="dedicate" id="dedicateNo" value="No" class="radio" onClick="toggleFields(this,'divDedicate',false)" checked />
          <label for="dedicateNo">No</label>
        </div>
        <div id="divDedicate">
          <div class="singleField">
            <label for="inHonorOf" class="descrip">In Honor Of</label>
            <input type="text" name="inHonorOf" id="inHonorOf" />
          </div>
          <div class="singleField">
            <label for="inMemoryOf" class="descrip">In Memory Of</label>
            <input type="text" name="inMemoryOf" id="inMemoryOf" />
          </div>
          <div class="singleField">
            <label class="descrip">Is there someone other than you to whom you wish to notify of this gift?</label>
            <input type="radio" name="notification" id="notificationYes" value="Yes" class="radio" onClick="toggleFields(this,'divNotification',true)" />
            <label for="notificationYes">Yes</label>
            <input type="radio" name="notification" id="notificationNo" value="No" class="radio" onClick="toggleFields(this,'divNotification',false)" checked />
            <label for="notificationNo">No</label>
          </div>
          <div id="divNotification">
            <label class="title">Individual to notify:</label>
            <div class="singleField">
              <label for="notifyTitle" class="descrip">Title</label>
              <select name="notifyTitle" id="notifyTitle">
                <option></option>
                <option value="Ms.">Ms.</option>
                <option value="Mrs.">Mrs.</option>
                <option value="Mr.">Mr.</option>
                <option value="Dr.">Dr.</option>
                <option value="">Other</option>
          	  </select>
            </div>
            <div class="singleField">
              <label for="notifyTitleOther" class="descrip">other title:</label>
              <input type="text" name="notifyTitleOther" id="notifyTitleOther" />
            </div>
            <div class="singleField">
              <label for="notifyFirstName" class="descrip">First Name</label>
              <input type="text" name="notifyFirstName" id="notifyFirstName" />
            </div>
            <div class="singleField">
              <label for="notifyMiddleInitial" class="descrip">Middle Name/Initial</label>
              <input name="notifyMiddleInitial" type="text" id="notifyMiddleInitial" size="3" />
            </div>
            <div class="singleField">
              <label for="notifyLastName" class="descrip">Last Name</label>
              <input type="text" name="notifyLastName" id="notifyLastName" />
            </div>
            <div class="singleField">
              <label for="notifyAddress1" class="descrip">Address</label>
              <input name="notifyAddress1" type="text" id="notifyAddress1" size="50" />
            </div>
            <div class="singleField">
              <label for="notifyAddress2" class="descrip">&nbsp;</label>
              <input type="text" name="notifyAddress2" id="notifyAddress2" />
            </div>
            <div class="singleField">
              <label for="notifyCity" class="descrip">City</label>
              <input type="text" name="notifyCity" id="notifyCity" />
            </div>
            <div class="singleField">
              <label for="notifyState" class="descrip">State</label>
              <select name="notifyState" id="notifyState">
                <option value=""></option>                
                <cfoutput>#UT.getOptionList("States")#</cfoutput>
              </select>
            </div>
            <div class="singleField">
              <label for="notifyZip" class="descrip">Zip</label>
              <input name="notifyZip" type="text" id="notifyZip" size="5" />
            </div>
            <div class="singleField">
              <label for="notifyPhonePart1" class="descrip">Phone</label>
              <input name="notifyPhonePart1" type="text" class="phone" id="notifyPhonePart1" size="3" maxlength="3" onKeyUp="autotab(this, document.frmDonation1.notifyPhonePart2, 3);" />
              <span style=" font-size:12px; line-height:102%;">-</span>
              <input name="notifyPhonePart2" type="text" class="phone" id="notifyPhonePart2" size="3" maxlength="3" onKeyUp="autotab(this, document.frmDonation1.notifyPhonePart3, 3);" />
              <span style=" font-size:12px; line-height:102%;">-</span>
              <input name="notifyPhonePart3" type="text" class="phone" id="notifyPhonePart3" size="4" maxlength="4" />
            </div>
            <div class="singleField">
              <label for="donatedBy" class="descrip">Donation given by: (if different than previously entered above)</label>
              <input type="text" name="donatedBy" id="donatedBy" />
            </div>
          </div>
        </div>
      </div>
      <!--- class=unit --->
      <div class="unit">
        <span class="section">Matching Gift</span>
        <div class="singleField">
          <label class="descrip">Do you work for or are retired from a company that matches gifts?</label>
          <input type="radio" name="matchGifts" id="matchGiftsYes" value="Yes" class="radio" onClick="toggleFields(this,'divMatchGifts',true)" />
          <label for="matchGiftsYes">Yes</label>
          <input type="radio" name="matchGifts" id="matchGiftsNo" value="No" class="radio" onClick="toggleFields(this,'divMatchGifts',false)" checked />
          <label for="matchGiftsNo">No</label>
        </div>
        <div id="divMatchGifts">
          <div class="singleField">
            <label for="matchCompany" class="descrip">If Yes, What is the company’s name?</label>
            <input type="text" name="matchCompany" id="matchCompany" />
          </div>
        </div>
      </div>
      <div class="unit"><span class="section">Please Tell Us Your Story</span>
        <div class="singleField">
          <label for="donorStory" class="descrip">&nbsp;</label>
          <textarea name="donorStory" id="donorStory" rows="5"></textarea>
        </div>
      </div>
      <div class="unit">
        <input type="image" src="images/btnFormContinue.gif" alt="Continue" name="btnFormContinue" style="width:82px;height:17px;border:0px solid #CCC;" border="0" id="btnFormContinue" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnFormContinue','','images/btnFormContinueo.gif',1)" />
      </div>
    </form>
  </div>
</div>
