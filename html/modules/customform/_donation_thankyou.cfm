<cfprocessingdirective pageencoding="utf-8">
<cfparam name="URL.sid"><!--- submissionID --->
<cfset CF=CreateObject("component", "com.CustomForm").init()>
<cfset formInfo=CF.getFormInfo(Application.customform.customFormID_Donation)>

<div id="topperHTMLCodes" style="display:none;">Giving Opportunities</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtGivingOpp.jpg" width="202" height="244" /></div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
});
</script>
<div class="contentWrapper">
  <div class="head">Help Support Woman's Hospital Foundation</div>
  
  <p>&nbsp;</p>
  
  <div class="floatLeftFullWdth">
  <cfoutput>#Replace(formInfo.thankyouMessage, Chr(13) & Chr(10), "<br />", "All")#</cfoutput>
  </div>
</div>
