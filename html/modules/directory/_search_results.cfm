<cfprocessingdirective pageencoding="utf-8">

<cfparam name="URL.keyword" default="">
<cfparam name="URL.startIndex" default="1">
<cfparam name="URL.numItemsPerPage" default="20">
<cfsilent>
<cfscript>
  UT=CreateObject("component","com.Utility").init();
  SS=CreateObject("component","com.SiteSearch").init();
  structResults=SS.directorySearch(keyword=URL.keyword, startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage); 
  if (structResults.numAllItems Is 1) {
    UT.location("index.cfm?md=directory&tmp=detail&itemID=#structResults.id[1]#");
  } 
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none">Locations &amp; Maps</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="assets/images/template-photos/pht_locations_maps-1.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_search_panel.cfm">
</div>

<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
	if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
	
	var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
    $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
    $("#thmemImage").after(searchPanelHTMLCodes); 
  });
</script>

<!--DISPLAY TOOLS-->
<cfif structResults.numAllItems GT URL.numItemsPerPage>
  <cfset navPanel=UT.displayNextPreviousLinks(pageURL="index.cfm?md=directory&amp;tmp=search_results&amp;keyword=#URLEncodedFormat(URL.keyword)#", startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage, numDisplayedItems=structResults.numDisplayedItems, numAllItems=structResults.numAllItems, itemName="Results")>
  <cfoutput>#navPanel#</cfoutput>
</cfif>
  
<div class="contentWrapper" id="classCategory">

  <div class="introResults"><span class="label">Results For </span><br /><br />
  <span class="accent02Lrg">&quot;<cfoutput>#URL.keyword#</cfoutput>&quot;</span></div>
  
  <br />
  
  <!--RESULTS-->    
  <cfif structResults.numAllItems GT 0>
  <ul>
    <cfoutput>
    <cfloop index="idx" from="1" to="#ArrayLen(structResults.id)#">
    <li><a href="index.cfm?md=directory&tmp=detail&itemID=#structResults.id[idx]#" class="className">#structResults.pageName[idx]#</a></li>
    </cfloop>
    </cfoutput>
  </ul>
  <cfelse>
    <br style="clear:both;"><br />
    <div class="accent03" align="center">Your search returns no results.</div>
  </cfif>
</div>

<!--DISPLAY TOOLS-->
<cfif structResults.numAllItems GT URL.numItemsPerPage>
  <cfoutput>#navPanel#</cfoutput>
</cfif>

<cfinclude template="_search_panel_scripts_include.cfm">