<cfparam name="URL.itemID">
<cfsilent>
<cfscript>
  DR=CreateObject("component","com.Directory").init();
  itemInfo=DR.getItem(URL.itemID, "O");
</cfscript>
</cfsilent>
<cfif itemInfo.recordcount Is 0>
  <cflocation url="/404error.cfm">
</cfif>

<div id="topperHTMLCodes" style="display:none">Locations &amp; Maps</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="assets/images/template-photos/pht_locations_maps-1.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_search_panel.cfm">
</div>
<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
	if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
	
	var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
    $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
    $("#thmemImage").after(searchPanelHTMLCodes);
  });
</script>
<cfhtmlhead text='<link rel="stylesheet" type="text/css" href="modules/form/form.css" />'>
<script type="text/javascript">
<!--
  function submitForm(form) {
    if (form.firstName.value == "") {
	  alert("Please enter your first name.");
	  form.firstName.focus();
	  return false;
	}
	
	if (form.lastName.value == "") {
	  alert("Please enter your last name.");
	  form.lastName.focus();
	  return false;
	}
	
	if (!checkEmail(form.email.value)) {
	  alert("Please enter a valid email address.");
	  form.email.focus();
	  return false;
	}
	
	if (form.comments.value == "") {
	  alert("Please enter your questions or comments.");
	  form.comments.focus();
	  return false;
	}

    return true;
  }
//-->
</script>

<div class="contentWrapper" id="forms">
  <div id="formLayout">
    <div class="head">Contact Our Department</div>
    <br style="clear:both;" /><br />
    <div class="subhead"><cfoutput>#itemInfo.orgName#</cfoutput></div>
    <br style="clear:both;" /><br />
    
    <form name="frmContactDepartment" action="action.cfm?md=directory&task=contactDepartment" method="post" onsubmit="return submitForm(this);">
    <cfoutput><input type="hidden" name="itemID" value="#URL.itemID#"></cfoutput>
    <div class="row">
      <div class="col-first-right">
        <label for="label19">Your Name</label>
      </div>
      <div class="col-second-left">
        <div class="col-name">
          <input type="text" name="firstName" class="Small" value="" />
          <br />
          <small>(First Name)</small></div>
        <div class="col-name">
          <input type="text" name="lastName" class="Small" value="" />
          <br />
          <small>(Last Name)</small></div>
      </div>
    </div>
    <div class="row validate">
      <div class="col-first-right">
        E-mail Address
      </div>
      <div class="col-second-left">
        <input type="text" name="email" class="Medium" value="" />
      </div>
    </div>
    <div class="row">
      <div class="col-first-right">
        Questions / Comments&nbsp;
      </div>
      <div class="col-second-left">
        <textarea name="comments" class="Medium" style="height:150px;"></textarea>
      </div>
    </div>
    <div class="row">
      <div class="col-whole-left">
        <input type="image" src="images/btnFormSubmit.gif" alt="Submit" name="btnFormSubmit" id="btnFormSubmit" style="width:63px;height:17px; border-width:0px;" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormSubmit','','images/btnFormSubmito.gif',1)" />
      </div>
    </div>
    </form>
  </div>
</div>
<cfinclude template="_search_panel_scripts_include.cfm">