<cfparam name="URL.itemID">

<cfsilent>
<cfscript>
  DR=CreateObject("component","com.Directory").init();
  if (FindNoCase("/admin",CGI.HTTP_REFERER) GT 0) {
    itemInfo=DR.getItem(itemID=URL.itemID, itemType="O", publishedOnly=0);
  } else {
    itemInfo=DR.getItem(URL.itemID, "O");
  }
  relatedItems=DR.getRelatedItems(URL.itemID,"I");
</cfscript>
</cfsilent>

<cfif itemInfo.recordcount Is 0><cflocation url="/404error.cfm"></cfif>

<div id="topperHTMLCodes" style="display:none">Locations &amp; Maps</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="assets/images/template-photos/pht_locations_maps-1.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_search_panel.cfm">
</div>

<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
	if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
	
	var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
    $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
    $("#thmemImage").after(searchPanelHTMLCodes);  	
	
	$("#btnInfo").mouseover(function() {
	  var img = $("#btnInfo").attr("src");
	  var newImg = img.replace("o.gif","oo.gif");
	  $("#btnInfo").attr("src",newImg);
	});
	$("#btnInfo").mouseout(function() {
	  var img = $("#btnInfo").attr("src");
	  var newImg = img.replace("oo.gif","o.gif");
	  $("#btnInfo").attr("src",newImg);
	});
  });
  
  function toggleInfo() {
    var infoObj=document.getElementById("moreInfo");
	if (infoObj.style.display == "block") {
	  infoObj.style.display="none";
	  $("#btnInfo").attr("src", "images/btnBarMoreInfo.gif");
	} else {
	  infoObj.style.display="block";
	  $("#btnInfo").attr("src", "images/btnBarLessInfo.gif");
	}
  }
</script>

<div class="contentWrapper" id="locDirect">
  <div class="subSectHd"><a href="index.cfm?md=directory&amp;tmp=home" class="slctOther"> Select Another Department</a> <cfoutput query="itemInfo">#orgName#</cfoutput></div>
  <!--LOCATION DETAILS-->
  <div class="twoColWrapper">
    <cfoutput query="itemInfo">
      <cfif Compare(orgLogoFile,"")>
      <div class="detailLft" align="left"><img src="#orgLogoFile#" width="148" height="148" class="phtStroke" /></div>
      <div class="detailRt">
        #orgDescription#
      </div>
      <cfelse>
      <div class="detailRt" style="margin-left:0px;">
        #orgDescription#
      </div>
      </cfif>      
    </cfoutput>
  </div>
  <div class="moreInfo"><a href="javascript:toggleInfo()"><img src="images/btnBarLessInfo.gif" width="460" height="18" border="0" id="btnInfo" /></a></div>
  
  <div id="moreInfo" style="display:block;">
      <cfoutput query="itemInfo">
      <div class="twoColWrapper">
        <cfif Compare(orgPhone,"") OR Compare(orgFax,"")>
        <div class="detailLft" align="left">
          <!--contact info-->
          <span class="titleBarSm">Contact Information</span>
          <p class="content" id="cntctInfo">
            <cfif Compare(orgPhone,"")>
            <span class="label">PH: </span>#orgPhone#<br />
            </cfif>
            <cfif Compare(orgFax,"")>
            <span class="label">FX:</span> #orgFax#<br />
            </cfif>
            
            <a href="index.cfm?md=directory&amp;tmp=contactform&amp;itemID=#URL.itemID#" class="lnkAccent01">Contact &gt;</a></p>
        </div>
        </cfif>
        <cfif Compare(orgAddressPhysical,"") OR Compare(orgCityPhysical,"") OR Compare(orgStatePhysical,"") OR Compare(orgZIPPhysical,"")>
        <div class="detailRt">          
          <!--address-->
          <span class="titleBarSm">Address </span>
          <p class="content">
            <cfif Compare(orgAddressPhysical,"")>
            #orgAddressPhysical#<br />
            </cfif>
            <cfif Compare(orgCityPhysical,"")>
            #orgCityPhysical#<cfif Compare(orgStatePhysical,"")>, #orgStatePhysical#  #orgZIPPhysical#</cfif><br />            
            </cfif>
            <cfif mapID>            
            <a href="javascript:openWindow('index.cfm?md=directory&amp;tmp=location_map&amp;itemID=#URL.itemID#&amp;popup=1','map',900,600)" class="lnkAccent01">Find Us &gt;</a></p>
            </cfif>
        </div>
        </cfif>
      </div>      
      </cfoutput>
      
      <cfif relatedItems.recordcount GT 0>      

      <div>
        <cfoutput query="relatedItems">
        <p class="empInfo">
          <span class="empName">
            #indivPrefix# #indivFirstName# #indivMiddleName# #indivLastName#<cfif Compare(indivSuffix,"")>, #indivSuffix#</cfif>           
            <cfif Compare(indivJobTitle,"") OR Compare(indivDivision,"")>
            <span class="empTitle">
              <cfif Compare(indivJobTitle,"")>#indivJobTitle#</cfif><cfif Compare(indivDivision,"")><cfif Compare(indivJobTitle,"")> &mdash;</cfif> #indivDivision#</cfif></span>
            </cfif>
          </span>
          
          <!--- <div class="detailLft" align="left"><img src="images/imgSpacer.gif" width="148" height="148" class="phtStroke" /></div> --->
          
          <cfif Compare(indivPhone,"")>
          <span class="label">PH:</span> #indivPhone#<br />
          </cfif>
          <cfif Compare(indivFax,"")>
          <span class="label">FX:</span> 225.201.2001<br />
          </cfif>
          <cfif Compare(indivEmail,"")>
          <span class="label">EMAIL:</span> <a href="mailto:#indivEmail#">#indivEmail#</a><br />
          </cfif>
          <cfif Compare(indivAddress,"")>
          <span class="labelNotInline">ADDRESS:</span>
          #indivAddress#<br />
          <cfif Compare(indivCity,"")>
          #indivCity#<cfif Compare(indivState,"")>, #indivState#</cfif> #indivZIP#
          </cfif>
          </cfif>
          </p>          
        </cfoutput>
      </div>
      </cfif> 
  </div>
</div>

<cfinclude template="_search_panel_scripts_include.cfm">