<script type="text/javascript" src="jsapis/jquery/js/jquery.tools.min.js"></script>
<script language="JavaScript" src="jsapis/jquery/js/jquery.jsonSuggest.js"></script>
<script language="JavaScript" src="jsapis/jquery/js/json2.js"></script>
<script language="JavaScript" src="index.cfm?md=directory&tmp=department_search_hint_words_json_js&nowrap=1"></script>
<style type="text/css">
div.jsonSuggestResults {
	position:absolute;
	border:1px solid #CCC;
	padding:0px;
	margin:0px 2px;
	z-index:1;
}
div.jsonSuggestResults div.resultItem {
	margin:0px;
	padding:5px;
	position:relative;
	height:auto;
	cursor:pointer;
}
div.jsonSuggestResults div.resultItem.odd {
	background-color:#FFEAFF;
}
div.jsonSuggestResults div.resultItem.even {
	background-color:#FFFFFF;
}
div.jsonSuggestResults div.resultItem.hover {
	background-color:#FFBFFF;
}
div.jsonSuggestResults div.resultItem img {
	float:left;
	margin-right:10px;
}
div.jsonSuggestResults div.resultItem p {
	margin:0px;
	padding:0px;
}
div.jsonSuggestResults div.resultItem p strong {
	font-weight:bold;
	text-decoration:underline;
}
div.jsonSuggestResults div.resultItem p.extra {
	font-size: x-small !important;
	position:absolute;
	bottom:3px;
	right: 3px;
}
</style>
<script type="text/javascript">
<!--
  $(document).ready(function(){
	$('input#deptName').jsonSuggest(hintWords.ORGNAME);
  });
  
  function submitDirectorySearch() {
	var form=document.frmDirectorySearch;	    
	if (form.keyword.value.length < 2) {
	  alert('Please enter a keyword with at least 2 characters.');
	  form.keyword.focus();
	  return;
	}
	form.submit();
  }
//-->
</script>