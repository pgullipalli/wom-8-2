<cfsilent>
<cfscript>
  DR=CreateObject("component","com.Directory").init();
  directorySummary=DR.getDirectorySummary();
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none">Locations &amp; Maps</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="assets/images/template-photos/pht_locations_maps-1.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_search_panel.cfm">
</div>

<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
	if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
	
	var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
    $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
    $("#thmemImage").after(searchPanelHTMLCodes);  	
  });
</script>

<div class="contentWrapper" id="classCategory">
  <div class="floatLeftFullWdth">
    <b>JUMP TO:</b><br />
    <cfset catIDTemp=0>
    <cfoutput query="directorySummary">
      <cfif Compare(categoryID,catIDTemp)>
        <a href="##c#categoryID#" class="lnkAccent01">#categoryName#</a><br />  	 
      </cfif>
      <cfset catIDTemp=categoryID>
    </cfoutput>
    <br /><br />
  </div>

  <cfset catIDTemp=0>
  <cfoutput query="directorySummary">
    <cfif Compare(categoryID,catIDTemp)>
      <cfif Compare(catIDTemp,0)></ul></cfif>
  	  <div class="floatLeftFullWdth"><a name="c#categoryID#"></a><span class="titleBarSm">#categoryName#</span></div>
        <ul>
    </cfif>
  	<li style="line-height:155%;">
      <a href="index.cfm?md=directory&tmp=detail&itemID=#itemID#" class="className">#orgName#</a>
      <cfset address="#orgAddressPhysical#, #orgCityPhysical#, #orgStatePhysical# #orgZIPPhysical#"><cfset address=Trim(Replace(address,", ,","","All"))><cfset hasContent=0>
      <cfif Compare(address,"")>#address#<cfset hasContent=1></cfif>
      <cfif Compare(orgPhone,"")><cfif hasContent>|</cfif> Ph. #orgPhone#<cfset hasContent=1></cfif>
      <cfif Compare(orgFax,"")><cfif hasContent>|</cfif> Fax. #orgFax#<cfset hasContent=1></cfif>
      <cfif Compare(orgWebAddress,"")>
        <cfif FindNoCase("http", orgWebAddress) Is 0><cfset webURL="http://#orgWebAddress#"><cfelse><cfset webURL="#orgWebAddress#"></cfif>
        <cfif Find("index.cfm",orgWebAddress) Is 0><cfset popup=1><cfelse><cfset popup=0></cfif>        
        <cfif hasContent>|</cfif> <a href="#webURL#"<cfif popup> target="_blank"</cfif>>Website</a>
      </cfif>
    </li>
  	<cfset catIDTemp=categoryID>
  </cfoutput>
</div>

<cfinclude template="_search_panel_scripts_include.cfm">