<cfsilent>
<cfscript>
  DR=CreateObject("component","com.Directory").init();
  allMaps=DR.getAllFeaturedMaps();
</cfscript>
</cfsilent>
<div class="gryWrapper" id="locDirectSrch">
  <div class="intro">
    <p><span class="title">Department Search</span>
    Enter the name of the department you're looking for below or <a href="index.cfm?md=directory&amp;tmp=categoryindex">browse through our locations</a>...</p>
    <form id="frmDirectorySearch" name="frmDirectorySearch" method="get" action="index.cfm" onsubmit="return false;">
      <div style="display:none;"><input type="hidden" name="md" value="directory" /><input type="hidden" name="tmp" value="search_results" /></div>
      <input name="keyword" type="text" id="deptName" value="" size="30" />
      <div class="submit" align="left"><a href="javascript:submitDirectorySearch()" class="btnMed">Search</a></div>
    </form>
    <ul>
      <cfoutput query="allMaps">
      <li><a href="javascript:openWindow('index.cfm?md=directory&tmp=map&mapID=#mapID#&popup=1','map',900,600)">#mapName#</a></li>
      </cfoutput>
    </ul>
  </div>
</div>