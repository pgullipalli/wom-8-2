<div id="topperHTMLCodes" style="display:none">Locations &amp; Maps</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="assets/images/template-photos/pht_locations_maps-1.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_search_panel.cfm">
</div>
<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
	if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
	
	var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
    $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
    $("#thmemImage").after(searchPanelHTMLCodes);  	
  });
</script>
        <div class="contentWrapper" id="locMaps"> 
          <div class="intro"> 
          <p class="title">Where are we?</p> 
          <p>Receiving care at Woman's is always easy and convenient. The majority of services provided by Woman's Hospital are available on the Main Campus, including Physician Tower I, at 9050 Airline Highway. Additional services, including our Fitness Club, Day Spa and Therapy services, are located in the Woman's Center for Wellness at Jefferson and Bluebonnet. Our mobile mammography coach can travel to churches, schools,  or community centers. Please search for a department for specific information on a department.</p></div> 
          <div class="floatLeftFullWdth"><span class="accent02Lrg">Woman's Hospital Main Campus</span> <br /> 
          9050 Airline Highway <br /> 
          Baton Rouge, LA 70815<br /> 
          <i>(Airline at Goodwood Boulevard) </i><br /> 
<br /> 
<span class="accent01"><b>Main Number: </b></span>225.927.1300 <br /> 
<span class="accent01"><b>Patient Rooms:</b></span> 225.231.5 [Plus room number]<br /> 
<span class="accent01"><b>Patient room Info:</b></span> 225.924.8157<br /> 
<iframe width="452" height="265" style="border:1px solid #d0d8d6; margin-top:14px;" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?q=woman's+hospital,&amp;hl=en&amp;cd=2&amp;ei=MffqS4yMEoa2yQTxgK3nDQ&amp;sig2=LCoOeQgNATfAehX4XWDflw&amp;sll=30.45093,-91.077733&amp;sspn=0.03596,0.056391&amp;ie=UTF8&amp;view=map&amp;cid=10264370628604018840&amp;ved=0CBkQpQY&amp;hq=woman's+hospital,&amp;hnear=&amp;ll=30.44527,-91.091552&amp;spn=0.004902,0.009677&amp;z=16&amp;iwloc=A&amp;output=embed"></iframe><br /> 
<small><a href="http://maps.google.com/maps?q=woman's+hospital,&amp;hl=en&amp;cd=2&amp;ei=MffqS4yMEoa2yQTxgK3nDQ&amp;sig2=LCoOeQgNATfAehX4XWDflw&amp;sll=30.45093,-91.077733&amp;sspn=0.03596,0.056391&amp;ie=UTF8&amp;view=map&amp;cid=10264370628604018840&amp;ved=0CBkQpQY&amp;hq=woman's+hospital,&amp;hnear=&amp;ll=30.44527,-91.091552&amp;spn=0.004902,0.009677&amp;z=16&amp;iwloc=A&amp;source=embed" class="lnkAccent01SmCaps">View Larger Map �</a></small> 
<div class="horizDivider"></div> 
<div class="floatLeftFullWdth"><span class="accent02Lrg">Woman�s Center for Wellness</span> <br /> 
          9637 Jefferson Highway<br /> 
          Baton Rouge, LA 70809<br /> 
          <i>(Jefferson Highway at Bluebonnet Boulevard)</i><br /> 
<br /> 
<span class="accent01"><b>Main Number: </b></span>225.924.8300 <br /> 
<span class="accent01"><b>Hours: </b></span>Monday - Friday 9:00 AM-5:00 PM<br /> 
<iframe width="452" height="265" style="border:1px solid #d0d8d6; margin-top:14px;" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Woman%E2%80%99s+Center+for+Wellness+9637+Jefferson+Highway+Baton+Rouge,+LA+70809&amp;sll=30.411948,-91.077894&amp;sspn=0.008994,0.014098&amp;ie=UTF8&amp;hq=Woman%E2%80%99s+Center+for+Wellness&amp;hnear=9637+Jefferson+Hwy,+Baton+Rouge,+LA+70809&amp;ll=30.411892,-91.077905&amp;spn=0.004904,0.009677&amp;z=16&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Woman%E2%80%99s+Center+for+Wellness+9637+Jefferson+Highway+Baton+Rouge,+LA+70809&amp;sll=30.411948,-91.077894&amp;sspn=0.008994,0.014098&amp;ie=UTF8&amp;hq=Woman%E2%80%99s+Center+for+Wellness&amp;hnear=9637+Jefferson+Hwy,+Baton+Rouge,+LA+70809&amp;ll=30.411892,-91.077905&amp;spn=0.004904,0.009677&amp;z=16&amp;iwloc=A" class="lnkAccent01SmCaps">View Larger Map �</a></small></div> 
        </div> 
      </div> 
<cfinclude template="_search_panel_scripts_include.cfm">
