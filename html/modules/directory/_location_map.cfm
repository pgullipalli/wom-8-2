<cfparam name="URL.itemID" default="0"><!--- location ID --->
<cfsilent>
<cfscript>
  DR=CreateObject("component","com.Directory").init();
  itemInfo=DR.getItem(itemID=URL.itemID, itemType="O", publishedOnly=0);
  if (itemInfo.recordcount GT 0) mapInfo=DR.getMapByID(itemInfo.mapID);
</cfscript>
</cfsilent>

<script type="text/javascript" src="jsapis/jquery/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
$("document").ready(function() {  
  showPin("map", "pin");
});
	
function showPin(mapID, pinID) {
  var pinPosX=$("#" + pinID).attr("posX");
  var pinPosY=$("#" + pinID).attr("posY");
  if (pinPosX=="0" && pinPosY=="0") return; //just show the map, no pin

  var p=$("#" + mapID).offset();
  var mapX=Math.ceil(p.left);
  var mapY=Math.ceil(p.top);
  var pinX=mapX + parseInt(pinPosX) - Math.ceil(parseInt($("#" + pinID).attr("width"))/2);
  var pinY=mapY + parseInt(pinPosY) - parseInt($("#" + pinID).attr("height"));
  $("#" + pinID).hide();
  $("#" + pinID).css({'position' : 'absolute', 'left' : pinX + 'px', 'top' : pinY + 'px', "display" : "inline"});
}

function openprintdialog () {
  var onWindows = navigator.platform ? navigator.platform == "Win32" : false;
  var macIE = document.all && !onWindows;
  if (macIE) {
	alert ("Press \"Cmd+p\" on your keyboard to print");
  } else {
	window.print();
  }  
}			
</script>


<div class="head"><cfoutput>#itemInfo.orgName#</cfoutput></div>

<br /><br />

<cfif itemInfo.recordcount GT 0 And itemInfo.mapID GT 0>
<cfoutput>
<div style="width:100%;display:block;">
  <img id="pin" src="images/imgPushpin.png" width="34" height="30" posX="#itemInfo.posX#" posY="#itemInfo.posY#" style="display:none;" />
  <img id="map" src="/#mapInfo.mapFile#" border="0" />
</div>
</cfoutput>
</cfif>