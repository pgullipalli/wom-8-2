<cfparam name="URL.itemID">
<cfsilent>
<cfscript>
  DR=CreateObject("component","com.Directory").init();
  itemInfo=DR.getItem(URL.itemID, "O");
</cfscript>
</cfsilent>
<cfif itemInfo.recordcount Is 0>
  <cflocation url="/404error.cfm">
</cfif>

<div id="topperHTMLCodes" style="display:none">Locations &amp; Maps</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="assets/images/template-photos/pht_locations_maps-1.jpg" width="202" height="244" /></div>
<div id="searchPanelHTMLCodes" style="display:none">
  <cfinclude template="_search_panel.cfm">
</div>
<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
	if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
	
	var searchPanelHTMLCodes=$("#searchPanelHTMLCodes").html();
    $("#searchPanelHTMLCodes").html('');//empty the HTML codes to prevent duplicated element IDs
    $("#thmemImage").after(searchPanelHTMLCodes);
  });
</script>

<div class="contentWrapper" id="forms">
  <div id="formLayout">
    <div class="head">Contact Our Department</div>
    <br style="clear:both;" /><br />
    <div class="subhead"><cfoutput>#itemInfo.orgName#</cfoutput></div>
    <br style="clear:both;" /><br /><br /><br />
    
    <p style="font-size:11px;">Thank you for your contacting our department!</p>
  </div>
</div>

<cfinclude template="_search_panel_scripts_include.cfm">