<cfparam name="URL.catid">
<cfsilent>
<cfscript>
  NR=CreateObject("component","com.Newsroom").init();
  categoryInfo=NR.getArticleCategoryByCategoryID(URL.catid);
  numDisplayedArticles=7;
  structArticles=NR.getArticlesByCategoryID(articleCategoryID=URL.catid, startIndex=1, numItemsPerPage=numDisplayedArticles);  
</cfscript>
</cfsilent>
<cfif categoryInfo.recordcount Is 0><cflocation url="/404error.cfm"></cfif>

<div id="topperHTMLCodes" style="display:none"><cfoutput>#categoryInfo.articleCategory[1]#</cfoutput></div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_info_library.jpg" width="202" height="244" /></div>
<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "")  $("#titleBar").html($("#topperHTMLCodes").html());
	if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  });
</script>


<div class="contentWrapper" id="infoLibrary">
  <cfif categoryInfo.RSSEnabled>
  <div class="rss"><cfoutput><a href="index.cfm?md=newsroom&tmp=rss&nowrap=1&catid=#URL.catid#"><img src="images/imgRssIcon.jpg" alt="RSS" width="36" height="43" border="0" /></a></cfoutput></div>
  </cfif>
  
  <cfoutput query="structArticles.articles" maxrows="1">
  <div class="newsIndexPrimary">
    <cfif categoryInfo.displayArticleDate>
    <span class="accent03"><b>#DateFormat(articleDate, "mmmm dd, yyyy")#</b></span>
    </cfif>
    <a href="#href#" <cfif popup>target="_blank"</cfif> class="lnkAccent01Lrg">#headline#</a>
    <cfif Compare(featureThumbnail,"")>
    <img src="#featureThumbnail#" width="148" class="phtThmbLft" />
    </cfif>
    #teaser#
  </div>
  </cfoutput>
  
  <cfset numRows=Ceiling((structArticles.articles.recordcount - 1)/2)>
  <cfoutput>
  <cfloop index="row" from="1" to="#numRows#">
  <div class="twoColWrapper">
    <cfset idx=(row - 1) * 2 + 2>
    <cfif idx LTE structArticles.articles.recordcount>
      <div class="halfLeft">
        <cfif categoryInfo.displayArticleDate>
        <span class="accent03"><b>#DateFormat(structArticles.articles.articleDate[idx],"mmmm dd, yyyy")#</b></span>
        </cfif>
        <a href="#structArticles.articles.href[idx]#" <cfif structArticles.articles.popup[idx]>target="_blank"</cfif> class="lnkAccent01Lrg">#structArticles.articles.headline[idx]#</a>
        #structArticles.articles.teaser[idx]#
      </div>
    </cfif>
    
    <cfset idx=idx + 1>
    <cfif idx LTE structArticles.articles.recordcount>
      <div class="halfRight">
        <cfif categoryInfo.displayArticleDate>
        <span class="accent03"><b>#DateFormat(structArticles.articles.articleDate[idx],"mmmm dd, yyyy")#</b></span>
        </cfif>
        <a href="#structArticles.articles.href[idx]#" <cfif structArticles.articles.popup[idx]>target="_blank"</cfif> class="lnkAccent01Lrg">#structArticles.articles.headline[idx]#</a>
        #structArticles.articles.teaser[idx]#
      </div>
    </cfif>
  </div>
  </cfloop>
  </cfoutput>

  <cfif structArticles.numAllItems GT numDisplayedArticles>
    <cfoutput>
    <div class="twoColWrapper"><b><a href="index.cfm?md=newsroom&tmp=archives&catid=#URL.catid#" class="lnkArchives">ARCHIVES &raquo;</a></b></div>
    </cfoutput>
  </cfif>
</div>