<cfparam name="URL.articleID">
<cfsilent>
<cfscript>
  NR=CreateObject("component","com.Newsroom").init();
  categoryInfo=NR.getArticleCategoryByArticleID(URL.articleID);
  if (Find("/admin/",CGI.HTTP_REFERER) GT 0) {
    articleInfo=NR.getArticleByID(articleID=URL.articleID, publishedOnly=0);
  } else {
    articleInfo=NR.getArticleByID(URL.articleID);
  }
  if (categoryInfo.displayArticleDate And articleInfo.displayArticleDate) {
    displayDate=true;
  } else {
    displayDate=false;
  }
  if (articleInfo.contentType Is 1 OR articleInfo.contentType Is 4) {//full story or media
	  relatedItems=NR.getRelatedItems(URL.articleID);
  }
</cfscript>
</cfsilent>

<cfif articleInfo.recordcount Is 0><cflocation url="/404error.cfm"></cfif>
<cfif articleInfo.contentType Is 2 OR articleInfo.contentType Is 3>
  <!--- doc file or external URL --->
  <cflocation url="#articleInfo.href#">
</cfif>

<!--- for embedded Flash video/MP3 player just in case --->
<script src="jsapis/flowplayer/flowplayer-3.1.4.min.js"></script>

<div id="topperHTMLCodes" style="display:none"><cfoutput>#categoryInfo.articleCategory[1]#</cfoutput></div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_info_library.jpg" width="202" height="244" /></div>
<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
	if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  });
</script>

<div class="contentWrapper">
<cfoutput query="articleInfo">
  <p><span class="head">#headline#<cfif Compare(subHeadline,"")><span class="subHead">#subHeadline#</span></cfif></span></p>
  
  <cfif displayDate OR Compare(byline,"")>
  <p class="date">
    <cfif Compare(byline,"")>By #byline#<br /></cfif>
    <cfif displayDate>#DateFormat(articleDate,"mmmm dd, yyyy")#</cfif>
  </p>
  </cfif>
  <cfif Compare(featureImage,"")>
  <div class="phtRight" align="center">
    <img src="#featureImage#" width="200" />
	<cfif Compare(featureImageCaption,"")><div class="phtCaptionBox">#featureImageCaption#</div></cfif>
  </div>
  </cfif>
  <cfif Compare(contentType, 4)>
	<cfif Compare(dateline,"")>
      <cfset fullStoryNew=Trim(fullStory)>
      <cfif FindNoCase("<p>",fullStoryNew) IS 1>
        <cfset fullStoryNew=ReplaceNoCase(fullStoryNew,"<p>","<p><b>#dateline# -</b>")>
        #fullStoryNew#
      <cfelseif FindNoCase("<p class=""body-text"">",fullStoryNew) IS 1>
        <cfset fullStoryNew=ReplaceNoCase(fullStoryNew,"<p class=""body-text"">","<p><b>#dateline# -</b>")>
        #fullStoryNew#
      <cfelse>
        <b>#dateline# -</b>
        #fullStoryNew#
      </cfif>
    <cfelse>
      #fullStory#
    </cfif> 
  <cfelse>
  	<!--- this is a audio/video; embedded video player here... --->
    <br /><br />
    <cfif Compare(mediaFile,"")><cfset mediaURL=mediaFile><cfelse><cfset mediaURL=mediaFileURL></cfif>
    <cfif Not Compare(mediaFileType, "V")><!--- Video --->
	  <script type="text/javascript">
      $(document).ready(function() {  
        var mediaURL=$("##embeddedPlayer").attr('href');
        flowplayer("embeddedPlayer", "jsapis/flowplayer/flowplayer-3.1.4.swf", { 
          clip: { 
            url: mediaURL, 
            autoPlay: false, 
            autoBuffering: true 
          }
        });
      });		
      </script>
      <table border="0" width="350" align="center">
        <tr>
          <td>
          <div id="embeddedPlayer" href="#mediaURL#" style="float:left;display:block;width:320px;height:240px;"></div>
          </td>
        </tr>
      </table>
    <cfelse>
	  <!--- Audio --->
	  <script type="text/javascript">
      $(document).ready(function() {  
        flowplayer("embeddedPlayer", "jsapis/flowplayer/flowplayer-3.1.4.swf", { 
          plugins: { 
            controls: {fullscreen: false, height: 30} 
          }, 
          clip: {autoPlay: false} 							 
         });                  
      });
      </script>
      <table border="0" width="350" align="center">
        <tr>
          <td>
          <div id="embeddedPlayer" style="display:block;width:320px;height:30px;" href="#mediaURL#"></div>  
          </td>
        </tr>
      </table>
    </cfif>    
  </cfif>

  <p><a href="index.cfm?md=newsroom&amp;tmp=category&amp;catid=#categoryInfo.articleCategoryID[1]#" class="lnkAccent02">Read More About &ldquo;#categoryInfo.articleCategory[1]#&rdquo; &raquo;</a></p>
</cfoutput>

<cfif relatedItems.recordcount GT 0>
  <!--Related Info-->
  <div class="relatedInfo">
    <div class="ttl">Related Info</div>
    <ul> 
    <cfoutput query="relatedItems">
      <cfswitch expression="#relatedItemType#">
        <cfcase value="article">
        <li><a href="#href#" <cfif popup>target="_blank"</cfif> class="lnkAccent02">#headline# &raquo;</a></li>
        </cfcase>
        <cfcase value="gallery">
        <li><a href="index.cfm?md=photogallery&tmp=album&albumID=#albumID#" class="lnkAccent02">#albumTitle# &raquo;</a></li>
        </cfcase>
        <cfcase value="link">
        <li><a href="#linkURL#" target="_blank" class="lnkAccent02">#linkName# &raquo;</a></li>
        </cfcase>
        <cfcase value="file">
        <li><a href="#filePath#" target="_blank" class="lnkAccent02">#fileTitle# &raquo;</a></li>
        </cfcase>
      </cfswitch>
    </cfoutput>
    </ul>
  </div>
</cfif>  
</div>