<cfparam name="URL.catid">
<cfparam name="URL.startIndex" default="1">
<cfparam name="URL.numItemsPerPage" default="20">
<cfsilent>
<cfscript>
  UT=CreateObject("component","com.Utility").init();
  NR=CreateObject("component","com.Newsroom").init();
  categoryInfo=NR.getArticleCategoryByCategoryID(URL.catid);
  structArticles=NR.getArticlesByCategoryID(articleCategoryID=URL.catid, startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage);
  hasDocumentFiles=false;
  for (i=1; i <= structArticles.articles.recordcount; i = i + 1) {
    if (structArticles.articles.contentType Is 3) {hasDocumentFiles=true; break;};
  }
</cfscript>

<cfif categoryInfo.recordcount Is 0><cflocation url="/404error.cfm"></cfif>

<cffunction name="getFileType" access="public" output="no" returntype="struct">
  <cfargument name="fileURL" type="string" required="yes">
  <cfscript>
    structFileType=StructNew();
	structFileType.imageFileName="";
	structFileType.fileExtension="";	
	if (Len(Arguments.fileURL) LT 5) {return structFileType;}
	fileExt=Right(Arguments.fileURL,5);
	dotPos=Find(".",fileExt);
	if (dotPos Is 0) {return structFileType;}
	fileExt=LCase(Right(fileExt, 6 - dotPos));
	switch(fileExt) {
	  case ".doc":
	    structFileType.imageFileName="imgIconWord.gif";
		structFileType.fileExtension=".DOC";
		break;
	  case ".docx":
	    structFileType.imageFileName="imgIconWord.gif";
		structFileType.fileExtension=".DOCX";
		break;
	  case ".ppt":
	    structFileType.imageFileName="imgIconPwrPoint.gif";
		structFileType.fileExtension=".PPT";
		break;
	  case "pptx":
	    structFileType.imageFileName="imgIconPwrPoint.gif";
		structFileType.fileExtension=".PPTX";
		break;
	  case ".pdf":
		structFileType.imageFileName="imgIconAcrobat.gif";
		structFileType.fileExtension=".PDF";
		break;
	  default: break;
  	}
	return structFileType;
  </cfscript>  
</cffunction>
</cfsilent>

<div id="topperHTMLCodes" style="display:none"><cfoutput>#categoryInfo.articleCategory[1]#</cfoutput></div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_info_library.jpg" width="202" height="244" /></div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#titleBar").html($("#topperHTMLCodes").html());
	$("#thmemImage").html($("#themeImageHTMLCodes").html());
  });
</script>

<cfif structArticles.numAllItems GT URL.numItemsPerPage>
  <cfset navPanel=UT.displayNextPreviousLinks(pageURL="index.cfm?md=newsroom&tmp=archives&catid=#URL.catid#", startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage, numDisplayedItems=structArticles.numDisplayedItems, numAllItems=structArticles.numAllItems, itemName="Results")>
  <cfoutput>#navPanel#</cfoutput>
</cfif>
 
<div class="contentWrapper">
<!--INTRO-->
<cfif Compare(categoryInfo.description, "")>
<div class="introNoBottBorder"><cfoutput>#categoryInfo.description#</cfoutput></div>
</cfif>   

<cfif categoryInfo.RSSEnabled>
<div class="rss"><cfoutput><a href="index.cfm?md=newsroom&tmp=rss&nowrap=1&catid=#URL.catid#"><img src="images/imgRssIcon.jpg" alt="RSS" width="36" height="43" border="0" /></a></cfoutput></div>
</cfif>

<div class="floatLeftFullWdth">
<table width="469" border="0" cellpadding="0" cellspacing="0" class="infoLib">
  <tr>
    <td width="85%" class="hdrBar">Item</td>
    <td width="15%" align="center" class="hdrBar"><cfif hasDocumentFiles>Type<cfelse>&nbsp;</cfif></td>
  </tr>
  <cfoutput query="structArticles.articles">
  <tr>
    <td align="left" valign="top">
      <cfif categoryInfo.displayArticleDate>
      #DateFormat(articleDate, "mm.dd.yy")#<br />
      </cfif>
      <a href="#href#"<cfif popup> target="_blank"</cfif>>#headline#</a>
    </td>
    <td align="center" valign="top" class="icon">
      <cfset fileType=getFileType(href)>
      <cfif Compare(fileType.imageFileName, "")>
      <img src="images/#fileType.imageFileName#" width="16" height="16" /><br />
      #fileType.fileExtension#
      <cfelse>
      &nbsp;
      </cfif>
    </td>
  </tr>
  </cfoutput>
</table>
</div>
</div>
<!--DISPLAY TOOLS-->
<cfif structArticles.numAllItems GT URL.numItemsPerPage>
  <cfoutput>#navPanel#</cfoutput>
</cfif>