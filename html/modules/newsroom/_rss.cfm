<cfparam name="URL.catid" default="0">
<cfsilent>
<cfparam name="URL.type" default="topic"><!--- topic: by category ID; region: by region Code --->
<cfparam name="URL.catid" default="0">
<cfparam name="URL.region" default="">
<cfscript>
  UT=CreateObject("component", "com.Utility").init();
  NR=CreateObject("component","com.Newsroom").init();
  if (Not Compare(URL.type, "topic")) {//list news by category
	resultStruct=NR.getArticlesByCategoryID(articleCategoryID=URL.catid, startIndex=1, numItemsPerPage=20);
	categoryInfo=NR.getArticleCategoryByCategoryID(URL.catid);
	sectionTitle="#categoryInfo.articleCategory# News";
	channelLink="#Application.siteURLRoot#/index.cfm?md=newsroom&amp;tmp=category&amp;catid=#URL.catid#";
  } else {//list news by region
	resultStruct=NR.getArticlesByRegionCode(regionCode=URL.region, startIndex=1, numItemsPerPage=20);
	switch (URL.region) {
	  case "AP":
	    sectionTitle="Asia/Pacific News";
		break;
	  case "AM":
	    sectionTitle="Americas News";
		break;
	  case "EU":
	    sectionTitle="Europe News";
		break;
	  case "ME":
	    sectionTitle="Middle East/Africa News";
		break;
	  default:
	    sectionTitle="News";
		break;
	}    
	channelLink="#Application.siteURLRoot#/index.cfm?md=newsroom&amp;tmp=category&amp;type=region&amp;region=#URL.region#";
  }
</cfscript>
</cfsilent>

<cfif resultStruct.numDisplayedItems GT 0>
<cfset articleDateTime = resultStruct.articles.articleDate[1]>

<cfsavecontent variable="XMLOutput"><cfoutput><?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
  <channel>
    <title>#HTMLEditFormat(Application.siteName)#<cfif Compare(sectionTitle,"")> &gt; #HTMLEditFormat(sectionTitle)#</cfif></title>
    <link>#channelLink#</link>
    <pubDate>#UT.getGMT(articleDateTime)#</pubDate>
    <description />
    <language>en-us</language>
    <copyright>Copyright #Year(now())# #HTMLEditFormat(Application.siteName)#</copyright>
    <lastBuildDate>#UT.getGMT(articleDateTime)#</lastBuildDate>
    <cfloop query="resultStruct.articles">
    <item>
      <title>#HTMLEditFormat(headline)#</title>
      <link>#Application.siteURLRoot#/index.cfm?md=newsroom&amp;tmp=detail&amp;articleID=#articleID#</link>
      <description>#HTMLEditFormat(teaser)#</description>
      <pubDate>#UT.getGMT(articleDate)#</pubDate>
      <guid isPermaLink="false">#URL.catid#-#articleID#</guid>
    </item>
    </cfloop>    
  </channel>
</rss>
</cfoutput>
</cfsavecontent>
<cfelse>
  <cfset XMLOutput="<?xml version=""1.0""?><rss version=""2.0""><channel><title></title></channel></rss>">
</cfif>

<cfcontent type="application/xml;charset=UTF-8"><cfoutput>#XMLOutput#</cfoutput>