<cfif Compare(Request.template.topMostAscendantNavID,0) OR ListLen(Request.template.mainNavIDList) EQ 1>
  <!--- need to either display the child nav tree branch of Request.template.topMostAscendantNavID or Request.template.mainNavIDList --->
  <cfsilent>
  <cfscript>
    NV=CreateObject("component", "com.Navigation").init();
    if (Compare(Request.template.topMostAscendantNavID,0)) {
      Variables.leftNavs=NV.getNavBranch(navGroupID=1, navID=Request.template.topMostAscendantNavID);
	  if (Variables.leftNavs.numSubNavs Is 0 And ListLen(Request.template.mainNavIDList) EQ 1) {//if there are no sub-nav branch to show, then show the default sub-nav branch set up in template
	    Variables.leftNavs=NV.getNavBranch(navGroupID=1, navID=Request.template.mainNavIDList);
	  }
    } else {//show the default sub-nav branch set up in template
      Variables.leftNavs=NV.getNavBranch(navGroupID=1, navID=Request.template.mainNavIDList);
    }
  </cfscript>
  </cfsilent>
    
  <cfif Variables.leftNavs.numSubNavs GT 0>
    <div class="vertNavWrapper">
      <ul class="firstLevel">
      <cfoutput>
      <cfloop index="idx" from="1" to="#Variables.leftNavs.numSubNavs#">
        <cfif (Not Compare(Variables.leftNavs.nav[idx].navID, Request.template.parentNavID) Or Not Compare(Variables.leftNavs.nav[idx].navID, Request.template.navID)) And Variables.leftNavs.nav[idx].numSubNavs GT 0><cfset expand=true><cfelse><cfset expand=false></cfif>
        <li<cfif expand> class="expanded"</cfif>>
          <a href="#Variables.leftNavs.nav[idx].href#"<cfif Variables.leftNavs.nav[idx].popup> target="_blank"</cfif>>#Variables.leftNavs.nav[idx].navName#</a>          
          <cfif Variables.leftNavs.nav[idx].numSubNavs GT 0>
            <cfif Not expand>
            <a href="#Variables.leftNavs.nav[idx].href#" <cfif Variables.leftNavs.nav[idx].popup> target="_blank"</cfif> onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgSubNavRt02','','images/imgSubNavRtArrowo.gif',1)"><img src="images/imgSubNavRtArrow.gif" alt="Click for Subnav" name="imgSubNavRt02" width="9" height="9" border="0" id="imgSubNavRt02" /></a>
            <cfelse>
            <img src="images/imgSubNavDownArrow.gif" width="9" height="6" border="0" />
            <ul class="secondLevel">
              <cfloop index="idx2" from="1" to="#Variables.leftNavs.nav[idx].numSubNavs#">
              <li><a href="#Variables.leftNavs.nav[idx].nav[idx2].href#"<cfif Variables.leftNavs.nav[idx].nav[idx2].popup> target="_blank"</cfif>>#Variables.leftNavs.nav[idx].nav[idx2].navName#</a></li>
              </cfloop>
            </ul>        
            </cfif>        
          </cfif>
        </li>    
      </cfloop>
      </cfoutput>
      </ul>
    </div>
  </cfif>
</cfif>