<cfparam name="URL.rn"><!--- physician record number --->
<cfparam name="URl.addtl" default="0"><!--- additional address --->
<cfsilent>
<cfscript>
  DD=CreateObject("component","com.Doctor").init();
  doctorInfo=DD.getDoctorByRecordNum(URL.rn);
  if (URL.addtl) {
    //address=doctorInfo.address1Additional & ", " & doctorInfo.cityAdditional & ", " & doctorInfo.stateAdditional & " " & doctorInfo.ZIPAdditional;
	address=doctorInfo.address1Additional & ", " & doctorInfo.cityAdditional & ", " & doctorInfo.stateAdditional;
  } else {
    //address=doctorInfo.address1 & ", " & doctorInfo.citya & ", " & doctorInfo.statea & " " & doctorInfo.zipa;
	address=doctorInfo.address1 & ", " & doctorInfo.citya & ", " & doctorInfo.statea;
  }
  address=Replace(address, "'", "\'", "All");  
</cfscript>
</cfsilent>

<cfoutput><script type="text/javascript" src="http://www.google.com/jsapi?key=#Application.googleMapAPIKey#"></script></cfoutput>
<script type="text/javascript">
google.load("maps", "2.x");
var map;
<cfoutput>var address = "#address#";</cfoutput>
function showGoogleMap() {
  if (google.maps.BrowserIsCompatible()) {
	map = new google.maps.Map2(document.getElementById("map"));	
	//map.addControl(new google.maps.SmallMapControl());
	geocoder = new google.maps.ClientGeocoder();	
	if (geocoder) {
	  geocoder.getLatLng(
		address,
		function(point) {
		  if (!point) {
			alert(address + " not found");
		  } else {
			map.setCenter(point, 15);
			map.setUIToDefault();
			map.addOverlay(new google.maps.Marker(point));
		  }
		}
	  );
	}	
  }
}

function getDirection () {
  var fAddress=document.frmGetDirectory.fromAddress.value;	
  if (fAddress == "") return;	  
  var panel = document.getElementById("direction");
  var dir = new GDirections(map, panel);
  
  var qString="from: " + fAddress + " to: " + address;
  map.closeInfoWindow();
  dir.load(qString);
}

google.setOnLoadCallback(showGoogleMap);
window.onunload= function () {google.maps.Unload();}
</script>

<style type="text/css">
.docName { font-size:18px; color:#6F7876; height:38px;}
.print {text-decoration:none; color:#6F7876; }
</style>
<style type="text/css" media="print">
#frmGetDirectory {display:none;}
</style>

<cfoutput query="doctorInfo">
  <div class="head">#firstName# #lastName#<cfif Compare(degree,"")>, <span class="small12">#degree#</span></cfif></div>

  <p>
	<cfif URL.addtl>
      #address1Additional#<br />
      <cfif Compare(address2Additional,"")>#address2Additional#<br /></cfif>
      #cityAdditional#, #stateAdditional# #ZIPAdditional#
    <cfelse>
      #address1#<br />
      <cfif Compare(address2,"")>#address2#<br /></cfif>
      #citya#, #statea# #zipa#
    </cfif>       
    
    <cfif Compare(phone,"")><br />
    #phone#
    </cfif>
  </p>
  <div id="map" style="margin:5px 0px;width:500px;height:300px;"></div>
  <div style="margin:15px 0px 5px 0px;width:500px;">
  <form name="frmGetDirectory" id="frmGetDirectory" onsubmit="return false;">
  <b>My Address: </b> (street address, city, state)<br />
  <input type="text" name="fromAddress" style="width:200px;" />  <input type="button" value="Get Direction" onclick="getDirection()" />
  </form>
  </div>  
  <div id="direction" style="margin:5px 0px;width:500px;"></div>
</cfoutput>