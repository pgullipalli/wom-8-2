<cfparam name="URL.searchBy" default="">
<cfparam name="URL.doctorName" default="">
<cfparam name="URL.officeName" default="">
<cfparam name="URL.spid" default="0">
<cfparam name="URL.startIndex" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfsilent>
<cfscript>
  DD=CreateObject("component","com.Doctor").init();
  UT=CreateObject("component","com.Utility").init();
  if (Compare(URL.spid,"0")) {//search by specialty ID
    specialty=DD.getSpecialtyBySpecialtyID(URL.spid);
	returnResult=DD.getDoctorsBySpecialtyID(specialty=specialty, startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage);
  } else if (Not Compare(URL.searchBy, "name")) {//search by doctor's name
    returnResult=DD.getDoctorsByNameKeyword(keyword=URL.doctorName, startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage);
  } else {//search by physician group name
    returnResult=DD.getDoctorsByOfficeNameKeyword(keyword=URL.officeName, startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage);
  }
  if (returnResult.numAllItems Is 1 And returnResult.numDisplayedItems Is 1) {
	UT.location("index.cfm?md=doctor&tmp=detail&rn=#returnResult.doctors.recordNum[1]#&referer=home&nid=#URL.nid#");
  }
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none"><cfoutput><a href="index.cfm?md=doctor&tmp=home&nid=#URL.nid#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnTpBarNewSearch','','images/btnTpBarNewSearcho.gif',1)"><img src="images/btnTpBarNewSearch.gif" alt="New Search" name="btnTpBarNewSearch" width="105" height="24" border="0" id="btnTpBarNewSearch" /></a><span>Doctor Directory</span></cfoutput></div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtDocNurse.jpg" width="202" height="244" /></div>
<script type="text/javascript">
  var scrollableApi;
  $(document).ready(function(){
    $("#titleBar").attr("id","titleBarWithBtnsText").html($("#topperHTMLCodes").remove().html());
	$("#thmemImage").html($("#themeImageHTMLCodes").html());
  });
</script>

<cfif returnResult.numAllItems GT URL.numItemsPerPage>
  <cfset navPanel = UT.displayNextPreviousLinks(pageURL="index.cfm?md=doctor&amp;tmp=searchresults&amp;searchBy=#URL.searchBy#&amp;doctorName=#URLEncodedFormat(URL.doctorName)#&amp;officeName=#URLEncodedFormat(URL.officeName)#&amp;spid=#URL.spid#&amp;nid=#URL.nid#", startIndex=URL.startIndex, numItemsPerPage=URL.numItemsPerPage, numDisplayedItems=returnResult.numDisplayedItems, numAllItems=returnResult.numAllItems, itemName="Results")>
  <cfoutput>#navPanel#</cfoutput>
  <br style="clear:both;" />
</cfif>


<!--DISPLAY TOOLS-->
<div class="contentWrapperIndent" id="docDirect">
  <cfif returnResult.numDisplayedItems GT 0>
    <cfif Compare(URL.spid,"0")>
  	  <div class="introResults"><span class="label">Specialty </span> <span class="accent02Lrg"><cfoutput>#specialty#</cfoutput></span></div>
    <cfelseif Not Compare(URL.searchBy, "name")>
  	  <div class="introResults"><span class="label">Search By Name </span> <span class="accent02Lrg"><cfoutput>"#URL.doctorName#"</cfoutput></span></div>
    <cfelse>
      <div class="introResults"><span class="label">Search By Physician Group </span> <span class="accent02Lrg"><cfoutput>"#URL.officeName#"</cfoutput></span></div>
    </cfif>
  
  <!--TWO COLUMN RESULTS-->
  <cfoutput>
  <div class="resultsLft">
    <cfloop index="idx" from="1" to="#returnResult.numDisplayedItems#">
      <cfif idx MOD 2 IS 1><!--- odd items --->
        <a href="index.cfm?md=doctor&tmp=detail&rn=#returnResult.doctors.recordNum[idx]#&nid=#URL.nid#">#returnResult.doctors.firstName[idx]# #returnResult.doctors.lastName[idx]#<cfif Compare(returnResult.doctors.degree[idx],"")>, <span class="small10">#returnResult.doctors.degree[idx]#</span></cfif></a>
        </cfif>
      </cfloop>
  </div>
    <div class="resultsRt">
      <cfloop index="idx" from="1" to="#returnResult.numDisplayedItems#">
        <cfif idx MOD 2 IS 0><!--- even items --->
          <a href="index.cfm?md=doctor&tmp=detail&rn=#returnResult.doctors.recordNum[idx]#&nid=#URL.nid#">#returnResult.doctors.firstName[idx]# #returnResult.doctors.lastName[idx]#<cfif Compare(returnResult.doctors.degree[idx],"")>, <span class="small10">#returnResult.doctors.degree[idx]#</span></cfif></a>
          </cfif>
        </cfloop>      
    </div>
</cfoutput>
<cfelse>
<cfoutput><b>No Results Found</b><br />Please try modifying your search terms.</cfoutput>


  </cfif>
</div>
 


<cfif returnResult.numAllItems GT URL.numItemsPerPage>
  <cfoutput>#navPanel#</cfoutput>
</cfif>
