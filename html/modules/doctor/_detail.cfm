<cfparam name="URL.rn"><!--- physician record number --->
<cfparam name="URL.referer" default="">
<cfsilent>
<cfscript>
  DD=CreateObject("component","com.Doctor").init();
  doctorInfo=DD.getDoctorByRecordNum(URL.rn);
  boardCertList=DD.getBoardCertList(URL.rn);
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none"><cfoutput><a href="index.cfm?md=doctor&tmp=home&nid=#URL.nid#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnTpBarNewSearch','','images/btnTpBarNewSearcho.gif',1)"><img src="images/btnTpBarNewSearch.gif" alt="New Search" name="btnTpBarNewSearch" width="105" height="24" border="0" id="btnTpBarNewSearch" /></a><cfif Not Compare(URL.referer,"")><a href="javascript:history.go(-1)" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnTpBarReturn','','images/btnTpBarReturno.gif',1)"><img src="images/btnTpBarReturn.gif" alt="Return To Results" name="btnTpBarReturn" width="137" height="24" border="0" id="btnTpBarReturn" /></a></cfif><span>Doctor Directory</span></cfoutput></div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtDocNurse.jpg" width="202" height="244" /></div>
<script type="text/javascript">
  var scrollableApi;
  $(document).ready(function(){
    $("#titleBar").attr("id","titleBarWithBtnsText").html($("#topperHTMLCodes").remove().html());
	$("#thmemImage").html($("#themeImageHTMLCodes").html());
  });
</script>

<!--DISPLAY TOOLS-->
<div class="contentWrapperIndent" id="docDirect">
  <cfoutput query="doctorInfo">
  <!--SPECIALTY-->
  <div class="introResults"><span class="label">Specialty </span> <span class="accent02Lrg">#specialty#</span></div>
  <!--DOC DETAILS-->
  <div class="docName">#firstName# #lastName#<cfif Compare(degree,"")>, <span class="small12">#degree#</span></cfif></div>
<div class="detailLft" align="center">
      <cfif Compare(photo,"")><img src="#photo#" width="148" height="148" class="phtStroke" /></cfif>
      <!--addresses-->
      <cfif Compare(address1,"") OR Compare(address1Additional,"")>
        <span class="titleBarSm">Address </span>
      </cfif>
      <cfif Compare(address1,"")>      
      <p class="content">
        #address1#<br />
		<cfif Compare(address2,"")>#address2#<br /></cfif>
        #citya#, #statea# #zipa#<br />
        <a href="javascript:openWindow('index.cfm?md=doctor&tmp=office_google_map&popup=1&rn=#URL.rn#','gmap',560,720)" class="lnkAccent01">View Map &gt;</a>
      </p>
      </cfif>
      
      <cfif Compare(address1Additional,"")>      
      <p class="content">
        #address1Additional#<br />
		<cfif Compare(address2Additional,"")>#address2Additional#<br /></cfif>
        #cityAdditional#, #stateAdditional# #ZIPAdditional#<br />
        <a href="javascript:openWindow('index.cfm?md=doctor&tmp=office_google_map&popup=1&rn=#URL.rn#&addtl=1','gmap',560,720)" class="lnkAccent01">View Map &gt;</a>
      </p>
      </cfif>            
      
      <cfif Compare(phone,"") OR Compare(fax,"") OR Compare(email,"") OR Compare(officeWebsite,"")>
      <!--contact info-->
      <span class="titleBarSm">Contact Information</span>
      <p class="content" id="cntctInfo">
        <cfif Compare(phone,"")>
        <span class="label">PH:</span> #phone#<br />
        </cfif>
        <cfif Compare(fax,"")>
        <span class="label">FX:</span> #fax#<br />
        </cfif>
        <cfif Compare(email,"")>
        <span class="label">EMAIL:</span> <a href="mailto:#email#">#email#</a><br />
        </cfif>
        <cfif Compare(officeWebsite,"")>
          <cfif FindNoCase("http", officeWebsite) IS 0><cfset webLink="http://" & officeWebsite><cfelse><cfset webLink=officeWebsite></cfif>
          <span class="label">WEB:</span> <a href="#webLink#" target="_blank">#officeWebsite#</a><br />
        </cfif>
      </p>
      </cfif>      
    </div>
    <div class="detailRt">
      <cfif Compare(bio,"")>
        <cfif Find("<p>",bio) Is 1><cfset bioNew=Replace(bio,"<p>","<p><span class=""label"">Bio</span>")><cfelse><cfset bioNew="<span class=""label"">Bio</span>" & bio></cfif>
        #bioNew#
        <cfif Find("<p>",bioNew) Is 0><br /><br /></cfif>
      </cfif>
      <cfif Compare(philosophy,"")>
        <cfif Find("<p>",philosophy) Is 1><cfset philosophyNew=Replace(philosophy,"<p>","<p><span class=""label"">Special Medical Interests</span>")><cfelse><cfset philosophyNew="<span class=""label"">Special Medical Interests</span>" & philosophy></cfif>
        #philosophyNew#
        <cfif Find("<p>",philosophyNew) Is 0><br /><br /></cfif>
      </cfif>
      
      <cfif Compare(boardCertList,"")>
		<p><span class="label">Board Certifications</span> #Replace(boardCertList,",",", ","All")#</p>
	  </cfif>
      
      <div id="docGroup">
        <cfif Compare(officeName,"")>
          <span class="label">Group</span> <span class="groupName">#officeName#</span>          
        </cfif> 
        <cfif Compare(groupLogo,"")>
          <cfif Compare(officeWebsite,"")>
            <a href="#webLink#"><img src="#groupLogo#" width="98" height="98" border="0" class="phtStroke" /></a>  
          <cfelse>
            <img src="#groupLogo#" width="98" height="98" border="0" class="phtStroke" />
          </cfif>
        </cfif>

        <cfif Compare(staff,"")>
        <div class="staff">
          <span class="label">Staff</span>
          Susan Smith, <i>RN</i><br />
          Jacob Jones, <i>RN</i><br />
          Rita Roberts, <i>Receptionist</i>
        </div>
        <br clear="left" />
        </cfif>
      </div>
    </div>    
  </cfoutput>
</div><!--- docDirect --->

