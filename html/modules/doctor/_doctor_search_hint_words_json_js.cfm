<cfsilent>
<cfscript>
  DD=CreateObject("component","com.Doctor").init();
  allOfficeNames=DD.getAllOfficeNames();
  allDoctorNames=DD.getAllDoctorNames();
  hintWords=StructNew();
  hintWords.officeName=ArrayNew(1);
  hintWords.doctorName=ArrayNew(1);
  for (i=1; i LTE allOfficeNames.recordcount; i=i+1) {
    hintWords.officeName[i]=StructNew();
	hintWords.officeName[i].id=i;
    hintWords.officeName[i].text=allOfficeNames.officeName[i];
  }
  for (i=1; i LTE allDoctorNames.recordcount; i=i+1) {
    hintWords.doctorName[i]=StructNew();
	hintWords.doctorName[i].id=i;
    hintWords.doctorName[i].text=allDoctorNames.firstName[i] & " " & allDoctorNames.lastName[i];
  }
</cfscript>
</cfsilent>

<cfcontent type="application/x-javascript;charset=utf-8">
<cfoutput>
var hintWords=#Replace(Replace(SerializeJSON(hintWords),'"ID"','"id"','All'),'"TEXT"','"text"','All')#;
</cfoutput>