<cfsilent>
<cfscript>
  DD=CreateObject("component","com.Doctor").init();
  allSpecialties=DD.getAllSpecialties();
  introductionText=DD.getIntroductionText();
</cfscript>
</cfsilent>

<div id="topperHTMLCodes" style="display:none;">Doctor Directory</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtDocNurse.jpg" width="202" height="244" /></div>
<script type="text/javascript" src="jsapis/jquery/js/jquery.tools.min.js"></script>
<script language="JavaScript" src="jsapis/jquery/js/jquery.jsonSuggest.js"></script>
<script language="JavaScript" src="jsapis/jquery/js/json2.js"></script>
<script language="JavaScript" src="index.cfm?md=doctor&tmp=doctor_search_hint_words_json_js&nowrap=1"></script>
<script type="text/javascript">
  var scrollableApi;
  var scrollStatus = "stop";
  var scrollSpeed = 2000;
  $(document).ready(function(){
    $("#titleBar").html($("#topperHTMLCodes").html());
	$("#thmemImage").html($("#themeImageHTMLCodes").html());
	$('input#doctorName').jsonSuggest(hintWords.DOCTORNAME);
	$('input#officeName').jsonSuggest(hintWords.OFFICENAME);
	// initialize scrollable  
    $("div.scrollable").scrollable({ 
        vertical:true,
		clickable:false,		
        size: 7   
    }).mousewheel();// use mousewheel plugin 
	scrollableApi=$("div.scrollable").scrollable();
	
	$("#scrollUp").mouseover(function(e) {scrollStatus="up"; scrolling();});
	$("#scrollUp").mouseout(function(e) {scrollStatus="stop";});
	$("#scrollDown").mouseover(function(e) {scrollStatus="down"; scrolling();});
	$("#scrollDown").mouseout(function(e) {scrollStatus="stop";});
	//$("#scrollUp").click(function(e) {scrollableApi.prevPage();});
	//$("#scrollDown").click(function(e) {scrollableApi.nextPage();});
	
  });
  
  function scrolling() {
    if (scrollStatus != "stop") {
	  if (scrollStatus == "up") {
		scrollableApi.prevPage();
		window.setTimeout("scrolling()",scrollSpeed);
	  } else if (scrollStatus == "down") {
	    scrollableApi.nextPage();
		window.setTimeout("scrolling()",scrollSpeed);
	  } else {
	    scrollStatus = "stop";
	  }
	}
  }
  
  function searchByName() {
    var form = document.frmSearchDoctors;
	form.searchBy.value = "name";
	if (form.doctorName.value.length < 2) {
	  alert('Please enter at least 3 characters.');
	  form.doctorName.focus();
	  return;
	}
	form.submit();
  }
  
  function searchByGroup() {
    var form = document.frmSearchDoctors;
    form.searchBy.value = "group";
	if (form.officeName.value.length < 2) {
	  alert('Please enter at least 3 characters.');
	  form.officeName.focus();
	  return;
	}
	form.submit();
  }
</script>

<style type="text/css">
div.jsonSuggestResults {
	position:absolute;
	border:1px solid #CCC;
	padding:0px;
	margin:0px 2px;
	z-index:1;
}
div.jsonSuggestResults div.resultItem {
	margin:0px;
	padding:5px;
	position:relative;
	height:auto;
	cursor:pointer;
}
div.jsonSuggestResults div.resultItem.odd {
	background-color:#FFEAFF;
}
div.jsonSuggestResults div.resultItem.even {
	background-color:#FFFFFF;
}
div.jsonSuggestResults div.resultItem.hover {
	background-color:#FFBFFF;
}
div.jsonSuggestResults div.resultItem img {
	float:left;
	margin-right:10px;
}
div.jsonSuggestResults div.resultItem p {
	margin:0px;
	padding:0px;
}
div.jsonSuggestResults div.resultItem p strong {
	font-weight:bold;
	text-decoration:underline;
}
div.jsonSuggestResults div.resultItem p.extra {
	font-size: x-small !important;
	position:absolute;
	bottom:3px;
	right: 3px;
}

/* root element for scrollable */ 
div.scrollable { 
    /* required settings */ 
    position:relative; 
    overflow:hidden; 
    /* vertical scrollers have typically larger height than width */ 
    height: 240px; 
    width: 290px; 
} 
 
/* root element for scrollable items */ 
div.scrollable div.items { 
    position:absolute; 
    /* this time we have very large space for the height */ 
    height:20000em;
}
div.scrollable div.items div {width:290px; margin:0px 0px 8px 0px; padding:0px 0px 6px 0px; border-bottom:1px dotted #999;}
div.scrollable div.items div a {font-size:14px; line-height:100%; color:#7F7F7F; text-decoration:none;}
div.scrollable div.items div a:hover {color:#4D4A4C; text-decoration:none;}
</style>

<div class="contentWrapperIndent" id="docDirect">
    <!--INTRO-->
    <div class="intro">
      <cfoutput>#introductionText#</cfoutput>
    </div>
    
    <form id="frmSearchDoctors" name="frmSearchDoctors" method="get" action="index.cfm" onsubmit="return false;">
    <input type="hidden" name="md" value="doctor" />
    <input type="hidden" name="tmp" value="searchresults" />
    <cfoutput><input type="hidden" name="nid" value="#URL.nid#" /></cfoutput>
    <input type="hidden" name="searchBy" value="" />   
    <div class="search"><span class="label">Search By </span><span class="accent02Lrg">Name:</span><a href="#" onclick="searchByName()" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormLrg01','','images/btnFormLrgo.gif',1)"><img src="images/btnFormLrg.gif" alt="Submit" name="btnFormLrg01" width="53" height="41" border="0" class="floatRight" id="btnFormLrg01" /></a><input type="text" name="doctorName" id="doctorName" /></div>
    <div class="search"><span class="label">Search By </span><span class="accent02Lrg">Physician Group:</span><a href="#" onclick="searchByGroup()" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormLrg02','','images/btnFormLrgo.gif',1)"><img src="images/btnFormLrg.gif" alt="Submit" name="btnFormLrg02" width="53" height="41" border="0" class="floatRight" id="btnFormLrg02" /></a><input type="text" name="officeName" id="officeName" /></div>
      <div class="scrollPod"><span class="label">Search By </span>      
      <span class="accent02Lrg">Specialty:</span><a id="scrollUp" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAjaxScrollTop','','images/btnAjaxScrollTopo.gif',1)"><img src="images/btnAjaxScrollTop.gif" alt="Scroll Up" name="btnAjaxScrollTop" width="333" height="17" border="0" id="btnAjaxScrollTop" /></a><br />
      <div class="content">
        <!-- root element for scrollable -->
        <div class="scrollable vertical"> 
          <!-- root element for the items --> 
          <div class="items">
            <cfoutput query="allSpecialties">
            <div><a href="index.cfm?md=doctor&tmp=searchresults&spid=#specialtyID#&nid=#URL.nid#">#specialty#</a></div>
            </cfoutput>  
          </div>
        </div>   
        
      </div><a id="scrollDown" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAjaxScrollBott','','images/btnAjaxScrollBotto.gif',1)"><img src="images/btnAjaxScrollBott.gif" alt="Scroll Down" name="btnAjaxScrollBott" width="333" height="17" border="0" id="btnAjaxScrollBott" /></a></div>
      </form>
</div>