<cfscript>
  CM=CreateObject("component","com.Communication").init();
  allLists=CM.getAllLists(published=1, orderBy="listID");
</cfscript>
<script type="text/javascript">
function subscribeMyWomans(form) {
  if (!checkEmail(form.email.value)) {
    displayInlineMessage('errorMsg','Please enter a valid e-mail address.');
    form.email.focus();
    return;	  
  }
  
/*
  if (!getCheckedValue(form.listIDList)) {
    displayInlineMessage('errorMsg','Please select at least one list to subscribe.');
    return;	 
  }

*/

	
  form.submit();
}

function resetField(obj, str) {
  if (obj.value == str) obj.value = "";
}
</script>

<div class="letter-box">
    <h3>myWoman's E-Newsletter</h3>
    <div class="letter-box-holder">
        <div class="section">
            <p>Looking for tailored content delivered straight to you? Sign up for our e-newsletters today! </p>
        </div>
<div id="errorMsg" style="background-color:#FFFF00;color: #000000;display: block;float: right;font-size: 11px;margin: 0;width: 176px;"></div>
        <form class="letter-box-frame" action="action.cfm?md=communication&amp;task=addMemberToLists" method="post" name="myWomansSignUp">
           <input type="hidden" id="listIDList" name="listIDList" value="1" />            
                <fieldset>
                <legend>email-address</legend>
                <div class="input">
                    <input type="text" title="first name"  value="First Name" id="firstName" />
                </div>
                <div class="input">
                    <input type="text" title="last name" value="Last Name" id="lastName" />
                </div>
                <div class="input">
                    <input type="text" title="email" value="E-Mail Address" id="email" />
                </div>
                <div class="submit">
                   <a class="btnMed" href="javascript:subscribeMyWomans(document.myWomansSignUp)">Subscribe</a>
                </div>
            </fieldset>
        </form>
    </div>
</div>
                                    
                                    
<!---<div id="myWomansHoriz">
  <div class="floatLeft"><img src="images/imgHmMyWomansHdr.gif" width="731" height="11" alt="" /></div>
  <div class="content"><form name="myWomansSignUp" action="action.cfm?md=communication&amp;task=addMemberToLists" method="post">
      <div class="intro"><span class="title">myWoman's</span> Looking for tailored content delivered straight to you? Sign up for our e-newsletters today! </div>
      <div class="options" align="left">
        <div id="errorMsg" style="margin:0px 10px 10px 0px;padding:5px 5px;color:#000000;background-color:#FFFF00;font-size:11px;display:none;"></div>
        <table border="0" cellspacing="0" cellpadding="1">
          <cfoutput>
          <cfloop index="rowNum" from="1" to="#Ceiling(allLists.recordcount/2)#">
          <tr>
            <cfloop index="idx" from="#(rowNum-1)*2 + 1#" to="#(rowNum-1)*2 + 2#">
            <cfif idx LTE allLists.recordcount>
            <td align="left" valign="middle"><input type="checkbox" name="listIDList" value="#allLists.listID[idx]#" /></td>
            <td align="left" valign="middle"><label>#allLists.listName[idx]#</label></td>
            <cfelse>
            <td colspan="2">&nbsp;</td>
            </cfif>  
            </cfloop>             
          </tr>
          </cfloop>
          </cfoutput>
        </table>
      </div>
       <div class="info" align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><input name="firstName" type="text" value="First Name" size="30" onclick="resetField(this,'First Name')" /></td>
          </tr>
          <tr>
            <td><input name="lastName" type="text" value="Last Name" size="30" onclick="resetField(this,'Last Name')" /></td>
          </tr>
          <tr>
            <td><input name="email" type="text" value="E-Mail Address" size="30" onclick="resetField(this,'E-Mail Address')" /></td>
          </tr>
        </table>
      </div>
      <div class="submit" align="right"><a href="javascript:subscribeMyWomans(document.myWomansSignUp)" class="btnMed">Subscribe</a></div>
  </form></div>
  <div class="floatLeft"><img src="images/imgHmMyWomansFtr.gif" width="731" height="11" alt="" /></div>
</div>
--->