<cfparam name="URL.elidlist">
<cfparam name="URL.error" default="0">

<div id="topperHTMLCodes" style="display:none;">Sign Up</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<script type="text/javascript">
<!--
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());  
});
//-->
</script>

<div class="contentWrapper">
  <cfif URL.error>
	<span class="head">Sign Up</span>
    
    <br />
    
    <p>It appears that there is an error in the URL that you tried to visit. Sorry, we can't opt you in at this time.</p>
  <cfelse>
	<cfset CM=CreateObject("component", "com.Communication").init()>
    <cfset listNames = CM.getListNamesByListIDList(URL.elidlist)>
    
    <span class="head">Thank You For Signing Up</span>
    
    <br />
    <table border="0" width="100%" cellpadding="3" cellspacing="0" align="center">
      <tr valign="top">
        <td>
        <cfoutput>
          <p>Congratulations!  You have successfully completed the subscription process for the following list(s):</p>
          
          <ul><cfloop query="listNames"><li>#listName#</li></cfloop></ul>
          
          <p>
          Remember that you can unsubscribe any time by clicking the unsubscribe link at the bottom of all future e-mail
          messages you receive from this site.  Thank you for your interest!
          </p>
          
          <p><a href="#Application.siteURLRoot#">Back To Home Page</a></p>
        </cfoutput>
        </td>
      </tr>
    </table>
  </cfif>
</div>