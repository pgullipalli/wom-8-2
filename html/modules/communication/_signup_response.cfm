<cfparam name="URL.elidlist">

<cfset CM=CreateObject("component", "com.Communication").init()>
<cfset listNames = CM.getListNamesByListIDList(URL.elidlist)>


<div id="topperHTMLCodes" style="display:none;">Thank You For Signing Up</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<script type="text/javascript">
<!--
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());  
});
//-->
</script>

<div class="contentWrapper">
    <span class="head">Thank You For Signing Up</span>
    
    <br />
    
    <cfoutput>
    
    <p>You have successfully initiated the opt-in process for the following e-mail list(s):</p>
    <ul>
      <cfloop query="listNames">
      <li><b>#listName#</b></li>
      </cfloop>
    </ul>
    <p>
    A confirmation e-mail message will be sent to the e-mail address you provided.
    To complete the opt-in process, you must follow the instructions provided in the confirmation
    e-mail message you receive before you will actually be subscribed to the
    list(s).  Once you complete the instructions provided in the
    confirmation e-mail message, your subscription process will be complete.
    </p>
    
    <p>
    Thank you for your interest!
    </p>
    </cfoutput>
</div>
