<cfparam name="URL.task">

<cffunction name="cfcontent" access="public" output="no">
  <cfargument name="filepath" type="string" required="yes">
  <cfcontent file="#arguments.filepath#" deletefile="no">
</cffunction>

<cfscript>
  UT=CreateObject("component", "com.Utility").init();
  CM=CreateObject("component", "com.Communication").init();
  
  switch (URL.task) {
    case "addMemberToLists":
	  Form.doubleOptinEmailRequired=1;
	  CM.addMemberToLists(argumentCollection=Form);
      UT.location("index.cfm?md=communication&tmp=signup_response&elidlist=#URLEncodedFormat(Form.listIDList)#");
	  break;
	  
	case "optIn":    
	  decryptedID=CM.simpleDecrypt(URL.ID);
	  if (IsNumeric(decryptedID)) {
	    CM.optIn(emailID=decryptedID, listIDList=URL.elidlist);
		UT.location("index.cfm?md=communication&tmp=optin_response&elidlist=#URL.elidlist#");
      } else {
	    UT.location("index.cfm?md=communication&tmp=optin_response&elidlist=#URL.elidlist#&error=1");
	  }
	  
      break;
	  
	case "addMailOpenCount":
      //URL.msgid
	  //URL.uid (Encrypted email ID)
	  emailID=CM.simpleDecrypt(URL.uid);
	  if (IsNumeric(emailID) AND Compare(emailID,"0")) {
	    CM.addMailOpenCount(emailID="#emailID#",messageID="#URL.msgid#");
	  }
	  cfcontent("#Replace(ExpandPath("."),"\","/","All")#/images/spacer.gif");  
      break;
	  
	case "addMessageClickThru":
	  //URL.msgid
	  //URL.uid (Encrypted email ID)

      emailID=CM.simpleDecrypt(URL.uid);
	  if (IsDefined("URL.redirect")) {
	    idx = FIND("redirect=", CGI.QUERY_STRING);
		if (idx GT 0) {	
	      linkURL = Right(CGI.QUERY_STRING, LEN(CGI.QUERY_STRING) - idx - 8);
		  linkURL="#Replace(URLDecode(linkURL),"&amp;", "&", "ALL")#";
		  if (IsNumeric(emailID) AND emailID IS NOT "0") {	  
	        CM.addMessageClickThru(messageID="#URL.msgid#", emailID="#emailID#", linkURL="#linkURL#");
		  }
		  UT.location("#linkURL#");
		}
	  }
	  UT.location("#Application.siteURLRoot#");
	  break;
	  
	case "updateMemberToLists":
	  Form.emailID=CM.simpleDecrypt(Form.uid);
	  if (IsNumeric(Form.emailID)) {
        Form.doubleOptinEmailRequired=1;
        CM.updateMemberToLists(argumentCollection=Form);
        UT.location("index.cfm?md=communication&tmp=updateprofile_response");
	  }
      break;
	  
	default:
	  break;
  }
</cfscript>