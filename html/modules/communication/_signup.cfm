<cfparam name="URL.sfmid" default="0">
<cfparam name="URL.msgid" default="0">
<cfparam name="URL.e" default=""><!--- email address --->
<cfsilent>
<cfscript>
  UT=CreateObject("component", "com.Utility").init();
  CM=CreateObject("component", "com.Communication").init();
  
  if (Compare(URL.msgid,"0") AND URL.sfmid IS "0") {
    URL.sfmid=CM.getSignUpFormIDByMessageID(URL.msgid);
  }
  
  signUpFormInfo=CM.getSignUpFormInfo(URL.sfmid);
  if (signUpFormInfo.recordcount GT 0) { Variables.hasSignUpForm=true; }
  else { Variables.hasSignUpForm=false; }
  
  if (Variables.hasSignUpForm AND Compare(signUpFormInfo.formID,0)) {//linked to a form
    UT.location("index.cfm?md=form&tmp=home&fmid=" & signUpFormInfo.formID & "&sfmid=" & URL.sfmid);
  }
  
  signUpFormLists=CM.getSignUpFormLists(URL.sfmid);
  if (signUpFormLists.recordcount Is 0) {//no lists associated with the signup form; then get all lists
    signUpFormLists=CM.getAllLists(published=1);
  }
  
  if (signUpFormLists.recordcount Is 0) { UT.abort(); };
  
  /* header files */
  newline=Chr(13) & Chr(10);
  htmlHead='<link rel="stylesheet" type="text/css" href="modules/form/form.css" />' & newline;
  htmlHead=htmlHead & '<script type="text/javascript" src="jsapis/jquery/js/jquery-1.3.2.min.js"></script>' & newline;
  htmlHead=htmlHead & '<script type="text/javascript" src="jsapis/jquery/js/jquery.xsite.js"></script>' & newline;
  htmlHead=htmlHead & '<script type="text/javascript" src="modules/communication/communication.js"></script>' & newline;
</cfscript>
</cfsilent>

<cfhtmlhead text="#htmlHead#">

<div id="topperHTMLCodes" style="display:none;"><cfoutput><cfif Variables.hasSignUpForm>#signUpFormInfo.signUpFormName#<cfelse>Sign Up</cfif></cfoutput></div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<script type="text/javascript">
<!--
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());  
});
//-->
</script>

<div class="contentWrapper">
	<cfoutput>
    <span class="head"><cfif Variables.hasSignUpForm>#signUpFormInfo.signUpFormName#<cfelse>Sign Up</cfif></span>
    
    <br />
    
    <div id="formLayout">
    <form name="frmSignUpForm" id="frmSignUpForm" action="action.cfm?md=communication&task=addMemberToLists" method="post" onsubmit="return submitForm(this);">
    <input type="hidden" name="signUpFormID" value="#URL.sfmid#" />
    <div class="row">
      <div class="col-first-right">Your Name</div>
      <div class="col-second-left">
        <div class="col-name">
          <input type="text" name="firstName" maxlength="50" class="Small" value="" /><br />
          <small>(First Name)</small></div>
        <div class="col-name">
          <input type="text" name="lastName" maxlength="50" class="Small" value="" /><br />
          <small>(Last Name)</small></div>
      </div>
    </div>
    
    <div class="row">
      <div class="col-first-right"><b>E-Mail Address</b></div>
      <div class="col-second-left">
        <input type="text" name="email" maxlength="100" class="Medium" value="#HTMLEditFormat(URL.e)#" />
      </div>
    </div>
    
    <cfif Variables.hasSignUpForm>
    <div class="row">
      <div class="col-whole-left">#signUpFormInfo.headerText#</div>
    </div>
    <cfelse>
    <div class="row">
      <div class="col-whole-left">
        <cfif signUpFormLists.recordcount IS 1>
        Check to subscribe to this email list.
        <cfelse>	
        Select lists to subscribe.
        </cfif>
      </div>
    </div>
    </cfif>
    
    <div class="row">
      <div class="col-first-right">&nbsp;</div>
      <div class="col-second-left">      
        <cfloop query="signUpFormLists">
          <input type="checkbox" name="listIDList" value="#listID#" checked /> #listName#<br />
        </cfloop>        
      </div>
    </div>  
    
    <div class="row">&nbsp;</div>
    
    <div class="row">
      <div class="col-first-right">&nbsp;</div>
      <div class="col-second-left">
        <input type="submit" value="Submit" class="btnSubmit">
      </div>
    </div>
    
    </form>
    </div>
    </cfoutput>
</div>