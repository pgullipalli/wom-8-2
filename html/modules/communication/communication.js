function submitForm(form) {
  if (!xsite.checkEmail(form.email.value)) {
    alert('Please enter an valid e-mail address.');
    form.email.focus();
    return false;	  
  }
  
  if (!xsite.getCheckedValue(form.listIDList)) {
    alert('Please select at least one list to subscribe.');
    return false;	 
  }
	
  return true;
}