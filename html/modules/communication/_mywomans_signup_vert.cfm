<cfscript>
  CM=CreateObject("component","com.Communication").init();
  allLists=CM.getAllLists(published=1, orderBy="listID");
</cfscript>
<script type="text/javascript">
function subscribeMyWomans(form) {
  if (!checkEmail(form.email.value)) {
    displayInlineMessage('errorMsg','Please enter a valid e-mail address.');
    form.email.focus();
    return;	  
  }
  
  if (!getCheckedValue(form.listIDList)) {
    displayInlineMessage('errorMsg','Please select at least one list to subscribe.');
    return;	 
  }
	
  form.submit();
}

function resetField(obj, str) {
  if (obj.value == str) obj.value = "";
}
</script>
<div id="myWomansVert">
  <form name="myWomansSignUp" action="action.cfm?md=communication&amp;task=addMemberToLists" method="post">
<span class="title">Subscribe Now</span> Looking for tailored content delivered straight to you? Sign up for our e-newsletters today! 
    <div class="options" align="left">
      <div id="errorMsg" style="margin:0px 10px 10px 0px;padding:5px 5px;color:#000000;background-color:#FFFF00;font-size:11px;display:none;"></div>
      <table border="0" cellpadding="1" cellspacing="0">
        <cfoutput query="allLists">
        <tr>
          <td align="left" valign="middle"><input type="checkbox" name="listIDList" value="#listID#" />
          </td>
          <td align="left" valign="middle">&nbsp;<label>#listName#</label>
          </td>
        </tr>
        </cfoutput>
      </table>
    </div>
    <div align="left" class="info">
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><input name="firstName" type="text" value="First Name" size="30" onclick="resetField(this,'First Name')" /></td>
        </tr>
        <tr>
          <td><input name="lastName" type="text" value="Last Name" size="30" onclick="resetField(this,'Last Name')" /></td>
        </tr>
        <tr>
          <td><input name="email" type="text" value="E-Mail Address" size="30" onclick="resetField(this,'E-Mail Address')" /></td>
        </tr>
      </table>
    </div>
    <div class="submit" align="left"><a href="javascript:subscribeMyWomans(document.myWomansSignUp)" class="btnMed">Subscribe</a></div>
  </form>
</div>