<cfparam name="URL.msgid" type="numeric">
<cfparam name="URL.uid" type="string"><!--- Need to decode --->

<cfset CM=CreateObject("component", "com.Communication").init()>
<cfset emailID=CM.simpleDecrypt(URL.uid)>

<div id="topperHTMLCodes" style="display:none;">Update Profile</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_classes_programs.jpg" width="202" height="244" /></div>
<script type="text/javascript">
<!--
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());  
});
//-->
</script>

<div class="contentWrapper">



<span class="head">Update Profile</span>
<br />

<cfif NOT IsNumeric(emailID)>
  <p>Sorry, but your e-mail address can not be found in our database.</p>
<cfelse>
  <cfset basicInfo=CM.getEmailByEmailID(emailID)>
  <cfset allLists=CM.getAllLists(published=1)>
  <cfset subscribedListIDsList=CM.getListIDListByEmailID(emailID)>
  
  <cfif allLists.recordcount IS 1>
    <p>
  	Please update your profile by completing your changes and submitting the form below.
	To unsubscribe from the list, remove the check mark and submit the form.
	Thank you for your interest. 
    </p>  
  <cfelseif allLists.recordcount GT 1>
    <p>
  	Please update your profile by completing your changes and submitting the form below.
	If you would like to subscribe to a new list, add a check mark in the box next to that list's name and
	submit the form. If you would like to unsubscribe from a list, remove the check mark in the box next to
	that list's name and submit the form. If you would like to unsubscribe from all lists, remove all check
	marks and submit the form. Thank you for your interest. 
    </p>  
  </cfif>
  <cfif allLists.recordcount IS NOT 0>
  <cfoutput>
  <form name="form1" action="action.cfm?md=communication&task=updateMemberToLists" method="post">
  <input type="hidden" name="uid" value="#URL.uid#">
  <table border="0" width="100%" cellpadding="3" cellspacing="0">
    <tr valign="top">
	  <td width="50%" align="right"><b>E-Mail Address</b></td>
	  <td width="50%"><input type="text" name="email" size="30" value="#HTMLEditFormat(basicInfo.email)#"></td>
	</tr>
	<tr valign="top">
	  <td align="right">First Name</td>
	  <td><input type="text" name="firstName" size="30" value="#HTMLEditFormat(basicInfo.firstName)#"></td>
	</tr>
	<tr valign="top">
	  <td align="right">Last Name</td>
	  <td><input type="text" name="lastName" size="30" value="#HTMLEditFormat(basicInfo.lastName)#"></td>
	</tr>
	<cfloop query="allLists">
	<tr valign="top">
	  <td align="right"><input type="checkbox" name="listIDList" value="#listID#"<cfif ListFind(subscribedListIDsList, listID) IS NOT 0> checked</cfif>></td>
	  <td>#listName#</td>
	</tr>
	</cfloop>
	<tr>
	  <td>&nbsp;</td>
	  <td><input type="submit" value=" Update "></td>
	</tr>	
  </table>  
  </form>
  </cfoutput>
  </cfif>
</cfif>

</div>