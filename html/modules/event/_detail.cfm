<cfparam name="URL.eventid" default="0">
<cfparam name="URL.catid" default="0">
<cfscript>
  CL=CreateObject("component","com.Event").init();
  if (Compare(URL.catid,"0")) {
    category=CL.getCategory(URL.catid);
  } else {
    category=CL.getCategoryByEventID(URL.eventid);
  }  
  eventInfo=CL.getEventByID(eventID=URL.eventid);  
</cfscript>
<cfif category.recordcount Is 0 OR eventInfo.recordcount Is 0><cfabort></cfif>

<div id="topperHTMLCodes" style="display:none;"><cfoutput>#category.categoryName#</cfoutput></div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_event_calendar.jpg" width="202" height="244" /></div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "")  $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
});
</script>

<div class="contentWrapper" id="classes">
  <cfoutput query="eventInfo">
  <span class="head">#eventName#</span>
  <div class="detailLft">
    <cfif Compare(eventImage,"")>
    <div class="thmbnail">
      <div class="floatLeft"><img src="#eventImage#" width="148" align="left" class="phtStroke" /></div>
      <cfif Compare(eventImageCaption,"")>
      <div class="caption">#eventImageCaption#</div>
      </cfif>
    </div>
    </cfif>
    
    #longDescription#
    
    <cfif Compare(eventURL,"")>
      <cfif Find("index.cfm?",eventURL) Is 0 AND Find("http",eventURL) Is 0>
        <cfset webAddress="http://" & eventURL>
      <cfelse>
        <cfset webAddress=eventURL>
      </cfif>
      <p><i>Event Website:</i> <a href="#webAddress#" <cfif Find("index.cfm?",eventURL) Is 0>target="_blank"</cfif> class="lnkAccent02">#eventURL#</a></p>
    </cfif>
  </div>
  <div class="detailRt">
    <div class="content">
      <p>
        <span class="labelAccent02">Date</span>
        <span class="med14">
          <cfset startDateStr=DateFormat(startDateTime,"mm.dd.yy")>
          <cfset endDateStr=DateFormat(endDateTime,"mm.dd.yy")>
          #startDateStr#
          <cfif Compare(startDateStr, endDateStr)> - #endDateStr#</cfif>
        </span>
      </p>
      <cfif Not allDayEvent>
      <p>
        <span class="labelAccent02">Time</span>
        <span class="med14">
          #LCase(TimeFormat(startDateTime,"h:mmtt"))# - #LCase(TimeFormat(endDateTime,"h:mmtt"))#
        </span>
      </p>      
      </cfif>
      <cfif Compare(eventLocation,"")>
      <p><span class="labelAccent02">Location</span> <span class="med14">#eventLocation#</span></p>
      </cfif>
    </div>
  </div>
  </cfoutput>
</div>