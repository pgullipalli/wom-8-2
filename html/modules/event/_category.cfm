<cfparam name="URL.catid" default="0">
<cfscript>
  CL=CreateObject("component","com.Event").init();
  category=CL.getCategory(URL.catid);
  currentDateTime=now();
  currentMonth=CreateDate(Year(currentDateTime), Month(currentDateTime), 1);
  Variables.fromDate=CreateDate(Year(currentDateTime), Month(currentDateTime), Day(currentDateTime));
  Variables.toDate=DateAdd("m", 12, currentMonth);
  events=CL.getEventsByDateRange(categoryID=URL.catid, fromDate=Variables.fromDate, toDate=Variables.toDate);  
</cfscript>
<cfif category.recordcount Is 0><cfabort></cfif>

<div id="topperHTMLCodes" style="display:none;"><cfoutput>#category.categoryName#</cfoutput></div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/pht_event_calendar.jpg" width="202" height="244" /></div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
});
</script>


<div class="contentWrapper" id="classes">
  <cfset eventMonthTemp="">
  <cfoutput query="events">
    <cfset eventMonth=DateFormat(startDateTime,"mmmm")>
    <cfif Compare(eventMonth, eventMonthTemp)>
    <div class="calMonth">#eventMonth#</div>
    </cfif>
    <div class="calEvent">
     <a href="index.cfm?md=event&tmp=detail&catid=#URL.catid#&eventid=#eventID#" class="lnkEventName">#eventName#</a><br />
     <span class="accent01"><b>#DayOfWeekAsString(DayOfWeek(startDateTime))# | #DateFormat(startDateTime, "mmmm d, yyyy")#<cfif Not allDayEvent> | #LCase(TimeFormat(startDateTime,"h:mmtt"))#</cfif></b></span><br />
     #shortDescription#
   </div>  
    <cfset eventMonthTemp=eventMonth>
  </cfoutput>
</div>