﻿<cfparam name="URL.task">

<cfset RA=CreateObject("component","com.RestrictedArea").init()>
<cfswitch expression="#URL.task#">
  <cfcase value="login">
    <cfif RA.login(username=Form.username, password=Form.password)>
      <cfif Compare(Form.qs,"")>
        <cflocation url="index.cfm?#Form.qs#">
      <cfelse>
        <cflocation url="#Application.siteURLRoot#">
      </cfif>
    <cfelse>
      <cflocation url="index.cfm?md=protected&tmp=login&qs=#URLEncodedFormat(Form.qs)#&failed=1">
    </cfif>
  </cfcase>
  
  <cfcase value="logout">
    <cfset StructDelete(Cookie, "groupIDList")>
    <cfcookie name="groupIDList" value="" expires="now">
    <cflocation url="index.cfm?md=protected&tmp=login">
  </cfcase>
</cfswitch>