﻿<cfparam name="URL.qs" default="">
<cfparam name="URL.failed" default="0">
<div id="topperHTMLCodes" style="display:none;">Log In</div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtMomDaughter.jpg" width="202" height="244" /></div>
<script type="text/javascript">
$(document).ready(function(){
  if (trim($("#titleBar").html()) == "") $("#titleBar").html($("#topperHTMLCodes").html());
  if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
});
</script>
<div class="contentWrapper" id="forms">
  <div class="floatLeftFullWdth">   
    <cfoutput>
    <form name="frmLogin" id="frmLogin" action="#Application.siteURLRootSSL#/action.cfm?md=protected&task=login" method="post">
      <input type="hidden" name="qs" value="#HTMLEditFormat(URL.qs)#">
      <div class="unit">
        <span class="section">Log In</span>
      </div>
      <cfif URL.failed>
      <div class="unit">
        <div class="accent02"><b>Sorry, but your login information is not recognized. Please try again.</b><br /><br /></div>
      </div>
      </cfif>
      <div class="unit">
        <div class="singleField">
          <label for="username" class="descrip">Username</label>
          <input type="text" name="username" id="username" />
        </div>        
      </div>
      <div class="unit">
        <div class="singleField">
          <label for="password" class="descrip">Password</label>
          <input type="password" name="password" id="password" />
        </div>        
      </div>
      <div class="unit">
        <input type="image" src="images/btnFormSubmit.gif" alt="Submit" name="btnFormSubmit" style="width:63px;height:17px; border:0px solid ##ffffff;" border="0" id="btnFormSubmit" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormSubmit','','images/btnFormSubmito.gif',1)" />
      </div>
    </form>  
    </cfoutput>
  </div>
</div>
