<cfparam name="URL.fmid">
<cfparam name="URL.elidlist" default=""><!--- subscribe to lists --->
<cfparam name="URL.sid">

<cfset FM=CreateObject("component", "com.Form").init()>
<cfset formInfo=FM.getFormProperties(URL.fmid)>
<cfset submission=FM.getSubmissionBasicInfo(URL.sid)>

<div class="contentWrapper" id="forms">

<div id="topperHTMLCodes" style="display:none"><cfoutput>#formInfo.formName#</cfoutput></div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtMomDaughterCouch.jpg" width="202" height="244" /></div>
<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "")  $("#titleBar").html($("#topperHTMLCodes").html());
	if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
  });
</script>

<div class="head"><cfoutput>#formInfo.formName#</cfoutput></div>

<cfoutput>
<br style="clear:both;" /><br />
<p class="small12">
#formInfo.submissionConfirmationMessage#
</p>
</cfoutput>

<cfif Compare(URL.elidlist,"")>
	<cfset CM=CreateObject("component", "com.Communication").init()>
    <cfset listNames = CM.getListNamesByListIDList(URL.elidlist)>
    
    <p>You have successfully initiated the opt-in process for the following e-mail list(s):</p>
    <ul>
      <cfoutput query="listNames">
      <li><b>#listName#</b></li>
      </cfoutput>
    </ul>
    <p>
    A confirmation e-mail message will be sent to the e-mail address you provided.
    To complete the opt-in process, you must follow the instructions provided in the confirmation
    e-mail message you receive before you will actually be subscribed to the
    list(s).  Once you complete the instructions provided in the
    confirmation e-mail message, your subscription process will be complete.
    </p>
    
    <p>
    Thank you for your interest!
    </p>
</cfif>
</div>
