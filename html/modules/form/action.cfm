<cfparam name="URL.task">
<cfparam name="URL.tid" default="0">
<cfscript>
  FM=CreateObject("component", "com.Form").init();
  UT=CreateObject("component", "com.Utility").init();

  switch (URL.task) {
    case 'submitForm':
	  Form.userIPAddress=CGI.REMOTE_ADDR;
	  submissionID=FM.submitForm(argumentCollection=Form);
	  
	  if (IsDefined("Form.listIDList")) {//sign up for email lists
	    CM=CreateObject("component", "com.Communication").init();
	    Form.doubleOptinEmailRequired=1;
		CM.addMemberToLists(argumentCollection=Form);
		UT.location("index.cfm?md=form&tmp=response&fmid=" & Form.formID & "&elidlist=" & URLEncodedFormat(Form.listIDList) & "&sid=" & submissionID & "&tid=" & URL.tid);
	  } else { 
  	    UT.location("index.cfm?md=form&tmp=response&fmid=" & Form.formID & "&sid=" & submissionID  & "&tid=" & URL.tid);
	  }
      break;
	case 'saveForm':
	  Form.userIPAddress=CGI.REMOTE_ADDR;
	  Form.emailForSavedData=URL.em;
	  Form.passwordForSavedData=URL.rcode;
	  submissionID=FM.saveForm(argumentCollection=Form);
	  encodedSubmissionID=Encrypt("#submissionID#", Application.encryptionKey);
	  formURL=Application.siteURLRoot & "/index.cfm?md=form&fmid=" & URL.fmid & "&sfmid=" & URL.sfmid;
	  dataRetrievalURL=Application.siteURLRoot & "/index.cfm?md=form&fmid=" & URL.fmid & "&sfmid=" & URL.sfmid & "&encSid=" & URLEncodedFormat(encodedSubmissionID);
	  FM.sendConfirmationEmailForRetrievingData(emailForSavedData=URL.em, passwordForSavedData=URL.rcode, formID=URL.fmid, formURL=formURL, dataRetrievalURL=dataRetrievalURL);
      UT.location("index.cfm?md=form&tmp=response_savedata&fmid=" & URL.fmid & "&sid=" & submissionID & "&em=" & URLEncodedFormat(URL.em));	
	  break;
	case 'retrieveForm':  
      submissionID=FM.retrieveSavedSubmissionID(formID=URL.fmid, emailForSavedData=URL.em, passwordForSavedData=URL.rcode);
	  if (submissionID GT 0) { // do have saved form data
        encodedSubmissionID=Encrypt("#submissionID#", Application.encryptionKey);
        UT.location("index.cfm?md=form&fmid=" & URL.fmid & "&sfmid=" & URL.sfmid & "&encSid=" & URLEncodedFormat(encodedSubmissionID));	
      } else { // Can't find saved form data
	    UT.location("index.cfm?md=form&fmid=" & URL.fmid & "&sfmid=" & URL.sfmid & "&em=" & URLEncodedFormat(URL.em) & "&datafound=0");
	  }
 	  break;
	case 'retrieveRetrieveCode':
	  success=FM.retrieveRetrieveCode(formID=URL.fmid, emailForSavedData=URL.em);
	  if (success) {
	    UT.location("index.cfm?md=form&fmid=" & URL.fmid & "&sfmid=" & URL.sfmid & "&rcfound=1"); // found retrieve code
	  } else {
	 	UT.location("index.cfm?md=form&fmid=" & URL.fmid & "&sfmid=" & URL.sfmid & "&em=" & URLEncodedFormat(URL.em) & "&rcfound=0"); // Can't find retrieve code
	  }
	  break;
	default:
	  break;
  }
</cfscript>
