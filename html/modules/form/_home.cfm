<cfsilent>
<cfparam name="URL.fmid">
<cfparam name="URL.sfmid" default="0"><!--- email sign-up form ID --->
<cfparam name="URL.encSid" default=""><!--- encrypted submission ID --->

<cfscript>
  FM=CreateObject("component", "com.Form").init();
  UT=CreateObject("component", "com.Utility").init();
  
  if (Find("/admin/",CGI.HTTP_REFERER)) {
    formInfo=FM.getFormProperties(formID=URL.fmid,publishedOnly=0);
  } else {
	formInfo=FM.getFormProperties(URL.fmid);
  }
  groupInfo=FM.getGroupInfoByFormID(URL.fmid);
  
  if (formInfo.recordcount EQ 0 OR groupInfo.recordcount EQ 0) {
    UT.abort();//form doesn't exist
  }

  numGroups=groupInfo.recordcount; // this form has numGroups pages
  
  if (IsDefined("Form.nextGroupIndex")) { // this is the page number of this page
    Variables.thisGroupIndex = Form.nextGroupIndex;
  } else {
    Variables.thisGroupIndex = 1;
  }
  
  Variables.isLastPage=true;
  
  if (Variables.thisGroupIndex LT numGroups) {
	Variables.nextGroupIndex=Variables.thisGroupIndex + 1; // there are more page(s)
	Variables.isLastPage=false;
  }
  
  if (Variables.isLastPage) {
    Variables.formAction="action.cfm?md=form&task=submitForm&fmid=" & URL.fmid & "&sfmid=" & sfmid & "&tid=" & URL.tid;
  } else {
    Variables.formAction="index.cfm?md=form&tmp=home&fmid=" & URL.fmid & "&sfmid=" & sfmid & "&encSid=" & URLEncodedFormat(URL.encSid) & "&tid=" & URL.tid;
  }
  
  if (Compare(URL.encSid,"")) {
    deString = Decrypt(URL.encSid, Application.encryptionKey);
	if (IsNumeric(deString)) {
  	  formBodyHTML=FM.getFormBodyHTML(formID=URL.fmid, groupID=groupInfo.groupID[Variables.thisGroupIndex], submissionID=deString, retrieveSavedData=1);
	} else {
	  formBodyHTML=FM.getFormBodyHTML(formID=URL.fmid, groupID=groupInfo.groupID[Variables.thisGroupIndex]);
	}
  } else {
    formBodyHTML=FM.getFormBodyHTML(formID=URL.fmid, groupID=groupInfo.groupID[Variables.thisGroupIndex]);
  }
  
  // alert messages
  alertMessage = "";
  if (IsDefined("URL.datafound") AND Not URL.datafound) {
    alertMessage="No saved data can be found with the information you provided. Please make sure you have entered the correct information.";
  } else if (IsDefined("URL.rcfound")) {
    if (URL.rcfound) {
	  alertMessage="Thank you. Your retrieve code has been sent to your e-mail address.";
	} else {
	  alertMessage="No retrieve code can be found with the e-mail address you provided. Please make sure you have entered the correct information.";
	}
  }
  
  /* header files */
  newline=Chr(13) & Chr(10);
  htmlHead='<link rel="stylesheet" href="jsapis/jquery/css/jquery-ui-1.7.2.datepicker.css" type="text/css">' & newline;
  htmlHead=htmlHead & '<link rel="stylesheet" type="text/css" href="modules/form/form.css" />' & newline;
  htmlHead=htmlHead & '<script type="text/javascript" src="jsapis/jquery/js/jquery-1.3.2.min.js"></script>' & newline;
  htmlHead=htmlHead & '<script type="text/javascript" src="jsapis/jquery/js/jquery-ui-1.7.2.core_datepicker.min.js"></script>' & newline;
  htmlHead=htmlHead & '<script type="text/javascript" src="jsapis/jquery/js/jquery.xsite.js"></script>' & newline;
  htmlHead=htmlHead & '<script type="text/javascript" src="modules/form/form.js"></script>' & newline;
</cfscript>
</cfsilent>
<cfhtmlhead text="#htmlHead#">

<div class="contentWrapper" id="forms">

<div id="topperHTMLCodes" style="display:none"><cfoutput>#formInfo.formName#</cfoutput></div>
<div id="themeImageHTMLCodes" style="display:none;"><img src="images/phtFtMomDaughterCouch.jpg" width="202" height="244" /></div>
<script type="text/javascript">
  $(document).ready(function(){
    if (trim($("#titleBar").html()) == "")  $("#titleBar").html($("#topperHTMLCodes").html()); 
	if (trim($("#thmemImage").html()) == "") $("#thmemImage").html($("#themeImageHTMLCodes").html());
	
  });
</script>

<div id="formLayout">

<div class="head"><cfoutput>#formInfo.formName#</cfoutput></div>

<cfif formInfo.enableDataSavingFeature AND Variables.thisGroupIndex IS 1><!--- Fist Page --->
  <p>
  <a href="javascript:xsite.displayDialogue('divRetrieveData',true)" class="accent01">Retrieve my previously saved data</a> |
  <a href="javascript:xsite.displayDialogue('divForgotRetrieveCode',true)" class="accent01">Forgot your retrieve code?</a>
  </p>
    
  <cfparam name="URL.em" default="">
  <div id="divRetrieveData" style="display:none;width:420px;height:250px;">
      <div style="padding:10px 10px;width:400px;height:230px;border:10px solid #FFC4E1;background-color:#ffffff;">
        <cfoutput>
        <form action="action.cfm" method="get">
        <input type="hidden" name="md" value="form">
        <input type="hidden" name="task" value="retrieveForm">
        <input type="hidden" name="fmid" value="#URL.fmid#">
        <input type="hidden" name="sfmid" value="#URL.sfmid#" />
        <div class="row">
          <div class="col-whole-right"><a href="javascript:xsite.closeHTMLDialogue();"><img src="images/imgClose.png" border="0" style="margin:-20px -20px 0px 0px;" /></a></div>
        </div>
        <div class="row">
          <div class="col-whole-left">
            <p>Please enter the e-mail address and retrieve code that you used to save your data.</p>            
          </div>
        </div>
        <div class="row">
          <div class="col-first-right">E-Mail Address:</div>
          <div class="col-second-left"><input type="text" name="em" size="25" maxlength="100" value="#HTMLEditFormat(URL.em)#"></div>
        </div>
        <div class="row">
          <div class="col-first-right">Retrieve Code:</div>
          <div class="col-second-left"><input type="text" name="rcode" size="12" maxlength="12"></div>
        </div>
        <div class="row">
          <div class="col-first-right">&nbsp;</div>
          <div class="col-second-left"><input type="button" value="Retrieve" onclick="retrieveForm(this)" class="button"></div>
        </div>
        </form>
        </cfoutput>
      </div>
  </div>
  
  <div id="divForgotRetrieveCode" style="width:420px;height:200px;display:none;">
      <div style="padding:10px 10px;width:400px;height:180px;border:10px solid #FFC4E1;background-color:#ffffff;">
        <cfoutput>
        <form action="action.cfm" method="get">
        <input type="hidden" name="md" value="form">
        <input type="hidden" name="task" value="retrieveRetrieveCode">
        <input type="hidden" name="fmid" value="#URL.fmid#">
        <input type="hidden" name="sfmid" value="#URL.sfmid#" />
        <div class="row">
          <div class="col-whole-right"><a href="javascript:xsite.closeHTMLDialogue()"><img src="images/imgClose.png" border="0" style="margin:-20px -20px 0px 0px;" /></a></div>
        </div>
        <div class="row">
          <div class="col-whole-left">
            <p>Please enter the e-mail address that you used to save your data.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-first-right">E-Mail Address:</div>
          <div class="col-second-left"><input type="text" name="em" size="30" maxlength="100" value="#HTMLEditFormat(URL.em)#"></div>
        </div>
        <div class="row">
          <div class="col-first-right">&nbsp;</div>
          <div class="col-second-left"><input type="button" value="Submit" onclick="retrieveCode(this)" class="button"></div>
        </div>
        </form>
        </cfoutput>
      </div>
  </div>
</cfif>

<!--- ************************ BEGIN: FORM BODY ***************************** --->
<cfif Compare(alertMessage, "")>
<p class="alert"><cfoutput>#alertMessage#</cfoutput></p>
</cfif>

<cfoutput>
<form name="frmXForm" id="frmXForm" action="#Variables.formAction#" method="post" onsubmit="return submitForm(this);">
<cfif Variables.thisGroupIndex IS 1><!--- Fist Page --->
  <input type="hidden" name="formID" value="#URL.fmid#">
  <input type="hidden" name="signUpFormID" value="#URL.sfmid#">
  <input type="hidden" name="isFirstPage" value="1">
</cfif>
<cfif Not Variables.isLastPage><!--- Not the Last Page --->
  <input type="hidden" name="nextGroupIndex" value="#Variables.nextGroupIndex#" />
</cfif>

<cfif Variables.thisGroupIndex GT 1>
  <!--- import submitted data from previous form --->
  <cfloop index="field" list="#Form.fieldNames#">
    <cfif CompareNoCase(field, "nextGroupIndex") AND CompareNoCase(field, "isFirstPage")>
    <input type="hidden" name="#LCase(field)#" value="#HTMLEditFormat(Form["#field#"])#">
    </cfif>
  </cfloop>
</cfif>

#formBodyHTML#

<cfif Compare(URL.sfmid,"0") AND Variables.isLastPage>
  <!--- Include email lists for signing up here; this block can be ignored if Communication Manager is not available  --->
  <cfset CM=CreateObject("component", "com.Communication").init()>
  <cfset signUpFormInfo=CM.getSignUpFormInfo(URL.sfmid)>
  <cfset signUpFormLists=CM.getSignUpFormLists(URL.sfmid)>
  <cfset hasEmailField=FM.hasEmailField(URL.fmid)>
  <cfset hasNameField=FM.hasNameField(URL.fmid)>
  
  <cfif signUpFormInfo.recordcount GT 0 AND signUpFormLists.recordcount GT 0>  
    <div class="row">
      <div class="col-whole-left">#signUpFormInfo.headerText#</div>
    </div>

    <cfif Not hasNameField>  
    <div class="row">
      <div class="col-first-right">Your Name</div>
      <div class="col-second-left">
        <div class="col-name">
          <input type="text" name="firstName" maxlength="50" class="Small" value="" /><br />
          <small>(First Name)</small></div>
        <div class="col-name">
          <input type="text" name="lastName" maxlength="50" class="Small" value="" /><br />
          <small>(Last Name)</small></div>
      </div>
    </div>
    </cfif>
    <cfif Not hasEmailField>
    <div class="row">
      <div class="col-first-right">E-Mail Address</div>
      <div class="col-second-left">
        <input type="text" name="email" maxlength="100" class="Medium" value=""  />
      </div>
    </div>
    </cfif>
    
    <div class="row">
      <div class="col-first-right">&nbsp;</div>
      <div class="col-second-left">
        <cfloop query="signUpFormLists">
          <input type="checkbox" name="listIDList" value="#listID#" /> #listName#<br />
        </cfloop>
      </div>
    </div> 
  </cfif>
</cfif>

<div class="row">
  <div class="col-whole-left">
  <cfif Variables.thisGroupIndex GT 1>
	<input type="button" value="Back" onClick="history.go(-1)" class="btnSubmit">
  </cfif>
  <cfif Variables.isLastPage>
    <cfif CompareNoCase(formInfo.submitButtonLabel,"Submit")>  
      <input type="submit" value="#formInfo.submitButtonLabel#" class="btnSubmit">
    <cfelse>
      <input type="image" src="images/btnFormSubmit.gif" alt="Submit" name="btnFormSubmit" id="btnFormSubmit" style="width:63px;height:17px; border-width:0px;" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnFormSubmit','','images/btnFormSubmito.gif',1)" />
    </cfif>
    <cfif formInfo.sendReceiptToUser>
    <br />
    <input type="checkbox" name="sendReceiptToUser" value="1" align="absmiddle" checked /> Send me a copy of my message.
    </cfif>
  <cfelse>
    <input type="submit" value="Continue" class="btnSubmit">
  </cfif>
  </div>
</div>
</form>

<div class="row">&nbsp;</div>
</cfoutput>
<!--- ************************ END: FORM BODY ***************************** --->


<cfif formInfo.enableDataSavingFeature>
  <p><a href="javascript:xsite.displayDialogue('divSavedData',true)" class="accent01">Please save my data. I'd like to finish it at later time.</a></p>
  <div id="divSavedData" style="width:420px;height:320px;display:none;">
      <div style="padding:10px 10px;width:400px;height:300px;border:10px solid #FFC4E1;background-color:#ffffff;">
        <form>
        <cfoutput>
        <input type="hidden" name="formID" value="#URL.fmid#" />
		<input type="hidden" name="signUpFormID" value="#URL.sfmid#" />
		</cfoutput>
        <div class="row">
          <div class="col-whole-right"><a href="javascript:xsite.closeHTMLDialogue();"><img src="images/imgClose.png" border="0" style="margin:-20px -20px 0px 0px;" /></a></div>
        </div>
        <div class="row">
          <div class="col-whole-left">
            <p>
            Please provide an e-mail address and a retrieve code that you will use to retrieve your data.
            After you click on the 'Save' button, an e-mail with the information for retrieving your saved data
            will be sending to this e-mail address.          
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-first-right">E-Mail Address:</div>
          <div class="col-second-left"><input type="text" name="em" size="30" maxlength="100"></div>
        </div>
        <div class="row">
          <div class="col-first-right">Retrieve Code:</div>
          <div class="col-second-left">
            <input type="text" name="rcode" size="12" maxlength="12"><br />
           (Any combination of numbers and letters between 2 and 12 characters.)
          </div>
        </div>
        <div class="row">
          <div class="col-first-right">&nbsp;</div>
          <div class="col-second-left"><input type="button" value="  Save  " onclick="saveForm(this)" class="button"></div>
        </div>
        </form>
      </div>
  </div>
</cfif>


</div><!--- #formLayout --->

</div>