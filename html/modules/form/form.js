  $(function() {
	$(".JQueryDate").datepicker();
  });

  function submitForm(form) {
    updateSpecialFields(form);
	if (validateForm(form)) return true;
	else return false;
  }
  
  function validateForm(form) {
    //validate="[fieldName],[1/0],[1/0],[Short Text],[Email],[Email],0";
    //validate="f#formItemID#,#mandatory#,#applyValidation#,#fieldType#,#fieldContext#,#dataType#,#numRequiredOptions#"
	var errorMessage="";
	$(".validate").each(function () {
	  var error = false;
	  var validateString=$(this).attr("validate");
	  $(this).removeClass("error");
	  var label=$(this).attr("label");
	  var items=validateString.split(",");
	  var fieldName, madatory, applyValidation, fieldType, fieldContext, dataType, numRequiredOptions;
	  if (items.length == 7) {
	    fieldName=items[0];
		madatory=items[1];
		applyValidation=items[2];
		fieldType=items[3];
		fieldContext=items[4];
		dataType=items[5];
		numRequiredOptions=parseInt(items[6]);
		if (madatory == "1") {		
	      if (fieldType == "Short Text" || fieldType == "Long Text") {
		    if (fieldContext == "Email") {
			  if (form.email.value == "") {
			    errorMessage = errorMessage + '\nThe field "' + label + '" can\'t be blank.';
				error = true;		  
			  }
			} else if (fieldContext == "Full Name") {
			  if (form.firstName.value == "" && form.lastName.value == "") {
			    errorMessage = errorMessage + '\nThe field "' + label + '" can\'t be blank.';
				error = true;		  
			  }
			} else {
			  if (form[fieldName].value == "") {
			    errorMessage = errorMessage + '\nThe field "' + label + '" can\'t be blank.';
				error = true;	
			  }
			}			
		  } else if (fieldType == "Check Boxes") {
		    if (numRequiredOptions <= 1) numRequiredOptions=1;
		    if (form[fieldName].length) {
			  var numChecked=0;
			  for (var i = 0; i < form[fieldName].length; i++) if (form[fieldName][i].checked) numChecked++;
			  if (numChecked < numRequiredOptions) {
			    if (numRequiredOptions == 1) {
				  errorMessage = errorMessage + '\nPlease check off at least one option for the field "' + label + '"';
				  error = true;
				} else {
				  errorMessage = errorMessage + '\nPlease check off at least ' + numRequiredOptions + ' options for the field "' + label + '"';
				  error = true;
				}
			  }			  
			} else {
			  if (!form[fieldName].checked) errorMessage = errorMessage + '\nPlease check off the field "' + label + '"';
			  error = true;
			}
		  } else if (fieldType == "Radio Group") {
		    if (form[fieldName].length) {
			  var checked=false;
			  for (var i = 0; i < form[fieldName].length; i++) if (form[fieldName][i].checked) checked=true;
			  if (!checked) {
			    errorMessage = errorMessage + '\nPlease select an option for the field "' + label + '"';
				error = true;
			  }
			} else {
			  if (!form[fieldName].checked) {
			    errorMessage = errorMessage + '\nPlease select an option for the field "' + label + '"';
				error = true;
			  }
			}
		  } else if (fieldType == "Drop-Down Menu") {
		    if (form[fieldName].options[form[fieldName].selectedIndex].value == "") {
			  errorMessage = errorMessage + '\nPlease select an option for the field "' + label + '"';
			  error = true;
			}
		  }
	    }
		if ((applyValidation == "1" && fieldType == "Short Text") || fieldContext == "Email") {
		  if (fieldContext == "Email" || dataType == "Email") { 
		    if (fieldContext == "Email") emailStr=form.email.value;
			else emailStr=form[fieldName].value;
		  
		    if (emailStr != "" && !xsite.checkEmail(emailStr)) {
			  errorMessage = errorMessage + '\nPlease enter a valid e-mail address for the field "' + label + '"';
			  error = true;
			}
		  } else if (dataType == "Date") {
			if (form[fieldName].value != "" && !xsite.checkDate(form[fieldName].value)) {
			  errorMessage = errorMessage + '\nPlease enter a valid date in a correct format for the field "' + label + '"';
			  error = true;
			}		
		  } else if (dataType == "Number") {
		    if (form[fieldName].value != "" && !xsite.checkNumber(form[fieldName].value)) {
			  errorMessage = errorMessage + '\nPlease enter a number in a correct format for the field "' + label + '"';
			  error = true;
			}
		  } else if (dataType == "Phone") {
		    if (form[fieldName].value != "" && !xsite.checkPhone(form[fieldName].value)) {
			  errorMessage = errorMessage + '\nPlease enter a phone number in a correct format for the field "' + label + '"';
			  error = true;
			}
		  } else if (dataType == "SSN") {
			if (form[fieldName].value != "" && !xsite.checkSSN(form[fieldName].value)) {
			  errorMessage = errorMessage + '\nPlease enter a SSN number in a correct format for the field "' + label + '"';
			  error = true;
			}
		  }
		}
		
		if (error) {
		  $(this).addClass("error");
		} 
	  }//items.length == 7
    });
	
	if (errorMessage != "") {
	  //hilight error fields
	  alert ("Please revisit the form fields that have been highlighted.\n-----------------\n" + errorMessage);
	  return false;		  
	} 
	
	if (form.listIDList) {// has email list sign-up
	  if (xsite.getCheckedValue(form.listIDList)) {
		if (form.email) {
	      if (!xsite.checkEmail(form.email.value)) {
			alert('Please enter a valid email address.');
			form.email.focus();
			return false;
		  }
		}
	  }
	}

    return true;
  }
  
  function updateSpecialFields(form) {
    if (form.isFirstPage) {
	  if (form.emailFieldID) {
	    //has special form item: email field
	    var formItemID=form.emailFieldID.value;
	    form['f' + formItemID].value = form.email.value;
	  }
	  if (form.nameFieldID) {
	    //has special form item: name field
	    var formItemID=form.nameFieldID.value;
	    form['f' + formItemID].value = form.firstName.value + ' ' + form.lastName.value;
	  }
	}
  }
  
  function saveForm(formItemObj) {
    var formObj1 = document.frmXForm;
	var formObj2 = formItemObj.form;
	var formID = formObj2.formID.value;
	var signUpFormID = formObj2.signUpFormID.value;
	var email = formObj2.em.value;
	var rcode = formObj2.rcode.value;
	
	if (!xsite.checkEmail(email)) {
	  alert('Please enter an valid e-mail address.');
	  formObj2.em.focus();
	  return;	  
	}
	
	if (rcode.length < 2) {
	  alert('Please enter a "retrieve code" with at least two characters.');
	  formObj2.rcode.focus();
	  return;	
	}

    formObj1.action = "action.cfm?md=form&task=saveForm&fmid=" + formID + "&sfmid=" + signUpFormID + "&em=" + escape(formObj2.em.value) + "&rcode=" + escape(formObj2.rcode.value);
	updateSpecialFields(formObj1);
	formObj1.submit();
  }

  function retrieveForm(formItemObj) {
	var formObj = formItemObj.form;
	var email = formObj.em.value;
	var rcode = formObj.rcode.value;
	
	if (!xsite.checkEmail(email)) {
	  alert('Please enter an valid e-mail address.');
	  formObj.em.focus();
	  return;	  
	}
	
	if (rcode.length < 2) {
	  alert('Please enter your "retrieve code".');
	  formObj.rcode.focus();
	  return;	
	}
	
	formObj.submit();
  }
  
  function retrieveCode(formItemObj) {
	var formObj = formItemObj.form;
	if (!xsite.checkEmail(formObj.em.value)) {
	  alert ("Please enter a valid e-mail address.");
	  formObj.em.focus();
	  return;
	}
	formObj.submit();
  }
  
  
  
  