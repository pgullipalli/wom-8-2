<!---<div class="floatRightFullWdth"><cfoutput>#Request.globalProperties.footer#</cfoutput></div>
<div class="floatRightFullWdth">
  <a href="http://www.covalentlogic.com" target="_blank" onmouseover="MM_swapImage('btnSiteCredits','','images/btnSiteCreditso.gif',1)" onmouseout="MM_swapImgRestore()"><img src="images/btnSiteCredits.gif" alt="Site Designed and Developed by Covalent Logic" name="btnSiteCredits" width="107" height="19" border="0" id="btnSiteCredits" /></a>
</div>--->
<cfscript>
// Change the navigation ID in place of 247 
subNavs=NV.getSubNavsByParentNavID(navGroupID=1, parentNavID=261);
navStruct=StructNew();
for (i=1; i<=subNavs.recordcount; i++) {
	navKey = "nav" & subNavs.navID[i];
	navStruct[navKey]=subNavs.navName[i];
  }
</cfscript>

<div class="footer-holder">
    <div class="footer-frame">
        <div class="box">
            <h3>Our Services</h3>
            <ul class="sub-nav"><cfoutput query="subNavs"><li><a href="#href#">#navName#</a></li></cfoutput>
                <!--<li><a href="#">Pregnancy &amp; Childbirth</a></li>
                <li><a href="#">Breast Care</a></li>
                <li><a href="#">Gynecological Care</a></li>
                <li><a href="#">Weight Loss</a></li>
                <li><a href="#">Pediatrics</a></li>
                <li><a href="#">Diabetes</a></li>
                <li><a href="#">Therapy</a></li>
                <li><a href="#">Surgery</a></li>-->
            </ul>
        </div>
        <div class="box">
            <div class="social">
                <h3>Connect with Us</h3>
                <ul>
                    <li class="facebook">
                        <div>
                            <a href="http://www.facebook.com/pages/Womans-Hospital/131273032914"><span>Facebook</span></a>
                        </div>
                    </li>

<!-- <li class="wordpress">
                        <div>
                            <a href="#"><span>WordPress</span></a>
                        </div>
                    </li>-->

<li class="twitter">
                        <div>
                            <a href="http://twitter.com/#!/WomansHospital"><span>Twitter</span></a>
                        </div>
                    </li>
<li class="youtube">
                        <div>
                            <a href="http://www.youtube.com/user/WomansHospitalBR"><span>YouTube</span></a>
                        </div>
                    </li>
<li class="mywomanschannel">
                        <div>
                            <a href="http://www.womans.org/index.cfm?md=newsroom&tmp=detail&articleID=148&nid=110&tid=2"><span>myWoman's Channel</span></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box">
            <strong class="logo"><a href="http://www.womans.org/"><img src="assets/imagesNew/img8.gif" width="84" height="43" alt="image description" /></a></strong>
            <div class="address">
                <address>
                    <span>Woman's Hospital Main Campus</span>
                    <span>9050 Airline Highway</span>
                    <span>Baton Rouge, LA 70815</span>
                </address>
                <dl>
                    <dt>Main Number:</dt>
                    <dd>225.927.1300</dd>
                    <dt>Patient Rooms:</dt>
                    <dd>225.231.5 [+] room number</dd>
                    <dt>Patient room Info:</dt>
                    <dd>225.924.8157</dd>
                </dl>
                <ul class="add-nav">
                    <li><a href="http://womans.org/index.cfm?md=pagebuilder&tmp=home&pid=288">Terms &amp; Conditions</a></li>
                    <li><a href="http://womans.org/index.cfm?md=pagebuilder&tmp=home&pid=287">Privacy Policy</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>