<cfscript>
  HP=CreateObject("component","com.Homepage").init();
  CL=CreateObject("component","com.Class").init();
  NR=CreateObject("component","com.Newsroom").init();
  homepageInfo=HP.getHomepageInfo();
  featureClass1=CL.getFeatureClassByCategoryID(homepageInfo.classCategoryID1);
  featureClass2=CL.getFeatureClassByCategoryID(homepageInfo.classCategoryID2);
  featureClass3=CL.getFeatureClassByCategoryID(homepageInfo.classCategoryID3);
  featureArticle1=NR.getFeatureArticleByCategoryID(homepageInfo.articleCategoryID1);
  featureArticle2=NR.getFeatureArticleByCategoryID(homepageInfo.articleCategoryID2);
  featureArticle3=NR.getFeatureArticleByCategoryID(homepageInfo.articleCategoryID3);
</cfscript>
<!--Pod Header-->

<div class="boxes">
<cfoutput>
  <ul class="boxes-blocks">
    <li> <a href="index.cfm?md=homepage&amp;tmp=subhome&amp;shpid=#homepageInfo.subHomePageID1#"> <img src="#homepageInfo.podImage1#" width="223" height="152" alt="image description" /> <span><img src="#homepageInfo.podImageOver1#" width="223" height="152" alt="#homepageInfo.podImageAltText1#" /></span> </a> </li>
    <li> <a href="index.cfm?md=homepage&amp;tmp=subhome&amp;shpid=#homepageInfo.subHomePageID2#"> <img src="#homepageInfo.podImage2#" width="223" height="152" alt="image description" /> <span><img src="#homepageInfo.podImageOver2#" width="223" height="152" alt="#homepageInfo.podImageAltText2#" /></span> </a> </li>
    <li> <a href="index.cfm?md=homepage&amp;tmp=subhome&amp;shpid=#homepageInfo.subHomePageID3#"> <img src="#homepageInfo.podImage3#" width="223" height="152" alt="image description" /> <span><img src="#homepageInfo.podImageOver3#" width="223" height="152" alt="#homepageInfo.podImageAltText3#" /></span> </a> </li>
  </ul>
</cfoutput>

<div class="boxes-holder">
<cfoutput>
<!--First BOx Start-->
<!---<cfif #featureClass1.startDateTime# neq "">
	<cfset classTime=TimeFormat(featureClass1.startDateTime,"h:mm tt")>
    <cfset classTime=Replace(Replace(classTime,"AM","am"),"PM","pm")>
<cfelse>
    <cfset classTime=#featureClass1.startDateTime#)>
</cfif>	--->	
<div class="box">
	<cfif Compare(featureClass1.courseTitle,"")>
  	<div class="title-block">
        <div class="date">
          <p>#DateFormat(featureClass1.startDateTime,"mmm")#</p>
          <span>#DateFormat(featureClass1.startDateTime,"dd")#</span> 
        </div>
        <cfif Compare(featureClass1.startDateTime,"")>
        	<cfset classTime=TimeFormat(featureClass1.startDateTime,"h:mm tt")>
          	<cfset classTime=Replace(Replace(classTime,"AM","am"),"PM","pm")>
        <cfelse>
        	<cfset classTime = "">
        </cfif>
        <div class="title-block-holder">
              <h2><a href="index.cfm?md=class&amp;tmp=detail&amp;cid=#featureClass1.courseID#&amp;tid=#Application.template.templateID_Class#">#Replace(featureClass1.courseTitle,"& ","&amp; ","All")#</a></h2>
              <p>#classTime#</p>
              <div class="view"> <a href="index.cfm?md=class&amp;tmp=category&amp;catid=#homepageInfo.classCategoryID1#">View All Classes</a> </div>
        </div>
  	</div>
    <cfelse>
    	<div class="title-block">
            <div class="date">
              <p>&nbsp;</p>
              <span>&nbsp;</span> 
            </div>
        <div class="title-block-holder">
              <h2>&nbsp;</h2>
              <p>&nbsp;</p>
              <div class="view"> <a href="index.cfm?md=class&amp;tmp=category&amp;catid=#homepageInfo.classCategoryID1#">View All Classes</a> </div>
        </div>
  	</div>
  </cfif>
  <cfif Compare(featureArticle1.headline,"")>
    <cfif Find("index.cfm?",featureArticle1.href) GT 0 And Find("tid=",featureArticle1.href) Is 0>
      <cfset artURL=featureArticle1.href & "&amp;tid=#Application.template.templateID_SubHome1#">
      <cfelse>
      <cfset artURL=featureArticle1.href>
    </cfif>
    <h3><a href="#artURL#" <cfif featureArticle1.popup>target="_blank"</cfif>>#featureArticle1.headline#</a></h3>
    <!---<h3><a href="#artURL#" >#featureArticle1.headline#</a></h3>--->
    <p>#Replace(featureArticle1.teaser,"& ","&amp; ","All")#</p>
    <cfelse>
    <p>&nbsp;</p>
  </cfif>
</div>
<!--First BOx End-->

<!--Second BOx Start-->
<div class="box">
<cfif Compare(featureClass2.courseTitle,"")>
  	<div class="title-block">
        <div class="date">
          <p>#DateFormat(featureClass2.startDateTime,"mmm")#</p>
          <span>#DateFormat(featureClass2.startDateTime,"dd")#</span> 
        </div>
        <cfif Compare(featureClass2.startDateTime,"")>
        	<cfset classTime=TimeFormat(featureClass2.startDateTime,"h:mm tt")>
          	<cfset classTime=Replace(Replace(classTime,"AM","am"),"PM","pm")>
        <cfelse>
        	<cfset classTime = "">
        </cfif>



        <div class="title-block-holder">
              <h2><a href="index.cfm?md=class&amp;tmp=detail&amp;cid=#featureClass2.courseID#&amp;tid=#Application.template.templateID_Class#">#Replace(featureClass2.courseTitle,"& ","&amp; ","All")#</a></h2>
              <p>#classTime#</p>
              <div class="view"> <a href="index.cfm?md=class&amp;tmp=category&amp;catid=#homepageInfo.classCategoryID2#">View All Classes</a> </div>
        </div>
  	</div>
    <cfelse>
    	<div class="title-block">
            <div class="date">
              <p>&nbsp;</p>
              <span>&nbsp;</span> 
            </div>
        <div class="title-block-holder">
              <h2>&nbsp;</h2>
              <p>&nbsp;</p>
              <div class="view"> <a href="index.cfm?md=class&amp;tmp=category&amp;catid=#homepageInfo.classCategoryID2#">View All Classes</a> </div>
        </div>
  	</div>
  </cfif>
  <cfif Compare(featureArticle2.headline,"")>
    <cfif Find("index.cfm?",featureArticle2.href) GT 0 And Find("tid=",featureArticle2.href) Is 0>
      <cfset artURL=featureArticle2.href & "&amp;tid=#Application.template.templateID_SubHome2#">
      <cfelse>
      <cfset artURL=featureArticle2.href>
    </cfif>
    <h3><a href="#artURL#" <cfif featureArticle2.popup>target="_blank"</cfif>>#featureArticle2.headline#</a></h3>
   <!--- <h3><a href="#artURL#" >#featureArticle2.headline#</a></h3>--->
    <p>#Replace(featureArticle2.teaser,"& ","&amp; ","All")#</p>
    <cfelse>
    <p>&nbsp;</p>
  </cfif>
</div>
<!--Second BOx End-->

<!--Third BOx Start-->

<div class="box">
  <cfif Compare(featureClass3.courseTitle,"")>
  	<div class="title-block">
        <div class="date">
          <p>#DateFormat(featureClass3.startDateTime,"mmm")#</p>
          <span>#DateFormat(featureClass3.startDateTime,"dd")#</span> 
        </div>
        <cfif Compare(featureClass3.startDateTime,"")>
        	<cfset classTime=TimeFormat(featureClass3.startDateTime,"h:mm tt")>
          	<cfset classTime=Replace(Replace(classTime,"AM","am"),"PM","pm")>
        <cfelse>
        	<cfset classTime = "">
        </cfif>
        <div class="title-block-holder">
              <h2><a href="index.cfm?md=class&amp;tmp=detail&amp;cid=#featureClass3.courseID#&amp;tid=#Application.template.templateID_Class#">#Replace(featureClass3.courseTitle,"& ","&amp; ","All")#</a></h2>
              <p>#classTime#</p>
              <div class="view"> <a href="index.cfm?md=class&amp;tmp=category&amp;catid=#homepageInfo.classCategoryID3#">View All Classes</a> </div>
        </div>
  	</div>
    <cfelse>
    	<div class="title-block">
            <div class="date">
              <p>&nbsp;</p>
              <span>&nbsp;</span> 
            </div>
        <div class="title-block-holder">
              <h2>&nbsp;</h2>
              <p>&nbsp;</p>
              <div class="view"> <a href="index.cfm?md=class&amp;tmp=category&amp;catid=#homepageInfo.classCategoryID3#">View All Classes</a> </div>
        </div>
  	</div>
  </cfif>
 <cfif Compare(featureArticle3.headline,"")>
    <cfif Find("index.cfm?",featureArticle3.href) GT 0 And Find("tid=",featureArticle3.href) Is 0>
      <cfset artURL=featureArticle3.href & "&amp;tid=#Application.template.templateID_SubHome2#">
      <cfelse>
      <cfset artURL=featureArticle3.href>
    </cfif>
    <h3><a href="#artURL#" <cfif featureArticle3.popup>target="_blank"</cfif>>#featureArticle3.headline#</a></h3>
    <!---<h3><a href="#artURL#" >#featureArticle3.headline#</a></h3>--->
    <p>#Replace(featureArticle3.teaser,"& ","&amp; ","All")#</p>
    <cfelse>
    <p>&nbsp;</p>
  </cfif>
 
</div>
<!--Third BOx End-->

</cfoutput>

</div>
</div>

