<cfparam name="URL.shpid"><!--- sub-home page id --->
<!--- pc: Pregancy & Childbirth, wp: Wellness & Prevention, tc: Treatment & Care --->
<cfscript>
  HP=CreateObject("component","com.Homepage").init();
  CL=CreateObject("component","com.Class").init();
  NR=CreateObject("component","com.Newsroom").init();
  PG=CreateObject("component","com.PhotoGallery").init();
  subHomePageInfo=HP.getSubHomePage(URL.shpid);
  
  if (subHomePageInfo.recordcount GT 0) {
    news=NR.getMostRecentArticlesByCategoryID(subHomePageInfo.articleCategoryID, 3);
    if (subHomePageInfo.showFeatureClass And subHomePageInfo.classCategoryID GT 0) {
      featureClass=CL.getFeatureClassByCategoryID(subHomePageInfo.classCategoryID);
    }
    if (subHomePageInfo.showFeatureAlbum And subHomePageInfo.albumCategoryID GT 0) {
      featureAlbum=PG.getFeatureAlbumByCategoryID(subHomePageInfo.albumCategoryID);
    }
  }
  
  if (Not Compare(URL.tid,"0")) URL.tid=Request.template.templateID;
  if (Not Compare(URL.nid,"0")) URL.nid=Request.template.navID;
</cfscript>

<div class="contentWrapperSbhm">
<cfif subHomePageInfo.recordcount GT 0>
  <!--INTRO-->
  <div class="intro"><cfoutput>#subHomePageInfo.welcomeMessage#</cfoutput></div>
    
    <!--NEWS-->
    <cfif Not subHomePageInfo.showFeatureClass OR subHomePageInfo.classCategoryID IS 0>
    <div id="news" style="float:left; width:420px;">
    <cfelse>
    <div class="contentNestedLft" id="news">
    </cfif>
      <img src="images/imgSbhmTabNews.gif" alt="News" width="111" height="18" />
      <div class="content">
        <cfoutput query="news">
        <cfif FindNoCase("index.cfm?",href) GT 0 And FindNoCase("tid=",href) Is 0 And FindNoCase("nid=",href) Is 0><cfset href2=href & "&nid=#URL.nid#&tid=#URL.tid#"><cfelse><cfset href2=href></cfif>
        <p><a href="#href2#" <cfif popup>target="_blank"</cfif> class="lnkHead">#headline#</a> #teaser#</p>
        </cfoutput>        
      </div>
    </div>
    
    <cfif subHomePageInfo.showFeatureClass And subHomePageInfo.classCategoryID GT 0>
    <!--EVENTS-->
    <div class="contentNestedRt">
      <img src="images/imgSbhmTabFeatured.gif" alt="Featured" width="177" height="18" /><br />
      <div class="featuredItem">
      <cfoutput>#subHomePageInfo.featureClassIntro#</cfoutput>
      <br /><br />    
      <cfoutput><a href="index.cfm?md=class&tmp=category&catid=#subHomePageInfo.classCategoryID#" class="lnkAccent01SmCaps">View Classes &amp; Programs &raquo;</a></cfoutput>
      </div>
      
        
      <cfif Compare(featureClass.courseTitle,"")>
        <img src="images/imgSbhmTabUpcoming.gif" alt="Upcoming" width="177" height="22" /><div class="upcomingItem"><div class="featEvent">
            <div class="eventDate">
              <div class="monthAbbr"><cfoutput>#DateFormat(featureClass.startDateTime,"mmm")#</cfoutput></div>
              <div class="dayNumber"><cfoutput>#DateFormat(featureClass.startDateTime,"dd")#</cfoutput></div>
            </div>
            <cfset classTime=TimeFormat(featureClass.startDateTime,"h:mm tt")>
      	    <cfset classTime=Replace(Replace(classTime,"AM","am"),"PM","pm")>
            <div class="eventDescrip"><cfoutput><a href="index.cfm?md=class&tmp=detail&cid=#featureClass.courseID#&nid=#URL.nid#&tid=#URL.tid#" class="lnkEventName">#featureClass.courseTitle#</a> #classTime#</cfoutput></div>
            <div class="clearLeft"></div>
          </div>
          <cfoutput>#featureClass.shortDescription#</cfoutput>
        </div><!--- upcomingItem --->     
        <img src="images/imgSbhmRtPodFtr.gif" width="177" height="18" /></cfif></div><!--- contentNestedRt --->                
    </cfif>
      
  
  <!--GALLERY-->
  <cfif subHomePageInfo.showFeatureAlbum And subHomePageInfo.albumCategoryID GT 0 And Compare(featureAlbum.albumTitle,"")>
  <cfoutput>
  <div class="floatLeftFullWdth"><img src="images/imgSbhmTabGallery.gif" alt="Gallery" width="111" height="18" /></div>
  <div id="gallery">
    <a href="index.cfm?md=photogallery&tmp=album_html&albumID=#featureAlbum.albumID#&nid=#URL.nid#&tid=#URL.tid#"><img src="#featureAlbum.coverImage#" width="160" height="138" border="0" class="floatLeft" /></a>
    <div class="content">
      <a href="index.cfm?md=photogallery&tmp=album_html&albumID=#featureAlbum.albumID#&nid=#URL.nid#&tid=#URL.tid#" class="lnkHead">#featureAlbum.albumTitle#</a>
      #featureAlbum.description#
    </div>
    <img src="images/imgGallRtCap.gif" width="9" height="138" class="floatLeft" /></div>
  </cfoutput>
  </cfif>
</cfif>
</div>