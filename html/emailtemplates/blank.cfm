<cfparam name="Form.emailBodyHTML" default="">
<cfparam name="Form.messageID" default="0">
<cfoutput>
<html>
<head>
<title>Woman's Hospital</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="scripts/css/styles_for_emailbody.css" rel="stylesheet" type="text/css" />
</head>
<body>

<div id="mailHTMLContent" style="font-family:Arial; font-size:12px; color:##000000">
#Form.emailBodyHTML#
</div>

<br>
<p align="center">
<cfif Form.messageID IS 0>
	<span style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:##333333; line-height:105%;">
	<b>PROFILE OPTIONS: </b>
	<a href="" style="color:##333333; text-decoration:underline;">Subscribe</a> |
	<a href="" style="color:##333333; text-decoration:underline;">Unsubscribe</a> |
	<a href="" style="color:##333333; text-decoration:underline;">Archives</a><br>
	-----------------------------------------------------------------------------------------------------
	<br>
	If you are having trouble viewing this message, please <a href="" style="color:##333333; text-decoration:underline;">click here</a>.</span><br>
<cfelse>
	<span style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:##333333; line-height:105%;">
	<b>PROFILE OPTIONS: </b>
	<a href="#Application.siteURLRoot#/index.cfm?md=communication&amp;tmp=signup&amp;msgid=#Form.messageID#" style="color:##333333; text-decoration:underline;">Subscribe</a> |
	<a href="#Application.siteURLRoot#/index.cfm?md=communication&amp;tmp=updateprofile&amp;msgid=#Form.messageID#&amp;uid=##emailID##" style="color:##333333; text-decoration:underline;">Unsubscribe</a> |
	<a href="#Application.siteURLRoot#/index.cfm?md=communication&amp;tmp=viewarchive&amp;uid=##emailID##" style="color:##333333; text-decoration:underline;">Archives</a><br>
	-----------------------------------------------------------------------------------------------------
	<br>
	If you are having trouble viewing this message, please <a href="#Application.siteURLRoot#/index.cfm?md=communication&amp;tmp=viewcampaign&amp;nowrap=1&amp;msgid=#Form.messageID#&amp;uid=##emailID##" style="color:##333333; text-decoration:underline;">click here</a>.</span><br>
	<img src="#Application.siteURLRoot#/action.cfm?md=communication&amp;task=addMailOpenCount&amp;msgid=#Form.messageID#&amp;uid=##emailID##" height="1" width="1" border="0">
</cfif>
</p>
<br>

</body>
</html>
</cfoutput> 