﻿<cfparam name="Form.emailBodyHTML" default="">
<cfparam name="Form.messageID" default="0">
<cfset imgDir="#Application.siteURLRoot#/emailtemplates/images">
<cffile action="read" file="#Application.communication.emailBodyStyleSheetPath#" variable="emailStyle">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Woman's Hospital | exceptional care, centered on you</title>
<style type="text/css">
<!--
body { margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; background-color: #d8dfde; }
<cfoutput>#emailStyle#</cfoutput>
-->
</style></head>

<body>
<cfoutput>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top" bgcolor="##d8dfde">
      <table width="580" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="147" align="center" valign="top"><a href="http://www.womans.org" target="_blank"><img src="#imgDir#/imgHeader.gif" alt="myWoman's E-Newsletter" width="580" height="147" border="0" style="display:block;" /></a></td>
        </tr>
      </table>      
      <table width="580" border="0" cellspacing="0" cellpadding="0" style="background-color:##FFF;">
        <tr>
          <td width="59" align="left" valign="top"><img src="#imgDir#/imgSpacer.gif" width="59" height="1" style="display:block;" /></td>
          <td align="left" valign="top" id="mailHTMLContent" style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:11px; color:##333132; line-height:120%;">
			#Form.emailBodyHTML#
		  </td>
          <td width="59" align="left" valign="top"><img src="#imgDir#/imgSpacer.gif" width="59" height="1" style="display:block;" /></td>
        </tr>
        <tr>
          <td height="40" align="left" valign="top"><img src="#imgDir#/imgSpacer.gif" width="1" height="40" style="display:block;" /></td>
          <td align="left" valign="top" style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:11px; color:##333132; line-height:120%;">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
      </table>     
      <!--FOOTER-->
      <table width="580" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="14" align="center" valign="top"><img src="#imgDir#/imgFooter.gif" width="580" height="14" style="display:block;" /></td>
        </tr>
        <tr>
          <td height="50" align="center" valign="top" style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:11px; color:##888c8b; line-height:130%;"><br /><br />
            <span style="color:##828685;"><b>WOMAN'S HOSPITAL</b> |  9050 Airline Highway  |  Baton Rouge, LA 70815  | <a href="http://www.womans.org" target="_blank" style="color:##828685; text-decoration:none;">www.womans.org</a></span> <br />
            ----------------------------------------------------------------------------------------------------------------------------<br />
            <span style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:##828685; line-height:120%;">
              <span style="color:##828685; line-height:105%;"><b>PROFILE OPTIONS: </b>
              <cfif Form.messageID IS 0>
              <a href="" style="color:##828685; text-decoration:underline;">Subscribe</a> |
              <a href="" style="color:##828685; text-decoration:underline;">Unsubscribe</a> |
              <a href="" style="color:##828685; text-decoration:underline;">Archives</a><br />
              If you are having trouble viewing this message, please <a href="" style="color:##828685; text-decoration:underline;">click here</a>.
              <cfelse>
              <a href="#Application.siteURLRoot#/index.cfm?md=communication&tmp=signup&msgid=#Form.messageID#&uid=##emailID##" style="color:##828685; text-decoration:underline;">Subscribe</a> |
              <a href="#Application.siteURLRoot#/index.cfm?md=communication&tmp=updateprofile&msgid=#Form.messageID#&uid=##emailID##" style="color:##828685; text-decoration:underline;">Unsubscribe</a> |
              <a href="#Application.siteURLRoot#/index.cfm?md=communication&tmp=viewarchive&uid=##emailID##" style="color:##828685; text-decoration:underline;">Archives</a><br />
              If you are having trouble viewing this message, please <a href="#Application.siteURLRoot#/index.cfm?md=communication&tmp=viewcampaign&nowrap=1&msgid=#Form.messageID#&uid=##emailID##" style="color:##828685; text-decoration:underline;">click here</a>.  
              <img src="#Application.siteURLRoot#/action.cfm?md=communication&task=addMailOpenCount&msgid=#Form.messageID#&uid=##emailID##" height="1" width="1" border="0">            
              </cfif>
              </span>
            </span><br /><br /><br /></td>
        </tr>
    </table></td>
  </tr>
</table>
</cfoutput>
</body>
</html>
