<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Status 404</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
html,body {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color:#FFFFFF;
	line-height:160%;
	margin: 0px;
	padding:0px;
	background-color:#324352;
	height: 100%;
}
.main {
	margin: auto;
	height: 100%;
	width: 100%;
}
-->
</style>

<script type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}  
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body onLoad="MM_preloadImages('/images/btnSubmito.gif'); if (document.form1) document.form1.visitorName.focus();">
<table width="100%" height="100%"  border="0" cellpadding="0" cellspacing="0" style="vertical-align:middle;" class="main">
  <tr>
    <td align="center" valign="middle">
      <table width="442" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="52" align="center" valign="top"><img src="/images/imgHdr.gif" alt="Page Not Found" width="442" height="52"></td>
        </tr>
        <tr>
          <td align="left" valign="top" background="/images/bkgMain404.gif">
		  <div style="padding:60px;">
		    <cfif IsDefined("FORM.visitorName")>
			  <p>Thank you for reporting the error!</p>
			  <p>Please <a href="/" style="color:#FFFFFF; text-decoration:underline;">click here</a> to go back to the home page.</p>
			  
			  <cftry>
			    <cfif Compare(FORM.visitorEmail, "")>
				  <cfset fromAddress=FORM.visitorEmail>
				<cfelse>
				  <cfset fromAddress=APPLICATION.errorRecipient>
				</cfif>
			    <cfmail from="#fromAddress#" to="#APPLICATION.errorRecipient#" subject="Error Occured - #CGI.HTTP_HOST#"  type="html">
				  <cfmailpart type="text">
					An error occured at #DateFormat(now(), "mm/dd/yyyy")# #TimeFormat(now(), "h:mm:ss tt")# on #CGI.HTTP_HOST#
					Visitor name: #FORM.visitorName#
					Visitor e-mail: #FORM.visitorEmail#
					What page is clicked on: #FORM.pageClicked#
				  </cfmailpart>
				  <cfmailpart type="html">
					<p>An error occured at #DateFormat(now(), "mm/dd/yyyy")# #TimeFormat(now(), "h:mm:ss tt")# on #CGI.HTTP_HOST#</p>
					<p>Visitor name: #FORM.visitorName#</p>
					<p>Visitor e-mail: #FORM.visitorEmail#</p>
					<p>What page is clicked on: #FORM.pageClicked#<p>
				  </cfmailpart>
			    </cfmail>				 
			  <cfcatch></cfcatch>
			  </cftry>			  
		    <cfelse>		  
			  <p>
			  You've reached a page that is not currently available on {this site}.
			  Please visit the <a href="/" style="color:#FFFFFF; text-decoration:underline;">home page</a> and
			  attempt to find the page again.
			  </p>
			  <p>If you would like to report the error, please let us know by using the form below:</p><br>
			  <center>
			  <form name="form1" action="404error.cfm" method="post">
			  <table border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td align="left" valign="top">
				  <b>Name:</b><br>
				  <input type="text" name="visitorName" style="width:180px"><br><br>
				  <b>Email Address:</b><br>
				  <input type="text" name="visitorEmail" style="width:180px"><br><br>
				  <b>What You Clicked On To Get Here:</b><br>
				  <input type="text" name="pageClicked" style="width:180px"><br><br>
				  <input type="image" src="/images/btnSubmit.gif" alt="Submit" name="btnSubmit" width="70" height="24" border="0" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnSubmit','','/images/btnSubmito.gif',1)">		  
				  </td>
				</tr>
			  </table>
			  </form>
			  </center>
		    </cfif>
		  </div>
		  </td>
        </tr>
        <tr>
          <td height="50" align="center" valign="top"><img src="/images/imgFtr.gif" alt="Covalent Logic" width="442" height="50"></td>
        </tr>
      </table>
	</td>
  </tr>
</table>

</body>
</html>
