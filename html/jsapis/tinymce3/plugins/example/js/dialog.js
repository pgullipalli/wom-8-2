tinyMCEPopup.requireLangPack();

var ExampleDialog = {

	init : function() {

	},

	insert : function() {

	var float_val = document.forms[0].float.value;
	var url_val = document.forms[0].url.value;

	var top_header = "<div id='" + float_val + "'><a class='cta' href='"+ url_val +"'><div id='cta-pinkbox'>";

	var p1a = "<p id='cta-p1a'>" + document.forms[0].title.value + "</p>";

	var p1b = "<p id='cta-p1b'>" + document.forms[0].description.value + "</p>";

	var p2 = "<p id='cta-p2'>" + document.forms[0].boldtext.value + "</p>";

	var top_footer = "</div></a>";

	var bottom_header = "<div id='cta-whitebox'>";

	var p3 = "<p id='cta-p3'>&ldquo;" + document.forms[0].caption.value + "&rdquo;</p>";

	var bottom_footer = "</div></div>";

	var the_text = top_header + p1a + p1b + p2 + top_footer + bottom_header + p3 + bottom_footer;

	tinyMCEPopup.editor.execCommand('mceInsertContent', false, the_text);

	tinyMCEPopup.close();
	
        }
};

tinyMCEPopup.onInit.add(ExampleDialog.init, ExampleDialog);
