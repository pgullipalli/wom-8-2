//JQuery and JQuery.xsite  must be available before this script
<cfcontent type="application/x-javascript" reset="no">
<cfsilent>
<cfparam name="URL.windowWidth" type="numeric" default="750">
<cfparam name="URL.windowHeight" type="numeric" default="600">
<cfparam name="URL.tinyMCEInstallationURL" type="string" default="#Application.tinyMCEInstallationURL#">
<cfparam name="URL.theme" type="string" default="advanced">
<cfif Not Compare(URL.theme, "advanced")>
  <cfparam name="URL.siteURLRoot" type="string" default="#Application.siteURLRoot#">
  <cfparam name="URL.stylesheetURL" type="string">
  <cfparam name="URL.assetRelativeDirectory" type="string">
  <cfparam name="URL.currentRelativeDirectory" type="string" default="">
</cfif>
<cfparam name="URL.templateListURL" type="string" default="#URL.tinyMCEInstallationURL#/extension/lists/template_list.js">
<cfparam name="URL.linkListURL" type="string" default="">
<cfparam name="URL.imageListURL" type="string" default="">
<cfparam name="URL.fileBrowserWidth" type="numeric" default="550">
<cfparam name="URL.fileBrowserHeight" type="numeric" default="500">
<cfparam name="URL.useAbsoluteURL" type="boolean" default="true">
<cfparam name="URL.forceBrNewlines" type="boolean" default="true">
<cfparam name="URL.replaceTokens" type="string" default="">
<cfparam name="URL.replaceValues" type="string" default="">
</cfsilent>

<cfoutput>
<cfif Compare(URL.theme, "advanced")>
  function setupTinyMCEEditor () {
	 tinyMCE.init({
		mode : "textareas",			
		editor_selector : "mceEditor",
		theme : "simple",
		oninit : "loadPageContent"			
	 });
<cfelse>   
  function setupTinyMCEEditor () { 
	  tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,table,advhr,advimage,advlink,searchreplace,contextmenu,paste,fullscreen,noneditable,visualchars,template,inlinepopups",
		<cfif URL.useAbsoluteURL>
		relative_urls : false,	// with absolute URLs and 		
		remove_script_host : false, //including domain on links and images
		<cfelse>
		relative_urls : true,
		remove_script_host : true,
		</cfif>
		accessibility_warnings: false,
		<cfif URL.forceBrNewlines>
		force_br_newlines : true,
		force_p_newlines : false,
		forced_root_block : '' ,
	    </cfif>
		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,backcolor,|,styleselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,|,removeformat,cleanup,help,code",
		theme_advanced_buttons3 : "tablecontrols,|,sub,sup,|,hr,advhr,charmap,template,|,visualaid,visualchars,fullscreen",
	
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,    
		
		// Drop lists for link/image/media/template dialogs
		<cfif Compare(URL.templateListURL, "")>
		template_external_list_url : "#URL.templateListURL#",
		</cfif>
		<cfif Compare(URL.linkListURL, "")>
		external_link_list_url : "#URL.linkListURL#",
		</cfif>
		<cfif Compare(URL.imageListURL, "")>
		external_image_list_url : "#URL.imageListURL#",
		</cfif>
	
		// stylesheet URL
		<cfif Compare(URL.stylesheetURL, "")>
		content_css : "#URL.stylesheetURL#",
		</cfif>
		editor_selector : "mceEditor",
	
		<cfset tokens=ArrayNew(1)>
		<cfset values=ArrayNew(1)>
		<cfif Compare(URL.replaceTokens, "") AND Compare(URL.replaceValues, "")>
		  <cfset tokens=ListToArray(URL.replaceTokens, "|")>
		  <cfset values=ListToArray(URL.replaceValues, "|")>		  
		</cfif>		
		
		<cfif ArrayLen(tokens) IS ArrayLen(values)>
		  <cfif ListFind(URL.replaceTokens, "templateURLRoot", "|") IS 0>	
		    <cfset tokens[ArrayLen(tokens)+1]="templateURLRoot">
		    <cfset values[ArrayLen(values)+1]="#URL.tinyMCEInstallationURL#/extension/templates">
		  </cfif>		
		  template_replace_values : {
		    <cfloop index="idx" from="1" to="#ArrayLen(tokens)#">
			  <cfif idx GT 1>,</cfif>#tokens[idx]#: "#values[idx]#"
			</cfloop>
		  },
		</cfif>
			
		file_browser_callback : 'myFileBrowser',
		oninit : "loadPageContent"
	  });
  }

  function myFileBrowser (field_name, url, type, win) {      
	  var cmsURL = "#URL.tinyMCEInstallationURL#/extension/filebrowser/filebrowser.cfm?type=" + type + 
				   "&siteURLRoot=#URLEncodedFormat(URL.siteURLRoot)#" +
				   "&assetRelativeDirectory=#URLEncodedFormat(URL.assetRelativeDirectory)#" +
				   "&currentRelativeDirectory=#URLEncodedFormat(URL.currentRelativeDirectory)#" +
				   "&uuid=" + (new Date()).getTime();
	
	  tinyMCE.activeEditor.windowManager.open({
			file : cmsURL,
			title : "File Browser",
			width : #URL.fileBrowserWidth#,  // Your dimensions may differ - toy around with them!
			height : #URL.fileBrowserHeight#,
			resizable : "yes",
			inline : "yes",  // This parameter only has an effect if you use the inlinepopups plugin!
			close_previous : "no"
	  }, {
			window : win,
			input : field_name
	  });
	  return false;
  }    
</cfif>

<!--- From this point, it's assumed that the JQuery APIs all have been imported --->

var htmlSourceFieldObj;

function loadWYSIWYGEditor (sourceObj) {
  htmlSourceFieldObj=sourceObj;
  $("##winWYSIWYG").dialog("open");
  
  setTimeout('setupTinyMCEEditor()', 2000);
}  

function loadPageContent() {
  var ed = tinyMCE.get("tinyMCETextArea"); 
  if (ed) {
    ed.setContent(htmlSourceFieldObj.value);
  } else {    
    alert("Sorry, there was an error while loading the WYSIWYG editor.");
  }
}  

function saveContent() {
  var ed = tinyMCE.get("tinyMCETextArea"); 
  if (ed) {
    htmlSourceFieldObj.value = ed.getContent();
  } else {    
    alert("Sorry, there was an error while saving your content. To prevent from losing your valuable content, please cut the content and manually paste on other text editor.");
  }
  $("##winWYSIWYG").dialog("close");
}

var windowContent='<form id="frmWYSIWYG" name="frmWYSIWYG">' +
	'<textarea name="tinyMCETextArea" id="tinyMCETextArea" class="mceEditor" style="width:100%;height:480px;"></textarea><br /><a href="javascript:loadPageContent()">Load WYSIWYG</a>' +
	'</form>';

$(function() {
  if ($("##winWYSIWYG").length == 0) {
    $("body").append('<div id="winWYSIWYG">'+ windowContent +'</div>');	
  }
    
  $("##winWYSIWYG").dialog({
	  title: "WYSIWYG Editor",
	  bgiframe: true,
	  dialogClass: 'dialog',
	  autoOpen: false,
	  width: #URL.windowWidth#,
	  height: 'auto',
	  position: 'top',
	  modal: true,
	  buttons: {
        ' Done ': saveContent,
        'Cancel': function() { $("##winWYSIWYG").dialog('close'); }
      },
	  show: 'clip',
	  hide: 'clip'
	});  
});

</cfoutput>
