<cfparam name="URL.tinyMCEInstallationURL">
<cfparam name="URL.useAbsoluteURL" default="true">
<cfparam name="URL.forceBrNewlines" default="false">
<cfparam name="URL.templateListURL" default="">
<cfparam name="URL.linkListURL" default="">
<cfparam name="URL.imageListURL" default="">
<cfparam name="URL.replaceTokens" default="">
<cfparam name="URL.replaceValues" default="">

<cfparam name="URL.formName">
<cfparam name="URL.fieldName">
<cfparam name="URL.assetRelativeDirectory">
<cfparam name="URL.currentRelativeDirectory">
<cfparam name="URL.stylesheetURL">
<cfparam name="URL.editorHeader" default="">

<html>
<head>
<title><cfif URL.editorHeader IS NOT ""><cfoutput>#URL.editorHeader#</cfoutput><cfelse>Edit Content</cfif></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<cfoutput>
<script type="text/javascript" src="#URL.tinyMCEInstallationURL#/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : 
"example,heading,safari,table,advhr,advimage,advlink,searchreplace,contextmenu,paste,fullscreen,noneditable,visualchars,template,inlinepopups",
		heading_clear_tag: "p",
		<cfif URL.useAbsoluteURL>
		relative_urls : false,	// with absolute URLs and 		
		remove_script_host : false, //ncluding domain on links and images
		<cfelse>
		relative_urls : true,
		remove_script_host : true,
		</cfif>
		accessibility_warnings: false,
		<cfif URL.forceBrNewlines>
		force_br_newlines : true,
		force_p_newlines : false,
		forced_root_block : '' ,
	    </cfif>
		// Theme options
		theme_advanced_buttons1 : "h1,h2,h3,h4,h5,h6,separator,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,backcolor,|,styleselect,example",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,|,removeformat,cleanup,help,code",
		theme_advanced_buttons3 : "tablecontrols,|,sub,sup,|,hr,advhr,charmap,template,|,visualaid,visualchars,fullscreen",

		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,    
		
		// Drop lists for link/image/media/template dialogs
		<cfif Compare(URL.templateListURL, "")>
		template_external_list_url : "#URL.templateListURL#",
		</cfif>
		<cfif Compare(URL.linkListURL, "")>
		external_link_list_url : "#URL.linkListURL#",
		</cfif>
		<cfif Compare(URL.imageListURL, "")>
		external_image_list_url : "#URL.imageListURL#",
		</cfif>

		// stylesheet URL
		<cfif Compare(URL.stylesheetURL, "")>
		content_css : "#URL.stylesheetURL#?" + new Date().getTime(),
		</cfif>
		
		<cfset tokens=ArrayNew(1)>
		<cfset values=ArrayNew(1)>
		<cfif Compare(URL.replaceTokens, "") AND Compare(URL.replaceValues, "")>
		  <cfset tokens=ListToArray(URL.replaceTokens, "|")>
		  <cfset values=ListToArray(URL.replaceValues, "|")>		  
		</cfif>		
		
		<cfif ArrayLen(tokens) IS ArrayLen(values)>
		  <cfif ListFind(URL.replaceTokens, "templateURLRoot", "|") IS 0>	
		    <cfset tokens[ArrayLen(tokens)+1]="templateURLRoot">
		    <cfset values[ArrayLen(values)+1]="#URL.tinyMCEInstallationURL#/extension/templates">
		  </cfif>		
		  template_replace_values : {
		    <cfloop index="idx" from="1" to="#ArrayLen(tokens)#">
			  <cfif idx GT 1>,</cfif>#tokens[idx]#: "#values[idx]#"
			</cfloop>
		  },
		</cfif>
		
		file_browser_callback : 'myFileBrowser'
	});
	
	function myFileBrowser (field_name, url, type, win) {      
	  var cmsURL = "#URL.tinyMCEInstallationURL#/extension/filebrowser/filebrowser.cfm?type=" + type + 
				   "&siteURLRoot=#URLEncodedFormat(URL.siteURLRoot)#" +
				   "&assetRelativeDirectory=#URLEncodedFormat(URL.assetRelativeDirectory)#" +
				   "&currentRelativeDirectory=#URLEncodedFormat(URL.currentRelativeDirectory)#" +
				   "&uuid=" + (new Date()).getTime();
 
	  tinyMCE.activeEditor.windowManager.open({
			file : cmsURL,
			title : "File Browser",
			width : 600,  // Your dimensions may differ - toy around with them!
			height : 600,
			resizable : "yes",
			inline : "yes",  // This parameter only has an effect if you use the inlinepopups plugin!
			close_previous : "no"
	  }, {
			window : win,
			input : field_name
	  });
	  return false;
	}    
	
	function submitContent() {
	   <cfoutput>
	   var ed = tinyMCE.get('#fieldName#'); 

	   var finalHTML = ed.getContent();  
	  //finalHTML = myRegexpReplace(finalHTML, "src=\"http://[a-zA-Z\._/0-9:]*/assets/", "src=\"/assets/", "gi");
 	  opener.document.#URL.formName#.#URL.fieldName#.value=finalHTML;	  
	  </cfoutput>
	  window.close();
	}
</script>
</cfoutput>
</head>

<body>
<div align="right">
<a href="javascript:window.close()"><img src="images/icon_cancel.gif" border="0" align="absmiddle" alt="Click To Close This Window"></a>&nbsp;&nbsp;
</div>

<p style="font-family:Arial; font-size:12px; font-weight:bold; color:#000066 ">
<cfif URL.editorHeader IS NOT "">
<cfoutput>#URL.editorHeader#</cfoutput>
<cfelse>
Edit Content
</cfif>
</p>

<div align="center">
<form name="form1">
<cfoutput>
<textarea name="#URL.fieldName#" cols="70" rows="30"></textarea><br>
</cfoutput>
<input type="button" style="font-family:Arial; font-size:11px; color:#000066 " value="  DONE  " onClick="submitContent()">
</form>
</div>


<script type="text/javascript">
<cfoutput>
  document.form1.#URL.fieldName#.value=opener.document.#URL.formName#.#URL.fieldName#.value;
</cfoutput>
</script>
</body>
</html>
