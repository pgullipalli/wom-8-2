﻿// This list may be created by a server logic page PHP/ASP/ASPX/JSP in some backend system.
// There templates will be displayed as a dropdown in all media dialog if the "template_external_list_url"
// option is defined in TinyMCE init.

var tinyMCETemplateList = [
	// Name, URL, Description
	["myWoman's - Hot Topics", "/jsapis/tinymce3/extension/templates/mywomans-hot-topics.htm", "myWoman's - Hot Topics"],
	["myWoman's - News", "/jsapis/tinymce3/extension/templates/mywomans-news.htm", "myWoman's - News"],
	["myWoman's - Testimonial", "/jsapis/tinymce3/extension/templates/mywomans-testimonial.htm", "myWoman's - Testimonial"],
	["myWoman's - Development Info", "/jsapis/tinymce3/extension/templates/mywomans-hot-development-info.htm", "myWoman's - Development Info"]
];