// This list may be created by a server logic page PHP/ASP/ASPX/JSP in some backend system.
// There templates will be displayed as a dropdown in all media dialog if the "template_external_list_url"
// option is defined in TinyMCE init.

var tinyMCETemplateList = [
	// Name, URL, Description
	["Nested Nav", "/jsapis/tinymce3/extension/templates/nested-nav.htm", "Nested Nav"],
	["General 1", "/jsapis/tinymce3/extension/templates/general-1.htm", "General 1"],
	["General 2", "/jsapis/tinymce3/extension/templates/general-2.htm", "General 2"],
	["Box Quote", "/jsapis/tinymce3/extension/templates/box-quote.htm", "Box Quote"],
	["List", "/jsapis/tinymce3/extension/templates/list.htm", "List"],
	["List with thumbnails", "/jsapis/tinymce3/extension/templates/list-thumbnails.htm", "List with thumbnails"],
	["FAQ", "/jsapis/tinymce3/extension/templates/faq.htm", "FAQ"],
	["Photo Gallery", "/jsapis/tinymce3/extension/templates/photo-gallery.htm", "Photo Gallery"],
	["Category Items", "/jsapis/tinymce3/extension/templates/category-items.htm", "Category Items"],
	["Calendar", "/jsapis/tinymce3/extension/templates/calendar.htm", "Calendar"],
	["Event Description", "/jsapis/tinymce3/extension/templates/event-description.htm", "Event Description"],
	["Staff", "/jsapis/tinymce3/extension/templates/staff.htm", "Staff"]
];