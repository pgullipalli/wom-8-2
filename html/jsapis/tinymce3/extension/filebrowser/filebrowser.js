var fileType="";
var siteURLRoot = "";
var assetRelativeDirectory = "";
var currentRelativeDirectory = "";
var fileListRetrivalURL = "";
var selectedRelatedFilePath = "";

var FileBrowserDialogue = {
    init : function () {
        // Here goes your code for setting your custom things onLoad.
		fileType = document.forms[0].type.value;
		siteURLRoot = document.forms[0].siteURLRoot.value;
		//siteDirectoryRoot = document.forms[0].siteDirectoryRoot.value;
		assetRelativeDirectory = document.forms[0].assetRelativeDirectory.value;
		currentRelativeDirectory = document.forms[0].currentRelativeDirectory.value;
		fileListRetrivalURL = "getfilelist.cfm?currentRelativeDirectory=" + escape(currentRelativeDirectory) + "&assetRelativeDirectory=" + escape(assetRelativeDirectory) + 
						      "&siteURLRoot=" + escape(siteURLRoot) +
							  "&uuid=" + getUUID();		
		//alert("assetRelativeDirectory=" + assetRelativeDirectory);
		//alert("currentRelativeDirectory=" + currentRelativeDirectory);
		//alert("fileListRetrivalURL=" + fileListRetrivalURL);
		//displayCurrentDirectory();
		var fileListHTML = getHTTPRequestContent(fileListRetrivalURL);
		displayLoadingMessage();
		loadText ("divFileListHTML", fileListHTML);
		displayCurrentDirectory();
    },
    selectFile : function () {
		if (selectedRelatedFilePath == "") {alert("Please select a file."); return;}
        var win = tinyMCEPopup.getWindowArg("window");
		var URL = siteURLRoot + "/" + selectedRelatedFilePath;
        // insert information now
        win.document.getElementById(tinyMCEPopup.getWindowArg("input")).value = URL;

        // for image browsers: update image dimensions
        if (fileType == "image" && win.ImageDialog.getImageData) win.ImageDialog.getImageData();
        if (fileType == "image" && win.ImageDialog.showPreviewImage) win.ImageDialog.showPreviewImage(URL);

        // close popup window
        tinyMCEPopup.close();
    }
}
tinyMCEPopup.onInit.add(FileBrowserDialogue.init, FileBrowserDialogue);

function changeDirectory(dirName) {
  if (dirName == "../") {
    var slashIndex = 0;
    for (var i = 0; i < currentRelativeDirectory.length; i++) {
	  if (currentRelativeDirectory.charAt(i) == "/") slashIndex = i;
  	}
	if (slashIndex > 0) currentRelativeDirectory = currentRelativeDirectory.substring(0, slashIndex);	
	else return;
  } else {
    currentRelativeDirectory = currentRelativeDirectory + "/" + dirName;
  }
  document.forms[0].currentRelativeDirectory.value = currentRelativeDirectory;  
  
  
  fileListRetrivalURL = "getfilelist.cfm?currentRelativeDirectory=" + escape(currentRelativeDirectory) + "&assetRelativeDirectory=" + escape(assetRelativeDirectory) +
  						"&siteURLRoot=" + escape(siteURLRoot) +
						"&uuid=" + getUUID();
						
  var fileListHTML = getHTTPRequestContent(fileListRetrivalURL);
  displayLoadingMessage();
  loadText ("divFileListHTML", fileListHTML);
  displayCurrentDirectory();
}

function displayCurrentDirectory() {
  loadText ("divSelectedDirectory", "<b>Selected Directory:</b> " + currentRelativeDirectory + "/");
}

function displayLoadingMessage () {
  loadText ("divSelectedDirectory", "<span style=\"color:#ff0000\">Loading...</span>");
}

function loadText (divID, htmlText) {
  var divObj = document.getElementById(divID);
  if (divObj) {
    divObj.innerHTML = htmlText;
  }
}

function preSelect(type, fileName, previewID) {
  selectedRelatedFilePath = currentRelativeDirectory + "/" + fileName;
  var previewImgObj = null;
  for (var i = 1; i <= parseInt(document.forms[0].numPreviewAreas.value); i++) {
	 previewImgObj=eval("document.previewImage" + i);
	 if (previewImgObj) previewImgObj.src = "images/spacer.gif";
	 loadText("divPreviewRelativeFilePath" + i, "");
  }
  
  previewImgObj=eval("document.previewImage" + previewID);
  if (type == "image") {	
	if (previewImgObj) previewImgObj.src = siteURLRoot + "/" + selectedRelatedFilePath;	
  } else {
	if (previewImgObj) previewImgObj.src = "images/spacer.gif";	
  }
  
  loadText("divPreviewRelativeFilePath" + previewID, fileName);
}

function getHTTPRequestContent(url) {
	var req;
	var returnedHTML = "";
	if (url.indexOf("?") == -1) {
	  url = url + "?uuid=" + getUUID();
	} else {
	  url = url + "&uuid=" + getUUID();
	}
	if (window.XMLHttpRequest) {
	  req = new XMLHttpRequest();
	  req.open("GET", url, false);
	  req.send(null);
	} else if (window.ActiveXObject) {
	  req = new ActiveXObject("Microsoft.XMLHTTP");
	  if (req) {
		req.open("GET", url, false);
		req.send();
	  }
	}	
	if (req) {
	  if (req.status == 200) {
		returnedHTML = req.responseText;
	  } else {
		alert("There was a problem retrieving data from server:\n" + req.statusText);
	  }
	} else {
	  alert("Sorry, this browser isn\'t equipped to perform the requested task.");
	}
	return returnedHTML;
}

function getUUID () {
	var date = new Date();
	return "" + date.getTime();
}

function uploadFile() { 
    var notAcceptableFiles = [".cfm", ".asp", ".aspx", ".dll", ".exe"];
    var form = document.forms[0];
	if (form.newFile.value != "") {
	  if (!acceptableFilesCheck(form.newFile.value, notAcceptableFiles)) {
		alert ("Please upload only acceptable files.");
	    return;
	  }
	  /*if (form.newFileName.value != "") {
	    if (!acceptableFilesCheck(form.newFileName.value, notAcceptableFiles)) {
		  alert ("Please upload only acceptable files.");
	      return;
	    }
		var ch="";
		for (var i = 0; i < form.newFileName.value.length; i++) {
		  ch = form.newFileName.value.charAt(i);
		  if (isBadFileNameChar(ch)) {
		    alert ("Please use only letters, numbers, hyphen(_), and dash(-) for file name.");
			return;
		  }			
		}
	  }*/
	  form.submit();
	} else {
	  alert ("Please select a file to upload.");
	  return;
	}
}

function isBadFileNameChar(ch) {
  if ((ch >= "a" && ch <= "z") || (ch >= "A" && ch <= "Z") || (ch >= "0" && ch <= "9") || ch == "_" || ch == "-" || ch == " ") return true;
  return false;
}

function acceptableFilesCheck(str, notAcceptableFiles) {
    var strlen = str.length;
	var extstr = "";
	var sidx = strlen - 5;
	var i = 0;
	if (sidx < 0) extstr = str.toLowerCase();
	else extstr = str.substring(sidx, strlen).toLowerCase();
	for (i = 0; i < notAcceptableFiles.length; i++) 
	  if (extstr.indexOf(notAcceptableFiles[i]) >= 0) return false;
	  
	return true;
}  

function showHide(id) {	
  if (document.all) {
    document.getElementById = document.all;
	w1 = document.getElementById (id);
  } else {
    w1 = document.getElementById (id);
  }	
  if (w1.style.display == "none") w1.style.display="block";
  else w1.style.display="none";	
}
