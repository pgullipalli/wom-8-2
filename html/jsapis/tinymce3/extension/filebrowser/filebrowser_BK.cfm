<cfparam name="URL.type" default="image">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>File Browser</title>
<script language="javascript" type="text/javascript" src="../../tiny_mce_popup.js"></script>
<script language="javascript" type="text/javascript">
var fileType="";
var siteDirectoryRoot = "";
var siteURLRoot = "";
var assetDirectory = "";
var currentDirectory = "";
var fileListRetrivalURL = "";

var FileBrowserDialogue = {
    init : function () {
        // Here goes your code for setting your custom things onLoad.
		fileType = document.forms[0].fileType.value;
		siteDirectoryRoot = document.forms[0].siteDirectoryRoot.value;
		siteURLRoot = document.forms[0].siteURLRoot.value;
		assetDirectory = document.forms[0].assetDirectory.value;
		currentDirectory = assetDirectory;
		fileListRetrivalURL = "getfilelist.cfm?siteDirectoryRoot=" + escape(siteDirectoryRoot) + "&currentDirectory=" + escape(currentDirectory) + "&assetDirectory=" + escape(assetDirectory) + "&uuid=" + getUUID();
		
		displayCurrentDirectory();
		var fileList = getHTTPRequestContent(fileListRetrivalURL);
		displayLoadingMessage();
		loadText ("divFileList", fileList);
		displayCurrentDirectory();
    },
    selectFile : function () {
		var selectedFile = "";
		var selectObj = document.forms[1].selectedFileName;
 		if (selectObj && selectObj.selectedIndex >= 0) {
    	  selectedFile = selectObj[selectObj.selectedIndex].value;
  		} 
		if (selectedFile == "") {alert("Please select a file."); return;}
        var win = tinyMCEPopup.getWindowArg("window");
		var URL = siteURLRoot + "/" + currentDirectory + "/" + selectedFile;
        // insert information now
        win.document.getElementById(tinyMCEPopup.getWindowArg("input")).value = URL;

        // for image browsers: update image dimensions
        if (fileType == "image" && win.ImageDialog.getImageData) win.ImageDialog.getImageData();
        if (fileType == "image" && win.ImageDialog.showPreviewImage) win.ImageDialog.showPreviewImage(URL);

        // close popup window
        tinyMCEPopup.close();
    }
}
tinyMCEPopup.onInit.add(FileBrowserDialogue.init, FileBrowserDialogue);


function changeDirectory(dirName) {
  if (dirName == "../") {
    var slashIndex = 0;
    for (var i = 0; i < currentDirectory.length; i++) {
	  if (currentDirectory.charAt(i) == "/") slashIndex = i;
  	}
	if (slashIndex > 0) currentDirectory = currentDirectory.substring(0, slashIndex);	
	else return;
  } else {
    currentDirectory = currentDirectory + "/" + dirName;
  }
  fileListRetrivalURL = "getfilelist.cfm?siteDirectoryRoot=" + escape(siteDirectoryRoot) + "&currentDirectory=" + escape(currentDirectory) + "&assetDirectory=" + escape(assetDirectory) + "&uuid=" + getUUID();
  var fileList = getHTTPRequestContent(fileListRetrivalURL);
  displayLoadingMessage();
  loadText ("divFileList", fileList);
  displayCurrentDirectory();
}


function displayCurrentDirectory() {
  loadText ("divSelectedDirectory", "<b>Selected Directory:</b> " + currentDirectory);
}

function displayLoadingMessage () {
  loadText ("divSelectedDirectory", "<span style=\"color:#ff0000\">Loading...</span>");
}

function loadText (divID, htmlText) {
  var divObj = document.getElementById(divID);
  if (divObj) {
    divObj.innerHTML = htmlText;
  }
}

function preview() {
  var selectObj = document.forms[1].selectedFileName;
  if (selectObj && selectObj.selectedIndex >= 0) {
    var fileName = selectObj[selectObj.selectedIndex].value;
    loadText("divPreview", fileName);
  }
}

function getHTTPRequestContent(url) {
	var req;
	var returnedHTML = "";
	if (url.indexOf("?") == -1) {
	  url = url + "?uuid=" + getUUID();
	} else {
	  url = url + "&uuid=" + getUUID();
	}
	if (window.XMLHttpRequest) {
	  req = new XMLHttpRequest();
	  req.open("GET", url, false);
	  req.send(null);
	} else if (window.ActiveXObject) {
	  req = new ActiveXObject("Microsoft.XMLHTTP");
	  if (req) {
		req.open("GET", url, false);
		req.send();
	  }
	}	
	if (req) {
	  if (req.status == 200) {
		returnedHTML = req.responseText;
	  } else {
		alert("There was a problem retrieving data from server:\n" + req.statusText);
	  }
	} else {
	  alert("Sorry, this browser isn\'t equipped to perform the requested task.");
	}
	return returnedHTML;
}

function getUUID () {
	var date = new Date();
	return "" + date.getTime();
}

</script>
</head>
<body>
<div style="display:none;">
<cfoutput><!--- parameters setup --->
<form>
  <input type="hidden" name="fileType" value="#URL.type#" />
  <input type="hidden" name="siteURLRoot" value="http://www.test.com/LSUTSA" />
  <input type="hidden" name="siteDirectoryRoot" value="C:/Data Files/WWW/Test/html/LSUTSA" />
  <input type="hidden" name="assetDirectory" value="assets" />
</form>
</cfoutput>
</div>

<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#dddddd">
  <tr valign="middle" height="15">
	<td><div id="divSelectedDirectory"></div></td>
  </tr>
</table>

<form>
<table border="0" cellpadding="3" cellspacing="0" width="100%">
  <tr valign="top">
    <td width="50%">
    <div id="divFileList"></div>
    </td>
    <td width="50%">
    <div id="divPreview" style="width:100%;height:300px;border:1px solid #dddddd">
    
    </div>
    <div style="text-align:center">
    <a href="javascript:FileBrowserDialogue.selectFile()">Select</a>
    </div>
    </td>  
  </tr>
</table>
</form>

<!---
<form name="my_form">
<input type="text" name="my_field" size="30" value="http://entimg.msn.com/i/50/gossip/HollyDomiCP086510_50.jpg" /> <input type="button" value=" Insert " onclick="FileBrowserDialogue.mySubmit();" />
</form>
---->
</body>
</html>
