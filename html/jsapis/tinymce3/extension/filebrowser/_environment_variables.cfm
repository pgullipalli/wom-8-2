<cfif IsDefined("Application.siteDirectoryRoot")>
  <cfset VARIABLES.siteDirectoryRoot=Application.siteDirectoryRoot>
<cfelse>
  <cfset configFile = Replace(ExpandPath(".."),"\","/","ALL") & "/" & "sitedirectoryroot.txt">
  <cfif FileExists(configFile)><!--- get siteDirectoryRoot from file  --->
    <cffile action="read" file="#configFile#" variable="fileContent">
    <cfset Evaluate("#Trim(fileContent)#")>
  </cfif>
</cfif>

<cfif NOT IsDefined("VARIABLES.siteDirectoryRoot")>
  <b>Site directory root was not found.</b>
  <cfabort>
</cfif>

<cfif IsDefined("URL.siteURLRoot")>
  <cfset VARIABLES.siteURLRoot=URL.siteURLRoot>
</cfif>
<cfif IsDefined("FORM.siteURLRoot")>
  <cfset VARIABLES.siteURLRoot=FORM.siteURLRoot>
</cfif>

<cfif IsDefined("URL.assetRelativeDirectory")>
  <cfset VARIABLES.assetRelativeDirectory=URL.assetRelativeDirectory>
</cfif>
<cfif IsDefined("FORM.assetRelativeDirectory")>
  <cfset VARIABLES.assetRelativeDirectory=FORM.assetRelativeDirectory>
</cfif>

<cfif IsDefined("URL.currentRelativeDirectory")>
  <cfset VARIABLES.currentRelativeDirectory=URL.currentRelativeDirectory>
</cfif>
<cfif IsDefined("FORM.currentRelativeDirectory")>
  <cfset VARIABLES.currentRelativeDirectory=FORM.currentRelativeDirectory>
</cfif>
<cfif NOT IsDefined("VARIABLES.currentRelativeDirectory") OR VARIABLES.currentRelativeDirectory IS "">
  <cfset VARIABLES.currentRelativeDirectory=VARIABLES.assetRelativeDirectory>
</cfif>

<!--- URL used for file upload --->
<!--- <cfif IsDefined("URL.siteDirectoryRoot")>
  <cfset URLForFileUpload="filebrowser.cfm?siteURLRoot=#URLEncodedFormat(VARIABLES.siteURLRoot)#&siteDirectoryRoot=#URLEncodedFormat(VARIABLES.siteDirectoryRoot)#&assetRelativeDirectory=#URLEncodedFormat(VARIABLES.assetRelativeDirectory)#&currentRelativeDirectory=#URLEncodedFormat(VARIABLES.currentRelativeDirectory)#">
<cfelse>
  <cfset URLForFileUpload="filebrowser.cfm?siteURLRoot=#URLEncodedFormat(VARIABLES.siteURLRoot)#&assetRelativeDirectory=#URLEncodedFormat(VARIABLES.assetRelativeDirectory)#&currentRelativeDirectory=#URLEncodedFormat(VARIABLES.currentRelativeDirectory)#">
</cfif> --->