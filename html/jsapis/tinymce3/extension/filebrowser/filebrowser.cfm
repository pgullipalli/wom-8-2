<cfparam name="URL.type" default="image"><!--- image or file --->
<!--- Set up variables VARIABLES.currentRelativeDirectory, VARIABLES.siteURLRoot, VARIABLES.siteDirectoryRoot, VARIABLES.assetRelativeDirectory, and VARIABLES.currentRelativeDirectory --->
<cfinclude template="_environment_variables.cfm">
<!--- <cfinclude template="_uploadfile.cfm"> --->

<style type="text/css">
body, a {
  font-family:arial;
  font-size:11px;
  color:#333333;
  line-height:135%;
}
a,a:link,a:visited,a:active,a:hover {
  text-decoration:none;
  color:#333333;
}
</style>

<script language="javascript" type="text/javascript" src="../../tiny_mce_popup.js"></script>
<script type="text/javascript" language="javascript" src="filebrowser.js"></script>

<div style="width:95%; padding:10px 10px 10px 10px;">
<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#dddddd">
  <tr valign="middle" height="15">
	<td><div id="divSelectedDirectory"></div></td>
  </tr>
</table>

<cfif IsDefined("VARIABLES.uploadMessage")  AND Compare(VARIABLES.uploadMessage, "")>
<cfoutput><p>#VARIABLES.uploadMessage#</p></cfoutput>
</cfif>

<cfoutput>
<form action="filebrowser.cfm" enctype="multipart/form-data" method="post">
  <cftry>
  <!--- parameters setup --->
  <input type="hidden" name="type" value="#URL.type#" />
  <input type="hidden" name="siteURLRoot" value="#VARIABLES.siteURLRoot#" />
  <input type="hidden" name="assetRelativeDirectory" value="#VARIABLES.assetRelativeDirectory#" />
  <input type="hidden" name="currentRelativeDirectory" value="#VARIABLES.currentRelativeDirectory#" />
  <cfcatch>
  <p style="##ff0000">ERROR: Can't set up environment variables</p>
  </cfcatch>
  </cftry>
</cfoutput>
<div id="divFileListHTML"></div>
</form>
</div>
