<!--- <cfparam name="URL.currentRelativeDirectory">
<cfparam name="URL.assetRelativeDirectory"> --->

<!--- Set up variables VARIABLES.siteURLRoot, VARIABLES.siteDirectoryRoot, VARIABLES.assetRelativeDirectory, and VARIABLES.currentRelativeDirectory --->
<cfinclude template="_environment_variables.cfm">

<cfdirectory action="list" directory="#VARIABLES.siteDirectoryRoot#/#VARIABLES.currentRelativeDirectory#" name="allItems" sort="name ASC">
<cfquery name="allDirs" dbtype="query">
  select *
  from allItems
  where type='Dir'
  order by name
</cfquery>

<cfquery name="allFiles" dbtype="query">
  select *
  from allItems
  where type='File'
  order by name
</cfquery>

<cfif Compare(VARIABLES.currentRelativeDirectory, VARIABLES.assetRelativeDirectory) AND Find(VARIABLES.assetRelativeDirectory, VARIABLES.currentRelativeDirectory) GT 0>
<div><a href="javascript:changeDirectory('../');" style="text-decoration:none;"><img src="images/icon_level_up.png" border="0" alt="go one level up" align="absmiddle" />&nbsp;&nbsp;[Go Back]</a><div>
</cfif>

<cfoutput query="allDirs">
   <div><a href="javascript:changeDirectory('#name#')" style="text-decoration:none;"><img src="images/icon_folder.png" border="0" align="absmiddle" />&nbsp;&nbsp;<b>#name#</b></a></div>
</cfoutput>

<br />

<cfset numRowsPerTable = 12>
<cfset numTables = Ceiling(allFiles.recordcount/numRowsPerTable)>

<!--- <cfoutput>#VARIABLES.currentRelativeDirectory#/<br /></cfoutput> --->

<cfif allFiles.recordcount GT 0>
<div style="background-color:#dddddd; padding:2px 2px 2px 2px; margin:0px 0px 3px 0px;">Click a file to preview and then click the 'Select' button to select:</div>
<cfloop index="idx1" from="1" to="#numTables#">
<table cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td width="65%" valign="top">
      <table border="0" cellpadding="2" cellspacing="0" width="100%">
	  <cfset startIdx = (idx1-1)*numRowsPerTable + 1>
      <cfoutput  query="allFiles" startrow="#startIdx#" maxrows="#numRowsPerTable#">
        <tr valign="middle">
          <cfif ListFindNoCase("jpg,gif,png", Mid(name,Len(name)-2, 3)) GT 0>
          <td><a href="javascript:preSelect('image','#Replace(name,"'","\'","ALL")#','#idx1#')" style="text-decoration:none;"><img src="images/icon_image.gif" border="0" align="absmiddle" /> #name#</a></td>
          <cfelse>
          <td><a href="javascript:preSelect('doc','#Replace(name,"'","\'","ALL")#','#idx1#')" style="text-decoration:none;"><img src="images/icon_document.gif" border="0" align="absmiddle" /> #name#</a></td>
          </cfif>
        </tr>
      </cfoutput>
      </table>
    </td>
    <td width="35%" valign="top" align="center">
      <cfoutput>
      <img src="images/spacer.gif" width="150" name="previewImage#idx1#" vspace="5" style="border:1px solid ##dddddd" />
      <div id="divPreviewRelativeFilePath#idx1#" style="text-align:center; font-weight:bold; padding:1px 0px 5px 0px;"></div>
	  </cfoutput>
      <div><input type="button" value="Select" onclick="FileBrowserDialogue.selectFile()" /></div>
    </td>
</table>
</cfloop>

<cfoutput><input type="hidden" name="numPreviewAreas" value="#numTables#" /></cfoutput>

<cfelse>
  <p><i>No files in this folder.</i></p>
</cfif>

<!--- <p>
Upload a new file to this folder:<br />
<input type="file" name="newFile" size="40" /> <input type="button" value="Upload" onclick="javascript:uploadFile()" /><br />
<input type="checkbox" name="overwrite" value="1"> Overwrite file on server if name conflict occurs<br />
</p> --->


<!--- <select name="selectedFileName" size="20" onChange="preview();">
<cfoutput query="allFiles">
  <cfif NOT Compare(type, "File")><option value="#name#">#name#</option></cfif>
</cfoutput>
</select> --->

<!--- <cfdump var="#allFiles#"> --->