function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function toggle(id) {
  var elt = document.getElementById(id);
  if (elt && elt.style) {
	if (elt.style.display == "none") elt.style.display="block";
	else elt.style.display="none";
  }	
}

function checkEmail(str) {
  var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
  return emailPattern.test(str); 
}

function checkNumber (str) {
  var start_format = " .+-0123456789";
  var number_format = " .0123456789";
  var check_char;
  var decimal = false;
  var trailing_blank = false;
  var digits = false;

  if(str.length == 0) return false;

  check_char = start_format.indexOf(str.charAt(0));

  if (check_char == 1) decimal = true;
  else if (check_char < 1) return false;

  for (var i = 1; i < str.length; i++) {
    check_char = number_format.indexOf(str.charAt(i));
    if (check_char < 0) return false;
    else if (check_char == 1) {
	  if (decimal) return false;
	  else decimal = true;
    }	else if (check_char == 0) {
	  if (decimal || digits) trailing_blank = true;
    } else if (trailing_blank) return false;
    else digits = true;
  }

  return true;
}

function checkDigits(str) {  
  var pattern=/^[0-9]+$/i;
  return pattern.test(str);
}

function getCheckedValue(fieldObj) {
  var checkedValue = null;
  if (fieldObj.length) {
	for (var i = 0; i < fieldObj.length; i++) {
	  if (fieldObj[i].checked) {
		if (checkedValue) checkedValue = checkedValue + ',' + fieldObj[i].value;
		else checkedValue = fieldObj[i].value;
	  }
	}
  } else {
	if (fieldObj.checked) checkedValue = fieldObj.value;
  }
  return checkedValue;
}

function displayInlineMessage(id, msg) {
  var obj = document.getElementById(id);
  if (obj && obj.style) {
	obj.style.display="block";
	obj.innerHTML = msg;
  }
}

function resetField(obj, str) {
  if (obj.value == str) obj.value = "";
}

function openwindow (url, winName, w,  h) { openWindow (url, winName, w,  h); }

function openWindow (url, winName, w,  h) {
  var nScrW = 1024;
  var nScrH = 768;
  if (screen && screen.availWidth) {
    nScrW = screen.availWidth;
    nScrH = screen.availHeight;
  }
  var leftPos = (nScrW-w)/2;
  var topPos = (nScrH-h)/2;
  window.open (url, '_blank', 'width=' + w + ',height=' + h + ',location=no,menubar=no,toolbar=no,status=yes,scrollbars=yes,resizable=yes,top=' + topPos + ',left=' + leftPos);
  return;
}

function autotab(obj1, obj2, objLength) {
  if (obj1.value.length == objLength) obj2.focus();
}

function LTrim(value) {
  if (value == null) return "";
  var re = /\s*((\S+\s*)*)/;
  return value.replace(re, "$1");
}

// Removes ending whitespaces
function RTrim(value) {
  if (value == null) return "";
  var re = /((\s*\S+)*)\s*/;
  return value.replace(re, "$1");
}

// Removes leading and ending whitespaces
function trim(value) {
  if (value == null) return "";
  return LTrim(RTrim(value));	
}

/*
$(function() {//patch for div shifting
  if (document.getElementById('contentMain')) {
    $("#contentMain").hide();
    $("#contentMain").show();
  }
});
*/
