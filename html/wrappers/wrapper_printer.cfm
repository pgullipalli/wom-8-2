<cfprocessingdirective pageencoding="utf-8">
<cfswitch expression="#thisTAG.executionmode#">
<cfcase value="start">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Woman's Hospital | exceptional care, centered on you</title>
<link href="scripts/css/master.css" rel="stylesheet" type="text/css" />
<link href="scripts/css/styles_extract_for_tinymce.css" rel="stylesheet" type="text/css" />
<link href="scripts/css/masterPrinter.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jsapis/jquery/js/jquery-1.3.2.min.js"></script>
<style type="text/css">
#printerControl {text-align:right;}
/* interior fixes */
.contentMain { float:left; width:529px; padding-bottom:8px; }
</style>
<style type="text/css" media="print">
#printerControl {display:none;text-align:right;}
</style>
<script type="text/javascript" src="scripts/default.js"></script>
<script language="javascript">
<!--
function openprintdialog () {
  var onWindows = navigator.platform ? navigator.platform == "Win32" : false;
  var macIE = document.all && !onWindows;
  if (macIE) {
	alert ("Press \"Cmd+p\" on your keyboard to print");
  } else {
	window.print();
  }  
}
//-->
</script>
</head>

<body onload="openprintdialog()">
<center>
  <table width="85%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="195" align="left" valign="middle"><cfoutput><a href="#Application.siteURLRoot#" target="_blank"><img src="images/imgLogoPrinter.gif" alt="Woman's - exceptional care, centered on you" width="127" height="110" border="0" /></a></cfoutput></td>
      <td width="141" align="right" valign="middle"><div id="printerControl"><a href="javascript:window.close();"><img src="images/btnPrntrClose.gif" alt="CLOSE" width="71" height="32" border="0" /></a><a href="javascript:openprintdialog();"><img src="images/btnPrntrPrint.gif" alt="PRINT" width="70" height="32" border="0" /></a></div></td>
    </tr>
    <tr>
      <td colspan="2" align="left" valign="top">
      <cfif Request.isHomePage>
        <div id="mainCol" align="left">
          <div class="bkgHm">
      <cfelse>
        <div class="contentMain">            
          <cfif Not Compare(Request.template.topperStyle,"ST")><!--- display section title --->
            <div id="titleBar"><cfoutput>#Request.template.sectionTitle#</cfoutput></div>
            <!--- <div id="titleBarWithBtnsText"></div>  --->             
          <cfelseif Not Compare(Request.template.topperStyle,"NV")><!--- display navigation items like the tri-part --->
            <div id="titleBarWithBtns"><cfinclude template="../modules/navigation/_interior_top_nav.cfm"></div>
          <cfelse><!--- display title image; to be determined --->
            <div id="titleBar"><cfoutput>&nbsp;</cfoutput></div>
          </cfif>
	  </cfif>
</cfcase>

<cfcase value="end">
	  <cfif Request.isHomePage>
        </div></div>
	  <cfelse>
        </div>
	  </cfif>    
	  </td>
    </tr>
  </table>
  <br />
  <br />
  <table width="100%" height="35" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td align="center" valign="middle" bgcolor="#B0BEBC"><table width="85%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" valign="middle" class="prntrFooter"><a href="http://www.womans.org/" target="_blank"><b>WWW.WOMANS.ORG</b></a></td>
          </tr>
      </table></td>
    </tr>
  </table>
</center>
</body>
</html>
</cfcase>
</cfswitch>