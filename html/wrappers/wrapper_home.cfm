<cfswitch expression="#thisTAG.executionmode#">
<cfcase value="start">

<cfscript>
  PB=CreateObject("component", "com.PageBuilder").init();
  pageTitle=PB.getPageTitle(pageID=2);
</cfscript>
<!---<cfoutput>#Request.globalProperties.primaryNavIDList#</cfoutput>--->
<!---<cfoutput>#Request.globalProperties.footer#</cfoutput>--->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><cfoutput>#Request.globalProperties.title#</cfoutput></title>
<link rel="shortcut icon" href="/favicon.ico" />
<cfif Compare(Request.globalProperties.description,"")><cfoutput><meta http-equiv="description" content="#HTMLEditFormat(Request.globalProperties.description)#" /></cfoutput></cfif>
<cfif Compare(Request.globalProperties.keywords,"")><cfoutput><meta http-equiv="keywords" content="#HTMLEditFormat(Request.globalProperties.keywords)#" /></cfoutput></cfif>
<link href="scripts/css/master.css" rel="stylesheet" type="text/css" />
<link href="scripts/css/styles_extract_for_tinymce.css" rel="stylesheet" type="text/css" />

<link href="scripts/css/all.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 8]><link rel="stylesheet" media="all" type="text/css" href="scripts/css/ie.css" /><![endif]-->
<!--<link href="scripts/css/print.css" rel="stylesheet" type="text/css" />
-->

<script type="text/javascript" src="jsapis/jquery/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="scripts/default.js"></script>

<script type="text/javascript" src="scripts/jquery1.5.2.js"></script>
<script type="text/javascript" src="scripts/main.js"></script>

<script type="text/javascript" src="scripts/gaAddons-2.1.2.min.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
 _gaq.push(
      ['_setAccount', 'UA-22770744-1'],
      ['_trackDownload'],
      ['_trackOutbound'], 
      ['_trackMailTo'],
      ['_trackRealBounce'],
      ['_trackPageview'],
      ['t2._setAccount', 'UA-4104464-16'],
      ['t2._trackPageview']
      );
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<!--FEEDBACK TAB-->
<div class="page">
            <a class="hidden" href="#menu">Skip to Navigation</a>
            <a class="hidden" href="#content">Skip to Content</a>
      <!-- Start of wrapper div -->
      <div id="wrapper">
        <!-- Start of header div HORIZONTAL NAVIGATION-->
        <div id="header" >
          <cfinclude template="../modules/navigation/_top_nav.cfm">
        </div>    
        
        <!-- Start of main div -->
      	<div id="main">
				<div class="image-logo">
					<img src="assets/imagesNew/logo.gif" width="128" height="110" alt="image description" />
				</div>
				<h1 class="logo"><a href="http://www.womans.org">Woman's exceptional care, centered on you</a></h1>
                
                <!-- Start of content div -->
				<div id="content">
					
                    <!-- Start of block2 div -->
                    <div class="block block2">
						<div class="block-holder">
							<div class="block-frame">
                                <div class="gallery xml-gallery" title="scripts/inc/gallery.xml">
                                    <div class="gallery-holder"></div>
                                    <a class="prev" href="#">prev</a>
                                    <a class="next" href="#">next</a>
                                </div>		
                                <!---<cfinclude template="../modules/homepage/_home.cfm">--->

</cfcase>	
<cfcase value="end">
							</div>
						</div>
					</div>
                    <!-- end of block2 div -->
                    
                    <!-- Start of block3 div -->
					<div class="block block3">
						<div class="block-holder">
							<div class="block-frame">
								<div class="block-inner">
									<cfinclude template="../modules/communication/_mywomans_signup_hor.cfm">
									<div class="social">
										<h3>Connect with Us</h3>
										<ul>
<li class="facebook">
<div>
<a href="http://www.facebook.com/pages/Womans-Hospital/131273032914">
<span>Facebook</span>
</a>
</div>
</li>


<!-- <li class="wordpress"><div><a href="#"><span>WordPress</span></a></div></li> -->

<li class="twitter">
<div>
<a href="http://twitter.com/WomansHospital"><span>Twitter</span></a></div></li>

<li class="youtube">
<div>
<a href="http://www.youtube.com/user/WomansHospitalBR">
<span>YouTube</span></a>
</div>
</li>

<li class="mywomanschannel">
<div>
<a href="http://www.womans.org/index.cfm?md=newsroom&tmp=detail&articleID=148&nid=110&tid=2">
<span>myWoman's Channel</span></a>
</div>
</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <!-- End of block3 div -->                    
				</div>
                <!-- End of content div -->
                
                <!-- Start of sidebar div -->                                
				<div id="sidebar">
							<cfinclude template="../modules/navigation/_left_primary_nav.cfm">
					<div class="box">
						<div class="box-frame">
							<cfinclude template="../modules/global/_donate_box.cfm">
							
							<div class="box-holder">
								<h3>Woman's for...</h3>
								<cfinclude template="../modules/navigation/_left_secondary_nav.cfm">
							</div>
						</div>
					</div>
				</div>
                <!-- End of sidebar div -->    
                            
			</div>
            <!-- End of main div -->            
		</div>
        <!-- end of wrapper div -->   
             
		<div id="footer">
			<cfinclude template="../modules/global/_footer.cfm">
		</div>
</div>
<!-- End of page div -->

  <!--Fade Script for Home Photo Pods-->
<script type="text/javascript" charset="utf-8">
<!--
    $(function () {
        if ($.browser.msie && $.browser.version < 7) return;
        
        $('.fadeThis')
            .append('<span class="hover"></span>').each(function () {
                    var $span = $('> span.hover', this).css('opacity', 0);
                    $(this).hover(function () {
                        // on hover
                        $span.stop().fadeTo(500, 1);
                    }, function () {
                        // off hover
                        $span.stop().fadeTo(500, 0);
                    });
                });
                
    });
//-->
</script>
</body>
</html>
</cfcase>
</cfswitch>