<cfparam name="Attributes.template" default="">
<cfparam name="URL.pid" default=0>
<cfswitch expression="#thisTAG.executionmode#">
<cfcase value="start">

<cfscript>
  PB=CreateObject("component", "com.PageBuilder").init();
  pageTitle=PB.getPageTitle(pageID=URL.pid);
</cfscript>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!---<title><cfoutput>#pageTitle#------#Request.globalProperties.title#</cfoutput></title>--->
<title><cfoutput><cfif #pageTitle# neq "">#pageTitle#<cfelse>#Request.globalProperties.title#</cfif></cfoutput></title>
<link rel="shortcut icon" href="/favicon.ico" />
<cfif Compare(Request.globalProperties.description,"")><cfoutput><meta http-equiv="description" content="#HTMLEditFormat(Request.globalProperties.description)#" /></cfoutput></cfif>
<cfif Compare(Request.globalProperties.keywords,"")><cfoutput><meta http-equiv="keywords" content="#HTMLEditFormat(Request.globalProperties.keywords)#" /></cfoutput></cfif>
<link href="scripts/css/master.css" rel="stylesheet" type="text/css" />
<link href="scripts/css/styles_extract_for_tinymce.css" rel="stylesheet" type="text/css" />

<link href="scripts/css/all.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 8]><link rel="stylesheet" media="all" type="text/css" href="scripts/css/ie.css" /><![endif]-->
<!--<link href="scripts/css/print.css" rel="stylesheet" type="text/css" />
-->


<script type="text/javascript" src="scripts/jquery1.5.2.js"></script>
<script type="text/javascript" src="scripts/main.js"></script>

<script type="text/javascript" src="jsapis/jquery/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="scripts/default.js"></script>

<script type="text/javascript" src="scripts/gaAddons-2.1.2.min.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
 _gaq.push(
      ['_setAccount', 'UA-22770744-1'],
      ['_trackDownload'],
      ['_trackOutbound'], 
      ['_trackMailTo'],
      ['_trackRealBounce'],
      ['_trackPageview'],
      ['t2._setAccount', 'UA-4104464-16'],
      ['t2._trackPageview']
      );

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script type='text/javascript' src='https://www.idealprotein.com/microsites/js/jquery.lightbox_me.js'></script>
<script type='text/javascript' src='/scripts/jquery.wt.lightbox.js'></script>
</head>
<body>
<div class="page">
            <a class="hidden" href="#menu">Skip to Navigation</a>
            <a class="hidden" href="#content">Skip to Content</a>
  <div id="wrapper">
    <!--HORIZONTAL NAVIGATION-->
    <div id="header" >
          <cfinclude template="../modules/navigation/_top_nav.cfm">
    </div>   
   <div id="main">
   		<div class="image-logo">
            <img src="assets/imagesNew/logo.gif" width="128" height="110" alt="image description" />
        </div>
				<h1 class="logo"><a href="http://www.womans.org">Woman's exceptional care, centered on you</a></h1>
    <div id="sidebar">
                <cfinclude template="../modules/navigation/_left_primary_nav.cfm">
           
        <div class="box">
            <div class="box-frame">
                <cfinclude template="../modules/global/_donate_box.cfm">
                
                <div class="box-holder">
                    <h3>Woman's for...</h3>
                    <cfinclude template="../modules/navigation/_left_secondary_nav.cfm">
                </div>
            </div>
        </div>
    </div>
    <div id="mainCol" align="left">
      <div class="bkgInt">
          <div class="contentLft">
            <div id="thmemImage"><cfif Compare(Request.template.themeImage,"")><cfoutput><img src="#Request.template.themeImage#" width="202" height="244" alt="" /></cfoutput></cfif></div>            
            <!--Vertical Gray Nav-->            
            <cfinclude template="../modules/navigation/_interior_left_nav.cfm">           
            <!--My Woman's-->
            <cfinclude template="../modules/communication/_mywomans_signup_vert.cfm">
          </div>
          <div class="contentMain">            
            <cfif Not Compare(Request.template.topperStyle,"ST")><!--- display section title --->
              <div id="titleBar"><cfoutput>#Request.template.sectionTitle#</cfoutput></div>
              <!--- <div id="titleBarWithBtnsText"></div>  --->             
            <cfelseif Not Compare(Request.template.topperStyle,"NV")><!--- display navigation items like the tri-part --->
              <div id="titleBarWithBtns"><cfinclude template="../modules/navigation/_interior_top_nav.cfm"></div>
            <cfelse><!--- display title image; to be determined --->
              <div id="titleBar"><cfoutput>&nbsp;</cfoutput></div>
            </cfif>            
</cfcase>	
<cfcase value="end">
          </div>
        <!--Ads-->
        <div class="adsHdr"><img src="images/imgAdsHdr.gif" width="731" height="1" alt="" /></div>
        <div id="ads">
		  <cfif Request.isSubHomePage>
		    <cfset Variables.categoryID=URL.shpid>
		  <cfelseif IsDefined("URL.catid")>
		    <cfset Variables.categoryID=URL.catid>
		  <cfelse>
		    <cfset Variables.categoryID=0>
		  </cfif>
		  <cfoutput>
		  <script type="text/javascript" src="index.cfm?md=promotion&amp;tmp=interior_ads_js&amp;nowrap=1&amp;module=#URL.md#&amp;template=#URL.tmp#&amp;categoryID=#Variables.categoryID#"></script>		
		  </cfoutput>
        </div>
      </div>
      <!--Pod Footer-->
      <div class="podFtrInt"><img src="images/imgIntPodsFtr.gif" width="731" height="11" alt="" /></div>
    </div>
  </div>
  </div>
  <div style="padding-top:12px;"></div>
   <!--FOOTER-->
      <div id="footer">
        <cfinclude template="../modules/global/_footer.cfm">
      </div>
</div>
<div id='idealprotein_lightbox_content' style="width:800px; height:600px; border:3px solid #000000; background-color:#000000; display:none; position:relative;"><img class="close" src="https://www.idealprotein.com/microsites/img/btn_close.png" style="margin:5px; position:absolute; right:0;" /><IFRAME src='https://www.idealprotein.com/microsites/index.php?l=en&id=rtyfgjsdw' width=800 height=600 frameborder=0 scrolling=no style=' overflow:hidden; background-color:#FFFFFF;'></IFRAME></div>
<div id='idealprotein_lightbox_product' style="width:800px; height:600px; border:3px solid #000000; background-color:#000000; display:none; position:relative;"><img class="close" src="https://www.idealprotein.com/microsites/img/btn_close.png" style="margin:5px; position:absolute; right:0;" /><IFRAME src='https://www.idealprotein.com/microsites/products.php?l=en&id=rtyfgjsdw' width=800 height=600 frameborder=0 scrolling=no style=' overflow:hidden; background-color:#FFFFFF;'></IFRAME></div>

</body>
</html>
</cfcase>
</cfswitch>