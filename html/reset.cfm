<cfparam name="URL.resetApplication" default="0">
<cfparam name="URL.resetSession" default="0">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
body {
  font-family:"Lucida Sans Unicode", Arial, Helvetica, sans-serif;
  font-size:11px;
  line-height:130%;
  color:#000099;
}

a { color:#000099 }
</style>
</head>

<body>

<center>
<br /><br />
<cfif URL.resetApplication>
<p>The application has been reset.</p>
<cfelseif URL.resetSession>
<p>Your session has been reset.</p>
</cfif>


<p>
<a href="reset.cfm?resetApplication=1">Reset Application</a>
</p>

<p>
<a href="reset.cfm?resetSession=1">Reset Session</a>
</p>

<p><a href="/admin/">Back to Home</a></p>


</center>
</body>
</html>
