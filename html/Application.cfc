<cfcomponent output="false">
  <cfset this.name = "WomansHospitalDev">
  <cfset this.clientmanagement="False">
  <cfset this.sessionmanagement="True">
  <cfset this.sessiontimeout="#createtimespan(0,2,0,0)#">
  <cfset this.applicationtimeout="#createtimespan(1,0,0,0)#">
  <cfset this.siteInstallationPath="#findSiteInstallationPath()#">
  <!--- set component path mapping; now we can use 'com' to reference CF component path --->
  <cfset this.mappings["/com"]=this.siteInstallationPath & "/components">
  <cfset this.mappings["/modules"]=this.siteInstallationPath & "/html/modules">
  <cfset this.customtagpaths=this.siteInstallationPath & "/customtags">

  <cffunction name="onApplicationStart">
	<cfset initializeApplication()>	
  </cffunction>

  <cffunction name="onRequestStart">
    <cfparam name="URL.md" type="string" default="homepage">
    <cfparam name="URL.tmp" type="string" default="home">    
    
    <!--- XSS attack blocker --->
    <cfif ReFind("<[^>]*>", ReplaceNoCase(ReplaceNoCase(CGI.QUERY_STRING,"%3C","<","All"),"%3E",">","All"))><cfabort></cfif>
    <cfif IsDefined("Form")>
      <cfloop item="field" collection="#Form#">
        <cfif ReFind("<script", Form[field])><cfabort></cfif>
      </cfloop>
    </cfif>
    
    <cfif Not ReFind("^[a-zA-Z0-9_\-]+$","#URL.md##URL.tmp#")><cfabort></cfif>
    
    <!--- <cfset Request.tickcountStart=getTickcount()> --->
    
	<cfif IsDefined("URL.resetApplication")><!--- initialize application environment variables --->
	  <cfset initializeApplication()>	
	</cfif>
    
    <!--- check if the requested page is password-protected --->
    <!--- <cfset RA=CreateObject("component","com.RestrictedArea").init()>
    <cfset restrictedGroupIDList=RA.getRestrictedGroupIDList(URLQueryStuct=URL, URLQueryString=CGI.QUERY_STRING)>
    <cfif Compare(restrictedGroupIDList,"")>
      <cfset grantAccess=false>
      <cfif IsDefined("cookie.groupIDList")>
        <cfloop index="restrictedGroupID" list="#restrictedGroupIDList#">
          <cfif ListFind(cookie.groupIDList, restrictedGroupID) GT 0>
            <cfset grantAccess=true>
            <cfbreak>
          </cfif>
        </cfloop>
      </cfif>
      <cfif Not grantAccess>
        <cflocation url="index.cfm?md=protected&tmp=login&qs=#URLEncodedFormat(CGI.QUERY_STRING)#">
      </cfif>
    </cfif> --->
  </cffunction>
  
  <cffunction name="initializeApplication" access="private" output="false">
	<!--- 
	Define these application variables:
	Application.siteInstallationPath: absolute path of the directory this application was installed
	Application.siteDirectoryRoot: absolute path of the root HTML directory for the site
	Application.siteAdminDirectoryRoot: absolute path of the root HTML directory for the admin site
	
	Here is a list all pre-definded Application variables:
	siteInstallationPath, siteDirectoryRoot, siteAdminDirectoryRoot, privilegeXMLObj
	--->

	<cflock scope="Application" timeout="30" type="exclusive">
	  <cfset Application.siteInstallationPath=this.siteInstallationPath>
	  <cfset Application.siteDirectoryRoot=Application.siteInstallationPath & "/html">
	  <cfset Application.modulePath=Application.siteInstallationPath & "/html/modules">
      <cfset configFilePath=Application.siteInstallationPath & "/config/sitewide.xml">
	  <cfset initializeSiteWideVariables(configFilePath)>
	</cflock>
  </cffunction>

  <cffunction name="initializeSiteWideVariables" access="private" output="false">
    <cfargument name="filePath" type="string" required="yes">  
	<cffile action="read" file="#Arguments.filePath#" variable="sitewideXml">	
	<cfscript>
	  xmlObj=XMLParse(sitewideXml);
	  varObjArray = xmlObj.XmlRoot.variable;
	  for (i=1; i LTE ArrayLen(varObjArray); i=i+1) {
	    varObj=varObjArray[i];
		scope=varObj.XmlAttributes.scope;
		switch (scope) {
		  case "application":
		    if (Not CompareNoCase(varObj.XmlAttributes.type, "string")) {
		      Application[varObj.XmlAttributes.name]=EvaluateCustomValue(varObj.XmlAttributes.value);		      
			} else if (Not CompareNoCase(varObj.XmlAttributes.type, "struct")) {
			  structName = varObj.XmlAttributes.name;
			  if (Not IsDefined("Application.#structName#")) {
			    Application[structName] = StructNew();
			  }
			  memberObjArray = varObj.member;
			  for (j=1; j LTE ArrayLen(memberObjArray); j=j+1) {
			    memberObj = memberObjArray[j];
				attName = memberObj.XmlAttributes.key;
				Application[structName][attName] = EvaluateCustomValue(memberObj.XmlAttributes.value);
			  }
			}
			break;
		  case "request":
		    if (Not CompareNoCase(varObj.XmlAttributes.type, "string")) {
		      Request[varObj.XmlAttributes.name]=EvaluateCustomValue(varObj.XmlAttributes.value);
			} else if (Not CompareNoCase(varObj.XmlAttributes.type, "struct")) {
			  structName = varObj.XmlAttributes.name;
			  if (Not IsDefined("REQUEST.#structName#")) {
			    Request[structName] = StructNew();
			  }
			  memberObjArray = varObj.member;
			  for (j=1; j LTE ArrayLen(memberObjArray); j=j+1) {
			    memberObj = memberObjArray[j];
				attName = memberObj.XmlAttributes.key;
				Request[structName][attName] = EvaluateCustomValue(memberObj.XmlAttributes.value);
			  }
			}
			break;
		  default:
		    break;
		}
	  }
	</cfscript>
  </cffunction>

  <cffunction name="findSiteInstallationPath" access="private" returntype="string">
	<cfset currentPath=Replace(ExpandPath("."), "\", "/", "All")>
	<cfreturn Mid(currentPath, 1, Find("/html", currentPath)-1)>	
  </cffunction>

  <cffunction  name="EvaluateCustomValue" access="private" output="false" returntype="string">
    <cfargument name="inputString" type="string" required="true">
	<cfscript>
	  startIndex=Find("$$", Arguments.inputString);
	  if (startIndex GT 0) {
	    endIndex=Find("$$", Arguments.inputString, startIndex+1);
	    if (endIndex GT 0) {
	      variableName=Mid(Arguments.inputString, startIndex + 2, endIndex-startIndex-2);
	      if (IsDefined("Application.#variableName#")) {
	      	return Replace(Arguments.inputString, "$$#variableName#$$", Application["#variableName#"], "All");
	      } else {
	      	return Replace(Arguments.inputString, "$$#variableName#$$", "", "All");
	      }
	    }
	  }	
	  return Arguments.inputString;
	</cfscript>
  </cffunction>
  
  <!--- <cffunction name="onRequestEnd" output="yes">
    <cfif Not IsDefined("URL.nowrap") OR Not URL.nowrap>
		<cfset tickcountEnd=getTickcount()>
        <cfset executionTime=NumberFormat((tickcountEnd-Request.tickcountStart)/1000, "9.999")>
        <cfoutput>
        <script type="text/javascript">
		<!--
          var displayArea=document.getElementById("tickcount");
          if (displayArea) {
            displayArea.innerHTML='Server Execution Time: #executionTime# sec.';
            displayArea.style.display="block";
          }
		//-->
        </script>
        </cfoutput>
    </cfif>
  </cffunction> --->
</cfcomponent>