<cfset flushThreshold=2000>

<cfset adminLogDirectory=Replace(ExpandPath("../../../admin_log"),"\","/","All")>

<cfquery name="qLogs" datasource="#Application.datasource#">
  SELECT COUNT(adminLogID) AS numLogs
  FROM admin_action_log
</cfquery>

<cfif qLogs.numLogs GT flushThreshold>
  <cfset fileName="adminlog" & DateFormat(now(),"yyyy-mm") & ".csv">
  <cfset filePath=adminLogDirectory & "/" & fileName>
  <cfquery name="qLogs" datasource="#Application.datasource#">
    SELECT admin_action_log.module, admin_action_log.task,
    	   admin_action_log.actionURL, admin_action_log.dateOfActivity, am_administrator.username
    FROM admin_action_log LEFT JOIN am_administrator ON admin_action_log.adminID=am_administrator.administratorID
  </cfquery>
  <cfset newLine=Chr(13) & Chr(10)>
  <cfsavecontent variable="logText">
  <cfoutput query="qLogs">#newLine##DateFormat(dateOfActivity,"YYYY-mm-dd")# #TimeFormat(dateOfActivity,"hh:mm::ss tt")#,#username#,#module#,#task#,#actionURL#</cfoutput>
  </cfsavecontent>  
  <cfif FileExists(filePath)>
	<cffile action="append" file="#filePath#" output="#newLine##Trim(logText)#" addnewline="no">
  <cfelse>
    <cfset logText="Date Time,Username,Module,Task,actionURL" & newLine & Trim(logText)>
    <cffile action="write" file="#filePath#" output="#Trim(logText)#" addnewline="no">
  </cfif>
  
  <cfquery datasource="#Application.datasource#">
    DELETE FROM admin_action_log
  </cfquery>
</cfif>