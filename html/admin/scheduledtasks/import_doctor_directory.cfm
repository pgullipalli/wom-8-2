<cfset Variables.dataFilePath="#Application.siteDirectoryRoot#/../ImportFiles/DoctorDirectory.csv">
<cfscript>
  DD=CreateObject("component", "com.DoctorAdmin").init();
  
  fileObj = CreateObject("java","java.io.File").init(Variables.dataFilePath);
  Variables.importFileTimestamp = CreateObject("java","java.util.Date").init(fileObj.lastModified());  
  Variables.lastImportFileTimestamp=DD.getLastImportFileTimestamp();
  
  strTimestamp=DateFormat(Variables.importFileTimestamp,"mm/dd/yyyy") & " " & TimeFormat(Variables.importFileTimestamp,"hh:mm:ss tt");
  strLastTimestamp=DateFormat(Variables.lastImportFileTimestamp,"mm/dd/yyyy") & " " & TimeFormat(Variables.lastImportFileTimestamp,"hh:mm:ss tt");
  
  if (Compare(strTimestamp, strLastTimestamp)) {
    importLog=DD.importDoctorData(dataFilePath=Variables.dataFilePath);
  }
</cfscript>
<cfif IsDefined("importLog")>
<cfoutput>
<pre>
#importLog#
</pre>
</cfoutput>
<cfelse>
Import file was not changed. No import necessary.
</cfif>