<cfset CMSK=CreateObject("component", "com.CommunicationScheduledTasks").init()>
<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>

<cfif CMSK.isProcessInProgress()>
  <cfabort>
<cfelse>
  <cfset CMSK.updateProcessStatus(1)>
</cfif>

<cftry>
	<!--- BEGIN: update campaign status --->
	<cfset CMSK.scanAndUpdateCampaignStatus()>
	<!--- END: update campaign status --->
	
	<!--- //////////////////////////////////////////////////////////////////////////////////////////// --->
	
	<!--- BEGIN: push e-mails in cm_emailqueue to mail server --->
	<cfset CMSK.processEmailQueue()>
	<!--- END: push e-mails in cm_emailqueue to mail server --->
	
	<!--- //////////////////////////////////////////////////////////////////////////////////////////// --->
	
	<!--- BEGIN: process scheduled send messages --->
	<cfset message=CMSK.getFirstScheduledSendMessage()>
	<cfif message.recordcount GT 0>
	  <cfset CM.moveMessageToEmailQueue(message.messageID)>
	</cfif>
	<!--- END: process scheduled send messages --->
	
	<!--- //////////////////////////////////////////////////////////////////////////////////////////// --->
	
	<!--- BEGIN: pop bounce-back emails; update table cm_bounce --->
	<cfset CMSK.parseBounceBackEmails()>
	<!--- END: pop bounce-back emails; update table cm_bounce --->
	
	<!--- //////////////////////////////////////////////////////////////////////////////////////////// --->
	
	<!--- BEGIN: check table cm_bouncebackresendschedule; push bounce back e-mails to e-mail queue (cm_emailqueue) --->
	<cfset CMSK.resendBounceBackMails()>
	<!--- END: check table cm_bouncebackresendschedule; push bounce back e-mails to e-mail queue (cm_emailqueue) --->
	
	<!--- //////////////////////////////////////////////////////////////////////////////////////////// --->
	
	<!--- BEGIN: update email status in each list --->
	<cfset CMSK.updateStatusBasedOnBounceNumber()>
	<!--- END: update email status in each list --->
	
	<!--- //////////////////////////////////////////////////////////////////////////////////////////// --->
	
	<!--- BEGIN: purge e-mail campaign stats --->
	<cfset CMSK.purgeCampaignStats()>
	<!--- END: purge e-mail campaign stats --->  
<cfcatch>
  <!--- <cfif IsDefined("APPLICATION.emaillist.errorNotificationEmail")>
    <cfset errorNotificationEmail=APPLICATION.emaillist.errorNotificationEmail>
  <cfelse>
    <cfset errorNotificationEmail="eturner@medtouch.com">
  </cfif>--->
  <!---
  <cfmail from="#errorNotificationEmail#" to="#errorNotificationEmail#" subject="Email List Manager Error - #APPLICATION.siteURLRoot#">
  #cfcatch.Message#  
  #cfcatch.Detail#
  </cfmail>
  --->
</cfcatch>
</cftry>

<cfset CMSK.updateProcessStatus(0)>

DONE!!!