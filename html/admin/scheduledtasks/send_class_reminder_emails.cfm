<cfset currentTime=now()>
<cfset twoDaysLater=DateAdd("d",2,currentTime)>

<cfquery name="qClassInfo" datasource="#Application.datasource#">
  SELECT cl_student.studentID, cl_student.email,
  		 cl_student_class.classID,
         cl_classinstance.classInstanceID, cl_classinstance.dayOfWeekAsNumber, cl_classinstance.startDateTime, cl_classinstance.endDateTime,
         cl_course.courseID, cl_course.courseTitle,
         cl_location.locationName
  FROM ((((cl_student INNER JOIN cl_student_class ON cl_student.studentID=cl_student_class.studentID) INNER JOIN cl_classinstance ON
  		cl_student_class.classID=cl_classinstance.classID) INNER JOIN cl_class ON 
        cl_classinstance.classID=cl_class.classID) INNER JOIN cl_course ON
        cl_class.courseID=cl_course.courseID) LEFT JOIN cl_location ON
        cl_classinstance.locationID=cl_location.locationID
  WHERE cl_student.sendReminderEmail=1 AND
  		cl_course.sendReminderEmail=1 AND
  		cl_classinstance.reminderSent=0 AND
  		cl_classinstance.startDateTime > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
        cl_classinstance.startDateTime < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#twoDaysLater#">
  ORDER BY cl_student.studentID, cl_classinstance.startDateTime
</cfquery>

<cfquery name="qEmailSetting" datasource="#Application.datasource#">
  SELECT *
  FROM cl_emailsettings
  WHERE emailCode='class_reminder'
</cfquery>

<cfif qEmailSetting.recordcount Is 0><cfabort></cfif>

<cfif Compare(qEmailSetting.replyName,"")>
  <cfset fromAddress="#qEmailSetting.replyName#<#qEmailSetting.replyAddress#>">
<cfelse>
  <cfset fromAddress="#qEmailSetting.replyAddress#">
</cfif>

<cfset newLine=Chr(13) & Chr(10)>

<cfloop query="qClassInfo">
  <!--- send email to studentID --->
  <cfset classInfo = courseTitle & newLine & "Time: " &
  					 DayOfWeekAsString(dayOfWeekAsNumber) & ", " & DateFormat(startDateTime, "mm/dd/yyyy") & " " &
					 LCase(TimeFormat(startDateTime,"h:mmtt")) & " - " & LCase(TimeFormat(endDateTime,"h:mmtt"))>
  <cfif Compare(locationName,"")>
    <cfset classInfo = classInfo & newLine & "Location: " & locationName>
  </cfif>
  <cfset finalEmailBodyText=ReplaceNoCase(qEmailSetting.emailBody, "$class", "#classInfo#", "All")>
  <cfset finalEmailBodyHTML=Replace(ReplaceNoCase(qEmailSetting.emailBody, "$class", "#classInfo#", "All"),Chr(10),"<br />","All")>
  

  <cfif IsValid("email", email)>
    <cfmail from="#fromAddress#" to="#email#" subject="#qEmailSetting.emailSubject#" type="html">
      <cfmailpart type="text">
#finalEmailBodyText# 
	  </cfmailpart>
      <cfmailpart type="html" charset="utf-8">
        <a href="#Application.siteURLRoot#"><img src="#Application.siteURLRoot#/images/imgLogoEmailNot.gif" alt="Woman's - exceptional care, centered on you" width="96" height="84" border="0" /></a>
        <br /><br /> 
        <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:12px;color:##333132;line-height:130%;">
          #finalEmailBodyHTML#
        </div>
        <br />
        <hr size="1" noshade="noshade" />
        <a href="#Application.siteURLRoot#" style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;font-size:19px;color:##627d79;line-height:110%;text-decoration:none;"><b>WWW.WOMANS.ORG</b></a>
      </cfmailpart>
    </cfmail>
  </cfif>

  <cfquery datasource="#Application.datasource#">
    UPDATE cl_classinstance
    SET reminderSent=1
    WHERE classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#classInstanceID#">
  </cfquery>
</cfloop>