<!--- get all classIDs that have been exoired for abiut three months --->
<cfset currentTime=now()>
<cfset threeMonthsBefore=DateAdd("m",-3,currentTime)>
<cfset aFewDaysBefore=DateAdd("d",-7,threeMonthsBefore)>

<cfquery name="qClassIDs" datasource="#Application.datasource#">
  SELECT DISTINCT cl_class.classID
  FROM cl_class INNER JOIN cl_classinstance ON cl_class.classID=cl_classinstance.classID
  WHERE cl_classinstance.startDateTime > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#aFewDaysBefore#"> AND
  		cl_classinstance.startDateTime < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#threeMonthsBefore#"> AND
        cl_classinstance.feedbackRequestSent=0
</cfquery>

<cfif qClassIDs.recordcount GT 0>
  <!--- students who took the class but haven't submitted a feedback yet --->
  <cfquery name="qStudents" datasource="#Application.datasource#">
    SELECT DISTINCT cl_student.studentID, cl_student.email, cl_student_class.classID, cl_course.courseTitle
    FROM ((cl_student INNER JOIN cl_student_class ON cl_student.studentID=cl_student_class.studentID) INNER JOIN cl_class ON
    	  cl_student_class.classID=cl_class.classID) INNER JOIN cl_course ON
          cl_class.courseID=cl_course.courseID
    WHERE cl_student_class.classID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qClassIDs.classID)#" list="yes">) AND
    	  cl_student_class.feedbackSent=0
  </cfquery>  
  
  <cfquery name="qEmailSetting" datasource="#Application.datasource#">
    SELECT *
    FROM cl_emailsettings
    WHERE emailCode='feedback_request'
  </cfquery>
    
  <cfif Compare(qEmailSetting.replyName,"")>
    <cfset fromAddress="#qEmailSetting.replyName#<#qEmailSetting.replyAddress#>">
  <cfelse>
    <cfset fromAddress="#qEmailSetting.replyAddress#">
  </cfif>
  
  <cfloop query="qStudents"> 
    <cfif IsValid("email", email)>      
      <cfset Variables.courseTitleText=courseTitle>
      <cfset Variables.courseTitleHTML="<span style=""font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:12px;font-weight:bold;color:##eb44a7;line-height:130%;"">#courseTitle#</span>">
      <cfset feedbackFormURLText="#Application.siteURLRoot#/review/?id=#classID#&stid=#studentID#">
      <cfset feedbackFormURLHTML='<a href="#Application.siteURLRoot#/review/?id=#classID#&stid=#studentID#">#Application.siteURLRoot#/review/?id=#classID#&stid=#studentID#</a>'>
  	  <cfset finalEmailBodyText=ReplaceNoCase(qEmailSetting.emailBody, "$class", "#Variables.courseTitleText#", "All")>
      <cfset finalEmailBodyText=ReplaceNoCase(finalEmailBodyText, "$link", "#feedbackFormURLText#", "All")>
      <cfset finalEmailBodyHTML=ReplaceNoCase(qEmailSetting.emailBody, "$class", "#Variables.courseTitleHTML#", "All")>
      <cfset finalEmailBodyHTML=ReplaceNoCase(finalEmailBodyHTML, "$link", "#feedbackFormURLHTML#", "All")>
      
      <cfmail from="#fromAddress#" to="#email#" subject="#qEmailSetting.emailSubject#" type="html">
        <cfmailpart type="text" charset="utf-8">
#finalEmailBodyText#
        </cfmailpart>
        <cfmailpart type="html" charset="utf-8">
          <a href="#Application.siteURLRoot#"><img src="#Application.siteURLRoot#/images/imgLogoEmailNot.gif" alt="Woman's - exceptional care, centered on you" width="96" height="84" border="0" /></a>
          <br /><br /> 
          <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:12px;color:##333132;line-height:130%;">
          #Replace(finalEmailBodyHTML,Chr(10),"<br />","All")#
          </div>
          <br />
          <hr size="1" noshade="noshade" />
          <a href="#Application.siteURLRoot#" style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;font-size:19px;color:##627d79;line-height:110%;text-decoration:none;"><b>WWW.WOMANS.ORG</b></a>
        </cfmailpart>
      </cfmail>
    </cfif>
  </cfloop>
  
  <cfquery datasource="#Application.datasource#">
    UPDATE cl_classinstance
    SET feedbackRequestSent=1
    WHERE classID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qClassIDs.classID)#" list="yes">)
  </cfquery>
</cfif>