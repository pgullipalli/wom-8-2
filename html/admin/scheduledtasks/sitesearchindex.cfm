<cfset SM=CreateObject("component", "com.SiteSearchAdmin").init()>

<cfset dbUpdateNeeded=SM.getSiteContentIndexStatus('db')>
<cfset fileUpdateNeeded=SM.getSiteContentIndexStatus('file')>
<cfset classUpdateNeeded=SM.getSiteContentIndexStatus('class')>
<cfset directoryUpdateNeeded=SM.getSiteContentIndexStatus('directory')>

<cfif dbUpdateNeeded OR fileUpdateNeeded OR classUpdateNeeded OR directoryUpdateNeeded>
  <cfset SM.indexSiteContent(directoryPath="#Application.siteDirectoryRoot#/assets/docs", urlPath="#Application.siteURLRoot#/assets/docs", searchableFiles="#Application.sitesearch.searchableFiles#")>
</cfif>