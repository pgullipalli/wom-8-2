<cfset Variables.dataFilePath="#Application.siteDirectoryRoot#/../ImportFiles/FitnessMembers.csv">
<cfscript>
  CL=CreateObject("component", "com.ClassAdmin").init();
  
  fileObj = CreateObject("java","java.io.File").init(Variables.dataFilePath);
  Variables.importFileTimestamp = CreateObject("java","java.util.Date").init(fileObj.lastModified());  
  Variables.lastImportFileTimestamp=CL.getLastImportFileTimestamp();
  
  strTimestamp=DateFormat(Variables.importFileTimestamp,"mm/dd/yyyy") & " " & TimeFormat(Variables.importFileTimestamp,"hh:mm:ss tt");
  strLastTimestamp=DateFormat(Variables.lastImportFileTimestamp,"mm/dd/yyyy") & " " & TimeFormat(Variables.lastImportFileTimestamp,"hh:mm:ss tt");
  
  if (Compare(strTimestamp, strLastTimestamp)) {
    importLog=CL.importFitnessClubMembers(dataFilePath=Variables.dataFilePath);
  }
</cfscript>
<cfif IsDefined("importLog")>
<cfoutput>
<pre>
#importLog#
</pre>
</cfoutput>
<cfelse>
Import file was not changed. No import necessary.
</cfif>