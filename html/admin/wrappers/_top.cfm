<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Management System</title>
<link rel="stylesheet" type="text/css" href="/jsapis/jquery/css/theme/hot-sneaks/jquery-ui-1.7.2.custom.css" />
<link rel="stylesheet" type="text/css" href="/jsapis/jquery/css/jquery-ui-1.7.1.extension.css" /><!--- for jquery buttons & layout --->
<link rel="stylesheet" type="text/css" href="scripts/css/main.css" /><!--- for non-api content --->
<link rel="stylesheet" type="text/css" href="scripts/css/font-color-override.css" /><!--- used to override main.css --->

<script src="/jsapis/jquery/js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="/jsapis/jquery/js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
<script src="/jsapis/jquery/js/jquery.form.js" type="text/javascript"></script><!--- ajax Form plugin --->
<script src="/jsapis/jquery/js/jquery.metadata.js" type="text/javascript"></script>
<script src="/jsapis/jquery/js/jquery.validate.min.js" type="text/javascript"></script><!--- Form Validate plugin --->
<script src="/jsapis/jquery/js/jquery-ui-1.7.1.button.js" type="text/javascript"></script><!--- for iquery buttons --->
<script type="text/javascript" src="/jsapis/jquery/js/jquery.xsite.js"></script>
<script type="text/javascript">
  $.metadata.setType("attr", "validate"); // for form validator
  function goToLogin() {
    if (top && top.location) {
	  top.location = "login.html";
	} else {
	  window.location = "login.html";
	}
  }  
</script>
</head>
<body>