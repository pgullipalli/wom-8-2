<br /><br />
<cflock scope="session" type="exclusive" timeout="30">
  <cfset isLoggedIn=Session.access.isLoggedIn>
  <cfset isSuperUser=Session.access.isSuperUser>
  <cfset isMightyUser=Session.access.isMightyUser>
  <cfif isLoggedIn>
    <cfset accessibleModuleList=SESSION.access.getAccessibleModules()>
  </cfif>
</cflock>

<cfif isLoggedIn>
<div id="accordion">
	<cfif isSuperUser OR ListFind(accessibleModuleList, "administration")>
	<h3><a href="#">Administration</a></h3>
	<div class="subMenu">
		<p>
        The Administration Manager allows you to create site administrators and grant administrators the administrative privileges
        to manage content in specific areas of the site.
        </p>
        <ul>
          <li><a href="index.cfm?md=administration&tmp=home&wrap=1" target="mainFrame">Administrators</a></li>
        </ul>
	</div>
    </cfif>
    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "global")>
	<h3><a href="#">Global Properties</a></h3>
	<div class="subMenu">
		<p>
        This area allows you to update site-wide properties of your web pages.
        </p>
        <ul>
          <li><a href="index.cfm?md=global&tmp=home&wrap=1" target="mainFrame">Global Properties</a></li>
        </ul>
	</div>
    </cfif>
    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "template")>
	<h3><a href="#">Template Manager</a></h3>
	<div class="subMenu">
		<p>
        This area allows you to create templates available to all pages of your web site.
        </p>
        <ul>
          <li><a href="index.cfm?md=template&tmp=templates&wrap=1" target="mainFrame">Templates</a></li>
        </ul>
	</div>
    </cfif>
    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "homepage")>
	<h3><a href="#">Home/Sub-Home Pages</a></h3>
	<div class="subMenu">
		<p>
        This area allows you to update home/sub-home page content.
        </p>
        <ul>
          <li><a href="index.cfm?md=homepage&tmp=homepage&wrap=1" target="mainFrame">Home Page</a></li>
          <li><a href="index.cfm?md=homepage&tmp=subhomepages&wrap=1" target="mainFrame">Sub-Home Pages</a></li>
        </ul>
	</div>
    </cfif>
    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "pagebuilder")>
	<h3><a href="#">Page Builder</a></h3>
	<div class="subMenu">
		<p>
        The Page Builder allows you to build pages for your website using an WYSIWYG editor.
        </p>
        <ul>
          <li><a href="index.cfm?md=pagebuilder&tmp=pagecategories&wrap=1" target="mainFrame">Page Categories</a></li>
          <li><a href="index.cfm?md=pagebuilder&tmp=pages&wrap=1" target="mainFrame">Pages</a></li>
        </ul>
	</div>
    </cfif>
    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "navigation")>
	<h3><a href="#">Navigation Manager</a></h3>
	<div class="subMenu">
		<p>
		The Navigation Manager allows you to customize navigation items of your web site.
		</p>
		<ul>
		  <li><a href="index.cfm?md=navigation&tmp=home&wrap=1" target="mainFrame">Navigation Manager</a></li>
		</ul>
	</div>
    </cfif>
    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "newsroom")>
	<h3><a href="#">Information Library</a></h3>
	<div class="subMenu">
		<p>
        The Information Library allows you to enter articles, meeting information, as well as Web resources.
        </p>
        <ul>
          <li><a href="index.cfm?md=newsroom&tmp=articlecategories&wrap=1" target="mainFrame">Categories</a></li>
          <li><a href="index.cfm?md=newsroom&tmp=articles&wrap=1" target="mainFrame">Resources</a></li>
        </ul>
	</div>
    </cfif>
        
    <cfif isSuperUser OR ListFind(accessibleModuleList, "directory")>
    <h3><a href="#">Directory Manager</a></h3>
	<div class="subMenu">
        The Directory Manager allows the creation of category directories that will be automatically plotted on a facility map.
        <ul>
          <li><a href="index.cfm?md=directory&tmp=categories&wrap=1" target="mainFrame">Categories</a></li>
          <li><a href="index.cfm?md=directory&tmp=items&wrap=1" target="mainFrame">Items</a></li>
          <li><a href="index.cfm?md=directory&tmp=maps&wrap=1" target="mainFrame">Maps</a></li>
        </ul>
	</div>
    </cfif>
    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "class")>
    <h3><a href="#">Class Manager</a></h3>
	<div class="subMenu">
        The Class Manager allows you to add, edit and delete class information.
        <ul>
          <li><a href="index.cfm?md=class&tmp=categories&wrap=1" target="mainFrame">Categories</a></li>
          <li><a href="index.cfm?md=class&tmp=courses&wrap=1" target="mainFrame">Courses</a></li>
          <li><a href="index.cfm?md=class&tmp=services&wrap=1" target="mainFrame">Services</a></li>
          <li><a href="index.cfm?md=class&tmp=curriculum&wrap=1" target="mainFrame">Curriculum</a></li>
          <li><a href="index.cfm?md=class&tmp=locations&wrap=1" target="mainFrame">Locations &amp Maps</a></li>
          <li><a href="index.cfm?md=class&tmp=fitnessclasses&wrap=1" target="mainFrame">Fitness Club Classes</a></li>
          <li><a href="index.cfm?md=class&tmp=emailsettings&wrap=1" target="mainFrame">E-Mail Settings</a></li>
          <li><a href="index.cfm?md=class&tmp=students&wrap=1" target="mainFrame">Registrants</a></li>
          <li><a href="index.cfm?md=class&tmp=fitnessmember_import&wrap=1" target="mainFrame">FC Member Import</a></li>
        </ul>
	</div>
    </cfif>
    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "event")>
    <h3><a href="#">Event Manager</a></h3>
	<div class="subMenu">
        The Event Manager allows you to add, edit and delete calendar event.
        <ul>
          <li><a href="index.cfm?md=event&tmp=categories&wrap=1" target="mainFrame">Categories</a></li>
          <li><a href="index.cfm?md=event&tmp=events&wrap=1" target="mainFrame">Events</a></li>
        </ul>
	</div>
    </cfif>
    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "doctor")>
	<h3><a href="#">Doctor Directory</a></h3>
	<div class="subMenu">
		<p>
        Doctor Directory allows you to manage physician's meta data.
        </p>
        <ul>
          <li><a href="index.cfm?md=doctor&tmp=introduction&wrap=1" target="mainFrame">Introductory Copy</a></li>
          <li><a href="index.cfm?md=doctor&tmp=metadata&wrap=1" target="mainFrame">META Data</a></li>
          <li><a href="index.cfm?md=doctor&tmp=import&wrap=1" target="mainFrame">Import</a></li>
        </ul>
	</div>
    </cfif> 

    <cfif isSuperUser OR ListFind(accessibleModuleList, "photogallery")>
	<h3><a href="#">Photo Gallery</a></h3>
	<div class="subMenu">
		<p>
        Photo Gallery allows you to create photo albums for your web site.
        </p>
        <ul>
          <li><a href="index.cfm?md=photogallery&tmp=albumcategories&wrap=1" target="mainFrame">Categories</a></li>
          <li><a href="index.cfm?md=photogallery&tmp=albums&wrap=1" target="mainFrame">Albums</a></li>
        </ul>
	</div>
    </cfif>   
        
    <cfif isSuperUser OR ListFind(accessibleModuleList, "file")>
	<h3><a href="#">File Manager</a></h3>
	<div class="subMenu">
		<p>
		The File Manager gives you the ability to create your own file structure on the server.
        All files uploaded to the server through this tool are available to all modules of your content manager system.
		</p>
		<ul>
          <li><a href="index.cfm?md=file&tmp=filemanager&wrap=1" target="mainFrame">File Manager</a></li>
        </ul>
	</div>
    </cfif>
    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "alias")>
    <h3><a href="#">URL Alias</a></h3>
	<div class="subMenu">
		<p>
		An URL alias provides a simpler way for a visitor to get to a specific location within your Web site by simply typing
        it's corresponding alias into the address bar.<!-- With this tool, you can create a very simple URL, such as http://www.yoursite.com/news,
        that will automatically redirect the user to the complex URL where the specific page actually resides.-->
		</p>
        <ul>
		  <li><a href="index.cfm?md=alias&tmp=home&wrap=1" target="mainFrame">URL Alias</a></li>
		</ul>
	</div>
    </cfif>
    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "form")>
	<h3><a href="#">Forms Manager</a></h3>
	<div class="subMenu">
		<p>
		The Forms Manager allows you to create online forms.
		</p>
		<ul>
		  <li><a href="index.cfm?md=form&tmp=forms&wrap=1" target="mainFrame">Forms</a></li>
          <li><a href="index.cfm?md=form&tmp=submissions&wrap=1" target="mainFrame">Submissions</a></li>
          <li><a href="index.cfm?md=form&tmp=submissions_breakdown&wrap=1" target="mainFrame">Breakdown</a></li>
          <li><a href="index.cfm?md=form&tmp=submissions_generate_csv&wrap=1" target="mainFrame">Generate CSV</a></li>
          <li><a href="index.cfm?md=form&tmp=submissions_search&wrap=1" target="mainFrame">Search</a></li>
		</ul>
	</div>
    </cfif>





    <cfif isSuperUser>
	<h3><a href="#">Secure Forms Manager</a></h3>
	<div class="subMenu">
		<p>
		The Secure Forms Manager allows you to create & manage secure forms, and assign admins to the forms.
		</p>
		<ul>
		  <li><a href="index.cfm?md=secureform&tmp=forms&wrap=1" target="mainFrame">Forms</a></li>
          <li><a href="index.cfm?md=secureform&tmp=submissions&wrap=1" target="mainFrame">Submissions</a></li>
          <li><a href="index.cfm?md=secureform&tmp=submissions_breakdown&wrap=1" target="mainFrame">Breakdown</a></li>
          <li><a href="index.cfm?md=secureform&tmp=submissions_generate_csv&wrap=1" target="mainFrame">Generate CSV</a></li>
		</ul>
	</div>
   <cfelseif ListFind(accessibleModuleList, "secureform")>

	<h3><a href="#">Secure Forms Manager</a></h3>
	<div class="subMenu">
		<p>
		The Secure Forms Manager allows you to view secure form submissions assigned to you.
		</p>
		<ul>
          <li><a href="index.cfm?md=secureform&tmp=submissions&wrap=1" target="mainFrame">Submissions</a></li>
          <li><a href="index.cfm?md=secureform&tmp=submissions_breakdown&wrap=1" target="mainFrame">Breakdown</a></li>
          <li><a href="index.cfm?md=secureform&tmp=submissions_generate_csv&wrap=1" target="mainFrame">Generate CSV</a></li>
		</ul>
	</div>

    </cfif>






    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "customform")>
	<h3><a href="#">Custom Forms</a></h3>
	<div class="subMenu">
		<p>
		The Custom Forms Manager allows you to manage custom-built forms such as donation for
		  <li><a href="index.cfm?md=customform&tmp=formsettings&wrap=1" target="mainFrame">Form Settings</a></li>m.
		</p>
		<ul>
          <li><a href="index.cfm?md=customform&tmp=submissions&wrap=1" target="mainFrame">Submissions</a></li>
          <!--- <li><a href="index.cfm?md=customform&tmp=export_submission_data&wrap=1" target="mainFrame">Export Submission Data</a></li> --->
          <li><a href="index.cfm?md=customform&tmp=sources&wrap=1" target="mainFrame">Source Tracking</a></li></li>
		</ul>
	</div>
    </cfif>



    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "communication")>
	<h3><a href="#">Communication Manager</a></h3>
	<div class="subMenu">
		<p>The Communication Manager allows you to maintain multiple e-mail lists and send mass e-mails to one or more e-mail lists.</p>
		<ul>
		  <li><a href="index.cfm?md=communication&tmp=emaillists&wrap=1" target="mainFrame">E-Mail Lists</a></li>
          <li><a href="index.cfm?md=communication&tmp=signupforms&wrap=1" target="mainFrame">Sign-Up Forms</a></li>
          <li><a href="index.cfm?md=communication&tmp=search_members&wrap=1" target="mainFrame">Search Members</a></li>
          <li><a href="index.cfm?md=communication&tmp=import_members&wrap=1" target="mainFrame">Import CSV</a></li>
          <li><a href="index.cfm?md=communication&tmp=messages&wrap=1" target="mainFrame">Message Center</a></li>
          <li><a href="index.cfm?md=communication&tmp=campaignreport&wrap=1" target="mainFrame">Reports</a></li>
          <cfif isMightyUser>
          <li><a href="index.cfm?md=communication&tmp=maillog&wrap=1" target="mainFrame">Mail Log</a></li>
          <li><a href="scheduledtasks/communication_scheduled_tasks.cfm?requestTimeout=200" target="mainFrame">Run Scheduled Tasks</a></li>
          </cfif>
		</ul>
	</div>
    </cfif>
    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "protected")>
	<h3><a href="#">Restricted Area</a></h3>
	<div class="subMenu">
		<p>
		This module lets you create distinct user groups that require passwords to enter certain areas of your site.
		</p>
		<ul>
          <li><a href="index.cfm?md=protected&tmp=usergroups&wrap=1" target="mainFrame">User Groups</a></li>
          <li><a href="index.cfm?md=protected&tmp=users&wrap=1" target="mainFrame">Users</a></li>
		  <li><a href="index.cfm?md=protected&tmp=restrictedarea&wrap=1" target="mainFrame">Restricted Area</a></li>
		</ul>
	</div>
    </cfif>
    
    <cfif isSuperUser OR ListFind(accessibleModuleList, "promotion")>
	<h3><a href="#">Promotion Manager</a></h3>
	<div class="subMenu">
		<p>
		This module lets you control images, copy and text throughout the site that act like advertsing to promote specific content to site visitors.
		</p>
		<ul>
          <li><a href="index.cfm?md=promotion&tmp=home&wrap=1" target="mainFrame">Promotions</a></li>
          <li><a href="index.cfm?md=promotion&tmp=report_overall&wrap=1" target="mainFrame">Report - Overall</a></li>
          <li><a href="index.cfm?md=promotion&tmp=report_detail&wrap=1" target="mainFrame">Report - Detail</a></li>
		</ul>
	</div>
    </cfif>

    <cfif isSuperUser OR ListFind(accessibleModuleList, "marketing")>
	<h3><a href="#">Viral Marketing Stats</a></h3>
	<div class="subMenu">
		<p>
		View stats of 'Email Friend' and 'Printer-friendly Version'.
		</p>
		<ul>
          <li><a href="index.cfm?md=marketing&tmp=message&wrap=1" target="mainFrame">E-Mail Message</a></li>
		  <li><a href="index.cfm?md=marketing&tmp=stats&wrap=1" target="mainFrame">View Stats</a></li>
		</ul>
	</div>
    </cfif>    
    
    <cfif isMightyUser>
    <h3><a href="#">Site Search</a></h3>
	<div class="subMenu">
		<p>
		This area allows you to re-index your site content. 
		</p>
        <ul>
		  <li><a href="index.cfm?md=sitesearch&tmp=home&wrap=1" target="mainFrame">Site Search</a></li>
		</ul>
	</div>
    </cfif>
    
    <cfif isMightyUser>
    <h3><a href="#">Reset Application</a></h3>
	<div class="subMenu">
		<p>
		Reset Application let you reset the application or your session. 
		</p>
        <ul>
		  <li><a href="reset.cfm" target="mainFrame">Reset</a></li>
		</ul>
	</div>
    </cfif>
</div>
</cfif>