<cfparam name="URL.md" default="">
<cfparam name="URL.tmp" default="">
<cfparam name="URL.wrap" default="0">

<cfif URL.wrap>
  <cfinclude template="wrappers/_top.cfm">
  <cfinclude template="modules/#URL.md#/_#URL.tmp#.cfm">
  <cfinclude template="wrappers/_bottom.cfm">
<cfelse>
  <cfinclude template="modules/#URL.md#/_#URL.tmp#.cfm">
</cfif>