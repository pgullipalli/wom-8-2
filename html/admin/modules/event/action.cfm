<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  EM=CreateObject("component", "com.EventAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";
  
  switch (URL.task) {
    case "addCategory":
	  EM.addCategory(argumentCollection=Form);
	  break;
	  
	case "editCategory":
	  EM.editCategory(argumentCollection=Form);
	  break;
	  
	case "deleteCategory":
	  if (NOT EM.deleteCategory(URL.catID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="It appears that there are still items in the category that you tried to delete. " &
	  		 "Please make sure you have deleted all items in this category before deleting this category.";
	  }
	  break;
	  
	case "addEvent":
	  if (Not EM.addEvent(argumentcollection=FORM)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="Please select at least one category for this new event.";
	  }
	  break;
	  
	case "editEvent":
	  if (Not EM.editEvent(argumentcollection=FORM)) {
	    returnedStruct.SUCCESS=false;
		returnedStruct.MESSAGE="Please select at least one category for this new event.";
	  }
	  break;
	  
	case "deleteEvent":
      EM.deleteEvent(URL.eventID);
      break;
	
	default:
	  break;
  }
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>