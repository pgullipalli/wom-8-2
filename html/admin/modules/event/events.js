var pageNum=1;//initial page number

$(function () {	
	loadEventList();
	
	$("#frmNavigator select[name='categoryID']").change(function() {
	  pageNum=1;
	  loadEventList();	  
	});
});

function loadEventList(pNum) {
  if (pNum) pageNum = pNum;
  var catID=$("#frmNavigator select[name='categoryID']").val();	
  
  var url="index.cfm?md=event&tmp=snip_event_list&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function addEvent() {
  var catID=$("#frmNavigator select[name='categoryID']").val();	
  xsite.createModalDialog({
	windowName: 'winAddEvent',
	title: 'Add Event',
	width: 650,
	position: 'center-up',
	url: 'index.cfm?md=event&tmp=snip_add_event&catID=' + catID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {//create date pickers for date fields
	  $(".dateField").datepicker();  
	},
	buttons: {
	  'Cancel': function() { $("#winAddEvent").dialog('close'); },
	  ' Add ': submitForm_AddEvent
	}
  }).dialog('open');
  
  function submitForm_AddEvent() {
	var form = $("#frmAddEvent");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=event&task=addEvent',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddEvent").dialog('close');
		loadEventList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editEvent(evtID) {
  var form = $("#frmEventList")[0];
  var eventID = null;
  if (evtID) eventID = evtID;
  else eventID = xsite.getCheckedValue(form.eventID);

  if (!eventID) {
	alert("Please select an event to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditEvent',
	title: 'Edit Event',
	width: 650,
	position: 'center-up',
	url: 'index.cfm?md=event&tmp=snip_edit_event&eventID=' + eventID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {//create date pickers for date fields
	  $(".dateField").datepicker();  
	},
	buttons: {
	  'Cancel': function() { $("#winEditEvent").dialog('close'); },
	  ' Save ': submitForm_EditEvent
	}
  }).dialog('open');
	
  function submitForm_EditEvent() {
	var form = $("#frmEditEvent");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=event&task=editEvent',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
		
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditEvent").dialog('close');
		loadEventList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteEvent() {
  var form = $("#frmEventList")[0];
  var eventID = xsite.getCheckedValue(form.eventID);
  if (!eventID) {
	alert("Please select an item to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected item?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=event&task=deleteEvent&eventID=' + eventID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadEventList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function toggleTimeFields(allDayField) {
  if (allDayField.checked) {
	$(".timeField").hide();
  } else {
	$(".timeField").show();
  }
}
