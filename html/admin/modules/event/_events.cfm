<cfparam name="URL.catID" default="0">

<script type="text/javascript" src="modules/event/events.js"></script>

<cfset EM=CreateObject("component", "com.EventAdmin").init()>
<cfset allCategories=EM.getAllCategories()>
<cfif URL.catID IS 0>
  <cfset URL.catID=EM.getFirstCategoryID()>
  <cfif URL.catID IS 0><!--- no categories were ever created --->
    <cflocation url="index.cfm?md=event&tmp=categories&wrap=1">
  </cfif>
</cfif>

<div class="header">EVENT MANAGER &gt; Events</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addEvent();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editEvent(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteEvent();">Delete</a>
</div>

<br /><br /><br />

<cfoutput>
<form id="frmNavigator">
  Category
  <select name="categoryID" style="min-width:250px;">
    <cfloop query="allCategories">
    <option value="#categoryID#"<cfif URL.catID IS categoryID> selected</cfif>>#categoryName#</option>
    </cfloop>
  </select>
</form>
</cfoutput>


<div id="mainDiv"></div>