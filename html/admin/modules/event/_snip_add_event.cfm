<cfparam name="URL.catID">
<cfset EM=CreateObject("component", "com.EventAdmin").init()>
<cfset allCategories=EM.getAllCategories()>
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset allLocations=CL.getLocations()>

<form id="frmAddEvent" name="frmAddEvent" onsubmit="return false">
<div class="row">
	<div class="col-30-right"><b>Categories:</b><br /><small>(Check all that apply.)</small></div>
    <div class="col-68-left">
      <cfset numCategories=allCategories.recordcount>
      <cfset numRows=Ceiling(numCategories/3)>
      <cfoutput><cfset idx = 0>
      <cfloop index="row" from="1" to="#numRows#">
        <div class="row">
        <cfloop index="col" from="1" to="3">
          <cfset idx = (row - 1) * 3 + col>
          <cfif idx LTE numCategories>
            <cfset categoryID=allCategories.categoryID[idx]>
            <cfset categoryName=allCategories.categoryName[idx]>
            <div class="col-30-left">
              <input type="checkbox" name="categoryIDList" value="#categoryID#"<cfif URL.catID Is categoryID> checked</cfif> />
              #categoryName#
            </div>
          </cfif>
        </cfloop>
        </div>
      </cfloop>
      </cfoutput>
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Event Name:</b></div>
    <div class="col-68-left"><input type="text" name="eventName" maxlength="255" value="" style="width:350px;" validate="required:true"></div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="published" value="1" /></div>
    <div class="col-68-left">Publish this item</div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="allDayEvent" value="1" checked onclick="toggleTimeFields(this);" /></div>
    <div class="col-68-left">This is an all-day event</div>
</div>

<div class="row">
	<div class="col-30-right"><b>When:</b></div>
    <div class="col-68-left">
      <div style="width:30px;float:left;">From</div>
      <input type="text" name="startDateTime" class="dateField" style="width:80px;" validate="required:true, date:true" />
      <span class="timeField" style="display:none;">
      <select name="startDateTime_hh">
        <option value="12">12</option>
        <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
      </select>
      <select name="startDateTime_mm">
        <cfoutput><cfloop index="mm" from="0" to="59"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
      </select>
      <select name="startDateTime_ampm">
        <option value="am">AM</option><option value="pm">PM</option>
      </select>
      </span>
      <div style="padding:0px 0px; margin:0px 0px;height:5px;width:100px;clear:left;"></div>
      <div style="width:30px;float:left;">To</div>
      <input type="text" name="endDateTime" class="dateField" style="width:80px;" validate="required:true, date:true" />
      <span class="timeField" style="display:none;">
      <select name="endDateTime_hh">
        <option value="12">12</option>
        <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
      </select>
      <select name="endDateTime_mm">
        <cfoutput><cfloop index="mm" from="0" to="59"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
      </select>
      <select name="endDateTime_ampm">
        <option value="am">AM</option><option value="pm">PM</option>
      </select>
      </span>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Location:</div>
    <div class="col-68-left"><input type="text" name="eventLocation" maxlength="150" value="" style="width:320px;"></div>
</div>
<div class="row">
	<div class="col-30-right">Event URL:</div>
    <div class="col-68-left"><input type="text" name="eventURL" maxlength="150" value="" style="width:320px;"></div>
</div>

<div class="row">
	<div class="col-30-right">Short Description:</div>
    <div class="col-68-left">
      <textarea name="shortDescription" style="width:320px;height:50px"></textarea>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Event Image:</div>
    <div class="col-68-left">
      <div class="row">
        <input type="hidden" name="eventImage" />
        <CF_SelectFile formName="frmAddEvent" fieldName="eventImage" previewID="preview1-1" previewWidth="148">
        <small>(148px x 148px)</small>
        <div id="preview1-1"></div> 
      </div>
      <div class="row">
        Caption:<br />
        <textarea name="eventImageCaption" style="width:320px;height:30px;"></textarea>
      </div>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Long Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="false"
            formName="frmAddEvent"
            fieldName="longDescription"
            cols="60"
            rows="3"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Long Description">
    </div>
</div>
</form>