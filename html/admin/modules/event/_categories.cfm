<script type="text/javascript" src="modules/event/categories.js"></script>

<div class="header">EVENT MANAGER &gt; Categories</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addCategory();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editCategory();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteCategory();">Delete</a>
</div>


<div id="mainDiv"></div>