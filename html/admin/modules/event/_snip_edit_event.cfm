<cfparam name="URL.eventID">
<cfset EM=CreateObject("component", "com.EventAdmin").init()>
<cfset Util=CreateObject("component","com.Utility").init()>
<cfset allCategories=EM.getAllCategories()>
<cfset eventInfo=EM.getEventByID(URL.eventID)>
<cfset categoryIDList=EM.getCategoryIDListByEventID(URL.eventID)>
<cfset startDateTimeStruct=Util.convertDateTimeObjectToString(DateAdd("d",0,eventInfo.startDateTime))>
<cfset endDateTimeStruct=Util.convertDateTimeObjectToString(DateAdd("d",0,eventInfo.endDateTime))>

<cfoutput query="eventInfo">
<form id="frmEditEvent" name="frmEditEvent" onsubmit="return false">
<input type="hidden" name="eventID" value="#URL.eventID#" />
<div class="row">
	<div class="col-30-right"><b>Categories:</b><br /><small>(Check all that apply.)</small></div>
    <div class="col-68-left">
      <cfset numCategories=allCategories.recordcount>
      <cfset numRows=Ceiling(numCategories/3)>
      <cfset idx = 0>
      <cfloop index="row" from="1" to="#numRows#">
        <div class="row">
        <cfloop index="col" from="1" to="3">
          <cfset idx = (row - 1) * 3 + col>
          <cfif idx LTE numCategories>
            <cfset categoryID=allCategories.categoryID[idx]>
            <cfset categoryName=allCategories.categoryName[idx]>
            <div class="col-30-left">
              <input type="checkbox" name="categoryIDList" value="#categoryID#"<cfif ListFind(categoryIDList, categoryID) GT 0> checked</cfif> />
              #categoryName#
            </div>
          </cfif>
        </cfloop>
        </div>
      </cfloop>
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Event Name:</b></div>
    <div class="col-68-left"><input type="text" name="eventName" maxlength="255" style="width:350px;" value="#HTMLEditFormat(eventName)#" validate="required:true"></div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="published" value="1" <cfif published>checked</cfif> /></div>
    <div class="col-68-left">Publish this item</div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="allDayEvent" value="1" <cfif allDayEvent>checked</cfif> onclick="toggleTimeFields(this);" /></div>
    <div class="col-68-left">This is an all-day event</div>
</div>

<div class="row">
	<div class="col-30-right"><b>When:</b></div>
    <div class="col-68-left">
      <div style="width:30px;float:left;">From</div>
      <input type="text" name="startDateTime" class="dateField" style="width:80px;" value="#startDateTimeStruct.date#" validate="required:true, date:true" />
      <span class="timeField" <cfif allDayEvent>style="display:none;"<cfelse>style="display:inline;"</cfif>>
      <select name="startDateTime_hh">
        <option value="12">12</option>
        <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"<cfif startDateTimeStruct.hh Is hh> selected</cfif>><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
      </select>
      <select name="startDateTime_mm">
        <cfoutput><cfloop index="mm" from="0" to="59"><option value="#mm#"<cfif startDateTimeStruct.mm Is mm> selected</cfif>><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
      </select>
      <select name="startDateTime_ampm">
        <option value="am"<cfif startDateTimeStruct.ampm Is "AM"> selected</cfif>>AM</option><option value="pm"<cfif startDateTimeStruct.ampm Is "PM"> selected</cfif>>PM</option>
      </select>
      </span>
      <div style="padding:0px 0px; margin:0px 0px;height:5px;width:100px;clear:left;"></div>
      <div style="width:30px;float:left;">To</div>
      <input type="text" name="endDateTime" class="dateField" style="width:80px;" value="#endDateTimeStruct.date#" validate="required:true, date:true" />
      <span class="timeField" <cfif allDayEvent>style="display:none;"<cfelse>style="display:inline;"</cfif>>
      <select name="endDateTime_hh">
        <option value="12">12</option>
        <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"<cfif endDateTimeStruct.hh Is hh> selected</cfif>><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
      </select>
      <select name="endDateTime_mm">
        <cfoutput><cfloop index="mm" from="0" to="59"><option value="#mm#"<cfif endDateTimeStruct.mm Is mm> selected</cfif>><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
      </select>
      <select name="endDateTime_ampm">
        <option value="am"<cfif endDateTimeStruct.ampm Is "AM"> selected</cfif>>AM</option><option value="pm"<cfif endDateTimeStruct.ampm Is "PM"> selected</cfif>>PM</option>
      </select>
      </span>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Location:</div>
    <div class="col-68-left"><input type="text" name="eventLocation" maxlength="150" value="#HTMLEditFormat(eventLocation)#" style="width:320px;"></div>
</div>
<div class="row">
	<div class="col-30-right">More Info URL:</div>
    <div class="col-68-left"><input type="text" name="eventURL" maxlength="150" value="#HTMLEditFormat(eventURL)#" style="width:320px;"></div>
</div>

<div class="row">
	<div class="col-30-right">Event Image:</div>
    <div class="col-68-left">
      <div class="row">
        <input type="hidden" name="eventImage" value="#HTMLEditFormat(eventImage)#" />
        <CF_SelectFile formName="frmEditEvent" fieldName="eventImage" previewID="preview2-1" previewWidth="148">
        <small>(148px x 148px)</small>
       <cfif Compare(eventImage, "")>
        <div id="preview2-1"><img src="/#eventImage#" border="0" width="148" /></div>  
        <cfelse>
        <div id="preview2-1"></div>   
        </cfif> 
      </div>
      <div class="row">
        Caption:<br />
        <textarea name="eventImageCaption" style="width:320px;height:30px;">#HTMLEditFormat(eventImageCaption)#</textarea>
      </div>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Short Description:</div>
    <div class="col-68-left">
      <textarea name="shortDescription" style="width:320px;height:50px">#HTMLEditFormat(shortDescription)#</textarea>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Long Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="false"
            formName="frmEditEvent"
            fieldName="longDescription"
            cols="60"
            rows="3"
            HTMLContent="#longDescription#"
            displayHTMLSource="true"
            editorHeader="Edit Long Description">
    </div>
</div>
</form>
</cfoutput>