<cfset EM=CreateObject("component", "com.EventAdmin").init()>
<cfset categorySummary=EM.getCategorySummary()>

<cfif categorySummary.recordcount IS 0>
  <p>
  It appears that there are no categories created.
  Please click on the 'New' button in the action bar above to create the first category.
  </p>
<cfelse>
<form id="frmCategoryList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="250">Category Name</th>
    <th width="100"># of Events</th>
    <th>URL (click to preview)</th>
  </tr>
  <cfoutput query="categorySummary">
  <tr>
    <td align="center"><input type="radio" name="categoryID" value="#categoryID#"></td>
    <td><a href="index.cfm?md=event&tmp=events&wrap=1&catID=#categoryID#">#categoryName#</a></td>
    <td align="center"><a href="index.cfm?md=event&tmp=events&wrap=1&catID=#categoryID#">#numEvents#</a></td>
    <td align="center"><a href="/index.cfm?md=event&tmp=category&catID=#categoryID#" target="_blank">/index.cfm?md=event&amp;tmp=category&amp;catID=#categoryID#</a></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>