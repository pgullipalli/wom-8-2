<cfparam name="URL.catID" default="0">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset EM=CreateObject("component", "com.EventAdmin").init()>

<cfset structEvents=EM.getEventsByCategoryID(categoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
<cfif structEvents.numAllItems IS 0>
  <p>
  It appears that there are no events created in this category.
  Please click on the 'New' button in the action bar above to create the first event.
  </p>
</cfif>

<cfif structEvents.numDisplayedItems GT 0>
<form id="frmEventList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th>Event Name</th>
    <th>Event Date/Time</th>
    <th>Location</th>
    <th width="100">Publish?</th>
  </tr>  
  <cfoutput query="structEvents.events">
  <tr>
    <td align="center"><input type="radio" name="eventID" value="#eventID#"></td>
	<td><a href="##" onclick="editEvent('#eventID#')">#eventName#</a></td> 
    <td>
      <cfset startDate=DateFormat(startDateTime,"mm/dd/yyyy")><cfset endDate=DateFormat(endDateTime,"mm/dd/yyyy")>
      <cfif allDayEvent>
        #startDate#<cfif Compare(startDate,endDate)> - #endDate#</cfif>
      <cfelse>
        #startDate# #TimeFormat(startDateTime,"hh:mm tt")# -
	    <cfif Compare(startDate,endDate)>#endDate#</cfif> #TimeFormat(endDateTime,"hh:mm tt")#
      </cfif>
    </td>
    <td>#eventLocation#&nbsp;</td>
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif structEvents.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structEvents.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadEventList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>