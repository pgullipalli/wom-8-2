<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  SM=CreateObject("component", "com.SiteSearchAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";
 
  switch (URL.task) {	  
	case "indexSiteContent":
	  SM.indexSiteContent(directoryPath="#Application.siteDirectoryRoot#/assets/docs", urlPath="#Application.siteURLRoot#/assets/docs", searchableFiles="#Application.sitesearch.searchableFiles#");
      break;
      
    default:
      break;
  }
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>