<script type="text/javascript" src="modules/sitesearch/home.js"></script>

<cfset SM=CreateObject("component", "com.SiteSearchAdmin").init()>

<div class="header">SITE SEARCH</div>

<cfset collectionName=SM.getSiteContentCollectionName()>

<cfif Compare(collectionName,"")>
<form id="frmSiteSearch">
  <cfset dbUpdateNeeded=SM.getSiteContentIndexStatus('db')>
  <cfset classUpdateNeeded=SM.getSiteContentIndexStatus('class')>
  <cfset fileUpdateNeeded=SM.getSiteContentIndexStatus('file')>
  
  <cfif dbUpdateNeeded OR fileUpdateNeeded OR classUpdateNeeded>  
  <p>
  Your site content has been updated. To re-index your site content now, 
  <a href="javascript:indexSiteContent()">click here ONLY ONCE</a>.
  </p>
  <cfelse>
  <p>Your site search feature has been activated. Your site content has been indexed. No re-indexing is necessary now.</p>
  </cfif>
</form>
</cfif>
