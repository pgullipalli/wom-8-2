function indexSiteContent() {
  if (!confirm("You are about to re-index your site. This will take several minutes. Are you sure you want to proceed?")) {
	return;		
  } else {
	xsite.showWaitingDialog({openCallback:function() {
		$('#frmSiteSearch').ajaxSubmit({ 
		  url: 'action.cfm?md=sitesearch&task=indexSiteContent',
		  type: 'get',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});}
	});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  window.location.reload(1);	
	} else {
	  xsite.closeWaitingDialog();
	  alert (jsonData.MESSAGE);
	}
  }  	
}