<cfparam name="URL.catID" default="0">
<cfparam name="URL.keyword" default="">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
<cfif Compare(URL.keyword,"")>
  <cfset structResult=PG.getAlbumsByKeyword(keyword=URL.keyword, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
<cfelse>
  <cfset structResult=PG.getAlbumsByCategoryID(albumCategoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
</cfif>

<cfif structResult.numAllItems IS 0>
  <p>
  No photo galleries have been created.
  Please click on the 'New' button in the action bar above to create the first album.
  </p>
<cfelse>
  <cfif structResult.numDisplayedItems GT 0>
    <form id="frmAlbumList">
    <div class="itemList">
    <table>
      <tr>
        <th width="40">&nbsp;</th>
        <th width="150">Cover</th>
        <th width="250">Album Title (click to update photos)</th>
        <th width="100">Date</th>
        <th width="100"># of Photos</th>
        <th width="100">Publish?</th>
        <th width="100">Featured</th>
        <th>URL (click to preview)</th>
      </tr>  
      <cfoutput query="structResult.albums">
      <tr>
        <td align="center"><input type="radio" name="albumID" value="#albumID#"></td>
        <td align="center"><cfif Compare(coverImage, "")><img src="/#coverImage#" width="100" border="0"><cfelse><img src="images/pic.gif" width="100" height="50" border="0"></cfif></td>
        <td><a href="index.cfm?md=photogallery&tmp=photos&albumID=#albumID#&keyword=#URLEncodedFormat(URL.keyword)#&catID=#URL.catID#&wrap=1">#albumTitle#</a></td>
        <td align="center">#DateFormat(albumDate, "mm/dd/yy")#</td>
        <td align="center"><a href="index.cfm?md=photogallery&tmp=photos&albumID=#albumID#&keyword=#URLEncodedFormat(URL.keyword)#&catID=#URL.catID#&wrap=1">#numPhotos#</a></td>
        <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
        <td align="center"><cfif featuredOnHomepage>Yes<cfelse>No</cfif></td>
        <td align="center"><a href="/index.cfm?md=photogallery&amp;tmp=album_html&amp;catid=#URL.catID#&amp;albumid=#albumID#" target="_blank">/index.cfm?md=photogallery&amp;tmp=album_html&amp;catid=#URL.catID#&amp;albumid=#albumID#</a></td>
      </tr>
      </cfoutput>
    </table>
    </div>
    </form>

	<cfif structResult.numAllItems GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(structResult.numAllItems/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadAlbumList(#idx#)">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
    </cfif>
  </cfif>
</cfif>