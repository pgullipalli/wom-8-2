<cfparam name="URL.albumID">

<cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
<cfset allCategories=PG.getAllAlbumCategories()>
<cfset albumInfo=PG.getAlbumByID(URL.albumID)>
<cfset albumCategoryIDList=PG.getAlbumCategoryIDListByAlbumID(URL.albumID)>

<cfoutput query="albumInfo">
<div class="snip">
<form id="frmEditAlbum" name="frmEditAlbum" onsubmit="return false">
<input type="hidden" name="albumID" value="#URL.albumID#">
<div class="row">
	<div class="col-30-right"><b>Categories:</b><br /><small>(Check all that apply.)</small></div>
    <div class="col-68-left">
      <cfset numCategories=allCategories.recordcount>
      <cfset numRows=Ceiling(numCategories/3)>
      <cfset idx = 0>
      <cfloop index="row" from="1" to="#numRows#">
        <div class="row">
        <cfloop index="col" from="1" to="3">
          <cfset idx = (row - 1) * 3 + col>
          <cfif idx LTE numCategories>
            <cfset albumCategoryID=allCategories.albumCategoryID[idx]>
            <cfset albumCategory=allCategories.albumCategory[idx]>
            <div class="col-30-left">
              <input type="checkbox" name="albumCategoryIDList" value="#albumCategoryID#"<cfif ListFind(albumCategoryIDList, albumCategoryID) GT 0> checked</cfif> />
              #albumCategory#
            </div>
          </cfif>
        </cfloop>
        </div>
      </cfloop>
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Album Title:</b></div>
    <div class="col-68-left">
      <input type="text" name="albumTitle" style="width:350px;" maxlength="100" validate="required:true" value="#HTMLEditFormat(albumTitle)#" /><br />
      <input type="checkbox" name="published" value="1" <cfif published>checked</cfif> /> Publish this album
      <input type="checkbox" name="featuredOnHomepage" value="1" <cfif featuredOnHomepage>checked</cfif> /> Featured on home/sub-home page
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Album Date:</b></div>
    <div class="col-68-left">
	  <input type="text" name="albumDate" id="albumDate2" style="width:100px;" validate="required:true, date:true" value="#DateFormat(albumDate, "mm/dd/yyyy")#" />      
	</div>
</div>

<div class="row">
	<div class="col-30-right">Cover Image:</div>
    <div class="col-68-left">
      <input type="hidden" name="coverImage" value="#HTMLEditFormat(coverImage)#">
      <CF_SelectFile formName="frmEditAlbum" fieldName="coverImage" previewID="preview2" previewWidth="150">
      <cfif Compare(coverImage,"")>
      <div id="preview2"><img src="/#coverImage#" width="150"></div>
      <cfelse>
      <div id="preview2"></div>
      </cfif>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Album Description:</div>
    <div class="col-68-left">
      <textarea name="description" style="width:350px;height:50px;">#HTMLEditFormat(description)#</textarea>
    </div>
</div>

</center>
</form>
</div>
</cfoutput>