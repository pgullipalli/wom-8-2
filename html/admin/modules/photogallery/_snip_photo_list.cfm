<cfparam name="URL.albumID">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="10">

<cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
<cfset resultStruct = PG.getPhotosByAlbumID(albumID=URL.albumID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>

<cfif resultStruct.numDisplayedItems GT 0>
<form id="frmPhotoList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th width="100">Photo</th>
    <th width="100">Up/Down</th>
    <th width="300">Photo Title</th>
    <th width="300">Photo File</th>
    <th width="100">Published?</th>
    <th>&nbsp;</th>
  </tr>  
  <cfoutput query="resultStruct.photos">
  <tr>
    <td align="center"><input type="radio" name="photoID" value="#photoID#" /></td>
    <td align="center">
      <cfif Compare(photoThumbnail, "")>
        <img src="/#photoThumbnail#" border="0" style="max-width:75px; max-height:75px;">
      <cfelseif Compare(photoFile,"")>
		<img src="/#photoFile#" border="0" style="max-width:75px; max-height:75px;">
      <cfelse>
		<img src="images/pic.gif" border="0" width="75" height="40">
	  </cfif>
    </td>
    <td align="center">
      <a href="##" onclick="movePhoto('#photoID#','up')"><img src="images/icon_up.gif" border="0" align="absbottom" alt="move up"></a>
      <a href="##" onclick="movePhoto('#photoID#','down')"><img src="images/icon_down.gif" border="0" align="absbottom" alt="move down"></a>
    </td>
    <td><a href="##" onclick="editPhoto('#photoID#')">#title#</a>&nbsp;</td>
    <td>#photoFile# (#fileSize#)</td>
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
    <td>&nbsp;</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif resultStruct.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(resultStruct.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadPhotoList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>
