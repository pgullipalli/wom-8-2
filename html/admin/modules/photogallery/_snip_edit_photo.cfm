<cfparam name="URL.photoID">

<cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
<cfset photoInfo=PG.getPhotoByID(URL.photoID)>

<cfoutput query="photoInfo">
<form id="frmEditPhoto" name="frmEditPhoto" onsubmit="return false">
<input type="hidden" name="photoID" value="#URL.photoID#">
<div class="row">
	<div class="col-30-right">Photo Title:</div>
    <div class="col-68-left">
      <input type="text" name="title" style="width:350px;" value="#HTMLEditFormat(title)#" /><br />
      <input type="checkbox" name="published" value="1" <cfif published>checked</cfif> /> Publish this photo
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Photo Image:</b></div>
    <div class="col-68-left">
      <input type="hidden" name="photoFile" value="#HTMLEditFormat(photoFile)#" />
      <CF_SelectFile formName="frmEditPhoto" fieldName="photoFile" previewID="preview3" previewWidth="150">
      (Width: #Application.photogallery.fullsizeWidth#px)
      <cfif Compare(photoFile, "")>
      <div id="preview3"><img src="/#photoFile#" width="150"></div>  
      <cfelse>
      <div id="preview3"></div>   
      </cfif>
      <div class="row">
      Orientation:
      <input type="radio" name="orientation" value="H" <cfif orientation Is "H">checked</cfif> /> Horizontal
      <input type="radio" name="orientation" value="V" <cfif orientation Is "V">checked</cfif> /> Vertical
      <input type="radio" name="orientation" value="S" <cfif orientation Is "S">checked</cfif> /> Square
      </div>
    </div>
</div>
<div class="row">
	<div class="col-30-right">Photo Thumbnail:</div>
    <div class="col-68-left">
      <input type="hidden" name="photoThumbnail" value="#HTMLEditFormat(photoThumbnail)#" />
      <input type="checkbox" name="resizeForThumbnail" value="1" /> Resize the full-size image to a thumbnail image; OR<br />
      <CF_SelectFile formName="frmEditPhoto" fieldName="photoThumbnail" previewID="preview4" previewWidth="#Application.photogallery.thumbnailWidth#">
      (Width: #Application.photogallery.thumbnailWidth#px)
      <cfif Compare(photoThumbnail, "")>
      <div id="preview4"><img src="/#photoThumbnail#" border="0" width="#Application.photogallery.thumbnailWidth#" /></div>  
      <cfelse>
      <div id="preview4"></div>   
      </cfif> 
    </div>
</div>
<div class="row">
	<div class="col-30-right">Description:</div>
    <div class="col-68-left"><textarea name="description" style="width:350px;height:35px;">#HTMLEditFormat(description)#</textarea></div>
</div>
<div class="row">
	<div class="col-30-right">Credit:</div>
    <div class="col-68-left"><input type="text" name="author" style="width:350px;" value="#HTMLEditFormat(author)#" /></div>
</div>
</form>
</cfoutput>