<cfparam name="URL.catID">

<cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
<cfset allCategories=PG.getAllAlbumCategories()>

<div class="snip">
<form id="frmAddAlbum" name="frmAddAlbum" onsubmit="return false">
<div class="row">
	<div class="col-30-right"><b>Categories:</b><br /><small>(Check all that apply.)</small></div>
    <div class="col-68-left">
      <cfset numCategories=allCategories.recordcount>
      <cfset numRows=Ceiling(numCategories/3)>
      <cfoutput><cfset idx = 0>
      <cfloop index="row" from="1" to="#numRows#">
        <div class="row">
        <cfloop index="col" from="1" to="3">
          <cfset idx = (row - 1) * 3 + col>
          <cfif idx LTE numCategories>
            <cfset albumCategoryID=allCategories.albumCategoryID[idx]>
            <cfset albumCategory=allCategories.albumCategory[idx]>
            <div class="col-30-left">
              <input type="checkbox" name="albumCategoryIDList" value="#albumCategoryID#"<cfif URL.catID Is albumCategoryID> checked</cfif> />
              #albumCategory#
            </div>
          </cfif>
        </cfloop>
        </div>
      </cfloop>
      </cfoutput>
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Album Title:</b></div>
    <div class="col-68-left">
      <input type="text" name="albumTitle" style="width:350px;" maxlength="100" validate="required:true" value="" /><br />      
      <input type="checkbox" name="published" value="1" /> Publish this album
      <input type="checkbox" name="featuredOnHomepage" value="1" /> Featured on home/sub-home page
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Album Date:</b></div>
    <div class="col-68-left">
	  <input type="text" name="albumDate" id="albumDate" style="width:100px;" validate="required:true, date:true" />
	</div>
</div>

<div class="row">
	<div class="col-30-right">Cover Image:</div>
    <div class="col-68-left">
      <input type="hidden" name="coverImage" value="">
      <CF_SelectFile formName="frmAddAlbum" fieldName="coverImage" previewID="preview1" previewWidth="150">
      <div id="preview1"></div>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Album Description:</div>
    <div class="col-68-left">
      <textarea name="description" style="width:350px;height:50px;"></textarea>
    </div>
</div>

</center>
</form>
</div>
