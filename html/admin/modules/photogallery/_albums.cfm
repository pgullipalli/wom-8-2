<cfparam name="URL.catID" default="0">
<cfparam name="URL.pageNum" default="1">

<script type="text/javascript" src="modules/photogallery/albums.js"></script>

<cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
<cfset allCategories=PG.getAllAlbumCategories()>
<cfif URL.catID IS 0>
  <cfset URL.catID=PG.getFirstAlbumCategoryID()>
  <cfif URL.catID IS 0><!--- no categories were ever created --->
    <cflocation url="index.cfm?md=photogallery&tmp=albumcategories&wrap=1">
  </cfif>
</cfif>

<div class="header">PHOTO GALLERY &gt; Albums</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addAlbum();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editAlbum();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteAlbum();">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="managePhotos();">Manage Photos</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="previewHTML();">Preview (HTML)</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button130" onclick="previewSlideshow();">Preview (Slideshow)</a>
</div>

<br /><br /><br />

<cfoutput>
<form id="frmNavigator">
  Category
  <select name="albumCategoryID" style="min-width:250px;">
    <cfloop query="allCategories">
    <option value="#albumCategoryID#"<cfif URL.catID IS albumCategoryID> selected</cfif>>#albumCategory#</option>
    </cfloop>
  </select>
  <input type="hidden" name="pageNum" value="#URL.pageNum#" />
  &nbsp;&nbsp;&nbsp;&nbsp;
  <a href="index.cfm?md=photogallery&tmp=searchalbums&wrap=1">Search Albums</a>
</form>
</cfoutput>


<div id="mainDiv"></div>