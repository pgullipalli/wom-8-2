<cfparam name="URL.keyword" default="">
<cfparam name="URL.pageNum" default="1">

<script type="text/javascript" src="modules/photogallery/searchalbums.js"></script>

<cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
<cfset allCategories=PG.getAllAlbumCategories()>
<cfif allCategories.recordcount IS 0>
  <!--- no page categories were ever created --->
  <cflocation url="index.cfm?md=photogallery&tmp=albumcategories&wrap=1">
</cfif>

<div class="header">PHOTO GALLERY &gt; Search Albums</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editAlbum();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteAlbum();">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="managePhotos();">Manage Photos</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="previewHTML();">Preview (HTML)</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button130" onclick="previewSlideshow();">Preview (Slideshow)</a>
</div>

<br /><br /><br />

<cfoutput>
<form id="frmSearchAlbums" onsubmit="return false">
Enter a keyword:
<input type="text" name="keyword" size="20" maxlength="30" value="#HTMLEditFormat(URL.keyword)#">
<input type="button" value=" Submit " onclick="loadAlbumList(1);">
<input type="hidden" name="pageNum" value="#URL.pageNum#" />
&nbsp;&nbsp;&nbsp;&nbsp;
<a href="index.cfm?md=photogallery&tmp=albums&wrap=1">Browse By Categories</a>
</form>
</cfoutput>

<div id="mainDiv"></div>
