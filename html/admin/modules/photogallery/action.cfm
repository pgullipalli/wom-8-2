<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  PG=CreateObject("component", "com.PhotoGalleryAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";

  switch (URL.task) {
    case "addAlbumCategory":
	  PG.addAlbumCategory(argumentCollection=Form);
      break;

    case "editAlbumCategory":
	  PG.editAlbumCategory(argumentCollection=Form);
      break;

    case "deleteAlbumCategory":
      if (NOT PG.deleteAlbumCategory(URL.catID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="It appears that there are still photo albums in the category that you tried to delete. " &
	  		 "Please make sure you have deleted all photo albums in this category before deleting this category.";	  
      }
      break;

    case "addAlbum":  	 
      if (Not PG.addAlbum(argumentcollection=FORM)) {
        returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="Please select at least one category for this new album.";
      }
      break;
  
    case "editAlbum":  
      if (Not PG.editAlbum(argumentcollection=FORM)) {
        returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="Please select at least one category for this new item.";
      }
      break;  

    case "deleteAlbum":
      if (NOT PG.deleteAlbum(URL.albumID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="It appears that there are still photos in the album that you tried to delete. " &
	  		 "Please make sure you have deleted all photos in this album before deleting this album.";	  
      }
      break;
  
    case "addPhoto":  	
      PG.addPhoto(argumentCollection=Form);
      break;
  
    case "editPhoto":  	
      PG.editPhoto(argumentCollection=Form);
      break;
  
    case "deletePhoto":
      PG.deletePhoto(URL.photoID);
      break;
  
    case "movePhoto":
      PG.movePhoto(photoID=URL.photoID, direction=URL.direction);	
      break;
      
    default:
      break;
  }
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>