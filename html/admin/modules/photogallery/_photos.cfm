<cfparam name="URL.catID" default="0">
<cfparam name="URL.keyword" default="">
<cfparam name="URL.albumID">
<cfparam name="URL.pageNum" default="1">

<script type="text/javascript" src="modules/photogallery/photos.js"></script>

<cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
<cfset albumInfo=PG.getAlbumByID(URL.albumID)>

<div class="header">PHOTO GALLERY &gt; Photos</div>

<cfif albumInfo.recordcount GT 0>    
    <div id="actionBar">
    <a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addPhoto();">New</a>
    <a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editPhoto(null);">Edit</a>
    <a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deletePhoto();">Delete</a>
    </div>
    
    <div style="clear:both;">&nbsp;</div>
    
    <cfoutput>
    <span id="albumID" style="display:none;">#URL.albumID#</span>
    
    <cfif Compare(URL.keyword,"")>
    <p><a href="index.cfm?md=photogallery&tmp=searchalbums&keyword=#URLEncodedFormat(URL.keyword)#&pageNum=#URL.pageNum#&wrap=1">&laquo; Back to Album List</a></p>
    <cfelse>
    <p><a href="index.cfm?md=photogallery&tmp=albums&catID=#URL.catID#&pageNum=#URL.pageNum#&wrap=1">&laquo; Back to Album List</a></p>
    </cfif>
    
	<p><b>Album:</b> #albumInfo.albumTitle#</p>	
	</cfoutput>    
    
    <div id="mainDiv"></div>

</cfif>
