<script type="text/javascript" src="modules/photogallery/albumcategories.js"></script>

<div class="header">PHOTO GALLERY &gt; Categories</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addCategory();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editCategory();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteCategory();">Delete</a>
</div>


<br style="clear:both;" /><br />

<div id="mainDiv"></div>