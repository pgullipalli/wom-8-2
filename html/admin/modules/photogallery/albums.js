var pageNum=1;//initial page number

$(function () {
	pageNum=$("#frmNavigator input[name='pageNum']").val();	
	loadAlbumList(pageNum);
	
	$("#frmNavigator select[name='albumCategoryID']").change(function() {
	  pageNum=1;
	  loadAlbumList();	  
	});
});


function loadAlbumList(pNum) {
  if (pNum) pageNum = pNum;
  var catID=$("#frmNavigator select[name='albumCategoryID']").val();	
  xsite.load("#mainDiv", "index.cfm?md=photogallery&tmp=snip_album_list&pageNum=" + pageNum + "&catID=" + catID + "&uuid=" + xsite.getUUID());
}

/*--------------------------------*/
/*    BEGIN: Add Album            */
/*--------------------------------*/
function addAlbum() {
  var catID=$("#frmNavigator select[name='albumCategoryID']").val();	
  xsite.createModalDialog({
	windowName: 'winAddAlbum',
	title: 'Add Album',
	position: 'center-up',
	width: 650,
	url: 'index.cfm?md=photogallery&tmp=snip_add_album&catID=' + catID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {//create date pickers for date fields
	  $("#albumDate").datepicker();  
	},
	buttons: {
	  'Cancel': function() { $("#winAddAlbum").dialog('close'); },
	  ' Add ': submitForm_AddAlbum
	}
  }).dialog('open');
  
  function submitForm_AddAlbum() {
	var form = $("#frmAddAlbum");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=photogallery&task=addAlbum',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddAlbum").dialog('close');
		loadAlbumList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------*/
/*    END: Add Album              */
/*--------------------------------*/

/*--------------------------------*/
/*    BEGIN: Edit Album           */
/*--------------------------------*/
function editAlbum() {
  var form = $("#frmAlbumList")[0];
  var albumID = xsite.getCheckedValue(form.albumID);
  if (!albumID) {
	alert("Please select an album to edit.");
	return;
  }
  xsite.createModalDialog({
	windowName: 'winEditAlbum',
	title: 'Edit Album',
	position: 'center-up',
	width: 650,
	url: 'index.cfm?md=photogallery&tmp=snip_edit_album&albumID=' + albumID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {//create date pickers for date fields
	  $("#albumDate2").datepicker();  
	},
	buttons: {
	  'Cancel': function() { $("#winEditAlbum").dialog('close'); },
	  ' Save ': submitForm_EditAlbum
	}
  }).dialog('open');
  
  function submitForm_EditAlbum() {
	var form = $("#frmEditAlbum");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=photogallery&task=editAlbum',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
	  return;	
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditAlbum").dialog('close');
		loadAlbumList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}	  
  }
}
/*--------------------------------*/
/*    END: Edit Album             */
/*--------------------------------*/


/*--------------------------------*/
/*  BEGIN: Delete Album           */
/*--------------------------------*/
function deleteAlbum() {
  var form = $("#frmAlbumList")[0];
  var albumID = xsite.getCheckedValue(form.albumID);
  if (!albumID) {
	alert("Please select an album to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected album?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=photogallery&task=deleteAlbum&albumID=' + albumID, {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadAlbumList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }
}
/*--------------------------------*/
/*  END: Delete Album             */
/*--------------------------------*/

function managePhotos() {
  var catID=$("#frmNavigator select[name='albumCategoryID']").val();
  var form = $("#frmAlbumList")[0];
  var albumID = xsite.getCheckedValue(form.albumID);
  if (!albumID) {
	alert("Please select an album to manage its photos.");
	return;
  }
  
  window.location="index.cfm?md=photogallery&tmp=photos&catID=" + catID + "&albumID=" + albumID + "&pageNum=" + pageNum + "&wrap=1&uuid=" + xsite.getUUID();
}

function previewHTML() {
  var form = $("#frmAlbumList")[0];
  var albumID = xsite.getCheckedValue(form.albumID);
  if (!albumID) {
	alert("Please select an album to preview.");
	return;
  }

  window.open('/index.cfm?md=photogallery&tmp=album_html&albumID=' + albumID, '_blank');
}

function previewSlideshow() {
  var form = $("#frmAlbumList")[0];
  var albumID = xsite.getCheckedValue(form.albumID);
  if (!albumID) {
	alert("Please select an album to preview.");
	return;
  }
  window.open('/index.cfm?md=photogallery&tmp=album&albumID=' + albumID, '_blank');
}