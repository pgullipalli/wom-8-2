var pageNum=1;//initial page number

$(function () {	
	loadPhotoList();	
	/*$("#frmNavigator select[name='albumID']").change(function() {
	  pageNum=1;
	  loadPhotoList();	  
	});*/
});

function loadPhotoList(pNum) {
  if (pNum) pageNum = pNum;
  var albumID=$("#albumID").html();	
  
  var url="index.cfm?md=photogallery&tmp=snip_photo_list&albumID=" + albumID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function addPhoto() {
  var albumID=$("#albumID").html();		
  xsite.createModalDialog({
	windowName: 'winAddPhoto',
	title: 'Add Photo',
	width: 650,
	position: 'center-up',
	url: 'index.cfm?md=photogallery&tmp=snip_add_photo&albumID=' + albumID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddPhoto").dialog('close'); },
	  ' Add ': submitForm_AddPhoto
	}
  }).dialog('open');
  
  function submitForm_AddPhoto() {
	var form = $("#frmAddPhoto");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=photogallery&task=addPhoto',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  beforeSubmit: validate,
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function validate() {
	  var form = $("#frmAddPhoto")[0];
	  if (form.photoFile.value == "") {
		alert('Please select a photo.');
		form.photoFile.focus();
		return false;
	  }
	}
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddPhoto").dialog('close');
		loadPhotoList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editPhoto(phID) {
  var form = $("#frmPhotoList")[0];
  var photoID = null;
  if (phID) photoID = phID;
  else photoID = xsite.getCheckedValue(form.photoID);

  if (!photoID) {
	alert("Please select an item to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditPhoto',
	title: 'Edit Photo',
	width: 650,
	position: 'center-up',
	url: 'index.cfm?md=photogallery&tmp=snip_edit_photo&photoID=' + photoID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditPhoto").dialog('close'); },
	  ' Save ': submitForm_EditPhoto
	}
  }).dialog('open');
	
  function submitForm_EditPhoto() {
	var form = $("#frmEditPhoto");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=photogallery&task=editPhoto',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  beforeSubmit: validate,
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function validate() {
	  var form = $("#frmEditPhoto")[0];
	  if (form.photoFile.value == "") {
		alert('Please select a photo.');
		form.photoFile.focus();
		return false;
	  }
	}
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditPhoto").dialog('close');
		loadPhotoList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deletePhoto() {
  var form = $("#frmPhotoList")[0];
  var photoID = xsite.getCheckedValue(form.photoID);
  if (!photoID) {
	alert("Please select an item to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected item?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=photogallery&task=deletePhoto&photoID=' + photoID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadPhotoList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}


function movePhoto(photoID, direction) {
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=photogallery&task=movePhoto&photoID=' + photoID + '&direction=' + direction + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  loadPhotoList();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}