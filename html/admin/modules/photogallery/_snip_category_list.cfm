<cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
<cfset albumCategorySummary=PG.getAlbumCategorySummary()>

<cfif albumCategorySummary.recordcount IS 0>
  <p>
  It appears that there are no categories created.
  Please click on the 'New' button in the action bar above to create the first category.
  </p>
<cfelse>
<form id="frmCategoryList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="250">Category Name</th>
    <th width="120"># of Albums</th>
    <th>URL (Click to preview)</th>
  </tr>
  <cfoutput query="albumCategorySummary">
  <tr>
    <td align="center"><input type="radio" name="albumCategoryID" value="#albumCategoryID#"></td>
    <td><a href="index.cfm?md=photogallery&tmp=albums&wrap=1&catID=#albumCategoryID#">#albumCategory#</a></td>
    <td align="center"><a href="index.cfm?md=photogallery&tmp=albums&wrap=1&catID=#albumCategoryID#">#numAlbums#</a></td>
    <td align="center"><a href="/index.cfm?md=photogallery&tmp=category&catid=#albumCategoryID#" target="_blank">/index.cfm?md=photogallery&amp;tmp=category&amp;catid=#albumCategoryID#</a></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>