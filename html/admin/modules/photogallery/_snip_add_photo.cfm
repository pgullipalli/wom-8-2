<cfparam name="URL.albumID">

<form id="frmAddPhoto" name="frmAddPhoto" onsubmit="return false">
<cfoutput><input type="hidden" name="albumID" value="#URL.albumID#"></cfoutput>
<div class="row">
	<div class="col-30-right">Photo Title:</div>
    <div class="col-68-left">
      <input type="text" name="title" style="width:350px;" /><br />
      <input type="checkbox" name="published" value="1" /> Publish this photo
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Photo Image:</b></div>
    <div class="col-68-left">
      <input type="hidden" name="photoFile" value="" />
      <CF_SelectFile formName="frmAddPhoto" fieldName="photoFile" previewID="preview1" previewWidth="150">
      (Width: <cfoutput>#Application.photogallery.fullsizeWidth#px</cfoutput>)
      <div id="preview1"></div>
      <div class="row">
      Orientation:
      <input type="radio" name="orientation" value="H" checked /> Horizontal
      <input type="radio" name="orientation" value="V" /> Vertical
      <input type="radio" name="orientation" value="S" /> Square
      </div>
    </div>
</div>
<div class="row">
	<div class="col-30-right">Photo Thumbnail:</div>
    <div class="col-68-left">
      <input type="hidden" name="photoThumbnail" value="" />
      <input type="checkbox" name="resizeForThumbnail" value="1" /> Resize the full-size image to a thumbnail image; OR<br />
      <CF_SelectFile formName="frmAddPhoto" fieldName="photoThumbnail" previewID="preview2" previewWidth="#Application.photogallery.thumbnailWidth#">
      (Width: <cfoutput>#Application.photogallery.thumbnailWidth#px</cfoutput>)
      <div id="preview2"></div>    
    </div>
</div>
<div class="row">
	<div class="col-30-right">Description:</div>
    <div class="col-68-left"><textarea name="description" style="width:350px;height:35px;"></textarea></div>
</div>
<div class="row">
	<div class="col-30-right">Credit:</div>
    <div class="col-68-left"><input type="text" name="author" style="width:350px;" /></div>
</div>
</form>