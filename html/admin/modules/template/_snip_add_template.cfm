<cfscript>
  NV=CreateObject("component", "com.NavigationAdmin").init();
  manNavs=NV.getSubNavsByParentNavID(navGroupID=1, parentNavID=0);
</cfscript>

<form id="frmAddTemplate" name="frmAddTemplate" onsubmit="return false">
<div class="row">
	<div class="col-30-right"><b>Template Name:</b></div>
    <div class="col-68-left"><input type="text" name="templateName" maxlength="100" value="" style="width:320px;" validate="required:true" /></div>
</div>
<!--- <div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="isDefault" value="1" /> Set as default template
    </div>
</div> --->
<div class="row">
	<div class="col-30-right">Select a Topper Style:</div>
    <div class="col-68-left">
      <div class="row">
        <div style="width:5%;padding:0px 2px;text-align:right;float:left;"><input type="radio" name="topperStyle" value="ST" checked /></div>
        <div style="width:90%;padding:0px 2px;text-align:left;float:left;">
          Section Title<br />
          Enter a section title: <input type="text" name="sectionTitle" maxlength="50" style="width:200px;" />
        </div>
      </div>
      <div class="row">
        <div style="width:5%;padding:0px 2px;text-align:right;float:left;"><input type="radio" name="topperStyle" value="IM" /></div>
        <div style="width:90%;padding:0px 2px;text-align:left;float:left;">
          Image<br />
          Select an image:
          <input type="hidden" name="topperImage" value="">
          <CF_SelectFile formName="frmAddTemplate" fieldName="topperImage" previewID="preview1">
          <div id="preview1"></div>
        </div>
      </div>
      <div class="row">
        <div style="width:5%;padding:0px 2px;text-align:right;float:left;"><input type="radio" name="topperStyle" value="NV" /></div>
        <div style="width:90%;padding:0px 2px;text-align:left;float:left;">Navigation Items</div>
      </div>
    </div>
</div>
<div class="row">
	<div class="col-30-right">Main Navigation Items:</div>
    <div class="col-68-left">
      <div style="width:400px;height:100px;overflow:scroll;">
      <cfoutput query="manNavs">
    	<input type="checkbox" name="mainNavIDList" value="#navID#" /> #navName#<br />
      </cfoutput>
      </div>
    </div>
</div>

<div class="row">
	<div class="col-30-right"><b>Theme Picture:</b></div>
    <div class="col-68-left">
      <input type="hidden" name="themeImage" value="">
      <CF_SelectFile formName="frmAddTemplate" fieldName="themeImage" previewID="preview2" previewWidth="150">
      <div id="preview2"></div>
    </div>
</div>
<div class="row">
	<div class="col-30-right">My Notes:</div>
    <div class="col-68-left">
      <textarea name="description" style="width:320px;height:30px;"></textarea><br /><small>(* For internal reference)</small>
    </div>
</div>
</form>
