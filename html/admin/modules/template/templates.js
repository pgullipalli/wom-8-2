$(function () {	
	loadTemplateList();
});

function loadTemplateList(pNum) { 
  var url="index.cfm?md=template&tmp=snip_template_list&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function addTemplate() {
  xsite.createModalDialog({
	windowName: 'winAddTemplate',
	title: 'Add Template',
	width: 650,
	position: 'center-up',
	url: 'index.cfm?md=template&tmp=snip_add_template&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddTemplate").dialog('close'); },
	  ' Add ': submitForm_AddTemplate
	}
  }).dialog('open');
  
  function submitForm_AddTemplate() {
	var form = $("#frmAddTemplate");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=template&task=addTemplate',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddTemplate").dialog('close');
		loadTemplateList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editTemplate(tID) {
  var form = $("#frmTemplateList")[0];
  var templateID = null;
  if (tID) templateID = tID;
  else templateID = xsite.getCheckedValue(form.templateID);

  if (!templateID) {
	alert("Please select a template to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditTemplate',
	title: 'Edit Template',
	width: 650,
	position: 'center-up',
	url: 'index.cfm?md=template&tmp=snip_edit_template&templateID=' + templateID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditTemplate").dialog('close'); },
	  ' Save ': submitForm_EditTemplate
	}
  }).dialog('open');
	
  function submitForm_EditTemplate() {
	var form = $("#frmEditTemplate");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=template&task=editTemplate',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
		
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditTemplate").dialog('close');
		loadTemplateList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteTemplate() {
  var form = $("#frmTemplateList")[0];
  var templateID = xsite.getCheckedValue(form.templateID);
  if (!templateID) {
	alert("Please select a template to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected template?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=template&task=deleteTemplate&templateID=' + templateID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadTemplateList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function showURLParameterHelp() {
  xsite.createModalDialog({
	windowName: 'winHelp',
	title: 'Help',
	width: 650,
	position: 'center-up',
	url: 'index.cfm?md=template&tmp=snip_help_urlparameter',
	buttons: {
	  'OK': function() { $("#winHelp").dialog('close'); }
	}
  }).dialog('open');
}