<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  TM=CreateObject("component", "com.TemplateAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";
  
  switch (URL.task) {
    case "addTemplate":
	  TM.addTemplate(argumentCollection=Form);
	  break;
	  
	case "editTemplate":
	  TM.editTemplate(argumentCollection=Form);
	  break;
	  
	case "deleteTemplate":
	  if (Not TM.deleteTemplate(URL.templateID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="Template can not be deleted. It appears that the template is currently used by a navigation item.";	
	  }
	  break;
	  
	default:
	  break;
  }
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>