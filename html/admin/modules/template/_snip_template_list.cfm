<cfscript>
  TM=CreateObject("component", "com.TemplateAdmin").init();
  NV=CreateObject("component", "com.NavigationAdmin").init();
  templates=TM.getTemplates();
  manNavs=NV.getSubNavsByParentNavID(navGroupID=1, parentNavID=0);
  navStruct=StructNew();
  for (i=1; i<=manNavs.recordcount; i++) {
    navKey = "nav" & manNavs.navID[i];
    navStruct[navKey]=manNavs.navName[i];
  }
</cfscript>

<cfif templates.recordcount IS 0>
  <p>
  It appears that there are no templates created.
  Please click on the 'New' button in the action bar above to create the first template.
  </p>
<cfelse>
<form id="frmTemplateList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th width="200">Template Name</th>
    <th width="200">Topper Style</th>
    <th width="250">Topper</th>
    <th width="150">URL Parameter (<a href="javascript:showURLParameterHelp();">?</a>)</th>
    <th>My Notes</th>
  </tr>  
  <cfoutput query="templates">
  <tr>
    <td align="center"><input type="radio" name="templateID" value="#templateID#"></td>
	<td>
      <a href="javascript:editTemplate('#templateID#')">#templateName#<!---  (ID: #templateID#) ---></a>
    </td> 
    <td align="center">
      <cfif topperStyle Is "ST">Section Title<cfelseif topperStyle Is "IM">Image<cfelseif topperStyle Is "NV">Navigation</cfif>&nbsp;
    </td>
    <td>
      <cfswitch expression="#topperStyle#">
        <cfcase value="ST">#sectionTitle#</cfcase>
        <cfcase value="IM">
          <img src="/#topperImage#" border="0">
        </cfcase>
        <cfcase value="NV">
          <cfloop index="navID" list="#mainNavIDList#"><div>#navStruct["nav#navID#"]#</div></cfloop>
        </cfcase>
      </cfswitch>
    </td>
    <td align="center">tid=#templateID#</td>
    <td>#description#&nbsp;</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>