<p>
Append this URL parameter/value pair to an URL will force the page to use the corresponding template.
For example, <cfoutput>#Application.siteURLRoot#/index.cfm?md=pagebuilder&amp;tmp=home&amp;pid=2&amp;tid=2</cfoutput>
will force the destination page to use a template whose ID is 2.
Please note adding the 'tid' parameter to an URL that has already the 'tid' parameter will cause error.
</p>

