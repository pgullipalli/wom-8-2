<script type="text/javascript" src="modules/template/templates.js"></script>

<div class="header">TEMPLATE MANAGER</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addTemplate();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editTemplate();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteTemplate();">Delete</a>
</div>

<br style="clear:both;" /><br />

<div id="mainDiv"></div>