<cfparam name="URL.templateID">
<cfscript>
  TM=CreateObject("component", "com.TemplateAdmin").init();
  NV=CreateObject("component", "com.NavigationAdmin").init();
  manNavs=NV.getSubNavsByParentNavID(navGroupID=1, parentNavID=0);
  templateInfo=TM.getTemplate(URL.templateID);
</cfscript>

<cfoutput query="templateInfo">
<form id="frmEditTemplate" name="frmEditTemplate" onsubmit="return false">
<input type="hidden" name="templateID" value="#URL.templateID#">
<div class="row">
	<div class="col-30-right"><b>Template Name:</b></div>
    <div class="col-68-left"><input type="text" name="templateName" maxlength="100" value="#HTMLEditFormat(templateName)#" style="width:320px;" validate="required:true" /></div>
</div>
<!--- <div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="isDefault" value="1" <cfif isDefault>checked</cfif> /> Set as default template
    </ div>
</div> --->
<div class="row">
	<div class="col-30-right">Select a Topper Style:</div>
    <div class="col-68-left">
      <div class="row">
        <div style="width:5%;padding:0px 2px;text-align:right;float:left;"><input type="radio" name="topperStyle" value="ST" <cfif topperStyle Is "ST">checked</cfif> /></div>
        <div style="width:90%;padding:0px 2px;text-align:left;float:left;">
          Section Title<br />
          Enter a section title: <input type="text" name="sectionTitle" maxlength="50" style="width:200px;" value="#HTMLEditFormat(sectionTitle)#" />
        </div>
      </div>
      <div class="row">
        <div style="width:5%;padding:0px 2px;text-align:right;float:left;"><input type="radio" name="topperStyle" value="IM" <cfif topperStyle Is "IM">checked</cfif> /></div>
        <div style="width:90%;padding:0px 2px;text-align:left;float:left;">
          Image<br />
          Select an image:
          <input type="hidden" name="topperImage" value="#HTMLEditFormat(topperImage)#">
          <CF_SelectFile formName="frmEditTemplate" fieldName="topperImage" previewID="preview3">
          <div id="preview3"><cfif Compare(topperImage,"")><img src="/#topperImage#" border="0"></cfif></div>
        </div>
      </div>
      <div class="row">
        <div style="width:5%;padding:0px 2px;text-align:right;float:left;"><input type="radio" name="topperStyle" value="NV" <cfif topperStyle Is "NV">checked</cfif> /></div>
        <div style="width:90%;padding:0px 2px;text-align:left;float:left;">Navigation Items</div>
      </div>
    </div>
</div>
<div class="row">
	<div class="col-30-right">Main Navigation Items:</div>
    <div class="col-68-left">
      <div style="width:400px;height:100px;overflow:scroll;">
      <cfloop query="manNavs">
    	<input type="checkbox" name="mainNavIDList" value="#navID#" <cfif ListFind(templateInfo.mainNavIDList, navID) GT 0>checked</cfif> /> #navName#<br />
      </cfloop>
      </div>
    </div>
</div>

<div class="row">
	<div class="col-30-right"><b>Theme Picture:</b></div>
    <div class="col-68-left">
      <input type="hidden" name="themeImage" value="#HTMLEditFormat(themeImage)#">
      <CF_SelectFile formName="frmEditTemplate" fieldName="themeImage" previewID="preview4" previewWidth="150">
      <div id="preview4"><cfif Compare(themeImage,"")><img src="/#themeImage#" border="0" width="150"></cfif></div>
    </div>
</div>
<div class="row">
	<div class="col-30-right">My Notes:</div>
    <div class="col-68-left">
      <textarea name="description" style="width:320px;height:30px;">#HTMLEditFormat(description)#</textarea><br /><small>(* For internal reference)</small>
    </div>
</div>
</form>
</cfoutput>