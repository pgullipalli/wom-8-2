$(function () {		
	loadAliasList();
});


function loadAliasList() {
  xsite.load("#mainDiv", "index.cfm?md=alias&tmp=snip_alias_list&uuid=" + xsite.getUUID());
}

/*--------------------------------*/
/*    BEGIN: Add URL Alias        */
/*--------------------------------*/
function addAlias() {
  xsite.createModalDialog({
	windowName: 'winAddAlias',
	title: 'Add URL Alias',
	position: 'center-up',
	width: 550,
	url: 'index.cfm?md=alias&tmp=snip_add_alias&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddAlias").dialog('close'); },
	  ' Add ': submitForm_AddAlias
	}
  }).dialog('open');
  
  function submitForm_AddAlias() {
	var form = $("#frmAddAlias");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=alias&task=addAlias',
		  type: 'post',
		  cache: false,
		  beforeSubmit: validate,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}
	
	function validate() {
	  var alias = $("#frmAddAlias input[name='alias']").val();
	  var re=/^[a-z0-9]+$/i;
	  if (!re.test(alias)) {
		alert("Please use only letters and number for your alias. No spaces and special characters are allowed.");
		return false;
	  } else {
		return true;
	  }
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddAlias").dialog('close');
		loadAliasList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------*/
/*    END:  Add URL Alias         */
/*--------------------------------*/

/*--------------------------------*/
/*    BEGIN: Edit URL Alias       */
/*--------------------------------*/
function editAlias() {
  var form = $("#frmAliasList")[0];
  var URLAliasID = xsite.getCheckedValue(form.URLAliasID);
  if (!URLAliasID) {
	alert("Please select an alias to edit.");
	return;
  }
  xsite.createModalDialog({
	windowName: 'winEditAlias',
	title: 'Edit URL Alias',
	position: 'center-up',
	width: 550,
	url: 'index.cfm?md=alias&tmp=snip_edit_alias&URLAliasID=' + URLAliasID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditAlias").dialog('close'); },
	  ' Save ': submitForm_EditAlias
	}
  }).dialog('open');
  
  function submitForm_EditAlias() {
	var form = $("#frmEditAlias");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=alias&task=editAlias',
		  type: 'post',
		  cache: false,
		  beforeSubmit: validate,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}
	
	function validate() {
	  var alias = $("#frmEditAlias input[name='alias']").val();
	  var re=/^[a-z0-9]+$/i;
	  if (!re.test(alias)) {
		alert("Please use only letters and number for your alias. No spaces and special characters are allowed.");
		return false;
	  } else {
		return true;
	  }
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditAlias").dialog('close');
		loadAliasList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------*/
/*    END: Edit URL Alias         */
/*--------------------------------*/


/*--------------------------------*/
/*  BEGIN: Delete URL Alias       */
/*--------------------------------*/
function deleteAlias() {
  var form = $("#frmAliasList")[0];
  var URLAliasID = xsite.getCheckedValue(form.URLAliasID);
  if (!URLAliasID) {
	alert("Please select an URL alias to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected URL alias?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function(){
		$.get('action.cfm?md=alias&task=deleteAlias&URLAliasID=' + URLAliasID, {}, processJson, 'json');}
	});				
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadAliasList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }
}
/*--------------------------------*/
/*  END: Delete URL ALias         */
/*--------------------------------*/