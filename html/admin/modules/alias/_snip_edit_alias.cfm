<cfparam name="URL.URLAliasID">
<cfset UA=CreateObject("component", "com.AliasAdmin").init()>
<cfset aliasInfo=UA.getAliasByID(URL.URLAliasID)>

<cfoutput query="aliasInfo">
<form id="frmEditAlias" onSubmit="return false;">
<input type="hidden" name="URLAliasID" value="#URL.URLAliasID#" />
<input type="hidden" name="alias" value="#HTMLEditFormat(alias)#" />
<table border="0" cellpadding="3" cellspacing="0" width="100%">
  <tr valign="top">
    <td align="right" width="30%">Name:</td>
    <td><input type="text" name="aliasName" size="20" maxlength="50" value="#HTMLEditFormat(aliasName)#" /><br /><small>(For internal reference only.)</small></td>
  </tr>
  <tr valign="top">
    <td align="right" width="30%"><b>Alias:</b></td>
    <td>#APPLICATION.alias.URLRoot#/#alias#</td>
  </tr>
  <tr valign="top">
    <td align="right"><b>Resolved (actual) URL:</b></td>
    <td>
      <input type="text" name="resolvedURL" size="48" validate="required:true" value="#HTMLEditFormat(resolvedURL)#" /><br />
      <small>(Enter a complete URL, for example,<br /> http://www.domainname.com/dir1/dir2/page?id=123)</small>
    </td>
  </tr>
</table>
</form>
</cfoutput>