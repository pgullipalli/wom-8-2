<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  UA=CreateObject("component", "com.AliasAdmin");
  
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";
  
  switch (URL.task) {
    case "addAlias":
	  Form.aliasDirectoryRoot=Application.alias.directoryRoot;
      returnedStruct=UA.addAlias(argumentCollection=FORM);
	  break;
	
	case "deleteAlias":
	  returnedStruct=UA.deleteAlias(URLAliasID=URL.URLAliasID, aliasDirectoryRoot=Application.alias.directoryRoot);
	  break;
	  
	case "editAlias":
	  Form.aliasDirectoryRoot=Application.alias.directoryRoot;
	  returnedStruct=UA.editAlias(argumentCollection=FORM);
	  
	  break;
	
	default:
	  break;
  }
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>