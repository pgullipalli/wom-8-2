<cfset UA=CreateObject("component", "com.AliasAdmin").init()>
<cfset allAlias=UA.getAllAlias()>

<cfif allAlias.recordcount IS 0>
  <p>
  It appears that there are no URL alias created.
  Please click on the 'New' button in the action bar above to create the first URL alias.
  </p>
<cfelse>
<form id="frmAliasList">
<div class="itemList">
<table>
  <tr>
    <th width="40">&nbsp;</th>
    <th width="200">Name</th>
    <th width="300">Alias</th>
    <th>Resolved URL</th>
  </tr>
  <cfoutput query="allAlias">
  <tr>
    <td align="center"><input type="radio" name="URLAliasID" value="#URLAliasID#"></td>
    <td align="center"><cfif Compare(aliasName, "")>#aliasName#<cfelse>--</cfif></td>
    <td><a href="#resolvedURL#" target="_blank">#APPLICATION.alias.URLRoot#/#alias#</a></td>
    <td>#resolvedURL#</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>