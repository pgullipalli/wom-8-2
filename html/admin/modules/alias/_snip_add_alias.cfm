<form id="frmAddAlias" onSubmit="return false;">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
  <tr valign="top">
    <td align="right" width="30%">Name:</td>
    <td><input type="text" name="aliasName" size="20" maxlength="50" /><br /><small>(For internal reference only.)</small></td>
  </tr>
  <tr valign="top">
    <td align="right" width="30%"><b>Alias:</b></td>
    <td><cfoutput>#APPLICATION.alias.URLRoot#</cfoutput>/<input type="text" name="alias" size="10" maxlength="50" validate="required:true" /></td>
  </tr>
  <tr valign="top">
    <td align="right"><b>Resolved (actual) URL:</b></td>
    <td>
      <input type="text" name="resolvedURL" size="48" value="" validate="required:true" /><br />
      <small>(Enter a complete URL, for example,<br /> http://www.domainname.com/dir1/dir2/page?id=123)</small>
    </td>
  </tr>
</table>
</form>
