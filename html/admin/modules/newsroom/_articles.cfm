<cfparam name="URL.catID" default="0">

<script type="text/javascript" src="modules/newsroom/articles.js"></script>

<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>
<cfset allCategories=NR.getAllArticleCategories()>
<cfif URL.catID IS 0>
  <cfset URL.catID=NR.getFirstArticleCategoryID()>
  <cfif URL.catID IS 0><!--- no categories were ever created --->
    <cflocation url="index.cfm?md=newsroom&tmp=articlecategories&wrap=1">
  </cfif>
</cfif>

<div class="header">INFORMATION LIBRARY &gt; Resources</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addArticle();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editArticle(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="copyArticle();">Copy</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteArticle();">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="editRelatedItems();">Related Items</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="preview();">Preview</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="getURL();">Get URL</a>
</div>

<br /><br /><br />

<cfoutput>
<form id="frmNavigator">
  Category
  <select name="articleCategoryID" style="min-width:250px;">
    <cfloop query="allCategories">
    <option value="#articleCategoryID#"<cfif URL.catID IS articleCategoryID> selected</cfif>>#articleCategory#</option>
    </cfloop>
  </select>
  &nbsp;&nbsp;&nbsp;&nbsp;
  <a href="index.cfm?md=newsroom&tmp=searcharticles&wrap=1">Search Resources</a>
</form>
</cfoutput>


<div id="mainDiv"></div>