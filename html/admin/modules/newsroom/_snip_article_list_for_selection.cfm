<cfparam name="URL.catID">
<cfparam name="URL.articleID">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>

<cfset structArticles=NR.getArticlesByCategoryID(articleCategoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>

<cfif structArticles.numAllItems IS 0>
  <p>No items created in this category.</p>
<cfelse>
  <cfif structArticles.numDisplayedItems GT 0>
    <form id="frmArticleList">
    <cfoutput><input type="hidden" name="articleID" value="#URL.articleID#"></cfoutput>
    <div class="itemList">
    <table>
      <tr>
        <th width="40">Select</th>
        <th>Headline</th>
      </tr>   
      </tr>  
      <cfoutput query="structArticles.articles">
      <tr>
        <td align="center"><input type="checkbox" name="articleIDList" value="#articleID#"></td>
        <td>#headline# (#DateFormat(articleDate, "mm/dd/yy")#)</td> 
      </tr>
      </cfoutput>
    </table>
    </div>
    </form>
    
    <cfif structArticles.numAllItems GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(structArticles.numAllItems/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadArticleListForSelection(#idx#)">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
    </cfif>
  </cfif>
</cfif>