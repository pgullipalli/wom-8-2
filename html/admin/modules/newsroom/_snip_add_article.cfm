<cfparam name="URL.catID">
<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>
<cfset allCategories=NR.getAllArticleCategories()>

<form id="frmAddArticle" name="frmAddArticle" onsubmit="return false">
<div class="row">
	<div class="col-30-right"><b>Categories:</b><br /><small>(Check all that apply.)</small></div>
    <div class="col-68-left">
      <cfset numCategories=allCategories.recordcount>
      <cfset numRows=Ceiling(numCategories/3)>
      <cfoutput><cfset idx = 0>
      <cfloop index="row" from="1" to="#numRows#">
        <div class="row">
        <cfloop index="col" from="1" to="3">
          <cfset idx = (row - 1) * 3 + col>
          <cfif idx LTE numCategories>
            <cfset articleCategoryID=allCategories.articleCategoryID[idx]>
            <cfset articleCategory=allCategories.articleCategory[idx]>
            <div class="col-30-left">
              <input type="checkbox" name="articleCategoryIDList" value="#articleCategoryID#"<cfif URL.catID Is articleCategoryID> checked</cfif> />
              #articleCategory#
            </div>
          </cfif>
        </cfloop>
        </div>
      </cfloop>
      </cfoutput>
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Headline:</b></div>
    <div class="col-68-left">
      <input type="text" name="headline" maxlength="255" value="" style="width:350px;" validate="required:true"><br />
      <input type="checkbox" name="featuredOnHomepage" value="1" /> Featured on home/sub-home page
    </div>
</div>
<div class="row">
	<div class="col-30-right">Sub-Headline:</div>
    <div class="col-68-left">
      <input type="text" name="subHeadline" maxlength="200" value="" style="width:350px;">
    </div>
</div>
<div class="row">
	<div class="col-30-right">Byline:</div>
    <div class="col-68-left">
      <input type="text" name="byline" maxlength="100" style="width:120px;"> &nbsp;&nbsp;&nbsp;&nbsp;
      Dateline:
      <input type="text" name="dateline" maxlength="100" style="width:120px;">
    </div>
</div>
<div class="row">
	<div class="col-30-right">Teaser:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmAddArticle"
            fieldName="teaser"
            cols="65"
            rows="3"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Article Teaser">
    </div>
</div>

<div class="row">
	<div class="col-30-right"><b>Publish?</b></div>
    <div class="col-68-left">
      <div class="row">
        <input type="radio" name="published" value="1" /> Yes
      </div>
      <div class="row">
        <div class="col-24-right">Publish Date:</div>
        <div class="col-74-left">
          <input type="text" name="publishDate" id="publishDate" style="width:60px;" validate="required:false, date:true" />
          <select name="publishDate_hh">
            <option value="12">12</option>
            <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="publishDate_mm">
            <cfoutput><cfloop index="mm" from="0" to="59"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="publishDate_ampm">
            <option value="am">AM</option><option value="pm">PM</option>
          </select>
          <input type="button" value="Now" onclick="getNow(document.frmAddArticle, 'publish')" />
          <input type="button" value="Clear" onclick="clearDate(document.frmAddArticle, 'publish')" />
        </div>
      </div>
      <div class="row">
        <div class="col-24-right">Unpublish Date:</div>
        <div class="col-74-left">
          <input type="text" name="unpublishDate" id="unpublishDate" style="width:60px;" validate="required:false, date:true" />
          <select name="unpublishDate_hh">
            <option value="12">12</option>
            <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="unpublishDate_mm">
            <cfoutput><cfloop index="mm" from="0" to="59"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="unpublishDate_ampm">
            <option value="am">AM</option><option value="pm">PM</option>
          </select>
          <input type="button" value="Now" onclick="getNow(document.frmAddArticle, 'unpublish')" />
          <input type="button" value="Clear" onclick="clearDate(document.frmAddArticle, 'unpublish')" />
        </div>
      </div>
      <div class="row">
        <input type="radio" name="published" value="0" checked /> No
      </div>    
    </div>
</div>

<div class="row">
	<div class="col-30-right"><b>Article Date:</b></div>
    <div class="col-68-left">
	  <input type="text" name="articleDate" id="articleDate" style="width:60px;" validate="required:true, date:true" />
      <select name="articleDate_hh">
        <option value="12">12</option>
        <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
      </select>
      <select name="articleDate_mm">
        <cfoutput><cfloop index="mm" from="0" to="59"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
      </select>
      <select name="articleDate_ampm">
        <option value="am">AM</option><option value="pm">PM</option>
      </select>
      <input type="button" value="Now" onclick="getNow(document.frmAddArticle, 'article')" />
      <input type="button" value="Clear" onclick="clearDate(document.frmAddArticle, 'article')" /><br />
      <small>(May be earlier than Publish Date if necessary.)</small><br />
      
      <input type="checkbox" name="displayArticleDate" value="1" checked /> Display date on front-end (index and detail pages).
	</div>
</div>

<div class="row">
	<div class="col-30-right"><b>Content Type:</b></div>
    <div class="col-68-left">
      <input type="radio" name="contentType" value="1" onclick="toggleContentType('frmAddArticle')" checked /> Article (HTML content)
      <input type="radio" name="contentType" value="2" onclick="toggleContentType('frmAddArticle')" /> External URL
      <input type="radio" name="contentType" value="3" onclick="toggleContentType('frmAddArticle')" /> File (Document Files)
      <input type="radio" name="contentType" value="4" onclick="toggleContentType('frmAddArticle')" /> Media File (Audio or Video)
    </div>
</div>

<div class="row" id="contentType1" style="display:block;">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      Use WYSIWIG ("What You See Is What You Get") tool to type text, cut-and-paste from another source,
      or enter HTML content.<br />
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="false"
            formName="frmAddArticle"
            fieldName="fullStory"
            cols="65"
            rows="4"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Article Content">
    </div>
</div>
<div class="row" id="contentType2" style="display:none;">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      External URL: <input type="text" name="externalURL" style="width:360px;" />
    </div>
</div>
<div class="row" id="contentType3" style="display:none;">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      Select a File: <input type="hidden" name="documentFile" value="">
      <CF_SelectFile formName="frmAddArticle" fieldName="documentFile" previewID="preview1-1" previewWidth="100">
	  <div id="preview1-1"></div>
    </div>
</div>
<div class="row" id="contentType4" style="display:none;">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      Media Type: <input type="radio" name="mediaFileType" value="V" checked /> Video <input type="radio" name="mediaFileType" value="A" /> Audio<br />
      Select a Media File:<input type="hidden" name="mediaFile" value="">
      <CF_SelectFile formName="frmAddArticle" fieldName="mediaFile" previewID="preview1-2" previewWidth="100">
	  <div id="preview1-2"></div>
      OR<br />
      Enter a Media URL: <input type="text" name="mediaFileURL" maxlength="200" style="width:350px;" value="" />
    </div>
</div>
<div class="row">
  <div class="col-30-right">&nbsp;</div>
  <div class="col-68-left"><input type="checkbox" name="popup" value="1" /> Open in a new window</div>
</div>
<div class="row">
	<div class="col-30-right">Feature Image:<br /><small>(Width: <cfoutput>#Application.newsroom.featureImageWidth#px</cfoutput>)</small></div>
    <div class="col-68-left">
      <div class="row">
        <input type="hidden" name="featureImage" value="">
        <CF_SelectFile formName="frmAddArticle" fieldName="featureImage" previewID="preview2" previewWidth="150">
        <!--- Image Alignment:
        <input type="radio" name="featureImageAlignment" value="L" /> Left <input type="radio" name="featureImageAlignment" value="R" checked /> Right --->
	    <div id="preview2"></div>
      </div>
      <div class="row">
        Caption:<br />
        <textarea name="featureImageCaption" style="width:360px;height:40px;"></textarea>
      </div>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Thumbnail Image:<br /><small>(Width: <cfoutput>#Application.newsroom.thumbnailWidth#px</cfoutput>)</small></div>
    <div class="col-68-left">
      <div class="row">
        <div class="row">
          <input type="checkbox" name="resizeForThumbnail" value="1" /> 
          Resize the feature image to a smaller image and use it as thumbnail image; OR
        </div>
        <div class="row"> 
          <input type="hidden" name="featureThumbnail" value="">
          <CF_SelectFile formName="frmAddArticle" fieldName="featureThumbnail" previewID="preview3" previewWidth="100">
	      <div id="preview3"></div>
        </div>
      </div>
    </div>
</div>
</form>