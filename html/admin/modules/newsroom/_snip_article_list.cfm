<cfparam name="URL.catID" default="0">
<cfparam name="URL.keyword" default="">
<cfparam name="URL.includeArticleContent" default="0">
<cfparam name="URL.featuredOnHomepage" default="0">
<cfparam name="URL.browseBy" default="category">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>

<cfif URL.browseBy IS "category"><!--- browse by category ID --->
  <cfset structArticles=NR.getArticlesByCategoryID(articleCategoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
  <cfif structArticles.numAllItems IS 0>
	<p>
	It appears that there are no items created in this category.
	Please click on the 'New' button in the action bar above to create the first item.
	</p>
  </cfif>
<cfelse><!--- browse by headline keyword --->
  <cfset structArticles=NR.getArticlesByKeyword(keyword=URL.keyword, includeArticleContent=URL.includeArticleContent, featuredOnHomepage=URL.featuredOnHomepage, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
  <cfoutput>
  <p>
  Your search for '<i>#URL.keyword#</i>' returns <cfif structArticles.numAllItems GT 0>#structArticles.numAllItems# items.<cfelse>no results.</cfif>
  </p>	
  </cfoutput>
</cfif>

<cfif structArticles.numDisplayedItems GT 0>
<form id="frmArticleList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th>Headline</th>
    <th width="150">Article Date</th>
    <th width="100">Publish?</th>
    <th width="150">Publish Date</th>
	<th width="150">Unpublish Date</th>
    <th width="100">Related Items</th>
    <th width="100">Featured?</th>
  </tr>  
  <cfoutput query="structArticles.articles">
  <tr>
    <td align="center"><input type="radio" name="articleID" value="#articleID#"></td>
	<td><a href="##" onclick="editArticle('#articleID#')">#headline#</a></td> 
	<td align="center">#DateFormat(articleDate, "mm/dd/yyyy")# #TimeFormat(articleDate, "h:mm tt")#</td>
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
	<td align="center">
	  <cfif published>#DateFormat(publishDate, "mm/dd/yyyy")# #TimeFormat(publishDate, "h:mm tt")#<cfelse>--</cfif>
    </td>
    <td align="center">
	  <cfif Not published OR Year(unpublishDate) IS "3000">--<cfelse>#DateFormat(unpublishDate, "mm/dd/yyyy")# #TimeFormat(unpublishDate, "h:mm tt")#</cfif>
    </td>
    <td align="center"><a href="index.cfm?md=newsroom&tmp=relateditems&wrap=1&catID=#URL.catID#&articleID=#articleID#&keyword=#URLEncodedFormat(URL.keyword)#&includeArticleContent=#URL.includeArticleContent#" style="text-decoration:underline;">#numRelatedItems#</a></td>
    <td align="center">
      <cfif featuredOnHomepage>Yes<cfelse>No</cfif>
    </td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif structArticles.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structArticles.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadArticleList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>