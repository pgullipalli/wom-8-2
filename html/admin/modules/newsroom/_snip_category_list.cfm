<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>
<cfset articleCategorySummary=NR.getArticleCategorySummary()>

<cfif articleCategorySummary.recordcount IS 0>
  <p>
  It appears that there are no categories created.
  Please click on the 'New' button in the action bar above to create the first category.
  </p>
<cfelse>
<form id="frmCategoryList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="250">Category Name</th>
    <th width="200">Contact Info.</th>
    <th width="100"># of Items</th>
    <th>URL (click to preview)</th>
    <th width="100">RSS Feed</th>
  </tr>
  <cfoutput query="articleCategorySummary">
  <tr>
    <td align="center"><input type="radio" name="articleCategoryID" value="#articleCategoryID#"></td>
    <td><a href="index.cfm?md=newsroom&tmp=articles&wrap=1&catID=#articleCategoryID#">#articleCategory#</a></td>
    <td>#contactInfo#&nbsp;</td>
    <td align="center"><a href="index.cfm?md=newsroom&tmp=articles&wrap=1&catID=#articleCategoryID#">#numArticles#</a></td>
    <td align="center"><a href="/index.cfm?md=newsroom&amp;tmp=category&amp;catID=#articleCategoryID#" target="_blank">/index.cfm?md=newsroom&amp;tmp=category&amp;catID=#articleCategoryID#</a></td>
    <td align="center"><cfif RSSEnabled>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>