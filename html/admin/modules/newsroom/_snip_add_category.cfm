<div class="snip">
<form id="frmAddCategory" name="frmAddCategory" onsubmit="return false">
<div class="row">
	<div class="col-30-right"><b>Category Name:</b></div>
    <div class="col-68-left">
      <input type="text" name="articleCategory" size="45" maxlength="50" value="" validate="required:true" />
    </div>
</div>
<div class="row">
	<div class="col-30-right">Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmAddCategory"
            fieldName="description"
            cols="45"
            rows="3"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Description">
    </div>
</div>
<div class="row">
	<div class="col-30-right">Contact Info:</div>
    <div class="col-68-left">
      <input type="text" name="contactInfo" size="45" maxlength="150" value="" />
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>RSS Enabled?</b></div>
    <div class="col-68-left">
      <input type="radio" name="RSSEnabled" value="0" checked /> No<br>
      <input type="radio" name="RSSEnabled" value="1" /> Yes.
      Number of articles in RSS feed: <input type="text" name="numArticlesRSS" size="2" value="20" />
    </div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="displayArticleDate" value="1" checked /></div>
    <div class="col-68-left">Display date on front-end (index and detail pages).</div>
</div>
</form>
</div>