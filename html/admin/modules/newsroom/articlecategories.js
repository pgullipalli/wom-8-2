$(function () {
	loadCategoryList();
});


function loadCategoryList() {
  xsite.load("#mainDiv", "index.cfm?md=newsroom&tmp=snip_category_list&uuid=" + xsite.getUUID());
}

/*--------------------------------*/
/*    BEGIN: Add Category         */
/*--------------------------------*/
function addCategory() {
  xsite.createModalDialog({
	windowName: 'winAddCat',
	title: 'Add Category',
	position: 'center-up',
	width: 650,
	url: 'index.cfm?md=newsroom&tmp=snip_add_category&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddCat").dialog('close'); },
	  ' Add ': submitForm_AddCategory
	}
  }).dialog('open');
  
  function submitForm_AddCategory() {
	var form = $("#frmAddCategory");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=newsroom&task=addArticleCategory',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddCat").dialog('close');
		loadCategoryList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------*/
/*    END: Add Category           */
/*--------------------------------*/

/*--------------------------------*/
/*    BEGIN: Edit Category        */
/*--------------------------------*/
function editCategory() {
  var form = $("#frmCategoryList")[0];
  var catID = xsite.getCheckedValue(form.articleCategoryID);
  if (!catID) {
	alert("Please select a category to edit.");
	return;
  }
  xsite.createModalDialog({
	windowName: 'winEditCat',
	title: 'Edit Category',
	position: 'center-up',
	width: 650,
	url: 'index.cfm?md=newsroom&tmp=snip_edit_category&catID=' + catID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditCat").dialog('close'); },
	  ' Save ': submitForm_EditCategory
	}
  }).dialog('open');
  
  function submitForm_EditCategory() {
	var form = $("#frmEditCategory");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=newsroom&task=editArticleCategory',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
	  return;	
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditCat").dialog('close');
		loadCategoryList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}	  
  }
}
/*--------------------------------*/
/*    END: Edit Category          */
/*--------------------------------*/


/*--------------------------------*/
/*  BEGIN: Delete Category        */
/*--------------------------------*/
function deleteCategory() {
  var form = $("#frmCategoryList")[0];
  var catID = xsite.getCheckedValue(form.articleCategoryID);
  if (!catID) {
	alert("Please select a category to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected category?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=newsroom&task=deleteArticleCategory&catID=' + catID, {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadCategoryList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }
}
/*--------------------------------*/
/*  END: Delete Category          */
/*--------------------------------*/