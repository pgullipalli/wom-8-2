<cfparam name="URL.keyword" default="">

<script type="text/javascript" src="modules/newsroom/searcharticles.js"></script>

<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>
<cfset allCategories=NR.getAllArticleCategories()>
<cfif allCategories.recordcount IS 0>
  <!--- no page categories were ever created --->
  <cflocation url="index.cfm?md=newsroom&tmp=articlecategories&wrap=1">
</cfif>

<div class="header">INFORMATION LIBRARY &gt; Search Resources</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editArticle(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="copyArticle();">Copy</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteArticle();">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="editRelatedItems();">Related Items</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="preview();">Preview</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="getURL();">Get URL</a>
</div>

<br /><br /><br />

<cfoutput>
<form id="frmSearchArticles" onsubmit="return false">
Enter a keyword:
<input type="text" name="keyword" size="20" maxlength="30" value="#HTMLEditFormat(URL.keyword)#">
<input type="button" value=" Submit " onclick="loadArticleList(null);">
&nbsp;&nbsp;&nbsp;&nbsp;
<a href="index.cfm?md=newsroom&tmp=articles&wrap=1">Browse By Categories</a><br />
<input type="checkbox" name="includeArticleContent" value="1"> Include resource content in the search<br />
<input type="checkbox" name="featuredOnHomepage" value="1"> Display featured resources only
</form>
</cfoutput>

<div id="mainDiv"></div>
