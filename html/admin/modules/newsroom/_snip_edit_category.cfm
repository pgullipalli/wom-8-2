<cfparam name="URL.catID">
<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>
<cfset articleCategory=NR.getArticleCategory(URL.catID)>

<cfoutput query="articleCategory">
<div class="snip">
<form id="frmEditCategory" name="frmEditCategory" onsubmit="return false">
<input type="hidden" name="articleCategoryID" value="#URL.catID#" />
<div class="row">
	<div class="col-30-right"><b>Category Name:</b></div>
    <div class="col-68-left">
      <input type="text" name="articleCategory" size="45" maxlength="50" value="#HTMLEditFormat(articleCategory)#" validate="required:true" />
    </div>
</div>
<div class="row">
	<div class="col-30-right">Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmEditCategory"
            fieldName="description"
            cols="45"
            rows="3"
            HTMLContent="#description#"
            displayHTMLSource="true"
            editorHeader="Edit Description">
    </div>
</div>
<div class="row">
	<div class="col-30-right">Contact Info:</div>
    <div class="col-68-left">
      <input type="text" name="contactInfo" size="45" maxlength="150" value="#HTMLEditFormat(contactInfo)#" />
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>RSS Enabled?</b></div>
    <div class="col-68-left">
      <input type="radio" name="RSSEnabled" value="0" <cfif Not RSSEnabled>checked</cfif> /> No<br>
      <input type="radio" name="RSSEnabled" value="1" <cfif RSSEnabled>checked</cfif> /> Yes.
      Number of articles in RSS feed: <input type="text" name="numArticlesRSS" size="2" value="#numArticlesRSS#" />
    </div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="displayArticleDate" value="1" <cfif displayArticleDate>checked</cfif> /></div>
    <div class="col-68-left">Display date on front-end (index and detail pages).</div>
</div>
</form>
</div>
</cfoutput>