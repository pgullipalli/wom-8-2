var pageNum=1;//initial page number

$(function() {
    loadArticleList(null);
});

function loadArticleList(pNum) {
  if (pNum) pageNum = pNum;
  var form=$("#frmSearchArticles")[0];
  var keyword=form.keyword.value;
  if (keyword == "") {
	if (!form.featuredOnHomepage.checked) {
	  //alert('Please enter a keyword or check off "Display featured resources only".');
	  return;	  
	}
  }
  if (keyword.length > 0 && keyword.length < 3 && !form.featuredOnHomepage.checked) { alert("Please enter a keyword with at least 3 characters."); return; }	
  var includeArticleContent=0;
  if (form.includeArticleContent.checked) includeArticleContent=1;
  var featuredOnHomepage=0;
  if (form.featuredOnHomepage.checked) featuredOnHomepage=1;
  var url="index.cfm?md=newsroom&tmp=snip_article_list&keyword=" + escape(keyword) + "&pageNum=" + pageNum + "&includeArticleContent=" + includeArticleContent +
  		  "&featuredOnHomepage=" + featuredOnHomepage + "&browseBy=search&uuid=" + xsite.getUUID();
  xsite.load("#mainDiv", url);
}

/* the following copied from articles.js */
function editArticle(artID) {
  var form = $("#frmArticleList")[0];
  var articleID = null;
  if (artID) articleID = artID;
  else articleID = xsite.getCheckedValue(form.articleID);

  if (!articleID) {
	alert("Please select an item to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditArticle',
	title: 'Edit Article',
	width: 800,
	height: 500,
	position: 'center-up',
	url: 'index.cfm?md=newsroom&tmp=snip_edit_article&articleID=' + articleID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {//create date pickers for date fields
	  $("#articleDate2").datepicker();  
	  $("#publishDate2").datepicker();  
	  $("#unpublishDate2").datepicker();  
	},
	buttons: {
	  'Cancel': function() { $("#winEditArticle").dialog('close'); },
	  ' Save ': submitForm_EditArticle
	}
  }).dialog('open');
	
  function submitForm_EditArticle() {
	var form = $("#frmEditArticle");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=newsroom&task=editArticle',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  beforeSubmit: validate,
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function validate() {
	  var form=$("#frmEditArticle")[0];
	  if (form.contentType[1].checked && form.externalURL.value == "") {
	    alert('Please enter an external URL for the content of this item.');
	    return false;
	  } else if (form.contentType[2].checked && form.documentFile.value == "") {
	    alert('Please select a file for the content of this item.');
	    return false;
	  } 
	}
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditArticle").dialog('close');
		loadArticleList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function copyArticle() {
  var form = $("#frmArticleList")[0];
  var articleID = xsite.getCheckedValue(form.articleID);
  if (!articleID) {
	alert("Please select an item to copy from.");
	return;
  }
  
  xsite.createModalDialog({
	windowName: 'winCopyArticle',
	title: 'Copy Article',
	width: 650,
	position: 'center-up',
	url: 'index.cfm?md=newsroom&tmp=snip_copy_article&articleID=' + articleID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winCopyArticle").dialog('close'); },
	  ' Copy ': submitForm_CopyArticle
	}
  }).dialog('open');
  
  function submitForm_CopyArticle() {
	var form = $("#frmCopyArticle");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=newsroom&task=copyArticle',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winCopyArticle").dialog('close');
		loadArticleList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteArticle() {
  var form = $("#frmArticleList")[0];
  var articleID = xsite.getCheckedValue(form.articleID);
  if (!articleID) {
	alert("Please select an item to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected item?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=newsroom&task=deleteArticle&articleID=' + articleID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadArticleList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function editRelatedItems() {
  var form=$("#frmSearchArticles")[0];
  var keyword=form.keyword.value;
  var includeArticleContent=0;
  if (form.includeArticleContent.checked) includeArticleContent=1;
  
  var form = $("#frmArticleList")[0];
  var articleID = xsite.getCheckedValue(form.articleID);
  if (!articleID) {
	alert("Please select an item to manage related items.");
	return;
  }
  window.location = "index.cfm?md=newsroom&tmp=relateditems&wrap=1&articleID=" + articleID + "&keyword=" + keyword + "&includeArticleContent=" + includeArticleContent + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();
}

function preview() {
  var catID=$("#frmNavigator select[name='articleCategoryID']").val();
  var form = $("#frmArticleList")[0];
  var articleID = xsite.getCheckedValue(form.articleID);
  if (!articleID) {
	alert("Please select an item to preview.");
	return;
  }
  window.open("/index.cfm?md=newsroom&tmp=detail&articleID=" + articleID + "&catID=" + catID + "&uuid=" + xsite.getUUID(), "_blank");
}



function getNow(form, fieldType) {
  var now = new Date();
  var dateString = (now.getMonth() + 1) + "/" + now.getDate() + "/" + now.getFullYear();
  var hh=now.getHours();
  var mm=now.getMinutes();
  var ampm = 0;
  if (hh >= 12) ampm = 1;
  if (hh > 12) hh = hh - 12;
  
  switch (fieldType) {
	case 'publish':
	  form.publishDate.value = dateString;
	  form.publishDate_hh.selectedIndex = hh;
	  form.publishDate_mm.selectedIndex = mm - 1;
	  form.publishDate_ampm.selectedIndex = ampm;
	  break;
	case 'unpublish':
	  form.unpublishDate.value = dateString;
	  form.unpublishDate_hh.selectedIndex = hh;
	  form.unpublishDate_mm.selectedIndex = mm - 1;
	  form.unpublishDate_ampm.selectedIndex = ampm;
	  break;
	case 'article':
	  form.articleDate.value = dateString;
	  form.articleDate_hh.selectedIndex = hh;
	  form.articleDate_mm.selectedIndex = mm - 1;
	  form.articleDate_ampm.selectedIndex = ampm;
	  break;
	default: break;	  
  }
}

function clearDate(form, fieldType) {
  switch (fieldType) {
	case 'publish':
	  form.publishDate.value = "";
	  form.publishDate_hh.selectedIndex = 0;
	  form.publishDate_mm.selectedIndex = 0;
	  form.publishDate_ampm.selectedIndex = 0;
	  break;
	case 'unpublish':
	  form.unpublishDate.value = "";
	  form.unpublishDate_hh.selectedIndex = 0;
	  form.unpublishDate_mm.selectedIndex = 0;
	  form.unpublishDate_ampm.selectedIndex = 0;
	  break;
	case 'article':
	  form.articleDate.value = "";
	  form.articleDate_hh.selectedIndex = 0;
	  form.articleDate_mm.selectedIndex = 0;
	  form.articleDate_ampm.selectedIndex = 0;
	  break;
	default: break;	  
  }
}

function toggleContentType(formName) {
  var form = document[formName];
  if (formName == "frmAddArticle") {//add article
	  document.getElementById("contentType1").style.display="none";
	  document.getElementById("contentType2").style.display="none";
	  document.getElementById("contentType3").style.display="none";
	  document.getElementById("contentType4").style.display="none";
	  if (form.contentType[0].checked) document.getElementById("contentType1").style.display="block";
	  else if (form.contentType[1].checked) document.getElementById("contentType2").style.display="block";
	  else if (form.contentType[2].checked) document.getElementById("contentType3").style.display="block";
	  else if (form.contentType[3].checked) document.getElementById("contentType4").style.display="block";
  } else if (formName == "frmEditArticle") {//edit article
	  document.getElementById("contentType5").style.display="none";
	  document.getElementById("contentType6").style.display="none";
	  document.getElementById("contentType7").style.display="none";
	  document.getElementById("contentType8").style.display="none";
	  if (form.contentType[0].checked) document.getElementById("contentType5").style.display="block";
	  else if (form.contentType[1].checked) document.getElementById("contentType6").style.display="block";
	  else if (form.contentType[2].checked) document.getElementById("contentType7").style.display="block";
	  else if (form.contentType[3].checked) document.getElementById("contentType8").style.display="block";
  }
}

function getURL() {
  var form = $("#frmArticleList")[0];
  var articleID = xsite.getCheckedValue(form.articleID);
  if (!articleID) {
	alert("Please select an item to get URL.");
	return;
  }
  
  xsite.createModalDialog({
	windowName: 'winGetURL',
	title: 'Get Resource URL',
	width: 550,
	position: 'center',
	url: 'index.cfm?md=newsroom&tmp=snip_show_url&articleID=' + articleID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'OK': function() { $("#winGetURL").dialog('close'); }
	}
  }).dialog('open');
}