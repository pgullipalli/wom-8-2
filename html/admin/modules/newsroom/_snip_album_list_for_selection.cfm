<cfparam name="URL.catID">
<cfparam name="URL.articleID">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
<cfset structResult=PG.getAlbumsByCategoryID(albumCategoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>

<cfif structResult.numAllItems IS 0>
  <p>No items created in this category.</p>
<cfelse>
  <cfif structResult.numDisplayedItems GT 0>
    <form id="frmAlbumList">
    <cfoutput><input type="hidden" name="articleID" value="#URL.articleID#"></cfoutput>
    <div class="itemList">
    <table>
      <tr>
        <th width="40">Select</th>
        <th>Gallery Name</th>
      </tr>   
      </tr>  
      <cfoutput query="structResult.albums">
      <tr>
        <td align="center"><input type="checkbox" name="albumIDList" value="#albumID#"></td>
        <td>#albumTitle# (#DateFormat(albumDate, "mm/dd/yyyy")#)</td> 
      </tr>
      </cfoutput>
    </table>
    </div>
    </form>
    
    <cfif structResult.numAllItems GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(structResult.numAllItems/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadAlbumListForSelection(#idx#)">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
    </cfif>
  </cfif>
</cfif>