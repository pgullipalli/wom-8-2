<cfparam name="URL.articleID">

<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>
<cfset relatedItems=NR.getRelatedItems(URL.articleID)>

<cfif relatedItems.recordcount Is 0>
<p>No related items created for this item. Click "New" button in the action bar to create one.</p>
<cfelse>
<form id="frmRelatedItemsList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="120">Display Order</th>
    <th width="450">Item Name</th>
    <th width="150">Item Type</th>
    <th>&nbsp;</th>
  </tr>  
  <cfoutput query="relatedItems">
  <tr>
    <td align="center"><input type="checkbox" name="articleRelatedItemIDList" value="#articleRelatedItemID#"></td>
    <td align="center">
      <a href="##" onclick="moveRelatedItem('#articleRelatedItemID#','up')"><img src="images/icon_up.gif" border="0" align="absbottom" alt="move up"></a>
      <a href="##" onclick="moveRelatedItem('#articleRelatedItemID#','down')"><img src="images/icon_down.gif" border="0" align="absbottom" alt="move down"></a>
    </td>
	<td>
	  <cfif relatedItemType Is "article">
        #DateFormat(articleDate, "mm/dd/yyyy")# - #headline#
      <cfelseif relatedItemType Is "gallery">
        #DateFormat(albumDate, "mm/dd/yyyy")# - #albumTitle#
      <cfelseif relatedItemType Is "link">
        #linkName# (#linkURL#)
      <cfelseif relatedItemType Is "file">
        #fileTitle# (#filePath# - #fileSize#)
      </cfif>    
    </td> 
	<td align="center"><i>#relatedItemType#</i></td>
    <td>&nbsp;</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>