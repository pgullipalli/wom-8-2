<cfparam name="URL.articleID">
<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>
<cfset Util=CreateObject("component","com.Utility").init()>
<cfset allCategories=NR.getAllArticleCategories()>
<cfset articleInfo=NR.getArticleByID(URL.articleID)>
<cfset articleCategoryIDList=NR.getArticleCategoryIDListByArticleID(URL.articleID)>
<cfset publishDateStruct=Util.convertDateTimeObjectToString(DateAdd("d",0,articleInfo.publishDate))>
<cfset unpublishDateStruct=Util.convertDateTimeObjectToString(DateAdd("d",0,articleInfo.unpublishDate))>
<cfset articleDateStruct=Util.convertDateTimeObjectToString(DateAdd("d",0,articleInfo.articleDate))>

<cfoutput query="articleInfo">
<form id="frmEditArticle" name="frmEditArticle" onsubmit="return false">
<input type="hidden" name="articleID" value="#URL.articleID#" />
<div class="row">
	<div class="col-30-right"><b>Categories:</b><br /><small>(Check all that apply.)</small></div>
    <div class="col-68-left">
      <cfset numCategories=allCategories.recordcount>
      <cfset numRows=Ceiling(numCategories/3)>
      <cfset idx = 0>
      <cfloop index="row" from="1" to="#numRows#">
        <div class="row">
        <cfloop index="col" from="1" to="3">
          <cfset idx = (row - 1) * 3 + col>
          <cfif idx LTE numCategories>
            <cfset articleCategoryID=allCategories.articleCategoryID[idx]>
            <cfset articleCategory=allCategories.articleCategory[idx]>
            <div class="col-30-left">
              <input type="checkbox" name="articleCategoryIDList" value="#articleCategoryID#"<cfif ListFind(articleCategoryIDList, articleCategoryID) GT 0> checked</cfif> />
              #articleCategory#
            </div>
          </cfif>
        </cfloop>
        </div>
      </cfloop>
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Headline:</b></div>
    <div class="col-68-left">
      <input type="text" name="headline" maxlength="255" value="#HTMLEditFormat(headline)#" style="width:350px;" validate="required:true"><br />
      <input type="checkbox" name="featuredOnHomepage" value="1" <cfif featuredOnHomepage>checked</cfif> /> Featured on home/sub-home page
    </div>
</div>
<div class="row">
	<div class="col-30-right">Sub-Headline:</div>
    <div class="col-68-left">
      <input type="text" name="subHeadline" maxlength="200" value="#HTMLEditFormat(subHeadline)#" style="width:350px;">
    </div>
</div>
<div class="row">
	<div class="col-30-right">Byline:</div>
    <div class="col-68-left">
      <input type="text" name="byline" maxlength="100" value="#HTMLEditFormat(byline)#" style="width:120px;"> &nbsp;&nbsp;&nbsp;&nbsp;
      Dateline:
      <input type="text" name="dateline" maxlength="100" value="#HTMLEditFormat(dateline)#" style="width:120px;">
    </div>
</div>

<div class="row">
	<div class="col-30-right">Teaser:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmEditArticle"
            fieldName="teaser"
            cols="65"
            rows="3"
            HTMLContent="#teaser#"
            displayHTMLSource="true"
            editorHeader="Edit Article Teaser">
    </div>
</div>

<div class="row">
	<div class="col-30-right"><b>Publish?</b></div>
    <div class="col-68-left">
      <div class="row">
        <input type="radio" name="published" value="1" <cfif published>checked</cfif> /> Yes
      </div>
      <div class="row">
        <div class="col-24-right">Publish Date:</div>
        <div class="col-74-left">
          <cfif published>
            <input type="text" name="publishDate" id="publishDate2" value="#publishDateStruct.date#" style="width:60px;" validate="required:false, date:true" />
            <select name="publishDate_hh">
              <option value="12">12</option>
              <cfloop index="hh" from="1" to="12"><option value="#hh#"<cfif publishDateStruct.hh Is hh> selected</cfif>><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
            </select>
            <select name="publishDate_mm">
              <cfloop index="mm" from="0" to="59"><option value="#mm#"<cfif publishDateStruct.mm Is mm> selected</cfif>><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
            </select>
            <select name="publishDate_ampm">
              <option value="am"<cfif publishDateStruct.ampm Is "AM"> selected</cfif>>AM</option><option value="pm"<cfif publishDateStruct.ampm Is "PM"> selected</cfif>>PM</option>
            </select>
          <cfelse>
            <input type="text" name="publishDate" id="publishDate2" value="" style="width:60px;" validate="required:false, date:true" />
            <select name="publishDate_hh">
              <option value="12">12</option>
              <cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
            </select>
            <select name="publishDate_mm">
              <cfloop index="mm" from="0" to="59"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
            </select>
            <select name="publishDate_ampm">
              <option value="am">AM</option><option value="pm">PM</option>
            </select>
          </cfif>
          <input type="button" value="Now" onclick="getNow(document.frmEditArticle, 'publish')" />
          <input type="button" value="Clear" onclick="clearDate(document.frmEditArticle, 'publish')" />
        </div>
      </div>
      <div class="row">
        <div class="col-24-right">Unpublish Date:</div>
        <div class="col-74-left">
          <cfif articleInfo.published AND Year(articleInfo.unpublishDate) IS NOT "3000">
            <input type="text" name="unpublishDate" id="unpublishDate2" value="#unpublishDateStruct.date#" style="width:60px;" validate="required:false, date:true" />
            <select name="unpublishDate_hh">
              <option value="12">12</option>
              <cfloop index="hh" from="1" to="12"><option value="#hh#"<cfif unpublishDateStruct.hh Is hh> selected</cfif>><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
            </select>
            <select name="unpublishDate_mm">
              <cfloop index="mm" from="0" to="59"><option value="#mm#"<cfif unpublishDateStruct.mm Is mm> selected</cfif>><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
            </select>
            <select name="unpublishDate_ampm">
              <option value="am"<cfif unpublishDateStruct.ampm Is "AM"> selected</cfif>>AM</option><option value="pm"<cfif unpublishDateStruct.ampm Is "PM"> selected</cfif>>PM</option>
            </select>
          <cfelse>
            <input type="text" name="unpublishDate" id="unpublishDate2" style="width:60px;" validate="required:false, date:true" />
            <select name="unpublishDate_hh">
              <option value="12">12</option>
              <cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
            </select>
            <select name="unpublishDate_mm">
              <cfloop index="mm" from="0" to="59"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
            </select>
            <select name="unpublishDate_ampm">
              <option value="am">AM</option><option value="pm">PM</option>
            </select>
          </cfif>
          <input type="button" value="Now" onclick="getNow(document.frmEditArticle, 'unpublish')" />
          <input type="button" value="Clear" onclick="clearDate(document.frmEditArticle, 'unpublish')" />
        </div>
      </div>
      <div class="row">
        <input type="radio" name="published" value="0" <cfif Not published>checked</cfif> /> No
      </div>    
    </div>
</div>

<div class="row">
	<div class="col-30-right"><b>Article Date:</b></div>
    <div class="col-68-left">
	  <input type="text" name="articleDate" id="articleDate2" style="width:60px;" value="#articleDateStruct.date#" validate="required:true, date:true" />
      <select name="articleDate_hh">
        <option value="12">12</option>
        <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"<cfif articleDateStruct.hh Is hh> selected</cfif>><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
      </select>
      <select name="articleDate_mm">
        <cfoutput><cfloop index="mm" from="0" to="59"><option value="#mm#"<cfif articleDateStruct.mm Is mm> selected</cfif>><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
      </select>
      <select name="articleDate_ampm">
        <option value="am"<cfif articleDateStruct.ampm Is "AM"> selected</cfif>>AM</option><option value="pm"<cfif articleDateStruct.ampm Is "PM"> selected</cfif>>PM</option>
      </select>
      <input type="button" value="Now" onclick="getNow(document.frmEditArticle, 'article')" />
      <input type="button" value="Clear" onclick="clearDate(document.frmEditArticle, 'article')" /><br />
      <small>(May be earlier than Publish Date if necessary.)</small><br />
      <input type="checkbox" name="displayArticleDate" value="1" <cfif displayArticleDate>checked</cfif> /> Display date on front-end (index and detail pages).
	</div>
</div>

<div class="row">
	<div class="col-30-right"><b>Content Type:</b></div>
    <div class="col-68-left">
      <input type="radio" name="contentType" value="1" onclick="toggleContentType('frmEditArticle')" <cfif contentType IS "1">checked</cfif> /> Article (HTML content)
      <input type="radio" name="contentType" value="2" onclick="toggleContentType('frmEditArticle')" <cfif contentType IS "2">checked</cfif> /> External URL
      <input type="radio" name="contentType" value="3" onclick="toggleContentType('frmEditArticle')" <cfif contentType IS "3">checked</cfif> /> File (Document Files)
      <input type="radio" name="contentType" value="4" onclick="toggleContentType('frmEditArticle')"  <cfif contentType IS "4">checked</cfif>/> Media File (Audio or Video)
    </div>
</div>

<div class="row" id="contentType5" style="display:<cfif contentType IS "1">block<cfelse>none</cfif>;">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      Use WYSIWIG ("What You See Is What You Get") tool to type text, cut-and-paste from another source,
      or enter HTML content.<br />
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="false"
            formName="frmEditArticle"
            fieldName="fullStory"
            cols="65"
            rows="4"
            HTMLContent="#fullStory#"
            displayHTMLSource="true"
            editorHeader="Edit Article Content">    
    </div>
</div>
<div class="row" id="contentType6" style="display:<cfif contentType IS "2">block<cfelse>none</cfif>;">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      External URL: <input type="text" name="externalURL" style="width:360px;" value="#HTMLEditFormat(externalURL)#" />
    </div>
</div>
<div class="row" id="contentType7" style="display:<cfif contentType IS "3">block<cfelse>none</cfif>;">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      Select a File: <input type="hidden" name="documentFile" value="#HTMLEditFormat(documentFile)#">
      <CF_SelectFile formName="frmEditArticle" fieldName="documentFile" previewID="preview2-1" previewWidth="100">
	  <div id="preview2-1">#HTMLEditFormat(documentFile)#</div>
    </div>
</div>
<div class="row" id="contentType8" style="display:<cfif contentType IS "4">block<cfelse>none</cfif>;">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      Media Type: <input type="radio" name="mediaFileType" value="V" <cfif Not Compare(mediaFileType,"V")>checked</cfif> /> Video <input type="radio" name="mediaFileType" value="A" <cfif Not Compare(mediaFileType,"A")>checked</cfif>  /> Audio<br />
      Select a Media File:<input type="hidden" name="mediaFile" value="#HTMLEditFormat(mediaFile)#">
      <CF_SelectFile formName="frmEditArticle" fieldName="mediaFile" previewID="preview2-2" previewWidth="100">
	  <div id="preview2-2"><cfif Compare(mediaFile,"")>/#mediaFile#</cfif></div>
      OR<br />
      Enter a Media URL: <input type="text" name="mediaFileURL" maxlength="200" style="width:350px;" value="#HTMLEditFormat(mediaFileURL)#" />
    </div>
</div>

<div class="row">
  <div class="col-30-right">&nbsp;</div>
  <div class="col-68-left"><input type="checkbox" name="popup" value="1" <cfif popup>checked</cfif> /> Open in a new window</div>
</div>

<div class="row">
	<div class="col-30-right">Feature Image:<br /><small>(Width: #Application.newsroom.featureImageWidth#px)</small></div>
    <div class="col-68-left">
      <div class="row">
        <input type="hidden" name="featureImage" value="#HTMLEditFormat(featureImage)#">
        <CF_SelectFile formName="frmEditArticle" fieldName="featureImage" previewID="preview5" previewWidth="150">
        <!--- Image Alignment:
        <input type="radio" name="featureImageAlignment" value="L" <cfif featureImageAlignment IS "L">checked</cfif> /> Left <input type="radio" name="featureImageAlignment" value="R" <cfif featureImageAlignment IS "R">checked</cfif> /> Right --->
        <cfif Compare(featureImage, "")>
          <div id="preview5"><img src="/#featureImage#" border="0" width="150" /></div>
        <cfelse>
	      <div id="preview5"></div>
        </cfif>
      </div>
      <div class="row">
        Caption:<br />
        <textarea name="featureImageCaption" style="width:360px;height:40px;">#HTMLEditFormat(featureImageCaption)#</textarea>
      </div>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Thumbnail Image:<br /><small>(Width: #Application.newsroom.thumbnailWidth#px)</small></div>
    <div class="col-68-left">
      <div class="row">
        <div class="row">
          <input type="checkbox" name="resizeForThumbnail" value="1" /> 
          Resize the feature image to a smaller image and use it as thumbnail image; OR
        </div>
        <div class="row"> 
          <input type="hidden" name="featureThumbnail" value="#HTMLEditFormat(featureThumbnail)#">
          <CF_SelectFile formName="frmEditArticle" fieldName="featureThumbnail" previewID="preview6" previewWidth="100">
          <cfif Compare(featureThumbnail, "")>
            <div id="preview6"><img src="/#featureThumbnail#" border="0" width="100" /></div>
          <cfelse>
	        <div id="preview6"></div>
          </cfif>
        </div>
      </div>
    </div>
</div>
</form>
</cfoutput>