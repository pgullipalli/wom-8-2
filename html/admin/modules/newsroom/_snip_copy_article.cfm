<cfparam name="URL.articleID">

<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>
<cfset articleInfo=NR.getArticleByID(URL.articleID)>

<p>
<b>Copy from:</b>
<cfoutput>#DateFormat(articleInfo.articleDate, "mm.dd.yyyy")# - #articleInfo.headline#</cfoutput>
</p>

<form id="frmCopyArticle" name="frmCopyArticle" onsubmit="return false">
<cfoutput>
<input type="hidden" name="articleID" value="#URL.articleID#">
<div class="row">
	<div class="col-30-right"><b>New Headline:</b></div>
    <div class="col-68-left"><input type="text" name="newHeadline" maxlength="255" style="width:300px;" value="#HTMLEditFormat(articleInfo.headline)#" validate="required:true"></div>
</div>

<div class="row">
	<div class="col-30-right">New Sub-Headline:</div>
    <div class="col-68-left"><input type="text" name="newSubHeadline" maxlength="255" value="#HTMLEditFormat(articleInfo.subHeadline)#" style="width:300px;"></div>
</div>

<div class="row">
	<div class="col-30-right">New Byline:</div>
    <div class="col-68-left">
      <input type="text" name="newByline" maxlength="100" value="#HTMLEditFormat(articleInfo.byline)#" style="width:120px;"> &nbsp;&nbsp;
      New Dateline:
      <input type="text" name="newDateline" maxlength="100" value="#HTMLEditFormat(articleInfo.dateline)#" style="width:120px;">
    </div>
</div>
</cfoutput>
</form>