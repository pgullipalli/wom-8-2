<cfparam name="URL.articleID">
<cfparam name="URL.catID" default="0">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.keyword" default="">
<cfparam name="URL.includeArticleContent" default="0">

<script type="text/javascript" src="modules/newsroom/relateditems.js"></script>

<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>
<cfset articleInfo=NR.getArticleByID(URL.articleID)>

<div class="header">INFORMATION LIBRARY &gt; Related Items</div>

<div id="actionBar">
<a href="#" id="btnNew" class="fg-button ui-state-default ui-corner-all button65" onclick="addRelatedItem()">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteRelatedItems()">Remove</a>
</div>

<div id="divSubMenuNew" style="position:absolute;display:none;">
<table class="subMenu">
  <tr valign="top">
    <td><a href="javascript:addRelatedItem('article');">Other Resources</a></td>
  </tr>
  <tr valign="top">
    <td><a href="javascript:addRelatedItem('gallery')">Photo Galleries</a></td>
  </tr>
  <tr valign="top">
    <td><a href="javascript:addRelatedItem('link')">Links</a></td>
  </tr>
  <tr valign="top">
    <td><a href="javascript:addRelatedItem('file')">Files</a></td>
  </tr>
</table>
</div>

<br /><br />

<cfoutput>
<cfif Compare(URL.keyword, "")>
<p><a href="index.cfm?md=newsroom&tmp=searcharticles&keyword=#URLEncodedFormat(URL.keyword)#&includeArticleContent=#URL.includeArticleContent#&browseBy=search&wrap=1">&laquo; Back to Resource List</a></p>
<cfelse>
<p><a href="index.cfm?md=newsroom&tmp=articles&catID=#URL.catID#&wrap=1">&laquo; Back to Resource List</a></p>
</cfif>

<p>
<b>Items Related To </b> #DateFormat(articleInfo.articleDate, "mm/dd/yyyy")# - #articleInfo.headline#
</p>
</cfoutput>

<div id="mainDiv"></div>



