var articleID;

$(function () {	
	articleID = xsite.getURLParameter("articleID");
	
	loadItemList();
	   
	$("body").click(function(e) { 
	  if (e.target.id != "btnNew" && e.target.id != "divSubMenuNew") $("#divSubMenuNew").hide();
	});
	
});

function loadItemList() {  
  var url="index.cfm?md=newsroom&tmp=snip_relateditem_list&articleID=" + articleID + "&uuid=" + xsite.getUUID();
  xsite.load("#mainDiv", url);
}

function addRelatedItem(itemType) {
  if (itemType) {
	  switch(itemType) {	
		case 'article':
		  xsite.createModalDialog({
			windowName: 'winAddRelatedArticles',
			title: 'Add Related Items',
			width: 600,
			height:500,
			position: 'center-up',
			url: 'index.cfm?md=newsroom&tmp=snip_add_relatedarticles&articleID=' + articleID + '&uuid=' + xsite.getUUID(),
			urlCallback: function () {
			  $("#frmNavigator").unbind();
			  $("#frmNavigator select[name='articleCategoryID']").change(function() {
				loadArticleListForSelection(1);	  
			  });
			  loadArticleListForSelection(1);
			},
			buttons: {
			  'Cancel': function() { $("#winAddRelatedArticles").dialog('close'); },
			  ' Add ': submitForm_AddRelatedArticles
			}
		  }).dialog('open');
		  break;
		case 'gallery':
		  xsite.createModalDialog({
			windowName: 'winAddRelatedGalleries',
			title: 'Add Related Galleries',
			width: 600,
			height:500,
			position: 'center-up',
			url: 'index.cfm?md=newsroom&tmp=snip_add_relatedgalleries&articleID=' + articleID + '&uuid=' + xsite.getUUID(),
			urlCallback: function () {
			  $("#frmNavigator").unbind();
			  $("#frmNavigator select[name='albumCategoryID']").change(function() {
				loadAlbumListForSelection(1);	  
			  });
			  loadAlbumListForSelection(1);
			},
			buttons: {
			  'Cancel': function() { $("#winAddRelatedGalleries").dialog('close'); },
			  ' Add ': submitForm_AddRelatedGalleries
			}
		  }).dialog('open');
		  break;
		case 'link':
		  xsite.createModalDialog({
			windowName: 'winAddRelatedLink',
			title: 'Add Related Link',
			width: 600,
			position: 'center-up',
			url: 'index.cfm?md=newsroom&tmp=snip_add_relatedlink&articleID=' + articleID + '&uuid=' + xsite.getUUID(),
			buttons: {
			  'Cancel': function() { $("#winAddRelatedLink").dialog('close'); },
			  ' Add ': submitForm_AddRelatedLink
			}
		  }).dialog('open');	
		  break;
		case 'file':
		  xsite.createModalDialog({
			windowName: 'winAddRelatedFile',
			title: 'Add Related File',
			width: 600,
			position: 'center-up',
			url: 'index.cfm?md=newsroom&tmp=snip_add_relatedfile&articleID=' + articleID + '&uuid=' + xsite.getUUID(),
			buttons: {
			  ' Add ': submitForm_AddRelatedFile,
			  'Cancel': function() { $("#winAddRelatedFile").dialog('close'); }
			}
		  }).dialog('open');
		  break;
		default: break;
	  }
  } else {
	  var offset = $("#btnNew").offset();
	  var height=$("#actionBar").height() + 2;
	  $("#divSubMenuNew").css({'left':offset.left+'px', 'top':(offset.top+height)+'px'}).toggle();
  }
}

function loadArticleListForSelection(pageNum) {
  var catID=$("#frmNavigator select[name='articleCategoryID']").val();
  var url="index.cfm?md=newsroom&tmp=snip_article_list_for_selection&articleID=" + articleID + "&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();
  xsite.load("#subDiv1", url);
}

function submitForm_AddRelatedArticles() {
  var form = $("#frmArticleList");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=newsroom&task=addRelatedArticles',
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winAddRelatedArticles").dialog('close');
	  loadItemList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }
}

function loadAlbumListForSelection(pageNum) {
  var catID=$("#frmNavigator select[name='albumCategoryID']").val();
  var url="index.cfm?md=newsroom&tmp=snip_album_list_for_selection&articleID=" + articleID + "&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();
  xsite.load("#subDiv2", url);
}

function submitForm_AddRelatedGalleries() {
  var form = $("#frmAlbumList");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=newsroom&task=addRelatedGalleries',
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winAddRelatedGalleries").dialog('close');
	  loadItemList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }
}

function submitForm_AddRelatedLink() {
  var form = $("#frmAddRelatedLink");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=newsroom&task=addRelatedLink',
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winAddRelatedLink").dialog('close');
	  loadItemList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }
}

function submitForm_AddRelatedFile() {
  var form = $("#frmAddRelatedFile");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=newsroom&task=addRelatedFile',
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winAddRelatedFile").dialog('close');
	  loadItemList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }	
}


function deleteRelatedItems() {
  if(!document.getElementById('frmRelatedItemsList')) return;
  var form = $("#frmRelatedItemsList")[0];
  var articleRelatedItemIDList = xsite.getCheckedValue(form.articleRelatedItemIDList);
  if (!articleRelatedItemIDList) {
	alert("Please select at least an item to remove.");
	return;	  
  }
  if (!confirm('Are you sure you want to remove the selected items?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=newsroom&task=deleteRelatedItems&articleRelatedItemIDList=' + escape(articleRelatedItemIDList) + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  loadItemList();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function moveRelatedItem(articleRelatedItemID, direction) {
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=newsroom&task=moveRelatedItem&articleRelatedItemID=' + articleRelatedItemID + '&direction=' + direction + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  loadItemList();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

