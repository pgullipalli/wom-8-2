<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  NR=CreateObject("component", "com.NewsroomAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";

  switch (URL.task) {
    case "addArticleCategory":
	  NR.addArticleCategory(argumentCollection=Form);
      break;

    case "editArticleCategory":
	  NR.editArticleCategory(argumentCollection=Form);
      break;

    case "deleteArticleCategory":
      if (NOT NR.deleteArticleCategory(URL.catID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="It appears that there are still items in the category that you tried to delete. " &
	  		 "Please make sure you have deleted all items in this category before deleting this category.";	  
      }
      break;
  
    case "addArticle":	 
      if (Not NR.addArticle(argumentcollection=FORM)) {
        returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="Please select at least one category for this new item.";
      }
      break;
  
    case "editArticle": 
      if (Not NR.editArticle(argumentcollection=FORM)) {
        returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="Please select at least one category for this new item.";
      }
      break;
  
    case "copyArticle": 
      NR.copyArticle(argumentcollection=FORM);
      break;
  
    case "deleteArticle":
      NR.deleteArticle(URL.articleID);	
      break;
  
    case "addRelatedArticles":
	  NR.addRelatedArticles(argumentcollection=FORM);	
      break;
  
    case "addRelatedGalleries":
      NR.addRelatedGalleries(argumentcollection=FORM);	
      break;
  
    case "addRelatedLink":
      NR.addRelatedLink(argumentcollection=FORM);	
      break;
  
    case "addRelatedFile":
      NR.addRelatedFile(argumentcollection=FORM);	
      break;
  
    case "deleteRelatedItems":
      NR.deleteRelatedItems(articleRelatedItemIDList=URL.articleRelatedItemIDList);
      break;

    case "moveRelatedItem":
      NR.moveRelatedItem(articleRelatedItemID=URL.articleRelatedItemID, direction=URL.direction);
	  break;
	  
	default:
	  break;
  }
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>