var curriculaID;

$(function () {	
	curriculaID=$("#curriculaID").html();
	
	loadCourseList();	
});

function loadCourseList() {  
  var url="index.cfm?md=class&tmp=snip_curriculacourse_list&curriculaID=" + curriculaID + "&uuid=" + xsite.getUUID();
  xsite.load("#mainDiv", url);
}

function addCurriculaCourses() {
  xsite.createModalDialog({
	windowName: 'winAddCurriculaCourses',
	title: 'Add Courses',
	width: 600,
	height:500,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_add_relatedcourses&uuid=' + xsite.getUUID(),
	urlCallback: function () {
	  $("#frmNavigator").unbind();
	  $("#frmNavigator select[name='categoryID']").change(function() {
		loadCourseListForSelection(1);	  
	  });
	  loadCourseListForSelection(1);
	},
	buttons: {
	  'Cancel': function() { $("#winAddCurriculaCourses").dialog('close'); },
	  ' Add ': submitForm_AddCurriculaCourses
	}
  }).dialog('open');

}

function loadCourseListForSelection(pageNum) {
  var catID=$("#frmNavigator select[name='categoryID']").val();
  var url="index.cfm?md=class&tmp=snip_course_list_for_selection&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();
  xsite.load("#subDiv1", url);
}

function submitForm_AddCurriculaCourses() {
  var form = $("#frmCourseList");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=class&task=addCurriculaCourses&curriculaID=' + curriculaID,
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winAddCurriculaCourses").dialog('close');
	  loadCourseList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }
}

function deleteCurriculaCourses() {
  if(!document.getElementById('frmCurrilaCourseList')) return;
  var form = $("#frmCurrilaCourseList")[0];
  var courseIDList = xsite.getCheckedValue(form.courseIDList);
  if (!courseIDList) {
	alert("Please select at least an item to remove.");
	return;	  
  }
  if (!confirm('Are you sure you want to remove the selected items?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deleteCurriculaCourses&curriculaID=' + curriculaID + '&courseIDList=' + escape(courseIDList) + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  loadCourseList();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function moveItem(courseID, direction) {
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=moveCurriculaCourse&curriculaID=' + curriculaID + '&courseID=' + courseID + '&direction=' + direction + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  loadCourseList();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

