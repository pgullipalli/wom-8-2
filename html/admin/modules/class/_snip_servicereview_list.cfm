<cfparam name="URL.serviceID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset serviceReviews=CL.getServiceReviews(URL.serviceID)>
<cfset newLine=Chr(13) & Chr(10)>

<cfif serviceReviews.recordcount Is 0>
<p>No reviews written for this service. Click "New" button in the action bar to create one.</p>
<cfelse>
<form id="frmServiceReviewList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="120">Name</th>
    <th>Reviews</th>
    <th width="100">Publish?</th>
  </tr>  
  <cfoutput query="serviceReviews">
  <tr>
    <td align="center"><input type="radio" name="serviceReviewIDList" value="#serviceReviewID#"></td>
    <td>#fullName#&nbsp;</td>
    <td>#Replace(review, newLine, "<br />", "All")#&nbsp;</td>
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>