<cfparam name="URL.serviceReviewID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset serviceReview=CL.getServiceReview(URL.serviceReviewID)>

<cfoutput query="serviceReview">
<form id="frmEditServiceReview" name="frmEditServiceReview" onsubmit="return false;">
<input type="hidden" name="serviceReviewID" value="#URL.serviceReviewID#">
<div class="row">
	<div class="col-30-right"><b>Name:</b></div>
    <div class="col-68-left"><input type="text" name="fullName" maxlength="100" style="width:320px;" value="#HTMLEditFormat(fullName)#" /></div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left"><input type="checkbox" name="published" value="1" <cfif published>checked</cfif> /> Publish this review</div>
</div>
<div class="row">
    <div class="col-30-right"><b>Review:</b></div>
    <div class="col-68-left">
      <textarea name="review" style="width:320px;height:100px;">#HTMLEditFormat(review)#</textarea>
    </div>
</div>
</form>
</cfoutput>