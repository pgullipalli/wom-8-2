﻿$(function () {	
	$(".dateField").datepicker();
});

function resetAllDateRange() {
  var form = document.frmDateRange;
  if (form.allDateRange.checked) {
    form.startDate.value="1/1/1900";
    form.endDate.value="1/1/3000";
  }
}

function getReport() {
  var discountCode=$("#frmDateRange select[name='discountCode']").val();	
  var startDate=$("#frmDateRange input[name='startDate']").val();	
  var endDate=$("#frmDateRange input[name='endDate']").val();
  
  var url="index.cfm?md=class&tmp=snip_report_couponcode&discountCode=" + discountCode + "&startDate=" + startDate  + "&endDate=" + endDate + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}