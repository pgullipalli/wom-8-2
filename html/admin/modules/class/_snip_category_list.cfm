<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset categorySummary=CL.getCategorySummary()>

<cfif categorySummary.recordcount IS 0>
  <p>
  It appears that there are no categories created.
  Please click on the 'New' button in the action bar above to create the first category.
  </p>
<cfelse>
<form id="frmCategoryList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="250">Category Name</th>
    <th width="120">Display Order</th>
    <th width="100"># of Courses</th>
    <th width="100"># of Classses</th>
    <th width="100"># of Services</th>
    <th width="100">Featured</th>
    <th>URL (click to preview)</th>
  </tr>
  <cfoutput query="categorySummary">
  <tr>
    <td align="center"><input type="radio" name="categoryID" value="#categoryID#"></td>
    <td><a href="index.cfm?md=class&tmp=courses&wrap=1&catID=#categoryID#">#categoryName#</a></td>
    <td align="center">
      <a href="##" onclick="moveCategory('#categoryID#','up')"><img src="images/icon_up.gif" border="0" align="absbottom" alt="move up"></a>
      <a href="##" onclick="moveCategory('#categoryID#','down')"><img src="images/icon_down.gif" border="0" align="absbottom" alt="move down"></a>
    </td>
    <td align="center"><a href="index.cfm?md=class&tmp=courses&wrap=1&catID=#categoryID#" class="accent01">#numCourses#</a></td>
    <td align="center">#numClasses#</td>
    <td align="center"><a href="index.cfm?md=class&tmp=services&wrap=1&catID=#categoryID#" class="accent01">#numServices#</a></td>
    <td align="center"><cfif featured>Yes<cfelse>No</cfif></td>
    <td align="center"><a href="/index.cfm?md=class&tmp=category&catID=#categoryID#" target="_blank">/index.cfm?md=class&amp;tmp=category&amp;catID=#categoryID#</a></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>