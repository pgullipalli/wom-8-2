<cfparam name="URL.classType" default="Aerobics">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>

<cfset structClasses=CL.getFitnessClasses(classType=URL.classType, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
<cfif structClasses.numAllItems IS 0>
  <p>
  It appears that there are no classes created.
  Please click on the 'New' button in the action bar above to create the first class.
  </p>
</cfif>

<cfif structClasses.numDisplayedItems GT 0>
<form id="frmClassList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th>Class Title</th>
    <th>Class Type</th>
    <th>Instructor</th>
    <th>Location</th>
    <th width="100">Publish?</th>
  </tr>  
  <cfoutput query="structClasses.classes">
  <tr>
    <td align="center"><input type="radio" name="fitnessClassID" value="#fitnessClassID#"></td>
	<td>
      <a href="javascript:editClass('#fitnessClassID#')" class="accent01">#classTitle#</a>
    </td> 
    <td align="center">#classType#</td>
    <td align="center">#instructorName#&nbsp;</td>
    <td align="center">#location#&nbsp;</td>
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif structClasses.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structClasses.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadClassList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>