<cfparam name="URL.classID">
<cfparam name="URL.courseID">
<cfparam name="URL.catID" default="0">

<script type="text/javascript" src="modules/class/class_attendance.js"></script>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset courseInfo=CL.getCourse(URL.courseID)>
<cfset allClassInstances=CL.getClassInstancesByClassID(URL.classID)>

<div class="header">CLASS MANAGER &gt; Class Attendance</div>

<span id="classID" style="display:none"><cfoutput>#URL.classID#</cfoutput></span>
<span id="courseID" style="display:none"><cfoutput>#URL.courseID#</cfoutput></span>

<div>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="updateAttendance();">Update Attendance</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="printAttendanceSheet();">Print Sign-Up Sheet</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="printAddressSheet();">Print Address Sheet</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="sendFollowUpFeedbackRequest();">Send Follow-Up Feedback Request</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="sendMissedClassFollowUp();">Send Missed Class Follow-Up Email</a>
</div>
<br /><br />

<cfoutput>
<p><a href="index.cfm?md=class&tmp=classes&wrap=1&catID=#URL.catID#&courseID=#URL.courseID#&classID=#URL.classID#">&laquo; Back</a></p>

<p class="subHeader">Course Title: <b><i>#courseInfo.courseTitle#</i></b></p>
</cfoutput>

<cfoutput>
<form id="frmNavigator">
  Class Instance
  <select name="classInstanceID" style="min-width:250px;">
    <cfloop query="allClassInstances">
    <option value="#classInstanceID#">#DateFormat(startDateTime,"mm/dd/yyyy")# #LCase(TimeFormat(startDateTime,"hh:mm tt"))# - #LCase(TimeFormat(endDateTime,"hh:mm tt"))#</option>
    </cfloop>
  </select>
</form>
</cfoutput>

<br style="clear:both;" />

<div id="message" class="ui-widget" style="display:none; margin:10px 0px; width:450px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgAttendanceUpdate"></span></p>
  </div>
</div>

<div id="mainDiv"></div>

<br style="clear:both;" />

<div>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="updateAttendance();">Update Attendance</a>
</div>
