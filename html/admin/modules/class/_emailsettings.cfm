<script type="text/javascript" src="modules/class/emailsettings.js"></script>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset allEmailSettings=CL.getAllEmailSettings()>

<div class="header">E-MAIL SETTINGS</div>

<cfoutput query="allEmailSettings">
<form id="frm#emailCode#" onSubmit="return false;">
<input type="hidden" name="emailCode" value="#emailCode#">
<div class="subHeader">#description#</div>  


<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msg#emailCode#"></span></p>
  </div>
</div>

<strong>Subject Line:</strong><br />
<input type="text" name="emailSubject" style="width:350px;" value="#HTMLEditFormat(emailSubject)#"><br /><br />

<strong>Reply-To Address:</strong><br />
<input type="text" name="replyAddress" style="width:350px;" value="#HTMLEditFormat(replyAddress)#"><br /><br />

Reply-To Name:<br />
<input type="text" name="replyName" style="width:350px;" value="#HTMLEditFormat(replyName)#"><br /><br />

<cfif Not sendToUser>
<strong>Recipient Addresses: </strong><br />
<input type="text" name="recipientAddresses" style="width:350px;" value="#HTMLEditFormat(recipientAddresses)#"><br />
<small>(Separate multiple addresses by comma.)</small><br /><br />
</cfif>

<strong>E-Mail Body:</strong><br />
<textarea name="emailBody" style="width:350px;height:100px;">#HTMLEditFormat(emailBody)#</textarea><br />


<input type="button" value=" Update " onclick="updateEmailSetting('#emailCode#');">
</form>

<!--- Instructions --->
<cfswitch expression="#emailCode#">
  <cfcase value="register">
  <p>* Use the variables $username and $password in the email body and they will be converted to the actual values in the emails that are sent out.</p>
  </cfcase>
  <cfcase value="forgot_password">
  <p>* Use the variable $password in the email body and it will be converted to the actual value in the emails that are sent out.</p>
  </cfcase>
  <cfcase value="feedback_submission">
  <p>* Use the variable $feedback in the email body and it will be converted to the user's submission in the emails that are sent out.</p>
  </cfcase>
  <cfcase value="order_notification">
  <p>* Use the variable $classes in the email body and it will be converted to the detail information of the registered classes in the emails that are sent out.</p>
  <p>** Use the variable $orderid and it will be converted to the actual 'order ID' in the emails that are sent out.</p>
  </cfcase>
  <cfcase value="order_confirmation">
  <p>* Use the variable $classes in the email body and it will be converted to the detail information of the registered classes in the emails that are sent out.</p>
  <p>** Use the variables $name and $orderid and they will be converted to the actual 'contact name' and 'order ID' in the emails that are sent out.</p>
  <p>*** Use the variable $signup in the email body and it will be converted to a link to enewsletters sign-up form with user's email address pre-filled.</p>
  </cfcase>
  <cfcase value="class_reminder">
  <p>* Use the variable $class in the email body and it will be converted to the actual class info in the emails that are sent out.</p>
  </cfcase>
  <cfcase value="feedback_request">
  <p>* Use the variable $class in the email body and it will be converted to the actual class info in the emails that are sent out.</p>
  <p>* Use the variable $link in the email body and it will be converted to the actual feedback form URL in the emails that are sent out.</p>
  </cfcase>
  <cfcase value="missed_class">
  <p>* Use the variable $class in the email body and it will be converted to the actual class info in the emails that are sent out.</p>
  </cfcase>
</cfswitch>
<br />
</cfoutput>