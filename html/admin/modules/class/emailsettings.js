function updateEmailSetting(emailCode) {
	$("#frm" + emailCode).ajaxSubmit({ 
		url: 'action.cfm?md=class&task=updateEmailSettings',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msg" + emailCode).html('The e-mail setting has been updated.');	
		  $("#frm" + emailCode + " div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}
}
