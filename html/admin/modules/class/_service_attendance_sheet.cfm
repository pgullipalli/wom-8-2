<cfparam name="URL.serviceID">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset serviceInfo=CL.getService(URL.serviceID)>
<cfset serviceAttendees=CL.getServiceAttendeesByServiceID(URL.serviceID)>
<cfset numAttendees=serviceAttendees.recordcount>

<script language="javascript">
$(function () {openprintdialog();});

function openprintdialog () {
  var onWindows = navigator.platform ? navigator.platform == "Win32" : false;
  var macIE = document.all && !onWindows;
  if (macIE) {
	alert ("Press \"Cmd+p\" on your keyboard to print");
  } else {
	window.print();
  }  
}			
</script>

<cfoutput>
<div style="text-align:right;"><a href="javascript:openprintdialog();">PRINT</a> | <a href="javascript:window.close();">CLOSE</a></div>

<div style="text-align:center; width:100%">
<img src="/images/imgLogo.gif" style="padding:0px 0px 10px 0px; border-width:0px;">
</div>

<p class="subHeader">Service Title: <b><i>#serviceInfo.serviceTitle#</i></b></p>

<table border="0" cellpadding="3" cellspacing="0" width="600">    
    <tr valign="top">
      <td width="300">
      <cfloop index="idx" from="1" to="#Ceiling(numAttendees/2)#">
        <input type="checkbox" name="attendedStudentServiceIDList" value="#serviceAttendees.studentServiceID[idx]#">
        #idx#. #serviceAttendees.attendeeName[idx]# (#DateFormat(serviceAttendees.dateCreated[idx],"mm/dd/yyyy")#)<br />
      </cfloop>
      &nbsp;
      </td>
      <td width="300">
      <cfloop index="idx" from="#Ceiling(numAttendees/2) + 1#" to="#numAttendees#">
        <input type="checkbox" name="attendedStudentServiceIDList" value="#serviceAttendees.studentServiceID[idx]#">
        #idx#. #serviceAttendees.attendeeName[idx]# (#DateFormat(serviceAttendees.dateCreated[idx],"mm/dd/yyyy")#)<br />
      </cfloop>
      &nbsp;
      </td>
    </tr>
</table>
</cfoutput>



