<cfparam name="URL.studentType" default="R">
<cfparam name="URL.keyword" default="">
<cfparam name="URL.browseBy" default="type">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>

<cfif URL.browseBy IS "type"><!--- browse by student type --->

  <cfset structStudents=CL.getStudentsByType(studentType=URL.studentType, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>  
<cfelse><!--- browse by email keyword --->
  <cfset structStudents=CL.getStudentsByKeyword(keyword=URL.keyword, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>

 <cfoutput>
  <p>
  Your search for '<i>#URL.keyword#</i>' returns <cfif structStudents.numAllItems GT 0>#structStudents.numAllItems# items.<cfelse>no results.</cfif>
  </p>	
  </cfoutput>
</cfif>

<cfif structStudents.numDisplayedItems GT 0>
<form id="frmStudentList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th width="200">E-Mail Address</th>
    <th width="150">Password</th>
    <th width="150">Type</th>    
    <cfif Compare(URL.browseBy,"type")>
    <th width="150">Contact Name</th>   
    </cfif>
    <th>&nbsp;</th>
  </tr>  
  <cfoutput query="structStudents.students">
  <tr>
    <td align="center"><input type="radio" name="studentID" value="#studentID#"></td>
	<td><a href="##" onclick="editStudent('#studentID#')">#email#</a></td> 
	<td align="center"><span class="password">#password#</span>&nbsp;</td>
    <td align="center">
	  <cfswitch expression="#studentType#">
        <cfcase value="R">Regular</cfcase>
        <cfcase value="F">Fitness Club Member</cfcase>
        <cfcase value="E">Employee</cfcase>
        <cfcase value="D">Doctor</cfcase>
        <cfdefaultcase>Regular</cfdefaultcase>
      </cfswitch>
    </td>
	<cfif Compare(URL.browseBy,"type")>
    <td>#contactFirstName# #contactLastName#&nbsp;</td>    
    </cfif>    
    <td>&nbsp;</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif structStudents.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structStudents.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadStudentList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>