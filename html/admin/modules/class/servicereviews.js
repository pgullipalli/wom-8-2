var serviceID;

$(function () {	
	serviceID=$("#serviceID").html();
	
	loadReviewList();	
});

function loadReviewList() {  
  var url="index.cfm?md=class&tmp=snip_servicereview_list&serviceID=" + serviceID + "&uuid=" + xsite.getUUID();
  xsite.load("#mainDiv", url);
}

function addServiceReview() {
  xsite.createModalDialog({
	windowName: 'winAddServiceReview',
	title: 'Add Service Review',
	width: 600,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_add_servicereview&serviceID=' + serviceID + '&uuid=' + xsite.getUUID(),	
	buttons: {
	  'Cancel': function() { $("#winAddServiceReview").dialog('close'); },
	  ' Add ': submitForm_AddServiceReview
	}
  }).dialog('open');

}

function submitForm_AddServiceReview() {
  var form = $("#frmAddServiceReview");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=class&task=addServiceReview',
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winAddServiceReview").dialog('close');
	  loadReviewList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }
}

function editServiceReview(svID) {
  var form = $("#frmServiceReviewList")[0];
  var serviceReviewID = null;
  if (svID) serviceReviewID = svID;
  else serviceReviewID = xsite.getCheckedValue(form.serviceReviewIDList);

  if (!serviceReviewID) {
	alert("Please select a review to edit.");
	return;	  
  }	
	
  xsite.createModalDialog({
	windowName: 'winEditServiceReview',
	title: 'Edit Service Review',
	width: 600,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_edit_servicereview&serviceReviewID=' + serviceReviewID + '&uuid=' + xsite.getUUID(),	
	buttons: {
	  'Cancel': function() { $("#winEditServiceReview").dialog('close'); },
	  ' Save ': submitForm_EditServiceReview
	}
  }).dialog('open');

}

function submitForm_EditServiceReview() {
  var form = $("#frmEditServiceReview");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=class&task=editServiceReview&serviceID=' + serviceID,
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winEditServiceReview").dialog('close');
	  loadReviewList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }
}


function deleteServiceReview() {
  if(!document.getElementById('frmServiceReviewList')) return;
  var form = $("#frmServiceReviewList")[0];
  var serviceReviewIDList = xsite.getCheckedValue(form.serviceReviewIDList);
  if (!serviceReviewIDList) {
	alert("Please select at least an item to remove.");
	return;	  
  }
  if (!confirm('Are you sure you want to delete the selected items?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deleteServiceReviews&&serviceReviewIDList=' + escape(serviceReviewIDList) + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  loadReviewList();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

