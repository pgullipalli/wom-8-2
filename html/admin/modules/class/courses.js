var pageNum=1;//initial page number

$(function () {	
	loadCourseList();
	
	$("#frmNavigator select[name='categoryID']").change(function() {
	  pageNum=1;
	  loadCourseList();	  
	});
});

function loadCourseList(pNum) {
  if (pNum) pageNum = pNum;
  var catID=$("#frmNavigator select[name='categoryID']").val();	
  
  var url="index.cfm?md=class&tmp=snip_course_list&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url, function() {	
	$("a.tooltip").tooltip({
		bodyHandler: function() {
		  return $($(this).attr("href")).html();
		},
		showURL: false
	});
  });
  
}

function addCourse() {
  var catID=$("#frmNavigator select[name='categoryID']").val();	
  xsite.createModalDialog({
	windowName: 'winAddCourse',
	title: 'Add Course',
	width: 750,
	height:500,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_add_course&catID=' + catID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddCourse").dialog('close'); },
	  ' Add ': submitForm_AddCourse
	}
  }).dialog('open');
  
  function submitForm_AddCourse() {
	var form = $("#frmAddCourse");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=class&task=addCourse',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddCourse").dialog('close');
		loadCourseList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editCourse(itID) {
  var form = $("#frmCourseList")[0];
  var courseID = null;
  if (itID) courseID = itID;
  else courseID = xsite.getCheckedValue(form.courseID);

  if (!courseID) {
	alert("Please select a course to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditCourse',
	title: 'Edit Course',
	width: 750,
	height: 500,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_edit_course&courseID=' + courseID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditCourse").dialog('close'); },
	  ' Save ': submitForm_EditCourse
	}
  }).dialog('open');
	
  function submitForm_EditCourse() {
	var form = $("#frmEditCourse");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=class&task=editCourse',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
		
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditCourse").dialog('close');
		loadCourseList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteCourse() {
  var form = $("#frmCourseList")[0];
  var courseID = xsite.getCheckedValue(form.courseID);
  if (!courseID) {
	alert("Please select a course to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected course?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deleteCourse&courseID=' + courseID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadCourseList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function manageClasses() {
  var form = $("#frmCourseList")[0];
  var courseID = xsite.getCheckedValue(form.courseID);
  if (!courseID) {
	alert("Please select a course to manage its classes.");
	return;
  }
  var catID=$("#frmNavigator select[name='categoryID']").val();	
  
  window.location="index.cfm?md=class&tmp=classes&wrap=1&courseID=" + courseID + "&catID=" + catID;  
}

function previewCourse() {
  var form = $("#frmCourseList")[0];
  var courseID = xsite.getCheckedValue(form.courseID);
  if (!courseID) {
	alert("Please select a course to preview.");
	return;
  }
  window.open('/index.cfm?md=class&tmp=detail&cid=' + courseID + '&uuid=' + xsite.getUUID(), '_blank');
}

function profitabilityReport() {
  var form = $("#frmCourseList")[0];
  var courseID = xsite.getCheckedValue(form.courseID);
  if (!courseID) {
	alert("Please select a course to create its profitability report.");
	return;
  }
  var catID=$("#frmNavigator select[name='categoryID']").val();	
  
  window.location="index.cfm?md=class&tmp=report_courseprofit&wrap=1&courseID=" + courseID + "&catID=" + catID;  
}

function couponCodeReport() {
  var catID=$("#frmNavigator select[name='categoryID']").val();
  window.location="index.cfm?md=class&tmp=report_couponcode&wrap=1&catID=" + catID;
}