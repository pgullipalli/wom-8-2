<cfparam name="URL.courseID">
<cfparam name="URL.catID" default="0">

<script type="text/javascript" src="modules/class/classes.js"></script>
<script language="javascript">
function openprintdialog () {
  var onWindows = navigator.platform ? navigator.platform == "Win32" : false;
  var macIE = document.all && !onWindows;
  if (macIE) {
	alert ("Press \"Cmd+p\" on your keyboard to print");
  } else {
	window.print();
  }  
}			
</script>
<style type="text/css" media="screen">
#printerControl {display:block;}
</style>

<style type="text/css" media="print">
#printerControl {display:none;}
</style>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset courseInfo=CL.getCourse(URL.courseID)>

<div id="printerControl">

<div class="header">CLASS MANAGER &gt; Classes &amp; Class Instances</div>

<span id="courseID" style="display:none"><cfoutput>#URL.courseID#</cfoutput></span>
<span id="catID" style="display:none"><cfoutput>#URL.catID#</cfoutput></span>
<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addClass();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editClass(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteClass();">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button130" onclick="manageAttendance();">Manage Attendance</a>
</div>

<br style="clear:both;" /><br />

<p align="right"><a href="javascript:openprintdialog();">PRINT</a></p>

<cfoutput>
<p><a href="index.cfm?md=class&tmp=courses&wrap=1&catID=#URL.catID#">&laquo; Back</a></p>
</cfoutput>

</div>

<cfoutput>
<p class="subHeader">Course Title: <b><i>#courseInfo.courseTitle#</i></b></p>
</cfoutput>


<div id="mainDiv"></div>

<br style="clear:both;" /><br />
<!--- <p>
* A date/time string '10/01/2009 6:00 PM - 6:55 PM (Thu, 25)' represents a class instance on Thursday with 25 participants attended the class.
</p> --->
