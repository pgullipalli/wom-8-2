$(function () {	
	loadCurriculaList();
});

function loadCurriculaList() {
  var url="index.cfm?md=class&tmp=snip_curricula_list&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function addCurricula() {
  xsite.createModalDialog({
	windowName: 'winAddCurricula',
	title: 'Add Curricula',
	width: 700,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_add_curricula&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddCurricula").dialog('close'); },
	  ' Add ': submitForm_AddCurricula
	}
  }).dialog('open');
  
  function submitForm_AddCurricula() {
	var form = $("#frmAddCurricula");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=class&task=addCurricula',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddCurricula").dialog('close');
		loadCurriculaList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editCurricula(cID) {
  var form = $("#frmCurriculaList")[0];
  var curriculaID = null;
  if (cID) curriculaID = cID;
  else curriculaID = xsite.getCheckedValue(form.curriculaID);

  if (!curriculaID) {
	alert("Please select a curricula to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditCurricula',
	title: 'Edit Curricula',
	width: 700,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_edit_curricula&curriculaID=' + curriculaID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditCurricula").dialog('close'); },
	  ' Save ': submitForm_EditCurricula
	}
  }).dialog('open');
	
  function submitForm_EditCurricula() {
	var form = $("#frmEditCurricula");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=class&task=editCurricula',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
		
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditCurricula").dialog('close');
		loadCurriculaList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteCurricula() {
  var form = $("#frmCurriculaList")[0];
  var curriculaID = xsite.getCheckedValue(form.curriculaID);
  if (!curriculaID) {
	alert("Please select a curricula to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected curricula?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deleteCurricula&curriculaID=' + curriculaID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadCurriculaList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function updateIntroductionText() {
	$("#frmIntroductoryCopy").ajaxSubmit({ 
		url: 'action.cfm?md=class&task=updateIntroductionText',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgIntroductoryCopy").html('The introductory copy has been updated.');	
		  $("#frmIntroductoryCopy div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}
}
