<script type="text/javascript" src="modules/class/categories.js"></script>

<div class="header">CLASS MANAGER &gt; Categories</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addCategory();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editCategory();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteCategory();">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="reachFrequencyReport();">Reach &amp; Frequency Report</a>
</div>

<br style="clear:both;" /><br />

<div id="mainDiv"></div>