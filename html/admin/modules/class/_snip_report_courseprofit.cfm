<cfparam name="URL.courseID">
<cfparam name="URL.startDate">
<cfparam name="URL.endDate">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset profitReport=CL.getCourseProfitabilityReport(courseID=URL.courseID, startDate=URL.startDate, endDate=URL.endDate)>

<div class="itemList">
<table>
  <tr>
	<th width="250">Class Date</th>
    <th># Participants</th>
    <th># Attended</th>
    <th># Coupon Used</th>
    <th>$ Collected</th>
  </tr>
  <cfset numTotalParticipants=0>
  <cfset numTotalAttends=0>
  <cfset numTotalCouponUsed=0>
  <cfset totalMoneyCollected=0>
  <cfoutput>
  <cfloop query="profitReport">
  <tr align="center">
    <td>#DateFormat(firstStartDateTime,"mm/dd/yyyy")# #LCase(TimeFormat(firstStartDateTime,"h:mm tt"))#</td>
    <td>#numParticipants#<cfset numTotalParticipants=numTotalParticipants + numParticipants></td>
    <td>#numAttends#<cfset numTotalAttends=numTotalAttends + numAttends></td>
    <td>#numCouponUsed#<cfset numTotalCouponUsed=numTotalCouponUsed + numCouponUsed></td>
    <td><cfif IsNumeric(feeCollected)>#DollarFormat(feeCollected)#<cfset totalMoneyCollected=totalMoneyCollected + feeCollected><cfelse>$0.00</cfif></td>
  </tr>
  </cfloop> 
  <tr align="center">
    <td align="right"><b>Total</b></td>
    <td><b>#numTotalParticipants#</b></td>
    <td><b>#numTotalAttends#</b></td>
    <td><b>#numTotalCouponUsed#</b></td>
    <td><b>#DollarFormat(totalMoneyCollected)#</b></td>
  </tr>
  </cfoutput>
</table>
</div>

