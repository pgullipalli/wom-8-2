var classID;
var courseID;

$(function () {	
	classID=$("#classID").html();
	courseID=$("#courseID").html();
	loadParticipantList();
});

function loadParticipantList() {  
  var url="index.cfm?md=class&tmp=snip_class_participant_list&classID=" + classID + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function deleteParticipants() {
  var form=$("#frmAttendeeList")[0];
  
  if (!(form.studentClassIDList && xsite.getCheckedValue(form.studentClassIDList))) {
	alert ("Please check off the participants you'd like to delete.");
	return;
  }
	
  if (!confirm('Are you sure you want to permantly delete the selected participants that have registered with this class?')) {
	return;
  } else {		
	//show waiting dialog
	var studentClassIDList = xsite.getCheckedValue(form.studentClassIDList);	
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deleteClassParticipants&classID=' + classID + '&studentClassIDList=' + escape(studentClassIDList) + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadParticipantList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }		
}

function changeClassPayWithCashStatus(studentClassID, payWithCash) {
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=changeClassPayWithCashStatus&studentClassID=' + studentClassID + '&payWithCash=' + payWithCash + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadParticipantList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }		
}