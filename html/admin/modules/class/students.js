var pageNum=1;//initial page number

$(function () {	
	pageNum=$("#pageNum").text();		
			
	loadStudentList();
	
	$("#frmNavigator select[name='studentType']").change(function() {
	  pageNum=1;
	  loadStudentList();	  
	});
});

function loadStudentList(pNum) {
  if (pNum) pageNum = pNum;
  var studentType=$("#frmNavigator select[name='studentType']").val();	
  
  var url="index.cfm?md=class&tmp=snip_student_list&studentType=" + studentType + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function addStudent() {
  var studentType=$("#frmNavigator select[name='studentType']").val();	
  xsite.createModalDialog({
	windowName: 'winAddStudent',
	title: 'Add User',
	width: 600,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_add_student&studentType=' + studentType + '&uuid=' + xsite.getUUID(),	
	buttons: {
	  'Cancel': function() { $("#winAddStudent").dialog('close'); },
	  ' Add ': submitForm_AddStudent
	}
  }).dialog('open');
  
  function submitForm_AddStudent() {
	var form = $("#frmAddStudent");

	form.ajaxSubmit({ 
	  url: 'action.cfm?md=class&task=addStudent',
	  type: 'post',
	  cache: false,
	  dataType: 'json', 
	  beforeSubmit: validate,
	  success: processJson,
	  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	
	function validate() {
	  var form=$("#frmAddStudent")[0];
	  /*if (!xsite.checkEmail(form.email.value)) {
	    alert('Please enter a vliad email address.');
		form.email.focus();
	    return false;
	  }*/
	  if (form.email.value=="") {
	    alert('Please enter a valid email address.');
		form.email.focus();
	    return false;
	  }	  
	  
	  if (form.password.value=="" || form.password.value.length < 6) {
	    alert('Please enter a password with at least 6 characters.');
		form.password.focus();
	    return false;
	  }
	  return true;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddStudent").dialog('close');
		loadStudentList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editStudent(sID) {
  var form = $("#frmStudentList")[0];
  var studentID = null;
  if (sID) studentID = sID;
  else studentID = xsite.getCheckedValue(form.studentID);

  if (!studentID) {
	alert("Please select an item to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditStudent',
	title: 'Edit User',
	width: 600,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_edit_student&studentID=' + studentID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditStudent").dialog('close'); },
	  ' Save ': submitForm_EditStudent
	}
  }).dialog('open');
	
  function submitForm_EditStudent() {
	var form = $("#frmEditStudent");
	form.ajaxSubmit({ 
	  url: 'action.cfm?md=class&task=editStudent',
	  type: 'post',
	  cache: false,
	  dataType: 'json', 
	  beforeSubmit: validate,
	  success: processJson,
	  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function validate() {
	  var form=$("#frmEditStudent")[0];
	  /*if (!xsite.checkEmail(form.email.value)) {
	    alert('Please enter a vliad email address.');
		form.email.focus();
	    return false;
	  }*/
	  
	  if (form.email.value=="") {
	    alert('Please enter a valid email address.');
		form.email.focus();
	    return false;
	  }
	  
	  if (form.password.value=="") {
	    alert('Please enter a password with at least 6 characters.');
		form.password.focus();
	    return false;
	  }
	  return true;
	}
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditStudent").dialog('close');
		loadStudentList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteStudent() {
  var form = $("#frmStudentList")[0];
  var studentID = xsite.getCheckedValue(form.studentID);
  if (!studentID) {
	alert("Please select an item to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected item?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deleteStudent&studentID=' + studentID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadStudentList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function togglePassword() {
  if ($(".password").css('display') == "none") $(".password").css('display','inline');
  else $(".password").css('display','none');
}

function manageSchedule() {
  var form = $("#frmStudentList")[0];
  var studentID = xsite.getCheckedValue(form.studentID);
  if (!studentID) {
	alert("Please select a registrant to edit.");
	return;
  }
  var studentType=$("#frmNavigator select[name='studentType']").val();
  
  window.location="index.cfm?md=class&tmp=student_class_schedule&wrap=1&studentID=" + studentID + '&studentType=' + studentType + '&pageNum=' + pageNum + '&browseBy=type&uuid=' + xsite.getUUID();  
}

function loginAsUser() {
  var form = $("#frmStudentList")[0];
  var studentID = xsite.getCheckedValue(form.studentID);
  if (!studentID) {
	alert("Please select a registrant to login as user.");
	return;
  }
  
  window.open('index.cfm?md=class&tmp=login_as_user&wrap=1&studentID=' + studentID + '&uuid=' + xsite.getUUID(),'_blank');	
}