<cfparam name="URL.startDate">
<cfparam name="URL.endDate">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset allCats=CL.getAllCategories()>
<cfset report=CL.getReachFrequencyReport(startDate=URL.startDate, endDate=URL.endDate)>

<div class="itemList">
<table>
  <tr>
	<th width="200">Category</th>
    <th># Individuals</th>
    <th># Registrants</th>
    <th># Classes/Individual</th>
  </tr>
  <cfoutput>
  <cfloop query="allCats">
    <cfset found=false>
    <cfset catID=allCats.categoryID>
    <cfset catName=allCats.categoryName>
    <cfloop query="report">    
      <cfif Not Compare(catID, report.categoryID)>
        <cfset found=true>
        <tr>
          <td>#catName#</td>
          <td align="center">#numIndividuals#</td>
          <td align="center">#numRegistrants#</td>
          <td align="center">#NumberFormat(numRegistrants/numIndividuals, "9.99")#</td>
        </tr>
        <cfbreak>
      </cfif>
    </cfloop>
    <cfif Not found>
    <tr>
      <td>#catName#</td>
      <td align="center">0</td>
      <td align="center">0</td>
      <td align="center">0.00</td>
    </tr>
    </cfif> 
  </cfloop> 
  </cfoutput>
</table>
</div>

