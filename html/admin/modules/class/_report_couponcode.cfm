﻿<cfparam name="URL.catID">
<cfparam name="URL.startDate" default="#DateFormat(DateAdd("m",-1,now()),"mm/dd/yyyy")#">
<cfparam name="URL.endDate" default="#DateFormat(now(),"mm/dd/yyyy")#">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset couponInfo=CL.getAllCoupons()>

<script type="text/javascript" src="modules/class/report_couponcode.js"></script>
<script language="javascript">
function openprintdialog () {
  var onWindows = navigator.platform ? navigator.platform == "Win32" : false;
  var macIE = document.all && !onWindows;
  if (macIE) {
	alert ("Press \"Cmd+p\" on your keyboard to print");
  } else {
	window.print();
  }  
}			
</script>
<style type="text/css" media="screen">
.printerControl {display:inline;}
</style>

<style type="text/css" media="print">
.printerControl {display:none;}
</style>

<div class="header"><span class="printerControl">CLASS MANAGER &gt; </span>Coupon Code Report</div>

<div class="printerControl">
  <cfoutput>
  <p><a href="index.cfm?md=class&tmp=courses&wrap=1&catID=#URL.catID#">&laquo; Back</a> | <a href="javascript:openprintdialog();">Print</a></p>
  </cfoutput>
</div>

<cfoutput>
<form id="frmDateRange" name="frmDateRange" onSubmit="return false;">

Coupon Code:
<select name="discountCode" style="min-width:150px;">
  <cfset temp="">
  <cfloop query="couponInfo">
  <cfif Compare(temp,discountCode)>
    <option value="#discountCode#">#discountCode#</option>
    <cfset temp=discountCode>
  </cfif>
  </cfloop>
</select><br /><br />

From:
<input type="text" name="startDate" size="14" maxlength="12" value="#URL.startDate#" class="dateField">
&nbsp;&nbsp;&nbsp;

To:
<input type="text" name="endDate" size="14" maxlength="12" value="#URL.endDate#" class="dateField">


<!--- <input type="checkbox" name="allDateRange" value="1" onClick="resetAllDateRange()" /> All Date Range --->
<br /><br />

<input type="button" value=" Submit " onclick="getReport();" class="printerControl">
</form>
</cfoutput>

<div id="mainDiv"></div>

<br style="clear:both;" /><br />


