<cfparam name="URL.catID">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>

<cfset structServices=CL.getServicesByCategoryID(categoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>

<cfif structServices.numAllItems IS 0>
  <p>No services created in this category.</p>
<cfelse>
  <cfif structServices.numDisplayedItems GT 0>  
    <form name="frmServiceList" id="frmServiceList">
    <div class="itemList">
    <table>
      <tr>
        <th width="40">&nbsp;</th>
        <th>Course Title</th>
      </tr>  
      <cfoutput query="structServices.services">
      <tr>
        <td align="center"><input type="checkbox" name="serviceIDList" value="#serviceID#"></td>
        <td>#serviceTitle#</td>
      </tr>
      </cfoutput>
    </table>
    </div>
    </form>

    
    <cfif structServices.numAllItems GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(structServices.numAllItems/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadServiceListForSelection(#idx#)">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
    </cfif>
  </cfif>
</cfif>

