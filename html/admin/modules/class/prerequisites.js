var courseID;

$(function () {	
	courseID=$("#courseID").html();
	
	loadCourseList();	
});

function loadCourseList() {  
  var url="index.cfm?md=class&tmp=snip_prerequisites_list&courseID=" + courseID + "&uuid=" + xsite.getUUID();
  xsite.load("#mainDiv", url);
}

function addPrerequisites() {
  xsite.createModalDialog({
	windowName: 'winAddPrerequisites',
	title: 'Add Recommended Prerequisites',
	width: 600,
	height:500,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_add_relatedcourses&uuid=' + xsite.getUUID(),
	urlCallback: function () {
	  $("#frmNavigator").unbind();
	  $("#frmNavigator select[name='categoryID']").change(function() {
		loadCourseListForSelection(1);	  
	  });
	  loadCourseListForSelection(1);
	},
	buttons: {
	  'Cancel': function() { $("#winAddPrerequisites").dialog('close'); },
	  ' Add ': submitForm_AddPrerequisites
	}
  }).dialog('open');

}

function loadCourseListForSelection(pageNum) {
  var catID=$("#frmNavigator select[name='categoryID']").val();
  var url="index.cfm?md=class&tmp=snip_course_list_for_selection&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();
  xsite.load("#subDiv1", url);
}

function submitForm_AddPrerequisites() {
  var form = $("#frmCourseList");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=class&task=addPrerequisites&courseID=' + courseID,
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winAddPrerequisites").dialog('close');
	  loadCourseList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }
}

function deletePrerequisites() {
  if(!document.getElementById('frmPrerequisitesList')) return;
  var form = $("#frmPrerequisitesList")[0];
  var courseIDList = xsite.getCheckedValue(form.courseIDList);
  if (!courseIDList) {
	alert("Please select at least an item to remove.");
	return;	  
  }
  if (!confirm('Are you sure you want to remove the selected items?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deletePrerequisites&courseID=' + courseID + '&courseIDList=' + escape(courseIDList) + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  loadCourseList();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

