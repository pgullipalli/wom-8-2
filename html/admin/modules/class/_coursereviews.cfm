<cfparam name="URL.courseID">
<cfparam name="URL.catID" default="0">

<script type="text/javascript" src="modules/class/coursereviews.js"></script>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset courseInfo=CL.getCourse(URL.courseID)>

<div class="header">CLASS MANAGER &gt; Course Reviews</div>

<span id="courseID" style="display:none"><cfoutput>#URL.courseID#</cfoutput></span>
<div id="actionBar">
<a href="#" id="btnNew" class="fg-button ui-state-default ui-corner-all button65" onclick="addCourseReview()">New</a>
<a href="#" id="btnNew" class="fg-button ui-state-default ui-corner-all button65" onclick="editCourseReview()">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteCourseReview()">Delete</a>
</div>

<br style="clear:both;" /><br />

<cfoutput>
<p><a href="index.cfm?md=class&tmp=courses&wrap=1&catID=#URL.catID#">&laquo; Back</a></p>

<p class="subHeader">Course Title: <b><i>#courseInfo.courseTitle#</i></b></p>
</cfoutput>


<div id="mainDiv"></div>
