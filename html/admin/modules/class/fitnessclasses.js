var pageNum=1;//initial page number

$(function () {	
	loadClassList();
	
	$("#frmNavigator select[name='classType']").change(function() {
	  pageNum=1;
	  loadClassList();	  
	});
});

function loadClassList(pNum) {
  if (pNum) pageNum = pNum;
  var classType=$("#frmNavigator select[name='classType']").val();	
  
  var url="index.cfm?md=class&tmp=snip_fitnessclass_list&classType=" + escape(classType) + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();
  xsite.load("#mainDiv", url);
}

function addClass() {
  var classType=$("#frmNavigator select[name='classType']").val();
  xsite.createModalDialog({
	windowName: 'winAddClass',
	title: 'Add Class',
	width: 650,
	height: 500,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_add_fitnessclass&classType=' + escape(classType) + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddClass").dialog('close'); },
	  ' Add ': submitForm_AddFitnessClass
	}
  }).dialog('open');
  
  function submitForm_AddFitnessClass() {
	var form = $("#frmAddClass");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=class&task=addFitnessClass',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddClass").dialog('close');
		loadClassList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editClass(cID) {
  var form = $("#frmClassList")[0];
  var fitnessClassID = null;
  if (cID) fitnessClassID = cID;
  else fitnessClassID = xsite.getCheckedValue(form.fitnessClassID);

  if (!fitnessClassID) {
	alert("Please select a class to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditClass',
	title: 'Edit Class',
	width: 650,
	height: 500,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_edit_fitnessclass&fitnessClassID=' + fitnessClassID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditClass").dialog('close'); },
	  ' Save ': submitForm_EditClass
	}
  }).dialog('open');
	
  function submitForm_EditClass() {
	var form = $("#frmEditClass");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=class&task=editFitnessClass',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
		
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditClass").dialog('close');
		loadClassList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteClass() {
  var form = $("#frmClassList")[0];
  var fitnessClassID = xsite.getCheckedValue(form.fitnessClassID);
  if (!fitnessClassID) {
	alert("Please select a class to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected class?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deleteFitnessClass&fitnessClassID=' + fitnessClassID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadClassList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

