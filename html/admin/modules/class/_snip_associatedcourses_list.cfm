<cfparam name="URL.courseID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset associatedCourses=CL.getAssociatedCourses(URL.courseID)>

<cfif associatedCourses.recordcount Is 0>
<p>No associated courses selected for this course. Click "New" button in the action bar to select associated courses.</p>
<cfelse>
<form id="frmAssociatedCoursesList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th>Course Title</th>
  </tr>  
  <cfoutput query="associatedCourses">
  <tr>
    <td align="center"><input type="checkbox" name="courseIDList" value="#courseID#"></td>
    <td>#courseTitle#</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>