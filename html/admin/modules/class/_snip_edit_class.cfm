<cfparam name="URL.courseID">
<cfparam name="URL.classID">
<cfset UT=CreateObject("component","com.Utility").init()>
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset allLocations=CL.getLocations()>
<cfset classInfo=CL.getClassByClassID(URL.classID)>
<cfset classInstanceInfo=CL.getClassInstancesByClassID(URL.classID)>

<cfset publishDateStruct=UT.convertDateTimeObjectToString(DateAdd("d",0,classInfo.publishDate))>
<cfset unpublishDateStruct=UT.convertDateTimeObjectToString(DateAdd("d",0,classInfo.unpublishDate))>

<cfoutput>
<form id="frmEditClass" name="frmEditClass" onsubmit="return false">
<input type="hidden" name="courseID" value="#URL.courseID#" />
<input type="hidden" name="classID" value="#URL.classID#" />
<div class="row">
	<div class="col-30-right"><b>Publish?</b></div>
    <div class="col-68-left">
      <div class="row">
        <input type="radio" name="published" value="1" <cfif classInfo.published>checked</cfif> /> Yes
      </div>
      <div class="row">
        <div class="col-24-right">Publish Date:</div>
        <div class="col-74-left">
          <cfif classInfo.published>
          <input type="text" name="publishDate" class="dateField" style="width:70px;" validate="required:false, date:true" value="#publishDateStruct.date#" />
          <select name="publishDate_hh">
            <option value="12">12</option>
            <cfloop index="hh" from="1" to="12"><option value="#hh#"<cfif publishDateStruct.hh Is hh> selected</cfif>><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
          </select>
          <select name="publishDate_mm">
            <cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"<cfif publishDateStruct.mm Is mm> selected</cfif>><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
          </select>
          <select name="publishDate_ampm">
            <option value="am"<cfif publishDateStruct.ampm Is "AM"> selected</cfif>>AM</option><option value="pm"<cfif publishDateStruct.ampm Is "PM"> selected</cfif>>PM</option>
          </select>
          <cfelse>
          <input type="text" name="publishDate" class="dateField" style="width:70px;" validate="required:false, date:true" />
          <select name="publishDate_hh">
            <option value="12">12</option>
            <cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
          </select>
          <select name="publishDate_mm">
            <cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
          </select>
          <select name="publishDate_ampm">
            <option value="am">AM</option><option value="pm">PM</option>
          </select>
          </cfif>
          <input type="button" value="Now" onclick="getNow(document.frmEditClass, 'publishDate')" />
          <input type="button" value="Clear" onclick="clearDate(document.frmEditClass, 'publishDate')" />
        </div>
      </div>
      <div class="row">
        <div class="col-24-right">Unpublish Date:</div>
        <div class="col-74-left">
          <cfif classInfo.published AND Compare(Year(classInfo.unpublishDate),"3000")>
          <input type="text" name="unpublishDate" id="unpublishDate" class="dateField" style="width:70px;" validate="required:false, date:true" value="#unpublishDateStruct.date#" />
          <select name="unpublishDate_hh">
            <option value="12">12</option>
            <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"<cfif unpublishDateStruct.hh Is hh> selected</cfif>><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="unpublishDate_mm">
            <cfoutput><cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"<cfif unpublishDateStruct.mm Is mm> selected</cfif>><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="unpublishDate_ampm">
            <option value="am"<cfif unpublishDateStruct.ampm Is "AM"> selected</cfif>>AM</option><option value="pm"<cfif unpublishDateStruct.ampm Is "PM"> selected</cfif>>PM</option>
          </select>
          <cfelse>
          <input type="text" name="unpublishDate" id="unpublishDate" class="dateField" style="width:70px;" validate="required:false, date:true" />
          <select name="unpublishDate_hh">
            <option value="12">12</option>
            <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="unpublishDate_mm">
            <cfoutput><cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="unpublishDate_ampm">
            <option value="am">AM</option><option value="pm">PM</option>
          </select>
          </cfif>
          <input type="button" value="Now" onclick="getNow(document.frmEditClass, 'unpublishDate')" />
          <input type="button" value="Clear" onclick="clearDate(document.frmEditClass, 'unpublishDate')" />
        </div>
      </div>
      <div class="row">
        <input type="radio" name="published" value="0" <cfif Not classInfo.published>checked</cfif> /> No
      </div>
    </div>
</div>
<div class="row">
	<div class="col-30-right">Registration Participant Limit:</div>
    <div class="col-68-left">
      <div class="col-24-left">
        <input type="text" name="regLimit" maxlength="3" <cfif classInfo.regLimit LT 10000>value="#classInfo.regLimit#"</cfif> style="width:80px;" validate="number:true" /><br />
        Limit
      </div>
      <div class="col-24-left">
        <input type="text" name="regMaximum" maxlength="3" <cfif classInfo.regMaximum LT 10000>value="#classInfo.regMaximum#"</cfif> style="width:80px;" validate="number:true" /><br />
        Maximum
      </div>
    </div>
</div>
<div class="row">
	<div class="col-30-right">Notes:</div>
    <div class="col-68-left">
	  <textarea name="notes" style="width:400px;height:35px">#HTMLEditFormat(classInfo.notes)#</textarea>
	</div>
</div>
<div class="row">
	<div class="col-30-right"><b>Class Instance(s):</b></div>
    <div class="col-68-left">
    <cfset setBGColor=true>
    <cfloop query="classInstanceInfo">
      <cfset startDateStruct=UT.convertDateTimeObjectToString(DateAdd("d",0,startDateTime))>
      <cfset endDateStruct=UT.convertDateTimeObjectToString(DateAdd("d",0,endDateTime))>
      <cfif setBGColor><cfset setBGColor=false><cfelse><cfset setBGColor=true></cfif>
      <div class="row" style="<cfif setBGColor>background-color:##f5f5f5;</cfif>padding:10px 0px;">
          <div class="row">
            <div class="col-24-right"><input type="checkbox" name="deleteClassInstance_#classInstanceID#" value="1" /></div>
            <div class="col-74-left"> Check here to delete the following instance.</div>
          </div>
          <div class="row">
            <div class="col-24-right">Date:</div>
            <div class="col-74-left">
              <input type="text" name="startDate_#classInstanceID#" class="dateField" style="width:70px;" validate="required:true, date:true" value="#startDateStruct.date#" />
            </div>
          </div>
          <div class="row">
            <div class="col-24-right">From:</div>
            <div class="col-74-left">
              <select name="startDate_hh_#classInstanceID#">
                <option value="12">12</option>
                <cfloop index="hh" from="1" to="12"><option value="#hh#"<cfif startDateStruct.hh Is hh> selected</cfif>><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
              </select>
              <select name="startDate_mm_#classInstanceID#">
                <cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"<cfif startDateStruct.mm Is mm> selected</cfif>><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
              </select>
              <select name="startDate_ampm_#classInstanceID#">
                <option value="am"<cfif startDateStruct.ampm Is "AM"> selected</cfif>>AM</option><option value="pm"<cfif startDateStruct.ampm Is "PM"> selected</cfif>>PM</option>
              </select> &nbsp;&nbsp;
              To:
              <select name="endDate_hh_#classInstanceID#">
                <option value="12">12</option>
                <cfloop index="hh" from="1" to="12"><option value="#hh#"<cfif endDateStruct.hh Is hh> selected</cfif>><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
              </select>
              <select name="endDate_mm_#classInstanceID#">
                <cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"<cfif endDateStruct.mm Is mm> selected</cfif>><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
              </select>
              <select name="endDate_ampm_#classInstanceID#">
                <option value="am"<cfif endDateStruct.ampm Is "AM"> selected</cfif>>AM</option><option value="pm"<cfif endDateStruct.ampm Is "PM"> selected</cfif>>PM</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-24-right">Location:</div>
            <div class="col-74-left">
              <select name="locationID_#classInstanceID#">
                <option value="0"></option>
                <cfloop query="allLocations">
                <option value="#allLocations.locationID#"<cfif Not Compare(allLocations.locationID,classInstanceInfo.locationID)> selected</cfif>>#allLocations.locationName#</option>
                </cfloop>
              </select>
            </div>
          </div>
      </div>
    </cfloop>
	</div>
</div>

<cfif setBGColor><cfset setBGColor=false><cfelse><cfset setBGColor=true></cfif>
<div class="row">
	<div class="col-30-right">New Class Instance:</div>
    <div class="col-68-left">
      <div class="row" style="<cfif setBGColor>background-color:##f5f5f5;</cfif>padding:10px 0px;">
          <div class="row">
            <div class="col-24-right">Date:</div>
            <div class="col-74-left">
              <input type="text" name="startDate" class="dateField" style="width:70px;" validate="required:false, date:true" />
            </div>
          </div>
          <div class="row">
            <div class="col-24-right">From:</div>
            <div class="col-74-left">
              <select name="startDate_hh">
                <option value="12">12</option>
                <cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
              </select>
              <select name="startDate_mm">
                <cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
              </select>
              <select name="startDate_ampm">
                <option value="am">AM</option><option value="pm">PM</option>
              </select> &nbsp;&nbsp;
              To:
              <select name="endDate_hh">
                <option value="12">12</option>
                <cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
              </select>
              <select name="endDate_mm">
                <cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
              </select>
              <select name="endDate_ampm">
                <option value="am">AM</option><option value="pm">PM</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-24-right">Location:</div>
            <div class="col-74-left">
              <select name="locationID">
                <option value="0"></option>
                <cfloop query="allLocations">
                <option value="#locationID#">#locationName#</option>
                </cfloop>
              </select>
            </div>
          </div>
        </div>
    </div>
</div>
</form>
</cfoutput>