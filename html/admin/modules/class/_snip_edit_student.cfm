<cfparam name="URL.studentID">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset studentInfo=CL.getStudent(URL.studentID)>

<cfoutput query="studentInfo">
<form id="frmEditStudent" name="frmEditStudent" onsubmit="return false">
<input type="hidden" name="studentID" value="#URL.studentID#" />
<div class="row">
	<div class="col-30-right">User Type:</div>
    <div class="col-68-left">
      <select name="studentType" style="min-width:250px;">
        <option value="R"<cfif studentType Is "R"> selected</cfif>>Regular</option>
        <option value="F"<cfif studentType Is "F"> selected</cfif>>Fitness Club Member</option>
        <option value="E"<cfif studentType Is "E"> selected</cfif>>Employee</option>
        <option value="D"<cfif studentType Is "D"> selected</cfif>>Doctor</option>
      </select>      
    </div>
</div>
<div class="row">
	<div class="col-30-right">E-Mail Address:</div>
    <div class="col-68-left"><input type="text" name="email" maxlength="100" style="width:250px;" value="#HTMLEditFormat(email)#"></div>
</div>
<div class="row">
	<div class="col-30-right">Password:</div>
    <div class="col-68-left"><input type="text" name="password" maxlength="20" style="width:250px;" value="#HTMLEditFormat(password)#"></div>
</div>

<div class="row">
	<div class="col-30-right"><input type="checkbox" name="sendReminderEmail" value="1" <cfif sendReminderEmail>checked</cfif> /></div>
    <div class="col-68-left">
    Send a class reminder via e-mail two days before the class.
    </div>
</div>
</form>
</cfoutput>