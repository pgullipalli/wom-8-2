<cfparam name="URL.courseID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset allLocations=CL.getLocations()>
<cfset courseInfo=CL.getCourse(URL.courseID)>

<form id="frmAddClass" name="frmAddClass" onsubmit="return false">
<cfoutput><input type="hidden" name="courseID" value="#URL.courseID#" /></cfoutput>
<div class="row">
	<div class="col-30-right"><b>Publish?</b></div>
    <div class="col-68-left">
      <div class="row">
        <input type="radio" name="published" value="1" /> Yes
      </div>
      <div class="row">
        <div class="col-24-right">Publish Date:</div>
        <div class="col-74-left">
          <input type="text" name="publishDate" class="dateField" style="width:70px;" validate="required:false, date:true" />
          <select name="publishDate_hh">
            <option value="12">12</option>
            <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="publishDate_mm">
            <cfoutput><cfloop index="mm" from="0" to="59"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="publishDate_ampm">
            <option value="am">AM</option><option value="pm">PM</option>
          </select>
          <input type="button" value="Now" onclick="getNow(document.frmAddClass, 'publishDate')" />
          <input type="button" value="Clear" onclick="clearDate(document.frmAddClass, 'publishDate')" />
        </div>
      </div>
      <div class="row">
        <div class="col-24-right">Unpublish Date:</div>
        <div class="col-74-left">
          <input type="text" name="unpublishDate" class="dateField" style="width:70px;" validate="required:false, date:true" />
          <select name="unpublishDate_hh">
            <option value="12">12</option>
            <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="unpublishDate_mm">
            <cfoutput><cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="unpublishDate_ampm">
            <option value="am">AM</option><option value="pm">PM</option>
          </select>
          <input type="button" value="Now" onclick="getNow(document.frmAddClass, 'unpublishDate')" />
          <input type="button" value="Clear" onclick="clearDate(document.frmAddClass, 'unpublishDate')" />
        </div>
      </div>
      <div class="row">
        <input type="radio" name="published" value="0" checked /> No
      </div>    
    </div>
</div>

<cfoutput>
<div class="row">
	<div class="col-30-right">Registration Participant Limit:</div>
    <div class="col-68-left">
      <div class="col-24-left">
        <input type="text" name="regLimit" maxlength="3" <cfif courseInfo.regLimit LT 10000>value="#courseInfo.regLimit#"</cfif> style="width:80px;" validate="number:true" /><br />
        Limit
      </div>
      <div class="col-24-left">
        <input type="text" name="regMaximum" maxlength="3" <cfif courseInfo.regMaximum LT 10000>value="#courseInfo.regMaximum#"</cfif> style="width:80px;" validate="number:true" /><br />
        Maximum
      </div>
    </div>
</div>
</cfoutput>

<div class="row">
	<div class="col-30-right">Notes:</div>
    <div class="col-68-left">
	  <textarea name="notes" style="width:400px;height:35px"></textarea>
	</div>
</div>
<div class="row">
	<div class="col-30-right"><b>First Class Instance:</b></div>
    <div class="col-68-left">
      <div class="row">
        <div class="col-24-right">Date:</div>
        <div class="col-74-left">
          <input type="text" name="startDate" class="dateField" style="width:70px;" validate="required:true, date:true" />
        </div>
      </div>
      <div class="row">
        <div class="col-24-right">From:</div>
        <div class="col-74-left">
          <select name="startDate_hh">
            <option value="12">12</option>
            <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="startDate_mm">
            <cfoutput><cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="startDate_ampm">
            <option value="am">AM</option><option value="pm">PM</option>
          </select> &nbsp;&nbsp;
          To:
          <select name="endDate_hh">
            <option value="12">12</option>
            <cfoutput><cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="endDate_mm">
            <cfoutput><cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop></cfoutput>
          </select>
          <select name="endDate_ampm">
            <option value="am">AM</option><option value="pm">PM</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col-24-right">Location:</div>
        <div class="col-74-left">
          <select name="locationID">
            <option value="0"></option>
            <cfoutput query="allLocations">
            <option value="#locationID#">#locationName#</option>
            </cfoutput>
          </select>
        </div>
      </div>
    </div>
</div>
<div class="row">
  <div class="col-30-right">&nbsp;</div>
  <div class="col-68-left">
  (If this class has multiple class instances, you can 'edit' this class later to add additional class instances after this information is saved.)
  </div>
</div>
</form>