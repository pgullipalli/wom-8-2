<cfparam name="URL.courseID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset PB=CreateObject("component", "com.PageBuilderAdmin").init()>
<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>
<cfset allCategories=CL.getAllCategories()>
<cfset categoryIDList=CL.getCategoryIDListByCourseID(URL.courseID)>
<cfset courseInfo=CL.getCourse(URL.courseID)>
<cfset pageInfo=PB.getPageByPageID(courseInfo.pageID)>
<cfset itemInfo=DR.getItemByID(courseInfo.directoryItemID)>
<cfif pageInfo.recordcount GT 0>
  <cfset pageName=pageInfo.pageName>
<cfelse>
  <cfset pageName="">
</cfif>
<cfif itemInfo.recordcount GT 0>
  <cfset departmentName=itemInfo.orgName>
<cfelse>
  <cfset departmentName="">
</cfif>

<cfoutput query="courseInfo">
<form id="frmEditCourse" name="frmEditCourse" onsubmit="return false">
<input type="hidden" name="courseID" value="#URL.courseID#" />
<div class="row">
	<div class="col-30-right"><b>Categories:</b><br /><small>(Check all that apply.)</small></div>
    <div class="col-68-left">
      <cfset numCategories=allCategories.recordcount>
      <cfset numRows=Ceiling(numCategories/2)>
      <cfoutput><cfset idx = 0>
      <cfloop index="row" from="1" to="#numRows#">
        <div class="row">
        <cfloop index="col" from="1" to="2">
          <cfset idx = (row - 1) * 2 + col>
          <cfif idx LTE numCategories>
            <cfset categoryID=allCategories.categoryID[idx]>
            <cfset categoryName=allCategories.categoryName[idx]>
            <div class="col-49-left">
              <input type="checkbox" name="categoryIDList" value="#categoryID#"<cfif ListFind(categoryIDList, categoryID) GT 0> checked</cfif> />
              #categoryName#
            </div>
          </cfif>
        </cfloop>
        </div>
      </cfloop>
      </cfoutput>
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Course Title:</b></div>
    <div class="col-68-left"><input type="text" name="courseTitle" maxlength="100" value="#HTMLEditFormat(courseTitle)#" style="width:320px;" validate="required:true" /></div>
</div>
<div class="row">
	<div class="col-30-right"><b>GL Number:</b></div>
    <div class="col-68-left"><input type="text" name="GLNumber" maxlength="100" value="#HTMLEditFormat(GLNumber)#" style="width:160px;"  /></div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="published" value="1" <cfif published>checked</cfif> /> Publish this course<br />
      <input type="checkbox" name="featuredOnHomepage" value="1" <cfif featuredOnHomepage>checked</cfif> /> Featured on home/sub-home page<br />
      <input type="checkbox" name="registerOnline" value="1" <cfif registerOnline>checked</cfif> /> Register online<br />
      <input type="checkbox" name="fitnessClubOnly" value="1" <cfif fitnessClubOnly>checked</cfif> /> Fitness Club Member Only<br />
      <input type="checkbox" name="featuredOnClassHomePage" value="1" <cfif featuredOnClassHomePage>checked</cfif> /> Featured on class landing page        
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Registration Fee:</b></div>
    <div class="col-68-left">
      <div class="col-24-left">
        $<input type="text" name="feeRegular" maxlength="10" value="#NumberFormat(feeRegular,'9.99')#" style="width:80px;" validate="number:true,required:true" /><br />
        Regular
      </div>
      <div class="col-24-left">
        $<input type="text" name="feeFitnessClub" maxlength="10" value="#NumberFormat(feeFitnessClub,'9.99')#" style="width:80px;" validate="number:true,required:true" /><br />
        Fitness Club
      </div>
      <div class="col-24-left">
        $<input type="text" name="feeEmployee" maxlength="10" value="#NumberFormat(feeEmployee,'9.99')#" style="width:80px;" validate="number:true,required:true" /><br />
        Employee
      </div>
      <div class="col-24-left">
        $<input type="text" name="feeDoctor" maxlength="10" value="#NumberFormat(feeDoctor,'9.99')#" style="width:80px;" validate="number:true,required:true" /><br />
        Doctor
      </div>
      <div class="row">
      <input type="checkbox" name="callForPrice" value="1" <cfif callForPrice>checked</cfif> /> Call for pricing<br />
      <input type="checkbox" name="feePerCouple" value="1" <cfif feePerCouple>checked</cfif> /> Fee per couple
      (Attendee <input type="text" name="attendeeNameLabel" maxlength="100" value="#HTMLEditFormat(attendeeNameLabel)#" style="width:80px;" />
      Guest <input type="text" name="guestNameLabel" maxlength="100" value="#HTMLEditFormat(guestNameLabel)#" style="width:80px;" />)
      </div>
    </div>
</div>
<div class="row">
	<div class="col-30-right">Registration Participant Limit:</div>
    <div class="col-68-left">
      <div class="col-24-left">
        <cfif regLimit LT 10000>
          <input type="text" name="regLimit" maxlength="3" value="#regLimit#" style="width:80px;" validate="number:true" /><br />
        <cfelse>
          <input type="text" name="regLimit" maxlength="3" value="" style="width:80px;" validate="number:true" /><br />
        </cfif>
        Limit
      </div>
      <div class="col-24-left">
        <cfif regMaximum LT 10000>
          <input type="text" name="regMaximum" maxlength="3" value="#regMaximum#" style="width:80px;" validate="number:true" /><br />
        <cfelse>
          <input type="text" name="regMaximum" maxlength="3" value="" style="width:80px;" validate="number:true" /><br />
        </cfif>
        Maximum
      </div>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Promotion:</div>
    <div class="col-68-left">
      <div class="col-68-left">
        <input type="text" name="discountCode" maxlength="20" value="#HTMLEditFormat(discountCode)#" style="width:80px;" />        
        <input type="radio" name="discountType" value="flat rate" <cfif Not Compare(discountType,"flat rate")> checked</cfif>  /> Flat Rate
        <input type="radio" name="discountType" value="percentage" <cfif Not Compare(discountType,"percentage")> checked</cfif> /> Percentage<br />
        Code
      </div>
      <div class="col-24-left">
        <input type="text" name="discountRate" maxlength="10" <cfif discountRate GT 0>value="#NumberFormat(discountRate,'9.99')#"</cfif> style="width:80px;" validate="number:true" /><br />
        Rate (% or $)
      </div>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Department:</div>
    <div class="col-68-left">
      <input type="hidden" name="directoryItemID" value="#directoryItemID#" />
      <CF_SelectDirectoryItem formName="frmAddCourse" fieldName="directoryItemID" previewID="preview3"> <span id="preview3" style="font-weight:bold;font-style:italic;">#HTMLEditFormat(departmentName)#</span>
    </div>
</div>
<div class="row">
	<div class="col-30-right">Contact Info:</div>
    <div class="col-68-left">
      <div class="col-32-left">
        <input type="text" name="contactName" maxlength="100" value="#HTMLEditFormat(contactName)#" style="width:140px;" /><br />
        Name
      </div>
      <div class="col-32-left">
        <input type="text" name="contactEmail" maxlength="100" value="#HTMLEditFormat(contactEmail)#" style="width:140px;" /><br />
        E-Mail
      </div>
      <div class="col-32-left">
        <input type="text" name="contactPhone" maxlength="100" value="#HTMLEditFormat(contactPhone)#" style="width:140px;" /><br />
        Phone
      </div>      
    </div>
</div>

<div class="row">
	<div class="col-30-right">Course Image:</div>
    <div class="col-68-left">
      <input type="hidden" name="courseImage" value="#HTMLEditFormat(courseImage)#" />
      <CF_SelectFile formName="frmEditCourse" fieldName="courseImage" previewID="preview15" previewWidth="100">
      <small>(148px x 148px)</small>
      <cfif Compare(courseImage, "")>
      <div id="preview15"><img src="/#courseImage#" border="0" width="100" /></div>  
      <cfelse>
      <div id="preview15"></div>   
      </cfif>
      <div class="row">
      Caption: <input type="text" name="courseImageCaption" maxlength="200" style="width:350px;" value="#HTMLEditFormat(courseImageCaption)#" />
      </div>
    </div>
</div>
<div class="row">
	<div class="col-30-right">Course Thumbnail:</div>
    <div class="col-68-left">
      <input type="hidden" name="courseThumbnail" value="#HTMLEditFormat(courseThumbnail)#" />
      <CF_SelectFile formName="frmEditCourse" fieldName="courseThumbnail" previewID="preview21" previewWidth="100">
      <small>(211px � 75px)</small>
      <cfif Compare(courseThumbnail, "")>
      <div id="preview21"><img src="/#courseThumbnail#" border="0" width="100" /></div>  
      <cfelse>
      <div id="preview21"></div>   
      </cfif>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Short Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmEditCourse"
            fieldName="shortDescription"
            cols="80"
            rows="3"
            HTMLContent="#shortDescription#"
            displayHTMLSource="true"
            editorHeader="Edit Short Description">
    </div>
</div>

<div class="row">
	<div class="col-30-right">Long Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="false"
            formName="frmEditCourse"
            fieldName="longDescription"
            cols="80"
            rows="3"
            HTMLContent="#longDescription#"
            displayHTMLSource="true"
            editorHeader="Edit Long Description">
    </div>
</div>

<div class="row">
	<div class="col-30-right">Special Instructions:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmEditCourse"
            fieldName="specialInstructions"
            cols="80"
            rows="3"
            HTMLContent="#specialInstructions#"
            displayHTMLSource="true"
            editorHeader="Edit Special Instructions">
    </div>
</div>

<div class="row">
	<div class="col-30-right">Registration URL:</div>
    <div class="col-68-left">
      <input type="text" name="registrationURL" maxlength="200" value="#HTMLEditFormat(registrationURL)#" style="width:320px;" /><br />
      (if controlled ouside of registration system)
    </div>
</div>
<div class="row">
	<div class="col-30-right">Info URL (external):</div>
    <div class="col-68-left">
      <input type="text" name="outsideEventURL" maxlength="200" value="#HTMLEditFormat(outsideEventURL)#" style="width:320px;" />
    </div>
</div>
<div class="row">
	<div class="col-30-right">More Info:</div>
    <div class="col-68-left">
      <input type="hidden" name="pageID" value="#pageID#">
      <CF_SelectPage formName="frmAddCourse" fieldName="pageID" previewID="preview4"> <span id="preview4" style="font-weight:bold;font-style:italic;">#HTMLEditFormat(pageName)#</span><br />
      (link to a Page Builder page)
    </div>
</div>
<div class="row">
	<div class="col-30-right">Send E-Mail?</div>
    <div class="col-68-left">
      <div class="col-32-left">
      Class Reminder<br />
     <input type="radio" name="sendReminderEmail" value="1" <cfif sendReminderEmail>checked</cfif>  /> Yes
      <input type="radio" name="sendReminderEmail" value="0" <cfif Not sendReminderEmail>checked</cfif> /> No
      </div>
      <div class="col-32-left">
      Follow-Up Feedback<br />
      <input type="radio" name="sendFollowUpEmailFeedback" value="1" <cfif sendFollowUpEmailFeedback>checked</cfif> /> Yes
      <input type="radio" name="sendFollowUpEmailFeedback" value="0" <cfif Not sendFollowUpEmailFeedback>checked</cfif> /> No
      </div>
      <div class="col-32-left">
      Missed Class Follow-Up<br />
      <input type="radio" name="FollowUpEmailMissedClass" value="1" <cfif FollowUpEmailMissedClass>checked</cfif> /> Yes
      <input type="radio" name="FollowUpEmailMissedClass" value="0" <cfif Not FollowUpEmailMissedClass>checked</cfif> /> No
      </div>      
    </div>
</div>
</form>
</cfoutput>