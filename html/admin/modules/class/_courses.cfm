<cfparam name="URL.catID" default="0">

<cfset newline=Chr(13) & Chr(10)>
<cfset htmlHead='<script type="text/javascript" src="modules/class/courses.js"></script>' & newline>
<cfset htmlHead=htmlHead & '<link rel="stylesheet" href="/jsapis/jquery/css/tooltip/jquery.tooltip.css" />' & newline>
<cfset htmlHead=htmlHead & '<script src="/jsapis/jquery/js/jquery.tooltip.min.js" type="text/javascript"></script>' & newline>
<cfhtmlhead text="#htmlHead#">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset allCategories=CL.getAllCategories()>
<cfif URL.catID IS 0>
  <cfset URL.catID=CL.getFirstCategoryID()>
  <cfif URL.catID IS 0><!--- no categories were ever created --->
    <cflocation url="index.cfm?md=class&tmp=categories&wrap=1">
  </cfif>
</cfif>

<div class="header">CLASS MANAGER &gt; Courses</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addCourse();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editCourse(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteCourse();">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="manageClasses();">Manage Classes</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="profitabilityReport();">Profitability Report</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="couponCodeReport();">Coupon Code Report</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="previewCourse();">Preview</a>
</div>

<br style="clear:both;" /><br />

<cfoutput>
<form id="frmNavigator">
  Category
  <select name="categoryID" style="min-width:250px;">
    <cfloop query="allCategories">
    <option value="#categoryID#"<cfif URL.catID IS categoryID> selected</cfif>>#categoryName#</option>
    </cfloop>
  </select>
</form>
</cfoutput>

<br style="clear:both;" />

<div id="mainDiv"></div>