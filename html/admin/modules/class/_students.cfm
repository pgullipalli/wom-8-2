<cfparam name="URL.studentType" default="R">
<cfparam name="URL.pageNum" default="1">

<script type="text/javascript" src="modules/class/students.js"></script>
<style type="text/css">.password{display:none;}</style>

<span id="pageNum" style="display:none;"><cfoutput>#URL.pageNum#</cfoutput></span>

<div class="header">CLASS MANAGER &gt; Registrants</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addStudent();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editStudent(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteStudent();">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="manageSchedule();">Manage Schedule</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="loginAsUser();">Login As User</a>
</div>

<br style="clear:both;" /><br />

<cfoutput>
<form id="frmNavigator">
  User Type
  <select name="studentType" style="min-width:250px;">    
    <option value="R"<cfif URL.studentType IS "R"> selected</cfif>>Regular</option>
    <option value="F"<cfif URL.studentType IS "F"> selected</cfif>>Fitness Club Member</option>
    <option value="E"<cfif URL.studentType IS "E"> selected</cfif>>Employee</option>
    <option value="D"<cfif URL.studentType IS "D"> selected</cfif>>Doctor</option>
  </select>
  &nbsp;&nbsp;&nbsp;&nbsp;
  <a href="index.cfm?md=class&tmp=searchstudents&wrap=1">Search Users</a>
</form>
</cfoutput>

<br style="clear:both;" />
<p><a href="javascript:togglePassword();" class="noUnderline">&raquo; Show/Hide Passwords</a></p>
<div id="mainDiv"></div>