<cfparam name="URL.courseID">
<cfparam name="URL.classID">
<cfparam name="URL.classInstanceID">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset courseInfo=CL.getCourse(URL.courseID)>
<cfset allClassInstances=CL.getClassInstancesByClassID(URL.classID)>
<cfset classInstanceInfo=CL.getClassInstanceByClassInstanceID(URL.classInstanceID)>

<script language="javascript">
function openprintdialog () {
  var onWindows = navigator.platform ? navigator.platform == "Win32" : false;
  var macIE = document.all && !onWindows;
  if (macIE) {
	alert ("Press \"Cmd+p\" on your keyboard to print");
  } else {
	window.print();
  }  
}			
</script>

<style type="text/css" media="screen">
#printerControl {text-align:right;}
</style>

<style type="text/css" media="print">
#printerControl {display:none;}
</style>

<div id="printerControl"><a href="javascript:openprintdialog();">PRINT</a> | <a href="javascript:window.close();">CLOSE</a></div>

<div style="margin:0px 0px; width:100%">
<img src="images/womans_logo2.gif" style="padding:0px 0px; height:100px; border-width:0px;">
</div>

<p class="header" align="center"><b>Class Address Sheet</b></p>
<p class="subHeader" align="center"><b><cfoutput>#courseInfo.courseTitle#</cfoutput></b></p>

<div class="row">    
  <cfoutput>
    <p>
    <b>Date/Time:</b>
    <ul>
    <cfloop query="allClassInstances">
      <cfif Compare(allClassInstances.classInstanceID, URL.classInstanceID)>
      <li><strike>#DateFormat(startDateTime,"mm/dd/yyyy")# #LCase(TimeFormat(startDateTime,"hh:mmtt"))# - #LCase(TimeFormat(endDateTime,"hh:mmtt"))#</strike></li>
      <cfelse>
      <li>#DateFormat(startDateTime,"mm/dd/yyyy")# #LCase(TimeFormat(startDateTime,"hh:mmtt"))# - #LCase(TimeFormat(endDateTime,"hh:mmtt"))#</li>
      </cfif>
    </cfloop>
    </ul>
    </p>
    
    <p><b>Location:</b> #classInstanceInfo.locationName#</p>
    <p><b>Contact:</b> #courseInfo.contactName#</p>
  </cfoutput>
</div>

<cfif Not IsDefined("URL.fieldsToDisplay")>
  <form name="frmAddressSheet" id="frmAddressSheet" action="index.cfm" method="get">
  <cfoutput>
  <input type="hidden" name="md" value="class" />
  <input type="hidden" name="tmp" value="class_address_sheet" />
  <input type="hidden" name="wrap" value="1" />
  <input type="hidden" name="courseID" value="#URL.courseID#" />
  <input type="hidden" name="classID" value="#URL.classID#" />
  <input type="hidden" name="classInstanceID" value="#URL.classInstanceID#">  
  <div class="row">
    <div class="col-24-left">&nbsp;</div>
    <div class="col-74-left"><b>Please select the fields that will appear on the address sheet:</b></div>
  </div>  
  <div class="row">
    <div class="col-24-left">&nbsp;</div>
    <div class="col-74-left">&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="checkbox" name="fieldsToDisplay" value="attendeeName" /> <cfif Compare(courseInfo.attendeeNameLabel,"")>#courseInfo.attendeeNameLabel#<cfelse>Attendee Name</cfif>
    </div>
  </div>
  <div class="row">
    <div class="col-24-left">&nbsp;</div>
    <div class="col-74-left">&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="checkbox" name="fieldsToDisplay" value="guestNames" /> <cfif Compare(courseInfo.guestNameLabel,"")>#courseInfo.guestNameLabel#<cfelse>Guest Name(s)</cfif>
    </div>
  </div>
  <div class="row">
    <div class="col-24-left">&nbsp;</div>
    <div class="col-74-left">&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="checkbox" name="fieldsToDisplay" value="studentType" /> Customer Type
    </div>
  </div>
  <div class="row">
    <div class="col-24-left">&nbsp;</div>
    <div class="col-74-left">&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="checkbox" name="fieldsToDisplay" value="address" /> Address
    </div>
  </div>
  <div class="row">
    <div class="col-24-left">&nbsp;</div>
    <div class="col-74-left">&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="checkbox" name="fieldsToDisplay" value="phone" /> Daytime/Cell Phone
    </div>
  </div>
  <div class="row">
    <div class="col-24-left">&nbsp;</div>
    <div class="col-74-left">&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="checkbox" name="fieldsToDisplay" value="email" /> E-Mail Address
    </div>
  </div>
  
  
  <div class="row">
    <div class="col-24-left">&nbsp;</div>
    <div class="col-74-left">&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="submit" value=" Submit ">
    </div>
  </div>
  </cfoutput>
  </form>   
<cfelse>
	<cfset classAttendees=CL.getClassAttendeesByClassID(URL.classID)>
    <cfset numAttendees=classAttendees.recordcount>

    <div class="row">    
      <img src="images/imgDisabilityLogo.gif" border="0" align="left" /> <b>Notify Resources for Women if you need assistance due to disability recognized by the ADA.</b></p>
    </div>
    
    <br style="clear:both;" /><br />
    <cfoutput>
    <form>
    <table border="0" cellpadding="3" cellspacing="0" width="100%" align="center">
      <tr valign="top">
        <cfset numColumns=1>
        <cfif ListFindNoCase(URL.fieldsToDisplay,"attendeeName") GT 0>
		  <cfset numColumns=numColumns+1>
          <th><cfif Compare(courseInfo.attendeeNameLabel,"")>#courseInfo.attendeeNameLabel#<cfelse>Attendee Name</cfif></th>
        </cfif>
        <cfif ListFindNoCase(URL.fieldsToDisplay,"guestNames") GT 0>
		  <cfset numColumns=numColumns+1>
          <th><cfif Compare(courseInfo.guestNameLabel,"")>#courseInfo.guestNameLabel#<cfelse>Guest Name(s)</cfif></th>
        </cfif>
        <cfif ListFindNoCase(URL.fieldsToDisplay,"studentType") GT 0>
		  <cfset numColumns=numColumns+1>
          <th>Customer Type</th>
        </cfif>
        <th>Attended</th>        
        <cfif ListFindNoCase(URL.fieldsToDisplay,"address") GT 0>
		  <cfset numColumns=numColumns+1>
          <th>Address</th>
        </cfif>
        <cfif ListFindNoCase(URL.fieldsToDisplay,"phone") GT 0>
		  <cfset numColumns=numColumns+1>
          <th>Daytime/Cell Phone</th>
        </cfif>
        <cfif ListFindNoCase(URL.fieldsToDisplay,"email") GT 0>
		  <cfset numColumns=numColumns+1>
          <th>E-Mail Address</th>
        </cfif>
      </tr>
      <tr valign="middle" height="10">
        <td colspan="#numColumns#"><div style="border-bottom:1px solid ##666666; width:100%;"></div></td>
      </tr>
      
      <cfloop index="idx" from="1" to="#numAttendees#">
        <cfif idx MOD 2 IS 0><cfset bgcolor=" bgcolor=""##eeeeee"""><cfelse><cfset bgcolor=""></cfif>
        <tr#bgcolor#>
        <cfif ListFindNoCase(URL.fieldsToDisplay,"attendeeName") GT 0>
          <td><b>#classAttendees.attendeeName[idx]#</b></td>
        </cfif>
        <cfif ListFindNoCase(URL.fieldsToDisplay,"guestNames") GT 0>
          <td><b>#classAttendees.guestNames[idx]#</b>&nbsp;</td>
        </cfif>
        <cfif ListFindNoCase(URL.fieldsToDisplay,"studentType") GT 0>
          <td align="center">
            <cfswitch expression="#classAttendees.studentType[idx]#">
              <cfcase value="F">Fitness Club</cfcase>
              <cfcase value="E">Employee</cfcase>
              <cfcase value="D">Doctor</cfcase>
              <cfdefaultcase>Regular</cfdefaultcase>
            </cfswitch>
          </td>
        </cfif>
		<td align="center"><input type="checkbox" name="attended" value="1" /></td>
        <cfif ListFindNoCase(URL.fieldsToDisplay,"address") GT 0>
          <td>          
            #classAttendees.address1[idx]#<cfif Compare(classAttendees.address2[idx],"")>, #classAttendees.address2[idx]#</cfif><br />
            #classAttendees.city[idx]#, #classAttendees.state[idx]# #classAttendees.zip[idx]#&nbsp;
          </td>
        </cfif>
        <cfif ListFindNoCase(URL.fieldsToDisplay,"phone") GT 0>
          <td align="center">
            <cfif Compare(classAttendees.daytimePhone[idx],"")>#classAttendees.daytimePhone[idx]#(D)</cfif>
            <cfif Compare(classAttendees.cellPhone[idx],"")>
              <cfif Compare(classAttendees.daytimePhone[idx],"")><br /></cfif>
              #classAttendees.cellPhone[idx]#(C)
			</cfif>&nbsp;
          </td>
        </cfif>
        <cfif ListFindNoCase(URL.fieldsToDisplay,"email") GT 0>
          <td>#classAttendees.email[idx]#&nbsp;</td>
        </cfif>
        </tr>
      </cfloop>      
    </table>
    </form>
    </cfoutput>
</cfif>
<br style="clear:both;" /><br /><br />