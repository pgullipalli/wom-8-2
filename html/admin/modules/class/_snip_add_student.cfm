<cfparam name="URL.studentType" default="R">

<form id="frmAddStudent" name="frmAddStudent" onsubmit="return false">
<div class="row">
	<div class="col-30-right">User Type:</div>
    <div class="col-68-left">
      <select name="studentType" style="min-width:250px;">
        <option value="R"<cfif URL.studentType Is "R"> selected</cfif>>Regular</option>
        <option value="F"<cfif URL.studentType Is "F"> selected</cfif>>Fitness Club Member</option>
        <option value="E"<cfif URL.studentType Is "E"> selected</cfif>>Employee</option>
        <option value="D"<cfif URL.studentType Is "D"> selected</cfif>>Doctor</option>
      </select>      
    </div>
</div>
<div class="row">
	<div class="col-30-right">E-Mail Address:</div>
    <div class="col-68-left"><input type="text" name="email" maxlength="100" style="width:250px;" value=""></div>
</div>
<div class="row">
	<div class="col-30-right">Password:</div>
    <div class="col-68-left"><input type="text" name="password" maxlength="20" style="width:250px;" value=""></div>
</div>

<div class="row">
	<div class="col-30-right"><input type="checkbox" name="sendReminderEmail" value="1" /></div>
    <div class="col-68-left">
    Send a class reminder via e-mail two days before the class.
    </div>
</div>
</form>