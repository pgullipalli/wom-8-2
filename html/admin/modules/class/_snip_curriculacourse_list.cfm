<cfparam name="URL.curriculaID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset curriculaCourses=CL.getCurriculaCourses(URL.curriculaID)>

<cfif curriculaCourses.recordcount IS 0>
<p>No courses selected for this curricula. Click "New" button in the action bar to select courses.</p>
<cfelse>
<form id="frmCurrilaCourseList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="100">Up/Down</th>
	<th>Course Title</th>
    <th width="100">Publish?</th>
  </tr>  
  <cfoutput query="curriculaCourses">
  <tr>
    <td align="center"><input type="checkbox" name="courseIDList" value="#courseID#"></td>
    <td align="center">
      <a href="##" onclick="moveItem('#courseID#','up')"><img src="images/icon_up.gif" border="0" align="absbottom" alt="move up"></a>
      <a href="##" onclick="moveItem('#courseID#','down')"><img src="images/icon_down.gif" border="0" align="absbottom" alt="move down"></a>
    </td>
	<td>#courseTitle#</td> 
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>





