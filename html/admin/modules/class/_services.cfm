<cfparam name="URL.catID" default="0">

<script type="text/javascript" src="modules/class/services.js"></script>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset allCategories=CL.getAllCategories()>
<cfif URL.catID IS 0>
  <cfset URL.catID=CL.getFirstCategoryID()>
  <cfif URL.catID IS 0><!--- no categories were ever created --->
    <cflocation url="index.cfm?md=class&tmp=categories&wrap=1">
  </cfif>
</cfif>

<div class="header">CLASS MANAGER &gt; Services</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addService();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editService(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteService();">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button130" onclick="manageAttendance();">Manage Attendance</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="previewService();">Preview</a>
</div>

<br style="clear:both;" /><br />

<cfoutput>
<form id="frmNavigator">
  Category
  <select name="categoryID" style="min-width:250px;">
    <cfloop query="allCategories">
    <option value="#categoryID#"<cfif URL.catID IS categoryID> selected</cfif>>#categoryName#</option>
    </cfloop>
  </select>
</form>
</cfoutput>

<br style="clear:both;" />

<div id="mainDiv"></div>