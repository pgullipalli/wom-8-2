var classID;
var courseID;

$(function () {	
	classID=$("#classID").html();
	courseID=$("#courseID").html();
	loadAttendeeList();
	
	$("#frmNavigator select[name='classInstanceID']").change(function() {
	  $("#message").hide();
	  loadAttendeeList();	  
	});
});

function loadAttendeeList() {
  var classInstanceID=$("#frmNavigator select[name='classInstanceID']").val();	
  
  var url="index.cfm?md=class&tmp=snip_attendee_list&classID=" + classID + "&classInstanceID=" + classInstanceID + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function updateAttendance() {
	$("#message").hide();	
	var form = $("#frmAttendeeList");
	if (form.length == 0) return;
	xsite.showWaitingDialog();
	form.ajaxSubmit({ 
	  url: 'action.cfm?md=class&task=updateAttendance',
	  type: 'post',
	  cache: false,
	  dataType: 'json', 
	  success: processJson,
	  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		xsite.closeWaitingDialog();
		$("#msgAttendanceUpdate").html('The attendance of this class instance has been updated.');	
		$("#message").show();
		loadAttendeeList();
	  } else {
		xsite.closeWaitingDialog();
		alert (jsonData.MESSAGE);
	  }
	}
}

function sendFollowUpFeedbackRequest() {
  var classInstanceID=$("#frmNavigator select[name='classInstanceID']").val();	
  if (!confirm('Are you sure you want to send a follow-up class feedback request email to the students who attended this class instance?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=sendFeedbackRequest&classInstanceID=' + classInstanceID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  $("#msgAttendanceUpdate").html('A follow-up class feedback request email has been sent to the students who attended this class instance.');	
	  $("#message").show();
	  loadAttendeeList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }		
}

function sendMissedClassFollowUp() {
  var classInstanceID=$("#frmNavigator select[name='classInstanceID']").val();	
  
  if (!confirm('Are you sure you want to send a missed class follow-up email to the students who didn\'t attend this class instance?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=sendMissedClassFollowUp&classInstanceID=' + classInstanceID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  $("#msgAttendanceUpdate").html('A missed class follow-up email has been sent to the students who didn\'t attend this class instance.');	
	  $("#message").show();
	  loadAttendeeList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function printAttendanceSheet() {
  var classInstanceID=$("#frmNavigator select[name='classInstanceID']").val();
  xsite.openWindow('index.cfm?md=class&tmp=class_attendance_sheet&wrap=1&courseID=' + courseID + '&classID=' + classID + '&classInstanceID=' + classInstanceID + '&uuid=' + xsite.getUUID(), 'attendance', 1000, 600);
}

function printAddressSheet() {
  var classInstanceID=$("#frmNavigator select[name='classInstanceID']").val();
  xsite.openWindow('index.cfm?md=class&tmp=class_address_sheet&wrap=1&courseID=' + courseID + '&classID=' + classID + '&classInstanceID=' + classInstanceID + '&uuid=' + xsite.getUUID(), 'attendance', 1000, 600);
}