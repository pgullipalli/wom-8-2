<cfparam name="URL.serviceID">
<cfparam name="URL.catID" default="0">

<script type="text/javascript" src="modules/class/service_attendance.js"></script>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset serviceInfo=CL.getService(URL.serviceID)>
<cfset serviceAttendees=CL.getServiceAttendeesByServiceID(URL.serviceID)>
<cfset attendedStudentServiceIDList=CL.getAttendedStudentServiceIDListByServiceID(URL.serviceID)>
<cfset numAttendees=serviceAttendees.recordcount>


<div class="header">CLASS MANAGER &gt; Service Attendance</div>

<span id="serviceID" style="display:none"><cfoutput>#URL.serviceID#</cfoutput></span>

<div>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="updateAttendance();">Update Attendance</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="printAttendanceSheet();">Print Attendance Sheet</a>
</div>
<br /><br />

<cfoutput>
<p><a href="index.cfm?md=class&tmp=services&wrap=1&catID=#URL.catID#">&laquo; Back</a></p>

<p class="subHeader">Service Title: <b><i>#serviceInfo.serviceTitle#</i></b></p>
</cfoutput>

<div id="message" class="ui-widget" style="display:none; margin:10px 0px; width:450px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgAttendanceUpdate"></span></p>
  </div>
</div>

<cfif numAttendees GT 0>
  <cfoutput>
  <form id="frmAttendeeList">
  <input type="hidden" name="serviceID" value="#URL.serviceID#">

  <table border="0" cellpadding="3" cellspacing="0" width="600">    
    <tr valign="top">
      <td width="300">
      <cfloop index="idx" from="1" to="#Ceiling(numAttendees/2)#">
        <input type="checkbox" name="attendedStudentServiceIDList" value="#serviceAttendees.studentServiceID[idx]#"<cfif ListFind(attendedStudentServiceIDList, serviceAttendees.studentServiceID[idx]) GT 0> checked</cfif>>
        #idx#. #serviceAttendees.attendeeName[idx]# (#DateFormat(serviceAttendees.dateCreated[idx],"mm/dd/yyyy")#)<br />
      </cfloop>
      &nbsp;
      </td>
      <td width="300">
      <cfloop index="idx" from="#Ceiling(numAttendees/2) + 1#" to="#numAttendees#">
        <input type="checkbox" name="attendedStudentServiceIDList" value="#serviceAttendees.studentServiceID[idx]#"<cfif ListFind(attendedStudentServiceIDList, serviceAttendees.studentServiceID[idx]) GT 0> checked</cfif>>
        #idx#. #serviceAttendees.attendeeName[idx]# (#DateFormat(serviceAttendees.dateCreated[idx],"mm/dd/yyyy")#)<br />
      </cfloop>
      &nbsp;
      </td>
    </tr>
  </table>
  </form>
  </cfoutput>
<cfelse>
  <p>No students registered for this service.</p>
</cfif>

<br style="clear:both;" />

<div>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="updateAttendance();">Update Attendance</a>
</div>
