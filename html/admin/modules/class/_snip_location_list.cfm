<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset allLocations=CL.getLocations()>

<cfif allLocations.recordcount IS 0>
  <p>
  It appears that there are no locations created.
  Please click on the 'New' button in the action bar above to create the first location.
  </p>
<cfelse>
<form id="frmLocationList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="250">Location Name</th>
    <th width="200">Campus Map</th>
    <th>&nbsp;</th>
  </tr>
  <cfoutput query="allLocations">
  <tr>
    <td align="center"><input type="radio" name="locationID" value="#locationID#"></td>
    <td><a href="javascript:editLocation('#locationID#')">#locationName#</a></td>
    <td align="center">
      <cfif mapID GT 0>
        <a href="javascript:xsite.openWindow('index.cfm?md=class&tmp=verifycampusmap&locationID=#locationID#&wrap=1','',850,650)" title="click to edit Campus Map" class="accent01">Edit/View</a>
      <cfelse>
        N/A |
        <a href="javascript:xsite.openWindow('index.cfm?md=class&tmp=verifycampusmap&locationID=#locationID#&wrap=1','',850,650)" title="click to add Campus Map" class="accent01">Add</a>
      </cfif>
    </td>
    <td>&nbsp;</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>