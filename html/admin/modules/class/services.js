var pageNum=1;//initial page number

$(function () {	
	loadServiceList();
	
	$("#frmNavigator select[name='categoryID']").change(function() {
	  pageNum=1;
	  loadServiceList();	  
	});
});

function loadServiceList(pNum) {
  if (pNum) pageNum = pNum;
  var catID=$("#frmNavigator select[name='categoryID']").val();	
  
  var url="index.cfm?md=class&tmp=snip_service_list&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function addService() {
  var catID=$("#frmNavigator select[name='categoryID']").val();	
  xsite.createModalDialog({
	windowName: 'winAddService',
	title: 'Add Service',
	width: 750,
	height:500,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_add_service&catID=' + catID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddService").dialog('close'); },
	  ' Add ': submitForm_AddService
	}
  }).dialog('open');
  
  function submitForm_AddService() {
	var form = $("#frmAddService");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=class&task=addService',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddService").dialog('close');
		loadServiceList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editService(itID) {
  var form = $("#frmServiceList")[0];
  var serviceID = null;
  if (itID) serviceID = itID;
  else serviceID = xsite.getCheckedValue(form.serviceID);

  if (!serviceID) {
	alert("Please select a service to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditService',
	title: 'Edit Service',
	width: 750,
	height: 500,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_edit_service&serviceID=' + serviceID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditService").dialog('close'); },
	  ' Save ': submitForm_EditService
	}
  }).dialog('open');
	
  function submitForm_EditService() {
	var form = $("#frmEditService");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=class&task=editService',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
		
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditService").dialog('close');
		loadServiceList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteService() {
  var form = $("#frmServiceList")[0];
  var serviceID = xsite.getCheckedValue(form.serviceID);
  if (!serviceID) {
	alert("Please select a service to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected service?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deleteService&serviceID=' + serviceID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadServiceList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function manageAttendance() {
  var form = $("#frmServiceList")[0];
  var catID=$("#frmNavigator select[name='categoryID']").val();	
  var serviceID = xsite.getCheckedValue(form.serviceID);
  if (!serviceID) {
	alert("Please select a service to manage its attendance.");
	return;
  }
  
  window.location="index.cfm?md=class&tmp=service_attendance&wrap=1&serviceID=" + serviceID + "&catID=" + catID;  
}

function previewService() {
  var form = $("#frmServiceList")[0];
  var serviceID = xsite.getCheckedValue(form.serviceID);
  if (!serviceID) {
	alert("Please select a service to preview.");
	return;
  }
  var catID=$("#frmNavigator select[name='categoryID']").val();	
  window.open('/index.cfm?md=class&tmp=detail_service&sid=' + serviceID + '&catid=' + catID + '&uuid=' + xsite.getUUID(), '_blank');
}