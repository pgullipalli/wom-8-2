<cfparam name="URL.serviceID">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset serviceAttendees=CL.getServiceAttendeesByServiceID(URL.serviceID)>
<cfset numAttendees=serviceAttendees.recordcount>

<cfif numAttendees GT 0>
  <form id="frmAttendeeList">
  <input type="hidden" name="serviceID" value="#URL.serviceID#">
  
  <div class="itemList">
    <table>
      <tr>
        <th width="40">&nbsp;</th>
        <th width="250">Attendee Name</th>
		<th width="100">Pay with Cash?</th>
        <th>&nbsp;</th>
      </tr>  
      <cfoutput query="serviceAttendees">
      <tr>
        <td align="center"><input type="checkbox" name="studentServiceIDList" value="#studentServiceID#"></td>
        <td>#attendeeName#</td> 
        <td align="center"><cfif payWithCash><b>Yes</b><cfelse>No</cfif> &nbsp;&nbsp;&nbsp; <a href="javascript:changeServicePayWithCashStatus(#studentServiceID#, #payWithCash#)" class="accent01">&raquo; change</a></td>
        <td>&nbsp;</td>
      </tr>
      </cfoutput>
    </table>
  </div>
  </form>
<cfelse>
  <p>No students registered for this service.</p>
</cfif>
