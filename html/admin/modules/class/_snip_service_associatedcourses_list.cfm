<cfparam name="URL.serviceID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset relatedCourses=CL.getServiceAssociatedCourses(URL.serviceID)>

<cfif relatedCourses.recordcount Is 0>
<p>No associated courses selected for this service. Click "New" button in the action bar to select associated courses.</p>
<cfelse>
<form id="frmAssociatedCoursesList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th>Course Title</th>
  </tr>  
  <cfoutput query="relatedCourses">
  <tr>
    <td align="center"><input type="checkbox" name="courseIDList" value="#courseID#"></td>
    <td>#courseTitle#</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>