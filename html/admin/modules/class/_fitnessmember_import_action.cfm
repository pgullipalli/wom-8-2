<cfscript>
  CL=CreateObject("component", "com.ClassAdmin").init();
  Form.targetAbsoluteDirectory=Application.siteDirectoryRoot & "/assets_temp/";
  Form.localFileFieldName="localFileField";
  tempStruct=StructNew();
  tempStruct.importLog=CL.importFitnessClubMembers(argumentCollection=Form);
</cfscript>

<script type="text/javascript">
  <cfoutput>
  var importLogJson=#SerializeJSON(tempStruct)#;
  </cfoutput>
  window.parent.xsite.closeWaitingDialog();
  window.parent.displayMessage(importLogJson.IMPORTLOG);
</script>