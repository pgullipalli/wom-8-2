<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  CL=CreateObject("component", "com.ClassAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";
  
  switch (URL.task) {
    case "addCategory":
	  CL.addCategory(argumentCollection=Form);
	  break;
	  
	case "editCategory":
	  CL.editCategory(argumentCollection=Form);
	  break;
	  
	case "deleteCategory":
	  if (NOT CL.deleteCategory(URL.catID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="It appears that there are still events in the category that you tried to delete. " &
	  		 "Please make sure you have deleted all events in this category before deleting this category.";
	  }
	  break;
	case "moveCategory":
	  CL.moveCategory(categoryID=URL.categoryID, direction=URL.direction);
	  break;
	  
	case "addLocation":
	  CL.addLocation(argumentCollection=Form);
	  break;
	  
	case "editLocation":
	  CL.editLocation(argumentCollection=Form);
	  break;
	  
	case "deleteLocation":
	  if (NOT CL.deleteLocation(URL.locationID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="This location can not be deleted. It appears that there are still entities associated with this location.";
	  }
	  break;
	  
	case "verifyCampusMap":
	  CL.verifyCampusMap(argumentCollection=URL);
	  break;
	  
	case "addCourse":
	  if (Not CL.addCourse(argumentCollection=FORM)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="Please select at least one category for this new course.";
	  }
	  break;
	  
	case "editCourse":
	  if (Not CL.editCourse(argumentCollection=FORM)) {
	    returnedStruct.SUCCESS=false;
		returnedStruct.MESSAGE="Please select at least one category for this new course.";
	  }
	  break;
	  
	case "deleteCourse":
	  if (Not CL.deleteCourse(URL.courseID)) {
	    returnedStruct.SUCCESS=false;
		returnedStruct.MESSAGE="This course can not be deleted. Please delete all classes that are associated with this course before deleting this course.";
	  }
	  break;
	  
	case "addPrerequisites":
	  Form.courseID=URL.courseID;
	  CL.addPrerequisites(argumentCollection=Form);
	  break;
	  
	case "deletePrerequisites":
	  CL.deletePrerequisites(argumentCollection=URL);
	  break;
	  
	case "addAssociatedCourses":
	  Form.courseID=URL.courseID;
	  CL.addAssociatedCourses(argumentCollection=Form);
	  break;
	  
	case "deleteAssociatedCourses":
	  CL.deleteAssociatedCourses(argumentCollection=URL);
	  break;
	  
	case "addCourseReview":
	  CL.addCourseReview(argumentCollection=Form);
	  break;
	  
	case "editCourseReview":
	  CL.editCourseReview(argumentCollection=Form);
	  break;
	  
	case "deleteCourseReviews":
	  CL.deleteCourseReviews(argumentCollection=URL);
	  break;
	  
	case "addService":
	  if (Not CL.addService(argumentCollection=FORM)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="Please select at least one category for this new service.";
	  }
	  break;
	  
	case "editService":
	  if (Not CL.editService(argumentCollection=FORM)) {
	    returnedStruct.SUCCESS=false;
		returnedStruct.MESSAGE="Please select at least one category for this new service.";
	  }
	  break;
	  
	case "deleteService":
	  if (Not CL.deleteService(URL.serviceID)) {
	    returnedStruct.SUCCESS=false;
		returnedStruct.MESSAGE="This serrvice can not be deleted. It appears that there are still students registered this services.";
	  }
	  break;
	  
	case "addServiceAssociatedCourses":
	  Form.serviceID=URL.serviceID;
	  CL.addServiceAssociatedCourses(argumentCollection=FORM);
	  break;
	  
	case "addServiceReview":
	  CL.addServiceReview(argumentCollection=Form);
	  break;
	  
	case "editServiceReview":
	  CL.editServiceReview(argumentCollection=Form);
	  break;
	  
	case "deleteServiceReviews":
	  CL.deleteServiceReviews(argumentCollection=URL);
	  break;
	  
	case "deleteServiceAssociatedCourses":
	  CL.deleteServiceAssociatedCourses(argumentCollection=URL);
	  break;

	case "addClass":	  
	  CL.addClass(argumentCollection=Form);
	  break;
	  
	case "editClass":
	  if (Not CL.editClass(argumentCollection=FORM)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="New class info can not be saved. Please make sure you have entered correct info. A class must have at least one class instance.";
	  }
	  break;
	  
	case "deleteClass":  
	  if (NOT CL.deleteClass(URL.classID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="This class can not be deleted. It appears that there are students registered associated with this class.";
	  }
	  break;
	  
	case "updateAttendance":
	  CL.updateAttendance(argumentCollection=Form);
	  break;
	  
	case "updateServiceAttendance":
	  CL.updateServiceAttendance(argumentCollection=Form);
	  break;
	  
	case "deleteClassParticipants":
	  CL.deleteClassParticipants(argumentCollection=URL);
	  break;	  
	  
	case "deleteServiceParticipants":
	  CL.deleteServiceParticipants(argumentCollection=URL);
	  break;
	  
	case "changeClassPayWithCashStatus":
	  CL.changeClassPayWithCashStatus(URL.studentClassID, URL.payWithCash);
	  break;
	  
	case "changeServicePayWithCashStatus":
	  CL.changeServicePayWithCashStatus(URL.studentServiceID, URL.payWithCash);
	  break;
	  
	case "sendFeedbackRequest":
	  CL.sendFeedbackRequest(URL.classInstanceID);
	  break;
	  
	case "sendMissedClassFollowUp":
	  CL.sendMissedClassFollowUp(URL.classInstanceID);
	  break;

	case "updateIntroductionText":
	  CL.updateIntroductionText(argumentCollection=Form);
	  break;
	  
	case "addCurricula":
	  CL.addCurricula(argumentCollection=Form);
	  break;
	  
	case "editCurricula":
	  CL.editCurricula(argumentCollection=Form);
	  break;
	  
	case "deleteCurricula":
	  if (NOT CL.deleteCurricula(URL.curriculaID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="This curricula can not be deleted. It appears that there are still courses or services set up for this curricula.";
	  }
	  break;
	  
	case "addCurriculaCourses":
	  Form.curriculaID=URL.curriculaID;
	  CL.addCurriculaCourses(argumentCollection=Form);
	  break;
	  
	case "deleteCurriculaCourses":
	  CL.deleteCurriculaCourses(argumentCollection=URL);
	  break;
	  
	case "moveCurriculaCourse":
	  CL.moveCurriculaCourse(curriculaID=URL.curriculaID, courseID=URL.courseID, direction=URL.direction);
	  break;
	  
	case "addCurriculaServices":
	  Form.curriculaID=URL.curriculaID;
	  CL.addCurriculaServices(argumentCollection=Form);
	  break;
	  
	case "deleteCurriculaServices":
	  CL.deleteCurriculaServices(argumentCollection=URL);
	  break;
	  
	case "moveCurriculaService":
	  CL.moveCurriculaService(curriculaID=URL.curriculaID, serviceID=URL.serviceID, direction=URL.direction);
	  break;
	  
	case "addFitnessClass":	  
	  if (NOT CL.addFitnessClass(argumentCollection=Form)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="This class can not be added. Please specify at least a the class time.";
	  }	  
	  break;
	  
	case "editFitnessClass":
	  if (NOT CL.editFitnessClass(argumentCollection=Form)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="This class can not be added. Please specify at least a the class time.";
	  }	 
	  break;
	  
	case "deleteFitnessClass":
	  CL.deleteFitnessClass(argumentCollection=URL);
	  break;
	  
	case "updateEmailSettings":
	  CL.updateEmailSettings(argumentCollection=Form);
	  break;
	  
	case "addStudent":
	  if (Not CL.addStudent(argumentCollection=Form)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="This new user can not be added because the email address has been used by other user. ";
	  }
	  break;
	  
	case "editStudent":
	  if (Not CL.editStudent(argumentCollection=Form)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="This user's info can not updated because the email address you tried to change to has been used by other user.";
	  }
	  break;
	  
	case "deleteStudent":
	  if (Not CL.deleteStudent(studentID=URL.studentID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="This user can not be deleted because it's an active user.";
	  }
	  break;
	  
	case "addClassToSchedule":
	  Form.studentID=URL.studentID;
	  CL.addClassToSchedule(argumentCollection=Form);  
	  break;
	  
	case "addServiceToSchedule":
	  Form.studentID=URL.studentID;
	  CL.addServiceToSchedule(argumentCollection=Form);  
	  break;
	  
	case "removeClassFromSchedule":
	  CL.removeClassFromSchedule(studentID=URL.studentID, studentClassID=URL.studentClassID);
	  break;
	  
	case "removeServiceFromSchedule":
	  CL.removeServiceFromSchedule(studentID=URL.studentID, studentServiceID=URL.studentServiceID);
	  break;
	  
	case "editClassAttendeeInfo":
	  CL.editClassAttendeeInfo(argumentCollection=Form);
	  break;
	  
	case "editServiceAttendeeInfo":
	  CL.editServiceAttendeeInfo(argumentCollection=Form);
	  break;
	  
	default:
	  break;
  }
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>