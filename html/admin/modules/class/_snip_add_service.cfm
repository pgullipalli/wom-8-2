<cfparam name="URL.catID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset allCategories=CL.getAllCategories()>
<cfset allLocations=CL.getLocations()>

<form id="frmAddService" name="frmAddService" onsubmit="return false">
<div class="row">
	<div class="col-30-right"><b>Categories:</b><br /><small>(Check all that apply.)</small></div>
    <div class="col-68-left">
      <cfset numCategories=allCategories.recordcount>
      <cfset numRows=Ceiling(numCategories/2)>
      <cfoutput><cfset idx = 0>
      <cfloop index="row" from="1" to="#numRows#">
        <div class="row">
        <cfloop index="col" from="1" to="2">
          <cfset idx = (row - 1) * 2 + col>
          <cfif idx LTE numCategories>
            <cfset categoryID=allCategories.categoryID[idx]>
            <cfset categoryName=allCategories.categoryName[idx]>
            <div class="col-49-left">
              <input type="checkbox" name="categoryIDList" value="#categoryID#"<cfif URL.catID Is categoryID> checked</cfif> />
              #categoryName#
            </div>
          </cfif>
        </cfloop>
        </div>
      </cfloop>
      </cfoutput>
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Service Title:</b></div>
    <div class="col-68-left"><input type="text" name="serviceTitle" maxlength="100" value="" style="width:320px;" validate="required:true" /></div>
</div>
<div class="row">
	<div class="col-30-right"><b>GL Number:</b></div>
    <div class="col-68-left"><input type="text" name="GLNumber" maxlength="100" value="" style="width:160px;" /></div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left"><input type="checkbox" name="published" value="1" /> Publish this service</div>
</div>
<div class="row">
	<div class="col-30-right"><b>Registration Fee:</b></div>
    <div class="col-68-left">
      <div class="col-24-left">
        $<input type="text" name="feeRegular" maxlength="10" value="" style="width:80px;" validate="number:true,required:true" /><br />
        Regular
      </div>
      <div class="col-24-left">
        $<input type="text" name="feeFitnessClub" maxlength="10" value="" style="width:80px;" validate="number:true,required:true" /><br />
        Fitness Club
      </div>
      <div class="col-24-left">
        $<input type="text" name="feeEmployee" maxlength="10" value="" style="width:80px;" validate="number:true,required:true" /><br />
        Employee
      </div>
      <div class="col-24-left">
        $<input type="text" name="feeDoctor" maxlength="10" value="" style="width:80px;" validate="number:true,required:true" /><br />
        Doctor
      </div>
    </div>
</div>
<div class="row">
    <div class="col-30-right">Location:</div>
    <div class="col-68-left">
      <select name="locationID">
        <option value="0"></option>
        <cfoutput query="allLocations">
        <option value="#locationID#">#locationName#</option>
        </cfoutput>
      </select>
    </div>
</div>


<div class="row">
	<div class="col-30-right">Contact Info:</div>
    <div class="col-68-left">
      <div class="col-32-left">
        <input type="text" name="contactName" maxlength="100" value="" style="width:140px;" /><br />
        Name/Department
      </div>
      <div class="col-32-left">
        <input type="text" name="contactEmail" maxlength="100" value="" style="width:140px;" /><br />
        E-Mail
      </div>
      <div class="col-32-left">
        <input type="text" name="contactPhone" maxlength="100" value="" style="width:140px;" /><br />
        Phone
      </div>      
    </div>
</div>
<div class="row">
	<div class="col-30-right">Short Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmAddService"
            fieldName="shortDescription"
            cols="80"
            rows="3"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Short Description">
    </div>
</div>

<div class="row">
	<div class="col-30-right">Long Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="false"
            formName="frmAddService"
            fieldName="longDescription"
            cols="80"
            rows="3"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Long Description">
    </div>
</div>

<div class="row">
	<div class="col-30-right">Special Instructions:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmAddService"
            fieldName="specialInstructions"
            cols="80"
            rows="3"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Special Instructions">
    </div>
</div>
</form>