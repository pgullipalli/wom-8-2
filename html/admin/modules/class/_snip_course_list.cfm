<cfparam name="URL.catID" default="0">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>

<cfset structCourses=CL.getCoursesByCategoryID(categoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
<cfif structCourses.numAllItems IS 0>
  <p>
  It appears that there are no courses created in this category.
  Please click on the 'New' button in the action bar above to create the first course.
  </p>
</cfif>

<cfif structCourses.numDisplayedItems GT 0>
<form id="frmCourseList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th>Course Title</th>
    <th width="80">GL</th>
    <th width="70"># Classes</th>
    <th width="70"># Prereq.</th>
    <th width="70"># Assoc.</th>
    <th width="70">Reviews</th>
    <th width="60">Fee</th>    
    <th width="70">Publish?</th>
    <th width="70">Featured?</th>
    <th width="50">More...</th>
  </tr>  
  <cfoutput query="structCourses.courses">
  <tr>
    <td align="center"><input type="radio" name="courseID" value="#courseID#"></td>
	<td>
      <a href="index.cfm?md=class&tmp=classes&wrap=1&courseID=#courseID#&catID=#URL.catID#" class="accent01" title="#HTMLEditFormat(courseTitle)#">#courseTitle#</a>
    </td> 
    <td align="center">#GLNumber#&nbsp;</td>
    <td align="center">
      <a href="index.cfm?md=class&tmp=classes&wrap=1&courseID=#courseID#&catID=#URL.catID#" class="accent01">#numClasses#</a>
    </td>
    <td align="center"><a href="index.cfm?md=class&tmp=prerequisites&wrap=1&courseID=#courseID#&catID=#URL.catID#" class="accent01">#numPrerequisites#</a></td>
    <td align="center"><a href="index.cfm?md=class&tmp=associatedcourses&wrap=1&courseID=#courseID#&catID=#URL.catID#" class="accent01">#numAssociatedCourses#</a></td>
    <td align="center"><a href="index.cfm?md=class&tmp=coursereviews&wrap=1&courseID=#courseID#&catID=#URL.catID#" class="accent01">#numReviews#</a></td>
    <td align="center">#DollarFormat(feeRegular)#</td>      
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
    <td align="center"><cfif featuredOnHomepage>Yes<cfelse>No</cfif></td>
    <td align="center">
      <a class="tooltip" href="##tooltip#courseID#"><img src="images/icon_pointer.gif" width="15" border="0" /></a>
      <div id="tooltip#courseID#" style="display:none;">
        Register Online: <cfif registerOnline>Yes<cfelse>No</cfif><br />
        Participant Limit: <cfif regLimit LT 10000>#regLimit#<cfelse>--</cfif><br />
        Participant Max: <cfif regMaximum LT 10000>#regMaximum#<cfelse>--</cfif><br />
        Fee (Regular): #DollarFormat(feeRegular)#<br />
        Fee (Fitness): #DollarFormat(feeFitnessClub)#<br />
        Fee (Employee): #DollarFormat(feeEmployee)#<br />
        Fee (Doctor): #DollarFormat(feeDoctor)#<br />
        Fee per couple: <cfif feePerCouple>Yes<cfelse>No</cfif><br />
        Call for price: <cfif callForPrice>Yes<cfelse>No</cfif><br />
        Featured (Class Home): <cfif featuredOnClassHomePage>Yes<cfelse>No</cfif>                
      </div>
    </td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif structCourses.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structCourses.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadCourseList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>