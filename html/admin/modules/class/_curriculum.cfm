<script type="text/javascript" src="modules/class/curriculum.js"></script>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset introductionText=CL.getIntroductionText('curriculum')>

<div class="header">CLASS MANAGER &gt; Curriculum</div>

<br />

<form id="frmIntroductoryCopy" onSubmit="return false;">
<input type="hidden" name="sectionCode" value="curriculum" />
<strong>Introductory Copy:</strong><br />

<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgIntroductoryCopy"></span></p>
  </div>
</div>

<cfoutput><textarea name="introductionText" cols="60" rows="3">#HTMLEditFormat(introductionText)#</textarea></cfoutput>
<input type="button" value=" Update " onclick="updateIntroductionText();">
</form>

<br /><br />

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addCurricula();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editCurricula(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteCurricula();">Delete</a>
</div>

<br style="clear:both;" /><br />

<div id="mainDiv"></div>