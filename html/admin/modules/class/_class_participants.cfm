<cfparam name="URL.classID">
<cfparam name="URL.courseID">
<cfparam name="URL.catID" default="0">

<script type="text/javascript" src="modules/class/class_participants.js"></script>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset courseInfo=CL.getCourse(URL.courseID)>
<cfset allClassInstances=CL.getClassInstancesByClassID(URL.classID)>

<div class="header">CLASS MANAGER &gt; Class Participants</div>

<span id="classID" style="display:none"><cfoutput>#URL.classID#</cfoutput></span>
<span id="courseID" style="display:none"><cfoutput>#URL.courseID#</cfoutput></span>

<cfoutput>
<p><a href="index.cfm?md=class&tmp=classes&wrap=1&catID=#URL.catID#&courseID=#URL.courseID#&classID=#URL.classID#">&laquo; Back</a></p>

<p class="subHeader">Course Title: <b><i>#courseInfo.courseTitle#</i></b></p>
<cfif allClassInstances.recordcount IS 1>
  #DateFormat(allClassInstances.startDateTime,"mm/dd/yyyy")# #LCase(TimeFormat(allClassInstances.startDateTime,"hh:mm tt"))# - #LCase(TimeFormat(allClassInstances.endDateTime,"hh:mm tt"))#
<cfelse>
  <ul>
    <cfloop query="allClassInstances">
    <li>#DateFormat(startDateTime,"mm/dd/yyyy")# #LCase(TimeFormat(startDateTime,"hh:mm tt"))# - #LCase(TimeFormat(endDateTime,"hh:mm tt"))#</li>
    </cfloop>
  </ul>
</cfif>
</cfoutput>

<br style="clear:both;" />

<div id="mainDiv"></div>

<br style="clear:both;" />

<div>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="deleteParticipants();">Delete Participants</a>
</div>
