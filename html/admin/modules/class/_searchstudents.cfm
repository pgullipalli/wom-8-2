<cfparam name="URL.keyword" default="">

<script type="text/javascript" src="modules/class/searchstudents.js"></script>
<style type="text/css">.password{display:none;}</style>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>

<div class="header">CLASS MANAGER &gt; Search Users</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editStudent(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteStudent();">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="manageSchedule();">Manage Schedule</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="loginAsUser();">Login As User</a>
</div>

<br /><br /><br />

<cfoutput>
<form id="frmSearchStudents" onsubmit="return submitSearch();">
Enter a name or an e-mail address:
<input type="text" name="keyword" size="20" maxlength="30" value="#HTMLEditFormat(URL.keyword)#">
<input type="submit" value=" Submit ">
&nbsp;&nbsp;&nbsp;&nbsp;
<a href="index.cfm?md=class&tmp=students&wrap=1">Browse By User Type</a>
</form>
</cfoutput>

<p><a href="javascript:togglePassword();" class="noUnderline">&raquo; Show/Hide Passwords</a></p>

<div id="mainDiv"></div>