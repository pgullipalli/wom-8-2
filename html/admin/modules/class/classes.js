var pageNum=1;//initial page number
var courseID;
var catID;

$(function () {	
	courseID=$("#courseID").html();
	catID=$("#catID").html();
	loadClassList();
});

function loadClassList(pNum) {
  if (pNum) pageNum = pNum;
  
  var url="index.cfm?md=class&tmp=snip_class_list&courseID=" + courseID + "&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function addClass() {
  xsite.createModalDialog({
	windowName: 'winAddClass',
	title: 'Add Class',
	width: 700,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_add_class&courseID=' + courseID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {//create date pickers for date fields
	  $(".dateField").datepicker();
	},
	buttons: {
	  'Cancel': function() { $("#winAddClass").dialog('close'); },
	  ' Add ': submitForm_AddClass
	}
  }).dialog('open');
  
  function submitForm_AddClass() {
	var form = $("#frmAddClass");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=class&task=addClass',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddClass").dialog('close');
		loadClassList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editClass(clID) {
  var form = $("#frmClassList")[0];
  var classID = null;
  if (clID) classID = clID;
  else classID = xsite.getCheckedValue(form.classID);

  if (!classID) {
	alert("Please select a class to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditClass',
	title: 'Edit Class',
	width: 700,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_edit_class&courseID=' + courseID + '&classID=' + classID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {//create date pickers for date fields
	  $(".dateField").datepicker();  
	},
	buttons: {
	  'Cancel': function() { $("#winEditClass").dialog('close'); },
	  ' Save ': submitForm_EditClass
	}
  }).dialog('open');
	
  function submitForm_EditClass() {
	var form = $("#frmEditClass");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=class&task=editClass',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
		
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditClass").dialog('close');
		loadClassList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteClass() {
  var form = $("#frmClassList")[0];
  var classID = xsite.getCheckedValue(form.classID);
  if (!classID) {
	alert("Please select a class to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected class?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deleteClass&classID=' + classID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadClassList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function manageAttendance() {
  var form = $("#frmClassList")[0];
  var classID = xsite.getCheckedValue(form.classID);
  if (!classID) {
	alert("Please select a class to manage its attendance.");
	return;
  }
  
  window.location="index.cfm?md=class&tmp=class_attendance&wrap=1&classID=" + classID + "&courseID=" + courseID + "&catID=" + catID;  
}

function getNow(form, fieldName) {
  var now = new Date();
  var dateString = (now.getMonth() + 1) + "/" + now.getDate() + "/" + now.getFullYear();
  var hh=now.getHours(); 
  var mm=now.getMinutes();
  var ampm = 0;
  if (hh >= 12) ampm = 1;
  if (hh > 12) hh = hh - 12;
  
  form[fieldName].value = dateString;
  form[fieldName+'_hh'].selectedIndex = hh;
  form[fieldName+'_mm'].selectedIndex = mm - 1;
  form[fieldName+'_ampm'].selectedIndex = ampm;
}

function clearDate(form, fieldName) {
  form[fieldName].value = "";
  form[fieldName+'_hh'].selectedIndex = 0;
  form[fieldName+'_mm'].selectedIndex = 0;
  form[fieldName+'_ampm'].selectedIndex = 0;
}
