<cfparam name="URL.curriculaID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset curriculaServices=CL.getCurriculaServices(URL.curriculaID)>

<cfif curriculaServices.recordcount IS 0>
<p>No services selected for this curricula. Click "New" button in the action bar to select services.</p>
<cfelse>
<form id="frmCurrilaServiceList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="100">Up/Down</th>
	<th>Service Title</th>
    <th width="100">Publish?</th>
  </tr>  
  <cfoutput query="curriculaServices">
  <tr>
    <td align="center"><input type="checkbox" name="serviceIDList" value="#serviceID#"></td>
    <td align="center">
      <a href="##" onclick="moveItem('#serviceID#','up')"><img src="images/icon_up.gif" border="0" align="absbottom" alt="move up"></a>
      <a href="##" onclick="moveItem('#serviceID#','down')"><img src="images/icon_down.gif" border="0" align="absbottom" alt="move down"></a>
    </td>
	<td>#serviceTitle#</td> 
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>





