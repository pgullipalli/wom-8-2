var courseID;
var catID;

$(function () {	
	courseID=$("#courseID").text();
	catID=$("#catID").text();
	$(".dateField").datepicker();
});

function getReport() {
  var startDate=$("#frmDateRange input[name='startDate']").val();	
  var endDate=$("#frmDateRange input[name='endDate']").val();
  
  var url="index.cfm?md=class&tmp=snip_report_courseprofit&courseID=" + courseID + "&startDate=" + startDate  + "&endDate=" + endDate + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}