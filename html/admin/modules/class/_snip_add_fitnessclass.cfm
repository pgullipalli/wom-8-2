<cfparam name="URL.classType">

<form id="frmAddClass" name="frmAddClass" onsubmit="return false">
<div class="row">
	<div class="col-30-right"><b>Class Title:</b></div>
    <div class="col-68-left"><input type="text" name="classTitle" maxlength="100" value="" style="width:320px;" validate="required:true" /></div>
</div>
<div class="row">
	<div class="col-30-right">Class Type:</div>
    <div class="col-68-left">
      <input type="radio" name="classType" value="Aerobics" <cfif Not Compare(URL.classType,"Aerobics")>checked</cfif> /> Aerobics
      <input type="radio" name="classType" value="Gym Classes" <cfif Not Compare(URL.classType,"Gym Classes")>checked</cfif> /> Gym Classes
      <input type="radio" name="classType" value="Mind/Body" <cfif Not Compare(URL.classType,"Mind/Body")>checked</cfif> /> Mind/Body
      <input type="radio" name="classType" value="Aquatics" <cfif Not Compare(URL.classType,"Aquatics")>checked</cfif> /> Aquatics
    </div>
</div>
<div class="row">
	<div class="col-30-right">Location:</div>
    <div class="col-68-left">
      <input type="radio" name="location" value="Studio 1" <cfif Not Compare(URL.classType,"Aerobics")>checked</cfif> /> Studio 1
      <input type="radio" name="location" value="Gym" <cfif Not Compare(URL.classType,"Gym Classes")>checked</cfif> /> Gym
      <input type="radio" name="location" value="Studio 3" <cfif Not Compare(URL.classType,"Mind/Body")>checked</cfif> /> Studio 3
      <input type="radio" name="location" value="Pool" <cfif Not Compare(URL.classType,"Aquatics")>checked</cfif> /> Pool  
    </div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="published" value="1" /> Publish this class
    </div>
</div>

<div class="row">
	<div class="col-30-right">Instructor's Name:</div>
    <div class="col-68-left"><input type="text" name="instructorName" maxlength="100" value="" style="width:200px;" /></div>
</div>

<div class="row">
	<div class="col-30-right"><b>Class Schedule:</b></div>
    <div class="col-68-left">
      <cfoutput>
      <cfloop index="idx" from="1" to="7">
      <div class="row">
          <div class="row">
            <div style="width:10%;padding:0px 2px;text-align:right;float:left;"><input type="checkbox" name="dayOfWeekAsNumber_#idx#" value="1" /></div>
            <div style="width:87%;padding:0px 2px;text-align:left;float:left;"> #DayOfWeekAsString(idx)#</div>
          </div>
          <div class="row">
            <div style="width:10%;padding:0px 2px;text-align:right;float:left;">From:</div>
            <div style="width:87%;padding:0px 2px;text-align:left;float:left;">
              <select name="startTime_hh_#idx#">
                <option value="12">12</option>
                <cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
              </select>
              <select name="startTime_mm_#idx#">
                <cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
              </select>
              <select name="startTime_ampm_#idx#">
                <option value="am">AM</option><option value="pm">PM</option>
              </select> &nbsp;&nbsp;
              To:
              <select name="endTime_hh_#idx#">
                <option value="12">12</option>
                <cfloop index="hh" from="1" to="12"><option value="#hh#"><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
              </select>
              <select name="endTime_mm_#idx#">
                <cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
              </select>
              <select name="endTime_ampm_#idx#">
                <option value="am">AM</option><option value="pm">PM</option>
              </select>
            </div>
          </div>
      </div>
      </cfloop>
      </cfoutput>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Full-Size Image:</div>
    <div class="col-68-left">
      <div class="row">
        <input type="hidden" name="fullsizeImage" value="">
        <CF_SelectFile formName="frmAddClass" fieldName="fullsizeImage" previewID="preview1" previewWidth="150">
	    <div id="preview1"></div>
      </div>
      <div class="row">
        Caption:<br />
        <textarea name="imageCaption" style="width:320px;height:40px;"></textarea>
      </div>
    </div>
</div>

<!--- <div class="row">
	<div class="col-30-right">Thumbnail Image:</div>
    <div class="col-68-left">
      <input type="hidden" name="thumbnailImage" value="">
      <CF_SelectFile formName="frmAddClass" fieldName="thumbnailImage" previewID="preview2" previewWidth="100">
      <div id="preview2"></div>
    </div>
</div> --->

<div class="row">
	<div class="col-30-right">Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="false"
            formName="frmAddClass"
            fieldName="description"
            cols="55"
            rows="3"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Description">
    </div>
</div>

<div class="row">
	<div class="col-30-right">Special Instructions:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmAddClass"
            fieldName="specialInstructions"
            cols="55"
            rows="3"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Special Instructions">
    </div>
</div>


<div class="row">
	<div class="col-30-right">More Info URL:</div>
    <div class="col-68-left">
      <input type="text" name="moreInfoURL" maxlength="200" style="width:320px;" />
    </div>
</div>
</form>