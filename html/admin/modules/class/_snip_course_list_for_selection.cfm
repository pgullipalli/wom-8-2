<cfparam name="URL.catID">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>

<cfset structCourses=CL.getCoursesByCategoryID(categoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>

<cfif structCourses.numAllItems IS 0>
  <p>No courses created in this category.</p>
<cfelse>
  <cfif structCourses.numDisplayedItems GT 0>  
    <form name="frmCourseList" id="frmCourseList">
    <div class="itemList">
    <table>
      <tr>
        <th width="40">&nbsp;</th>
        <th>Course Title</th>
      </tr>  
      <cfoutput query="structCourses.courses">
      <tr>
        <td align="center"><input type="checkbox" name="courseIDList" value="#courseID#"></td>
        <td>#courseTitle#</td>
      </tr>
      </cfoutput>
    </table>
    </div>
    </form>

    
    <cfif structCourses.numAllItems GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(structCourses.numAllItems/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadCourseListForSelection(#idx#)">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
    </cfif>
  </cfif>
</cfif>

