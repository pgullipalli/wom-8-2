<cfparam name="URL.studentID">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset registeredClasses=CL.getRegisteredClassesByStudentID(URL.studentID)>
<cfset registeredServices=CL.getRegisteredServicesByStudentID(URL.studentID)>

<form id="frmClassList">
<div class="itemList">

<p class="subHeader">Registered Classes</p><br />

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="addClassToSchedule();">Add Class</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="removeClassFromSchedule();">Remove Class</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="editAttendeeInfo();">Edit Attendee Info</a>
</div>

<br style="clear:both;" /><br />

<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th>Course Title</th>
    <th>Attendee Name</th>
    <th>Guest Name(s)</th>
    <th width="100">Pre-pay/Cash</th>
    <th width="140">Phone</th>
    <th>Address</th>
    <!--- <th>Amount Paid*</th>
    <th>Discount/Coupon</th> --->
  </tr>    
  <cfif ArrayLen(registeredClasses) GT 0>
  <cfoutput>
  <cfloop index="idx" from="1" to="#ArrayLen(registeredClasses)#">
  <tr valign="top">
    <td align="center"><input type="radio" name="studentClassID" value="#registeredClasses[idx].studentClassID#"></td>
	<td style="overflow:auto;">
      <b>#registeredClasses[idx].classObj.courseTitle#</b><br />
      <cfloop index="idx2" from="1" to="#ArrayLen(registeredClasses[idx].classObj.classInstance)#">
        <cfset sDateTime=registeredClasses[idx].classObj.classInstance[idx2].startDateTime>
        <cfset eDateTime=registeredClasses[idx].classObj.classInstance[idx2].endDateTime>
        #DateFormat(sDateTime, "mm/dd/yyyy")# #LCase(TimeFormat(sDateTime,"h:mm tt"))# - #LCase(TimeFormat(eDateTime,"h:mm tt"))# (#Left(DayOfWeekAsString(registeredClasses[idx].classObj.classInstance[idx2].dayOfWeekAsNumber),3)#)<br />
      </cfloop>      
    </td>
    <td align="center">#registeredClasses[idx].attendeeName#</td>
    <td align="center">#registeredClasses[idx].guestNames#&nbsp;</td>
    <td align="center"><cfif registeredClasses[idx].payWithCash><b>Yes</b><cfelse>No</cfif></td>
    <td>
      <cfif Compare(registeredClasses[idx].daytimePhone,"")>#registeredClasses[idx].daytimePhone# (daytime)</cfif>
      <cfif Compare(registeredClasses[idx].cellPhone,"")><cfif Compare(registeredClasses[idx].daytimePhone,"")><br /></cfif>#registeredClasses[idx].cellPhone# (cell)</cfif>
    </td>
    <td>
      <cfif Compare(registeredClasses[idx].address1,"")>
        #registeredClasses[idx].address1#<br />
		<cfif Compare(registeredClasses[idx].address2,"")>#registeredClasses[idx].address2#<br /></cfif>
      </cfif>
      <cfif Compare(registeredClasses[idx].city,"")>#registeredClasses[idx].city#, </cfif>
      <cfif Compare(registeredClasses[idx].state,"")>#registeredClasses[idx].state# #registeredClasses[idx].zip#</cfif>&nbsp;
    </td>
    <!--- <td align="center">#DollarFormat(registeredClasses[idx].amountPaid)#</td>
    <td align="center">
      <cfif registeredClasses[idx].discountAmount GT 0>
      #DollarFormat(registeredClasses[idx].discountAmount)#/#registeredClasses[idx].discountCode#
      <cfelse>
      &nbsp;
      </cfif>
    </td> --->
  </tr>
  </cfloop>
  </cfoutput>
  <cfelse>
  <tr><td colspan="7" align="center">This user is not registered in any classes.</td></tr>
  </cfif>
</table>

<p>&nbsp;</p><br />


<p class="subHeader">Registered Services</p><br />

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="addServiceToSchedule();">Add Service</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="removeServiceFromSchedule();">Remove Service</a>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="editAttendeeInfo();">Edit Attendee Info</a>
</div>

<br style="clear:both;" /><br />

<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th style="overflow:auto;">Services Title</th>
    <th>Attendee Name</th>
    <th>Guest Name(s)</th>
    <th width="100">Pay with Cash?</th>
    <th width="140">Phone</th>
    <th>Address</th>
    <!--- <th>Amount Paid*</th>
    <th>Discount/Coupon</th> --->
  </tr>    
  <cfif registeredServices.recordcount GT 0>
  <cfoutput query="registeredServices">
  <tr valign="top">
    <td align="center"><input type="radio" name="studentServiceID" value="#studentServiceID#"></td>
	<td><b>#serviceTitle#</b></td>
    <td align="center">#attendeeName#</td>
    <td align="center">#guestNames#&nbsp;</td>
    <td align="center"><cfif payWithCash><b>Yes</b><cfelse>No</cfif></td>
    <td>
      <cfif Compare(daytimePhone,"")>#daytimePhone# (daytime)</cfif>
      <cfif Compare(cellPhone,"")><cfif Compare(daytimePhone,"")><br /></cfif>#cellPhone# (cell)</cfif>
    </td>
    <td>
      <cfif Compare(address1,"")>
        #address1#<br /><cfif Compare(address2,"")>#address2#<br /></cfif>
      </cfif>
      <cfif Compare(city,"")>#city#, </cfif>
      <cfif Compare(state,"")>#state# #zip#</cfif>&nbsp;
    </td>
    <!--- <td align="center">#DollarFormat(amountPaid)#</td>
    <td align="center">
      <cfif discountAmount GT 0>
      #DollarFormat(discountAmount)#/#discountCode#
      <cfelse>
      &nbsp;
      </cfif>
    </td> --->
  </tr>
  </cfoutput>
  <cfelse>
  <tr><td colspan="7" align="center">This user is not registered in any services.</td></tr>
  </cfif>
</table>
</div>
</form>

<!--- <p>&nbsp;</p>
<p>* Amout paid (first payment if enrolled in payment plan) during online class registration</p> --->

<br style="clear:both;" /><br />
