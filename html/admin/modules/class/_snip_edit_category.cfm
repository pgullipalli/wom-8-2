<cfparam name="URL.catID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset category=CL.getCategory(URL.catID)>

<cfoutput query="category">
<div class="snip">
<form id="frmEditCategory" name="frmEditCategory" onsubmit="return false">
<input type="hidden" name="categoryID" value="#URL.catID#" />
<div class="row">
	<div class="col-30-right"><b>Category Name:</b></div>
    <div class="col-68-left">
      <input type="text" name="categoryName" style="width:250px;" maxlength="50" value="#HTMLEditFormat(categoryName)#" validate="required:true" /><br />
      <input type="checkbox" name="featured" value="1" <cfif featured>checked</cfif> /> Featured (display in category list)
    </div>
</div>
<div class="row">
	<div class="col-30-right">Description:</div>
	<div class="col-68-left">
      <textarea name="description" style="width:250px;height:70px;">#HTMLEditFormat(description)#</textarea>
    </div>
</div>
</form>
</div>
</cfoutput>