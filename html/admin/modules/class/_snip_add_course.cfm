<cfparam name="URL.catID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset allCategories=CL.getAllCategories()>

<form id="frmAddCourse" name="frmAddCourse" onsubmit="return false">
<div class="row">
	<div class="col-30-right"><b>Categories:</b><br /><small>(Check all that apply.)</small></div>
    <div class="col-68-left">
      <cfset numCategories=allCategories.recordcount>
      <cfset numRows=Ceiling(numCategories/2)>
      <cfoutput><cfset idx = 0>
      <cfloop index="row" from="1" to="#numRows#">
        <div class="row">
        <cfloop index="col" from="1" to="2">
          <cfset idx = (row - 1) * 2 + col>
          <cfif idx LTE numCategories>
            <cfset categoryID=allCategories.categoryID[idx]>
            <cfset categoryName=allCategories.categoryName[idx]>
            <div class="col-49-left">
              <input type="checkbox" name="categoryIDList" value="#categoryID#"<cfif URL.catID Is categoryID> checked</cfif> />
              #categoryName#
            </div>
          </cfif>
        </cfloop>
        </div>
      </cfloop>
      </cfoutput>
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Course Title:</b></div>
    <div class="col-68-left"><input type="text" name="courseTitle" maxlength="100" value="" style="width:320px;" validate="required:true" /></div>
</div>
<div class="row">
	<div class="col-30-right"><b>GL Number:</b></div>
    <div class="col-68-left"><input type="text" name="GLNumber" maxlength="100" value="" style="width:160px;" /></div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="published" value="1" /> Publish this course<br />
      <input type="checkbox" name="featuredOnHomepage" value="1" /> Featured on home/sub-home page<br />
      <input type="checkbox" name="registerOnline" value="1" checked /> Register online<br />
      <input type="checkbox" name="fitnessClubOnly" value="1" /> Fitness Club Member Only<br />
      <input type="checkbox" name="featuredOnClassHomePage" value="1" /> Featured on class landing page
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Registration Fee:</b></div>
    <div class="col-68-left">
      <div class="col-24-left">
        $<input type="text" name="feeRegular" maxlength="10" value="" style="width:80px;" validate="number:true,required:true" /><br />
        Regular
      </div>
      <div class="col-24-left">
        $<input type="text" name="feeFitnessClub" maxlength="10" value="" style="width:80px;" validate="number:true,required:true" /><br />
        Fitness Club
      </div>
      <div class="col-24-left">
        $<input type="text" name="feeEmployee" maxlength="10" value="" style="width:80px;" validate="number:true,required:true" /><br />
        Employee
      </div>
      <div class="col-24-left">
        $<input type="text" name="feeDoctor" maxlength="10" value="" style="width:80px;" validate="number:true,required:true" /><br />
        Doctor
      </div>
      <div class="row">
      <input type="checkbox" name="callForPrice" value="1" /> Call for pricing<br />
      <input type="checkbox" name="feePerCouple" value="1" /> Fee per couple
      (Attendee <input type="text" name="attendeeNameLabel" maxlength="100" value="" style="width:80px;" />
      Guest <input type="text" name="guestNameLabel" maxlength="100" value="" style="width:80px;" />)
      </div>
    </div>
</div>
<div class="row">
	<div class="col-30-right">Registration Participant Limit:</div>
    <div class="col-68-left">
      <div class="col-24-left">
        <input type="text" name="regLimit" maxlength="3" value="" style="width:80px;" validate="number:true" /><br />
        Limit
      </div>
      <div class="col-24-left">
        <input type="text" name="regMaximum" maxlength="3" value="" style="width:80px;" validate="number:true" /><br />
        Maximum
      </div>
    </div>
</div>
<div class="row">
	<div class="col-30-right">Promotion:</div>
    <div class="col-68-left">
      <div class="col-68-left">
        <input type="text" name="discountCode" maxlength="20" value="" style="width:80px;" />        
        <input type="radio" name="discountType" value="flat rate" checked /> Flat Rate
        <input type="radio" name="discountType" value="percentage" /> Percentage<br />
        Code
      </div>
      <div class="col-24-left">
        <input type="text" name="discountRate" maxlength="10" value="" style="width:80px;" validate="number:true" /><br />
        Rate (% or $)
      </div>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Department:</div>
    <div class="col-68-left">
      <input type="hidden" name="directoryItemID" value="" />
      <CF_SelectDirectoryItem formName="frmAddCourse" fieldName="directoryItemID" previewID="preview1"> <span id="preview1" style="font-weight:bold;font-style:italic;"></span>
    </div>
</div>
<div class="row">
	<div class="col-30-right">Contact Info:</div>
    <div class="col-68-left">
      <div class="col-32-left">
        <input type="text" name="contactName" maxlength="100" value="" style="width:140px;" /><br />
        Name
      </div>
      <div class="col-32-left">
        <input type="text" name="contactEmail" maxlength="100" value="" style="width:140px;" /><br />
        E-Mail
      </div>
      <div class="col-32-left">
        <input type="text" name="contactPhone" maxlength="100" value="" style="width:140px;" /><br />
        Phone
      </div>      
    </div>
</div>

<div class="row">
	<div class="col-30-right">Course Image:</div>
    <div class="col-68-left">
      <input type="hidden" name="courseImage" value="" />
      <CF_SelectFile formName="frmAddCourse" fieldName="courseImage" previewID="preview10" previewWidth="100">
      <small>(148px x 148px)</small>
      <div id="preview10"></div>
      <div class="row">
      Caption: <input type="text" name="courseImageCaption" maxlength="200" style="width:350px;" value="" />
      </div>
    </div>
</div>
<div class="row">
	<div class="col-30-right">Course Thumbnail:</div>
    <div class="col-68-left">
      <input type="hidden" name="courseThumbnail" value="" />
      <CF_SelectFile formName="frmAddCourse" fieldName="courseThumbnail" previewID="preview11" previewWidth="100">
      <small>(211px � 75px)</small>
      <div id="preview11"></div>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Short Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmAddCourse"
            fieldName="shortDescription"
            cols="80"
            rows="3"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Short Description">
    </div>
</div>

<div class="row">
	<div class="col-30-right">Long Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="false"
            formName="frmAddCourse"
            fieldName="longDescription"
            cols="80"
            rows="3"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Long Description">
    </div>
</div>

<div class="row">
	<div class="col-30-right">Special Instructions:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmAddCourse"
            fieldName="specialInstructions"
            cols="80"
            rows="3"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Special Instructions">
    </div>
</div>

<!--- <div class="row">
	<div class="col-30-right">Registration Participant Limit:</div>
    <div class="col-68-left">
      <div class="col-24-left">
        <input type="text" name="regLimit" maxlength="3" value="" style="width:80px;" validate="number:true" /><br />
        Limit
      </div>
      <div class="col-24-left">
        <input type="text" name="regMaximum" maxlength="3" value="" style="width:80px;" validate="number:true" /><br />
        Maximum
      </div>
    </div>
</div> --->

<div class="row">
	<div class="col-30-right">Registration URL:</div>
    <div class="col-68-left">
      <input type="text" name="registrationURL" maxlength="200" style="width:320px;" /><br />
      (if controlled ouside of registration system)
    </div>
</div>
<div class="row">
	<div class="col-30-right">Info URL (external):</div>
    <div class="col-68-left">
      <input type="text" name="outsideEventURL" maxlength="200" style="width:320px;" />
    </div>
</div>
<div class="row">
	<div class="col-30-right">More Info:</div>
    <div class="col-68-left">
      <input type="hidden" name="pageID" value="">
      <CF_SelectPage formName="frmAddCourse" fieldName="pageID" previewID="preview2"> <span id="preview2" style="font-weight:bold;font-style:italic;"></span><br />
      (link to a Page Builder page)
    </div>
</div>
<div class="row">
	<div class="col-30-right">Send E-Mail?</div>
    <div class="col-68-left">
      <div class="col-32-left">
      Class Reminder<br />
     <input type="radio" name="sendReminderEmail" value="1" /> Yes
      <input type="radio" name="sendReminderEmail" value="0" checked /> No
      </div>
      <div class="col-32-left">
      Follow-Up Feedback<br />
      <input type="radio" name="sendFollowUpEmailFeedback" value="1" /> Yes
      <input type="radio" name="sendFollowUpEmailFeedback" value="0" checked /> No
      </div>
      <div class="col-32-left">
      Missed Class Follow-Up<br />
      <input type="radio" name="FollowUpEmailMissedClass" value="1" /> Yes
      <input type="radio" name="FollowUpEmailMissedClass" value="0" checked /> No
      </div>      
    </div>
</div>


</form>