$(function () {
	loadLocationList();
});


function loadLocationList() {
  xsite.load("#mainDiv", "index.cfm?md=class&tmp=snip_location_list&uuid=" + xsite.getUUID());
}

/*--------------------------------*/
/*    BEGIN: Add Location         */
/*--------------------------------*/
function addLocation() {
  xsite.createModalDialog({
	windowName: 'winAddLocation',
	title: 'Add Location',
	position: 'center-up',
	width: 500,
	url: 'index.cfm?md=class&tmp=snip_add_location&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddLocation").dialog('close'); },
	  ' Add ': submitForm_AddLocation
	}
  }).dialog('open');
  
  function submitForm_AddLocation() {
	var form = $("#frmAddLocation");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=class&task=addLocation',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddLocation").dialog('close');
		loadLocationList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------*/
/*    END: Add Location           */
/*--------------------------------*/

/*--------------------------------*/
/*    BEGIN: Edit Location        */
/*--------------------------------*/
function editLocation(lID) {
  var form = $("#frmLocationList")[0];
  var locationID;
  if (lID) locationID=lID;
  else locationID = xsite.getCheckedValue(form.locationID);
  if (!locationID) {
	alert("Please select a location to edit.");
	return;
  }
  xsite.createModalDialog({
	windowName: 'winEditLocation',
	title: 'Edit Location',
	position: 'center-up',
	width: 500,
	url: 'index.cfm?md=class&tmp=snip_edit_location&locationID=' + locationID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditLocation").dialog('close'); },
	  ' Save ': submitForm_EditLocation
	}
  }).dialog('open');
  
  function submitForm_EditLocation() {
	var form = $("#frmEditLocation");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=class&task=editLocation',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
	  return;	
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditLocation").dialog('close');
		loadLocationList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}	  
  }
}
/*--------------------------------*/
/*    END: Edit Location          */
/*--------------------------------*/


/*--------------------------------*/
/*  BEGIN: Delete Location        */
/*--------------------------------*/
function deleteLocation() {
  var form = $("#frmLocationList")[0];
  var locationID = xsite.getCheckedValue(form.locationID);
  if (!locationID) {
	alert("Please select a location to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected location?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deleteLocation&locationID=' + locationID, {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadLocationList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }
}
/*--------------------------------*/
/*  END: Delete Location          */
/*--------------------------------*/