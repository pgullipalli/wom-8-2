<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset curriculum=CL.getCurriculum()>

<cfif curriculum.recordcount IS 0>
  <p>
  There are no curriculum created. Please click on the 'New' button in the action bar above to create the first curricula.
  </p>
<cfelse>
<form id="frmCurriculaList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th>Curricula Title</th>   
    <th># Courses</th>
    <th># Services</th>
    <th width="100">Publish?</th>
  </tr>  
  <cfoutput query="curriculum">
  <tr>
    <td align="center"><input type="radio" name="curriculaID" value="#curriculaID#"></td>
	<td>
      <a href="javascript:editCurricula('#curriculaID#')" class="accent01">#curriculaTitle#</a>
    </td> 
    <td align="center">
      <a href="index.cfm?md=class&tmp=curricula_courses&wrap=1&curriculaID=#curriculaID#" class="accent01">#numCourses#</a>
    </td>
    <td align="center">
      <a href="index.cfm?md=class&tmp=curricula_services&wrap=1&curriculaID=#curriculaID#" class="accent01">#numServices#</a>
    </td>
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>