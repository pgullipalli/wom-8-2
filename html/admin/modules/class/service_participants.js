var serviceID;

$(function () {	
	serviceID=$("#serviceID").html();
	loadParticipantList();
});

function loadParticipantList() {  
  var url="index.cfm?md=class&tmp=snip_service_participant_list&serviceID=" + serviceID + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function deleteParticipants() {
  var form=$("#frmAttendeeList")[0];
  
  if (!(form.studentServiceIDList && xsite.getCheckedValue(form.studentServiceIDList))) {
	alert ("Please check off the participants you'd like to delete.");
	return;
  }
	
  if (!confirm('Are you sure you want to permantly delete the selected participants that have registered with this service?')) {
	return;
  } else {		
	//show waiting dialog
	var studentServiceIDList = xsite.getCheckedValue(form.studentServiceIDList);	
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deleteServiceParticipants&serviceID=' + serviceID + '&studentServiceIDList=' + escape(studentServiceIDList) + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadParticipantList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }		
}

function changeServicePayWithCashStatus(studentServiceID, payWithCash) {
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=changeServicePayWithCashStatus&studentServiceID=' + studentServiceID + '&payWithCash=' + payWithCash + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadParticipantList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }		
}