<cfparam name="URL.courseReviewID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset courseReview=CL.getCourseReview(URL.courseReviewID)>

<cfoutput query="courseReview">
<form id="frmEditCourseReview" name="frmEditCourseReview" onsubmit="return false;">
<input type="hidden" name="courseReviewID" value="#URL.courseReviewID#">
<div class="row">
	<div class="col-30-right"><b>Name:</b></div>
    <div class="col-68-left"><input type="text" name="fullName" maxlength="100" style="width:320px;" value="#HTMLEditFormat(fullName)#" /></div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left"><input type="checkbox" name="published" value="1" <cfif published>checked</cfif> /> Publish this review</div>
</div>
<div class="row">
    <div class="col-30-right"><b>Review:</b></div>
    <div class="col-68-left">
      <textarea name="review" style="width:320px;height:100px;">#HTMLEditFormat(review)#</textarea>
    </div>
</div>
</form>
</cfoutput>