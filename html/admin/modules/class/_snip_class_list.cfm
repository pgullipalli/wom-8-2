<cfparam name="URL.courseID">
<cfparam name="URL.catID">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="100">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset structClasses=CL.getClassesByCourseID(courseID=URL.courseID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
<cfset courseInfo=CL.getCourse(URL.courseID)>

<cfif structClasses.numAllItems Is 0>
<p>No classes have been created for this course. Please click 'New' in the action bar to create the first class.</p>
<cfelse>

<cfset numTotalParticipants=0>
<cfset numTotalAttendedStudents=0>
<cfset numTotalRegLimit=0>
<cfset numTotalRegMaximum=0>
<cfset totalMoneyCollected=0>

<form id="frmClassList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th width="250">Date/Time</th>
    <th># Participants</th>
    <th># Attended</th>
    <th># Part. Limit</th>
    <th># Part. Max.</th>
    <th>Publish?</th>
    <th>Publish Date</th>
	<th>Unpublish Date</th>
    <th>$ Collected</th>
  </tr>
  <cfset classIDTemp="0">
  <cfset tableRowHTMLPart1=StructNew()>
  <cfset tableRowHTMLPart2=StructNew()>
  <cfloop query="structClasses.classInstances">
    <cfif Compare(classIDTemp, classID)><!--- new class --->
  	  <cfsavecontent variable="htmlCodes">  
      <cfoutput>    
      <tr>
        <td align="center"><input type="radio" name="classID" value="#classID#"></td>
        <td>$$</td>
        </td>
	    <td align="center">
          <cfif numParticipants GT 0>
            <a href="index.cfm?md=class&tmp=class_participants&wrap=1&classID=#classID#&courseID=#URL.courseID#&catID=#URL.catID#" class="accent01">#numParticipants#</a>
            <cfset numTotalParticipants=numTotalParticipants + numParticipants>
          <cfelse>
            0
          </cfif>
        </td>
        <td align="center">#numAttendedStudents#<cfset numTotalAttendedStudents=numTotalAttendedStudents + numAttendedStudents></td>
        <td align="center"><cfif regLimit LT 10000>#regLimit#<cfelse>--</cfif><cfset numTotalRegLimit=numTotalRegLimit + regLimit></td>
        <td align="center"><cfif regMaximum LT 10000>#regMaximum#<cfelse>--</cfif><cfset numTotalRegMaximum=numTotalRegMaximum + regMaximum></td>
        
        <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
	    <td align="center">
	      <cfif published>#DateFormat(publishDate, "mm/dd/yyyy")# #TimeFormat(publishDate, "h:mm tt")#<cfelse>--</cfif>
        </td>
        <td align="center">
	    <cfif Not published OR Year(unpublishDate) IS "3000">--<cfelse>#DateFormat(unpublishDate, "mm/dd/yyyy")# #TimeFormat(unpublishDate, "h:mm tt")#</cfif>
        </td>
        <td align="center"><cfif IsNumeric(moneyCollected)>#DollarFormat(moneyCollected)#<cfset totalMoneyCollected=totalMoneyCollected + moneyCollected><cfelse>$0.00</cfif></td>
      </tr>
      </cfoutput>
      </cfsavecontent>
      <cfset tableRowHTMLPart1["#classID#"]=htmlCodes>
      <cfset tableRowHTMLPart2["#classID#"]="">
    </cfif>
    
    <cfset tableRowHTMLPart2["#classID#"] =
		   tableRowHTMLPart2["#classID#"] &
		   "<a href=""javascript:editClass('#classID#')"">#DateFormat(startDateTime,"mm/dd/yyyy")# #LCase(TimeFormat(startDateTime,"h:mm tt"))# - #LCase(TimeFormat(endDateTime,"h:mm tt"))# (#Left(DayOfWeekAsString(dayOfWeekAsNumber),3)#)</a><br />">
    
    <cfset classIDTemp=classID>
  </cfloop>
  
  <cfset classIDTemp="0">
  <cfoutput query="structClasses.classInstances">
	<cfif Compare(classIDTemp, classID)><!--- new class --->
      #Replace(tableRowHTMLPart1["#classID#"],"$$",tableRowHTMLPart2["#classID#"])#
    </cfif>
	<cfset classIDTemp=classID>
  </cfoutput>
  <cfoutput>
  <tr>
    <td colspan="2" align="right"><b>Totals</b></td>
    <td align="center"><b>#numTotalParticipants#</b></td>
    <td align="center"><b>#numTotalAttendedStudents#</b></td>
    <td align="center"><b><cfif numTotalRegLimit LT 10000>#numTotalRegLimit#<cfelse>--</cfif></b></td>
    <td align="center"><b><cfif numTotalRegMaximum LT 10000>#numTotalRegMaximum#<cfelse>--</cfif></b></td>
    <td colspan="3">&nbsp;</td>
    <td align="center"><b>#DollarFormat(totalMoneyCollected)#</b></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif structClasses.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structClasses.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadClassList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>