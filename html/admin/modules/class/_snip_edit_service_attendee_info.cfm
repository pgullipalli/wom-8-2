<cfparam name="URL.studentServiceID">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset studentServiceInfo=CL.getStudentServiceInfo(URL.studentServiceID)>

<cfoutput query="studentServiceInfo">
<form id="frmEditServiceAttendeeInfo" name="frmEditServiceAttendeeInfo" onsubmit="return false">
<input type="hidden" name="studentServiceID" value="#URL.studentServiceID#" />
<div class="row">
	<div class="col-30-right"><b>Attendee Name:</b></div>
    <div class="col-68-left"><input type="text" name="attendeeName" maxlength="100" style="width:250px;" value="#HTMLEditFormat(attendeeName)#" validate="required:true"></div>
</div>
<div class="row">
	<div class="col-30-right">Guest Names:</div>
    <div class="col-68-left"><input type="text" name="guestNames" maxlength="200" style="width:250px;" value="#HTMLEditFormat(guestNames)#"></div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="payWithCash" value="1" <cfif payWithCash>checked</cfif> /></div>
    <div class="col-68-left">Paywith cash</div>
</div>

<div class="row">
	<div class="col-30-right">Daytime Phone:</div>
    <div class="col-68-left"><input type="text" name="daytimePhone" maxlength="50" style="width:250px;" value="#HTMLEditFormat(daytimePhone)#"></div>
</div>
<div class="row">
	<div class="col-30-right">Cell Phone:</div>
    <div class="col-68-left"><input type="text" name="cellPhone" maxlength="50" style="width:250px;" value="#HTMLEditFormat(cellPhone)#"></div>
</div>

<div class="row">
	<div class="col-30-right">Address:</div>
    <div class="col-68-left">
      <input type="text" name="address1" maxlength="100" style="width:250px;" value="#HTMLEditFormat(address1)#"><br />
	  <input type="text" name="address2" maxlength="100" style="width:250px;" value="#HTMLEditFormat(address2)#">      
    </div>
</div>

<div class="row">
	<div class="col-30-right">City, State ZIP:</div>
    <div class="col-68-left">
      <input type="text" name="city" maxlength="50" style="width:100px;" value="#HTMLEditFormat(city)#">,
	  <input type="text" name="state" maxlength="30" style="width:60px;" value="#HTMLEditFormat(state)#">
      <input type="text" name="zip" maxlength="10" style="width:60px;" value="#HTMLEditFormat(zip)#">
    </div>
</div>
</form>
</cfoutput>