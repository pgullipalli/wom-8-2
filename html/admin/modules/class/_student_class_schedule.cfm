<cfparam name="URL.studentID">
<cfparam name="URL.browseBy">
<cfparam name="URL.studentType" default="R">
<cfparam name="URL.pageNum" default="1">

<script type="text/javascript" src="modules/class/student_class_schedule.js"></script>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset studentInfo=CL.getStudent(URL.studentID)>

<span id="studentID" style="display:none;"><cfoutput>#URL.studentID#</cfoutput></span>

<div class="header">CLASS MANAGER &gt; Class Schedule</div>

<cfoutput>
<cfif URL.browseBy Is "type">
<p><a href="index.cfm?md=class&tmp=students&wrap=1&studentType=#URL.studentType#&pageNum=#URL.pageNum#">&laquo; Back</a></p>
<cfelse>
<p><a href="index.cfm?md=class&tmp=searchstudents&wrap=1&keyword=#URLEncodedFormat(URL.keyword)#&pageNum=#URL.pageNum#">&laquo; Back</a></p>
</cfif>

<p class="subHeader">Registrant: <b><i>#studentInfo.email#</i></b></p>
</cfoutput>

<br style="clear:both;" />

<div id="mainDiv"></div>