<cfparam name="URL.curriculaID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset curriculaInfo=CL.getCurricula(URL.curriculaID)>

<cfoutput query="curriculaInfo">
<form id="frmEditCurricula" name="frmEditCurricula" onsubmit="return false">
<input type="hidden" name="curriculaID" value="#URL.curriculaID#" />
<div class="row">
	<div class="col-30-right"><b>Curricula Title:</b></div>
    <div class="col-68-left"><input type="text" name="curriculaTitle" maxlength="100" value="#HTMLEditFormat(curriculaTitle)#" style="width:320px;" validate="required:true" /></div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left"><input type="checkbox" name="published" value="1" <cfif published>checked</cfif> /> Publish this curricula</div>
</div>

<div class="row">
	<div class="col-30-right">Short Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmEditCurricula"
            fieldName="shortDescription"
            cols="80"
            rows="3"
            HTMLContent="#shortDescription#"
            displayHTMLSource="true"
            editorHeader="Edit Short Description">
    </div>
</div>

<div class="row">
	<div class="col-30-right">Long Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="false"
            formName="frmEditCurricula"
            fieldName="longDescription"
            cols="80"
            rows="3"
            HTMLContent="#longDescription#"
            displayHTMLSource="true"
            editorHeader="Edit Long Description">
    </div>
</div>
</form>
</cfoutput>