var curriculaID;

$(function () {	
	curriculaID=$("#curriculaID").html();
	
	loadServiceList();	
});

function loadServiceList() {  
  var url="index.cfm?md=class&tmp=snip_curriculaservice_list&curriculaID=" + curriculaID + "&uuid=" + xsite.getUUID();
  xsite.load("#mainDiv", url);
}

function addCurriculaServices() {
  xsite.createModalDialog({
	windowName: 'winAddCurriculaServices',
	title: 'Add Services',
	width: 600,
	height:500,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_add_relatedservices&uuid=' + xsite.getUUID(),
	urlCallback: function () {
	  $("#frmNavigator").unbind();
	  $("#frmNavigator select[name='categoryID']").change(function() {
		loadServiceListForSelection(1);	  
	  });
	  loadServiceListForSelection(1);
	},
	buttons: {
	  'Cancel': function() { $("#winAddCurriculaServices").dialog('close'); },
	  ' Add ': submitForm_AddCurriculaServices
	}
  }).dialog('open');

}

function loadServiceListForSelection(pageNum) {
  var catID=$("#frmNavigator select[name='categoryID']").val();
  var url="index.cfm?md=class&tmp=snip_service_list_for_selection&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();
  xsite.load("#subDiv1", url);
}

function submitForm_AddCurriculaServices() {
  var form = $("#frmServiceList");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=class&task=addCurriculaServices&curriculaID=' + curriculaID,
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winAddCurriculaServices").dialog('close');
	  loadServiceList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }
}

function deleteCurriculaServices() {
  if(!document.getElementById('frmCurrilaServiceList')) return;
  var form = $("#frmCurrilaServiceList")[0];
  var serviceIDList = xsite.getCheckedValue(form.serviceIDList);
  if (!serviceIDList) {
	alert("Please select at least an item to remove.");
	return;	  
  }
  if (!confirm('Are you sure you want to remove the selected items?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deleteCurriculaServices&curriculaID=' + curriculaID + '&serviceIDList=' + escape(serviceIDList) + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  loadServiceList();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function moveItem(serviceID, direction) {
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=moveCurriculaService&curriculaID=' + curriculaID + '&serviceID=' + serviceID + '&direction=' + direction + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  loadServiceList();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

