<cfparam name="URL.serviceID">
<cfparam name="URL.catID" default="0">

<script type="text/javascript" src="modules/class/service_participants.js"></script>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset serviceInfo=CL.getService(URL.serviceID)>

<div class="header">CLASS MANAGER &gt; Service Participants</div>

<span id="serviceID" style="display:none"><cfoutput>#URL.serviceID#</cfoutput></span>

<cfoutput>
<p><a href="index.cfm?md=class&tmp=services&wrap=1&catID=#URL.catID#">&laquo; Back</a></p>

<p class="subHeader">Service Title: <b><i>#serviceInfo.serviceTitle#</i></b></p>
</cfoutput>

<br style="clear:both;" />

<div id="mainDiv"></div>

<br style="clear:both;" />

<div>
<a href="#" class="fg-button ui-state-default ui-corner-all" onclick="deleteParticipants();">Delete Participants</a>
</div>
