<cfparam name="URL.catID">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="10">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>

<cfset structCourses=CL.getCoursesByCategoryID(categoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>

<cfif structCourses.numAllItems IS 0>
  <p>No courses in this category.</p>
<cfelse>
  <cfif structCourses.numDisplayedItems GT 0>  
    <form name="frmCourseList" id="frmCourseList">
    <div class="itemList">
    <table>
      <tr>
        <th>Course</th>
      </tr>  
      <cfoutput query="structCourses.courses">
        <tr>
          <td>
            <a href="javascript:xsite.toggle('classList#courseID#')"><b>#courseTitle#</b></a> -
            #DollarFormat(feeRegular)#<!--- (R)/#DollarFormat(feeFitnessClub)#(F)/#DollarFormat(feeEmployee)#(E)/#DollarFormat(feeDoctor)#(D) ---><br />
            <cfset classDateTime=CL.getAllClassStartDateTimeCourseID(courseID)>        
            <div id="classList#courseID#" style="display:none;">
            <cfloop query="classDateTime"> 
            <input type="radio" name="classID" value="#classID#" />
            #DayOfWeekAsString(DayOfWeek(firstStartDateTime))#, #DateFormat(firstStartDateTime,"mm/dd/yyyy")# #Lcase(TimeFormat(firstStartDateTime,"h:mm tt"))#<br />
            </cfloop>
            </div>
          </td>
        </tr>
      </cfoutput>
    </table>
    </div>
    </form>

    
    <cfif structCourses.numAllItems GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(structCourses.numAllItems/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadClassListForSelection(#idx#)">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
    </cfif>
  </cfif>
</cfif>

