var studentID;

$(function () {
	studentID=$("#studentID").text();
	
	loadStudentClassList();
});

function loadStudentClassList() {  
  var url="index.cfm?md=class&tmp=snip_student_class_list&studentID=" + studentID + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function addClassToSchedule() {
  xsite.createModalDialog({
	windowName: 'winAddClassToSchedule',
	title: 'Add Class To6 Schedule',
	width: 600,
	height:500,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_add_class_to_schedule&uuid=' + xsite.getUUID(),
	urlCallback: function () {
	  $("#frmNavigator2").unbind();
	  $("#frmNavigator2 select[name='categoryID']").change(function() {
		loadClassListForSelection(1);	  
	  });
	  loadClassListForSelection(1);
	},
	buttons: {
	  'Cancel': function() { $("#winAddClassToSchedule").dialog('close'); },
	  ' Add ': submitForm_AddClassToSchedule
	}
  }).dialog('open');
}

function loadClassListForSelection(pageNum) {
  var catID=$("#frmNavigator2 select[name='categoryID']").val();
  var url="index.cfm?md=class&tmp=snip_class_list_for_selection&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();
  xsite.load("#subDiv2", url);
}

function submitForm_AddClassToSchedule() {
  var form = $("#frmCourseList");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=class&task=addClassToSchedule&studentID=' + studentID,
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winAddClassToSchedule").dialog('close');
	  loadStudentClassList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }
}

function addServiceToSchedule() {
  xsite.createModalDialog({
	windowName: 'winAddServiceToSchedule',
	title: 'Add Service To Schedule',
	width: 600,
	height:500,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_add_service_to_schedule&uuid=' + xsite.getUUID(),
	urlCallback: function () {
	  $("#frmNavigator").unbind();
	  $("#frmNavigator select[name='categoryID']").change(function() {
		loadServiceListForSelection(1);	  
	  });
	  loadServiceListForSelection(1);
	},
	buttons: {
	  'Cancel': function() { $("#winAddServiceToSchedule").dialog('close'); },
	  ' Add ': submitForm_AddServiceToSchedule
	}
  }).dialog('open');
}

function loadServiceListForSelection(pageNum) {
  var catID=$("#frmNavigator select[name='categoryID']").val();
  var url="index.cfm?md=class&tmp=snip_service_list_for_selection&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();
  xsite.load("#subDiv1", url);
}

function submitForm_AddServiceToSchedule() {
  var form = $("#frmServiceList");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=class&task=addServiceToSchedule&studentID=' + studentID,
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winAddServiceToSchedule").dialog('close');
	  loadStudentClassList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }
}

function removeClassFromSchedule() {
  var form = $("#frmClassList")[0];
  var studentClassID = xsite.getCheckedValue(form.studentClassID);
  if (!studentClassID) {
	alert("Please select a class to remove.");
	return;
  }
  
  if (!confirm('Are you sure you want to remove the selected class from this registrant\'s schedule?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=removeClassFromSchedule&studentID=' + studentID + '&studentClassID=' + studentClassID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadStudentClassList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function removeServiceFromSchedule() {
  var form = $("#frmClassList")[0];
  var studentServiceID = xsite.getCheckedValue(form.studentServiceID);
  if (!studentServiceID) {
	alert("Please select a service to remove.");
	return;
  }
  
  if (!confirm('Are you sure you want to remove the selected service from this registrant\'s schedule?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=removeServiceFromSchedule&studentID=' + studentID + '&studentServiceID=' + studentServiceID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadStudentClassList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function editAttendeeInfo() {
  var form = $("#frmClassList")[0];
  if (form.studentClassID) {
    var studentClassID = xsite.getCheckedValue(form.studentClassID);
	if (studentClassID) {
	  editClassAttendeeInfo(studentClassID);
	  return;
	}
  } 
  
  if (form.studentServiceID) {
    var studentServiceID = xsite.getCheckedValue(form.studentServiceID);
	if (studentServiceID) {
	  editServiceAttendeeInfo(studentServiceID);
	  return;
	}  
  }
  
  alert("Please select a class or a service to edit it\'s attendee information.");
  return;
}

function editClassAttendeeInfo(studentClassID) {  
  xsite.createModalDialog({
	windowName: 'winEditClassAttendeeInfo',
	title: 'Edit Attendee Info',
	width: 600,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_edit_class_attendee_info&studentID=' + studentID + '&studentClassID=' + studentClassID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditClassAttendeeInfo").dialog('close'); },
	  ' Save ': submitForm_EditClassAttendeeInfo
	}
  }).dialog('open');
	
  function submitForm_EditClassAttendeeInfo() {
	var form = $("#frmEditClassAttendeeInfo");
	form.validate();
	form.ajaxSubmit({ 
	  url: 'action.cfm?md=class&task=editClassAttendeeInfo',
	  type: 'post',
	  cache: false,
	  dataType: 'json', 
	  success: processJson,
	  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditClassAttendeeInfo").dialog('close');
		loadStudentClassList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editServiceAttendeeInfo(studentServiceID) {  
  xsite.createModalDialog({
	windowName: 'winEditServiceAttendeeInfo',
	title: 'Edit Attendee Info',
	width: 600,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_edit_service_attendee_info&studentID=' + studentID + '&studentServiceID=' + studentServiceID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditServiceAttendeeInfo").dialog('close'); },
	  ' Save ': submitForm_EditServiceAttendeeInfo
	}
  }).dialog('open');
	
  function submitForm_EditServiceAttendeeInfo() {
	var form = $("#frmEditServiceAttendeeInfo");
	form.validate();
	form.ajaxSubmit({ 
	  url: 'action.cfm?md=class&task=editServiceAttendeeInfo',
	  type: 'post',
	  cache: false,
	  dataType: 'json', 
	  success: processJson,
	  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditServiceAttendeeInfo").dialog('close');
		loadStudentClassList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
