<cfparam name="URL.classID">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset classAttendees=CL.getClassAttendeesByClassID(URL.classID)>
<cfset numAttendees=classAttendees.recordcount>

<cfif numAttendees GT 0>
  <form id="frmAttendeeList">
  <input type="hidden" name="classID" value="#URL.classID#">  
  <div class="itemList">
    <table>
      <tr>
        <th width="40">&nbsp;</th>
        <th width="200">Attendee Name</th>
        <th width="250">Guest Name(s)</th>
		<th width="100">Pay with Cash?</th>
        <th>&nbsp;</th>
      </tr>  
      <cfoutput query="classAttendees">
      <tr>
        <td align="center"><input type="checkbox" name="studentClassIDList" value="#studentClassID#"></td>
        <td>#attendeeName#</td> 
        <td><cfif Compare(guestNames,"")>#guestNames#<cfelse>&nbsp;</cfif></td>
        <td align="center"><cfif payWithCash><b>Yes</b><cfelse>No</cfif> &nbsp;&nbsp;&nbsp; <a href="javascript:changeClassPayWithCashStatus(#studentClassID#, #payWithCash#)" class="accent01">&raquo; change</a></td>
        <td>&nbsp;</td>
      </tr>
      </cfoutput>
    </table>
  </div>
  </form>
<cfelse>
  <p>No students registered for this class.</p>
</cfif>
