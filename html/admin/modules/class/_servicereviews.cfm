<cfparam name="URL.serviceID">
<cfparam name="URL.catID" default="0">

<script type="text/javascript" src="modules/class/servicereviews.js"></script>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset serviceInfo=CL.getService(URL.serviceID)>

<div class="header">CLASS MANAGER &gt; Service Reviews</div>

<span id="serviceID" style="display:none"><cfoutput>#URL.serviceID#</cfoutput></span>
<div id="actionBar">
<a href="#" id="btnNew" class="fg-button ui-state-default ui-corner-all button65" onclick="addServiceReview()">New</a>
<a href="#" id="btnNew" class="fg-button ui-state-default ui-corner-all button65" onclick="editServiceReview()">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteServiceReview()">Delete</a>
</div>

<br style="clear:both;" /><br />

<cfoutput>
<p><a href="index.cfm?md=class&tmp=services&wrap=1&catID=#URL.catID#">&laquo; Back</a></p>

<p class="subHeader">Service Title: <b><i>#serviceInfo.serviceTitle#</i></b></p>
</cfoutput>


<div id="mainDiv"></div>
