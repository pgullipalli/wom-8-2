<cfparam name="URL.classType" default="Aerobics">

<script type="text/javascript" src="modules/class/fitnessclasses.js"></script>

<div class="header">CLASS MANAGER &gt; Fitness Club Classes</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addClass();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editClass();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteClass();">Delete</a>
</div>

<br style="clear:both;" /><br />

<cfoutput>
<form id="frmNavigator">
  Class Type
  <select name="classType" style="min-width:200px;">
    <option value="Aerobics"<cfif URL.classType IS "Aerobics"> selected</cfif>>Aerobics</option>
    <option value="Gym Classes"<cfif URL.classType IS "Gym Classes"> selected</cfif>>Gym Classes</option>
    <option value="Mind/Body"<cfif URL.classType IS "Mind/Body"> selected</cfif>>Mind/Body</option>
    <option value="Aquatics"<cfif URL.classType IS "Aquatics"> selected</cfif>>Aquatics</option>
  </select>
</form>
</cfoutput>

<br style="clear:both;" />

<div id="mainDiv"></div>