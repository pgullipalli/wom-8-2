<cfparam name="URL.courseID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset prerequisites=CL.getPrerequisites(URL.courseID)>

<cfif prerequisites.recordcount Is 0>
<p>No recommended prerequisites selected for this course. Click "New" button in the action bar to select prerequisites.</p>
<cfelse>
<form id="frmPrerequisitesList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th>Course Title</th>
  </tr>  
  <cfoutput query="prerequisites">
  <tr>
    <td align="center"><input type="checkbox" name="courseIDList" value="#courseID#"></td>
    <td>#courseTitle#</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>