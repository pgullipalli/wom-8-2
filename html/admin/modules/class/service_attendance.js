var serviceID;

$(function () {	
	serviceID=$("#serviceID").html();
});

function updateAttendance() {
	$("#message").hide();	
	var form = $("#frmAttendeeList");
	if (form.length == 0) return;
	xsite.showWaitingDialog();
	form.ajaxSubmit({ 
	  url: 'action.cfm?md=class&task=updateServiceAttendance',
	  type: 'post',
	  cache: false,
	  dataType: 'json', 
	  success: processJson,
	  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		xsite.closeWaitingDialog();
		$("#msgAttendanceUpdate").html('The attendance of this service has been updated.');	
		$("#message").show();
	  } else {
		xsite.closeWaitingDialog();
		alert (jsonData.MESSAGE);
	  }
	}
}

function printAttendanceSheet() {
	xsite.openWindow('index.cfm?md=class&tmp=service_attendance_sheet&wrap=1&serviceID=' + serviceID + '&uuid=' + xsite.getUUID(), 'attendance', 700, 600);
}