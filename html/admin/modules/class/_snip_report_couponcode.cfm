﻿<cfparam name="URL.discountCode">
<cfparam name="URL.startDate">
<cfparam name="URL.endDate">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset couponReport=CL.getCouponCodeReport(discountCode=URL.discountCode, startDate=URL.startDate, endDate=URL.endDate)>

<div class="itemList">
<table>
  <tr>
	<th width="250">Date Registered</th>
    <th>Course Title</th>
    <th>Contact Name</th>
    <th>Amount Paid</th>
    <th>Discount Amount</th>
  </tr>
  <cfset amountPaidTotal=0>
  <cfset discountAmountTotal=0>
  <cfoutput>
  <cfloop query="couponReport">
  <tr align="center">
    <td>#DateFormat(orderDate,"mm/dd/yyyy")# #LCase(TimeFormat(orderDate,"h:mm tt"))#</td>
    <td>#courseTitle#</td>
    <td>#contactFirstName# #contactLastName#</td>
    <td>#DollarFormat(amountPaid)#<cfset amountPaidTotal=amountPaidTotal + amountPaid></td>
    <td>#DollarFormat(discountAmount)#<cfset discountAmountTotal=discountAmountTotal + discountAmount></td>
  </tr>
  </cfloop> 
  <tr align="center">
    <td align="right" colspan="3"><b>Total</b></td>
    <td><b>#DollarFormat(amountPaidTotal)#</b></td>
    <td><b>#DollarFormat(discountAmountTotal)#</b></td>
  </tr>
  </cfoutput>
</table>
</div>

