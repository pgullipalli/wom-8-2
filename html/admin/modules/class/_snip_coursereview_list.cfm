<cfparam name="URL.courseID">
<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset coursReviews=CL.getCourseReviews(URL.courseID)>
<cfset newLine=Chr(13) & Chr(10)>

<cfif coursReviews.recordcount Is 0>
<p>No course reviews written for this course. Click "New" button in the action bar to create one.</p>
<cfelse>
<form id="frmCourseReviewList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="120">Name</th>
    <th>Reviews</th>
    <th width="100">Publish?</th>
  </tr>  
  <cfoutput query="coursReviews">
  <tr>
    <td align="center"><input type="radio" name="courseReviewIDList" value="#courseReviewID#"></td>
    <td>#fullName#&nbsp;</td>
    <td>#Replace(review, newLine, "<br />", "All")#&nbsp;</td>
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>