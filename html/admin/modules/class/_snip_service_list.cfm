<cfparam name="URL.catID" default="0">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>

<cfset structServices=CL.getServicesByCategoryID(categoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
<cfif structServices.numAllItems IS 0>
  <p>
  It appears that there are no services created in this category.
  Please click on the 'New' button in the action bar above to create the first service.
  </p>
</cfif>

<cfif structServices.numDisplayedItems GT 0>
<form id="frmServiceList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th>Service Title</th>
    <th>GL</th>    
    <th>Fee</th>
    <th>Reviews</th>
    <th>Associated Courses</th>
    <th># Participants</th>
    <th width="100">Publish?</th>
  </tr>  
  <cfoutput query="structServices.services">
  <tr>
    <td align="center"><input type="radio" name="serviceID" value="#serviceID#"></td>
	<td>
      <a href="javascript:editService('#serviceID#')" class="accent01">#serviceTitle#</a>
    </td> 
    <td align="center">#GLNumber#&nbsp;</td>
    <td align="center">#DollarFormat(feeRegular)#</td>
    <td align="center"><a href="index.cfm?md=class&tmp=servicereviews&wrap=1&serviceID=#serviceID#&catID=#URL.catID#" class="accent01">#numReviews#</a></td>
    <td align="center"><a href="index.cfm?md=class&tmp=service_associatedcourses&wrap=1&serviceID=#serviceID#&catID=#URL.catID#" class="accent01">#numAssociatedCourses#</a></td>
    <td align="center">
      <cfif numParticipants GT 0>
        <a href="index.cfm?md=class&tmp=service_participants&wrap=1&serviceID=#serviceID#&catID=#URL.catID#" class="accent01">#numParticipants#</a>
      <cfelse>
        0
      </cfif>
    </td>
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif structServices.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structServices.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadServiceList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>