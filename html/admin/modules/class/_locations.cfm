<script type="text/javascript" src="modules/class/locations.js"></script>

<div class="header">CLASS MANAGER &gt; Locations &amp; Maps</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addLocation();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editLocation(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteLocation();">Delete</a>
</div>

<br style="clear:both;" /><br />

<div id="mainDiv"></div>