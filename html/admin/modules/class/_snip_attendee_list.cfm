<cfparam name="URL.classID">
<cfparam name="URL.classInstanceID">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset classAttendees=CL.getClassAttendeesByClassID(URL.classID)>
<cfset followUpStatus=CL.getFollowUpStatus(URL.classInstanceID)>
<cfset attendedStudentClassIDList=CL.getAttendedStudentClassIDListByClassInstanceID(URL.classInstanceID)>
<cfset numAttendees=classAttendees.recordcount>

<cfif numAttendees GT 0>
  <cfoutput>
  <form id="frmAttendeeList">
  <input type="hidden" name="classID" value="#URL.classID#">
  <input type="hidden" name="classInstanceID" value="#URL.classInstanceID#">

  <table border="0" cellpadding="3" cellspacing="0" width="600">    
    <tr valign="top">
      <td width="300">
      <cfloop index="idx" from="1" to="#Ceiling(numAttendees/2)#">
        <input type="checkbox" name="attendedStudentClassIDList" value="#classAttendees.studentClassID[idx]#"<cfif ListFind(attendedStudentClassIDList, classAttendees.studentClassID[idx]) GT 0> checked</cfif>>
        #idx#. #classAttendees.attendeeName[idx]#<cfif classAttendees.feePerCouple[idx]>,
          #classAttendees.guestNames[idx]#
        </cfif>
        <br />
      </cfloop>
      &nbsp;
      </td>
      <td width="300">
      <cfloop index="idx" from="#Ceiling(numAttendees/2) + 1#" to="#numAttendees#">
        <input type="checkbox" name="attendedStudentClassIDList" value="#classAttendees.studentClassID[idx]#"<cfif ListFind(attendedStudentClassIDList, classAttendees.studentClassID[idx]) GT 0> checked</cfif>>
        #idx#. #classAttendees.attendeeName[idx]#<cfif classAttendees.feePerCouple[idx]>,
          #classAttendees.guestNames[idx]#
        </cfif>
        <br />
      </cfloop>
      &nbsp;
      </td>
    </tr>
  </table>
  </form>
  </cfoutput>
  
  <cfif followUpStatus.feedbackRequestSent>
  <p>* Follow-up class feedback request has been sent.</p>
  </cfif>
  
  <cfif followUpStatus.missedClassFollowUpSent>
  <p>* Missed class follow-up mail has been sent.</p>
  </cfif>
<cfelse>
  <p>No students registered for this class.</p>
</cfif>
