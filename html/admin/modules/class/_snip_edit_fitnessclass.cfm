<cfparam name="URL.fitnessClassID">
<cfscript>
  CL=CreateObject("component", "com.ClassAdmin").init();
  classInfo=CL.getFitnessClass(URL.fitnessClassID);
  classTime=CL.getFitnessClassSchedule(URL.fitnessClassID);
  if (classTime.recordcount GT 0) {
    dayOfWeekList=ValueList(classTime.dayOfWeekAsNumber);
  } else {
    dayOfWeekList="";
  }
  startTime=ArrayNew(1);
  endTime=ArrayNew(1);
  for (i=1; i LTE 7; i=i+1) {
    startTime[i]=StructNew();
	endTime[i]=StructNew();
	startTime[i].hh="12";
	startTime[i].mm="0";
	startTime[i].ampm="AM";
	endTime[i].hh="12";
	endTime[i].mm="0";
	endTime[i].ampm="AM";
  }
  for (idx=1; idx LTE classTime.recordcount; idx=idx+1) {
    startTime[classTime.dayOfWeekAsNumber[idx]].hh=TimeFormat(classTime.startTime[idx],"h");
	startTime[classTime.dayOfWeekAsNumber[idx]].mm=TimeFormat(classTime.startTime[idx],"m");
	startTime[classTime.dayOfWeekAsNumber[idx]].ampm=TimeFormat(classTime.startTime[idx],"tt");	
	endTime[classTime.dayOfWeekAsNumber[idx]].hh=TimeFormat(classTime.endTime[idx],"h");
	endTime[classTime.dayOfWeekAsNumber[idx]].mm=TimeFormat(classTime.endTime[idx],"m");
	endTime[classTime.dayOfWeekAsNumber[idx]].ampm=TimeFormat(classTime.endTime[idx],"tt");
  }
</cfscript>

<cfoutput query="classInfo">
<form id="frmEditClass" name="frmEditClass" onsubmit="return false">
<input type="hidden" name="fitnessClassID" value="#URL.fitnessClassID#" />
<div class="row">
	<div class="col-30-right"><b>Class Title:</b></div>
    <div class="col-68-left"><input type="text" name="classTitle" maxlength="100" value="#HTMLEditFormat(classTitle)#" style="width:320px;" validate="required:true" /></div>
</div>
<div class="row">
	<div class="col-30-right">Class Type:</div>
    <div class="col-68-left">
      <input type="radio" name="classType" value="Aerobics" <cfif Not Compare(classType,"Aerobics")>checked</cfif> /> Aerobics
      <input type="radio" name="classType" value="Gym Classes" <cfif Not Compare(classType,"Gym Classes")>checked</cfif> /> Gym Classes
      <input type="radio" name="classType" value="Mind/Body" <cfif Not Compare(classType,"Mind/Body")>checked</cfif> /> Mind/Body
      <input type="radio" name="classType" value="Aquatics" <cfif Not Compare(classType,"Aquatics")>checked</cfif> /> Aquatics
    </div>
</div>
<div class="row">
	<div class="col-30-right">Location:</div>
    <div class="col-68-left">
      <input type="radio" name="location" value="Studio 1" <cfif Not Compare(location,"Studio 1")>checked</cfif> /> Studio 1
      <input type="radio" name="location" value="Gym" <cfif Not Compare(location,"Gym")>checked</cfif> /> Gym
      <input type="radio" name="location" value="Studio 3" <cfif Not Compare(location,"Studio 3")>checked</cfif> /> Studio 3
      <input type="radio" name="location" value="Pool" <cfif Not Compare(location,"Pool")>checked</cfif> /> Pool  
    </div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="published" value="1" <cfif published>checked</cfif> /> Publish this class
    </div>
</div>
<div class="row">
	<div class="col-30-right">Instructor's Name:</div>
    <div class="col-68-left"><input type="text" name="instructorName" maxlength="100" value="#HTMLEditFormat(instructorName)#" style="width:320px;" /></div>
</div>

<div class="row">
	<div class="col-30-right"><b>Class Schedule:</b></div>
    <div class="col-68-left">
      <cfloop index="idx" from="1" to="7">
      <div class="row">
          <div class="row">
            <div style="width:10%;padding:0px 2px;text-align:right;float:left;"><input type="checkbox" name="dayOfWeekAsNumber_#idx#" value="1" <cfif ListFind(dayOfWeekList,idx) GT 0>checked</cfif> /></div>
            <div style="width:87%;padding:0px 2px;text-align:left;float:left;"> #DayOfWeekAsString(idx)#</div>
          </div>
          <div class="row">
            <div style="width:10%;padding:0px 2px;text-align:right;float:left;">From:</div>
            <div style="width:87%;padding:0px 2px;text-align:left;float:left;">
              <select name="startTime_hh_#idx#">
                <option value="12">12</option>
                <cfloop index="hh" from="1" to="12"><option value="#hh#" <cfif startTime[idx].hh Is hh>selected</cfif>><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
              </select>
              <select name="startTime_mm_#idx#">
                <cfloop index="mm" from="0" to="55" step="5"><option value="#mm#" <cfif startTime[idx].mm Is mm>selected</cfif>><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
              </select>
              <select name="startTime_ampm_#idx#">
                <option value="am" <cfif startTime[idx].ampm Is "AM">selected</cfif>>AM</option><option value="pm" <cfif startTime[idx].ampm Is "PM">selected</cfif>>PM</option>
              </select> &nbsp;&nbsp;
              To:
              <select name="endTime_hh_#idx#">
                <option value="12">12</option>
                <cfloop index="hh" from="1" to="12"><option value="#hh#"<cfif endTime[idx].hh Is hh>selected</cfif>><cfif hh LT 10>0#hh#<cfelse>#hh#</cfif></option></cfloop>
              </select>
              <select name="endTime_mm_#idx#">
                <cfloop index="mm" from="0" to="55" step="5"><option value="#mm#"<cfif endTime[idx].mm Is mm>selected</cfif>><cfif mm LT 10>0#mm#<cfelse>#mm#</cfif></option></cfloop>
              </select>
              <select name="endTime_ampm_#idx#">
                <option value="am" <cfif endTime[idx].ampm Is "AM">selected</cfif>>AM</option><option value="pm" <cfif endTime[idx].ampm Is "PM">selected</cfif>>PM</option>
              </select>
            </div>
          </div>
      </div>
      </cfloop>
    </div>
</div>

<div class="row">
	<div class="col-30-right">Full-Size Image:</div>
    <div class="col-68-left">
      <div class="row">
        <input type="hidden" name="fullsizeImage" value="#HTMLEditFormat(fullsizeImage)#">
        <CF_SelectFile formName="frmEditClass" fieldName="fullsizeImage" previewID="preview3" previewWidth="150">
	    <div id="preview3"><cfif Compare(fullsizeImage,"")><img src="/#fullsizeImage#" border="0" width="150"></cfif></div>
      </div>
      <div class="row">
        Caption:<br />
        <textarea name="imageCaption" style="width:320px;height:40px;">#HTMLEditFormat(imageCaption)#</textarea>
      </div>
    </div>
</div>

<!--- <div class="row">
	<div class="col-30-right">Thumbnail Image:</div>
    <div class="col-68-left">
      <input type="hidden" name="thumbnailImage" value="#HTMLEditFormat(thumbnailImage)#">
      <CF_SelectFile formName="frmEditClass" fieldName="thumbnailImage" previewID="preview4" previewWidth="100">
      <div id="preview4"><cfif Compare(thumbnailImage,"")><img src="/#thumbnailImage#" border="0" width="100"></cfif></div>
    </div>
</div> --->

<div class="row">
	<div class="col-30-right">Description:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="false"
            formName="frmEditClass"
            fieldName="description"
            cols="55"
            rows="3"
            HTMLContent="#description#"
            displayHTMLSource="true"
            editorHeader="Edit Description">
    </div>
</div>

<div class="row">
	<div class="col-30-right">Special Instructions:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmEditClass"
            fieldName="specialInstructions"
            cols="55"
            rows="3"
            HTMLContent="#specialInstructions#"
            displayHTMLSource="true"
            editorHeader="Edit Special Instructions">
    </div>
</div>

<div class="row">
	<div class="col-30-right">More Info URL:</div>
    <div class="col-68-left">
      <input type="text" name="moreInfoURL" maxlength="200" value="#HTMLEditFormat(moreInfoURL)#" style="width:320px;" />
    </div>
</div>
</form>
</cfoutput>