var courseID;

$(function () {	
	courseID=$("#courseID").html();
	
	loadReviewList();	
});

function loadReviewList() {  
  var url="index.cfm?md=class&tmp=snip_coursereview_list&courseID=" + courseID + "&uuid=" + xsite.getUUID();
  xsite.load("#mainDiv", url);
}

function addCourseReview() {
  xsite.createModalDialog({
	windowName: 'winAddCourseReview',
	title: 'Add Course Review',
	width: 600,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_add_coursereview&courseID=' + courseID + '&uuid=' + xsite.getUUID(),	
	buttons: {
	  'Cancel': function() { $("#winAddCourseReview").dialog('close'); },
	  ' Add ': submitForm_AddCourseReview
	}
  }).dialog('open');

}

function submitForm_AddCourseReview() {
  var form = $("#frmAddCourseReview");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=class&task=addCourseReview',
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winAddCourseReview").dialog('close');
	  loadReviewList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }
}

function editCourseReview(crID) {
  var form = $("#frmCourseReviewList")[0];
  var courseReviewID = null;
  if (crID) courseReviewID = crID;
  else courseReviewID = xsite.getCheckedValue(form.courseReviewIDList);

  if (!courseReviewID) {
	alert("Please select a review to edit.");
	return;	  
  }	
	
  xsite.createModalDialog({
	windowName: 'winEditCourseReview',
	title: 'Edit Course Review',
	width: 600,
	position: 'center-up',
	url: 'index.cfm?md=class&tmp=snip_edit_coursereview&courseReviewID=' + courseReviewID + '&uuid=' + xsite.getUUID(),	
	buttons: {
	  'Cancel': function() { $("#winEditCourseReview").dialog('close'); },
	  ' Save ': submitForm_EditCourseReview
	}
  }).dialog('open');

}

function submitForm_EditCourseReview() {
  var form = $("#frmEditCourseReview");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=class&task=editCourseReview&courseID=' + courseID,
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winEditCourseReview").dialog('close');
	  loadReviewList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }
}


function deleteCourseReview() {
  if(!document.getElementById('frmCourseReviewList')) return;
  var form = $("#frmCourseReviewList")[0];
  var courseReviewIDList = xsite.getCheckedValue(form.courseReviewIDList);
  if (!courseReviewIDList) {
	alert("Please select at least an item to remove.");
	return;	  
  }
  if (!confirm('Are you sure you want to delete the selected items?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=class&task=deleteCourseReviews&&courseReviewIDList=' + escape(courseReviewIDList) + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  loadReviewList();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

