<cfparam name="URL.courseID">
<cfparam name="URL.classID">
<cfparam name="URL.classInstanceID">

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset courseInfo=CL.getCourse(URL.courseID)>
<cfset allClassInstances=CL.getClassInstancesByClassID(URL.classID)>
<cfset classAttendees=CL.getClassAttendeesByClassID(URL.classID)>
<cfset numAttendees=classAttendees.recordcount>
<cfset classInstanceInfo=CL.getClassInstanceByClassInstanceID(URL.classInstanceID)>

<script language="javascript">
//$(function () {openprintdialog();});

function openprintdialog () {
  var onWindows = navigator.platform ? navigator.platform == "Win32" : false;
  var macIE = document.all && !onWindows;
  if (macIE) {
	alert ("Press \"Cmd+p\" on your keyboard to print");
  } else {
	window.print();
  }  
}			
</script>

<style type="text/css" media="screen">
#printerControl {text-align:right;}
</style>

<style type="text/css" media="print">
#printerControl {display:none;}
</style>



<div id="printerControl"><a href="javascript:openprintdialog();">PRINT</a> | <a href="javascript:window.close();">CLOSE</a></div>

<div style="margin:0px 0px; width:100%">
<img src="images/womans_logo2.gif" style="padding:0px 0px; height:100px; border-width:0px;">
</div>

<p class="header" align="center"><b>Sign Up Sheet</b></p>
<p class="subHeader" align="center"><b><cfoutput>#courseInfo.courseTitle#</cfoutput></b></p>

<div class="row">
  <div class="col-48-left">    	
	<cfoutput>
    <p>
    <b>Date/Time:</b>
    <ul>
    <cfloop query="allClassInstances">
      <cfif Compare(allClassInstances.classInstanceID, URL.classInstanceID)>
      <li><strike>#DateFormat(startDateTime,"mm/dd/yyyy")# #LCase(TimeFormat(startDateTime,"hh:mmtt"))# - #LCase(TimeFormat(endDateTime,"hh:mmtt"))#</strike></li>
      <cfelse>
      <li>#DateFormat(startDateTime,"mm/dd/yyyy")# #LCase(TimeFormat(startDateTime,"hh:mmtt"))# - #LCase(TimeFormat(endDateTime,"hh:mmtt"))#</li>
      </cfif>
    </cfloop>
    </ul>
    </p>
    
    <p><b>Location:</b> #classInstanceInfo.locationName#</p>
    <p><b>Contact:</b> #courseInfo.contactName#</p>
    </cfoutput>
  </div>

  <div class="col-48-right">
      <div style="width:300px; padding:10px 10px; border:1px solid #666666; float:right;">
          <div class="row">
            <div class="col-30-right">Administrator: </div>
            <div class="col-66-left" style="border-bottom:1px solid #666666;">&nbsp;</div>
          </div>
          <div class="row">
            <div class="col-30-right">Assistants: </div>
            <div class="col-66-left" style="border-bottom:1px solid #666666;">&nbsp;</div>
          </div>
          <div class="row">
            <div class="col-30-right">&nbsp; </div>
            <div class="col-66-left" style="border-bottom:1px solid #666666;">&nbsp;</div>
          </div>
          <div class="row">
            <div class="col-30-right">$Collected: </div>
            <div class="col-66-left" style="border-bottom:1px solid #666666;">&nbsp;</div>
          </div>
      </div>
  </div>
</div>

<br style="clear:both;" /><br />

<p><b>If this class is held during your meal time, we encourage you to either eat before you come to class or to bring snack food with you to eat during class.</b></p>
    
<p><img src="images/imgDisabilityLogo.gif" border="0" align="left" /> <b>Notify Resources for Women if you need assistance due to disability recognized by the ADA.</b></p>

<br style="clear:both;" /><br />

<form>
<table border="0" cellpadding="3" cellspacing="0" width="100%" align="center">
  <tr valign="top">
    <th width="150">Attendee</th>
    <th>Guest(s)</th>
    <th width="100">Paid/Prev Pay</th>
    <th width="460">Signature</th>
  </tr>
  <tr valign="middle" height="10">
    <td colspan="4"><div style="border-top:1px solid #666666; width:100%;"></div></td>
  </tr>
  <cfoutput>
  <cfloop index="idx" from="1" to="#numAttendees#">
  <cfif idx MOD 2 IS 0><cfset bgcolor=" bgcolor=""##eeeeee"""><cfelse><cfset bgcolor=""></cfif>
  <tr height="40"#bgcolor#>
    <td><b>#classAttendees.attendeeName[idx]#</b></td>
    <td>
	  <cfif classAttendees.feePerCouple[idx]>
        <b>#classAttendees.guestNames[idx]#</b>
      <cfelse>
      &nbsp;
	  </cfif>      
    </td>
    <td valign="top">
      <cfif classAttendees.payWithCash[idx]>
      <input type="checkbox" name="payWithCash" value="1" />  #DollarFormat(classAttendees.fee[idx])#
      </cfif>
    </td>    
    <td valign="bottom">
      <div style="border-bottom:1px solid ##666666; width:220px; margin:0px 0px; float:left;"></div>
      <cfif classAttendees.feePerCouple[idx]>
      <div style="border-bottom:1px solid ##666666; width:220px; margin:0px 0px 0px 10px; float:left;"></div>
      </cfif>
    </td>
  </tr>
  </cfloop>
  </cfoutput>
</table>
</form>

<!--- <cfoutput>
<table border="0" cellpadding="3" cellspacing="0" width="600">    
    <tr valign="top">
      <td width="300">
      <cfloop index="idx" from="1" to="#Ceiling(numAttendees/2)#">
        <input type="checkbox" name="attendedStudentIDList" value="#classAttendees.studentID[idx]#">
        #idx#. #classAttendees.attendeeFirstName[idx]# #classAttendees.attendeeLastName[idx]#<br />
      </cfloop>
      &nbsp;
      </td>
      <td width="300">
      <cfloop index="idx" from="#Ceiling(numAttendees/2) + 1#" to="#numAttendees#">
        <input type="checkbox" name="attendedStudentIDList" value="#classAttendees.studentID[idx]#">
        #idx#. #classAttendees.attendeeFirstName[idx]# #classAttendees.attendeeLastName[idx]#<br />
      </cfloop>
      &nbsp;
      </td>
    </tr>
</table>


</cfoutput> --->



<br /><br /><br />