<cfparam name="URL.fromDate" default="#DateFormat(DateAdd("m", -1, now()),"mm/dd/yyyy")#">
<cfparam name="URL.toDate" default="#DateFormat(now(), "mm/dd/yyyy")#">

<script type="text/javascript" src="modules/promotion/report_overall.js"></script>

<div class="header">PROMOTION MANAGER &gt; Overall Report</div>

<cfoutput>
<form id="frmReportDateRange" onSubmit="return false;">
  <div class="row">
  Select a date range to examine the page views and click-throughs for all ads over the specified period of time:<br /><br />
  </div>  
  <div class="row">
  From <input type="text" name="fromDate" id="fromDate" size="12" value="#URL.fromDate#" validate="required: true, date: true"> &nbsp;
  To <input type="text" name="toDate" id="toDate" size="12" value="#URL.toDate#" validate="required: true, date: true">
  <button onClick="loadReport();">Create Report</button>  
  </div>  
</form>
</cfoutput>

<div id="mainDiv"></div>
