var sortBy="views";
var sortDirection="DESC";
var showImages=true;

$(function () {	
  $("#fromDate").datepicker();
  $("#toDate").datepicker();
});

function loadReport(sBy) {
  if (sBy) {
	if (sortBy == sBy) {
	  if (sortDirection=="ASC") sortDirection="DESC"; else sortDirection="ASC";
	}
	sortBy = sBy;	
  }
  
  var form = $("#frmReportDateRange");
  form.validate();
  if (!form.valid()) return;
  var fromDate=$("#frmReportDateRange input[name='fromDate']").val();
  var toDate=$("#frmReportDateRange input[name='toDate']").val();
  
  
  var url="index.cfm?md=promotion&tmp=snip_report_overall&fromDate=" + escape(fromDate) + "&toDate=" + escape(toDate) + "&sortBy=" + sortBy + "&sortDirection=" + sortDirection + "&uuid=" + xsite.getUUID();
  xsite.load("#mainDiv", url);  
}

function toggleImages() {
  if (showImages) {
    $(".promoImage").hide();	
    showImages=false;
  } else {
	$(".promoImage").show();	
    showImages=true;
  }
}