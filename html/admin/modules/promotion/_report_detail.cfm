<cfset PM=CreateObject("component", "com.PromotionAdmin").init()>
<cfset allPromos=PM.getAllPromos()>

<script type="text/javascript" src="modules/promotion/report_detail.js"></script>

<div class="header">PROMOTION MANAGER &gt; Detail Report</div>

<cfoutput>
<form id="frmDetailReport" onSubmit="return false;">
  <div class="row">
  Select a month and a promo to check page views and click-throughs for the specific promo during the given month: <br /><br />
  </div>  
  <div class="row">
  Month
  <cfset currentTime=now()>
  <cfset startDate=CreateDate(Year(currentTime), Month(currentTime), 1)>
  <select name="startDate" style="min-width:100px;">
  <cfloop index="m" from="0" to="-11" step="-1">
    <cfset dateObj=DateAdd("m",m,startDate)>
    <option value="#DateFormat(dateObj,"mm/dd/yyyy")#">#DateFormat(dateObj,"MMMM, yyyy")#</option>
  </cfloop>
  </select>
  
  &nbsp;&nbsp;
  
  Promo  
  <select name="promoID" style="min-width:100px;">
    <cfloop query="allPromos">
    <option value="#promoID#">#promoName#<cfif Not published> (Inactive)</cfif></option>
    </cfloop>
  </select>
    
  <button onClick="loadReport();">Create Detail Report</button>  
  </div>  
</form>
</cfoutput>

<div id="mainDiv"></div>

