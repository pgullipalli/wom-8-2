<cfparam name="URL.promoID">

<cfset PM=CreateObject("component", "com.PromotionAdmin").init()>
<cfset UT=CreateObject("component", "com.Utility").init()>

<cfset allPlacements=PM.getAllPlacements()>
<cfset promoInfo=PM.getPromo(URL.promoID)>
<cfset promoPlacementInfo=PM.getPromoPlacementInfo(URL.promoID)>
<cfset placementList=ValueList(promoPlacementInfo.placement)>
<cfset allCats=StructNew()>
<cfset allCats["homepage"]=UT.getCategoriesByModuleName("subhome")>
<cfset allCats["pagebuilder"]=UT.getCategoriesByModuleName("pagebuilder")>
<cfset allCats["class"]=UT.getCategoriesByModuleName("class")>
<cfset allCats["newsroom"]=UT.getCategoriesByModuleName("newsroom")>
<cfset allCats["photogallery"]=UT.getCategoriesByModuleName("photogallery")>
<cfset allCats["directory"]=UT.getCategoriesByModuleName("directory")>
<cfset allCats["event"]=UT.getCategoriesByModuleName("event")>

<cfset PB=CreateObject("component", "com.PageBuilderAdmin").init()>
<cfset pageName="">
<cfif Compare(promoInfo.pageID,0)>
  <cfset pageInfo=PB.getPageByPageID(promoInfo.pageID)>
  <cfif pageInfo.recordcount GT 0>
    <cfset pageName=pageInfo.pageName>
  </cfif>
</cfif>
<cfset allPages=PB.getAllPages()>


<cfoutput query="promoInfo">
<form name="frmEditPromo" id="frmEditPromo">
<input type="hidden" name="promoID" value="#URL.promoID#" />
<div class="row">
	<div class="col-30-right"><b>Promo Name:</b></div>
    <div class="col-68-left">
      <input type="text" name="promoName" maxlength="50" value="#HTMLEditFormat(promoName)#" style="width:200px;" validate="required:true"> &nbsp;&nbsp;
      <input type="checkbox" name="published" value="1" <cfif published>checked</cfif> /> Publish this promo
    </div>
</div>

<!--- all promo date will be default to 1900 - 3000 --->
<div class="row">
	<div class="col-30-right"><b>Start Date:</b></div>
    <div class="col-68-left">
      <cfset startDateStr=DateFormat(startDate,"mm/dd/yyyy")>
      <cfif Compare(startDateStr,"01/01/1900")>
        <input type="text" name="startDate" class="dateField" style="width:120px;" value="#startDateStr#" validate="required:false, date:true" /> &nbsp;&nbsp;&nbsp;
      <cfelse>
        <input type="text" name="startDate" class="dateField" style="width:120px;" value="" validate="required:false, date:true" /> &nbsp;&nbsp;&nbsp;
      </cfif>
      <b>End Date:</b>
      <cfset endDateStr=DateFormat(endDate,"mm/dd/yyyy")>
      <cfif Compare(endDateStr,"01/01/3000")>
        <input type="text" name="endDate" class="dateField" style="width:120px;" value="#endDateStr#" validate="required:false, date:true" />
      <cfelse>
        <input type="text" name="endDate" class="dateField" style="width:120px;" value="" validate="required:false, date:true" />
      </cfif>
    </div>
</div>

<div class="row">
	<div class="col-30-right"><b>Promo Image:</b></div>
    <div class="col-68-left">
      <input type="hidden" name="promoImage" value="#HTMLEditFormat(promoImage)#" />
      <CF_SelectFile formName="frmEditPromo" fieldName="promoImage" previewID="preview3" previewWidth="150">
      <div id="preview3"><cfif Compare(promoImage,"")><img src="/#promoImage#" width="150" border="0" /></cfif></div>
    </div>
</div>

<!--- <div class="row">
	<div class="col-30-right">Promo Image (Mouse-Over):</div>
    <div class="col-68-left">
      <input type="hidden" name="promoImageOver" value="#HTMLEditFormat(promoImageOver)#" />
      <CF_SelectFile formName="frmEditPromo" fieldName="promoImageOver" previewID="preview4" previewWidth="150">
      <div id="preview4"><cfif Compare(promoImageOver,"")><img src="/#promoImageOver#" width="150" border="0" /></cfif></div>
    </div>
</div> --->

<div class="row">
	<div class="col-30-right">Description Text:<br /><small>(for placements where live text is associated with the promo)</small></div>
    <div class="col-68-left">
      <textarea name="descriptionText" style="width:300px;height:80px;">#HTMLEditFormat(descriptionText)#</textarea>
    </div>
</div>

<div class="row">
	<div class="col-30-right"><b>Destination URL:</b></div>
    <div class="col-68-left">
      External URL: <input type="text" name="destinationURL" maxlength="200" value="#HTMLEditFormat(destinationURL)#" style="width:300px;" /> - OR - <br />
    </div>
</div>

<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      Select a page created by Page Builder:<br />
	  <input type="hidden" name="pageID" value="#pageID#" />
	  <CF_SelectPage formName="frmEditPromo" fieldName="pageID" previewID="previewPageName">
	  <div id="previewPageName" style="margin:5px 0px;">#pageName#</div>	  
    </div>
</div>

<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="popup" value="1" <cfif popup>checked</cfif> /> Open a new browser window when promo is clicked. 
    </div>
</div>

<div class="row">
	<div class="col-95-left"><b>Select placements and categories:</b> (Check all that apply.)</div>
</div>

<cfloop query="allPlacements">
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <b>#placementDescription#</b><br />      
      <cfset tempPlacementID=placementID>
      <cfif ListFind("class|*,event|*,homepage|subhome,newsroom|*,pagebuilder|home,photogallery|*","#module#|#template#") GT 0>
        <a href="javascript:checkAll(document.frmEditPromo, '#placementID#')">Check All</a> | <a href="javascript:checkNone(document.frmEditPromo,'#placementID#')">Check None</a>
        <div class="row">	
          <cfset qRef=allCats[module]>
          <cfloop query="qRef">
            <div class="col-48-left">
              <input type="checkbox" name="placementList" value="#tempPlacementID#|#catID#" <cfif ListFind(placementList,"#tempPlacementID#|#catID#") GT 0>checked</cfif> /> #catName#
            </div>
          </cfloop>
        </div>        
      <cfelse>  
        <div class="row">
          <div class="col-48-left">
            <input type="checkbox" name="placementList" value="#tempPlacementID#|0" <cfif ListFind(placementList,"#tempPlacementID#|#catID#") GT 0>checked</cfif> /> All
          </div>
        </div>
      </cfif>
    </div>
</div>
</cfloop>
</form>
</cfoutput>