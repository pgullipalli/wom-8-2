var pageNum=1;//initial page number
var placementID=0;
var categoryID=0;

$(function () {	
	loadPromotionList(1, null);
	
	$("#frmPlacement select[name='placementID']").change(function() {
	  pageNum=1;
	  categoryID=0;
	  loadPromotionList();	  
	});
});

function loadPromotionList(pNum) {
  if (pNum) pageNum = pNum;
  
  placementID=$("#frmPlacement select[name='placementID']").val();
  
  var url="index.cfm?md=promotion&tmp=snip_promotion_list&placementID=" + placementID + "&catID=" + categoryID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url, resetCategories);
}

function resetCategories() {
	categoryID=$("#frmCategory select[name='categoryID']").val();
	$("#frmCategory select[name='categoryID']").unbind();
    $("#frmCategory select[name='categoryID']").change(function() {
	  categoryID=$("#frmCategory select[name='categoryID']").val();
	  loadPromotionList();
	});
}

function addPromo() {
  xsite.createModalDialog({
	windowName: 'winAddPromo',
	title: 'Add Promotion',
	width: 750,
	height: 500,
	position: 'center-up',
	url: 'index.cfm?md=promotion&tmp=snip_add_promo&placementID=' + placementID + '&catID=' + categoryID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {//create date pickers for date fields
	  $(".dateField").datepicker();  
	},
	buttons: {
	  'Cancel': function() { $("#winAddPromo").dialog('close'); },
	  ' Add ': submitForm_AddPromo
	}
  }).dialog('open');
  
  function submitForm_AddPromo() {
	var form = $("#frmAddPromo");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=promotion&task=addPromo',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  beforeSubmit: validate,
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function validate() {
	  var form = $("#frmAddPromo")[0];
	  var found = false;
	  if (form.placementList.length) {
	    for (var i = 0; i < form.placementList.length; i++)
		  if (form.placementList[i].checked) found = true;
	  } else {
		  if (form.placementList.checked) found = true;
	  }
	  if (!found) {
		alert('Please select at least one placement for this promo.');
		return false;
	  }
	  if (form.destinationURL.value == "" && form.pageID.selectedIndex < 1) {
	    alert('Please enter a destination URL or select a page created by Page Builder.');
		return false;
	  }
	}
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddPromo").dialog('close');
		loadPromotionList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }		
}

function editPromo(pID) {
  var form = $("#frmPromoList")[0];
  var promoID = null;
  if (pID) promoID = pID;
  else promoID = xsite.getCheckedValue(form.promoID);

  if (!promoID) {
	alert("Please select a promo to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditPromo',
	title: 'Edit Promotion',
	width: 750,
	height: 500,
	position: 'center-up',
	url: 'index.cfm?md=promotion&tmp=snip_edit_promo&promoID=' + promoID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {//create date pickers for date fields
	  $(".dateField").datepicker();  
	},
	buttons: {
	  'Cancel': function() { $("#winEditPromo").dialog('close'); },
	  ' Save ': submitForm_EditPromo
	}
  }).dialog('open');
  
  function submitForm_EditPromo() {
	var form = $("#frmEditPromo");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=promotion&task=editPromo',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  beforeSubmit: validate,
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function validate() {
	  var form = $("#frmEditPromo")[0];
	  var found = false;
	  if (form.placementList.length) {
	    for (var i = 0; i < form.placementList.length; i++)
		  if (form.placementList[i].checked) found = true;
	  } else {
		  if (form.placementList.checked) found = true;
	  }
	  if (!found) {
		alert('Please select at least one placement for this promo.');
		return false;
	  }
	  if (form.destinationURL.value == "" && form.pageID.selectedIndex < 1) {
	    alert('Please enter a destination URL or select a page created by Page Builder.');
		return false;
	  }
	}
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditPromo").dialog('close');
		loadPromotionList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }	  
}

function deletePromo() {
  var form = $("#frmPromoList")[0];
  var promoID = xsite.getCheckedValue(form.promoID);
  if (!promoID) {
	alert("Please select a promo to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected promo?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=promotion&task=deletePromo&promoID=' + promoID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadPromotionList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	

}

function checkAll(form, placementID) {
  if (form.placementList.length) {
    for (var i =0; i < form.placementList.length; i++) {
	  if (form.placementList[i].value.indexOf(placementID+'|') == 0) form.placementList[i].checked=true;	  
    }
  }
}

function checkNone(form, placementID) {
  if (form.placementList.length) {
    for (var i =0; i < form.placementList.length; i++) {
	  if (form.placementList[i].value.indexOf(placementID+'|') == 0) form.placementList[i].checked=false;	  
    }
  }
}