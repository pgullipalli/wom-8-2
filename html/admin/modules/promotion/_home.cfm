<script type="text/javascript" src="modules/promotion/home.js"></script>

<cfset PM=CreateObject("component", "com.PromotionAdmin").init()>
<cfset placements=PM.getAllPlacements()>

<cfset Variables.placementID=PM.getFirstPlacementID()>

<div class="header">PROMOTION MANAGER</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addPromo();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editPromo(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deletePromo();">Delete</a>
</div>

<br /><br /><br />

<cfoutput>
<form id="frmPlacement">
  <div style="width:70px;padding:0px 2px 0px 0px;float:left;text-align:left;">
  Placement:
  </div>
  <div style="width:600px;padding:0px 2px 0px 0px;float:left;text-align:left;">
  <select name="placementID" style="min-width:300px;">
    <cfloop query="placements">
    <option value="#placements.placementID#"<cfif Variables.placementID IS placements.placementID> selected</cfif>>#placements.placementDescription#</option>
    </cfloop>
  </select>
  </div>
</form>
</cfoutput>
<br style="clear:both;" />

<div id="mainDiv"></div>