<cfparam name="URL.placementID">
<cfparam name="URL.catID">

<cfset PM=CreateObject("component", "com.PromotionAdmin").init()>
<cfset UT=CreateObject("component", "com.Utility").init()>

<cfset allPlacements=PM.getAllPlacements()>
<cfset allCats=StructNew()>
<cfset allCats["homepage"]=UT.getCategoriesByModuleName("subhome")>
<cfset allCats["pagebuilder"]=UT.getCategoriesByModuleName("pagebuilder")>
<cfset allCats["class"]=UT.getCategoriesByModuleName("class")>
<cfset allCats["newsroom"]=UT.getCategoriesByModuleName("newsroom")>
<cfset allCats["photogallery"]=UT.getCategoriesByModuleName("photogallery")>
<cfset allCats["directory"]=UT.getCategoriesByModuleName("directory")>
<cfset allCats["event"]=UT.getCategoriesByModuleName("event")>
    
<form name="frmAddPromo" id="frmAddPromo">
<div class="row">
	<div class="col-30-right"><b>Promo Name:</b></div>
    <div class="col-68-left">
      <input type="text" name="promoName" maxlength="50" value="" style="width:200px;" validate="required:true"> &nbsp;&nbsp;
      <input type="checkbox" name="published" value="1" /> Publish this promo
    </div>
</div>

<!--- all promo date will be default to 1900 - 3000 --->
<div class="row">
	<div class="col-30-right"><b>Start Date:</b></div>
    <div class="col-68-left">
      <cfoutput><input type="text" name="startDate" class="dateField" style="width:120px;" validate="required:false, date:true" /></cfoutput> &nbsp;&nbsp;&nbsp;
      <b>End Date:</b>
      <input type="text" name="endDate" class="dateField" style="width:120px;" validate="required:false, date:true" />
    </div>
</div>

<div class="row">
	<div class="col-30-right"><b>Promo Image:</b></div>
    <div class="col-68-left">
      <input type="hidden" name="promoImage" value="" />
      <CF_SelectFile formName="frmAddPromo" fieldName="promoImage" previewID="preview1" previewWidth="150">
      <div id="preview1"></div>
    </div>
</div>

<!--- <div class="row">
	<div class="col-30-right">Promo Image (Mouse-Over):</div>
    <div class="col-68-left">
      <input type="hidden" name="promoImageOver" value="" />
      <CF_SelectFile formName="frmAddPromo" fieldName="promoImageOver" previewID="preview2" previewWidth="150">
      <div id="preview2"></div>
    </div>
</div> --->

<div class="row">
	<div class="col-30-right">Description Text:<br /><small>(for placements where live text is associated with the promo)</small></div>
    <div class="col-68-left">
      <textarea name="descriptionText" style="width:300px;height:80px;"></textarea>
    </div>
</div>

<div class="row">
	<div class="col-30-right"><b>Destination URL:</b></div>
    <div class="col-68-left">
      External URL: <input type="text" name="destinationURL" maxlength="200" value="" style="width:300px;" /> - OR - <br />
    </div>
</div>

<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      Select a page created by Page Builder:<br />
	  <input type="hidden" name="pageID" value="0" />
	  <CF_SelectPage formName="frmAddPromo" fieldName="pageID">
    </div>
</div>

<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="popup" value="1" /> Open a new browser window when promo is clicked. 
    </div>
</div>

<div class="row">
	<div class="col-95-left"><b>Select placements and categories:</b> (Check all that apply.)</div>
</div>

<cfoutput query="allPlacements">
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <b>#placementDescription#</b><br /> 
      <cfset tempPlacementID=placementID>
      <cfif ListFind("class|*,event|*,homepage|subhome,newsroom|*,pagebuilder|home,photogallery|*","#module#|#template#") GT 0>
        <a href="javascript:checkAll(document.frmAddPromo, '#placementID#')">Check All</a> | <a href="javascript:checkNone(document.frmAddPromo,'#placementID#')">Check None</a>
        <div class="row">	
          <cfset qRef=allCats[module]>
          <cfloop query="qRef">
            <div class="col-48-left">
              <input type="checkbox" name="placementList" value="#tempPlacementID#|#catID#" <cfif Not Compare("#tempPlacementID#|#catID#","#URL.placementID#|#URL.catID#")>checked</cfif> /> #catName#
            </div>
          </cfloop>
        </div>        
      <cfelse>  
        <div class="row">
          <div class="col-48-left">
            <input type="checkbox" name="placementList" value="#tempPlacementID#|0" <cfif Not Compare("#tempPlacementID#|0","#URL.placementID#|#URL.catID#")>checked</cfif> /> All
          </div>
        </div>
      </cfif>
    </div>
</div>
</cfoutput>

</form>