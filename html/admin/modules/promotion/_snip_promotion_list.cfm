<cfparam name="URL.placementID">
<cfparam name="URL.catID" default="0">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="10">

<cfset PM=CreateObject("component", "com.PromotionAdmin").init()>
<cfset placementInfo=PM.getPlacementInfo(URL.placementID)>
<cfset UT=CreateObject("component", "com.Utility").init()>
<cfif placementInfo.module Is "homepage" AND placementInfo.template Is "subhome">
  <cfset allCats=UT.getCategoriesByModuleName("subhome")>
<cfelse>
  <cfif Compare(placementInfo.module,"*")>
    <cfset allCats=UT.getCategoriesByModuleName(placementInfo.module)>  
  <cfelse>
    <cfset allCats=QueryNew("catID,catName")><!--- empty query result set --->
  </cfif>
</cfif>

<cfoutput>
<form id="frmCategory">
  <div style="width:70px;padding:0px 2px 0px 0px;float:left;text-align:left;">
  Category:
  </div>
  <div style="width:600px;padding:0px 2px 0px 0px;float:left;text-align:left;">
  <select name="categoryID" style="min-width:300px;">
    <cfswitch expression="#placementInfo.module#|#placementInfo.template#">
      <cfcase value="class|*,event|*,homepage|subhome,newsroom|*,pagebuilder|home,photogallery|*">
		<cfif URL.catID Is "0"><cfset URL.catID="-1"></cfif>
        <option value="-1"<cfif URL.catID Is "-1"> selected</cfif>>All Categories</option>
        <cfloop query="allCats">
          <option value="#allCats.catID#"<cfif URL.catID Is allCats.catID> selected</cfif>>#allCats.catName#</option>
        </cfloop>
      </cfcase>
      <cfdefaultcase>
    	<option value="0">N/A</option>
      </cfdefaultcase>
    </cfswitch>
  </select>
  </div>
</form>
</cfoutput>

<br style="clear:both;" />

<cfset structItems=PM.getPromos(placementID=URL.placementID, categoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
<cfif structItems.numAllItems IS 0>
<p>
It appears that there are no promos created in this category.
Please click on the 'New' button in the action bar above to create the first promo.
</p>
</cfif>

<cfif structItems.numDisplayedItems GT 0>
<br /><br />
<form id="frmPromoList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th width="250">Promo Name</th>
    <th width="200">Promo Date</th>
    <th width="150">Publish?</th>
    <th>&nbsp;</th>
  </tr>  
  <cfoutput query="structItems.promos">
  <tr>
    <td align="center"><input type="radio" name="promoID" value="#promoID#"></td>
	<td align="center"><a href="##" onclick="editPromo('#promoID#')">#promoName#</a></td> 
    <td align="center">
      <cfset startDateStr=DateFormat(startDate,"mm/dd/yyyy")> <cfset endDateStr=DateFormat(endDate,"mm/dd/yyyy")>
      <cfif Compare(startDateStr, "01/01/1900")>#startDateStr#<cfelse>N/A</cfif>
      -
      <cfif Compare(endDateStr, "01/01/3000")>#endDateStr#<cfelse>N/A</cfif>      
    </td>
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
    <td>
      <cfif Compare(promoImage,"")>
        <a href="##" onclick="editPromo('#promoID#')"><img src="/#promoImage#" border="0" /></a>
      <cfelse>
        [No image]
      </cfif>
    </td>
  </tr>
  <!--- <tr align="center">
    <td colspan="4">
      <cfif Compare(promoImage,"")>
        <a href="##" onclick="editPromo('#promoID#')"><img src="/#promoImage#" border="0" /></a>
      <cfelse>
        [No image]
      </cfif>
    </td>
  </tr> --->
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif structItems.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structItems.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadPromotionList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>
