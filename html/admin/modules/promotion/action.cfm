<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  PM=CreateObject("component", "com.PromotionAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";
  
  switch (URL.task) {
    case "addPromo":
	  PM.addPromo(argumentCollection=Form);
	  break;
	  
	case "editPromo":
	  PM.editPromo(argumentCollection=Form);
	  break;
	  
	case "deletePromo":
	  PM.deletePromo(URL.promoID);
	  break;
	  
	default:
	  break;
  }
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>