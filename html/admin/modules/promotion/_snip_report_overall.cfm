<cfparam name="URL.fromDate">
<cfparam name="URL.toDate">
<cfparam name="URL.sortBy">
<cfparam name="URL.sortDirection">

<cfset PM=CreateObject("component", "com.PromotionAdmin").init()>
<cfset structReport=PM.getOverallReport(fromDate=URL.fromDate, toDate=URL.toDate, sortBy=URL.sortBy, sortDirection=URL.sortDirection)>

<style type="text/css">
.views {width:50px; height:15px; padding:0px 0px; margin:2px 2px; background-color:#EC6CB8;}
.click-thrus {width:50px; height:15px; padding:0px 0px; margin:2px 2px; background-color:#959494;}
.promoImage {display:block;}
</style>

<!---
	structResult
		.numPromos
		.maxCount
		.promoName[]
		.promoImage[]
		.published[]
		.views[]
		.clickthrus[]
--->

<div class="subHeader">
<cfoutput>Date Range: #URL.fromDate# - #URL.toDate#</cfoutput>
</div>

<div class="row">
Sort By <a href="javascript:loadReport('promoName')">Name</a> | <a href="javascript:loadReport('views')">Views</a> | <a href="javascript:loadReport('clickthrus')">Click-thrus</a>
</div>

<br style="clear:both;" /><br />

<div class="rows">
  <div class="views" style="float:left;"></div> <div style="float:left">Views &nbsp;</div>
  <div class="click-thrus" style="float:left;"></div> <div style="float:left">Click-thrus &nbsp;&nbsp;&nbsp;&nbsp;</div>
  <div style="float:left"><a href="javascript:toggleImages();">Show/Hide Images</a></div>
</div>

<br style="clear:both;" /><br />

<div class="itemList">
<table style="width:620px;">
  <tr>
	<th width="220">Promo</th>
    <th width="350">Views/Click-thrus</th>
    <th width="50">&nbsp;</th>
  </tr>  
  <cfset fullWidth=300>
  <cfset totalViews = 0>
  <cfset totalClicks = 0>
  <cfoutput>
  <cfloop index="idx" from="1" to="#structReport.numPromos#">
    <cfset viewWidth=Ceiling((fullWidth * structReport.views[idx])/structReport.maxCount)>
	<cfset clickThrusWidth=Ceiling((fullWidth * structReport.clickthrus[idx])/structReport.maxCount)>
  <tr>
	<td rowspan="2" align="center">
    <img src="/#structReport.promoImage[idx]#" border="0" class="promoImage" />
    <b>#structReport.promoName[idx]#</b><cfif Not structReport.published[idx]> (Inactive)</cfif>
    </td> 
    <td align="left">    
    <cfif viewWidth LT 1><cfset viewWidth=1></cfif>
    <div class="views" style="width:#viewWidth#px"></div>
    </td>
    <td align="right">#structReport.views[idx]#<cfset totalViews = totalViews + structReport.views[idx]></td>
  </tr>
  <tr>
    <td align="left">
    <cfif clickThrusWidth LT 1><cfset clickThrusWidth=1></cfif>
    <div class="click-thrus" style="width:#clickThrusWidth#px"></div>
    </td>
    <td align="right">#structReport.clickthrus[idx]#<cfset totalClicks = totalClicks + structReport.clickthrus[idx]></td>
  </tr>
  </cfloop>
  </cfoutput>
  <cfoutput>
  <tr>
    <td colspan="2" align="right">Total Views:<br />Total Click-thrus:</td>
    <td align="right"><b>#totalViews#<br />#totalClicks#</b></td>
  </tr>
  </cfoutput>
</table>
</div>