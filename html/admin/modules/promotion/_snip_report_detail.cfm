<cfparam name="URL.startDate">
<cfparam name="URL.promoID">

<cfset PM=CreateObject("component", "com.PromotionAdmin").init()>
<cfset promoInfo=PM.getPromo(URL.promoID)>
<cfset structReport=PM.getDetailReport(startDate=URL.startDate, promoID=URL.promoID)>

<!---
	structResult
		.numDays
		.monthInDate
		.maxCount
		.views[]
		.clickthrus[]
--->

<style type="text/css">
.views {width:50px; height:15px; padding:0px 0px; margin:2px 2px; background-color:#EC6CB8;}
.click-thrus {width:50px; height:15px; padding:0px 0px; margin:2px 2px; background-color:#959494;}
</style>


<div class="subHeader">
<cfoutput>
Month: #DateFormat(URL.startDate,"MMMM, yyyy")#
&nbsp;&nbsp;
Promo: #promoInfo.promoName#
</cfoutput>
</div>


<div class="row">
<cfoutput><img src="/#promoInfo.promoImage#" border="0"></cfoutput>
</div>

<br style="clear:both;" /><br />


<div class="itemList">
<table style="width:500px;">
  <tr>
	<th width="100">Date</th>
    <th width="350">Views/Click-thrus</th>
    <th width="50">&nbsp;</th>
  </tr>  
  <cfset fullWidth=300>
  <cfset totalViews = 0>
  <cfset totalClicks = 0>
  <cfoutput>
  <cfloop index="idx" from="#structReport.numDays#" to="1" step="-1">
  <cfset viewWidth=Ceiling((fullWidth * structReport.views[idx])/structReport.maxCount)>
  <cfset clickThrusWidth=Ceiling((fullWidth * structReport.clickthrus[idx])/structReport.maxCount)>
  <tr>
	<td rowspan="2" align="center">#structReport.statsDate[idx]#</td> 
    <td align="left">    
    <cfif viewWidth LT 1><cfset viewWidth=1></cfif>
    <div class="views" style="width:#viewWidth#px"></div>
    </td>
    <td align="right">#structReport.views[idx]#<cfset totalViews = totalViews + structReport.views[idx]></td>
  </tr>
  <tr>
    <td align="left">
    <cfif clickThrusWidth LT 1><cfset clickThrusWidth=1></cfif>
    <div class="click-thrus" style="width:#clickThrusWidth#px"></div>
    </td>
    <td align="right">#structReport.clickthrus[idx]#<cfset totalClicks = totalClicks + structReport.clickthrus[idx]></td>
  </tr>
  </cfloop>
  </cfoutput>
  <cfoutput>
  <tr>
    <td colspan="2" align="right">Total Views:<br />Total Click-thrus:</td>
    <td align="right"><b>#totalViews#<br />#totalClicks#</b></td>
  </tr>
  </cfoutput>
</table>
</div>

