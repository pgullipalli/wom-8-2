var pageNum=1;//initial page number

$(function () {
	loadMessageList();
});

function loadMessageList(pNum) {
  if (pNum) pageNum = pNum;
  //else pageNum=1;

  xsite.load("#mainDiv", "index.cfm?md=communication&tmp=snip_message_list&pageNum=" + pageNum + "&uuid=" + xsite.getUUID());
}

function addMessage() {  
  xsite.createModalDialog({
	windowName: 'winAddMessage',
	title: 'Create New Message',
	position: 'top',
	width: 750,
	url: 'index.cfm?md=communication&tmp=snip_add_message&uuid=' + xsite.getUUID(),
	urlCallback: function() {//create date pickers for date fields
	  $(".datePicker").datepicker(); 
	},
	buttons: {
	  'Cancel': function() { $("#winAddMessage").dialog('close'); },
	  ' Submit ': submitForm_AddMessage
	}
  }).dialog('open');
  
  function submitForm_AddMessage() {
	var form = $("#frmAddMessage");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=communication&task=addMessage',
		  type: 'post',
		  cache: false,
		  dataType: 'json',
		  beforeSubmit: function() { return validate($("#frmAddMessage")[0]); },
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddMessage").dialog('close');
		loadMessageList(pageNum);
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editMessage(messageID) {  
  xsite.createModalDialog({
	windowName: 'winEditMessage',
	title: 'Edit Message',
	position: 'top',
	width: 750,
	url: 'index.cfm?md=communication&tmp=snip_edit_message&messageID=' + messageID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {//create date pickers for date fields
	  $(".datePicker").datepicker(); 
	},
	buttons: {
	  'Cancel': function() { $("#winEditMessage").dialog('close'); },
	  ' Submit ': submitForm_EditMessage
	}
  }).dialog('open');
  
  function submitForm_EditMessage() {
	var form = $("#frmEditMessage");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=communication&task=editMessage',
		  type: 'post',
		  cache: false,
		  dataType: 'json',
		  beforeSubmit: function() { return validate($("#frmEditMessage")[0]); },
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditMessage").dialog('close');
		loadMessageList(pageNum);
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function copyMessage() {
  if (!document.getElementById("frmMessageList")) return;
  var form = $("#frmMessageList")[0];
  var messageID = xsite.getCheckedValue(form.messageID);
  if (!messageID) {
	alert("Please select a message to copy from.");
	return;
  }

  xsite.createModalDialog({
	windowName: 'winCopyMessage',
	title: 'Edit Message',
	position: 'top',
	width: 750,
	url: 'index.cfm?md=communication&tmp=snip_copy_message&messageID=' + messageID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {//create date pickers for date fields
	  $(".datePicker").datepicker(); 
	},
	buttons: {
	  'Cancel': function() { $("#winCopyMessage").dialog('close'); },
	  ' Submit ': submitForm_AddMessage
	}
  }).dialog('open');
  
  function submitForm_AddMessage() {
	var form = $("#frmCopyMessage");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=communication&task=addMessage',
		  type: 'post',
		  cache: false,
		  dataType: 'json',
		  beforeSubmit: function() { return validate($("#frmCopyMessage")[0]); },
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winCopyMessage").dialog('close');
		loadMessageList(pageNum);
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }	
}

function validate(form) {
  var found = false;
  var selectedLists="";
  if (form.sendToListIDsList.length) {
    for (i = 0; i < form.sendToListIDsList.length; i++) {
	  if (form.sendToListIDsList[i].checked) {
		found=true;
		selectedLists=selectedLists + "\n" + form["listName" + form.sendToListIDsList[i].value].value;
	  }
    }
  } else {
	if (form.sendToListIDsList.checked) {
	  found=true;
	  selectedLists=form["listName" + form.sendToListIDsList.value].value;
	}
  }
  if (!found) {
	alert ("Please select at least an e-mail list for this message.");  
	return false;
  }
  
  
  /*if (form.emailBodyHTML.value == "") {
    alert ("Please enter the \"HTML E-Mail Body.\"");
	return false;
  }*/
  
  if (form.emailTemplate.selectedIndex <= 0) {
    alert ("Please select an e-mail template.");
	form.emailTemplate.focus();
	return false;
  }
  
  /*if (form.emailBodyText.value == "") {
    alert ("Please enter the \"Text E-Mail Body.\"");
	form.emailBodyText.focus();
	return false;
  }*/
  
  if (!form.sendMode[0].checked && !form.sendMode[1].checked && !form.sendMode[2].checked) {
    alert ("Please select an option for when to send this campaign.");
	return false;
  }
  if (form.sendMode[1].checked) {	
    if (!confirm("You are about to send a new message to the following list(s):\n" + selectedLists + "\n\nAre you sure you want to proceed?")) return false;
  }
  
  if (form.sendMode[2].checked) {
    if (!xsite.checkDate(form.dateScheduled.value)) {
	  alert ("Please enter valid date for this campaign to be scheduled to send.");
	  form.dateScheduled.focus();
	  return false;
	}
	if (!xsite.checkInteger(form.timeScheduled_hh.value) || parseInt(form.timeScheduled_hh.value) < 1 ||  parseInt(form.timeScheduled_hh.value) > 12) {
	  alert ("Please enter valid hour for this campaign to be scheduled to send.");
	  form.timeScheduled_hh.focus();
	  return false;
	}
	if (!xsite.checkInteger(form.timeScheduled_mm.value) || parseInt(form.timeScheduled_mm.value) < 0 ||  parseInt(form.timeScheduled_mm.value) > 59) {
	  alert ("Please enter valid minute for this campaign to be scheduled to send.");
	  form.timeScheduled_mm.focus();
	  return false;
	}
  }
	  
  return true;	
}

function deleteMessage(messageID) { 
  if (!confirm('Are you sure you want to delete this message?')) {
	return;
  } else {
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=communication&task=deleteMessage&messageID=' + messageID, {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadMessageList(pageNum);
	} else {

	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function toggleMessageArchive() {
  if (!document.getElementById("frmMessageList")) return;
  var form = $("#frmMessageList")[0];
  var messageID = xsite.getCheckedValue(form.messageID);
  if (!messageID) {
	alert("Please select a message to archive/de-archive.");
	return;
  }
  
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=communication&task=toggleMessageArchive&messageID=' + messageID, {}, processJson, 'json');}});

  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadMessageList(pageNum);
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function viewMessageStatus(messageID) {
  xsite.createModalDialog({
	windowName: 'winViewMessageStatus',
	title: 'View Message Status',
	position: 'center-top',
	width: 500,
	url: 'index.cfm?md=communication&tmp=snip_viewmessagestatus' + '&messageID=' + messageID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'OK': function() { $("#winViewMessageStatus").dialog('close'); }
	}
  }).dialog('open');
}

function previewHTML (template, formName) {  
  if (template == "") {
    alert ("Please select a template to apply to the HTML e-mail body.");
    return;
  }
  xsite.openWindow('index.cfm?md=communication&tmp=previewhtmlemail&template=' + escape(template) + '&formName=' + formName, 'preview', 820, 700);	
}

function extractTextFromHTMLBody (form) {
	var finalText = myRegexpReplace(form.emailBodyHTML.value, ">", "> ", "gi");//pad a space after every tag
	finalText = myRegexpReplace(finalText, "<", " <", "gi");//pad a space before every tag
	finalText = myRegexpReplace(finalText, "<[p|P]", "\r\n\r\n<p", "gi");//pad a newline before a p tag
	finalText = myRegexpReplace(finalText, "<[h|H]", "\r\n\r\n<h", "gi");//pad a newline before a h tag
	//finalText = myRegexpReplace(finalText, "<br", "\r\n<br", "gi");	
	finalText = myRegexpReplace(finalText, "<[^<]*>", "", "gi");//remove all tags
	finalText = myRegexpReplace(finalText, "[\t]", " ", "gi");//replace tab by space
	finalText = myRegexpReplace(finalText, "&nbsp;", " ", "gi");//spaces
	finalText = myRegexpReplace(finalText, "&amp;", "&", "gi");//spaces	
	finalText = myRegexpReplace(finalText, "(&ldquo;|&rdquo;|&quot;)", "\"", "gi");//double quote
	finalText = myRegexpReplace(finalText, "(&rsquo;|&lsquo;)", "'", "gi");//single quote
	finalText = myRegexpReplace(finalText, "&hellip;", "...", "gi");//...	
	finalText = myRegexpReplace(finalText, "(&ndash;|&mdash;)", "...", "gi");//-	
	finalText = myRegexpReplace(finalText, " [ ]+", " ", "gi");//compress spaces
	finalText = myRegexpReplace(finalText, "(\r\n){3,}", "\r\n\r\n", "gi");//compress newlines					
	finalText = myRegexpReplace(finalText, "[ ]+\r\n", "\r\n", "gi");//remove trailing spaces for each line
	finalText = myRegexpReplace(finalText, "\r\n[ ]+", "\r\n", "gi");//remove leading spaces for each line
	finalText = myRegexpReplace(finalText, "^[^A-Za-z0-9_]+", "", "gi");//remove all leading white spaces from the beginning	
	form.emailBodyText.value = finalText;
}

function myRegexpReplace(in_str, reg_exp, replace_str, opts) {
	if (typeof opts == "undefined")
		opts = 'g';
	var re = new RegExp(reg_exp, opts);
	return in_str.replace(re, replace_str);
}