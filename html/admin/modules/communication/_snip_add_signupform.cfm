<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>

<cfset allForms=FM.getAllFormNames()>
<cfset allLists=CM.getAllListNames()>

<form id="frmAddSignUpForm" name="frmAddSignUpForm" onsubmit="return false;">
<div class="row">
  <div class="col-38-right"><b>Sign-Up Form Name:</b></div>
  <div class="col-58-left"><input type="text" name="signUpFormName" size="40" maxlength="150" validate="required:true" /></div>
</div>
<div class="row">
  <div class="col-38-right"><input type="checkbox" name="published" value="1" checked /></div>
  <div class="col-58-left">Publish this sign-up form</div>
</div>
<cfif allForms.recordcount GT 0>
<div class="row">
  <div class="col-38-right">Use a form created by "Form Manager":</div>
  <div class="col-58-left">
    <select name="formID" style="min-width:250px;">
      <option value="0"></option>
      <cfoutput query="allForms">
        <option value="#formID#">#formName#</option>
      </cfoutput>
    </select>
  </div>
</div>
</cfif>
<div class="row">
  <div class="col-38-right">E-Mail From Name:</div>
  <div class="col-58-left"><input type="text" name="emailFromName" size="40" maxlength="100" /></div>
</div>
<div class="row">
  <div class="col-38-right"><b>E-Mail From Address:</b></div>
  <div class="col-58-left">
    <cfoutput>
    <input type="text" name="emailFromAddress" size="40" maxlength="100" value="#Application.communication.emailDefaultFromAddress#" validate="email:true, required:true" />
    <br /><small>(Used for double opt-in e-mail)</small>
    </cfoutput>
  </div>
</div>
<div class="row">
  <div class="col-38-right"><b>E-Mail Reply Address:</b></div>
  <div class="col-58-left">
    <cfoutput>
    <input type="text" name="emailReplyAddress" size="40" maxlength="100" value="" validate="email:true, required:true" />
    <br /><small>(Used for double opt-in e-mail)</small>
    </cfoutput>
  </div>
</div>
<div class="row">
  <div class="col-38-right">Text appear above the listing of e-mail lists:</div>
  <div class="col-58-left">
    <textarea name="headerText" cols="40" rows="3">Select lists to subscribe</textarea>
  </div>
</div>
<div class="row">
  <div class="col-38-right">Lists included in this sign-up form:</div>
  <div class="col-58-left">
    <cfoutput query="allLists">
    <input type="checkbox" name="listIDList" value="#listID#" /> #listName#<br />
    </cfoutput>
  </div>
</div>
</form>