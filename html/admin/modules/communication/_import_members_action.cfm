<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>

<div class="header">COMMUNICATION MANAGER &gt; Import Members Using CSV Data</div>

<script type="text/javascript">
  var displayPercentage;
  var displayImportStatus;
  
  function updateStatus(perc) {
    $("#statusBar").css({'width' : perc + '%'});
	displayPercentage.innerHTML = perc + " %";
  }
</script>

<br />
<br />

<table border="0" cellpadding="0" cellspacing="0" width="400" align="center">
  <tr valign="middle">
    <td width="350">
    <div style="width:100%;height:20px;border:1px solid #555555;"><div id="statusBar" style="width:0%;height:20px;background-color:#555555;"></div></div>
    </td>
    <td width="50">&nbsp;&nbsp;
    <span id="percentage">0 %</span>
    </td>
  </tr>
  <tr valign="top"><td colspan="2">&nbsp;</td></tr>
  <tr valign="top">
    <td colspan="2" align="center"><span id="importStatus"></span></td>
  </tr>
</table>

<script type="text/javascript">
  displayPercentage = document.getElementById("percentage");
  displayImportStatus = document.getElementById("importStatus"); 
</script>

<cfif IsDefined("Form.CSVDataFile")><!--- update data file --->
  <cfset Form.absoluteDirectoryTemp=Application.siteDirectoryRoot & "/admin/modules/communication/temp">
  <cfset CM.importMembersFromFile(argumentCollection=Form)>
<cfelseif IsDefined("Form.CSVData")>
  <cfset CM.importMembersFromData(argumentCollection=Form)>
</cfif>

<br /><br />