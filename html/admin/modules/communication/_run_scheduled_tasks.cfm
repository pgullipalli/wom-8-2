<div class="header">COMMUNICATION MANAGER &gt; Trigger Scheduled Tasks</div>

<cfset CMSK=CreateObject("component", "com.CommunicationScheduledTasks").init()>
<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>

<cfif CMSK.isProcessInProgress()>
	<p>The emailing process is either in progress or has locked up. Futhur investigation is needed.</p>
<cfelse>
	<cfset CMSK.updateProcessStatus(1)>
  
	<!--- BEGIN: update campaign status --->
	<cfset CMSK.scanAndUpdateCampaignStatus()>
    <p>"scanAndUpdateCampaignStatus" complete</p>
	<!--- END: update campaign status --->
	
	<!--- //////////////////////////////////////////////////////////////////////////////////////////// --->
	
	<!--- BEGIN: push e-mails in cm_emailqueue to mail server --->
	<cfset CMSK.processEmailQueue()>
    <p>"processEmailQueue" complete</p>
	<!--- END: push e-mails in cm_emailqueue to mail server --->
	
	<!--- //////////////////////////////////////////////////////////////////////////////////////////// --->
	
	<!--- BEGIN: process scheduled send messages --->
	<cfset message=CMSK.getFirstScheduledSendMessage()>
	<cfif message.recordcount GT 0>
	  <cfset CM.moveMessageToEmailQueue(message.messageID)>
      <p>"moveMessageToEmailQueue" complete</p>
	</cfif>
	<!--- END: process scheduled send messages --->
	
	<!--- //////////////////////////////////////////////////////////////////////////////////////////// --->
	
	<!--- BEGIN: pop bounce-back emails; update table cm_bounce --->
	<cfset CMSK.parseBounceBackEmails()>
    <p>"parseBounceBackEmails" complete</p>
	<!--- END: pop bounce-back emails; update table cm_bounce --->
	
	<!--- //////////////////////////////////////////////////////////////////////////////////////////// --->
	
	<!--- BEGIN: check table cm_bouncebackresendschedule; push bounce back e-mails to e-mail queue (cm_emailqueue) --->
	<cfset CMSK.resendBounceBackMails()>
    <p>"resendBounceBackMails" complete</p>
	<!--- END: check table cm_bouncebackresendschedule; push bounce back e-mails to e-mail queue (cm_emailqueue) --->
	
	<!--- //////////////////////////////////////////////////////////////////////////////////////////// --->
	
	<!--- BEGIN: update email status in each list --->
	<cfset CMSK.updateStatusBasedOnBounceNumber()>
    <p>"updateStatusBasedOnBounceNumber" complete</p>
	<!--- END: update email status in each list --->
	
	<!--- //////////////////////////////////////////////////////////////////////////////////////////// --->
	
	<!--- BEGIN: purge e-mail campaign stats --->
	<cfset CMSK.purgeCampaignStats()>
    <p>"purgeCampaignStats" complete</p>
	<!--- END: purge e-mail campaign stats --->  
  
    <cfset CMSK.updateProcessStatus(0)>
    
    <p><br /><br /><b>DONE!!</b></p>
</cfif>