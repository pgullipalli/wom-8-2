<cfparam name="messageID">

<div class="header">COMMUNICATION MANAGER &gt; View Campaign Status Detail</div>

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset message=CM.getMessage(URL.messageID)>

<cfoutput>
<p>
<b>Mesage: #message.messageName#
<cfif message.status IS "P">
  (has not been processed)
<cfelseif message.status IS "S">
  (has been sent)
<cfelseif message.status IS "Q">
  (e-mails delivery is in progress...)
</cfif>
</b>
</p>

<cfset messageListIDsList=CM.getMessageListIDsList(URL.messageID)>
<cfif ListLen(messageListIDsList) IS NOT 0>
  <cfset messageEmailList=CM.getListsSummaryInfo(messageListIDsList)>
  <p>
  <b>This campaign was set up to send to the following e-mail list(s):</b><br><br>
  <cfloop query="messageEmailList">
  #listName# - #numSubscribers# subscribers<br>
  </cfloop>
  </p>
</cfif>

<cfset sendHistory=CM.getSendHistory(URL.messageID)>

<cfloop query="sendHistory">
  <p>
  <cfif attemptNum IS 1>
    First attempt
  <cfelseif attemptNum IS 2>
    Second attempt
  <cfelse>
    Third attempt
  </cfif>
  sent at #DateFormat(dateSent, "mm/dd/yyyy")# #TimeFormat(dateSent, "hh:mm:ss tt")#<br>
  Number of e-mails sent: #numSent#<br>
  Number of e-mails bounced back: #numBounces#
  </p>
</cfloop>
</cfoutput>