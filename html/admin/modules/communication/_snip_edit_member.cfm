<cfparam name="URL.listID" type="numeric">
<cfparam name="URL.emailID" type="numeric">

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset memberInfo=CM.getMemberInfo(emailID=URL.emailID, listID=URL.listID)>

<cfoutput query="memberInfo">
<form id="frmEditMember" name="frmEditMember" onSubmit="return false;">
<input type="hidden" name="listID" value="#URL.listID#">
<input type="hidden" name="emailID" value="#URL.emailID#">
<div class="row">
  <div class="col-38-right"><b>E-Mail Address:</b></div>
  <div class="col-58-left"><input type="text" name="email" size="40" maxlength="100" value="#HTMLEditFormat(email)#" validate="email:true, required:true" /></div>
</div>
<div class="row">
  <div class="col-38-right">Name:</div>
  <div class="col-24-left">
  <input type="text" name="firstName" size="17" maxlength="50" value="#HTMLEditFormat(firstName)#"><br />
  <small>(First Name)</small>
  </div>
  <div class="col-24-left">
  <input type="text" name="lastName" size="17" maxlength="50" value="#HTMLEditFormat(lastName)#"><br />
  <small>(LastName Name)</small>
  </div>
</div>
<div class="row">
  <div class="col-38-right"><b>Status:</b></div>
  <div class="col-58-left">
    <select name="status">
      <option value="S"<cfif status Is "S"> selected</cfif>>Subscribed</option>
      <option value="P"<cfif status Is "P"> selected</cfif>>Pending</option>
      <option value="U"<cfif status Is "U"> selected</cfif>>Unsubscribed</option>
      <option value="B"<cfif status Is "B"> selected</cfif>>Bad</option>
    </select><br />
    <small>(note: no double opt-in e-mail will be sent)</small>
  </div>
</div>
</form>
</cfoutput>