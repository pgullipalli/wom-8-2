<cfparam name="URL.fromDate">
<cfparam name="URL.toDate">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="10">

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>

<cfset resultStruct=CM.getCampaignReport(fromDate=URL.fromDate, toDate=URL.toDate, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
<cfset dateToPurge=DateAdd("d", (-1 * Application.communication.daysToKeepCampaignStats), now())>

<!---
resultStruct
	.numAllItems
	.numDisplayedItems
	.campaign[numDisplayedItems]
		.messageID
		.messageName
		.numEmailsOpened
		.numClickThru
		.emailList[]
		.numEmailsSent[3]
		.dateSent[3]
		.numBounces[3]
--->

<cfif resultStruct.numAllItems Is 0>
  <p>There are no messages were sent during the selected date range.</p>
<cfelse>
  <cfset startIdx=(URL.pageNum - 1) * URL.numItemsPerPage + 1>
  <cfset endIdx=URL.pageNum * URL.numItemsPerPage>
  <cfif endIdx GT resultStruct.numAllItems><cfset endIdx=resultStruct.numAllItems></cfif>
  <p><cfoutput>Display #startIdx# - #endIdx# of #resultStruct.numAllItems# Campaigns</cfoutput></p>

  <div class="itemList">
  <table>
    <tr>
  	  <th>Message</th>		
      <th width="300">Email List(s)</th>
	  <th width="150">Date Sent</th>
      <th width="150"># Emails Sent</th>
	  <th width="135"># Bounces</th>
      <th width="120"># Opened</th>
      <!--- <th># Forwarded</th> --->
	  <th width="120"># Clicks</th>
    </tr>
    <cfoutput>
    <cfloop index="idx" from="1" to="#resultStruct.numDisplayedItems#">
    <tr>
      <td>
        <a href="javascript:xsite.openWindow('index.cfm?md=communication&tmp=viewcampaigncontent&messageID=#resultStruct.campaign[idx].messageID#','viewcampaign', 800, 600)" class="accent01">#resultStruct.campaign[idx].messageName#</a>
      </td>
      <td>
        <ul style="padding:0px 10px;">
          <cfloop index="idx1" from="1" to="#ArrayLen(resultStruct.campaign[idx].emailList)#">
          <li>#resultStruct.campaign[idx].emailList[idx1]#</li>
          </cfloop>
        </ul>
      </td>
      <td>
        <cfset showStatsLink=1>
        <cfloop index="idx1" from="1" to="#ArrayLen(resultStruct.campaign[idx].dateSent)#">
          #idx1#: #resultStruct.campaign[idx].dateSent[idx1]#<br>
          <cfif idx1 IS 1 AND DateCompare(dateToPurge, resultStruct.campaign[idx].dateSent[idx1]) IS "1">
            <cfset showStatsLink=0>
          </cfif>
        </cfloop>
      </td>
      <td>
        <cfloop index="idx1" from="1" to="#ArrayLen(resultStruct.campaign[idx].numEmailsSent)#">
          #idx1#: #resultStruct.campaign[idx].numEmailsSent[idx1]#<br>
        </cfloop>
      </td>
      <td>
        <cfloop index="idx1" from="1" to="#ArrayLen(resultStruct.campaign[idx].numBounces)#">
          #idx1#: #resultStruct.campaign[idx].numBounces[idx1]#<br>
        </cfloop>
      </td>
      <td align="center">
        <cfif resultStruct.campaign[idx].numEmailsOpened GT 0 And showStatsLink>
          <a href="javascript:viewEmailsOpened('#resultStruct.campaign[idx].messageID#')" class="accent01">#resultStruct.campaign[idx].numEmailsOpened#</a>
        <cfelse>
          #resultStruct.campaign[idx].numEmailsOpened#
        </cfif>
      </td>
      <!--- <td align="center">
        <cfif resultStruct.campaign[idx].numEmailsForwarded GT 0 AND showStatsLink>
          <a href="javascript:viewEmailsForwarded('#resultStruct.campaign[idx].messageID#')"  class="accent01">#resultStruct.campaign[idx].numEmailsForwarded#</a>
        <cfelse>
          #resultStruct.campaign[idx].numEmailsForwarded#
        </cfif>
      </td> --->
      <td align="center">
        <cfif resultStruct.campaign[idx].numClickThru GT 0 AND showStatsLink>
          <a href="javascript:viewURLClickThru('#resultStruct.campaign[idx].messageID#')" class="accent01">#resultStruct.campaign[idx].numClickThru#</a>
        <cfelse>
          #resultStruct.campaign[idx].numClickThru#
        </cfif>	
      </td>
    </tr>
    </cfloop>
    </cfoutput>
  </table>
  </div>
  
  <cfif resultStruct.numAllItems GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(resultStruct.numAllItems/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadReport(#idx#)">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
  </cfif>
</cfif>