<cfparam name="URL.messageID">

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset messageInfo=CM.getMessage(URL.messageID)>
<cfset clickThruInfo=CM.getClickThruByMessageID(URL.messageID)>

<cfoutput>
<p><b>Campaign Name:</b> #messageInfo.messageName# (Date Created: #DateFormat(messageInfo.dateCreated, "mm/dd/yyyy")#)</p>
</cfoutput>

<cfif clickThruInfo.recordcount GT 0>
  <div class="itemList">
  <table>
    <tr>
	  <th width="400">Web Link</th>
	  <th># Clicks</th>
    </tr>
    <cfoutput query="clickThruInfo">
    <tr>
      <td>#linkURL#</td>
      <td align="center">
        <a href="javascript:viewEmailsClickThru('#URL.messageID#', '#messageClickThruID#')" class="accent01">#totalClicks#</a>
	  </td>
    </tr>
    </cfoutput>
  </table>
  </div>
</cfif>