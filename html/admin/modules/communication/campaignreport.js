var pageNum=1;//initial page number

$(function () {
	loadReport();
});

function loadReport(pNum) {
  if (pNum) pageNum = pNum;
  else pageNum=1;
  var dateRangeForm=document.frmDateRange;
  var fromDate=dateRangeForm.fromDate.value;
  var toDate=dateRangeForm.toDate.value;
  
  if (!xsite.checkDate(fromDate) || !xsite.checkDate(toDate)) {
    alert('Please enter date range in correct date format, for example, 3/24/2009');
	return;
  }
  
  var url="index.cfm?md=communication&tmp=snip_campaignreport_list&fromDate=" + escape(fromDate) + "&toDate=" + escape(toDate) +
  		  "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url, function() {
	$("#fromDate").datepicker();  
	$("#toDate").datepicker();  
  });
}

function viewEmailsOpened(messageID, pNum) {
  var pgNum = 1;
  if (pNum) pgNum = pNum;  
  
  if ($("#winViewEmailsOpened").length > 0 && $("#winViewEmailsOpened").dialog( 'isOpen' )) $("#winViewEmailsOpened").dialog('close'); 
  
  window.location.hash="top";
  xsite.createModalDialog({
	windowName: 'winViewEmailsOpened',
	title: 'Emails Opened',
	position: 'center-top',
	width: 700,
	height:500,
	url: 'index.cfm?md=communication&tmp=snip_viewemailsopened' + '&messageID=' + messageID + '&pageNum=' + pgNum + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'OK': function() { $("#winViewEmailsOpened").dialog('close'); }
	}
  }).dialog('open');
}

function viewEmailsForwarded(messageID, pNum) {
  var pgNum = 1;
  if (pNum) pgNum = pNum;  
  
  if ($("#winViewEmailsForwarded").length > 0 && $("#winViewEmailsForwarded").dialog( 'isOpen' )) $("#winViewEmailsForwarded").dialog('close'); 
  
  window.location.hash="top";
  xsite.createModalDialog({
	windowName: 'winViewEmailsForwarded',
	title: 'Emails Forwarded',
	position: 'center-top',
	width: 700,
	height:500,
	url: 'index.cfm?md=communication&tmp=snip_viewemailsforwarded' + '&messageID=' + messageID + '&pageNum=' + pgNum + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'OK': function() { $("#winViewEmailsForwarded").dialog('close'); }
	}
  }).dialog('open');
}

function viewURLClickThru(messageID) {  
  if ($("#winViewURLClickThru").length > 0 && $("#winViewURLClickThru").dialog( 'isOpen' )) $("#winViewURLClickThru").dialog('close'); 
  if ($("#winViewEmailsClickThru").length > 0 && $("#winViewEmailsClickThru").dialog( 'isOpen' )) $("#winViewEmailsClickThru").dialog('close'); 

  window.location.hash="top";
  xsite.createModalDialog({
	windowName: 'winViewURLClickThru',
	title: 'Web Link Click-Thrus',
	position: 'center-top',
	width: 600,
	height:400,
	url: 'index.cfm?md=communication&tmp=snip_viewurlclickthru' + '&messageID=' + messageID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'OK': function() { $("#winViewURLClickThru").dialog('close'); }
	}
  }).dialog('open');
}

function viewEmailsClickThru(messageID, messageClickThruID, pNum) {
  var pgNum = 1;
  if (pNum) pgNum = pNum;  
  
  if ($("#winViewEmailsClickThru").length > 0 && $("#winViewEmailsClickThru").dialog( 'isOpen' )) $("#winViewEmailsClickThru").dialog('close'); 
  if ($("#winViewURLClickThru").length > 0 && $("#winViewURLClickThru").dialog( 'isOpen' )) $("#winViewURLClickThru").dialog('close'); 
  
  window.location.hash="top";
  xsite.createModalDialog({
	windowName: 'winViewEmailsClickThru',
	title: 'Web Link Click-Thrus',
	position: 'center-top',
	width: 700,
	height:500,
	url: 'index.cfm?md=communication&tmp=snip_viewemailsclickthru' + '&messageID=' + messageID + '&messageClickThruID=' + messageClickThruID + '&pageNum=' + pgNum + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'OK': function() { $("#winViewEmailsClickThru").dialog('close'); }
	}
  }).dialog('open');
}
