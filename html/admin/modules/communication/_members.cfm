<cfparam name="URL.listID" default="0">

<script type="text/javascript" src="modules/communication/members.js"></script>

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset allLists=CM.getAllListNames()>

<cfif URL.listID IS 0>
  <cfset URL.listID=CM.getFirstListID()>
  <cfif URL.listID IS 0><!--- no Lists were ever created --->
    <cflocation url="index.cfm?md=communication&tmp=emaillists&wrap=1">
  </cfif>
</cfif>

<div class="header">COMMUNICATION MANAGER &gt; Members</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addMember();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editMember();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteMembers();">Delete</a>
<cfif allLists.recordcount GT 1>
<a href="#" id="btnImport" class="fg-button ui-state-default ui-corner-all button180" onclick="importMembers();">Import To Other List</a>
</cfif>
</div>

<div id="divSubMenuImport" style="position:absolute;display:none;">
<table class="subMenu">
  <cfoutput query="allLists">
  <tr valign="top">
    <td><a href="javascript:importMembers('#listID#')">#listName#</a></td>
  </tr>
  </cfoutput>
</table>
</div>

<div style="clear:both;"><br /></div>

<cfoutput>
<form id="frmNavigator">
  <b>List</b>
  <select name="listID" style="min-width:300px;">
    <cfloop query="allLists">
    <option value="#allLists.listID#"<cfif URL.listID IS allLists.listID> selected</cfif>>#listName#</option>
    </cfloop>
  </select><br />
</form>
</cfoutput>

<div id="mainDiv"></div>