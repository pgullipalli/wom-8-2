<div class="header">COMMUNICATION MANAGER &gt; Mail Logs</div>

<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset resultStruct=CM.getMessages(pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>

<cfif resultStruct.numAllItems IS NOT 0>
  <cfset startIdx=(URL.pageNum - 1) * URL.numItemsPerPage + 1>
  <cfset endIdx=URL.pageNum * URL.numItemsPerPage>
  <cfif endIdx GT resultStruct.numAllItems><cfset endIdx=resultStruct.numAllItems></cfif>
  <p><cfoutput>Display #startIdx# - #endIdx# of #resultStruct.numAllItems#</cfoutput></p>
  
  <div class="itemList">
  <table>
    <tr>
  	  <th width="80">Logs</th>		
      <th width="250">Message</th>
      <th>Date Composed</th>
	  <th>Date Sent</th>
	  <th>Scheduled to Send</th>
	  <th>Status</th>
    </tr>
    <cfoutput query="resultStruct.messages">
    <tr align="center">
      <td><a href="javascript:xsite.openWindow('index.cfm?md=communication&tmp=viewlog&messageID=#messageID#&wrap=1','viewlog',800,500)"><img src="images/icon_view.gif" border="0" alt="View Delivery Logs"></a></a></td>
      <td align="left">#messageName#</td>
      <td>#DateFormat(dateCreated, "mm/dd/yyyy")# #TimeFormat(dateCreated, "hh:mm:ss tt")#</td>
      <td>
        <cfif Compare(dateFirstSend,"")>#DateFormat(dateFirstSend, "mm/dd/yyyy")# #TimeFormat(dateFirstSend, "hh:mm:ss tt")#<cfelse>has not been sent</cfif>
      </td>
      <td>
        <cfif NOT Compare(sendMode, "S") AND Compare(dateScheduledSend, "")>
          #DateFormat(dateScheduledSend, "mm/dd/yyyy")# #TimeFormat(dateScheduledSend, "hh:mm:ss tt")#
		<cfelse>
          &nbsp;
		</cfif>
      </td>
      <td>
        <cfif NOT Compare(Status, "Q")>
	      <span class="hilite"><i>In Progress</i></span>
        <cfelseif NOT Compare(Status, "P")>
	      <b>Pending</b>
	    <cfelse>
	      Sent
	    </cfif>
	      <a href="javascript:xsite.openWindow('index.cfm?md=communication&tmp=viewcampaignstatusdetail&wrap=1&messageID=#messageID#', 'viewcampaignstatusdetail', 500,500)"><img src="images/icon_view.gif" border="0" alt="View detail" align="absmiddle"></a>
      </td>
    </tr>
    </cfoutput>
  </table>
  </div>
  
  
  <cfif resultStruct.numAllItems GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(resultStruct.numAllItems/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="index.cfm?md=communication&tmp=maillog&pageNum=#idx#&wrap=1">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
  </cfif>
</cfif>