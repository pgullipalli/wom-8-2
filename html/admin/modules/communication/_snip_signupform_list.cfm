<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset allSignUpForms=CM.getSignUpFormsSummary()>

<cfif allSignUpForms.recordcount Is 0>
<p>
It appears that there are no sign-up forms created.
Please click on the 'New' button in the action bar above to create the first sign-up form.
</p>
<cfelse>


<form id="frmSignUpFormList" name="frmSignUpFormList">
<div class="itemList">
<table>
  <tr>
  	<th width="40">&nbsp;</th>		
    <th width="250">Sign-Up Form Name</hd>
	<th width="100">Published?</th>
	<th width="100"># Lists Included</th>
	<th width="200">Form Used</th>
    <th>URL</th>
  </tr>
  <cfoutput query="allSignUpForms">
  <tr align="center">
    <td><input type="radio" name="signUpFormID" value="#signUpFormID#"></td>
    <td align="left"><a href="javascript:editSignUpForm('#signUpFormID#')" class="accent01">#signUpFormName#</a></td>
    <td><cfif published>Yes<cfelse>No</cfif></td>
    <td><cfif numLists GT 0><a href="javascript:showIncludedLists('#signUpFormID#')" class="accent01">#numLists#</a><cfelse>#numLists#</cfif></td>
    <td><cfif Compare(formName,"")>#formName#<cfelse>None</cfif></td>
    <td><a href="/index.cfm?md=communication&tmp=signup&sfmid=#signUpFormID#" target="_blank">/index.cfm?md=communication&amp;tmp=signup&amp;sfmid=#signUpFormID#</a></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>