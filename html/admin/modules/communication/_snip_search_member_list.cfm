<cfparam name="URL.keyword" default="">
<cfparam name="URL.status" default="">
<cfparam name="URL.listIDList" default="">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset resultStruct=CM.searchMembers(keyword=URL.keyword, status=URL.status, listIDList=URL.listIDList, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>

<cfif resultStruct.numAllItems GT 0>
  <cfset startIdx=(URL.pageNum - 1) * URL.numItemsPerPage + 1>
  <cfset endIdx=URL.pageNum * URL.numItemsPerPage>
  <cfif endIdx GT resultStruct.numAllItems><cfset endIdx=resultStruct.numAllItems></cfif>
  <p><cfoutput>Display #startIdx# - #endIdx# of #resultStruct.numAllItems# Members</cfoutput></p>
<cfelse>
  <p>Your search returns no results.</p>
</cfif>

<cfif resultStruct.numDisplayedItems GT 0>
<form id="frmMemberList" name="frmMemberList" onsubmit="return false;">
<div class="itemList">
<table>
  <tr>
	<th width="70">
      <a href="javascript:xsite.checkAllBoxes(document.frmMemberList.emailAndListID)"><b>All</b></a> &nbsp;
	  <a href="javascript:xsite.uncheckAllBoxes(document.frmMemberList.emailAndListID)"><b>None</b></a>
    </th>
	<th>E-Mail</th>
	<th>First Name</th>
	<th>Last Name</th>
	<th width="120">Status</th>
    <th width="120">Bounces</th>
  </tr>
  <cfset listIDTemp = 0>
  <cfoutput query="resultStruct.members">
  <cfif Compare(listIDTemp, listID)>
  <tr>
	<td colspan="6"><b><i>#listName#</i></b></td>
  </tr>
  </cfif>
  <cfset listIDTemp=listID>
  <tr>
    <td align="center"><input type="checkbox" name="emailAndListID" value="#emailID#|#listID#"></td>
    <td>#email#</td>
    <td>#firstName#&nbsp;</td>
    <td>#lastName#&nbsp;</td>
    <td align="center">
      <cfswitch expression="#status#">
	    <cfcase value="P">Pending</cfcase>
		<cfcase value="S">Subscribed</cfcase>
		<cfcase value="U">Unsubscribed</cfcase>
		<cfcase value="B"><a href="javascript:showBounceMessage('#listID#', '#emailID#','B');" title="show bounce-back message">Bad</a></cfcase>
	  </cfswitch>
    </td>
    <td align="center">
      <cfif numBouncedCampaigns IS 0>
	    0
	  <cfelse>
	    <a href="javascript:showBounceMessage('#listID#','#emailID#','');" title="show bounce-back message">#numBouncedCampaigns#</a>
	  </cfif>
    </td>
  </tr>
  </cfoutput>
</table>
</div>
</form>

<cfif resultStruct.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(resultStruct.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadMemberList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>

</cfif>
