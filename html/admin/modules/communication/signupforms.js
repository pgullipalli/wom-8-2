$(function () {
	loadSignUpFormList();
});

function loadSignUpFormList() {
  xsite.load("#mainDiv", "index.cfm?md=communication&tmp=snip_signupform_list&uuid=" + xsite.getUUID());
}

function addSignUpForm() {
  xsite.createModalDialog({
	windowName: 'winAddSignUpForm',
	title: 'Add Sign-Up Form',
	position: 'center-up',
	width: 600,
	url: 'index.cfm?md=communication&tmp=snip_add_signupform&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddSignUpForm").dialog('close'); },
	  ' Add ': submitForm_AddSignUpForm
	}
  }).dialog('open');
  
  function submitForm_AddSignUpForm() {
	var form = $("#frmAddSignUpForm");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=communication&task=addSignUpForm',
		  type: 'post',
		  cache: false,
		  dataType: 'json',
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddSignUpForm").dialog('close');
		loadSignUpFormList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}


function editSignUpForm(sID) {
  if (!document.getElementById("frmSignUpFormList")) return;
  var form = $("#frmSignUpFormList")[0];
  var signUpFormID = null;
  if (sID) signUpFormID=sID;
  else signUpFormID = xsite.getCheckedValue(form.signUpFormID);
  
  if (!signUpFormID) {
	alert("Please select a sign-up form to edit.");
	return;
  }
  
  xsite.createModalDialog({
	windowName: 'winEditSignUpForm',
	title: 'Edit Sign-Up Form',
	position: 'center-up',
	width: 650,
	url: 'index.cfm?md=communication&tmp=snip_edit_signupform&signUpFormID=' + signUpFormID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditSignUpForm").dialog('close'); },
	  ' Save ': submitForm_EditSignUpForm
	}
  }).dialog('open');
  
  function submitForm_EditSignUpForm() {
	var form = $("#frmEditSignUpForm");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=communication&task=editSignUpForm',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditSignUpForm").dialog('close');
		loadSignUpFormList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}


function deleteSignUpForm() {
  if (!document.getElementById("frmSignUpFormList")) return;
  var form = $("#frmSignUpFormList")[0];
  var signUpFormID = xsite.getCheckedValue(form.signUpFormID);
  if (!signUpFormID) {
	alert("Please select a sign-up form to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected sign-up form?')) {
	return;
  } else {
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=communication&task=deleteSignUpForm&signUpFormID=' + signUpFormID, {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadSignUpFormList();
	} else {

	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function showIncludedLists(signUpFormID) {
  xsite.createModalDialog({
	windowName: 'winShowLists',
	title: 'Lists Included In Sign-Up Form',
	position: 'center-up',
	width: 500,
	url: 'index.cfm?md=communication&tmp=snip_signupform_emaillist_list&signUpFormID=' + signUpFormID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'OK': function() { $("#winShowLists").dialog('close'); }
	}
  }).dialog('open');
	
}
