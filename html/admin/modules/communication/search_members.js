var pageNum=1;//initial page number

$(function () {
	//loadMemberList();
	   
	$("body").click(function(e) { 
	  if (e.target.id != "btnImport" && e.target.id != "divSubMenuImport") $("#divSubMenuImport").hide();
	});
});


function loadMemberList(pNum) {
  if (pNum) pageNum = pNum;
  else pageNum=1;
  
  var form=document.frmSearch;
  var keyword=form.keyword.value;
  var status=form.status.options[form.status.selectedIndex].value;
  var listIDList=xsite.getCheckedValue(form.listID);
  if (!listIDList) {
	alert("Please select at least one list.");
	return;
  }
  
  xsite.load("#mainDiv", "index.cfm?md=communication&tmp=snip_search_member_list&keyword=" + escape(keyword) + "&status=" + status + "&listIDList=" + escape(listIDList) + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID(), function(){window.location.hash="searchResults";});
}


function editMember() {
  if (!document.getElementById("frmMemberList")) return;
  var form = $("#frmMemberList")[0];
  var emailAndListID = xsite.getCheckedValue(form.emailAndListID);
  if (!emailAndListID) {
	alert("Please select a member to edit.");
	return;
  }
  
  if (emailAndListID.indexOf(",") != -1) {
	alert("Please select only one member to move to edit.");
	return;
  }
  
  var emailAndListIDParts=emailAndListID.split("|");
  if (emailAndListIDParts.length != 2) return;
  var emailID=emailAndListIDParts[0];
  var listID=emailAndListIDParts[1];
  
  xsite.createModalDialog({
	windowName: 'winEditMember',
	title: 'Edit Member',
	position: 'center-up',
	width: 650,
	url: 'index.cfm?md=communication&tmp=snip_edit_member&listID=' + listID + '&emailID=' + emailID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditMember").dialog('close'); },
	  ' Save ': submitForm_EditMember
	}
  }).dialog('open');
  
  function submitForm_EditMember() {
	var form = $("#frmEditMember");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=communication&task=editMember',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditMember").dialog('close');
		loadMemberList(pageNum);
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteMembers() {
  if (!document.getElementById("frmMemberList")) return;
  var form = $("#frmMemberList")[0];
  var emailAndListID = xsite.getCheckedValue(form.emailAndListID);
  if (!emailAndListID) {
	alert("Please select members to delete.");
	return;
  }
  
  var emailAndListIDs=emailAndListID.split(",");
  var emailAndListIDParts=emailAndListIDs[0].split("|");
  
  var emailIDList=emailAndListIDParts[0];;
  var listID=emailAndListIDParts[1];
  
  for (i=1; i < emailAndListIDs.length; i++) {
    emailAndListIDParts=emailAndListIDs[i].split("|");
    if (listID != emailAndListIDParts[1]) {
	  alert("Please select only members in the same list to delete.");
	  return;
	} else {
	  emailIDList = emailIDList + "," + emailAndListIDParts[0];
	}
  }
  
  if (!confirm('Are you sure you want to delete the selected members?')) {
	return;
  } else {
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=communication&task=deleteMembers&listID=' + listID + '&emailIDList=' + escape(emailIDList), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadMemberList(pageNum);
	} else {

	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function importMembers(newListID) {
  if (!document.getElementById("frmMemberList")) return;
  
  var form = $("#frmMemberList")[0];  
  var emailAndListID = xsite.getCheckedValue(form.emailAndListID);
  if (!emailAndListID) {
	alert("Please select members to import to other list.");
	return;
  }
  
  var emailAndListIDs=emailAndListID.split(",");
  var emailAndListIDParts=emailAndListIDs[0].split("|");
  
  var emailIDList=emailAndListIDParts[0];;
  var listID=emailAndListIDParts[1];
  
  if(newListID) {
	$("#divSubMenuImport").hide();  
	
	for (i=1; i < emailAndListIDs.length; i++) {
      emailAndListIDParts=emailAndListIDs[i].split("|");
      if (listID != emailAndListIDParts[1]) {
	    alert("Please select only members in the same list to import to other list.");
	    return;
	  } else {
	    emailIDList = emailIDList + "," + emailAndListIDParts[0];
	  }
    }

	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=communication&task=importMembers&listID=' + listID + '&newListID=' + newListID + '&emailIDList=' + escape(emailIDList), {}, processJson, 'json');}});
  } else {
    var offset = $("#btnImport").offset();
    var height=$("#actionBar").height() + 2;
    $("#divSubMenuImport").css({'left':offset.left+'px', 'top':(offset.top+height)+'px'}).toggle();  
  }  
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadMemberList(pageNum);
	} else {

	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function showBounceMessage(listID, emailID, status) {
  xsite.createModalDialog({
	windowName: 'winViewBounceMessage',
	title: 'View Bounce Back Message',
	position: 'center-up',
	width: 500,
	url: 'index.cfm?md=communication&tmp=snip_view_bounce_message&listID=' + listID + '&emailID=' + emailID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'OK': function() { $("#winViewBounceMessage").dialog('close'); }
	}
  }).dialog('open');
}