<cfparam name="URL.messageID">
<cfparam name="URL.messageClickThruID">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="30">

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset messageInfo=CM.getMessage(URL.messageID)>
<cfset linkURL=CM.getURLByMessageClickThruID(URL.messageClickThruID)>
<cfset resultStruct=CM.getEmailsByClickThru(messageID=URL.messageID, linkURL=linkURL, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>


<cfoutput>
<p><b>Campaign Name:</b> #messageInfo.messageName# (Date Created: #DateFormat(messageInfo.dateCreated, "mm/dd/yyyy")#)</p>
<p><b>Web Link:</b> #linkURL#</p>
</cfoutput>


<cfif resultStruct.numDisplayedItems GT 0>
  <cfset startIdx=(URL.pageNum - 1) * URL.numItemsPerPage + 1>
  <cfset endIdx=URL.pageNum * URL.numItemsPerPage>
  <cfif endIdx GT resultStruct.numAllItems><cfset endIdx=resultStruct.numAllItems></cfif>
  <p><cfoutput>Display #startIdx# - #endIdx# of #resultStruct.numAllItems# Email Addresses</cfoutput></p>
  
  <div class="itemList">
  <table>
    <tr>
	  <th width="235">E-mail Address</th>
	  <th>Name</th>
	  <th># Clicks</th>
	  <th>Date Last Clicked</th>
    </tr>
    <cfoutput query="resultStruct.members">
    <tr>
      <td>#email#</td>
      <td>#firstName#&nbsp;#lastName#</td>
      <td align="center">#numClicks#</td>
      <td>#DateFormat(dateLastClicked, "mm/dd/yyyy")# #TimeFormat(dateLastClicked, "hh:mm:ss tt")#</td>
    </tr>
    </cfoutput>
  </table>
  </div>
  
  <cfif resultStruct.numAllItems GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(resultStruct.numAllItems/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="viewEmailsClickThru('#URL.messageID#','#URL.messageClickThruID#',#idx#)">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
  </cfif>
</cfif>

<cfoutput>
<p align="right">
<a href="javascript:viewURLClickThru('#URL.messageID#')"><img src="images/icon_back.gif" border="0" alt="click to go back"></a>
</p>
</cfoutput>
