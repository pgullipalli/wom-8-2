<cfparam name="URL.signUpFormID">
<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset FM=CreateObject("component", "com.FormAdmin").init()>

<cfset signUpFormInfo=CM.getSignUpForm(URL.signUpFormID)>
<cfset signUpFormLists=CM.getSignFormLists(URL.signUpFormID)>
<cfset allForms=FM.getAllFormNames()>
<cfset allLists=CM.getAllListNames()>

<cfoutput query="signUpFormInfo">
<form id="frmEditSignUpForm" name="frmEditSignUpForm" onsubmit="return false;">
<input type="hidden" name="signUpFormID" value="#URL.signUpFormID#" />
<div class="row">
  <div class="col-38-right"><b>Sign-Up Form Name:</b></div>
  <div class="col-58-left"><input type="text" name="signUpFormName" size="40" maxlength="150" value="#HTMLEditFormat(signUpFormName)#" validate="required:true" /></div>
</div>
<div class="row">
  <div class="col-38-right"><input type="checkbox" name="published" value="1" <cfif published>checked</cfif> /></div>
  <div class="col-58-left">Publish this sign-up form</div>
</div>
<cfif allForms.recordcount GT 0>
<div class="row">
  <div class="col-38-right">Use a form created by "Form Manager":</div>
  <div class="col-58-left">
    <select name="formID" style="min-width:250px;">
      <option value="0"></option>
      <cfloop query="allForms">
        <option value="#allForms.formID#"<cfif signUpFormInfo.formID Is allForms.formID> selected</cfif>>#allForms.formName#</option>
      </cfloop>
    </select>
  </div>
</div>
</cfif>
<div class="row">
  <div class="col-38-right">Default E-Mail From Name:</div>
  <div class="col-58-left"><input type="text" name="emailFromName" size="40" value="#HTMLEditFormat(emailFromName)#" maxlength="100" /></div>
</div>
<div class="row">
  <div class="col-38-right"><b>Default E-Mail From Address:</b></div>
  <div class="col-58-left">
    <input type="text" name="emailFromAddress" size="40" maxlength="100" value="#HTMLEditFormat(emailFromAddress)#" validate="email:true, required:true" />
    <br /><small>(Used for double opt-in e-mail)</small>
  </div>
</div>
<div class="row">
  <div class="col-38-right"><b>E-Mail Reply Address:</b></div>
  <div class="col-58-left">
    <cfoutput>
    <input type="text" name="emailReplyAddress" size="40" maxlength="100" value="#HTMLEditFormat(emailReplyAddress)#" validate="email:true, required:true" />
    <br /><small>(Used for double opt-in e-mail)</small>
    </cfoutput>
  </div>
</div>
<div class="row">
  <div class="col-38-right">Text appear above the listing of e-mail lists:</div>
  <div class="col-58-left">
    <textarea name="headerText" cols="40" rows="3">#HTMLEditFormat(headerText)#</textarea>
  </div>
</div>
<div class="row">
  <div class="col-38-right">Lists included in this sign-up form:</div>
  <div class="col-58-left">
    <cfloop query="allLists">
    <input type="checkbox" name="listIDList" value="#allLists.listID#" <cfif ListFind(ValueList(signUpFormLists.listID), allLists.listID) GT 0>checked</cfif> /> #allLists.listName#<br />
    </cfloop>
  </div>
</div>
</form>
</cfoutput>