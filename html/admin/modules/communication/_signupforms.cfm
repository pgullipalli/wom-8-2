<script type="text/javascript" src="modules/communication/signupforms.js"></script>

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset allLists=CM.getAllListNames()>

<cfif allLists.recordcount IS 0>
  <cflocation url="index.cfm?md=communication&tmp=emaillists&wrap=1"><!--- no Lists were ever created --->
</cfif>

<div class="header">COMMUNICATION MANAGER &gt; Sign-Up Forms</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addSignUpForm()">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editSignUpForm()">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteSignUpForm()">Delete</a>
</div>

<div style="clear:both;"></div>

<div id="mainDiv"></div>