<cfparam name="URL.listID">

<form id="frmAddMember" name="frmAddMember" onSubmit="return false;">
<cfoutput>
<input type="hidden" name="listID" value="#URL.listID#">
</cfoutput>
<div class="row">
  <div class="col-38-right"><b>E-Mail Address:</b></div>
  <div class="col-58-left"><input type="text" name="email" size="40" maxlength="100" validate="email:true, required:true" /></div>
</div>
<div class="row">
  <div class="col-38-right">Name:</div>
  <div class="col-24-left">
  <input type="text" name="firstName" size="17" maxlength="50"><br />
  <small>(First Name)</small>
  </div>
  <div class="col-24-left">
  <input type="text" name="lastName" size="17" maxlength="50"><br />
  <small>(LastName Name)</small>
  </div>
</div>
<div class="row">
  <div class="col-38-right"><b>Status:</b></div>
  <div class="col-58-left">
    <select name="status">
      <option value="S">Subscribed</option>
      <option value="P">Pending</option>
      <option value="U">Unsubscribed</option>
      <option value="B">Bad</option>
    </select><br />
    <small>(note: no double opt-in e-mail will be sent)</small>
  </div>
</div>
</form>