<script type="text/javascript" src="modules/communication/import_members.js"></script>

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset allLists=CM.getAllListNames()>

<cfif allLists.recordcount IS 0>
  <cflocation url="index.cfm?md=communication&tmp=emaillists&wrap=1"><!--- no Lists were ever created --->
</cfif>

<div class="header">COMMUNICATION MANAGER &gt; Import Members Using CSV Data</div>

<p>
A CSV (Comma Separated Values) file is a specially formatted plain text file which stores spreadsheet or basic database-style information in a very simple format,
with one record on each line, and each field within that record separated by a comma.
</p>

<p>
<b>The CSV Format:</b>
<ul>
  <li>Each record is one line</li>
  <li>Fields are separated with commas.</li>
  <li>Embedded commas - Field must be delimited with double-quotes.</li>
  <li>Embedded double-quotes - Embedded double-quote characters must be doubled, and the field must be delimited with double-quotes.</li>
</ul>
</p>

<p>
To import a list of members to a selected list, you can either upload the CSV file to the remote server, or enter the CSV data directly into the text box, . 
</p>

<p>
In order to import multiple e-mail addresses into the list you've selected, please follow these specific <b>formatting guidelines</b>:
<ul>
  <li>
  Create a comma delimited text (.txt) file with this information for each member, in this order (email, first name, last name) and in this format:<br />
  <blockquote>
  john@yahoo.com,John,Smith<br />
  joe@gmail.com,Joe,"Doe, Jr."<br />
  michael@hotmail.com,,<br />
  lisa@gmail.com,,Williams
  </blockquote>
  </li>
  <li>Make sure that each record in the file is separated by carriage (hard) return. (Hit "Enter" between each record.)</li>
  <li>For better performance, please limit your file size to no more than 2,000 records per file.</li>
</ul>
</p>

<cfoutput>
<form id="frmNavigator">
  <b>Select a list you wish to import members to:</b>
  <select name="listID" style="min-width:300px;">
    <cfloop query="allLists">
    <option value="#allLists.listID#">#listName#</option>
    </cfloop>
  </select><br />
</form>
</cfoutput>

<p>
<form id="frmCSVFile" name="frmCSVFile" action="index.cfm?md=communication&tmp=import_members_action&wrap=1" method="post" enctype="multipart/form-data">
<input type="hidden" name="listID" value="0">
<b>Select your CSV data file from your computer:</b>
<input type="hidden" name="localFileField" value="CSVDataFile">
<input type="file" name="CSVDataFile" size="30"> <input type="button" value=" Upload &amp; Import " onClick="importMembers(document.frmCSVFile);">
</form>
</p>

<p>OR</p>

<p>
<form id="frmCSVData" name="frmCSVData" action="index.cfm?md=communication&tmp=import_members_action&wrap=1"  method="post">
<input type="hidden" name="listID" value="0">
<b>Enter your CSV data here:</b><br />
<textarea name="CSVData" cols="80" rows="5"></textarea> <input type="button" value=" Import " onClick="importMembers(document.frmCSVData);">
</form>
</p>

<br /><br />