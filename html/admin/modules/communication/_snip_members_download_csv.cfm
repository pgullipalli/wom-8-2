<cfparam name="URL.fileName">

<cfset absoluteFilePath="#Application.siteDirectoryRoot#/admin/modules/communication/temp/" & URL.fileName>

<cfheader name="Content-Disposition" value="attachment; filename=#URL.fileName#">
<cfcontent file="#absoluteFilePath#" deletefile="yes" type="xyz/dummy">