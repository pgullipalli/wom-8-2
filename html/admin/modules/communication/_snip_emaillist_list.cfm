<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset allLists=CM.getListsSummaryInfo()>

<cfif allLists.recordcount Is 0>
<p>
It appears that there are no lists created.
Please click on the 'New' button in the action bar above to create the first list.
</p>
<cfelse>
<p>The table below displays all e-mail lists you have created using Commmunication Manager, along with the number of current active list subscribers.</p>
<form id="frmEmailLists" name="frmEmailLists">
<div class="itemList">
<table>
  <tr>
  	<th width="40">&nbsp;</th>		
    <th width="250">List Name</th>
	<th>Published?</th>
	<th># Members</th>
	<th># Subscribed</th>
	<th>Generate CSV</th>
  </tr>
  <cfoutput query="allLists">
  <tr align="center">
    <td><input type="radio" name="listID" value="#listID#"></td>
    <td align="left"><a href="index.cfm?md=communication&tmp=members&listID=#listID#&wrap=1" class="accent01">#listName#</a></td>
    <td><cfif published>Yes<cfelse>No</cfif></td>
    <td>#numMembers#</td>
    <td>#numSubscribers#</td>
    <td><a href="javascript:generateCSV('#listID#')" class="accent01">&gt;&gt;</a></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>