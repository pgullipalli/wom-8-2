var pageNum=1;//initial page number
var sortBy='email';
var order='ASC';
$(function () {
	loadMemberList();
	
	$("#frmNavigator select[name='listID']").change(function() {
	  pageNum=1;
	  loadMemberList();	  
	});
	   
	$("body").click(function(e) { 
	  if (e.target.id != "btnImport" && e.target.id != "divSubMenuImport") $("#divSubMenuImport").hide();
	});
});

function getListID() {
  return $("#frmNavigator select[name='listID']").val();
}

function loadMemberList(pNum) {
  if (pNum) pageNum = pNum;
  else pageNum=1;
  var listID=getListID();
  xsite.load("#mainDiv", "index.cfm?md=communication&tmp=snip_member_list&listID=" + listID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID());
}

function sortMembers(s, o) {
  sortBy=s;
  order=o;
  pageNum = 1;
  var listID=getListID();
  xsite.load("#mainDiv", "index.cfm?md=communication&tmp=snip_member_list&listID=" + listID + "&sortBy=" + sortBy + "&order=" + order + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID());
}

function addMember() {
  var listID=getListID();
  
  xsite.createModalDialog({
	windowName: 'winAddMember',
	title: 'Add Member',
	position: 'center-up',
	width: 550,
	url: 'index.cfm?md=communication&tmp=snip_add_member&listID=' + listID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddMember").dialog('close'); },
	  ' Add ': submitForm_AddMember
	}
  }).dialog('open');
  
  function submitForm_AddMember() {
	var form = $("#frmAddMember");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=communication&task=addMember',
		  type: 'post',
		  cache: false,
		  dataType: 'json',
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddMember").dialog('close');
		loadMemberList(pageNum);
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editMember() {
  if (!document.getElementById("frmMemberList")) return;
  var listID=getListID();
  var form = $("#frmMemberList")[0];
  var emailID = xsite.getCheckedValue(form.emailID);
  if (!emailID) {
	alert("Please select a member to edit.");
	return;
  }
  
  if (emailID.indexOf(",") != -1) {
	alert("Please select only one member to edit.");
	return;
  }
  
  xsite.createModalDialog({
	windowName: 'winEditMember',
	title: 'Edit Member',
	position: 'center-up',
	width: 650,
	url: 'index.cfm?md=communication&tmp=snip_edit_member&listID=' + listID + '&emailID=' + emailID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditMember").dialog('close'); },
	  ' Save ': submitForm_EditMember
	}
  }).dialog('open');
  
  function submitForm_EditMember() {
	var form = $("#frmEditMember");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=communication&task=editMember',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditMember").dialog('close');
		loadMemberList(pageNum);
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteMembers() {
  if (!document.getElementById("frmMemberList")) return;
  var listID=getListID();
  var form = $("#frmMemberList")[0];
  var emailIDList = xsite.getCheckedValue(form.emailID);
  if (!emailIDList) {
	alert("Please select members to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected members?')) {
	return;
  } else {
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=communication&task=deleteMembers&listID=' + listID + '&emailIDList=' + escape(emailIDList), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadMemberList(pageNum);
	} else {

	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function importMembers(newListID) {
  if (!document.getElementById("frmMemberList")) return;
  var listID=getListID();
  
  var form = $("#frmMemberList")[0];  
  var emailIDList = xsite.getCheckedValue(form.emailID);
  if (!emailIDList) {
	alert("Please select members to import to other list.");
	return;
  }
  
  if(newListID) {
	$("#divSubMenuImport").hide();  
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=communication&task=importMembers&listID=' + listID + '&newListID=' + newListID + '&emailIDList=' + escape(emailIDList), {}, processJson, 'json');}});
  } else {
    var offset = $("#btnImport").offset();
    var height=$("#actionBar").height() + 2;
    $("#divSubMenuImport").css({'left':offset.left+'px', 'top':(offset.top+height)+'px'}).toggle();  
  }  
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadMemberList(pageNum);
	} else {

	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function showBounceMessage(listID, emailID, status) {
  xsite.createModalDialog({
	windowName: 'winViewBounceMessage',
	title: 'View Bounce Back Message',
	position: 'center-up',
	width: 500,
	url: 'index.cfm?md=communication&tmp=snip_view_bounce_message&listID=' + listID + '&emailID=' + emailID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'OK': function() { $("#winViewBounceMessage").dialog('close'); }
	}
  }).dialog('open');
}
