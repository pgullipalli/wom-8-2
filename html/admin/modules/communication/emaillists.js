$(function () {
	loadEmailLists();
});

function loadEmailLists() {
  xsite.load("#mainDiv", "index.cfm?md=communication&tmp=snip_emaillist_list&uuid=" + xsite.getUUID());
}

/*--------------------------------*/
/*    BEGIN: Add List             */
/*--------------------------------*/
function addList() {
  xsite.createModalDialog({
	windowName: 'winAddList',
	title: 'Add List',
	position: 'center-up',
	width: 550,
	url: 'index.cfm?md=communication&tmp=snip_add_list&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddList").dialog('close'); },
	  ' Add ': submitForm_AddList
	}
  }).dialog('open');
  
  function submitForm_AddList() {
	var form = $("#frmAddList");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=communication&task=addList',
		  type: 'post',
		  cache: false,
		  dataType: 'json',
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddList").dialog('close');
		loadEmailLists();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------*/
/*    END: Add List               */
/*--------------------------------*/

/*--------------------------------*/
/*    BEGIN: Edit List            */
/*--------------------------------*/
function editList() {
  if (!document.getElementById("frmEmailLists")) return;
  var form = $("#frmEmailLists")[0];
  var listID = xsite.getCheckedValue(form.listID);
  if (!listID) {
	alert("Please select a list to edit.");
	return;
  }
  
  xsite.createModalDialog({
	windowName: 'winEditList',
	title: 'Edit List',
	position: 'center-up',
	width: 650,
	url: 'index.cfm?md=communication&tmp=snip_edit_list&listID=' + listID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditList").dialog('close'); },
	  ' Save ': submitForm_EditList
	}
  }).dialog('open');
  
  function submitForm_EditList() {
	var form = $("#frmEditList");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=communication&task=editList',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditList").dialog('close');
		loadEmailLists();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------*/
/*    END: Edit List              */
/*--------------------------------*/

/*--------------------------------*/
/*  BEGIN: Delete List            */
/*--------------------------------*/
function deleteList() {
  if (!document.getElementById("frmEmailLists")) return;
  var form = $("#frmEmailLists")[0];
  var listID = xsite.getCheckedValue(form.listID);
  if (!listID) {
	alert("Please select a list to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected list? Please be aware that when a list is deleted, the list\'s members will also be deleted.')) {
	return;
  } else {
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=communication&task=deleteList&listID=' + listID, {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadEmailLists();
	} else {

	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function listMembers() {
  if (!document.getElementById("frmEmailLists")) return;
  var form = $("#frmEmailLists")[0];
  var listID = xsite.getCheckedValue(form.listID);
  if (!listID) {
	alert("Please select a list.");
	return;
  }
  
  window.location="index.cfm?md=communication&tmp=members&listID=" + listID + "&wrap=1" + "&uuid=" + xsite.getUUID();
}

function generateCSV(listID) {
  if (!confirm("Are you sure you want to generate a CSV file for this list? The CSV generation process may take up to several minutes.")) return;
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=communication&task=generateCSV&listID=' + listID
													+ "&uuid=" + xsite.getUUID(), {}, processJson, 'json');}});
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  var fileName = jsonData.FILENAME;
	  window.open('index.cfm?md=communication&tmp=snip_members_download_csv&fileName=' + escape(fileName) + '&uuid=' + xsite.getUUID(),'_self');	
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}