<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
CM=CreateObject("component", "com.CommunicationAdmin").init();
returnedStruct=StructNew();
returnedStruct.SUCCESS=true;
returnedStruct.MESSAGE="";

switch (URL.task) {
  case "addList":
    CM.addList(argumentCollection=Form);  
    break;
	
  case "editList":
    CM.editList(argumentCollection=Form); 
    break;

  case "deleteList":
    CM.deleteList(URL.listID);
    break;

  case "addMember":
    returnedStruct=CM.addMember(argumentCollection=Form);
    break;

  case "editMember":
    returnedStruct=CM.editMember(argumentCollection=Form);
    break;
		
  case "deleteMembers":
    CM.deleteMembers(listID=URL.listID, emailIDList=URL.emailIDList);
	break;

  case "importMembers":
    CM.importMembers(listID=URL.listID, newListID=URL.newListID, emailIDList=URL.emailIDList);
    break;

  case "addSignUpForm":
    returnedStruct=CM.addSignUpForm(argumentCollection=Form);
    break;
	
  case "editSignUpForm":
    returnedStruct=CM.editSignUpForm(argumentCollection=Form);
    break;
	
  case "deleteSignUpForm":
    CM.deleteSignUpForm(signUpFormID=URL.signUpFormID);
    break;
	
  case "addMessage":
    returnedStruct=CM.addMessage(argumentCollection=Form);
    break;
  
  case "editMessage":
    returnedStruct=CM.editMessage(argumentCollection=Form);
    break;
	
  case "deleteMessage":
    CM.deleteMessage(messageID=URL.messageID);
    break;
	
  case "toggleMessageArchive":
    CM.toggleMessageArchive(messageID=URL.messageID);
    break;
	
  case "generateCSV":
    CSVDirectory="#Application.siteDirectoryRoot#/admin/modules/communication/temp";
    fileName=CM.generateCSV(URL.listID, CSVDirectory);
	returnedStruct.FILENAME="#fileName#";
	break;

  default:
    break;
}
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>