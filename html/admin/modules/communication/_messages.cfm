<script type="text/javascript" src="modules/communication/messages.js"></script>

<div class="header">COMMUNICATION MANAGER &gt; Message Center</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addMessage()">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="copyMessage()">Copy</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button115" onclick="toggleMessageArchive()">Archive/De-Archive</a>
</div>

<div style="clear:both;"><br /></div>

<p align="right">
<img src="images/icon_refresh.gif" border="0" align="absmiddle" hspace="5"><a href="javascript:loadMessageList();">Refresh Status</a>
&nbsp;&nbsp;&nbsp;&nbsp;
</p>

<div id="mainDiv"></div>