<cfparam name="URL.messageID">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="30">

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset resultStruct=CM.getEmailsByMessageOpened(messageID=URL.messageID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
<cfset messageInfo=CM.getMessage(URL.messageID)>

<cfoutput>
<p><b>Campaign Name:</b> #messageInfo.messageName# (Date Created: #DateFormat(messageInfo.dateCreated, "mm/dd/yyyy")#)</p>
</cfoutput>

<cfif resultStruct.numDisplayedItems GT 0>
  <cfset startIdx=(URL.pageNum - 1) * URL.numItemsPerPage + 1>
  <cfset endIdx=URL.pageNum * URL.numItemsPerPage>
  <cfif endIdx GT resultStruct.numAllItems><cfset endIdx=resultStruct.numAllItems></cfif>
  <p><cfoutput>Display #startIdx# - #endIdx# of #resultStruct.numAllItems# Email Addresses</cfoutput></p>

  <div class="itemList">
  <table>
    <tr>
	  <th width="235">E-mail Address</th>
	  <th>Name</th>
	  <th>Date Opened</th>
    </tr>
    <cfoutput query="resultStruct.members">
    <tr>
      <td>#email#</td>
      <td>#firstName#&nbsp;#lastName#</td>
      <td>#DateFormat(dateOpen, "mm/dd/yyyy")# #TimeFormat(dateOpen, "hh:mm:ss tt")#</td>
    </tr>
    </cfoutput>
  </table>
  </div>
  
  <cfif resultStruct.numAllItems GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(resultStruct.numAllItems/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="viewEmailsOpened('#URL.messageID#',#idx#)">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
  </cfif>
</cfif>