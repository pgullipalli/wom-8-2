<script type="text/javascript" src="modules/communication/search_members.js"></script>

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset allLists=CM.getAllListNames()>

<cfif allLists.recordcount IS 0>
  <cflocation url="index.cfm?md=communication&tmp=emaillists&wrap=1"><!--- no Lists were ever created --->
</cfif>

<div class="header">COMMUNICATION MANAGER &gt; Search Members</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editMember()">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteMembers()">Delete</a>
<cfif allLists.recordcount GT 1>
<a href="#" id="btnImport" class="fg-button ui-state-default ui-corner-all button180" onclick="importMembers()">Import To Other List</a>
</cfif>
</div>

<div id="divSubMenuImport" style="position:absolute;display:none;">
<table class="subMenu">
  <cfoutput query="allLists">
  <tr valign="top">
    <td><a href="javascript:importMembers('#listID#')">#listName#</a></td>
  </tr>
  </cfoutput>
</table>
</div>

<div style="clear:both;"><br /></div>

<form name="frmSearch" id="frmSearch" onSubmit="return false;">
<b>Keyword:</b> <input type="text" name="keyword" id="keyword" size="20" value=""> &nbsp;
<b>Status:</b>
<select name="status">
  <option value="">All</option>
  <option value="S">Subsribed</option>
  <option value="P">Pending</option>
  <option value="U">Unsubscribed</option>
  <option value="B">Bad</option>
</select><br />

<p><b>Search against the following selected lists:</b></p>

<p>
Check <a href="javascript:xsite.checkAllBoxes(document.frmSearch.listID)">All</a> |
<a href="javascript:xsite.uncheckAllBoxes(document.frmSearch.listID)">None</a><br />

<cfoutput query="allLists">
<input type="checkbox" name="listID" value="#listID#"> #listname#<br />
</cfoutput>
</p>
<p><button onClick="loadMemberList();"> Search </button></p>
</form>

<a name="searchResults"></a>
<div id="mainDiv"></div>
<br /><br /><br /><br />
