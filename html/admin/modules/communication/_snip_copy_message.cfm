<cfparam name="URL.messageID">

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset message=CM.getMessage(URL.messageID)>
<cfset messageListIDsList=CM.getMessageListIDsList(URL.messageID)>
<cfset allLists = CM.getListsSummaryInfo()>
<cfset allTemplates=CM.getAllEmailTemplates()>
<cfset allSignUpForms=CM.getSignUpFormsSummary()>

<cfoutput query="message">
<form id="frmCopyMessage" name="frmCopyMessage" onSubmit="return false">
<input type="hidden" name="emailBodyStyleSheetPath" value="#Application.communication.emailBodyStyleSheetPath#" />

<br />
<div class="row">
  <div class="col-38-right"><b>New Message Name: </b></div>
  <div class="col-58-left">
    <input type="text" name="messageName" size="60" maxlength="100" validate="required:true" /><br />
	* for your internal reference
  </div>
</div>

<div class="row">
  <div class="col-38-right"><b>E-Mail Subject Line: </b></div>
  <div class="col-58-left"><input type="text" name="emailSubject" size="60" maxlength="100" validate="required:true" value="#HTMLEditFormat(emailSubject)#" /></div>
</div>
<div class="row">
  <div class="col-38-right">E-Mail From Name: </div>
  <div class="col-58-left"><input type="text" name="emailFromName" size="30" maxlength="100" value="#HTMLEditFormat(emailFromName)#" /></div>
</div>
<div class="row">
  <div class="col-38-right"><b>E-Mail From Address: </b></div>
  <div class="col-58-left">
    <input type="text" name="emailFromAddress" size="30" maxlength="100" validate="email:true, required:true" value="#HTMLEditFormat(emailFromAddress)#" />
  </div>
</div>
<div class="row">
  <div class="col-38-right">E-Mail Reply Address: </div>
  <div class="col-58-left"><input type="text" name="emailReplyAddress" size="30" maxlength="100" validate="email:true" value="#HTMLEditFormat(emailReplyAddress)#" /></div>
</div>
<div class="row">
  <div class="col-38-right"><b>Send To: </b></div>
  <div class="col-58-left">
    <div style="width:400px;height:100px;overflow:auto;">
      <cfloop query="allLists">
      <input type="checkbox" name="sendToListIDsList" value="#listID#" <cfif ListFind(messageListIDsList, listID) IS NOT 0>checked</cfif> /> #listName# (#numSubscribers# subscribers)<br />
      <input type="hidden" name="listName#listID#" value="#HTMLEditFormat(listName)# (#numSubscribers# subscribers)" />
      </cfloop>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-38-right">Send to Test Address: </div>
  <div class="col-58-left"><input type="text" name="testAddress" size="30" maxlength="100" value="#HTMLEditFormat(testAddress)#" /></div>
</div>

<div class="row">
  <div class="col-38-right"><b>HTML E-Mail Body: </b></div>
  <div class="col-58-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#Application.siteURLRoot#"
            stylesheetURL="#Application.communication.emailBodyStyleSheetURL#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/email_template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmCopyMessage"
            fieldName="emailBodyHTML"
            cols="60"
            rows="5"
            HTMLContent="#emailBodyHTML#"
            displayHTMLSource="true"
            editorHeader="Edit Email Body">
      * please don't include HTML header<br /><br />
	  <select name="emailTemplate">
	    <option value="">- select an e-mail template -</option>
		<cfloop query="allTemplates">
		<option value="#allTemplates.templateFileName#"<cfif Not Compare(allTemplates.templateFileName, message.emailTemplate)> selected</cfif>>#allTemplates.templateName#</option>
		</cfloop>
	  </select>
	  <input type="button" class="button" value="preview" onClick="previewHTML(document.frmCopyMessage.emailTemplate.options[document.frmCopyMessage.emailTemplate.selectedIndex].value, 'frmCopyMessage')">
  </div>
</div>

<div class="row">
  <div class="col-38-right"><b>Text E-Mail Body: </b></div>
  <div class="col-58-left">
    <textarea name="emailBodyText" rows="5" cols="60">#HTMLEditFormat(emailBodyText)#</textarea><br />
    <input type="button" value="Extract Text From HTML E-Mail Body" class="button" onClick="extractTextFromHTMLBody(document.frmCopyMessage)">
  </div>
</div>

<cfif allSignUpForms.recordcount GT 0>
<div class="row">
  <div class="col-38-right">Sign-Up form to use when recipients click on 'subscribe' in the e-mail: </div>
  <div class="col-58-left">
    <select name="signUpFormID" style="min-width:250px;">
    <option value="0"></option>
    <cfloop query="allSignUpForms">
    <option value="#allSignUpForms.signUpFormID#"<cfif message.signUpFormID Is allSignUpForms.signUpFormID> selected</cfif>>#allSignUpForms.signUpFormName#</option>
    </cfloop>
    </select>
  </div>
</div>
</cfif>

<div class="row">
  <div class="col-38-right"><b>When to send? </b></div>
  <div class="col-58-left">
	  <input type="radio" name="sendMode" value="U"<cfif Not Compare(message.sendMode, "U")> checked</cfif>> <!--- Undetermined --->
	  Save this message without sending it and, if applicable, send immediately to test address only.<br />
	  <input type="radio" name="sendMode" value="R"<cfif Not Compare(message.sendMode, "R")> checked</cfif>> <!--- Real Time --->
	  Send immediately. <a href="javascript:alert('All e-mails will be pushed to mail server for delivery.\nActual time for completion depends on the load of the mail server.')">see note</a><br />
	  <input type="radio" name="sendMode" value="S"<cfif Not Compare(message.sendMode, "S")> checked</cfif>> <!--- Scheduled --->
	  Schedule to send at (if applicable, send immediately to test address only):<br />	  
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <cfset dateStruct=StructNew()>
	  <cfif NOT Compare(message.sendMode, "S")>
	    <cfset dateStruct=CreateObject("component", "com.Utility").ConvertDateTimeObjectToString(message.dateScheduledSend)>
	  <cfelse>
	    <cfset dateStruct.date="">
		<cfset dateStruct.hh="">
		<cfset dateStruct.mm="">
		<cfset dateStruct.ampm="am">
	  </cfif>
	  Date
	  <input type="text" name="dateScheduled" class="datePicker" size="10" value="#dateStruct.date#">
	  Time
	  <input type="text" name="timeScheduled_hh" size="2" value="#dateStruct.hh#"> :
	  <input type="text" name="timeScheduled_mm" size="2" value="#dateStruct.mm#">
	  <select name="timeScheduled_ampm">
	    <option value="am"<cfif dateStruct.ampm IS "am"> selected</cfif>>am</option>
		<option value="pm"<cfif dateStruct.ampm IS "pm"> selected</cfif>>pm</option>
	  </select>	  
  </div>
</div>
</form>
</cfoutput>