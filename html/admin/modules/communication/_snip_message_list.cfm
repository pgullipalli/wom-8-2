<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>

<cfset resultStruct=CM.getMessages(pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>

<cfif resultStruct.numAllItems Is 0>
  <p>There are no messages were created. To create one, pleasse click on 'New' in the action bar.</p>
<cfelse>
  <cfset startIdx=(URL.pageNum - 1) * URL.numItemsPerPage + 1>
  <cfset endIdx=URL.pageNum * URL.numItemsPerPage>
  <cfif endIdx GT resultStruct.numAllItems><cfset endIdx=resultStruct.numAllItems></cfif>
  <p><cfoutput>Display #startIdx# - #endIdx# of #resultStruct.numAllItems# Messages</cfoutput></p>
  
  <form id="frmMessageList" name="frmMessageList">
  <div class="itemList">
  <table>
    <tr>
  	  <th width="40">&nbsp;</th>		
      <th width="80">Edit</th>
      <th width="80">Delete</th>
	  <th width="250">Message</th>
      <th width="80">Archived</th>
	  <th>Date Composed</th>
	  <th>Date Sent</th>
	  <th>Scheduled to Send</th>
	  <th>Status</th>
    </tr>
    <cfoutput query="resultStruct.messages">
    <tr align="center">
      <td><input type="radio" name="messageID" value="#messageID#"></td>
      <td>
	    <cfif NOT Compare(status, "P")>
          <a href="javascript:editMessage('#messageID#')" class="accent01"><img src="images/icon_edit.gif" border="0" title="edit this message" align="absmiddle"></a>
		<cfelse>
          N/A
		</cfif>
      </td>
      <td>
	    <cfif Compare(Status, "S")>
          <a href="javascript:deleteMessage('#messageID#')"><img src="images/icon_delete.gif" border="0" title="delete this message" align="absmiddle"></a>
		<cfelse>
          N/A
		</cfif>
      </td>
      <td align="left"><a href="javascript:xsite.openWindow('index.cfm?md=communication&tmp=viewmessagecontent&messageID=#messageID#','view message',800,600)" title="View HTML e-mail comtent">#messageName#</a></td>
      <td><cfif published>Yes<cfelse>No</cfif></td>
      <td>#DateFormat(dateCreated, "mm/dd/yyyy")# #TimeFormat(dateCreated, "hh:mm:ss tt")#</td>
	  <td>
	    <cfif Compare(dateFirstSend,"")>
          #DateFormat(dateFirstSend, "mm/dd/yyyy")# #TimeFormat(dateFirstSend, "hh:mm:ss tt")#
		<cfelse>
          has not been sent
		</cfif>
      </td>
	  <td>
	    <cfif NOT Compare(sendMode, "S") AND Compare(dateScheduledSend, "")>
          #DateFormat(dateScheduledSend, "mm/dd/yyyy")# #TimeFormat(dateScheduledSend, "hh:mm:ss tt")#
		<cfelse>
          &nbsp;
		</cfif>
      </td>
	  <td>
	    <cfif NOT Compare(Status, "Q")>
	      <span class="hilite"><i>In Progress</i></span>
        <cfelseif NOT Compare(Status, "P")>
	      <b>Pending</b>
	    <cfelse>
	      Sent
	    </cfif>
	    <a href="javascript:viewMessageStatus('#messageID#')"><img src="images/icon_view.gif" border="0" title="View detail" align="absmiddle"></a>
	</td>
    </tr>
    </cfoutput>
  </table>
  </div>
  </form>
  
  <cfif resultStruct.numAllItems GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(resultStruct.numAllItems/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadMessageList(#idx#)">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
  </cfif>
</cfif>