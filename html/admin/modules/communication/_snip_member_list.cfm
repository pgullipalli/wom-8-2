<cfparam name="URL.listID">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">
<cfparam name="URL.sortBy" default="email">
<cfparam name="URL.order" default="ASC">

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset resultStruct=CM.getListMembersByListID(listID=URL.listID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage, sortBy=URL.sortBy, order=URL.order)>

<cfif resultStruct.numAllItems GT 0>
  <cfset startIdx=(URL.pageNum - 1) * URL.numItemsPerPage + 1>
  <cfset endIdx=URL.pageNum * URL.numItemsPerPage>
  <cfif endIdx GT resultStruct.numAllItems><cfset endIdx=resultStruct.numAllItems></cfif>
  <p><cfoutput>Display #startIdx# - #endIdx# of #resultStruct.numAllItems# Members</cfoutput></p>
<cfelse>
  <p>There are no members in this list. To create one, pleasse click on 'New' in the action bar.</p>
</cfif>

<cfif resultStruct.numDisplayedItems GT 0>
<form id="frmMemberList" name="frmMemberList" onsubmit="return false;">
<div class="itemList">
<table>
  <tr>
	<th width="70">
      <a href="javascript:xsite.checkAllBoxes(document.frmMemberList.emailID)"><b>All</b></a> &nbsp;
	  <a href="javascript:xsite.uncheckAllBoxes(document.frmMemberList.emailID)"><b>None</b></a>
    </th>
	<th>
	  E-Mail
	  <a href="#" onclick="sortMembers('email','ASC');"><img src="images/icon_up.gif" border="0" title="ascending order" align="absbottom"></a>
	  <a href="#" onclick="sortMembers('email','DESC');"><img src="images/icon_down.gif" border="0" title="decending order" align="absbottom"></a>
	</th>
	<th>
	  First Name
	  <a href="#" onclick="sortMembers('firstName','ASC');"><img src="images/icon_up.gif" border="0" title="ascending order" align="absbottom"></a>
	  <a href="#" onclick="sortMembers('firstName','DESC');"><img src="images/icon_down.gif" border="0" title="decending order" align="absbottom"></a>
	</th>
	<th>
	  Last Name
	  <a href="#" onclick="sortMembers('lastName','ASC');"><img src="images/icon_up.gif" border="0" title="ascending order" align="absbottom"></a>
	  <a href="#" onclick="sortMembers('lastName','DESC');"><img src="images/icon_down.gif" border="0" title="decending order" align="absbottom"></a>
	</th>
	<th width="120">
	  Status
	  <a href="#" onclick="sortMembers('status','ASC');"><img src="images/icon_up.gif" border="0" title="ascending order" align="absbottom"></a>
	  <a href="#" onclick="sortMembers('status','DESC');"><img src="images/icon_down.gif" border="0" title="decending order" align="absbottom"></a>
	</th>
    <th width="120">
      Bounces
      <a href="#" onclick="sortMembers('bounces','ASC');"><img src="images/icon_up.gif" border="0" title="ascending order" align="absbottom"></a>
	  <a href="#" onclick="sortMembers('bounces','DESC');"><img src="images/icon_down.gif" border="0" title="decending order" align="absbottom"></a>
    </th>
  </tr>
  <cfoutput query="resultStruct.members">
  <tr>
    <td align="center"><input type="checkbox" name="emailID" value="#emailID#"></td>
    <td>#email#</td>
    <td>#firstName#&nbsp;</td>
    <td>#lastName#&nbsp;</td>
    <td align="center">
      <cfswitch expression="#status#">
	    <cfcase value="P">Pending</cfcase>
		<cfcase value="S">Subscribed</cfcase>
		<cfcase value="U">Unsubscribed</cfcase>
		<cfcase value="B"><a href="javascript:showBounceMessage('#URL.listID#','#emailID#','B');" title="show bounce-back message">Bad</a></cfcase>
		<cfcase value="I">Invalid</cfcase>
	  </cfswitch>
    </td>
    <td align="center">
      <cfif numBouncedCampaigns IS 0>
	    0
	  <cfelse>
	    <a href="javascript:showBounceMessage('#URL.listID#','#emailID#','');" title="show bounce-back message">#numBouncedCampaigns#</a>
	  </cfif>
    </td>
  </tr>
  </cfoutput>
</table>
</div>
</form>

<cfif resultStruct.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(resultStruct.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadMemberList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>

</cfif>