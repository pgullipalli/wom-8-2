<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset allLists = CM.getListsSummaryInfo()>
<cfset allTemplates=CM.getAllEmailTemplates()>
<cfset allSignUpForms=CM.getSignUpFormsSummary()>

<form id="frmAddMessage" name="frmAddMessage" onSubmit="return false">
<cfoutput>
<input type="hidden" name="emailBodyStyleSheetPath" value="#Application.communication.emailBodyStyleSheetPath#" />
</cfoutput>

<br />

<div class="row">
  <div class="col-38-right"><b>Message Name: </b></div>
  <div class="col-58-left">
    <input type="text" name="messageName" size="60" maxlength="100" validate="required:true" /><br />
	* for your internal reference
  </div>
</div>
<div class="row">
  <div class="col-38-right"><b>E-Mail Subject Line: </b></div>
  <div class="col-58-left"><input type="text" name="emailSubject" size="60" maxlength="100" validate="required:true" /></div>
</div>
<div class="row">
  <div class="col-38-right">E-Mail From Name: </div>
  <div class="col-58-left"><input type="text" name="emailFromName" size="60" maxlength="100" /></div>
</div>
<div class="row">
  <div class="col-38-right"><b>E-Mail From Address: </b></div>
  <div class="col-58-left">
    <cfoutput>
    <input type="text" name="emailFromAddress" size="60" maxlength="100" validate="email:true, required:true" value="#Application.communication.emailDefaultFromAddress#" />
	</cfoutput>
  </div>
</div>
<div class="row">
  <div class="col-38-right">E-Mail Reply Address: </div>
  <div class="col-58-left"><input type="text" name="emailReplyAddress" size="60" maxlength="100" validate="email:true" /></div>
</div>
<div class="row">
  <div class="col-38-right"><b>Send To: </b></div>
  <div class="col-58-left">
    <div style="width:400px;height:100px;overflow:auto;">
      <cfoutput query="allLists">
      <input type="checkbox" name="sendToListIDsList" value="#listID#" /> #listName# (#numSubscribers# subscribers)<br />
      <input type="hidden" name="listName#listID#" value="#HTMLEditFormat(listName)# (#numSubscribers# subscribers)" />
      </cfoutput>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-38-right">Send to Test Address: </div>
  <div class="col-58-left"><input type="text" name="testAddress" size="60" maxlength="100" /></div>
</div>

<!--- <div class="row">
    NOTE: Some recipients do not have the ability to view HTML e-mails, or may have turned off
	this function in their Web browser preferences. We therefore strongly recommend creating messages
	in both HTML and Text versions. HTML e-mails can be created easily in WYSIWYG editor,
	which will be launched when you click "Open WYSIWYG Editor" below.
	It functions like a word processing program. Subscribers' individual e-mail client software
	will display the appropriate version of the e-mail message when it is read. 
</div> --->

<div class="row">
  <div class="col-38-right"><b>HTML E-Mail Body: </b></div>
  <div class="col-58-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#Application.siteURLRoot#"
            stylesheetURL="#Application.communication.emailBodyStyleSheetURL#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/email_template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmAddMessage"
            fieldName="emailBodyHTML"
            cols="60"
            rows="5"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Email Body">
      * please don't include HTML header<br /><br />
	  <select name="emailTemplate">
	    <option value="">- select an e-mail template -</option>
		<cfoutput query="allTemplates">
		<option value="#templateFileName#">#templateName#</option>
		</cfoutput>
	  </select>
	  <input type="button" class="button" value="preview" onClick="previewHTML(document.frmAddMessage.emailTemplate.options[document.frmAddMessage.emailTemplate.selectedIndex].value, 'frmAddMessage')">
  </div>
</div>

<div class="row">
  <div class="col-38-right"><b>Text E-Mail Body: </b></div>
  <div class="col-58-left">
    <textarea name="emailBodyText" rows="5" cols="60"></textarea><br />
    <input type="button" value="Extract Text From HTML E-Mail Body" class="button" onClick="extractTextFromHTMLBody(document.frmAddMessage)">
  </div>
</div>

<cfif allSignUpForms.recordcount GT 0>
<div class="row">
  <div class="col-38-right">Sign-Up form to use when recipients click on 'subscribe' in the e-mail: </div>
  <div class="col-58-left">
    <select name="signUpFormID" style="min-width:250px;">
    <option value="0"></option>
    <cfoutput query="allSignUpForms">
    <option value="#signUpFormID#">#signUpFormName#</option>
    </cfoutput>
    </select>
  </div>
</div>
</cfif>

<div class="row">
  <div class="col-38-right"><b>When to send? </b></div>
  <div class="col-58-left">
	  <input type="radio" name="sendMode" value="U" /> <!--- Undetermined --->
	  Save this message without sending it and, if applicable, send immediately to test address only.<br />
	  <input type="radio" name="sendMode" value="R" /> <!--- Real Time --->
	  Send immediately. <a href="javascript:alert('All e-mails will be pushed to mail server for delivery.\nActual time for completion depends on the load of the mail server.')">see note</a><br />
	  <input type="radio" name="sendMode" value="S" /> <!--- Scheduled --->
	  Schedule to send at (if applicable, send immediately to test address only):<br />	  
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  Date
	  <input type="text" name="dateScheduled" class="datePicker" size="10" />
	  Time
	  <input type="text" name="timeScheduled_hh" size="2" /> :
	  <input type="text" name="timeScheduled_mm" size="2" />
	  <select name="timeScheduled_ampm">
	    <option value="am">am</option>
		<option value="pm">pm</option>
	  </select>	  
  </div>
</div>
</form>