<cfparam name="URL.sendHistoryID">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="10">

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>

<cfset campaignInfo=CM.getCampaignBasicInfoBySendHistoryID(URL.sendHistoryID)>
<cfset resultStruct=CM.getBounceInfo(URL.sendHistoryID, URL.pageNum, URL.numItemsPerPage)>

<div class="header">COMMUNICATION MANAGER &gt; Bounces Report</div>

<cfoutput>
<h2>#campaignInfo.messageName#</h2>
<p><b>
<cfif campaignInfo.attemptNum IS 1>First<cfelseif campaignInfo.attemptNum IS 2>Second<cfelseif campaignInfo.attemptNum IS 3>Third<cfelse>#campaignInfo.attemptNum#th</cfif>
attpemt at
#TimeFormat(campaignInfo.dateSent, "h:mm:ss tt")#, #DateFormat(campaignInfo.dateSent, "mm/dd/yyyy")#.
</b></p>
</cfoutput>


<cfif resultStruct.numDisplayedItems GT 0>
  <cfset startIdx=(URL.pageNum - 1) * URL.numItemsPerPage + 1>
  <cfset endIdx=URL.pageNum * URL.numItemsPerPage>
  <cfif endIdx GT resultStruct.numAllItems><cfset endIdx=resultStruct.numAllItems></cfif>
  <p><cfoutput>Display #startIdx# - #endIdx# of #resultStruct.numAllItems#</cfoutput></p>

  <div class="itemList">
  <table>
    <tr>
  	  <th width="200">E-Mail</th>		
      <th width="200">Name</th>
      <th width="100">Bounce Type</th>
	  <th width="200">Date Bounced</th>
	  <th>Error Message</th>
    </tr>
    <cfoutput query="resultStruct.bounces">
      <tr valign="top">
        <td>#email#</td>
        <td>#firstName# #lastName#&nbsp;</td>
        <td align="center"><cfif bounceType IS "S">Soft<cfelse>Hard</cfif></td>
        <td align="center">#DateFormat(dateBounced, "mm/dd/yy")# #TimeFormat(dateBounced, "h:mm:ss tt")#</td>
        <td>
          #HTMLEditFormat(errorMessage)#
        </td>
      </tr>
    </cfoutput>
  </table>
  </div>
  
  <cfif resultStruct.numAllItems GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(resultStruct.numAllItems/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="index.cfm?md=communication&tmp=bouncebackmessagespercampaign&pageNum=#idx#&wrap=1">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
  </cfif>

<cfelse>
  <p>There are no bounces information available for this campaign.</p>
</cfif>

