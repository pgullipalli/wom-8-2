<script type="text/javascript" src="modules/communication/campaignreport.js"></script>

<cfparam name="URL.fromDate" default="#DateFormat(DateAdd("m", -1, now()),"mm/dd/yyyy")#">
<cfparam name="URL.toDate" default="#DateFormat(now(), "mm/dd/yyyy")#">

<a name="top"></a>
<div class="header">COMMUNICATION MANAGER &gt; Campaign Report</div>

<p>
Full campaign reports are available for <cfoutput>#Application.communication.daysToKeepCampaignStats#</cfoutput> days by default. You will always be able to view the sum totals, but the email addresses associated with email open/click-thru "detail" stats will be removed from storage two months after the campaign is sent. 
</p>

<cfoutput>
<form name="frmDateRange" id="frmDateRange" onSubmit="return false;">
From <input type="text" name="fromDate" id="fromDate" size="12" value="#URL.fromDate#"> &nbsp;
To <input type="text" name="toDate" id="toDate" size="12" value="#URL.toDate#">

<button onClick="loadReport();">Get Report</button>
</form>
</cfoutput>

<div id="mainDiv"></div>