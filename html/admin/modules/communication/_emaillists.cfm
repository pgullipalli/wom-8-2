<script type="text/javascript" src="modules/communication/emaillists.js"></script>

<div class="header">COMMUNICATION MANAGER &gt; E-Mail Lists</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addList()">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editList()">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteList()">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button130" onclick="listMembers()">List Members</a>
</div>

<div style="clear:both;"></div>

<div id="mainDiv"></div>