<cfparam name="URL.messageID">

<div class="header">COMMUNICATION MANAGER &gt; View Log</div>

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset sendHistoryInfo=CM.getSendHistoryDetail(URL.messageID)>

<cfoutput>
<p><b>#sendHistoryInfo.messageName#</b></p>
</cfoutput>


<cfoutput query="sendHistoryInfo">
<p>
<b>Attempt:</b> #attemptNum#<br>
<b>Sent:</b> #DateFormat(dateSent, "mm/dd/yy")# #TimeFormat(dateSent, "h:mm:ss tt")#<br>
<b>Mails Sent:</b> #numSent#<br>
<b>Bounces:</b> <a href="javascript:xsite.openWindow('index.cfm?md=communication&tmp=bouncebackmessagespercampaign&sendHistoryID=#sendHistoryID#&wrap=1','viewbounces',600,500)">#numBounces#</a><br>
<cfset fileName="#DateFormat(dateSent, "mm-dd-yy")#-delivery-#sendHistoryID#.log">
<cfset filePath="#Application.communication.mailLogDirectory#/#fileName#">
<cfif fileExists("#filePath#")>
  <cffile action="read" file="#filePath#" variable="deliveryLog">
  <b>Delivery Log:</b>
  <pre>#Trim(deliveryLog)#</pre><br>
</cfif>
<cfset fileName="#DateFormat(dateSent, "mm-dd-yy")#-error-#sendHistoryID#.log">
<cfset filePath="#Application.communication.mailLogDirectory#/#fileName#">
<cfif fileExists("#filePath#")>
  <cffile action="read" file="#filePath#" variable="errorLog">
  <b>Error Log:</b>
  <pre>#Trim(errorLog)#</pre><br>
</cfif>
</p>
</cfoutput>