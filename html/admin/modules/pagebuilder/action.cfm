<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  PB=CreateObject("component", "com.PageBuilderAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";

  switch (URL.task) {
    case "addPageCategory":
	  pageCategoryID=PB.addPageCategory(argumentCollection=Form);
	  returnedStruct.pageCategoryID=pageCategoryID;
      break;

    case "editPageCategory":
	  PB.editPageCategory(argumentCollection=Form);
      break;

    case "deletePageCategory":
      if (NOT PB.deletePageCategory(URL.catID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="It appears that there are still pages in the page category that you tried to delete. " &
	  		 "Please make sure you have deleted all pages in this category before deleting this category.";	  
      }
      break;

    case "addPage":
	  PB.addPage(argumentCollection=Form);	
      break;

    case "editPage":
	  PB.editPage(argumentCollection=Form);	
      break;

    case "copyPage":
	  PB.copyPage(argumentCollection=Form);	
      break;

    case "deletePage":
	  PB.deletePage(URL.pageID);	
      break;

    case "movePage":
	  PB.movePage(URL.pageID, URL.catID);	
      break;
	  
	default:
	  break;
  }
</cfscript>
<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>