$(function () {
	loadCategoryList();
});


function loadCategoryList() {
  xsite.load("#mainDiv", "index.cfm?md=pagebuilder&tmp=snip_category_list&uuid=" + xsite.getUUID());
}

/*--------------------------------*/
/*    BEGIN: Add Page Category    */
/*--------------------------------*/
function addPageCategory() {
  xsite.createModalDialog({
	windowName: 'winAddCat',
	title: 'Add Page Category',
	position: 'center-up',
	width: 450,
	url: 'index.cfm?md=pagebuilder&tmp=snip_add_category&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddCat").dialog('close'); },
	  ' Add ': submitForm_AddCategory
	}
  }).dialog('open');
  
  function submitForm_AddCategory() {
	var form = $("#frmAddCategory");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=pagebuilder&task=addPageCategory',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddCat").dialog('close');
		loadCategoryList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------*/
/*    END: Add Page Category      */
/*--------------------------------*/

/*--------------------------------*/
/*    BEGIN: Edit Page Category   */
/*--------------------------------*/
function editPageCategory() {
  var form = $("#frmCategoryList")[0];
  var catID = xsite.getCheckedValue(form.pageCategoryID);
  if (!catID) {
	alert("Please select a page category to edit.");
	//xsite.showInformationDialog('Please select a page category to edit.');
	return;
  }
  xsite.createModalDialog({
	windowName: 'winEditCat',
	title: 'Edit Page Category',
	position: 'center-up',
	width: 450,
	url: 'index.cfm?md=pagebuilder&tmp=snip_edit_category&catID=' + catID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditCat").dialog('close'); },
	  ' Save ': submitForm_EditCategory
	}
  }).dialog('open');
  
  function submitForm_EditCategory() {
	var form = $("#frmEditCategory");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=pagebuilder&task=editPageCategory',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
	  return;	
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditCat").dialog('close');
		loadCategoryList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}	  
  }
}
/*--------------------------------*/
/*    END: Edit Page Category     */
/*--------------------------------*/


/*--------------------------------*/
/*  BEGIN: Delete Page Category   */
/*--------------------------------*/
function deletePageCategory() {
  var form = $("#frmCategoryList")[0];
  var catID = xsite.getCheckedValue(form.pageCategoryID);
  if (!catID) {
	alert("Please select a page category to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected category?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=pagebuilder&task=deletePageCategory&catID=' + catID, {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadCategoryList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }
}
/*--------------------------------*/
/*  END: Delete Page Category   */
/*--------------------------------*/