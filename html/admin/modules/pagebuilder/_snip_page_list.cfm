<cfparam name="URL.catID" default="0">
<cfparam name="URL.keyword" default="">
<cfparam name="URL.browseBy" default="category">
<cfparam name="URL.includePageContent" default="0">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset PB=CreateObject("component", "com.PageBuilderAdmin").init()>

<cfif URL.browseBy IS "category"><!--- browse by category ID --->
  <cfset structPages=PB.getPagesByCategoryID(pageCategoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
  <!--- <cfoutput><span class="subHeader">#structPages.pageCategoryName#</span></cfoutput> --->
  <cfif structPages.numAllPages IS 0>
	<p>
	It appears that there are no pages created in this category.
	Please click on the 'New' button in the action bar above to create the first page.
	</p>
  </cfif>
<cfelse><!--- browse by page name keyword --->
  <cfset structPages=PB.getPagesByKeyword(keyword=URL.keyword, includePageContent=URL.includePageContent, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
  <cfoutput>
  <p>
  Your search for '<i>#URL.keyword#</i>' returns <cfif structPages.numAllPages GT 0>#structPages.numAllPages# pages.<cfelse>no results.</cfif>
  </p>	
  </cfoutput>
</cfif>

<cfif structPages.numDisplayedPages GT 0>
<form id="frmPageList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<cfif URL.browseBy IS "category">
	<th width="250">Page Name</th>
	<cfelse><!--- browse by search --->
	<th width="350">[Category] Page Name</th>
	</cfif>    
	<th>URL (click to preview)</th>
    <th width="100">Publish?</th>
	<th width="150">Searchable?</th>
  </tr>
  <cfoutput query="structPages.pages">
  <tr>
    <td align="center"><input type="radio" name="pageID" value="#pageID#"></td>
	<cfif URL.browseBy IS "category">
	<td><a href="##" onclick="editPage('#pageID#')">#pageName#</a></td>
	<cfelse>
	<td>
	  <a href="index.cfm?md=pagebuilder&tmp=pages&catID=#pageCategoryID#&wrap=1" title="List pages in this category">[#pageCategoryName#]</a>
	  <a href="##" onclick="editPage('#pageID#')">#pageName#</a>
	</td>
	</cfif>    
	<td><a href="/index.cfm?md=pagebuilder&tmp=home&pid=#pageID#" title="Preview this page" target="_blank">/index.cfm?md=pagebuilder&amp;tmp=home&amp;pid=#pageID#</a></td>
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
	<td align="center"><cfif isSearchable>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>

<cfif structPages.numAllPages GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structPages.numAllPages/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadPageList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>
</cfif>