<cfparam name="URL.catID" default="0">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset PB=CreateObject("component", "com.PageBuilderAdmin").init()>
<cfset structPages=PB.getPagesByCategoryID(pageCategoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
<cfif structPages.numAllPages IS 0>
<p>
No pages in this category.
</p>
</cfif>

<cfif structPages.numDisplayedPages GT 0>
<form id="frmPageList">
<div class="itemList">
<table>
  <tr>
	<th width="100">&nbsp;</th>
	<th>Page Name</th>  
    <th width="100">Publish?</th>
  </tr>
  <cfoutput query="structPages.pages">
  <tr>
    <td align="center">
    <a href="##" class="fg-button ui-state-default ui-corner-all button65" onClick="selectPage('#pageID#');">Select</a>
    </td>
	<td><a href="/index.cfm?md=pagebuilder&tmp=home&pid=#pageID#" target="_blank" id="page#pageID#">#pageName#</a></td>
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>

<cfif structPages.numAllPages GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structPages.numAllPages/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadPageList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>
</cfif>