<script type="text/javascript" src="modules/pagebuilder/pagecategories.js"></script>

<div class="header">PAGE BUILDER &gt; Page Categories</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addPageCategory();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editPageCategory();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deletePageCategory();">Delete</a>
</div>

<br style="clear:both;" /><br />

<div id="mainDiv"></div>

