<cfparam name="URL.pageID">

<cfset PB=CreateObject("component", "com.PageBuilderAdmin").init()>
<cfset page=PB.getPageByPageID(URL.pageID)>

<cfoutput>
<p><b>Copy a page from "#page.pageName#".</b></p>
</cfoutput>

<form id="frmCopyPage" name="frmCopyPage" onsubmit="return false">
<cfoutput><input type="hidden" name="pageID" value="#URL.pageID#"></cfoutput>
<div class="row">
	<div class="col-30-right"><b>New Page Name:</b></div>
    <div class="col-68-left"><input type="text" name="newPageName" size="30" value="" validate="required:true"></div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="published" value="1"></div>
    <div class="col-68-left">Publish this page</div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="isSearchable" value="1"></div>
    <div class="col-68-left">Make this page searchable</div>
</div>
</form>
