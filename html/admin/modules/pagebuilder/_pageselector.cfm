<cfset PB=CreateObject("component", "com.PageBuilderAdmin").init()>
<cfset allCategories=PB.getAllPageCategories()>

<script type="text/javascript" src="modules/pagebuilder/pageselector.js"></script>

<p align="right"><a href="javascript:window.close();"><img src="images/icon_cancel.gif" border="0" /></a></p>

<div class="header">PAGE SELECTOR</div>

<cfoutput>
<form id="frmNavigator">
  Select a Category:
  <select name="pageCategoryID" style="min-width:200px;">
    <cfloop query="allCategories">
    <option value="#pageCategoryID#">#pageCategoryName#</option>
    </cfloop>
  </select>
</form>
</cfoutput>

<br /><br />

<div id="mainDiv"></div>