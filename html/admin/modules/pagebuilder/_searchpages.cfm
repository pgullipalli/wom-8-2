<script type="text/javascript" src="modules/pagebuilder/searchpages.js"></script>

<cfparam name="URL.keyword" default="">

<cfset PB=CreateObject("component", "com.PageBuilderAdmin").init()>
<cfset allCategories=PB.getAllPageCategories()>
<cfif URL.catID IS 0>
  <cfset URL.catID=PB.getFirstPageCategoryID()>
  <cfif URL.catID IS 0><!--- no page categories were ever created --->
    <cflocation url="index.cfm?md=pagebuilder&tmp=page_categories&wrap=1">
  </cfif>
</cfif>

<div class="header">PAGE BUILDER &gt; Search Pages</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editPage();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="copyPage();">Copy</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deletePage();">Delete</a>
<cfif allCategories.recordcount GT 1>
<a href="#" id="btnMove" class="fg-button ui-state-default fg-button-icon-right ui-corner-all button65" onclick="movePage();"><span class="ui-icon ui-icon-arrow-4"></span>Move To</a>
</cfif>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="previewPage();">Preview</a>
</div>

<div id="divSubMenuMove" style="position:absolute;display:none;">
<table class="subMenu">
  <cfoutput query="allCategories">
  <tr valign="top">
    <td><a href="javascript:movePage('#pageCategoryID#')">#pageCategoryName#</a></td>
  </tr>
  </cfoutput>
</table>
</div>

<br /><br /><br />

<form id="frmSearchPages" onsubmit="return false">
Enter a keyword:
<input type="text" name="keyword" size="20" maxlength="30">
<input id="btnSearch" type="button" value=" Submit " onclick="loadPageList(null);">
&nbsp;&nbsp;
<a href="index.cfm?md=pagebuilder&tmp=pages&wrap=1">Browse By Categories</a><br />
<input type="checkbox" name="includePageContent" value="1"> Include page content in the search
</form>

<div id="mainDiv"></div>
