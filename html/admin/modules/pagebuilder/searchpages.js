var pageNum = 1;

$(function () {	
	$("body").click(function(e) { 
	  if (e.target.id != "btnMove" && e.target.id != "divSubMenuMove") $("#divSubMenuMove").hide();
	});
});


function loadPageList(pNum) {
  if (pNum) pageNum = pNum;
  var form=$("#frmSearchPages")[0];  
  var keyword=form.keyword.value;  
  if (keyword.length < 3) { alert("Please enter a keyword with at least 3 characters."); return; }
  var includePageContent=0;
  if (form.includePageContent.checked) includePageContent = 1;
  
  var url="index.cfm?md=pagebuilder&tmp=snip_page_list&keyword=" + escape(keyword) + "&pageNum=" + pageNum + "&includePageContent=" + includePageContent + "&browseBy=search&uuid=" + xsite.getUUID();
	
  xsite.load("#mainDiv", url);
}

function editPage() {
  var form = $("#frmPageList")[0];
  var pageID = xsite.getCheckedValue(form.pageID);
  if (!pageID) {
	alert("Please select a page to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditPage',
	title: 'Edit Page',
	width: 600,
	position: 'center-up',
	url: 'index.cfm?md=pagebuilder&tmp=snip_edit_page&pageID=' + pageID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditPage").dialog('close'); },
	  ' Save ': submitForm_EditPage
	}
  }).dialog('open');
	
  function submitForm_EditPage() {
	var form = $("#frmEditPage");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=pagebuilder&task=editPage',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditPage").dialog('close');
		loadPageList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function copyPage() {
  var form = $("#frmPageList")[0];
  var pageID = xsite.getCheckedValue(form.pageID);
  if (!pageID) {
	alert("Please select a page to copy from.");
	return;
  }
  
  xsite.createModalDialog({
	windowName: 'winCopyPage',
	title: 'Copy Page',
	width: 600,
	position: 'center-up',
	url: 'index.cfm?md=pagebuilder&tmp=snip_copy_page&pageID=' + pageID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winCopyPage").dialog('close'); },
	  ' Copy ': submitForm_CopyPage
	}
  }).dialog('open');
  
  function submitForm_CopyPage() {
	var form = $("#frmCopyPage");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=pagebuilder&task=copyPage',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winCopyPage").dialog('close');
		loadPageList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deletePage() {
  var form = $("#frmPageList")[0];
  var pageID = xsite.getCheckedValue(form.pageID);
  if (!pageID) {
	alert("Please select a page to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected page?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog();
	$.get('action.cfm?md=pagebuilder&task=deletePage&pageID=' + pageID, {}, processJson, 'json');		
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadPageList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function previewPage() {
  var form = $("#frmPageList")[0];
  var pageID = xsite.getCheckedValue(form.pageID);
  if (!pageID) {
	alert("Please select a page to preview.");
	return;
  }
  
  window.open('/index.cfm?md=pagebuilder&tmp=home&pid=' + pageID, '_blank');
}

function movePage(newCatID) {
  if (!document.getElementById("frmPageList")) return;
  var form = $("#frmPageList")[0];
  var pageID = xsite.getCheckedValue(form.pageID);
  if (!pageID) {
	alert("Please select a page to move.");
	return;
  }
  
  if (newCatID) {
	  $("#divSubMenuMove").hide();
	    
	  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=pagebuilder&task=movePage&catID=' + newCatID + '&pageID=' + pageID, {}, processJson, 'json');}});
  } else {
    var offset = $("#btnMove").offset();
    var height=$("#actionBar").height() + 2;
    $("#divSubMenuMove").css({'left':offset.left+'px', 'top':(offset.top+height)+'px'}).toggle();
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();			
	  loadPageList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }
}
