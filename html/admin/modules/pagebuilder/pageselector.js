var pageNum=1;//initial page number
var formName, fieldName, previewID;

$(function () {
	formName = xsite.getURLParameter('formName');
	fieldName = xsite.getURLParameter('fieldName');
	previewID = xsite.getURLParameter('previewID');

	loadPageList();
	
	$("#frmNavigator select[name='pageCategoryID']").change(function() {
	  pageNum=1;
	  loadPageList();	  
	});
});

function loadPageList(pNum) {
  if (pNum) pageNum = pNum;
  var catID=$("#frmNavigator select[name='pageCategoryID']").val();	
  
  var url="index.cfm?md=pagebuilder&tmp=snip_page_list_ps&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function selectPage(pageID) {
  var openerFormField=opener.document[formName][fieldName];
  var openerPreview=opener.document.getElementById(previewID);
  var pageName=$("#page" + pageID).html();
  openerFormField.value=pageID;
  openerPreview.innerHTML=pageName;
  window.close();
}