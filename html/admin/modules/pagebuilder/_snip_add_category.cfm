<div class="snip">
<form id="frmAddCategory" name="frmAddCategory" onsubmit="return false">
<div class="row">
    <div class="col-38-right"><b>Category Name:</b></div>
    <div class="col-58-left">
      <input type="text" name="pageCategoryName" size="35" maxlength="50" value="" validate="required:true">
    </div>
</div>
<div class="row">
    <div class="col-38-right">Header Image:</div>
    <div class="col-58-left">
      <input type="hidden" name="headerImage" value="">
      <CF_SelectFile formName="frmAddCategory" fieldName="headerImage" previewID="preview1" previewWidth="100">
      <div id="preview1"></div>
    </div>
</div>
</form>
</div>