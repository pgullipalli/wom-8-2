var pageNum=1;//initial page number

$(function () {		
	loadPageList();
		
	$("#frmNavigator select[name='pageCategoryID']").change(function() {
	  pageNum=1;
	  loadPageList();	  
	});
	   
	$("body").click(function(e) { 
	  if (e.target.id != "btnMove" && e.target.id != "divSubMenuMove") $("#divSubMenuMove").hide();
	});
});

function loadPageList(pNum) {
  if (pNum) pageNum = pNum;
  var catID=$("#frmNavigator select[name='pageCategoryID']").val();	
  
  var url="index.cfm?md=pagebuilder&tmp=snip_page_list&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function addPage() {
  var catID=$("#frmNavigator select[name='pageCategoryID']").val();	
  xsite.createModalDialog({
	windowName: 'winAddPage',
	title: 'Add Page',
	width: 600,
	position: 'center-up',
	url: 'index.cfm?md=pagebuilder&tmp=snip_add_page&catID=' + catID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddPage").dialog('close'); },
	  ' Add ': submitForm_AddPage
	}
  }).dialog('open');
  
  function submitForm_AddPage() {
	var form = $("#frmAddPage");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=pagebuilder&task=addPage',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddPage").dialog('close');
		loadPageList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editPage(pID) {
  var form = $("#frmPageList")[0];
  var pageID = null;
  if (pID) pageID = pID;
  else pageID = xsite.getCheckedValue(form.pageID);
  if (!pageID) {
	alert("Please select a page to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditPage',
	title: 'Edit Page',
	width: 600,
	position: 'center-up',
	url: 'index.cfm?md=pagebuilder&tmp=snip_edit_page&pageID=' + pageID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditPage").dialog('close'); },
	  ' Save ': submitForm_EditPage
	}
  }).dialog('open');
	
  function submitForm_EditPage() {
	var form = $("#frmEditPage");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=pagebuilder&task=editPage',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditPage").dialog('close');
		loadPageList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function copyPage() {
  var form = $("#frmPageList")[0];
  var pageID = xsite.getCheckedValue(form.pageID);
  if (!pageID) {
	alert("Please select a page to copy from.");
	return;
  }
  
  xsite.createModalDialog({
	windowName: 'winCopyPage',
	title: 'Copy Page',
	width: 450,
	position: 'center-up',
	url: 'index.cfm?md=pagebuilder&tmp=snip_copy_page&pageID=' + pageID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winCopyPage").dialog('close'); },
	  ' Copy ': submitForm_CopyPage
	}
  }).dialog('open');
  
  function submitForm_CopyPage() {
	var form = $("#frmCopyPage");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=pagebuilder&task=copyPage',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winCopyPage").dialog('close');
		loadPageList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deletePage() {
  var form = $("#frmPageList")[0];
  var pageID = xsite.getCheckedValue(form.pageID);
  if (!pageID) {
	alert("Please select a page to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected page?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=pagebuilder&task=deletePage&pageID=' + pageID, {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadPageList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function previewPage() {
  var form = $("#frmPageList")[0];
  var pageID = xsite.getCheckedValue(form.pageID);
  if (!pageID) {
	alert("Please select a page to preview.");
	return;
  }
  
  window.open('/index.cfm?md=pagebuilder&tmp=home&pid=' + pageID, '_blank');
}

function movePage(newCatID) {
  if (!document.getElementById("frmPageList")) return;
  var form = $("#frmPageList")[0];
  var pageID = xsite.getCheckedValue(form.pageID);
  if (!pageID) {
	alert("Please select a page to move.");
	return;
  }
  
  if (newCatID) {
	  $("#divSubMenuMove").hide();
	    
	  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=pagebuilder&task=movePage&catID=' + newCatID + '&pageID=' + pageID, {}, processJson, 'json');}});
  } else {
    var offset = $("#btnMove").offset();
    var height=$("#actionBar").height() + 2;
    $("#divSubMenuMove").css({'left':offset.left+'px', 'top':(offset.top+height)+'px'}).toggle();
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();			
	  loadPageList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }
}