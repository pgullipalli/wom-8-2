<cfparam name="URL.catID">
<cfset PB=CreateObject("component", "com.PageBuilderAdmin").init()>
<cfset TM=CreateObject("component", "com.TemplateAdmin").init()>
<cfset allCategories=PB.getAllPageCategories()>
<cfset templates=TM.getTemplates()>

<form id="frmAddPage" name="frmAddPage" onsubmit="return false">
<div class="row">
	<div class="col-30-right"><b>Page Category:</b></div>
    <div class="col-68-left">
      <select name="pageCategoryID">
	    <cfoutput query="allCategories">
	    <option value="#pageCategoryID#"<cfif URL.catID IS pageCategoryID> selected</cfif>>#pageCategoryName#</option>
	    </cfoutput>
	  </select>
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Page Name:</b></div>
    <div class="col-68-left"><input type="text" name="pageName" maxlength="150" value="" style="width:350px;" validate="required:true"></div>
</div>
<div class="row">
	<div class="col-30-right"><b>Page Title:</b></div>
    <div class="col-68-left"><input type="text" name="pageTitle" maxlength="150"  value="" style="width:350px;" validate="required:true"></div>
</div>

<div class="row">
	<div class="col-30-right"><input type="checkbox" name="published" value="1"></div>
    <div class="col-68-left">Publish this page</div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="isSearchable" value="1"></div>
    <div class="col-68-left">Make this page searchable</div>
</div>

<div class="row">
	<div class="col-30-right">Default Template</div>
    <div class="col-68-left">
    <select name="defaultTemplateID" style="min-width:250px;">
      <option value="0"></option>
      <cfoutput query="templates">
        <option value="#templateID#">#templateName#</option>
      </cfoutput>
    </select>
    </div>
</div>

<div class="row">
	<div class="col-30-right"><b>Page Content:</b></div>
    <div class="col-68-left">
        Use WYSIWIG ("What You See Is What You Get") tool to type text, cut-and-paste from another source,
        or enter HTML content.<br />
        <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="false"
            formName="frmAddPage"
            fieldName="pageContent"
            cols="70"
            rows="6"
            HTMLContent=""
            displayHTMLSource="false"
            editorHeader="Edit Page Content">
    </div>
</div>
<div class="row">
  <div class="col-30-right">&nbsp;</div>
  <div class="col-68-left"><a href="#" onClick="xsite.toggle('forAdvancedUser')" class="accent01">For Advanced User &dArr;</a></div>
</div>
<div id="forAdvancedUser" style="display:none;">
  <div class="row">
	<div class="col-30-right"><input type="checkbox" name="useHTML" value="1" /></div>
    <div class="col-68-left">Check this box if you wish to use the following HTML for your page content instead of the above "WYSIWYG" content.</div>
  </div>
  <div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left"><textarea name="pageContentHTML" cols="70" rows="6"></textarea></div>
  </div>
</div>

</form>