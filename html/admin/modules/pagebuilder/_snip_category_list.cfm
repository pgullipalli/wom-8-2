<cfset PB=CreateObject("component", "com.PageBuilderAdmin").init()>
<cfset pageCategorySummary=PB.getPageCategorySummary()>

<cfif pageCategorySummary.recordcount IS 0>
  <p>
  It appears that there are no page categories created.
  Please click on the 'New' button in the action bar above to create the first page category.
  You can rename your page category name later.
  </p>
<cfelse>
<form id="frmCategoryList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th>
    Category Name
    </th>
    <th width="100">
    # of Pages
    </th>
  </tr>
  <cfoutput query="pageCategorySummary">
  <tr>
    <td align="center"><input type="radio" name="pageCategoryID" value="#pageCategoryID#"></td>
    <td><a href="index.cfm?md=pagebuilder&tmp=pages&wrap=1&catID=#pageCategoryID#">#pageCategoryName#</a></td>
    <td align="center"><a href="index.cfm?md=pagebuilder&tmp=pages&wrap=1&catID=#pageCategoryID#">#numPages#</a></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>