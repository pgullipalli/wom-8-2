<script type="text/javascript" src="modules/pagebuilder/pages.js"></script>

<cfparam name="URL.catID" default="0">

<cfset PB=CreateObject("component", "com.PageBuilderAdmin").init()>
<cfset allCategories=PB.getAllPageCategories()>
<cfif URL.catID IS 0>
  <cfset URL.catID=PB.getFirstPageCategoryID()>
  <cfif URL.catID IS 0><!--- no page categories were ever created --->
    <cflocation url="index.cfm?md=pagebuilder&tmp=pagecategories&wrap=1">
  </cfif>
</cfif>

<div class="header">PAGE BUILDER &gt; Pages</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addPage()">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editPage(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="copyPage();">Copy</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deletePage();">Delete</a>
<cfif allCategories.recordcount GT 1>
<a href="#" id="btnMove" class="fg-button ui-state-default fg-button-icon-right ui-corner-all button65" onclick="movePage();"><span class="ui-icon ui-icon-arrow-4"></span>Move To</a>
</cfif>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="previewPage();">Preview</a>
</div>

<div id="divSubMenuMove" style="position:absolute;display:none;">
<table class="subMenu">
  <cfoutput query="allCategories">
  <tr valign="top">
    <td><a href="javascript:movePage('#pageCategoryID#')">#pageCategoryName#</a></td>
  </tr>
  </cfoutput>
</table>
</div>

<div style="clear:both;"><br /></div>

<cfoutput>
<form id="frmNavigator">
  Page Category
  <select name="pageCategoryID" style="min-width:250px;">
    <cfloop query="allCategories">
    <option value="#pageCategoryID#"<cfif URL.catID IS pageCategoryID> selected</cfif>>#pageCategoryName#</option>
    </cfloop>
  </select>
  &nbsp;&nbsp;&nbsp;&nbsp;
  <a href="index.cfm?md=pagebuilder&tmp=searchpages&wrap=1">Search Pages</a>
</form>
</cfoutput>


<div id="mainDiv"></div>
