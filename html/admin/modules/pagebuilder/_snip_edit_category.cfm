<cfparam name="URL.catID" default="0">

<cfset PB=CreateObject("component", "com.PageBuilderAdmin").init()>
<cfset pageCategory=PB.getPageCategory(URL.catID)>

<div class="snip">
<form id="frmEditCategory" name="frmEditCategory" onsubmit="return false">
<cfoutput query="pageCategory">
<input type="hidden" name="pageCategoryID" value="#URL.catID#">
<div class="row">
    <div class="col-38-right"><b>Category Name:</b></div>
    <div class="col-58-left">
      <input type="text" name="pageCategoryName" size="35" maxlength="50" value="#HTMLEditFormat(pageCategoryName)#" validate="required:true">
    </div>
</div>
<div class="row">
    <div class="col-38-right">Header Image:</div>
    <div class="col-58-left">
      <input type="hidden" name="headerImage" value="#HTMLEditFormat(headerImage)#">
      <CF_SelectFile formName="frmEditCategory" fieldName="headerImage" previewID="preview2" previewWidth="100">
      <div id="preview2"><cfif Compare(headerImage,"")><img src="/#headerImage#" border="0" /></cfif></div>
    </div>
</div>
</cfoutput>
</form>
</div>