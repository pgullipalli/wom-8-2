function updateHomepageInfo() {
	$("#frmHomePage").ajaxSubmit({ 
		url: 'action.cfm?md=homepage&task=updateHomepageInfo',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgHomepage").html('The home page content has been updated.');	
		  $("#frmHomePage div").show();
		  location.href="#pagetop";
		} else {
		  alert (jsonData.MESSAGE);
		}
	}
}
