<script type="text/javascript" src="modules/homepage/subhomepages.js"></script>

<div class="header">HOMEPAGE MANAGER &gt; Sub-Home Pages</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addSubHomePage();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editSubHomePage();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteSubHomePage();">Delete</a>
</div>

<br style="clear:both;" /><br />

<div id="mainDiv"></div>