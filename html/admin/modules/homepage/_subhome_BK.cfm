<script type="text/javascript" src="modules/homepage/subhome.js"></script>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>
<cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
<cfset HP=CreateObject("component", "com.HomepageAdmin").init()>
<cfset allClassCategories=CL.getAllCategories()>
<cfset allArticleCategories=NR.getAllArticleCategories()>
<cfset allAlbumCategories=PG.getAllAlbumCategories()>
<cfset subHomepageInfo=HP.getSubHomepageInfo()>
<a name="pagetop"></a>
<div class="header">SUB-HOME PAGES</div>

<p>Please provide content that will be used by sub-home pages.</p>

<form id="frmSubHomePages" name="frmSubHomePages" onSubmit="return false;">

<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgSubHomepages"></span></p>
  </div>
</div>

<div class="subHeader">Pregnancy &amp; Childbirth</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Welcome Message:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmSubHomePages"
            fieldName="welcomeMessage1"
            cols="60"
            rows="3"
            HTMLContent="#subHomepageInfo.welcomeMessage1#"
            displayHTMLSource="true"
            editorHeader="Edit Welcome Message">    
    </div>
</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Information Library:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="articleCategoryID1" style="min-width:250px;">
        <option value="0"></option>
        <cfoutput query="allArticleCategories">
        <option value="#articleCategoryID#"<cfif Not Compare(articleCategoryID,subHomepageInfo.articleCategoryID1)> selected</cfif>>#articleCategory#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Feature Class Intro:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmSubHomePages"
            fieldName="featureClassIntro1"
            cols="60"
            rows="3"
            HTMLContent="#subHomepageInfo.featureClassIntro1#"
            displayHTMLSource="true"
            editorHeader="Edit Intro Message">    
    </div>
</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Feature Class Category:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="classCategoryID1" style="min-width:250px;">
        <option value="0"></option>
        <cfoutput query="allClassCategories">
        <option value="#categoryID#"<cfif Not Compare(categoryID,subHomepageInfo.classCategoryID1)> selected</cfif>>#categoryName#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Photo Gallery:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="albumCategoryID1" style="min-width:250px;">
        <option value="0"></option>
        <cfoutput query="allAlbumCategories">
        <option value="#albumCategoryID#"<cfif Not Compare(albumCategoryID,subHomepageInfo.albumCategoryID1)> selected</cfif>>#albumCategory#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<br style="clear:both;" />

<div class="subHeader">Wellness &amp; Prevention</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Welcome Message:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmSubHomePages"
            fieldName="welcomeMessage2"
            cols="60"
            rows="3"
            HTMLContent="#subHomepageInfo.welcomeMessage2#"
            displayHTMLSource="true"
            editorHeader="Edit Welcome Message">
    </div>
</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Information Library:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="articleCategoryID2" style="min-width:250px;">
        <option value="0"></option>
        <cfoutput query="allArticleCategories">
        <option value="#articleCategoryID#"<cfif Not Compare(articleCategoryID,subHomepageInfo.articleCategoryID2)> selected</cfif>>#articleCategory#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Feature Class Intro:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmSubHomePages"
            fieldName="featureClassIntro2"
            cols="60"
            rows="3"
            HTMLContent="#subHomepageInfo.featureClassIntro2#"
            displayHTMLSource="true"
            editorHeader="Edit Intro Message">    
    </div>
</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Feature Class Category:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="classCategoryID2" style="min-width:250px;">
        <option value="0"></option>
        <cfoutput query="allClassCategories">
        <option value="#categoryID#"<cfif Not Compare(categoryID,subHomepageInfo.classCategoryID2)> selected</cfif>>#categoryName#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Photo Gallery:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="albumCategoryID2" style="min-width:250px;">
        <option value="0"></option>
        <cfoutput query="allAlbumCategories">
        <option value="#albumCategoryID#"<cfif Not Compare(albumCategoryID,subHomepageInfo.albumCategoryID2)> selected</cfif>>#albumCategory#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<br style="clear:both;" />

<div class="subHeader">Treatment &amp; Care</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Welcome Message:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmSubHomePages"
            fieldName="welcomeMessage3"
            cols="60"
            rows="3"
            HTMLContent="#subHomepageInfo.welcomeMessage3#"
            displayHTMLSource="true"
            editorHeader="Edit Welcome Message">
    </div>
</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Information Library:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="articleCategoryID3" style="min-width:250px;">
        <option value="0"></option>
        <cfoutput query="allArticleCategories">
        <option value="#articleCategoryID#"<cfif Not Compare(articleCategoryID,subHomepageInfo.articleCategoryID3)> selected</cfif>>#articleCategory#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Feature Class Intro:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmSubHomePages"
            fieldName="featureClassIntro3"
            cols="60"
            rows="3"
            HTMLContent="#subHomepageInfo.featureClassIntro3#"
            displayHTMLSource="true"
            editorHeader="Edit Intro Message">    
    </div>
</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Feature Class Category:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="classCategoryID3" style="min-width:250px;">
        <option value="0"></option>
        <cfoutput query="allClassCategories">
        <option value="#categoryID#"<cfif Not Compare(categoryID,subHomepageInfo.classCategoryID3)> selected</cfif>>#categoryName#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<div class="row">
    <div style="width:130px;padding:0px 2px;text-align:right;float:left;">
      Photo Gallery:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="albumCategoryID3" style="min-width:250px;">
        <option value="0"></option>
        <cfoutput query="allAlbumCategories">
        <option value="#albumCategoryID#"<cfif Not Compare(albumCategoryID,subHomepageInfo.albumCategoryID3)> selected</cfif>>#albumCategory#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<br style="clear:both;" /><br />

<input type="button" value=" Update " onclick="updateSubHomepageInfo();">
</form>