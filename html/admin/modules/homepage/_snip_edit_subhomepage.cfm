<cfparam name="URL.subHomePageID">
<cfscript>
  CL=CreateObject("component", "com.ClassAdmin").init();
  NR=CreateObject("component", "com.NewsroomAdmin").init();
  PG=CreateObject("component", "com.PhotoGalleryAdmin").init();  
  HP=CreateObject("component", "com.HomepageAdmin").init();
  allClassCategories=CL.getAllCategories();
  allArticleCategories=NR.getAllArticleCategories();
  allAlbumCategories=PG.getAllAlbumCategories();
  subHomePageInfo=HP.getSubHomePage(URL.subHomePageID);
</cfscript>

<cfoutput query="subHomePageInfo">
<form id="frmEditSubHomePage" name="frmEditSubHomePage" onsubmit="return false">
<input type="hidden" name="subHomePageID" value="#URL.subHomePageID#" />
<div class="row">
	<div class="col-30-right"><b>Sub-Home Page Name:</b></div>
    <div class="col-68-left"><input type="text" name="subHomePageName" maxlength="100" value="#HTMLEditFormat(subHomePageName)#" style="width:320px;" validate="required:true" /></div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="published" value="1" <cfif published>checked</cfif> /> Publish this sub-home page
    </div>
</div>
<div class="row">
  <div class="col-30-right">&nbsp;</div><div class="col-68-left"><span class="subHeader">Welcome Message</span></div>
</div>
<div class="row">
	<div class="col-30-right">Message:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmEditSubHomePage"
            fieldName="welcomeMessage"
            cols="60"
            rows="3"
            HTMLContent="#welcomeMessage#"
            displayHTMLSource="true"
            editorHeader="Edit Welcome Message">
    </div>
</div>
<div class="row">
  <div class="col-30-right">&nbsp;</div><div class="col-68-left"><span class="subHeader">News</span></div>
</div>
<div class="row">
	<div class="col-30-right">Select a Category in Information Library:</div>
    <div class="col-68-left">
      <select name="articleCategoryID" style="min-width:320px;">
        <option value="0"></option>
        <cfloop query="allArticleCategories">
        <option value="#allArticleCategories.articleCategoryID#"<cfif Not Compare(allArticleCategories.articleCategoryID,subHomePageInfo.articleCategoryID)> selected</cfif>>#allArticleCategories.articleCategory#</option>  
        </cfloop>
      </select>
    </div>
</div>
<div class="row">
  <div class="col-30-right">&nbsp;</div><div class="col-68-left"><span class="subHeader">Featured and Upcoming Class</span></div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="showFeatureClass" value="1" <cfif showFeatureClass>checked</cfif> /> Show featured and upcoming class
    </div>
</div>
<div class="row">
	<div class="col-30-right">Intro Copy:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmEditSubHomePage"
            fieldName="featureClassIntro"
            cols="60"
            rows="2"
            HTMLContent="#featureClassIntro#"
            displayHTMLSource="true"
            editorHeader="Edit Intro Copy">
    </div>
</div>
<div class="row">
	<div class="col-30-right">Select a Category in Class Manager:</div>
    <div class="col-68-left">
      <select name="classCategoryID" style="min-width:320px;">
        <option value="0"></option>
        <cfloop query="allClassCategories">
        <option value="#allClassCategories.categoryID#"<cfif Not Compare(allClassCategories.categoryID,subHomePageInfo.classCategoryID)> selected</cfif>>#allClassCategories.categoryName#</option>  
        </cfloop>
      </select>
    </div>
</div>
<div class="row">
  <div class="col-30-right">&nbsp;</div><div class="col-68-left"><span class="subHeader">Featured Photo Album</span></div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="showFeatureAlbum" value="1" <cfif showFeatureAlbum>checked</cfif> /> Show featured photo album
    </div>
</div>
<div class="row">
	<div class="col-30-right">Select a Category in Photo Gallery:</div>
    <div class="col-68-left">
      <select name="albumCategoryID" style="min-width:320px;">
        <option value="0"></option>
        <cfloop query="allAlbumCategories">
        <option value="#allAlbumCategories.albumCategoryID#"<cfif Not Compare(allAlbumCategories.albumCategoryID,subHomePageInfo.albumCategoryID)> selected</cfif>>#allAlbumCategories.albumCategory#</option>  
        </cfloop>
      </select>
    </div>
</div>
</form>
</cfoutput>