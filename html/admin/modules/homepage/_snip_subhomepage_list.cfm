<cfscript>
  HP=CreateObject("component", "com.HomepageAdmin").init();
  allSubHomePages=HP.getAllSubHomePages();
</cfscript>

<cfif allSubHomePages.recordcount IS 0>
  <p>
  It appears that there are no sub-home pages created.
  Please click on the 'New' button in the action bar above to create the first sub-home page.
  </p>
<cfelse>
<form id="frmSubHomePageList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th width="300">Sub-Home Page Name</th>
    <th width="100">Publish?</th>
    <th width="350">URL (click to preview)</th>
    <th>&nbsp;</th>
  </tr>  
  <cfoutput query="allSubHomePages">
  <tr>
    <td align="center"><input type="radio" name="subHomePageID" value="#subHomePageID#"></td>
	<td>
      <a href="javascript:editSubHomePage('#subHomePageID#')">#subHomePageName#<!---  (ID: #subHomePageID#) ---></a>
    </td> 
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
    <td><a href="/index.cfm?md=homepage&tmp=subhome&shpid=#subHomePageID#" target="_blank">/index.cfm?md=homepage&amp;tmp=subhome&amp;shpid=#subHomePageID#</a></td>
    <td>&nbsp;</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>