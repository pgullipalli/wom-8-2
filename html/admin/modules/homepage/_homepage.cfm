<script type="text/javascript" src="modules/homepage/homepage.js"></script>

<cfset CL=CreateObject("component", "com.ClassAdmin").init()>
<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>
<cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
<cfset HP=CreateObject("component", "com.HomepageAdmin").init()>
<cfset allClassCategories=CL.getAllCategories()>
<cfset allArticleCategories=NR.getAllArticleCategories()>
<cfset allAlbumCategories=PG.getAllAlbumCategories()>
<cfset homepageInfo=HP.getHomepageInfo()>
<cfset allSubHomePages=HP.getAllSubHomePages()>
<a name="pagetop"></a>
<div class="header">HOME PAGE</div>

<p>Please select the class and resource categories that will be used by the three pods on home page.</p>

<form id="frmHomePage" name="frmHomePage" onSubmit="return false;">

<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgHomepage"></span></p>
  </div>
</div>

<div class="subHeader">Pod #1</div>

<div class="row">
	<div style="width:140px;padding:0px 2px;text-align:right;float:left;">
      Pod Image:
    </div>
    <div style="width:160px;padding:0px 2px;text-align:left;float:left;">
      <cfoutput><input type="hidden" name="podImage1" value="#HTMLEditFormat(homepageInfo.podImage1)#"></cfoutput>
      <CF_SelectFile formName="frmHomePage" fieldName="podImage1" previewWidth="150" previewID="preview1">
      <div id="preview1"><cfif Compare(homepageInfo.podImage1,"")><cfoutput><img src="/#homepageInfo.podImage1#" width="150" /></cfoutput></cfif></div>
    </div>
    <div style="width:80px;padding:0px 2px;text-align:right;float:left;">
      Mouse Over:
    </div>
    <div style="width:160px;padding:0px 2px;text-align:left;float:left;">
      <cfoutput><input type="hidden" name="podImageOver1" value="#HTMLEditFormat(homepageInfo.podImageOver1)#"></cfoutput>
      <CF_SelectFile formName="frmHomePage" fieldName="podImageOver1" previewWidth="150" previewID="preview2">
      <div id="preview2"><cfif Compare(homepageInfo.podImageOver1,"")><cfoutput><img src="/#homepageInfo.podImageOver1#" width="150" /></cfoutput></cfif></div>
    </div>
</div>

<div class="row">
	<div style="width:140px;padding:0px 2px;text-align:right;float:left;">Pod Image Alt-Text:</div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <cfoutput><input type="text" name="podImageAltText1" style="width:320px;" value="#HTMLEditFormat(homepageInfo.podImageAltText1)#" /></cfoutput>
    </div>
</div>

<div class="row">
	<div style="width:140px;padding:0px 2px;text-align:right;float:left;">
      Sub-Home Page
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="subHomePageID1" style="min-width:320px;">
        <option value="0"></option>
        <cfoutput query="allSubHomePages">
        <option value="#subHomePageID#"<cfif Not Compare(subHomePageID,homepageInfo.subHomePageID1)> selected</cfif>>#subHomePageName#</option>
        </cfoutput>
      </select>
    </div>
</div>

<div class="row">
    <div style="width:140px;padding:0px 2px;text-align:right;float:left;">
      Class Category:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="classCategoryID1" style="min-width:320px;">
        <option value="0"></option>
        <cfoutput query="allClassCategories">
        <option value="#categoryID#"<cfif Not Compare(categoryID,homepageInfo.classCategoryID1)> selected</cfif>>#categoryName#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<div class="row">
    <div style="width:140px;padding:0px 2px;text-align:right;float:left;">
      Information Library:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="articleCategoryID1" style="min-width:320px;">
        <option value="0"></option>
        <cfoutput query="allArticleCategories">
        <option value="#articleCategoryID#"<cfif Not Compare(articleCategoryID,homepageInfo.articleCategoryID1)> selected</cfif>>#articleCategory#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<br style="clear:both;" />

<div class="subHeader">Pod #2</div>

<div class="row">
	<div style="width:140px;padding:0px 2px;text-align:right;float:left;">
      Pod Image:
    </div>
    <div style="width:160px;padding:0px 2px;text-align:left;float:left;">
      <cfoutput><input type="hidden" name="podImage2" value="#HTMLEditFormat(homepageInfo.podImage2)#"></cfoutput>
      <CF_SelectFile formName="frmHomePage" fieldName="podImage2" previewWidth="150" previewID="preview3">
      <div id="preview3"><cfif Compare(homepageInfo.podImage2,"")><cfoutput><img src="/#homepageInfo.podImage2#" width="150" /></cfoutput></cfif></div>
    </div>
    <div style="width:80px;padding:0px 2px;text-align:right;float:left;">
      Mouse Over:
    </div>
    <div style="width:160px;padding:0px 2px;text-align:left;float:left;">
      <cfoutput><input type="hidden" name="podImageOver2" value="#HTMLEditFormat(homepageInfo.podImageOver2)#"></cfoutput>
      <CF_SelectFile formName="frmHomePage" fieldName="podImageOver2" previewWidth="150" previewID="preview4">
      <div id="preview4"><cfif Compare(homepageInfo.podImageOver2,"")><cfoutput><img src="/#homepageInfo.podImageOver2#" width="150" /></cfoutput></cfif></div>
    </div>
</div>

<div class="row">
	<div style="width:140px;padding:0px 2px;text-align:right;float:left;">Pod Image Alt-Text:</div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <cfoutput><input type="text" name="podImageAltText2" style="width:320px;" value="#HTMLEditFormat(homepageInfo.podImageAltText2)#" /></cfoutput>
    </div>
</div>

<div class="row">
	<div style="width:140px;padding:0px 2px;text-align:right;float:left;">
      Sub-Home Page
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="subHomePageID2" style="min-width:320px;">
        <option value="0"></option>
        <cfoutput query="allSubHomePages">
        <option value="#subHomePageID#"<cfif Not Compare(subHomePageID,homepageInfo.subHomePageID2)> selected</cfif>>#subHomePageName#</option>
        </cfoutput>
      </select>
    </div>
</div>

<div class="row">
    <div style="width:140px;padding:0px 2px;text-align:right;float:left;">
      Class Category:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="classCategoryID2" style="min-width:320px;">
        <option value="0"></option>
        <cfoutput query="allClassCategories">
        <option value="#categoryID#"<cfif Not Compare(categoryID,homepageInfo.classCategoryID2)> selected</cfif>>#categoryName#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<div class="row">
    <div style="width:140px;padding:0px 2px;text-align:right;float:left;">
      Information Library:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="articleCategoryID2" style="min-width:320px;">
        <option value="0"></option>
        <cfoutput query="allArticleCategories">
        <option value="#articleCategoryID#"<cfif Not Compare(articleCategoryID,homepageInfo.articleCategoryID2)> selected</cfif>>#articleCategory#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<br style="clear:both;" />

<div class="subHeader">Pod #3</div>

<div class="row">
	<div style="width:140px;padding:0px 2px;text-align:right;float:left;">
      Pod Image:
    </div>
    <div style="width:160px;padding:0px 2px;text-align:left;float:left;">
      <cfoutput><input type="hidden" name="podImage3" value="#HTMLEditFormat(homepageInfo.podImage3)#"></cfoutput>
      <CF_SelectFile formName="frmHomePage" fieldName="podImage3" previewWidth="150" previewID="preview5">
      <div id="preview5"><cfif Compare(homepageInfo.podImage3,"")><cfoutput><img src="/#homepageInfo.podImage3#" width="150" /></cfoutput></cfif></div>
    </div>
    <div style="width:80px;padding:0px 2px;text-align:right;float:left;">
      Mouse Over:
    </div>
    <div style="width:160px;padding:0px 2px;text-align:left;float:left;">
      <cfoutput><input type="hidden" name="podImageOver3" value="#HTMLEditFormat(homepageInfo.podImageOver3)#"></cfoutput>
      <CF_SelectFile formName="frmHomePage" fieldName="podImageOver3" previewWidth="150" previewID="preview6">
      <div id="preview6"><cfif Compare(homepageInfo.podImageOver3,"")><cfoutput><img src="/#homepageInfo.podImageOver3#" width="150" /></cfoutput></cfif></div>
    </div>
</div>

<div class="row">
	<div style="width:140px;padding:0px 2px;text-align:right;float:left;">Pod Image Alt-Text:</div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <cfoutput><input type="text" name="podImageAltText3" style="width:320px;" value="#HTMLEditFormat(homepageInfo.podImageAltText3)#" /></cfoutput>
    </div>
</div>

<div class="row">
	<div style="width:140px;padding:0px 2px;text-align:right;float:left;">
      Sub-Home Page
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="subHomePageID3" style="min-width:320px;">
        <option value="0"></option>
        <cfoutput query="allSubHomePages">
        <option value="#subHomePageID#"<cfif Not Compare(subHomePageID,homepageInfo.subHomePageID3)> selected</cfif>>#subHomePageName#</option>
        </cfoutput>
      </select>
    </div>
</div>

<div class="row">
    <div style="width:140px;padding:0px 2px;text-align:right;float:left;">
      Class Category:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="classCategoryID3" style="min-width:320px;">
        <option value="0"></option>
        <cfoutput query="allClassCategories">
        <option value="#categoryID#"<cfif Not Compare(categoryID,homepageInfo.classCategoryID3)> selected</cfif>>#categoryName#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<div class="row">
    <div style="width:140px;padding:0px 2px;text-align:right;float:left;">
      Information Library:
    </div>
    <div style="width:500px;padding:0px 2px;text-align:left;float:left;">
      <select name="articleCategoryID3" style="min-width:320px;">
        <option value="0"></option>
        <cfoutput query="allArticleCategories">
        <option value="#articleCategoryID#"<cfif Not Compare(articleCategoryID,homepageInfo.articleCategoryID3)> selected</cfif>>#articleCategory#</option>  
        </cfoutput>
      </select>
    </div>
</div>

<br style="clear:both;" /><br />

<input type="button" value=" Update " onclick="updateHomepageInfo();">
</form>