<cfscript>
  HP=CreateObject("component","com.Homepage").init();
  CL=CreateObject("component","com.Class").init();
  NR=CreateObject("component","com.Newsroom").init();
  homepageInfo=HP.getHomepageInfo();
  featureClass1=CL.getFeatureClassByCategoryID(homepageInfo.classCategoryID1);
  featureClass2=CL.getFeatureClassByCategoryID(homepageInfo.classCategoryID2);
  featureClass3=CL.getFeatureClassByCategoryID(homepageInfo.classCategoryID3);
  featureArticle1=NR.getFeatureArticleByCategoryID(homepageInfo.articleCategoryID1);
  featureArticle2=NR.getFeatureArticleByCategoryID(homepageInfo.articleCategoryID2);
  featureArticle3=NR.getFeatureArticleByCategoryID(homepageInfo.articleCategoryID3);
</cfscript>
    <!--Pod Header-->
    <div class="podHdr"><img src="images/imgHmPodsHdr.gif" width="731" height="11" alt="" /></div>
    <div class="threeColContainer">
      <cfoutput>
      <div class="nestedLftCol">
        <a href="index.cfm?md=homepage&amp;tmp=subhome&amp;shpid=#homepageInfo.subHomePageID1#" onmouseout="MM_swapImgRestore()" class="fadeThis featPreg"><span>#homepageInfo.podImageAltText1#</span></a></div>
      
      <div class="nestedCntrCol"><a href="index.cfm?md=homepage&amp;tmp=subhome&amp;shpid=#homepageInfo.subHomePageID2#" class="fadeThis featWell"><span>#homepageInfo.podImageAltText2#</span></a></div>
      
      <div class="nestedRtCol"><a href="index.cfm?md=homepage&amp;tmp=subhome&amp;shpid=#homepageInfo.subHomePageID3#" class="fadeThis featTreat"><span>#homepageInfo.podImageAltText3#</span></a></div>
      </cfoutput>
    </div>
    <!--Events-->
    <div class="threeColContainer">
      <cfoutput>
      <div class="nestedLftCol">
        <div class="eventsHm">
          <cfif Compare(featureClass1.courseTitle,"")>
          <div class="eventDate">
            <div class="monthAbbr">#DateFormat(featureClass1.startDateTime,"mmm")#</div>
            <div class="dayNumber">#DateFormat(featureClass1.startDateTime,"dd")#</div>
          </div>
          <cfset classTime=TimeFormat(featureClass1.startDateTime,"h:mm tt")>
          <cfset classTime=Replace(Replace(classTime,"AM","am"),"PM","pm")>
          <div class="eventDescrip"><a href="index.cfm?md=class&amp;tmp=detail&amp;cid=#featureClass1.courseID#&amp;tid=#Application.template.templateID_Class#" class="lnkEventName">#featureClass1.courseTitle#</a> #classTime#<br />
          <cfelse>
          <div class="eventDate">
            <div class="monthAbbr">&nbsp;</div>
            <div class="dayNumber">&nbsp;</div>
          </div>
          <div class="eventDescrip">&nbsp;<br />
          </cfif>
          <a href="index.cfm?md=class&amp;tmp=category&amp;catid=#homepageInfo.classCategoryID1#&amp;tid=#Application.template.templateID_Class#" class="lnkViewAll">View All Classes &raquo;</a></div>
        </div>
      </div>
      <div class="nestedCntrCol">
        <div class="eventsHm">
          <cfif Compare(featureClass2.courseTitle,"")>
          <div class="eventDate">
            <div class="monthAbbr">#DateFormat(featureClass2.startDateTime,"mmm")#</div>
            <div class="dayNumber">#DateFormat(featureClass2.startDateTime,"dd")#</div>
          </div>
          <cfset classTime=TimeFormat(featureClass2.startDateTime,"h:mm tt")>
          <cfset classTime=Replace(Replace(classTime,"AM","am"),"PM","pm")>
          <div class="eventDescrip"><a href="index.cfm?md=class&amp;tmp=detail&amp;cid=#featureClass2.courseID#&amp;tid=#Application.template.templateID_Class#" class="lnkEventName">#featureClass2.courseTitle#</a> #classTime#<br />
          <cfelse>
          <div class="eventDate">
            <div class="monthAbbr">&nbsp;</div>
            <div class="dayNumber">&nbsp;</div>
          </div>
          <div class="eventDescrip">&nbsp;<br />
          </cfif>
          <a href="index.cfm?md=class&amp;tmp=category&amp;catid=#homepageInfo.classCategoryID2#&amp;tid=#Application.template.templateID_Class#" class="lnkViewAll">View All Classes &raquo;</a></div>
        </div>
      </div>
      <div class="nestedRtCol">
        <div class="eventsHm">
          <cfif Compare(featureClass3.courseTitle,"")>
          <div class="eventDate">
            <div class="monthAbbr">#DateFormat(featureClass3.startDateTime,"mmm")#</div>
            <div class="dayNumber">#DateFormat(featureClass3.startDateTime,"dd")#</div>
          </div>
          <cfset classTime=TimeFormat(featureClass3.startDateTime,"h:mm tt")>
          <cfset classTime=Replace(Replace(classTime,"AM","am"),"PM","pm")>
          <div class="eventDescrip"><a href="index.cfm?md=class&amp;tmp=detail&amp;cid=#featureClass3.courseID#&amp;tid=#Application.template.templateID_Class#" class="lnkEventName">#featureClass3.courseTitle#</a> #classTime#<br />
          <cfelse>
          <div class="eventDate">
            <div class="monthAbbr">&nbsp;</div>
            <div class="dayNumber">&nbsp;</div>
          </div>
          <div class="eventDescrip">&nbsp;<br />
          </cfif>
          <a href="index.cfm?md=class&amp;tmp=category&amp;catid=#homepageInfo.classCategoryID3#&amp;tid=#Application.template.templateID_Class#" class="lnkViewAll">View All Classes &raquo;</a></div>
        </div>
      </div>
      </cfoutput>
    </div>
    <!--Horizontal Dividers-->
    <div class="threeColContainer">
      <div class="nestedLftCol"><img src="images/imgHmPodsDivider.gif" width="223" height="33" alt="" /></div>
      <div class="nestedCntrCol"><img src="images/imgHmPodsDivider.gif" width="223" height="33" alt="" /></div>
      <div class="nestedRtCol"><img src="images/imgHmPodsDivider.gif" width="223" height="33" alt="" /></div>
    </div>
    <!--News-->
    <div class="threeColContainer">
      <cfoutput>
      <div class="nestedLftCol">
        <cfif Compare(featureArticle1.headline,"")>
        <div class="newsHm"><a href="#featureArticle1.href#" <cfif featureArticle1.popup>target="_blank"</cfif>>#featureArticle1.headline#</a> #featureArticle1.teaser#</div>
        <cfelse>
        <div class="newsHm">&nbsp;</div>
        </cfif>
      </div>
      <div class="nestedCntrCol">
        <cfif Compare(featureArticle2.headline,"")>
        <div class="newsHm"><a href="#featureArticle2.href#" <cfif featureArticle2.popup>target="_blank"</cfif>>#featureArticle2.headline#</a> #featureArticle2.teaser#</div>
        <cfelse>
        <div class="newsHm">&nbsp;</div>
        </cfif>
      </div>
      <div class="nestedRtCol">
        <cfif Compare(featureArticle3.headline,"")>
        <div class="newsHm"><a href="#featureArticle3.href#" <cfif featureArticle3.popup>target="_blank"</cfif>>#featureArticle3.headline#</a> #featureArticle3.teaser#</div>
        <cfelse>
        <div class="newsHm">&nbsp;</div>
        </cfif>
      </div>
      </cfoutput>
    </div>
    <!--Horizontal Dividers-->
    <div class="threeColContainer">
      <div class="nestedLftCol"><img src="images/imgHmPodsDivider.gif" width="223" height="33" alt="" /></div>
      <div class="nestedCntrCol"><img src="images/imgHmPodsDivider.gif" width="223" height="33" alt="" /></div>
      <div class="nestedRtCol"><img src="images/imgHmPodsDivider.gif" width="223" height="33" alt="" /></div>
    </div>