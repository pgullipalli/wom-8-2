function updateSubHomepageInfo() {
	$("#frmSubHomePages").ajaxSubmit({ 
		url: 'action.cfm?md=homepage&task=updateSubHomepageInfo',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgSubHomepages").html('The sub-home page content has been updated.');	
		  $("#frmSubHomePages div").show();
		  location.href="#pagetop";
		} else {
		  alert (jsonData.MESSAGE);
		}
	}
}

