<cfscript>
  CL=CreateObject("component", "com.ClassAdmin").init();
  NR=CreateObject("component", "com.NewsroomAdmin").init();
  PG=CreateObject("component", "com.PhotoGalleryAdmin").init();  
  allClassCategories=CL.getAllCategories();
  allArticleCategories=NR.getAllArticleCategories();
  allAlbumCategories=PG.getAllAlbumCategories();
</cfscript>

<form id="frmAddSubHomePage" name="frmAddSubHomePage" onsubmit="return false">
<div class="row">
	<div class="col-30-right"><b>Sub-Home Page Name:</b></div>
    <div class="col-68-left"><input type="text" name="subHomePageName" maxlength="100" value="" style="width:320px;" validate="required:true" /></div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="published" value="1" /> Publish this sub-home page
    </div>
</div>
<div class="row">
  <div class="col-30-right">&nbsp;</div><div class="col-68-left"><span class="subHeader">Welcome Message</span></div>
</div>
<div class="row">
	<div class="col-30-right">Message:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmAddSubHomePage"
            fieldName="welcomeMessage"
            cols="60"
            rows="3"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Welcome Message">
    </div>
</div>
<div class="row">
  <div class="col-30-right">&nbsp;</div><div class="col-68-left"><span class="subHeader">News</span></div>
</div>
<div class="row">
	<div class="col-30-right">Select a Category in Information Library:</div>
    <div class="col-68-left">
      <select name="articleCategoryID" style="min-width:320px;">
        <option value="0"></option>
        <cfoutput query="allArticleCategories">
        <option value="#articleCategoryID#">#articleCategory#</option>  
        </cfoutput>
      </select>
    </div>
</div>
<div class="row">
  <div class="col-30-right">&nbsp;</div><div class="col-68-left"><span class="subHeader">Featured and Upcoming Class</span></div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="showFeatureClass" value="1" /> Show featured and upcoming class
    </div>
</div>
<div class="row">
	<div class="col-30-right">Intro Copy:</div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmAddSubHomePage"
            fieldName="featureClassIntro"
            cols="60"
            rows="2"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Intro Copy">
    </div>
</div>
<div class="row">
	<div class="col-30-right">Select a Category in Class Manager:</div>
    <div class="col-68-left">
      <select name="classCategoryID" style="min-width:320px;">
        <option value="0"></option>
        <cfoutput query="allClassCategories">
        <option value="#categoryID#">#categoryName#</option>  
        </cfoutput>
      </select>
    </div>
</div>
<div class="row">
  <div class="col-30-right">&nbsp;</div><div class="col-68-left"><span class="subHeader">Featured Photo Album</span></div>
</div>
<div class="row">
	<div class="col-30-right">&nbsp;</div>
    <div class="col-68-left">
      <input type="checkbox" name="showFeatureAlbum" value="1" /> Show featured photo album
    </div>
</div>
<div class="row">
	<div class="col-30-right">Select a Category in Photo Gallery:</div>
    <div class="col-68-left">
      <select name="albumCategoryID" style="min-width:320px;">
        <option value="0"></option>
        <cfoutput query="allAlbumCategories">
        <option value="#albumCategoryID#">#albumCategory#</option>  
        </cfoutput>
      </select>
    </div>
</div>
</form>
