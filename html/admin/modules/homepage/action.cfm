<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  HP=CreateObject("component", "com.HomepageAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";
  
  switch (URL.task) {
    case "updateHomepageInfo":
	  HP.updateHomepageInfo(argumentCollection=Form);
	  break;
	  
    case "addSubHomePage":
	  HP.addSubHomePage(argumentCollection=Form);
	  break;
	
	case "editSubHomePage":
	  HP.editSubHomePage(argumentCollection=Form);
	  break;
	  
	case "deleteSubHomePage":
	  HP.deleteSubHomePage(URL.subHomePageID);
	  break;
	  
	default:
	  break;
  }
</cfscript>
<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>