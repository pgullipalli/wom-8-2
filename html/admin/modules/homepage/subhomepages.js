$(function () {	
	loadSubHomePageList();
});

function loadSubHomePageList(pNum) { 
  var url="index.cfm?md=homepage&tmp=snip_subhomepage_list&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function addSubHomePage() {
  xsite.createModalDialog({
	windowName: 'winAddSubHomePage',
	title: 'Add SubHomePage',
	width: 650,
	position: 'center-up',
	url: 'index.cfm?md=homepage&tmp=snip_add_subhomepage&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddSubHomePage").dialog('close'); },
	  ' Add ': submitForm_AddSubHomePage
	}
  }).dialog('open');
  
  function submitForm_AddSubHomePage() {
	var form = $("#frmAddSubHomePage");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=homepage&task=addSubHomePage',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddSubHomePage").dialog('close');
		loadSubHomePageList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editSubHomePage(sID) {
  var form = $("#frmSubHomePageList")[0];
  var subHomePageID = null;
  if (sID) subHomePageID = sID;
  else subHomePageID = xsite.getCheckedValue(form.subHomePageID);

  if (!subHomePageID) {
	alert("Please select a subhomepage to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditSubHomePage',
	title: 'Edit SubHomePage',
	width: 650,
	position: 'center-up',
	url: 'index.cfm?md=homepage&tmp=snip_edit_subhomepage&subHomePageID=' + subHomePageID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditSubHomePage").dialog('close'); },
	  ' Save ': submitForm_EditSubHomePage
	}
  }).dialog('open');
	
  function submitForm_EditSubHomePage() {
	var form = $("#frmEditSubHomePage");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=homepage&task=editSubHomePage',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
		
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditSubHomePage").dialog('close');
		loadSubHomePageList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteSubHomePage() {
  var form = $("#frmSubHomePageList")[0];
  var subHomePageID = xsite.getCheckedValue(form.subHomePageID);
  if (!subHomePageID) {
	alert("Please select a subhomepage to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected subhomepage?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=homepage&task=deleteSubHomePage&subHomePageID=' + subHomePageID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadSubHomePageList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

