<cfparam name="URL.sourceID">
<cfset CF=CreateObject("component", "com.CustomFormAdmin").init()>
<cfset sourceInfo=CF.getSourceByID(URL.sourceID)>

<cfoutput query="sourceInfo">
<form id="frmEditSource" name="frmEditSource" onsubmit="return false">
<input type="hidden" name="sourceID" value="#URL.sourceID#" />
<div class="row">
	<div class="col-30-right"><b>Source Name:</b></div>
    <div class="col-68-left"><input type="text" name="sourceName" maxlength="200" value="#HTMLEditFormat(sourceName)#" style="width:200px;" validate="required:true"></div>
</div>
<div class="row">
	<div class="col-30-right"><b>Tracking Code:</b></div>
    <div class="col-68-left">
      <input type="text" name="sourceCode" maxlength="30" value="#HTMLEditFormat(sourceCode)#" style="width:50px;" validate="required:true"><br />
      <small>(Enter a short unique code using only letters and numbers.)</small>
    </div>
</div>
</form>
</cfoutput>