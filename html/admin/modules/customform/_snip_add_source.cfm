<cfparam name="URL.formID">

<form id="frmAddSource" name="frmAddSource" onsubmit="return false">
<cfoutput><input type="hidden" name="formID" value="#URL.formID#" /></cfoutput>
<div class="row">
	<div class="col-30-right"><b>Source Name:</b></div>
    <div class="col-68-left"><input type="text" name="sourceName" maxlength="200" value="" style="width:200px;" validate="required:true"></div>
</div>
<div class="row">
	<div class="col-30-right"><b>Tracking Code:</b></div>
    <div class="col-68-left">
      <input type="text" name="sourceCode" maxlength="30" value="" style="width:50px;" validate="required:true"><br />
      <small>(Enter a short unique code using only letters and numbers.)</small>
    </div>
</div>
</form>