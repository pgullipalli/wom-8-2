function updateFormSetting(formID, messageBoxID) {
	$("#" + formID).ajaxSubmit({ 
		url: 'action.cfm?md=customform&task=updateFormSetting',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#" + messageBoxID).html('The form settings have been updated.');	
		  $("#" + formID + " div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}
}