<cfparam name="URL.startDate" default="#DateFormat(DateAdd("m",-1,now()),"mm/dd/yyyy")#">
<cfparam name="URL.endDate" default="#DateFormat(now(),"mm/dd/yyyy")#">

<script type="text/javascript" src="modules/customform/export_submission_data.js"></script>
<cfset CF=CreateObject("component", "com.CustomFormAdmin").init()>
<cfset allForms=CF.getAllForms()>

<div class="header">CUSTOM FORMS &gt; Export Submission Data</div>

<form id="frmExportDate" name="frmExportDate" onSubmit="return false;">

Form:
<select name="formID" style="min-width:250px;">
  <cfoutput query="allForms">
  <option value="#formID#">#formName#</option>
  </cfoutput>
</select><br /><br />
      
<p class="subHeader">Select a date range to export</p>

<cfoutput>
From:
<input type="text" name="startDate" size="14" maxlength="12" value="#URL.startDate#" class="dateField">
&nbsp;&nbsp;&nbsp;

To:
<input type="text" name="endDate" size="14" maxlength="12" value="#URL.endDate#" class="dateField">

</cfoutput>

<input type="button" value=" Export Submission Data " onclick="exportSubmissionData();">

<br style="clear:both" /><br />

<div class="ui-widget" style="display:none; margin:10px 0px; width:450px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgResponse"></span></p>
  </div>
</div>
</form>


