$(function () {	
  $(".dateField").datepicker();  
})

function exportSubmissionData() {
  var form = document.frmExportDate;
  var formID=$("#frmExportDate select[name='formID']").val();
  var startDate=form.startDate.value;
  var endDate=form.endDate.value;
  
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=customform&task=exportSubmissionData&formID=' + formID + '&startDate=' + escape(startDate) + '&endDate=' + endDate + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  
	  var msg = 'The submission data file has been generated. Click the file name below to download the file to your computer.<br /><br />' +
	  			'<a href="index.cfm?md=customform&tmp=download_submission_data&fileName=' + escape(jsonData.FILENAME) +'"><b>' + jsonData.FILENAME + '</b></a>';
	  $("#msgResponse").html(msg);	
	  $("#frmExportDate div").show();
	  
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}