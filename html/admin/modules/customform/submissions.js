var pageNum=1;//initial page number
var sortedBy="date";
var trend="DESC";

$(function () {	
	loadSubmissionList();
	$("#frmNavigator select[name='formID']").change(function() {
	  pageNum=1;
	  loadSubmissionList();	  
	});
});

function loadSubmissionList(pNum) {
  if (pNum) pageNum = pNum;
  var formID=$("#frmNavigator select[name='formID']").val();	
  
  var url="index.cfm?md=customform&tmp=snip_submission_list&formID=" + formID + "&pageNum=" + pageNum + "&sortedBy=" + sortedBy + "&trend=" + trend + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

/*
function viewSubmission(sid) {
  var form = $("#frmSubmissionList")[0];
  
  var submissionID = null;
  if (sid) submissionID = sid;
  else submissionID = xsite.getCheckedValue(form.submissionID);

  if (!submissionID) {
	alert("Please select a submission to preview.");
	return;
  }
  
  if (submissionID.indexOf(",") != -1) {
	alert("Please select only one submission to preview.");
	return;
  }
  
  xsite.createModalDialog({
	windowName: 'winPreview',
	title: 'View Submission',
	width: 650,
	height: 500,
	position: 'center-up',
	url: 'index.cfm?md=customform&tmp=snip_preview_submission&submissionID=' + submissionID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'OK': function() { $("#winPreview").dialog('close'); }
	}
  }).dialog('open');  
}
*/

function viewSubmission(sid) {
  var form = $("#frmSubmissionList")[0];
  
  var submissionID = null;
  if (sid) submissionID = sid;
  else submissionID = xsite.getCheckedValue(form.submissionID);

  if (!submissionID) {
	alert("Please select a submission to preview.");
	return;
  }
  
  if (submissionID.indexOf(",") != -1) {
	alert("Please select only one submission to preview.");
	return;
  }
  
  xsite.openWindow('index.cfm?md=customform&tmp=snip_preview_submission&wrap=1&submissionID=' + submissionID,'viewSubmission'+submissionID,650,500); 
}

function deleteSubmission() {
  var form = $("#frmSubmissionList")[0];
  var submissionID = xsite.getCheckedValue(form.submissionID);
  if (!submissionID) {
	alert("Please select a submission to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected submission?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=customform&task=deleteSubmission&submissionIDList=' + submissionID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadSubmissionList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}
