<cfparam name="URL.formID" default="#Application.customform.customFormID_Donation#">
<cfparam name="URL.startDate">
<cfparam name="URL.endDate">

<cfset CF=CreateObject("component", "com.CustomFormAdmin").init()>
<cfset allSources=CF.getAllSources(URL.formID)>
<cfset tracking=CF.getSourceTracking(formID=URL.formID, startDate=URL.startDate, endDate=URL.endDate)>

<cfif allSources.recordcount GT 0>
<form id="frmSourceList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th width="250">Source Name</a></th>
    <th width="120">Tracking Code</th>
    <th width="250">URL</th>
    <th width="80"># Sub.</th>
    <th>&nbsp;</th>
  </tr> 
  
  <cfoutput query="allSources">
  <tr>
    <td align="center"><input type="checkbox" name="sourceID" value="#sourceID#"></td>
	<td><a href="javascript:editSource('#sourceID#')" class="accent01">#sourceName#</a></td>    
	<td>#sourceCode#</td>
    <td>/donate/?src=#sourceCode#</td>
    <td align="center">
      <cfset found=false>
      <cfloop query="tracking">
        <cfif Not Compare(allSources.sourceCode, tracking.sourceCode)>          
      	  #numSubmissions#
          <cfset found=true><cfbreak>
        </cfif>
      </cfloop>
      <cfif Not found>0</cfif>
    </td>
    <td>&nbsp;</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>