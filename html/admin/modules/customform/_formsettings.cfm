<script type="text/javascript" src="modules/customform/formsettings.js"></script>

<cfset CF=CreateObject("component", "com.CustomFormAdmin").init()>
<cfset donationFormInfo=CF.getFormInfo(Application.customform.customFormID_Donation)>

<div class="header">Donation Form</div>

<cfoutput query="donationFormInfo">
<form name="frmDonation" id="frmDonation" onSubmit="return false;">
<input type="hidden" name="formID" value="#Application.customform.customFormID_Donation#" />

<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgDonation"></span></p>
  </div>
</div>

<div class="subHeader">Introduction Text</div>
<CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="no"
            formName="frmDonation"
            fieldName="introductionText"
            cols="62"
            rows="3"
            HTMLContent="#donationFormInfo.introductionText#"
            displayHTMLSource="true"
            editorHeader="Edit Introduction Text">
            
<br style="clear:both;" />

<div class="subHeader">Submission Notification Email</div>  
      

<div class="row">
  <strong>Email Subject Line:</strong><br />
  <input type="text" name="notificationEmailSubject" maxlength="100" style="width:350px;" value="#HTMLEditFormat(notificationEmailSubject)#">
</div>

<div class="row">
  <strong>Set Reply To:</strong><br />
  <input type="text" name="notificationEmailReplyAddress" maxlength="100" style="width:350px;" value="#HTMLEditFormat(notificationEmailReplyAddress)#">
</div>

<div class="row">
  <strong>Notification Email Address(es):</strong><br />  
  <input type="text" name="notificationEmailRecipients" maxlength="200" style="width:350px;" value="#HTMLEditFormat(notificationEmailRecipients)#"><br />
  (Separate multiple addresses with commas)
</div>

<br style="clear:both;" />


<div class="subHeader">Thank You Page/Email</div>

<div class="row">
  <strong>Email Subject Line:</strong><br />
  <input type="text" name="thankyouEmailSubject" maxlength="100" style="width:350px;" value="#HTMLEditFormat(thankyouEmailSubject)#">
</div>

<div class="row">
  <strong>Set Reply To:</strong><br />
  <input type="text" name="thankyouEmailReplyAddress" maxlength="100" style="width:350px;" value="#HTMLEditFormat(thankyouEmailReplyAddress)#">
</div>

<div class="row">
  <strong>Thank you message (copy to display after a form is submitted):</strong><br />
  <textarea name="thankyouMessage" style="width:350px;height:100px;">#HTMLEditFormat(thankyouMessage)#</textarea>  
  <!--- <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmDonation"
            fieldName="thankyouMessage"
            cols="60"
            rows="3"
            HTMLContent="#thankyouMessage#"
            displayHTMLSource="true"
            editorHeader="Edit Message"> --->
</div>

<input type="button" value=" Update " onclick="updateFormSetting('frmDonation','msgDonation');">
</form>
</cfoutput>

