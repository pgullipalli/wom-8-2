<cfparam name="URL.formID">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">
<cfparam name="URL.sortedBy" default="date">
<cfparam name="URL.trend" default="DESC">

<cfset CF=CreateObject("component", "com.CustomFormAdmin").init()>
<cfset structSubmissions=CF.getSubmissions(formID=URL.formID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage, sortedBy=URL.sortedBy, trend=URL.trend)>

<cfif structSubmissions.numDisplayedItems GT 0>
<form id="frmSubmissionList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th width="150">Submission Date</a></th>
    <th width="150">Name</th>
    <th width="200">Email Address</th>
    <th width="250">Source</th>
    <th>&nbsp;</th>
  </tr> 
  
  <cfoutput query="structSubmissions.submissions">
  <tr>
    <td align="center"><input type="checkbox" name="submissionID" value="#submissionID#"></td>
	<td align="center"><a href="javascript:viewSubmission('#submissionID#')">#DateFormat(submissionDate, "mm/dd/yyyy")# #TimeFormat(submissionDate, "h:mm tt")#</a></td>    
	<td align="center">#firstName# #lastName#&nbsp;</td>
    <td>#email#&nbsp;</td>
    <td>#sourceName#&nbsp;</td>
    <td>#DollarFormat(amount)#</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif structSubmissions.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structSubmissions.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadSubmissionList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>