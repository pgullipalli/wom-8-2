<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  CF=CreateObject("component", "com.CustomFormAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";
  
  switch (URL.task) {
    case "updateFormSetting":
	  CF.updateFormSetting(argumentCollection=Form);
	  break;
	  
	case "deleteSubmission":
	  CF.deleteSubmission(URL.submissionIDList);
	  break;
	  
	case "exportSubmissionData":	
	  fileDirectory="#Application.siteDirectoryRoot#/admin/modules/customform/temp/";	
	  returnedStruct.FILENAME=CF.exportSubmissionData(formID="#URL.formID#", startDate="#URL.startDate#", endDate="#URL.endDate#", fileDirectory="#fileDirectory#");
	  break;

	case "addSource":
	  CF.addSource(argumentCollection=Form);
	  break;
	  
	case "editSource":
	  CF.editSource(argumentCollection=Form);
	  break;
	  
	case "deleteSource":
	  CF.deleteSource(URL.sourceID);
	  break;	
		  
	default:
	  break;
  }
</cfscript>
<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>