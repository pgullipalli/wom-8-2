<cfparam name="URL.startDate" default="#DateFormat(DateAdd("m",-1,now()),"mm/dd/yyyy")#">
<cfparam name="URL.endDate" default="#DateFormat(now(),"mm/dd/yyyy")#">

<script type="text/javascript" src="modules/customform/sources.js"></script>

<cfset CF=CreateObject("component", "com.CustomFormAdmin").init()>
<cfset allForms=CF.getAllForms()>

<div class="header">Donation Form - Source Tracking</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addSource();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editSource();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteSource();">Delete</a>
</div>

<br style="clear:both;" />
<br />

<form id="frmNavigator">
  Form
  <select name="formID" style="min-width:250px;">
    <cfoutput query="allForms">
    <option value="#formID#">#formName#</option>
    </cfoutput>
  </select>
</form>

<br />

<cfoutput>
<form id="frmDateRange" name="frmDateRange" onSubmit="return false;">
From:
<input type="text" name="startDate" size="14" maxlength="12" value="#URL.startDate#" class="dateField">
&nbsp;&nbsp;&nbsp;

To:
<input type="text" name="endDate" size="14" maxlength="12" value="#URL.endDate#" class="dateField">

<input type="button" value=" Submit " onclick="loadSourceList();">
</form>
</cfoutput>

<br style="clear:both;" />
<br />

<div id="mainDiv"></div>

<br style="clear:both;" />
<br /><br />