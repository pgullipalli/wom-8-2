var formID;

$(function () {	
  formID=$("#frmNavigator select[name='formID']").val();
  $(".dateField").datepicker(); 
  loadSourceList();
});

function loadSourceList() {
  var startDate=$("#frmDateRange input[name='startDate']").val();	
  var endDate=$("#frmDateRange input[name='endDate']").val();
  
  var url="index.cfm?md=customform&tmp=snip_source_list&formID=" + formID + "&startDate=" + escape(startDate) + "&endDate=" + escape(endDate) + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function addSource() {
  xsite.createModalDialog({
	windowName: 'winAddSource',
	title: 'Add Source',
	width: 450,
	position: 'center-up',
	url: 'index.cfm?md=customform&tmp=snip_add_source&formID=' + formID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddSource").dialog('close'); },
	  ' Add ': submitForm_AddSource
	}
  }).dialog('open');
  
  function submitForm_AddSource() {
	var form = $("#frmAddSource");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=customform&task=addSource',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  beforeSubmit: validate,
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function validate() {
	  var form=$("#frmAddSource")[0];
	  var code=form.sourceCode.value;
	  var pattern=/^[0-9a-zA-Z]+$/i;
	  if (!pattern.test(code)) {
		alert('Please use only letters and numbers for the unique tracking code.');
		return false;
	  } else {
		return true;  
	  }
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddSource").dialog('close');
		loadSourceList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editSource(sID) {
  var form = $("#frmSourceList")[0];
  var sourceID = null;
  if (sID) sourceID = sID;
  else sourceID = xsite.getCheckedValue(form.sourceID);

  if (!sourceID) {
	alert("Please select a source to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditSource',
	title: 'Edit Source',
	width: 450,
	position: 'center-up',
	url: 'index.cfm?md=customform&tmp=snip_edit_source&formID=' + formID + '&sourceID=' + sourceID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditSource").dialog('close'); },
	  ' Save ': submitForm_EditSource
	}
  }).dialog('open');
	
  function submitForm_EditSource() {
	var form = $("#frmEditSource");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=customform&task=editSource',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  beforeSubmit: validate,
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function validate() {
	  var form=$("#frmEditSource")[0];
	  var code=form.sourceCode.value;
	  var pattern=/^[0-9a-zA-Z]+$/i;
	  if (!pattern.test(code)) {
		alert('Please use only letters and numbers for the unique tracking code.');
		return false;
	  } else {
		return true;  
	  }
	}
		
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditSource").dialog('close');
		loadSourceList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteSource() {
  var form = $("#frmSourceList")[0];
  var sourceID = xsite.getCheckedValue(form.sourceID);
  if (!sourceID) {
	alert("Please select a source to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected item?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=customform&task=deleteSource&sourceID=' + sourceID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadSourceList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}