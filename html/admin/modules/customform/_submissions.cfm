<script type="text/javascript" src="modules/customform/submissions.js"></script>

<cfset CF=CreateObject("component", "com.CustomFormAdmin").init()>
<cfset allForms=CF.getAllForms()>

<div class="header">CUSTOM FORMS &gt; Submissions</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteSubmission();">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="viewSubmission();">View Submission</a>
</div>

<br style="clear:both" /><br />

<form id="frmNavigator">
  Form
  <select name="formID" style="min-width:250px;">
    <cfoutput query="allForms">
    <option value="#formID#">#formName#</option>
    </cfoutput>
  </select>
</form>

<div id="mainDiv"></div>