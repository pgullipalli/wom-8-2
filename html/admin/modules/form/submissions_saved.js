var pageNum=1;//initial page number

$(function () {
	loadSubmissionList();
	
	$("#frmNavigator select[name='formID']").change(function() {
	  pageNum=1;
	  loadSubmissionList();	  
	});
});

function loadSubmissionList(pNum) {
  if (pNum) pageNum = pNum;
  else pageNum=1;
  var formID=$("#frmNavigator select[name='formID']").val();	
  
  var url="index.cfm?md=form&tmp=snip_submission_list_saved&formID=" + formID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function viewSubmission(formID, submissionID) {
  xsite.createModalDialog({
	windowName: 'winVewSubmission',
	title: 'View Submission',
	width: 600,
	height: 500,
	position: 'center',
	url: 'index.cfm?md=form&tmp=snip_view_submission_saved&formID=' + formID + '&submissionID=' + submissionID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'OK': function() { $("#winVewSubmission").dialog('close'); }
	}
  }).dialog('open');
}