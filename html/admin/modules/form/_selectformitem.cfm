<cfparam name="URL.formID">
<cfparam name="URL.groupID">
<cfparam name="URL.tabIndex">
<cfparam name="URL.formItemID" default="0">


<h2>FORM MANAGER &gt;&gt; Select a Form Item</h2>

<p>
There are six main types of form items, which are listed below with a corresponding example to the right of each.
(Do not edit the examples.)<br>
<b>To specify content/characteristics for a new form item,</b>
click "Select" next to the type of item you wish to create.
</p>

<hr size="1" noshade>

<form>
<b>Select a form Item type:<br><br></b>
<table width="100%" align="center" border="0" cellpadding="3" cellspacing="1" class="tbGridLineColor">
  <tr align="center">
    <th>&nbsp;</th>
	<th>Type</th>
	<th>Example<br>(Click "Select" at left to edit.)</th>
  </tr>
  <tr>
    <td align="center"><cfoutput><a href="index.cfm?md=form&tmp=addformitem&popup=1&type=Instructional+Text&formID=#URL.formID#&groupID=#URL.groupID#&formItemID=#URL.formItemID#&tabIndex=#URL.tabIndex#"><b>Select</b></a></cfoutput></td>
	<td><b>Instructional Text</b></td>
	<td valign="top"><b>Please check all boxes that are applied:</b></td>
  </tr>
  <tr>
    <td align="center"><cfoutput><a href="index.cfm?md=form&tmp=addformitem&popup=1&type=Short+Text&formID=#URL.formID#&groupID=#URL.groupID#&formItemID=#URL.formItemID#&tabIndex=#URL.tabIndex#"><b>Select</b></a></cfoutput></td>
	<td><b>Input Field</b></td>
	<td valign="top"><b>Your Name: </b><br><input type="text" name="inputfield" size="20"></td>
  </tr>
  <tr>
    <td align="center"><cfoutput><a href="index.cfm?md=form&tmp=addformitem&popup=1&type=Long+Text&formID=#URL.formID#&groupID=#URL.groupID#&formItemID=#URL.formItemID#&tabIndex=#URL.tabIndex#"><b>Select</b></a></cfoutput></td>
	<td><b>Text Area</b></td>
	<td valign="top"><b>Your Comment: </b><br><textarea name="textarea" rows="3" cols="40"></textarea></td>
  </tr>
  <tr>
    <td align="center"><cfoutput><a href="index.cfm?md=form&tmp=addformitem&popup=1&type=Radio+Group&formID=#URL.formID#&groupID=#URL.groupID#&formItemID=#URL.formItemID#&tabIndex=#URL.tabIndex#"><b>Select</b></a></cfoutput></td>
	<td><b>Radio Group</b></td>
	<td valign="top">
	  <b>Gender:</b><br>
	  <input type="radio" name="radiogroup" value="1" checked> Yes
	  <input type="radio" name="radiogroup" value="0"> No
	</td>
  </tr>
  <tr>
    <td align="center"><cfoutput><a href="index.cfm?md=form&tmp=addformitem&popup=1&type=Check+Boxes&formID=#URL.formID#&groupID=#URL.groupID#&formItemID=#URL.formItemID#&tabIndex=#URL.tabIndex#"><b>Select</b></a></cfoutput></td>
	<td><b>Check Boxes</b></td>
	<td valign="top">
	  <b>Your Interests: </b><br>
	  <input type="checkbox" name="checkbox1" value="0"> Fishing
	  <input type="checkbox" name="checkbox1" value="0"> Reading
	  <input type="checkbox" name="checkbox1" value="0"> Cooking
	</td>
  </tr>
  <tr>
    <td align="center"><cfoutput><a href="index.cfm?md=form&tmp=addformitem&popup=1&type=Select+Menu&formID=#URL.formID#&groupID=#URL.groupID#&formItemID=#URL.formItemID#&tabIndex=#URL.tabIndex#"><b>Select</b></a></cfoutput></td>
	<td><b>Select Menu</b></td>
	<td valign="top">
	  <b>Your State:</b><br>
	  <select name="list">	  
	    <option value="">Alabama</option>
		<option value="">Alaska</option>
		<option value="">Arkansa</option>
	  </select>
	</td>
  </tr>
</table>

</form>
