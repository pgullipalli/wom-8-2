<cfparam name="URL.task">

<cfset FMM=CreateObject("component", "#APPLICATION.componentPath#.modules.form.FormAdmin")>

<cfswitch expression="#URL.task#">
  <cfcase value="addForm">
    <cfset formID=FMM.addForm(argumentCollection=FORM)>
	<cflocation url="index.cfm?md=form&tmp=manageformitems&formID=#formID#">
  </cfcase>
  
  <cfcase value="updateForm">
    <cfset formID=FMM.updateForm(argumentCollection=FORM)>
	<cflocation url="index.cfm?md=form&tmp=manageformitems&formID=#formID#">
  </cfcase>

  <cfcase value="deleteForm">
    <cfset FMM.deleteForm(URL.formID)>
	<cflocation url="index.cfm?md=form&tmp=forms">
  </cfcase>
  
  <cfcase value="cloneForm">
    <cfset formID=FMM.cloneForm(argumentCollection=FORM)>
	<cflocation url="index.cfm?md=form&tmp=manageformitems&formID=#formID#">
  </cfcase>

  <cfcase value="updateHeaderFooter">
    <cfset FMM.updateHeaderFooter(argumentCollection=FORM)>
	<cflocation url="index.cfm?md=form&tmp=reloadmanageformitems&formID=#FORM.formID#&tabIndex=#FORM.tabIndex#">
  </cfcase>
  
  <cfcase value="addFormItem">
    <cfset FMM.addFormItem(argumentCollection=FORM)>
	<cflocation url="index.cfm?md=form&tmp=reloadmanageformitems&formID=#FORM.formID#&tabIndex=#FORM.tabIndex#">
  </cfcase>
  
  <cfcase value="updateFormItem">    
    <cfset FMM.updateFormItem(argumentCollection=FORM)>
	<cflocation url="index.cfm?md=form&tmp=reloadmanageformitems&formID=#FORM.formID#&tabIndex=#FORM.tabIndex#">
  </cfcase>
  
  <cfcase value="deleteFormItem">
	<cfset FMM.deleteFormItem(URL.formItemID)>
	<cflocation url="index.cfm?md=form&tmp=manageformitems&formID=#URL.formID#&tabIndex=#URL.tabIndex#">
  </cfcase>  
  
  <cfcase value="addGroup">
    <cfset FMM.addGroup(URL.groupID)>
	<cflocation url="index.cfm?md=form&tmp=manageformitems&formID=#URL.formID#&tabIndex=#URL.tabIndex#">
  </cfcase>  
  
  <cfcase value="deleteGroup">
    <cfset FMM.deleteGroup(URL.groupID)>
    <cflocation url="index.cfm?md=form&tmp=manageformitems&formID=#URL.formID#">
  </cfcase>    
  
  <cfcase value="moveGroup">
    <cfset FMM.moveGroup(argumentCollection=URL)>
	<cflocation url="index.cfm?md=form&tmp=manageformitems&formID=#URL.formID#&tabIndex=#URL.tabIndex#">
  </cfcase>
  
  <cfcase value="moveFormItem">
    <cfset FMM.moveFormItem(argumentCollection=URL)>  
    <cflocation url="index.cfm?md=form&tmp=manageformitems&formID=#URL.formID#&tabIndex=#URL.tabIndex#">
  </cfcase>
 
  
  <cfcase value="deleteSubmissions">
	<cfset FMM.deleteSubmissions(FORM.submissionIDList)>
	<cfif IsDefined("FORM.searchAction")>
	  <cflocation url="index.cfm?md=form&tmp=searchsubmissions&formID=#FORM.formID#&formItemIDList=#URLEncodedFormat(FORM.formItemIDList)#&startIndex=#FORM.startIndex#&numItemsPerPage=#FORM.numItemsPerPage#&searchAction=1&keyword=#URLEncodedFormat(FORM.keyword)#">
	<cfelse>
	  <cflocation url="index.cfm?md=form&tmp=submissions_detailview&formID=#FORM.formID#&dateSubmitted=#URLEncodedFormat(FORM.dateSubmitted)#&startIndex=#FORM.startIndex#&numItemsPerPage=#FORM.numItemsPerPage#">
    </cfif>
  </cfcase>
  
  <cfcase value="moveSubmissionToSavedTable">  
    <cfparam name="FORM.formID">
    <cfparam name="FORM.isEmailList">
    <cfparam name="FORM.emailListID">  
    <cfparam name="FORM.emailForSavedData">
    <cfparam name="FORM.passwordForSavedData">
    
    <cfset submissionID=FMM.moveSubmissionToSavedTable(argumentCollection=FORM)>
    <cfif submissionID GT 0>
		<cfset FM=CreateObject("component", "#APPLICATION.componentPath#.modules.form.Form")>
        <cfset encodedSubmissionID=Encrypt("LA#submissionID#Recovery", "LRA")>
        <cfset formURL="#APPLICATION.siteURLRoot#/index.cfm?md=form&tmp=home&fmid=#FORM.formID#&emaillist=#FORM.isEmailList#&elid=#FORM.emailListID#">
        <cfset dataRetrievalURL="#APPLICATION.siteURLRoot#/index.cfm?md=form&tmp=home&fmid=#FORM.formID#&emaillist=#FORM.isEmailList#&elid=#FORM.emailListID#&code=#URLEncodedFormat(encodedSubmissionID)#">
        
        <cfset FM.sendConfirmationEmailForRetrievingData(emailForSavedData=FORM.emailForSavedData, passwordForSavedData=FORM.passwordForSavedData, formID=FORM.formID, formURL="#formURL#", dataRetrievalURL="#dataRetrievalURL#")>
    </cfif>
    <cfset CreateObject("component", "#APPLICATION.componentPath#.Utility").closeWindowAndReload()>
  </cfcase>
  
  <cfcase value="generateCSV">
    <cfset FORM.CSVDirectory="#APPLICATION.siteDirectoryRoot#/admin/modules/form/temp">
    <cfset fileName=FMM.generateCVS(argumentCollection=FORM)>
	<cflocation url="index.cfm?md=form&tmp=viewCSV&csv=#URLEncodedFormat(fileName)#&formID=#FORM.formID#&dateRange=#FORM.dateRange#&fromDate=#URLEncodedFormat(FORM.fromDate)#&toDate=#URLEncodedFormat(FORM.toDate)#&formItemIDs=#URLEncodedFormat(FORM.formItemIDs)#">
  </cfcase>

</cfswitch>