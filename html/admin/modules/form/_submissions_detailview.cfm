<cfparam name="URL.formID">
<cfparam name="URL.dateSubmitted">
<cfparam name="URL.startIndex" default="1">
<cfparam name="URL.numItemsPerPage" default="20">
<cfparam name="URL.dateRange" default="1">
<cfparam name="URL.fromDate" default="#URL.dateSubmitted#">
<cfparam name="URL.toDate" default="#URL.dateSubmitted#">

<cfset FMM=CreateObject("component", "#APPLICATION.componentPath#.modules.form.FormAdmin")>
<cfset formInfo=FMM.getFormInfoByFormID(URL.formID)>
<cfset numSubmissions=FMM.getNumSubmissions(URL.formID, URL.fromDate, URL.toDate)>
<cfset getSubmissions=FMM.getSubmissions(argumentCollection=URL)>

<h1>FORM MANAGER &gt;&gt; Submissions Detail View</h1>

<p>
Click the "Date Submitted" link for any form to view all the individual data submitted through that form.
</p>

<hr size="1" noshade>

<script type="text/javascript">
  function deletesubmissions(form) {
    if (form.submissionIDList) {
	  if (form.submissionIDList.length) {
	    var i = 0;
		var found = false;
		for (i=0; i<form.submissionIDList.length; i++) {
		  if (form.submissionIDList[i].checked) {
		    found = true;
			break;
		  }
		}
		if (!found) {
		  alert ("To delete submissions, please check at least one submission to delete.");
		  return false;
		}
	  } else {
	    if (!form.submissionIDList.checked) {
		  alert ("To delete submissions, please check at least one submission to delete.");
		  return false;
		}
	  }	
	} else {
	  return false;
	}
	
	if (!confirm("Are you sure you want to delete the selected submissions?")) return false;
	
	showwaiting();
    return true;
  }
  
  function checkall (form, isAll) {
    if (form.submissionIDList.length) {
	  for (var i = 0; i < form.submissionIDList.length; i++) {
	    if (isAll) {
		  form.submissionIDList[i].checked = true;
		} else {
		  form.submissionIDList[i].checked = false;
		}
	  }
	} else {
	  if (isAll) {
		form.submissionIDList.checked = true;
	  } else {
		form.submissionIDList.checked = false;
	  }	  
	}
  }
</script>

<cfoutput>
<h2>
Form: #formInfo.formName#<br>
Submissions: #numSubmissions# (#URL.fromDate#)
</h2>
</cfoutput>

<cfoutput>
<cfset endIndex = URL.startIndex + getSubmissions.numDisplayedSubmissions - 1>
<p>Display #URL.startIndex# - #endIndex# of #getSubmissions.numTotalSubmissions# Submissions</p>

<cfif getSubmissions.numDisplayedSubmissions GT 0>
<form name="form1" action="action.cfm?md=form&task=deleteSubmissions" method="post" onSubmit="return deletesubmissions(this)">
<input type="hidden" name="formID" value="#URL.formID#">
<input type="hidden" name="dateSubmitted" value="#URL.dateSubmitted#">
<input type="hidden" name="startIndex" value="#URL.startIndex#">
<input type="hidden" name="numItemsPerPage" value="#URL.numItemsPerPage#">

<table border="0" width="100%" cellpadding="3" cellspacing="1" class="tbGridLineColor">
  <tr align="center">
    <th width="10%">
	  <a href="javascript:checkall(document.form1, 1)"><b>All</b></a> &nbsp;
	  <a href="javascript:checkall(document.form1, 0)"><b>None</b></a>
	</th>
    <th>Date Submitted</th>
	<cfif formInfo.isSignUpForm>
	<th>E-Mail</th>
	<th>First Name</th>
	<th>Last Name</th>
	<cfelseif formInfo.useUserEmailAsSender>
	<th>E-Mail</th>
	</cfif>
    <cfloop index="idx" from="1" to="#getSubmissions.numColumns#">
  	<th>#getSubmissions.columnNames[idx]#</th>
	</cfloop>
  </tr>
  <cfset class="altrow">
  <cfloop index="idx1" from="1" to="#getSubmissions.numDisplayedSubmissions#">
  <cfif class IS "class=""altrow"""><cfset class=""><cfelse><cfset class="class=""altrow"""></cfif>
  <tr #class# valign="top">
    <td align="center"><input type="checkbox" name="submissionIDList" value="#getSubmissions.submissions[idx1].submissionID#"></td>
    <td align="center"><a href="javascript:openwindow('index.cfm?md=form&tmp=submissions_singleview&popup=1&formID=#URL.formID#&submissionID=#getSubmissions.submissions[idx1].submissionID#','_blank', 500, 500)">#DateFormat(getSubmissions.submissions[idx1].dateSubmitted,"mm/dd/yyyy")# #TimeFormat(getSubmissions.submissions[idx1].dateSubmitted, "h:mm tt")#</a></td>
	<cfif formInfo.isSignUpForm>
    <td>#getSubmissions.submissions[idx1].userEmail#&nbsp;</td>
	<td>#getSubmissions.submissions[idx1].userFirstName#&nbsp;</td>
	<td>#getSubmissions.submissions[idx1].userLastName#&nbsp;</td>	
	<cfelseif formInfo.useUserEmailAsSender>
	<td>#getSubmissions.submissions[idx1].userEmail#&nbsp;</td>
	</cfif>
	<cfloop index="idx2" from="1" to="#getSubmissions.numColumns#">
    <td>
	  #getSubmissions.submissions[idx1]["#getSubmissions.formItemIDs[idx2]#"]#
	  &nbsp;
    </td>
	</cfloop>
  </tr>
  </cfloop>  
  
</table>
<p align="center">
<input type="submit" class="button"  value=" delete selected submissions ">
</p>
</form>
</cfif>

<cfset CreateObject("component", "#APPLICATION.componentPath#.Utility").displayResultPageLinks("index.cfm?md=form&tmp=submissions_detailview&formID=#URL.formID#&dateSubmitted=#URLEncodedFormat(URL.dateSubmitted)#", "#getSubmissions.numTotalSubmissions#", "#URL.startIndex#", "#URL.numItemsPerPage#")>  

</cfoutput>

<p>
<a href="javascript:history.go(-1)"><b>Back Previous Page</b></a> | <a href="index.cfm?md=form&tmp=forms"><b>Back To All Forms</b></a><br>
</p>
