<cfparam name="URL.formID">
<cfparam name="URL.groupID">
<cfparam name="URL.fieldType">

<cfoutput>
<form id="frmAddFormItem" name="frmAddFormItem" onsubmit="return false;">
<input type="hidden" name="formID" value="#URL.formID#" />
<input type="hidden" name="groupID" value="#URL.groupID#" />
<input type="hidden" name="fieldType" value="#URL.fieldType#" />
<div class="row">
  <div class="col-32-right"><input type="checkbox" name="active" value="1" checked /></div>
  <div class="col-66-left">Set Active</div>
</div>

<cfswitch expression="#URL.fieldType#">
  <cfcase value="Text Only">
    <div class="row">
      <div class="col-32-right">Enter plain text or HTML:</div>
      <div class="col-66-left">
        <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteURLRoot#/scripts/css/styles_for_forms.css"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"            
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmAddFormItem"
            fieldName="instructionText"
            cols="60"
            rows="8"
            HTMLContent=""
            displayHTMLSource="true"
            editorHeader="Edit Instructional Text">
      </div>
    </div>
  </cfcase>
  <cfdefaultcase>
    <div class="row">
	  <div class="col-32-right">Item Label:</div>
  	  <div class="col-66-left">
        <input type="text" name="itemLabel" size="40" />
        <input type="checkbox" name="boldItemLabel" value="1" /> Bold
      </div>
    </div>
    <div class="row">
      <div class="col-32-right"><b>Item Name:</b></div>
  	  <div class="col-66-left">
        <input type="checkbox" name="itemNameSameAsLabel" value="1" /> Same as item label; OR<br />
        <input type="text" name="itemName" size="40" /><br />
        <small>(For internal reference only.)</small>
      </div>
    </div>
    <div class="row">
      <div class="col-32-right"><input type="checkbox" name="mandatory" value="1" /></div>
      <div class="col-66-left">This is a required field.</div>
    </div>
  </cfdefaultcase>
</cfswitch>

<cfswitch expression="#URL.fieldType#">
  <cfcase value="Short Text">
    <div class="row">
      <div class="col-32-right">Predefined special field:</div>
      <div class="col-66-left">
        <select name="fieldContext">
          <option value="">None</option>
          <option value="Email">E-mail Address</option>
          <option value="Full Name">Full Name</option>
          <option value="Date">Date (with date picker)</option>
        </select><br />
        <small>(A form can have at most one "E-mail" and one "Full Name" predefined field.)</small>
      </div>
    </div>
    <div class="row">
      <div class="col-32-right">&nbsp;</div>
      <div class="col-66-left"><a href="##" onClick="xsite.toggle('optionalSettings')" class="accent01">Optional &dArr;</a></div>
    </div>
    <div id="optionalSettings" style="display:none;">
      <div class="row">
        <div class="col-32-right">Field Size:</div>
        <div class="col-66-left">
          <select name="fieldSize">
            <option value="Small">Small</option>
            <option value="Medium" selected>Medium</option>
            <option value="Large">Large</option>
          </select>
        </div>
      </div>
      <div class="row">
	    <div class="col-32-right">Apply Validation:</div>
  	    <div class="col-66-left">
          <select name="dataType">
            <option value="Text">No</option>
            <option value="Email">E-mail Address</option>
            <option value="Date">Date</option>
            <option value="Number">Number</option>
            <option value="Phone">Telephone Number</option>
            <option value="SSN">Social Security Number</option>
          </select>
        </div>
      </div>
      <div class="row">
	    <div class="col-32-right">User Instructions:</div>
  	    <div class="col-66-left">
          <input type="text" name="itemNotes" size="40" /><br />
          <small>(Will appear below this field.)</small>
        </div>
      </div>
      <div class="row">
        <div class="col-32-right">Field Default Value:</div>
        <div class="col-66-left"><input type="text" name="shortTextDefault" size="40" /></div>
      </div>
    </div>
  </cfcase>
  <cfcase value="Long Text">
    <div class="row">
      <div class="col-32-right">&nbsp;</div>
      <div class="col-66-left"><a href="##" onClick="xsite.toggle('optionalSettings')" class="accent01">Optional &dArr;</a></div>
    </div>
    <div id="optionalSettings" style="display:none;">
      <div class="row">
        <div class="col-32-right">Field Size:</div>
        <div class="col-66-left">
          <select name="fieldSize">
            <option value="Small">Small</option>
            <option value="Medium" selected>Medium</option>
            <option value="Large">Large</option>
          </select>
        </div>
      </div>
      <div class="row">
	    <div class="col-32-right">User Instructions:</div>
  	    <div class="col-66-left">
          <input type="text" name="itemNotes" size="40" /><br />
          <small>(Will appear below this field.)</small>
        </div>
      </div>
      <div class="row">
        <div class="col-32-right">Field Default Value:</div>
        <div class="col-66-left"><textarea name="longTextDefault" cols="38" rows="5"></textarea></div>
      </div>
    </div>
  </cfcase>
  <cfcase value="Radio Group">
    <div class="row">
      <div class="col-32-right"><b>Options:</b></div>
      <div class="col-66-left">
        <input type="button" value="Import From Predefined Option List" onclick="xsite.openWindow('index.cfm?md=form&tmp=import_options&formName=frmAddFormItem&wrap=1&fieldName=optionList','importOptions',500,350)" /><br /> 
        <textarea name="optionList" cols="38" rows="5"></textarea> <small>(Enter an option per line)</small><br />
        Default(Pre-select) Value:<br />
        <input type="text" name="shortTextDefault" size="40" />
      </div>
    </div>
    
    <div class="row">
	  <div class="col-32-right">User Instructions:</div>
  	  <div class="col-66-left">
        <input type="text" name="itemNotes" size="40" /><br />
        <small>(Will appear below this field.)</small>
      </div>
    </div>
  </cfcase>
  <cfcase value="Check Boxes">
    <div class="row">
      <div class="col-32-right"><b>Options:</b></div>
      <div class="col-66-left">
        <input type="button" value="Import Predefined Options" onclick="xsite.openWindow('index.cfm?md=form&tmp=import_options&formName=frmAddFormItem&wrap=1&fieldName=optionList','importOptions',500,350)" /><br />
        <textarea name="optionList" cols="38" rows="5"></textarea> <small>(Enter an option per line)</small><br />
        Default(Pre-select) Values:<br />
        <textarea name="longTextDefault" cols="40" rows="3"></textarea> <small>(Enter an option per line)</small>
      </div>
    </div>
    <div class="row">
      <div class="col-32-right">Minimum number of items to be selected (if required):</div>
      <div class="col-66-left"><input type="text" name="numRequiredOptions" size="2" value="1" /></div>
    </div>   
    
    <div class="row">
	  <div class="col-32-right">User Instructions:</div>
  	  <div class="col-66-left">
        <input type="text" name="itemNotes" size="40" /><br />
        <small>(Will appear below this field.)</small>
      </div>
    </div>
  </cfcase>
  <cfcase value="Drop-Down Menu">
    <div class="row">
      <div class="col-32-right"><b>Options:</b></div>
      <div class="col-66-left">
        <input type="button" value="Import From Predefined Option List" onclick="xsite.openWindow('index.cfm?md=form&tmp=import_options&formName=frmAddFormItem&wrap=1&fieldName=optionList','importOptions',500,350)" /><br />        
        <textarea name="optionList" cols="38" rows="5"></textarea> <small>(Enter an option per line)</small><br />
        Default(Pre-select) Value:<br />
        <input type="text" name="shortTextDefault" size="40" />
      </div>
    </div>
    <div class="row">
	  <div class="col-32-right">User Instructions:</div>
  	  <div class="col-66-left">
        <input type="text" name="itemNotes" size="40" /><br />
        <small>(Will appear below this field.)</small>
      </div>
    </div> 
  </cfcase>
</cfswitch>
</form>
</cfoutput>