<cfparam name="URL.formID" type="numeric">
<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset formInfo=FM.getFormProperties(URL.formID)>

<cfoutput query="formInfo">
<form id="frmCopyForm" name="frmCopyForm" onsubmit="return false">
<input type="hidden" name="formID" value="#formID#" />
<div class="row">
  <div class="col-32-right"><b>Original Form Name: </b></div>
  <div class="col-66-left">#formName#</div>
</div>
<div class="row">
  <div class="col-32-right"><b>New Form Name: </b></div>
  <div class="col-66-left"><input type="text" name="formName" size="40" validate="required:true" value="" /></div>
</div>
<div class="row">
  <div class="col-32-right"><input type="checkbox" name="published" value="1" /></div>
  <div class="col-66-left">Publish this form.</div>
</div>
</form>
</cfoutput>