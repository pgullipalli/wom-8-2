<cfparam name="URL.groupID">

<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset formInfo=FM.getFormProperties(FM.getFormIDByGroupID(URL.groupID))>
<cfset itemLabelPlacement=formInfo.itemLabelPlacement>
<cfset formItems=FM.getFormItemsByGroupID(URL.groupID)>

<cfoutput>
<div class="subActionBar">
<a href="##" onclick="addFormItem()">New Item</a><a href="##" onclick="editFormItem()">Edit Item</a><a href="##" onclick="deleteFormItem()">Delete</a>
</div>
</cfoutput>

<cfoutput>
<form id="frmFormItemList#URL.groupID#" name="frmFormItemList#URL.groupID#" onsubmit="return false;">
</cfoutput>
<div class="formItemList">
<table>
<cfoutput query="formItems">
<tr>
  <td width="30" align="center" valign="middle"><input type="radio" name="formItemID" value="#formItemID#" /></td>
  <td width="60" align="center" valign="middle">
    <a href="##" onclick="moveFormItem('#formItemID#','up')"><img src="images/icon_up.gif" border="0" align="absbottom" alt="move up"></a>
    <a href="##" onclick="moveFormItem('#formItemID#','down')"><img src="images/icon_down.gif" border="0" align="absbottom" alt="move down"></a>
  </td>
  <td valign="top"<cfif Not active> style="text-decoration:line-through"</cfif>>
  <div class="row">
  <cfif fieldType Is "Text Only">
    <div class="col-95-left">#instructionText#</div>
  <cfelse>
    <cfif itemLabelPlacement Is "Top">
      <div class="col-95-left">
    	<cfif Compare(itemLabel,"")><cfif boldItemLabel><b>#itemLabel#</b><cfelse>#itemLabel#</cfif><br /></cfif>
    <cfelseif itemLabelPlacement Is "Left">
      <div class="col-24-left"><cfif Compare(itemLabel,"")><cfif boldItemLabel><b>#itemLabel#</b><cfelse>#itemLabel#</cfif><cfelse>&nbsp;</cfif></div>
      <div class="col-74-left">
    <cfelse>
      <div class="col-24-right"><cfif Compare(itemLabel,"")><cfif boldItemLabel><b>#itemLabel#</b><cfelse>#itemLabel#</cfif><cfelse>&nbsp;</cfif></div>
      <div class="col-74-left">
    </cfif>
  
    <cfswitch expression="#fieldType#">
  	  <cfcase value="Short Text">
        <cfif fieldContext Is "Email">
          <input type="text" name="email" class="#fieldSize#" value="#HTMLEditFormat(shortTextDefault)#" />
          <span style="color:##cccccc">(Predefined Special Field: E-Mail Address)</span>
        <cfelseif fieldContext Is "Full Name">
          <div class="col-name"><input type="text" name="firstName" class="Small" value="" /><br /><small>(First Name)</small></div>
          <div class="col-74-left">
            <input type="text" name="lastName" class="Small" value="" />
            <span style="color:##cccccc">(Predefined Special Field: Full Name)</span><br /><small>(Last Name)</small>
          </div>
        <cfelseif fieldContext Is "Date">
          <input type="text" name="f#formItemID#" class="Small" value="#HTMLEditFormat(shortTextDefault)#" />
          <span style="color:##cccccc">(fly-out date-picker upon click)</span>
          <!--- <img src="images/icon_date_picker.png" align="absmiddle" /> --->
        <cfelse>
          <input type="text" name="f#formItemID#" class="#fieldSize#" value="#HTMLEditFormat(shortTextDefault)#" />
        </cfif>
        <cfif Compare(itemNotes, "")><br /><small>(#itemNotes#)</small></cfif>
      </cfcase>
      <cfcase value="Long Text">
    	<textarea name="f#formItemID#" class="#fieldSize#">#HTMLEditFormat(longTextDefault)#</textarea>
        <cfif Compare(itemNotes, "")><br /><small>(#itemNotes#)</small></cfif>
      </cfcase>
      <cfcase value="Radio Group">
    	<cfloop index="option" list="#optionList#" delimiters="#Chr(13)##Chr(10)#">
          <input type="radio" name="f#formItemID#" value="" <cfif Not Compare(shortTextDefault, option)>checked</cfif> /> #option#<br />
        </cfloop>
        <cfif Compare(itemNotes, "")><small>(#itemNotes#)</small></cfif>
      </cfcase>
      <cfcase value="Check Boxes">
    	<cfloop index="option" list="#optionList#" delimiters="#Chr(13)##Chr(10)#">
          <input type="checkbox" name="f#formItemID#" value="" <cfif ListFind(longTextDefault, option, "#Chr(13)##Chr(10)#") GT 0>checked</cfif> /> #option#<br />
        </cfloop>
        <cfif Compare(itemNotes, "")><small>(#itemNotes#)</small></cfif>
      </cfcase>
      <cfcase value="Drop-Down Menu">
    	<select name="f#formItemID#">
          <option value=""></option>
          <cfloop index="option" list="#optionList#" delimiters="#Chr(13)##Chr(10)#">
          <option <cfif Not Compare(shortTextDefault, option)>selected</cfif>>#option#</option>
          </cfloop>
        </select>
        <cfif Compare(itemNotes, "")><br /><small>(#itemNotes#)</small></cfif>
      </cfcase>
      <cfdefaultcase></cfdefaultcase>
    </cfswitch>
    </div>
  </cfif>
  </div>
  </td>
</tr>
</cfoutput>
</table><br /><br />
</div>
</form>