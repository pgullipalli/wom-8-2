<cfparam name="URL.formID">
<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset groupIDs=FM.getGroupIDsByFormID(URL.formID)>

<!--- overwrite JQuery UI default styles --->
<cfset stylesheet='<link rel="stylesheet" type="text/css" href="scripts/css/form-manager-edit-form-content.css" media="screen" />'>
<cfhtmlhead text="#stylesheet#">

<div id="groupList">
  <ul><cfset idx = 0>
  <cfoutput query="groupIDs"><cfset idx = idx + 1>
	<li><a href="##page-#groupID#">Form Page #idx#</a></li>
  </cfoutput>
  </ul>
  <cfset idx = 0>
  <cfoutput query="groupIDs"><cfset idx = idx + 1>
  <!--- _snip_formitem_list.cfm?groupID will be loaded into this div --->
  <div id="page-#groupID#"><br /><br /><p align="center"><img src="images/ajax-loader-indicator.gif" border="0"></p></div>
  </cfoutput>
</div>