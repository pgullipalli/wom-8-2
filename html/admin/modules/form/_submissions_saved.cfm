<script type="text/javascript" src="modules/form/submissions_saved.js"></script>

<cfparam name="URL.formID" default="0">

<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset allForms=FM.getAllFormNames()>

<cfif URL.formID IS 0>
  <cfset URL.formID=FM.getFirstFormID()>
  <cfif URL.formID IS 0><!--- no Forms were ever created --->
    <cflocation url="index.cfm?md=form&tmp=forms&wrap=1">
  </cfif>
</cfif>

<div class="header">FORM MANAGER &gt; "In Progress" Submissions</div>

<div style="clear:both;"><br /></div>

<cfoutput>
<form id="frmNavigator">
  <b>Form</b>
  <select name="formID" style="min-width:300px;">
    <cfloop query="allForms">
    <option value="#allForms.formID#"<cfif URL.formID IS allForms.formID> selected</cfif>>#formName#</option>
    </cfloop>
  </select><br />
</form>
</cfoutput>

<p>
Click the "Date Submitted" link to view all the individual data submitted through that form.
</p>

<div id="mainDiv"></div>