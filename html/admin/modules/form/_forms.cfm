<script type="text/javascript" src="modules/form/forms.js"></script>

<div class="header">FORM MANAGER &gt; Forms</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addForm()">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="copyForm()">Copy</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteForm()">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button130" onclick="editForm()">Edit Form Settings</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button130" onclick="editFormContent()">Edit Form Content</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="previewForm()">Preview</a>
</div>

<div style="clear:both;"></div>

<div id="mainDiv"></div>