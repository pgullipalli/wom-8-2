<form id="frmAddForm" name="frmAddForm" onsubmit="return false">
<div class="row">
  <div class="col-24-right"><b>Form Name: </b></div>
  <div class="col-74-left"><input type="text" name="formName" value="" size="40" validate="required:true" /></div>
</div>
<div class="row">
  <div class="col-24-right"><input type="checkbox" name="published" value="1" /></div>
  <div class="col-74-left">Publish this form.</div>
</div>
<div class="row">
  <div class="col-24-right">Confirmation message to display after form is submitted:</div>
  <div class="col-74-left">
  		<CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmAddForm"
            fieldName="submissionConfirmationMessage"
            cols="60"
            rows="4"
            HTMLContent="Thank you for your submission!"
            displayHTMLSource="true"
            editorHeader="Edit Confirmation Message">
            <small>(This copy will automatically display to site users immediately after following the successful submission.)</small>
 	</div>
</div>
<div class="row">
  <div class="col-24-right">&nbsp;</div>
  <div class="col-74-left"><a href="#" onClick="xsite.toggle('optionalSettings')" class="accent01">Optional &dArr;</a></div>
</div>
<div id="optionalSettings" style="display:none;">
  <div class="row">
	<div class="col-24-right"><input type="checkbox" name="sendNotificationEmail" value="1" onClick="xsite.toggle('notificationSettings', this)" /></div>
	<div class="col-74-left">
        Notify me upon form submission.
        <div id="notificationSettings" style="display:none;">
            <div class="row">
              <div class="col-24-right">Recipient E-mail Address:</div>
              <div class="col-74-left"><input type="text" name="notificationEmailTo" size="40" maxlength="200" validate="email:true" /><br /><small>(Separate e-mail addresses by commas.)</small></div>
            </div>
            <div class="row">
              <div class="col-24-right">Set Reply To:</div>
              <div class="col-74-left">
                <input type="text" name="notificationEmailFrom" size="40" maxlength="100" validate="email:true" /><br />
                <input type="checkbox" name="useUserEmailAsSender" value="1" />
                Use site visitor's e-mail address as sender.<br />
                <small>(An e-mail form field needs to be created for this to work.)</small>
              </div>
            </div>
            <div class="row">
              <div class="col-24-right">E-mail Subject Line:</div>
              <div class="col-74-left"><input type="text" name="notificationEmailSubject" size="40" maxlength="150" value="Online form submission notification" /></div>
            </div>
        </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-24-right"><input type="checkbox" name="sendReceiptToUser" value="1" onClick="xsite.toggle('receiptSettings', this)" /></div>
    <div class="col-74-left">
        Enable users to receive a confirmation email. <small>(An e-mail form field needs to be created for this to work.)</small>
        <div id="receiptSettings" style="display:none;">
      		<div class="row">
              <div class="col-24-right">Set Reply To:</div>
              <div class="col-74-left"><input type="text" name="receiptEmailFrom" size="40" maxlength="100" validate="email:true" /></div>
            </div>
            <div class="row">
              <div class="col-24-right">E-mail Subject Line:</div>
              <div class="col-74-left"><input type="text" name="receiptEmailSubject" size="40" maxlength="200" value="" /></div>
            </div>
            <div class="row">
              <div class="col-24-right">Message to be included in the confirmation email:</div>
              <div class="col-74-left">
                <textarea name="headerMessageInReceipt" cols="40" rows="3"></textarea><br />
                <input type="checkbox" name="includeSubmissionInReciept" value="1" />
                Include submitted information.
              </div>
            </div>            
        </div>
    </div>
  </div>
  
  <div class="row">
	<div class="col-24-right"><input type="checkbox" name="enableDataSavingFeature" value="1" onClick="xsite.toggle('dataSavingSettings', this)" /></div>
	<div class="col-74-left">
        Enable users to save/retrieve incomplete form data.
        <div id="dataSavingSettings" style="display:none;">
            <div class="row">
              <div class="col-24-right">Set Reply To:</div>
              <div class="col-74-left">
                <input type="text" name="dataRetrievalSenderEmailAddress" size="40" maxlength="100" validate="email:true" />
              </div>
            </div>
        </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-24-right">Form Field Label Placement:</div>
    <div class="col-74-left">
      <select name="itemLabelPlacement">
        <option value="Right">Left side of the field and right-aligned</option>
        <option value="Left">Left side of the field and left-aligned</option>
        <option value="Top">Label above the field</option>
      </select>
    </div>
  </div>
  
  <div class="row">
  	<div class="col-24-right">Label on submit button:</div>
    <div class="col-74-left"><input type="text" name="submitButtonLabel" value="Submit" /></div>  
  </div>
</div>
</form>
