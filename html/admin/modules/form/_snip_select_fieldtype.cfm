<cfparam name="URL.formItemID" default="0">
<style type="text/css">
.fieldTypeButton {
  border:1px solid #555555;
  background-color:#eeeeee;
  padding:10px 10px;
  margin:0px 0px;
  text-align:left;
}
</style>
<cfset fieldType="Text Only">
<cfif Compare(URL.formItemID, "0")>
  <cfset fieldType=""><!--- TBD --->
</cfif>
<form id="frmSelectFieldType" name="frmSelectFieldType" onsubmit="return false">
<p>Please select a "Field Type" you wish to use.</p>
<table border="0" cellpadding="5" cellspacing="0" width="95%">
  <tr valign="middle">
    <td width="10%" align="center"><input type="radio" name="fieldType" value="Text Only"<cfif fieldType Is "Text Only"> checked</cfif>></td>
    <td>
    <div class="fieldTypeButton">
      <b>Text Only</b> - It doesn't accept data. Best used for instructions or detailed questions.
    </div>
    </td>
  </tr>
  <tr valign="middle">
    <td align="center"><input type="radio" name="fieldType" value="Short Text"<cfif fieldType Is "Short Text"> checked</cfif>></td>
    <td>
    <div class="fieldTypeButton">
      <b>Short Text</b> - This field accepts a single line text that is limited to 255 characters.
    </div>
    </td>
  </tr>
  <tr valign="middle">
    <td align="center"><input type="radio" name="fieldType" value="Long Text"<cfif fieldType Is "Long Text"> checked</cfif>></td>
    <td>
    <div class="fieldTypeButton">
      <b>Long Text</b> - This is a paragraph field that spans across multiple lines. Best used for essay type questions.
    </div>
    </td>
  </tr>
  <tr valign="middle">
    <td align="center"><input type="radio" name="fieldType" value="Radio Group"<cfif fieldType Is "Radio Group"> checked</cfif>></td>
    <td>
    <div class="fieldTypeButton">
      <b>Radio Group</b> - It's a multiple choice field. It presents a list of options that only one of the options can be selected.
    </div>
    </td>
  </tr>
  <tr valign="middle">
    <td align="center"><input type="radio" name="fieldType" value="Check Boxes"<cfif fieldType Is "Check Boxes"> checked</cfif>></td>
    <td>
    <div class="fieldTypeButton">
      <b>Check Boxes</b> - Similar to the Radio Group, this field also presents a list of options, but none or multiple of the options can be selected.
    </div>
    </td>
  </tr>
  <tr valign="middle">
    <td align="center"><input type="radio" name="fieldType" value="Drop-Down Menu"<cfif fieldType Is "Drop-Down Menu"> checked</cfif>></td>
    <td>
    <div class="fieldTypeButton">
      <b>Drop-Down Menu</b> - This field presents a list of options in a drop-down menu.<!---  Best used when there are many options to choose from. --->
    </div>
    </td>
  </tr>
</table>
</form>


