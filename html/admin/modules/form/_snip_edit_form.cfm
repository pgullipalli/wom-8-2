<cfparam name="URL.formID" type="numeric">
<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset formInfo=FM.getFormProperties(URL.formID)>

<cfoutput query="formInfo">
<form id="frmEditForm" name="frmEditForm" onsubmit="return false">
<input type="hidden" name="formID" value="#formID#" />
<div class="row">
  <div class="col-24-right"><b>Form Name: </b></div>
  <div class="col-74-left"><input type="text" name="formName" size="40" validate="required:true" value="#HTMLEditFormat(formName)#" /></div>
</div>
<div class="row">
  <div class="col-24-right"><input type="checkbox" name="published" value="1" <cfif published>checked</cfif> /></div>
  <div class="col-74-left">Publish this form.</div>
</div>
<div class="row">
  <div class="col-24-right">Confirmation message to display after form is submitted:</div>
  <div class="col-74-left">
  		<CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="false"
            formName="frmEditForm"
            fieldName="submissionConfirmationMessage"
            cols="60"
            rows="4"
            HTMLContent="#submissionConfirmationMessage#"
            displayHTMLSource="true"
            editorHeader="Edit Confirmation Message">
            <small>(This copy will automatically display to site users immediately after following the successful submission.)</small>
 	</div>
</div>
<div class="row">
  <div class="col-24-right">&nbsp;</div>
  <div class="col-74-left"><a href="##" onClick="xsite.toggle('optionalSettings2')" class="accent01">Optional &dArr;</a></div>
</div>
<div id="optionalSettings2" style="display:none;">
  <div class="row">
	<div class="col-24-right"><input type="checkbox" name="sendNotificationEmail" value="1" onClick="xsite.toggle('notificationSettings2', this)" <cfif sendNotificationEmail>checked</cfif> /></div>
	<div class="col-74-left">
        Notify me upon form submission.
        <div id="notificationSettings2" style="display:<cfif sendNotificationEmail>block<cfelse>none</cfif>;">
            <div class="row">
              <div class="col-24-right">Recipient E-mail Address:</div>
              <div class="col-74-left"><input type="text" name="notificationEmailTo" size="40" maxlength="200" value="#HTMLEditFormat(notificationEmailTo)#" /><br /><small>(Separate e-mail addresses by commas.)</small></div>
            </div>
            <div class="row">
              <div class="col-24-right">Set Reply To:</div>
              <div class="col-74-left">
                <input type="text" name="notificationEmailFrom" size="40" maxlength="100" validate="email:true" value="#HTMLEditFormat(notificationEmailFrom)#" /><br />
                <input type="checkbox" name="useUserEmailAsSender" value="1" <cfif useUserEmailAsSender>checked</cfif> />
                Use site visitor's e-mail address as sender.<br />
                <small>(An e-mail form field needs to be created for this to work.)</small>
              </div>
            </div>
            <div class="row">
              <div class="col-24-right">E-mail Subject Line:</div>
              <div class="col-74-left"><input type="text" name="notificationEmailSubject" size="40" maxlength="150" value="#HTMLEditFormat(notificationEmailSubject)#" /></div>
            </div>
        </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-24-right"><input type="checkbox" name="sendReceiptToUser" value="1" onClick="xsite.toggle('receiptSettings2', this)" <cfif sendReceiptToUser>checked</cfif> /></div>
    <div class="col-74-left">
        Enable users to receive a confirmation email. <small>(An e-mail form field needs to be created for this to work.)</small>
        <div id="receiptSettings2" style="display:<cfif sendReceiptToUser>block<cfelse>none</cfif>;">
      		<div class="row">
              <div class="col-24-right">Set Reply To:</div>
              <div class="col-74-left"><input type="text" name="receiptEmailFrom" size="40" maxlength="100" validate="email:true" value="#HTMLEditFormat(receiptEmailFrom)#" /></div>
            </div>
            <div class="row">
              <div class="col-24-right">E-mail Subject Line:</div>
              <div class="col-74-left"><input type="text" name="receiptEmailSubject" size="40" maxlength="200" value="#HTMLEditFormat(receiptEmailSubject)#" /></div>
            </div>
            <div class="row">
              <div class="col-24-right">Message to be included in the confirmation email:</div>
              <div class="col-74-left">
                <textarea name="headerMessageInReceipt" cols="40" rows="3">#HTMLEditFormat(headerMessageInReceipt)#</textarea><br />
                <input type="checkbox" name="includeSubmissionInReciept" value="1" <cfif includeSubmissionInReciept>checked</cfif> />
                Include submitted information.
              </div>
            </div>            
        </div>
    </div>
  </div>
  
  <div class="row">
	<div class="col-24-right"><input type="checkbox" name="enableDataSavingFeature" value="1" onClick="xsite.toggle('dataSavingSettings2', this)" <cfif enableDataSavingFeature>checked</cfif> /></div>
	<div class="col-74-left">
        Enable users to save/retrieve incomplete form data.
        <div id="dataSavingSettings2" style="display:<cfif enableDataSavingFeature>block<cfelse>none</cfif>;">
            <div class="row">
              <div class="col-24-right">Set Reply To:</div>
              <div class="col-74-left">
                <input type="text" name="dataRetrievalSenderEmailAddress" size="40" maxlength="100" validate="email:true" value="#HTMLEditFormat(dataRetrievalSenderEmailAddress)#" />
              </div>
            </div>
        </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-24-right">Form Field Label Placement:</div>
    <div class="col-74-left">
      <select name="itemLabelPlacement">
        <option value="Right"<cfif itemLabelPlacement Is "Right"> selected</cfif>>Left side of the field and right-aligned</option>
        <option value="Left"<cfif itemLabelPlacement Is "Left"> selected</cfif>>Left side of the field and left-aligned</option>
        <option value="Top"<cfif itemLabelPlacement Is "Top"> selected</cfif>>Label above the field</option>
      </select>
    </div>
  </div>
  
  <div class="row">
  	<div class="col-24-right">Label on submit button:</div>
    <div class="col-74-left"><input type="text" name="submitButtonLabel" value="#HTMLEditFormat(submitButtonLabel)#" /></div>  
  </div>
</div>
</form>
</cfoutput>