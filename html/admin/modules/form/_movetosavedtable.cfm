<cfparam name="URL.formID">
<cfparam name="URL.submissionID">

<cfset FMM=CreateObject("component", "#APPLICATION.componentPath#.modules.form.FormAdmin")>
<cfset formInfo = FMM.getFormInfoByFormID(URL.formID)>

<cfset emailListID=0>
<cfset isEmailList=0>
<cfif formInfo.isSignUpForm>
  <cfset listInfo=CreateObject("component", "#APPLICATION.componentPath#.modules.emaillist.EmailListAdmin").getListsByFormID(URL.formID)>
  <cfif listInfo.recordcount GT 0>
    <cfset emailListID=listInfo.emailListID[1]>
    <cfset isEmailList=1>
  </cfif>
</cfif>

<h2>FORM MANAGER &gt;&gt; Move Submission to "In Progress" Database</h2> 
<script src="scripts/cfform.js" type="text/javascript" ></script>
<script type="text/javascript">
  function submitform(form) {
    if (!_CF_checkemailaddress(form.emailForSavedData.value, true)) {
	  alert("Please enter a valid email address.");
	  form.emailForSavedData.focus();
	  return false;
	}
	
	if (form.passwordForSavedData.value == "") {
	  alert("Please enter a retrieve code.");
	  form.passwordForSavedData.focus();
	  return false;
	}
    return true;
  }
</script>

<p>
Enter an e-mail address and a retrieve code that will be used to retrieve the saved data. An e-mail along with the information for retrieving the saved data will be sending to this e-mail address.
</p>

<form name="form1" action="action.cfm?md=form&task=moveSubmissionToSavedTable" method="post" onSubmit="return submitform(this)">
<cfoutput query="formInfo">
<input type="hidden" name="submissionID" value="#URL.submissionID#">
<input type="hidden" name="formID" value="#URL.formID#">
<input type="hidden" name="isEmailList" value="#isEmailList#">
<input type="hidden" name="emailListID" value="#emailListID#">
</cfoutput>

<table border="0" width="100%" cellpadding="3" cellspacing="0">
  <tr valign="top">
    <td width="40%" align="right">E-Mail Address:</td>
    <td width="60%"><input type="text" name="emailForSavedData" size="20" maxlength="100"></td>          
  </tr>
  <tr valign="top">
    <td align="right">Retrieve Code</td>
    <td>
    <input type="text" name="passwordForSavedData" size="20" maxlength="12"><br />
    (Any combination of numbers and letters between 2 and 12 characters.)
    </td>
  </tr>
  <tr valign="top">
    <td>&nbsp;</td>
    <td><input type="submit" value=" move " class="button"></td>
  </tr>
</table>
</form>