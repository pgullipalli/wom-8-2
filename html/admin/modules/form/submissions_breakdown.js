$(function () {
	loadSubmissionBreakdown();
	
	$("#frmNavigator select[name='formID']").change(function() {
	  document.frmDateRange.reset();
	  loadSubmissionBreakdown();	  
	});
});

function loadSubmissionBreakdown() {
  var formID=$("#frmNavigator select[name='formID']").val();	
  var dateRangeForm=document.frmDateRange;
  var fromDate=dateRangeForm.fromDate.value;
  var toDate=dateRangeForm.toDate.value;
  
  if (!xsite.checkDate(fromDate) || !xsite.checkDate(toDate)) {
    alert('Please enter date range in correct date format, for example, 3/24/2009');
	return;
  }
  
  var formItemIDList="";
  if (document.frmFormItemList) {
	formItemIDList=xsite.getCheckedValue(document.frmFormItemList.formItemID);
	if (!formItemIDList) formItemIDList="";
  }
  
  var url="index.cfm?md=form&tmp=snip_submissions_breakdown_detail&formID=" + formID + "&fromDate=" + escape(fromDate) + "&toDate=" + escape(toDate) +
  		  "&formItemIDList=" + formItemIDList + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url, function() {
	$("#fromDate").datepicker();  
	$("#toDate").datepicker();  
  });
}