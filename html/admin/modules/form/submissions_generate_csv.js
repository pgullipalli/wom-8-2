$(function () {
	loadFormItemList();
	
	$("#frmNavigator select[name='formID']").change(function() {
	  document.frmDateRange.reset();
	  loadFormItemList();	  
	});
});

function loadFormItemList() {
  var formID=$("#frmNavigator select[name='formID']").val();	
  var dateRangeForm=document.frmDateRange;
  var fromDate=dateRangeForm.fromDate.value;
  var toDate=dateRangeForm.toDate.value;
  
  if (!xsite.checkDate(fromDate) || !xsite.checkDate(toDate)) {
    alert('Please enter date range in correct date format, for example, 3/24/2009');
	return;
  }
  
  var url="index.cfm?md=form&tmp=snip_submissions_generate_csv_select&formID=" + formID + "&fromDate=" + escape(fromDate) + "&toDate=" + escape(toDate) +
  		  "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url, function() {
	$("#fromDate").datepicker();  
	$("#toDate").datepicker();  
  });
}

function generateCSV() {
  var formID=$("#frmNavigator select[name='formID']").val();
  var dateRangeForm=document.frmDateRange;
  var fromDate=dateRangeForm.fromDate.value;
  var toDate=dateRangeForm.toDate.value;
  var formItemIDList="";
  formItemIDList=xsite.getCheckedValue(document.frmFormItemList.formItemID);
  if (!formItemIDList) {
	alert('Please select at least one form item.');
	return;
  } else {
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=form&task=generateCSV&formID=' + formID +
							 "&fromDate=" + escape(fromDate) + "&toDate=" + escape(toDate) +
							 "&formItemIDList=" + escape(formItemIDList) + "&uuid=" + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  var fileName = jsonData.FILENAME;
	  window.open('index.cfm?md=form&tmp=snip_submissions_download_csv&fileName=' + escape(fileName) + '&uuid=' + xsite.getUUID(),'_self');	
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}


