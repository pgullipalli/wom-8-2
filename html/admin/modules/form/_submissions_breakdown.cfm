<script type="text/javascript" src="modules/form/submissions_breakdown.js"></script>

<cfparam name="URL.formID" default="0">
<cfparam name="URL.fromDate" default="#DateFormat(DateAdd("m", -1, now()),"mm/dd/yyyy")#">
<cfparam name="URL.toDate" default="#DateFormat(now(), "mm/dd/yyyy")#">

<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset allForms=FM.getAllFormNames()>

<cfif URL.formID IS 0>
  <cfset URL.formID=FM.getFirstFormID()>
  <cfif URL.formID IS 0><!--- no Forms were ever created --->
    <cflocation url="index.cfm?md=form&tmp=forms&wrap=1">
  </cfif>
</cfif>

<div class="header">FORM MANAGER &gt; Submissions Breakdown</div>

<div style="clear:both;"><br /></div>

<cfoutput>
<form id="frmNavigator">
  <b>Form</b>
  <select name="formID" style="min-width:300px;">
    <cfloop query="allForms">
    <option value="#allForms.formID#"<cfif URL.formID IS allForms.formID> selected</cfif>>#formName#</option>
    </cfloop>
  </select><br />
</form>

<br />

<form name="frmDateRange" id="frmDateRange" onSubmit="return false;">
From <input type="text" name="fromDate" id="fromDate" size="12" value="#URL.fromDate#"> &nbsp;
To <input type="text" name="toDate" id="toDate" size="12" value="#URL.toDate#">

<button onClick="loadSubmissionBreakdown();">Change</button>
</form>
</cfoutput>

<p>
The table below shows all items for the given form. Check all items you wish to compile for reporting (or click "Select All"), specify the date range, then click "View Report." </p>

<div id="mainDiv"></div>