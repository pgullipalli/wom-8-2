<cfparam name="URL.formID">

<script type="text/javascript" src="modules/form/formitems.js"></script>

<style type="text/css">
/*--- Form Layout ---*/
/*extracted from master.css */
#mainDiv .intro { font-size:12px; line-height:150%; border-bottom: none; margin:0px; padding:10px 16px 10px 16px; float:left; width:415px; }
#mainDiv .section { font-size:14px; line-height:100%; color:#6f7876; background:#e8eceb; padding:6px; margin:0px 0px; float:left; width:456px; }
#mainDiv .title {font-size:10px; color:#EC6CB8; font-weight:bold; line-height:110%; text-transform:uppercase; text-align:left; padding:0px 0px 5px 0px; float:left; width:100%;}
#mainDiv .info { font-size:12px; line-height:110%; float:left; width:100%; padding:0px 0px;}
</style>

<div class="header">FORM MANAGER &gt; Form Content</div>

<cfoutput><span id="formID" formID="#URL.formID#" /></cfoutput><!--- hold formID here --->
<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addPage()">New Page</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button90" onclick="deletePage()">Delete Page</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="movePage('left')">Move Page Left</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="movePage('right')">Move Page Right</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="previewForm()">Preview</a>
</div>

<div style="clear:both; height:20px;"></div>
<div id="mainDiv"></div>

<p>
* 'line-through' text indicates 'inactive' form fields. They will not appear on the front-end web pages.
</p>