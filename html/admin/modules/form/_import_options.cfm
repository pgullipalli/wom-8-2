<script type="text/javascript">
$(function() {
  var formName = xsite.getURLParameter('formName');
  var fieldName = xsite.getURLParameter('fieldName');
  var states = ['Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Florida',
  				'Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine',
				'Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire',
				'New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island',
				'South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming'];
  var parishes = ['Acadia Parish','Allen Parish','Ascension Parish','Assumption Parish','Avoyelles Parish','Beauregard Parish','Bienville Parish',
  				  'Bossier Parish','Caddo Parish','Calcasieu Parish','Caldwell Parish','Cameron Parish','Catahoula Parish','Claiborne Parish',
  				  'Concordia Parish','De Soto Parish','East Baton Rouge Parish','East Carroll Parish','East Feliciana Parish','Evangeline Parishb',
				  'Franklin Parish','Grant Parish','Iberia Parish','Iberville Parish','Jackson Parish','Jefferson Parish','Jefferson Davis Parish',
				  'Lafayette Parish','Lafourche','La Salle Parish','Lincoln Parish','Livingston Parish','Madison Parish','Morehouse Parish','Natchitoches Parish',
				  'Orleans Parish','Ouachita Parish','Plaquemines Parish','Pointe Coupee Parish','Rapides Parish','Red River Parish','Richland Parish',
				  'Sabine Parish','Saint Bernard Parish','Saint Charles Parish','Saint Helena Parish','Saint James Parish','Saint John the Baptist Parish',
				  'Saint Landry Parish','Saint Martin Parish','Saint Mary Parish','Saint Tammany Parish','Tangipahoa Parish','Tensas Parish','Terrebonne Parish',
				  'Union Parish','Vermilion Parish','Vernon Parish','Washington Parish','Webster Parish','West Baton Rouge Parish','West Carroll Parish',
				  'West Feliciana Parish','Winn Parish'];
  var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
  var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
  var satisfaction = ['Very Satisfied','Satisfied','Neutral','Unsatisfied','Very Unsatisfied'];
  var agreement = ['Strongly Agree','Agree','Neutral','Disagree','Strongly Disagree'];
  var importance = ['Very Important','Important','Neutral','Somewhat Important','Not at all Important'];

  $(".btnOption").click(function(e) {
    var IDClicked = e.target.id;
	
	switch (IDClicked) {
	  case 'States':
	    importOptions(states);
	    break;
	  case 'Parishes':
	    importOptions(parishes);
	    break;
	  case 'Months':
	    importOptions(months);
	    break;
	  case 'Days':
	    importOptions(days);
	    break;
	  case 'Satisfaction':
	    importOptions(satisfaction);
	    break;
	  case 'Agreement':
	    importOptions(agreement);
	    break;
	  case 'Importance':
	    importOptions(importance);
	    break;
	  default: break;	
	}	
  });
  
  function importOptions(arr){
    var openerFieldObj = opener.document[formName][fieldName];
	if (!openerFieldObj) window.close();
	
	var separator = '\n';
	var existingContent = openerFieldObj.value;
	if (existingContent == "") openerFieldObj.value = arr.join(separator);
	else openerFieldObj.value = existingContent + separator + arr.join(separator);
	window.close();	
  }
});
</script>
<style type="text/css">
.btnOption { width:250px;height:20px; margin:3px 0px; padding:3px 5px;border:1px solid #dddddd;background-color:#eeeeee;text-align:center;cursor:pointer; }
</style>

<p align="right"><a href="#" onClick="window.close();">Close</a></p>

<p><b>Select a predefined option list:</b></p>

<table border="0" align="center" cellpadding="0" cellspacing="0" width="250"><tr valign="top"><td>
<div class="btnOption" id="States">States of U.S.</div>
<div class="btnOption" id="Parishes">Louisiana Parishes</div>
<div class="btnOption" id="Months">Months of the Year</div>
<div class="btnOption" id="Days">Days of the Week</div>
<div class="btnOption" id="Satisfaction">Satisfaction</div>
<div class="btnOption" id="Agreement">Agreement</div>
<div class="btnOption" id="Importance">Importance</div>
</td></tr></table>