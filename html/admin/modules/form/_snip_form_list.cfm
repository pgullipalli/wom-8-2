<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset allForms=FM.getFormsSummaryInfo()>

<cfif allForms.recordcount IS 0>
  <p>
  It appears that there are no forms created.
  Please click on the 'New' button in the action bar above to create the first form.
  </p>
<cfelse>
  <p>
  The table below shows the forms created using Form Manager. To view submission data, click on the number in the '# of submissions' column.
  To view complied report, click on   the number in the '# of form items' column to select form items you wish to compile.
  </p>
<form id="frmFormList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="200">Form Name</th>
    <th width="100">Published?</th>
    <th>URL</th>
    <th width="80"># Subs.</th>
	<th width="80"># Items</th>
    <th width="100">Generate CSV</th>
    <th width="100">In Progress</th>
  </tr>
  <cfoutput query="allForms">
  <tr align="center">
    <td><input type="radio" name="formID" value="#formID#"></td>
    <td align="left"><a href="index.cfm?md=form&tmp=formitems&formID=#formID#&wrap=1">#formName#</a></td>
    <td><cfif published>Yes<cfelse>No</cfif></td>
    <td align="left"><a href="/index.cfm?md=form&tmp=home&fmid=#formID#" target="_blank">/index.cfm?md=form&amp;fmid=#formID#</a></td>
    <td><cfif numSubmissions GT 0><a href="index.cfm?md=form&tmp=submissions&formID=#formID#&wrap=1" class="accent01">#numSubmissions#</a><cfelse>0</cfif></td>
    <td><cfif numFormItems GT 0><a href="index.cfm?md=form&tmp=submissions_breakdown&formID=#formID#&wrap=1" class="accent01">#numFormItems#</a><cfelse>0</cfif></td>
    <td><a href="index.cfm?md=form&tmp=submissions_generate_csv&formID=#formID#&wrap=1" class="accent01">&gt;&gt;</a></td>
    <td><cfif numSubmissionsSaved GT 0><a href="index.cfm?md=form&tmp=submissions_saved&formID=#formID#&wrap=1" class="accent01">#numSubmissionsSaved#</a><cfelse>0</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>