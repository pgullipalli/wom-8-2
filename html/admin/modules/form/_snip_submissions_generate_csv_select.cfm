<cfparam name="URL.formID">
<cfparam name="URL.fromDate">
<cfparam name="URL.toDate">

<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset numSubmissions=FM.getNumSubmissions(URL.formID, URL.fromDate, URL.toDate)>
<cfset formItems=FM.getFormItemsInfoByFormID(URL.formID)>

<cfoutput>
<p><b>Total Number of Submissions:</b> #numSubmissions#</p>
</cfoutput>

<form id="frmFormItemList" name="frmFormItemList" onsubmit="return false;">

<div class="itemList">
<table style="width:465px;">
  <tr>
    <th width="105">&nbsp;</th>
	<th width="250">Item Name</th>
	<th>Item Type</th>
  </tr>
  <cfoutput query="formItems">
  <cfif Compare(fieldType, "Text Only")>
  <tr valign="top">
    <td align="center"><input type="checkbox" name="formItemID" value="#formItemID#"></td>
	<td><b>#itemName#</b></td>
	<td>#fieldType#</td>
  </tr>
  </cfif>
  </cfoutput>
  <tr>
    <td align="center">
      <a href="javascript:xsite.checkAllBoxes(document.frmFormItemList.formItemID)">Check All</a>/<a
       href="javascript:xsite.uncheckAllBoxes(document.frmFormItemList.formItemID)">None</a></td>
    <td align="left" colspan="2"><button onclick="generateCSV();">Generate CSV</button></td>
  </tr>
</table>
</div>
</form>


