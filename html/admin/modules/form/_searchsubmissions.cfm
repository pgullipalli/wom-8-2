<cfparam name="URL.formID" default="0">
<cfparam name="URL.keyword" default="">
<cfparam name="URL.formItemIDList" default="">
<cfparam name="URL.startIndex" default="1">
<cfparam name="URL.numItemsPerPage" default="25">
<cfparam name="URL.searchAction" default="0">

<h1>FORM MANAGER &gt;&gt; Search Submissions</h1>
<cfset FMM=CreateObject("component", "#APPLICATION.componentPath#.modules.form.FormAdmin")>
<cfset allForms=FMM.getAllFormNames()>
<cfif URL.formID IS NOT 0>
  <cfset formItems=FMM.getFormItemsInfoByFormID(URL.formID)>
  <cfset formInfo=FMM.getFormInfoByFormID(URL.formID)>
  <cfset hasSearchable=0>
  <cfloop query="formItems">
    <cfif NOT instructionOnly><cfset hasSearchable=1><cfbreak></cfif>
  </cfloop>
<cfelse>
  <cfset URL.searchAction=0>
</cfif>

<script type="text/javascript">
  function checkallfields(form) {	
    if (form.formItemIDList) {
      if (form.formItemIDList.length) {
    	for (var i = 0; i < form.formItemIDList.length; i++) form.formItemIDList[i].checked=true;		
	  } else {
		form.formItemIDList.checked = true;
	  }
    }
  }
  
  function submitform(form) {
    if (form.keyword.value.length < 3) {
      alert("Please enter a keyword with at least three characters.");
      form.keyword.focus();
	  return false;
	}
	
	if (form.formItemIDList) {
      if (form.formItemIDList.length) {
    	for (var i = 0; i < form.formItemIDList.length; i++) 
		  if (form.formItemIDList[i].checked) return true
	  } else {
		if (form.formItemIDList.checked) return true;
	  }
    }
	alert ("Please check at least one field to search against.");
	return false;
  }
  
  function checkall(form, flag) {
    if (form.submissionIDList) {
      if (form.submissionIDList.length) {
	    if (flag == 1) {
		  for (var i = 0; i < form.submissionIDList.length; i++) form.submissionIDList[i].checked=true;
		} else {
		  for (var i = 0; i < form.submissionIDList.length; i++) form.submissionIDList[i].checked=false;
		}
	  } else {
	    if (flag == 1) {
		  form.submissionIDList.checked = true;
		} else {
		  form.submissionIDList.checked = false;
		}
	  }
    }
  }
  
  function deletesubmissions(form) {
    if (form.submissionIDList) {
	  if (form.submissionIDList.length) {
	    var i = 0;
		var found = false;
		for (i=0; i<form.submissionIDList.length; i++) {
		  if (form.submissionIDList[i].checked) {
		    found = true;
			break;
		  }
		}
		if (!found) {
		  alert ("To delete submissions, please check at least one submission to delete.");
		  return false;
		}
	  } else {
	    if (!form.submissionIDList.checked) {
		  alert ("To delete submissions, please check at least one submission to delete.");
		  return false;
		}
	  }	
	} else {
	  return false;
	}
	
	if (!confirm("Are you sure you want to delete the selected submissions?")) return false;
	
	showwaiting();
    return true;
  }
</script>

<form name="form1" action="index.cfm" method="get">
<input type="hidden" name="md" value="form">
<input type="hidden" name="tmp" value="searchsubmissions">
<cfoutput>
<input type="hidden" name="keyword" value="#HTMLEditFormat(URL.keyword)#">
</cfoutput>
<select name="formID">
  <option value="0">- select a form to search for -</option>
  <cfoutput query="allForms">
  <option value="#allForms.formID#"<cfif URL.formID IS allForms.formID> selected</cfif>>#allForms.formName#</option>
  </cfoutput>
</select>
<input type="submit" class="button" value="change">
</form>

<cfif URL.formID IS NOT 0 AND hasSearchable>
<cfoutput>
<form name="form2" action="index.cfm" method="get" onSubmit="return submitform(this)">
<input type="hidden" name="md" value="form">
<input type="hidden" name="tmp" value="searchsubmissions">
<input type="hidden" name="formID" value="#URL.formID#">
<input type="hidden" name="searchAction" value="1">
<table border="0" width="700" cellpadding="3" cellspacing="0">
  <tr valign="top">
    <td width="100" align="right"><b>Keyword</b></td>
    <td width="600">	
	  <input type="text" name="keyword" size="20" value="#HTMLEditFormat(URL.keyword)#">
	  <a href="javascript:checkallfields(document.form2)">check all fields</a><br>
	  <cfloop query="formItems">
		<cfif NOT instructionOnly>
		<input type="checkbox" name="formItemIDList" value="#formItemID#"<cfif ListFind(URL.formItemIDList,formItemID) IS NOT 0> checked</cfif>> #itemName#<br>
	    </cfif>
	  </cfloop>
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
	<td><input type="submit" value=" submit " class="button"></td>
  </tr>
</table>
</form>
</cfoutput>
</cfif>

<hr size="1" noshade>

<cfif URL.searchAction>
  <cfoutput>
  <h2>#formInfo.formName#</h2>
  </cfoutput>

  <!--- Display search results --->
  <cfset structResult=FMM.searchSubmissions(argumentCollection=URL)>

  <cfif structResult.numTotalSubmissions IS 0>
  <p class="hilite">Your search returns no results.</p>
  <cfelse>

  <p>
  <cfoutput>Display #URL.startIndex# - #structResult.numDisplayedSubmissions + URL.startIndex - 1# of #structResult.numTotalSubmissions# Submissions</cfoutput>
  </p>
    
  <cfset CreateObject("component", "#APPLICATION.componentPath#.Utility").displayResultPageLinks("index.cfm?md=form&tmp=searchsubmissions&searchAction=1&formID=#URL.formID#&formItemIDList=#URLEncodedFormat(URL.formItemIDList)#&keyword=#URLEncodedFormat(URL.keyword)#", "#structResult.numTotalSubmissions#", "#URL.startIndex#", "#URL.numItemsPerPage#")>  
  
  <cfoutput>
  <form name="form3" action="action.cfm?md=form&task=deleteSubmissions" method="post" onSubmit="return deletesubmissions(this)">
  <input type="hidden" name="md" value="form">
  <input type="hidden" name="tmp" value="searchsubmissions">
  <input type="hidden" name="formID" value="#URL.formID#">
  <input type="hidden" name="formItemIDList" value="#URL.formItemIDList#">
  <input type="hidden" name="startIndex" value="#URL.startIndex#">
  <input type="hidden" name="numItemsPerPage" value="#URL.numItemsPerPage#">
  <input type="hidden" name="searchAction" value="1">
  <input type="hidden" name="keyword" value="#HTMLEditFormat(URL.keyword)#">
  <table border="0" width="100%" cellpadding="3" cellspacing="1" class="tbGridLineColor">
    <tr>
	  <th width="10%">
	    <a href="javascript:checkall(document.form3, 1)"><b>All</b></a> &nbsp;
	    <a href="javascript:checkall(document.form3, 0)"><b>None</b></a>
	  </th>
	  <th>Date Submitted</th>
	  <cfif formInfo.isSignUpForm>
	  <th>E-Mail</th>
	  <th>First Name</th>
	  <th>Last Name</th>
	  </cfif>
	  <cfloop index="idx" from="1" to="#structResult.numColumns#">
  	  <th>#structResult.columnNames[idx]#</th>
	  </cfloop>
	</tr>
	<cfset class="altrow">
    <cfloop index="idx1" from="1" to="#structResult.numDisplayedSubmissions#">
    <cfif class IS "altrow"><cfset class=""><cfelse><cfset class="class=""altrow"""></cfif>	
	<tr #class# valign="top">
	  <td align="center"><input type="checkbox" name="submissionIDList" value="#structResult.submissions[idx1].submissionID#"></td>
      <td align="center"><a href="javascript:openwindow('index.cfm?md=form&tmp=submissions_singleview&popup=1&formID=#URL.formID#&submissionID=#structResult.submissions[idx1].submissionID#','_blank', 500, 500)">#DateFormat(structResult.submissions[idx1].dateSubmitted,"mm/dd/yyyy")# #TimeFormat(structResult.submissions[idx1].dateSubmitted, "h:mm tt")#</a></td>
	  <cfif formInfo.isSignUpForm>
      <td>#structResult.submissions[idx1].userEmail#&nbsp;</td>
	  <td>#structResult.submissions[idx1].userFirstName#&nbsp;</td>
	  <td>#structResult.submissions[idx1].userLastName#&nbsp;</td>	
	  </cfif>
	  <cfloop index="idx2" from="1" to="#structResult.numColumns#">
      <td>
	  #structResult.submissions[idx1]["#structResult.formItemIDs[idx2]#"]#
	  &nbsp;
      </td>
	  </cfloop>	  
	</tr>	
	</cfloop>	
  </table>
  <p align="center">
  <input type="submit" class="button"  value=" delete selected submissions ">
  </p>
  </form>
  </cfoutput> 

  
  <cfset CreateObject("component", "#APPLICATION.componentPath#.Utility").displayResultPageLinks("index.cfm?md=form&tmp=searchsubmissions&searchAction=1&formID=#URL.formID#&formItemIDList=#URLEncodedFormat(URL.formItemIDList)#&keyword=#URLEncodedFormat(URL.keyword)#", "#structResult.numTotalSubmissions#", "#URL.startIndex#", "#URL.numItemsPerPage#")>  
  
  </form>
  </cfif>  

<p>
<a href="javascript:history.go(-1)"><b>Back Previous Page</b></a> | <a href="index.cfm?md=form&tmp=forms"><b>Back To All Forms</b></a><br>
</p>

</cfif>


