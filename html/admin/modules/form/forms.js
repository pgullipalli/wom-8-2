$(function () {
	loadFormList();
});

function loadFormList() {
  xsite.load("#mainDiv", "index.cfm?md=form&tmp=snip_form_list&uuid=" + xsite.getUUID());
}

/*--------------------------------*/
/*    BEGIN: Add Form             */
/*--------------------------------*/
function addForm() {
  xsite.createModalDialog({
	windowName: 'winAddForm',
	title: 'Add Form',
	position: 'center-up',
	width: 650,
	url: 'index.cfm?md=form&tmp=snip_add_form&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddForm").dialog('close'); },
	  ' Add ': submitForm_AddForm
	}
  }).dialog('open');
  
  function submitForm_AddForm() {
	var form = $("#frmAddForm");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=form&task=addForm',
		  type: 'post',
		  cache: false,
		  dataType: 'json',
		  beforeSubmit: function() { return validate($("#frmAddForm")[0]); },
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddForm").dialog('close');
		loadFormList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------*/
/*    END: Add Form               */
/*--------------------------------*/

/*--------------------------------*/
/*    BEGIN: Edit Form            */
/*--------------------------------*/
function editForm() {
  var form = $("#frmFormList")[0];
  var formID = xsite.getCheckedValue(form.formID);
  if (!formID) {
	alert("Please select a form to edit.");
	return;
  }
  
  xsite.createModalDialog({
	windowName: 'winEditForm',
	title: 'Edit Form',
	position: 'center-up',
	width: 650,
	url: 'index.cfm?md=form&tmp=snip_edit_form&formID=' + formID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditForm").dialog('close'); },
	  ' Save ': submitForm_EditForm
	}
  }).dialog('open');
  
  function submitForm_EditForm() {
	var form = $("#frmEditForm");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=form&task=editForm',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  beforeSubmit: function() { return validate($("#frmEditForm")[0]); },
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditForm").dialog('close');
		loadFormList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------*/
/*    END: Edit Form              */
/*--------------------------------*/

function validate(form) {//shared by addForm and editForm
  if (form.sendNotificationEmail.checked) {
	if (form.notificationEmailTo.value == "") {
	  alert("Please enter a valid email address for the notification email to send to.");
	  form.notificationEmailTo.focus();
	  return false;
	}
	
	if (form.notificationEmailFrom.value == "" && !form.useUserEmailAsSender.checked) {
	  alert("Please either enter a valid email address or check off the \"Use site visitor's e-mail address as sender\" box for the \"from\" address of the notification email.");
	  return false;
	}
	if (form.notificationEmailSubject.value == "") {
	  alert("Please enter an email subject line for the notification email.");
	  form.notificationEmailSubject.focus();
	  return false;
	}
  }
  
  if (form.sendReceiptToUser.checked) {
	if (form.receiptEmailFrom.value == "") {
	  alert("Please enter a valid email address for the 'from address' of the confirmation email.");
	  form.receiptEmailFrom.focus();
	  return false;
	}
	if (form.receiptEmailSubject.value == "") {
	  alert("Please enter an email subject line for the confirmation email.");
	  form.receiptEmailSubject.focus();

	  return false;
	}
  }
  
  if (form.submitButtonLabel.value == "") {
	alert("Please enter the label used for the 'submit' button.");
	form.submitButtonLabel.focus();
	return false;
  }
  
  return true;
}

/*--------------------------------*/
/*  BEGIN: Copy Form              */
/*--------------------------------*/
function copyForm() {
  var form = $("#frmFormList")[0];
  var formID = xsite.getCheckedValue(form.formID);
  if (!formID) {
	alert("Please select a form to copy from.");
	return;
  }
  
  xsite.createModalDialog({
	windowName: 'winCopyForm',
	title: 'Copy Form',
	position: 'center-up',
	width: 500,
	url: 'index.cfm?md=form&tmp=snip_copy_form&formID=' + formID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winCopyForm").dialog('close'); },
	  ' Copy ': submitForm_CopyForm
	}
  }).dialog('open');
  
  function submitForm_CopyForm() {
	var form = $("#frmCopyForm");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=form&task=copyForm',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winCopyForm").dialog('close');
		loadFormList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------*/
/*  END: Copy Form                */
/*--------------------------------*/

/*--------------------------------*/
/*  BEGIN: Delete Form            */
/*--------------------------------*/
function deleteForm() {
  var form = $("#frmFormList")[0];
  var formID = xsite.getCheckedValue(form.formID);
  if (!formID) {
	alert("Please select a form to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected form? Please be aware that when a form is deleted, the submision data associated with the form will also be deleted.')) {
	return;
  } else {
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=form&task=deleteForm&formID=' + formID, {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadFormList();
	} else {

	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function editFormContent() {
  var form = $("#frmFormList")[0];
  var formID = xsite.getCheckedValue(form.formID);
  if (!formID) {
	alert("Please select a form to edit its content.");
	return;
  }
  
  window.location='index.cfm?md=form&tmp=formitems&formID=' + formID + '&wrap=1&&uuid=' + xsite.getUUID();  
}

function previewForm() {
  var form = $("#frmFormList")[0];
  var formID = xsite.getCheckedValue(form.formID);
  if (!formID) {
	alert("Please select a form to preview.");
	return;
  }
  
  window.open('/index.cfm?md=form&tmp=home&fmid=' + formID, '_blank');
}