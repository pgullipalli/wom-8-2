<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
FM=CreateObject("component", "com.FormAdmin").init();
returnedStruct=StructNew();
returnedStruct.SUCCESS=true;
returnedStruct.MESSAGE="";

switch (URL.task) {
  case "addForm":
	FM.addForm(argumentCollection=Form);
	break;

  case "copyForm":
    FM.copyForm(argumentCollection=Form);
	break;
	
  case "editForm":
	FM.editForm(argumentCollection=Form);
	break;
	
  case  "deleteForm":
    FM.deleteForm(URL.formID);
	break;
	
  case "addPage":
    returnedStruct.GROUPID=FM.addPage(URL.formID);
	break;
	
  case "deletePage":
    FM.deletePage(URL.groupID);
	break;
	
  case "movePage":
    FM.movePage(URL.groupID, URL.direction);
	break;
	
  case "addFormItem":
    returnedStruct=FM.addFormItem(argumentCollection=Form);
	break;
	
  case "editFormItem":
    returnedStruct=FM.editFormItem(argumentCollection=Form);
	break;
	
  case "moveFormItem":
    FM.moveFormItem(formItemID="#URL.formItemID#", direction="#URL.direction#");
	break;
	
  case "deleteFormItem":
    FM.deleteFormItem(URL.formItemID);
	break;
	
  case "deleteSubmissions":
    FM.deleteSubmissions(URL.submissionIDList);
	break;
	
  case "generateCSV":
    CSVDirectory="#Application.siteDirectoryRoot#/admin/modules/form/temp";
	fileName=FM.generateCSV(formID="#URL.formID#", fromDate="#URL.fromDate#", toDate="#URL.toDate#", formItemIDList="#URL.formItemIDList#", CSVDirectory="#CSVDirectory#");
    returnedStruct.FILENAME="#fileName#";
	break;
	
  case "moveSubmissionToSavedTable":
    saveSubmissionID=FM.moveSubmissionToSavedTable(argumentCollection=Form);
	// need to sendConfirmationEmailForRetrievingData
    FM=CreateObject("component", "com.Form").init();
    encodedSubmissionID=Encrypt("#saveSubmissionID#", Application.encryptionKey);
    formURL="#Application.siteURLRoot#/index.cfm?md=form&fmid=#Form.formID#";
    dataRetrievalURL="#Application.siteURLRoot#/index.cfm?md=form&fmid=#Form.formID#&encSid=#URLEncodedFormat(encodedSubmissionID)#";
	FM.sendConfirmationEmailForRetrievingData(emailForSavedData=Form.emailForSavedData, passwordForSavedData=Form.passwordForSavedData, formID=Form.formID, formURL="#formURL#", dataRetrievalURL="#dataRetrievalURL#");
    break;
    
  default: break;
}
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>