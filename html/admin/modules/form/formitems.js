var formID;
var arrayGroupID = new Array();

$(function () {
	formID = $("#formID").attr('formID');
	loadGroupList();
});

//load page snip_group_list.cfm into the 'mainDiv'
function loadGroupList(defaultSelectedTabNum) {
  var selectedTabNum = defaultSelectedTabNum || 0;
  xsite.load("#mainDiv", "index.cfm?md=form&tmp=snip_group_list&formID=" + formID + "&uuid=" + xsite.getUUID(), function() {
	arrayGroupID = new Array();//clear array
	$('#groupList').unbind();//This is to prevent events from binding multiple times
	$tabs = $('#groupList').tabs({//convert group list to tabs
	  add: function(event, ui) {//automatically select the newly added tab
		$tabs.tabs('select', '#' + ui.panel.id);
	  },
	  selected: selectedTabNum  
	});
		
	$("#groupList div").each(function(i) {//load each tab's content via ajax
	  var divID = this.id;
	  var groupID = divID.replace('page-', '');
	  arrayGroupID[i] = groupID;
	  loadPage(i);
	});
	
	/*$("#mainDiv").unbind();
	$("#mainDiv").click(function(e) {
	  if($(e.target).attr("type") == "checkbox") xsite.resetCheckBoxes(e.target); //enable checkboxes to be mutual-exclusive
	});*/
  });
}	

//load page snip_formitem_list.cfm (form items grouped by groupID) into each page and define all button events
function loadPage(tabIndex) {
  if (tabIndex < 0) return;
  if (tabIndex >= arrayGroupID.length) return;
  var groupID = arrayGroupID[tabIndex];
  $("#page-" + groupID).load('index.cfm?md=form&tmp=snip_formitem_list&groupID=' + groupID + '&uuid=' + xsite.getUUID(), {}, function() {
	//callback function after load; define buttons click event
	
  });
}

function addPage() {
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=form&task=addPage&formID=' + formID, {}, processJson, 'json');}});
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  var groupID=jsonData.GROUPID;
	  var pageNum = $tabs.tabs('length') + 1;
	  arrayGroupID[pageNum-1] = groupID;
	  $('#groupList').tabs( 'add' , '#page-' +  groupID, 'Form Page ' + pageNum);
	  loadPage(pageNum-1);
	  xsite.closeWaitingDialog();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function deletePage() {
  var selectedIndex = $('#groupList').tabs('option', 'selected');
  var pageNum = selectedIndex + 1; 
  var groupID = arrayGroupID[selectedIndex];
  if (!confirm('Are you sure you want to delete Page ' + pageNum + '? Please be aware that all submission data associated with the form items in this page will also be deleted.')) return;
  
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=form&task=deletePage&groupID=' + groupID, {}, processJson, 'json');}});

  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadGroupList();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function movePage(direction) {
  var selectedIndex = $('#groupList').tabs('option', 'selected');
  var groupID = arrayGroupID[selectedIndex];
  var nextSelectedIndex = 0;//selected tab after the move
  var numTabs=$('#groupList').tabs('length');
  if (direction == 'right') {
	nextSelectedIndex=selectedIndex+1;
	if (nextSelectedIndex >= numTabs) nextSelectedIndex=selectedIndex;
  } else {
	nextSelectedIndex=selectedIndex-1;
	if (nextSelectedIndex < 0) nextSelectedIndex=selectedIndex;
  }
  
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=form&task=movePage&direction=' + direction + '&groupID=' + groupID, {}, processJson, 'json');}});

  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadGroupList(nextSelectedIndex);
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function addFormItem() {
  var fieldType; //make this avaiable within all child functions
  //select a field type first
  xsite.createModalDialog({
	windowName: 'winSelectFieldType',
	title: 'Select a Field Type',
	position: 'center-up',
	width: 500,
	url: 'index.cfm?md=form&tmp=snip_select_fieldtype&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winSelectFieldType").dialog('close'); },
	  ' Select ': selectFieldType
	}
  }).dialog('open');

  function selectFieldType() {
	var form = $("#frmSelectFieldType")[0];
	fieldType = xsite.getCheckedValue(form.fieldType);
	if (!fieldType) {
	  alert("Please select a field type.");
	  return;
	} else {
	  $("#winSelectFieldType").dialog('close');
	  addFormItem();
	}
  }
  
  function addFormItem () {
	var selectedIndex = $('#groupList').tabs('option', 'selected');
    var groupID = arrayGroupID[selectedIndex];
	xsite.createModalDialog({
		windowName: 'winAddFormItem',
		title: 'Add a Form Item',
		position: 'center-up',
		width: 650,
		url: 'index.cfm?md=form&tmp=snip_add_formitem&formID=' + formID + '&groupID=' + groupID + '&fieldType=' + escape(fieldType) + '&uuid=' + xsite.getUUID(),
		buttons: {
		 'Cancel': function() { $("#winAddFormItem").dialog('close'); },
		 ' Add ': submitForm_AddFormItem
		}
	}).dialog('open');
			
	function submitForm_AddFormItem() {
	  $('#frmAddFormItem').ajaxSubmit({ 
		url: 'action.cfm?md=form&task=addFormItem',
		type: 'post',
		cache: false,
		dataType: 'json', 
		beforeSubmit: validate,
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });	  
	}
	
	function validate (formData, jqForm, options) {
	  var form = jqForm[0];
	  if (fieldType != "Text Only") {
		if (!form.itemNameSameAsLabel.checked && form.itemName.value == "") {
		  alert("Please enter an item name.");
		  form.itemName.focus();
		  return false;
		}
	  }
	  if (fieldType == "Radio Group" || fieldType == "Check Boxes" || fieldType == "Drop-Down Menu") {
		if (form.optionList.value == "") {
		  alert("Please enter options for the " + fieldType + ".");
		  form.optionList.focus();
		  return false;
		}
	  }
	  if (fieldType == "Check Boxes") {
		if (!xsite.checkInteger(form.numRequiredOptions.value)) { 
		  alert("Please enter the minimum number of items to be selected in a whole number.");
		  form.numRequiredOptions.focus();
		  return false;
		}
	  }
	  return true;;
	}
	
	function processJson(jsonData) {
	  var selectedIndex = $('#groupList').tabs('option', 'selected');
	  if (jsonData.SUCCESS) {
		$("#winAddFormItem").dialog('close');
		loadPage(selectedIndex);			
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editFormItem() {
  var selectedIndex = $('#groupList').tabs('option', 'selected');
  var groupID = arrayGroupID[selectedIndex];
  
  var form = $("#frmFormItemList" + groupID)[0];
  var formItemID = xsite.getCheckedValue(form.formItemID);
  if (!formItemID) {
	alert("Please select a form item to edit.");
	return;
  }
  
  xsite.createModalDialog({
	windowName: 'winEditFormItem',
	title: 'Edit Form Item',
	position: 'center-up',
	width: 650,
	url: 'index.cfm?md=form&tmp=snip_edit_formitem&formID=' + formID + '&groupID=' + groupID + '&formItemID=' + formItemID + '&uuid=' + xsite.getUUID(),
	buttons: {
	 'Cancel': function() { $("#winEditFormItem").dialog('close'); },
	 ' Save ': submitForm_EditFormItem
	}
  }).dialog('open');
  
  function submitForm_EditFormItem() {
    $('#frmEditFormItem').ajaxSubmit({ 
	  url: 'action.cfm?md=form&task=editFormItem',
	  type: 'post',
	  cache: false,
	  dataType: 'json', 
	  beforeSubmit: validate,
	  success: processJson,
	  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});	 
  }
  
  function validate (formData, jqForm, options) {
    var form = jqForm[0];
	var fieldType = form.fieldType.value;
	
    if (fieldType != "Text Only") {
	  if (!form.itemNameSameAsLabel.checked && form.itemName.value == "") {
	    alert("Please enter an item name.");
	    form.itemName.focus();
	    return false;
	  }
    }
    if (fieldType == "Radio Group" || fieldType == "Check Boxes" || fieldType == "Drop-Down Menu") {
	  if (form.optionList.value == "") {
	    alert("Please enter options for the " + fieldType + ".");
	    form.optionList.focus();
	    return false;
	  }
    }
    if (fieldType == "Check Boxes") {
	  if (!xsite.checkInteger(form.numRequiredOptions.value)) { 
	    alert("Please enter the minimum number of items to be selected in a whole number.");
	    form.numRequiredOptions.focus();
	    return false;
	  }
    }
    return true;;
  }
  
  function processJson(jsonData) {
	var selectedIndex = $('#groupList').tabs('option', 'selected');
	if (jsonData.SUCCESS) {
	  $("#winEditFormItem").dialog('close');
	  loadPage(selectedIndex);			
	} else {
	  alert (jsonData.MESSAGE);
	}
  }
}

function deleteFormItem() {
  var selectedIndex = $('#groupList').tabs('option', 'selected');
  var groupID = arrayGroupID[selectedIndex];
  
  var form = $("#frmFormItemList" + groupID)[0];
  var formItemID = xsite.getCheckedValue(form.formItemID);
  if (!formItemID) {
	alert("Please select a form item to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected form item? Please be aware that when a form item is deleted, the submision data associated with the form item will also be deleted. You can also set the form item to \'inactive\' instead of deleting it.')) {
	return;
  } else {
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=form&task=deleteFormItem&formItemID=' + formItemID, {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadPage(selectedIndex);
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function moveFormItem(formItemID, direction) {
  var selectedIndex = $('#groupList').tabs('option', 'selected');
  var groupID = arrayGroupID[selectedIndex];
  
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=form&task=moveFormItem&formItemID=' + formItemID + '&direction=' + direction + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  loadPage(selectedIndex);
	  loadPage(selectedIndex-1);
	  loadPage(selectedIndex+1);
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function previewForm() {  
  window.open('/index.cfm?md=form&tmp=home&fmid=' + formID, '_blank');
}