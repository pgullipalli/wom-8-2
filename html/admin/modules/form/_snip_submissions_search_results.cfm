<cfparam name="URL.formID" default="0">
<cfparam name="URL.fromDate">
<cfparam name="URL.toDate">
<cfparam name="URL.keyword">
<cfparam name="URL.formItemIDList" default="">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">
<cfparam name="URL.useDateRange" default="1">
<cfparam name="URL.numColumns" default="5">

<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset formItems=FM.getFormItemsInfoByFormID(URL.formID)>

<cfif Compare(URL.keyword, "")>
  <cfset submissionStruct=FM.searchSubmissions(argumentCollection=URL)>
</cfif>

<form name="frmFormItemList" id="frmFormItemList" onSubmit="return false;">
<div class="itemList">
<table style="width:465px;">
  <tr>
    <th width="105">&nbsp;</th>
	<th width="250">Item Name</th>
	<th>Item Type</th>
  </tr>
  <cfoutput query="formItems">
  <cfif Compare(fieldType, "Text Only")>
  <tr valign="top">
    <td align="center"><input type="checkbox" name="formItemID" value="#formItemID#"<cfif ListFind(URL.formItemIDList, formItemID) GT 0> checked</cfif>></td>
	<td><b>#itemName#</b></td>
	<td>#fieldType#</td>
  </tr>
  </cfif>
  </cfoutput>
  <tr>
    <td><a href="javascript:xsite.checkAllBoxes(document.frmFormItemList.formItemID)">Check All</a>/<a
       href="javascript:xsite.uncheckAllBoxes(document.frmFormItemList.formItemID)">None</a></td>
    <td align="left" colspan="2"><button onclick="searchSubmissions();">Search</button></td>
  </tr>
</table>
</div>
</form>

<br />


<cfif Compare(URL.keyword, "")>
  <p class="subHeader">Search Results</p>

  <p>
  <b>Number of Results:</b> <cfoutput>#submissionStruct.numAllSubmissions#</cfoutput>
  </p>

  <cfif submissionStruct.numDisplayedSubmissions GT 0>
    <form id="frmSubmissionList">
    <div class="itemList">
    <cfoutput>
    <table>
      <tr>
        <th width="40">&nbsp;</th>
        <th width="150">Date Submitted</th> 
        <cfloop index="idx" from="1" to="#submissionStruct.numColumns#">
        <th>#submissionStruct.columnNames[idx]#</th>
        </cfloop>
      </tr>
      <cfloop index="idx" from="1" to="#submissionStruct.numDisplayedSubmissions#">
      <cfset dateSubmitted=submissionStruct.submissions[idx].dateSubmitted>
      <cfset submissionID=submissionStruct.submissions[idx].submissionID>
      <tr>
        <td align="center"><input type="checkbox" name="submissionID" value="#submissionID#"></td>
        <td align="center"><a href="##" onClick="viewSubmission(#URL.formID#, #submissionID#);" class="accent01">#DateFormat(dateSubmitted,"mm/dd/yyyy")# #TimeFormat(dateSubmitted, "h:mm tt")#</a></td>
        <cfloop index="idx1" from="1" to="#submissionStruct.numColumns#">
        <td>#submissionStruct.submissions[idx]["#submissionStruct.formItemIDs[idx1]#"]#</td>
        </cfloop>
      </tr>
      </cfloop>
    </table>
    </cfoutput>
    </div>
    </form>
    
    <cfif submissionStruct.numAllSubmissions GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(submissionStruct.numAllSubmissions/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="searchSubmissions(#idx#)">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
    </cfif>

  </cfif>
</cfif>

