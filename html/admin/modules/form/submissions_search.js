var pageNum=1;

$(function () {
	searchSubmissions();
	
	$("#frmNavigator select[name='formID']").change(function() {
	  if (document.frmSubmissionSearch) document.frmSubmissionSearch.keyword="";

	  searchSubmissions();	  
	});
});

function searchSubmissions(pNum) {
  if (pNum) pageNum = pNum;
  else pageNum=1;
  
  var formID=$("#frmNavigator select[name='formID']").val();	
  var dateRangeForm=document.frmDateRange;
  var fromDate=dateRangeForm.fromDate.value;
  var toDate=dateRangeForm.toDate.value;
  var keyword=dateRangeForm.keyword.value;
  
  var formItemIDList="";
  if (document.frmFormItemList) {
	if (document.frmFormItemList.formItemID) formItemIDList=xsite.getCheckedValue(document.frmFormItemList.formItemID);
  }
  
  var url="index.cfm?md=form&tmp=snip_submissions_search_results&formID=" + formID + "&keyword=" + escape(keyword) +
  		  "&formItemIDList=" + escape(formItemIDList) + 
		  "&fromDate=" + escape(fromDate) + "&toDate=" + escape(toDate) +
		  "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url, function() {
	$("#fromDate").datepicker();  
	$("#toDate").datepicker();  
  });
}

function viewSubmission(formID, submissionID) {
  xsite.createModalDialog({
	windowName: 'winVewSubmission',
	title: 'View Submission',
	width: 600,
	height: 500,
	position: 'center',
	url: 'index.cfm?md=form&tmp=snip_view_submission&formID=' + formID + '&submissionID=' + submissionID + '&uuid=' + xsite.getUUID()
  }).dialog('open');
}

function deleteSubmissions() {
  if (!document.getElementById('frmSubmissionList')) return;
  var form = $("#frmSubmissionList")[0];
  
  var submissionIDList = xsite.getCheckedValue(form.submissionID);
  if (!submissionIDList) {
	alert("Please check off the submissions you'd like to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected submissions?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=form&task=deleteSubmissions&submissionIDList=' + escape(submissionIDList), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  searchSubmissions(pageNum);
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function moveToSavedTable() {
  if (!document.getElementById('frmSubmissionList')) return;
  var formID=$("#frmNavigator select[name='formID']").val();
  var form = $("#frmSubmissionList")[0];
  var submissionID = xsite.getCheckedValue(form.submissionID);
  if (!submissionID) {
	alert("Please select a user submission to move to 'in progress.'");
	return;
  }
  if (submissionID.indexOf(",") != -1) {
	alert("Please select only one user submission to move to 'in progress.'");
	return;
  }

  xsite.createModalDialog({
	windowName: 'winMoveToInProgress',
	title: 'Move To "In Progress"',
	width: 500,
	height: 300,
	position: 'center',
	url: 'index.cfm?md=form&tmp=snip_submission_move_to_saved_table&formID=' + formID + '&submissionID=' + submissionID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winMoveToInProgress").dialog('close'); },
	  ' Move ': move
	}
  }).dialog('open');
  
  function move() {
	var form = $("#frmMoveToSavedTable");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=form&task=moveSubmissionToSavedTable',
		  type: 'post',
		  cache: false,
		  dataType: 'json',
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winMoveToInProgress").dialog('close');
		searchSubmissions(pageNum);
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}  
  }
}
