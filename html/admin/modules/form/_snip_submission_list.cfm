<cfparam name="URL.formID">
<cfparam name="URL.fromDate">
<cfparam name="URL.toDate">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset submissionStruct=FM.getSubmissions(formID=URL.formID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage, useDateRange=1, fromDate=URL.fromDate, toDate=URL.toDate, numColumns=5)>

<cfif submissionStruct.numAllSubmissions GT 0>
  <cfset startIdx=(URL.pageNum - 1) * URL.numItemsPerPage + 1>
  <cfset endIdx=URL.pageNum * URL.numItemsPerPage>
  <cfif endIdx GT submissionStruct.numAllSubmissions><cfset endIdx=submissionStruct.numAllSubmissions></cfif>
  <p><cfoutput>Display #startIdx# - #endIdx# of #submissionStruct.numAllSubmissions# Submissions</cfoutput></p>
</cfif>

<cfif submissionStruct.numDisplayedSubmissions GT 0>
<form id="frmSubmissionList">
<div class="itemList">
<cfoutput>
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th width="150">Date Submitted</th> 
	<cfloop index="idx" from="1" to="#submissionStruct.numColumns#">
  	<th>#submissionStruct.columnNames[idx]#</th>
	</cfloop>
  </tr>
  <cfloop index="idx" from="1" to="#submissionStruct.numDisplayedSubmissions#">
  <cfset dateSubmitted=submissionStruct.submissions[idx].dateSubmitted>
  <cfset submissionID=submissionStruct.submissions[idx].submissionID>
  <tr>
    <td align="center"><input type="checkbox" name="submissionID" value="#submissionID#"></td>
    <td align="center"><a href="##" onClick="viewSubmission(#URL.formID#, #submissionID#);" class="accent01">#DateFormat(dateSubmitted,"mm/dd/yyyy")# #TimeFormat(dateSubmitted, "h:mm tt")#</a></td>
    <cfloop index="idx1" from="1" to="#submissionStruct.numColumns#">
    <td>#submissionStruct.submissions[idx]["#submissionStruct.formItemIDs[idx1]#"]#</td>
    </cfloop>
  </tr>
  </cfloop>
</table>
</cfoutput>
</div>
</form>

<cfif submissionStruct.numAllSubmissions GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(submissionStruct.numAllSubmissions/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadSubmissionList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>

</cfif>

