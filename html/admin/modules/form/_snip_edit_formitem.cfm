<cfparam name="URL.formID">
<cfparam name="URL.groupID">
<cfparam name="URL.formItemID">

<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset formItemInfo=FM.getFormItemInfo(URL.formItemID)>

<cfoutput query="formItemInfo">
<form id="frmEditFormItem" name="frmEditFormItem" onsubmit="return false;">
<input type="hidden" name="formID" value="#URL.formID#" />
<input type="hidden" name="groupID" value="#URL.groupID#" />
<input type="hidden" name="formItemID" value="#URL.formItemID#" />
<input type="hidden" name="fieldType" value="#fieldType#">
<div class="row">
  <div class="col-32-right"><input type="checkbox" name="active" value="1" <cfif active>checked</cfif> /></div>
  <div class="col-66-left">Set Active</div>
</div>

<cfswitch expression="#fieldType#">
  <cfcase value="Text Only">
    <div class="row">
      <div class="col-32-right">Enter plain text or HTML:</div>
      <div class="col-66-left">
        <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteURLRoot#/scripts/css/styles_for_forms.css"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"            
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmEditFormItem"
            fieldName="instructionText"
            cols="60"
            rows="8"
            HTMLContent="#instructionText#"
            displayHTMLSource="true"
            editorHeader="Edit Instructional Text">
      </div>
    </div>
  </cfcase>
  <cfdefaultcase>
    <div class="row">
	  <div class="col-32-right">Item Label:</div>
  	  <div class="col-66-left">
        <input type="text" name="itemLabel" size="40" value="#HTMLEditFormat(itemLabel)#" />
        <input type="checkbox" name="boldItemLabel" value="1" <cfif boldItemLabel>checked</cfif> /> Bold
      </div>
    </div>
    <div class="row">
      <div class="col-32-right"><b>Item Name:</b></div>
  	  <div class="col-66-left">
        <input type="checkbox" name="itemNameSameAsLabel" value="1" /> Same as item label; OR<br />
        <input type="text" name="itemName" size="40" value="#HTMLEditFormat(itemName)#" /><br />
        <small>(For internal reference only.)</small>
      </div>
    </div>
    <div class="row">
      <div class="col-32-right"><input type="checkbox" name="mandatory" value="1" <cfif mandatory>checked</cfif> /></div>
      <div class="col-66-left">This is a required field.</div>
    </div>
  </cfdefaultcase>
</cfswitch>

<cfswitch expression="#fieldType#">
  <cfcase value="Short Text">
    <div class="row">
      <div class="col-32-right">Predefined special field:</div>
      <div class="col-66-left">
        <select name="fieldContext">
          <option value="">None</option>
          <option value="Email"<cfif fieldContext Is "Email"> selected</cfif>>E-mail Address</option>
          <option value="Full Name"<cfif fieldContext Is "Full Name"> selected</cfif>>Full Name</option>
          <option value="Date"<cfif fieldContext Is "Date"> selected</cfif>>Date (with date picker)</option>
        </select><br />
        <small>(A form can have at most one "E-mail" and one "Full Name" predefined field.)</small>
      </div>
    </div>
    <div class="row">
      <div class="col-32-right">&nbsp;</div>
      <div class="col-66-left"><a href="##" onClick="xsite.toggle('optionalSettings2')" class="accent01">Optional &dArr;</a></div>
    </div>
    <div id="optionalSettings2" style="display:none;">
      <div class="row">
        <div class="col-32-right">Field Size:</div>
        <div class="col-66-left">
          <select name="fieldSize">
            <option value="Small"<cfif fieldSize Is "Small"> selected</cfif>>Small</option>
            <option value="Medium"<cfif fieldSize Is "Medium"> selected</cfif>>Medium</option>
            <option value="Large"<cfif fieldSize Is "Large"> selected</cfif>>Large</option>
          </select>
        </div>
      </div>
      <div class="row">
	    <div class="col-32-right">Apply Validation:</div>
  	    <div class="col-66-left">
          <select name="dataType">
            <option value="Text">No</option>
            <option value="Email"<cfif applyValidation AND (dataType Is "Email" OR fieldContext Is "Email")> selected</cfif>>E-mail Address</option>
            <option value="Date"<cfif applyValidation AND (dataType Is "Date" OR fieldContext Is "Date")> selected</cfif>>Date</option>
            <option value="Number"<cfif applyValidation AND dataType Is "Number"> selected</cfif>>Number</option>
            <option value="Phone"<cfif applyValidation AND dataType Is "Phone"> selected</cfif>>Telephone Number</option>
            <option value="SSN"<cfif applyValidation AND dataType Is "SSN"> selected</cfif>>Social Security Number</option>
          </select>
        </div>
      </div>
      <div class="row">
	    <div class="col-32-right">User Instructions:</div>
  	    <div class="col-66-left">
          <input type="text" name="itemNotes" size="40" value="#HTMLEditFormat(itemNotes)#" /><br />
          <small>(Will appear below this field.)</small>
        </div>
      </div>
      <div class="row">
        <div class="col-32-right">Field Default Value:</div>
        <div class="col-66-left"><input type="text" name="shortTextDefault" size="40" value="#HTMLEditFormat(shortTextDefault)#" /></div>
      </div>
    </div>
  </cfcase>
  <cfcase value="Long Text">
    <div class="row">
      <div class="col-32-right">&nbsp;</div>
      <div class="col-66-left"><a href="##" onClick="xsite.toggle('optionalSettings2')" class="accent01">Optional &dArr;</a></div>
    </div>
    <div id="optionalSettings2" style="display:none;">
      <div class="row">
        <div class="col-32-right">Field Size:</div>
        <div class="col-66-left">
          <select name="fieldSize">
            <option value="Small"<cfif fieldSize Is "Small"> selected</cfif>>Small</option>
            <option value="Medium"<cfif fieldSize Is "Medium"> selected</cfif>>Medium</option>
            <option value="Large"<cfif fieldSize Is "Large"> selected</cfif>>Large</option>
          </select>
        </div>
      </div>
      <div class="row">
	    <div class="col-32-right">User Instructions:</div>
  	    <div class="col-66-left">
          <input type="text" name="itemNotes" size="40" value="#HTMLEditFormat(itemNotes)#" /><br />
          <small>(Will appear below this field.)</small>
        </div>
      </div>
      <div class="row">
        <div class="col-32-right">Field Default Value:</div>
        <div class="col-66-left"><textarea name="longTextDefault" cols="38" rows="5">#HTMLEditFormat(longTextDefault)#</textarea></div>
      </div>
    </div>
  </cfcase>
  <cfcase value="Radio Group">
    <div class="row">
      <div class="col-32-right"><b>Options:</b></div>
      <div class="col-66-left">
        <input type="button" value="Import From Predefined Option List" onclick="xsite.openWindow('index.cfm?md=form&tmp=import_options&formName=frmEditFormItem&wrap=1&fieldName=optionList','importOptions',500,350)" /><br /> 
        <textarea name="optionList" cols="38" rows="5">#HTMLEditFormat(optionList)#</textarea> <small>(Enter an option per line)</small><br />
        Default(Pre-select) Value:<br />
        <input type="text" name="shortTextDefault" size="40" value="#HTMLEditFormat(shortTextDefault)#" />
      </div>
    </div>
    <div class="row">
	  <div class="col-32-right">User Instructions:</div>
  	  <div class="col-66-left">
        <input type="text" name="itemNotes" size="40" value="#HTMLEditFormat(itemNotes)#" /><br />
        <small>(Will appear below this field.)</small>
      </div>
    </div> 
  </cfcase>
  <cfcase value="Check Boxes">
    <div class="row">
      <div class="col-32-right"><b>Options:</b></div>
      <div class="col-66-left">
        <input type="button" value="Import Predefined Options" onclick="xsite.openWindow('index.cfm?md=form&tmp=import_options&formName=frmEditFormItem&wrap=1&fieldName=optionList','importOptions',500,350)" /><br />
        <textarea name="optionList" cols="38" rows="5">#HTMLEditFormat(optionList)#</textarea> <small>(Enter an option per line)</small><br />
        Default(Pre-select) Values:<br />
        <textarea name="longTextDefault" cols="40" rows="3">#HTMLEditFormat(longTextDefault)#</textarea> <small>(Enter an option per line)</small>
      </div>
    </div>
    <div class="row">
      <div class="col-32-right">Minimum number of items to be selected (if required):</div>
      <div class="col-66-left"><input type="text" name="numRequiredOptions" size="2" value="#numRequiredOptions#" /></div>
    </div>   

    <div class="row">
	  <div class="col-32-right">User Instructions:</div>
  	  <div class="col-66-left">
        <input type="text" name="itemNotes" size="40" value="#HTMLEditFormat(itemNotes)#" /><br />
        <small>(Will appear below this field.)</small>
      </div>
    </div> 
  </cfcase>
  <cfcase value="Drop-Down Menu">
    <div class="row">
      <div class="col-32-right"><b>Options:</b></div>
      <div class="col-66-left">
        <input type="button" value="Import From Predefined Option List" onclick="xsite.openWindow('index.cfm?md=form&tmp=import_options&formName=frmEditFormItem&wrap=1&fieldName=optionList','importOptions',500,350)" /><br />        
        <textarea name="optionList" cols="38" rows="5">#HTMLEditFormat(optionList)#</textarea> <small>(Enter an option per line)</small><br />
        Default(Pre-select) Value:<br />
        <input type="text" name="shortTextDefault" size="40" value="#HTMLEditFormat(shortTextDefault)#" />
      </div>
    </div>
    <div class="row">
	  <div class="col-32-right">User Instructions:</div>
  	  <div class="col-66-left">
        <input type="text" name="itemNotes" size="40" value="#HTMLEditFormat(itemNotes)#" /><br />
        <small>(Will appear below this field.)</small>
      </div>
    </div> 
  </cfcase>
</cfswitch>
</form>
</cfoutput>