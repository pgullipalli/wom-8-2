<cfparam name="URL.formID">
<cfparam name="URL.submissionID">

<cfset FM=CreateObject("component", "com.FormAdmin").init()>



<p>
Enter an e-mail address and a retrieve code that will be used to retrieve the saved data.
An e-mail along with the information for retrieving the saved data will be sending to this e-mail address.
</p>

<form name="frmMoveToSavedTable" id="frmMoveToSavedTable" onSubmit="return false;">
<cfoutput>
<input type="hidden" name="submissionID" value="#URL.submissionID#">
<input type="hidden" name="formID" value="#URL.formID#">
</cfoutput>

<div class="row">
  <div class="col-32-right">E-Mail Address:</div>
  <div class="col-66-left"><input type="text" name="emailForSavedData" size="35" maxlength="100" validate="email:true, required:true"></div>
</div>
<div class="row">
  <div class="col-32-right">Retrieve Code:</div>
  <div class="col-66-left">
    <input type="text" name="passwordForSavedData" size="12" maxlength="12" validate="required:true"><br />
    (Any combination of numbers and letters between 2 and 12 characters.)
  </div>
</div>
</form>