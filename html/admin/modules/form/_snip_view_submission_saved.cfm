<cfparam name="URL.formID">
<cfparam name="URL.submissionID">

<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset submissionStruct=FM.getSubmissionsInSavedTable(formID=URL.formID, submissionID=URL.submissionID)>

<br />
<cfif submissionStruct.numDisplayedSubmissions GT 0>
  <cfset dateSubmitted=submissionStruct.submissions[1].dateSubmitted>
  <cfoutput>
  <p>
  <b>Date Submitted: </b>#DateFormat(dateSubmitted,"mm/dd/yyyy")# #TimeFormat(dateSubmitted, "h:mm tt")#
  </p>
  
  <p><b>E-Mail Address:</b> #submissionStruct.submissions[1].emailForSavedData# (for retrieving saved data)</p>
  
  <p><b>Retrieve Code:</b> #submissionStruct.submissions[1].passwordForSavedData#</p>
  
  <br />
  <p>
  <table border="0" cellpadding="5" cellspacing="1" width="100%" align="center" bgcolor="##eeeeee">
    <tr valign="top" bgcolor="##ffffff">
      <td width="40%" align="center"><b>Item Name</b></td>
      <td width="60%" align="center"><b>User Data</b></td>
    </tr>
    <cfloop index="idx" from="1" to="#submissionStruct.numColumns#">
    <tr valign="top" bgcolor="##ffffff">
      <td align="right">#submissionStruct.columnNames[idx]#</td>
      <td>#Replace(submissionStruct.submissions[1]["#submissionStruct.formItemIDs[idx]#"],","," ,","All")#&nbsp;</td>
    </tr>
    </cfloop>
  </table>
  </p>
  </cfoutput>
</cfif>