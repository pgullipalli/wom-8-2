<cfparam name="URL.relativeDirectory">

<br />
<p>Please select files that you'd like to upload from your local computer. You can select up to 5 files per upload</p>
<div align="center">
  <div style="display:block;width:300px;text-align:left;">
	<form id="frmUpload" onsubmit="return false;">
	  <cfoutput><input type="hidden" name="relativeDirectory" value="#URL.relativeDirectory#"></cfoutput>
	  <input type="hidden" name="numFiles" value="5">
	  <input type="hidden" name="localFilePath1" value="">
	  <input type="hidden" name="localFilePath2" value="">
	  <input type="hidden" name="localFilePath3" value="">
	  <input type="hidden" name="localFilePath4" value="">
	  <input type="hidden" name="localFilePath5" value="">
	  File 1: <input type="file" name="localFileField1" size="30" value=""><br /><br />
	  File 2: <input type="file" name="localFileField2" size="30" value=""><br /><br />
	  File 3: <input type="file" name="localFileField3" size="30" value=""><br /><br />
	  File 4: <input type="file" name="localFileField4" size="30" value=""><br /><br />
	  File 5: <input type="file" name="localFileField5" size="30" value=""><br /><br />
	  <input type="checkbox" name="overwrite" value="1"> Overwrite file on server if name conflict occurs<br /><br />
    </form>
  </div>  
</div>
<iframe id="fileUploadTargetedFrame" name="fileUploadTargetedFrame" src="" style="width:200px;height:20px;border:1px solid ##cccccc;display:none;"></iframe>