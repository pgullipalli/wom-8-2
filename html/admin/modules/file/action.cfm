<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  FM=CreateObject("component", "com.FileManagerAdmin").init();

  returnedStruct=StructNew(); // used to return a JSON object
  returnedStruct.success=true;
  returnedStruct.message="";

  switch (URL.task) {
    case "createDirectory":
	  Form.absoluteDirectory=Application.siteDirectoryRoot & "/" & Form.relativeDirectory & "/" & Form.directoryName;
	  returnedStruct=FM.createDirectory(argumentCollection=Form);
      break;

    case "rename":	
	  Form.oldAbsolutePath=Application.siteDirectoryRoot & "/" & Form.relativeDirectory & "/" & Form.oldName;
      Form.newAbsolutePath=Application.siteDirectoryRoot & "/" & Form.relativeDirectory & "/" & Form.newName;
	  Form.acceptableFiles=Application.file.acceptableFiles;
	  returnedStruct=FM.rename(argumentCollection=Form);
      break;

    case "moveFiles":
	  Form.sourceAbsoluteDirectory=Application.siteDirectoryRoot & "/" & Form.relativeDirectory;
	  Form.targetAbsoluteDirectory=Application.siteDirectoryRoot & "/" & URL.targetRelativeDirectory;
	
	  returnedStruct=FM.moveFiles(argumentCollection=Form);
  	  break;

    case "deleteFiles":
      if (Not IsDefined("Form.dir")) { Form.dir = ""; }
      if (Not IsDefined("Form.file")) { Form.file = ""; }
      
      absoluteDirectories=ArrayNew(1);
      absoluteFilePaths=ArrayNew(1);
      
      if (Compare(Form.dir, "")) {
        dirs=ListToArray(Form.dir);
        numDirs=ArrayLen(dirs);
        for (idx=1; idx <= numDirs; idx=idx+1) {
          absoluteDirectories[idx]=Application.siteDirectoryRoot & "/" & Form.relativeDirectory & "/" & dirs[idx];
        }
	  }

	  if (Compare(Form.file, "")) {
	    files=ListToArray(Form.file);
        numFiles=ArrayLen(files);
        for (idx=1; idx <= numFiles; idx=idx+1) {
          absoluteFilePaths[idx]=Application.siteDirectoryRoot & "/" & Form.relativeDirectory & "/" & files[idx];
        }
      }

	  returnedStruct=FM.deleteFiles(absoluteDirectories, absoluteFilePaths);
  	  break;

    case "resizeImage":
	  Form.imageAbsolutePath=Application.siteDirectoryRoot & "/" & Form.imageRelativeDirectory & Form.fileName;
      Form.newImageAbsolutePath=Application.siteDirectoryRoot & "/" & Form.imageRelativeDirectory & Form.newFileName;
    
      if (directoryExists("#Application.siteDirectoryRoot#/assets_temp/")) { // need a buffer directory if this version of coldfusion is unpatched
        Form.imageBufferDirectory="#Application.siteDirectoryRoot#/assets_temp/";
      }
      
      returnedStruct = FM.resizeImage(argumentCollection=Form);
      break;
      
    default:
      break;
  }
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>