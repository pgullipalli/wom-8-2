<cfparam name="URL.type">
<cfparam name="URL.relativeDirectory">
<cfparam name="URL.oldName">

<cfoutput>
<form id="frmRename" onsubmit="return false;">
<input type="hidden" name="type" value="#URL.type#">
<input type="hidden" name="relativeDirectory" value="#URL.relativeDirectory#">
<input type="hidden" name="oldName" value="#HTMLEditFormat(URL.oldName)#">
<cfif URL.type Is "dir">
  <p><b>Current Folder Name: </b> #URL.oldName#</p>
  <p>
  <b>New Folder Name:</b> <input type="text" name="newName" maxlength="100" value="#HTMLEditFormat(URL.oldName)#">
  </p>
<cfelse>
  <p><b>Current File Name: </b> #URL.oldName#</p>
  <p>
  <b>New File Name:</b> <input type="text" name="newName" maxlength="100" value="#HTMLEditFormat(URL.oldName)#">
  </p>
</cfif>
</form>
</cfoutput>