<cfparam name="URL.relativeFilePath">

<!--- For Security --->
<cfif Find(Application.assetRelativeDirectory, URL.relativeFilePath) Is Not 1><cfabort></cfif>

<cfset absoluteFilePath=Application.siteDirectoryRoot & "/" & URL.relativeFilePath>

<cfheader name="Content-Disposition" value="attachment; filename=#getFileFromPath(absoluteFilePath)#">
<cfcontent file="#absoluteFilePath#" deletefile="no" type="xyz/dummy">