<cfset newline=Chr(13) & Chr(10)>
<cfset htmlHead='<link rel="stylesheet" href="/jsapis/jquery/css/treeview/jquery.treeview.css" type="text/css" />' & newline>
<cfset htmlHead=htmlHead & '<script src="/jsapis/jquery/js/jquery.treeview.min.js" type="text/javascript"></script>' & newline>
<cfset htmlHead=htmlHead & '<script src="/jsapis/jquery/js/jquery.treeview.async.js" type="text/javascript"></script>' & newline>
<cfset htmlHead=htmlHead & '<script type="text/javascript" src="modules/file/filemanager.js"></script>' & newline>
<cfhtmlhead text="#htmlHead#">

<div class="header">FILE MANAGER</div>

<!--- Javascript Ref#00001 --->
<div id="actionBar">
<a href="#" id="btnUpload" class="fg-button ui-state-default ui-corner-all button65">Upload</a>
<a href="#" id="btnDownload" class="fg-button ui-state-default ui-corner-all button65">Download</a>
<a href="#" id="btnNewFolder" class="fg-button ui-state-default ui-corner-all button90">New Folder</a>
<a href="#" id="btnRename" class="fg-button ui-state-default ui-corner-all button65">Rename</a>
<a href="#" id="btnMove" class="fg-button ui-state-default ui-corner-all button90">Move Files</a>
<a href="#" id="btnDelete" class="fg-button ui-state-default ui-corner-all button65">Delete</a>
</div>

<form id="frmPathInfo"><!--- used to hold the current path status; the value changes when a directory in the path finder is clicked --->
<cfoutput>
<input type="hidden" name="currentRelativeDirectory" value="#Application.assetRelativeDirectory#"><!--- default starting directory --->
</cfoutput>
</form>

<br /><br /><br />

<!--- Javascript Ref#00002 --->
<div id="pathFinder"></div>

<br />

<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr valign="top">
    <td>
      <!--- Javascript Ref#00003 --->
      <div id="fileListPanel" style="height:500px;overflow:auto;"></div>
    </td>
    <td width="300">
      <!--- Javascript Ref#00004 --->
      <div id="previewPanel" style="margin:0px 0px;padding:0px 15px 5px 15px;overflow:auto;"></div>
    </td>
  </tr>
</table>
