<cfparam name="URL.relativeDirectory">

<cffunction name="IsImageFileName" access="private" output="false" returntype="boolean">
  <cfargument name="fileName" type="string" required="true">
  <cfif FindNoCase(".jpg",Arguments.fileName) GT 0 OR FindNoCase(".gif",Arguments.fileName) GT 0 OR FindNoCase(".png",Arguments.fileName) GT 0>
    <cfreturn true>
  <cfelse>
    <cfreturn false>
  </cfif>
</cffunction>

<cfset absoluteDirectory=Application.siteDirectoryRoot & "/" & URL.relativeDirectory>
<cfdirectory action="list" type="dir" directory="#absoluteDirectory#" name="allDirs" sort="name asc">
<cfdirectory action="list" type="file" directory="#absoluteDirectory#" name="allFiles" sort="name asc">

<!--- Javascript Ref#00003 --->
<div style="padding:0px 0px 5px 0px;">
  <a href="#" action="checkAll" class="noUnderline">Check All</a> |
  <a href="#" action="checkNone" class="noUnderline">Check None</a> |
  <a href="#" action="refresh" class="noUnderline"><img src="images/icon_refresh.png" action="refresh" border="0" align="absmiddle"> Refresh</a>
</div>

<form id="frmFileList">
<cfoutput><input type="hidden" name="relativeDirectory" value="#URL.relativeDirectory#" /></cfoutput>
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th width="250">File/Folder Name</th>  
    <th width="70">Size</th>
	<th>Date Modified</th>
  </tr>

  <cfif Compare(Application.assetRelativeDirectory, URL.relativeDirectory) AND Find(Application.assetRelativeDirectory & "/", URL.relativeDirectory) GT 0>
   <cfset folders=ListToArray(URL.relativeDirectory,"/")>
   <cfset currentDirectory=folders[ArrayLen(folders)]> 
   <cfset relativeParentDirectory=Left(URL.relativeDirectory,Len(URL.relativeDirectory)-Len(currentDirectory)-1)>
   <tr>
	<td>&nbsp;</td>
	<td colspan="3">
	<cfoutput><a href="##"><img src="images/icon_up.png" relativeDirectory="#relativeParentDirectory#" border="0" alt="Go up one level"></a></cfoutput>
	</td>
   </tr>
  </cfif>
  <cfif allDirs.recordcount GT 0 OR allFiles.recordcount GT 0>
    <cfset dirIndex=0>
    <cfoutput query="allDirs">
	  <cfset dirIndex=dirIndex+1>
      <tr>
        <td align="center"><input type="checkbox" name="dir" value="#HTMLEditFormat(name)#"></td>
	    <td>
          <cfset relDir=Replace(URL.relativeDirectory & "/" & name, "'", "\'", "All")>
          <img src="images/icon_folder_close.gif" align="absmiddle" border="0" relativeDirectory="#relDir#">
          <a href="##" relativeDirectory="#relDir#" class="noUnderline" title="#HTMLEditFormat(name)#">#name#</a><!--- Javascript Ref#00003 (click event: same as in Path Finder --->
        </td>
	    <td>&nbsp;</td>
	    <td align="center">#DateFormat(dateLastModified,"mm/dd/yyyy")# #TimeFormat(dateLastModified,"hh:mm:ss tt")#</td>
      </tr>
    </cfoutput>
	<cfset fileIndex=0>
	<cfset uuid=CreateUUID()>
    <cfoutput query="allFiles">
	  <cfset fileIndex=fileIndex+1>
      <tr>
        <td align="center"><input type="checkbox" name="file" value="#HTMLEditFormat(name)#"></td>
	    <td>
          <cfset relativeURL=Replace(URL.relativeDirectory & "/" & name, "'", "\'", "All") & "?" & uuid>
		  <cfif IsImageFileName(name)>
            <img src="images/icon_image.gif" relativeURL="#relativeURL#" align="absmiddle" border="0">
		  <cfelse>
            <img src="images/icon_document.gif" relativeURL="#relativeURL#" align="absmiddle" border="0">
		  </cfif>
          <a href="##" relativeURL="#relativeURL#" class="noUnderline" title="#HTMLEditFormat(name)#">#name#</a>
        </td>
	    <td align="right">#NumberFormat(Ceiling(size/1000), "999,999")# KB</td>
	    <td align="center">#DateFormat(dateLastModified,"mm/dd/yyyy")# #TimeFormat(dateLastModified,"hh:mm:ss tt")#</td>
      </tr>
    </cfoutput>
  <cfelse>
    <tr>
	  <td colspan="4" align="center">This folder is empty.</td>
    </tr>
  </cfif>
</table>
</div>
</form>