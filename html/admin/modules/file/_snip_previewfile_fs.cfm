<cfparam name="URL.relativeURL" default="">

<cfif Compare(URL.relativeURL, "")>
	<cfset fileName=LCase(getFileFromPath(URL.relativeURL))>
	<cfif Find(".jpg", fileName) GT 0 OR Find(".gif", fileName) GT 0 OR Find(".png", fileName) GT 0>
	  <cfset IsImageFile=true>
	<cfelse>
	  <cfset IsImageFile=false>
	</cfif>
	
	<cfif IsImageFile>
	  <!--- this is an image; show image --->
	  <cfoutput>
		<div style="margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;width:260px;height:230px;overflow:auto;">
		<img id="imagePreviewSrcID" name="imagePreviewSrcID" src="#Application.siteURLRoot#/#URL.relativeURL#">
		</div>
	  </cfoutput>	
	</cfif>
	
	<p>
	<b>URL of this file:</b><br />
	<cfoutput>/<span id="selectedRelativeURL">#relativeURL#</span></cfoutput>
	<br />
    <!--- Javascript Ref#00004 --->
    <a href="#" id="btnSelect" class="fg-button ui-state-default ui-corner-all button65">Select</a>
	</p>
</cfif>