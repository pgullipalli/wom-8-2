<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload Files</title>
</head>
<body>
<cfparam name="Form.relativeDirectory">
<cfparam name="Form.overwrite" default="0">

<cfset FM=CreateObject("component", "com.FileManagerAdmin").init()>

<cfset argStruct=StructNew()>
<cfset argStruct.targetAbsoluteDirectory=Application.siteDirectoryRoot & "/" & Form.relativeDirectory>
<cfset argStruct.overwrite=Form.overwrite>
<cfset argStruct.acceptableFiles=Application.file.acceptableFiles>

<cfset error=false>
<cfset uploadStatus=StructNew()>
<cfloop index="idx" from="1" to="#Form.numFiles#">
  <cfset argStruct.localFilePath=Evaluate("Form.localFilePath#idx#")>
  <cfif Compare(argStruct.localFilePath, "")>	
	<cfset argStruct.localFileFieldName="localFileField#idx#">
	<cfset returnedStruct=FM.upload(argumentCollection=argStruct)>
	<cfif Not returnedStruct.success>
	  <cfset error=true>
      <cfset uploadStatus.STATUS="alert">
    <cfelse>
      <cfset uploadStatus.STATUS="update">
    </cfif>	
	<cfset uploadStatus.MESSAGE=returnedStruct.message>
	<script type="text/javascript">
	  var uploadStatus = <cfoutput>#Trim(SerializeJSON(uploadStatus))#</cfoutput>;
	  if (window.parent.fileManager)
        window.parent.fileManager.updateUploadStatus (uploadStatus);
	  else if (window.parent.fileSelector)
	    window.parent.fileSelector.updateUploadStatus (uploadStatus);	
	</script>
    <cfflush>
  </cfif>
</cfloop>
<cfif error>
  <script type="text/javascript">
    var uploadStatus = {STATUS:'terminate', MESSAGE: ''};
	if (window.parent.fileManager)
      window.parent.fileManager.updateUploadStatus (uploadStatus);
	else if (window.parent.fileSelector)
	  window.parent.fileSelector.updateUploadStatus (uploadStatus);
  </script>
  <cfflush>
<cfelse>
  <script type="text/javascript">
    var uploadStatus = {STATUS:'complete', MESSAGE: ''};
	if (window.parent.fileManager)
      window.parent.fileManager.updateUploadStatus (uploadStatus);	
	else if (window.parent.fileSelector)
	  window.parent.fileSelector.updateUploadStatus (uploadStatus);	
  </script>
  <cfflush>
</cfif>
</body>
</html>