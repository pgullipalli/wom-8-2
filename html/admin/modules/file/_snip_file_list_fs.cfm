<!--- For File Selector --->
<cfparam name="URL.relativeDirectory">

<cffunction name="IsImageFileName" access="private" output="false" returntype="boolean">
  <cfargument name="fileName" type="string" required="true">
  <cfif FindNoCase(".jpg",Arguments.fileName) GT 0 OR FindNoCase(".gif",Arguments.fileName) GT 0 OR FindNoCase(".png",Arguments.fileName) GT 0>
    <cfreturn true>
  <cfelse>
    <cfreturn false>
  </cfif>
</cffunction>

<cfset absoluteDirectory=Application.siteDirectoryRoot & "/" & URL.relativeDirectory>
<cfdirectory action="list" type="dir" directory="#absoluteDirectory#" name="allDirs" sort="name asc">
<cfdirectory action="list" type="file" directory="#absoluteDirectory#" name="allFiles" sort="name asc">

<form id="frmFileList">
<cfoutput><input type="hidden" name="relativeDirectory" value="#URL.relativeDirectory#" /></cfoutput>
<div class="itemList">
<table>
  <tr>
	<th>File/Folder Name</th>  
    <th width="50">Size</th>
	<th width="150">Date Modified</th>
  </tr>

  <cfif Compare(Application.assetRelativeDirectory, URL.relativeDirectory) AND Find(Application.assetRelativeDirectory & "/", URL.relativeDirectory) GT 0>
   <cfset folders=ListToArray(URL.relativeDirectory,"/")>
   <cfset currentDirectory=folders[ArrayLen(folders)]> 
   <cfset relativeParentDirectory=Left(URL.relativeDirectory,Len(URL.relativeDirectory)-Len(currentDirectory)-1)>
   <tr>
	<td colspan="3">
	<cfoutput><a href="##"><img src="images/icon_up.png" relativeDirectory="#relativeParentDirectory#" border="0" alt="Go up one level"></a></cfoutput>
	</td>
   </tr>
  </cfif>
  <cfif allDirs.recordcount GT 0 OR allFiles.recordcount GT 0>
    <cfset dirIndex=0>
    <cfoutput query="allDirs">
	  <cfset dirIndex=dirIndex+1>
      <tr>
	    <td>
          <cfset relDir=Replace(URL.relativeDirectory & "/" & name, "'", "\'", "All")>
          <img src="images/icon_folder_close.gif" align="absmiddle" border="0" relativeDirectory="#relDir#">
          <a href="##" relativeDirectory="#relDir#" class="noUnderline">#name#</a><!--- Javascript Ref#00003 (click event: same as in Path Finder --->
        </td>
	    <td>&nbsp;</td>
	    <td align="center">#DateFormat(dateLastModified,"mm/dd/yyyy")# #TimeFormat(dateLastModified,"hh:mm:ss tt")#</td>
      </tr>
    </cfoutput>
	<cfset fileIndex=0>
    <cfoutput query="allFiles">
	  <cfset fileIndex=fileIndex+1>
      <tr>
	    <td>
          <cfset relativeURL=Replace(URL.relativeDirectory & "/" & name, "'", "\'", "All")>
		  <cfif IsImageFileName(name)>
            <img src="images/icon_image.gif" relativeURL="#relativeURL#" align="absmiddle" border="0">
		  <cfelse>
            <img src="images/icon_document.gif" relativeURL="#relativeURL#" align="absmiddle" border="0">
		  </cfif>
          <a href="##" relativeURL="#relativeURL#" class="noUnderline">#name#</a>
        </td>
	    <td align="right">#NumberFormat(Ceiling(size/1000), "999,999")# KB</td>
	    <td align="center">#DateFormat(dateLastModified,"mm/dd/yyyy")# #TimeFormat(dateLastModified,"hh:mm:ss tt")#</td>
      </tr>
    </cfoutput>
  <cfelse>
    <tr>
	  <td colspan="3" align="center">This folder is empty.</td>
    </tr>
  </cfif>
</table>
</div>
</form>