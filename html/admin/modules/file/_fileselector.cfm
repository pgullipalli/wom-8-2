<script type="text/javascript" src="modules/file/fileselector.js"></script>

<p align="right"><a href="javascript:window.close();"><img src="images/icon_cancel.gif" border="0" /></a></p>

<div class="header">FILE SELECTOR</div>


<div id="actionBar">
<a href="#" id="btnUpload" class="fg-button ui-state-default ui-corner-all button65">Upload</a>
</div>

<form id="frmPathInfo"><!--- used to hold the current path status; the value changes when a directory in the path finder is clicked --->
<cfoutput>
<input type="hidden" name="currentRelativeDirectory" value="#Application.assetRelativeDirectory#"><!--- default starting directory --->
</cfoutput>
</form>

<br /><br /><br />

<!--- Javascript Ref#00002 --->
<div id="pathFinder"></div>

<br />

<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr valign="top">
    <td>
      <!--- Javascript Ref#00003 --->
      <div id="fileListPanel" style="height:500px;overflow:auto;"></div>
    </td>
    <td width="300">
      <!--- Javascript Ref#00004 --->
      <div id="previewPanel" style="margin:0px 0px;padding:0px 15px 5px 15px;overflow:auto;"></div>
    </td>
  </tr>
</table>
