<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.root">

<cfif URL.root Is "source"><!--- starting directory --->
  <cfset dir = Application.siteDirectoryRoot & "/" & Application.assetRelativeDirectory>
  <cfset currentDir = Application.assetRelativeDirectory>
  <cfdirectory action="list" directory="#dir#" name="allDirs" sort="name ASC">
  <cfoutput>
  [
    { 'text': ' <input type="checkbox" name="relativeDirectory" value="#Application.assetRelativeDirectory#" /> #Application.assetRelativeDirectory#',
      "expanded": true,
	  "children":
	  [
        <cfset idx=0>
		<cfloop query="allDirs">
        <cfif type Is "Dir"><cfset idx = idx + 1><cfif idx GT 1>,</cfif>
		{ 'text': ' <input type="checkbox" name="relativeDirectory" value="#currentDir#/#Replace(name,"'","\'","All")#" /> #Replace(name,"'","\'","All")#',
    	  "hasChildren": true,
   		  "id": "#currentDir#/#Replace(name,"'","\'","All")#"		  
		}
        </cfif>
        </cfloop>
	  ]
    }
  ]
  </cfoutput>
<cfelse>
  <cfset dir=Application.siteDirectoryRoot & "/" & URL.root >
  <cfset currentDir=URL.root>
  <cfdirectory action="list" directory="#dir#" name="allDirs" sort="name ASC">
  <cfoutput>[</cfoutput>
  <cfset idx=0>
  <cfoutput query="allDirs">
  <cfif type Is "Dir"><cfset idx = idx + 1><cfif idx GT 1>,</cfif>
  { 'text': ' <input type="checkbox" name="relativeDirectory" value="#currentDir#/#Replace(name,"'","\'","All")#" /> #Replace(name,"'","\'","All")#',
    "hasChildren": true,
    "id": "#currentDir#/#Replace(name,"'","\'","All")#"
  }
  </cfif>
  </cfoutput>
  <cfoutput>]</cfoutput>
</cfif>
</cfsetting>