<cfparam name="URL.imageRelativeURL">
<cfset fileName=GetFileFromPath(URL.imageRelativeURL)>
<cfset imageRelativeDirectory = GetDirectoryFromPath(URL.imageRelativeURL)>
<cfset imageAbsoluteURL=Application.siteURLRoot & "/" & imageRelativeDirectory & fileName>

<div align="center">
  <div id="imageWorkspace" style="margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;width:650px;height:375px;overflow:auto;"></div>
</div>
<div align="center">
  <div id="divImageResizeAlert" style="width:400px;padding:0px 100px 0px 0px;text-align:left;display:none"></div>
</div>
<div>
<form id="frmResizeImage">
<input type="hidden" name="origWidth" value="">
<input type="hidden" name="origHeight" value="">
<cfoutput>
<input type="hidden" name="fileName" value="#fileName#" />
<input type="hidden" name="imageRelativeDirectory" value="#imageRelativeDirectory#" />
<input type="hidden" name="siteURLRoot" value="#Application.siteURLRoot#" />
<input type="hidden" name="imageAbsoluteURL" value="#imageAbsoluteURL#">
</cfoutput>
<table border="0" cellpadding="3" cellspacing="0" width="450" align="center">
  <tr valign="top">
	<td width="40%"><b>Resize Image</b></td><td>&nbsp;</td>
  </tr>
  <tr valign="top">
    <td align="right">New width in pixels:</td>
	 <td><input type="text" name="newWidth" size="4" value=""></td>
  </tr>
  <tr valign="top">
    <td align="right">New height in pixels:</td>
	 <td><input type="text" name="newHeight" size="4" value=""></td>
  </tr>
  <tr valign="top">
    <td align="right"><input type="checkbox" name="constrainProportions" checked></td>
    <td>Constrain Proportions</td>
  </tr>
  <tr valign="top">
    <td align="right">Save As:</td>
	 <td>
	   <cfoutput><input type="text" name="newFileName" size="20" value="#GetFileFromPath(URL.imageRelativeURL)#"></cfoutput><br />
       <input type="checkbox" name="overwrite" value="1" checked /> Overwrite file if name conflict occurs
     </td>
  </tr>
</table>
</form>
</div>