<cfparam name="URL.relativeURL" default="">
<cfparam name="URL.clearCache" default="0">

<cfset qStrStartIdx=Find("?", URL.relativeURL)><!--- remove appending stuff from image URL --->
<cfif qStrStartIdx GT 0>
  <cfset relativeURLActual=Left(URL.relativeURL, qStrStartIdx - 1)>
<cfelse>
  <cfset relativeURLActual=URL.relativeURL>
</cfif>

<cfif URL.clearCache>
  <cfset URL.relativeURL=relativeURLActual & "?" & CreateUUID()><!--- prevent getting image from browser cache --->
</cfif>

<cfif Compare(URL.relativeURL, "")>
	<cfset fileName=LCase(getFileFromPath(URL.relativeURL))>
	<cfif Find(".jpg", fileName) GT 0 OR Find(".gif", fileName) GT 0 OR Find(".png", fileName) GT 0>
	  <cfset IsImageFile=true>
	<cfelse>
	  <cfset IsImageFile=false>
	</cfif>
	
	<cfif IsImageFile>
	  <!--- this is an image; show image --->
	  <cfoutput>
		<cfif IsImageFile>
	      <div align="center">
            <!--- Javascript Ref#00004 --->
            [ <a action="resize" relativeURL="#relativeURLActual#" href="##" class="noUnderline">Preview in Larger Window / Resize</a> ]
            <a href="##" class="noUnderline"><img action="refresh" relativeURL="#URL.relativeURL#" src="images/icon_refresh.png" border="0" alt="Reload this image" align="absmiddle"></a>
		  </div>
	    </cfif>
		<div style="margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;width:260px;height:230px;overflow:auto;">
		<img id="imagePreviewSrcID" name="imagePreviewSrcID" src="#Application.siteURLRoot#/#URL.relativeURL#"><!--- prevent getting image from browser cache --->
		</div>
	  </cfoutput>	
	</cfif>
	
	<p>
	<b>URL of this file:</b><br />
	<cfoutput>/#relativeURLActual#</cfoutput>
	<br /><br />
	</p>
</cfif>