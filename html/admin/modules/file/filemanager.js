var fileManager = null;

$(function () {	
	fileManager = this;//do this so that functions here can be called externally
	
	//Ref#00001
	$("#btnUpload").click( upload );
	$("#btnDownload").click( download );
	$("#btnNewFolder").click( createDirectory );
	$("#btnRename").click( rename );
	$("#btnMove").click( moveFiles );
	$("#btnDelete").click ( deleteFiles );
	
	//alert text that will be used many times
	var fileNameRequirementAlert='Please use only the following characters in your file/folder name: letters, numbers, "-" (dash), "_" (underscore), and "." (dot)';
	
	loadFileList();
	
	//Ref#00002
	$("#pathFinder").click(function(e) {
	  if (e.target.tagName == "A") {//a directory in the pathfinder path was clicked
	    //change the currentRelativeDirectory to the directiry just clicked
		$("#frmPathInfo input[name='currentRelativeDirectory']").val($(e.target).attr('relativeDirectory'));
		loadFileList();
	  }
	});
	
	//Ref#00003
	$("#fileListPanel").click(function(e) {//define all actions within the file list panel
	  if ((e.target.tagName == "A" || e.target.tagName == "IMG") && $(e.target).attr('relativeDirectory') != null) {
		$("#frmPathInfo input[name='currentRelativeDirectory']").val($(e.target).attr('relativeDirectory'));//change the current relative directory
		loadFileList();	
	  } else if ((e.target.tagName == "A" || e.target.tagName == "IMG") && $(e.target).attr('relativeURL') != null) {
		loadPreviewPanel($(e.target).attr('relativeURL'));		  
	  } else if ((e.target.tagName == "A" || e.target.tagName == "IMG") && $(e.target).attr('action') != null) {
		var action = $(e.target).attr('action');
		if (action == "checkAll") {
		  var form = $("#frmFileList")[0];
		  if (form.dir) xsite.checkAllBoxes(form.dir);
		  if (form.file) xsite.checkAllBoxes(form.file);
		} else if (action == "checkNone") {
		  var form = $("#frmFileList")[0];
		  if (form.dir) xsite.uncheckAllBoxes(form.dir);
		  if (form.file) xsite.uncheckAllBoxes(form.file);
		} else if (action == "refresh") {
		  loadFileList();
		}
	  }
	});
	
	//Ref#00004
	$("#previewPanel").click(function(e) {//define all actions (preview, resize, refresh within the preview panel
	  if (e.target.tagName == "A" && $(e.target).attr('action') == "resize") {
		var imageRelativeURL = $(e.target).attr('relativeURL');
		previewInLargerWindow (imageRelativeURL);//this function is at the end of this file
	  } else if (e.target.tagName == "IMG" && $(e.target).attr('action') == "refresh") {
	    var relativeURL = $(e.target).attr('relativeURL');
		loadPreviewPanel(relativeURL, true);
	  }
	});
	
	function loadFileList() {
	  var relativeDirectory = $("#frmPathInfo input[name='currentRelativeDirectory']").val();//get the currentRelativeDirectory
	  var url="index.cfm?md=file&tmp=snip_file_list_pathfinder&relativeDirectory=" + relativeDirectory + "&uuid=" + xsite.getUUID();
	  xsite.load("#pathFinder", url);//load path finder
	  
	  url="index.cfm?md=file&tmp=snip_file_list&relativeDirectory=" + relativeDirectory + "&uuid=" + xsite.getUUID();
	  xsite.load("#fileListPanel", url);//list files in the current directory
	  $("#previewPanel").html('');//clear preview panel
	}

	function loadPreviewPanel(relativeURL, clearCache) {	  
	  var url = "index.cfm?md=file&tmp=snip_previewfile&relativeURL=" + escape(relativeURL);
	  if (clearCache) url = url + "&clearCache=1";
	  xsite.load("#previewPanel", url);//load previewPanel
	}
	
	function upload() {
	  var relativeDirectory = $("#frmPathInfo input[name='currentRelativeDirectory']").val();
	  xsite.createModalDialog({
		windowName: 'winUploadFile',
		title: 'Upload Files',
		width: 600,
		position: 'center-up',
		url: 'index.cfm?md=file&tmp=snip_upload&relativeDirectory=' + relativeDirectory + '&uuid=' + xsite.getUUID(),
		buttons: {
		  'Cancel': function() { $("#winUploadFile").dialog('close'); },
		  ' Reset ': function() {$("#frmUpload")[0].reset();},
		  ' Upload ': submitForm_UploadFile
		}
	  }).dialog('open');
		
	  function submitForm_UploadFile() {
		var form = $("#frmUpload")[0];
		var numFilesMax = parseInt(form.numFiles.value);    
		
		//validate first
		var found = false;
		for (var i = 1; i <= numFilesMax; i++) {
		  var fileFieldObj = eval("form.localFileField" + i);
		  var localFilePathObj = eval("form.localFilePath" + i);
		  if (fileFieldObj) {
			if (fileFieldObj.value != "") {
			  found = true;
			  localFilePathObj.value=fileFieldObj.value;
			  var fileName=xsite.getFileFromPath(fileFieldObj.value);
			  if (!xsite.checkFileName(fileName)) {
				alert('The file name "' + fileName + '" contains characters that may cause problems when accessed over the Internet. ' + fileNameRequirementAlert);
				return;
			  }
			}
		  }  
		}
		if (!found) {
		  alert ("Please select files to upload.");
		  return;
		}  
		  
		form.target = "fileUploadTargetedFrame";
		form.method="post";
		form.encoding="multipart/form-data";
		form.action='index.cfm?md=file&tmp=snip_upload_action';

	    xsite.showWaitingDialog({width:450,height:400,openCallback:function() {form.submit();}});
	  }
	}
	
	this.updateUploadStatus = function (uploadStatus) {
		if (uploadStatus.STATUS=='complete') {
		  xsite.closeWaitingDialog();
		  $("#winUploadFile").dialog('close');
		  loadFileList();
		} else if (uploadStatus.STATUS=='update') {
		  xsite.addMessageToWaitingDialog("<br /><br />- " + uploadStatus.MESSAGE);
		} else if (uploadStatus.STATUS=='alert') {
		  xsite.addMessageToWaitingDialog('<br /><br />- <span class="alert">' + uploadStatus.MESSAGE + '</span>');
		} else {
		  //complete but with errors
		  if (uploadStatus.MESSAGE != "")  xsite.addMessageToWaitingDialog('<br /><br />- <span class="alert">' + uploadStatus.MESSAGE + '</span>');
		  xsite.addMessageToWaitingDialog('<br /><br /><div align="center"><a href="javascript:xsite.closeWaitingDialog();" class="noUnderline"><b>[ OK ]</b></a></div>');
		  $("#winUploadFile").dialog('close');
		  loadFileList();
		}
	}
	
	function download() {
	  var form = $("#frmFileList")[0];
	  if (form.dir && xsite.getCheckedValue(form.dir)) {
		alert("Please select a file (not a folder) that you'd like to download.");
		return;
	  }
	  if (!form.file) {
		alert("Please select a file that you'd like to download.");
		return;
	  }
	  
	  var filesSelected = xsite.getCheckedValue(form.file);
	  if (!filesSelected) {
		alert ("Please select a file that you'd like to download.");
		return;
	  }
	  var relativeDirectory = $("#frmPathInfo input[name='currentRelativeDirectory']").val();
	  var relativeFilePath=relativeDirectory + '/' + filesSelected.split(",")[0];
	  window.open('index.cfm?md=file&tmp=snip_downloadfile&relativeFilePath=' + escape(relativeFilePath),'_self');	
	}
	
	function createDirectory() {
	  var relativeDirectory = $("#frmPathInfo input[name='currentRelativeDirectory']").val();
	  xsite.createModalDialog({
		windowName: 'winCreateDirectory',
		title: 'Create New Folder',
		width: 400,
		position: 'center',
		url: 'index.cfm?md=file&tmp=snip_create_directory&relativeDirectory=' + escape(relativeDirectory) + '&uuid=' + xsite.getUUID(),
		buttons: {
		  'Cancel': function() { $("#winCreateDirectory").dialog('close'); },
		  ' Create ': submitForm_CreateDirectory
		}
	  }).dialog('open');

	  function submitForm_CreateDirectory() {
		$('#frmCreateDirectory').ajaxSubmit({ 
		  url: 'action.cfm?md=file&task=createDirectory',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  beforeSubmit: validate,
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	  
		function validate(formData, jqForm, options) {
		  var directoryName = $("#frmCreateDirectory input[name='directoryName']").val();
		  if (!xsite.checkDirectoryName(directoryName)) {
		    alert('The folder name "' + directoryName + '" contains characters that may cause problems when accessed over the Internet. ' + fileNameRequirementAlert);
			return false;
		  }
		}
	
		function processJson(jsonData) {
		  if (jsonData.SUCCESS) {
			$("#winCreateDirectory").dialog('close');
			loadFileList();		
		  } else {
			alert (jsonData.MESSAGE);
		  }
		}  
	  }
	}
	
	function rename() {
	  var relativeDirectory = $("#frmPathInfo input[name='currentRelativeDirectory']").val();
	  var form = $("#frmFileList")[0];
	  if (!(form.dir && xsite.getCheckedValue(form.dir) || form.file && xsite.getCheckedValue(form.file))) {
		alert ("Please select a the file/folder you'd like to rename.");
		return;
	  }
	  var url = "";
	  var targetType = "";
	  if (form.dir && xsite.getCheckedValue(form.dir)) {
		//rename a directory
		targetType = "dir";
		var checkedDirs = xsite.getCheckedValue(form.dir);
		var dirNames = checkedDirs.split(",");
		var dirName = dirNames[0];
		url =  'index.cfm?md=file&tmp=snip_rename&type=dir&relativeDirectory=' + escape(relativeDirectory) +
	  		   '&oldName=' + escape(dirName) + '&uuid=' + xsite.getUUID();
	  } else if (form.file && xsite.getCheckedValue(form.file)) {
		targetType = "file";
		var checkedFiles = xsite.getCheckedValue(form.file);
		var fileNames = checkedFiles.split(",");
		var fileName = fileNames[0];
		url =  'index.cfm?md=file&tmp=snip_rename&type=file&relativeDirectory=' + escape(relativeDirectory) +
	  		 '&oldName=' + escape(fileName) + '&uuid=' + xsite.getUUID();
	  }
	  	
	  xsite.createModalDialog({
		windowName: 'winRename',
		title: 'Rename',
		width: 400,
		position: 'center',
		url: url,
		buttons: {
		  'Cancel': function() { $("#winRename").dialog('close'); },
		  ' Rename ': submitForm_Rename
		}
	  }).dialog('open');
		
	  function submitForm_Rename() {
		$('#frmRename').ajaxSubmit({ 
		  url: 'action.cfm?md=file&task=rename',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  beforeSubmit: validate,
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	  
		function validate(formData, jqForm, options) {
		  var fileName = $("#frmRename input[name='newName']").val();
		  if (targetType=="file") {
		    if (!xsite.checkFileName(fileName)) {
		      alert('The file/folder name "' + fileName + '" contains characters that may cause problems when accessed over the Internet. ' + fileNameRequirementAlert);
			  return false;
		    }
		  } else {
			if (!xsite.checkDirectoryName(fileName)) {
		      alert('The file/folder name "' + fileName + '" contains characters that may cause problems when accessed over the Internet. ' + fileNameRequirementAlert);
			  return false;
		    }
		  }
		}
	
		function processJson(jsonData) {
		  if (jsonData.SUCCESS) {
			$("#winRename").dialog('close');
			loadFileList();		
		  } else {
			alert (jsonData.MESSAGE);
		  }
		}  	
	  }
	}
	
	function moveFiles() {
	  var form = $("#frmFileList")[0];
		
	  if (form.dir && xsite.getCheckedValue(form.dir)) {
		alert ("Sorry, folders can not be moved. Please check off only files you'd like to move.");
		return;
	  }
		
	  if (!(form.file && xsite.getCheckedValue(form.file))) {
		alert ("Please check off the files you'd like to move.");
		return;
	  }
	  
	  xsite.createModalDialog({
		windowName: 'winMoveFiles',
		title: 'Move Files',
		width: 500,
		position: 'center-up',
		modal: 'no',
		url: 'index.cfm?md=file&tmp=snip_move_files',
		urlCallback: function () {
		  $("#dirTree").treeview({
			url: "index.cfm?md=file&tmp=snip_fetch_subdirectories&uuid=" + xsite.getUUID()
		  });
		  $("#dirTree").click(function(e) {
	        if($(e.target).attr("type") == "checkbox") xsite.resetCheckBoxes(e.target);
	      });	
		},
		buttons: {
		  'Cancel': function() { $("#winMoveFiles").dialog('close'); },
		  ' Move ': submitForm_MoveFiles
		}
	  }).dialog('open');

	  function submitForm_MoveFiles() {
		var form = $("#frmFileList")[0];
	    if (form.dir && xsite.getCheckedValue(form.dir)) {
		  alert ("Sorry, folders can not be moved. Please check off only files you'd like to move.");
		  return;
	    }
	    if (!(form.file && xsite.getCheckedValue(form.file))) {
		  alert ("Please check off the files you'd like to move.");
		  return;
	    }
		
		form = $("#frmMoveFiles")[0];
		var targetRelativeDirectory = xsite.getCheckedValue(form.relativeDirectory);
		if (!targetRelativeDirectory) {
		  alert("Please select a folder that the selected files will move to.");
		  return;
		}
		  
		if (!confirm("You are about to move the selected files to this folder. Are you sure you want to proceed?")) {
		  return;
		} else {
		  xsite.showWaitingDialog({openCallback:function() {
			  $('#frmFileList').ajaxSubmit({ 
				url: 'action.cfm?md=file&task=moveFiles&targetRelativeDirectory=' + escape(targetRelativeDirectory),
				type: 'post',
				cache: false,
				dataType: 'json', 
				success: processJson,
				error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
			  });}
		  });
		}
		
		function processJson(jsonData) {
		  if (jsonData.SUCCESS) {
			xsite.closeWaitingDialog();
			$("#winMoveFiles").dialog('close');
			loadFileList();		
		  } else {
			xsite.closeWaitingDialog();
			$("#winMoveFiles").dialog('close');
			alert (jsonData.MESSAGE);
		  }
	    }  	
	  }
	}
	
	function deleteFiles() {
	  var form = $("#frmFileList")[0];
	  if (!(form.dir && xsite.getCheckedValue(form.dir) || form.file && xsite.getCheckedValue(form.file))) {
		alert ("Please check off the files/folders you'd like to delete.");
		return;
	  }
	  
	  if (!confirm("You are about to delete the selected files/folders. Are you sure you want to proceed?")) {
		return;		
	  } else {
		xsite.showWaitingDialog({openCallback:function() {
			$('#frmFileList').ajaxSubmit({ 
			  url: 'action.cfm?md=file&task=deleteFiles',
			  type: 'post',
			  cache: false,
			  dataType: 'json', 
			  success: processJson,
			  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
			});}
		});
	  }
	  
	  function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  xsite.closeWaitingDialog();
		  loadFileList();		
		} else {
		  xsite.closeWaitingDialog();
		  alert (jsonData.MESSAGE);
		}
	  }  	
	}
	
	function previewInLargerWindow(imageRelativeURL) {
	  xsite.createModalDialog({
		windowName: 'winPreviewInLW',
		title: 'Preview/Resize Image',
		width: 750,
		position: 'top',
		url: 'index.cfm?md=file&tmp=snip_preview_resize_image&imageRelativeURL=' + escape(imageRelativeURL),
		urlCallback: initImageWorkSpace,
		buttons: {
		  'Done/Cancel': function() { $("#winPreviewInLW").dialog('close'); },
		  ' Reset ': resetImageResizeForm,
		  ' Resize ': resizeImage
		}
	  }).dialog('open');

	  function initImageWorkSpace() {
		$("#imageWorkspace").html('<br /><br /><br /><img name="imageSrcID" id="imageSrcID" src="images/ajax-loader-indicator.gif">'); 
		  
		var imageAbsoluteURL=$("#frmResizeImage input[name='imageAbsoluteURL']").val() + "?" + xsite.getUUID(); //to ensure image is not pulled from broswer cache
		var imageObj = new Image();
		imageObj.src = imageAbsoluteURL;
		imageObj.onload = function () {
		  var form = $("#frmResizeImage")[0];		  
		  $("#imageWorkspace").html('<img name="imageSrcID" id="imageSrcID" src="' + imageObj.src + '">');
		  form.newWidth.value = imageObj.width;
		  form.origWidth.value = imageObj.width;
		  form.newHeight.value = imageObj.height;
		  form.origHeight.value = imageObj.height;
		}
		var form = $("#frmResizeImage")[0];
		$("#frmResizeImage input[name='newWidth']").keyup(function (e) {
		  if (form.newWidth.value != "") recalculateImageSize('w');
		});
		
		$("#frmResizeImage input[name='newHeight']").keyup(function (e) {
		  if (form.newHeight.value != "") recalculateImageSize('h');
		});
		
		$("#frmResizeImage input[name='constrainProportions']").click(function (e) {
		  if (form.newWidth.value != "") recalculateImageSize('w');
		});
	  }
	  
	  function recalculateImageSize(type) {
		var form = $("#frmResizeImage")[0];
		var nWidth = form.newWidth.value;
		var nHeight = form.newHeight.value
		var oWidth = form.origWidth.value;
		var oHeight = form.origHeight.value
		if (xsite.checkInteger(nWidth) && xsite.checkInteger(nHeight) && parseInt(nWidth) > 0 && parseInt(nHeight) > 0) {
		  if (form.constrainProportions.checked) {
		    if (type == "w") {//change width   
		      form.newHeight.value = '' + Math.round(parseInt(nWidth) * (parseInt(oHeight) / parseInt(oWidth)));   
			} else {//change height
			  form.newWidth.value = '' + Math.round(parseInt(nHeight) * (parseInt(oWidth) / parseInt(oHeight)));
			}
		  }
		  changeImageSize('imageSrcID', form.newWidth.value, form.newHeight.value);
		}
	  }
	  
	  function resetImageResizeForm () {
	    var form = $("#frmResizeImage")[0];
		form.newWidth.value = form.origWidth.value;
		form.newHeight.value = form.origHeight.value;
		changeImageSize('imageSrcID', form.newWidth.value, form.newHeight.value);
	  }
	  
 	  function changeImageSize (srcID, width, height) {
		if (xsite.checkInteger(width) && xsite.checkInteger(height) && parseInt(width) > 0 && parseInt(height) > 0) {
		  document.getElementById('imageSrcID').width = width;
		  document.getElementById('imageSrcID').height = height;
		}    
	  }		

	  function resizeImage() {
	    var form = $("#frmResizeImage")[0];
		
		var nWidth = form.newWidth.value;
		var nHeight = form.newHeight.value
		if (!(xsite.checkInteger(nWidth) && xsite.checkInteger(nHeight) && parseInt(nWidth) > 0 && parseInt(nHeight) > 0)) {
		  alert ("Please make sure the new image width and height you specified are non-zero integers.");	
		  return;
		}
		
		if (!xsite.checkFileName(form.newFileName.value)) {
		  alert('The file name "' + form.newFileName.value + '" contains characters that may cause problems when accessed over the Internet. ' + fileNameRequirementAlert);
		  form.newFileName.focus();
		  return;
		}

		if (form.fileName.value == form.newFileName.value && !form.overwrite.checked) {
		  alert('Please either checked off "overwrite file if name conflict occurs", or enter a different file name in the "Save As" field.');
		  form.newFileName.focus();
		  return;
		}
		
		if (!confirm("You are about to resize the image. Are you sure you want to proceed?")) return;
		
		xsite.showWaitingDialog({modal:'no',width:750,height:600,position:'top',openCallback:submitResize});
		function submitResize() {
			$('#frmResizeImage').ajaxSubmit({ 
			  url: 'action.cfm?md=file&task=resizeImage',
			  type: 'post',
			  cache: false,
			  dataType: 'json', 
			  success: processJson,
			  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
			});		
		}
		
	    function processJson(jsonData) {
		  if (jsonData.SUCCESS) {
			var form = $("#frmResizeImage")[0];
			form.origWidth.value = form.newWidth.value; //alert(form.origWidth.value);
			form.origHeight.value = form.newHeight.value;// alert(form.origHeight.value);
			form.fileName.value = form.newFileName.value;
			form.imageAbsoluteURL.value = form.siteURLRoot.value + "/" + form.imageRelativeDirectory.value + form.fileName.value;
			
		    xsite.closeWaitingDialog();
		    var alertMessage='<p align="center"><span class="hilite">Your image has been resized. ' +
						 //'If you\'d like to preview the image you just resized in the preview panel after you close this window, ' +
						 //'please click on the refresh icon <img src="images/icon_refresh.png" align="absmiddle">.' +
						 '</span> ' +
						 '<a href="#" onclick="$(\'#winPreviewInLW\').dialog(\'close\');" class="noUnderline"><b>[ OK ]</b></p>';

		    $("#divImageResizeAlert").html(alertMessage).show();
			
			
		    var newAbsoluteImageURL=form.imageAbsoluteURL.value + "?" + xsite.getUUID();
			var newRelativeURL=form.imageRelativeDirectory.value + form.fileName.value  + "?" + xsite.getUUID();
			document.getElementById('imageSrcID').src = newAbsoluteImageURL;
		    document.getElementById('imagePreviewSrcID').src = newAbsoluteImageURL;
			//var relativeURL = $("#previewPanel a[action='resize']").attr('relativeURL');//find the actual image URL and reload it
			loadPreviewPanel(newRelativeURL, true);
			loadFileList();
		  } else {
		    xsite.closeWaitingDialog();
		    alert (jsonData.MESSAGE);
		  }
	    }
	  }
	}
});