var fileSelector = null;
$(function() {
	fileSelector = this;
	var formName = xsite.getURLParameter('formName');
	var fieldName = xsite.getURLParameter('fieldName');
	var previewID = xsite.getURLParameter('previewID');
	var previewWidth = xsite.getURLParameter('previewWidth');
	var previewHeight = xsite.getURLParameter('previewHeight');
	
	if (formName=="" || fieldName=="") {
	  alert("The 'formName' and 'fieldName' were not passed. The file selection window will be closed.");
	  window.close();
	}
	
	//alert text that will be used many times
	var fileNameRequirementAlert='Please use only the following characters in your file/folder name: letters, numbers, "-" (dash), "_" (underscore), and "." (dot)';
	
	$("#btnUpload").click( upload );
	
	loadFileList();
	
	$("#pathFinder").click(function(e) {
	  if (e.target.tagName == "A") {//a directory in the pathfinder path was clicked
	    //change the currentRelativeDirectory to the directiry just clicked
		$("#frmPathInfo input[name='currentRelativeDirectory']").val($(e.target).attr('relativeDirectory'));
		loadFileList();
	  }
	});
	
	//Ref#00003
	$("#fileListPanel").click(function(e) {//define all actions within the file list panel
	  if ((e.target.tagName == "A" || e.target.tagName == "IMG") && $(e.target).attr('relativeDirectory') != null) {
		$("#frmPathInfo input[name='currentRelativeDirectory']").val($(e.target).attr('relativeDirectory'));//change the current relative directory
		loadFileList();	
	  } else if ((e.target.tagName == "A" || e.target.tagName == "IMG") && $(e.target).attr('relativeURL') != null) {
		loadPreviewPanel($(e.target).attr('relativeURL'));		  
	  }
	});
	
	//Ref#00004
	$("#previewPanel").click(function(e) {
	  if (e.target.tagName == "A" && $(e.target).attr('id') == 'btnSelect') {  
	    var openerFormField=opener.document[formName][fieldName];
	    var openerPreview=opener.document.getElementById(previewID);
	  
	    var selectedRelativeURL=$("#selectedRelativeURL").html();
	    openerFormField.value=selectedRelativeURL;
		if (openerPreview != null) {
		  if ($("#imagePreviewSrcID").length > 0) {//imagePreviewSrcID exists in preview window indicates it's an image		  
		    if (previewWidth=="" && previewHeight=="") {
			  openerPreview.innerHTML='<img src="/' + selectedRelativeURL + '" align="absbottom"><br />' + selectedRelativeURL;
			} else {
			  if (previewHeight!="") {//preview image height was provided
			    openerPreview.innerHTML='<img src="/' + selectedRelativeURL + '" height=' + previewHeight + '" align="absbottom"><br />' + selectedRelativeURL;
			  } else {
			    openerPreview.innerHTML='<img src="/' + selectedRelativeURL + '" width=' + previewWidth + '" align="absbottom"><br />' + selectedRelativeURL;
			  }
			}
		  } else {//not an image
		    openerPreview.innerHTML='/' + selectedRelativeURL;
		  }
		}
	    window.close();
	  }
	});
	
	function loadFileList() {
	  var relativeDirectory = $("#frmPathInfo input[name='currentRelativeDirectory']").val();//get the currentRelativeDirectory
	  var url="index.cfm?md=file&tmp=snip_file_list_pathfinder&relativeDirectory=" + relativeDirectory + "&uuid=" + xsite.getUUID();
	  xsite.load("#pathFinder", url);//load path finder
	  
	  url="index.cfm?md=file&tmp=snip_file_list_fs&relativeDirectory=" + relativeDirectory + "&uuid=" + xsite.getUUID();
	  xsite.load("#fileListPanel", url);//list files in the current directory
	  $("#previewPanel").html('');//clear preview panel
	}
	
	//Ref#00004
	function loadPreviewPanel(relativeURL) {	  
	  var url = "index.cfm?md=file&tmp=snip_previewfile_fs&relativeURL=" + escape(relativeURL);
	  xsite.load("#previewPanel", url);//load previewPanel
	}
	
	function upload() {
	  var relativeDirectory = $("#frmPathInfo input[name='currentRelativeDirectory']").val();
	  xsite.createModalDialog({
		windowName: 'winUploadFile',
		title: 'Upload Files',
		width: 600,
		position: 'center-up',
		url: 'index.cfm?md=file&tmp=snip_upload&relativeDirectory=' + relativeDirectory + '&uuid=' + xsite.getUUID(),
		buttons: {
		  'Cancel': function() { $("#winUploadFile").dialog('close'); },
		  ' Upload ': submitForm_UploadFile,
		  ' Reset ': function() {$("#frmUpload")[0].reset();}
		}
	  }).dialog('open');
		
	  function submitForm_UploadFile() {
		var form = $("#frmUpload")[0];
		var numFilesMax = parseInt(form.numFiles.value);    
		
		//validate first
		var found = false;
		for (var i = 1; i <= numFilesMax; i++) {
		  var fileFieldObj = eval("form.localFileField" + i);
		  var localFilePathObj = eval("form.localFilePath" + i);
		  if (fileFieldObj) {
			if (fileFieldObj.value != "") {
			  found = true;
			  localFilePathObj.value=fileFieldObj.value;
			  var fileName=xsite.getFileFromPath(fileFieldObj.value);
			  if (!xsite.checkFileName(fileName)) {
				alert('The file name "' + fileName + '" contains characters that may cause problems when accessed over the Internet. ' + fileNameRequirementAlert);
				return;
			  }
			}
		  }  
		}
		if (!found) {
		  alert ("Please select files to upload.");
		  return;
		}  
		  
		form.target = "fileUploadTargetedFrame";
		form.method="post";
		form.encoding="multipart/form-data";
		form.action='index.cfm?md=file&tmp=snip_upload_action';

	    xsite.showWaitingDialog({width:450,height:400,openCallback:function() {form.submit();}});
	  }
	}
	
	this.updateUploadStatus = function (uploadStatus) {
		if (uploadStatus.STATUS=='complete') {
		  xsite.closeWaitingDialog();
		  $("#winUploadFile").dialog('close');
		  loadFileList();
		} else if (uploadStatus.STATUS=='update') {
		  xsite.addMessageToWaitingDialog("<br /><br />- " + uploadStatus.MESSAGE);
		} else if (uploadStatus.STATUS=='alert') {
		  xsite.addMessageToWaitingDialog('<br /><br />- <span class="alert">' + uploadStatus.MESSAGE + '</span>');
		} else {
		  //complete but with errors
		  if (uploadStatus.MESSAGE != "")  xsite.addMessageToWaitingDialog('<br /><br />- <span class="alert">' + uploadStatus.MESSAGE + '</span>');
		  xsite.addMessageToWaitingDialog('<br /><br /><div align="center"><a href="javascript:xsite.closeWaitingDialog();" class="noUnderline"><b>[ OK ]</b></a></div>');
		  $("#winUploadFile").dialog('close');
		  loadFileList();
		}
	}
});