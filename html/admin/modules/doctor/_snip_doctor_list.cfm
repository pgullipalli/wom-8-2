<cfparam name="URL.keyword" default="">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="25">

<cfset DD=CreateObject("component", "com.DoctorAdmin").init()>

<cfset structItems=DD.getDoctorsByKeyword(keyword=URL.keyword, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
<cfif structItems.numAllItems IS 0>
  <cfif Compare(URL.keyword,"")>
    <cfoutput><p>Your search for '<i>#URL.keyword#</i>' returns <cfif structItems.numAllItems GT 0>#structItems.numAllItems# items.<cfelse>no results.</cfif></p></cfoutput>
  <cfelse>
    <p>It appears that there are no doctors in the database.</p>
  </cfif>
</cfif>

<cfif structItems.numDisplayedItems GT 0>
<form id="frmItemList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th width="200">Photo</th>
    <th>Name</th>
    <th>Specialty</th>
    <th>Physician Group</th>
  </tr>  
  <cfoutput query="structItems.doctors">
  <tr>
    <td align="center"><input type="radio" name="recordNum" value="#recordNum#"></td>
    <td align="center"><cfif Compare(photo,"")><img src="/#photo#" border="0" width="80" /><cfelse>&nbsp;</cfif></td>
	<td><a href="##" onclick="editDoctor('#recordNum#')">#lastName#, #firstName# #middleName#<cfif Compare(degree,"")>, #degree#</cfif></a></td>
    <td>#specialty#</td>
    <td>#officeName#&nbsp;</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif structItems.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structItems.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadDoctorList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>