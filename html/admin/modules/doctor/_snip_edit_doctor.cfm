<cfparam name="URL.recordNum">

<cfset DD=CreateObject("component", "com.DoctorAdmin").init()>
<cfset doctorInfo=DD.getDoctorInfo(URL.recordNum)>
<cfset boardCertList=DD.getBoardCertList(URL.recordNum)>

<cfoutput query="doctorInfo">
<form id="frmEditDoctor" name="frmEditDoctor" onsubmit="return false">
<input type="hidden" name="recordNum" value="#URL.recordNum#" />
<div class="row">
	<div class="col-30-right"><b>Name:</b></div>
    <div class="col-68-left">#lastName# #firstName# #middleName#<cfif Compare(degree,"")>, #degree#</cfif></div>
</div>
<div class="row">
  <div class="col-30-right">&nbsp;</div>
  <div class="col-68-left"><a href="##" onClick="xsite.toggle('doctorBasicInfo')" class="accent01">View Additional Basic Info &dArr;</a></div>
</div>
<div id="doctorBasicInfo" style="display:none;">
    <div class="row">
        <div class="col-30-right"><b>Specialty:</b></div>
        <div class="col-68-left">#specialty#</div>
    </div>
    <div class="row">
        <div class="col-30-right"><b>Physician Group:</b></div>
        <div class="col-68-left">#officeName#</div>
    </div>
    <div class="row">
        <div class="col-30-right"><b>Address:</b></div>
        <div class="col-68-left">
        #address1#<cfif Compare(address2,"")><br />#address2#</cfif><br />
        #citya#, #statea# #zipa#
        </div>
    </div>
    <div class="row">
        <div class="col-30-right"><b>Phone:</b></div>
        <div class="col-68-left">#phone# &nbsp;&nbsp; <b>Fax:</b> #fax# &nbsp;&nbsp;</div>
    </div>
    <div class="row">
        <div class="col-30-right"><b>Answering Phone:</b></div>
        <div class="col-68-left">#answering#</div>
    </div>
    <div class="row">
        <div class="col-30-right"><b>Web Address:</b></div>
        <div class="col-68-left">#officeWebsite#</div>
    </div>
    <cfif Compare(languages,"")>
    <div class="row">
        <div class="col-30-right"><b>Languages:</b></div>
        <div class="col-68-left">#languages#</div>
    </div>
    </cfif>
    <div class="row">
        <div class="col-30-right"><b>Board Certifications:</b></div>
        <div class="col-68-left">#replace(boardCertList,",",", ","All")#</div>
    </div>
</div>

<div class="row">
	<div class="col-30-right"><b>E-Mail Address:</b></div>
    <div class="col-68-left"><input type="text" name="email" maxlength="100" style="width:200px;" value="#HTMLEditFormat(email)#" /></div>
</div>
<div class="row">
	<div class="col-30-right"><b>Photo:</b></div>
    <div class="col-68-left">
      <input type="hidden" name="photo" value="#HTMLEditFormat(photo)#" />
      <CF_SelectFile formName="frmEditDoctor" fieldName="photo" previewID="preview1" previewWidth="100">
      <small>(148px x 148px)</small>
      <cfif Compare(photo, "")>
      <div id="preview1"><img src="/#photo#" border="0" width="100" /></div>  
      <cfelse>
      <div id="preview1"></div>   
      </cfif> 
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Physician Group Logo:</b></div>
    <div class="col-68-left">
      <input type="hidden" name="groupLogo" value="#HTMLEditFormat(groupLogo)#" />
      <CF_SelectFile formName="frmEditDoctor" fieldName="groupLogo" previewID="preview2" previewWidth="100">
      <cfif Compare(groupLogo, "")>
      <div id="preview2"><img src="/#groupLogo#" border="0" width="100" /></div>  
      <cfelse>
      <div id="preview2"></div>   
      </cfif> 
    </div>
</div>

<div class="row">
	<div class="col-30-right"><b>Additional Address:</b></div>
    <div class="col-68-left">
      <div class="row">
      <input type="text" name="address1Additional" maxlength="100" style="width:200px;" value="#HTMLEditFormat(address1Additional)#" /> (Address 1)
	  </div>
      <div class="row">
      <input type="text" name="address2Additional" maxlength="100" style="width:200px;" value="#HTMLEditFormat(address2Additional)#" /> (Address 2)
      </div>
      <div class="row">
      <input type="text" name="cityAdditional" maxlength="50" style="width:100px;" value="#HTMLEditFormat(cityAdditional)#" />,
      <input type="text" name="stateAdditional" maxlength="50" style="width:25px;" value="#HTMLEditFormat(stateAdditional)#" />
      <input type="text" name="ZIPAdditional" maxlength="50" style="width:55px;" value="#HTMLEditFormat(ZIPAdditional)#" /> (City, State ZIP)    
      </div>
	</div>
</div>

<div class="row">
	<div class="col-30-right"><b>Bio:</b></div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmEditDoctor"
            fieldName="bio"
            cols="60"
            rows="3"
            HTMLContent="#bio#"
            displayHTMLSource="true"
            editorHeader="Edit Bio">
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Special Medical Interests:</b></div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmEditDoctor"
            fieldName="philosophy"
            cols="60"
            rows="3"
            HTMLContent="#philosophy#"
            displayHTMLSource="true"
            editorHeader="Edit Special Medical Interests">
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Staff:</b></div>
    <div class="col-68-left">
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmEditDoctor"
            fieldName="staff"
            cols="60"
            rows="3"
            HTMLContent="#staff#"
            displayHTMLSource="true"
            editorHeader="Edit Staff">
    </div>
</div>
</form>
</cfoutput>