<script type="text/javascript" src="modules/doctor/introduction.js"></script>

<cfset DD=CreateObject("component", "com.DoctorAdmin").init()>
<cfset introductionText = DD.getIntroductionText()>

<div class="header">DOCTOR DIRECTORY &gt; Introductory Copy</div>

<form name="frmIntroductionText" id="frmIntroductionText" onSubmit="return false;">
<div class="subHeader">Introductory Copy</div>     

<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgIntroductionText"></span></p>
  </div>
</div>

<CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmIntroductionText"
            fieldName="introductionText"
            cols="60"
            rows="3"
            HTMLContent="#introductionText#"
            displayHTMLSource="true"
            editorHeader="Edit Introductory Copy">
<input type="button" value=" Update " onclick="updateIntroductionText();">
</form>