var pageNum=1;//initial page number

$(function () {	
	loadDoctorList(null);
});

function loadDoctorList(pNum) {
  if (pNum) pageNum = pNum;
  var form=$("#frmSearchDoctors")[0];
  var keyword=form.keyword.value;
  
  var url="index.cfm?md=doctor&tmp=snip_doctor_list&keyword=" + escape(keyword) + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function browseAllDoctors() {
  var form=$("#frmSearchDoctors")[0];
  form.keyword.value="";
  pageNum=1;
  loadDoctorList(1);
}

function editDoctor(rNum) {
  var form = $("#frmItemList")[0];
  var recordNum = null;
  if (rNum) recordNum = rNum;
  else recordNum = xsite.getCheckedValue(form.recordNum);

  if (!recordNum) {
	alert("Please select as doctor to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditDoctor',
	title: 'Edit Doctor META Data',
	width: 650,
	position: 'center-up',
	url: 'index.cfm?md=doctor&tmp=snip_edit_doctor&recordNum=' + recordNum + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditDoctor").dialog('close'); },
	  ' Save ': submitForm_EditDoctor
	}
  }).dialog('open');
	
  function submitForm_EditDoctor() {
	var form = $("#frmEditDoctor");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=doctor&task=editDoctor',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
		
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditDoctor").dialog('close');
		loadDoctorList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}