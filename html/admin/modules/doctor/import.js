function uploadImport() {
  var form = document.frmDataImport;
  
  form.target = "fileUploadTargetedFrame";
  form.method="post";
  form.encoding="multipart/form-data";
  form.action='index.cfm?md=doctor&tmp=import_action';
  
  xsite.showWaitingDialog({width:450,height:400,openCallback:function() {form.submit();}});	
}

function displayMessage(msg) {
  $("#message").html('<b>Import Log:</b><br /><span class="greenlight">' + msg + '</span>');	
}