function updateIntroductionText() {
	$("#frmIntroductionText").ajaxSubmit({ 
		url: 'action.cfm?md=doctor&task=updateIntroductionText',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgIntroductionText").html('The introductory copy has been updated.');	
		  $("#frmIntroductionText div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}
}