<script type="text/javascript" src="modules/doctor/import.js"></script>

<div class="header">DOCTOR DIRECTORY</div>

<br />

<p>To update doctor directory data in the system, please upload a data file generated from the existing Hospital directory system.</p>

<form id="frmDataImport" name="frmDataImport" onsubmit="return false;">
Select a file: <input type="file" name="localFileField" size="30" value=""><br /><br />
<input type="button" value=" Upload &amp Import " onClick="uploadImport()">
</form>

<br />
<div id="message"></div>

<iframe id="fileUploadTargetedFrame" name="fileUploadTargetedFrame" src="" style="width:200px;height:20px;border:1px solid ##cccccc;display:none;"></iframe>