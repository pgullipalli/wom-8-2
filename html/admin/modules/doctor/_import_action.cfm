<cfscript>
  DD=CreateObject("component", "com.DoctorAdmin").init();
  Form.targetAbsoluteDirectory=Application.siteDirectoryRoot & "/assets_temp/";
  Form.localFileFieldName="localFileField";
  tempStruct=StructNew();
  tempStruct.importLog=DD.importDoctorData(argumentCollection=Form);
</cfscript>

<script type="text/javascript">
  <cfoutput>
  var importLogJson=#SerializeJSON(tempStruct)#;
  </cfoutput>
  window.parent.xsite.closeWaitingDialog();
  window.parent.displayMessage(importLogJson.IMPORTLOG);
</script>