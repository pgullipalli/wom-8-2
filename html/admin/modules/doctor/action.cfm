<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  DD=CreateObject("component", "com.DoctorAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";
  
  switch (URL.task) {
    case "importDoctorData":
	  FORM.targetAbsoluteDirectory=Application.siteDirectoryRoot & "/assets_temp/";
	  DD.importDoctorData(argumentCollection=Form);
	  break;
	  
	case "editDoctor":
	  DD.editDoctor(argumentCollection=Form);
	  break;
	  
	case "updateIntroductionText":
	  DD.updateIntroductionText(argumentCollection=Form);
	  break;
	  
	default:
	  break;
  }
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>