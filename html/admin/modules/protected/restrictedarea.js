$(function () {	
	loadRestrictedArea();
	
	$("#frmNavigator select[name='groupID']").change(function() {
	  $("#msg").hide();
	  loadRestrictedArea();	  
	});
});

function loadRestrictedArea() {
  var groupID=$("#frmNavigator select[name='groupID']").val();	
  
  var url="index.cfm?md=protected&tmp=snip_restrictedarea&groupID=" + groupID + "&uuid=" + xsite.getUUID();  
  xsite.load("#mainDiv", url);
}

function updateRestrictedPages() {
  var form = $("#frmPageList");
  
  xsite.showWaitingDialog({openCallback:function() {
	form.ajaxSubmit({ 
	  url: 'action.cfm?md=protected&task=updateRestrictedPages',
	  type: 'post',
	  cache: false,
	  dataType: 'json', 
	  success: processJson,
	  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});							 
												 
  }});
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  $("#msgRestrictedArea").html('The restricted area has been updated.');	
	  $("#msg").show();
	  loadRestrictedArea();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}