<script type="text/javascript" src="modules/protected/usergroups.js"></script>

<div class="header">RESTRICTED AREA &gt; User Groups</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addGroup();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editGroup();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteGroup();">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="setupRestrictedArea();">Restricted Area</a>
</div>

<br /><br /><br />

<div id="mainDiv"></div>