<cfparam name="URL.groupID">
<cfset RA=CreateObject("component", "com.RestrictedAreaAdmin").init()>
<cfset allGroups=RA.getAllGroups()>

<form id="frmAddUser" name="frmAddUser" onsubmit="return false">
<div class="row">
	<div class="col-30-right"><b>User Groups:</b><br /><small>(Check all that apply.)</small></div>
    <div class="col-68-left">
      <cfset numGroups=allGroups.recordcount>
      <cfset numRows=Ceiling(numGroups/3)>
      <cfoutput><cfset idx = 0>
      <cfloop index="row" from="1" to="#numRows#">
        <div class="row">
        <cfloop index="col" from="1" to="3">
          <cfset idx = (row - 1) * 3 + col>
          <cfif idx LTE numGroups>
            <cfset Variables.groupID=allGroups.groupID[idx]>
            <cfset Variables.groupName=allGroups.groupName[idx]>
            <div class="col-30-left">
              <input type="checkbox" name="groupIDList" value="#Variables.groupID#"<cfif URL.groupID Is Variables.groupID> checked</cfif> />
              #Variables.groupName#
            </div>
          </cfif>
        </cfloop>
        </div>
      </cfloop>
      </cfoutput>
    </div>
</div>
<div class="row">
	<div class="col-30-right"><b>Username:</b></div>
    <div class="col-68-left"><input type="text" name="username" maxlength="255" value="" style="width:200px;" validate="required:true"></div>
</div>
<div class="row">
	<div class="col-30-right"><b>Password:</b></div>
    <div class="col-68-left"><input type="text" name="password" maxlength="255" value="" style="width:200px;" validate="required:true"></div>
</div>
<div class="row">
	<div class="col-30-right">E-mail:</div>
    <div class="col-68-left"><input type="text" name="email" maxlength="255" value="" style="width:200px;"></div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="published" value="1" /></div>
    <div class="col-68-left">Publish this user</div>
</div>
</form>