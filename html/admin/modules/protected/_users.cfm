<cfparam name="URL.groupID" default="0">

<script type="text/javascript" src="modules/protected/users.js"></script>

<cfset RA=CreateObject("component", "com.RestrictedAreaAdmin").init()>
<cfset allGroups=RA.getAllGroups()>
<cfif URL.groupID IS 0>
  <cfset URL.groupID=RA.getFirstGroupID()>
  <cfif URL.groupID IS 0><!--- no categories were ever created --->
    <cflocation url="index.cfm?md=protected&tmp=usergroups&wrap=1">
  </cfif>
</cfif>

<div class="header">RESTRICTED AREA &gt; Userrs</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addUser();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editUser(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteUser();">Delete</a>
</div>

<br /><br /><br />

<cfoutput>
<form id="frmNavigator">
  User Group
  <select name="groupID" style="min-width:250px;">
    <cfloop query="allGroups">
    <option value="#allGroups.groupID#"<cfif URL.groupID IS allGroups.groupID> selected</cfif>>#allGroups.groupName#</option>
    </cfloop>
  </select>
</form>
</cfoutput>

<div id="mainDiv"></div>