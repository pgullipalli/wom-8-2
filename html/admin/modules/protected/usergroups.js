$(function () {
	loadGroupList();
});


function loadGroupList() {
  xsite.load("#mainDiv", "index.cfm?md=protected&tmp=snip_group_list&uuid=" + xsite.getUUID());
}

/*--------------------------------*/
/*    BEGIN: Add Group         */
/*--------------------------------*/
function addGroup() {
  xsite.createModalDialog({
	windowName: 'winAddGroup',
	title: 'Add User Group',
	position: 'center-up',
	width: 500,
	url: 'index.cfm?md=protected&tmp=snip_add_group&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddGroup").dialog('close'); },
	  ' Add ': submitForm_AddGroup
	}
  }).dialog('open');
  
  function submitForm_AddGroup() {
	var form = $("#frmAddGroup");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=protected&task=addGroup',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddGroup").dialog('close');
		loadGroupList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------*/
/*    END: Add Group           */
/*--------------------------------*/

/*--------------------------------*/
/*    BEGIN: Edit Group        */
/*--------------------------------*/
function editGroup() {
  var form = $("#frmGroupList")[0];
  var groupID = xsite.getCheckedValue(form.groupID);
  if (!groupID) {
	alert("Please select a user group to edit.");
	return;
  }
  xsite.createModalDialog({
	windowName: 'winEditGroup',
	title: 'Edit User Group',
	position: 'center-up',
	width: 500,
	url: 'index.cfm?md=protected&tmp=snip_edit_group&groupID=' + groupID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditGroup").dialog('close'); },
	  ' Update ': submitForm_EditGroup
	}
  }).dialog('open');
  
  function submitForm_EditGroup() {
	var form = $("#frmEditGroup");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=protected&task=editGroup',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
	  return;	
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditGroup").dialog('close');
		loadGroupList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}	  
  }
}
/*--------------------------------*/
/*    END: Edit Group          */
/*--------------------------------*/


/*--------------------------------*/
/*  BEGIN: Delete Group        */
/*--------------------------------*/
function deleteGroup() {
  var form = $("#frmGroupList")[0];
  var groupID = xsite.getCheckedValue(form.groupID);
  if (!groupID) {
	alert("Please select a user group to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected user group?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=protected&task=deleteGroup&groupID=' + groupID, {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadGroupList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }
}
/*--------------------------------*/
/*  END: Delete Group          */
/*--------------------------------*/

function setupRestrictedArea() {
  var form = $("#frmGroupList")[0];
  var groupID = xsite.getCheckedValue(form.groupID);
  if (!groupID) {
	alert("Please select an user group.");
	return;
  }

  window.location="index.cfm?md=protected&tmp=restrictedarea&wrap=1&groupID=" + groupID;
}