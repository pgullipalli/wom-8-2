<cfparam name="URL.groupID" default="0">

<script type="text/javascript" src="modules/protected/restrictedarea.js"></script>

<cfset RA=CreateObject("component", "com.RestrictedAreaAdmin").init()>
<cfset allGroups=RA.getAllGroups()>
<cfif URL.groupID IS 0>
  <cfset URL.groupID=RA.getFirstGroupID()>
  <cfif URL.groupID IS 0><!--- no categories were ever created --->
    <cflocation url="index.cfm?md=protected&tmp=usergroups&wrap=1">
  </cfif>
</cfif>

<div class="header">RESTRICTED AREA</div>

<p>
Select only the pages or sections below for which you are granting password-protected access to the selected user group.
</p>

<cfoutput>
<form id="frmNavigator">
  User Group
  <select name="groupID" style="min-width:250px;">
    <cfloop query="allGroups">
    <option value="#allGroups.groupID#"<cfif URL.groupID IS allGroups.groupID> selected</cfif>>#allGroups.groupName#</option>
    </cfloop>
  </select>
</form>
</cfoutput>

<div id="msg" class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgRestrictedArea"></span></p>
  </div>
</div>

<div id="mainDiv"></div>