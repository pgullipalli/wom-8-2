var pageNum=1;//initial page number
var showPW=0;//can be toggled to show passwords or not

$(function () {	
	loadUserList();
	
	$("#frmNavigator select[name='groupID']").change(function() {
	  pageNum=1;
	  loadUserList();	  
	});
});

function loadUserList(pNum) {
  if (pNum) pageNum = pNum;
  var groupID=$("#frmNavigator select[name='groupID']").val();	
  
  var url="index.cfm?md=protected&tmp=snip_user_list&groupID=" + groupID + "&showPW=" + showPW + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function togglePassword() {
  if (showPW) showPW = 0;
  else showPW = 1;  
  loadUserList();
}


function addUser() {
  var groupID=$("#frmNavigator select[name='groupID']").val();	
  xsite.createModalDialog({
	windowName: 'winAddUser',
	title: 'Add User',
	width: 550,
	position: 'center-up',
	url: 'index.cfm?md=protected&tmp=snip_add_user&groupID=' + groupID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddUser").dialog('close'); },
	  ' Add ': submitForm_AddUser
	}
  }).dialog('open');
  
  function submitForm_AddUser() {
	var form = $("#frmAddUser");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=protected&task=addUser',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddUser").dialog('close');
		loadUserList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editUser(itID) {
  var form = $("#frmUserList")[0];
  var userID = null;
  if (itID) userID = itID;
  else userID = xsite.getCheckedValue(form.userID);

  if (!userID) {
	alert("Please select an user to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditUser',
	title: 'Edit User',
	width: 550,
	position: 'center-up',
	url: 'index.cfm?md=protected&tmp=snip_edit_user&userID=' + userID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditUser").dialog('close'); },
	  ' Save ': submitForm_EditUser
	}
  }).dialog('open');
	
  function submitForm_EditUser() {
	var form = $("#frmEditUser");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=protected&task=editUser',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
		
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditUser").dialog('close');
		loadUserList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteUser() {
  var form = $("#frmUserList")[0];
  var userID = xsite.getCheckedValue(form.userID);
  if (!userID) {
	alert("Please select an user to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected user?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=protected&task=deleteUser&userID=' + userID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadUserList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}
