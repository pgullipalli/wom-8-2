<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  RA=CreateObject("component", "com.RestrictedAreaAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";
  
  switch (URL.task) {
    case "addGroup":
	  RA.addGroup(argumentCollection=Form);
	  break;
	  
	case "editGroup":
	  RA.editGroup(argumentCollection=Form);
	  break;
	  
	case "deleteGroup":
	  RA.deleteGroup(URL.groupID);
      break;
	  
	case "addUser":
	  RA.addUser(argumentCollection=Form);
	  break;
    
	case "editUser":
	  RA.editUser(argumentCollection=Form);
	  break;
	  
	case "deleteUser":
	  RA.deleteUser(URL.userID);
	  break;
	
	case "updateRestrictedPages":
	  RA.updateRestrictedPages(argumentCollection=Form);
	  break;
	
	default:
	  break;
  }
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>