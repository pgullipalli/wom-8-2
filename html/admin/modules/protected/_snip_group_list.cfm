<cfset RA=CreateObject("component", "com.RestrictedAreaAdmin").init()>
<cfset groupSummary=RA.getGroupSummary()>

<cfif groupSummary.recordcount IS 0>
  <p>
  It appears that there are no user groups created.
  Please click on the 'New' button in the action bar above to create the first user group.
  </p>
<cfelse>
<form id="frmGroupList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="250">Group Name</th>
    <th width="100"># of Users</th>
    <th>&nbsp;</th>
  </tr>
  <cfoutput query="groupSummary">
  <tr>
    <td align="center"><input type="radio" name="groupID" value="#groupID#"></td>
    <td><a href="index.cfm?md=protected&tmp=users&wrap=1&groupID=#groupID#">#groupName#</a></td>
    <td align="center"><a href="index.cfm?md=protected&tmp=users&wrap=1&groupID=#groupID#">#numUsers#</a></td>
    <td align="center">&nbsp;</td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>