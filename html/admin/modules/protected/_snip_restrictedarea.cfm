<cfparam name="URL.groupID">
<cfset RA=CreateObject("component", "com.RestrictedAreaAdmin").init()>
<cfset protectedURLIndicstorList=RA.getProtectedURLIndicatorListByGroupID(URL.groupID)>

<form id="frmPageList" name="frmPageList">
<cfoutput><input type="hidden" name="groupID" value="#URL.groupID#"></cfoutput>
<div class="subHeader">[PAGE BUILDER PAGES]</div>
<cfset PB=CreateObject("component", "com.PageBuilderAdmin").init()>
<cfset allPages=PB.getAllPages()>   
<cfset allCats=PB.getAllPageCategories()>

<cfif allCats.recordcount GT 0>
    <div class="row">
    <cfoutput query="allCats">
      <div class="col-24-left"><input type="checkbox" name="pageURLIndicator" value="pagebuilder|home|catid|#pageCategoryID#" <cfif ListFind(protectedURLIndicstorList,"pagebuilder|home|catid|#pageCategoryID#") GT 0>checked</cfif> /> #pageCategoryName#</div>
    </cfoutput>
    </div>
<cfelse>
    <div class="row">
    No pages have been created in this area.
    </div>
</cfif>

<!--- <cfif allPages.recordcount GT 0>
	<cfset tempPageCategoryID=0>
    <div class="row">
    <cfoutput query="allPages">
      <cfif Compare(pageCategoryID,tempPageCategoryID)><!--- new category; new row --->
      </div>
      <div class="row">
        <div class="col-95-left"><b>#pageCategoryName#</b></div>
      </cfif>
      <div class="col-24-left"><input type="checkbox" name="pageURLIndicator" value="pagebuilder|home|pid|#pageID#" <cfif ListFind(protectedURLIndicstorList,"pagebuilder|home|pid|#pageID#") GT 0>checked</cfif> /> #pageName#</div>
      <cfset tempPageCategoryID=pageCategoryID>
    </cfoutput>
    </div>
<cfelse>
    <div class="row">
    No pages have been created in this area.
    </div>
</cfif> --->


<br style="clear:both;"><br />
<div class="subHeader">[INFORMATION LIBRARY PAGES]</div>

<cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>
<cfset allCats=NR.getAllArticleCategories()>
<cfif allCats.recordcount GT 0>
    <div class="row">
    <cfoutput query="allCats">
      <div class="col-24-left"><input type="checkbox" name="pageURLIndicator" value="newsroom|category|catid|#articleCategoryID#" <cfif ListFind(protectedURLIndicstorList,"newsroom|category|catid|#articleCategoryID#") GT 0>checked</cfif> /> #articleCategory#</div>
    </cfoutput>
    </div>
<cfelse>
    <div class="row">
    No pages have been created in this area.
    </div>
</cfif>

<br style="clear:both;"><br />
<div class="subHeader">[EVENT MANAGER]</div>

<cfset EV=CreateObject("component", "com.EventAdmin").init()>
<cfset allCats=EV.getAllCategories()>
<cfif allCats.recordcount GT 0>
    <div class="row">
    <cfoutput query="allCats">
      <div class="col-24-left"><input type="checkbox" name="pageURLIndicator" value="event|category|catid|#categoryID#" <cfif ListFind(protectedURLIndicstorList,"event|category|catid|#categoryID#") GT 0>checked</cfif> /> #categoryName#</div>
    </cfoutput>
    </div>
<cfelse>
    <div class="row">
    No pages have been created in this area.
    </div>
</cfif>

<br style="clear:both;"><br />
<div class="subHeader">[FORMS]</div>

<cfset FM=CreateObject("component", "com.FormAdmin").init()>
<cfset allForms=FM.getAllFormNames()>
<cfif allForms.recordcount GT 0>
<div class="row">
	<cfoutput query="allForms">
      <div class="col-24-left"><input type="checkbox" name="pageURLIndicator" value="form|home|fmid|#formID#" <cfif ListFind(protectedURLIndicstorList,"form|home|fmid|#formID#") GT 0>checked</cfif> /> #formName#</div>
    </cfoutput>
    </div>
<cfelse>
    <div class="row">
    No pages have been created in this area.
    </div>
</cfif>

<br style="clear:both;"><br />
<div class="subHeader">[E-MAIl LIST SIGN-UP FORMS]</div>

<cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
<cfset allSignupForms=CM.getSignUpFormsSummary()>
<cfif allSignupForms.recordcount GT 0>
    <div class="row">
    <cfoutput query="allSignupForms">
      <div class="col-24-left"><input type="checkbox" name="pageURLIndicator" value="communication|signup|sfmid|#signUpFormID#" <cfif ListFind(protectedURLIndicstorList,"communication|signup|sfmid|#signUpFormID#") GT 0>checked</cfif> /> #signupFormName#</div>
    </cfoutput>
    </div>
<cfelse>
    <div class="row">
    No pages have been created in this area.
    </div>
</cfif>


<!--- <br style="clear:both;"><br />
<div class="subHeader">[PHOTO GALLERY PAGES]</div>

<cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
<cfset allCats=PG.getAllAlbumCategories()>
<cfif allCats.recordcount GT 0>
    <div class="row">
    <cfoutput query="allCats">
      <div class="col-24-left"><input type="checkbox" name="pageURLIndicator" value="photogallery|category|catid|#albumCategoryID#" <cfif ListFind(protectedURLIndicstorList,"photogallery|category|catid|#albumCategoryID#") GT 0>checked</cfif> /> #albumCategory#</div>
    </cfoutput>
    </div>
<cfelse>
    <div class="row">
    No pages have been created in this area.
    </div>
</cfif> --->


<br style="clear:both;" /><br />
<div class="row">
  <a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="updateRestrictedPages();">Submit</a>
</div>
</form>