<cfparam name="URL.groupID">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">
<cfparam name="URL.showPW" default="0">

<cfset RA=CreateObject("component", "com.RestrictedAreaAdmin").init()>

<cfset structItems=RA.getUsersByGroupID(groupID=URL.groupID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
<cfif structItems.numAllItems IS 0>
<p>
It appears that there are no users created in this category.
Please click on the 'New' button in the action bar above to create the first user.
</p>
</cfif>

<cfif structItems.numDisplayedItems GT 0>

<p align="center"><a href="javascript:togglePassword();" class="noUnderline">[ Show/Hide Passwords ]</a></p>

<form id="frmUserList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
	<th>Username</th>
    <cfif URL.showPW>
	<th>Password</th>
	</cfif>
    <th>E-mail</th>
    <th width="100">Publish?</th>
  </tr>  
  <cfoutput query="structItems.users">
  <tr align="center">
    <td><input type="radio" name="userID" value="#userID#"></td>
	<td><a href="##" onclick="editUser('#userID#')">#username#</a></td> 
    <cfif URL.showPW>
	<td>#password#</td>
	</cfif>
    <td>#email#&nbsp;</td>
    <td><cfif published>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif structItems.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structItems.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadUserList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>