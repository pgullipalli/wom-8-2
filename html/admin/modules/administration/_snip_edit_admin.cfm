<cfparam name="URL.administratorID">

<cfset Util=CreateObject("component", "com.Utility").init()>
<cfset AMM=CreateObject("component", "com.AccessAdmin").init()>
<cfset adminInfo=AMM.getAdministratorBasicInfo(URL.administratorID)>
<cfset adminPrivilegeInfo=AMM.getAdministratorPrivilegeInfo(URL.administratorID)>

<cfoutput query="adminInfo">
<form id="frmEditAdmin" onsubmit="return false;">
<input type="hidden" name="administratorID" value="#URL.administratorID#">
<table border="0" cellpadding="3" cellspacing="0" width="90%" align="center">
  <tr valign="top">
	<td width="25%">
	  First Name<br />
	  <input type="text" name="firstName" maxlength="30" style="width:100px;" value="#HTMLEditFormat(firstName)#">
	</td>
	<td width="25%">
	  Last Name<br />
	  <input type="text" name="lastName" maxlength="30" style="width:100px;" value="#HTMLEditFormat(lastName)#">
	</td>
	<td>
	  E-Mail Address<br />
	  <input type="text" name="email" maxlength="100" style="width:150px;" value="#HTMLEditFormat(email)#">
	</td>
  </tr>
  <tr valign="top">
	<td>
	  <b>Username</b><br />
	  <input type="text" name="username" maxlength="30" style="width:100px;" validate="required:true" value="#HTMLEditFormat(username)#">
    </td>
	<td colspan="2">
	  <b>Password</b><br />
	  <input type="text" name="password" maxlength="30" style="width:100px;" validate="required:true" value="#HTMLEditFormat(password)#">
    </td>
  </tr>
  <tr valign="top">
	<td colspan="3">
	  <input type="checkbox" name="isSuperUser" value="1" onclick="xsite.toggle('divPrivilege2', this, true)"<cfif isSuperUser> checked</cfif>> This is a master administrator
	</td>
  </tr>
</table>
</cfoutput>
<div id="divPrivilege2" style="display:<cfif adminInfo.isSuperUser>none<cfelse>block</cfif>;">
<hr size="1" width="90%" noshade="true">
<br />
<table border="0" cellpadding="3" cellspacing="0" width="90%" align="center">
  <tr valign="top">
	<td colspan="2"><b>Please select access privileges for this administrator.</b></td>
  </tr>
  <cfset privilegeGroupArray=Application.privilegeXMLObj.XmlRoot["privilege-group"]>

  <cfoutput>
	<cfset idx = 0>
	<cfloop index="idx1" from="1" to="#arrayLen(privilegeGroupArray)#">
	  <cfset idx = idx1>
	  <cfif idx MOD 2 IS 1>
	  <tr valign="top">	      
	  </cfif>
	  <td width="50%">
	    <cfset privilegeGroupID=privilegeGroupArray[idx1].XmlAttributes.id>
        <cfset checked=0>
		<cfloop query="adminPrivilegeInfo">
		  <cfif VARIABLES.privilegeGroupID IS adminPrivilegeInfo.privilegeGroupID>
			<cfset checked=1>
			<cfbreak>
		  </cfif>
		</cfloop>
		<input type="checkbox" name="privilegeGroupID" value="#privilegeGroupID#"<cfif checked> checked</cfif>> #privilegeGroupArray[idx1].XmlAttributes.name#<br>
		<cfset privilegeArray=privilegeGroupArray[idx1].privilege>
		<cfloop index="idx2" from="1" to="#arrayLen(privilegeArray)#">
		  <cfset privilegeObj=privilegeArray[idx2]>
		  <cfset privilegeID=privilegeObj.XmlAttributes.id> 
		  <cfset privilegeName=privilegeObj.XmlAttributes.name>
		  <img src="images/spacer.gif" width="25" height="10">
          <cfset checked=0>
		  <cfloop query="adminPrivilegeInfo">
		    <cfif VARIABLES.privilegeGroupID IS adminPrivilegeInfo.privilegeGroupID AND VARIABLES.privilegeID IS adminPrivilegeInfo.privilegeID>
			  <cfset checked=1>
			  <cfbreak>
			</cfif>
		  </cfloop>
		  <input type="checkbox" name="privilegeID_#privilegeGroupID#" value="#privilegeID#"<cfif checked> checked</cfif>> #privilegeName#<br>		  
		  
		  <cfif IsDefined("privilegeObj.module.XmlAttributes.categoryrestriction") AND privilegeObj.module.XmlAttributes.categoryrestriction>
		    <cfset moduleName=privilegeObj.module.XmlAttributes.name>
		    <img src="images/spacer.gif" width="50" height="10">
		    Accessible Categories:<br>
		    <img src="images/spacer.gif" width="50" height="10">
		    <cfset allCategories=Util.getCategoriesByModuleName(moduleName)>
            <cfset checkAllCategories=0>						
			<cfloop query="adminPrivilegeInfo">
		      <cfif VARIABLES.privilegeGroupID IS adminPrivilegeInfo.privilegeGroupID AND VARIABLES.privilegeID IS adminPrivilegeInfo.privilegeID>
			    <cfif ListFind(adminPrivilegeInfo.categoryIDList, "-1") IS NOT 0>
				  <cfset checkAllCategories=1>
				</cfif>
			  </cfif>
		    </cfloop>
		    <input type="checkbox" name="categoryID_#privilegeGroupID#_#privilegeID#" value="-1"<cfif checkAllCategories> checked</cfif>> All Categories<br>
		    <cfloop query="allCategories">
		      <img src="images/spacer.gif" width="50" height="10">
              <cfset checked=0>
			  <cfif NOT checkAllCategories>
			    <cfset categoryIDTemp=allCategories.catID>
			    <cfloop query="adminPrivilegeInfo">
				  <cfif VARIABLES.privilegeGroupID IS adminPrivilegeInfo.privilegeGroupID AND VARIABLES.privilegeID IS adminPrivilegeInfo.privilegeID>
				    <cfif ListFind(adminPrivilegeInfo.categoryIDList, "#categoryIDTemp#") IS NOT 0>
				      <cfset checked=1>
				    </cfif>
				  </cfif>
				</cfloop>
			  </cfif>
		      <input type="checkbox" name="categoryID_#privilegeGroupID#_#privilegeID#" value="#catID#"<cfif checked> checked</cfif>> #catName#<br>
		    </cfloop>
		  </cfif>	  
		</cfloop>
	  </td>
	  <cfif idx MOD 2 IS 0>
	  </tr>
	  </cfif>	
	</cfloop>
	<cfif idx MOD 2 IS 1>
	    <td width="50%">&nbsp;</td>
	  </tr>
	</cfif>	
	</cfoutput>
</table>
</div>
</form>







