<cfparam name="URL.showPW" default="0">
<cfset AMM=CreateObject("component", "com.AccessAdmin").init()>

<cfset allAdministrators=AMM.getAllAdministrators()>

<cfif allAdministrators.recordcount Is 0>
  <p>
  It appears that there doesn't exist any administrators. 
  Please click on the 'New' button in the action bar above to create an administrator.
  </p>
<cfelse>
<form id="frmAdminList" onsubmit="return false;">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th>Username</th>
	<cfif URL.showPW>
	<th>Password</th>
	</cfif>
	<th>Name</th>
	<th>E-Mail</th>
	<th width="150">Master Administrator</th>
  </tr>
  <cfoutput query="allAdministrators">
  <tr align="center">
    <td><input type="radio" name="administratorID" value="#administratorID#"></td>
	<td>#username#</td>
	<cfif URL.showPW>
	<td>#password#</td>
	</cfif>
	<td>#firstName# #lastName#</td>
    <td>#email#</td>
	<td><cfif isSuperUser>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>