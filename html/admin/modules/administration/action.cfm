<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cffunction name="logout" access="private" output="no">
  <cflock scope="session" type="exclusive" timeout="30">
    <cfset StructDelete(Session, "access")>
	<cfset Session.access=CreateObject("component", "com.Access").init()>
  </cflock>
</cffunction>

<cfscript>
  AM=CreateObject("component", "com.AccessAdmin").init();
  
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=false;
  returnedStruct.MESSAGE="";

  switch (URL.task) {
    case "addAdmin":
	  returnedStruct=AM.addAdmin(argumentCollection=Form);
	  break;
	
	case  "editAdmin":
	  returnedStruct=AM.editAdmin(argumentCollection=Form);
	  break;
	  
	case "verifyLogin":
	  if (Session.access.verifyLogin(argumentCollection=Form)) {
        returnedStruct.SUCCESS=true;
	  } else {
        returnedStruct.MESSAGE="The username or password you entered is invalid.";
	  }
	  break;
	  
	case "login":
	  if (Session.access.login(argumentCollection=Form)) {
	    returnedStruct.SUCCESS=true;
	  } else {
	    returnedStruct.MESSAGE="The username or password you entered is invalid.";
	  }
	  break;
	  
	case "deleteAdmin":
	  AM.deleteAdmin(URL.administratorID);
	  returnedStruct.SUCCESS=true;
	  break;
	  
	case "logout":
	  logout();
	  UT=CreateObject("component", "com.Utility").init();
	  UT.location("/admin/login.html");
	  break;
	  
	case "checkLogin":
	  returnedStruct.SUCCESS=Session.access.isLoggedIn;
	  break;
	  
	default:
	  break;
  }
</cfscript>

<cfoutput>#Trim(SerializeJSON(returnedStruct))#</cfoutput>
</cfsetting>