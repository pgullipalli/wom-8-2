<div class="header">ADMINISTRATION &gt; Administrators</div>

<script src="modules/administration/home.js"></script>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addAdmin();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editAdmin();">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteAdmin();">Delete</a>
</div>

<p align="center"><a href="javascript:togglePassword();" class="noUnderline">[ Show/Hide Passwords ]</a></p>

<div id="mainDiv"></div>
