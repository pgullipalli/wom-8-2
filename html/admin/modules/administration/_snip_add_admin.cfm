<cfset Util=CreateObject("component", "com.Utility").init()>
<form id="frmAddAdmin" onsubmit="return false;">
<table border="0" cellpadding="3" cellspacing="0" width="90%" align="center">
  <tr valign="top">
	<td width="25%">
	  First Name<br />
	  <input type="text" name="firstName" value="" maxlength="30" style="width:100px;">
	</td>
	<td width="25%">
	  Last Name<br />
	  <input type="text" name="lastName" value="" maxlength="30" style="width:100px;">
	</td>
	<td>
	  E-Mail Address<br />
	  <input type="text" name="email" value="" maxlength="100" style="width:150px;">
	</td>
  </tr>
  <tr valign="top">
	<td>
	  <b>Username</b><br />
	  <input type="text" name="username" value="" maxlength="30" style="width:100px;" validate="required:true">
    </td>
	<td colspan="2">
	  <b>Password</b><br />
	  <input type="text" name="password" value="" maxlength="30" style="width:100px;" validate="required:true">
    </td>
  </tr>
  <tr valign="top">
	<td colspan="3">
	  <input type="checkbox" name="isSuperUser" value="1" onclick="xsite.toggle('divPrivilege', this, true)" checked> This is a master administrator
	</td>
  </tr>
</table>
<div id="divPrivilege" style="display:none;">
<hr size="1" width="90%" noshade="true">
<br />
<table border="0" cellpadding="3" cellspacing="0" width="90%" align="center">
  <tr valign="top">
	<td colspan="2"><b>Please select access privileges for this administrator.</b></td>
  </tr>
  <cfset privilegeGroupArray=Application.privilegeXMLObj.XmlRoot["privilege-group"]>
  <cfoutput>
	<cfset idx = 0>
	<cfloop index="idx1" from="1" to="#arrayLen(privilegeGroupArray)#">
	  <cfset idx = idx1>
	  <cfif idx MOD 2 IS 1>
	  <tr valign="top">	      
	  </cfif>
	  <td width="50%">
	    <cfset privilegeGroupID=privilegeGroupArray[idx1].XmlAttributes.id>
		<input type="checkbox" name="privilegeGroupID" value="#privilegeGroupID#"> #privilegeGroupArray[idx1].XmlAttributes.name#<br>
		<cfset privilegeArray=privilegeGroupArray[idx1].privilege>
		<cfloop index="idx2" from="1" to="#arrayLen(privilegeArray)#">
		  <cfset privilegeObj=privilegeArray[idx2]>
		  <cfset privilegeID=privilegeObj.XmlAttributes.id> 
		  <cfset privilegeName=privilegeObj.XmlAttributes.name>
		  <img src="images/spacer.gif" width="25" height="10">
		  <input type="checkbox" name="privilegeID_#privilegeGroupID#" value="#privilegeID#"> #privilegeName#<br>		  
		  
		  <cfif IsDefined("privilegeObj.module.XmlAttributes.categoryrestriction") AND privilegeObj.module.XmlAttributes.categoryrestriction>
		    <cfset moduleName=privilegeObj.module.XmlAttributes.name>
		    <img src="images/spacer.gif" width="50" height="10">
		    Accessible Categories:<br>
		    <img src="images/spacer.gif" width="50" height="10">
		    <cfset allCategories=Util.getCategoriesByModuleName(moduleName)>
		    <input type="checkbox" name="categoryID_#privilegeGroupID#_#privilegeID#" value="-1"> All Categories<br>
		    <cfloop query="allCategories">
		    <img src="images/spacer.gif" width="50" height="10">
		    <input type="checkbox" name="categoryID_#privilegeGroupID#_#privilegeID#" value="#catID#"> #catName#<br>
		    </cfloop>
		  </cfif>	  
		</cfloop>
	  </td>
	  <cfif idx MOD 2 IS 0>
	  </tr>
	  </cfif>	
	</cfloop>
	<cfif idx MOD 2 IS 1>
	    <td width="50%">&nbsp;</td>
	  </tr>
	</cfif>	
	</cfoutput>
</table>
</div>
</form>