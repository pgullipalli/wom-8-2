var showPW=0;//can be toggled to show passwords or not

$(function() {
	loadAdminList();
});


function loadAdminList() {
  xsite.load("#mainDiv", "index.cfm?md=administration&tmp=snip_admin_list&showPW=" + showPW + "&uuid=" + xsite.getUUID());
}

function togglePassword() {
  if (showPW) showPW = 0;
  else showPW = 1;  
  loadAdminList();
}

function addAdmin() {
  xsite.createModalDialog({
	windowName: 'winAddAdmin',
	title: 'Add Administrator',
	position: 'center-up',
	width: 800,
	url: 'index.cfm?md=administration&tmp=snip_add_admin&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddAdmin").dialog('close'); },
	  ' Add ': submitForm_AddAdmin
	}
  }).dialog('open');
  
  function submitForm_AddAdmin() {
	var form = $("#frmAddAdmin");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=administration&task=addAdmin',
		  type: 'post',
		  cache: false,
		  beforeSubmit: validate,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function validate() {
	  var form = $("#frmAddAdmin")[0];
	  if (checkSubAdminPrivileges(form)) return true;
	  return false;
	}
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddAdmin").dialog('close');
		loadAdminList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
	
  }
}

function editAdmin() {
  var form = $("#frmAdminList")[0];
  var administratorID = xsite.getCheckedValue(form.administratorID);
  if (!administratorID) {
	alert("Please select an administrator to edit.");
	return;	  
  }

  xsite.createModalDialog({
	windowName: 'winEditAdmin',
	title: 'Edit Administrator',
	position: 'center-up',
	width: 800,
	url: 'index.cfm?md=administration&tmp=snip_edit_admin&administratorID=' + administratorID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditAdmin").dialog('close'); },
	  ' Save ': submitForm_EditAdmin
	}
  }).dialog('open');
  
  function submitForm_EditAdmin() {
	var form = $("#frmEditAdmin");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=administration&task=editAdmin',
		  type: 'post',
		  cache: false,
		  beforeSubmit: validate,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function validate() {
	  var form = $("#frmEditAdmin")[0];
	  if (checkSubAdminPrivileges(form)) return true;
	  return false;
	}
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditAdmin").dialog('close');
		loadAdminList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }		
}

function deleteAdmin() {
  var form = $("#frmAdminList")[0];
  var administratorID = xsite.getCheckedValue(form.administratorID);
  if (!administratorID) {
	alert("Please select an administrator to delete.");
	return;	  
  }
  
  if (!confirm('Are you sure you want to delete the selected administrator?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {
		$.get('action.cfm?md=administration&task=deleteAdmin&administratorID=' + administratorID, {}, processJson, 'json');}
	});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadAdminList();
	} else {
	  xsite.closeWaitingDialog();
	}
  }			
}

function checkSubAdminPrivileges(form) {
  if (!form.isSuperUser.checked) {
	  var found = false;
	  if (form.privilegeGroupID.length) {
		var i = 0;
		for (i = 0; i < form.privilegeGroupID.length; i++) {
		  if (form.privilegeGroupID[i].checked) {
			found = true;
			found2 = false;
			var obj=eval("form.privilegeID_" + form.privilegeGroupID[i].value);
			if (obj.length) {
			  var j = 0;
			  for (j=0; j < obj.length; j++) {
				if (obj[j].checked) found2 = true;
			  }
			} else {
			  if (obj.checked) found2 = true;
			}
			if (!found2) {
			  alert ("Please select at least an access privilege for all access privilege groups you have selected.");
			  return false;
			}
		  }
		}	  
	  } else {
		if (form.privilegeGroupID.checked) found = true;
	  }	
	  if (!found) {
		alert ("Please select at least an access privilege group for this administrator.");
		return false;
	  }
	}
	
  return true;
}