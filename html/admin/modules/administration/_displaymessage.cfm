<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.ajax" default="0">
<cfif Not URL.ajax>
  <cfset displayedMessage=Session.access.getMessage(1)>
<cfelse>
  <cfset returnedStruct=StructNew()>
  <cfset returnedStruct.success=false>
  <cfset returnedStruct.message=Session.access.getMessage(1)>
</cfif>

<cfif URL.ajax>
  <cfoutput>#Trim(SerializeJSON(returnedStruct))#</cfoutput>
<cfelse>
  <cfoutput><br />#displayedMessage#</cfoutput>
</cfif>
