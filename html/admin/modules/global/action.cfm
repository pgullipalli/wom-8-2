<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  GP=CreateObject("component", "com.GlobalAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";
  
  switch (URL.task) {
    case "updateTitle":
	  GP.updateTitle(argumentCollection=Form);
	  break;
	  
	case "updateDescription":
	  GP.updateDescription(argumentCollection=Form);
	  break;
	  
	case "updateKeywords":
	  GP.updateKeywords(argumentCollection=Form);
	  break;  
	  
	case "updateWebmasterEmail":
	  GP.updateWebmasterEmail(argumentCollection=Form);
	  break;
	
	case "updatePrimaryNavs":
	  GP.updatePrimaryNavs(argumentCollection=Form);
	  break;

	case "updateTopNavs":
	  GP.updateTopNavs(argumentCollection=Form);
	  break;
	  
	case "updateLeftSecondaryNavs":
	  GP.updateLeftSecondaryNavs(argumentCollection=Form);
	  break;
	  
	case "updateDonateNowCopy":
	  GP.updateDonateNowCopy(argumentCollection=Form);
	  break;
	  
	case "updateFooter":
	  GP.updateFooter(argumentCollection=Form);
	  break;
	  
	default:
	  break;
  }
</cfscript>
<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>