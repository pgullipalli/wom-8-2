function updateTitle() {
	$("#frmPageTitle").ajaxSubmit({ 
		url: 'action.cfm?md=global&task=updateTitle',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgPageTitle").html('The default page title has been updated.');	
		  $("#frmPageTitle div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}
}

function updateDescription() {
	$("#frmDescription").ajaxSubmit({ 
		url: 'action.cfm?md=global&task=updateDescription',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgDescription").html('The default page description has been updated.');	
		  $("#frmDescription div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}
}

function updateKeywords() {
	$("#frmKeywords").ajaxSubmit({ 
		url: 'action.cfm?md=global&task=updateKeywords',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgDescription").html('The default page keywords has been updated.');	
		  $("#frmDescription div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}
}

function updateWebmasterEmail() {
	$("#frmWebmasterEmail").ajaxSubmit({ 
		url: 'action.cfm?md=global&task=updateWebmasterEmail',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgWebmasterEmail").html('The webmaster email has been updated.');	
		  $("#frmWebmasterEmail div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}

}

function updatePrimaryNavs() {
	$("#frmPrimaryNavs").ajaxSubmit({ 
		url: 'action.cfm?md=global&task=updatePrimaryNavs',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgPrimaryNavs").html('The primary navigation items have been updated.');	
		  $("#frmPrimaryNavs div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}	
}

function updateTopNavs() {
	$("#frmTopNavs").ajaxSubmit({ 
		url: 'action.cfm?md=global&task=updateTopNavs',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgTopNavs").html('The top navigation items have been updated.');	
		  $("#frmTopNavs div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}	
}

function updateLeftSecondaryNavs() {
	$("#frmLeftSecondaryNavs").ajaxSubmit({ 
		url: 'action.cfm?md=global&task=updateLeftSecondaryNavs',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgLeftSecondaryNavs").html('The left secondary navigation items have been updated.');	
		  $("#frmLeftSecondaryNavs div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}	
}

function updateDonateNowCopy() {
	$("#frmDonateNowCopy").ajaxSubmit({ 
		url: 'action.cfm?md=global&task=updateDonateNowCopy',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgDonateNowCopy").html('The "Donate Now" copy has been updated.');	
		  $("#frmDonateNowCopy div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}
}

function updateFooter() {
	$("#frmFooter").ajaxSubmit({ 
		url: 'action.cfm?md=global&task=updateFooter',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgFooter").html('The footer has been updated.');	
		  $("#frmFooter div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}
}