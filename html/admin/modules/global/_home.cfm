<script type="text/javascript" src="modules/global/home.js"></script>

<cfset GP=CreateObject("component", "com.GlobalAdmin").init()>
<cfset globalProperties = GP.getGlobalProperties()>
<cfset NV=CreateObject("component", "com.NavigationAdmin").init()>
<cfset manNavs=NV.getSubNavsByParentNavID(navGroupID=1, parentNavID=0)>

<div class="header">GLOBAL PROPERTIES</div>

<form id="frmPageTitle" onSubmit="return false;">
<div class="subHeader">Default Page Title</div>  
      
<p>This is the default page title that will be used to display on top of the browser window if a page title was not specified for the particular page.</p>

<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgPageTitle"></span></p>
  </div>
</div>

<strong>Title:</strong><br />
<cfoutput><input type="text" name="title" maxlength="150" size="60" value="#HTMLEditFormat(globalProperties.title)#"></cfoutput><br />
<input type="button" value=" Update " onclick="updateTitle();">
</form>

<br />

<form id="frmDescription" onSubmit="return false;">
<div class="subHeader">Default Page Description &amp; Keywords</div>      
      
<p>This is the default page description &amp; keywords in the 'head' (meta tags) area of your web pages.  All meta tags are invisible as a site visitor looks at your website, but meta tags can be exposed to crawler-based search engines to allow them to more accurately list your site in their indexes.
</p>

<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgDescription"></span></p>
  </div>
</div>

<strong>Description:</strong><br />
<cfoutput><textarea name="description" cols="60" rows="3">#HTMLEditFormat(globalProperties.description)#</textarea></cfoutput><br />
<input type="button" value=" Update " onclick="updateDescription();">
</form>

<br />

<form id="frmKeywords" onSubmit="return false;">
<strong>Keywords:</strong><br />
<cfoutput><textarea name="keywords" cols="60" rows="3">#HTMLEditFormat(globalProperties.keywords)#</textarea></cfoutput><br />
<input type="button" value=" Update " onclick="updateKeywords();">
</form>

<br />

<form id="frmPrimaryNavs" onSubmit="return false;">
<div class="subHeader">Primary Navigation:</div>

<p>Select the navigation items that will appear on the left of every page.</p>

<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgPrimaryNavs"></span></p>
  </div>
</div>

<div style="width:400px;height:120px;overflow:scroll;">
<cfoutput query="manNavs">
  <input type="checkbox" name="primaryNavIDList" value="#navID#" <cfif ListFind(globalProperties.primaryNavIDList,manNavs.navID) GT 0>checked</cfif> /> #navName#<br />
</cfoutput>
</div>

<input type="button" value=" Update " onclick="updatePrimaryNavs();">
</form>

<br />

<form id="frmTopNavs" onSubmit="return false;">
<div class="subHeader">Top Navigation:</div>

<p>Select the navigation items that will appear on the top of every page.</p>

<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgTopNavs"></span></p>
  </div>
</div>

<div style="width:400px;height:200px;overflow:scroll;">
<cfoutput query="manNavs">
  <input type="checkbox" name="topNavIDList" value="#navID#" <cfif ListFind(globalProperties.topNavIDList,manNavs.navID) GT 0>checked</cfif> /> #navName#<br />
</cfoutput>
</div>

<input type="button" value=" Update " onclick="updateTopNavs();">
</form>

<br />

<form id="frmLeftSecondaryNavs" onSubmit="return false;">
<div class="subHeader">Left Secondary Navigation:</div>

<p>Select the navigation items that will appear in the 'Woman's for...' section.</p>

<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgLeftSecondaryNavs"></span></p>
  </div>
</div>

<div style="width:400px;height:200px;overflow:scroll;">
<cfoutput query="manNavs">
  <input type="checkbox" name="leftSecondaryNavIDList" value="#navID#" <cfif ListFind(globalProperties.leftSecondaryNavIDList,manNavs.navID) GT 0>checked</cfif> /> #navName#<br />
</cfoutput>
</div>

<input type="button" value=" Update " onclick="updateLeftSecondaryNavs();">
</form>


<br />

<form id="frmDonateNowCopy" name="frmDonateNowCopy" onSubmit="return false;">
<div class="subHeader">'Donate Now' Copy:</div>

<p>Enter the text that will display below the 'Donate Now' button.</p>

<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgDonateNowCopy"></span></p>
  </div>
</div>


<CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmDonateNowCopy"
            fieldName="donateNowCopy"
            cols="60"
            rows="3"
            HTMLContent="#globalProperties.donateNowCopy#"
            displayHTMLSource="true"
            editorHeader="Edit 'Donate Now' Copy">
<input type="button" value=" Update " onclick="updateDonateNowCopy();">
</form>

<br />

<form id="frmFooter" name="frmFooter" onSubmit="return false;">
<div class="subHeader">Page Footer</div>      
      
<p>The footer is the area at the bottom of the page.</p>

<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgFooter"></span></p>
  </div>
</div>

<strong>Footer:</strong>
<CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmFooter"
            fieldName="footer"
            cols="60"
            rows="3"
            HTMLContent="#globalProperties.footer#"
            displayHTMLSource="true"
            editorHeader="Edit Page Footer">
<input type="button" value=" Update " onclick="updateFooter();">
</form>

<br />

<form id="frmWebmasterEmail" onSubmit="return false;">

<div class="ui-widget" style="display:none; margin:10px 0px; width:350px;">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><span id="msgWebmasterEmail"></span></p>
  </div>
</div>

<strong>Webmaster Email Address:</strong><br />
<cfoutput><input type="text" name="webmasterEmail" maxlength="150" size="60" value="#HTMLEditFormat(globalProperties.webmasterEmail)#"></cfoutput><br />
<input type="button" value=" Update " onclick="updateWebmasterEmail();">
</form>

</form>