<cfparam name="URL.reportYear">

<cfset VM=CreateObject("component", "com.Marketing").init()>
<cfset report=VM.getStats(URL.reportYear)>

<div class="itemList">
<table>
  <tr>
	<!--- <th width="150">Year</th> --->
    <th width="150">Month</th>
    <th width="200"># Pages E-mailed</th>
	<th width="200"># Pages Printed</th>
    <th>&nbsp;</th>
  </tr>
  <cfoutput query="report">
  <tr align="center">
	<!--- <td>#URL.reportYear#</td> --->
    <td>#DateFormat(CreateDate(URL.reportYear,reportMonth,1),"MMMM")#</td>
    <td>#numPagesEmailed#</td>
    <td>#numPagesPrinted#</td>
    <td>&nbsp;</td>
  </tr>
  </cfoutput>
</table>
</div>
