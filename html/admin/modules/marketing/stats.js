$(function () {	
	loadReport();
	
	$("#frmNavigator select[name='reportYear']").change(function() {
	  loadReport();	  
	});
});

function loadReport() {
  var reportYear=$("#frmNavigator select[name='reportYear']").val();	
  
  var url="index.cfm?md=marketing&tmp=snip_stats_list&reportYear=" + reportYear + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}
