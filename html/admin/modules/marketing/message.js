function updateEmailMessage() {
	$("#frmEmailMessage").ajaxSubmit({ 
		url: 'action.cfm?md=marketing&task=updateEmailMessage',
		type: 'post',
		cache: false,
		dataType: 'json', 
		success: processJson,
		error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
	
	function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  $("#msgEmailMessage").html('The e-mail message has been updated.');	
		  $("#frmEmailMessage div").show();
		} else {
		  alert (jsonData.MESSAGE);
		}
	}
}
