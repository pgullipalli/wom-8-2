var pageNum=1;//initial page number

$(function () {
	loadMapList();
});


function loadMapList(pNum) {
  if (pNum) pageNum = pNum;
  xsite.load("#mainDiv", "index.cfm?md=directory&tmp=snip_map_list" + "&pageNum=" + pageNum +"&uuid=" + xsite.getUUID());
}

/*--------------------------------*/
/*    BEGIN: Add Map              */
/*--------------------------------*/
function addMap() {
  xsite.createModalDialog({
	windowName: 'winAddMap',
	title: 'Add Map',
	position: 'center-up',
	width: 600,
	url: 'index.cfm?md=directory&tmp=snip_add_map&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddMap").dialog('close'); },
	  ' Add ': submitForm_AddMap
	}
  }).dialog('open');
  
  function submitForm_AddMap() {
	var form = $("#frmAddMap");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=directory&task=addMap',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  beforeSubmit: validate,
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
		return;
	}
	
	function validate(formData, jqForm, options) {
	  var form = $("#frmAddMap")[0];
	  if (form.mapFile.value == "") {
		alert('Please select a map image file.');
		return false;
	  }
	  return true;
	}
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddMap").dialog('close');
		loadMapList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------*/
/*    END: Add Map                */
/*--------------------------------*/

/*--------------------------------*/
/*    BEGIN: Edit Map             */
/*--------------------------------*/
function editMap(mID) {
  var form = $("#frmMapList")[0];
  var mapID;
  if (mID) mapID = mID;
  else
    mapID = xsite.getCheckedValue(form.mapID);
  if (!mapID) {
	alert("Please select a map to edit.");
	return;
  }
    
  xsite.createModalDialog({
	windowName: 'winEditMap',
	title: 'Edit Map',
	position: 'center-up',
	width: 600,
	url: 'index.cfm?md=directory&tmp=snip_edit_map&mapID=' + mapID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditMap").dialog('close'); },
	  ' Save ': submitForm_EditMap
	}
  }).dialog('open');
  
  function submitForm_EditMap() {
	if (!confirm('Please note that if the old map had already been used by some directory items, ' +
				 'you will need to update their location info on the new map after the map is updated. Are you sure you want to proceed?')) return;
	
	var form = $("#frmEditMap");
	form.validate();
	if (form.valid()) {
	  form.ajaxSubmit({ 
		  url: 'action.cfm?md=directory&task=editMap',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  beforeSubmit: validate,
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	  });
	} else {
	  return;	
	}
	
	function validate(formData, jqForm, options) {
	  var form = $("#frmEditMap")[0];
	  if (form.mapFile.value == "") {
		alert('Please select a map image file.');
		return false;
	  }
	  return true;
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditMap").dialog('close');
		loadMapList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}	  
  }
}
/*--------------------------------*/
/*    END: Edit Map               */
/*--------------------------------*/


/*--------------------------------*/
/*  BEGIN: Delete Map             */
/*--------------------------------*/
function deleteMap() {
  var form = $("#frmMapList")[0];
  var mapID = xsite.getCheckedValue(form.mapID);
  if (!mapID) {
	alert("Please select a map to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected map?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=directory&task=deleteMap&mapID=' + mapID, {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadMapList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }
}
/*--------------------------------*/
/*  END: Delete Map               */
/*--------------------------------*/