var pageNum=1;//initial page number

$(function() {
    loadItemList(null);
});

function loadItemList(pNum) {
  if (pNum) pageNum = pNum;
  var form=$("#frmSearchItems")[0];
  var keyword=form.keyword.value;
  if (keyword == "") return;
  if (keyword.length > 0 && keyword.length < 3) { alert("Please enter a keyword with at least 3 characters."); return; }
  
  var itemType = "O";
  if (form.itemType[1].checked) itemType="I";

  var url="index.cfm?md=directory&tmp=snip_item_list&keyword=" + escape(keyword) + "&itemType=" + itemType + "&pageNum=" + pageNum + "&browseBy=search&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

/* the following copied from articles.js */
function editItem(itID) {
  var form = $("#frmItemList")[0];
  var itemID = null;
  if (itID) itemID = itID;
  else itemID = xsite.getCheckedValue(form.itemID);

  if (!itemID) {
	alert("Please select an item to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditItem',
	title: 'Edit Item',
	width: 650,
	position: 'center-up',
	url: 'index.cfm?md=directory&tmp=snip_edit_item&itemID=' + itemID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditItem").dialog('close'); },
	  ' Save ': submitForm_EditItem
	}
  }).dialog('open');
	
  function submitForm_EditItem() {
	var form = $("#frmEditItem");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=directory&task=editItem',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
	
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditItem").dialog('close');
		loadItemList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteItem() {
  var form = $("#frmItemList")[0];
  var itemID = xsite.getCheckedValue(form.itemID);
  if (!itemID) {
	alert("Please select an item to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected item?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=directory&task=deleteItem&itemID=' + itemID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadItemList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function toggleItemFields(itemTypeField) {
  if (itemTypeField.checked && itemTypeField.value == "I") {
	$(".individual").show();
	$(".organization").hide();
  } else {
	$(".individual").hide();
	$(".organization").show();
  }
}
