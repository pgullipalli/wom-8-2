<cfparam name="URL.itemID">
<cfparam name="URL.itemType">

<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>
<cfset relatedItems=DR.getRelatedItems(URL.itemID)>

<cfif relatedItems.recordcount Is 0>
<p>No related items selected for the selected item. Click "New" button in the action bar to select related items.</p>
<cfelse>
<form id="frmRelatedItemsList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="100">Up/Down</th>
    <th>Item Name</th>
  </tr>  
  <cfoutput query="relatedItems">
  <tr>
    <td align="center"><input type="checkbox" name="itemIDList" value="#itemID#"></td>
    <td align="center">
      <a href="##" onclick="moveRelatedItem('#itemID#','up')"><img src="images/icon_up.gif" border="0" align="absbottom" alt="move up"></a>
      <a href="##" onclick="moveRelatedItem('#itemID#','down')"><img src="images/icon_down.gif" border="0" align="absbottom" alt="move down"></a>
    </td>
    <td>
      <cfif Compare(URL.itemType,"I")>#orgName#<cfelse>#indivPrefix# #indivFirstName# #indivMiddleName# #indivLastName#<cfif Compare(indivSuffix,"")>, #indivSuffix#</cfif></cfif>
    </td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>