<cfparam name="URL.catID" default="0">
<cfparam name="URL.itemType" default="O">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>

<cfset structItems=DR.getItemsByCategoryID(categoryID=URL.catID, itemType=URL.itemType, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
<cfif structItems.numAllItems IS 0>
  <p>No items in this category.</p>
</cfif>


<cfif structItems.numDisplayedItems GT 0>
<form id="frmItemList">
<div class="itemList">
<table>
  <tr>
	<th width="100">&nbsp;</th>
	<th>Name</th>
    <th width="100">Publish?</th>
  </tr>  
  <cfoutput query="structItems.items">
  <tr>
    <td align="center">
      <a href="##" class="fg-button ui-state-default ui-corner-all button65" onClick="selectItem('#itemID#');">Select</a>
    </td>
	<td><span id="item#itemID#">#orgName#</span></td> 
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif structItems.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structItems.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadItemList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>