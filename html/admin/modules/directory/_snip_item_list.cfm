<cfparam name="URL.catID" default="0">
<cfparam name="URL.itemType" default="I">
<cfparam name="URL.keyword" default="">
<cfparam name="URL.browseBy" default="category">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>

<cfif URL.browseBy IS "category"><!--- browse by category ID --->
  <cfset structItems=DR.getItemsByCategoryID(categoryID=URL.catID, itemType=URL.itemType, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
  <cfif structItems.numAllItems IS 0>
	<p>
	It appears that there are no items created in this category.
	Please click on the 'New' button in the action bar above to create the first item.
	</p>
  </cfif>
<cfelse><!--- browse by headline keyword --->
  <cfset structItems=DR.getItemsByKeyword(keyword=URL.keyword, itemType=URL.itemType, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>
  <cfoutput>
  <p>
  Your search for '<i>#URL.keyword#</i>' returns <cfif structItems.numAllItems GT 0>#structItems.numAllItems# items.<cfelse>no results.</cfif>
  </p>	
  </cfoutput>
</cfif>


<cfif structItems.numDisplayedItems GT 0>
<form id="frmItemList">
<div class="itemList">
<table>
  <cfif URL.itemType Is "I">
  <tr>
	<th width="40">&nbsp;</th>
	<th>Name</th>
    <th>Division/Title</th>
    <th>Address</th>
    <th>Email Address</th>
    <th>Phone/Fax</th>
    <th>Publish?</th>
    <th>Campus Map</th>
  </tr>  
  <cfelse>
  <tr>
	<th width="40">&nbsp;</th>
	<th>Name</th>
    <th>Address</th>
    <!--- <th>Mailing Address</th> --->
    <th>Email Address</th>
    <th>Phone/Fax</th>
    <th># Related Indiv.</th>
    <th>Publish?</th>
    <th>Campus Map</th>
  </tr>
  </cfif>
  <cfif URL.itemType Is "I">
  <cfoutput query="structItems.items">
  <tr>
    <td align="center"><input type="radio" name="itemID" value="#itemID#"></td>
	<td>
      <a href="##" onclick="editItem('#itemID#')"><cfif Compare(indivPrefix,"")>#indivPrefix# </cfif>#indivFirstName# #indivMiddleName#
      #indivLastName#<cfif Compare(indivSuffix,"")>, #indivSuffix#</cfif></a></td>
    <td>
      #indivDivision#
	  <cfif Compare(indivJobTitle,"")>
	    <cfif Compare(indivDivision,"")><br /></cfif>
        <i>#indivJobTitle#</i>
      </cfif>
      &nbsp;
    </td>
    <td>
      #indivAddress#
      <cfif Compare(indivCity,"")><cfif Compare(indivAddress,"")><br /></cfif>
       #indivCity#</cfif><cfif Compare(indivState,"")><cfif Compare(indivCity,"")>, </cfif>#indivState#</cfif> #indivZIP#      
      &nbsp;
    </td>
    <td>#indivEmail#&nbsp;</td>
    <td>
      <cfif Compare(indivPhone,"")>Ph. #indivPhone#</cfif>
      <cfif Compare(indivFax,"")><cfif Compare(indivPhone,"")><br /></cfif>Fax. #indivFax#</cfif>
      &nbsp;
    </td>    
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
    <td align="center">
      <cfif mapID GT 0>
        <a href="javascript:xsite.openWindow('index.cfm?md=directory&tmp=verifycampusmap&itemID=#itemID#&wrap=1','',850,650)" title="click to verify Campus Map" class="accent01">Edit/View</a>
      <cfelse>
        N/A |
        <a href="javascript:xsite.openWindow('index.cfm?md=directory&tmp=verifycampusmap&itemID=#itemID#&wrap=1','',850,650)" title="click to verify Campus Map" class="accent01">Add</a>
      </cfif>
    </td>
  </tr>
  </cfoutput>
  <cfelse>
  <cfoutput query="structItems.items">
  <tr>
    <td align="center"><input type="radio" name="itemID" value="#itemID#"></td>
	<td><a href="##" onclick="editItem('#itemID#')">#orgName#</a></td> 
    <td>
      #orgAddressPhysical#
      <cfif Compare(orgCityPhysical,"")><cfif Compare(orgAddressPhysical,"")><br /></cfif>
       #orgCityPhysical#</cfif><cfif Compare(orgStatePhysical,"")><cfif Compare(orgCityPhysical,"")>, </cfif>#orgStatePhysical#</cfif> #orgZIPPhysical#      
      &nbsp;
    </td>
    <!--- <td>
      #orgAddressMail#
      <cfif Compare(orgCityMail,"")><cfif Compare(orgAddressMail,"")><br /></cfif>
       #orgCityMail#</cfif><cfif Compare(orgStateMail,"")><cfif Compare(orgCityMail,"")>, </cfif>#orgStateMail#</cfif> #orgZIPMail#      
      &nbsp;
    </td> --->
    <td>
      #orgEmail#
      &nbsp;</td>
    <td>
      <cfif Compare(orgPhone,"")>Ph. #orgPhone#</cfif>
      <cfif Compare(orgFax,"")><cfif Compare(orgPhone,"")><br /></cfif>Fax. #orgFax#</cfif>
      &nbsp;
    </td>
    <td align="center"><a href="index.cfm?md=directory&tmp=relateditems&wrap=1&itemID=#itemID#&catID=#URL.catID#&keyword=#URLEncodedFormat(URL.keyword)#&itemType=I&activeItemType=#URL.itemType#" class="accent01">#numRelatedItems#</a></td>
    <td align="center"><cfif published>Yes<cfelse>No</cfif></td>
    <td align="center">
      <cfif mapID GT 0>
        <a href="javascript:xsite.openWindow('index.cfm?md=directory&tmp=verifycampusmap&itemID=#itemID#&wrap=1','',850,650)" title="click to verify Campus Map" class="accent01">Edit/View</a>
      <cfelse>
        N/A |
        <a href="javascript:xsite.openWindow('index.cfm?md=directory&tmp=verifycampusmap&itemID=#itemID#&wrap=1','',850,650)" title="click to verify Campus Map" class="accent01">Add</a>
      </cfif>
    </td>
  </tr>
  </cfoutput>
  </cfif>
</table>
</div>
</form>
</cfif>

<cfif structItems.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structItems.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadItemList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>