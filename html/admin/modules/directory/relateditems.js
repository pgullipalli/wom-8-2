var itemID;
var itemType;
$(function () {	
	itemID=$("#itemID").text();
	itemType=$("#itemType").text();
	
	loadItemList();	
});

function loadItemList() {  
  var url="index.cfm?md=directory&tmp=snip_relateditems_list&itemID=" + itemID + "&itemType=" + itemType + "&uuid=" + xsite.getUUID();
  xsite.load("#mainDiv", url);
}

function addRelatedItems() {
  xsite.createModalDialog({
	windowName: 'winAddRelatedItems',
	title: 'Add Related Items',
	width: 600,
	height:500,
	position: 'center-up',
	url: 'index.cfm?md=directory&tmp=snip_add_relateditems&itemType=' + itemType + '&uuid=' + xsite.getUUID(),
	urlCallback: function () {
	  $("#frmNavigator").unbind();
	  $("#frmNavigator select[name='categoryID']").change(function() {
		loadItemListForSelection(1);	  
	  });
	  loadItemListForSelection(1);
	},
	buttons: {
	  'Cancel': function() { $("#winAddRelatedItems").dialog('close'); },
	  ' Add ': submitForm_AddRelatedItems
	}
  }).dialog('open');

}

function loadItemListForSelection(pageNum) {
  var catID=$("#frmNavigator select[name='categoryID']").val();
  var url="index.cfm?md=directory&tmp=snip_item_list_for_selection&catID=" + catID + "&itemType=" + itemType + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();
  xsite.load("#subDiv1", url);
}

function submitForm_AddRelatedItems() {
  var form = $("#frmItemList");
  form.ajaxSubmit({ 
    url: 'action.cfm?md=directory&task=addRelatedItems&itemID=' + itemID,
    type: 'post',
    cache: false,
    dataType: 'json', 
    success: processJson,
    error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
  });
	
  function processJson(jsonData) {
    if (jsonData.SUCCESS) {
	  $("#winAddRelatedItems").dialog('close');
	  loadItemList();
    } else {
 	  alert (jsonData.MESSAGE);
    }
  }
}

function deleteRelatedItems() {
  if(!document.getElementById('frmRelatedItemsList')) return;
  var form = $("#frmRelatedItemsList")[0];
  var itemIDList = xsite.getCheckedValue(form.itemIDList);
  if (!itemIDList) {
	alert("Please select at least an item to remove.");
	return;	  
  }
  if (!confirm('Are you sure you want to remove the selected items?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=directory&task=deleteRelatedItems&itemID=' + itemID + '&itemIDList=' + escape(itemIDList) + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  loadItemList();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}

function moveRelatedItem(itemID2, direction) {
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=directory&task=moveRelatedItem&itemID1=' + itemID + '&itemID2=' + itemID2 + '&direction=' + direction + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  xsite.closeWaitingDialog();
	  loadItemList();
	} else {
	  xsite.closeWaitingDialog();
	  alert(jsonData.MESSAGE);
	}
  }
}
