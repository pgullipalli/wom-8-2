<cfparam name="URL.keyword" default="">

<script type="text/javascript" src="modules/directory/searchitems.js"></script>

<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>
<cfset allCategories=DR.getAllCategories()>
<cfif allCategories.recordcount IS 0>
  <!--- no page categories were ever created --->
  <cflocation url="index.cfm?md=directory&tmp=categories&wrap=1">
</cfif>

<div class="header">DIRECTORY MANAGER &gt; Search Organizations/Individuals</div>

<div id="actionBar">
<a href="#" id="btnEdit" class="fg-button ui-state-default ui-corner-all button65" onclick="editItem(null);">Edit</a>
<a href="#" id="btnDelete" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteItem();">Delete</a>
</div>

<br style="clear:both" /><br />

<cfoutput>
<form id="frmSearchItems" onsubmit="return false">
Enter a keyword:
<input type="text" name="keyword" size="20" maxlength="30" value="#HTMLEditFormat(URL.keyword)#">
<input id="btnSearch" type="button" value=" Submit " onclick="loadItemList(null);">
&nbsp;&nbsp;&nbsp;&nbsp;
<a href="index.cfm?md=directory&tmp=items&wrap=1">Browse By Categories</a><br />
Search type: <input type="radio" name="itemType" value="O" checked /> Organization <input type="radio" name="itemType" value="I" /> Individual 
</form>
</cfoutput>

<div id="mainDiv"></div>