<div class="snip">
<form id="frmAddMap" name="frmAddMap" onsubmit="return false">
<div class="row">
	<div class="col-30-right"><b>Map Name:</b></div>
    <div class="col-68-left">
      <input type="text" name="mapName" size="45" maxlength="100" value="" validate="required:true" />
    </div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="featured" value="1" /></div>
    <div class="col-68-left">Featured</div>
</div>
<div class="row">
	<div class="col-30-right"><b>Map Image:</b></div>
    <div class="col-68-left">
      <input type="hidden" name="mapFile" value="">
      <CF_SelectFile formName="frmAddMap" fieldName="mapFile" previewID="preview1" previewWidth="350">
      <div id="preview1"></div>
      <br />
      (Max width for Printer-friendly: 720px)
    </div>
</div>
</form>
</div>