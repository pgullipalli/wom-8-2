var pageNum=1;//initial page number
var formName, fieldName, previewID;

$(function () {
	formName = xsite.getURLParameter('formName');
	fieldName = xsite.getURLParameter('fieldName');
	previewID = xsite.getURLParameter('previewID');

	loadItemList();
	
	$("#frmNavigator select[name='categoryID']").change(function() {
	  pageNum=1;
	  loadItemList();	  
	});
});

function loadItemList(pNum) {
  if (pNum) pageNum = pNum;
  var catID=$("#frmNavigator select[name='categoryID']").val();	
  
  var url="index.cfm?md=directory&tmp=snip_item_list_ds&catID=" + catID + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function selectItem(itemID) {
  var openerFormField=opener.document[formName][fieldName];
  var openerPreview=opener.document.getElementById(previewID);
  var itemName=$("#item" + itemID).html();
  openerFormField.value=itemID;
  openerPreview.innerHTML=itemName;
  window.close();
}