<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="10">

<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>
<cfset structItems=DR.getMapsByPageNum(pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage)>

<cfif structItems.numDisplayedItems GT 0>
<form id="frmMapList">
<div class="itemList">
<table>
  <tr>
	<th width="40">&nbsp;</th>
    <th width="250">Map Name</th>
    <th width="350">Map Image</th>    
    <th>Map File</th>
    <th width="70">Featured</th>
  </tr>
  <cfoutput query="structItems.maps">
  <tr>
    <td align="center"><input type="radio" name="mapID" value="#mapID#"></td>
    <td><a href="javascript:editMap('#mapID#')">#mapName#</a></td>
    <td align="center"><img src="/#mapFile#" border="0" width="300"></td>
    <td align="center">/#mapFile#</td>
    <td align="center"><cfif featured>Yes<cfelse>No</cfif></td>
  </tr>
  </cfoutput>
</table>
</div>
</form>
</cfif>

<cfif structItems.numAllItems GT URL.numItemsPerPage>
<p align="center">
  Page
  <cfoutput>
  <cfloop index="idx" from="1" to="#Ceiling(structItems.numAllItems/URL.numItemsPerPage)#">
    <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadMapList(#idx#)">#idx#</a></cfif>
  </cfloop>
  </cfoutput>
</p>
</cfif>