<cfparam name="URL.itemID">

<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>
<cfset itemInfo=DR.getItemByID(URL.itemID)>

<cfif Not Compare(itemInfo.itemType,"I")><!--- individual --->
  <cfset address=itemInfo.indivAddress & ", " & itemInfo.indivCity & ", " & itemInfo.indivState & " " & itemInfo.indivZIP>
<cfelse><!--- organization --->
  <cfset address=itemInfo.orgAddressPhysical & ", " & itemInfo.orgCityPhysical & ", " & itemInfo.orgStatePhysical & " " & itemInfo.orgZIPPhysical>
</cfif>
<cfset address=Replace(address, "'", "\'", "All")>

<h2>DIRECTORY MANAGER &gt;&gt; Verify Google Map</h2>

<cfoutput>
<script type="text/javascript" src="http://www.google.com/jsapi?key=#Application.googleMapAPIKey#"></script>
<p>#address#</p>
</cfoutput>

<script type="text/javascript">
google.load("maps", "2.x");

function showGoogleMap() {
  <cfoutput>var address = "#address#";</cfoutput>

  if (google.maps.BrowserIsCompatible()) {
	var map = new google.maps.Map2(document.getElementById("map_canvas"));
	
	map.addControl(new google.maps.SmallMapControl());
	geocoder = new google.maps.ClientGeocoder();
	
	if (geocoder) {
	  geocoder.getLatLng(
		address,
		function(point) {
		  if (!point) {
			alert(address + " not found");
			document.frmVerifyGoogleMap.googleMapVerified[1].checked = true;
		  } else {
			map.setCenter(point, 15);
			map.addOverlay(new google.maps.Marker(point));
			document.frmVerifyGoogleMap.googleMapVerified[0].checked = true;
			var lat = '' + point.lat();
			var lng = '' + point.lng();
			document.frmVerifyGoogleMap.latitude.value = lat;
			document.frmVerifyGoogleMap.longitude.value = lng;
			document.getElementById('spanLat').innerHTML = lat;
			document.getElementById('spanLng').innerHTML = lng;
		  }
		}
	  );
	}
	
  }
}

google.setOnLoadCallback(showGoogleMap);

function updateStatus() {
  //show waiting dialog
  var form = $("#frmVerifyGoogleMap")[0];
  var itemID = form.itemID.value;
  var latitude=form.latitude.value;
  var longitude=form.longitude.value;
  var googleMapVerified=0;
  if (form.googleMapVerified[0].checked) googleMapVerified=1;
  
  xsite.showWaitingDialog({
    openCallback:function() {
	  $.get('action.cfm?md=directory&task=verifyGoogleMap&itemID=' + itemID + '&latitude=' + latitude + '&longitude=' + longitude +
	  		'&googleMapVerified=' + googleMapVerified + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');
	}
  });

  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  opener.loadItemList();
	  window.close();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	

}
</script>

<div id="map_canvas" style="width: 500px; height: 300px"></div>

<form id="frmVerifyGoogleMap" name="frmVerifyGoogleMap" onSubmit="return false;">
<cfoutput><input type="hidden" name="itemID" value="#URL.itemID#" /></cfoutput>
<input type="hidden" name="latitude" value="0" />
<input type="hidden" name="longitude" value="0" />
<br>
Does this Google Map shows up correctly?
<input type="radio" name="googleMapVerified" value="1" /> Yes
<input type="radio" name="googleMapVerified" value="0" checked />No
<input type="button" value=" Update Status " onClick="updateStatus()" /><br />
(Latitude, Longitude) = (<span id="spanLat">---</span>, <span id="spanLng">---</span>)<br />
</form>