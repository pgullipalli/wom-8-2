<cfparam name="URL.catID">
<cfparam name="URL.itemType">
<cfparam name="URL.pageNum" default="1">
<cfparam name="URL.numItemsPerPage" default="20">

<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>

<cfset structItemss=DR.getItemsByCategoryID(categoryID=URL.catID, pageNum=URL.pageNum, numItemsPerPage=URL.numItemsPerPage, itemType=URL.itemType)>

<cfif structItemss.numAllItems IS 0>
  <p>No items created in this category.</p>
<cfelse>
  <cfif structItemss.numDisplayedItems GT 0>  
    <form name="frmItemList" id="frmItemList">
    <div class="itemList">
    <table>
      <tr>
        <th width="40">&nbsp;</th>
        <th>Name</th>
      </tr>  
      <cfoutput query="structItemss.items">
      <tr>
        <td align="center"><input type="checkbox" name="itemIDList" value="#itemID#"></td>
        <td>
          <cfif Compare(URL.itemType,"I")>#orgName#<cfelse>#indivPrefix# #indivFirstName# #indivMiddleName# #indivLastName#<cfif Compare(indivSuffix,"")>, #indivSuffix#</cfif></cfif>
        </td>
      </tr>
      </cfoutput>
    </table>
    </div>
    </form>
    
    <cfif structItemss.numAllItems GT URL.numItemsPerPage>
    <p align="center">
      Page
      <cfoutput>
      <cfloop index="idx" from="1" to="#Ceiling(structItemss.numAllItems/URL.numItemsPerPage)#">
        <cfif idx Is URL.pageNum>#idx#<cfelse><a href="##" onclick="loadItemListForSelection(#idx#)">#idx#</a></cfif>
      </cfloop>
      </cfoutput>
    </p>
    </cfif>
  </cfif>
</cfif>

