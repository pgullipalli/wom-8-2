<cfparam name="URL.mapID">
<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>
<cfset map=DR.getMap(URL.mapID)>

<div class="snip">
<cfoutput query="map">
<form id="frmEditMap" name="frmEditMap" onsubmit="return false">
<input type="hidden" name="mapID" value="#URL.mapID#">
<div class="row">
	<div class="col-30-right"><b>Map Name:</b></div>
    <div class="col-68-left">
      <input type="text" name="mapName" size="45" maxlength="100" value="#HTMLEditFormat(mapName)#" validate="required:true" />
    </div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="featured" value="1" <cfif featured>checked</cfif> /></div>
    <div class="col-68-left">Featured</div>
</div>
<div class="row">
	<div class="col-30-right"><b>Map Image:</b></div>
    <div class="col-68-left">
      <input type="hidden" name="mapFile" value="#HTMLEditFormat(mapFile)#">
      <CF_SelectFile formName="frmEditMap" fieldName="mapFile" previewID="preview2" previewWidth="350">
      <cfif Compare(mapFile,"")>
      <div id="preview2"><img src="/#mapFile#" width="350"></div>
      <cfelse>
      <div id="preview2"></div>
      </cfif>
      <br />
      (Max width for Printer-friendly: 720px)
    </div>
</div>
</form>
</cfoutput>
</div>