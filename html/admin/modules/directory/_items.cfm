<cfparam name="URL.catID" default="0">
<cfparam name="URL.itemType" default="O">

<script type="text/javascript" src="modules/directory/items.js"></script>

<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>
<cfset allCategories=DR.getAllCategories()>
<cfif URL.catID IS 0>
  <cfset URL.catID=DR.getFirstCategoryID()>
  <cfif URL.catID IS 0><!--- no categories were ever created --->
    <cflocation url="index.cfm?md=directory&tmp=categories&wrap=1">
  </cfif>
</cfif>

<div class="header">DIRECTORY MANAGER &gt; Organizations/Individuals</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addItem();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editItem(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteItem();">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="previewItem();">Preview</a>
</div>

<br style="clear:both;" /><br />

<cfoutput>
<form id="frmNavigator">
  Category
  <select name="categoryID" style="min-width:250px;">
    <cfloop query="allCategories">
    <option value="#categoryID#"<cfif URL.catID IS categoryID> selected</cfif>>#categoryName#</option>
    </cfloop>
  </select> &nbsp;&nbsp;
  Type
  <select name="itemType" style="min-width:150px;">
    <option value="O"<cfif URL.itemType IS "O"> selected</cfif>>Organizations</option>
    <option value="I"<cfif URL.itemType IS "I"> selected</cfif>>Individuals</option>
  </select>
  &nbsp;&nbsp;&nbsp;&nbsp;
  <a href="index.cfm?md=directory&tmp=searchitems&wrap=1">Search Items</a>
</form>
</cfoutput>

<br style="clear:both;" />

<div id="mainDiv"></div>