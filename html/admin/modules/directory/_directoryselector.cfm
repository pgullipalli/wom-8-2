<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>
<cfset allCategories=DR.getAllCategories()>

<script type="text/javascript" src="modules/directory/directoryselector.js"></script>

<p align="right"><a href="javascript:window.close();"><img src="images/icon_cancel.gif" border="0" /></a></p>

<div class="header">DIRECTORY ITEM SELECTOR</div>

<cfoutput>
<form id="frmNavigator">
  Select a Category:
  <select name="categoryID" style="min-width:200px;">
    <cfloop query="allCategories">
    <option value="#categoryID#">#categoryName#</option>
    </cfloop>
  </select>
</form>
</cfoutput>

<br /><br />

<div id="mainDiv"></div>