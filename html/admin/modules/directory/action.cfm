<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  DR=CreateObject("component", "com.DirectoryAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";
  
  switch (URL.task) {
    case "addCategory":
	  DR.addCategory(argumentCollection=Form);
	  break;
	  
	case "editCategory":
	  DR.editCategory(argumentCollection=Form);
	  break;
	  
	case "deleteCategory":
	  if (NOT DR.deleteCategory(URL.catID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="It appears that there are still items in the category that you tried to delete. " &
	  		 "Please make sure you have deleted all items in this category before deleting this category.";
	  }
	  break;
	  
	case "addItem":
	  if (Not DR.addItem(argumentcollection=FORM)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="Please select at least one category for this new item.";
	  }
	  break;
	  
	case "editItem":
	  if (Not DR.editItem(argumentcollection=FORM)) {
	    returnedStruct.SUCCESS=false;
		returnedStruct.MESSAGE="Please select at least one category for this new item.";
	  }
	  break;
	  
	case "deleteItem":
      DR.deleteItem(URL.itemID);
	  if (NOT DR.deleteItem(URL.itemID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="This item can not be deleted. It appears that this it was still referenced by other entities.";
	  }
      break;
	  
	case "addRelatedItems":
	  Form.itemID=URL.itemID;
	  DR.addRelatedItems(argumentCollection=Form);
	  break;
	  
	case "deleteRelatedItems":
	  DR.deleteRelatedItems(argumentCollection=URL);
	  break;
	 
	case "moveRelatedItem":
	  DR.moveRelatedItem(itemID1=URL.itemID1,itemID2=URL.itemID2,direction=URL.direction);
	  break;
	  
	case "verifyCampusMap":
	  DR.verifyCampusMap(argumentcollection=URL);
	  break;
	  
	case "verifyGoogleMap":
	  DR.verifyGoogleMap(argumentcollection=URL);
	  break;
	  
	case "addMap":
	  DR.addMap(argumentCollection=Form);
	  break;
	  
	case "editMap":
	  DR.editMap(argumentCollection=Form);
	  break;
	  
	case "deleteMap":
	  if (Not DR.deleteMap(URL.mapID)) {
	    returnedStruct.SUCCESS=false;
		returnedStruct.MESSAGE="The map can not be deleted because it was still referenced by directory items.";
	  }
	  break;
	  
	default:
	  break;
  }
</cfscript>

<cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
</cfsetting>