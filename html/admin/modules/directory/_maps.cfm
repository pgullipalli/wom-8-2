<script type="text/javascript" src="modules/directory/maps.js"></script>

<div class="header">DIRECTORY MANAGER &gt; Maps</div>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addMap();">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editMap(null);">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteMap();">Delete</a>
</div>

<br style="clear:both;" />
<p>
The maps set up here will be available for plotting the locations of category directories.
</p>

<div id="mainDiv"></div>