<cfparam name="URL.itemID">
<cfparam name="URL.mapID" default="0">

<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>
<cfset itemInfo=DR.getItemByID(URL.itemID)>
<cfset allMaps=DR.getMaps()>

<div class="header">DIRECTORY MANAGER &gt;&gt; Verify Campus Map</div>

<cfoutput query="itemInfo">
<div class="subHeader"><i><b>
<cfif Not Compare(itemType,"I")><!--- individual --->
  #indivPrefix# #indivFirstName# #indivMiddleName# #indivLastName#<cfif Compare(indivSuffix,"")>, #indivSuffix#</cfif>
<cfelse><!--- organization --->
  #orgName#
</cfif>
</b></i></div>
</cfoutput>

<br />

<cfif allMaps.recordcount Is 0>
  <p>No map has been set up yet. Please <a href="javascript:goToMapSetup();">click here</a> to set up at least one map.</p>
  <script type="text/javascript">
    function goToMapSetup() {
	  opener.location="index.cfm?md=directory&tmp=maps&wrap=1";
	  window.close();
	}
  </script>
<cfelse>
  <script type="text/javascript">
	$("document").ready(function() {
	  $("#map").click(function(event) {
		var p=$("#map").offset();
		var mapX=Math.ceil(p.left);
		var mapY=Math.ceil(p.top);
		var posX = parseInt(event.pageX) - mapX;
		var posY = parseInt(event.pageY) - mapY;
		var form = document.frmCampusMap;
		form.posX.value='' + posX;
		form.posY.value='' + posY;
		showPin("map", "pin");
	  });
	  
	  showPin("map", "pin");
	});
		
	function showPin(mapID, pinID) {
	  var form = document.frmCampusMap;
	  var p=$("#" + mapID).offset();
	  var mapX=Math.ceil(p.left);
	  var mapY=Math.ceil(p.top);
	  var pinX=mapX + parseInt(form.posX.value) - Math.ceil(parseInt($("#" + pinID).attr("width"))/2);
	  var pinY=mapY + parseInt(form.posY.value) - parseInt($("#" + pinID).attr("height"));
	  $("#" + pinID).hide();
	  $("#" + pinID).css({'position' : 'absolute', 'left' : pinX + 'px', 'top' : pinY + 'px', "display" : "inline"});
	}

	function saveMap (verified) {
	  var form = document.frmCampusMap;
	  var itemID = form.itemID.value;
	  var mapID = form.mapID.options[form.mapID.selectedIndex].value;
	  if (!verified) {	    
	    mapID="0";
	    form.posX.value="0";
	    form.posY.value="0";
	  }
	  
	  xsite.showWaitingDialog({
		openCallback:function() {
		  $.get('action.cfm?md=directory&task=verifyCampusMap&itemID=' + itemID + '&mapID=' + mapID + '&posX=' + form.posX.value + '&posY=' + form.posY.value +
				'&uuid=' + xsite.getUUID(), {}, processJson, 'json');
		}
	  });
	
	  function processJson(jsonData) {
		if (jsonData.SUCCESS) {
		  //remove waiting dialog
		  xsite.closeWaitingDialog();
		  opener.loadItemList();
		  window.close();
		} else {
		  xsite.closeWaitingDialog();
		  xsite.showAlertDialog(jsonData.MESSAGE);
		}
	  }	
	}
	
	function changeMap() {
	  var form = document.frmCampusMap;
	  var itemID = form.itemID.value;
	  var mapID = form.mapID.options[form.mapID.selectedIndex].value;
	  window.location="index.cfm?md=directory&tmp=verifycampusmap&wrap=1&itemID=" + itemID + "&mapID=" + mapID;
	}
	
	function resetMap() {
	  var form = document.frmCampusMap;
	  form.posX.value="0";
	  form.posY.value="0";
	  showPin("map", "pin");
    }
  </script>
  
  <cfif Not Compare(URL.mapID, "0")>
    <cfif itemInfo.mapID GT 0>
      <cfset URL.mapID=itemInfo.mapID>
    <cfelse>
      <cfset URL.mapID=allMaps.mapID[1]>
    </cfif>
  </cfif>
  
  <cfset mapInfo=DR.getMap(URL.mapID)>
  
  <form name="frmCampusMap" id="frmCampusMap" onSubmit="return false;">
  <cfoutput>
  <input type="hidden" name="itemID" value="#URL.itemID#" />
  </cfoutput>
  <p>
  1. Select a map
   	 <select name="mapID" style="min-width:200px;" onchange="changeMap()">
       <cfoutput query="allMaps">
       <option value="#allMaps.mapID#"<cfif Not Compare(URL.mapID,allMaps.mapID)> selected</cfif>>#allMaps.mapName#</option>
       </cfoutput>
     </select>
  </p>
  
  <p>
  2. Plot the location on the map:<br /><br />

  <div style="width:100%; display:block;">
  <cfoutput>
  <img id="pin" src="images/imgPushpin.png" width="34" height="30" posX="#itemInfo.posX#" posY="#itemInfo.posY#" style="display:none;" />
  <img id="map" src="/#mapInfo.mapFile#" border="0" />
  </cfoutput>
  </div>
  </p>
  
  <p>
  <cfoutput>
  X: <input type="text" name="posX" size="3" value="#itemInfo.posX#" />  Y: <input type="text" name="posY" size="3" value="#itemInfo.posY#" />
  </cfoutput>
  </p>

  <p align="center">
  <input type="button" value=" Save Map " onclick="saveMap(1);" />  
  <input type="button" value=" Reset " onclick="resetMap();" /> 
  <input type="button" value=" Cancel " onclick="window.close();" />
  </p>
  
  <p align="center">
  <input type="button" value=" Do Not Use Map " onclick="saveMap(0);" />
  </p>
  </form>

</cfif>
