<cfparam name="URL.itemID">
<cfparam name="URL.itemType">
<cfparam name="URL.activeItemType">
<cfparam name="URL.catID" default="0">
<cfparam name="URL.keyword" default="">

<script type="text/javascript" src="modules/directory/relateditems.js"></script>

<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>
<cfset itemInfo=DR.getItemByID(URL.itemID)>

<div class="header">DIRECTORY MANAGER &gt; Related Items</div>

<span id="itemID" style="display:none"><cfoutput>#URL.itemID#</cfoutput></span>
<span id="itemType" style="display:none"><cfoutput>#URL.itemType#</cfoutput></span>
<div id="actionBar">
<a href="#" id="btnNew" class="fg-button ui-state-default ui-corner-all button65" onclick="addRelatedItems()">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteRelatedItems()">Remove</a>
</div>

<br style="clear:both;" /><br />

<cfoutput>
<cfif Compare(URL.catID,"0")>
<p><a href="index.cfm?md=directory&tmp=items&wrap=1&itemType=#URL.activeItemType#&catID=#URL.catID#">&laquo; Back</a></p>
<cfelse>
<p><a href="index.cfm?md=directory&tmp=items&wrap=1&itemType=#URL.activeItemType#&keyword=#URLEncodedFormat(URL.keyword)#">&laquo; Back</a></p>
</cfif>

<p class="subHeader">Item Name: <b><i><cfif Compare(itemType,"I")>#itemInfo.orgName#<cfelse>#itemInfo.indivPrefix# #itemInfo.indivFirstName# #itemInfo.indivMiddleName# #itemInfo.indivLastName#<cfif Compare(itemInfo.indivSuffix,"")>, #itemInfo.indivSuffix#</cfif></cfif></i></b></p>
</cfoutput>


<div id="mainDiv"></div>
