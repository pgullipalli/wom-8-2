var pageNum=1;//initial page number

$(function () {	
	loadItemList();
	
	$("#frmNavigator select[name='categoryID']").change(function() {
	  pageNum=1;
	  loadItemList();	  
	});
	
	$("#frmNavigator select[name='itemType']").change(function() {
	  pageNum=1;
	  loadItemList();	  
	});
});

function loadItemList(pNum) {
  if (pNum) pageNum = pNum;
  var catID=$("#frmNavigator select[name='categoryID']").val();	
  var itemType=$("#frmNavigator select[name='itemType']").val();
  
  var url="index.cfm?md=directory&tmp=snip_item_list&catID=" + catID + "&itemType=" + itemType + "&pageNum=" + pageNum + "&uuid=" + xsite.getUUID();

  xsite.load("#mainDiv", url);
}

function addItem() {
  var catID=$("#frmNavigator select[name='categoryID']").val();	
  var itemType=$("#frmNavigator select[name='itemType']").val();
  
  xsite.createModalDialog({
	windowName: 'winAddItem',
	title: 'Add Item',
	width: 700,
	position: 'center-up',
	url: 'index.cfm?md=directory&tmp=snip_add_item&catID=' + catID + '&itemType=' + itemType + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winAddItem").dialog('close'); },
	  ' Add ': submitForm_AddItem
	}
  }).dialog('open');
  
  function submitForm_AddItem() {
	var form = $("#frmAddItem");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=directory&task=addItem',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddItem").dialog('close');
		loadItemList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function editItem(itID) {
  var form = $("#frmItemList")[0];
  var itemID = null;
  if (itID) itemID = itID;
  else itemID = xsite.getCheckedValue(form.itemID);

  if (!itemID) {
	alert("Please select an item to edit.");
	return;	  
  }
  
  xsite.createModalDialog({
	windowName: 'winEditItem',
	title: 'Edit Item',
	width: 650,
	position: 'center-up',
	url: 'index.cfm?md=directory&tmp=snip_edit_item&itemID=' + itemID + '&uuid=' + xsite.getUUID(),
	buttons: {
	  'Cancel': function() { $("#winEditItem").dialog('close'); },
	  ' Save ': submitForm_EditItem
	}
  }).dialog('open');
	
  function submitForm_EditItem() {
	var form = $("#frmEditItem");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=directory&task=editItem',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }
		
	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditItem").dialog('close');
		loadItemList();
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function deleteItem() {
  var form = $("#frmItemList")[0];
  var itemID = xsite.getCheckedValue(form.itemID);
  if (!itemID) {
	alert("Please select an item to delete.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected item?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=directory&task=deleteItem&itemID=' + itemID + '&uuid=' + xsite.getUUID(), {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadItemList();
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	
}

function previewItem() {
  var form = $("#frmItemList")[0];
  var itemID = xsite.getCheckedValue(form.itemID);
  if (!itemID) {
	alert("Please select an item to preview.");
	return;
  }
  var itemType=$("#frmNavigator select[name='itemType']").val();
  if (itemType == "O")
    window.open('/index.cfm?md=directory&tmp=detail&itemID=' + itemID, '_blank');
  else
    alert("Only organization can be previewed.");
}

function toggleItemFields(itemTypeField) {
  if (itemTypeField.checked && itemTypeField.value == "I") {
	$(".individual").show();
	$(".organization").hide();
  } else {
	$(".individual").hide();
	$(".organization").show();
  }
}