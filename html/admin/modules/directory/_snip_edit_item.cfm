<cfparam name="URL.itemID">
<cfset DR=CreateObject("component", "com.DirectoryAdmin").init()>
<cfset allCategories=DR.getAllCategories()>
<cfset itemInfo=DR.getItemByID(URL.itemID)>
<cfset categoryIDList=DR.getCategoryIDListByItemID(URL.itemID)>

<cfoutput query="itemInfo">
<form id="frmEditItem" name="frmEditItem" onsubmit="return false">
<input type="hidden" name="itemID" value="#URL.itemID#" />
<div class="row">
	<div class="col-30-right"><b>Categories:</b><br /><small>(Check all that apply.)</small></div>
    <div class="col-68-left">
      <cfset numCategories=allCategories.recordcount>
      <cfset numRows=Ceiling(numCategories/3)>
      <cfset idx = 0>
      <cfloop index="row" from="1" to="#numRows#">
        <div class="row">
        <cfloop index="col" from="1" to="3">
          <cfset idx = (row - 1) * 3 + col>
          <cfif idx LTE numCategories>
            <cfset categoryID=allCategories.categoryID[idx]>
            <cfset categoryName=allCategories.categoryName[idx]>
            <div class="col-30-left">
              <input type="checkbox" name="categoryIDList" value="#categoryID#"<cfif ListFind(categoryIDList, categoryID) GT 0> checked</cfif> />
              #categoryName#
            </div>
          </cfif>
        </cfloop>
        </div>
      </cfloop>
    </div>
</div>
<div class="row">
	<div class="col-30-right"><input type="checkbox" name="published" value="1" <cfif published>checked</cfif> /></div>
    <div class="col-68-left">Publish this item</div>
</div>

<div class="row">
	<div class="col-30-right"><b>Type:</b></div>
    <div class="col-68-left">
      <input type="radio" name="itemType" value="I" <cfif Not Compare(itemType,"I")>checked</cfif> onclick="toggleItemFields(this);" /> Individual
      <input type="radio" name="itemType" value="O" <cfif Compare(itemType,"I")>checked</cfif> onclick="toggleItemFields(this);" /> Organization      
    </div>
</div>

<div class="individual" <cfif itemType IS "I">style="display:block;"<cfelse>style="display:none;"</cfif>>
  <div class="row">
	<div class="col-30-right"><b>Name:</b></div>
    <div class="col-68-left">
      <div style="float:left;width:10%;padding:0px 2px;">
      <input type="text" name="indivPrefix" maxlength="20" style="width:35px;" value="#HTMLEditFormat(indivPrefix)#" /><br />
      Prefix
      </div>
      <div style="float:left;width:20%;padding:0px 2px;">
      <input type="text" name="indivFirstName" maxlength="20" style="width:80px;" value="#HTMLEditFormat(indivFirstName)#" /><br />
      First
      </div>
      <div style="float:left;width:20%;padding:0px 2px;">
      <input type="text" name="indivMiddleName" maxlength="20" style="width:80px;" value="#HTMLEditFormat(indivMiddleName)#" /><br />
      Middle
      </div>
      <div style="float:left;width:20%;padding:0px 2px;">
      <input type="text" name="indivLastName" maxlength="20" style="width:80px;" value="#HTMLEditFormat(indivLastName)#" /><br />
      Last
      </div>      
      <div style="float:left;width:10%;padding:0px 2px;">
      <input type="text" name="indivSuffix" maxlength="20" style="width:35px;" value="#HTMLEditFormat(indivSuffix)#" /><br />
      Suffix
      </div>
    </div>
  </div>
  <div class="row">
	<div class="col-30-right">Division:</div>
    <div class="col-68-left"><input type="text" name="indivDivision" maxlength="150" style="width:320px;" value="#HTMLEditFormat(indivDivision)#" /></div>
  </div>
  <div class="row">
	<div class="col-30-right">Job Title:</div>
    <div class="col-68-left"><input type="text" name="indivJobTitle" maxlength="100" style="width:320px;" value="#HTMLEditFormat(indivJobTitle)#" /></div>
  </div>
  <div class="row">
	<div class="col-30-right">Address:</div>
    <div class="col-68-left"><input type="text" name="indivAddress" maxlength="100" style="width:320px;" value="#HTMLEditFormat(indivAddress)#" /></div>
  </div>
  <div class="row">
	<div class="col-30-right">City:</div>
    <div class="col-68-left">
      <input type="text" name="indivCity" maxlength="50" style="width:120px;" value="#HTMLEditFormat(indivCity)#" /> &nbsp;&nbsp;
      State:
      <input type="text" name="indivState" maxlength="50" style="width:30px;" value="#HTMLEditFormat(indivState)#" /> &nbsp;&nbsp;
      ZIP:
      <input type="text" name="indivZIP" maxlength="10" style="width:78px;" value="#HTMLEditFormat(indivZIP)#" />
    </div>
  </div>
  <div class="row">
	<div class="col-30-right">E-Mail Address:</div>
    <div class="col-68-left">
      <input type="text" name="indivEmail" maxlength="100" style="width:320px;" value="#HTMLEditFormat(indivEmail)#" />
    </div>
  </div>
  <div class="row">
	<div class="col-30-right">Phone:</div>
    <div class="col-68-left">
      <input type="text" name="indivPhone" maxlength="50" style="width:138px;" value="#HTMLEditFormat(indivPhone)#" /> &nbsp;&nbsp;
      Fax:
      <input type="text" name="indivFax" maxlength="50" style="width:138px;" value="#HTMLEditFormat(indivFax)#" />
    </div>
  </div>
  <div class="row">
	<div class="col-30-right">Photo:</div>
    <div class="col-68-left">
      <input type="hidden" name="indivPhotoFile" value="#HTMLEditFormat(indivPhotoFile)#" />
      <CF_SelectFile formName="frmEditItem" fieldName="indivPhotoFile" previewID="preview3" previewWidth="150">
      <small>(148px x 148px)</small>
      <cfif Compare(indivPhotoFile, "")>
      <div id="preview3"><img src="/#indivPhotoFile#" border="0" width="150" /></div>  
      <cfelse>
      <div id="preview3"></div>   
      </cfif> 
      
    </div>
  </div>
</div>

<div class="organization" <cfif itemType IS "O">style="display:block;"<cfelse>style="display:none;"</cfif>>
  <div class="row">
	<div class="col-30-right"><b>Name of Organization/<br  />Division or Group:</b></div>
    <div class="col-68-left"><input type="text" name="orgName" maxlength="150" style="width:320px;" value="#HTMLEditFormat(orgName)#" /></div>
  </div>
  <div class="row">
	<div class="col-30-right">Address:</div>
    <div class="col-68-left"><input type="text" name="orgAddressPhysical" maxlength="100" style="width:320px;" value="#HTMLEditFormat(orgAddressPhysical)#" /></div>
  </div>
  <div class="row">
	<div class="col-30-right">City:</div>
    <div class="col-68-left">
      <input type="text" name="orgCityPhysical" maxlength="50" style="width:120px;" value="#HTMLEditFormat(orgCityPhysical)#" /> &nbsp;&nbsp;
      State:
      <input type="text" name="orgStatePhysical" maxlength="50" style="width:30px;" value="#HTMLEditFormat(orgStatePhysical)#" /> &nbsp;&nbsp;
      ZIP:
      <input type="text" name="orgZIPPhysical" maxlength="10" style="width:78px;" value="#HTMLEditFormat(orgZIPPhysical)#" />
    </div>
  </div>
  <!--- <div class="row">
	<div class="col-30-right">Mailing Address:</div>
    <div class="col-68-left"><input type="text" name="orgAddressMail" maxlength="100" style="width:320px;" value="#HTMLEditFormat(orgAddressMail)#" /></div>
  </div>
  <div class="row">
	<div class="col-30-right">City:</div>
    <div class="col-68-left">
      <input type="text" name="orgCityMail" maxlength="50" style="width:120px;" value="#HTMLEditFormat(orgCityMail)#" /> &nbsp;&nbsp;
      State:
      <input type="text" name="orgStateMail" maxlength="50" style="width:30px;" value="#HTMLEditFormat(orgStateMail)#" /> &nbsp;&nbsp;
      ZIP:
      <input type="text" name="orgZIPMail" maxlength="10" style="width:78px;" value="#HTMLEditFormat(orgZIPMail)#" />
    </div>
  </div>
  <div class="row">
	<div class="col-30-right">Web Address:</div>
    <div class="col-68-left">
      <input type="text" name="orgWebAddress" maxlength="150" style="width:320px;" value="#HTMLEditFormat(orgWebAddress)#" />
    </div>
  </div> --->
  <div class="row">
	<div class="col-30-right">E-Mail Address:</div>
    <div class="col-68-left">
      <input type="text" name="orgEmail" maxlength="100" style="width:320px;" value="#HTMLEditFormat(orgEmail)#" />
    </div>
  </div>
  <div class="row">
	<div class="col-30-right">Phone:</div>
    <div class="col-68-left">
      <input type="text" name="orgPhone" maxlength="50" style="width:138px;" value="#HTMLEditFormat(orgPhone)#" /> &nbsp;&nbsp;
      Fax:
      <input type="text" name="orgFax" maxlength="50" style="width:138px;" value="#HTMLEditFormat(orgFax)#" />
    </div>
  </div>
  <div class="row">
	<div class="col-30-right">Logo:</div>
    <div class="col-68-left">
      <input type="hidden" name="orgLogoFile" value="" />
      <CF_SelectFile formName="frmEditItem" fieldName="orgLogoFile" previewID="preview4" previewWidth="150">
      <small>(148px x 148px)</small>
      <cfif Compare(orgLogoFile, "")>
      <div id="preview4"><img src="/#orgLogoFile#" border="0" width="150" /></div>  
      <cfelse>
      <div id="preview4"></div>   
      </cfif>
    </div>
  </div>
  <div class="row">
	<div class="col-30-right">Description:</div>
    <div class="col-68-left">
      <!--- <textarea name="orgDescription" style="width:320px;height:50px">#HTMLEditFormat(orgDescription)#</textarea> --->
      <CF_CreateTinyMCEButton
            tinyMCEInstallationURL="#Application.tinyMCEInstallationURL#"
            siteURLRoot="#APPLICATION.siteURLRoot#"
            stylesheetURL="#APPLICATION.siteStyleSheetURLForWYSIWYG#"
            assetRelativeDirectory="#Application.assetRelativeDirectory#"
            templateListURL="#Application.tinyMCEInstallationURL#/extension/lists/template_list.js"
            useAbsoluteURL="true"
            forceBrNewlines="true"
            formName="frmEditItem"
            fieldName="orgDescription"
            cols="60"
            rows="3"
            HTMLContent="#orgDescription#"
            displayHTMLSource="true"
            editorHeader="Edit Description">
    </div>
  </div>
</div>
</form>
</cfoutput>