<cfparam name="URL.catID" type="numeric"><!--- navGroupID --->
<cfparam name="URL.navID" type="numeric">

<cfif URL.navID IS 0>
	<br />
	<p>You can't edit the root element.</p>
<cfelse>

<cfset NM=CreateObject("component", "com.NavigationAdmin").init()>
<cfset navDetail=NM.getNavByNavID(URL.navID)>

<cfset TM=CreateObject("component", "com.TemplateAdmin").init()>
<cfset templates=TM.getTemplates()>

<br />
<cfoutput query="navDetail">
<form id="frmEditNav" name="frmEditNav">
<input type="hidden" name="navGroupID" value="#URL.catID#">
<input type="hidden" name="targetNavID" value="#URL.navID#">
<input type="hidden" name="navID" value="#URL.navID#">
<input type="hidden" name="originalURLQueryString" value="#URLQueryString#" />
<table border="0" cellpadding="5" cellspacing="0" width="100%">
  <tr valign="top">
    <td width="50%">
	  <strong>Navigation Name</strong><br />
	  <input type="text" name="navName" maxlength="50" style="width:280px;" value="#HTMLEditFormat(navName)#" validate="required:true"><br />
	  Page Title<br />
	  <input type="text" name="pageTitle" maxlength="100" style="width:280px;" value="#HTMLEditFormat(pageTitle)#"><br />
	  <small>(Enter a title here if it's different from the navigation name.)</small>
	</td>
	<td width="50%">
	  <input type="checkbox" name="published" value="1"<cfif published> checked</cfif>> Publish (appear in navigation).<br />
	  <input type="checkbox" name="popup" value="1"<cfif popup> checked</cfif>> This page will show in a pop-up window.<br />
	  <input type="checkbox" name="SSLEnabled" value="1"<cfif SSLEnabled> checked</cfif>> This page will be secured by SSL encryption.	  	  
	</td>
  </tr>
  <tr valign="top">
    <td width="50%">
    Image <input type="hidden" name="navImage" value="#HTMLEditFormat(navImage)#">
    <CF_SelectFile formName="frmEditNav" fieldName="navImage" previewID="preview3">
    <div id="preview3"><cfif compare(navImage,"")><img src="/#navImage#" border="0" /></cfif></div>
    </td>
    <td width="50%">
    Image (Hover) <input type="hidden" name="navImageOver" value="#HTMLEditFormat(navImageOver)#">
    <CF_SelectFile formName="frmEditNav" fieldName="navImageOver" previewID="preview4">
    <div id="preview4"><cfif compare(navImageOver,"")><img src="/#navImageOver#" border="0" /></cfif></div>
    </td>
  </tr>
  <tr valign="top">
    <td colspan="2">
    <b>Template</b>
    <select name="templateID" style="min-width:250px;">
      <option value="0"<cfif Not Compare(navDetail.templateID,0)> selected</cfif>></option>
      <cfloop query="templates">
        <option value="#templateID#" <cfif Not Compare(navDetail.templateID,templates.templateID)> selected</cfif>>#templateName#</option>
      </cfloop>
    </select>
    </td>
  </tr>
  
  <tr valign="top">
    <td colspan="2">
	  <strong>Select what type of page this navigation item will link to?</strong><br />
	  <select name="pageType">
		<option value="Nothing"<cfif pageType IS "Nothing"> selected</cfif>>No link.</option>
	    <option value="Page Builder"<cfif pageType IS "Page Builder"> selected</cfif>>A page that I created using Page Builder.</option>
        <option value="Sub-Home Page"<cfif pageType IS "Sub-Home Page"> selected</cfif>>A sub-home page I created using Home Page Manager</option>
	    <option value="Newsroom"<cfif pageType IS "Newsroom"> selected</cfif>>A Newsroom index page.</option>
	    <option value="Form"<cfif pageType IS "Form"> selected</cfif>>A form that I created using Form/E-Mail List Manager.</option>
	    <option value="Signup Form"<cfif pageType IS "Signup Form"> selected</cfif>>An e-mail sign-up form that I created using Form/E-Mail List Manager.</option>
	    <option value="Personnel"<cfif pageType IS "Personnel"> selected</cfif>>A Personnel index page.</option>
        <option value="Gallery"<cfif pageType IS "Gallery"> selected</cfif>>A Photo Gallery index page.</option>
	    <option value="Event"<cfif pageType IS "Event"> selected</cfif>>An Event index page</option>
	    <option value="Misc"<cfif pageType IS "Misc"> selected</cfif>>A miscellaneous page</option>
	    <option value="External Link"<cfif pageType IS "External Link"> selected</cfif>>An URL.</option>
	  </select>	
	</td>
  </tr>
  <tr valign="top">
	<td colspan="2">
	<div id="pageParameters2"></div>
	</td>
  </tr>
</table>
</form>
</cfoutput>
</cfif>