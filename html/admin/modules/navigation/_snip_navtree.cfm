<cfparam name="URL.catID">
<cfparam name="URL.expandedNavID" default="0">
<cfparam name="URL.checkedNavID" default="0">
<cfset NM=CreateObject("component", "com.NavigationAdmin").init()>
<cfset navGroupName=NM.getGroupNameByNavGroupID(URL.catID)>
<cfset nav=NM.getNavTreeByNavGroupID(URL.catID)>

<!--- Determine which treeitems to expand. Level one items are always displayed --->
<cfset expandedNavLevelNum=NM.getLevelNumByNavID(URL.expandedNavID)>
<cfif expandedNavLevelNum IS 2>
  <cfset expandedNavID2=NM.getParentNavID(URL.expandedNavID)>
<cfelse>
  <cfset expandedNavID2=0>
</cfif>

<cfif nav.numSubNavs IS 0>
<p>
No navigation items have been created in the navigation group. To create the first navigation item, click on the "New" button in the tool bar.
</p>
</cfif>

<p id="treecontrol"><a href="#">Collapse All</a> | <a href="#">Expand All</a> | <a href="#">Toggle All</a></p>

<cfoutput>
<form id="frmNavTree">
<ul id="navTree">
  <li>
    <input type="radio" name="navID" value="0" <cfif nav.numSubNavs IS 0 OR URL.checkedNavID IS "0">checked</cfif> />
    <a class="tooltip" href="##tooltip0">#navGroupName#</a>
    <div id="tooltip0" style="display:none;">
      This is the navigation root. It can not be edited, deleted, or moved. 
    </div>
    <cfif nav.numSubNavs GT 0>
      <ul>
      <cfloop index="idx1" from="1" to="#nav.numSubNavs#"><!--- Level One --->
        <cfif nav.nav[idx1].navID IS URL.expandedNavID OR nav.nav[idx1].navID IS expandedNavID2><cfset expand="true"><cfelse><cfset expand="false"></cfif>
	    <cfif nav.nav[idx1].navID IS URL.checkedNavID><cfset check="checked"><cfelse><cfset check=""></cfif>
        <li <cfif Not expand>class="closed"</cfif>>
          <cfset navName=nav.nav[idx1].navName>
          <cfif Not nav.nav[idx1].published>
			<cfset navName='<span class="lite">#navName#</span>'>				
		  </cfif>
          <input type="radio" name="navID" value="#nav.nav[idx1].navID#" #check# />
          <a class="tooltip" href="##tooltip#nav.nav[idx1].navID#" onclick="editNavItem('#nav.nav[idx1].navID#')">#navName#</a>
          <div id="tooltip#nav.nav[idx1].navID#" style="display:none;">
            <b>#nav.nav[idx1].navName#</b><cfif NOT nav.nav[idx1].published> (Non-published)</cfif><br />
            Page Title: #nav.nav[idx1].pageTitle#<br />
            Page Type: #nav.nav[idx1].pageType#
            <cfif Compare(nav.nav[idx1].pageType,"Nothing")>
            <br />URL: #nav.nav[idx1].resolvedURL#
            </cfif>
          </div>
          <cfif nav.nav[idx1].numSubNavs GT 0>
            <ul>
            <cfloop index="idx2" from="1" to="#nav.nav[idx1].numSubNavs#"><!--- Level Two --->
              <cfif nav.nav[idx1].nav[idx2].navID IS URL.expandedNavID OR nav.nav[idx1].nav[idx2].navID IS expandedNavID2>
			    <cfset expand="true"><cfelse><cfset expand="false">
			  </cfif>
              <cfif nav.nav[idx1].nav[idx2].navID IS URL.checkedNavID><cfset check="checked"><cfelse><cfset check=""></cfif>
              <li <cfif Not expand>class="closed"</cfif>>
                <cfset navName=nav.nav[idx1].nav[idx2].navName>
                <cfif Not nav.nav[idx1].nav[idx2].published>
				  <cfset navName='<span class="lite">#navName#</span>'>				
				</cfif>
                <input type="radio" name="navID" value="#nav.nav[idx1].nav[idx2].navID#" #check# />
                <a class="tooltip" href="##tooltip#nav.nav[idx1].nav[idx2].navID#" onclick="editNavItem('#nav.nav[idx1].nav[idx2].navID#')">#navName#</a>
                <div id="tooltip#nav.nav[idx1].nav[idx2].navID#" style="display:none;">
                  <b>#nav.nav[idx1].nav[idx2].navName#</b><cfif NOT nav.nav[idx1].nav[idx2].published> (Non-published)</cfif><br />
                  Page Title: #nav.nav[idx1].nav[idx2].pageTitle#<br />
                  Page Type: #nav.nav[idx1].nav[idx2].pageType#
                  <cfif Compare(nav.nav[idx1].nav[idx2].pageType,"Nothing")>
                  <br />URL: #nav.nav[idx1].nav[idx2].resolvedURL#
                  </cfif>
                </div>
                <cfif nav.nav[idx1].nav[idx2].numSubNavs GT 0>
                  <ul>
                  <cfloop index="idx3" from="1" to="#nav.nav[idx1].nav[idx2].numSubNavs#"><!--- Level Three --->
                    <cfif nav.nav[idx1].nav[idx2].nav[idx3].navID IS URL.checkedNavID><cfset check="checked"><cfelse><cfset check=""></cfif>
					<li>
                    <cfset navName=nav.nav[idx1].nav[idx2].nav[idx3].navName>
                    <cfif Not nav.nav[idx1].nav[idx2].nav[idx3].published>
					  <cfset navName='<span class="lite">#navName#</span>'>				
					</cfif>
                    <input type="radio" name="navID" value="#nav.nav[idx1].nav[idx2].nav[idx3].navID#" #check# />
                    <a class="tooltip" href="##tooltip#nav.nav[idx1].nav[idx2].nav[idx3].navID#" onclick="editNavItem('#nav.nav[idx1].nav[idx2].nav[idx3].navID#')">#navName#</a>
                    <div id="tooltip#nav.nav[idx1].nav[idx2].nav[idx3].navID#" style="display:none;">
                      <b>#nav.nav[idx1].nav[idx2].nav[idx3].navName#</b><cfif NOT nav.nav[idx1].nav[idx2].nav[idx3].published> (Non-published)</cfif><br />
                      Page Title: #nav.nav[idx1].nav[idx2].nav[idx3].pageTitle#<br />
                      Page Type: #nav.nav[idx1].nav[idx2].nav[idx3].pageType#
                      <cfif Compare(nav.nav[idx1].nav[idx2].nav[idx3].pageType,"Nothing")>
                      <br />URL: #nav.nav[idx1].nav[idx2].nav[idx3].resolvedURL#
                      </cfif>
                    </div>
                    </li>
                  </cfloop>
                  </ul>
                </cfif>
              </li>
            </cfloop>
            </ul>
          </cfif>
        </li>    
      </cfloop>
      </ul>
    </cfif>
  </li>
</ul>
</form>
</cfoutput>
