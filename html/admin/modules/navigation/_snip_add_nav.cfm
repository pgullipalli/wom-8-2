<cfparam name="URL.catID" type="numeric"><!--- navGroupID --->
<cfparam name="URL.navID" type="numeric">
<cfparam name="URL.insert" default="0">

<cfset TM=CreateObject("component", "com.TemplateAdmin").init()>
<cfset templates=TM.getTemplates()>

<form id="frmAddNav" name="frmAddNav">
<cfoutput>
<input type="hidden" name="navGroupID" value="#URL.catID#">
<input type="hidden" name="targetNavID" value="#URL.navID#">
<input type="hidden" name="insertMode" value="#URL.insert#">
</cfoutput>
<table border="0" cellpadding="5" cellspacing="0" width="100%">
  <tr valign="top">
    <td width="50%">
	  <b>Navigation Name</b><br />
	  <input type="text" name="navName" maxlength="50" style="width:280px;" validate="required:true"><br />
	  Page Title<br />
	  <input type="text" name="pageTitle" maxlength="100" style="width:280px;"><br />
	  <small>(Enter a title here if it's different from the navigation name.)</small>
	</td>
	<td width="50%">
	  <input type="checkbox" name="published" value="1" checked> Publish (appear in navigation).<br />
	  <input type="checkbox" name="popup" value="1"> This page will show in a pop-up window.<br />
	  <input type="checkbox" name="SSLEnabled" value="1"> This page will be secured by SSL encryption.
	</td>
  </tr>
  
  <tr valign="top">
    <td width="50%">
    Image <input type="hidden" name="navImage" value="">
    <CF_SelectFile formName="frmAddNav" fieldName="navImage" previewID="preview1">
    <div id="preview1"></div>
    </td>
    <td width="50%">
    Image (Hover) <input type="hidden" name="navImageOver" value="">
    <CF_SelectFile formName="frmAddNav" fieldName="navImageOver" previewID="preview2">
    <div id="preview2"></div>
    </td>
  </tr> 
  <tr valign="top">
    <td colspan="2">
    <b>Template</b>
    <select name="templateID" style="min-width:250px;">
      <option value="0"></option>
      <cfoutput query="templates">
        <option value="#templateID#">#templateName#</option>
      </cfoutput>
    </select>
    </td>
  </tr>
  
  <tr valign="top">
    <td colspan="2">
	  <b>Select what type of page this navigation item will link to?</b><br />
	  <select name="pageType">
		<option value="Nothing">No link.</option>
	    <option value="Page Builder">A page that I created using Page Builder.</option>
        <option value="Sub-Home Page">A sub-home page I created using Home Page Manager</option>
	    <option value="Newsroom">A Newsroom index page.</option>
	    <option value="Form">A form that I created using Form/E-Mail List Manager.</option>
	    <option value="Signup Form">An e-mail sign-up form that I created using Form/E-Mail List Manager.</option>
	    <option value="Personnel">A Personnel index page.</option>
        <option value="Gallery">A Photo Gallery index Page.</option>
	    <option value="Event">An Event index page</option>
	    <option value="Misc">A miscellaneous page</option>
	    <option value="External Link">An URL.</option>
	  </select>	
	</td>
  </tr>
  <tr valign="top">
	<td colspan="2">
	<div id="pageParameters"></div>
	</td>
  </tr>
</table>
</form>