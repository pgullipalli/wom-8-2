<cfsetting enablecfoutputonly="yes">
<cfparam name="URL.task">

<cfscript>
  NM=CreateObject("component", "com.NavigationAdmin").init();
  returnedStruct=StructNew();
  returnedStruct.SUCCESS=true;
  returnedStruct.MESSAGE="";
  returnedStruct.EXPANDEDNAVID="0";
  returnedStruct.CHECKEDNAVID="0";

  switch (URL.task) {
    case "addNav":
	  if (NOT NM.addNav(argumentCollection=Form)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="It appears that you were trying to add a navigation item to a level that's exceeding the limit. Please make sure you have checked an appropriate navigation item as the parent item.";
	  } else {
	    if (URL.task Is "addNav") {
	      returnedStruct.EXPANDEDNAVID=FORM.targetNavID;
          returnedStruct.CHECKEDNAVID=FORM.targetNavID;
	    } else {
	      returnedStruct.EXPANDEDNAVID=NM.getParentNavID(FORM.targetNavID);
          returnedStruct.CHECKEDNAVID=FORM.targetNavID;
	    }
	  }
	  break;	  

    case "editNav":
	  NM.editNav(argumentCollection=Form);
	  returnedStruct.EXPANDEDNAVID=NM.getParentNavID(FORM.targetNavID);
	  returnedStruct.CHECKEDNAVID=FORM.targetNavID;
      break;

    case "deleteNav":
	  parentNavID=NM.getParentNavID(URL.navID);
	  if (NOT NM.deleteNav(URL.navID)) {
	    returnedStruct.SUCCESS=false;
	    returnedStruct.MESSAGE="It appears that the navigation item you are deleting either contains other child navigation items or is referenced by a template.<br />" &
	  		 "Please delete all child items before delete this navigation item.";	
	  } else {
	    returnedStruct.EXPANDEDNAVID=parentNavID;
	    returnedStruct.CHECKEDNAVID=URL.navID;
      }
	  break;

    case "copyNav":
	  newNavID=NM.copyNav(argumentCollection=Form);
	  returnedStruct.EXPANDEDNAVID=NM.getParentNavID(FORM.navID);
	
	  returnedStruct.CHECKEDNAVID=newNavID;
      break;

    case "insertNav":
	  parentNavID=NM.getParentNavID(URL.navID);
	  newNavID=NM.insertNav(URL.navID, URL.catID);
	  returnedStruct.EXPANDEDNAVID=parentNavID;
	  returnedStruct.CHECKEDNAVID=newNavID;
      break;

    case "moveUpNav":
	  parentNavID=NM.getParentNavID(URL.navID);
	  NM.moveUpNav(URL.navID);
	  returnedStruct.EXPANDEDNAVID=parentNavID;
	  returnedStruct.CHECKEDNAVID=URL.navID;
      break;

    case "moveDownNav":
	  parentNavID=NM.getParentNavID(URL.navID);
	  NM.moveDownNav(URL.navID);
	  returnedStruct.EXPANDEDNAVID=parentNavID;
	  returnedStruct.CHECKEDNAVID=URL.navID;
      break;
	  
	default:
	  break;
  }
</cfscript>
<cfoutput>#Trim(SerializeJSON(returnedStruct))#</cfoutput>
</cfsetting>