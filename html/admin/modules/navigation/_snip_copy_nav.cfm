<cfparam name="URL.catID" type="numeric"><!--- navGroupID --->
<cfparam name="URL.navID" type="numeric">

<cfif URL.navID IS 0>	
	<p>You can't copy the root element.</p>
<cfelse>
  <cfset NM=CreateObject("component", "com.NavigationAdmin").init()>
  <cfset navGroupName=NM.getGroupNameByNavGroupID(URL.catID)>
  <cfset navDetail=NM.getNavByNavID(URL.navID)>
  <cfset nav=NM.getNavTreeByNavGroupID(URL.catID)>
  <cfoutput>
  <p><b>Copy a navigation item from "#navDetail.navName#".</b></p>
  <form id="frmNavTreeCopy" onsubmit="return false">
  <input type="hidden" name="navGroupID" value="#URL.catID#">
  <input type="hidden" name="targetNavID" value="#URL.navID#">
  <p><b>Select a navigation item that the duplicated item will be placed below it:</b></p>
  <ul id="navTreeCopy">
    <li> 
      #navGroupName#
      <cfif nav.numSubNavs GT 0>
        <ul>
        <cfloop index="idx1" from="1" to="#nav.numSubNavs#"><!--- Level One --->
          <cfset navName=nav.nav[idx1].navName>
          <cfif Not nav.nav[idx1].published><cfset navName="<span class='lite'>#navName#</span>"></cfif>
          <li>
          <input type="radio" name="navID" value="#nav.nav[idx1].navID#" /> #navName#
          <cfif nav.nav[idx1].numSubNavs GT 0>
        	<ul>
            <cfloop index="idx2" from="1" to="#nav.nav[idx1].numSubNavs#"><!--- Level Two --->
			  <cfset navName=nav.nav[idx1].nav[idx2].navName>
              <cfif Not nav.nav[idx1].nav[idx2].published><cfset navName="<span class='lite'>#navName#</span>"></cfif>
              <li>
              <input type="radio" name="navID" value="#nav.nav[idx1].nav[idx2].navID#" /> #navName#
              <cfif nav.nav[idx1].nav[idx2].numSubNavs GT 0>
                <ul>
                <cfloop index="idx3" from="1" to="#nav.nav[idx1].nav[idx2].numSubNavs#"><!--- Level Three --->
                  <cfset navName=nav.nav[idx1].nav[idx2].nav[idx3].navName>
                  <cfif Not nav.nav[idx1].nav[idx2].nav[idx3].published><cfset navName="<span class='lite'>#navName#</span>"></cfif>
                  <li>
                    <input type="radio" name="navID" value="#nav.nav[idx1].nav[idx2].nav[idx3].navID#" /> #navName# 
                  </li>
                </cfloop>
                </ul>
              </cfif>
              </li>
            </cfloop>
            </ul>
          </cfif>
          </li>
        </cfloop>
        </ul>
      </cfif>
    </li>
  </ul>
  </form>
  </cfoutput>
</cfif>