<cfset newline=Chr(13) & Chr(10)>
<cfset htmlHead='<link rel="stylesheet" href="/jsapis/jquery/css/treeview/jquery.treeview.css" type="text/css" />' & newline>
<cfset htmlHead=htmlHead & '<link rel="stylesheet" href="/jsapis/jquery/css/tooltip/jquery.tooltip.css" />' & newline>
<cfset htmlHead=htmlHead & '<script src="/jsapis/jquery/js/jquery.treeview.min.js" type="text/javascript"></script>' & newline>
<cfset htmlHead=htmlHead & '<script src="/jsapis/jquery/js/jquery.tooltip.min.js" type="text/javascript"></script>' & newline>
<cfset htmlHead=htmlHead & '<script type="text/javascript" src="modules/navigation/home.js"></script>' & newline>
<cfhtmlhead text="#htmlHead#">

<cfparam name="URL.catID" default="0">

<cfset NM=CreateObject("component", "com.NavigationAdmin").init()>
<cfif URL.catID IS 0>
  <cfset URL.catID=NM.getFirstNavGroupID()>  
</cfif>
<cfset navGroupName=NM.getGroupNameByNavGroupID(URL.catID)>
<cfset allGroups=NM.getAllNavGroups()>

<cfoutput>
<div class="header">Navigation Manager &gt; #navGroupName#</div>
</cfoutput>

<div id="actionBar">
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="addNavItem()">New</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="editNavItem()">Edit</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="deleteNavItem()">Delete</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button65" onclick="copyNavItem()">Copy</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="insertBelow()">Insert Below</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="moveUp()">Move Up</a>
<a href="#" class="fg-button ui-state-default ui-corner-all button110" onclick="moveDown()">Move Down</a>
</div>

<br /><br />

<p>
<form id="frmNavigator">
Navigation Group
<select name="navGroupID">
  <cfoutput query="allGroups">
  <option value="#navGroupID#"<cfif navGroupID IS URL.catID> selected</cfif>>#groupName#</option>
  </cfoutput>
</select>
</form>
</p>

<div id="mainDiv"></div>