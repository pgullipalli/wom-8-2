<cfparam name="URL.pageType" type="string">
<cfparam name="URL.URLQueryString" type="string" default="">
<cfparam name="URL.navID" type="string" default="0">
<cfparam name="URL.editMode" type="boolean" default="0">

<cfswitch expression="#URL.pageType#">
  <cfcase value="Page Builder">
	<cfset PB=CreateObject("component", "com.PageBuilderAdmin").init()>
	<cfset allPages=PB.getAllPages()>
	<b>Select a page</b><br />
	<select name="URLQueryString" id="URLQueryString"  style="min-width:280px;">
      <cfif allPages.recordcount Is 0>
      <option value="">No pages were created</option>
      </cfif>
	  <cfoutput query="allPages"><cfset qStr="md=pagebuilder&tmp=home&pid=#pageID#">
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>[#pageCategoryName#] #pageName#</option>
	  </cfoutput>
	</select>
  </cfcase>
  <cfcase value="Sub-Home Page">
    <cfset HP=CreateObject("component", "com.HomepageAdmin").init()>
    <cfset  allSubHomePages=HP.getAllSubHomePages()>
    <b>Select a sub-home page</b><br />
    <select name="URLQueryString" id="URLQueryString"  style="min-width:280px;">
      <cfif allSubHomePages.recordcount Is 0>
      <option value="">No pages were created</option>
      </cfif>
	  <cfoutput query="allSubHomePages"><cfset qStr="md=homepage&tmp=subhome&shpid=#subHomePageID#">
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>#subHomePageName#</option>
	  </cfoutput>
	</select>  
  </cfcase>
  <cfcase value="Newsroom">
    <cfset NR=CreateObject("component", "com.NewsroomAdmin").init()>
	<cfset allCats=NR.getAllArticleCategories()>
	<b>Select a newsroom index page</b><br />
	<select name="URLQueryString" id="URLQueryString" style="min-width:280px;">
      <cfif allCats.recordcount Is 0>
      <option value="">No pages were created</option>
      </cfif>
      <cfoutput query="allCats"><cfset qStr="md=newsroom&tmp=category&catid=#articleCategoryID#">
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>#articleCategory#</option>
	  </cfoutput>
	</select>
  </cfcase>
  <cfcase value="Form">
    <cfset FM=CreateObject("component", "com.FormAdmin").init()>
	<cfset allForms=FM.getAllFormNames()>
	<b>Select a form</b><br />
	<select name="URLQueryString" id="URLQueryString" style="min-width:280px;">
      <cfif allForms.recordcount Is 0>
      <option value="">No forms were created</option>
      </cfif>
      <cfoutput query="allForms"><cfset qStr="md=form&tmp=home&fmid=#formID#">
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>#formName#</option>
	  </cfoutput>
	</select>
  </cfcase>
  <cfcase value="Signup Form">
    <cfset CM=CreateObject("component", "com.CommunicationAdmin").init()>
	<cfset allSignupForms=CM.getSignUpFormsSummary()>
	<b>Select a sign-up form</b><br />
	<select name="URLQueryString" id="URLQueryString" style="min-width:280px;">
	  <cfif allSignupForms.recordcount Is 0>
      <option value="">No forms were created</option>
      </cfif>
      <cfoutput query="allSignupForms"><cfset qStr="md=communication&tmp=signup&sfmid=#signUpFormID#">
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>#signUpFormName#</option>
	  </cfoutput>
	</select>
  </cfcase>
  <cfcase value="Gallery">
    <cfset PG=CreateObject("component", "com.PhotoGalleryAdmin").init()>
	<cfset allCats=PG.getAllAlbumCategories()>
    <b>Select a photo gallery category</b><br />
	<select name="URLQueryString" id="URLQueryString" style="min-width:280px;">
      <cfif allCats.recordcount Is 0>
      <option value="">No galleries were created</option>
      </cfif>
      <cfoutput query="allCats"><cfset qStr="md=photogallery&tmp=category&catid=#albumCategoryID#">
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>#albumCategory#</option>
	  </cfoutput>
	</select>
  </cfcase>
  <cfcase value="Event">
    <cfset EV=CreateObject("component", "com.EVentAdmin").init()>
	<cfset allCats=EV.getAllCategories()>
	<b>Select an event index page</b><br />
	<select name="URLQueryString" id="URLQueryString" style="min-width:280px;">
      <cfif allCats.recordcount Is 0>
      <option value="">No pages were created</option>
      </cfif>
      <cfoutput query="allCats"><cfset qStr="md=event&tmp=category&catid=#categoryID#">
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>#categoryName#</option>
	  </cfoutput>
	</select>
  </cfcase>
  <cfcase value="Misc">
	<!--- MISC PAGES --->
	<cfoutput>
	<b>Select a miscellaneous page</b><br />
	<select name="URLQueryString" id="URLQueryString" style="min-width:280px;">
	  <cfset qStr="md=homepage&tmp=home">
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>Home Page</option>          
      <cfset qStr="md=doctor&tmp=home">
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>Doctor Directory</option>
      <cfset qStr="md=class&tmp=home">
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>Programs & Classes Landing Page</option>
      <cfset qStr="md=class&tmp=schedule_fitness">
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>Fitness Club Schedule</option>
      <cfset qStr="md=directory&tmp=home">
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>Locations &amp; Maps Landing Page</option>
	  <cfset qStr="md=photogallery&tmp=home">      
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>Photo Gallery Landing Page</option>
      <cfset qStr="md=customform&tmp=donation1">
	  <option value="#qStr#"<cfif Not Compare(qStr,URL.URLQueryString)> selected</cfif>>Donation Form</option>
	</select>
	</cfoutput>
  </cfcase>
  <cfcase value="External Link">
	<cfif URL.editMode>
	  <cfset NM=CreateObject("component", "com.NavigationAdmin").init()>
	  <cfset navDetail=NM.getNavByNavID(URL.navID)>
	  <cfset externalLink=navDetail.externalLink>
	  <cfset iframe=navDetail.iframe>
	<cfelse>
	  <cfset externalLink="">
	  <cfset iframe=0>
	</cfif>
	
	<b>Enter an URL</b><br />
	<cfoutput>
	<input type="text" name="externalLink" style="width:500px;" value="#HTMLEditFormat(externalLink)#"><br />
	<input type="checkbox" name="iframe" value="1"<cfif iframe> checked</cfif>> This page should be included in a frame.
	</cfoutput>
  </cfcase>
</cfswitch>
<cfif Compare(URL.pageType,"External Link") AND Compare(URL.pageType,"Nothing")>
<a href="#" onclick="window.open('/index.cfm?'+ xsite.getFormFieldValue(document.getElementById('URLQueryString')), '_blank')">Preview</a>
</cfif>