$(function () {
	loadNavigationTree();
	
	$("#frmNavigator select[name='navGroupID']").change(function () {
	  loadNavigationTree();
	});
});



function loadNavigationTree(expandedNavID, checkedNavID) {
  var catID=xsite.getSelectedValue($("#frmNavigator")[0].navGroupID);
  var url = "index.cfm?md=navigation&tmp=snip_navtree&catID=" + catID + "&uuid=" + xsite.getUUID();
  if (expandedNavID) {
	url = url + "&expandedNavID=" + expandedNavID;
  }	  
  if (checkedNavID) {
	url = url + "&checkedNavID=" + checkedNavID;
  }
  xsite.load("#mainDiv", url, function() {
	$("#navTree").treeview({ animated: "medium", control: "#treecontrol" });
	
	$("#navTree a.tooltip").tooltip({
		bodyHandler: function() {
			return $($(this).attr("href")).html();
		},
		showURL: false
	});
  });
}

/*--------------------------------------*/
/*    BEGIN: Add New Navigation Item    */
/*--------------------------------------*/	
function addNavItem() {
  //check to see if any nav item is selected
  var navID=xsite.getCheckedValue($("#frmNavTree")[0].navID);
  
  if (!navID) {
	alert ("Please select an item that the new item will become its child item.");
	return;
  }
  var catID=$("#frmNavigator select[name='navGroupID']").val();
  
  xsite.createModalDialog({
	windowName: 'winAddNav',
	title: 'Add Navigation Item',
	position: 'center-up',
	width: 650,
	url: 'index.cfm?md=navigation&tmp=snip_add_nav&catID=' + catID + '&navID=' + navID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {//dynamically load the page selection drop-down
	  $("#frmAddNav select[name='pageType']").change(function() {
		var pageType = $("#frmAddNav select[name='pageType']").val();
		xsite.load("#pageParameters", "index.cfm?md=navigation&tmp=snip_pageselect&pageType=" + escape(pageType)) + '&uuid=' + xsite.getUUID();
	  });		  
	},
	buttons: {
	  'Cancel': function() { $("#winAddNav").dialog('close'); },
	  ' Add ': submitForm_AddNavItem
	}
  }).dialog('open');
  

  function submitForm_AddNavItem() {
	var form = $("#frmAddNav");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=navigation&task=addNav',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winAddNav").dialog('close');
		loadNavigationTree(jsonData.EXPANDEDNAVID, jsonData.CHECKEDNAVID);			
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}
/*--------------------------------------*/
/*    END: Add New Navigation Item      */
/*--------------------------------------*/

/*--------------------------------------*/
/*    BEGIN: Edit Navigation Item       */
/*--------------------------------------*/	
function editNavItem(nID) {
  //check to see if any nav item is selected
  
  var navID=xsite.getCheckedValue($("#frmNavTree")[0].navID);
  if (nID) navID = nID;  
  
  if (!navID) {
	alert ("Please select an item that you want to edit.");
	return;
  } else if (navID == "0") {
	alert ("You can't edit the root element. It's not a navigation item.");
	return;
  }
  var catID=$("#frmNavigator select[name='navGroupID']").val();
  
  xsite.createModalDialog({
	windowName: 'winEditNav',
	title: 'Edit Navigation Item',
	position: 'center-up',
	width: 650,
	url: 'index.cfm?md=navigation&tmp=snip_edit_nav&catID=' + catID + '&navID=' + navID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {//dynamically load the page selection drop-down
	  reloadPageSelection();//load pre-selected page
	  $("#frmEditNav select[name='pageType']").change(reloadPageSelection);		  
	},
	buttons: {
	  'Cancel': function() { $("#winEditNav").dialog('close'); },
	  ' Save ': submitForm_EditNavItem
	}
  }).dialog('open');

  function reloadPageSelection () {
	var pageType = $("#frmEditNav select[name='pageType']").val();
	var URLQueryString = $("#frmEditNav input[name='originalURLQueryString']").val();
	xsite.load("#pageParameters2", "index.cfm?md=navigation&tmp=snip_pageselect&navID=" + navID + "&pageType=" + escape(pageType) + "&URLQueryString=" + escape(URLQueryString) + "&editMode=1&uuid=" + xsite.getUUID());
  }
  
  function submitForm_EditNavItem() {
	var form = $("#frmEditNav");
	form.validate();
	if (form.valid()) {
		form.ajaxSubmit({ 
		  url: 'action.cfm?md=navigation&task=editNav',
		  type: 'post',
		  cache: false,
		  dataType: 'json', 
		  success: processJson,
		  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
		});
	} else { return; }

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winEditNav").dialog('close');
		loadNavigationTree(jsonData.EXPANDEDNAVID, jsonData.CHECKEDNAVID);			
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }//submitForm_EditNavItem	  
}
/*--------------------------------------*/
/*    END: Edit Navigation Item         */
/*--------------------------------------*/

function deleteNavItem() {
  //check to see if any nav item is selected
  var navID=xsite.getCheckedValue($("#frmNavTree")[0].navID);
  
  if (!navID) {
	alert ("Please select an item that you want to delete.");
	return;
  } else if (navID == "0") {
	alert ("You can't delete the root element. It's not a navigation item.");
	return;
  }
  
  if (!confirm('Are you sure you want to delete the selected navigation item?')) {
	return;
  } else {		
	//show waiting dialog
	xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=navigation&task=deleteNav&navID=' + navID, {}, processJson, 'json');}});
  }
  
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadNavigationTree(jsonData.EXPANDEDNAVID, jsonData.CHECKEDNAVID);	
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }	  
}

function copyNavItem() {
  //check to see if any nav item is selected
  var navID=xsite.getCheckedValue($("#frmNavTree")[0].navID);
  
  if (!navID) {
	alert ("Please select an item to copy from.");
	return;
  } else if (navID == "0") {
	alert ("You can't copy the root element. It's not a navigation item.");
	return;
  }
  var catID=$("#frmNavigator select[name='navGroupID']").val();
  
  //window.open('index.cfm?md=navigation&tmp=snip_copy_nav&catID=' + catID + '&navID=' + navID + '&uuid=' + xsite.getUUID()); return;
  xsite.createModalDialog({
	windowName: 'winCopyNav',
	title: 'Copy Navigation Item',
	width: 650,
	position: 'top',
	url: 'index.cfm?md=navigation&tmp=snip_copy_nav&catID=' + catID + '&navID=' + navID + '&uuid=' + xsite.getUUID(),
	urlCallback: function() {
	  $("#frmNavTreeCopy").click(function(e) {
		if($(e.target).attr("type") == "checkbox") xsite.resetCheckBoxes(e.target);
	  });	
	},
	buttons: {
	  'Cancel': function() { $("#winCopyNav").dialog('close'); },
	  ' Copy ': submitForm_CopyNavItem
	}
  }).dialog('open');
  
  
  function submitForm_CopyNavItem() {
	$('#frmNavTreeCopy').ajaxSubmit({ 
	  url: 'action.cfm?md=navigation&task=copyNav',
	  type: 'post',
	  cache: false,
	  dataType: 'json', 
	  beforeSubmit: validate,
	  success: processJson,
	  error : function (XMLHttpRequest, textStatus) { alert ("Error: " + textStatus); }
	});
  
	function validate(formData, jqForm, options) {
	  var navID=xsite.getCheckedValue($("#frmNavTreeCopy")[0].navID);
	  if (!navID) {
		alert("Please check off a navigation item that the duplicated item will be placed below it.");
		return false;
	  }
	}

	function processJson(jsonData) {
	  if (jsonData.SUCCESS) {
		$("#winCopyNav").dialog('close');
		loadNavigationTree(jsonData.EXPANDEDNAVID, jsonData.CHECKEDNAVID);			
	  } else {
		alert (jsonData.MESSAGE);
	  }
	}
  }
}

function insertBelow() {
  //check to see if any nav item is selected
  var navID=xsite.getCheckedValue($("#frmNavTree")[0].navID);
  
  if (!navID) {
	alert ("Please select an item that the new item will be placed below it.");
	return;
  } else if (navID == "0") {
	alert ("You can't select the root element. It's not a navigation item.");
	return;
  }
  var catID=$("#frmNavigator select[name='navGroupID']").val();
  
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=navigation&task=insertNav&catID=' + catID + '&navID=' + navID, {}, processJson, 'json');}});
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadNavigationTree(jsonData.EXPANDEDNAVID, jsonData.CHECKEDNAVID);	
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }
}

function moveUp() {
  //check to see if any nav item is selected
  var navID=xsite.getCheckedValue($("#frmNavTree")[0].navID);
  
  if (!navID) {
	alert ("Please select an item to move up.");
	return;
  } else if (navID == "0") {
	alert ("You can't select the root element. It's not a navigation item.");
	return;
  }
  
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=navigation&task=moveUpNav&navID=' + navID, {}, processJson, 'json');}});

  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadNavigationTree(jsonData.EXPANDEDNAVID, jsonData.CHECKEDNAVID);	
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }
}

function moveDown() {
  //check to see if any nav item is selected
  var navID=xsite.getCheckedValue($("#frmNavTree")[0].navID);
  
  if (!navID) {
	alert ("Please select an item to move down.");
	return;
  } else if (navID == "0") {
	alert ("You can't select the root element. It's not a navigation item.");
	return;
  }
  
  xsite.showWaitingDialog({openCallback:function() {$.get('action.cfm?md=navigation&task=moveDownNav&navID=' + navID, {}, processJson, 'json');}});
	
  function processJson(jsonData) {
	if (jsonData.SUCCESS) {
	  //remove waiting dialog
	  xsite.closeWaitingDialog();
	  loadNavigationTree(jsonData.EXPANDEDNAVID, jsonData.CHECKEDNAVID);	
	} else {
	  xsite.closeWaitingDialog();
	  xsite.showAlertDialog(jsonData.MESSAGE);
	}
  }
}