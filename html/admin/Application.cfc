<cfcomponent output="false">
  <cfset this.name = "WomansHospitalAdminDev2">
  <cfset this.clientmanagement="False">
  <cfset this.sessionmanagement="True">
  <cfset this.sessiontimeout="#createtimespan(0,2,0,0)#">
  <cfset this.applicationtimeout="#createtimespan(1,0,0,0)#">
  <cfset this.siteInstallationPath="#findSiteInstallationPath()#">
  <!--- set component path mapping; now we can use 'com' to reference CF component path --->
  <cfset this.mappings["/com"]=this.siteInstallationPath & "/components">
  <cfset this.mappings["/modules"]=this.siteInstallationPath & "/html/admin/modules">
  <cfset this.customtagpaths=this.siteInstallationPath & "/customtags">

 
  <cffunction name="onApplicationStart">
	<cfset initializeApplication()>	
  </cffunction>
  
  <cffunction name="onSessionStart">
    <cfset initializeSession()>
  </cffunction>


  <cffunction name="onRequestStart">
    <cfparam name="URL.md" default=""><!--- module --->
	<cfparam name="URL.tmp" default=""><!--- module template --->
	<cfparam name="URL.catID" default="0">
	<cfparam name="URL.task" default="">
	<cfparam name="URL.ajax" default="1">
	<cfparam name="URL.wrap" default="0">

	<!---<cfset Request.startTicks=getTickcount()>--->
    
    <cfif FindNoCase("/admin/modules/", CGI.SCRIPT_NAME) GT 0><cfabort></cfif><!--- unauthorized module files access attempt --->
	
	
	<cfif IsDefined("URL.resetApplication")> <!--- initialize application environment variables --->
	  <cfset initializeApplication()>	
	</cfif>

	<cfif IsDefined("URL.resetSession") OR Not IsDefined("Session.access")>
	  <cfset initializeSession()>
	</cfif>

    
    <!--- log admin activities --->
    <cfif Compare(URL.task,"")>
    <cftry>
      <cfset adminLog=CreateObject("component","com.AdminLog").init()>
      <cfset argStruct=StructNew()>
      <cfset argStruct.adminID=Session.access.administratorID>
      <cfset argStruct.module=URL.md>
      <cfset argStruct.task=URL.task>
      <cfset argStruct.actionURL="action.cfm?" & CGI.QUERY_STRING>
      <cfset adminLog.addLog(argumentCollection=argStruct)>
    <cfcatch></cfcatch>
    </cftry>
    </cfif>
	
	<cfif Compare(URL.md, "")>
      <cfif Not Compare(URL.md, "administration") And (ListFind("login,sessionexpires,displaymessage", URL.tmp) GT 0 Or ListFind("login,logout,checkLogin,verifyLogin",URL.task) GT 0)>
        <!--- no need to authenticate 'login' and 'message display' pages; free to go --->
        <!--- no need to authenticate 'login' & 'logout' action; free to go --->
	  <cfelse>
	    <cflock scope="session" type="exclusive" timeout="30">
	      <cfset returnedStruct=Session.access.authenticate(argumentCollection=URL)>	  
	      <cfif Not returnedStruct.success>
	        <cfset Session.access.setMessage(returnedStruct.message)>
		    <cfif Compare(URL.task, "")>
			  <cflocation url="index.cfm?md=administration&tmp=displaymessage&wrap=#URL.wrap#&ajax=#URL.ajax#&uuid=#CreateUUID()#">
			<cfelse>			  
	          <cflocation url="index.cfm?md=administration&tmp=displaymessage&wrap=#URL.wrap#&ajax=0&uuid=#CreateUUID()#">
			</cfif>	        
	      </cfif>
	    </cflock>
	  </cfif>
	</cfif>
    
    <!--- update site search re-indexing status --->
    <cfif Compare(URL.task, "") OR Not Compare(URL.tmp,"snip_upload_action")>
	  <cfset argsStruct=StructNew()>
	  <cfset argsStruct.md=URL.md>
      <cfset argsStruct.tmp=URL.tmp>
	  <cfset argsStruct.task=URL.task>	  
	  <cfif IsDefined("URL.dir")>
	    <cfset argsStruct.dir=URL.dir>
      <cfelseif IsDefined("Form.relativeDirectory")>
        <cfset argsStruct.dir=Form.relativeDirectory>
	  </cfif>
	  <cfset SM=CreateObject("component", "com.SiteSearchAdmin").init()>
      <cfset SM.updateSiteSearchIndexStatus(argumentCollection=argsStruct)>
	</cfif>
  </cffunction>
  
  <cffunction name="initializeSession" access="private" output="false">
    <cflock scope="session" type="exclusive" timeout="30">
      <cfset Session.access=CreateObject("component", "com.Access").init()>
	</cflock>
  </cffunction>
  
  <cffunction name="initializeApplication" access="private" output="false">
	<!--- 
	Define these application variables:
	Application.siteInstallationPath: absolute path of the directory this application was installed
	Application.siteDirectoryRoot: absolute path of the root HTML directory for the site
	Application.siteAdminDirectoryRoot: absolute path of the root HTML directory for the admin site
	
	Here is a list all pre-definded Application variables:
	siteInstallationPath, siteDirectoryRoot, siteAdminDirectoryRoot, privilegeXMLObj
	--->

	  <cflock scope="Application" timeout="30" type="exclusive"> 
	  <cfset Application.siteInstallationPath=this.siteInstallationPath>
	  <cfset Application.siteDirectoryRoot=Application.siteInstallationPath & "/html">
	  <cfset Application.siteAdminDirectoryRoot=Application.siteInstallationPath & "/html/admin">
	  <cfset Application.modulePath=Application.siteInstallationPath & "/html/admin/modules">
      <cfset configFilePath=Application.siteInstallationPath & "/config/sitewide.xml">  
	  <cfset privilegeFilePath=Application.siteInstallationPath & "/config/privilege.xml">

	  <cfset initializeSiteWideVariables(configFilePath)>
	  <cffile action="read" file="#privilegeFilePath#" variable="privilegeXml">

      <cfset Application.privilegeXMLObj=XMLParse(privilegeXml)>
      </cflock>

  </cffunction>

  <cffunction name="initializeSiteWideVariables" access="private" output="false">
    <cfargument name="filePath" type="string" required="yes">  
	<cffile action="read" file="#Arguments.filePath#" variable="sitewideXml">	
	<cfscript>
	  xmlObj=XMLParse(sitewideXml);
	  varObjArray = xmlObj.XmlRoot.variable;
	  for (i=1; i LTE ArrayLen(varObjArray); i=i+1) {
	    varObj=varObjArray[i];
		scope=varObj.XmlAttributes.scope;
		switch (scope) {
		  case "application":
		    if (Not CompareNoCase(varObj.XmlAttributes.type, "string")) {
		      Application[varObj.XmlAttributes.name]=EvaluateCustomValue(varObj.XmlAttributes.value);		      
			} else if (Not CompareNoCase(varObj.XmlAttributes.type, "struct")) {
			  structName = varObj.XmlAttributes.name;
			  if (Not IsDefined("Application.#structName#")) {
			    Application[structName] = StructNew();
			  }
			  memberObjArray = varObj.member;
			  for (j=1; j LTE ArrayLen(memberObjArray); j=j+1) {
			    memberObj = memberObjArray[j];
				attName = memberObj.XmlAttributes.key;
				Application[structName][attName] = EvaluateCustomValue(memberObj.XmlAttributes.value);
			  }
			}
			break;
		  case "request":
		    if (Not CompareNoCase(varObj.XmlAttributes.type, "string")) {
		      Request[varObj.XmlAttributes.name]=EvaluateCustomValue(varObj.XmlAttributes.value);
			} else if (Not CompareNoCase(varObj.XmlAttributes.type, "struct")) {
			  structName = varObj.XmlAttributes.name;
			  if (Not IsDefined("REQUEST.#structName#")) {
			    Request[structName] = StructNew();
			  }
			  memberObjArray = varObj.member;
			  for (j=1; j LTE ArrayLen(memberObjArray); j=j+1) {
			    memberObj = memberObjArray[j];
				attName = memberObj.XmlAttributes.key;
				Request[structName][attName] = EvaluateCustomValue(memberObj.XmlAttributes.value);
			  }
			}
			break;
		  default:
		    break;
		}
	  }
	</cfscript>
  </cffunction>

  <cffunction name="findSiteInstallationPath" access="private" returntype="string">
	<cfset currentPath=Replace(ExpandPath("."), "\", "/", "All")>
	<cfreturn Mid(currentPath, 1, Find("/html/admin", currentPath)-1)>	
  </cffunction>

  <cffunction  name="EvaluateCustomValue" access="private" output="false" returntype="string">
    <cfargument name="inputString" type="string" required="true">
	<cfscript>
	  startIndex=Find("$$", Arguments.inputString);
	  if (startIndex GT 0) {
	    endIndex=Find("$$", Arguments.inputString, startIndex+1);
	    if (endIndex GT 0) {
	      variableName=Mid(Arguments.inputString, startIndex + 2, endIndex-startIndex-2);
	      if (IsDefined("Application.#variableName#")) {
	      	return Replace(Arguments.inputString, "$$#variableName#$$", Application["#variableName#"], "All");
	      } else {
	      	return Replace(Arguments.inputString, "$$#variableName#$$", "", "All");
	      }
	    }
	  }	
	  return Arguments.inputString;
	</cfscript>
  </cffunction>
  
  <cffunction name="onError" output="yes" returnType="string">
    <cfargument name="Exception" required="true">
    <cfargument name="EventName" type="String" required="true">
    <cfsetting enablecfoutputonly="yes">
    <cfif FindNoCase("action.cfm", CGI.SCRIPT_NAME)>
      <cfset returnedStruct=StructNew()>
      <cfset returnedStruct.SUCCESS=false>
      <cfset returnedStruct.MESSAGE="Error Message: " & arguments.Exception.message & Chr(13) & Chr(10) & Chr(13) & Chr(10) & "Detail: " & arguments.Exception.detail>
      <cfoutput>#SerializeJSON(returnedStruct)#</cfoutput>
    <cfelse>
      <cfoutput>Error Message: #arguments.Exception.message#<br /><br />Detail: #arguments.Exception.detail#</cfoutput>
    </cfif>
    </cfsetting>
  </cffunction>
</cfcomponent>