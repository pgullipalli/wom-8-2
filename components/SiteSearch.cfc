<cfcomponent displayname="Site Search" extends="Base" output="no">
  <cffunction name="siteSearch" access="public" output="no" returntype="struct">
    <cfargument name="keyword" type="string" required="yes">
	<cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
	
	<cfset var resultStruct=StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numPages=0>
	<cfset resultStruct.numDisplayedItems=0>
	
	<cfset resultStruct.id=ArrayNew(1)>
	<cfset resultStruct.pagename=ArrayNew(1)>
	<cfset resultStruct.pagetype=ArrayNew(1)>
	<cfset resultStruct.searchType=ArrayNew(1)>
    <cfset resultStruct.category=ArrayNew(1)>
    <cfset resultStruct.teaser=ArrayNew(1)>
	
	<cfset DBCollectionname=getSiteContentCollectionName("db")>
	<cfset FileCollectionname=getSiteContentCollectionName("file")>	
    
    <cfsearch collection="#DBCollectionname#"
			    name="getDBResults"
			    criteria="#LCase(Arguments.keyword)#">    

	<cfsearch collection="#FileCollectionname#"
		  criteria="#LCase(Arguments.keyword)#"
		  name="getFileResults">    
    
    <cfset resultStruct.numAllItems=getDBResults.recordcount + getFileResults.recordcount>
					
	<cfset endIndex = Arguments.startIndex + Arguments.numItemsPerPage - 1>
	
	<cfif resultStruct.numAllItems GTE endIndex>
	  <cfset resultStruct.numDisplayedItems=Arguments.numItemsPerPage>
	<cfelse>
	  <cfset resultStruct.numDisplayedItems=resultStruct.numAllItems - Arguments.startIndex + 1>
	  <cfset endIndex = resultStruct.numAllItems>
	</cfif>
	  
	<cfset resultStruct.numPages=Ceiling(resultStruct.numAllItems/Arguments.numItemsPerPage)>
	
	<cfset idx=0>
	<cfset idx1=0>
	<cfloop query="getDBResults">
	  <cfset idx1 = idx1 + 1>
	  <cfif idx1 GTE Arguments.startIndex AND idx1 LTE endIndex>	  
		<cfset idx = idx + 1>
		<cfset resultStruct.id[idx]=Mid(key,2, Len(key)-1)>
		<cfset resultStruct.pagename[idx]=title>
		<cfset resultStruct.pagetype[idx]=custom1>
		<cfset resultStruct.popup[idx]=custom2>
        <cfset resultStruct.category[idx]=custom3>
		<cfset resultStruct.searchType[idx]="DB">
	  </cfif>
	</cfloop>
    <cfloop query="getFileResults">
	  <cfset idx1 = idx1 + 1>
	  <cfif idx1 GTE Arguments.startIndex AND idx1 LTE endIndex>	  
		<cfset idx = idx + 1>
		<cfset resultStruct.id[idx]=URL>
		<cfset resultStruct.pagename[idx]=getFileFromPath(key)>
		<cfset resultStruct.pagetype[idx]="#NumberFormat(size/1000, "999,999.9")# KB">
		<cfset resultStruct.popup[idx]=0>
        <cfset resultStruct.category[idx]="Resource Library">
		<cfset resultStruct.searchType[idx]="File">
	  </cfif>
	</cfloop>	
    
	<cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="classSearch" access="public" output="no" returntype="struct">
    <cfargument name="keyword" type="string" required="yes">
	<cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
	
	<cfset var resultStruct=StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numPages=0>
	<cfset resultStruct.numDisplayedItems=0>
	
	<cfset resultStruct.id=ArrayNew(1)>
	<cfset resultStruct.pagename=ArrayNew(1)>
	<cfset resultStruct.pagetype=ArrayNew(1)>
	<cfset resultStruct.searchType=ArrayNew(1)>
    <cfset resultStruct.category=ArrayNew(1)>
    <cfset resultStruct.teaser=ArrayNew(1)>
	
	<cfset DBCollectionname=getSiteContentCollectionName("class")>
    
    <cfsearch collection="#DBCollectionname#"
			    name="getDBResults"
			    criteria="#LCase(Arguments.keyword)#">
    
    <cfset resultStruct.numAllItems=getDBResults.recordcount>
					
	<cfset endIndex = Arguments.startIndex + Arguments.numItemsPerPage - 1>
	
	<cfif resultStruct.numAllItems GTE endIndex>
	  <cfset resultStruct.numDisplayedItems=Arguments.numItemsPerPage>
	<cfelse>
	  <cfset resultStruct.numDisplayedItems=resultStruct.numAllItems - Arguments.startIndex + 1>
	  <cfset endIndex = resultStruct.numAllItems>
	</cfif>
	  
	<cfset resultStruct.numPages=Ceiling(resultStruct.numAllItems/Arguments.numItemsPerPage)>
	
	<cfset idx=0>
	<cfset idx1=0>
	<cfloop query="getDBResults">
	  <cfset idx1 = idx1 + 1>
	  <cfif idx1 GTE Arguments.startIndex AND idx1 LTE endIndex>	  
		<cfset idx = idx + 1>
		<cfset resultStruct.id[idx]=Mid(key,2, Len(key)-1)>
		<cfset resultStruct.pagename[idx]=title>
		<cfset resultStruct.pagetype[idx]=custom1>
		<cfset resultStruct.popup[idx]=custom2>
        <cfset resultStruct.teaser[idx]=custom3>
		<cfset resultStruct.searchType[idx]="DB">        
	  </cfif>
	</cfloop>
    
	<cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="directorySearch" access="public" output="no" returntype="struct">
    <cfargument name="keyword" type="string" required="yes">
	<cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
	
	<cfset var resultStruct=StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numPages=0>
	<cfset resultStruct.numDisplayedItems=0>
	
	<cfset resultStruct.id=ArrayNew(1)>
	<cfset resultStruct.pagename=ArrayNew(1)>
	<cfset resultStruct.pagetype=ArrayNew(1)>
	<cfset resultStruct.searchType=ArrayNew(1)>
    <cfset resultStruct.category=ArrayNew(1)>
    <cfset resultStruct.teaser=ArrayNew(1)>
	
	<cfset DBCollectionname=getSiteContentCollectionName("directory")>
    
    <cfset Arguments.keyword=Replace(Arguments.keyword,"& ","","All")>
    <cfset Arguments.keyword=Replace(Replace(Arguments.keyword,"(","","All"),")","","All")>
    
    <cfsearch collection="#DBCollectionname#"
			    name="getDBResults"
			    criteria="#LCase(Arguments.keyword)#">
    
    <cfset resultStruct.numAllItems=getDBResults.recordcount>
					
	<cfset endIndex = Arguments.startIndex + Arguments.numItemsPerPage - 1>
	
	<cfif resultStruct.numAllItems GTE endIndex>
	  <cfset resultStruct.numDisplayedItems=Arguments.numItemsPerPage>
	<cfelse>
	  <cfset resultStruct.numDisplayedItems=resultStruct.numAllItems - Arguments.startIndex + 1>
	  <cfset endIndex = resultStruct.numAllItems>
	</cfif>
	  
	<cfset resultStruct.numPages=Ceiling(resultStruct.numAllItems/Arguments.numItemsPerPage)>
	
	<cfset idx=0>
	<cfset idx1=0>
	<cfloop query="getDBResults">
	  <cfset idx1 = idx1 + 1>
	  <cfif idx1 GTE Arguments.startIndex AND idx1 LTE endIndex>	  
		<cfset idx = idx + 1>
		<cfset resultStruct.id[idx]=Mid(key,2, Len(key)-1)>
		<cfset resultStruct.pagename[idx]=title>
		<cfset resultStruct.pagetype[idx]=custom1>
		<cfset resultStruct.popup[idx]=custom2>
        <cfset resultStruct.teaser[idx]=custom3>
		<cfset resultStruct.searchType[idx]="DB">        
	  </cfif>
	</cfloop>
    
	<cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="getSiteContentCollectionName" access="public" output="false" returntype="string">
    <cfargument name="collectionType" type="string" default="db">
  
    <cfquery name="qCollectionName" datasource="#this.datasource#">
	  SELECT collectionName
	  FROM ss_collection
	  WHERE collectionType=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.collectionType#">
	</cfquery>
	<cfif qCollectionName.recordcount>
	  <cfreturn qCollectionName.collectionName[1]>
	<cfelse>
	  <cfreturn "">
	</cfif>
  </cffunction> 
</cfcomponent>