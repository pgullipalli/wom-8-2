<cfcomponent displayname="Custom Form Manager" extends="Base" output="false">
  <cffunction name="getFormInfo" access="public" output="no" returntype="query">
    <cfargument name="formID" type="numeric" required="yes">
    
    <cfquery name="qFormInfo" datasource="#this.datasource#">
      SELECT *
      FROM fm_cf_form
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    
    <cfreturn qFormInfo>
  </cffunction>
  
  <cffunction name="updateFormSetting" access="public" output="no">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="introductionText" type="string" default="">
    <cfargument name="notificationEmailSubject" type="string" required="yes">
    <cfargument name="notificationEmailReplyAddress" type="string" required="yes">
    <cfargument name="notificationEmailRecipients" type="string" default="">
    <cfargument name="thankyouEmailSubject" type="string" required="yes">
    <cfargument name="thankyouEmailReplyAddress" type="string" required="yes">
    <cfargument name="thankyouMessage" type="string" default="">
    
    <cfquery datasource="#this.datasource#">
      UPDATE fm_cf_form
      SET introductionText=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.introductionText#">,
      	  notificationEmailSubject=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.notificationEmailSubject#">,
      	  notificationEmailReplyAddress=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.notificationEmailReplyAddress#">,
          notificationEmailRecipients=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.notificationEmailRecipients#">,
          thankyouEmailSubject=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.thankyouEmailSubject#">,
          thankyouEmailReplyAddress=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.thankyouEmailReplyAddress#">,
          thankyouMessage=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.thankyouMessage#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>  
  </cffunction>
  
  <cffunction name="getAllForms" access="public" output="no" returntype="query">
    <cfquery name="qForms" datasource="#this.datasource#">
      SELECT formID, formName
      FROM fm_cf_form
      ORDER BY formID
    </cfquery>
    <cfreturn qForms>
  </cffunction>

  <cffunction name="getSubmissions" access="public" output="no" returntype="struct">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.submissions=QueryNew("submissionID")>
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
    
    <cfquery name="qNumSubmissions" datasource="#this.datasource#">
      SELECT COUNT(submissionID) AS numSubmisions
      FROM fm_cf_submission
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    <cfset resultStruct.numAllItems = qNumSubmissions.numSubmisions>
    
    <cfquery name="qSubmissions" datasource="#this.datasource#">
      SELECT fm_cf_submission.submissionID, fm_cf_submission.submissionDate, fm_cf_submission.firstName,
      		 fm_cf_submission.lastName, fm_cf_submission.email, fm_cf_submission.amount, fm_cf_submission.sourceCode, fm_cf_source.sourceName
      FROM fm_cf_submission LEFT JOIN fm_cf_source ON fm_cf_submission.sourceCode=fm_cf_source.sourceCode
      WHERE fm_cf_submission.formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
      ORDER BY fm_cf_submission.submissionDate DESC      
      LIMIT #Arguments.startIndex-1#, #arguments.numItemsPerPage#
    </cfquery>
    
    <cfset resultStruct.numDisplayedItems=qSubmissions.recordcount>	
    <cfset resultStruct.submissions = qSubmissions>
    
    <cfreturn resultStruct>
  </cffunction>

  <cffunction name="getSubmissionHTML" access="public" output="no" returntype="string">
    <cfargument name="submissionID" type="numeric" required="yes">
    
    <cfquery name="qSubmission" datasource="#this.datasource#">
      SELECT submissionDataHTML	 
      FROM fm_cf_submission
      WHERE submissionID=<cfqueryparam cfsqltype="cf_sql_integer" value="#URL.submissionID#">
    </cfquery>  
  
    <cfif qSubmission.recordcount>
      <cfreturn qSubmission.submissionDataHTML>
    <cfelse>
      <cfreturn "No submission found.">
    </cfif>
  </cffunction>
  
  <cffunction name="deleteSubmission" access="public" output="no">
    <cfargument name="submissionIDList" type="string" required="yes">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_cf_submission
      WHERE submissionID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.submissionIDList#" list="yes">)
    </cfquery>
  </cffunction>
  
  <cffunction name="exportSubmissionData" access="public" output="no" returntype="string">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="startDate" type="string" required="yes">
  	<cfargument name="endDate" type="string" required="yes">
    <cfargument name="fileDirectory" type="string" required="yes">


    <!--- delete old files first --->
    <cfdirectory action="list" directory="#Arguments.fileDirectory#" name="allFiles">
    <cfloop query="allFiles">
      <cffile action="delete" file="#Arguments.fileDirectory##name#">
    </cfloop>
    
    <cfset formInfo=getFormInfo(Arguments.formID)>
            
    <cfset Variables.startDate=DateAdd("d", 0, Arguments.startDate)>
    <cfset Variables.endDate=DateAdd("d", 1, Arguments.endDate)>
    
    <cfset newline=Chr(13) & Chr(10)>
  
    <cfquery name="qSubmissions" datasource="#this.datasource#">
      SELECT <cfif Not Compare(formInfo.exportFileFormat,"Text")>
      		 submissionDate, submissionDataText
             <cfelse>
             submissionDate, firstName, lastName, email, amount
             </cfif>
      FROM fm_cf_submission
      WHERE submissionDate >=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDate#"> AND
      	    submissionDate < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDate#"> AND
            formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">      
      ORDER BY submissionDate ASC
    </cfquery>
    
    <cfif Not Compare(formInfo.exportFileFormat,"Text")>
      <cfsavecontent variable="allSubmissions">
<cfoutput query="qSubmissions">
==================================================================
Submission Date: #DateFormat(submissionDate, "mm/dd/yyyy")# #TimeFormat(submissionDate, "h:mm:ss tt")#
==================================================================

#TRIM(submissionDataText)#

</cfoutput>
      </cfsavecontent>
    
      <cfset fileName="#Replace(formInfo.formName," ","_","All")#_#DateFormat(Variables.startDate, "yyyy-mm-dd")#_#DateFormat(Variables.endDate, "yyyy-mm-dd")#.txt">
    <cfelse>
      <cfsavecontent variable="allSubmissions"><cfoutput>Date Submitted,Name,Email,Amount</cfoutput><cfoutput query="qSubmissions">#newline##DateFormat(submissionDate,"mm/dd/yyyy")# #TimeFormat(submissionDate, "h:mm:ss tt")#,"#Replace(firstName,'"','""','All')# #Replace(lastName,'"','""','All')#","#Replace(email,'"','""','All')#",#DollarFormat(amount)#</cfoutput></cfsavecontent>    
      <cfset fileName="#Replace(formInfo.formName," ","_","All")#_#DateFormat(Variables.startDate, "yyyy-mm-dd")#_#DateFormat(Variables.endDate, "yyyy-mm-dd")#.csv">
    </cfif>
    
    <cfset filePath="#fileDirectory#" & fileName>        
	<cffile action="write" file="#filePath#" output="#Trim(allSubmissions)#" nameconflict="overwrite">
    <cfreturn fileName>
  </cffunction>
  
  <!--- <cffunction name="getSourceSummary" access="public" output="no" returntype="query">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="startDate" type="string" required="yes">
    <cfargument name="endDate" type="string" required="yes">
    
    <cfset Variables.startDate=DateAdd("d",0,Arguments.startDate)>
    <cfset Variables.endDate=DateAdd("d",1,Arguments.endDate)>
    
    <cfquery name="qSources" datasource="#this.datasource#">
      SELECT fm_cf_source.sourceID, fm_cf_source.sourceCode, fm_cf_source.sourceName, COUNT(fm_cf_submission.submissionID) AS numSubmissions
      FROM fm_cf_source LEFT JOIN fm_cf_submission ON fm_cf_source.sourceCode=fm_cf_submission.sourceCode
      WHERE fm_cf_source.formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
      		fm_cf_submission.submissionDate >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDate#"> AND
			fm_cf_submission.submissionDate < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDate#">
      GROUP BY fm_cf_source.sourceID
      ORDER BY fm_cf_source.sourceName
    </cfquery>
  
    <cfreturn qSources>
  </cffunction> --->
  
  <cffunction name="getSourceTracking" access="public" output="no" returntype="query">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="startDate" type="string" required="yes">
    <cfargument name="endDate" type="string" required="yes">
    
    <cfset Variables.startDate=DateAdd("d",0,Arguments.startDate)>
    <cfset Variables.endDate=DateAdd("d",1,Arguments.endDate)>
    
    <cfquery name="qTracking" datasource="#this.datasource#">
      SELECT sourceCode, COUNT(fm_cf_submission.submissionID) AS numSubmissions
      FROM fm_cf_submission
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
      		submissionDate >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDate#"> AND
			submissionDate < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDate#">
      GROUP BY sourceCode
    </cfquery>
  
    <cfreturn qTracking>
  </cffunction>
  
  <cffunction name="getAllSources" access="public" output="no" returntype="query">
    <cfargument name="formID" type="numeric" required="yes">
    
    <cfquery name="qSources" datasource="#this.datasource#">
      SELECT *
      FROM fm_cf_source
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
      ORDER BY sourceName
    </cfquery>
  
    <cfreturn qSources>
  </cffunction>
  
  <cffunction name="getSourceByID" access="public" output="no" returntype="query">
    <cfargument name="sourceID" type="numeric" required="yes">
    
    <cfquery name="qSource" datasource="#this.datasource#">
      SELECT *
      FROM fm_cf_source
      WHERE sourceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.sourceID#">
    </cfquery>
  
    <cfreturn qSource>
  </cffunction>
  
  <cffunction name="getSourceByCode" access="public" output="no" returntype="query">
    <cfargument name="sourceCode" type="numeric" required="yes">
    
    <cfquery name="qSource" datasource="#this.datasource#">
      SELECT *
      FROM fm_cf_source
      WHERE sourceCode=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sourceCode#">
    </cfquery>
  
    <cfreturn qSource>
  </cffunction>
  
  <cffunction name="addSource" access="public" output="no" returntype="void">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="sourceName" type="string" required="yes">
    <cfargument name="sourceCode" type="string" required="yes">
    
    <cfquery name="qName" datasource="#this.datasource#">
      INSERT INTO fm_cf_source (formID, sourceName, sourceCode, dateCreated, dateLastModified)
      VALUES (
        <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sourceName#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sourceCode#">,
        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      )
    </cfquery>   
  </cffunction>
  
  <cffunction name="editSource" access="public" output="no" returntype="void">
    <cfargument name="sourceID" type="numeric" required="yes">
    <cfargument name="sourceName" type="string" required="yes">
    <cfargument name="sourceCode" type="string" required="yes">
    
    <cfquery name="qName" datasource="#this.datasource#">
      UPDATE fm_cf_source
      SET sourceName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sourceName#">,
          sourceCode=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sourceCode#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE sourceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.sourceID#">
    </cfquery>   
  </cffunction>
  
  <cffunction name="deleteSource" access="public" output="no" returntype="void">
    <cfargument name="sourceID" type="numeric" required="yes">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_cf_source
      WHERE sourceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.sourceID#">
    </cfquery>
  </cffunction>
</cfcomponent>