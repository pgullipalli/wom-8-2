<cfcomponent displayname="Promotion Administrator" extends="Base" output="false">
  <cffunction name="getAllPlacements" access="public" output="no" returntype="query">
    <cfquery name="qPlacements" datasource="#this.datasource#">
      SELECT *
      FROM pm_placement
      ORDER BY displaySeq
    </cfquery>
  
    <cfreturn qPlacements>
  </cffunction>

  <cffunction name="getFirstPlacementID" access="public" output="no" returntype="numeric">
    <cfquery name="qPlacement" datasource="#this.datasource#">
      SELECT placementID
      FROM pm_placement
      ORDER BY displaySeq
      LIMIT 0,1
    </cfquery>
    <cfreturn qPlacement.placementID>
  </cffunction>
  
  <cffunction name="getPlacementInfo" access="public" output="no" returntype="query">
    <cfargument name="placementID" type="numeric" required="yes">
    
    <cfquery name="qPlacementInfo" datasource="#this.datasource#">
      SELECT *
      FROM pm_placement
      WHERE placementID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.placementID#">
    </cfquery>
    
    <cfreturn qPlacementInfo>
  </cffunction>
  
  <cffunction name="getAllPromos" access="public" output="no" returntype="query">
    <cfquery name="qPromos" datasource="#this.datasource#">
      SELECT promoID, promoName, published
      FROM pm_promo
      ORDER BY promoName ASC
    </cfquery>
    
    <cfreturn qPromos>
  </cffunction>
  
  <cffunction name="getPromos" access="public" output="no" returntype="struct">
    <cfargument name="placementID" type="numeric" required="yes">
    <cfargument name="categoryID" type="numeric" required="yes">
	<cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
  
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.promo=QueryNew("promoID")>	
  
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
    
    <cfquery name="qAllPromos" datasource="#this.datasource#">
	  SELECT DISTINCT pm_promo.promoID
	  FROM	 pm_promo INNER JOIN pm_promo_placement ON
	  		 pm_promo.promoID=pm_promo_placement.promoID
	  WHERE	 pm_promo_placement.placementID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.placementID#">
      		 <cfif Compare(Arguments.categoryID,-1)> AND
      		 pm_promo_placement.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
             </cfif>
    </cfquery>
	<cfset resultStruct.numAllItems = qAllPromos.recordcount>
  
    <cfquery name="qPromos" datasource="#this.datasource#">
	  SELECT promoID, promoName, startDate, endDate, 
      		 promoImage, published, destinationURL     
	  FROM   pm_promo
	  WHERE	 <cfif qAllPromos.recordcount GT 0>
      		 promoID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qAllPromos.promoID)#" list="yes">)
      		 <cfelse>
      		 1=-1
      		 </cfif>
	  ORDER  BY startDate DESC
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
    <cfif qPromos.recordcount GT 0>            
      <cfset resultStruct.numDisplayedItems=qPromos.recordcount>	
      <cfset resultStruct.promos = qPromos>
	<cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.promos = QueryNew("promoID")>
    </cfif>
    
	<cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="addPromo" access="public" output="no">
    <cfargument name="promoName" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="startDate" type="string" default="">
    <cfargument name="endDate" type="string" default="">
    <cfargument name="promoImage" type="string" required="yes">
    <cfargument name="promoImageOver" type="string" default="">
    <cfargument name="descriptionText" type="string" required="yes">
    <cfargument name="destinationURL" type="string" required="yes">
    <cfargument name="pageID" type="any" required="yes">
    <cfargument name="popup" type="boolean" default="0">
    <cfargument name="placementList" type="string" required="yes">
    
    <cfif Not Compare(Arguments.placementList,"")><cfreturn></cfif>
    
    <cfif Compare(Arguments.startDate,"")>
      <cfset Variables.startDate=DateAdd("d",0,Arguments.startDate)>
    <cfelse>
      <cfset Variables.startDate=CreateDate(1900,1,1)>
    </cfif>
    <cfif Compare(Arguments.endDate,"")>
      <cfset Variables.endDate=DateAdd("d",0,Arguments.endDate)>
    <cfelse>
      <cfset Variables.endDate=CreateDate(3000,1,1)>
    </cfif>
    
    <cfif Not IsNumeric(Arguments.pageID)>
      <cfset Arguments.pageID="0">
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      INSERT INTO pm_promo
      (promoName, startDate, endDate, promoImage, promoImageOver,
       descriptionText, pageID, destinationURL, popup, published,
       dateCreated, dateLastModified)
      VALUES
      (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.promoName#">,
       <cfqueryparam cfsqltype="cf_sql_date" value="#Variables.startDate#">,
       <cfqueryparam cfsqltype="cf_sql_date" value="#Variables.endDate#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.promoImage#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.promoImageOver#">,
       
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.descriptionText#">,
       <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageID#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.destinationURL#">,
       <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.popup#">,
       <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
       
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)       
    </cfquery>
    
    <cfquery name="qNewPromoID" datasource="#this.datasource#">
      SELECT MAX(promoID) AS promoID
      FROM pm_promo
    </cfquery>
    <cfset newPromoID=qNewPromoID.promoID>
    
    <cfloop index="placement" list="#Arguments.placementList#">
      <cfset items=ListToArray(placement,"|")>
      <cfset placementID=items[1]>
      <cfset categoryID=items[2]>
      
      <cfquery datasource="#this.datasource#">
        INSERT INTO pm_promo_placement
        (placementID, categoryID, promoID, rotationSeed, dateCreated)
        VALUES
        (<cfqueryparam cfsqltype="cf_sql_integer" value="#placementID#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#newPromoID#">,
         0,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
      </cfquery>
      <cfset resetRotationSeed(placementID)>
    </cfloop>   
  </cffunction>
  
  <cffunction name="editPromo" access="public" output="no">
    <cfargument name="promoID" type="numeric" required="yes">
    <cfargument name="promoName" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="startDate" type="string" default="">
    <cfargument name="endDate" type="string" default="">
    <cfargument name="promoImage" type="string" required="yes">
    <cfargument name="promoImageOver" type="string" default="">
    <cfargument name="descriptionText" type="string" required="yes">
    <cfargument name="destinationURL" type="string" required="yes">
    <cfargument name="pageID" type="any" required="yes">
    <cfargument name="popup" type="boolean" default="0">
    <cfargument name="placementList" type="string" required="yes">
  
    <cfif Not Compare(Arguments.placementList,"")><cfreturn></cfif>
        
    <cfif Compare(Arguments.startDate,"")>
      <cfset Variables.startDate=DateAdd("d",0,Arguments.startDate)>
    <cfelse>
      <cfset Variables.startDate=CreateDate(1900,1,1)>
    </cfif>
    <cfif Compare(Arguments.endDate,"")>
      <cfset Variables.endDate=DateAdd("d",0,Arguments.endDate)>
    <cfelse>
      <cfset Variables.endDate=CreateDate(3000,1,1)>
    </cfif>
    
    <cfif Not IsNumeric(Arguments.pageID)>
      <cfset Arguments.pageID="0">
    </cfif>
    
    
    <cfquery datasource="#this.datasource#">
      UPDATE pm_promo
      SET promoName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.promoName#">,
      	  startDate=<cfqueryparam cfsqltype="cf_sql_date" value="#Variables.startDate#">,
          endDate=<cfqueryparam cfsqltype="cf_sql_date" value="#Variables.endDate#">,
          promoImage=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.promoImage#">,
          promoImageOver=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.promoImageOver#">,
          descriptionText=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.descriptionText#">,
          pageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageID#">,
          destinationURL=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.destinationURL#">,
          popup=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.popup#">,
          published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery>
    
	<cfquery datasource="#this.datasource#">
      DELETE FROM pm_promo_placement
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery>
    
    <cfloop index="placement" list="#Arguments.placementList#">
      <cfset items=ListToArray(placement,"|")>
      <cfset placementID=items[1]>
      <cfset categoryID=items[2]>
      
      <cfquery datasource="#this.datasource#">
        INSERT INTO pm_promo_placement
        (placementID, categoryID, promoID, rotationSeed, dateCreated)
        VALUES
        (<cfqueryparam cfsqltype="cf_sql_integer" value="#placementID#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">,
         0,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
      </cfquery>
      <cfset resetRotationSeed(placementID)>
    </cfloop> 
  </cffunction>
  
  <cffunction name="deletePromo" access="public" output="no">
    <cfargument name="promoID" type="numeric" required="yes">
    
	<cfset placementInfo=getPromoPlacementInfo(Arguments.promoID)>
	<cfset resetRotationSeed(placementInfo.placementID)>
	
    <cfquery datasource="#this.datasource#">
      DELETE FROM pm_stats
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM pm_promo_placement
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM pm_promo
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery> 
  </cffunction>
  
  <cffunction name="getPromo" access="public" output="no" returntype="query">
    <cfargument name="promoID" type="numeric" required="yes">
  
    <cfquery name="qPromo" datasource="#this.datasource#">
      SELECT *
      FROM pm_promo
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery>
    
    <cfreturn qPromo>
  </cffunction>
  
  <cffunction name="getPromoPlacementInfo" access="public" output="no" returntype="query">
    <cfargument name="promoID" type="numeric" required="yes">
    
    <cfquery name="qPlacementInfo" datasource="#this.datasource#">
      SELECT placementID, categoryID, CONCAT(CAST(placementID AS CHAR) ,'|',CAST(categoryID AS CHAR)) AS placement
      FROM pm_promo_placement
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery>
    
    <cfreturn qPlacementInfo>
  </cffunction>

  <cffunction name="resetRotationSeed" access="public" output="no">
    <cfargument name="placementID" type="numeric" required="yes">
  
    <cfquery datasource="#this.datasource#">
      UPDATE pm_promo_placement
      SET rotationSeed=0
      WHERE placementID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.placementID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="getOverallReport" access="public" output="false" returntype="struct">
    <cfargument name="fromDate" type="string" required="yes">
    <cfargument name="toDate" type="string" required="yes">
    <cfargument name="sortBy" type="string" default="views">
    <cfargument name="sortDirection" type="string" default="DESC">
    
    <cfset var structResult=StructNew()>
    
    <cfset Variables.fromDate=DateAdd("d",0,Arguments.fromDate)>
    <cfset Variables.toDate=DateAdd("d",0,Arguments.toDate)>
	
    <cfquery name="qStats" datasource="#this.datasource#">
	  SELECT pm_promo.promoID,
	  		 pm_promo.promoName,
             pm_promo.promoImage,
			 pm_promo.published,
	  		 SUM(pm_stats.views) AS totalViews,
	  		 SUM(pm_stats.clickthrus) AS totalClickthrus
	  FROM pm_promo LEFT JOIN pm_stats ON
	  	   pm_promo.promoID = pm_stats.promoID
	  WHERE (pm_stats.statsDate >= <cfqueryparam cfsqltype="cf_sql_date" value="#Arguments.fromDate#"> AND
	  		pm_stats.statsDate <= <cfqueryparam cfsqltype="cf_sql_date" value="#Arguments.toDate#">) OR
			pm_stats.statsDate IS NULL
	  GROUP BY pm_promo.promoID
      <cfif Arguments.sortBy IS "views">
      ORDER BY totalViews #Arguments.sortDirection#
      <cfelseif Arguments.sortBy Is "clickthrus">
      ORDER BY totalClickthrus #Arguments.sortDirection#
      <cfelse>
	  ORDER BY pm_promo.promoName #Arguments.sortDirection#
      </cfif>
	</cfquery>
	  
    <cfset structResult.numPromos=qStats.recordcount>
	<cfset structResult.promoName = ArrayNew(1)>
    <cfset structResult.promoImage = ArrayNew(1)>
	<cfset structResult.published = ArrayNew(1)>
	<cfset structResult.views = ArrayNew(1)>
	<cfset structResult.clickthrus = ArrayNew(1)>
	<cfset idx = 0>
	<cfset structResult.maxCount=1>
	<cfloop query="qStats">
	  <cfset idx = idx + 1>
	  <cfset structResult.promoName[idx] = promoName>
      <cfset structResult.promoImage[idx] = promoImage>
	  <cfset structResult.published[idx] = published>
	  <cfif totalViews IS "">
	    <cfset structResult.views[idx] = 0>
	  <cfelse>
	    <cfset structResult.views[idx] = totalViews>
	  </cfif>
	  <cfif structResult.maxCount LT structResult.views[idx]><cfset structResult.maxCount=structResult.views[idx]></cfif>
	  <cfif totalClickthrus IS "">
	    <cfset structResult.clickthrus[idx] = 0>
	  <cfelse>
	    <cfset structResult.clickthrus[idx] = totalClickthrus>
	  </cfif>
	  <cfif structResult.maxCount LT structResult.clickthrus[idx]><cfset structResult.maxCount=structResult.clickthrus[idx]></cfif>
	</cfloop>  
	
	<cfreturn structResult>
  </cffunction>
  
  <cffunction name="getDetailReport" access="public" output="false" returntype="struct">
    <cfargument name="startDate" type="date" required="yes">
	<cfargument name="promoID" type="numeric" required="yes">
	
	<cfset var structResult=StructNew()>	
	
	<cfset VARIABLES.endDate=DateAdd("m", 1, Arguments.startDate)>
	
	<cfquery name="qStats" datasource="#this.datasource#">
	  SELECT statsDate, SUM(views) AS totalViews, SUM(clickthrus) AS totalClickthrus
	  FROM pm_stats
	  WHERE statsDate >= <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.startDate#"> AND
	  	    statsDate < <cfqueryparam cfsqltype="cf_sql_date" value="#VARIABLES.endDate#"> AND
			promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.promoID#">
	  GROUP by statsDate
	  ORDER BY statsDate
	</cfquery>
	
	<cfset structResult.numDays=DaysInMonth(Arguments.startDate)>
    <cfset structResult.statsDate=ArrayNew(1)>
	<cfset structResult.views=ArrayNew(1)>
	<cfset structResult.clickthrus=ArrayNew(1)>	
	
	<cfloop index="idx" from="1" to="#structResult.numDays#">
      <cfset structResult.statsDate[idx]=DateFormat(DateAdd("d",idx-1,Arguments.startDate),"mm/dd/yyyy")>
	  <cfset structResult.views[idx]=0>
	  <cfset structResult.clickthrus[idx]=0>
	</cfloop>	
	
	<cfset structResult.maxCount=1>
	<cfloop query="qStats">
	  <cfif totalViews GT 0>
	    <cfset structResult.views[Day(statsDate)]=totalViews>
		<cfif structResult.maxCount LT totalViews>
		  <cfset structResult.maxCount = totalViews>
		</cfif>
	  </cfif>
	   
	  <cfif totalClickthrus GT 0>
	    <cfset structResult.clickthrus[Day(statsDate)]=totalClickthrus>
		<cfif structResult.maxCount LT totalClickthrus>
		  <cfset structResult.maxCount = totalClickthrus>
		</cfif>
	  </cfif>
	</cfloop>   
    <cfreturn structResult>
  </cffunction>
  
</cfcomponent>