<cfcomponent displayname="USAePay" extends="Base" output="false">
  <cfif IsDefined("Application.USAePayKey")>
    <cfset this.USAePayKey=Application.USAePayKey>
  </cfif>
  <cfif IsDefined("Application.USAePayCGIURL")>
    <cfset this.USAePayCGIURL=Application.USAePayCGIURL>
  </cfif>
  
  
  
  <!---  
  ***** CGI Post Variables (Base Fields) For Credit Cards Process cc:sale, cc:authonly *****
  
  UMcommand		-	Processing Command. Possible values are: cc:sale, cc:authonly, cc:capture, cc:credit, cc:postauth, check:sale, check:credit, void, refund and creditvoid. Default is sale.
  					In here, we only support cc:sale, cc:authonly.
  UMkey			-	The source key (assigned by the server when you created a source in your virtual terminal).
  
  UMname		-	Name on card or checking account. While not required, it is strongly recommended that this field be populated for CC:Sale,
					CC:AuthOnly, CC:PostAuth, CC:Credit, Check:Sale and Check:Credit
  UMcard		-	Credit Card Number with no spaces or dashes.
  UMexpir		-	Expiration Date in the form of MMYY with no spaces or punctuation.
  UMcvv2		-	CVV2 data for Visa. Set to -2 if the code is not legible, -9 if the code is not on the card. While not required, this field should be populated
					for Fraud Prevention and to obtain the best rate for Ecommerce credit card transactions. It should be populated for CC:Sale and CC:AuthOnly
  UMamount		-	Charge amount without $. This is the grand total including tax, shipping and any discounts.  
  UMstreet		-	Billing Street Address for credit cards. Used for Address Verification System. While not required, this field should be populated
					for Fraud Prevention and to obtain the best rate for Ecommerce credit card transactions. It should be populated for CC:Sale and CC:AuthOnly
  UMzip			-	Billing Zip Code for credit cards. Used for Address Verification System. While not required, this field should be populated
					for Fraud Prevention and to obtain the best rate for Ecommerce credit card transactions. It should be populated for CC:Sale and CC:AuthOnly
					
  UMinvoice*	-	Unique Invoice or order number. 10 digits. While not required, it is strongly recommended that this field be populated for CC:Sale,
					CC:AuthOnly, CC:PostAuth, CC:Credit, Check:Sale and Check:Credit
	- or -
  UMorderid*	-	Order identifier. This field can be used to reference the order to which this transaction corresponds.
  					This field can contain up to 64 characters and should be used instead of UMinvoice when orderid is longer that 10 digits.
  UMdescription	-	Description of transaction.
  UMip			-	The IP address of the client requesting the transaction. This is used in many of the fraud modules.
  UMtestmode	-	If UMtestmode is set to 1 the gateway will simulate a transaction without actually processing the card.
					No transaction data is stored when testmode is enabled. You will not see the transaction on reports or in the batch.
					Use of testmode is discouraged for anything more than rudimentary integration testing. For a better simulation of a transaction,
					it is recommended that you use the Sandbox.
  
  ***** CGI Result Variables *****
  UMstatus			-	Status of the transaction. The possible values are: Approved, Declined, Verification and Error.
  UMauthCode		-	Authorization number.
  UMrefNum			-	Transaction reference number
  UMbatch			-	Batch reference number. This will only be returned for sale and auth commands.
						Warning: The batch number returned is for the batch that was open when the transaction was initiated.
						It is possible that the batch was closed while the transaction was processing. In this case the transaction will get queued for the next batch to open.
  UMavsResult		-	AVS result in readable format
  UMavsResultCode	-	AVS result code.
  UMcvv2Result		-	CVV2 result in readable format.
  UMcvv2ResultCode	-	CVV2 result code.
  UMvpasResultCode	-	Verified by Visa (VPAS) or Mastercard SecureCode (UCAF) result code.
  UMresult			-	Transaction result
  UMerror			-	Error description if UMstatus is Declined or Error.
  UMerrorcode		-	A numerical error code.
  UMacsurl			-	Verification URL to send cardholder to. Sent when UMstatus is verification (cardholder authentication is required).
  UMpayload			-	Authentication data to pass to verification url. Sent when UMstatus is verification (cardholder authentication is required).
  UMisDuplicate		-	Indicates whether this transaction is a folded duplicate or not. 'Y' means that this transaction was flagged as duplicate and has already been processed.
  						The details returned are from the original transaction. Send UMignoreDuplicate to override the duplicate folding.
  UMconvertedAmount	-	Amount converted to merchant's currency, when using a multi-currency processor.
  UMconvertedAmountCurrency	-	Merchant's currency, when using a multi-currency processor.
  UMconversionRate	- Conversion rate used to convert currency, when using a multi-currency processor.
  UMcustnum			-	Customer reference number assigned by gateway. Returned only if UMaddcustomer=yes.
  UMresponseHash	-	Response verification hash. Only present if response hash was requested in the UMhash. (See Source Pin Code section for further details)
  UMprocRefNum		-	Transaction Reference number provided by backend processor (platform), blank if not available)
  UMcardLevelResult	-	Card level results (for Visa cards only), blank if no results provided					
  --->
  
  <cffunction name="makeCreditCardPayment" access="public" output="no" returntype="struct">
    <cfreturn makePayment(argumentCollection=Arguments)>
  </cffunction>
  
  <cffunction name="makePayment" access="public" output="no" returntype="struct">
    <cfargument name="paymentMethod" type="string" default="Credit Card">
    <cfargument name="USAePayKey" type="string" required="no">
    <cfargument name="command" type="string" default="cc:sale">     
    <cfargument name="nameOnCard" type="string" required="no">
    <cfargument name="cardNumber" type="string" required="no">
    <cfargument name="expirationDate" type="string" required="no"><!--- MMYY --->
  	<cfargument name="CVV2" type="string" required="no">
    
    <cfargument name="accountType" type="string" required="no">
    <cfargument name="nameOnAccount" type="string" required="no">
    <cfargument name="accountNumber" type="string" required="no">
    <cfargument name="routingNumber" type="string" required="no">    
    
    <cfargument name="amount" type="numeric" required="yes">
    <cfargument name="address" type="string" required="yes">
    <cfargument name="zip" type="string" required="yes">
    <cfargument name="orderID" type="string" required="yes">
    <cfargument name="description" type="string" default="">
    <cfargument name="clientIP" type="string" default="">    
    <cfargument name="testMode" type="string" default="0">
    
    <cfargument name="contactPhone" type="string" required="no">
    
    <cfargument name="billingFirstName" type="string" required="no">
    <cfargument name="billingLastName" type="string" required="no">
    <cfargument name="billingAddress1" type="string" required="no">
    <cfargument name="billingAddress2" type="string" required="no">
    <cfargument name="billingCity" type="string" required="no">
    <cfargument name="billingState" type="string" required="no">
    <cfargument name="billingZip" type="string" required="no">
    <cfargument name="billingPhone" type="string" required="no">
    
    <cfargument name="splitPayments" type="boolean" default="0">
    <cfargument name="lineItems" type="array" required="no">
    
    
    <cfset var returnedStruct=StructNew()>
    <cfset var idx=0>
    
    <cfif Arguments.splitPayments And (Not IsDefined("Arguments.lineItems") OR ArrayLen(Arguments.lineItems) LT 1)>
      <!--- need to provide line item array because each line item will be a separated transaction --->
      <cfset returnedStruct.approved=false>
      <cfset returnedStruct.serverError=true>
      <cfreturn returnedStruct>
    </cfif>
    
    <cfif Not IsDefined("Arguments.orderID")>
      <cfset currentTime=now()>
      <cfset Arguments.orderID=currentTime.getTime()>
    </cfif>
    
    <cftry>
      <cfif Not Arguments.splitPayments>
          <cfhttp method="Post" URL="#this.USAePayCGIURL#">
            <cfhttpparam type="Formfield" name="UMcommand" value="#Arguments.command#">
            <cfif IsDefined("Arguments.USAePayKey")>
              <cfhttpparam type="Formfield" name="UMkey" value="#Arguments.USAePayKey#">
            <cfelse>
              <cfhttpparam type="Formfield" name="UMkey" value="#this.USAePayKey#">
            </cfif>
            <cfif Not CompareNoCase(Arguments.paymentMethod,"Credit Card")>
              <cfhttpparam type="Formfield" name="UMname" value="#Arguments.nameOnCard#">
              <cfhttpparam type="Formfield" name="UMcard" value="#Arguments.cardNumber#">
              <cfhttpparam type="Formfield" name="UMexpir" value="#Arguments.expirationDate#">
              <cfhttpparam type="Formfield" name="UMcvv2" value="#Arguments.CVV2#">
            <cfelse>              
              <cfhttpparam type="Formfield" name="UMname" value="#Arguments.nameOnAccount#">
              <cfhttpparam type="Formfield" name="UMaccounttype" value="#Arguments.accountType#">              
              <cfhttpparam type="Formfield" name="UMaccount" value="#Arguments.accountNumber#">
              <cfhttpparam type="Formfield" name="UMrouting" value="#Arguments.routingNumber#">
            </cfif>            
            <cfhttpparam type="Formfield" name="UMamount" value="#NumberFormat(Arguments.amount,"0.00")#">
            <cfhttpparam type="Formfield" name="UMstreet" value="#Arguments.address#">
            <cfhttpparam type="Formfield" name="UMzip" value="#Arguments.zip#">
            <cfhttpparam type="Formfield" name="UMorderid" value="#Arguments.orderID#">
            <cfhttpparam type="Formfield" name="UMdescription" value="#Arguments.description#">
            <cfhttpparam type="Formfield" name="UMip" value="#Arguments.clientIP#">           
            <cfif IsDefined("Arguments.contactPhone")>
              <cfhttpparam type="Formfield" name="UMshipphone" value="#Arguments.contactPhone#">
            </cfif>
            <cfif IsDefined("Arguments.billingFirstName")>
              <cfhttpparam type="Formfield" name="UMbillfname" value="#Arguments.billingFirstName#">
            </cfif>
            <cfif IsDefined("Arguments.billingLastName")>
              <cfhttpparam type="Formfield" name="UMbilllname" value="#Arguments.billingLastName#">
            </cfif>
            <cfif IsDefined("Arguments.billingAddress1")>
              <cfhttpparam type="Formfield" name="UMbillstreet" value="#Arguments.billingAddress1#">
            </cfif>
            <cfif IsDefined("Arguments.billingAddress2")>
              <cfhttpparam type="Formfield" name="UMbillstreet2" value="#Arguments.billingAddress2#">
            </cfif>
            <cfif IsDefined("Arguments.billingCity")>
              <cfhttpparam type="Formfield" name="UMbillcity" value="#Arguments.billingCity#">
            </cfif>
            <cfif IsDefined("Arguments.billingState")>
              <cfhttpparam type="Formfield" name="UMbillstate" value="#Arguments.billingState#">
            </cfif>
            <cfif IsDefined("Arguments.billingZip")>
              <cfhttpparam type="Formfield" name="UMbillzip" value="#Arguments.billingZip#">
            </cfif>
            <cfif IsDefined("Arguments.billingPhone")>
              <cfhttpparam type="Formfield" name="UMbillphone" value="#Arguments.billingPhone#">
            </cfif>     
            
            <cfif IsDefined("Arguments.lineItems") AND ArrayLen(Arguments.lineItems) GT 0>
              <cfloop index="idx" from="1" to="#ArrayLen(Arguments.lineItems)#">
                <cfhttpparam type="Formfield" name="UMline#idx#sku" value="#Arguments.lineItems[idx].SKU#">
                <cfhttpparam type="Formfield" name="UMline#idx#name" value="#Arguments.lineItems[idx].itemName#">
                <cfhttpparam type="Formfield" name="UMline#idx#description" value="#Arguments.lineItems[idx].description#">
                <cfhttpparam type="Formfield" name="UMline#idx#cost" value="#NumberFormat(Arguments.lineItems[idx].cost,"0.00")#">
                <cfhttpparam type="Formfield" name="UMline#idx#qty" value="1">
                <cfhttpparam type="Formfield" name="UMline#idx#taxable" value="N">            
              </cfloop>
            </cfif>
    
            <cfhttpparam type="Formfield" name="UMtestmode" value="#Arguments.testMode#">
          </cfhttp>
      <cfelse>
      	  <!--- split payments: make each line item a separate transaction  --->
          <cfhttp method="Post" URL="#this.USAePayCGIURL#">
            <cfhttpparam type="Formfield" name="UMcommand" value="#Arguments.command#">   
            <cfhttpparam type="Formfield" name="UMkey" value="#this.USAePayKey#"> 
            
            <cfif Not CompareNoCase(Arguments.paymentMethod,"Credit Card")>
              <cfhttpparam type="Formfield" name="UMname" value="#Arguments.nameOnCard#">
              <cfhttpparam type="Formfield" name="UMcard" value="#Arguments.cardNumber#">
              <cfhttpparam type="Formfield" name="UMexpir" value="#Arguments.expirationDate#">
              <cfhttpparam type="Formfield" name="UMcvv2" value="#Arguments.CVV2#">
            <cfelse>              
              <cfhttpparam type="Formfield" name="UMname" value="#Arguments.nameOnAccount#">
              <cfhttpparam type="Formfield" name="UMaccounttype" value="#Arguments.accountType#">              
              <cfhttpparam type="Formfield" name="UMaccount" value="#Arguments.accountNumber#">
              <cfhttpparam type="Formfield" name="UMrouting" value="#Arguments.routingNumber#">
            </cfif>       
                   
            <cfhttpparam type="Formfield" name="UMstreet" value="#Arguments.address#">
            <cfhttpparam type="Formfield" name="UMzip" value="#Arguments.zip#">
            <cfhttpparam type="Formfield" name="UMorderid" value="#Arguments.orderID#">
            
            <cfhttpparam type="Formfield" name="UMip" value="#Arguments.clientIP#">            
            
            <cfif IsDefined("Arguments.contactPhone")>
              <cfhttpparam type="Formfield" name="UMshipphone" value="#Arguments.contactPhone#">
            </cfif>
            
            <cfif IsDefined("Arguments.billingFirstName")>
              <cfhttpparam type="Formfield" name="UMbillfname" value="#Arguments.billingFirstName#">
            </cfif>
            <cfif IsDefined("Arguments.billingLastName")>
              <cfhttpparam type="Formfield" name="UMbilllname" value="#Arguments.billingLastName#">
            </cfif>
            <cfif IsDefined("Arguments.billingAddress1")>
              <cfhttpparam type="Formfield" name="UMbillstreet" value="#Arguments.billingAddress1#">
            </cfif>
            <cfif IsDefined("Arguments.billingAddress2")>
              <cfhttpparam type="Formfield" name="UMbillstreet2" value="#Arguments.billingAddress2#">
            </cfif>
            <cfif IsDefined("Arguments.billingCity")>
              <cfhttpparam type="Formfield" name="UMbillcity" value="#Arguments.billingCity#">
            </cfif>
            <cfif IsDefined("Arguments.billingState")>
              <cfhttpparam type="Formfield" name="UMbillstate" value="#Arguments.billingState#">
            </cfif>
            <cfif IsDefined("Arguments.billingZip")>
              <cfhttpparam type="Formfield" name="UMbillzip" value="#Arguments.billingZip#">
            </cfif>
            <cfif IsDefined("Arguments.billingPhone")>
              <cfhttpparam type="Formfield" name="UMbillphone" value="#Arguments.billingPhone#">
            </cfif>
            
            <cfhttpparam type="Formfield" name="UMamount" value="#NumberFormat(Arguments.lineItems[1].cost,"0.00")#">
            <cfhttpparam type="Formfield" name="UMdescription" value="#Arguments.lineItems[1].description#">
            <cfhttpparam type="Formfield" name="UMcustom1" value="#Arguments.lineItems[1].GLNumber#">
            <!--- <cfhttpparam type="Formfield" name="UMGLNumber" value="#Arguments.lineItems[1].GLNumber#"> --->
            
            <cfloop index="idx" from="2" to="#ArrayLen(Arguments.lineItems)#">
              <cfif idx LT 10><cfset fieldIndex="0#idx#"><cfelse><cfset fieldIndex="#idx#"></cfif>
              <cfhttpparam type="Formfield" name="UM#fieldIndex#amount" value="#NumberFormat(Arguments.lineItems[idx].cost,"0.00")#">
              <cfhttpparam type="Formfield" name="UM#fieldIndex#description" value="#Arguments.lineItems[idx].description#">
              <cfhttpparam type="Formfield" name="UM#fieldIndex#custom1" value="#Arguments.lineItems[idx].GLNumber#">
              <!--- <cfhttpparam type="Formfield" name="UM#fieldIndex#GLNumber" value="#Arguments.lineItems[idx].GLNumber#">   --->          
            </cfloop>
            
            <cfhttpparam type="Formfield" name="UMtestmode" value="#Arguments.testMode#">
          </cfhttp>      
      </cfif>    

      <cfset returnedString=cfhttp.FileContent>
      <cfset paramPair=ListToArray(returnedString,"&")>
      <cfloop index="idx" from="1" to="#ArrayLen(paramPair)#">
        <cfset separatorIdx=Find("=",paramPair[idx])>
        <cfset paramName=Left(paramPair[idx],separatorIdx-1)>
        <cfset paramValue=URLDecode(Mid(paramPair[idx], separatorIdx+1, Len(paramPair[idx])-separatorIdx))>
        <cfset returnedStruct["#paramName#"]=paramValue>
      </cfloop>
      <cfif IsDefined("returnedStruct.UMstatus") AND Not CompareNoCase(returnedStruct.UMstatus,"Approved")>
        <cfset returnedStruct.approved=true>
        <cfset returnedStruct.authCode=returnedStruct.UMauthCode>
      <cfelse>
        <cfset returnedStruct.approved=false>
        <cfset returnedStruct.serverError=false>
        <cfif IsDefined("returnedStruct.UMerror")>
          <cfset returnedStruct.error=returnedStruct.UMerror>
        <cfelse>
          <cfset returnedStruct.error="">
        </cfif>
      </cfif>     
      
      <cfreturn returnedStruct>
    <cfcatch>
      <cfset returnedStruct.approved=false>
      <cfset returnedStruct.serverError=true>
      <cfset returnedStruct.error=cfcatch.Message>
      <cfreturn returnedStruct>
    </cfcatch>
    </cftry>
  </cffunction>
</cfcomponent>