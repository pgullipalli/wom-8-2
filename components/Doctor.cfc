<cfcomponent displayname="Doctor Directory" extends="Base" output="no">
  <cffunction name="getAllSpecialties" access="public" output="no" returntype="query">
	<cfquery name="qSpecialties" datasource="#this.datasource#">
      SELECT specialtyID, specialty
      FROM dd_specialty
      ORDER BY specialty
    </cfquery>

	<cfreturn qSpecialties>
  </cffunction>
  
  <cffunction name="getAllOfficeNames" access="public" output="no" returntype="query">
	<cfquery name="qOfficeNames" datasource="#this.datasource#" cachedwithin="#CreateTimeSpan(0,0,30,0)#">
      SELECT DISTINCT officeName
      FROM dd_doctor
      ORDER BY officeName
    </cfquery>

	<cfreturn qOfficeNames>
  </cffunction>
  
  <cffunction name="getAllDoctorNames" access="public" output="no" returntype="query">
	<cfquery name="qDoctorNames" datasource="#this.datasource#" cachedwithin="#CreateTimeSpan(0,0,30,0)#">
      SELECT firstName, lastName
      FROM dd_doctor
      ORDER BY lastName
    </cfquery>

	<cfreturn qDoctorNames>
  </cffunction>
  
  <cffunction name="getSpecialtyBySpecialtyID" access="public" output="no" returntype="string">
    <cfargument name="specialtyID" type="numeric" required="yes">
    <cfquery name="qSpecialty" datasource="#this.datasource#">
      SELECT specialty
      FROM dd_specialty
      WHERE specialtyID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.specialtyID#">
    </cfquery>
    <cfif qSpecialty.recordcount Is 0>
      <cfreturn "">
    <cfelse>
      <cfreturn qSpecialty.specialty>
    </cfif>
  </cffunction>
  
  <cffunction name="getDoctorsBySpecialtyID" access="public" output="no" returntype="struct">
    <cfargument name="specialty" type="string" required="yes">
    <cfargument name="startIndex" type="numeric" default="1">
    <cfargument name="numItemsPerPage" type="numeric" default="20">
    <cfset var resultStruct = StructNew()>    
    
    <cfquery name="qNumAllDoctors" datasource="#this.datasource#">
      SELECT COUNT(doctorID) AS numDoctors
      FROM dd_doctor
      WHERE specialty=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.specialty#">
    </cfquery>
    <cfset resultStruct.numAllItems = qNumAllDoctors.numDoctors>
    
    <cfquery name="qDoctors" datasource="#this.datasource#">
      SELECT recordNum, firstName, lastName, degree
      FROM dd_doctor
      WHERE specialty=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.specialty#">
      ORDER BY lastName, firstName DESC
      LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
    </cfquery>
    
    <cfset resultStruct.numDisplayedItems=qDoctors.recordcount>	
    <cfset resultStruct.doctors = qDoctors>    
  
    <cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="getDoctorsByNameKeyword" access="public" output="no" returntype="struct">
    <cfargument name="keyword" type="string" required="yes">
    <cfargument name="startIndex" type="numeric" default="1">
    <cfargument name="numItemsPerPage" type="numeric" default="20">
    <cfset var resultStruct = StructNew()>    
    
    <cfquery name="qNumAllDoctors" datasource="#this.datasource#">
      SELECT COUNT(doctorID) AS numDoctors
      FROM dd_doctor
      WHERE CONCAT(firstName,' ',lastName) LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
    </cfquery>
    <cfset resultStruct.numAllItems = qNumAllDoctors.numDoctors>
    
    <cfquery name="qDoctors" datasource="#this.datasource#">
      SELECT recordNum, firstName, lastName, degree
      FROM dd_doctor
      WHERE CONCAT(firstName,' ',lastName) LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
      ORDER BY lastName, firstName DESC
      LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
    </cfquery>
    
    <cfset resultStruct.numDisplayedItems=qDoctors.recordcount>	
    <cfset resultStruct.doctors = qDoctors>    
  
    <cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="getDoctorsByOfficeNameKeyword" access="public" output="no" returntype="struct">
    <cfargument name="keyword" type="string" required="yes">
    <cfargument name="startIndex" type="numeric" default="1">
    <cfargument name="numItemsPerPage" type="numeric" default="20">
    <cfset var resultStruct = StructNew()>    
    
    <cfquery name="qNumAllDoctors" datasource="#this.datasource#">
      SELECT COUNT(doctorID) AS numDoctors
      FROM dd_doctor
      WHERE officeName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
    </cfquery>
    <cfset resultStruct.numAllItems = qNumAllDoctors.numDoctors>
    
    <cfquery name="qDoctors" datasource="#this.datasource#">
      SELECT recordNum, firstName, lastName, degree
      FROM dd_doctor
      WHERE officeName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
      ORDER BY lastName, firstName DESC
      LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
    </cfquery>
    
    <cfset resultStruct.numDisplayedItems=qDoctors.recordcount>	
    <cfset resultStruct.doctors = qDoctors>    
  
    <cfreturn resultStruct>
  </cffunction>

  <cffunction name="getDoctorByRecordNum" access="public" output="no" returntype="query">
    <cfargument name="recordNum" type="numeric" required="yes">
	
    <cfquery name="qDoctor" datasource="#this.datasource#">
	  SELECT *
	  FROM   dd_doctor LEFT JOIN dd_doctor_meta ON
	         dd_doctor.recordNum=dd_doctor_meta.recordNum
      WHERE dd_doctor.recordNum=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.recordNum#">
    </cfquery>
    <cfreturn qDoctor>
  </cffunction>
  
  <cffunction name="getBoardCertList" access="public" output="no" returntype="string">
    <cfargument name="recordNum" type="numeric" required="yes">
    <cfquery name="qCerts" datasource="#this.datasource#">
	  SELECT boardcert1, boardcert2, boardcert3
	  FROM   dd_doctor
      WHERE dd_doctor.recordNum=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.recordNum#">
    </cfquery>
    
	<cfif qCerts.recordcount Is 0><cfreturn ""></cfif>
    
    <cfset boardCertList="">
    
    <cfif Compare(qCerts.boardcert1,"")>
      <cfloop index="cert" list="#qCerts.boardcert1#">
        <cfset cert=Trim(cert)>
    	<cfif Compare(cert,"") And ListFind(boardCertList, cert) IS 0>
          <cfset boardCertList=listAppend(boardCertList,cert)>
        </cfif>
      </cfloop>
    </cfif>
    <cfif Compare(qCerts.boardcert2,"")>
      <cfloop index="cert" list="#qCerts.boardcert2#">
        <cfset cert=Trim(cert)>
    	<cfif Compare(cert,"") And ListFind(boardCertList, cert) IS 0>
          <cfset boardCertList=listAppend(boardCertList,cert)>
        </cfif>
      </cfloop>
    </cfif>
    <cfif Compare(qCerts.boardcert3,"")>
      <cfloop index="cert" list="#qCerts.boardcert3#">
        <cfset cert=Trim(cert)>
    	<cfif Compare(cert,"") And ListFind(boardCertList, cert) IS 0>
          <cfset boardCertList=listAppend(boardCertList,cert)>
        </cfif>
      </cfloop>
    </cfif>
    
    <cfreturn boardCertList>
  </cffunction>
  
  <cffunction name="getIntroductionText" access="public" output="no" returntype="string">
    <cfquery name="qText" datasource="#this.datasource#">
      SELECT introductionText
      FROM dd_introduction
    </cfquery>
    
    <cfif qText.recordcount GT 0>
	  <cfreturn qText.introductionText>
	<cfelse>      
      <cfreturn "">
	</cfif>
  </cffunction>
</cfcomponent>