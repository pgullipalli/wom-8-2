<cfcomponent displayname="Newsroom Administrator" extends="Base" output="false">
  <cfif IsDefined("Application.newsroom")>
    <cfset this.thumbnailWidth=Application.newsroom.thumbnailWidth>
  <cfelse>
    <cfset this.thumbnailWidth="100">
  </cfif>
  
  <cffunction name="getAllArticleCategories" access="public" output="no" returntype="query">
	<cfquery name="qArticleCategories" datasource="#this.datasource#">
	  SELECT articleCategoryID, articleCategory
	  FROM nr_articlecategory
	  ORDER BY articleCategory	
	</cfquery>	
    
    <cfreturn qArticleCategories>  
  </cffunction>
  
  <cffunction name="getArticleCategorySummary" access="public" output="no" returntype="query">
    <cfquery name="qCatSummary" datasource="#this.datasource#">
	  SELECT nr_articlecategory.articleCategory,
      	  	 nr_articlecategory.articleCategoryID,
             nr_articlecategory.RSSEnabled,
             nr_articlecategory.contactInfo,
			 COUNT(nr_article_articlecategory.articleID) AS numArticles
	  FROM nr_articlecategory LEFT JOIN nr_article_articlecategory ON
	  	   nr_articlecategory.articleCategoryID=nr_article_articlecategory.articleCategoryID
	  GROUP by nr_articlecategory.articleCategoryID
	  ORDER by nr_articlecategory.articleCategory		
	</cfquery>
    
    <cfreturn qCatSummary>
  </cffunction>
  
  <cffunction name="getFirstArticleCategoryID" access="public" output="no" returntype="numeric">
    <cfquery name="qArticleCategoryID" datasource="#this.datasource#">
	  SELECT articleCategoryID
	  FROM nr_articlecategory
      ORDER BY articleCategory
	</cfquery>
    
    <cfif qArticleCategoryID.recordcount GT 0>
	  <cfreturn qArticleCategoryID.articleCategoryID[1]>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="getArticleCategory" access="public" output="no" returntype="query">
    <cfargument name="articleCategoryID" required="yes" type="numeric">	
	<cfquery name="qArticleCategory" datasource="#this.datasource#">
	  SELECT *
      FROM nr_articlecategory
      WHERE articleCategoryID=<cfqueryparam value="#Arguments.articleCategoryID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
	<cfreturn qArticleCategory>
  </cffunction>
  
  <cffunction name="addArticleCategory" access="public" output="no">
    <cfargument name="articleCategory" type="string" required="yes">
    <cfargument name="description" type="string" required="yes">
    <cfargument name="contactInfo" type="string" required="yes">
	<cfargument name="RSSEnabled" type="boolean" default="0">
    <cfargument name="numArticlesRSS" type="string" default="20">
    <cfargument name="displayArticleDate" type="boolean" default="0">
    
    <cfif Not IsNumeric(Arguments.numArticlesRSS)>
      <cfset Arguments.numArticlesRSS="20">
    </cfif>

    <cfquery datasource="#this.datasource#">
	  INSERT INTO nr_articlecategory
        (articleCategory, description, contactInfo, RSSEnabled, numArticlesRSS, displayArticleDate, dateCreated, dateLastModified)
	  VALUES
        (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.articleCategory#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.description#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactInfo#">,
	  	 <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.RSSEnabled#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.numArticlesRSS#">, 
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.displayArticleDate#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)	  
	</cfquery>  
  </cffunction>
  
  <cffunction name="editArticleCategory" access="public" output="no">
    <cfargument name="articleCategoryID" type="numeric" required="yes">
    <cfargument name="articleCategory" type="string" required="yes">
    <cfargument name="description" type="string" required="yes">
    <cfargument name="contactInfo" type="string" required="yes">
	<cfargument name="RSSEnabled" type="boolean" default="0">
    <cfargument name="numArticlesRSS" type="string" default="20">
    <cfargument name="displayArticleDate" type="boolean" default="0">
    
    <cfif Not IsNumeric(Arguments.numArticlesRSS)>
      <cfset Arguments.numArticlesRSS="20">
    </cfif>

    <cfquery datasource="#this.datasource#">
	  UPDATE nr_articlecategory
      SET articleCategory=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.articleCategory#">,
      	  description=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.description#">,
      	  contactInfo=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactInfo#">,
      	  RSSEnabled=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.RSSEnabled#">,
          numArticlesRSS=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.numArticlesRSS#">,
          displayArticleDate=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.displayArticleDate#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE articleCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID#">  
	</cfquery>  
  </cffunction>
    
  <cffunction name="deleteArticleCategory" access="public" output="false" returntype="boolean">
    <cfargument name="articleCategoryID" required="yes" type="numeric">
	
	<cfquery name="qArticles" datasource="#this.datasource#">
	  SELECT articleID
	  FROM nr_article_articlecategory
      WHERE articleCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID#">			
	</cfquery>
	
	<cfif qArticles.recordcount GT 0>
	  <cfreturn false>
	</cfif>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM nr_articlecategory
      WHERE articleCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID#">
	</cfquery>
    
	<cfreturn true>
  </cffunction>
  
  <cffunction name="getArticlesByCategoryID" access="public" output="false" returntype="struct">
    <cfargument name="articleCategoryID" type="numeric" required="yes">
	<cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.articles=QueryNew("articleID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qAllArticleIDs" datasource="#this.datasource#">
	  SELECT COUNT(nr_article.articleID) AS numArticles
	  FROM	 nr_article INNER JOIN nr_article_articlecategory ON
	  		 nr_article.articleID=nr_article_articlecategory.articleID
	  WHERE	 nr_article_articlecategory.articleCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID#">
    </cfquery>
	<cfset resultStruct.numAllItems = qAllArticleIDs.numArticles>
	
	<cfquery name="qArticles" datasource="#this.datasource#">
	  SELECT nr_article.articleID, nr_article.headline,
	  		 nr_article.articleDate, nr_article.published, nr_article.featuredOnHomepage,
			 nr_article.publishDate, nr_article.unpublishDate			 
	  FROM   nr_article INNER JOIN nr_article_articlecategory ON
	         nr_article.articleID=nr_article_articlecategory.articleID
	  WHERE  nr_article_articlecategory.articleCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID#">
	  ORDER  BY nr_article.articleDate DESC
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
    <cfif qArticles.recordcount GT 0>
        <cfquery name="qRelatedItems" datasource="#this.datasource#">
          SELECT nr_article.articleID,
                 COUNT(nr_article_relateditem.articleRelatedItemID) AS numRelatedItems
          FROM   nr_article LEFT JOIN nr_article_relateditem ON
                 nr_article.articleID=nr_article_relateditem.articleID
          WHERE  nr_article.articleID IN (#valueList(qArticles.articleID)#)
          GROUP BY nr_article.articleID	  		
        </cfquery>
        
        <cfquery name="qArticles" dbtype="query">
          SELECT *
          FROM qArticles, qRelatedItems
          WHERE qArticles.articleID=qRelatedItems.articleID
          ORDER BY articleDate DESC, articleID DESC
        </cfquery>
            
        <cfset resultStruct.numDisplayedItems=qArticles.recordcount>	
        <cfset resultStruct.articles = qArticles>
	<cfelse>
        <cfset resultStruct.numDisplayedItems=0>	
        <cfset resultStruct.articles = QueryNew("articleID")>
    </cfif>
    
	<cfreturn resultStruct>
  </cffunction>  
  
  <cffunction name="getArticleByID" access="public" output="no" returntype="query">
    <cfargument name="articleID" type="numeric" required="yes">
	<cfquery name="qArticle" datasource="#this.datasource#">
	  SELECT *
	  FROM nr_article
      WHERE articleID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.articleID#">
	</cfquery>
    
	<cfreturn qArticle>	
  </cffunction>
  
  <cffunction name="getArticleCategoryIDListByArticleID" access="public" output="no" returntype="string">
    <cfargument name="articleID" type="numeric" required="yes">
    
	<cfquery name="qCategories" datasource="#this.datasource#">
	  SELECT articleCategoryID
      FROM nr_article_articlecategory
      WHERE articleID=<cfqueryparam value="#arguments.articleID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
    <cfif qCategories.recordcount Is 0>
      <cfreturn "">
    <cfelse>
	  <cfreturn valueList(qCategories.articleCategoryID)>	  
    </cfif>
  </cffunction>
  
  <cffunction name="getArticleCategoryByID" access="public" output="no" returntype="query">
    <cfargument name="articleCategoryID" required="yes" type="numeric">	
	<cfquery name="qCategory" datasource="#this.datasource#">
	  SELECT *
      FROM nr_articlecategory
      WHERE articleCategoryID=<cfqueryparam value="#Arguments.articleCategoryID#" cfsqltype="cf_sql_integer">
	</cfquery>
	<cfreturn qCategory>
  </cffunction>

  <cffunction name="addArticle" access="public" output="no" returntype="boolean">
    <cfargument name="articleCategoryIDList" type="string" default="">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="featuredOnHomepage" type="boolean" default="0"> 
    
    <cfargument name="publishDate" type="string" default="">
    <cfargument name="publishDate_hh" type="string" default="">
    <cfargument name="publishDate_mm" type="string" default="">
    <cfargument name="publishDate_ampm" type="string" default="">
    
    <cfargument name="unpublishDate" type="string" default="">
    <cfargument name="unpublishDate_hh" type="string" default="">
    <cfargument name="unpublishDate_mm" type="string" default="">
    <cfargument name="unpublishDate_ampm" type="string" default="">
    
    <cfargument name="articleDate" type="string" default="">
    <cfargument name="articleDate_hh" type="string" default="">
    <cfargument name="articleDate_mm" type="string" default="">
    <cfargument name="articleDate_ampm" type="string" default="">   
    <cfargument name="displayArticleDate" type="boolean" default="0">     
    
    <cfargument name="headline" type="string" default="">
    <cfargument name="subHeadline" type="string" default="">
    <cfargument name="byline" type="string" default="">
    <cfargument name="dateline" type="string" default="">
    <cfargument name="teaser" type="string" default="">
    <cfargument name="featureImage" type="string" default="">
    <cfargument name="featureImageCaption" type="string" default="">
    <cfargument name="featureImageAlignment" type="string" default="L">
    <cfargument name="featureThumbnail" type="string" default="">
    <cfargument name="resizeForThumbnail" type="boolean" default="0">
    <cfargument name="contentType" type="numeric" default="1">
    <cfargument name="fullStory" type="string" default="">
    <cfargument name="externalURL" type="string" default="">
    <cfargument name="documentFile" type="string" default="">
    <cfargument name="mediaFileType" type="string" default="V">
    <cfargument name="mediaFile" type="string" default="">
    <cfargument name="mediaFileURL" type="string" default="">
    <cfargument name="popup" type="boolean" default="0">
    
    <cfif Not Compare(Arguments.articleCategoryIDList, "")><cfreturn false></cfif>
	<cfset Util=CreateObject("component","Utility").init()>
    <cftry>
	  <cfif Arguments.resizeForThumbnail>
	    <cfset Arguments.featureThumbnail=Util.createThumbnail("#this.siteDirectoryRoot#/#Arguments.featureImage#", this.thumbnailWidth)>
	  </cfif>		
	<cfcatch></cfcatch>
    </cftry>
    
	<cfset Variables.articleDate=Util.convertStringToDateTimeObject(Arguments.articleDate, Arguments.articleDate_hh, Arguments.articleDate_mm, 0, Arguments.articleDate_ampm)>
    
    <cfif Arguments.publishDate IS "">
	  <cfset Variables.publishDate=now()>
	<cfelse>
   	  <cfset Variables.publishDate=Util.convertStringToDateTimeObject(Arguments.publishDate, Arguments.publishDate_hh, Arguments.publishDate_mm, 0, Arguments.publishDate_ampm)>
	</cfif>
	
    <cfif Arguments.unpublishDate IS "">
	  <cfset Variables.unpublishDate=CreateDateTime(3000,1,1,0,0,0)>
	<cfelse>
   	  <cfset Variables.unpublishDate=Util.convertStringToDateTimeObject(Arguments.unpublishDate, Arguments.unpublishDate_hh, Arguments.unpublishDate_mm, 0, Arguments.unpublishDate_ampm)>
	</cfif>
		
    <cfquery datasource="#this.datasource#">
	  INSERT INTO nr_article
        (published, featuredOnHomepage, publishDate, unpublishDate, articleDate, displayArticleDate,
         headline, subHeadline, byline, dateline, teaser, featureImage,
         featureImageCaption, featureImageAlignment, featureThumbnail, contentType, fullStory,
         externalURL, documentFile, mediaFileType, mediaFile, mediaFileURL, popup,
         dateCreated, dateLastModified)
	  VALUES
	    (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.featuredOnHomepage#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.publishDate#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.unpublishDate#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.articleDate#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.displayArticleDate#">,
         
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.headline#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.subHeadline#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.byline#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.dateline#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.teaser#">,         
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.featureImage#">,
         
		 <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.featureImageCaption#">,
         <cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.featureImageAlignment#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.featureThumbnail#">,
		 <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.contentType#">,
		 <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.fullStory#">,
         
		 <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.externalURL#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.documentFile#">,
         <cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.mediaFileType#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.mediaFile#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.mediaFileURL#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.popup#">,
         
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)		
	</cfquery>

	<cfquery name="qArticleID" datasource="#this.datasource#">
	  SELECT MAX(articleID) AS newArticleID
	  FROM nr_article
    </cfquery>
	<cfset Variables.articleID=qArticleID.newArticleID>	
    
    <cfloop index="articleCategoryID" list="#Arguments.articleCategoryIDList#">
      <cfquery datasource="#this.datasource#">
		INSERT INTO nr_article_articlecategory (articleID, articleCategoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.articleID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#articleCategoryID#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>

  <cffunction name="editArticle" access="public" output="no" returntype="boolean">
    <cfargument name="articleID" type="numeric" required="yes">
    <cfargument name="articleCategoryIDList" type="string" default="">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="featuredOnHomepage" type="boolean" default="0"> 
    
    <cfargument name="publishDate" type="string" default="">
    <cfargument name="publishDate_hh" type="string" default="">
    <cfargument name="publishDate_mm" type="string" default="">
    <cfargument name="publishDate_ampm" type="string" default="">
    
    <cfargument name="unpublishDate" type="string" default="">
    <cfargument name="unpublishDate_hh" type="string" default="">
    <cfargument name="unpublishDate_mm" type="string" default="">
    <cfargument name="unpublishDate_ampm" type="string" default="">
    
    <cfargument name="articleDate" type="string" default="">
    <cfargument name="articleDate_hh" type="string" default="">
    <cfargument name="articleDate_mm" type="string" default="">
    <cfargument name="articleDate_ampm" type="string" default="">
    <cfargument name="displayArticleDate" type="boolean" default="0"> 
    
    <cfargument name="headline" type="string" default="">
    <cfargument name="subHeadline" type="string" default="">
    <cfargument name="byline" type="string" default="">
    <cfargument name="dateline" type="string" default="">
    <cfargument name="teaser" type="string" default="">
    <cfargument name="featureImage" type="string" default="">
    <cfargument name="featureImageCaption" type="string" default="">
    <cfargument name="featureImageAlignment" type="string" default="L">
    <cfargument name="featureThumbnail" type="string" default="">
    <cfargument name="resizeForThumbnail" type="boolean" default="0">
    <cfargument name="contentType" type="numeric" default="1">
    <cfargument name="fullStory" type="string" default="">
    <cfargument name="externalURL" type="string" default="">
    <cfargument name="documentFile" type="string" default="">
    <cfargument name="mediaFileType" type="string" default="V">
    <cfargument name="mediaFile" type="string" default="">
    <cfargument name="mediaFileURL" type="string" default="">
    <cfargument name="popup" type="boolean" default="0">
    
    <cfif Not Compare(Arguments.articleCategoryIDList, "")><cfreturn false></cfif>
	
    <cfset Util=CreateObject("component","Utility").init()>
	<cftry>
	  <cfif Arguments.resizeForThumbnail>
	    <cfset Arguments.featureThumbnail=Util.createThumbnail("#this.siteDirectoryRoot#/#Arguments.featureImage#", this.thumbnailWidth)>
	  </cfif>		
	<cfcatch></cfcatch>
    </cftry>		

	<cfset Variables.articleDate=Util.convertStringToDateTimeObject(Arguments.articleDate, Arguments.articleDate_hh, Arguments.articleDate_mm, 0, Arguments.articleDate_ampm)>
    
    <cfif Arguments.publishDate IS "">
	  <cfset Variables.publishDate=now()>
	<cfelse>
   	  <cfset Variables.publishDate=Util.convertStringToDateTimeObject(Arguments.publishDate, Arguments.publishDate_hh, Arguments.publishDate_mm, 0, Arguments.publishDate_ampm)>
	</cfif>
	
    <cfif Arguments.unpublishDate IS "">
	  <cfset Variables.unpublishDate=CreateDateTime(3000,1,1,0,0,0)>
	<cfelse>
   	  <cfset Variables.unpublishDate=Util.convertStringToDateTimeObject(Arguments.unpublishDate, Arguments.unpublishDate_hh, Arguments.unpublishDate_mm, 0, Arguments.unpublishDate_ampm)>
	</cfif>
		
    <cfquery datasource="#this.datasource#">
	  UPDATE nr_article
      SET published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
      	  featuredOnHomepage=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.featuredOnHomepage#">,
      	  publishDate=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.publishDate#">,
          unpublishDate=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.unpublishDate#">,
          articleDate=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.articleDate#">,
          displayArticleDate=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.displayArticleDate#">,
          headline=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.headline#">,
          subHeadline=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.subHeadline#">,
          byline=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.byline#">,
          dateline=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.dateline#">,
          teaser=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.teaser#">,
          featureImage=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.featureImage#">,
          featureImageCaption=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.featureImageCaption#">,
          featureImageAlignment=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.featureImageAlignment#">,
          featureThumbnail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.featureThumbnail#">,
          contentType=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.contentType#">,
          fullStory=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.fullStory#">,
          externalURL=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.externalURL#">,
          documentFile=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.documentFile#">,
          mediaFileType=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.mediaFileType#">,
          mediaFile=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.mediaFile#">,
          mediaFileURL=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.mediaFileURL#">,
          popup=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.popup#">, 
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE articleID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleID#">
	</cfquery>

	<cfquery datasource="#this.datasource#">
      DELETE FROM nr_article_articlecategory
      WHERE articleID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleID#">
    </cfquery>
    
    <cfloop index="articleCategoryID" list="#Arguments.articleCategoryIDList#">
      <cfquery datasource="#this.datasource#">
		INSERT INTO nr_article_articlecategory (articleID, articleCategoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#articleCategoryID#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>
  
  <cffunction name="copyArticle" access="public" output="no">
    <cfargument name="newHeadline" type="string" required="yes">
	<cfargument name="articleID" type="numeric" required="yes">
	
    <cfset articleInfo = getArticleByID(arguments.articleID)>
	<cfset articleCategoryIDList = getArticleCategoryIDListByArticleID(arguments.articleID)>
    
    <cfquery datasource="#this.datasource#">
	  INSERT INTO nr_article
        (published, featuredOnHomepage, publishDate, unpublishDate, articleDate, displayArticleDate,
         headline, subHeadline, byline, dateline, teaser, featureImage,
         featureImageCaption, featureImageAlignment, featureThumbnail, contentType, fullStory,
         externalURL, documentFile, mediaFileType, mediaFile, mediaFileURL, popup,
         dateCreated, dateLastModified)
	  VALUES
	    (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#articleInfo.published#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#articleInfo.featuredOnHomepage#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#articleInfo.publishDate#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#articleInfo.unpublishDate#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#articleInfo.articleDate#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#articleInfo.displayArticleDate#">,
         
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.newHeadline#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.newSubHeadline#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.newByline#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.newDateline#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#articleInfo.teaser#">,         
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#articleInfo.featureImage#">,
         
		 <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#articleInfo.featureImageCaption#">,
         <cfqueryparam cfsqltype="cf_sql_char" value="#articleInfo.featureImageAlignment#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#articleInfo.featureThumbnail#">,
		 <cfqueryparam cfsqltype="cf_sql_tinyint" value="#articleInfo.contentType#">,
		 <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#articleInfo.fullStory#">,
         
		 <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#articleInfo.externalURL#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#articleInfo.documentFile#">,
         <cfqueryparam cfsqltype="cf_sql_char" value="#articleInfo.mediaFileType#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#articleInfo.mediaFile#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#articleInfo.mediaFileURL#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#articleInfo.popup#">
         
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)		
	</cfquery>

	<cfquery name="qArticleID" datasource="#this.datasource#">
	  SELECT MAX(articleID) AS newArticleID
	  FROM nr_article
    </cfquery>
	<cfset Variables.articleID=qArticleID.newArticleID>	
    
    <cfloop index="articleCategoryID" list="#articleCategoryIDList#">
      <cfquery datasource="#this.datasource#">
		INSERT INTO nr_article_articlecategory (articleID, articleCategoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.articleID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#articleCategoryID#">)
	  </cfquery>
    </cfloop>
  </cffunction>

  <cffunction name="deleteArticle" access="public" output="false">
    <cfargument name="articleID" required="yes" type="numeric">  

    <cfquery datasource="#this.datasource#">
      DELETE FROM nr_article_relateditem
      WHERE articleID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM nr_article_articlecategory
      WHERE articleID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM nr_article
	  WHERE articleID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="getArticlesByKeyword" access="public" output="no" returntype="struct">
	<cfargument name="keyword" type="string" required="yes">
    <cfargument name="includeArticleContent" type="boolean" default="0">
    <cfargument name="featuredOnHomepage" type="boolean" default="0">    
    <cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllArticles=0>
	<cfset resultStruct.numDisplayedArticles=0>
	<cfset resultStruct.articles=QueryNew("articleID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qAllArticleIDs" datasource="#this.datasource#">
	  SELECT COUNT(nr_article.articleID) AS numArticles
	  FROM	 nr_article
	  WHERE	<cfif Compare(Arguments.keyword,"")>
      		  nr_article.headline LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.keyword#%">
      	      <cfif Arguments.includeArticleContent>
              OR (nr_article.contentType=1 AND nr_article.fullStory LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.keyword#%">)
              </cfif>
            <cfelse>
            1=1
            </cfif>            
            <cfif Arguments.featuredOnHomepage>
            AND featuredOnHomepage=1
            </cfif>            
    </cfquery>
	<cfset resultStruct.numAllItems = qAllArticleIDs.numArticles>
    
	<cfquery name="qArticles" datasource="#this.datasource#">
	  SELECT nr_article.articleID, nr_article.headline,
	  		 nr_article.articleDate, nr_article.articleDate,
	  		 nr_article.published, nr_article.featuredOnHomepage, nr_article.publishDate,
			 nr_article.unpublishDate
	  FROM nr_article
      WHERE <cfif Compare(Arguments.keyword,"")>
      		  nr_article.headline LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.keyword#%">
      	      <cfif Arguments.includeArticleContent>
              OR (nr_article.contentType=1 AND nr_article.fullStory LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.keyword#%">)
              </cfif>
            <cfelse>
            1=1
            </cfif>
            
            <cfif Arguments.featuredOnHomepage>
            AND featuredOnHomepage=1
            </cfif> 
	  ORDER BY nr_article.articleDate DESC
      LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>	
    
    <cfif qArticles.recordcount GT 0>
      <cfquery name="qRelatedItems" datasource="#this.datasource#">
	    SELECT nr_article.articleID,
	  		   COUNT(nr_article_relateditem.articleRelatedItemID) AS numRelatedItems
	    FROM nr_article LEFT JOIN nr_article_relateditem ON
	  	     nr_article.articleID=nr_article_relateditem.articleID
        WHERE nr_article.articleID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qArticles.articleID)#" list="yes">)
	    GROUP BY nr_article.articleID	  		
	  </cfquery>
	
	  <cfquery name="qArticles" dbtype="query">
	    SELECT *
	    FROM qArticles, qRelatedItems
	    WHERE qArticles.articleID=qRelatedItems.articleID	  		
	    ORDER BY articleDate DESC, articleID DESC
	  </cfquery>
      
      <cfset resultStruct.numDisplayedItems=qArticles.recordcount>	
      <cfset resultStruct.articles = qArticles>
    <cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.articles = QueryNew("articleID")>
	</cfif>

	<cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="getRelatedItems" access="public" output="no" returntype="query">
    <cfargument name="articleID" required="yes" type="numeric">
    
    <cfquery name="qRelatedItems" datasource="#this.datasource#">
      SELECT nr_article_relateditem.articleRelatedItemID, nr_article_relateditem.relatedItemType,
      		 nr_article_relateditem.linkName, nr_article_relateditem.linkURL,
             nr_article_relateditem.fileTitle, nr_article_relateditem.filePath, nr_article_relateditem.fileSize,
      		 nr_article.headline, nr_article.articleDate, pg_album.albumTitle, pg_album.albumDate,         
             nr_article_relateditem.displaySeq
      FROM (nr_article_relateditem LEFT JOIN nr_article ON nr_article_relateditem.articleID2=nr_article.articleID) LEFT JOIN
      	   pg_album ON nr_article_relateditem.albumID=pg_album.albumID
      WHERE nr_article_relateditem.articleID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleID#">
      ORDER BY nr_article_relateditem.displaySeq
    </cfquery>
    
    <cfreturn qRelatedItems>
  </cffunction>
  
  
  <cffunction name="addRelatedArticles" access="public" output="no">
    <cfargument name="articleID" required="yes" type="numeric">
    <cfargument name="articleIDList" type="string" default="">
    
    <cfif Arguments.articleIDList Is ""><cfreturn></cfif> 
      
    <cfquery name="qDisplaySeq" datasource="#this.datasource#">
      SELECT displaySeq
      FROM nr_article_relateditem
      WHERE articleID=<cfqueryparam value="#arguments.articleID#" cfsqltype="cf_sql_integer">
      ORDER BY displaySeq DESC
    </cfquery>
    
    <cfif qDisplaySeq.recordcount Is 0>
      <cfset newDisplaySeq=1>
    <cfelse>
      <cfset newDisplaySeq=qDisplaySeq.displaySeq + 1>
    </cfif>
    
    <cfloop index="articleID2" list="#arguments.articleIDList#">
      <cfquery datasource="#this.datasource#">
	    INSERT INTO nr_article_relateditem (articleID, relatedItemType, articleID2, displaySeq)
		VALUES
		  (<cfqueryparam value="#arguments.articleID#" cfsqltype="cf_sql_integer">,
           'article',
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#articleID2#">,
		   <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">)
	  </cfquery>
      <cfset newDisplaySeq = newDisplaySeq + 1>
    </cfloop>  
  </cffunction>
  
  <cffunction name="addRelatedGalleries" access="public" output="no">
    <cfargument name="articleID" required="yes" type="numeric">
    <cfargument name="albumIDList" type="string" default="">
    
    <cfif arguments.albumIDList Is ""><cfreturn></cfif> 
        
    <cfquery name="qDisplaySeq" datasource="#this.datasource#">
      SELECT displaySeq
      FROM nr_article_relateditem
      WHERE articleID=<cfqueryparam value="#arguments.articleID#" cfsqltype="cf_sql_integer">
      ORDER BY displaySeq DESC
    </cfquery>
    
    <cfif qDisplaySeq.recordcount Is 0>
      <cfset newDisplaySeq=1>
    <cfelse>
      <cfset newDisplaySeq=qDisplaySeq.displaySeq + 1>
    </cfif>
    
    <cfloop index="albumID" list="#arguments.albumIDList#">
      <cfquery datasource="#this.datasource#">
	    INSERT INTO nr_article_relateditem (articleID, relatedItemType, albumID, displaySeq)
		VALUES
		  (<cfqueryparam value="#arguments.articleID#" cfsqltype="cf_sql_integer">,
           'gallery',
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#albumID#">,
		   <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">)
	  </cfquery>
      <cfset newDisplaySeq = newDisplaySeq + 1>
    </cfloop>
  </cffunction>
  
  <cffunction name="addRelatedLink" access="public" output="no" displayname="Update Related Links">
    <cfargument name="articleID" required="yes" type="numeric">
    <cfargument name="linkName" type="string" required="yes">
    <cfargument name="linkURL" type="string" required="yes">
  
    <cfquery name="qDisplaySeq" datasource="#this.datasource#">
      SELECT displaySeq
      FROM nr_article_relateditem where articleID=<cfqueryparam value="#arguments.articleID#" cfsqltype="cf_sql_integer">
      ORDER BY displaySeq DESC
    </cfquery>
    
    <cfif qDisplaySeq.recordcount Is 0>
      <cfset newDisplaySeq=1>
    <cfelse>
      <cfset newDisplaySeq=qDisplaySeq.displaySeq + 1>
    </cfif>
  
    <cfquery datasource="#this.datasource#">
	  INSERT INTO nr_article_relateditem (articleID, relatedItemType, linkName, linkURL, displaySeq)
	  VALUES
	  (<cfqueryparam value="#arguments.articleID#" cfsqltype="cf_sql_integer">,
       'link',
	   <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.linkName#">,
	   <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.linkURL#">,
	   <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">)
    </cfquery> 
  </cffunction>
  
  <cffunction name="addRelatedFile" access="public" output="no">
    <cfargument name="articleID" required="yes" type="numeric">
    <cfargument name="filePath" type="string" required="yes">
    <cfargument name="fileTitle" type="string" required="yes">
    
    <cfset Util=CreateObject("component","Utility").init()>
    <cfset fileSize=Util.getFileSize(this.siteDirectoryRoot & "/" & Arguments.filePath)>
    <cfif fileSize LT 1000>
      <cfset fileSize = fileSize & " bytes">
    <cfelseif fileSize GTE 1000 AND fileSize LTE 1000000>
      <cfset fileSize = NumberFormat(fileSize/1000, "9.9") & " KB">
    <cfelse>
      <cfset fileSize = NumberFormat(fileSize/1000000, "9.999") & " MB">
    </cfif>
    
	<!--- <cfset fileSize = "0 K">
	<cftry>
	  <cfset dir = getDirectoryFromPath(Arguments.siteDirectoryRoot & "/" & Arguments.filePath)>
	  <cfset fileName = getFileFromPath(Arguments.filePath)>
	  <cfdirectory action="list" directory="#dir#" name="allFiles">
	  <cfloop query="allFiles">
	    <cfif NOT Compare(name, fileName)>
		  <cfset fileSize = size>
		  <cfif fileSize LT 1000>
		    <cfset fileSize = fileSize & " bytes">
		  <cfelseif fileSize GTE 1000 AND fileSize LTE 1000000>
		    <cfset fileSize = NumberFormat(fileSize/1000, "9.9") & " KB">
		  <cfelse>
		    <cfset fileSize = NumberFormat(fileSize/1000000, "9.999") & " MB">
		  </cfif>
		</cfif>
	  </cfloop>
	<cfcatch></cfcatch>
	</cftry> --->
    
    <cfquery name="qDisplaySeq" datasource="#this.datasource#">
      SELECT displaySeq
      FROM nr_article_relateditem where articleID=<cfqueryparam value="#arguments.articleID#" cfsqltype="cf_sql_integer">
      ORDER BY displaySeq DESC
    </cfquery>
    
    <cfif qDisplaySeq.recordcount Is 0>
      <cfset newDisplaySeq=1>
    <cfelse>
      <cfset newDisplaySeq=qDisplaySeq.displaySeq + 1>
    </cfif>  
  
    <cfquery datasource="#this.datasource#">
	  INSERT INTO nr_article_relateditem (articleID, relatedItemType, filePath, fileSize, fileTitle, displaySeq)
	  VALUES
	  (<cfqueryparam value="#arguments.articleID#" cfsqltype="cf_sql_integer">,
       'file',
	   <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.filePath#">,
	   <cfqueryparam cfsqltype="cf_sql_varchar" value="#fileSize#">,
	   <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.fileTitle#">,
	   <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">)
	</cfquery> 
  </cffunction>
  
  <cffunction name="deleteRelatedItems" access="public" output="no">
    <cfargument name="articleRelatedItemIDList" type="string" default="">
    <cfif Compare(articleRelatedItemIDList, "")>
	  <cfquery datasource="#this.datasource#">
	    DELETE FROM nr_article_relateditem
        WHERE articleRelatedItemID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.articleRelatedItemIDList#" list="yes">)
	  </cfquery>
    </cfif>
  </cffunction>  
  
  <cffunction name="moveRelatedItem" access="public" output="no">
    <cfargument name="articleRelatedItemID" required="yes" type="numeric">
    <cfargument name="direction" required="yes" type="string">
    
    <cfquery name="qTargetDisplaySeq" datasource="#this.datasource#">
      SELECT articleID, displaySeq
      FROM nr_article_relateditem WHERE articleRelatedItemID=<cfqueryparam value="#Arguments.articleRelatedItemID#" cfsqltype="cf_sql_integer">
    </cfquery>
    
    <cfset targetArticleID=qTargetDisplaySeq.articleID>
    <cfset targetDisplaySeq=qTargetDisplaySeq.displaySeq>
    
    <cfif arguments.direction IS "up">
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT articleRelatedItemID, displaySeq
        FROM nr_article_relateditem
        WHERE articleID=<cfqueryparam value="#targetArticleID#" cfsqltype="cf_sql_integer"> AND
        	  displaySeq < <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq DESC
      </cfquery>
    <cfelse>
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT articleRelatedItemID, displaySeq
        FROM nr_article_relateditem
        WHERE articleID=<cfqueryparam value="#targetArticleID#" cfsqltype="cf_sql_integer"> AND
        	  displaySeq > <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq ASC
      </cfquery>
    </cfif>
    
    <cfif qNextDisplaySeq.recordcount Is 0><cfreturn></cfif>
    
    <cfset nextDisplaySeq=qNextDisplaySeq.displaySeq>
        
    <cfquery datasource="#this.datasource#">
      UPDATE nr_article_relateditem SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#nextDisplaySeq#">
      WHERE articleRelatedItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.articleRelatedItemID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      UPDATE nr_article_relateditem SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
      WHERE articleRelatedItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qNextDisplaySeq.articleRelatedItemID#">
    </cfquery>
  </cffunction>

  <cffunction name="getURLByArticleID" access="public" output="no" returntype="string">
    <cfargument name="articleID" required="yes" type="numeric">
  
    <cfquery name="qArticle" datasource="#this.datasource#">
	  SELECT CASE WHEN nr_article.contentType=1
             THEN CONCAT('index.cfm?md=newsroom&tmp=detail','&articleID=',CAST(articleID AS CHAR))
			 ELSE
			   CASE WHEN contentType=2
			   THEN externalURL
			   ELSE documentFile
			   END									
			 END AS href
	  FROM nr_article
	  WHERE articleID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleID#">
	</cfquery>
  
    <cfreturn qArticle.href>  
  </cffunction>
</cfcomponent>