<cfcomponent displayname="Global" extends="Base" output="false">
  <cffunction name="getGlobalProperties" access="public" output="no" returntype="query">
    <cfquery name="qGlobalProperties" datasource="#this.datasource#">
      SELECT *
      FROM gp_global_properties
    </cfquery>
    <cfreturn qGlobalProperties>
  </cffunction>
  
  <cffunction name="getWebmasterEmail" access="public" output="no" returntype="string">
    <cfquery name="qEmail" datasource="#this.datasource#">
      SELECT webmasterEmail
      FROM gp_global_properties
    </cfquery>
    <cfreturn qEmail.webmasterEmail>
  </cffunction>
</cfcomponent>