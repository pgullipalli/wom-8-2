<cfcomponent displayname="Communication Manager" extends="Base">
  <cfscript>  
    if (IsDefined("Application.communication")) {
      this.numEmailsSentPerProcess=Application.communication.numEmailsSentPerProcess;
	  this.hoursApartToResendBounceBackMails=Application.communication.hoursApartToResendBounceBackMails;
	  this.HTMLSpecialCharactersConversionPath=Application.communication.HTMLSpecialCharactersConversionPath;
    }  
  </cfscript>

  <cffunction name="getListsSummaryInfo" access="public" output="false" returntype="query">
    <cfargument name="listIDsList" default="">
    
    <cfquery name="qListsSummary" datasource="#this.datasource#">
	  SELECT listID, listName, published, dateCreated, 0 AS numSubscribers, 0 AS numMembers
	  FROM cm_list
      <cfif Compare(Arguments.listIDsList,"")>
	  WHERE listID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.listIDsList#" list="yes">)
	  </cfif>
	  ORDER BY listName
	</cfquery>
	
	<cfquery name="qNumSubscribers" datasource="#this.datasource#">
	  SELECT listID, COUNT(emailID) AS numSubscribers
	  FROM cm_email_list	  
	  WHERE status='S'
	  GROUP BY listID
	</cfquery>
	
	<cfquery name="qNumMembers" datasource="#this.datasource#">
	  SELECT listID, COUNT(emailID) AS numMembers
	  FROM cm_email_list	  
	  GROUP BY listID
	</cfquery>
	
	<cfloop query="qListsSummary">
	  <cfset tempID=qListsSummary.listID>
	  <cfset tempNumSubscribers=0>
	  <cfset tempNumMembers=0>
	  <cfloop query="qNumSubscribers">
	    <cfif tempID IS qNumSubscribers.listID>
		  <cfset tempNumSubscribers=qNumSubscribers.numSubscribers>
		  <cfbreak>
		</cfif>
	  </cfloop>
	  <cfset qListsSummary.numSubscribers=tempNumSubscribers>
	  
	  <cfloop query="qNumMembers">
	    <cfif tempID IS qNumMembers.listID>
		  <cfset tempNumMembers=qNumMembers.numMembers>
		  <cfbreak>
		</cfif>
	  </cfloop>
	  <cfset qListsSummary.numMembers=tempNumMembers>	  
	</cfloop>
	<cfreturn qListsSummary>  
  </cffunction>
  
  <cffunction name="getAllListNames" access="public" output="no" returntype="query">
    <cfargument name="excludedListID" type="numeric" required="no">
	
    <cfquery name="qAllListNames" datasource="#this.datasource#">
	  SELECT listID, listName
	  FROM cm_list
      <cfif IsDefined("Arguments.excludedListID")>
	  WHERE listID <> <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.excludedListID#">
	  </cfif>
	  ORDER BY listName
	</cfquery>
	<cfreturn qAllListNames>
  </cffunction>  
  
  <cffunction name="getFirstListID" access="public" output="no" returntype="numeric">
    <cfquery name="qListID" datasource="#this.datasource#">
      SELECT listID
      FROM cm_list
      ORDER BY listName
    </cfquery>
    <cfif qListID.recordcount GT 0>
      <cfreturn qListID.listID>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="addList" access="public" output="false">
    <cfargument name="listName" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    
    <cfquery datasource="#this.datasource#">
	  INSERT INTO cm_list (listName, published, dateCreated, dateLastModified)
	  VALUES (
	    <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.listName#">,
		<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
		<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">	  
	  )
	</cfquery>
  </cffunction>
  
  <cffunction name="getListByListID" access="public" output="false" returntype="query">
    <cfargument name="listID" type="numeric" required="yes">
	
	<cfquery name="qList" datasource="#this.datasource#">
	  SELECT *
	  FROM cm_list
	  WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">
	</cfquery>
	<cfreturn qList>
  </cffunction>
  
  <cffunction name="editList" access="public" output="false">
    <cfargument name="listID" type="numeric" required="yes">
    <cfargument name="listName" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    
    <cfquery datasource="#this.datasource#">
	  UPDATE cm_list
	  SET listName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.listName#">,
		  published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
		  dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">
	</cfquery>
  </cffunction>
  
  <cffunction name="deleteList" access="public" output="false">
    <cfargument name="listID" type="numeric" required="yes">
	
    <cfquery datasource="#this.datasource#">
	  DELETE FROM cm_list
	  WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">
	</cfquery>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cm_email_list
	  WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">
	</cfquery>	
    
    <cfquery datasource="#this.datasource#">
	  DELETE FROM cm_signupform_list
	  WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">
	</cfquery>
  </cffunction>
  
  <cffunction name="getListMembersByListID" access="public" output="false" returntype="struct">
    <cfargument name="listID" type="numeric" required="yes">
    <cfargument name="pageNum" type="numeric" required="no" default="1">
	<cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="100">
	<cfargument name="sortBy" type="string" default="email">
	<cfargument name="order" type="string" default="ASC">
	
	<cfset var resultStruct = StructNew()>
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qNumMembers" datasource="#this.datasource#">
	  SELECT COUNT(cm_email_list.emailID) AS num
	  FROM cm_email_list JOIN cm_email ON
	  	   cm_email_list.emailID=cm_email.emailID
	  WHERE cm_email_list.listID=<cfqueryparam value="#Arguments.listID#" cfsqltype="cf_sql_integer">
	</cfquery> 
	<cfset resultStruct.numAllItems=qNumMembers.num>	
	
	<cfquery name="qMembers" datasource="#this.datasource#">
	  SELECT cm_email_list.emailID, cm_email_list.status, cm_email_list.numBouncedCampaigns,
	  		 cm_email.firstName, cm_email.lastName, cm_email.email
	  FROM cm_email_list JOIN cm_email ON
	  	   cm_email_list.emailID=cm_email.emailID
	  WHERE cm_email_list.listID=<cfqueryparam value="#Arguments.listID#" cfsqltype="cf_sql_integer">
	  <cfif Arguments.sortBy IS "firstName">
	  ORDER BY cm_email.firstName #Arguments.order#
	  <cfelseif Arguments.sortBy IS "lastName">
	  ORDER BY cm_email.lastName #Arguments.order#
	  <cfelseif Arguments.sortBy IS "status">
	  ORDER BY cm_email_list.status #Arguments.order#
      <cfelseif Arguments.sortBy IS "bounces">
      ORDER BY cm_email_list.numBouncedCampaigns #Arguments.order#
	  <cfelse>
	  ORDER BY cm_email.email #Arguments.order#
	  </cfif>
	  LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
	<cfset resultStruct.numDisplayedItems=qMembers.recordcount>
	<cfset resultStruct.members = qMembers>
		
	<cfreturn resultStruct>  
  </cffunction>
  
  <cffunction name="searchMembers" access="public" output="false" returntype="struct">
    <cfargument name="keyword" type="string" required="yes">
    <cfargument name="status" type="string" required="yes">
    <cfargument name="listIDList" type="string" required="yes">
    <cfargument name="pageNum" type="numeric" required="no" default="1">
	<cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="100">
    
    <cfset var resultStruct = StructNew()>
    <cfset resultStruct.numAllItems=0>
    <cfset resultStruct.numDisplayedItems=0>
    <cfif Not Compare(Arguments.listIDList, "")><cfreturn resultStruct></cfif>
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
    	
	<cfquery name="qNumMembers" datasource="#this.datasource#">
	  SELECT COUNT(cm_email_list.emailID) AS num
	  FROM (cm_email_list INNER JOIN cm_list ON cm_email_list.listID=cm_list.listID) INNER JOIN cm_email ON
	   		cm_email_list.emailID=cm_email.emailID
	  WHERE cm_email_list.listID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.listIDList#" list="yes">)
			<cfif Compare(Arguments.status,"")>
			AND status=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.status#">
			</cfif> 
	   		<cfif Compare(Arguments.keyword,"")>
			AND (cm_email.email LIKE '%#Arguments.keyword#%' OR cm_email.firstName LIKE '%#Arguments.keyword#%' OR cm_email.lastName LIKE '%#Arguments.keyword#%')
			</cfif>
	</cfquery> 
	<cfset resultStruct.numAllItems=qNumMembers.num>
	
	<cfquery name="qMembers" datasource="#this.datasource#">
	  SELECT cm_email_list.listID, cm_email_list.emailID, cm_email_list.status, cm_email_list.numBouncedCampaigns,
	   		 cm_list.listName, cm_email.email, cm_email.firstName, cm_email.lastName
	  FROM (cm_email_list INNER JOIN cm_list ON cm_email_list.listID=cm_list.listID) INNER JOIN cm_email ON
	   		cm_email_list.emailID=cm_email.emailID
	  WHERE cm_email_list.listID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.listIDList#" list="yes">)
			<cfif Compare(Arguments.status,"")>
			AND status=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.status#">
			</cfif> 
	   		<cfif Compare(Arguments.keyword,"")>
			AND (cm_email.email LIKE '%#Arguments.keyword#%' OR cm_email.firstName LIKE '%#Arguments.keyword#%' OR cm_email.lastName LIKE '%#Arguments.keyword#%')
			</cfif>
	  ORDER BY cm_list.listName, cm_email.email
	  LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
	<cfset resultStruct.numDisplayedItems=qMembers.recordcount>
	<cfset resultStruct.members = qMembers>
		
	<cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="isEmailExistInList" access="public" output="false" returntype="boolean">
    <cfargument name="email" type="string" required="yes">
	<cfargument name="listID" type="numeric" required="yes">
	
	<cfquery name="qGetExistence" datasource="#this.datasource#">
	  SELECT cm_email_list.emailID
	  FROM cm_email_list JOIN cm_email ON
	  	   cm_email_list.emailID=cm_email.emailID
	  WHERE cm_email.email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#"> AND
			cm_email_list.listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">
	</cfquery>
	<cfif qGetExistence.recordcount>
	  <cfreturn true>
	<cfelse>
	  <cfreturn false>
	</cfif>
  </cffunction>
  
  <cffunction name="getEmailByEmailID" access="public" output="false" returntype="string">
    <cfargument name="emailID" type="numeric" required="yes">
	
	<cfquery name="qEmail" datasource="#this.datasource#">
	  SELECT email
	  FROM cm_email
	  WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
	</cfquery>
	<cfif qEmail.recordcount IS 0>
	  <cfreturn "">
	<cfelse>
	  <cfreturn qEmail.email>
	</cfif>
  </cffunction>
  
  <cffunction name="addEmail" access="public" output="no" returntype="numeric">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="firstName" type="string" default="">
    <cfargument name="lastName" type="string" default="">
    
    <cfquery name="qEmailID" datasource="#this.datasource#">
      SELECT emailID
      FROM cm_email
      WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">
    </cfquery>
    <cfif qEmailID.recordcount GT 0>
      <cfif Compare(Arguments.firstName, "") OR Compare(Arguments.lastName, "")>
        <cfquery datasource="#this.datasource#">
          UPDATE cm_email
          SET firstName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.firstName#">,
        	  lastName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.lastName#">,
              dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
          WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qEmailID.emailID#">
        </cfquery>
      </cfif>
      <cfreturn qEmailID.emailID>
    <cfelse>
      <cfquery datasource="#this.datasource#">
        INSERT INTO cm_email (email, firstName, lastName, dateCreated, dateLastModified)
        VALUES (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">,
        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.firstName#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.lastName#">,
                <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
                <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
      </cfquery>
      <cfquery name="qMaxEmailID" datasource="#this.datasource#">
        SELECT MAX(emailID) AS emailID
        FROM cm_email
      </cfquery>
      <cfreturn qMaxEmailID.emailID>
    </cfif>
  </cffunction>
  
  <cffunction name="getMemberInfo" access="public" output="no" returntype="query">
    <cfargument name="listID" type="numeric" required="yes">
	<cfargument name="emailID" type="numeric" required="yes">
	
	<cfquery name="qMemberInfo" datasource="#this.datasource#">
	  SELECT cm_email_list.status,
	  		 cm_email.email, cm_email.firstName, cm_email.lastName
	  FROM cm_email_list JOIN cm_email ON
	  	   cm_email_list.emailID=cm_email.emailID
	  WHERE cm_email_list.listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#"> AND
	  		cm_email_list.emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
	</cfquery>
	<cfreturn qMemberInfo> 
  </cffunction>
  
  <cffunction name="addMember" access="public" output="no" returntype="struct">
    <cfargument name="listID" type="numeric" required="yes">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="firstName" type="string" required="yes">
    <cfargument name="lastName" type="string" required="yes">
    <cfargument name="status" type="string" default="S">
    
    <cfset var resultStruct=StructNew()>
    <cfset resultStruct.SUCCESS=true>
    <cfset resultStruct.MESSAGE="">
    
    <cfif isEmailExistInList(Arguments.email, Arguments.listID)>
      <cfset resultStruct.SUCCESS=true>
      <cfset resultStruct.MESSAGE="It appears that the e-mail address is already in this list.">
    <cfelse>
      <cfset emailID=addEmail(Arguments.email, Arguments.firstName, Arguments.lastName)>
      <cfquery datasource="#this.datasource#">
        INSERT INTO cm_email_list(emailID, listID, status, dateCreated, dateLastModified)
        VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#emailID#">,
        		<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">,
                <cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.status#">,
                <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
                <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
      </cfquery>
    </cfif>
    
    <cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="editMember" access="public" output="no" returntype="struct">
    <cfargument name="listID" type="numeric" required="yes">
    <cfargument name="emailID" type="numeric" required="yes">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="firstName" type="string" required="yes">
    <cfargument name="lastName" type="string" required="yes">
    <cfargument name="status" type="string" default="N">
    
    <cfset var resultStruct=StructNew()>
    <cfset resultStruct.SUCCESS=true>
    <cfset resultStruct.MESSAGE="">
    
    <cfset originalEmail=getEmailByEmailID(Arguments.emailID)>
	<cfif Compare(originalEmail, Arguments.email)><!--- email changed --->
      <cfquery name="qRedunancy" datasource="#this.datasource#">
        SELECT emailID
        FROM cm_email
        WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">
      </cfquery>
    
      <cfif qRedunancy.recordcount GT 0><!--- email address already existed --->
        <cfset resultStruct.SUCCESS=false>
        <cfset resultStruct.MESSAGE="The email address you changed to already existed.">
        <cfreturn resultStruct>
      <cfelse><!--- new email address; update email and name --->
        <cfquery datasource="#this.datasource#"><!--- update name --->
          UPDATE cm_email
          SET email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">,
          	  firstName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.firstName#">,
              lastName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.lastName#">,
              dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
          WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
        </cfquery>
      </cfif>
    <cfelse><!--- same email; just update name --->
      <cfquery datasource="#this.datasource#"><!--- update name --->
        UPDATE cm_email
        SET firstName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.firstName#">,
            lastName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.lastName#">,
            dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
        WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
      </cfquery>
    </cfif>
    
    <cfquery datasource="#this.datasource#"><!--- update status --->
      UPDATE cm_email_list
      SET status=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.status#">,
      	  <cfif Arguments.status Is "S" OR Arguments.status Is "P">
          numBouncedCampaigns=0,
          </cfif>
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#"> AND
            emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
    </cfquery>

    <cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="deleteMembers" access="public" output="no">
    <cfargument name="listID" type="numeric" required="yes">
	<cfargument name="emailIDList" type="string" required="yes">
    
	<cfif Compare(Arguments.emailIDList,"")>
      <cfquery datasource="#this.datasource#">
	    DELETE FROM cm_email_list
	    WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#"> AND
	  		  emailID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#emailIDList#" list="yes">)
	  </cfquery>  
    </cfif>
  </cffunction>
  
  <cffunction name="importMembers" access="public" output="false">
    <cfargument name="listID" type="numeric" required="no">
    <cfargument name="newListID" type="numeric" required="yes">
	<cfargument name="emailIDList" type="string" required="yes">
	
	<cfif Arguments.newListID Is 0 Or Arguments.newListID Is Arguments.listID Or Arguments.emailIDList Is ""><cfreturn></cfif>
    
    <cfquery name="qExistingEmailIDs" datasource="#this.datasource#">
      SELECT emailID
      FROM cm_email_list
      WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.newListID#"> AND
      		emailID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailIDList#" list="yes">)
    </cfquery>
    
    <cfif qExistingEmailIDs.recordcount GT 0>
      <cfset existingEmailIDList=ValueList(qExistingEmailIDs.emailID)>
    <cfelse>
      <cfset existingEmailIDList="">
    </cfif>
    
    <cfquery name="qMemberInfo" datasource="#this.datasource#">
	  SELECT emailID, status
	  FROM cm_email_list
	  WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#"> AND
      	    emailID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailIDList#" list="yes">)
	</cfquery>
    
    <cfloop query="qMemberInfo">
      <cfif ListFind(existingEmailIDList, emailID) Is 0>
        <cfquery datasource="#this.datasource#">
          INSERT INTO cm_email_list(emailID, listID, status, dateCreated, dateLastModified)
          VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#emailID#">,
          		  <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.newListID#">,
                  <cfqueryparam cfsqltype="cf_sql_char" value="#status#">,
                  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
                  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
        </cfquery>
      </cfif>
    </cfloop>
  </cffunction>
  
  <cffunction name="importMembersFromFile" access="public" output="yes">
    <cfargument name="listID" type="numeric" required="yes">
    <cfargument name="absoluteDirectoryTemp" type="string" required="yes">
    <cfargument name="localFileField" type="string" required="yes">  
    
    <cfsetting enablecfoutputonly="yes" requesttimeout="1000">
    
    <cfdirectory action="list" directory="#Arguments.absoluteDirectoryTemp#" filter="*.csv" name="allCSVFiiles">
    <cfloop query="allCSVFiiles"><!--- clean up old import files --->
      <cffile action="delete" file="#Arguments.absoluteDirectoryTemp#/#name#">
    </cfloop>
  
    <cffile action="upload"
            destination="#Arguments.absoluteDirectoryTemp#"
            nameconflict="overwrite"
            mode="775"
            filefield="#Arguments.localFileField#">

    <cfset importMembersFromData(listID=Arguments.listID, CSVFilePath=Arguments.absoluteDirectoryTemp & "/" & cffile.serverFile)>
    </cfsetting>
  </cffunction>
  
  <cffunction name="importMembersFromData" access="public" output="yes">
    <cfargument name="listID" type="numeric" required="yes">
    <cfargument name="CSVData" type="string" required="no">
    <cfargument name="CSVFilePath" type="string" required="no">
    <cfsetting enablecfoutputonly="yes" requesttimeout="1000">
      <cfif IsDefined("Arguments.CSVFilePath")>
        <cfset data=CreateObject("component","com.Utility").CSVToArray(CSVFilePath=Arguments.CSVFilePath)>
      <cfelse>
        <cfset data=CreateObject("component","com.Utility").CSVToArray(CSVData=Arguments.CSVData)>
      </cfif>
    
      <cfset badRecords="">
      <cfset newline=Chr(13) & Chr(10)>
      <cfset numLines=ArrayLen(data)>
      
      <cfset numStatusUpdates=100>
      <cfset numLinesPerStatusUpdate=Ceiling(numLines/numStatusUpdates)>
      
      <cfset statusUpdateNum=1>
      
      <cfloop index="idx1" from="1" to="#numLines#">
        <cfset numItems=ArrayLen(data[idx1])>
        <cfif numItems GT 0>
          <cfset email=Trim(data[idx1][1])>
          <cfif numItems GT 1>
            <cfset firstName=Trim(data[idx1][2])>
          <cfelse>
            <cfset firstName="">
          </cfif>
          <cfif numItems GT 2>
            <cfset lastName=Trim(data[idx1][3])>
          <cfelse>
            <cfset lastName="">
          </cfif>
          <cfif checkEmailAddress(email) AND Len(firstName) LTE 50 AND Len(lastName) LTE 50>
    		<cfset resultStruct=addMember(listID=Arguments.listID, email=email, firstName=firstName, lastName=lastName)>
            <cfif Not resultStruct.SUCCESS>
              <cfset badRecords = badRecords & email & "," & firstName & "," & lastName & newline>
            </cfif>
          <cfelse>
            <cfset badRecords = badRecords & email & "," & firstName & "," & lastName & newline>
          </cfif>
        </cfif>
        
        <cfif idx1 GTE (statusUpdateNum * numLinesPerStatusUpdate)><cfset statusUpdateNum = statusUpdateNum + 1>
          <cfset percentage=Ceiling(idx1 * 100 / numLines)>
          <cfset importStatus = idx1 & " of " & numLines & " records have been processed.">
          <cfoutput>
          <script type="text/javascript">
		  updateStatus(#percentage#);
		  displayImportStatus.innerHTML = '#importStatus#';
		  </script>
          </cfoutput>
          <cfflush>
        </cfif>
      </cfloop>
      
      <cfoutput>
      <script type="text/javascript">
	  updateStatus(100);
	  displayImportStatus.innerHTML = "#numLines# of #numLines# records have been processed.";
	  </script>
      </cfoutput>
      <cfflush>
      
      <cfif Compare(badRecords,"")>
        <cfoutput>
		<p>The following record(s) can't be imported. Please make sure each record has an valid e-mail address and the formatting guidelines are followed.</p>
        <p>You may want to correct the records and use the "Enter your CSV data here" option to re-import the data again.</p>
        
        <p>Invalid Records:</p>
		<p>
        <pre>
#badRecords#
        </pre>
        </p>
		</cfoutput>
      <cfelse>
      	<cfoutput><p align="center"><b>All records have been imported successfully!</b></p></cfoutput>
      </cfif>
      <cfflush>
    </cfsetting>  
  </cffunction>
  
  <cffunction name="getBounceBackErrorMessage" access="public" output="false" returntype="string">
    <cfargument name="listID" type="numeric" required="yes">
	<cfargument name="emailID" type="numeric" required="yes">lastBouncedSendHistoryID
	
	<cfquery name="qLastBouncedSendHistoryID" datasource="#this.datasource#">
	  SELECT lastBouncedSendHistoryID
	  FROM cm_email_list
	  WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#"> AND
	  		emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
	</cfquery>
	
	<cfif qLastBouncedSendHistoryID.recordcount GT 0>
	  <cfquery name="qMessage" datasource="#this.datasource#">
	    SELECT errorMessage
	    FROM cm_bounce
	    WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#"> AND
	  	      sendHistoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qLastBouncedSendHistoryID.lastBouncedSendHistoryID#">
	  </cfquery>
	
	  <cfif qMessage.recordcount GT 0>	    
	    <cfreturn qMessage.errorMessage>
	  </cfif>
	</cfif>
	<cfreturn "">
  </cffunction>
  
  <cffunction name="getAllSignUpForms" access="public" output="no" returntype="query">
    <cfquery name="qSignUpForms" datasource="#this.datasource#">
      SELECT cm_signupform.signUpFormID, cm_signupform.published, cm_signupform.formID, cm_signupform.signUpFormName,
      		 fm_form.formName
      FROM cm_signupform LEFT JOIN fm_form ON
      	   cm_signupform.formID=fm_form.formID
      ORDER BY cm_signupform.signUpFormName  
    </cfquery>
    <cfreturn qSignUpForms>
  </cffunction>
  
  <cffunction name="getSignUpFormsSummary" access="public" output="no" returntype="query">
    <cfquery name="qSignUpForms" datasource="#this.datasource#">
      SELECT cm_signupform.signUpFormID, cm_signupform.published, cm_signupform.formID, cm_signupform.signUpFormName,
      		 fm_form.formName
      FROM cm_signupform LEFT JOIN fm_form ON
      	   cm_signupform.formID=fm_form.formID
      ORDER BY cm_signupform.signUpFormName  
    </cfquery>
    
    <cfquery name="qSignUpFormList" datasource="#this.datasource#">
      SELECT signupFormID, COUNT(listID) AS numLists
      FROM cm_signupform_list
      GROUP BY signupFormID
    </cfquery>
    
    <cfquery name="qSignUpForms" dbtype="query">
      SELECT *
      FROM qSignUpForms, qSignUpFormList
      WHERE qSignUpForms.signUpFormID=qSignUpFormList.signUpFormID
    </cfquery>
    
    <cfreturn qSignUpForms>
  </cffunction>
  
  <cffunction name="getSignUpForm" access="public" output="no" returntype="query">
    <cfargument name="signUpFormID" type="numeric" required="yes">
  
    <cfquery name="qSignUpForm" datasource="#this.datasource#">
      SELECT *
      FROM cm_signupform
      WHERE  signUpFormID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.signUpFormID#">
    </cfquery>
    <cfreturn qSignUpForm>
  </cffunction>
  
  <cffunction name="getSignFormLists" access="public" output="no" returntype="query">
    <cfargument name="signUpFormID" type="numeric" required="yes">
  
    <cfquery name="qSignUpFormLists" datasource="#this.datasource#">
      SELECT cm_list.listID, cm_list.listName
      FROM cm_signupform_list JOIN cm_list ON cm_signupform_list.listID=cm_list.listID
      WHERE  cm_signupform_list.signUpFormID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.signUpFormID#">
    </cfquery>
    <cfreturn qSignUpFormLists>
  </cffunction>

  <cffunction name="addSignUpForm" access="public" output="no" returntype="struct">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="formID" type="numeric" default="0">
    <cfargument name="signUpFormName" type="string" required="yes">
    <cfargument name="headerText" type="string" default="">
    <cfargument name="emailFromName" type="string" default="">
    <cfargument name="emailFromAddress" type="string" default="">
    <cfargument name="emailReplyAddress" type="string" default="">
    <cfargument name="listIDList" type="string" required="yes">
    
    <cfset var resultStruct=StructNew()>
    <cfset resultStruct.SUCCESS=true>
    <cfset resultStruct.MESSAGE="">
    
    <cfif Compare(Arguments.listIDList,"")>
      <cfquery datasource="#this.datasource#">
        INSERT INTO cm_signupform
          (published, formID, signUpFormName, headerText, emailFromName, emailFromAddress, emailReplyAddress, dateCreated, dateLastModified)
        VALUES (
          <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">,
		  <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.signUpFormName#">,
          <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.headerText#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailFromName#">,
		  <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailFromAddress#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailReplyAddress#">,
          <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
          <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
        )
      </cfquery>
      
      <cfquery name="qNewSignUpFormID" datasource="#this.datasource#">
        SELECT MAX(signUpFormID) AS signUpFormID
        FROM cm_signupform
      </cfquery>
      <cfset newSignUpFormID=qNewSignUpFormID.signUpFormID>
      
      <cfset displaySeq=0>
      <cfloop index="listID" list="#Arguments.listIDList#"><cfset displaySeq = displaySeq + 1>
        <cfquery datasource="#this.datasource#">
          INSERT INTO cm_signupform_list(signupFormID, listID, displaySeq)
          VALUES (
            <cfqueryparam cfsqltype="cf_sql_integer" value="#newSignUpFormID#">,
            <cfqueryparam cfsqltype="cf_sql_integer" value="#listID#">,
            <cfqueryparam cfsqltype="cf_sql_float" value="#displaySeq#">
          )
        </cfquery>
      </cfloop>
    <cfelse>
      <cfset resultStruct.SUCCESS=false>
      <cfset resultStruct.MESSAGE="Please select at least a list for the sign-up form.">
    </cfif>

    <cfreturn resultStruct>  
  </cffunction>

  <cffunction name="editSignUpForm" access="public" output="no">
    <cfargument name="signUpFormID" type="numeric" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="formID" type="numeric" default="0">
    <cfargument name="signUpFormName" type="string" required="yes">
    <cfargument name="headerText" type="string" default="">
    <cfargument name="emailFromName" type="string" default="">
    <cfargument name="emailFromAddress" type="string" default="">
    <cfargument name="emailReplyAddress" type="string" default="">
    <cfargument name="listIDList" type="string" required="yes">
    
    <cfset var resultStruct=StructNew()>
    <cfset resultStruct.SUCCESS=true>
    <cfset resultStruct.MESSAGE="">
    
    <cfif Compare(Arguments.listIDList,"")>
      <cfquery datasource="#this.datasource#">
        UPDATE cm_signupform
        SET published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
        	formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">,
            signUpFormName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.signUpFormName#">,
            headerText=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.headerText#">,
            emailFromName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailFromName#">,
		    emailFromAddress=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailFromAddress#">,
            emailReplyAddress=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailReplyAddress#">,
            dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
        WHERE signUpFormID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.signUpFormID#">
      </cfquery>
      
      <cfquery datasource="#this.datasource#">
        DELETE FROM cm_signupform_list
        WHERE signUpFormID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.signUpFormID#">
      </cfquery>
      
      <cfset displaySeq=0>
      <cfloop index="listID" list="#Arguments.listIDList#"><cfset displaySeq = displaySeq + 1>
        <cfquery datasource="#this.datasource#">
          INSERT INTO cm_signupform_list(signupFormID, listID, displaySeq)
          VALUES (
            <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.signUpFormID#">,
            <cfqueryparam cfsqltype="cf_sql_integer" value="#listID#">,
            <cfqueryparam cfsqltype="cf_sql_float" value="#displaySeq#">
          )
        </cfquery>
      </cfloop>
    <cfelse>
      <cfset resultStruct.SUCCESS=false>
      <cfset resultStruct.MESSAGE="Please select at least a list for the sign-up form.">
    </cfif>

    <cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="deleteSignUpForm" access="public" output="no">
    <cfargument name="signUpFormID" type="numeric" required="yes">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cm_signupform_list
      WHERE signUpFormID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.signUpFormID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cm_signupform
      WHERE signUpFormID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.signUpFormID#">
    </cfquery>
  </cffunction>

  <cffunction name="getMessages" access="public" output="no" returntype="struct">
    <cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
	
	<cfset var resultStruct = StructNew()>
    <cfset resultStruct.numAllItems=0>
    <cfset resultStruct.numDisplayedItems=0>
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qNumMessages" datasource="#this.datasource#">
	  SELECT COUNT(messageID) AS num
	  FROM cm_message
	</cfquery>
	<cfset resultStruct.numAllItems=qNumMessages.num>	
	
	<cfquery name="qMessages" datasource="#this.datasource#">
	  SELECT messageID, messageName, emailSubject,sendMode, dateCreated, dateScheduledSend, dateFirstSend, status, published
	  FROM cm_message
	  ORDER BY dateCreated DESC
	  LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    <cfset resultStruct.numDisplayedItems=qMessages.recordcount>
	<cfset resultStruct.messages = qMessages>
		
	<cfreturn resultStruct> 
  </cffunction>

  <cffunction name="getAllEmailTemplates" access="public" output="no" returntype="query">
    <cfquery name="qTemplates" datasource="#this.datasource#">
	  SELECT *
	  FROM cm_emailtemplate
	  ORDER BY displaySeq
	</cfquery>
    <cfreturn qTemplates>
  </cffunction>
  
  <cffunction name="getMessage" access="public" output="no" returntype="query">
    <cfargument name="messageID" type="numeric" required="yes">
    
	<cfquery name="qMessage" datasource="#this.datasource#">
	  SELECT *
	  FROM cm_message
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>  
  
    <cfreturn qMessage>
  </cffunction>

  <cffunction name="addMessage" access="public" output="no" returntype="struct">
    <cfargument name="messageName" type="string">
	<cfargument name="emailSubject" type="string">
	<cfargument name="emailFromName" type="string" default="">
	<cfargument name="emailFromAddress" type="string">
    <cfargument name="emailReplyAddress" type="string">
	<cfargument name="sendToListIDsList" type="string">
	<cfargument name="testAddress" type="string" default="">
    <cfargument name="signUpFormID" type="numeric" default="0">
	<cfargument name="emailBodyHTML" type="string">
	<cfargument name="emailTemplate" type="string">
	<cfargument name="emailBodyText" type="string">
	<cfargument name="sendMode" type="string"><!--- U: undetermined, R: real-time, S:scheduled --->
	<cfargument name="dateScheduled" type="string" default="">
	<cfargument name="timeScheduled_hh" type="string" default="">
	<cfargument name="timeScheduled_mm" type="string" default="">
	<cfargument name="timeScheduled_ampm" type="string" default="">
	
    <cfset var result=StructNew()>
	<cfset result.SUCCESS=true>
	<cfset result.MESSAGE="">
	
	<cfif IsDefined("Arguments.emailBodyStyleSheetPath")>
	  <cfset Arguments.emailBodyHTML=replaceByInlineStyle(Arguments.emailBodyHTML, Arguments.emailBodyStyleSheetPath)>  
	</cfif>
	
	<cfset 	Arguments.emailBodyHTML=convertHTMLSpecialCharacters(Arguments.emailBodyHTML)>
		
	<cfquery datasource="#this.datasource#">
	  INSERT INTO cm_message
	    (messageName, emailSubject, emailFromName, emailFromAddress, emailReplyAddress,
		 signUpFormID, emailTemplate, testAddress, emailBodyHTML, emailBodyText,
		 sendMode, dateCreated,	 dateLastModified)
	  VALUES
	    (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.messageName#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailSubject#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailFromName#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailFromAddress#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailReplyAddress#">,
		 
         <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.signUpFormID#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailTemplate#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.testAddress#">, 
		 <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.emailBodyHTML#">,
		 <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.emailBodyText#">,
		 
		 <cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.sendMode#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,		 
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)		 
	</cfquery>
	<cfquery name="getMaxID" datasource="#this.datasource#">
	  SELECT MAX(messageID) AS ID
	  FROM cm_message
	</cfquery>
	<cfset messageID = getMaxID.ID>	
	
	<!--- get complete HTML Email: completeHTMLEmail --->
	<cftry>
	  <cfhttp url="#this.siteURLRootForCFHTTP#/emailtemplates/#Arguments.emailTemplate#" method="post">
	    <cfhttpparam type="formfield" name="emailBodyHTML" value="#Arguments.emailBodyHTML#">
	    <cfhttpparam type="formfield" name="messageID" value="#messageID#">
	  </cfhttp>
	  <cfset completeHTMLEmail=cfhttp.FileContent>
	<cfcatch>
	  <cfset deleteMessage(messageID)>
	  <cfset result.SUCCESS=false>
	  <cfset result.MESSAGE="Can not apply e-mail template to the HTML e-mail body.\n\n#cfcatch.Message#">
	  <cfreturn result>
	</cfcatch>
	</cftry>	
	
	<cfset completeHTMLEmail=replaceBodyLink(strInputBody="#completeHTMLEmail#", messageID="#messageID#")>	
	<cfquery datasource="#this.datasource#">
	  UPDATE cm_message
	  SET completeHTMLEmail=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#completeHTMLEmail#">
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#messageID#">
	</cfquery>
	
	<cfloop index="listID" list="#Arguments.sendToListIDsList#" delimiters=",">
	  <cfquery datasource="#this.datasource#">
	    INSERT INTO cm_message_list
		  (messageID, listID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#messageID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#listID#">)
	  </cfquery>
	</cfloop>	
	
	<!--- define status: P: pending, Q: in queue, S: sent --->
	<cfswitch expression="#sendMode#">
	  <cfcase value="U"><!--- undetermined --->
		<cfset updateMessageStatus(messageID, "P")>
		<cfset result.MESSAGE="The campaign ""#Arguments.messageName#"" has been saved.">
	  </cfcase>
	  <cfcase value="S"><!--- scheduled --->
	    <cfset updateMessageStatus(messageID, "P")>
		<cfset dateTimeObj=CreateObject("component", "Utility").ConvertStringToDateTimeObject(Arguments.dateScheduled, Arguments.timeScheduled_hh, Arguments.timeScheduled_mm, 0, Arguments.timeScheduled_ampm)>
		<cfquery datasource="#this.datasource#">
		  UPDATE cm_message
		  SET dateScheduledSend=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#dateTimeObj#">
		  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#messageID#">
		</cfquery>
		<cfset result.MESSAGE="The campaign ""#Arguments.messageName#"" has been scheduled to send at #DateFormat(dateTimeObj, "mm/dd/yyyy")# #TimeFormat(dateTimeObj, "hh:mm tt")#.">
	  </cfcase>
	  <cfcase value="R"><!--- real time --->
		<cfset moveMessageToEmailQueue(messageID)>
		<cfset result.MESSAGE="Congratulation! You have successfully placed the campaign ""#Arguments.messageName#"" in the e-mail queue for delivery.">
	  </cfcase>	
	</cfswitch> 
  
    <cfif Compare(Arguments.testAddress, "")>
	  <cfset sendToTestAddress(messageID)>
	</cfif>
	  
    <cfreturn result>
  </cffunction>
  
  <cffunction name="editMessage" access="public" output="no" returntype="struct">
    <cfargument name="messageID" type="numeric">
    <cfargument name="messageName" type="string">
	<cfargument name="emailSubject" type="string">
	<cfargument name="emailFromName" type="string" default="">
	<cfargument name="emailFromAddress" type="string">
    <cfargument name="emailReplyAddress" type="string">
	<cfargument name="sendToListIDsList" type="string">
	<cfargument name="testAddress" type="string" default="">
    <cfargument name="signUpFormID" type="numeric" default="0">
	<cfargument name="emailBodyHTML" type="string">
	<cfargument name="emailTemplate" type="string">
	<cfargument name="emailBodyText" type="string">
	<cfargument name="sendMode" type="string"><!--- U: undetermined, R: real-time, S:scheduled --->
	<cfargument name="dateScheduled" type="string" default="">
	<cfargument name="timeScheduled_hh" type="string" default="">
	<cfargument name="timeScheduled_mm" type="string" default="">
	<cfargument name="timeScheduled_ampm" type="string" default="">
	
    <cfset var result=StructNew()>
	<cfset result.SUCCESS=true>
	<cfset result.MESSAGE="">
	
	<cfif IsDefined("Arguments.emailBodyStyleSheetPath")>
	  <cfset Arguments.emailBodyHTML=replaceByInlineStyle(Arguments.emailBodyHTML, Arguments.emailBodyStyleSheetPath)>	  
	</cfif>	
	
	<cfset 	Arguments.emailBodyHTML=convertHTMLSpecialCharacters(Arguments.emailBodyHTML)>
	
	<cfquery datasource="#this.datasource#">
	  UPDATE cm_message
	  SET messageName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.messageName#">,
	  	  emailSubject=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailSubject#">,
		  emailFromName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailFromName#">,
		  emailFromAddress=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailFromAddress#">,
          emailReplyAddress=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailReplyAddress#">,
		  emailBodyHTML=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.emailBodyHTML#">,
		  emailBodyText=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.emailBodyText#">,
		  emailTemplate=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailTemplate#">,
		  testAddress=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.testAddress#">,
          signUpFormID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.signUpFormID#">,
		  sendMode=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.sendMode#">,
		  dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>
	
	<!--- get complete HTML Email: completeHTMLEmail --->
	<cftry>
	  <cfhttp url="#this.siteURLRootForCFHTTP#/emailtemplates/#Arguments.emailTemplate#" method="post">
	    <cfhttpparam type="formfield" name="emailBodyHTML" value="#Arguments.emailBodyHTML#">
	    <cfhttpparam type="formfield" name="messageID" value="#Arguments.messageID#">
	  </cfhttp>
	  <cfset completeHTMLEmail=cfhttp.FileContent>
	<cfcatch>
	  <cfset deleteMessage(messageID)>
	  <cfset result.SUCCESS=false>
	  <cfset result.MESSAGE="Can not apply e-mail template to the HTML e-mail body.<br><br>#cfcatch.Message#">
	  <cfreturn result>
	</cfcatch>
	</cftry>
	
	<cfset completeHTMLEmail=replaceBodyLink(strInputBody="#completeHTMLEmail#", messageID="#Arguments.messageID#")>	
	<cfquery datasource="#this.datasource#">
	  UPDATE cm_message
	  SET completeHTMLEmail=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#completeHTMLEmail#">
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>

	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cm_message_list WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>
	
	<cfloop index="listID" list="#Arguments.sendToListIDsList#" delimiters=",">
	  <cfquery datasource="#this.datasource#">
	    INSERT INTO cm_message_list
		  (messageID, listID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#listID#">)
	  </cfquery>
	</cfloop>	
	
	<!--- define status: P: pending, Q: in queue, S: sent --->
	<cfswitch expression="#sendMode#">
	  <cfcase value="U"><!--- undetermined --->		
		<cfset updateMessageStatus(Arguments.messageID, "P")>
		<cfset result.MESSAGE="The campaign ""#Arguments.messageName#"" has been saved.">
	  </cfcase>
	  <cfcase value="S"><!--- scheduled --->
	    <cfset updateMessageStatus(Arguments.messageID, "P")>
		<cfset dateTimeObj=CreateObject("component", "Utility").ConvertStringToDateTimeObject(Arguments.dateScheduled, Arguments.timeScheduled_hh, Arguments.timeScheduled_mm, 0, Arguments.timeScheduled_ampm)>
		<cfquery datasource="#this.datasource#">
		  UPDATE cm_message
		  SET dateScheduledSend=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#dateTimeObj#">
		  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
		</cfquery>
		<cfset result.message="The campaign ""#Arguments.messageName#"" has been scheduled to send at #DateFormat(dateTimeObj, "mm/dd/yyyy")# #TimeFormat(dateTimeObj, "hh:mm tt")#.">
	  </cfcase>
	  <cfcase value="R"><!--- real time --->	    	
		<cfset removeMessageFromEmailQueue(Arguments.messageID)>
		<cfset moveMessageToEmailQueue(Arguments.messageID)>
		<cfset result.message="Congratulation! You have successfully placed the campaign ""#Arguments.messageName#"" in the e-mail queue for delivery.">
	  </cfcase>	
	</cfswitch>
	
	<cfif Compare(Arguments.testAddress, "")>
	  <cfset sendToTestAddress(Arguments.messageID)>
	</cfif> 
  
    <cfreturn result>
  </cffunction>

  <cffunction name="sendToTestAddress" access="public" output="no">
    <cfargument name="messageID" type="numeric" required="yes">
  
    <cfset messageInfo=getMessage(Arguments.messageID)>
	<cfif messageInfo.recordcount IS 0><cfreturn></cfif>
    <cfif Compare(messageInfo.emailFromName, "")>
	  <cfset emailFrom="#messageInfo.emailFromName# <#messageInfo.emailFromAddress#>">
	<cfelse>
	  <cfset emailFrom="#messageInfo.emailFromAddress#">
	</cfif>
    <cfif Compare(messageInfo.emailReplyAddress, "")>
      <cfset emailReply=messageInfo.emailReplyAddress>
    <cfelse>
      <cfset emailReply=emailFrom>
    </cfif>
    
    <cfset HTMLEmailContent=ReplaceNoCase(messageInfo.completeHTMLEmail, "##emailID##", "0", "ALL")>
	<cfset textEmailContent=messageInfo.emailBodyText & Chr(13) & Chr(10) & Chr(13) & Chr(10)& 
							"This e-mail is being sent to you at your request. " &
							"If you prefer not to receive future e-mail updates, " &
							"please cut and paste this entire link into your Internet browser: " &
							"#this.siteURLRoot#/index.cfm?md=communication&tmp=updateprofile&msgid=#Arguments.messageID#&uid=0">

	<cfmail from="#emailFrom#" replyto="#emailReply#" to="#messageInfo.testAddress#" subject="#messageInfo.emailSubject# (Test)" type="html">
	  <cfmailpart type="text">
#textEmailContent#
	  </cfmailpart>
	  <cfmailpart type="html" charset="utf-8">
		#HTMLEmailContent#
	  </cfmailpart>
	</cfmail>
  </cffunction>

  <cffunction name="moveMessageToEmailQueue" access="public" output="false">
    <cfargument name="messageID" type="numeric" required="yes">
	
	<cfset updateMessageStatus(Arguments.messageID, "Q")>	
  
    <cfquery name="qEmails" datasource="#this.datasource#">
	  SELECT DISTINCT cm_email.emailID, cm_email.email
	  FROM (cm_message_list INNER JOIN cm_email_list ON
	  	    cm_message_list.listID=cm_email_list.listID) INNER JOIN cm_email ON
		    cm_email_list.emailID=cm_email.emailID
	  WHERE cm_message_list.messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#"> AND
	  		cm_email_list.status='S'
	  ORDER BY cm_email.email  
    </cfquery>
	
	<cfquery datasource="#this.datasource#">
	  INSERT INTO cm_sendhistory
	    (messageID, attemptNum, dateSent, numSent)
	  VALUES
	    (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">,
		 1,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		 <cfqueryparam cfsqltype="cf_sql_integer" value="#qEmails.recordcount#">)
	</cfquery>
	<cfquery name="getMaxID" datasource="#this.datasource#">
	  SELECT MAX(sendHistoryID) AS ID
	  FROM cm_sendhistory
	</cfquery>
	<cfset sendHistoryID=getMaxID.ID>
	
	<cfset nextResendDateTime1=DateAdd("h", this.hoursApartToResendBounceBackMails, now())>
	<cfset nextResendDateTime2=DateAdd("h", this.hoursApartToResendBounceBackMails * 2, now())>
	<cfquery datasource="#this.datasource#">
	  INSERT INTO cm_bouncebackresendschedule
	    (messageID, sendHistoryID, resendNum, dateToResend, dateCreated)
	  VALUES
	    (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">,
		 <cfqueryparam cfsqltype="cf_sql_integer" value="#sendHistoryID#">,
		 1,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#nextResendDateTime1#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	</cfquery>
	<cfquery datasource="#this.datasource#">
	  INSERT INTO cm_bouncebackresendschedule
	    (messageID, sendHistoryID, resendNum, dateToResend, dateCreated)
	  VALUES
	    (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">,
		 0,
		 2,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#nextResendDateTime2#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	</cfquery>
	
	<cfquery datasource="#this.datasource#">
	  UPDATE cm_message
	  SET dateFirstSend=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>
		
	<cfset emailsList="">
	<cfif qEmails.recordcount GT 0>
	  <cfset idx = 0>
	  <cfloop query="qEmails">
	    <cfif Compare(email, "")>
			<cfset idx = idx + 1>
			<cfif listLen(emailsList) IS 0>
			  <cfset emailsList = emailID & "," & email>
			<cfelse>
			  <cfset emailsList = emailsList & ";" & emailID & "," & email>
			</cfif>
			<cfif idx GTE this.numEmailsSentPerProcess>
			  <cfquery datasource="#this.datasource#">
				INSERT INTO cm_emailqueue
				 (sendHistoryID, emails, messageID, dateCreated)
				VALUES
				 (<cfqueryparam cfsqltype="cf_sql_integer" value="#sendHistoryID#">,
				  <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#emailsList#">,
				  <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">,
				  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)		
			  </cfquery>		
			  <cfset idx = 0>
			  <cfset emailsList="">	
			</cfif>  
		</cfif>
	  </cfloop>
	  <cfif Compare(emailsList, "")>
		<cfquery datasource="#this.datasource#">
		  INSERT INTO cm_emailqueue
		   (sendHistoryID, emails, messageID, dateCreated)
		  VALUES
		   (<cfqueryparam cfsqltype="cf_sql_integer" value="#sendHistoryID#">,
		    <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#emailsList#">,
		 	<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">,
		 	<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)		
		</cfquery>		
	  </cfif>		
	</cfif> 
  </cffunction>

  <cffunction name="updateMessageStatus" access="public" output="false">
    <cfargument name="messageID" type="numeric" required="yes">
	<cfargument name="status" type="string" required="yes">
    <cfquery datasource="#this.datasource#">
	  UPDATE cm_message
	  SET status=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.status#">
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>
  </cffunction>
  
  <cffunction name="toggleMessageArchive" access="public" output="false">
    <cfargument name="messageID" type="numeric" required="yes">
    
    <cfquery name="qPublished" datasource="#this.datasource#">
	  SELECT  published
      FROM cm_message
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>
    <cfif qPublished.recordcount Gt 0>
      <cfquery datasource="#this.datasource#">
	    UPDATE cm_message
        <cfif qPublished.published>
	    SET published=0
        <cfelse>
        SET published=1
        </cfif>
	    WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	  </cfquery>
    </cfif>
  </cffunction>

  <cffunction name="deleteMessage" access="public" output="false">
    <cfargument name="messageID" type="numeric" required="yes">	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cm_message WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cm_bounce WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cm_bouncebackresendschedule WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cm_emailqueue WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">	
	</cfquery>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cm_message_list WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">	
	</cfquery>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cm_messageclickthru WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">	
	</cfquery>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cm_messageopen WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">	
	</cfquery>
    
    <cfquery datasource="#this.datasource#">
	  DELETE FROM cm_messageforward WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">	
	</cfquery>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cm_sendhistory WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">	
	</cfquery>  
  </cffunction>
  
  <cffunction name="removeMessageFromEmailQueue" access="public" output="false">
    <cfargument name="messageID" type="numeric" required="yes">
  
    <cfquery datasource="#this.datasource#">
	  DELETE FROM cm_sendhistory
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cm_emailqueue
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cm_bouncebackresendschedule
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>		
  </cffunction>
  
  <cffunction name="getMessageListIDsList" access="public" output="no" returntype="string">
    <cfargument name="messageID" type="numeric" required="yes">
	
	<cfquery name="qListIDs" datasource="#this.datasource#">
	  SELECT listID
	  FROM cm_message_list
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>  
    <cfreturn ValueList(qListIDs.listID)>
  </cffunction>
  
  <cffunction name="getSendHistory" access="public" output="no" returntype="query">
    <cfargument name="messageID" type="numeric" required="yes">
	
	<cfquery name="qHistory" datasource="#this.datasource#">
	  SELECT *
	  FROM cm_sendhistory
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	  ORDER BY attemptNum
	</cfquery>
    <cfreturn qHistory>  
  </cffunction> 

  <cffunction name="getSendHistoryDetail" access="public" output="no" returntype="query">
    <cfargument name="messageID" type="numeric" required="yes">
	
	<cfquery name="qLogs" datasource="#this.datasource#">
	  SELECT cm_message.messageName, cm_sendhistory.sendHistoryID,
	  		 cm_sendhistory.dateSent, cm_sendhistory.numSent,
	  		 cm_sendhistory.numBounces, cm_sendhistory.attemptNum
	  FROM cm_sendhistory INNER JOIN cm_message ON
	  	   cm_sendhistory.messageID=cm_message.messageID
	  WHERE cm_sendhistory.messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	  ORDER BY cm_sendhistory.attemptNum ASC
	</cfquery> 
    <cfreturn qLogs>
  </cffunction>

  <cffunction name="getCampaignBasicInfoBySendHistoryID" access="public" output="no" returntype="query">
    <cfargument name="sendHistoryID" type="numeric" required="yes">
    
	<cfquery name="qCampaign" datasource="#this.datasource#">
	  SELECT cm_message.messageName, cm_sendhistory.dateSent, cm_sendhistory.attemptNum
	  FROM cm_message JOIN cm_sendhistory ON
	  	   cm_message.messageID=cm_sendhistory.messageID
	  WHERE cm_sendhistory.sendHistoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.sendHistoryID#">
	</cfquery>  
  
    <cfreturn qCampaign>
  </cffunction>

  <cffunction name="getBounceInfo" access="public" output="false" returntype="struct">
    <cfargument name="sendHistoryID" type="numeric" required="yes">
	<cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="10">
	
	<cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
    <cfset resultStruct.numDisplayedItems=0>
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>	
	
	<cfquery name="qNumBounces" datasource="#this.datasource#">
	  SELECT COUNT(cm_bounce.bounceID) AS num
	  FROM cm_bounce JOIN cm_email ON
	  	   cm_bounce.emailID=cm_email.emailID
	  WHERE sendHistoryID=<cfqueryparam value="#Arguments.sendHistoryID#" cfsqltype="cf_sql_integer">
	</cfquery> 
	<cfset resultStruct.numAllItems=qNumBounces.num>	
	
	<cfquery name="qBounces" datasource="#this.datasource#">
	  SELECT cm_email.email, cm_email.firstName, cm_email.lastName,
	  		 cm_bounce.bounceID, cm_bounce.bounceType, cm_bounce.dateBounced, cm_bounce.errorMessage
	  FROM cm_bounce JOIN cm_email ON
	  	   cm_bounce.emailID=cm_email.emailID
	  WHERE cm_bounce.sendHistoryID=<cfqueryparam value="#Arguments.sendHistoryID#" cfsqltype="cf_sql_integer">	  
	  LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
	<cfset resultStruct.numDisplayedItems=qBounces.recordcount>
	<cfset resultStruct.bounces = qBounces>		
	<cfreturn resultStruct> 
  </cffunction>

  <cffunction name="getCampaignReport" access="public" output="no" returntype="struct">
    <cfargument name="fromDate" type="string" required="yes">
	<cfargument name="toDate" type="string" required="yes">
    <cfargument name="pageNum" type="numeric" default="1">
	<cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="10">
	
    <cfset var resultStruct = StructNew()>	
	<cfset var fromDateObj=DateAdd("d", 0, Arguments.fromDate)>
	<cfset var toDateObj=DateAdd("d", 1, Arguments.toDate)>
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>	
	
	<cfquery name="qAllCampaigns" datasource="#this.datasource#">
	  SELECT COUNT(messageID) AS totNum
	  FROM cm_message
	  WHERE status='S' AND
	  		dateFirstSend >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#fromDateObj#"> AND
	  		dateFirstSend < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#toDateObj#">			
	</cfquery>
	<cfset resultStruct.numAllItems=qAllCampaigns.totNum>
	<!---
	resultStruct
		.numAllItems
		.numDisplayedItems
		.campaign[numDisplayedItems]
			.messageID
			.messageName
			.numEmailsOpened
			.numClickThru
			.numEmailsForwarded
			.emailList[]
			.numEmailsSent[3]
			.dateSent[3]
			.numBounces[3]
	--->	
	
	<cfquery name="qCampaigns" datasource="#this.datasource#">
	  SELECT messageID, messageName, dateFirstSend, numEmailsOpen, numClickThru, numEmailsForwarded
	  FROM cm_message
	  WHERE status='S' AND
	  		dateFirstSend >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#fromDateObj#"> AND
	  		dateFirstSend < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#toDateObj#">
	  ORDER BY dateFirstSend DESC
	  LIMIT #Arguments.startIndex-1#,#Arguments.numItemsPerPage#
	</cfquery>
	<cfset resultStruct.numDisplayedItems=qCampaigns.recordcount>
	
	<cfif qCampaigns.recordcount GT 0>
	  <cfquery name="qEmailLists" datasource="#this.datasource#">
	    SELECT cm_message_list.messageID, cm_list.listName
		FROM cm_message_list INNER JOIN cm_list ON
			 cm_message_list.listID=cm_list.listID
		WHERE cm_message_list.messageID IN (#ValueList(qCampaigns.messageID)#)
		ORDER BY cm_message_list.messageID, cm_list.listName
	  </cfquery>
	  <cfquery name="qSendHistory" datasource="#this.datasource#">
	    SELECT sendHistoryID, messageID, attemptNum, dateSent, numSent, numBounces
		FROM cm_sendhistory
		WHERE cm_sendhistory.messageID IN (#ValueList(qCampaigns.messageID)#)
		ORDER BY messageID, sendHistoryID
	  </cfquery>

	  <cfset resultStruct.campaign=ArrayNew(1)>
	  <cfset idx = 0>
	  <cfloop query="qCampaigns">
	    <cfset idx = idx + 1>
		<cfset resultStruct.campaign[idx]=StructNew()>
		<cfset resultStruct.campaign[idx].messageID = messageID>
		<cfset resultStruct.campaign[idx].messageName = messageName>
		<cfset resultStruct.campaign[idx].numEmailsOpened = numEmailsOpen>
		<cfset resultStruct.campaign[idx].numClickThru = numClickThru>
        <cfset resultStruct.campaign[idx].numEmailsForwarded = numEmailsForwarded>
		<cfset resultStruct.campaign[idx].emailList=ArrayNew(1)>
		<cfset jdx=0>
		<cfset temp=messageID>
		<cfloop query="qEmailLists">
		  <cfif temp IS qEmailLists.messageID>
		    <cfset jdx = jdx + 1>
		    <cfset resultStruct.campaign[idx].emailList[jdx]=qEmailLists.listName>
		  </cfif>
		</cfloop>
		<cfset resultStruct.campaign[idx].numEmailsSent=ArrayNew(1)>
		<cfset resultStruct.campaign[idx].numEmailsSent[1]=0>
		<cfset resultStruct.campaign[idx].numEmailsSent[2]=0>
		<cfset resultStruct.campaign[idx].numEmailsSent[3]=0>
		<cfset resultStruct.campaign[idx].sendHistoryID=ArrayNew(1)>
		<cfset resultStruct.campaign[idx].sendHistoryID[1]=0>
		<cfset resultStruct.campaign[idx].sendHistoryID[2]=0>
		<cfset resultStruct.campaign[idx].sendHistoryID[3]=0>
		<cfset resultStruct.campaign[idx].dateSent=ArrayNew(1)>
		<cfset resultStruct.campaign[idx].dateSent[1]="#DateFormat(dateFirstSend, "mm/dd/yyyy")# #TimeFormat(dateFirstSend, "hh:mm tt")#">
		<cfset resultStruct.campaign[idx].dateSent[2]="">
		<cfset resultStruct.campaign[idx].dateSent[3]="">
		<cfset resultStruct.campaign[idx].numBounces=ArrayNew(1)>
		<cfset resultStruct.campaign[idx].numBounces[1]=0>
		<cfset resultStruct.campaign[idx].numBounces[2]=0>
		<cfset resultStruct.campaign[idx].numBounces[3]=0>
		<cfset jdx = 0>
		<cfloop query="qSendHistory">
		  <cfif temp IS qSendHistory.messageID>
		    <cfif qSendHistory.attemptNum GTE 1 AND qSendHistory.attemptNum LTE 3>
			  <cfset resultStruct.campaign[idx].sendHistoryID[attemptNum]=qSendHistory.sendHistoryID>
			  <cfset resultStruct.campaign[idx].numEmailsSent[attemptNum]=qSendHistory.numSent>
			  <cfset resultStruct.campaign[idx].numBounces[attemptNum]=qSendHistory.numBounces>			  
			  <cfset resultStruct.campaign[idx].dateSent[attemptNum]="#DateFormat(qSendHistory.dateSent, "mm/dd/yyyy")# #TimeFormat(qSendHistory.dateSent, "hh:mm tt")#">
			</cfif>
		  </cfif>		
		</cfloop>
	  </cfloop> 
    </cfif>
	
    <cfreturn resultStruct>
  </cffunction> 

  <cffunction name="getEmailsByMessageOpened" access="public" output="no" returntype="struct">
    <cfargument name="messageID" type="numeric" required="yes">
    <cfargument name="pageNum" type="numeric" default="1">
	<cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="100">
	
	<cfset var resultStruct = StructNew()>
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>	
  
    <cfquery name="qNumMembers" datasource="#this.datasource#">
	  SELECT COUNT(cm_messageopen.emailID) AS num
	  FROM cm_messageopen JOIN cm_email ON
	  	   cm_messageopen.emailID=cm_email.emailID
	  WHERE cm_messageopen.messageID=<cfqueryparam value="#Arguments.messageID#" cfsqltype="cf_sql_integer">
	</cfquery> 
	<cfset resultStruct.numAllItems=qNumMembers.num>	
  
	<cfquery name="qMembers" datasource="#this.datasource#">
	  SELECT cm_messageopen.emailID, cm_messageopen.dateOpen,
	  		 cm_email.firstName, cm_email.lastName, cm_email.email
	  FROM cm_messageopen JOIN cm_email ON
	  	   cm_messageopen.emailID=cm_email.emailID
	  WHERE cm_messageopen.messageID=<cfqueryparam value="#Arguments.messageID#" cfsqltype="cf_sql_integer">
	  ORDER BY cm_email.email
	  LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
	<cfset resultStruct.numDisplayedItems=qMembers.recordcount>
	<cfset resultStruct.members = qMembers>
		
	<cfreturn resultStruct>  
  </cffunction>
  
  <cffunction name="getEmailsByMessageForwarded" access="public" output="no" returntype="struct">
    <cfargument name="messageID" type="numeric" required="yes">
    <cfargument name="pageNum" type="numeric" default="1">
	<cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="100">
	
	<cfset var resultStruct = StructNew()>
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>	
  
    <cfquery name="qNumMembers" datasource="#this.datasource#">
	  SELECT COUNT(cm_messageforward.emailID) AS num
	  FROM cm_messageforward JOIN cm_email ON
	  	   cm_messageforward.emailID=cm_email.emailID
	  WHERE cm_messageforward.messageID=<cfqueryparam value="#Arguments.messageID#" cfsqltype="cf_sql_integer">
	</cfquery> 
	<cfset resultStruct.numAllItems=qNumMembers.num>	
  
	<cfquery name="qMembers" datasource="#this.datasource#">
	  SELECT cm_messageforward.emailID, cm_messageforward.emailForwardedTo, cm_messageforward.dateForwarded,
	  		 cm_email.firstName, cm_email.lastName, cm_email.email
	  FROM cm_messageforward JOIN cm_email ON
	  	   cm_messageforward.emailID=cm_email.emailID
	  WHERE cm_messageforward.messageID=<cfqueryparam value="#Arguments.messageID#" cfsqltype="cf_sql_integer">
	  ORDER BY cm_email.email
	  LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
	<cfset resultStruct.numDisplayedItems=qMembers.recordcount>
	<cfset resultStruct.members = qMembers>
		
	<cfreturn resultStruct>  
  </cffunction>

  <cffunction name="getClickThruByMessageID" access="public" output="no" returntype="query">
    <cfargument name="messageID" type="numeric" required="yes">
	
	<cfquery name="qClickThru" datasource="#this.datasource#">
	  SELECT linkURL, MIN(messageClickThruID) AS messageClickThruID, SUM(numClicks) totalClicks
	  FROM cm_messageclickthru
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	  GROUP BY linkURL
	  ORDER BY totalClicks DESC
	</cfquery>
  
    <cfreturn qClickThru>  
  </cffunction>
    
  <cffunction name="getEmailsByClickThru" access="public" output="no" returntype="struct">
    <cfargument name="messageID" type="numeric" required="yes">
	<cfargument name="linkURL" type="string" required="yes">
    <cfargument name="pageNum" type="numeric" default="1">
	<cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="100">
	
	<cfset var resultStruct = StructNew()>

    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>	
  
    <cfquery name="qNumMembers" datasource="#this.datasource#">
	  SELECT COUNT(cm_messageclickthru.emailID) AS num
	  FROM cm_messageclickthru JOIN cm_email ON
	  	   cm_messageclickthru.emailID=cm_email.emailID
	  WHERE cm_messageclickthru.messageID=<cfqueryparam value="#Arguments.messageID#" cfsqltype="cf_sql_integer"> AND
	  		cm_messageclickthru.linkURL=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.linkURL#">
	</cfquery> 
	<cfset resultStruct.numAllItems=qNumMembers.num>	
  
	<cfquery name="qMembers" datasource="#this.datasource#">
	  SELECT cm_messageclickthru.emailID,
	  		 cm_messageclickthru.numClicks,
	         cm_messageclickthru.dateLastClicked,
	  		 cm_email.firstName, cm_email.lastName, cm_email.email
	  FROM cm_messageclickthru JOIN cm_email ON
	  	   cm_messageclickthru.emailID=cm_email.emailID
	  WHERE cm_messageclickthru.messageID=<cfqueryparam value="#Arguments.messageID#" cfsqltype="cf_sql_integer"> AND
	  		cm_messageclickthru.linkURL=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.linkURL#">
	  ORDER BY cm_email.email
	  LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
	<cfset resultStruct.numDisplayedItems=qMembers.recordcount>
	<cfset resultStruct.members = qMembers>
		
	<cfreturn resultStruct>  
  </cffunction>
  
  <cffunction name="getURLByMessageClickThruID" access="public" output="no" returntype="string">
    <cfargument name="messageClickThruID" type="numeric" required="yes">
	
    <cfquery name="qURL" datasource="#this.datasource#">
	  SELECT linkURL
	  FROM cm_messageclickthru
	  WHERE messageClickThruID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageClickThruID#">
	</cfquery>
	
	<cfif qURL.recordcount IS 0>
	  <cfreturn "">
	<cfelse>
	  <cfreturn qURL.linkURL>
	</cfif>
  </cffunction>
  
  <cffunction name="generateCSV" access="public" output="no" returntype="string">
    <cfargument name="listID" type="numeric" required="yes">
    <cfargument name="CSVDirectory" type="string" required="yes">		

	<cfquery name="qSubmissions" datasource="#this.datasource#">
	  SELECT cm_email.email, cm_email.firstName, cm_email.lastName, cm_email_list.status
	  FROM cm_email_list JOIN cm_email ON
	  	   cm_email_list.emailID = cm_email.emailID
	  WHERE cm_email_list.listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">
	  ORDER BY cm_email.email
	</cfquery>
    
    <cftry>
	  <cfdirectory action="list" directory="#Arguments.CSVDirectory#" name="allFiles">
	  <cfloop query="allFiles">
	    <cfif type IS "File">
	      <cffile action="delete" file="#Arguments.CSVDirectory#/#name#">
	    </cfif>
	  </cfloop>
	  <cfcatch></cfcatch>
    </cftry>
	
	<cfset dateStr = DateFormat(now(), "mm_dd_yyyy")>
	
	<cfset fileName="list_" & Arguments.listID & "_" & dateStr & ".csv">
	<cfset newline=Chr(13) & Chr(10)>
    <cfsavecontent variable="CSVFile"><cfoutput>E-Mail Address,First Name,Last Name,Status#newline#<cfloop query="qSubmissions">#Replace(email,","," ","ALL")#,#Replace(firstName,","," ","ALL")#,#Replace(lastName,","," ","ALL")#,<cfif NOT Compare(status, "S")>Subscribed<cfelseif NOT Compare(status, "U")>Unsubscribed<cfelseif NOT Compare(status, "P")>Pending<cfelseif NOT Compare(status, "B")>Bad<cfelse>Invalid</cfif>#newline#</cfloop></cfoutput></cfsavecontent>
	<cffile action="write" file="#Arguments.CSVDirectory#/#fileName#" output="#CSVFile#" nameconflict="overwrite">
    <cfreturn fileName>
  </cffunction>
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  







  
  <cffunction name="checkEmailAddress" access="private" output="false" returntype="boolean">
    <cfargument name="emailAddress" type="string" required="yes">
	<cfif Find(Arguments.emailAddress, " ") IS NOT 0><cfreturn false></cfif>
	<cfif ReFind("[A-Za-z0-9_\-\.\+']+@[A-Za-z0-9_\-\.]+\.[A-Za-z]+", Arguments.emailAddress) IS 1>
	  <cfreturn true>
	<cfelse>
	  <cfreturn false>
	</cfif>  
  </cffunction>
  
  <cffunction name="replaceByInlineStyle" access="public" output="false" returntype="string">
    <cfargument name="emailBodyHTML" type="string" required="yes">
    <cfargument name="emailBodyStyleSheetPath" type="string" required="yes">
	
	<cftry>
	  <cffile action="read" file="#Arguments.emailBodyStyleSheetPath#" variable="stylesheet">
	  
	  <cfscript>
		styleName=ArrayNew(1);
		styleDefinition=ArrayNew(1);
		newline = Chr(13) & Chr(10);
		count = 0;
		idx=0;
		startpos = 1;
		while (startpos IS NOT 0 AND count LTE 100) {
		  count = count + 1;
		  st=REFind("\.[a-zA-Z0-9\- ]+\{[^}]+\}", stylesheet, startpos, "TRUE");
		  startpos = st.pos[1];		  
		  if (startpos IS NOT 0) {
			idx = idx + 1;
			length = st.len[1];
			style = Trim(MID(stylesheet, startpos, length));
			sName=REFind("\.[a-zA-Z0-9\-]+", style, 1, "TRUE");
			styleName[idx]=Trim(MID(style, sName.pos[1]+1, sName.len[1]-1));
			sDefinition=REFind("\{[^}]+\}", style, 1, "TRUE");
			styleDefinition[idx]=Replace(MID(style, sDefinition.pos[1]+1, sDefinition.len[1]-2),newline," ","ALL");
			
			startpos = startpos + length-1;
			//WriteOutput(styleName[idx] & "<br>");
			//WriteOutput(styleDefinition[idx] & "<br><br>");
		  }
		}
	  </cfscript>
	  <cfloop index="idx" FROM="1" to="#Arraylen(styleName)#">
	    <cfset Arguments.emailBodyHTML=Replace(Arguments.emailBodyHTML, "class=""#styleName[idx]#""", "style=""#styleDefinition[idx]#""", "ALL")>
	  </cfloop>
	  
	  <cfreturn Arguments.emailBodyHTML>
	<cfcatch>
	  <cfreturn Arguments.emailBodyHTML>
	</cfcatch>
	</cftry>
  </cffunction>
  
  <cffunction name="convertHTMLSpecialCharacters" access="public" output="false" returntype="string">
    <cfargument name="emailBodyHTML" type="string" required="yes">
	<cfset var idx = 0>
	<cftry>
	  <cffile action="read" file="#this.HTMLSpecialCharactersConversionPath#" variable="configFile">
	  <cfset lines=ArrayNew(1)>
	  <cfset items=ArrayNew(1)>
	  <cfset newline=Chr(13) & Chr(10)>
	  <cfset lines=ListToArray(configFile, newline)>
	  <cfloop index="idx" FROM="1" to="#ArrayLen(lines)#">
	    <cfset items=ListToArray(lines[idx])>
		<cfif ArrayLen(items) IS 2>
		  <cfset Arguments.emailBodyHTML=Replace(Arguments.emailBodyHTML, "#items[1]#", "#items[2]#", "ALL")>	  
		</cfif>
	  </cfloop>
	  
	  <cfset Arguments.emailBodyHTML=Replace(Arguments.emailBodyHTML, chr(8217), chr(39), "ALL")>
	  <cfset Arguments.emailBodyHTML=Replace(Arguments.emailBodyHTML, chr(8216), chr(39), "ALL")>
	  <cfset Arguments.emailBodyHTML=Replace(Arguments.emailBodyHTML, chr(8220), chr(34), "ALL")>
	  <cfset Arguments.emailBodyHTML=Replace(Arguments.emailBodyHTML, chr(8221), chr(34), "ALL")>
	  <cfset Arguments.emailBodyHTML=Replace(Arguments.emailBodyHTML, chr(8211), "-", "ALL")>	  
	  	
	  <cfreturn Arguments.emailBodyHTML>
	<cfcatch>
	  <cfreturn Arguments.emailBodyHTML>
	</cfcatch>
	</cftry>
  </cffunction>
  
  <cffunction name="replaceBodyLink" access="public" output="false" returntype="string">
   	<cfargument name="strInputBody" type="string" required="yes">				
	<cfargument name="messageID" type="numeric" required="yes">
	
	<cfset var idx = 0>
	
	<cfscript>
	  Arguments.strInputBody=REReplaceNoCase(Arguments.strInputBody, "href=""([a-zA-Z0-9_:=/\?&%\.\-\+ ]+)""", "href=""#this.siteURLRoot#/action.cfm?md=communication&task=addMessageClickThru&msgid=#Arguments.messageID#&uid=##emailID##&redirect=\1""", "ALL");
	  replaceable=ArrayNew(1);
	  count = 0;
	  startpos = 1;
	  while (startpos IS NOT 0 AND count LTE 100) {
	    count = count + 1;
		st=REFind("redirect=[a-zA-Z0-9_:=/\?&%\.\-\+ ]+""", Arguments.strInputBody, startpos, "TRUE");
		if (st.pos[1] IS NOT 0) {		  
		  idx = idx + 1;
  		  startpos = st.pos[1] + 9;
		  length = st.len[1] - 10;
		  replaceable[idx] = MID(Arguments.strInputBody, startpos, length);
		  startpos = startpos + length-1;
		}
	  }
	  for (idx=1; idx LTE ArrayLen(replaceable); idx = idx + 1) {
	    Arguments.strInputBody=Replace(Arguments.strInputBody, "redirect=#replaceable[idx]#""", "encoded=1&redirect=#URLEncodedFormat(replaceable[idx])#""", "ALL");
	  }
	</cfscript>
	<cfreturn Arguments.strInputBody>
  </cffunction>
  
  <cffunction name="simpleEncrypt" access="public" output="false" returntype="string">
    <cfargument name="inputString" type="string" required="yes">
    <cfset chars = ArrayNew(1)>
	<cfset nums = ArrayNew(1)>
	<cfif Len(Arguments.inputString) IS 0><cfreturn ""></cfif>
	<cfloop index="i" FROM="1" to="#Len(Arguments.inputString)#">
	  <cfset chars[i] = MID(Arguments.inputString, i, 1)>
	  <cfset nums[2*i-1] = ((ASC(chars[i]) + i + 18) MOD 95) + 32>  
	  <cfset nums[2*i] = ((INT(Rand() * 95) + 1) MOD 95) + 32>
	</cfloop>
    <cfset outputString = "">
	<cfloop index="i" FROM="1" to="#ArrayLen(nums)#">
	  <cfset outputString = outputString & Chr(nums[i])>
	</cfloop>
	<cfreturn outputString>
  </cffunction>

  <cffunction name="simpleDecrypt" access="public" output="false" returntype="string">
    <cfargument name="inputString" type="string" required="yes">
    <cfset chars = ArrayNew(1)>
	<cfset nums = ArrayNew(1)>

	<cfif Len(Arguments.inputString) IS 0><cfreturn ""></cfif>
	<cfloop index="i" FROM="1" to="#Len(Arguments.inputString)#" step="2">
	  <cfset j = (i+1)/2>
	  <cfset chars[j] = MID(Arguments.inputString, i, 1)>
	  <cfset nums[j] = ((ASC(chars[j]) - j + 108) MOD 95) + 32>  
	</cfloop>
    <cfset outputString = "">
	<cfloop index="i" FROM="1" to="#ArrayLen(nums)#">
	  <cfset outputString = outputString & Chr(nums[i])>
	</cfloop>
	<cfreturn outputString>  
  </cffunction>
  
  <!--- ******************************************************************************************************************** --->
  <!--- ******************************************************************************************************************** --->
  <!--- ******************************************************************************************************************** --->
  <!--- ******************************************************************************************************************** --->
  <!--- ******************************************************************************************************************** --->
  <!--- ******************************************************************************************************************** --->
  
  <cffunction name="getListNameByListID" access="public" output="no" returntype="string">
    <cfargument name="listID" type="numeric" required="yes">
	<cfquery name="qListName" datasource="#this.datasource#">
	  SELECT listName
	  FROM cm_list
	  WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">
	</cfquery>
	<cfif qListName.recordcount>
	  <cfreturn qListName.listName>
	<cfelse>
	  <cfreturn "">
	</cfif>
  </cffunction>  
  
  <cffunction name="deleteMembersSubmissionID" access="public" output="false">
    <cfargument name="submissionID" type="numeric" required="yes">
	
	<cfquery datasource="#this.datasource#">
	  UPDATE cm_email_list
	  SET submissionID=0
	  WHERE submissionID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.submissionID#">
	</cfquery>	
  </cffunction>
  
  <cffunction name="updateMemberSubmissionID" access="public" output="false">
    <cfargument name="emailID" type="numeric" required="yes">
	<cfargument name="listID" type="numeric" required="yes">
	<cfargument name="submissionID" type="numeric" required="yes">
	
	<cfquery datasource="#this.datasource#">
	  UPDATE cm_email_list
	  SET submissionID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.submissionID#">
	  WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#"> AND
	  		listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">
	</cfquery>	  
  </cffunction>
  
  <cffunction name="deleteMemberFromList" access="public" output="false">
    <cfargument name="listID" type="numeric" required="yes">
	<cfargument name="emailID" type="string" required="yes">    
	
    <cfquery datasource="#this.datasource#">
	  DELETE FROM cm_email_list
	  WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#"> AND
	  		emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
	</cfquery>  
  </cffunction>

  
  <cffunction name="deleteCampaign" access="public" output="false">
    <cfargument name="messageID" type="numeric" required="yes">
    <cfset deleteMessage(Arguments.messageID)>
  </cffunction>
    
  <cffunction name="getEmailIDByEmail" access="public" output="false" returntype="numeric">
    <cfargument name="emailAddress" type="string" required="yes">
  
    <cfquery name="qEmailID" datasource="#this.datasource#">
	  SELECT emailID
	  FROM cm_email
 	  WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailAddress#">
    </cfquery>
  
    <cfif qEmailID.recordcount IS NOT "0">
	  <cfreturn qEmailID.emailID>
    <cfelse>
	  <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="getSendHistoryEmailID" access="public" output="false" returntype="query">
    <cfargument name="emailID" type="numeric" required="yes">
  
    <cfquery name="qSendHistory" datasource="#this.datasource#">
	  SELECT cm_sendhistory.sendHistoryID, cm_sendhistory.messageID, cm_message_list.listID
	  FROM (cm_sendhistory INNER JOIN cm_message_list ON
		   cm_sendhistory.messageID=cm_message_list.messageID) JOIN cm_email_list ON
		   cm_message_list.listID=cm_email_list.listID
	  WHERE cm_email_list.emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
	  ORDER BY cm_sendhistory.dateSent DESC
    </cfquery>
    <cfreturn qSendHistory>
  </cffunction>

  <cffunction name="getListIDByEmailIDAndMessageID" access="public" output="false" returntype="numeric">
    <cfargument name="emailID" type="numeric" required="yes">
	<cfargument name="messageID" type="numeric" required="yes">
    <cfquery name="qListID" datasource="#this.datasource#">
	  SELECT cm_email_list.listID
	  FROM cm_message_list INNER JOIN cm_email_list ON
	  	   cm_message_list.listID=cm_email_list.listID
	  WHERE cm_message_list.messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#"> AND
	  		cm_email_list.emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
	</cfquery>
    <cfif qListID.recordcount IS 0>
	  <cfreturn 0>
	<cfelse>
	  <cfreturn qListID.listID>
	</cfif>  
  </cffunction>
  
  <cffunction name="getListByEmailID" access="public" output="false" returntype="query">
    <cfargument name="emailID" type="numeric" required="yes">
	
	<cfquery name="qLists" datasource="#this.datasource#">
	  SELECT cm_email_list.listID, cm_list.listName
	  FROM cm_email_list JOIN cm_list ON
	  	   cm_email_list.listID=cm_list.listID
	  WHERE cm_email_list.emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
	</cfquery>
    <cfreturn qLists>
  </cffunction>

  <cffunction name="getCampaignArchiveByEmailID" access="public" output="false" returntype="query">
    <cfargument name="emailID" type="numeric" required="yes">
	
	<cfquery name="qArchive" datasource="#this.datasource#">
	  SELECT cm_list.listID, cm_list.listName,
	         cm_message.messageID, cm_message.dateFirstSend,
			 cm_message.emailSubject
	  FROM ((cm_email_list INNER JOIN cm_list ON
	        cm_email_list.listID=cm_list.listID) INNER JOIN cm_message_list ON
	  	   cm_list.listID=cm_message_list.listID) INNER JOIN cm_message ON
		   cm_message_list.messageID=cm_message.messageID
	  WHERE cm_email_list.emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#"> AND
			cm_message.published=1 AND
			cm_message.status='S'
	  ORDER BY cm_list.listName ASC, cm_message.dateFirstSend DESC
	</cfquery>
    <cfreturn qArchive>
  </cffunction>

  <cffunction name="getHTMLEmailContent" access="public" output="false" returntype="string">
    <cfargument name="messageID" type="numeric" required="yes">
	<cfargument name="emailID" type="numeric" required="yes">
	<cfset var HTMLEmailContent="">
	
	<cfset campaignInfo=getMessage(Arguments.messageID)>
	<cfif campaignInfo.recordcount IS 0><cfreturn ""></cfif>
    <cfset HTMLEmailContent=ReplaceNoCase(campaignInfo.completeHTMLEmail, "##emailID##", "#URLEncodedFormat(simpleEncrypt(Arguments.emailID))#", "ALL")>
    <cfreturn HTMLEmailContent>    
  </cffunction>
</cfcomponent>