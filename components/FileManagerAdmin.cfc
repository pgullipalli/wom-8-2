<cfcomponent displayname="FM_FileManagerAdmin" extends="Base" output="false">
  <cffunction name="upload" access="public" returntype="struct" output="false" displayname="uploadFileForFileSelector">
	<cfargument name="targetAbsoluteDirectory" type="string" required="true">
	<cfargument name="localFileFieldName" type="string" required="true">
	<cfargument name="localFilePath" type="string" required="true">
	<cfargument name="acceptableFiles" type="string" required="true">
	<cfargument name="overwrite" type="boolean" default="false">
	
	<cfset var returnedStruct=StructNew()>
	<cfset returnedStruct.success=false>
	<cfset returnedStruct.filename="">
	<cfset returnedStruct.message="">
	
	<cfset localFileName=getFileFromPath(Arguments.localFilePath)>
	<cfif Not checkAcceptableFileType(localFileName, Arguments.acceptableFiles)>
	  <cfset returnedStruct.success=false>
	  <cfset returnedStruct.message="<span class=""hilite"">The file ""#localFileName#"" was not uploaded because the file type can not be accepted.</span>">
	<cfelse>
	  <cftry>
		<cfif Arguments.overwrite>
	      <cffile action="upload"
	            destination="#Arguments.targetAbsoluteDirectory#"
	            nameconflict="overwrite"
	            mode="775"
	            filefield="#Arguments.localFileFieldName#">
	    <cfelse>
	      <cffile action="upload"
	            destination="#Arguments.targetAbsoluteDirectory#"
	            nameconflict="error"
	            mode="775"
	            filefield="#Arguments.localFileFieldName#">
	    </cfif>	  	  
	    <cfset returnedStruct.filename=cffile.serverFile>
	    <cfset returnedStruct.success=true>
	    <cfset returnedStruct.message="<span>The file ""#localFileName#"" has been uploaded successfully!</span>">
	    <cfif Not checkAcceptableFileType(returnedStruct.filename, Arguments.acceptableFiles)>
	      <cffile action="delete" file="#Arguments.targetAbsoluteDirectory#/#VARIABLES.filename#">
	      <cfset returnedStruct.success=false>
	      <cfset returnedStruct.message="<span class=""hilite"">The file ""#localFileName#"" was not uploaded because the file type can not accepted.</span>">
	    </cfif>
	  <cfcatch><!--- ERROR OCCURS --->
		<cfset returnedStruct.success=false>
	    <cfset returnedStruct.message="<span class=""hilite"">It seems that the file ""#localFileName#"" you tried to upload already existed in your remote folder. You can either rename it or check off the option for replacing the existing file.</span>">
	  </cfcatch>
	  </cftry>
	</cfif>
	
	<cfreturn returnedStruct>	
  </cffunction>
	
  <cffunction name="createDirectory" access="public" returntype="struct" output="false" displayname="createDirectory">
	<cfargument name="absoluteDirectory" required="true" type="string">
	
	<cfset var returnedStruct=StructNew()>
	<cfset returnedStruct.success=false>
	<cfset returnedStruct.message="">
	
	<cfif directoryExists(Arguments.absoluteDirectory)>
	  <cfset returnedStruct.success=false>
	  <cfset returnedStruct.message="[Error] No new directory was created. The directory you tried to create already existed.">
	<cfelse>
	  <cftry>
	    <cfdirectory action="create" directory="#Arguments.absoluteDirectory#" mode="775">
	    <cfset returnedStruct.success=true>
	  <cfcatch>
		<cfset returnedStruct.success=false>
	    <cfset returnedStruct.message="[Error] unexpected server error.">
	  </cfcatch>
	  </cftry>
	</cfif>
	
	<cfreturn returnedStruct>	    
  </cffunction>

  <cffunction name="rename" access="public" returntype="struct" output="false" displayname="rename">
	<cfargument name="type" required="true" type="string">
    <cfargument name="oldAbsolutePath" required="true" type="string">
	<cfargument name="newAbsolutePath" required="true" type="string">
	<cfargument name="acceptableFiles" required="true" type="string">
	
	<cfset var returnedStruct=StructNew()>
	<cfset returnedStruct.success=true>
	<cfset returnedStruct.message="">
	
	<cfif Not Compare(Arguments.oldAbsolutePath, Arguments.newAbsolutePath)><cfreturn returnedStruct></cfif>
	
	<cfset fileName=getFileFromPath(Arguments.newAbsolutePath)>
	
	<cfif Arguments.type Is "dir">
	  <cfif directoryExists(Arguments.newAbsolutePath)>
		<cfset returnedStruct.success=false>
	    <cfset returnedStruct.message="[Error] The folder name <b><i>" & fileName & "</i></b> already existed. Please try other name.">
	    <cfreturn returnedStruct>		  
	  <cfelse>
	    <cftry>
	      <cfdirectory action="rename" directory="#Arguments.oldAbsolutePath#" newdirectory="#getFileFromPath(Arguments.newAbsolutePath)#">
	    <cfcatch>
		  <cfset returnedStruct.success=false>
	      <cfset returnedStruct.message="[Error] unexpected server error.">
	      <cfreturn returnedStruct>
	  </cfcatch>
	    </cftry>
	  </cfif>
	<cfelse>
	 <cfif fileExists(Arguments.newAbsolutePath)>
		<cfset returnedStruct.success=false>
	    <cfset returnedStruct.message="[Error] The file name <b><i>" & fileName & "</i></b> already existed. Please try other name.">
	    <cfreturn returnedStruct>		  
	  <cfelse>		
		<cfif Not checkAcceptableFileType(fileName, Arguments.acceptableFiles)>
		  <cfset returnedStruct.success=false>
	      <cfset returnedStruct.message="[Error] Sorry, the file type you specified can't be accepted.">
	      <cfreturn returnedStruct>
	    </cfif>
	    <cftry>
		  <cffile action="rename" source="#Arguments.oldAbsolutePath#" destination="#Arguments.newAbsolutePath#">
	    <cfcatch>
		  <cfset returnedStruct.success=false>
	      <cfset returnedStruct.message="[Error] unexpected server error.">
	      <cfreturn returnedStruct>
	    </cfcatch>
	    </cftry>
	  </cfif>	
	</cfif>
	<cfreturn returnedStruct>
  </cffunction>

  <cffunction name="moveFiles" access="public" returntype="struct" output="false" displayname="moveFiles">
    <cfargument name="sourceAbsoluteDirectory" required="true" type="string">
	<cfargument name="targetAbsoluteDirectory" required="true" type="string">
	<cfargument name="file" required="true" type="string">
	
	<cfset var returnedStruct=StructNew()>
	<cfset returnedStruct.success=true>
	<cfset returnedStruct.message="">
	<cfset returnedStruct.filesMoved=false>
	
	<cfloop index="fileName" list="#Arguments.file#">
      <cftry>
		<cffile action="move" source="#Arguments.sourceAbsoluteDirectory#/#fileName#" destination="#Arguments.targetAbsoluteDirectory#">
		<cfset returnedStruct.filesMoved=true>
	  <cfcatch>
		<cfset returnedStruct.success=false>
		<cfset returnedStruct.message= returnedStruct.message & "<br />[Error] The file #fileName# can not be moved. A file with the same name may appear in the folder that you tried to move to.">
	  </cfcatch>
	  </cftry>	
	</cfloop>
	<cfreturn returnedStruct>
  </cffunction>

  <cffunction name="deleteFiles" access="public" returntype="struct" output="false" displayname="deleteFiles">
	<!--- it actually deletes directories and files --->
	<cfargument name="absoluteDirectories" required="true" type="array">
	<cfargument name="absoluteFilePaths" required="true" type="array">
	
	<cfset var returnedStruct=StructNew()>
	<cfset returnedStruct.success=true>
	<cfset returnedStruct.message="">
	<cfset returnedStruct.filesDeleted=false>
	
	<cfset numDirs=ArrayLen(Arguments.absoluteDirectories)>
	<cfloop index="idx" from="1" to="#numDirs#">
	  <cfdirectory action="list" directory="#Arguments.absoluteDirectories[idx]#" name="allFiles">
	  <cfif allFiles.recordcount GT 0><!--- still has directories or files in the directory --->
	    <cfset returnedStruct.success=false>
	    <cfset returnedStruct.message=returnedStruct.message & "There are files or sub-folders in the folder '" & getFileFromPath(Arguments.absoluteDirectories[idx]) & "' that you tried to delete. Please delete them before delete this folder.">
	    <cfreturn returnedStruct>
	  </cfif>
	</cfloop>
	
	<cfloop index="idx" from="1" to="#numDirs#">
	  <cftry>
	    <cfdirectory action="delete" directory="#Arguments.absoluteDirectories[idx]#">
	    <cfset returnedStruct.filesDeleted=true>
	  <cfcatch>
		<cfset returnedStruct.success=false>
	    <cfset returnedStruct.message="[Error] unexpected server error.">
	    <cfreturn returnedStruct>
	  </cfcatch>
	  </cftry>
	</cfloop>
	
	<cfset numFiles=ArrayLen(Arguments.absoluteFilePaths)>
	<cfloop index="idx" from="1" to="#numFiles#">
	  <cftry>
	    <cffile action="delete" file="#Arguments.absoluteFilePaths[idx]#">
	    <cfset returnedStruct.filesDeleted=true>
	  <cfcatch>
		<cfset returnedStruct.success=false>
	    <cfset returnedStruct.message="[Error] unexpected server error.">
	    <cfreturn returnedStruct>
	  </cfcatch>
	  </cftry>
	</cfloop>	

    <cfreturn returnedStruct>
  </cffunction>

  <cffunction name="checkAcceptableFileType" access="public" output="false" returntype="boolean" displayname="checkAcceptableFileType">
	<cfargument name="fileName" type="string" required="true">
	<cfargument name="acceptableFiles" type="string" required="true">
	<cfset Arguments.fileName=LCase(Arguments.fileName)>
	<cfset strLen=Len(Arguments.fileName)>
	<cfif strLen LT 4>
	  <cfreturn false>
	</cfif>
	<cfset dotIndex=Find(".", Arguments.fileName, strLen-3)>
	<cfif dotIndex Is 0 AND strLen GT 4>
	  <cfset dotIndex=Find(".", Arguments.fileName, strLen-4)>
	</cfif>
	<cfif dotIndex Is 0 AND strLen GT 5>
	  <cfset dotIndex=Find(".", Arguments.fileName, strLen-5)>
	</cfif>
	<cfif dotIndex Is 0><cfreturn false></cfif>
	
	<cfset fileExt=Mid(Arguments.fileName, dotIndex, strLen - dotIndex + 1)>
	
	<cfif ListFind(Arguments.acceptableFiles, fileExt) GT 0>
	  <cfreturn true>
	<cfelse>
	  <cfreturn false>
	</cfif>
  </cffunction>

  <cffunction name="resizeImage" access="public" output="false" returntype="struct" displayname="resizeImage">
	<cfargument name="imageAbsolutePath" type="string" required="true">
    <cfargument name="newImageAbsolutePath" type="string" required="true">
	<cfargument name="newWidth" type="numeric" required="true">
	<cfargument name="newHeight" type="numeric" required="true">
    <cfargument name="overwrite" type="boolean" default="0">
    <cfargument name="imageBufferDirectory" type="string" required="false">
    
	<cfset var returnedStruct=StructNew()>
	<cfset returnedStruct.success=true>
	<cfset returnedStruct.message="">
    
    <cfif Not Arguments.overwrite And FileExists(Arguments.newImageAbsolutePath)>
      <cfset returnedStruct.success=false>
	  <cfset returnedStruct.message="The file name you tried to save as already exists. Please choose other file name. No action was performed.">
      <cfreturn returnedStruct>
    </cfif>
    
    <cfif IsDefined("Arguments.imageBufferDirectory")>
	  <!--- clean up the buffer directory if this version of coldfusion is unpatched --->
      <cfdirectory action="list" directory="#Arguments.imageBufferDirectory#" name="allFiles" type="file">
      <cftry><cfloop query="allFiles"><cffile action="delete" file="#tempDir##name#"></cfloop><cfcatch></cfcatch></cftry>
      
      <cfset fileNameTemp=Minute(now()) & Second(now()) & "_" & getFileFromPath(Arguments.imageAbsolutePath)>
      <cfset imageAbsolutePathTemp=Arguments.imageBufferDirectory & fileNameTemp>
      
      <cfif Compare(Arguments.imageAbsolutePath, Arguments.newImageAbsolutePath)>
		<cffile action="copy" source="#Arguments.imageAbsolutePath#" destination="#imageAbsolutePathTemp#" nameconflict="overwrite"> 
      <cfelse>
        <cffile action="move" source="#Arguments.imageAbsolutePath#" destination="#imageAbsolutePathTemp#" nameconflict="overwrite">      
      </cfif>
      
      <cfimage action="resize"
			 source="#imageAbsolutePathTemp#"
			 width="#Arguments.newWidth#"
			 height="#Arguments.newHeight#"
			 destination="#Arguments.newImageAbsolutePath#"  
			 overwrite="yes">
    <cfelse>
      <cfimage action="resize"
			 source="#Arguments.imageAbsolutePath#"
			 width="#Arguments.newWidth#"
			 height="#Arguments.newHeight#"
			 destination="#Arguments.newImageAbsolutePath#"  
			 overwrite="yes">
    </cfif>
	<cfreturn returnedStruct>
  </cffunction>
</cfcomponent>