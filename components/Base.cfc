<cfcomponent hint="Initialize datasource and some other application variables" output="false">
  <cfif IsDefined("Application.datasource")>
    <cfset this.datasource=Application.datasource>
  </cfif>

  <cfif IsDefined("Application.siteURLRoot")>
    <cfset this.siteURLRoot=Application.siteURLRoot>
  </cfif>
  
  <cfif IsDefined("Application.siteURLRootSSL")>
    <cfset this.siteURLRootSSL=Application.siteURLRootSSL>
  </cfif>
  
  <cfif IsDefined("Application.siteDirectoryRoot")>
    <cfset this.siteDirectoryRoot=Application.siteDirectoryRoot>
  </cfif>
  
  <cfif IsDefined("Application.siteURLRootForCFHTTP")>
    <cfset this.siteURLRootForCFHTTP=Application.siteURLRootForCFHTTP>
  <cfelse>
    <cfset this.siteURLRootForCFHTTP=this.siteURLRoot>
  </cfif>

  <cfif IsDefined("Application.privilegeXMLObj")>
    <cfset this.privilegeXMLObj=Application.privilegeXMLObj>
  </cfif>
  
  <cfif IsDefined("Application.isColdfusion8")>
    <cfset this.isColdfusion8=APPLICATION.isColdfusion8>
  <cfelse>
    <cfset this.isColdfusion8=0>
  </cfif>
    
  <cffunction name="init" access="public" output="false">
    <cfargument name="datasource" type="string" required="no">
	<cfargument name="siteURLRoot" type="string" required="no">
	<cfargument name="siteDirectoryRoot" type="string" required="no">
	
	<cfif IsDefined("Arguments.datasource")>
      <cfset this.datasource=Arguments.datasource>
    </cfif>

    <cfif IsDefined("Arguments.siteURLRoot")>
      <cfset this.siteURLRoot=Arguments.siteURLRoot>
    </cfif>
  
    <cfif IsDefined("Arguments.siteDirectoryRoot")>
      <cfset this.siteDirectoryRoot=Arguments.siteDirectoryRoot>
    </cfif>

	<cfreturn this>
  </cffunction>
</cfcomponent>