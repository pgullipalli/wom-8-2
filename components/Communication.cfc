<cfcomponent displayname="Communication Manager" extends="Base">
  <cfif IsDefined("Application.communication")>
    <cfset this.emailDefaultFromAddress=Application.communication.emailDefaultFromAddress>
    <cfset this.emailDefaultReplyAddress=Application.communication.emailDefaultReplyAddress>
    <cfset this.emailDefaultFromName=Application.communication.emailDefaultFromName>  
  </cfif>

  <cffunction name="getSignUpFormInfo" access="public" output="no" returntype="query">
    <cfargument name="signUpFormID" type="numeric" required="yes">
    
    <cfquery name="qSignUpForm" datasource="#this.datasource#">
      SELECT *
      FROM cm_signupform
      WHERE signUpFormID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.signUpFormID#"> AND
      		published=1
    </cfquery>
  
    <cfreturn qSignUpForm>
  </cffunction>
  
  <cffunction name="getSignUpFormLists" access="public" output="no" returntype="query">
    <cfargument name="signUpFormID" type="numeric" required="yes">
  
    <cfquery name="qSignUpFormLists" datasource="#this.datasource#">
      SELECT cm_list.listID, cm_list.listName
      FROM cm_signupform_list JOIN cm_list ON cm_signupform_list.listID=cm_list.listID
      WHERE  cm_signupform_list.signUpFormID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.signUpFormID#"> AND
      		 cm_list.published=1
    </cfquery>
    <cfreturn qSignUpFormLists>
  </cffunction>
  
  <cffunction name="addMemberToLists" access="public" output="no">
    <cfargument name="submittedByAdmin" default="0">
    <cfargument name="signUpFormID" type="numeric" default="0">
    <cfargument name="emailID" type="numeric" default="0">
    <cfargument name="email" type="string" default="">
	<cfargument name="firstName" type="string" default="">
	<cfargument name="lastName" type="string" default="">
	<cfargument name="listIDList" type="string" default="">
	<cfargument name="status" type="string" default="P">
	<cfargument name="doubleOptinEmailRequired" type="boolean" default="0">
	<cfargument name="submissionID" type="numeric" default="0">
  
    <cfif Arguments.listIDList IS ""><cfreturn></cfif>
	<cfif Arguments.emailID IS 0 AND Arguments.email IS ""><cfreturn></cfif>
	
	<!--- BEGIN: determin emailID --->
	<cfif Arguments.emailID IS 0>
      <cfquery name="qEmailID" datasource="#this.datasource#">
	    SELECT emailID
	    FROM cm_email
	    WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">
	  </cfquery>
	  <cfif qEmailID.recordcount IS 0>
	    <!--- Never register --->
	    <cfquery datasource="#this.datasource#">
	      INSERT INTO cm_email
		    (email, firstName, lastName, dateCreated, dateLastModified)
		  VALUES
		    (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">,
		     <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.firstName#">,
		     <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.lastName#">,
		     <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		     <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	    </cfquery>
	    <cfquery name="qMaxID" datasource="#this.datasource#">
	      SELECT MAX(emailID) as ID
		  FROM cm_email
	    </cfquery>
	    <cfset Arguments.emailID=qMaxID.ID>
	  <cfelse>
	    <cfquery datasource="#this.datasource#">
		  UPDATE cm_email
		  SET firstName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.firstName#">,
		  	  lastName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.lastName#">,
			  dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
		  WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qEmailID.emailID[1]#">
		</cfquery>	  
	  
	    <cfset Arguments.emailID=qEmailID.emailID[1]>
	  </cfif>
	</cfif>
    <!--- BEGIN: determin emailID --->
  
    <cfset doubleOptinListIDList="">
    <cfloop index="listID" list="#Arguments.listIDList#" delimiters=",">
	  <!--- check if registered with this list --->
	  <cfquery name="qFindDuplicate" datasource="#this.datasource#">
	    SELECT status
		FROM cm_email_list
		WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#"> AND
			  listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#listID#">
	  </cfquery>
	  <cfif qFindDuplicate.recordcount IS 0>
	    <!--- Never register with this list --->
		<cfquery datasource="#this.datasource#">
		  INSERT INTO cm_email_list
		    (emailID, listID, status, submissionID, signupFormID, dateCreated, dateLastModified)
		  VALUES
		    (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">,
			 <cfqueryparam cfsqltype="cf_sql_integer" value="#listID#">,
			 <cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.status#">,
			 <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.submissionID#">,
             <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.signUpFormID#">,
			 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
			 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
		</cfquery>
		<cfif Arguments.doubleOptinEmailRequired AND Arguments.status IS "P">
          <cfif Compare(doubleOptinListIDList,"")>
            <cfset doubleOptinListIDList=doubleOptinListIDList & "," & listID>
          <cfelse>
            <cfset doubleOptinListIDList=listID>
          </cfif>
		</cfif>		
	  <cfelse>
	    <!--- has registered with this list --->
		<cfif Arguments.submittedByAdmin>
		  <cfif Compare(Arguments.status, qFindDuplicate.status)>
		    <cfset changeMemberStatus(Arguments.emailID, listID, Arguments.status)>
		  </cfif>
		<cfelse>
		  <cfswitch expression="#qFindDuplicate.status#">
		    <cfcase value="U,I,B">
		      <cfif Arguments.status IS "P" OR Arguments.status IS "S">
			    <cfset changeMemberStatus(Arguments.emailID, listID, Arguments.status)>
			  </cfif>
		    </cfcase>
		    <cfcase value="P">
			  <cfif Arguments.status IS "S">
			    <cfset changeMemberStatus(Arguments.emailID, listID, "S")>
			  </cfif>
		    </cfcase>		
		  </cfswitch>	  
		</cfif> 
		
		<cfif Arguments.doubleOptinEmailRequired AND Arguments.status IS "P" AND Compare(qFindDuplicate.status, "S")>
		  <cfif Compare(doubleOptinListIDList,"")>
            <cfset doubleOptinListIDList=doubleOptinListIDList & "," & listID>
          <cfelse>
            <cfset doubleOptinListIDList=listID>
          </cfif>
		</cfif>
	  </cfif>	
	</cfloop>  
    
    <cfif Compare(doubleOptinListIDList,"")>
      <cfset sendDoubleOptinEmail(emailID=Arguments.emailID, listIDList=Arguments.listIDList, signUpFormID=Arguments.signUpFormID)>
    </cfif>   
  </cffunction>
  
  <cffunction name="sendDoubleOptinEmail" access="public" output="no">
    <cfargument name="emailID" type="numeric" required="yes">
	<cfargument name="listIDList" type="string" required="yes">
    <cfargument name="signUpFormID" type="numeric" required="yes">
  
	<cfset emailInfo=getEmailByEmailID(Arguments.emailID)>
    
    <cfif emailInfo.recordcount IS 0 OR Not Compare(Arguments.listIDList,"")>
	  <cfreturn>
	</cfif>
    
    <cfset listNames=getListNamesByListIDList(Arguments.listIDList)>
    
    <cfif Arguments.signUpFormID Is 0>
      <cfset fromAddress="#this.emailDefaultFromName# <#this.emailDefaultFromAddress#>">
      <cfset replyAddress=this.emailDefaultReplyAddress>
    <cfelse>
      <cfset signUpFromInfo=getSignUpFormInfo(Arguments.signUpFormID)>
      <cfif Compare(signUpFromInfo.emailFromAddress, "")>
	    <cfset fromAddress="#signUpFromInfo.emailFromName# <#signUpFromInfo.emailFromAddress#>">
	  <cfelse>
	    <cfset fromAddress=signUpFromInfo.emailFromAddress>
	  </cfif>
      <cfset replyAddress=signUpFromInfo.emailReplyAddress>
    </cfif>
	
	<cfset encryptedEmailID=simpleEncrypt(Arguments.emailID)>
	<cfset strOptInLink = "#this.siteURLRoot#/r/?#URLEncodedFormat(Arguments.listIDList)#&#URLEncodedFormat(encryptedEmailID)#">
    
	<cfmail from="#fromAddress#" replyto="#replyAddress#" to="#emailInfo.email#" subject="RE: Your subscription" type="html">
	  <cfmailpart type="text">
<cfif Compare(emailInfo.firstName,"") OR Compare(emailInfo.lastName,"")>Dear #emailInfo.firstName# #emailInfo.lastName#,</cfif>

Thank you for your interest in subscribing to the following list(s):

<cfloop query="listNames">#listName#

</cfloop>
We are in receipt of your subscription request, and require that you please click the link below to complete your subscription to the email list(s). If you are unable to click on the link below, please cut and paste the entire link into your Internet browser to complete the subscription process.  Here's the subscription link:

#strOptInLink#

If you experience any problems with this process, or if you believe you have received this notification in error, please write us at #replyAddress#.  If you did NOT attempt to subscribe to the list(s) listed above, please do NOT visit the subscription link provided above and alert us that this subscription attempt is not valid by writing us at #replyAddress#.

Thank you.
      </cfmailpart>
	  <cfmailpart type="html" charset="utf-8">	 
      <div style="font-family:Arial;font-size:12px;color:##333333">
        <cfif Compare(emailInfo.firstName,"") OR Compare(emailInfo.lastName,"")>
        Dear #emailInfo.firstName# #emailInfo.lastName#,
        </cfif>
        
        <p>Thank you for your interest in subscribing to the following list(s):</p>
		
        <ul><cfloop query="listNames"><li><b>#listName#</b></li></cfloop></ul>
        
        <p>
        We are in receipt of your subscription request, and require that you please click the link below to complete your subscription to the email list(s).
        If you are unable to click on the link below, please cut and paste the entire link into your Internet browser to complete the subscription process.
        Here's the subscription link:
        <p>
        
        <p><a href="#strOptInLink#" style="font-family:Arial;font-size:11px;color:##000099">#strOptInLink#</a></p>
        
 		<p>        
        If you experience any problems with this process, or if you believe you have received this notification in error, please write us at #replyAddress#.
        If you did NOT attempt to subscribe to the list(s) listed above, please do NOT visit the subscription link provided above and alert us that this
        subscription attempt is not valid by writing us at #replyAddress#.
        </p>
        
        <p>        
        Thank you.
        </p>
      </div>
      </cfmailpart>
    </cfmail>	
  </cffunction>
  
  <cffunction name="getEmailByEmailID" access="public" output="no" returntype="query">
    <cfargument name="emailID" type="numeric" required="yes">
	<cfquery name="qEmail" datasource="#this.datasource#">
	  SELECT *
	  FROM cm_email
	  WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
	</cfquery>
	<cfreturn qEmail>
  </cffunction>
  
  <cffunction name="getListNamesByListIDList" access="public" output="no" returntype="query">
    <cfargument name="listIDList" type="string" required="yes">
    
    <cfquery name="qLists" datasource="#this.datasource#">
      SELECT listName
      FROM cm_list
      WHERE listID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.listIDList#" list="yes">)
    </cfquery>
    
    <cfreturn qLists>
  </cffunction>
  
  <cffunction name="changeMemberStatus" access="public" output="no">
    <cfargument name="emailID" type="numeric" required="yes">
	<cfargument name="listID" type="numeric" required="yes">
	<cfargument name="status" type="string" required="yes">
	<cfquery datasource="#this.datasource#">
	  UPDATE cm_email_list
	  SET <cfif NOT Compare(Arguments.status, "S")>numBouncedCampaigns=0,</cfif>
	  	  status=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.status#">,
	  	  dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#"> AND
			listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">
	</cfquery>  
  </cffunction>

  <cffunction name="optIn" access="public" output="false">
    <cfargument name="emailID" type="numeric" required="yes">
	<cfargument name="listIDList" type="string" required="yes">
	
    <cfloop index="listID" list="#Arguments.listIDList#">
        <cfquery name="qCheckExistence" datasource="#this.datasource#">
          SELECT *
          FROM cm_email_list
          WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#"> AND
                listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#listID#">
        </cfquery>
      
        <cfif qCheckExistence.recordcount IS 0>
          <cfquery name="qEmail" datasource="#this.datasource#">
            SELECT *
            FROM cm_email
            WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
          </cfquery>
          <cfif qEmail.recordcount GT 0>
            <cfset argStruct=StructNew()>
            <cfset argStruct.submittedByAdmin=1>
            <cfset argStruct.email=qEmail.email>
            <cfset argStruct.firstName=qEmail.firstName>
            <cfset argStruct.lastName=qEmail.lastName>
            <cfset argStruct.listIDList="#listID#">
            <cfset argStruct.status="S">
            <cfset argStruct.doubleOptinEmailRequired=0>
            <cfset argStruct.submissionID=0>
            <cfset addMemberToLists(argumentCollection=argStruct)>	
          </cfif>	
        <cfelse>
          <cfquery datasource="#this.datasource#">
            UPDATE cm_email_list
            SET status='S',
                dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
            WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#"> AND
                  listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#listID#">
          </cfquery>
        </cfif>  
      </cfloop>
  </cffunction>
  
  <cffunction name="addMailOpenCount" access="public" output="no">
    <cfargument name="messageID" type="numeric" required="yes">
	<cfargument name="emailID" type="numeric" required="yes">
	
	<cfquery name="qGetDuplicate" datasource="#this.datasource#">
	  SELECT messageOpenID
	  FROM cm_messageopen
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#"> AND
	  		emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
	</cfquery>
    <cfif qGetDuplicate.recordcount IS 0>
	  <cfquery datasource="#this.datasource#">
	    INSERT INTO cm_messageopen
		  (messageID, emailID, dateOpen)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">,
		   <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	  </cfquery>
	  <cfquery datasource="#this.datasource#">
	    UPDATE cm_message
		SET numEmailsOpen=numEmailsOpen+1
		WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	  </cfquery>
	</cfif>  
  </cffunction>

  <cffunction name="addMessageClickThru" access="public" output="no">
    <cfargument name="messageID" type="numeric" required="yes">
	<cfargument name="emailID" type="numeric" required="yes">
	<cfargument name="linkURL" type="string" required="yes">
	
	<cfquery datasource="#this.datasource#">
	  UPDATE cm_message
	  SET numClickThru=numClickThru+1
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>
	
	<cfquery name="qGetDuplicate" datasource="#this.datasource#">
	  SELECT messageClickThruID
	  FROM cm_messageclickthru
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#"> AND
	  		emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#"> AND
	  		linkURL=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.linkURL#">
	</cfquery>
	
	<cfif qGetDuplicate.recordcount IS 0>
	  <cftry>
	    <cfquery datasource="#this.datasource#">
	      INSERT INTO cm_messageclickthru
		    (messageID, emailID, numClicks, linkURL, dateLastClicked)
		  VALUES
		    (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">,
		     <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">,
		     1,
		     <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.linkURL#">,
		     <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	    </cfquery>
	  <cfcatch></cfcatch>
	  </cftry>	
	<cfelse>
	  <cfquery datasource="#this.datasource#">
	    UPDATE cm_messageclickthru
	    SET numClicks=numClicks+1,
			dateLastClicked=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	    WHERE messageClickThruID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qGetDuplicate.messageClickThruID#">			  
	  </cfquery>
	</cfif>  
  </cffunction> 

  <cffunction name="updateMemberToLists" access="public" output="no">
    <cfargument name="submittedByAdmin" default="0">
    <cfargument name="emailID" type="numeric" default="0">
    <cfargument name="email" type="string" default="">
	<cfargument name="firstName" type="string" default="">
	<cfargument name="lastName" type="string" default="">
	<cfargument name="listIDList" type="string" default="">
	<cfargument name="status" type="string" default="P">
	<cfargument name="doubleOptinEmailRequired" type="boolean" default="0">
	<cfargument name="submissionID" type="numeric" default="0">
	
	<cfset basicInfo=getEmailByEmailID(Arguments.emailID)>
	<cfif basicInfo.recordcount IS 0><cfreturn></cfif>
	
	<cfif Compare(Arguments.email, basicInfo.email)>
	  <!--- email address has been changed --->
	  <cfquery name="qExistingEmail" datasource="#this.datasource#">
	    SELECT emailID
		FROM cm_email
		WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#"> AND
			  emailID<><cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
	  </cfquery>
	  <cfif qExistingEmail.recordcount IS 0>
	    <!--- New E-Mail Address --->
		<cfquery datasource="#this.datasource#">
		  UPDATE cm_email
		  SET email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">
		  WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
		</cfquery>
	  <cfelse>
	    <!--- Existing Email --->
	    <cfset newEmailID=qExistingEmail.emailID>
		<cfquery name="qLists" datasource="#this.datasource#">
		  SELECT listID
		  FROM cm_email_list
		  WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#newEmailID#">
		</cfquery>
		<cfif qLists.recordcount IS 0>
		  <cfset existingListIDList="0">
		<cfelse>
		  <cfset existingListIDList=ValueList(qLists.listID)>
		</cfif>
		
		<cfquery datasource="#this.datasource#">
		  DELETE FROM cm_email_list
		  WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#"> AND
				listID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#existingListIDList#" list="yes">)
		</cfquery>
		
		<cfquery datasource="#this.datasource#">
		  UPDATE cm_email_list
		  SET emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#newEmailID#">
		  WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#"> AND
				listID NOT IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#existingListIDList#" list="yes">)
		</cfquery>
	  
	    <cfset Arguments.emailID=newEmailID>
	  </cfif>
	</cfif>
	
	<cfquery datasource="#this.datasource#">
	  UPDATE cm_email
	  SET firstName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.firstName#">,
	  	  lastName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.lastName#">
	  WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#">
	</cfquery>	
	
	<cfif NOT Arguments.submittedByAdmin>
	  <cfset listIDList_subscribed=getListIDListByEmailID(Arguments.emailID)>
	  <cfif Compare(listIDList_subscribed, "")>
	    <cfloop index="listID" list="#listIDList_subscribed#" delimiters=",">
	      <cfif ListFind(Arguments.listIDList, listID) IS 0>
		    <cfset changeMemberStatus(Arguments.emailID, listID, "U")>
		  </cfif>
	    </cfloop>
	  </cfif>
	</cfif>	
	
	<cfset argStruct=StructNew()>
    <cfset argStruct.submittedByAdmin=Arguments.submittedByAdmin>
	<cfset argStruct.email=Arguments.email>
	<cfset argStruct.firstName=Arguments.firstName>
	<cfset argStruct.lastName=Arguments.lastName>
	<cfset argStruct.listIDList=Arguments.listIDList>
	<cfset argStruct.status=Arguments.status>
    <cfset argStruct.doubleOptinEmailRequired=Arguments.doubleOptinEmailRequired>
	<cfset argStruct.submissionID=Arguments.submissionID>
	
	<cfset addMemberToLists(argumentCollection=argStruct)>
  </cffunction>

  <cffunction name="getListIDListByEmailID" access="public" output="no" returntype="string">
    <cfargument name="emailID" type="numeric" required="yes">
	<cfquery name="getListIDs" datasource="#this.datasource#">
	  SELECT listID
	  FROM cm_email_list
	  WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#"> AND
	  		status='S'
	</cfquery>
	<cfif getListIDs.recordcount>
	  <cfreturn ValueList(getListIDs.listID)>	
	<cfelse>
	  <cfreturn "">
	</cfif>
  </cffunction>

  <cffunction name="getHTMLEmailContent" access="public" output="no" returntype="string">
    <cfargument name="messageID" type="numeric" required="yes">
	<cfargument name="emailID" type="numeric" required="yes">
	<cfset var HTMLEmailContent="">
	
	<cfset campaignInfo=getMessage(Arguments.messageID)>
	<cfif campaignInfo.recordcount IS 0><cfreturn ""></cfif>
    <cfset HTMLEmailContent=ReplaceNoCase(campaignInfo.completeHTMLEmail, "##emailID##", "#URLEncodedFormat(simpleEncrypt(Arguments.emailID))#", "ALL")>
    <cfreturn HTMLEmailContent>    
  </cffunction>

  <cffunction name="getMessage" access="public" output="no" returntype="query">
    <cfargument name="messageID" type="numeric" required="yes">
    
	<cfquery name="qMessage" datasource="#this.datasource#">
	  SELECT *
	  FROM cm_message
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>  
  
    <cfreturn qMessage>
  </cffunction>
  
  <cffunction name="getCampaignArchiveByEmailID" access="public" output="false" returntype="query">
    <cfargument name="emailID" type="numeric" required="yes">
	
	<cfquery name="qArchive" datasource="#this.datasource#">
	  SELECT cm_list.listID, cm_list.listName,
	         cm_message.messageID, cm_message.dateFirstSend,
			 cm_message.emailSubject
	  FROM ((cm_email_list INNER JOIN cm_list ON
	        cm_email_list.listID=cm_list.listID) INNER JOIN cm_message_list ON
	  	   cm_list.listID=cm_message_list.listID) INNER JOIN cm_message ON
		   cm_message_list.messageID=cm_message.messageID
	  WHERE cm_email_list.emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.emailID#"> AND
			cm_message.published=1 AND
			cm_message.status='S'
	  ORDER BY cm_list.listName ASC, cm_message.dateFirstSend DESC
	</cfquery>
    <cfreturn qArchive>
  </cffunction>
  
  <cffunction name="getAllListNames" access="public" output="no" returntype="query">
    <cfargument name="published" type="boolean" required="no">
    <cfargument name="orderBy" type="string" default="listName">
	
    <cfquery name="qAllListNames" datasource="#this.datasource#">
	  SELECT listID, listName
	  FROM cm_list
      <cfif IsDefined("Arguments.published")>
	  WHERE published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">
	  </cfif>
	  ORDER BY #Arguments.orderBy#
	</cfquery>
	<cfreturn qAllListNames>
  </cffunction>
  
  <cffunction name="getAllLists" access="public" output="no" returntype="query">
    <cfargument name="published" type="boolean" required="no">
    <cfargument name="orderBy" type="string" default="listName">
	
    <cfquery name="qAllLists" datasource="#this.datasource#">
	  SELECT listID, listName
	  FROM cm_list
      <cfif IsDefined("Arguments.published")>
	  WHERE published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">
	  </cfif>
	  ORDER BY #Arguments.orderBy#
	</cfquery>
	<cfreturn qAllLists>
  </cffunction>
  
  <cffunction name="getEmailListIDByMessageID" access="public" output="no" returntype="numeric">
    <cfargument name="messageID" type="numeric" required="yes">
	
	<cfquery name="qListID" datasource="#this.datasource#">
	  SELECT listID
	  FROM cm_message_list
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>
	<cfif qListID.recordcount IS 0>
	  <cfreturn 0>
	<cfelse>
	  <cfreturn qListID.listID[1]>
	</cfif>  
  </cffunction>
  
  <cffunction name="getSignUpFormIDByMessageID" access="public" output="no" returntype="numeric">
    <cfargument name="messageID" type="numeric" required="yes">
    
    <cfquery name="qSignupFormID" datasource="#this.datasource#">
      SELECT signUpFormID
      FROM cm_message
      WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
    </cfquery>
    <cfif qSignupFormID.recordcount GT 0 AND qSignupFormID.signUpFormID GT 0>
      <cfreturn qSignupFormID.signUpFormID>
    </cfif>
	
	<cfquery name="qListID" datasource="#this.datasource#">
	  SELECT listID
	  FROM cm_message_list
	  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.messageID#">
	</cfquery>
	<cfif qListID.recordcount IS 0>
	  <cfreturn 0>
	<cfelse>
	  <cfset listIDList=ValueList(qListID.listID)>
      <cfquery name="qListID" datasource="#this.datasource#">
	    SELECT signupFormID
	    FROM cm_signupform_list
	    WHERE listID IN (<cfqueryparam cfsqltype="cf_sql_integer" value="#listIDList#" list="yes">)
        ORDER BY displaySeq
	  </cfquery>
      <cfif qListID.recordcount GT 0>
        <cfreturn qListID.signupFormID>
      <cfelse>
        <cfreturn 0>
      </cfif>
	</cfif>  
  </cffunction>


  <cffunction name="simpleEncrypt" access="public" output="false" returntype="string">
    <cfargument name="inputString" type="string" required="yes">
    <cfset chars = ArrayNew(1)>
	<cfset nums = ArrayNew(1)>
	<cfif Len(Arguments.inputString) IS 0><cfreturn ""></cfif>
	<cfloop index="i" FROM="1" to="#Len(Arguments.inputString)#">
	  <cfset chars[i] = MID(Arguments.inputString, i, 1)>
	  <cfset nums[2*i-1] = ((ASC(chars[i]) + i + 18) MOD 95) + 32>  
	  <cfset nums[2*i] = ((INT(Rand() * 95) + 1) MOD 95) + 32>
	</cfloop>
    <cfset outputString = "">
	<cfloop index="i" FROM="1" to="#ArrayLen(nums)#">
	  <cfset outputString = outputString & Chr(nums[i])>
	</cfloop>
	<cfreturn outputString>
  </cffunction>

  <cffunction name="simpleDecrypt" access="public" output="false" returntype="string">
    <cfargument name="inputString" type="string" required="yes">
    <cfset chars = ArrayNew(1)>
	<cfset nums = ArrayNew(1)>

	<cfif Len(Arguments.inputString) IS 0><cfreturn ""></cfif>
	<cfloop index="i" FROM="1" to="#Len(Arguments.inputString)#" step="2">
	  <cfset j = (i+1)/2>
	  <cfset chars[j] = MID(Arguments.inputString, i, 1)>
	  <cfset nums[j] = ((ASC(chars[j]) - j + 108) MOD 95) + 32>  
	</cfloop>
    <cfset outputString = "">
	<cfloop index="i" FROM="1" to="#ArrayLen(nums)#">
	  <cfset outputString = outputString & Chr(nums[i])>
	</cfloop>
	<cfreturn outputString>  
  </cffunction>

  <!--- ******************************************************************************* --->
  <!--- ******************************************************************************* --->
  <!--- ******************************************************************************* --->
  <!--- ******************************************************************************* --->
  
  
  
  
  <cffunction name="getListNameByListID" access="public" output="false" returntype="string">
    <cfargument name="listID" type="numeric" required="yes">
	<cfquery name="qListName" datasource="#this.datasource#">
	  SELECT listName
	  FROM cm_list
	  WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">
	</cfquery>
	<cfif qListName.recordcount>
	  <cfreturn qListName.listName>
	<cfelse>
	  <cfreturn "">
	</cfif>
  </cffunction>
  
  <cffunction name="getListByListID" access="public" output="false" returntype="query">
    <cfargument name="listID" type="numeric" required="yes">
	
	<cfquery name="qList" datasource="#this.datasource#">
	  SELECT *
	  FROM cm_list
	  WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">
	</cfquery>
	<cfreturn qList>
  </cffunction>
  
  
  
  
  <cffunction name="getListsByListIDsList" access="public" output="false" returntype="query">
    <cfargument name="listIDList" type="string" default="0">
	<cfquery name="qLists" datasource="#this.datasource#">
	  SELECT *
	  FROM cm_list
	  WHERE listID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.listIDList#" list="yes">)
	</cfquery>
    <cfreturn qLists>
  </cffunction>
  
  
  
  <cffunction name="getListsByFormID" access="public" output="false" returntype="query">
    <cfargument name="formID" type="numeric" required="yes">
	<cfargument name="published" type="boolean" required="no">
	<cfquery name="qLists" datasource="#this.datasource#">
	  SELECT listID, listName
	  FROM cm_list
	  WHERE formID<>0 AND
	  	    formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
			<cfif IsDefined("Arguments.published")>
			AND published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">
			</cfif>
	</cfquery>
	<cfreturn qLists>  
  </cffunction>
  
  <cffunction name="getListsWithoutForms" access="public" output="false" returntype="query">
    <cfargument name="published" type="boolean" required="no">
    <cfquery name="qLists" datasource="#this.datasource#">
	  SELECT listID, listName
	  FROM cm_list
	  WHERE formID=0
			<cfif IsDefined("Arguments.published")>
			AND published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">
			</cfif>
	</cfquery>
	<cfreturn qLists> 
  </cffunction>
  
  <cffunction name="getFormIDByListID" access="public" output="false" returntype="numeric">
    <cfargument name="listID" type="numeric" required="yes">
	
	<cfquery name="qFormID" datasource="#this.datasource#">
	  SELECT formID
	  FROM cm_list
	  WHERE listID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.listID#">
	</cfquery>
  
    <cfif qFormID.recordcount IS 0>
	  <cfreturn 0>
	<cfelse>
	  <cfreturn qFormID.formID>
	</cfif>
  
  </cffunction>
  
  
  
      
  


  
  
  
  
  
  
  
  
  
  
  
   
  
  
  
</cfcomponent>