<cfcomponent displayname="Form Manager Administrator" extends="Base" output="false">
  <cffunction name="getAllFormNames" access="public" output="no" returntype="query">
    <cfquery name="qAllFormNames" datasource="#this.datasource#">
	  SELECT formID, formName
	  FROM fm_form
	  ORDER BY formName
	</cfquery>
	<cfreturn qAllFormNames>
  </cffunction>
  
  <cffunction name="getFormProperties" access="public" output="no" returntype="query">
    <cfargument name="formID" type="numeric" required="yes">
    
    <cfquery name="qForm" datasource="#this.datasource#">
      SELECT *
      FROM fm_form
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    
    <cfreturn qForm>    
  </cffunction>
  
  <cffunction name="getFormsSummaryInfo" access="public" output="false" returntype="query">
    <cfquery name="qForms" datasource="#this.datasource#">
	  SELECT formID, formName, published, dateLastModified, 0 AS numSubmissions, 0 AS numFormItems, 0 AS numSubmissionsSaved
	  FROM fm_form
	  ORDER BY formName
	</cfquery>	
	
	<cfquery name="qNumSubmissions" datasource="#this.datasource#">
	  SELECT formID, COUNT(submissionID) AS numSubmissions
	  FROM fm_submission
	  GROUP BY formID
	</cfquery>
    
    <cfquery name="qNumSubmissionsSaved" datasource="#this.datasource#">
	  SELECT formID, COUNT(submissionID) AS numSubmissions
	  FROM fm_submission_saved
	  GROUP BY formID
	</cfquery>
	
	<cfquery name="qNumFormItems" datasource="#this.datasource#">
	  SELECT formID, COUNT(formItemID) AS numFormItems
	  FROM fm_formitem
	  WHERE fieldType <> 'Text Only'
	  GROUP BY formID
	</cfquery>
	
	<cfloop query="qForms">
	  <cfset tempID=qForms.formID>
	  <cfset tempNumSubmissions=0>
      <cfset tempNumSubmissionsSaved=0>
	  <cfset tempNumFormItems=0>
	  <cfloop query="qNumSubmissions">
	    <cfif tempID IS qNumSubmissions.formID>
		  <cfset tempNumSubmissions=qNumSubmissions.numSubmissions>
		  <cfbreak>
		</cfif>
	  </cfloop>
	  <cfset qForms.numSubmissions=tempNumSubmissions>
      
      <cfloop query="qNumSubmissionsSaved">
	    <cfif tempID IS qNumSubmissionsSaved.formID>
		  <cfset tempNumSubmissionsSaved=qNumSubmissionsSaved.numSubmissions>
		  <cfbreak>
		</cfif>
	  </cfloop>
	  <cfset qForms.numSubmissionsSaved=tempNumSubmissionsSaved>
      
	  <cfloop query="qNumFormItems">
	    <cfif tempID IS qNumFormItems.formID>
		  <cfset tempNumFormItems=qNumFormItems.numFormItems>
		  <cfbreak>
		</cfif>
	  </cfloop>
	  <cfset qForms.numFormItems=tempNumFormItems>	  
	</cfloop>
		
    <cfreturn qForms>
  </cffunction>
  
  <cffunction name="getFirstFormID" access="public" output="no" returntype="numeric">
    <cfquery name="qFormID" datasource="#this.datasource#" maxrows="1">
      SELECT formID
      FROM fm_form
      ORDER BY formName
    </cfquery>
    <cfif qFormID.recordcount GT 0>
      <cfreturn qFormID.formID>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
   
  <cffunction name="addForm" access="public" output="no">
    <cfargument name="formName" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="submissionConfirmationMessage" type="string" default="">
    <cfargument name="itemLabelPlacement" type="string" default="Right">
    <cfargument name="submitButtonLabel" type="string" default="Submit">
    <cfargument name="sendNotificationEmail" type="boolean" default="0">
    <cfargument name="useUserEmailAsSender" type="boolean" default="0">
    <cfargument name="notificationEmailFrom" type="string" default="">
    <cfargument name="notificationEmailTo" type="string" default="">
    <cfargument name="notificationEmailSubject" type="string" default="">
    <cfargument name="sendReceiptToUser" type="boolean" default="0">
    <cfargument name="receiptEmailFrom" type="string" default="">
    <cfargument name="receiptEmailSubject" type="string" default="">
    <cfargument name="includeSubmissionInReciept" type="boolean" default="0">
    <cfargument name="headerMessageInReceipt" type="string" default="">
    <cfargument name="enableDataSavingFeature" type="boolean" default="0">
    <cfargument name="dataRetrievalSenderEmailAddress" type="string" default="">
    
    <cfquery datasource="#this.datasource#">
      INSERT INTO fm_form
        (formName, published, submissionConfirmationMessage, submitButtonLabel, itemLabelPlacement,
         sendNotificationEmail, useUserEmailAsSender, notificationEmailFrom, notificationEmailTo,
         notificationEmailSubject, sendReceiptToUser, receiptEmailFrom, receiptEmailSubject,
         includeSubmissionInReciept, headerMessageInReceipt, enableDataSavingFeature, dataRetrievalSenderEmailAddress,
         dateCreated, dateLastModified)
      VALUES
        (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.formName#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.submissionConfirmationMessage#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.submitButtonLabel#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.itemLabelPlacement#">,
         
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.sendNotificationEmail#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.useUserEmailAsSender#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.notificationEmailFrom#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.notificationEmailTo#">,
         
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.notificationEmailSubject#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.sendReceiptToUser#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.receiptEmailFrom#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.receiptEmailSubject#">,
         
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.includeSubmissionInReciept#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.headerMessageInReceipt#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.enableDataSavingFeature#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.dataRetrievalSenderEmailAddress#">,
         
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
    </cfquery>
    
    <cfquery name="qNewFormID" datasource="#this.datasource#">
      SELECT MAX(formID) AS newFormID
      FROM fm_form
    </cfquery>
    <cfquery datasource="#this.datasource#">
      INSERT INTO fm_group (formID)
      VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#qNewFormID.newFormID#">)
    </cfquery>
    
    <cfif (Arguments.sendNotificationEmail And Arguments.useUserEmailAsSender) OR Arguments.sendReceiptToUser>
      <cfset addEmailField(qNewFormID.newFormID)>
    </cfif>    
  </cffunction>
  
  <cffunction name="editForm" access="public" output="no">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="formName" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="submissionConfirmationMessage" type="string" default="">
    <cfargument name="submitButtonLabel" type="string" default="Submit">
    <cfargument name="sendNotificationEmail" type="boolean" default="0">
    <cfargument name="useUserEmailAsSender" type="boolean" default="0">
    <cfargument name="notificationEmailFrom" type="string" default="">
    <cfargument name="notificationEmailTo" type="string" default="">
    <cfargument name="notificationEmailSubject" type="string" default="">
    <cfargument name="sendReceiptToUser" type="boolean" default="0">
    <cfargument name="receiptEmailFrom" type="string" default="">
    <cfargument name="receiptEmailSubject" type="string" default="">
    <cfargument name="includeSubmissionInReciept" type="boolean" default="0">
    <cfargument name="headerMessageInReceipt" type="string" default="">
    <cfargument name="enableDataSavingFeature" type="boolean" default="0">
    <cfargument name="dataRetrievalSenderEmailAddress" type="string" default="">
    
    <cfquery datasource="#this.datasource#">
      UPDATE fm_form
      SET
         formName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.formName#">,
         published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
         submissionConfirmationMessage=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.submissionConfirmationMessage#">,
         submitButtonLabel=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.submitButtonLabel#">,
         itemLabelPlacement=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.itemLabelPlacement#">,
         
         sendNotificationEmail=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.sendNotificationEmail#">,
         useUserEmailAsSender=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.useUserEmailAsSender#">,
         notificationEmailFrom=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.notificationEmailFrom#">,
         notificationEmailTo=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.notificationEmailTo#">,
         
         notificationEmailSubject=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.notificationEmailSubject#">,
         sendReceiptToUser=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.sendReceiptToUser#">,
         receiptEmailFrom=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.receiptEmailFrom#">,
         receiptEmailSubject=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.receiptEmailSubject#">,
         
         includeSubmissionInReciept=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.includeSubmissionInReciept#">,
         headerMessageInReceipt=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.headerMessageInReceipt#">,
         enableDataSavingFeature=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.enableDataSavingFeature#">,
         dataRetrievalSenderEmailAddress=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.dataRetrievalSenderEmailAddress#">,
         dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    
    <cfif (Arguments.sendNotificationEmail And Arguments.useUserEmailAsSender) OR Arguments.sendReceiptToUser>
      <cfset addEmailField(Arguments.formID)>
    </cfif>
  </cffunction>
  
  <cffunction name="copyForm" access="public" output="no">
    <cfargument name="formID" type="numeric" required="yes">
	<cfargument name="formName" type="string" required="yes">
	<cfargument name="published" type="boolean" default="0">
    
	<cfset formInfo=getFormProperties(Arguments.formID)>
	
    <cfquery datasource="#this.datasource#">
	  INSERT INTO fm_form
	  (formName, published, submissionConfirmationMessage, submitButtonLabel, itemLabelPlacement,
       sendNotificationEmail, useUserEmailAsSender, notificationEmailFrom, notificationEmailTo, notificationEmailSubject,
       sendReceiptToUser, receiptEmailFrom, receiptEmailSubject, includeSubmissionInReciept, headerMessageInReceipt,
       enableDataSavingFeature, dataRetrievalSenderEmailAddress, dateCreated, dateLastModified)
	   VALUES       
	   (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.formName#">,
		<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
		<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#formInfo.submissionConfirmationMessage#">,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#formInfo.submitButtonLabel#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#formInfo.itemLabelPlacement#">,

		<cfqueryparam cfsqltype="cf_sql_tinyint" value="#formInfo.sendNotificationEmail#">,
        <cfqueryparam cfsqltype="cf_sql_tinyint" value="#formInfo.useUserEmailAsSender#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#formInfo.notificationEmailFrom#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#formInfo.notificationEmailTo#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#formInfo.notificationEmailSubject#">,
        
        <cfqueryparam cfsqltype="cf_sql_tinyint" value="#formInfo.sendReceiptToUser#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#formInfo.receiptEmailFrom#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#formInfo.receiptEmailSubject#">,
        <cfqueryparam cfsqltype="cf_sql_tinyint" value="#formInfo.includeSubmissionInReciept#">,
        <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#formInfo.headerMessageInReceipt#">,
        
        <cfqueryparam cfsqltype="cf_sql_tinyint" value="#formInfo.enableDataSavingFeature#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#formInfo.dataRetrievalSenderEmailAddress#">,
		<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
		)
	</cfquery>
	
	<cfquery name="qFormID" datasource="#this.datasource#">
	  SELECT MAX(formID) as ID
	  FROM fm_form
	</cfquery>
	
	<cfset newFormID=qFormID.ID>
	
	<cfquery name="qGroup" datasource="#this.datasource#">
	  SELECT *
	  FROM fm_group
	  WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
	</cfquery>
	
	<cfloop query="qGroup">
	  <cfquery datasource="#this.datasource#">
	    INSERT INTO fm_group(formID, displaySeq)
	    VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#newFormID#">,
			    <cfqueryparam cfsqltype="cf_sql_float" value="#displaySeq#">)
	  </cfquery>
	  <cfquery name="qGroupID" datasource="#this.datasource#">
	    select MAX(groupID) as ID
	    from fm_group
	  </cfquery>
	  <cfset newGroupID=qGroupID.ID>	
	
	  <cfquery name="qFormItems" datasource="#this.datasource#">
	    SELECT *
		FROM fm_formitem
		WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
			  groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#groupID#">
	  </cfquery>
		  
	  <cfloop query="qFormItems">
	    <cfquery datasource="#this.datasource#">
	    INSERT INTO fm_formitem
	     (formID, groupID, displaySeq, active, fieldType, 
          fieldContext, fieldSize, mandatory, applyValidation, dataType,
          optionList, numRequiredOptions, instructionText, shortTextDefault, longTextDefault,
          itemLabel, itemName, itemNotes, boldItemLabel,
		  dateLastModified, dateCreated)
		VALUES (		    
		  <cfqueryparam cfsqltype="cf_sql_integer" value="#newFormID#">,
		  <cfqueryparam cfsqltype="cf_sql_integer" value="#newGroupID#">,
		  <cfqueryparam cfsqltype="cf_sql_float" value="#displaySeq#">,			
		  <cfqueryparam cfsqltype="cf_sql_tinyint" value="#active#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#fieldType#">,
          
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#fieldContext#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#fieldSize#">,
          <cfqueryparam cfsqltype="cf_sql_tinyint" value="#mandatory#">,
          <cfqueryparam cfsqltype="cf_sql_tinyint" value="#applyValidation#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#dataType#">,
          
          <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#optionList#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#numRequiredOptions#">,
          <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#instructionText#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortTextDefault#">,
          <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#longTextDefault#">,
          
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#itemLabel#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#itemName#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#itemNotes#">,
          <cfqueryparam cfsqltype="cf_sql_tinyint" value="#boldItemLabel#">,
				
		  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	    </cfquery>
	  </cfloop>    	
	</cfloop> 
  </cffunction>
  
  <cffunction name="deleteForm" access="public" output="no">
    <cfargument name="formID" type="numeric" required="yes">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_formitem
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
       
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_group
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_form
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_submitteddata
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_submission
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_submitteddata_saved
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_submission_saved
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    
    <!--- remove link from email list sign-up forms ---->
    <cftry>
    <cfquery datasource="#this.datasource#">
      UPDATE cm_signupform
      SET formID=0
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    <cfcatch></cfcatch>
    </cftry>
  </cffunction>
  
  <cffunction name="getGroupIDsByFormID" access="public" output="no" returntype="query">
    <cfargument name="formID" type="numeric" required="yes">
    
    <cfquery name="qGroupIDs" datasource="#this.datasource#">
      SELECT groupID
      FROM fm_group
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
      ORDER BY displaySeq
    </cfquery>
    
    <cfreturn qGroupIDs>
  </cffunction>
  
  <cffunction name="addPage" access="public" output="no" returntype="numeric">
    <cfargument name="formID" type="numeric" required="yes">
    
    <cfquery name="qMaxDisplaySeq" datasource="#this.datasource#">
      SELECT MAX(displaySeq) maxDisplaySeq
      FROM fm_group
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    <cfif IsNumeric(qMaxDisplaySeq.maxDisplaySeq)>
      <cfset newDisplaySeq=qMaxDisplaySeq.maxDisplaySeq + 1>
    <cfelse>
      <cfset newDisplaySeq=1>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      INSERT INTO fm_group (formID, displaySeq)
      VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">,
      		  <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">)
    </cfquery>
    
    <cfquery name="qMaxGroupID" datasource="#this.datasource#">
      SELECT MAX(groupID) maxGroupID
      FROM fm_group
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    <cfreturn qMaxGroupID.maxGroupID>
  </cffunction>
  
  <cffunction name="deletePage" access="public" output="no">
    <cfargument name="groupID" type="numeric" required="yes">

    <cfquery name="qFormID" datasource="#this.datasource#">
      SELECT formID
      FROM fm_group
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
    </cfquery>
    <cfif qFormID.recordcount Is 0>
      <cfreturn>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_formitem
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_group
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
    </cfquery>
    
    <cfquery name="qGroupID" datasource="#this.datasource#">
      SELECT groupID
      FROM fm_group
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qFormID.formID#">
    </cfquery>
    <cfif qGroupID.recordcount Is 0>
      <cfset addPage(qFormID.formID)>     
    </cfif>
  </cffunction>
  
  <cffunction name="getFormIDByGroupID" access="public" output="no" returntype="numeric">
    <cfargument name="groupID" type="numeric" required="yes">
    
    <cfquery name="qFormID" datasource="#this.datasource#">
      SELECT formID
      FROM fm_group
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
    </cfquery>
    <cfif qFormID.recordcount GT 0>
      <cfreturn qFormID.formID>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="movePage" access="public" output="no">
    <cfargument name="groupID" type="numeric" required="yes">
    <cfargument name="direction" type="string" required="yes">
    
    <cfset formID=getFormIDByGroupID(Arguments.groupID)>
    <cfif formID Is 0><cfreturn></cfif>
    
    <cfquery name="qDisplaySeq" datasource="#this.datasource#">
      SELECT displaySeq
      FROM fm_group
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
    </cfquery>
    <cfif qDisplaySeq.recordcount Is 0><cfreturn></cfif>
    <cfset targetDisplaySeq=qDisplaySeq.displaySeq>
    
    <cfif Arguments.direction Is "right">
      <cfquery name="qNext" datasource="#this.datasource#">
        SELECT groupID, displaySeq
        FROM fm_group
        WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#formID#"> AND
        	  displaySeq > <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq ASC
      </cfquery>
    <cfelse>
      <cfquery name="qNext" datasource="#this.datasource#">
        SELECT groupID, displaySeq
        FROM fm_group
        WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#formID#"> AND
        	  displaySeq < <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq DESC
      </cfquery>    
    </cfif>
    
    <cfif qNext.recordcount Is 0><cfreturn></cfif>
    
    <!--- Swap --->
    <cfquery datasource="#this.datasource#">
      UPDATE fm_group
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#qNext.displaySeq#">
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      UPDATE fm_group
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qNext.groupID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="getFormItemsByGroupID" access="public" output="no" returntype="query">
    <cfargument name="groupID" type="numeric" required="yes">
    
    <cfquery name="qFormItems" datasource="#this.datasource#">
      SELECT *
      FROM	fm_formitem
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
      ORDER BY displaySeq
    </cfquery>
  
    <cfreturn qFormItems>  
  </cffunction>
  
  <cffunction name="addEmailField" access="public" output="no">
    <cfargument name="formID" type="numeric" required="yes">
    
    <cfquery name="qEmailField" datasource="#this.datasource#">
      SELECT formItemID
      FROM	fm_formitem
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
      		fieldContext='Email'
    </cfquery>
    <cfif qEmailField.recordcount GT 0><!--- already existed --->
      <cfreturn>
    <cfelse>
  	  <cfquery name="qFirstPage" datasource="#this.datasource#">
        SELECT groupID
        FROM fm_group
        WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
        ORDER BY displaySeq
      </cfquery>
      <cfif qFirstPage.recordcount GT 0>
        <cfset groupID=qFirstPage.groupID>
        <cfquery name="qFirstDisplaySeq" datasource="#this.datasource#">
          SELECT displaySeq
          FROM fm_formitem
          WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#groupID#">
          ORDER BY displaySeq
        </cfquery>
    	<cfif qFirstDisplaySeq.recordcount GT 0>
          <cfset displaySeq=qFirstDisplaySeq.displaySeq - 1>
        <cfelse>
          <cfset displaySeq=1>
        </cfif>
        <cfquery datasource="#this.datasource#">
          INSERT INTO fm_formitem
          (formID, groupID, active, displaySeq, fieldType,
           fieldContext, fieldSize, mandatory, applyValidation, dataType,
           itemLabel, itemName, itemNotes, boldItemLabel,
           dateCreated, dateLastModified)
          VALUES
          (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">,
           <cfqueryparam cfsqltype="cf_sql_integer" value="#groupID#">,       
           1,
           <cfqueryparam cfsqltype="cf_sql_float" value="#displaySeq#">,
           'Short Text',
           
           'Email',       
           'Medium',
           1,
           1,
           'Email',
           
           'E-Mail Address',
           'E-mail Address',
           '',
           0,
           
           <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
           <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
        </cfquery>
      </cfif>
    </cfif>
  </cffunction>
  
  <cffunction name="getFormItemInfo" access="public" output="no" returntype="query">
    <cfargument name="formItemID" type="numeric" required="yes">
  
    <cfquery name="qFormItems" datasource="#this.datasource#">
      SELECT *
      FROM fm_formitem
      WHERE formItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formItemID#">
    </cfquery>
    
    <cfreturn qFormItems>
  </cffunction>
  
  <cffunction name="addFormItem" access="public" output="no" returntype="struct">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="groupID" type="numeric" required="yes">
    <cfargument name="active" type="boolean" default="0">
    <cfargument name="fieldType" type="string" required="yes">
    <cfargument name="fieldContext" type="string" default="">
    <cfargument name="fieldSize" type="string" default="Medium">
    <cfargument name="mandatory" type="boolean" default="0">
    <cfargument name="applyValidation" type="boolean" default="0">
	<cfargument name="dataType" type="string" default="Text">
    <cfargument name="optionList" type="string" default="">
    <cfargument name="numRequiredOptions" type="numeric" default="0">
	<cfargument name="instructionText" type="string" default="">
    <cfargument name="shortTextDefault" type="string" default="">
    <cfargument name="longTextDefault" type="string" default="">
    <cfargument name="itemLabel" type="string" default="">
    <cfargument name="itemName" type="string" default="">
    <cfargument name="itemNotes" type="string" default="">
    <cfargument name="boldItemLabel" type="boolean" default="0">
    <cfargument name="itemNameSameAsLabel" type="boolean" default="0">
    
    <cfset var returnedStruct=StructNew()>
	<cfset returnedStruct.SUCCESS=true>
	<cfset returnedStruct.MESSAGE="">
    
    <cfif NOT Compare(Arguments.fieldContext, "Email")>
      <!--- a form can have at most one predefined Email field --->
      <cfquery name="qEmailFields" datasource="#this.datasource#">
        SELECT formItemID
        FROM fm_formitem
        WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
        	  fieldContext='Email'
      </cfquery>
      <cfif qEmailFields.recordcount GT 0>
        <cfset returnedStruct.SUCCESS=false>
	    <cfset returnedStruct.MESSAGE="This form already had a predefined 'E-Mail' field. A form can only have at most one predefined 'E-Mail' field.">
        <cfreturn returnedStruct>
      </cfif>
    </cfif>
    
    <cfif NOT Compare(Arguments.fieldContext, "Full Name")>
      <!--- a form can have at most one predefined Name field --->
      <cfquery name="qNameFields" datasource="#this.datasource#">
        SELECT formItemID
        FROM fm_formitem
        WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
        	  fieldContext='Full Name'
      </cfquery>
      <cfif qNameFields.recordcount GT 0>
        <cfset returnedStruct.SUCCESS=false>
	    <cfset returnedStruct.MESSAGE="This form already had a predefined 'Full Name' field. A form can only have at most one predefined 'Full Name' field.">
        <cfreturn returnedStruct>
      </cfif>
    </cfif>
    
    <cfif Compare(Arguments.dataType, "Text")>
      <cfset Arguments.applyValidation=1>
    </cfif>
    
    <cfif Arguments.itemNameSameAsLabel>
      <cfset Arguments.itemName=Arguments.itemLabel>
    </cfif>
    
    <cfquery name="qDisplaySeq" datasource="#this.datasource#">
      SELECT MAX(displaySeq) AS maxDisplaySeq
      FROM fm_formitem
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
      	    groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
    </cfquery>
    <cfif IsNumeric(qDisplaySeq.maxDisplaySeq)>
      <cfset displaySeq=qDisplaySeq.maxDisplaySeq+1>
    <cfelse>
      <cfset displaySeq=1>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      INSERT INTO fm_formitem
      (formID, groupID, active, displaySeq, fieldType,
       fieldContext, fieldSize, mandatory, applyValidation, dataType,
       optionList, numRequiredOptions, instructionText, shortTextDefault, longTextDefault,
       itemLabel, itemName, itemNotes, boldItemLabel,
       dateCreated, dateLastModified)
      VALUES
      (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">,
       <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">,       
       <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.active#">,
       <cfqueryparam cfsqltype="cf_sql_float" value="#displaySeq#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.fieldType#">,
       
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.fieldContext#">,       
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.fieldSize#">,
       <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.mandatory#">,
       <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.applyValidation#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.dataType#">,
       
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.optionList#">,
       <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.numRequiredOptions#">,
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.instructionText#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.shortTextDefault#">,
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.longTextDefault#">,
       
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.itemLabel#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.itemName#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.itemNotes#">,
       <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.boldItemLabel#">,
       
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
    </cfquery>
    
    <cfreturn returnedStruct>
  </cffunction>

  <cffunction name="editFormItem" access="public" output="no" returntype="struct">
    <cfargument name="formItemID" type="numeric" required="yes">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="groupID" type="numeric" required="yes">
    <cfargument name="active" type="boolean" default="0">
    <cfargument name="fieldType" type="string" required="yes">
    <cfargument name="fieldContext" type="string" default="">
    <cfargument name="fieldSize" type="string" default="Medium">
    <cfargument name="mandatory" type="boolean" default="0">
    <cfargument name="applyValidation" type="boolean" default="0">
	<cfargument name="dataType" type="string" default="Text">
    <cfargument name="optionList" type="string" default="">
    <cfargument name="numRequiredOptions" type="numeric" default="0">
	<cfargument name="instructionText" type="string" default="">
    <cfargument name="shortTextDefault" type="string" default="">
    <cfargument name="longTextDefault" type="string" default="">
    <cfargument name="itemLabel" type="string" default="">
    <cfargument name="itemName" type="string" default="">
    <cfargument name="itemNotes" type="string" default="">
    <cfargument name="boldItemLabel" type="boolean" default="0">
    <cfargument name="itemNameSameAsLabel" type="boolean" default="0">
    
    <cfset var returnedStruct=StructNew()>
	<cfset returnedStruct.SUCCESS=true>
	<cfset returnedStruct.MESSAGE="">
    
    <cfif NOT Compare(Arguments.fieldContext, "Email")>
      <!--- a form can have at most one predefined Email field --->
      <cfquery name="qEmailFields" datasource="#this.datasource#">
        SELECT formItemID
        FROM fm_formitem
        WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
        	  fieldContext='Email' AND
              formItemID<><cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formItemID#">
      </cfquery>
      <cfif qEmailFields.recordcount GT 0>
        <cfset returnedStruct.SUCCESS=false>
	    <cfset returnedStruct.MESSAGE="This form already had a predefined 'E-Mail' field. A form can only have at most one predefined 'E-Mail' field.">
        <cfreturn returnedStruct>
      </cfif>
    </cfif>
    
    <cfif NOT Compare(Arguments.fieldContext, "Full Name")>
      <!--- a form can have at most one predefined Name field --->
      <cfquery name="qNameFields" datasource="#this.datasource#">
        SELECT formItemID
        FROM fm_formitem
        WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
        	  fieldContext='Full Name' AND
              formItemID<><cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formItemID#">
      </cfquery>
      <cfif qNameFields.recordcount GT 0>
        <cfset returnedStruct.SUCCESS=false>
	    <cfset returnedStruct.MESSAGE="This form already had a predefined 'Full Name' field. A form can only have at most one predefined 'Full Name' field.">
        <cfreturn returnedStruct>
      </cfif>
    </cfif>
    
    <cfif Compare(Arguments.dataType, "Text")>
      <cfset Arguments.applyValidation=1>
    </cfif>
    
    <cfif Arguments.itemNameSameAsLabel>
      <cfset Arguments.itemName=Arguments.itemLabel>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      UPDATE fm_formitem
      SET active=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.active#">,
          fieldContext=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.fieldContext#">,
          fieldSize=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.fieldSize#">,
          mandatory=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.mandatory#">,
          applyValidation=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.applyValidation#">,
          dataType=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.dataType#">,
          optionList=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.optionList#">,
          numRequiredOptions=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.numRequiredOptions#">,
          instructionText=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.instructionText#">,
          shortTextDefault=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.shortTextDefault#">,
          longTextDefault=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.longTextDefault#">,
          itemLabel=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.itemLabel#">,
          itemName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.itemName#">,
          itemNotes=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.itemNotes#">,
          boldItemLabel=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.boldItemLabel#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE formItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formItemID#">
    </cfquery>
    
    <cfreturn returnedStruct>
  </cffunction>
  
  <cffunction name="deleteFormItem" access="public" output="no">
    <cfargument name="formItemID" type="numeric" required="yes">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_formitem
      WHERE formItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formItemID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_submitteddata
      WHERE formItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formItemID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM fm_submitteddata_saved
      WHERE formItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formItemID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="moveFormItem" access="public" output="no">
    <cfargument name="formItemID" type="numeric" required="yes">
    <cfargument name="direction" type="string" required="yes">
    
    <cfquery name="qItemBasicInfo" datasource="#this.datasource#">
      SELECT groupID, displaySeq
      FROM fm_formitem
      WHERE formItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formItemID#">
    </cfquery>
    <cfif qItemBasicInfo.recordcount Is 0><cfreturn></cfif>
    
    <cfset groupID=qItemBasicInfo.groupID>
    <cfset targetDisplaySeq=qItemBasicInfo.displaySeq>
    
    <cfquery name="qNextItem" datasource="#this.datasource#">
      SELECT formItemID, displaySeq
      FROM fm_formitem
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#groupID#"> AND
      		<cfif Not CompareNoCase(Arguments.direction, "Up")><!--- move up --->
            displaySeq < <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
            <cfelse><!--- move down --->
            displaySeq > <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
            </cfif>
      <cfif Not CompareNoCase(Arguments.direction, "Up")><!--- move up --->
      ORDER BY displaySeq DESC
      <cfelse><!--- move down --->
      ORDER BY displaySeq ASC
	  </cfif>
    </cfquery>
    
    <cfif qNextItem.recordcount Is 0>
      <cfif Not CompareNoCase(Arguments.direction, "Up")>
        <!--- move this form item to previous group --->
		<cfset moveToPreviousGroup(Arguments.formItemID)>
	  <cfelse>
	    <!--- move this form item to next group --->
        <cfset moveToNextGroup(Arguments.formItemID)>
      </cfif>
      <cfreturn>
	</cfif>
    
    <cfquery datasource="#this.datasource#">
      UPDATE fm_formitem
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#qNextItem.displaySeq#">
      WHERE formItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formItemID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      UPDATE fm_formitem
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
      WHERE formItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qNextItem.formItemID#">
    </cfquery>
    
    <cfreturn>
  </cffunction>
  
  <cffunction name="moveToPreviousGroup" access="public" output="no">
    <cfargument name="formItemID" type="numeric" required="yes">
    
    <cfquery name="qItemBasicInfo" datasource="#this.datasource#">
      SELECT fm_formitem.formID, fm_formitem.groupID, fm_group.displaySeq
      FROM fm_formitem JOIN fm_group ON fm_formitem.groupID=fm_group.groupID
      WHERE fm_formitem.formItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formItemID#">
    </cfquery>
    <cfif qItemBasicInfo.recordcount Is 0><cfreturn></cfif>
    
    <cfquery name="qPreviousGroup" datasource="#this.datasource#">
      SELECT groupID
      FROM fm_group
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qItemBasicInfo.formID#"> AND
      	    displaySeq < <cfqueryparam cfsqltype="cf_sql_float" value="#qItemBasicInfo.displaySeq#">
      ORDER BY displaySeq DESC
    </cfquery>
    <cfif qPreviousGroup.recordcount Is 0><cfreturn></cfif>
    <cfset newGroupID=qPreviousGroup.groupID>
    
	<cfquery name="qMaxDisplaySeq" datasource="#this.datasource#">
      SELECT MAX(displaySeq) AS displaySeq
      FROM fm_formitem
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#newGroupID#">
    </cfquery>
    <cfif IsNumeric(qMaxDisplaySeq.displaySeq)>
      <cfset newDisplaySeq=qMaxDisplaySeq.displaySeq+1>
    <cfelse>
      <cfset newDisplaySeq=1>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      UPDATE fm_formitem
      SET groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#newGroupID#">,
      	  displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">
      WHERE formItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formItemID#">
    </cfquery>    
  </cffunction>
  
  <cffunction name="moveToNextGroup" access="public" output="no">
    <cfargument name="formItemID" type="numeric" required="yes">
    
    <cfquery name="qItemBasicInfo" datasource="#this.datasource#">
      SELECT fm_formitem.formID, fm_formitem.groupID, fm_group.displaySeq
      FROM fm_formitem JOIN fm_group ON fm_formitem.groupID=fm_group.groupID
      WHERE fm_formitem.formItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formItemID#">
    </cfquery>
    <cfif qItemBasicInfo.recordcount Is 0><cfreturn></cfif>
    
    <cfquery name="qNextGroup" datasource="#this.datasource#">
      SELECT groupID
      FROM fm_group
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qItemBasicInfo.formID#"> AND
      		displaySeq > <cfqueryparam cfsqltype="cf_sql_float" value="#qItemBasicInfo.displaySeq#">
      ORDER BY displaySeq ASC
    </cfquery>
    <cfif qNextGroup.recordcount Is 0><cfreturn></cfif>
    <cfset newGroupID=qNextGroup.groupID>
    
	<cfquery name="qMinDisplaySeq" datasource="#this.datasource#">
      SELECT MIN(displaySeq) AS displaySeq
      FROM fm_formitem
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#newGroupID#">
    </cfquery>
    <cfif IsNumeric(qMinDisplaySeq.displaySeq)>
      <cfset newDisplaySeq=qMinDisplaySeq.displaySeq-1>
    <cfelse>
      <cfset newDisplaySeq=1>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      UPDATE fm_formitem
      SET groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#newGroupID#">,
      	  displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">
      WHERE formItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formItemID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="getNumSubmissions" access="public" output="no" returntype="numeric">
    <cfargument name="formID" required="yes" type="numeric">
	<cfargument name="FROMDate" required="no" type="string">
	<cfargument name="toDate" required="no" type="string">
    
	<cfquery name="qNumSubmissions" datasource="#this.datasource#">
	  SELECT COUNT(submissionID) AS numSubmissions
	  FROM fm_submission
	  WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
			<cfif IsDefined("Arguments.FROMDate")>
			AND dateSubmitted >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#DateAdd("d", 0, Arguments.FROMDate)#">
			  <cfif IsDefined("Arguments.toDate")>
			  AND dateSubmitted < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#DateAdd("d", 1, Arguments.toDate)#">
			  </cfif>
			</cfif>
	</cfquery>
    
	<cfreturn qNumSubmissions.numSubmissions>
  </cffunction>
  
  <cffunction name="getNumSubmissionsInSavedTable" access="public" output="no" returntype="numeric">
    <cfargument name="formID" required="yes" type="numeric">
    
	<cfquery name="qNumSubmissions" datasource="#this.datasource#">
	  SELECT COUNT(submissionID) AS numSubmissions
	  FROM fm_submission_saved
	  WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
	</cfquery>
    
	<cfreturn qNumSubmissions.numSubmissions>
  </cffunction>
  
  <cffunction name="getSubmissions" access="public" output="no" returntype="struct">
    <cfargument name="formID" required="yes" type="numeric">
	<cfargument name="pageNum" type="numeric" required="no" default="1">
	<cfargument name="startIndex" type="numeric" required="no" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
	<cfargument name="useDateRange" type="boolean" required="no" default="0">
    <cfargument name="fromDate" type="string" required="no">
    <cfargument name="toDate" type="string" required="no">    
	<cfargument name="submissionID" type="numeric" required="no">
	<cfargument name="submissionIDList" type="string" required="no">
	<cfargument name="displayAll" type="boolean" required="no" default="0">
	<cfargument name="formItemIDList" type="string" required="no" default="">
    <cfargument name="numColumns" type="numeric" required="no">
	
	<cfset var resultStruct = StructNew()>
	<!--- ::
	  .numAllSubmissions
	  .numDisplayedSubmissions
	  .numColumns
	  .columnNames[]
	  .formItemIDs[]
	  .submissions[]
	  	.submissionID
	  	.["formItemID"]				
	--->
	
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
    
	<cfif Arguments.useDateRange>
	  <cfset Variables.fromDate=DateAdd("d", 0, Arguments.fromDate)>
	  <cfset Variables.toDate=DateAdd("d", 1, Arguments.toDate)>
	  <cfset resultStruct.numAllSubmissions=getNumSubmissions(Arguments.formID, Arguments.fromDate, Arguments.toDate)>	
	<cfelse>
	  <cfset resultStruct.numAllSubmissions=getNumSubmissions(Arguments.formID)>	
	</cfif>	
	
	<cfset sid = Arguments.startIndex - 1>	
	<!--- :: numAllSubmissions --->	
	<cfquery name="qSubmissions" datasource="#this.datasource#">
	  SELECT submissionID, formID, userEmail, userFirstName, userLastName, dateSubmitted
	  FROM fm_submission
	  WHERE fm_submission.formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
	  	  <cfif IsDefined("Arguments.submissionID")>
	  		AND submissionID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.submissionID#">
	  	  <cfelseif IsDefined("Arguments.submissionIDList") AND Compare(Arguments.submissionIDList, "")>
	  		AND submissionID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.submissionIDList#" list="yes">)
	  	  <cfelseif Arguments.useDateRange>
	  	    AND dateSubmitted >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.fromDate#">
	  		AND dateSubmitted < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.toDate#">
	  	  </cfif>  
	  ORDER BY dateSubmitted DESC
	  <cfif NOT Arguments.displayAll>
	  LIMIT #sid#, #Arguments.numItemsPerPage#
	  </cfif>
	</cfquery>
	<!--- :: numDisplayedSubmissions --->
	<cfset resultStruct.numDisplayedSubmissions=qSubmissions.recordcount>	
	
	<cfquery name="qColumns" datasource="#this.datasource#">
	  SELECT fm_formitem.formItemID, fm_formitem.itemName,
	  		 fm_formitem.itemLabel, fm_formitem.fieldType
	  FROM fm_formitem JOIN fm_group ON fm_formitem.groupID=fm_group.groupID
	  WHERE fm_formitem.formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
			fm_formitem.fieldType<>'Text Only'		
	  		<cfif Compare(Arguments.formItemIDList, "")>
	  		AND fm_formitem.formItemID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.formItemIDList#" list="yes">)
	  		</cfif>	
	  ORDER BY fm_group.displaySeq, fm_formitem.displaySeq
      <cfif IsDefined("Arguments.numColumns")>
      LIMIT 0, #Arguments.numColumns-1#
      </cfif>
	</cfquery>
    
    <cfscript>
	  resultStruct.numColumns = qColumns.recordcount; // numColumns
	  resultStruct.columnNames = ArrayNew(1); // columnNames[]
	  resultStruct.formItemIDs = ArrayNew(1);
	  for (idx = 1; idx LTE qColumns.recordcount; idx = idx +1) {
		resultStruct.columnNames[idx] = qColumns.itemName[idx];
	    resultStruct.formItemIDs[idx] = qColumns.formItemID[idx];
	  }
	</cfscript>
		
	<cfquery name="qSubmittedData" datasource="#this.datasource#">
	  SELECT fm_submitteddata.submissionID, fm_submitteddata.formItemID, fm_submitteddata.valueText
	  FROM fm_submitteddata JOIN fm_submission ON fm_submitteddata.submissionID=fm_submission.submissionID
	  WHERE fm_submitteddata.formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
			<cfif IsDefined("Arguments.submissionID")>
	  		AND fm_submitteddata.submissionID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.submissionID#">
			<cfelseif IsDefined("Arguments.submissionIDList") AND Compare(Arguments.submissionIDList,"")>
	  		AND fm_submitteddata.submissionID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.submissionIDList#" list="yes">)
			<cfelseif Arguments.useDateRange>
	  	    AND fm_submission.dateSubmitted >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.fromDate#">
	  		AND fm_submission.dateSubmitted < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.toDate#">
            </cfif>
			<cfif Compare(Arguments.formItemIDList, "")>
	  		AND fm_submitteddata.formItemID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.formItemIDList#" list="yes">)
            <cfelse>
              <cfif qColumns.recordcount GT 0>
              AND fm_submitteddata.formItemID IN (#ValueList(qColumns.formItemID)#)
              <cfelse>
              AND 1=-1 <!--- nothing will be SELECTed since no columns were SELECTed --->
              </cfif>
	  		</cfif>
      ORDER BY fm_submitteddata.submissionID
	</cfquery>
    
    <cfscript>
	  data = StructNew();
	  tempID=0;
	  for (dataIdx = 1; dataIdx LTE qSubmittedData.recordcount; dataIdx = dataIdx +1) {
	    submissionID=qSubmittedData.submissionID[dataIdx];
		formItemID=qSubmittedData.formItemID[dataIdx];
	    if (Compare(tempID, submissionID)) {
		  data["s#submissionID#"]=StructNew();
		}
		data["s#submissionID#"]["f#formItemID#"]=qSubmittedData.valueText[dataIdx];
		tempID=submissionID;
	  }
	
	  resultStruct.submissions = ArrayNew(1); // resultStruct.submissions[]
	  for (idx = 1; idx LTE qSubmissions.recordcount; idx = idx +1) {
	    resultStruct.submissions[idx] = StructNew();
	    resultStruct.submissions[idx].submissionID = qSubmissions.submissionID[idx];
	    resultStruct.submissions[idx].dateSubmitted = qSubmissions.dateSubmitted[idx];
	    resultStruct.submissions[idx].userEmail = qSubmissions.userEmail[idx];
	    resultStruct.submissions[idx].userFirstName = qSubmissions.userFirstName[idx];
	    resultStruct.submissions[idx].userLastName = qSubmissions.userLastName[idx];
		for (idx2 = 1; idx2 LTE qColumns.recordcount; idx2 = idx2 +1) {
		  if (IsDefined("data.s#qSubmissions.submissionID[idx]#.f#qColumns.formItemID[idx2]#")) {
		    resultStruct.submissions[idx]["#qColumns.formItemID[idx2]#"]=data["s#qSubmissions.submissionID[idx]#"]["f#qColumns.formItemID[idx2]#"];
		  } else {
	        resultStruct.submissions[idx]["#qColumns.formItemID[idx2]#"]="";
		  }
	    }
	  }
	</cfscript>
	
	<cfreturn resultStruct>
  </cffunction>  
  
  <cffunction name="getSubmissionsInSavedTable" access="public" output="no" returntype="struct">
    <cfargument name="formID" required="yes" type="numeric">
    <cfargument name="submissionID" type="numeric" required="no">
	<cfargument name="pageNum" type="numeric" required="no" default="1">
	<cfargument name="startIndex" type="numeric" required="no" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    <cfargument name="numColumns" type="numeric" required="no">
	
	<cfset var resultStruct = StructNew()>
	<!--- ::
	  .numAllSubmissions
	  .numDisplayedSubmissions
	  .numColumns
	  .columnNames[]
	  .formItemIDs[]
	  .submissions[]
	  	.submissionID
	  	.["formItemID"]				
	--->
	
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
    
	<cfset resultStruct.numAllSubmissions=getNumSubmissionsInSavedTable(Arguments.formID)>
	
	<cfset sid = Arguments.startIndex - 1>	
	<!--- :: numAllSubmissions --->	
	<cfquery name="qSubmissions" datasource="#this.datasource#">
	  SELECT submissionID, formID, userEmail, userFirstName, userLastName, emailForSavedData, passwordForSavedData, dateSubmitted
	  FROM fm_submission_saved
	  WHERE fm_submission_saved.formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
      	    <cfif IsDefined("Arguments.submissionID")>
            AND submissionID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.submissionID#">
            </cfif>
	  ORDER BY dateSubmitted DESC
	  LIMIT #sid#, #Arguments.numItemsPerPage#
	</cfquery>
	<!--- :: numDisplayedSubmissions --->
	<cfset resultStruct.numDisplayedSubmissions=qSubmissions.recordcount>	
	
	<cfquery name="qColumns" datasource="#this.datasource#">
	  SELECT fm_formitem.formItemID, fm_formitem.itemName,
	  		 fm_formitem.itemLabel, fm_formitem.fieldType
	  FROM fm_formitem JOIN fm_group ON fm_formitem.groupID=fm_group.groupID
	  WHERE fm_formitem.formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
			fm_formitem.fieldType<>'Text Only'		
	  ORDER BY fm_group.displaySeq, fm_formitem.displaySeq
      <cfif IsDefined("Arguments.numColumns")>
      LIMIT 0, #Arguments.numColumns-1#
      </cfif>
	</cfquery>
    
    <cfscript>
	  resultStruct.numColumns = qColumns.recordcount; // numColumns
	  resultStruct.columnNames = ArrayNew(1); // columnNames[]
	  resultStruct.formItemIDs = ArrayNew(1);
	  for (idx = 1; idx LTE qColumns.recordcount; idx = idx +1) {
		resultStruct.columnNames[idx] = qColumns.itemName[idx];
	    resultStruct.formItemIDs[idx] = qColumns.formItemID[idx];
	  }
	</cfscript>
		
	<cfquery name="qSubmittedData" datasource="#this.datasource#">
	  SELECT fm_submitteddata_saved.submissionID, fm_submitteddata_saved.formItemID, fm_submitteddata_saved.valueText
	  FROM fm_submitteddata_saved JOIN fm_submission_saved ON fm_submitteddata_saved.submissionID=fm_submission_saved.submissionID
	  WHERE fm_submitteddata_saved.formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
			<cfif IsDefined("Arguments.submissionID")>
            AND fm_submitteddata_saved.submissionID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.submissionID#">
            </cfif>
			<cfif qColumns.recordcount GT 0>
            AND fm_submitteddata_saved.formItemID IN (#ValueList(qColumns.formItemID)#)
            <cfelse>
            AND 1=-1 <!--- nothing will be SELECTed since no columns were SELECTed --->
            </cfif>
      ORDER BY fm_submitteddata_saved.submissionID
	</cfquery>
    
    <cfscript>
	  data = StructNew();
	  tempID=0;
	  for (dataIdx = 1; dataIdx LTE qSubmittedData.recordcount; dataIdx = dataIdx +1) {
	    submissionID=qSubmittedData.submissionID[dataIdx];
		formItemID=qSubmittedData.formItemID[dataIdx];
	    if (Compare(tempID, submissionID)) {
		  data["s#submissionID#"]=StructNew();
		}
		data["s#submissionID#"]["f#formItemID#"]=qSubmittedData.valueText[dataIdx];
		tempID=submissionID;
	  }
	
	  resultStruct.submissions = ArrayNew(1); // resultStruct.submissions[]
	  for (idx = 1; idx LTE qSubmissions.recordcount; idx = idx +1) {
	    resultStruct.submissions[idx] = StructNew();
	    resultStruct.submissions[idx].submissionID = qSubmissions.submissionID[idx];
	    resultStruct.submissions[idx].dateSubmitted = qSubmissions.dateSubmitted[idx];
	    resultStruct.submissions[idx].userEmail = qSubmissions.userEmail[idx];
	    resultStruct.submissions[idx].userFirstName = qSubmissions.userFirstName[idx];
	    resultStruct.submissions[idx].userLastName = qSubmissions.userLastName[idx];
		resultStruct.submissions[idx].emailForSavedData = qSubmissions.emailForSavedData[idx];
		resultStruct.submissions[idx].passwordForSavedData = qSubmissions.passwordForSavedData[idx];
		
		for (idx2 = 1; idx2 LTE qColumns.recordcount; idx2 = idx2 +1) {
		  if (IsDefined("data.s#qSubmissions.submissionID[idx]#.f#qColumns.formItemID[idx2]#")) {
		    resultStruct.submissions[idx]["#qColumns.formItemID[idx2]#"]=data["s#qSubmissions.submissionID[idx]#"]["f#qColumns.formItemID[idx2]#"];
		  } else {
	        resultStruct.submissions[idx]["#qColumns.formItemID[idx2]#"]="";
		  }
	    }
	  }
	</cfscript>
	
	<cfreturn resultStruct>
  </cffunction>
    
  <cffunction name="deleteSubmissions" access="public" output="no">
    <cfargument name="submissionIDList" type="string" required="yes">
	
	<cfif Not Compare(Arguments.submissionIDList,"")><cfreturn></cfif>
		
	<cfquery datasource="#this.datasource#">
	  DELETE FROM fm_submission
	  WHERE submissionID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.submissionIDList#" list="yes">)
	</cfquery>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM fm_submitteddata
	  WHERE submissionID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.submissionIDList#" list="yes">)
	</cfquery> 
  </cffunction>
  
  <cffunction name="getFormItemsInfoByFormID" access="public" output="no" returntype="query">
    <cfargument name="formID" required="yes" type="numeric">  
    
    <cfquery name="qItems" datasource="#this.datasource#">
	  SELECT fm_formitem.formItemID, fm_formitem.itemName, fm_formitem.fieldType
	  FROM fm_formitem JOIN fm_group ON
	  	   fm_formitem.groupID=fm_group.groupID
	  WHERE fm_formitem.formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
	  ORDER BY fm_group.displaySeq, fm_formitem.displaySeq
	</cfquery>  
    <cfreturn qItems>
  </cffunction>
  
  <cffunction name="getSubmissionBreakdown" access="public" output="no" returntype="struct">
    <cfargument name="formID" type="numeric" required="yes">
	<cfargument name="FROMDate" type="string" required="yes">
	<cfargument name="toDate" type="string" required="yes">
	<cfargument name="formItemIDList" type="string" required="yes">
    
	<cfset var resultStruct = StructNew()>
	<!--- ::
		resultStruct
			.formItemID
				.itemName
				.fieldType
				.numSubmissions
				.optionList
				.valueQuery	
	:: --->
	
	<cfquery name="qItemInfo" datasource="#this.datasource#">
	  SELECT formItemID, itemName, fieldType, optionList
	  FROM fm_formitem
	  WHERE formItemID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.formItemIDList#" list="yes">)
	</cfquery>
	
	<cfloop query="qItemInfo">
	  <cfset resultStruct[formItemID] = StructNew()>
	  <cfset resultStruct[formItemID].numSubmissions=0>
	  <cfset resultStruct[formItemID].itemName=itemName>
	  <cfset resultStruct[formItemID].fieldType=fieldType>
	  <cfset resultStruct[formItemID].optionList=optionList>
	  <cfset resultStruct[formItemID].valueQuery=QueryNew("value")> 		  
	</cfloop>
	
	<cfquery name="qSummaryData" datasource="#this.datasource#">
	  SELECT fm_submitteddata.formItemID, COUNT(fm_submitteddata.formItemID) AS numSubmissions
	  FROM fm_submission JOIN fm_submitteddata ON fm_submission.submissionID=fm_submitteddata.submissionID
	  WHERE fm_submission.formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
			fm_submission.dateSubmitted >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#DateAdd("d", 0, Arguments.FROMDate)#"> AND
	  		fm_submission.dateSubmitted < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#DateAdd("d", 1, Arguments.toDate)#"> AND
			fm_submitteddata.formItemID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.formItemIDList#" list="yes">)
	  GROUP BY fm_submitteddata.formItemID
	</cfquery>

	<cfloop query="qSummaryData">
	  <cfset resultStruct[formItemID].numSubmissions = qSummaryData.numSubmissions>
	  <cfif qSummaryData.numSubmissions GT 0>	    
			<cfquery name="qValues" datasource="#this.datasource#">
			  SELECT valueText			   
			  FROM (fm_submission JOIN fm_submitteddata ON fm_submission.submissionID=fm_submitteddata.submissionID) JOIN
					fm_formitem ON fm_submitteddata.formItemID=fm_formitem.formItemID
			  WHERE fm_submitteddata.formItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#formItemID#"> AND
  					fm_submission.dateSubmitted >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#DateAdd("d", 0, Arguments.FROMDate)#"> AND
			  		fm_submission.dateSubmitted < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#DateAdd("d", 1, Arguments.toDate)#">
			</cfquery>		  
	    <cfset resultStruct[formItemID].valueQuery=qValues>	  
	  </cfif>
	</cfloop>
	<cfreturn resultStruct>	
  </cffunction>
  
  <cffunction name="searchSubmissions" access="public" output="no" returntype="struct">
    <cfargument name="formID" type="numeric" required="yes">
	<cfargument name="keyword" type="string" required="yes">
	<cfargument name="formItemIDList" type="string" required="yes">
    <cfargument name="useDateRange" type="boolean" required="no" default="1">
    <cfargument name="FROMDate" type="string" required="yes">
    <cfargument name="toDate" type="string" required="yes">
    <cfargument name="pageNum" type="numeric" required="no" default="1">
	<cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    <cfargument name="submissionIDList" type="string" default="">
    
    <cfset var resultStruct = StructNew()>
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfset resultStruct.numAllSubmissions = 0>
	<cfset resultStruct.numDisplayedSubmissions = 0>
	
	<cfset Arguments.keyword=TRIM(Arguments.keyword)>
	
	<cfif ListLen(Arguments.formItemIDList) IS 0 OR LEN(TRIM(Arguments.keyword)) LT 3><cfreturn resultStruct></cfif>
	
	<cfquery name="qSubmissionIDs" datasource="#this.datasource#">
	  SELECT DISTINCT submissionID
	  FROM fm_submitteddata
	  WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
	  		formItemID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.formItemIDList#" list="yes">) AND
			valueText LIKE '%#Arguments.keyword#%'
	  ORDER BY submissionID			
	</cfquery>
	
	<cfif qSubmissionIDs.recordcount IS 0>
	  <cfreturn resultStruct>
	<cfelse>
	  <cfset Arguments.submissionIDList=ValueList(qSubmissionIDs.submissionID)>	
	  <cfset resultStruct = getSubmissions(argumentCollection=Arguments)>
	  
	  <cfreturn resultStruct>
	</cfif>  
  </cffunction>
  
  <cffunction name="generateCSV" access="public" output="no" returntype="string">
    <cfargument name="formID" type="numeric" required="yes">
	<cfargument name="FROMDate" type="string" required="yes">
	<cfargument name="toDate" type="string" required="yes">
	<cfargument name="formItemIDList" type="string" required="yes">
    <cfargument name="displayAll" type="boolean" default="1">
    <cfargument name="CSVDirectory" type="string" required="yes">
		
	<cfset submissionStruct=getSubmissions(argumentCollection=Arguments)>
    
    <cftry>
	  <cfdirectory action="list" directory="#Arguments.CSVDirectory#" name="allFiles">
	  <cfloop query="allFiles">
	    <cfif type IS "File">
	      <cffile action="delete" file="#Arguments.CSVDirectory#/#name#">
	    </cfif>
	  </cfloop>
	  <cfcatch></cfcatch>
    </cftry>
    
	<cfset fileName="form" & Arguments.formID & "_" & Replace(Arguments.FromDate, "/","-","ALL") & "_to_" & Replace(Arguments.toDate, "/","-","ALL") & ".csv">
	<cfset newline=Chr(13) & Chr(10)>
    <cfsavecontent variable="CSVFile"><cfoutput>Date Submitted,<cfloop index="column" FROM="1" to="#submissionStruct.numColumns#">"#Replace(submissionStruct.columnNames[column],'"','""','All')#",</cfloop>#newline#<cfloop index="recordIdx" FROM="1" to="#submissionStruct.numAllSubmissions#"><cfset dateSubmitted=submissionStruct.submissions[recordIdx].dateSubmitted>#DateFormat(dateSubmitted,"mm/dd/yyyy")# #TimeFormat(dateSubmitted, "h:mm:ss tt")#,<cfloop index="column" FROM="1" to="#submissionStruct.numColumns#">"#Replace(submissionStruct.submissions[recordIdx][submissionStruct.formItemIDs[column]],'"','""','All')#",</cfloop>#newline#</cfloop></cfoutput></cfsavecontent>
    
    <cffile action="write" file="#Arguments.CSVDirectory#/#fileName#" output="#CSVFile#" nameconflict="overwrite">
    <cfreturn fileName>
  </cffunction>
  
  <cffunction name="moveSubmissionToSavedTable" access="public" output="no" returntype="numeric">
    <cfargument name="submissionID" type="numeric" required="yes">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="emailForSavedData" type="string" required="yes">
    <cfargument name="passwordForSavedData" type="string" required="yes">
  
    <cfquery name="qSubmission" datasource="#this.datasource#">
      SELECT submissionID, formID, userEmail, userFirstName, userLastName, userIPAddress, dateSubmitted, dateLastModified
      FROM fm_submission
      WHERE submissionID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.submissionID#">
    </cfquery>

    <cfquery name="qSubmittedData" datasource="#this.datasource#">
      SELECT submittedDataID, formID, submissionID, formItemID, valueText
      FROM fm_submitteddata
      WHERE submissionID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.submissionID#">
    </cfquery>

    <cfif qSubmission.recordcount GT 0 AND qSubmittedData.recordcount GT 0>
        <cfquery datasource="#this.datasource#">
          INSERT INTO fm_submission_saved
            (formID, emailForSavedData, passwordForSavedData, userEmail,
             userFirstName, userLastName, userIPAddress, dateSubmitted, dateLastModified)
          VALUES (
            <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailForSavedData#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.passwordForSavedData#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#qSubmission.userEmail#">,            
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#qSubmission.userFirstName#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#qSubmission.userLastName#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#qSubmission.userIPAddress#">,
            <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
            <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#"> 
          )
        </cfquery>
        
        <cfquery name="qNewSubmissionID" datasource="#this.datasource#">
          SELECT MAX(submissionID) AS newSubmissionID
          FROM fm_submission_saved
        </cfquery>
        
        <cfloop query="qSubmittedData">
          <cfquery datasource="#this.datasource#">
            INSERT INTO fm_submitteddata_saved
              (formID, submissionID, formItemID, valueText)
            VALUES (
              <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">,
              <cfqueryparam cfsqltype="cf_sql_integer" value="#qNewSubmissionID.newSubmissionID#">,
              <cfqueryparam cfsqltype="cf_sql_integer" value="#formItemID#">,
              <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#valueText#">
            )
          </cfquery>
        </cfloop> 
        
        <cfset deleteSubmissions("#Arguments.submissionID#")>
        <cfreturn qNewSubmissionID.newSubmissionID>
    <cfelse>
        <cfreturn 0>
    </cfif>   
  </cffunction>
</cfcomponent>
