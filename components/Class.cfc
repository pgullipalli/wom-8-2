<cfcomponent displayname="Class" extends="Base" output="no">
  <cffunction name="getAllCategories" access="public" output="no" returntype="query">
    <cfquery name="qCats" datasource="#this.datasource#">
      SELECT categoryID, categoryName
      FROM cl_category
      ORDER BY displaySeq
    </cfquery>
    <cfreturn qCats>
  </cffunction>
  
  <cffunction name="getAllFeaturedCategories" access="public" output="no" returntype="query">
    <cfquery name="qCats" datasource="#this.datasource#">
      SELECT categoryID, categoryName
      FROM cl_category
      WHERE featured=1
      ORDER BY displaySeq
    </cfquery>
    <cfreturn qCats>
  </cffunction>
  
  <cffunction name="getFeatureClassByCategoryID" access="public" output="no" returntype="struct">
    <!--- return a most recent coming featured class of the that category; randomly return one if there are more than one --->
    <cfargument name="categoryID" type="numeric" required="yes">
    <cfset var resultStruct = StructNew()>
    <cfset resultStruct.courseID=0>
    <cfset resultStruct.courseTitle="">
    <cfset resultStruct.shortDescription="">
    <cfset resultStruct.classID=0>     
    
	<cfset currentTime = now()>
    <cfset classTime=DateAdd("h", 2, currentTime)>
    
    <cfquery name="qCourseIDs" datasource="#this.datasource#">
      SELECT DISTINCT cl_course.courseID
      FROM ((cl_course_category INNER JOIN cl_course ON
      		 cl_course_category.courseID=cl_course.courseID) INNER JOIN cl_class ON
             cl_course.courseID=cl_class.courseID) INNER JOIN cl_classinstance ON
             cl_class.classID=cl_classinstance.classID
      WHERE cl_course_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#"> AND
      		cl_course.featuredOnHomepage=1 AND
      		cl_course.published=1 AND
      		cl_class.published=1 AND
            cl_class.publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
            cl_class.unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
            cl_classinstance.startDateTime > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#classTime#">
    </cfquery>
    
    <cfif qCourseIDs.recordcount Is 0>
      <cfreturn resultStruct>
    <cfelseif qCourseIDs.recordcount GT 1>
      <cfset targetCourseID=qCourseIDs.courseID[RandRange(1,qCourseIDs.recordcount)]>
    <cfelse>
      <cfset targetCourseID=qCourseIDs.courseID[1]>
    </cfif>
    
    <cfquery name="qClass" datasource="#this.datasource#">
      SELECT cl_course.courseID, cl_course.courseTitle, cl_course.shortDescription, cl_class.classID, Min(cl_classinstance.startDateTime) AS startDateTime
      FROM ((cl_course_category INNER JOIN cl_course ON
      		 cl_course_category.courseID=cl_course.courseID) INNER JOIN cl_class ON
             cl_course.courseID=cl_class.courseID) INNER JOIN cl_classinstance ON
             cl_class.classID=cl_classinstance.classID
      WHERE cl_course.courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#targetCourseID#"> AND
      		cl_class.published=1 AND
            cl_class.publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
            cl_class.unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
      		cl_classinstance.startDateTime > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#classTime#">
      GROUP BY cl_class.classID
      ORDER BY cl_classinstance.startDateTime
    </cfquery>
    
    <cfset resultStruct.courseID=qClass.courseID>
    <cfset resultStruct.courseTitle=qClass.courseTitle>
    <cfset resultStruct.shortDescription=qClass.shortDescription>
    <cfset resultStruct.classID=qClass.classID>
    <cfset resultStruct.startDateTime=qClass.startDateTime>

    <cfreturn resultStruct>
  </cffunction>
  
  <!--- <cffunction name="getFeaturedUpComingClasses" access="public" output="no" returntype="query">
    <cfargument name="numClasses" type="numeric" default="2">    
	<cfset currentTime = now()>
    <cfset classTime=DateAdd("h", 2, currentTime)>
    
    <cfquery name="qClassIDs" datasource="#this.datasource#">
      SELECT DISTINCT cl_class.classID
      FROM (cl_course INNER JOIN cl_class ON
            cl_course.courseID=cl_class.courseID) INNER JOIN cl_classinstance ON
            cl_class.classID=cl_classinstance.classID
      WHERE cl_course.published=1 AND
      		cl_course.featuredOnHomepage=1 AND
      		cl_class.published=1 AND
            cl_class.publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
            cl_class.unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
            cl_classinstance.startDateTime > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#classTime#">
      ORDER BY cl_classinstance.startDateTime ASC
      LIMIT 0, #Arguments.numClasses#
    </cfquery>
    
    <cfif qClassIDs.recordcount Is 0>
      <cfreturn QueryNew("courseID")>    
    </cfif>
    
    <cfquery name="qClass" datasource="#this.datasource#">
        SELECT cl_course.courseID, cl_course.courseTitle, cl_course.courseThumbnail,
             cl_course.shortDescription, cl_class.classID, Min(cl_classinstance.startDateTime) AS startDateTime
        FROM (cl_course INNER JOIN cl_class ON
             cl_course.courseID=cl_class.courseID) INNER JOIN cl_classinstance ON
             cl_class.classID=cl_classinstance.classID
        WHERE cl_class.classID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qClassIDs.classID)#" list="yes">)
        GROUP BY cl_class.classID
        ORDER BY cl_classinstance.startDateTime
    </cfquery>
    
    <cfreturn qClass>
  </cffunction> --->
  
  <cffunction name="getFeaturedUpComingClasses" access="public" output="no" returntype="query">
    <cfargument name="numClasses" type="numeric" default="2">    
	<cfset currentTime = now()>
    <cfset classTime=DateAdd("h", 2, currentTime)>
    
	<!--- all featured upcoming class IDs --->
    <cfquery name="qClassIDs" datasource="#this.datasource#">
      SELECT DISTINCT cl_class.classID
      FROM (cl_course INNER JOIN cl_class ON
            cl_course.courseID=cl_class.courseID) INNER JOIN cl_classinstance ON
            cl_class.classID=cl_classinstance.classID
      WHERE cl_course.published=1 AND
      		cl_course.featuredOnHomepage=1 AND
      		cl_class.published=1 AND
            cl_class.publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
            cl_class.unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
            cl_classinstance.startDateTime > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#classTime#">
    </cfquery>
    
    <cfif qClassIDs.recordcount Is 0>
      <cfreturn QueryNew("courseID")>    
    </cfif>
    
    <cfset classIDList=ValueList(qClassIDs.classID)>
    
    <cfquery name="qCatIDs" datasource="#this.datasource#">
      SELECT DISTINCT categoryID
      FROM cl_course_category INNER JOIN cl_class ON
      	   cl_course_category.courseID=cl_class.courseID
      WHERE cl_class.classID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#classIDList#" list="yes">)
    </cfquery>
    
    <cfset categoryIDList=ValueList(qCatIDs.categoryID)>
    
    <cfset UT=CreateObject("component","Utility").init()>
    
    <cfif ListLen(categoryIDList) LT Arguments.numClasses>
      <!--- not enough different categories for #Arguments.numClasses# featured upcoming classes --->
      <cfset classIDList=UT.getRandomSubList(classIDList, Arguments.numClasses)>
      <cfquery name="qClass" datasource="#this.datasource#">
        SELECT cl_course.courseID, cl_course.courseTitle, cl_course.courseThumbnail,
             cl_course.shortDescription, cl_class.classID, Min(cl_classinstance.startDateTime) AS startDateTime
        FROM (cl_course INNER JOIN cl_class ON
             cl_course.courseID=cl_class.courseID) INNER JOIN cl_classinstance ON
             cl_class.classID=cl_classinstance.classID
        WHERE cl_class.classID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#classIDList#" list="yes">)
        GROUP BY cl_class.classID
        ORDER BY startDateTime
      </cfquery>
    <cfelse>
      <cfset categoryIDList=UT.getRandomSubList(categoryIDList, Arguments.numClasses)>
      <cfset finalClassIDList="">
      <cfloop index="categoryID" list="#categoryIDList#">
        <cfquery name="qClassID" datasource="#this.datasource#">
          SELECT cl_class.classID, Min(cl_classinstance.startDateTime) AS startDateTime
          FROM (cl_class INNER JOIN cl_classinstance ON
                cl_class.classID=cl_classinstance.classID) INNER JOIN cl_course_category ON
                cl_class.courseID=cl_course_category.courseID
          WHERE cl_class.classID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#classIDList#" list="yes">) AND
          		cl_course_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">
                <cfif Compare(finalClassIDList,"")>
                AND cl_class.classID NOT IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#finalClassIDList#" list="yes">)
                </cfif>
          GROUP BY cl_class.classID
          ORDER BY startDateTime
        </cfquery>
        <cfif qClassID.recordcount GT 0>
          <cfset finalClassIDList=ListAppend(finalClassIDList, qClassID.classID)>
        </cfif>
      </cfloop>
    
      <cfif Compare(finalClassIDList,"")>
        <cfquery name="qClass" datasource="#this.datasource#">
          SELECT cl_course.courseID, cl_course.courseTitle, cl_course.courseThumbnail,
               cl_course.shortDescription, cl_class.classID, Min(cl_classinstance.startDateTime) AS startDateTime
          FROM (cl_course INNER JOIN cl_class ON
               cl_course.courseID=cl_class.courseID) INNER JOIN cl_classinstance ON
               cl_class.classID=cl_classinstance.classID
          WHERE cl_class.classID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#finalClassIDList#" list="yes">)
          GROUP BY cl_class.classID
          ORDER BY startDateTime
        </cfquery>
        <cfreturn qClass>
      <cfelse>
        <cfreturn QueryNew("courseID")>
      </cfif>
    </cfif>  
  </cffunction>
    
  <cffunction name="getFeaturedClassOnLandingPage" access="public" output="no" returntype="query">
    <cfset currentTime = now()>
    
    <cfquery name="qClass" datasource="#this.datasource#">
      SELECT cl_course.courseID, cl_course.courseTitle, cl_course.shortDescription, Min(cl_classinstance.startDateTime) AS startDateTime
      FROM (cl_course INNER JOIN cl_class ON
             cl_course.courseID=cl_class.courseID) INNER JOIN cl_classinstance ON
             cl_class.classID=cl_classinstance.classID
      WHERE cl_course.featuredOnClassHomePage=1 AND
      		cl_course.published=1 AND
      		cl_class.published=1 AND
            cl_class.publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
            cl_class.unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">  AND
            cl_classinstance.startDateTime > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
      GROUP BY cl_class.courseID
      ORDER BY cl_classinstance.startDateTime
    </cfquery>

    <cfreturn qClass> 
  </cffunction>
  
  <cffunction name="getCategoriesByCourseID" access="public" output="no" returntype="query">
    <cfargument name="courseID" type="numeric" required="yes">
    
    <cfquery name="qCats" datasource="#this.datasource#">
      SELECT cl_category.categoryID, cl_category.categoryName
      FROM cl_category INNER JOIN cl_course_category ON
      	   cl_category.categoryID=cl_course_category.categoryID
      WHERE cl_course_category.courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
      ORDER BY cl_category.displaySeq
    </cfquery>
    <cfreturn qCats>
  </cffunction>
  
  <cffunction name="getCategoriesByServiceID" access="public" output="no" returntype="query">
    <cfargument name="serviceID" type="numeric" required="yes">
    
    <cfquery name="qCats" datasource="#this.datasource#">
      SELECT cl_category.categoryID, cl_category.categoryName
      FROM cl_category INNER JOIN cl_service_category ON
      	   cl_category.categoryID=cl_service_category.categoryID
      WHERE cl_service_category.serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
      ORDER BY cl_category.displaySeq
    </cfquery>
    <cfreturn qCats>
  </cffunction>
  
  <cffunction name="getCourseByID" access="public" output="no" returntype="query">
    <cfargument name="courseID" type="numeric" required="yes">
    <cfargument name="publishedOnly" type="boolean" default="1">
    
    <cfquery name="qCourse" datasource="#this.datasource#">
      SELECT * 
      FROM cl_course
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
       		<cfif Arguments.publishedOnly> AND
      		published=1
            </cfif>
    </cfquery>
    
    <cfreturn qCourse>
  </cffunction>
  
  <cffunction name="getCourseByClassID" access="public" output="no" returntype="query">
    <cfargument name="classID" type="numeric" required="yes">
    <cfargument name="publishedOnly" type="boolean" default="1">
    
    <cfquery name="qCourse" datasource="#this.datasource#">
      SELECT *
      FROM cl_course INNER JOIN cl_class ON cl_course.courseID=cl_class.courseID
      WHERE cl_class.classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
      		<cfif Arguments.publishedOnly> AND
      		cl_course.published=1
            </cfif>
    </cfquery>
    
    <cfreturn qCourse>
  </cffunction>
  
  <cffunction name="getServiceByID" access="public" output="no" returntype="query">
    <cfargument name="serviceID" type="numeric" required="yes">
    
    <cfquery name="qService" datasource="#this.datasource#">
      SELECT * 
      FROM cl_service
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#"> AND
      		published=1
    </cfquery>
    
    <cfreturn qService>
  </cffunction>
  
  <cffunction name="getClassesByCourseID" access="public" output="no" returntype="struct">
    <cfargument name="courseID" type="numeric" required="yes">
    <cfset var structResult=StructNew()>
    <!---
		.numClasses
		.class[]
			.classID
			.regLimit
			.regMaximum
			.notes
			.numClassInstances
			.classInstance[]
				.startDateTime
				.endDateTime
				.dayOfWeekAsNumber
				.locationID
				.locationName
				.mapVerified
			.numParticipants
	--->
    
    <cfset currentTime = now()>
    <!--- select all classes that still have class instances available --->
    <cfquery name="qClassIDs" datasource="#this.datasource#">
      SELECT DISTINCT cl_class.classID
      FROM cl_class INNER JOIN cl_classinstance ON
      	   cl_class.classID=cl_classinstance.classID
      WHERE cl_class.courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#"> AND
      		cl_class.published=1 AND
            cl_class.publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
            cl_class.unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
            cl_classinstance.startDateTime > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
    </cfquery>
    
    <cfif qClassIDs.recordcount Is 0>
      <cfset structResult.numClasses=0>
      <cfreturn structResult>
    <cfelse>
      <cfquery name="qClasses" datasource="#this.datasource#">
        SELECT cl_class.classID, cl_class.regLimit, cl_class.regMaximum, cl_class.notes, cl_classinstance.startDateTime, cl_classinstance.endDateTime, cl_classinstance.dayOfWeekAsNumber,
        	   cl_classinstance.locationID, cl_location.locationName, cl_location.mapID, cl_location.posX AS mapVerified <!--- if posX != 0 then verified --->
        FROM (cl_class INNER JOIN cl_classinstance ON
        	  cl_class.classID=cl_classinstance.classID) LEFT JOIN cl_location ON
              cl_classinstance.locationID=cl_location.locationID
        WHERE cl_class.classID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qClassIDs.classID)#" list="yes">)
      </cfquery>
      
      <cfquery name="qClassOrder" datasource="#this.datasource#">
        SELECT cl_class.classID, MIN(cl_classinstance.startDateTime) AS classStartTime
        FROM cl_class INNER JOIN cl_classinstance ON cl_class.classID=cl_classinstance.classID
        WHERE cl_class.classID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qClassIDs.classID)#" list="yes">)
        GROUP BY cl_class.classID
        ORDER BY classStartTime
      </cfquery>  
      
      <cfset structResult.numClasses=qClassOrder.recordcount>
      <cfset structResult.class=ArrayNew(1)>
      <cfset idx1 = 0>      
      <cfloop query="qClassOrder">
        <cfset idx1 = idx1 + 1>
        <cfset structResult.class[idx1]=StructNew()>
        <cfset structResult.class[idx1].classID=classID>        
        <cfquery name="qClassInstance" dbtype="query">
          SELECT *
          FROM qClasses
          WHERE classID=#classID#
          ORDER BY startDateTime
        </cfquery>
        
        <cfset structResult.class[idx1].regLimit=qClassInstance.regLimit[1]>
        <cfset structResult.class[idx1].regMaximum=qClassInstance.regMaximum[1]>
        
        <cfset structResult.class[idx1].notes=qClassInstance.notes[1]>
        <cfset structResult.class[idx1].numClassInstances=qClassInstance.recordcount>
        <cfset structResult.class[idx1].classInstance=ArrayNew(1)>
        <cfloop index="idx2" from="1" to="#qClassInstance.recordcount#">
          <cfset structResult.class[idx1].classInstance[idx2]=StructNew()>
          <cfset structResult.class[idx1].classInstance[idx2].startDateTime=qClassInstance.startDateTime[idx2]>
          <cfset structResult.class[idx1].classInstance[idx2].endDateTime=qClassInstance.endDateTime[idx2]>
          <cfset structResult.class[idx1].classInstance[idx2].dayOfWeekAsNumber=qClassInstance.dayOfWeekAsNumber[idx2]>
          <cfset structResult.class[idx1].classInstance[idx2].locationID=qClassInstance.locationID[idx2]>
          <cfset structResult.class[idx1].classInstance[idx2].locationName=qClassInstance.locationName[idx2]>
          <cfset structResult.class[idx1].classInstance[idx2].mapID=qClassInstance.mapID[idx2]>
          <cfif IsNumeric(qClassInstance.mapVerified[idx2])>
            <cfset structResult.class[idx1].classInstance[idx2].mapVerified=qClassInstance.mapVerified[idx2]>
          <cfelse>
            <cfset structResult.class[idx1].classInstance[idx2].mapVerified=0>
          </cfif>
        </cfloop>
        
        <cfquery name="qParticipants" datasource="#this.datasource#">
      	  SELECT COUNT(studentID) AS numParticipants 
          FROM cl_student_class        
          WHERE classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#classID#">
        </cfquery>
        <cfset structResult.class[idx1].numParticipants=qParticipants.numParticipants> 
      </cfloop>
      
      <cfreturn structResult>      
  	</cfif>  
  </cffunction>
  
  <cffunction name="getParticipantLimit" access="public" output="no" returntype="query">
    <cfargument name="courseID" type="numeric" required="yes">
    
    <cfquery name="qLimit" datasource="#this.datasource#">
      SELECT MIN(regLimit) AS regLimitMin, MAX(regLimit) AS regLimitMax
      FROM cl_class
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
    <cfreturn qLimit>
  </cffunction>
  
  <cffunction name="getRandomCourseReview" access="public" output="no" returntype="string">
    <cfargument name="courseID" type="numeric" required="yes">
    
    <cfquery name="qReviews" datasource="#this.datasource#">
      SELECT review
      FROM cl_coursereview
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#"> AND
      		published=1
    </cfquery>
    
    <cfif qReviews.recordcount GT 0>
      <cfif qReviews.recordcount GT 1>
        <cfreturn qReviews.review[RandRange(1,qReviews.recordcount)]>
      <cfelse>
        <cfreturn qReviews.review>
      </cfif>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="getRandomServiceReview" access="public" output="no" returntype="string">
    <cfargument name="serviceID" type="numeric" required="yes">
    
    <cfquery name="qReviews" datasource="#this.datasource#">
      SELECT review
      FROM cl_servicereview
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#"> AND
      		published=1
    </cfquery>
    
    <cfif qReviews.recordcount GT 0>
      <cfif qReviews.recordcount GT 1>
        <cfreturn qReviews.review[RandRange(1,qReviews.recordcount)]>
      <cfelse>
        <cfreturn qReviews.review>
      </cfif>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="getReviewsByCourseID" access="public" output="no" returntype="struct">
    <cfargument name="courseID" required="yes" type="numeric">
	<cfargument name="startIndex" required="no" default="1" type="numeric">
	<cfargument name="numItemsPerPage" required="no" default="20" type="numeric">
    
	<cfset var resultStruct = StructNew()>
    
    <cfquery name="qNumReviews" datasource="#this.datasource#">
      SELECT COUNT(courseReviewID) AS numReviews
      FROM cl_coursereview
      WHERE published=1 AND
            courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
    <cfset resultStruct.numAllItems = qNumReviews.numReviews>
    
    <cfquery name="qReviews" datasource="#this.datasource#">
      SELECT fullName, review
      FROM cl_coursereview
      WHERE published=1 AND
            courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
      ORDER BY dateCreated DESC
      LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
    </cfquery>

	<cfset resultStruct.numDisplayedItems=qReviews.recordcount>	
    <cfset resultStruct.reviews = qReviews>
    
    <cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="getReviewsByServiceID" access="public" output="no" returntype="struct">
    <cfargument name="serviceID" required="yes" type="numeric">
	<cfargument name="startIndex" required="no" default="1" type="numeric">
	<cfargument name="numItemsPerPage" required="no" default="20" type="numeric">
    
	<cfset var resultStruct = StructNew()>
    
    <cfquery name="qNumReviews" datasource="#this.datasource#">
      SELECT COUNT(serviceReviewID) AS numReviews
      FROM cl_servicereview
      WHERE published=1 AND
            serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
    </cfquery>
    <cfset resultStruct.numAllItems = qNumReviews.numReviews>
    
    <cfquery name="qReviews" datasource="#this.datasource#">
      SELECT fullName, review
      FROM cl_servicereview
      WHERE published=1 AND
            serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
      ORDER BY dateCreated DESC
      LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
    </cfquery>

	<cfset resultStruct.numDisplayedItems=qReviews.recordcount>	
    <cfset resultStruct.reviews = qReviews>
    
    <cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="getPrerequisitesByCourseID" access="public" output="no" returntype="query">
    <cfargument name="courseID" type="numeric" required="yes">
    
    <cfquery name="qPrerequisites" datasource="#this.datasource#">
      SELECT cl_course.courseID, cl_course.courseTitle
      FROM cl_course_course_prerequired INNER JOIN cl_course ON
      	   cl_course_course_prerequired.courseID2=cl_course.courseID
      WHERE cl_course_course_prerequired.courseID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#"> AND
      		cl_course.published=1
    </cfquery>
  
    <cfreturn qPrerequisites>
  </cffunction>
  
  <cffunction name="getAssociatedCoursesByCourseID" access="public" output="no" returntype="query">
    <cfargument name="courseID" type="numeric" required="yes">
    
    <cfquery name="qAssociatedCourses" datasource="#this.datasource#">
      SELECT cl_course.courseID, cl_course.courseTitle
      FROM cl_course_course_associated INNER JOIN cl_course ON
      	   cl_course_course_associated.courseID2=cl_course.courseID
      WHERE cl_course_course_associated.courseID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#"> AND
      		cl_course.published=1
    </cfquery>
  
    <cfreturn qAssociatedCourses>
  </cffunction>
  
  <cffunction name="getAssociatedCoursesByServiceID" access="public" output="no" returntype="query">
    <cfargument name="serviceID" type="numeric" required="yes">
    
    <cfquery name="qAssociatedCourses" datasource="#this.datasource#">
      SELECT cl_course.courseID, cl_course.courseTitle
      FROM cl_service_course_associated INNER JOIN cl_course ON
      	   cl_service_course_associated.courseID=cl_course.courseID
      WHERE cl_service_course_associated.serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#"> AND
      		cl_course.published=1
    </cfquery>
  
    <cfreturn qAssociatedCourses>
  </cffunction>
  
  <cffunction name="getCategoryName" access="public" output="no" returntype="string">
    <cfargument name="categoryID" type="numeric" required="yes">
    
    <cfquery name="qCategoryName" datasource="#this.datasource#">
      SELECT categoryName
      FROM cl_category
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
    </cfquery>
    <cfif qCategoryName.recordcount GT 0>
      <cfreturn qCategoryName.categoryName>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="getCoursesByCategoryID" access="public" output="no" returntype="struct">
    <cfargument name="categoryID" required="yes" type="numeric">
	<cfargument name="startIndex" required="no" default="1" type="numeric">
	<cfargument name="numItemsPerPage" required="no" default="20" type="numeric">
    
	<cfset var resultStruct = StructNew()>
    
    <cfquery name="qAllCourseID" datasource="#this.datasource#">
      SELECT DISTINCT cl_course.courseID
      FROM (cl_course INNER JOIN cl_course_category ON cl_course.courseID=cl_course_category.courseID) INNER JOIN cl_class ON
      		cl_course_category.courseID=cl_class.courseID 
      WHERE cl_course.published=1 AND
      		cl_course_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
    </cfquery>  
    
    <cfset resultStruct.numAllItems = qAllCourseID.recordcount>
  
    <cfif qAllCourseID.recordcount GT 0>
      <cfquery name="qCourses" datasource="#this.datasource#">
        SELECT courseID, courseTitle, shortDescription
        FROM cl_course
        WHERE courseID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qAllCourseID.courseID)#" list="yes">)
        ORDER BY courseTitle
        LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
      </cfquery>
      <cfset resultStruct.numDisplayedItems=qCourses.recordcount>	
      <cfset resultStruct.courses = qCourses>   
    <cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.courses = QueryNew("courseID,courseTitle,shortDescription")>   
    </cfif>
    
    <cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="getCoursesAndSerivcesByCategoryID" access="public" output="no" returntype="struct">
    <cfargument name="categoryID" required="yes" type="numeric">
	<cfargument name="startIndex" required="no" default="1" type="numeric">
	<cfargument name="numItemsPerPage" required="no" default="20" type="numeric">
    
	<cfset var resultStruct = StructNew()>
    
    <cfquery name="qAllCourseIDs" datasource="#this.datasource#">
      SELECT DISTINCT cl_course.courseID
      FROM (cl_course INNER JOIN cl_course_category ON cl_course.courseID=cl_course_category.courseID) INNER JOIN cl_class ON
      		cl_course_category.courseID=cl_class.courseID 
      WHERE cl_course.published=1 AND
      		cl_course_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
    </cfquery>
    
    <cfquery name="qAllServiceIDs" datasource="#this.datasource#">
      SELECT DISTINCT cl_service.serviceID
      FROM cl_service INNER JOIN cl_service_category ON cl_service.serviceID=cl_service_category.serviceID
      WHERE cl_service.published=1 AND
      		cl_service_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
    </cfquery>  
    
    <cfset resultStruct.numAllItems = qAllCourseIDs.recordcount + qAllServiceIDs.recordcount>
  
    <cfif qAllCourseIDs.recordcount GT 0 OR qAllServiceIDs.recordcount GT 0>
      <cfif qAllCourseIDs.recordcount GT 0>
        <cfset courseIDList=ValueList(qAllCourseIDs.courseID)>
      <cfelse>
        <cfset courseIDList="0">
      </cfif>
      <cfif qAllServiceIDs.recordcount GT 0>
        <cfset serviceIDList=ValueList(qAllServiceIDs.serviceID)>
      <cfelse>
        <cfset serviceIDList="0">
      </cfif> 
    
      <cfquery name="qCoursesAndServices" datasource="#this.datasource#">
         SELECT courseID AS ID, courseTitle AS title, 'course' AS type, shortDescription AS description
         FROM cl_course
         WHERE courseID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#courseIDList#" list="yes">)
        
         UNION
        
         SELECT serviceID AS ID, serviceTitle AS title, 'service' AS type, shortDescription AS description
         FROM cl_service
         WHERE serviceID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#serviceIDList#" list="yes">)
                 
         ORDER BY title    
         LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
      </cfquery>
      <cfset resultStruct.numDisplayedItems=qCoursesAndServices.recordcount>	
      <cfset resultStruct.coursesAndServices = qCoursesAndServices>   
    <cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.coursesAndServices = QueryNew("ID,title,description")>   
    </cfif>
    
    <cfreturn resultStruct>
  </cffunction>
  
  <!--- <cffunction name="getClassCategorySummary" access="public" output="no" returntype="struct">
    <cfset var currentTime=now()>
    <cfset var courseIDList="0">
    <cfset var idx = 0>
    <cfset var resultStruct=StructNew()>
    <!---
		resultStruct
			.category[]
				.categoryID
				.categoryName
				.course[]
					.courseID
					.courseTitle
					.shortDescription	
	--->
    
    <cfquery name="qAllCatIDs" datasource="#this.datasource#">
      SELECT DISTINCT cl_course_category.categoryID
      FROM cl_course_category INNER JOIN cl_course ON cl_course_category.courseID=cl_course.courseID
      WHERE cl_course.published=1
    </cfquery>   
    
    <cfquery name="qAllCourseIDs" datasource="#this.datasource#">
      SELECT DISTINCT cl_course_category.categoryID, cl_course.courseID
      FROM ((cl_course INNER JOIN cl_course_category ON cl_course.courseID=cl_course_category.courseID) INNER JOIN cl_class ON
      		cl_course.courseID=cl_class.courseID) INNER JOIN view_cl_classinstance_summary ON
            cl_class.classID=view_cl_classinstance_summary.classID
      WHERE cl_course_category.categoryID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qAllCatIDs.categoryID)#" list="yes">) AND
      		cl_course.published=1 AND
            view_cl_classinstance_summary.lastStartDateTime > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
      ORDER BY cl_course_category.categoryID, cl_course.courseID
    </cfquery>
    
    <cfquery name="qCatSummary" datasource="#this.datasource#">
      SELECT cl_category.displaySeq, cl_category.categoryID, cl_category.categoryName, cl_course.courseID, cl_course.courseTitle, cl_course.shortDescription
      FROM (cl_category INNER JOIN cl_course_category ON cl_category.categoryID=cl_course_category.categoryID) INNER JOIN cl_course ON
      		cl_course_category.courseID=cl_course.courseID
      WHERE cl_course.courseID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qAllCourseIDs.courseID)#" list="yes">) AND
      		cl_category.categoryID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qAllCatIDs.categoryID)#" list="yes">)
    </cfquery>
    
    <cfquery name="qCatSummary" dbtype="query">
      SELECT qCatSummary.displaySeq, qCatSummary.categoryID, qCatSummary.categoryName, qCatSummary.courseID, qCatSummary.courseTitle, qCatSummary.shortDescription
      FROM qCatSummary, qAllCourseIDs
      WHERE qCatSummary.categoryID=qAllCourseIDs.categoryID AND qCatSummary.courseID=qAllCourseIDs.courseID
      ORDER BY qCatSummary.displaySeq, qCatSummary.categoryID, qCatSummary.courseTitle
    </cfquery>
    
    <cfset tempCatID="0">
    <cfset resultStruct.category=ArrayNew(1)>    
    <cfloop query="qCatSummary">
      <cfif Compare(tempCatID, categoryID)>
        <cfset idx = idx + 1>
        <cfset resultStruct.category[idx]=StructNew()>
        <cfset resultStruct.category[idx].categoryID=categoryID>
        <cfset resultStruct.category[idx].categoryName=categoryName>
        <cfset resultStruct.category[idx].course=ArrayNew(1)>        
      </cfif>
      <cfset courseIdx=ArrayLen(resultStruct.category[idx].course)+1>
      <cfif courseIdx LTE 2>
        <cfset resultStruct.category[idx].course[courseIdx]=StructNew()>
        <cfset resultStruct.category[idx].course[courseIdx].courseID=courseID>
        <cfset resultStruct.category[idx].course[courseIdx].courseTitle=courseTitle>
        <cfset resultStruct.category[idx].course[courseIdx].shortDescription=shortDescription>    
      </cfif>
      <cfset tempCatID=categoryID>
    </cfloop>
    
    <cfreturn resultStruct>
  </cffunction> --->
  
  <cffunction name="getClassCategorySummary" access="public" output="no" returntype="struct">
    <cfset var currentTime=now()>
    <cfset var courseIDList="0">
    <cfset var idx = 0>
    <cfset var resultStruct=StructNew()>
    <!---
		resultStruct
			.category[]
				.categoryID
				.categoryName
				.course[]
					.courseID (also used for serviceID)
					.courseTitle
					.classType
					.shortDescription	
	--->
    <cfquery name="qAllCategories" datasource="#this.datasource#">
      SELECT categoryID, categoryName
      FROM cl_category
      WHERE featured=1
      ORDER BY displaySeq
    </cfquery>
    
    <cfif qAllCategories.recordcount GT 0>
      <cfset allCatIDList=ValueList(qAllCategories.categoryID)>
    <cfelse>
      <cfset allCatIDList="0">
    </cfif>
    
    <cfquery name="qAllCatIDs" datasource="#this.datasource#">
      SELECT DISTINCT cl_course_category.categoryID
      FROM cl_course_category INNER JOIN cl_course ON cl_course_category.courseID=cl_course.courseID
      WHERE cl_course.published=1 AND cl_course_category.categoryID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#allCatIDList#" list="yes">)
    </cfquery>   
    
    <cfquery name="qAllCourseIDs" datasource="#this.datasource#">
      SELECT DISTINCT cl_course_category.categoryID, cl_course.courseID
      FROM ((cl_course INNER JOIN cl_course_category ON cl_course.courseID=cl_course_category.courseID) INNER JOIN cl_class ON
      		cl_course.courseID=cl_class.courseID) INNER JOIN view_cl_classinstance_summary ON
            cl_class.classID=view_cl_classinstance_summary.classID
      WHERE cl_course_category.categoryID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qAllCatIDs.categoryID)#" list="yes">) AND
      		cl_course.published=1 AND
            view_cl_classinstance_summary.lastStartDateTime > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
      ORDER BY cl_course_category.categoryID, cl_course.courseID
    </cfquery>
    
    <cfquery name="qCatSummary" datasource="#this.datasource#">
      SELECT cl_category.displaySeq, cl_category.categoryID, cl_category.categoryName, cl_course.courseID, cl_course.courseTitle, cl_course.shortDescription
      FROM (cl_category INNER JOIN cl_course_category ON cl_category.categoryID=cl_course_category.categoryID) INNER JOIN cl_course ON
      		cl_course_category.courseID=cl_course.courseID
      WHERE cl_course.courseID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qAllCourseIDs.courseID)#" list="yes">) AND
      		cl_category.categoryID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qAllCatIDs.categoryID)#" list="yes">)
    </cfquery>
    
    <cfquery name="qCatSummary" dbtype="query">
      SELECT qCatSummary.displaySeq, qCatSummary.categoryID, qCatSummary.categoryName, qCatSummary.courseID, qCatSummary.courseTitle, qCatSummary.shortDescription
      FROM qCatSummary, qAllCourseIDs
      WHERE qCatSummary.categoryID=qAllCourseIDs.categoryID AND qCatSummary.courseID=qAllCourseIDs.courseID
      ORDER BY qCatSummary.displaySeq, qCatSummary.categoryID, qCatSummary.courseTitle
    </cfquery>
    
    <cfset resultStruct.category=ArrayNew(1)>  
    <cfif qCatSummary.recordcount GT 0>
      <cfset selectedCategoryIDList=ValueList(qCatSummary.categoryID)>
      <cfloop query="qAllCategories">
        <cfset idx = idx + 1>
        <cfset resultStruct.category[idx]=StructNew()>
        <cfset resultStruct.category[idx].categoryID=categoryID>
        <cfset resultStruct.category[idx].categoryName=categoryName>
        <cfset resultStruct.category[idx].course=ArrayNew(1)>
        <cfset resultStruct.category[idx].service=ArrayNew(1)>
        <cfif ListFind(selectedCategoryIDList, categoryID) GT 0><!--- has courses --->
          <cfset categoryIDTemp=categoryID>
          <cfset courseIdx=0>
          <cfloop query="qCatSummary">
    		<cfif categoryIDTemp Is categoryID>
              <cfset courseIdx=courseIdx + 1>
              <cfif courseIdx GT 2>
                <cfbreak>
              <cfelse>
                <cfset resultStruct.category[idx].course[courseIdx]=StructNew()>
				<cfset resultStruct.category[idx].course[courseIdx].courseID=courseID>
                <cfset resultStruct.category[idx].course[courseIdx].courseTitle=courseTitle>
                <cfset resultStruct.category[idx].course[courseIdx].shortDescription=shortDescription>
              </cfif>
            </cfif>
      	  </cfloop>
        <cfelse>
          <cfquery name="qServices" datasource="#this.datasource#" maxrows="2">
            SELECT cl_service.serviceID, cl_service.serviceTitle, cl_service.shortDescription
            FROM cl_service_category INNER JOIN cl_service ON cl_service_category.serviceID=cl_service.serviceID
            WHERE cl_service_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#"> AND
            	  cl_service.published=1
            ORDER BY cl_service.serviceTitle
          </cfquery>
          <cfset serviceIdx=0>
          <cfloop query="qServices">
            <cfset serviceIdx=serviceIdx + 1>
            <cfset resultStruct.category[idx].service[serviceIdx]=StructNew()>
			<cfset resultStruct.category[idx].service[serviceIdx].serviceID=serviceID>
            <cfset resultStruct.category[idx].service[serviceIdx].serviceTitle=serviceTitle>
            <cfset resultStruct.category[idx].service[serviceIdx].shortDescription=shortDescription>
          </cfloop>
        </cfif>
      </cfloop>
    </cfif>
    
    <cfreturn resultStruct>
  </cffunction>

  <cffunction name="getClassInstance" access="public" output="no" returntype="query">
    <cfargument name="classInstanceID" type="numeric" required="yes">
    
    <cfquery name="qClassInstance" datasource="#this.datasource#">
      SELECT cl_course.courseID, cl_course.courseTitle, cl_classinstance.startDateTime, cl_classinstance.endDateTime,
      		 cl_course.contactName, cl_course.contactEmail, cl_course.contactPhone, cl_location.locationName
      FROM ((cl_course INNER JOIN cl_class ON cl_course.courseID=cl_class.courseID) INNER JOIN cl_classinstance ON
      		cl_class.classID=cl_classinstance.classID) LEFT JOIN cl_location ON
            cl_classinstance.locationID=cl_location.locationID
      WHERE cl_classinstance.classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">
    </cfquery>
  
    <cfreturn qClassInstance>
  </cffunction>
  
  <cffunction name="getIntroductionText" access="public" output="no" returntype="string">
    <cfargument name="sectionCode" type="string" required="yes">
    <cfquery name="qText" datasource="#this.datasource#">
  	  SELECT introductionText
      FROM cl_introductory_copy
      WHERE sectionCode=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sectionCode#">
    </cfquery>
    
    <cfif qText.recordcount GT 0>
      <cfreturn qText.introductionText>
    <cfelse>      
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="getAllCurriculum" access="public" output="no" returntype="query">
    <cfquery name="qCurriculum" datasource="#this.datasource#">
      SELECT curriculaID, curriculaTitle, shortDescription
      FROM cl_curricula
      WHERE published=1
      ORDER BY curriculaTitle
    </cfquery>
    
    <cfreturn qCurriculum>
  </cffunction>
  
  <cffunction name="getCurriculaByID" access="public" output="no" returntype="query">
    <cfargument name="curriculaID" type="numeric" required="yes">
    
    <cfquery name="qCurricula" datasource="#this.datasource#">
      SELECT *
      FROM cl_curricula
      WHERE curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#"> AND
      		published=1
    </cfquery>
    
    <cfreturn qCurricula>
  </cffunction>
  
  <cffunction name="getCurriculaCourses" access="public" output="no" returntype="query">
    <cfargument name="curriculaID" type="numeric" required="yes">
    
    <cfquery name="qCourses" datasource="#this.datasource#">
      SELECT cl_course.courseID, cl_course.courseTitle, cl_course.registerOnline, cl_course.fitnessClubOnly,
      		 cl_course.regLimit, cl_course.regMaximum, cl_course.feeRegular, cl_course.feeEmployee, cl_course.feeDoctor, cl_course.feeFitnessClub,
             cl_course.registrationURL, cl_course.feePerCouple, cl_course.callForPrice
      FROM cl_course INNER JOIN cl_course_curricula ON cl_course.courseID=cl_course_curricula.courseID
      WHERE cl_course_curricula.curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#">
      ORDER BY cl_course.courseTitle
    </cfquery>
    
    <cfreturn qCourses>
  </cffunction>
  
  <cffunction name="getCurriculaServices" access="public" output="no" returntype="query">
    <cfargument name="curriculaID" type="numeric" required="yes">
    
    <cfquery name="qServices" datasource="#this.datasource#">
      SELECT cl_service.serviceID, cl_service.serviceTitle
      FROM cl_service INNER JOIN cl_service_curricula ON cl_service.serviceID=cl_service_curricula.serviceID
      WHERE cl_service_curricula.curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#">
      ORDER BY cl_service.serviceTitle
    </cfquery>
  
    <cfreturn qServices>
  </cffunction>  
  
  <cffunction name="getCategoryIDByCourseID" access="public" output="no" returntype="numeric">
    <cfargument name="courseID" type="numeric" required="yes">
    
    <cfquery name="qCatID" datasource="#this.datasource#">
      SELECT categoryID
      FROM cl_course_category
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
  
    <cfif qCatID.recordcount GT 0>
      <cfreturn qCatID.categoryID>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="getCategoryIDByServiceID" access="public" output="no" returntype="numeric">
    <cfargument name="serviceID" type="numeric" required="yes">
    
    <cfquery name="qCatID" datasource="#this.datasource#">
      SELECT categoryID
      FROM cl_service_category
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
    </cfquery>
  
    <cfif qCatID.recordcount GT 0>
      <cfreturn qCatID.categoryID>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="submitReview" access="public" output="no">
    <cfargument name="studentID" type="numeric" required="yes">
    <cfargument name="classType" type="string" required="yes">
    <cfargument name="serviceID" type="numeric" required="no">
    <cfargument name="classID" type="numeric" required="no">
    <cfargument name="className" type="string" required="yes">
    <cfargument name="fullName" type="string" required="yes">
    <cfargument name="feedback" type="string" required="yes">    
    
    <cfif Arguments.classType Is "course">
      <cfquery datasource="#this.datasource#">
        UPDATE cl_student_class
        SET feedbackSent=1
        WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#"> AND
        	  classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
      </cfquery>
    <cfelse>
      <cfquery datasource="#this.datasource#">
        UPDATE cl_student_service
        SET feedbackSent=1
        WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#"> AND
        	  serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
      </cfquery>
    </cfif>
    
    <cfsavecontent variable="reviewText">
      <cfoutput>
<cfif Arguments.classType IS "course">Course: #Arguments.className#
<cfelse>Service: #Arguments.className#</cfif>

Name: #Arguments.fullName#

PROGRAM CONTENT

For each of the following questions please check the response that best matches your opinion.
#Arguments.programContentMetExpectation#

The handouts and presentation aids were valuable.
#Arguments.programContentValuable#

PRESENTER

The presenter(s) was easy to follow and understand.
#Arguments.presenterUnderstandable#

The presenter(s) was knowledgable.
#Arguments.presenterKnowledgable#

OVERALL

I was satisfied with the program.
#Arguments.overallSatisfied#

I would recommend this program to a friend.
#Arguments.overallRecommendToFriends#

ADDITIONAL COMMENTS

#Arguments.feedback#
      </cfoutput>
    </cfsavecontent>
    
    <cfsavecontent variable="reviewHTML">
      <cfoutput>
      <div style="font-family:Arial; font-size:12px; color:##666666;">
        <cfif Arguments.classType IS "course">
          <span style="color:##009F9F;"><b>Course: #Arguments.className#</b></span><br /><br />
        <cfelse>
          <span style="color:##009F9F;"><b>Service: #Arguments.className#</b></span><br /><br />
	    </cfif>
        
        Name: <span style="color::##FF3399">#Arguments.fullName#</span><br /><br />

        PROGRAM CONTENT<br /><br />
        
        For each of the following questions please check the response that best matches your opinion.<br />
        <span style="color:##FF3399">#Arguments.programContentMetExpectation#</span><br /><br />
        
        The handouts and presentation aids were valuable.<br />
        <span style="color:##FF3399">#Arguments.programContentValuable#</span><br /><br />
        
        PRESENTER
        
        The presenter(s) was easy to follow and understand.<br />
        <span style="color:##FF3399">#Arguments.presenterUnderstandable#</span><br /><br />
        
        The presenter(s) was knowledgable.<br />
        <span style="color:##FF3399">#Arguments.presenterKnowledgable#</span><br /><br />
        
        OVERALL
        
        I was satisfied with the program.<br />
        <span style="color:##FF3399">#Arguments.overallSatisfied#</span><br /><br />
        
        I would recommend this program to a friend.<br />
        <span style="color:##FF3399">#Arguments.overallRecommendToFriends#</span><br /><br />
        
        ADDITIONAL COMMENTS<br /><br />
        
        <span style="color:##FF3399">#Replace(Arguments.feedback, Chr(10), "<br />", "All")#</span>
      </div>
      </cfoutput>
    </cfsavecontent>
    
    <cfquery name="qEmailSetting" datasource="#this.datasource#">
      SELECT *
      FROM cl_emailsettings
      WHERE emailCode='feedback_submission'
    </cfquery>
    
    <cfset finalEmailBodyText=ReplaceNoCase(qEmailSetting.emailBody, "$feedback", "#reviewText#", "All")>
    <cfset finalEmailBodyHTML=ReplaceNoCase(Replace(qEmailSetting.emailBody,Chr(13) & Chr(10), "<br />", "All"), "$feedback", "#reviewHTML#", "All")>
    <cfif Compare(qEmailSetting.replyName,"")>
      <cfset fromAddress="#qEmailSetting.replyName#<#qEmailSetting.replyAddress#>">
    <cfelse>
      <cfset fromAddress="#qEmailSetting.replyAddress#">
    </cfif>
    
    <cfmail from="#fromAddress#" to="#qEmailSetting.recipientAddresses#" subject="#qEmailSetting.emailSubject#" type="html">
      <cfmailpart type="text">
#Trim(finalEmailBodyText)#
      </cfmailpart>        
      <cfmailpart type="html" charset="utf-8">
        <div style="font-family:Arial; font-size:12px;">
		#finalEmailBodyHTML#
        </div>
      </cfmailpart>
    </cfmail>
  </cffunction>
  
  <cffunction name="getLocation" access="public" output="no" returntype="query">
    <cfargument name="locationID" required="yes" type="numeric">	
	<cfquery name="qLocation" datasource="#this.datasource#">
	  SELECT *
      FROM cl_location
      WHERE locationID=<cfqueryparam value="#Arguments.locationID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
	<cfreturn qLocation>
  </cffunction>
  
  <cffunction name="getMap" access="public" output="no" returntype="query">
    <cfargument name="mapID" required="yes" type="numeric">
    
    <cfquery name="qMap" datasource="#this.datasource#">
      SELECT *
      FROM dr_map
      WHERE mapID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.mapID#">
    </cfquery>
    <cfreturn qMap>
  </cffunction>
  
  <!--- Fintness Classes --->
  <cffunction name="getFitnessClassesByDayNumber" access="public" output="no" returntype="query">
    <cfargument name="dayOfWeekAsNumber" type="numeric" required="yes">
    
    <cfquery name="qClasses" datasource="#this.datasource#">
      SELECT cl_fitnessclass.fitnessClassID, cl_fitnessclass.classType, cl_fitnessclass.classTitle, cl_fitnessclass.location,
      		 cl_fitnessclass_time.fitnessClassTimeID, cl_fitnessclass_time.dayOfWeekAsNumber,
             cl_fitnessclass_time.startTime, cl_fitnessclass_time.endTime
      FROM cl_fitnessclass INNER JOIN cl_fitnessclass_time ON
      	   cl_fitnessclass.fitnessClassID=cl_fitnessclass_time.fitnessClassID
      WHERE cl_fitnessclass.published=1 AND cl_fitnessclass_time.dayOfWeekAsNumber=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.dayOfWeekAsNumber#">
      ORDER BY cl_fitnessclass_time.startTime
    </cfquery>
    
    <cfreturn qClasses>
  </cffunction>
  
  <cffunction name="getFitnessClassesByKeyword" access="public" output="no" returntype="struct">
    <cfargument name="keyword" type="string" required="yes">
    <cfargument name="startIndex" required="no" default="1" type="numeric">
	<cfargument name="numItemsPerPage" required="no" default="20" type="numeric">
    
	<cfset var resultStruct = StructNew()>
    
    <cfquery name="qClasses" datasource="#this.datasource#">
      SELECT cl_fitnessclass.fitnessClassID, cl_fitnessclass_time.dayOfWeekAsNumber
      FROM cl_fitnessclass INNER JOIN cl_fitnessclass_time ON
      	   cl_fitnessclass.fitnessClassID=cl_fitnessclass_time.fitnessClassID
      WHERE cl_fitnessclass.published=1 AND
      		cl_fitnessclass.classTitle LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
    </cfquery>
    <cfset resultStruct.numAllItems = qClasses.recordcount>
    
    <cfquery name="qClasses" datasource="#this.datasource#">
      SELECT cl_fitnessclass.fitnessClassID, cl_fitnessclass.classType, cl_fitnessclass.classTitle, cl_fitnessclass.location,
      		 cl_fitnessclass_time.fitnessClassTimeID, cl_fitnessclass_time.dayOfWeekAsNumber,
             cl_fitnessclass_time.startTime, cl_fitnessclass_time.endTime
      FROM cl_fitnessclass INNER JOIN cl_fitnessclass_time ON
      	   cl_fitnessclass.fitnessClassID=cl_fitnessclass_time.fitnessClassID
      WHERE cl_fitnessclass.published=1 AND
      		cl_fitnessclass.classTitle LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
      ORDER BY cl_fitnessclass.classTitle, cl_fitnessclass_time.startTime
      LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
    </cfquery>
    
    <cfset resultStruct.numDisplayedItems=qClasses.recordcount>	
    <cfset resultStruct.classes = qClasses>    
    
    <cfreturn resultStruct>
  </cffunction> 
  
  
  <cffunction name="getFitnessClassBasicInfo" access="public" output="no" returntype="query">
    <cfargument name="fitnessClassID" type="numeric" required="yes">
    
    <cfquery name="qClass" datasource="#this.datasource#">
      SELECT *
      FROM cl_fitnessclass
      WHERE published=1 AND fitnessClassID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClassID#">
    </cfquery>
     <cfreturn qClass>
  </cffunction>
  
  <cffunction name="getFitnessClassTimeInfo" access="public" output="no" returntype="query">
    <cfargument name="fitnessClassID" type="numeric" required="yes">
    
    <cfquery name="qClass" datasource="#this.datasource#">
      SELECT cl_fitnessclass_time.fitnessClassTimeID, cl_fitnessclass_time.dayOfWeekAsNumber,
      		 cl_fitnessclass_time.startTime, cl_fitnessclass_time.endTime
      FROM cl_fitnessclass INNER JOIN cl_fitnessclass_time ON
      	   cl_fitnessclass.fitnessClassID=cl_fitnessclass_time.fitnessClassID
      WHERE cl_fitnessclass.published=1 AND
      		cl_fitnessclass.fitnessClassID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClassID#">
    </cfquery>
     <cfreturn qClass>
  </cffunction>
  
  <cffunction name="getFitnessClassInfoByFitnessClassTimeID" access="public" output="no" returntype="query">
    <cfargument name="fitnessClassTimeID" type="numeric" required="yes">
    
    <cfquery name="qClassInfo" datasource="#this.datasource#">
      SELECT cl_fitnessclass.fitnessClassID, cl_fitnessclass.classType, cl_fitnessclass.classTitle,
      		 cl_fitnessclass.instructorName, cl_fitnessclass.location, cl_fitnessclass.description,
             cl_fitnessclass_time.dayOfWeekAsNumber, cl_fitnessclass_time.startTime, cl_fitnessclass_time.endTime
      FROM cl_fitnessclass INNER JOIN cl_fitnessclass_time ON
      	   cl_fitnessclass.fitnessClassID=cl_fitnessclass_time.fitnessClassID
      WHERE cl_fitnessclass_time.fitnessClassTimeID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClassTimeID#">      
    </cfquery>
  
    <cfreturn qClassInfo>
  </cffunction>
  
  <cffunction name="checkFitnessClubMembership" access="public" output="no" returntype="numeric">
    <cfargument name="fitnessClubMemberID" type="numeric" required="yes">
    
    <cfquery name="qMemberID" datasource="#this.datasource#">
      SELECT memberID
      FROM cl_fitnessmember
      WHERE memberID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClubMemberID#">
    </cfquery>
    
    <cfif qMemberID.recordcount Is 0>
      <cfreturn 2>
    <cfelse>
      <cfquery name="qMemberID" datasource="#this.datasource#">
        SELECT fitnessClubMemberID
        FROM cl_student
        WHERE fitnessClubMemberID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClubMemberID#"> AND
        	  studentType='F'
      </cfquery>
      <cfif qMemberID.recordcount GT 0><!--- member ID has been used --->
        <cfreturn 3>
      </cfif>
    </cfif>
    <cfreturn 1>
  </cffunction>
</cfcomponent>