<cfcomponent displayname="User" extends="Base" output="no" hint="Used for a session variable that store user's class sheduling info">
  <cfscript>
    this.isLoggedIn=false;
	this.studentID=0;
	this.email="";
	this.studentType="R";//Regular as default
	this.sendReminderEmail=0;
	//this.penciledInClassIDList="";
	this.penciledInClasses=ArrayNew(1);//ClassEntity array
	//this.penciledInServiceIDList="";
	this.penciledInServices=ArrayNew(1);
	
	//this.discountCodeList="";
	this.discountItems=ArrayNew(1);
	/*
    An array of discount items (Struct type)
    discountItems[]
		.courseID
		.discountCode
		.discountAmount
	*/
	this.donationAmount=0;
	
	this.attendeeFirstName="";
	this.attendeeLastName="";
	this.contactFirstName="";
	this.contactLastName="";
	this.contactAddress1="";
	this.contactAddress2="";
	this.contactCity="";
	this.contactState="";
	this.contactZip="";
	this.billingFirstName="";
	this.billingLastName="";
	this.billingAddress1="";
	this.billingAddress2="";
	this.billingCity="";
	this.billingState="";
	this.billingZip="";
	this.cardType="";
	this.nameOnCard="";
	this.cardNumber="";
	this.cardExpirationDate="";
	this.cardCVV2="";
	this.payWithCash=0;	
  </cfscript>
  
  <cffunction name="login" access="public" output="no" returntype="boolean">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="password" type="string" required="yes">
    
    <cfquery name="qStudent" datasource="#this.datasource#">
      SELECT *
      FROM cl_student
      WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#"> AND
      		password=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.password#">
    </cfquery>
  
    <cfif qStudent.recordcount GT 0>
      <cfset this.isLoggedIn=true>
      <cfset this.studentID=qStudent.studentID>
  	  <cfset this.email=qStudent.email>
 	  <cfset this.studentType=qStudent.studentType>
      <cfset this.sendReminderEmail=qStudent.sendReminderEmail>
      <cfreturn true>
    <cfelse>
      <cfreturn false>    
    </cfif>
  </cffunction>
  
  <!--- <cffunction name="logout" access="public" output="no" returntype="void">
    <cfset this.isLoggedIn=false>
    <cfset this.studentID=0>
    <cfset this.email="">
    <cfset this.studentType="R">
    <cfset this.sendReminderEmail=0>
    <cfset this.penciledInClassIDList="">
    <cfset ArrayClear(this.penciledInClasses)>
    <cfset this.penciledInServiceIDList="">
    <cfset ArrayClear(this.penciledInServices)>
    <cfset this.discountCodeList="">  
    <cfset ArrayClear(this.discountItems)>  
  </cffunction> --->
  
  <cffunction name="updateClassReminder" access="public" output="no" returntype="void">
    <cfargument name="sendReminderEmail" type="boolean" default="0">
    
    <cfif Arguments.sendReminderEmail>
      <cfquery datasource="#this.datasource#">
        UPDATE cl_student
        SET sendReminderEmail=1
        WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#">
      </cfquery>
      <cfset this.sendReminderEmail=1>
    </cfif>
  </cffunction>
  
  <cffunction name="emptyCart" access="public" output="no" returntype="void">
    <!--- <cfset this.penciledInClassIDList=""> --->
    <cfset ArrayClear(this.penciledInClasses)>
    <!--- <cfset this.penciledInServiceIDList=""> --->
    <cfset ArrayClear(this.penciledInServices)>
    <cfset this.discountCodeList="">  
    <cfset ArrayClear(this.discountItems)>  
    <cfset this.subTotalCost=0>
    <cfset this.totalCost=0>
  </cffunction>
  
  <cffunction name="addClassToCart" access="public" output="no" returntype="void">
    <cfargument name="classID" type="numeric" required="yes">
    
    <cfset var idx = 1>
	<cfset var numClasses=ArrayLen(this.penciledInClasses)>
    <cfset var found=false>
    
    <cfloop index="idx" from="1" to="#numClasses#">
      <cfif this.penciledInClasses[idx].classID Is Arguments.classID>
        <cfset found=true>
        <cfbreak>
      </cfif>
    </cfloop>
    
    <cfif Not found>
      <cfset newClassEntity=CreateObject("component","ClassEntity").getClassEntityByID(classID="#Arguments.classID#", studentType="#this.studentType#")>
      <cfset ArrayAppend(this.penciledInClasses,newClassEntity)>
    </cfif>
  </cffunction>  
  
  <cffunction name="addServiceToCart" access="public" output="no" returntype="void">
    <cfargument name="serviceID" type="numeric" required="yes">
    
    <cfset var idx = 1>
    <cfset var numServices=ArrayLen(this.penciledInServices)>
    <cfset var found=false>
    
    <cfloop index="idx" from="1" to="#numServices#">
      <cfif this.penciledInServices[idx].serviceID Is Arguments.serviceID>
        <cfset found=true>
        <cfbreak>
      </cfif>
    </cfloop>
    
    <cfif Not found>
      <cfset newServiceEntity=CreateObject("component","ServiceEntity").getServiceEntityByID(serviceID="#Arguments.serviceID#")>
      <cfset ArrayAppend(this.penciledInServices, newServiceEntity)>    
    </cfif>    
  </cffunction>
  
  <cffunction name="addDiscountCodeToCart" access="public" output="no" returntype="boolean">
    <cfargument name="discountCode" type="string" required="yes">
    
    <cfset var idx = 1>
    <cfset var found=false>
    
    <cfset numDiscountItems=ArrayLen(this.discountItems)>
    
    <cfif ArrayLen(this.penciledInClasses) IS 0>
	  <cfreturn false>
	</cfif>
    
    <cfloop index="idx" from="1" to="#numDiscountItems#">
      <cfif Not CompareNoCase(this.discountItems[idx].discountCode, Arguments.discountCode)>
        <!--- discount code has been used --->
        <cfreturn false>
      </cfif>
    </cfloop>
    
    <!--- discount code is not used yet --->
    <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClasses)#">
   	  <cfif Not Compare(this.penciledInClasses[idx].discountCode, Arguments.discountCode)>
        <!--- valid discount code --->
        <cfset newDiscountItem=StructNew()>
        <cfset newDiscountItem.courseID=this.penciledInClasses[idx].courseID>
        <cfset newDiscountItem.discountCode=this.penciledInClasses[idx].discountCode>
        <cfif this.penciledInClasses[idx].discountType IS "percentage">
          <cfset newDiscountItem.discountAmount=NumberFormat((this.penciledInClasses[idx].discountRate * this.penciledInClasses[idx].cost) / 100, ".99")>
        <cfelse>
          <cfset newDiscountItem.discountAmount=this.penciledInClasses[idx].discountRate>   
        </cfif>
             
        <cfset ArrayAppend(this.discountItems, newDiscountItem)>
        <cfreturn true>
      </cfif>
    </cfloop>      
    <cfreturn false>
  </cffunction>
  
  <cffunction name="removeClassesFromCart" access="public" output="no" returntype="void">
    <cfargument name="classIDList" type="string" default=""> 
    
    <cfset var idx = 1>
    
    <cfif Not Compare(Arguments.classIDList,"")><cfreturn></cfif>   
    
    <cfloop index="classID" list="#Arguments.classIDList#">
      <cfset removeClassFromCart(classID)>      
    </cfloop>
  </cffunction>
  
  <cffunction name="removeClassFromCart" access="public" output="no" returntype="void">
    <cfargument name="classID" type="numeric" required="yes"> 
    
    <cfset var idx = 1>
  
    <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClasses)#">
      <cfif Not Compare(this.penciledInClasses[idx].classID, Arguments.classID)>
        <cfset courseID=this.penciledInClasses[idx].courseID>
        <cfset ArrayDeleteAt(this.penciledInClasses,idx)>
        <cfset removeDiscountCodeByCourseID(courseID)>
        <cfbreak>
      </cfif>
    </cfloop>      
  </cffunction>
  
  <cffunction name="removeServicesFromCart" access="public" output="no" returntype="void">
    <cfargument name="serviceIDList" type="string" default=""> 
    
    <cfset var idx = 1>
    
    <cfif Not Compare(Arguments.serviceIDList,"")><cfreturn></cfif>  
    
    <cfloop index="serviceID" list="#Arguments.serviceIDList#">
      <cfset removeServiceFromCart(serviceID)>      
    </cfloop>
  </cffunction>
  
  <cffunction name="removeServiceFromCart" access="public" output="no" returntype="void">
    <cfargument name="serviceID" type="numeric" required="yes"> 
    
    <cfset var idx = 1>
  
    <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInServices)#">
      <cfif Not Compare(this.penciledInServices[idx].serviceID, Arguments.serviceID)>
        <cfset ArrayDeleteAt(this.penciledInServices,idx)>
        <cfbreak>
      </cfif>
    </cfloop>      
  </cffunction>   
  
  <cffunction name="removeDiscountCodesFromCart" access="public" output="no" returntype="void">
    <cfargument name="discountCodeList" type="string" default=""> 
    
    <cfset var idx = 0>    
    
    <cfif Not Compare(Arguments.discountCodeList, "")><cfreturn></cfif>
    
    <cfloop index="discountCode" list="#Arguments.discountCodeList#">
      <cfset removeDiscountCodeFromCart(discountCode)>    
    </cfloop>  
  </cffunction>
  
  <cffunction name="removeDiscountCodeFromCart" access="public" output="no" returntype="void">
    <cfargument name="discountCode" type="string" required="yes">
    
    <cfset var idx = 0>
    
    <cfloop index="idx" from="1" to="#ArrayLen(this.discountItems)#">
      <cfif Not CompareNoCase(this.discountItems[idx].discountCode, Arguments.discountCode)>
        <cfset ArrayDeleteAt(this.discountItems, idx)>
        <cfbreak>
      </cfif>
    </cfloop> 
  </cffunction>

  <cffunction name="removeDiscountCodeByCourseID" access="public" output="no" returntype="void">
    <cfargument name="courseID" type="numeric" required="yes">
    
    <cfset var idx = 1>
    
    <cfloop index="idx" from="1" to="#ArrayLen(this.discountItems)#">
      <cfif Not Compare(this.discountItems[idx], Arguments.courseID)>
        <cfset ArrayDeleteAt(this.discountItems, idx)>
        <cfbreak>
      </cfif>
    </cfloop>  
  </cffunction>
  
  <cffunction name="checkDiscountCode" access="public" output="no" returntype="boolean">
    <cfargument name="discountCode" type="string" required="yes">
    
    <cfset var idx = 0>
    
    <cfif ArrayLen(this.penciledInClasses) IS 0>
	  <cfreturn false>
	</cfif>
    
    <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClasses)#">
      <cfif Not CompareNoCase(this.penciledInClasses[idx].discountCode, Arguments.discountCode)>
        <cfreturn true>
      </cfif>
    </cfloop>
    
    <cfreturn false>
  </cffunction>
  
  <cffunction name="addDonationToCart" access="public" output="no" returntype="void">
    <cfargument name="donationAmount" type="numeric" default="0">
    
    <cfset this.donationAmount=Arguments.donationAmount>
  </cffunction>
  
  <cffunction name="removeDonationFromCart" access="public" output="no" returntype="void">
    <cfargument name="removeDonation" type="boolean" default="0">
    
    <cfif Arguments.removeDonation>
      <cfset this.donationAmount=0>
    </cfif>
  </cffunction>  
  
  <cffunction name="getPenciledInClasses" access="public" output="no" returntype="array">    
    <cfreturn this.penciledInClasses>
  </cffunction>
  
  <cffunction name="getPenciledInServices" access="public" output="no" returntype="array">    
    <cfreturn this.penciledInServices>
  </cffunction>
  
  <cffunction name="getDiscountItems" access="public" output="no" returntype="array">
    <cfreturn this.discountItems>
  </cffunction>
  
  <cffunction name="getDonationAmount" access="public" output="no" returntype="numeric">
    <cfreturn this.donationAmount>
  </cffunction>
  
  <cffunction name="getCartTotal" access="public" output="no" returntype="numeric">
    <cfset var idx1 = 0>
    <cfset var idx2 = 0>
    <cfset var totalCost = 0>
    
    <cfloop index="idx1" from="1" to="#ArrayLen(this.penciledInClasses)#">
      <cfset totalCost = totalCost + this.penciledInClasses[idx1].cost>
    </cfloop>
    <cfloop index="idx1" from="1" to="#ArrayLen(this.penciledInServices)#">
      <cfset totalCost = totalCost + this.penciledInServices[idx1].cost>
    </cfloop>
    
	<cfloop index="idx1" from="1" to="#ArrayLen(this.discountItems)#">
      <cfloop index="idx2" from="1" to="#ArrayLen(this.penciledInClasses)#">
        <cfif Not CompareNoCase(this.discountItems[idx1].discountCode, this.penciledInClasses[idx2].discountCode)>
          <cfset totalCost = totalCost - this.discountItems[idx1].discountAmount>
        </cfif>
      </cfloop>
    </cfloop>
    
    <cfset totalCost = totalCost+ this.donationAmount>
    
    <cfif totalCost LT 0>
      <cfreturn 0>
    <cfelse>
      <cfreturn totalCost>
    </cfif>
  </cffunction>
  
  <!--- <cffunction name="updateCartTotal" access="public" output="no" returntype="void">
    <!--- syn discountItems[] with discountCodeList --->
    <!--- syn subTotalCost, totalCost --->
    <cfset var idx = 0>
    <cfset var idx2 = 0>
    
    <cfset ArrayClear(this.discountItems)>
    <cfset this.subTotalCost=0>
    
    <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClass)#">
      <cfset this.subTotalCost = this.subTotalCost + this.penciledInClass[idx].cost>
    </cfloop>
    <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInService)#">
      <cfset this.subTotalCost = this.subTotalCost + this.penciledInService[idx].cost>
    </cfloop>
    <cfset this.totalCost=this.subTotalCost>
    
    <cfif ListLen(this.discountCodeList) GT 0>
      <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClass)#">
        <cfif Compare(this.penciledInClass[idx].discountCode, "")>
          <cfif ListFind(this.discountCodeList, this.penciledInClass[idx].discountCode) GT 0><!--- user did use the code; so give her discount --->
            <cfset idx2 = idx2 + 1>
            <cfset this.discountItems[idx2]=StructNew()>
            <cfset this.discountItems[idx2].discountCode=this.penciledInClass[idx].discountCode>
            <cfif Not CompareNoCase(this.penciledInClass[idx].discountType, "flat rate")><!--- discount by fixed amount --->
              <cfset this.discountItems[idx2].discountAmount = this.penciledInClass[idx].discountRate>              
            <cfelse><!--- by percentage --->
              <cfset this.discountItems[idx2].discountAmount = Int(this.penciledInClass[idx].discountRate * this.penciledInClass[idx].cost * 100)/100>
            </cfif>		
            <cfset this.totalCost = this.totalCost - this.discountItems[idx2].discountAmount>
          </cfif>
        </cfif>
      </cfloop>
    </cfif>
  </cffunction> --->
  
  <cffunction name="getUpcomingClasses" access="public" output="no" returntype="array">
    <cfset var currentTime=now()>
    <cfset var upcomingClasses=ArrayNew(1)>
    
    <cfquery name="qClassIDs" datasource="#this.datasource#">
      SELECT cl_student_class.classID, view_cl_classinstance_summary.firstStartDateTime
      FROM  cl_student_class INNER JOIN view_cl_classinstance_summary ON
      		cl_student_class.classID=view_cl_classinstance_summary.classID
      WHERE cl_student_class.studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#"> AND
      		view_cl_classinstance_summary.lastStartDateTime >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
      ORDER BY view_cl_classinstance_summary.firstStartDateTime
    </cfquery>
  
    <cfif qClassIDs.recordcount GT 0>
      <cfloop index="idx" from="1" to="#qClassIDs.recordcount#">
        <cfset upcomingClasses[idx]=CreateObject("component","com.ClassEntity").getClassEntityByID(qClassIDs.classID[idx])>
      </cfloop>    
    </cfif>
    
    <cfreturn upcomingClasses>
  </cffunction>
  
  
  <cffunction name="getCompletedClasses" access="public" output="no" returntype="array">
    <cfset var currentTime=now()>
    <cfset var completedClasses=ArrayNew(1)>
    
    <cfquery name="qClassIDs" datasource="#this.datasource#">
      SELECT cl_student_class.classID, cl_student_class.feedbackSent, view_cl_classinstance_summary.firstStartDateTime
      FROM  cl_student_class INNER JOIN view_cl_classinstance_summary ON
      		cl_student_class.classID=view_cl_classinstance_summary.classID
      WHERE cl_student_class.studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#"> AND
      		view_cl_classinstance_summary.lastStartDateTime < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
      ORDER BY view_cl_classinstance_summary.firstStartDateTime DESC
    </cfquery>
  
    <cfif qClassIDs.recordcount GT 0>
      <cfloop index="idx" from="1" to="#qClassIDs.recordcount#">
        <cfset completedClasses[idx]=CreateObject("component","com.ClassEntity").getClassEntityByID(qClassIDs.classID[idx])>
        <cfset completedClasses[idx].feedbackSent=qClassIDs.feedbackSent[idx]>
      </cfloop>    
    </cfif>
    
    <cfreturn completedClasses>
  </cffunction>


  <cffunction name="updateBillingInfo" access="public" output="no" returntype="void">
    <cfargument name="attendeeFirstName" type="string" required="yes">
    <cfargument name="attendeeLastName" type="string" required="yes">
    <cfargument name="contactFirstName" type="string" required="yes">
    <cfargument name="contactLastName" type="string" required="yes">
    <cfargument name="contactAddress1" type="string" required="yes">
    <cfargument name="contactAddress2" type="string" default="">
    <cfargument name="contactCity" type="string" required="yes">
    <cfargument name="contactState" type="string" required="yes">
    <cfargument name="contactZip" type="string" required="yes">
    
    <cfargument name="payWithCash" type="boolean" default="0">
    <cfargument name="cardType" type="string" default="">
    <cfargument name="nameOnCard" type="string" default="">
    <cfargument name="cardNumber" type="string" default="">
    <cfargument name="cardExpirationDate" type="string" default="">
    <cfargument name="cardCVV2" type="string" default="">
    
    <cfargument name="billingFirstName" type="string" default="">
    <cfargument name="billingLastName" type="string" default="">
    <cfargument name="billingAddress1" type="string" default="">
    <cfargument name="billingAddress2" type="string" default="">
    <cfargument name="billingCity" type="string" default="">
    <cfargument name="billingState" type="string" default="">
    <cfargument name="billingZip" type="string" default="">
    
    <cfscript>
      this.attendeeFirstName=Arguments.attendeeFirstName;
	  this.attendeeLastName=Arguments.attendeeLastName;
	  
	  this.contactFirstName=Arguments.contactFirstName;
	  this.contactLastName=Arguments.contactLastName;
	  this.contactAddress1=Arguments.contactAddress1;
	  this.contactAddress2=Arguments.contactAddress2;
	  this.contactCity=Arguments.contactCity;
	  this.contactState=Arguments.contactState;
	  this.contactZip=Arguments.contactZip;
	  
	  this.payWithCash=Arguments.payWithCash;
	  if (Not Arguments.payWithCash) {
   		this.cardType=Arguments.cardType;
		this.nameOnCard=Arguments.nameOnCard;
		this.cardNumber=Arguments.cardNumber;
		this.cardExpirationDate=Arguments.cardExpirationDate;
		this.cardCVV2=Arguments.cardCVV2;
      }
	  
	  this.billingFirstName=Arguments.billingFirstName;
	  this.billingLastName=Arguments.billingLastName;
	  this.billingAddress1=Arguments.billingAddress1;
	  this.billingAddress2=Arguments.billingAddress2;
	  this.billingCity=Arguments.billingCity;
	  this.billingState=Arguments.billingState;
	  this.billingZip=Arguments.billingZip;    
    </cfscript>
  </cffunction>
</cfcomponent>