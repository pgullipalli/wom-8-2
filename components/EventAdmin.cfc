<cfcomponent displayname="Event Manager" extends="Base" output="false">
  
  <cffunction name="getAllCategories" access="public" output="no" returntype="query">
	<cfquery name="qAllCategories" datasource="#this.datasource#">
	  SELECT categoryID, categoryName
	  FROM em_category
	  ORDER BY categoryName	
	</cfquery>	
    
    <cfreturn qAllCategories>  
  </cffunction>
  
  <cffunction name="getCategorySummary" access="public" output="no" returntype="query">
    <cfquery name="qCatSummary" datasource="#this.datasource#">
	  SELECT em_category.categoryName,
      	  	 em_category.categoryID,
			 COUNT(em_event_category.eventID) AS numEvents
	  FROM em_category LEFT JOIN em_event_category ON
	  	   em_category.categoryID=em_event_category.categoryID
	  GROUP by em_category.categoryID
	  ORDER by em_category.categoryName		
	</cfquery>
    
    <cfreturn qCatSummary>
  </cffunction>
  
  <cffunction name="getFirstCategoryID" access="public" output="no" returntype="numeric">
    <cfquery name="qCategoryID" datasource="#this.datasource#">
	  SELECT categoryID
	  FROM em_category
      ORDER BY categoryName
	</cfquery>
    <cfif qCategoryID.recordcount GT 0>
	  <cfreturn qCategoryID.categoryID[1]>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="getCategory" access="public" output="no" returntype="query">
    <cfargument name="categoryID" required="yes" type="numeric">	
	<cfquery name="qCategory" datasource="#this.datasource#">
	  SELECT *
      FROM em_category
      WHERE categoryID=<cfqueryparam value="#Arguments.categoryID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
	<cfreturn qCategory>
  </cffunction>
  
  <cffunction name="addCategory" access="public" output="no">
    <cfargument name="categoryName" type="string" required="yes">

    <cfquery datasource="#this.datasource#">
	  INSERT INTO em_category
        (categoryName, dateCreated, dateLastModified)
	  VALUES
        (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.categoryName#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)	  
	</cfquery>  
  </cffunction>
  
  <cffunction name="editCategory" access="public" output="no">
    <cfargument name="categoryID" type="numeric" required="yes">
    <cfargument name="categoryName" type="string" required="yes">

    <cfquery datasource="#this.datasource#">
	  UPDATE em_category
      SET categoryName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.categoryName#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">  
	</cfquery>  
  </cffunction>
    
  <cffunction name="deleteCategory" access="public" output="false" returntype="boolean">
    <cfargument name="categoryID" required="yes" type="numeric">
	
	<cfquery name="qEvents" datasource="#this.datasource#">
	  SELECT eventID
	  FROM em_event_category
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">			
	</cfquery>
	
	<cfif qEvents.recordcount GT 0>
	  <cfreturn false>
	</cfif>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM em_category
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
	</cfquery>
    
	<cfreturn true>
  </cffunction>
  
  <cffunction name="getEventsByCategoryID" access="public" output="false" returntype="struct">
    <cfargument name="categoryID" type="numeric" required="yes">
	<cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.items=QueryNew("eventID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qAllEventIDs" datasource="#this.datasource#">
	  SELECT COUNT(em_event.eventID) AS numItems
	  FROM	 em_event INNER JOIN em_event_category ON
	  		 em_event.eventID=em_event_category.eventID
	  WHERE	 em_event_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
    </cfquery>
	<cfset resultStruct.numAllItems = qAllEventIDs.numItems>
	
	<cfquery name="qEvents" datasource="#this.datasource#">
	  SELECT em_event.eventID, em_event.eventName, em_event.published, em_event.allDayEvent,
      		 em_event.startDateTime, em_event.endDateTime, em_event.eventLocation
	  FROM   em_event INNER JOIN em_event_category ON
	         em_event.eventID=em_event_category.eventID   
	  WHERE  em_event_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
	  ORDER  BY em_event.startDateTime DESC
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
    <cfif qEvents.recordcount GT 0>            
      <cfset resultStruct.numDisplayedItems=qEvents.recordcount>	
      <cfset resultStruct.events = qEvents>
	<cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.events = QueryNew("eventID")>
    </cfif>
    
	<cfreturn resultStruct>
  </cffunction>  
  
  <cffunction name="getEventByID" access="public" output="no" returntype="query">
    <cfargument name="eventID" type="numeric" required="yes">
	<cfquery name="qEvent" datasource="#this.datasource#">
	  SELECT *
	  FROM em_event
      WHERE eventID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.eventID#">
	</cfquery>
    
	<cfreturn qEvent>	
  </cffunction>
  
  <cffunction name="getCategoryIDListByEventID" access="public" output="no" returntype="string">
    <cfargument name="eventID" type="numeric" required="yes">
    
	<cfquery name="qCategories" datasource="#this.datasource#">
	  SELECT categoryID
      FROM em_event_category
      WHERE eventID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.eventID#">
	</cfquery>
    
    <cfif qCategories.recordcount Is 0>
      <cfreturn "">
    <cfelse>
	  <cfreturn valueList(qCategories.categoryID)>	  
    </cfif>
  </cffunction>
  
  <cffunction name="getCategoryByID" access="public" output="no" returntype="query">
    <cfargument name="categoryID" required="yes" type="numeric">	
	<cfquery name="qCategory" datasource="#this.datasource#">
	  SELECT *
      FROM em_category
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
	</cfquery>
	<cfreturn qCategory>
  </cffunction>

  <cffunction name="addEvent" access="public" output="no" returntype="boolean">
    <cfargument name="categoryIDList" type="string" default="">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="allDayEvent" type="boolean" default="0">
    <cfargument name="startDateTime" type="string" required="yes">
    <cfargument name="endDateTime" type="string" required="yes">
    <cfargument name="eventLocation" type="string" required="yes">
    <cfargument name="eventURL" type="string" required="yes">
    <cfargument name="eventImage" type="string" default="">
    <cfargument name="eventImageCaption" type="string" default="">
    <cfargument name="shortDescription" type="string" required="yes">
    <cfargument name="longDescription" type="string" required="yes"> 
    
    <cfif Not Compare(Arguments.categoryIDList, "")><cfreturn false></cfif>
    <cfset Util=CreateObject("component","Utility").init()>
    <cfset Variables.startDateTime=Util.convertStringToDateTimeObject(Arguments.startDateTime, Arguments.startDateTime_hh, Arguments.startDateTime_mm, 0, Arguments.startDateTime_ampm)>
    <cfset Variables.endDateTime=Util.convertStringToDateTimeObject(Arguments.endDateTime, Arguments.endDateTime_hh, Arguments.endDateTime_mm, 0, Arguments.endDateTime_ampm)>
    		
    <cfquery datasource="#this.datasource#">
	  INSERT INTO em_event
        (eventName, published, allDayEvent, startDateTime, endDateTime,
         eventLocation, eventURL, eventImage, eventImageCaption, shortDescription, longDescription,
         dateCreated, dateLastModified)
	  VALUES
	    (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.eventName#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.allDayEvent#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDateTime#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDateTime#">,
         
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.eventLocation#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.eventURL#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.eventImage#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.eventImageCaption#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.shortDescription#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.longDescription#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)		
	</cfquery>

	<cfquery name="qEventID" datasource="#this.datasource#">
	  SELECT MAX(eventID) AS newEventID
	  FROM em_event
    </cfquery>
	<cfset Variables.eventID=qEventID.newEventID>	
    
    <cfloop index="categoryID" list="#Arguments.categoryIDList#">      
      <cfquery datasource="#this.datasource#">
		INSERT INTO em_event_category (eventID, categoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.eventID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>

  <cffunction name="editEvent" access="public" output="no" returntype="boolean">
    <cfargument name="eventID" type="numeric" required="yes">
    <cfargument name="categoryIDList" type="string" default="">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="allDayEvent" type="boolean" default="0">
    <cfargument name="startDateTime" type="string" required="yes">
    <cfargument name="endDateTime" type="string" required="yes">
    <cfargument name="eventLocation" type="string" required="yes">
    <cfargument name="eventURL" type="string" required="yes">
    <cfargument name="eventImage" type="string" default="">
    <cfargument name="eventImageCaption" type="string" default="">
    <cfargument name="shortDescription" type="string" required="yes">
    <cfargument name="longDescription" type="string" required="yes">
    
    <cfif Not Compare(Arguments.categoryIDList, "")><cfreturn false></cfif>
    <cfset Util=CreateObject("component","Utility").init()>
    <cfset Variables.startDateTime=Util.convertStringToDateTimeObject(Arguments.startDateTime, Arguments.startDateTime_hh, Arguments.startDateTime_mm, 0, Arguments.startDateTime_ampm)>
    <cfset Variables.endDateTime=Util.convertStringToDateTimeObject(Arguments.endDateTime, Arguments.endDateTime_hh, Arguments.endDateTime_mm, 0, Arguments.endDateTime_ampm)>
    
    <cfquery datasource="#this.datasource#">
	  UPDATE em_event
      SET published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
          eventName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.eventName#">,
          allDayEvent=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.allDayEvent#">,
          startDateTime=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDateTime#">,
          endDateTime=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDateTime#">,
         
          eventLocation=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.eventLocation#">,
          eventURL=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.eventURL#">,
          eventImage=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.eventImage#">,
          eventImageCaption=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.eventImageCaption#">,
          shortDescription=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.shortDescription#">,
          longDescription=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.longDescription#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE eventID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.eventID#">
	</cfquery>
        
    <cfquery datasource="#this.datasource#">
      DELETE FROM em_event_category
      WHERE eventID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.eventID#">
    </cfquery>
    
    <cfloop index="categoryID" list="#Arguments.categoryIDList#">    
      <cfquery datasource="#this.datasource#">
		INSERT INTO em_event_category (eventID, categoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.eventID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>
  
  <cffunction name="deleteEvent" access="public" output="false">
    <cfargument name="eventID" required="yes" type="numeric">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM em_event_category
      WHERE eventID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.eventID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM em_event
	  WHERE eventID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.eventID#">
    </cfquery>
  </cffunction>
</cfcomponent>