<cfcomponent displayname="Generic Module" extends="Base" output="false">
  
  <cffunction name="getAllCategories" access="public" output="no" returntype="query">
	<cfquery name="qAllCategories" datasource="#this.datasource#">
	  SELECT categoryID, categoryName
	  FROM md_category
	  ORDER BY categoryName	
	</cfquery>	
    
    <cfreturn qAllCategories>  
  </cffunction>
  
  <cffunction name="getCategorySummary" access="public" output="no" returntype="query">
    <cfquery name="qCatSummary" datasource="#this.datasource#">
	  SELECT md_category.categoryName,
      	  	 md_category.categoryID,
			 COUNT(md_item_category.itemID) AS numItems
	  FROM md_category LEFT JOIN md_item_category ON
	  	   md_category.categoryID=md_item_category.categoryID
	  GROUP by md_category.categoryID
	  ORDER by md_category.categoryName		
	</cfquery>
    
    <cfreturn qCatSummary>
  </cffunction>
  
  <cffunction name="getFirstCategoryID" access="public" output="no" returntype="numeric">
    <cfquery name="qCategoryID" datasource="#this.datasource#">
	  SELECT categoryID
	  FROM md_category
      ORDER BY categoryName
	</cfquery>
    <cfif qCategoryID.recordcount GT 0>
	  <cfreturn qCategoryID.categoryID[1]>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="getCategory" access="public" output="no" returntype="query">
    <cfargument name="categoryID" required="yes" type="numeric">	
	<cfquery name="qCategory" datasource="#this.datasource#">
	  SELECT *
      FROM md_category
      WHERE categoryID=<cfqueryparam value="#Arguments.categoryID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
	<cfreturn qCategory>
  </cffunction>
  
  <cffunction name="addCategory" access="public" output="no">
    <cfargument name="categoryName" type="string" required="yes">

    <cfquery datasource="#this.datasource#">
	  INSERT INTO md_category
        (categoryName, dateCreated, dateLastModified)
	  VALUES
        (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.categoryName#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)	  
	</cfquery>  
  </cffunction>
  
  <cffunction name="editCategory" access="public" output="no">
    <cfargument name="categoryID" type="numeric" required="yes">
    <cfargument name="categoryName" type="string" required="yes">

    <cfquery datasource="#this.datasource#">
	  UPDATE md_category
      SET categoryName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.categoryName#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">  
	</cfquery>  
  </cffunction>
    
  <cffunction name="deleteCategory" access="public" output="false" returntype="boolean">
    <cfargument name="categoryID" required="yes" type="numeric">
	
	<cfquery name="qItems" datasource="#this.datasource#">
	  SELECT itemID
	  FROM md_item_category
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">			
	</cfquery>
	
	<cfif qItems.recordcount GT 0>
	  <cfreturn false>
	</cfif>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM md_category
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
	</cfquery>
    
	<cfreturn true>
  </cffunction>
  
  <cffunction name="getItemsByCategoryID" access="public" output="false" returntype="struct">
    <cfargument name="categoryID" type="numeric" required="yes">
	<cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.items=QueryNew("itemID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qAllItemIDs" datasource="#this.datasource#">
	  SELECT COUNT(md_item.itemID) AS numItems
	  FROM	 md_item INNER JOIN md_item_category ON
	  		 md_item.itemID=md_item_category.itemID
	  WHERE	 md_item_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
    </cfquery>
	<cfset resultStruct.numAllItems = qAllItemIDs.numItems>
	
	<cfquery name="qItems" datasource="#this.datasource#">
	  SELECT md_item.itemID, md_item.itemName, md_item.published	 
	  FROM   md_item INNER JOIN md_item_category ON
	         md_item.itemID=md_item_category.itemID   
	  WHERE  md_item_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
	  ORDER  BY md_item_category.displaySeq
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
    <cfif qItems.recordcount GT 0>            
      <cfset resultStruct.numDisplayedItems=qItems.recordcount>	
      <cfset resultStruct.items = qItems>
	<cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.items = QueryNew("itemID")>
    </cfif>
    
	<cfreturn resultStruct>
  </cffunction>  
  
  <cffunction name="getItemByID" access="public" output="no" returntype="query">
    <cfargument name="itemID" type="numeric" required="yes">
	<cfquery name="qItem" datasource="#this.datasource#">
	  SELECT *
	  FROM md_item
      WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.itemID#">
	</cfquery>
    
	<cfreturn qItem>	
  </cffunction>
  
  <cffunction name="getCategoryIDListByItemID" access="public" output="no" returntype="string">
    <cfargument name="itemID" type="numeric" required="yes">
    
	<cfquery name="qCategories" datasource="#this.datasource#">
	  SELECT categoryID
      FROM md_item_category
      WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.itemID#">
	</cfquery>
    
    <cfif qCategories.recordcount Is 0>
      <cfreturn "">
    <cfelse>
	  <cfreturn valueList(qCategories.categoryID)>	  
    </cfif>
  </cffunction>
  
  <cffunction name="getCategoryByID" access="public" output="no" returntype="query">
    <cfargument name="categoryID" required="yes" type="numeric">	
	<cfquery name="qCategory" datasource="#this.datasource#">
	  SELECT *
      FROM md_category
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
	</cfquery>
	<cfreturn qCategory>
  </cffunction>

  <cffunction name="addItem" access="public" output="no" returntype="boolean">
    <cfargument name="categoryIDList" type="string" default="">
    <cfargument name="published" type="boolean" default="0">
    
    <cfif Not Compare(Arguments.categoryIDList, "")><cfreturn false></cfif>
    		
    <cfquery datasource="#this.datasource#">
	  INSERT INTO md_item
        (itemName, published, dateCreated, dateLastModified)
	  VALUES
	    (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.itemName#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)		
	</cfquery>

	<cfquery name="qItemID" datasource="#this.datasource#">
	  SELECT MAX(itemID) AS newItemID
	  FROM md_item
    </cfquery>
	<cfset Variables.itemID=qItemID.newItemID>	
    
    <cfloop index="categoryID" list="#Arguments.categoryIDList#">
      <cfquery name="qMinSeq" datasource="#this.datasource#">
        SELECT MIN(displaySeq) AS minDisplaySeq
        FROM md_item_category
        WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">
      </cfquery>
      
      <cfif qMinSeq.recordcount Is 0>
        <cfset newDisplaySeq=1>
      <cfelse>
        <cfset newDisplaySeq=qMinSeq.minDisplaySeq - 1>
      </cfif>
      
      <cfquery datasource="#this.datasource#">
		INSERT INTO md_item_category (itemID, categoryID, displaySeq)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.itemID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">,
           <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>

  <cffunction name="editItem" access="public" output="no" returntype="boolean">
    <cfargument name="itemID" type="numeric" required="yes">
    <cfargument name="categoryIDList" type="string" default="">
    <cfargument name="published" type="boolean" default="0">
    
    <cfif Not Compare(Arguments.categoryIDList, "")><cfreturn false></cfif>
    
    <cfquery datasource="#this.datasource#">
	  UPDATE md_item
      SET published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
          itemName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.itemName#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">
	</cfquery>

	<cfquery datasource="#this.datasource#">
      DELETE FROM md_item_category
      WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">
    </cfquery>
    
    <cfloop index="categoryID" list="#Arguments.categoryIDList#">
      <cfquery datasource="#this.datasource#">
		INSERT INTO md_item_category (itemID, categoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>
  
  <cffunction name="deleteItem" access="public" output="false">
    <cfargument name="itemID" required="yes" type="numeric">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM md_item_category
      WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM md_item
	  WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="getItemsByKeyword" access="public" output="no" returntype="struct">
	<cfargument name="keyword" type="string" required="yes">
    <cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.items=QueryNew("itemID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qAllItemIDs" datasource="#this.datasource#">
	  SELECT COUNT(itemID) AS numItems
	  FROM	 md_item
	  WHERE	 itemName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.keyword#%">
    </cfquery>
	<cfset resultStruct.numAllItems = qAllItemIDs.numItems>
    
	<cfquery name="qItems" datasource="#this.datasource#">
	  SELECT itemID, itemName, published
	  FROM md_item
      WHERE itemName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.keyword#%">
	  ORDER BY itemName
      LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>	
    
    <cfif qItems.recordcount GT 0>      
      <cfset resultStruct.numDisplayedItems=qItems.recordcount>	
      <cfset resultStruct.items = qItems>
    <cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.items = QueryNew("itemID")>
	</cfif>

	<cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="moveItem" access="public" output="no">
    <cfargument name="itemID" required="yes" type="numeric">
    <cfargument name="direction" required="yes" type="string">
    
    <cfquery name="qTargetDisplaySeq" datasource="#this.datasource#">
      SELECT categoryID, displaySeq
      FROM md_item_category
      WHERE itemID=<cfqueryparam value="#Arguments.itemID#" cfsqltype="cf_sql_integer">
    </cfquery>
    
    <cfset targetCategoryID=qTargetDisplaySeq.categoryID>
    <cfset targetDisplaySeq=qTargetDisplaySeq.displaySeq>
    
    <cfif Arguments.direction IS "up">
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT itemID, displaySeq
        FROM md_item_category
        WHERE categoryID=<cfqueryparam value="#targetCategoryID#" cfsqltype="cf_sql_integer"> AND
        	  displaySeq < <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq DESC
      </cfquery>
    <cfelse>
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT itemID, displaySeq
        FROM md_item_category
        WHERE categoryID=<cfqueryparam value="#targetCategoryID#" cfsqltype="cf_sql_integer"> AND
        	  displaySeq > <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq ASC
      </cfquery>
    </cfif>
    
    <cfif qNextDisplaySeq.recordcount Is 0><cfreturn></cfif>
    
    <cfset nextDisplaySeq=qNextDisplaySeq.displaySeq>
        
    <cfquery datasource="#this.datasource#">
      UPDATE md_item_category
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#nextDisplaySeq#">
      WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      UPDATE md_item_category
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
      WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qNextDisplaySeq.itemID#">
    </cfquery>
  </cffunction>
</cfcomponent>