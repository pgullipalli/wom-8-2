<cfcomponent displayname="ClassInstanceEntity" output="no">
  <cfif IsDefined("Application.datasource")>
    <cfset this.datasource=Application.datasource>
  </cfif>
  
  <cfset this.classInstanceID=0>
  <cfset this.classID=0>
  <cfset this.startDateTime="">
  <cfset this.endDateTime="">
  <cfset this.dayOfWeekAsNumber=0>
  <cfset this.locationID=0>
  <cfset this.locationName="">
  <cfset this.mapID=0>
  <cfset this.mapVerified=0><!--- map verification flag --->
  
  <cffunction name="init" access="public" output="false" returntype="ClassInstanceEntity">
    <cfargument name="classInstanceID" type="numeric" required="yes">
    <cfargument name="classID" type="numeric" required="yes">
    <cfargument name="startDateTime" type="date" required="yes">
    <cfargument name="endDateTime" type="date" required="yes">
    <cfargument name="dayOfWeekAsNumber" type="numeric" required="yes">
    <cfargument name="locationID" type="numeric" required="yes">
    <cfargument name="locationName" type="string" required="yes">
    <cfargument name="mapID" type="numeric" required="yes">
    <cfargument name="mapVerified" type="boolean" default="0">
    
    <cfset this.classInstanceID=Arguments.classInstanceID>
    <cfset this.classID=Arguments.classID>
    <cfset this.startDateTime=Arguments.startDateTime>
    <cfset this.endDateTime=Arguments.endDateTime>
    <cfset this.dayOfWeekAsNumber=Arguments.dayOfWeekAsNumber>
    <cfset this.locationID=Arguments.locationID>
    <cfset this.locationName=Arguments.locationName>
    <cfset this.mapID=Arguments.mapID>
    <cfset this.mapVerified=Arguments.mapVerified>
    
    <cfreturn this>
  </cffunction>
  
  <cffunction name="getClassInstanceEntityByID" access="public" output="false" returntype="ClassInstanceEntity">
    <cfargument name="classInstanceID" type="numeric" required="yes">

    <cfquery name="qClassInstance" datasource="#this.datasource#">
      SELECT cl_classinstance.classID, cl_classinstance.locationID, cl_classinstance.startDateTime,
      		 cl_classinstance.endDateTime, cl_classinstance.dayOfWeekAsNumber,
             cl_location.locationName, cl_location.mapID, cl_location.posX AS mapVerified
      FROM cl_classinstance LEFT JOIN cl_location ON
      	   cl_classinstance.locationID=cl_location.locationID
      WHERE cl_classinstance.classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">
    </cfquery>
    <cfif qClassInstance.recordcount GT 0>
      <cfset this.classInstanceID=Arguments.classInstanceID>
      <cfset this.classID=qClassInstance.classID>
      <cfset this.startDateTime=qClassInstance.startDateTime>
      <cfset this.endDateTime=qClassInstance.endDateTime>
      <cfset this.dayOfWeekAsNumber=qClassInstance.dayOfWeekAsNumber>
      <cfset this.locationID=qClassInstance.locationID>
      <cfset this.locationName=qClassInstance.locationName>
      <cfif IsNumeric(qClassInstance.mapID)>
        <cfset this.mapID=qClassInstance.mapID>
      <cfelse>
        <cfset this.mapID=0>
      </cfif>
      <cfif IsNumeric(qClassInstance.mapVerified)><cfset this.mapVerified=qClassInstance.mapVerified></cfif>
    </cfif>
    <cfreturn this>
  </cffunction>
</cfcomponent>