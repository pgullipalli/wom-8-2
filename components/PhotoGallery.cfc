<cfcomponent displayname="Photo Gallery" extends="Base" output="No">
  <cffunction name="getAllAlbumCategories" access="public" output="no" returntype="query">
    <cfquery name="qCatSummary" datasource="#this.datasource#">
	  SELECT pg_albumcategory.albumCategory,
      	  	 pg_albumcategory.albumCategoryID
	  FROM (pg_albumcategory INNER JOIN pg_album_albumcategory ON
	  	   pg_albumcategory.albumCategoryID=pg_album_albumcategory.albumCategoryID) INNER JOIN pg_album ON
           pg_album_albumcategory.albumID=pg_album.albumID
      WHERE pg_album.published=1
	  GROUP by pg_albumcategory.albumCategoryID
	  ORDER by pg_albumcategory.albumCategory		
	</cfquery>
    
    <cfreturn qCatSummary>
  </cffunction>
  
  <cffunction name="getAlbumCategoryName" access="public" output="no" returntype="string">
    <cfargument name="albumCategoryID" required="yes" type="numeric">	
	<cfquery name="qAlbumCategory" datasource="#this.datasource#">
	  SELECT albumCategory
      FROM pg_albumcategory
      WHERE albumCategoryID=<cfqueryparam value="#Arguments.albumCategoryID#" cfsqltype="cf_sql_integer">
	</cfquery>
    <cfif qAlbumCategory.recordcount GT 0>
	  <cfreturn qAlbumCategory.albumCategory>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="getAlbumsByCategoryID" access="public" output="no" returntype="struct">
	<cfargument name="albumCategoryID" type="numeric" required="yes">
	<cfargument name="startIndex" required="no" default="1" type="numeric">
	<cfargument name="numItemsPerPage" required="no" default="20" type="numeric">
    
	<cfset var resultStruct = StructNew()>
    
    <cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.albums=QueryNew("albumID")>
    <cfset resultStruct.associatedProperties=QueryNew("albumID")>
  
	<cfquery name="qNumAlbums" datasource="#this.datasource#">
	  SELECT COUNT(pg_album.albumID) AS numAlbums
	  FROM pg_album INNER JOIN pg_album_albumcategory ON
	  	   pg_album.albumID=pg_album_albumcategory.albumID
      WHERE pg_album_albumcategory.albumCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumCategoryID#"> AND
      		pg_album.published=1
	</cfquery> 
	<cfset resultStruct.numAllItems=qNumAlbums.numAlbums>
    
    <cfquery name="qAlbums" datasource="#this.datasource#">
	  SELECT pg_album.albumID, pg_album.coverImage, pg_album.albumTitle, pg_album.albumDate,
      		 pg_album.published, pg_album.description			 
	  FROM   pg_album INNER JOIN pg_album_albumcategory ON
	         pg_album.albumID=pg_album_albumcategory.albumID   
	  WHERE  pg_album_albumcategory.albumCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumCategoryID#"> AND
      		 pg_album.published=1
	  ORDER  BY pg_album.albumDate DESC, pg_album.albumTitle ASC
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
    <cfset resultStruct.numDisplayedItems=qAlbums.recordcount>	
    <cfset resultStruct.albums = qAlbums>
		
	<cfreturn resultStruct> 
  </cffunction>
  
  <cffunction name="getAlbumTitle" access="public" output="no" returntype="string">
    <cfargument name="albumID" type="numeric" required="yes">
    
    <cfquery name="qAlbum" datasource="#this.datasource#">
      SELECT albumTitle
      FROM pg_album
      WHERE albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#"> AND
      		published=1
    </cfquery>
    <cfif qAlbum.recordcount GT 0>
      <cfreturn qAlbum.albumTitle>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="getAlbum" access="public" output="no" returntype="query">
    <cfargument name="albumID" type="numeric" required="yes">
    
    <cfquery name="qAlbum" datasource="#this.datasource#">
      SELECT *
      FROM pg_album
      WHERE albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#"> AND
      		published=1
    </cfquery>
    
	<cfreturn qAlbum>
  </cffunction>
  
  <cffunction name="getAlbumPhotos" access="public" output="no" returntype="struct">
    <cfargument name="albumID" type="numeric" required="yes">
	<cfargument name="startIndex" required="no" default="1" type="numeric">
	<cfargument name="numItemsPerPage" required="no" default="20" type="numeric">
    
	<cfset var resultStruct = StructNew()>
    
    <cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.photos=QueryNew("photoID")>
  
    <cfquery name="qNumPhotos" datasource="#this.datasource#">
	  SELECT COUNT(photoID) AS numPhotos
	  FROM pg_photo
      WHERE pg_photo.albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#"> AND
      		pg_photo.published=1
	</cfquery> 
	<cfset resultStruct.numAllItems=qNumPhotos.numPhotos>    
    
    <cfquery name="qPhotos" datasource="#this.datasource#">
	  SELECT title, photoFile, photoThumbnail, author, fileSize, description AS caption
	  FROM   pg_photo
      WHERE  albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#"> AND
      		 published=1
      ORDER  BY displaySeq
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery> 
    
    <cfset resultStruct.numDisplayedItems=qPhotos.recordcount>	
    <cfset resultStruct.photos = qPhotos>
    
    <cfreturn resultStruct>   
  </cffunction>

  <cffunction name="getAllAlbumPhotos" access="public" output="no" returntype="query">
    <cfargument name="albumID" type="numeric" required="yes">   
    
    <cfquery name="qPhotos" datasource="#this.datasource#">
	  SELECT title, photoFile, photoThumbnail, author, fileSize, description AS caption
	  FROM   pg_photo
      WHERE  albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#"> AND
      		 published=1
      ORDER  BY displaySeq
	</cfquery>
        
    <cfreturn qPhotos>   
  </cffunction>

  <cffunction name="getFeatureAlbumByCategoryID" access="public" output="no" returntype="struct">
    <cfargument name="albumCategoryID" type="numeric" required="yes">
    
	<cfset var resultStruct = StructNew()>
    <cfset resultStruct.albumID=0>    
    <cfset resultStruct.coverImage="">
	<cfset resultStruct.albumTitle="">
	<cfset resultStruct.description="">
  
	<cfquery name="qAlbumIDs" datasource="#this.datasource#">
	  SELECT DISTINCT pg_album.albumID
	  FROM pg_album INNER JOIN pg_album_albumcategory ON
	  	   pg_album.albumID=pg_album_albumcategory.albumID
      WHERE pg_album_albumcategory.albumCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumCategoryID#"> AND
      		pg_album.published=1 AND
            pg_album.featuredOnHomepage=1
	</cfquery> 
	
	<cfif qAlbumIDs.recordcount Is 0><cfreturn resultStruct></cfif>
    
    <cfif qAlbumIDs.recordcount GT 1>
      <cfset targetAlbumID=qAlbumIDs.albumID[RandRange(1,qAlbumIDs.recordcount)]>
    <cfelse>
      <cfset targetAlbumID=qAlbumIDs.albumID[1]>
    </cfif>
    
    <cfquery name="qAlbum" datasource="#this.datasource#">
	  SELECT albumID, albumTitle, coverImage, description
	  FROM pg_album
      WHERE albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#targetAlbumID#">
	</cfquery> 
    
    <cfset resultStruct.albumID=targetAlbumID>    
    <cfset resultStruct.coverImage=qAlbum.coverImage>
	<cfset resultStruct.albumTitle=qAlbum.albumTitle>
	<cfset resultStruct.description=qAlbum.description>

	<cfreturn resultStruct>  
  </cffunction>
</cfcomponent>