<cfcomponent displayname="Newsroom" extends="Base" output="no">    
  <cffunction name="getArticlesByCategoryID" access="public" output="false" returntype="struct">
    <cfargument name="articleCategoryID" required="yes" type="numeric">
	<cfargument name="startIndex" required="no" default="1" type="numeric">
	<cfargument name="numItemsPerPage" required="no" default="20" type="numeric">
    
	<cfset var resultStruct = StructNew()>
	<cfset var currentTime = now()>
    
    <cfif NOT Compare(Arguments.articleCategoryID,"0")>
    	<!--- articles from all categories --->
        <cfquery name="qAllArticles" datasource="#this.datasource#">
          SELECT COUNT(articleID) AS numArticles
          FROM nr_article
          WHERE published=1 AND
                publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
                unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
        </cfquery>
        <cfset resultStruct.numAllItems = qAllArticles.numArticles>
        
        <cfquery name="qArticles" datasource="#this.datasource#">
          SELECT articleID, articleDate, displayArticleDate,
                 contentType, documentFile, externalURL,				
                 headline, subHeadline, byline, featureThumbnail, popup,                 
                 CASE WHEN contentType=1 OR contentType=4
                 THEN CONCAT('index.cfm?md=newsroom&amp;tmp=detail','&amp;articleID=',CAST(articleID AS CHAR))
                 ELSE
                   CASE WHEN contentType=2
                   THEN externalURL
                   ELSE documentFile
                   END									
                 END AS href,				
                 teaser
          FROM nr_article
          WHERE published=1 AND
                publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
                unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
          ORDER BY articleDate DESC
          LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
        </cfquery>	
            
        <cfset resultStruct.numDisplayedItems=qArticles.recordcount>	
        <cfset resultStruct.articles = qArticles>    
    <cfelse>
        <cfquery name="qAllArticles" datasource="#this.datasource#">
          SELECT COUNT(nr_article.articleID) AS numArticles
          FROM nr_article INNER JOIN nr_article_articlecategory ON
               nr_article.articleID=nr_article_articlecategory.articleID
          WHERE nr_article_articlecategory.articleCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID#"> AND
                nr_article.published=1 AND
                nr_article.publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
                nr_article.unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
        </cfquery>
        <cfset resultStruct.numAllItems = qAllArticles.numArticles>
        
        <cfquery name="qArticles" datasource="#this.datasource#">
          SELECT nr_article.articleID, nr_article.articleDate, displayArticleDate,
                 nr_article.contentType, nr_article.documentFile, nr_article.externalURL,				
                 nr_article.headline, nr_article.subHeadline, nr_article.byline, nr_article.featureThumbnail, nr_article.popup,                 
                 CASE WHEN nr_article.contentType=1 OR nr_article.contentType=4
                 THEN CONCAT('index.cfm?md=newsroom&amp;tmp=detail','&amp;articleID=',CAST(nr_article.articleID AS CHAR))
                 ELSE
                   CASE WHEN nr_article.contentType=2
                   THEN nr_article.externalURL
                   ELSE nr_article.documentFile
                   END									
                 END AS href,				
                 nr_article.teaser
          FROM nr_article INNER JOIN nr_article_articlecategory ON
               nr_article.articleID=nr_article_articlecategory.articleID
          WHERE nr_article_articlecategory.articleCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID#"> AND
                nr_article.published=1 AND
                nr_article.publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
                nr_article.unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
          ORDER BY nr_article.articleDate DESC
          LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
        </cfquery>	
            
        <cfset resultStruct.numDisplayedItems=qArticles.recordcount>	
        <cfset resultStruct.articles = qArticles>    
    </cfif>

	<cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="getMostRecentArticlesByCategoryID" access="public" output="no" returntype="query">
    <cfargument name="articleCategoryID" type="numeric" required="yes">
    <cfargument name="numArticles" type="numeric" default="1">
    
    <cfset var currentTime = now()>
    
    <cfquery name="qArticle" datasource="#this.datasource#">
	  SELECT nr_article.articleID, nr_article.articleDate,
	  		 nr_article.contentType, nr_article.documentFile, nr_article.externalURL,				
	  		 nr_article.headline, nr_article.subHeadline, nr_article.byline, nr_article.featureThumbnail, nr_article.popup,			 
			 CASE WHEN nr_article.contentType=1  OR nr_article.contentType=4
			 THEN CONCAT('index.cfm?md=newsroom&amp;tmp=detail','&amp;articleID=',CAST(nr_article.articleID AS CHAR))
			 ELSE
			   CASE WHEN nr_article.contentType=2
			   THEN nr_article.externalURL
			   ELSE nr_article.documentFile
			   END									
			 END AS href,				
			 nr_article.teaser
	  FROM nr_article INNER JOIN nr_article_articlecategory ON
	       nr_article.articleID=nr_article_articlecategory.articleID
	  WHERE nr_article_articlecategory.articleCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID#"> AND
			nr_article.published=1 AND
			nr_article.publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
			nr_article.unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
	  ORDER BY nr_article.articleDate DESC
	  LIMIT 0,#Arguments.numArticles#
	</cfquery>
  
    <cfreturn qArticle>  
  </cffunction>
  
  <cffunction name="getMostRecentArticleByCategoryID" access="public" output="no" returntype="query">
    <cfargument name="articleCategoryID" type="numeric" required="yes">
    
    <cfset var currentTime = now()>
    
    <cfquery name="qArticle" datasource="#this.datasource#">
	  SELECT nr_article.articleID, nr_article.articleDate,
	  		 nr_article.contentType, nr_article.documentFile, nr_article.externalURL,				
	  		 nr_article.headline, nr_article.subHeadline, nr_article.byline, nr_article.featureThumbnail, nr_article.popup,			 
			 CASE WHEN nr_article.contentType=1 OR nr_article.contentType=4
			 THEN CONCAT('index.cfm?md=newsroom&amp;tmp=detail','&amp;articleID=',CAST(nr_article.articleID AS CHAR))
			 ELSE
			   CASE WHEN nr_article.contentType=2
			   THEN nr_article.externalURL
			   ELSE nr_article.documentFile
			   END									
			 END AS href,				
			 nr_article.teaser
	  FROM nr_article INNER JOIN nr_article_articlecategory ON
	       nr_article.articleID=nr_article_articlecategory.articleID
	  WHERE nr_article_articlecategory.articleCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID#"> AND
			nr_article.published=1 AND
			nr_article.publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
			nr_article.unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
	  ORDER BY nr_article.articleDate DESC
	  LIMIT 0,1
	</cfquery>
  
    <cfreturn qArticle>  
  </cffunction>
  
  <cffunction name="getArticleCategoryByCategoryID" access="public" output="no" returntype="query">
    <cfargument name="articleCategoryID" type="numeric" required="yes">
    <cfquery name="qCategory" datasource="#this.datasource#">
	  SELECT *
	  FROM nr_articlecategory
	  WHERE articleCategoryID=<cfqueryparam value="#Arguments.articleCategoryID#" cfsqltype="cf_sql_integer">
	</cfquery>  
	<cfreturn qCategory>
  </cffunction>
  
  <cffunction name="getArticleCategoryByArticleID" access="public" output="false" returntype="query">
    <cfargument name="articleID" type="numeric" required="yes">
    <cfquery name="qCategory" datasource="#this.datasource#">
	  SELECT nr_articlecategory.articleCategoryID, nr_articlecategory.articleCategory, nr_articlecategory.RSSEnabled, nr_articlecategory.displayArticleDate, nr_articlecategory.contactInfo
	  FROM nr_article_articlecategory JOIN nr_articlecategory ON
      	   nr_article_articlecategory.articleCategoryID = nr_articlecategory.articleCategoryID
	  WHERE nr_article_articlecategory.articleID=<cfqueryparam value="#Arguments.articleID#" cfsqltype="cf_sql_integer">
	</cfquery>  
	<cfreturn qCategory>
  </cffunction>
  
  <cffunction name="getCategoryIDByArticleID" access="public" output="no" returntype="numeric">
    <cfargument name="articleID" type="numeric" required="yes">
    <cfquery name="qCatID" datasource="#this.datasource#">
	  SELECT articleCategoryID
	  FROM nr_article_articlecategory
	  WHERE articleID=<cfqueryparam value="#Arguments.articleID#" cfsqltype="cf_sql_integer">
	</cfquery>  
    <cfif qCatID.recordcount GT 0>
	  <cfreturn qCatID.articleCategoryID>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
    
  <cffunction name="getArticleByID" access="public" output="false" returntype="query">
    <cfargument name="articleID" type="numeric" required="yes">
    <cfargument name="publishedOnly" type="boolean" default="1">
    
	<cfset var currentTime = now()>
	<cfquery name="qArticle" datasource="#this.datasource#">
	  SELECT contentType, articleDate, displayArticleDate, headline, subHeadline, byline, dateline,
      		 featureImage, featureImageCaption, featureImageAlignment,
             mediaFile, mediaFileURL, mediaFileType,
             CASE WHEN nr_article.contentType=1 OR nr_article.contentType=4
             THEN CONCAT('index.cfm?md=newsroom&amp;tmp=detail','&amp;articleID=',CAST(articleID AS CHAR))
			 ELSE
			   CASE WHEN contentType=2
			   THEN externalURL
			   ELSE documentFile
			   END									
			 END AS href,
  	  		 teaser, fullStory
	  FROM nr_article
	  WHERE articleID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleID#"> AND
      		<cfif Arguments.publishedOnly>
      		published=1 AND
            </cfif>
            publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
            unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">    
	</cfquery>
    <cfreturn qArticle>  
  </cffunction>
  
  <cffunction name="getRelatedItems" access="public" output="no" returntype="query">
    <cfargument name="articleID" required="yes" type="numeric">
    
    <cfquery name="qRelatedItems" datasource="#this.datasource#">
      SELECT nr_article_relateditem.articleRelatedItemID, nr_article_relateditem.relatedItemType,
      		 nr_article_relateditem.linkName, nr_article_relateditem.linkURL,
             nr_article_relateditem.fileTitle, nr_article_relateditem.filePath, nr_article_relateditem.fileSize,
      		 nr_article.articleID, nr_article.headline, nr_article.articleDate, nr_article.popup,             
			 CASE WHEN nr_article.contentType=1 OR nr_article.contentType=4
			 THEN CONCAT('index.cfm?md=newsroom&amp;tmp=detail','&amp;articleID=',CAST(nr_article.articleID AS CHAR))
			 ELSE
			   CASE WHEN nr_article.contentType=2
			   THEN nr_article.externalURL
			   ELSE nr_article.documentFile
			   END									
			 END AS href,
             pg_album.albumTitle, pg_album.albumID,
             nr_article_relateditem.displaySeq
      FROM (nr_article_relateditem LEFT JOIN nr_article ON nr_article_relateditem.articleID2=nr_article.articleID) LEFT JOIN pg_album ON
      		nr_article_relateditem.albumID=pg_album.albumID
      WHERE nr_article_relateditem.articleID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleID#">
      ORDER BY nr_article_relateditem.displaySeq
    </cfquery>
    
    <cfreturn qRelatedItems>
  </cffunction>
  
  <cffunction name="getFeatureArticleByCategoryID" access="public" output="no" returntype="struct">
    <cfargument name="articleCategoryID" type="numeric" required="yes">
    
    <cfset var resultStruct = StructNew()>
    <cfset resultStruct.articleID=0>
    <cfset resultStruct.headline="">
    <cfset resultStruct.teaser="">
    <cfset resultStruct.href="">
    <cfset resultStruct.popup=0>  
    
    <cfset currentTime = now()>
    
    <cfquery name="qArticleIDs" datasource="#this.datasource#">
      SELECT DISTINCT nr_article.articleID
      FROM nr_article_articlecategory INNER JOIN nr_article ON
      	   nr_article_articlecategory.articleID=nr_article.articleID
      WHERE nr_article_articlecategory.articleCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID#"> AND
      		nr_article.published=1 AND
      		nr_article.featuredOnHomepage=1 AND
            nr_article.publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
            nr_article.unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
    </cfquery>
    
    <cfif qArticleIDs.recordcount Is 0>
      <cfreturn resultStruct>
    <cfelseif qArticleIDs.recordcount GT 1>
      <cfset targetArticleID=qArticleIDs.articleID[RandRange(1,qArticleIDs.recordcount)]>
    <cfelse>
      <cfset targetArticleID=qArticleIDs.articleID[1]>
    </cfif>
    
    <cfquery name="qArticle" datasource="#this.datasource#">
      SELECT articleID, headline, popup,      		 
                 CASE WHEN contentType=1
                 THEN CONCAT('index.cfm?md=newsroom&amp;tmp=detail','&amp;articleID=',CAST(articleID AS CHAR))
                 ELSE
                   CASE WHEN contentType=2
                   THEN externalURL
                   ELSE documentFile
                   END									
                 END AS href,				
                 teaser
      FROM nr_article
      WHERE articleID=<cfqueryparam cfsqltype="cf_sql_integer" value="#targetArticleID#">
    </cfquery>
    
    <cfset resultStruct.articleID=qArticle.articleID>
    <cfset resultStruct.headline=qArticle.headline>
    <cfset resultStruct.teaser=qArticle.teaser>
    <cfset resultStruct.href=qArticle.href>
    <cfset resultStruct.popup=qArticle.popup>  
  
    <cfreturn resultStruct>
  </cffunction>
</cfcomponent>