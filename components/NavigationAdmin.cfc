<cfcomponent displayname="Navigation Administrator" extends="Base" output="false">
  <cffunction name="getGroupNameByNavGroupID" access="public" returntype="string" output="false" displayname="getGroupNameByNavGroupID">
	<cfargument name="navGroupID" type="numeric">
	<cfquery name="qGroup" datasource="#this.datasource#">
	  SELECT groupName
	  FROM nm_nav_group
	  WHERE navGroupID=<cfqueryparam value="#Arguments.navGroupID#" cfsqltype="cf_sql_integer">
	</cfquery>
	<cfif qGroup.recordcount GT 0>
	  <cfreturn qGroup.groupName>
	<cfelse>
	  <cfreturn "">
	</cfif>
  </cffunction>

  <cffunction name="getNumLevelsByNavGroupID" access="public" returntype="numeric" output="false" displayname="getNumLevelsByNavGroupID">
	<cfargument name="navGroupID" type="numeric" required="true">
	
	<cfquery name="qNumLevels" datasource="#this.datasource#">
	  SELECT numLevels
	  FROM nm_nav_group
	  WHERE navGroupID=<cfqueryparam value="#Arguments.navGroupID#" cfsqltype="cf_sql_integer">
	</cfquery>
	
	<cfreturn qNumLevels.numLevels>
  </cffunction>

  <cffunction name="getFirstNavGroupID" access="public" returntype="numeric" output="false" displayname="getFirstNavGroupID">
	<cfquery name="qGroup" datasource="#this.datasource#">
	  SELECT navGroupID
	  FROM nm_nav_group
	  ORDER BY displaySeq
	</cfquery>
    
    <cfif qGroup.recordcount GT 0>
	  <cfreturn qGroup.navGroupID>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>

  <cffunction name="getAllNavGroups" access="public" returntype="query" output="false" displayname="getAllNavGroups">
	<cfquery name="qGroups" datasource="#this.datasource#">
	  SELECT *
	  FROM nm_nav_group
	  ORDER BY displaySeq
	</cfquery>
	<cfreturn qGroups>
  </cffunction>

  <cffunction name="getNavTreeByNavGroupID" access="public" output="false" returntype="struct" displayname="getNavTreeByNavGroupID">
	<cfargument name="navGroupID" type="numeric" required="true">
		
	<cfset var nav = StructNew()>
	
	<cfset nav.numLevels=getNumLevelsByNavGroupID(Arguments.navGroupID)>
	
	<cfquery name="qAllNavs" datasource="#this.datasource#">
	  SELECT	nm_navrel.parentNavID,
	  			nm_navrel.displaySeq,
	  			nm_nav.navID,
	  			nm_nav.navName,
                nm_nav.navImage,
                nm_nav.templateID,
                nm_nav.pageTitle,
	  			nm_nav.pageType,			
				nm_nav.levelNum,
                nm_nav.resolvedURL,
				nm_nav.published						
	  FROM  nm_navrel JOIN nm_nav ON nm_navrel.navID = nm_nav.navID
	  WHERE nm_nav.navGroupID=<cfqueryparam value="#Arguments.navGroupID#" cfsqltype="cf_sql_integer">  		
	  ORDER BY nm_navrel.displaySeq
	</cfquery>    
    
	<cfloop index="idx" from="1" to="#nav.numLevels#">
	  <cfquery name="qLevel#idx#" dbtype="query">
	    SELECT *
	    FROM qAllNavs
	    WHERE levelNum=#idx#
	    ORDER BY displaySeq
	  </cfquery>	
	</cfloop>	

	<cfset nav.numSubNavs = 0>
	<cfset nav.nav = ArrayNew(1)>
	<cfset idx1=0>	
	<cfloop query="qLevel1"><!--- BEGIN: level 1 loop --->
	  <cfset idx1=idx1 + 1>
	  <cfset nav.numSubNavs=idx1>
	  <cfset nav.nav[idx1]=StructNew()>
	  <cfset nav.nav[idx1].parentNavID=parentNavID>
	  <cfset nav.nav[idx1].displaySeq=displaySeq>
	  <cfset nav.nav[idx1].navID=navID>
	  <cfset nav.nav[idx1].navName=navname>	  
      <cfset nav.nav[idx1].navImage=navImage>
      <cfset nav.nav[idx1].templateID=templateID>
	  <cfset nav.nav[idx1].pageType=pagetype>
      <cfset nav.nav[idx1].pageTitle=pageTitle>
      <cfset nav.nav[idx1].resolvedURL=resolvedURL>
	  <cfset nav.nav[idx1].published=published>
	  <cfset nav.nav[idx1].numSubNavs=0>
	  
	  <cfif nav.numLevels GTE 2><!--- At least 2 levels --->
		<cfset nav.nav[idx1].nav=ArrayNew(1)>
		<cfset idx2=0>
		<cfloop query="qLevel2"><!--- BEGIN: level 2 loop --->
		  <cfif parentNavID IS nav.nav[idx1].navID>
			<cfset idx2=idx2 + 1>
			<cfset nav.nav[idx1].numSubNavs=idx2>
			<cfset nav.nav[idx1].nav[idx2]=StructNew()>
			<cfset nav.nav[idx1].nav[idx2].parentNavID=parentNavID>
			<cfset nav.nav[idx1].nav[idx2].displaySeq=displaySeq>
			<cfset nav.nav[idx1].nav[idx2].navID=navID>
			<cfset nav.nav[idx1].nav[idx2].navName=navName>
            <cfset nav.nav[idx1].nav[idx2].navImage=navImage>
            <cfset nav.nav[idx1].nav[idx2].templateID=templateID>
			<cfset nav.nav[idx1].nav[idx2].pageType=pageType>
            <cfset nav.nav[idx1].nav[idx2].pageTitle=pageTitle>
            <cfset nav.nav[idx1].nav[idx2].resolvedURL=resolvedURL>
		    <cfset nav.nav[idx1].nav[idx2].published=published>
		    <cfset nav.nav[idx1].nav[idx2].numSubNavs=0>
		    
		    <cfif nav.numLevels GTE 3><!--- BEGIN: At least 3 levels --->
			  <cfset nav.nav[idx1].nav[idx2].nav=ArrayNew(1)>
			  <cfset idx3=0>
			  <cfloop query="qLevel3"><!--- BEGIN: level 3 loop --->
				<cfif parentNavID IS nav.nav[idx1].nav[idx2].navID>
				  <cfset idx3=idx3 + 1>
				  <cfset nav.nav[idx1].nav[idx2].numSubNavs=idx3>
				  <cfset nav.nav[idx1].nav[idx2].nav[idx3]=StructNew()>
				  <cfset nav.nav[idx1].nav[idx2].nav[idx3].parentNavID=parentNavID>
				  <cfset nav.nav[idx1].nav[idx2].nav[idx3].displaySeq=displaySeq>
				  <cfset nav.nav[idx1].nav[idx2].nav[idx3].navID=navID>
				  <cfset nav.nav[idx1].nav[idx2].nav[idx3].navName=navName>
                  <cfset nav.nav[idx1].nav[idx2].nav[idx3].navImage=navImage>
                  <cfset nav.nav[idx1].nav[idx2].nav[idx3].templateID=templateID>
				  <cfset nav.nav[idx1].nav[idx2].nav[idx3].pageType=pagetype>
                  <cfset nav.nav[idx1].nav[idx2].nav[idx3].pageTitle=pageTitle>
                  <cfset nav.nav[idx1].nav[idx2].nav[idx3].resolvedURL=resolvedURL>
				  <cfset nav.nav[idx1].nav[idx2].nav[idx3].published=published>
				  
				  <cfset nav.nav[idx1].nav[idx2].nav[idx3].numSubNavs=0>				  
				</cfif>
			  </cfloop><!--- END: level 3 loop --->
			</cfif><!--- END: At least 3 levels --->
		  </cfif>
		</cfloop><!--- END: level 2 loop --->
	  </cfif>
	</cfloop><!--- END: level 1 loop --->
	
    <cfreturn nav>
  </cffunction>

  <cffunction name="getLevelNumByNavID" access="public" returntype="numeric" output="false" displayname="getLevelNumByNavID">
	<cfargument name="navID" required="true" type="numeric">
	
	<cfquery name="qLevelNum" datasource="#this.datasource#">
	  SELECT levelNum
	  FROM nm_nav
	  WHERE navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navID#">
	</cfquery>
	<cfif qLevelNum.recordcount GT 0>
	  <cfreturn qLevelNum.levelNum>
	<cfelse>
	  <cfreturn 1>
	</cfif>
  </cffunction>

  <cffunction name="getParentNavID" access="public" returntype="numeric" output="false" displayname="getParentNavID">
	<cfargument name="navID" required="true" type="numeric">
	
	<cfquery name="qParentNavID" datasource="#this.datasource#">
	  SELECT parentNavID
	  FROM nm_navrel
	  WHERE navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navID#">
	</cfquery>
	<cfif qParentNavID.recordcount GT 0>
	  <cfreturn qParentNavID.parentNavID>
	<cfelse>
	  <cfreturn 0>
	</cfif>
  </cffunction>

  <cffunction name="getNavGroupIDByNavID" access="public" returntype="numeric" output="false" displayname="getNavGroupIDByNavID">
	<cfargument name="navID" type="numeric" required="true">
	<cfquery name="qNavGroupID" datasource="#this.datasource#">
	  SELECT navGroupID
	  FROM nm_nav
	  WHERE navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navID#">
	</cfquery>
	<cfreturn qNavGroupID.navGroupID>
  </cffunction>

  <cffunction name="getNavByNavID" access="public" returntype="query" output="false" displayname="getNavByNavID">
	<cfargument name="navID" required="true" type="numeric">
	
	<cfquery name="qNavDetail" datasource="#this.datasource#">
	  SELECT *
	  FROM nm_nav
	  WHERE navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navID#">
	</cfquery>
	<cfreturn qNavDetail>    
  </cffunction>

  <cffunction name="addNav" access="public" output="false" returntype="boolean" displayname="addNav">
	<cfargument name="navGroupID" type="numeric" required="true">
	<cfargument name="targetNavID" type="numeric" required="true">
	<cfargument name="insertMode" type="boolean" default="0">
	<cfargument name="pageTitle" required="true" type="string">
    <cfargument name="navName" required="yes" type="string">
    <cfargument name="navImage" type="string" default="">
    <cfargument name="navImageOver" type="string" default="">
    <cfargument name="templateID" type="numeric" default="0">
	<cfargument name="published" type="boolean" default="0">
	<cfargument name="popup" type="boolean" default="0">
	<cfargument name="SSLEnabled" type="boolean" default="0">
	<cfargument name="pageType" required="true" type="string">
	<cfargument name="URLQueryString" type="string" default="">	
	<cfargument name="externalLink" type="string" default="">
	<cfargument name="iframe" type="boolean" default="0">

	<cfif Not Compare(Arguments.pageTitle, "")>
	  <cfset Arguments.pageTitle=Arguments.navName>
	</cfif>
	
	<cfif Arguments.targetNavID IS 0><!--- add this nav to the bottom of the first level nav --->
	  <cfset levelNum=1>
	  <cfset parentNavID=0>
	  <cfquery name="qMaxDisplaySeq" datasource="#this.datasource#">
		SELECT MAX(nm_navrel.displaySeq) AS maxDisplaySeq
		FROM nm_navrel INNER JOIN nm_nav ON nm_navrel.navID=nm_nav.navID
		WHERE nm_navrel.parentNavID=0 AND
			  nm_nav.navGroupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navGroupID#">
	  </cfquery>
	  <cfif isNumeric(qMaxDisplaySeq.maxDisplaySeq)>
		<cfset newDisplaySeq=qMaxDisplaySeq.maxDisplaySeq+10>
	  <cfelse>
	    <cfset newDisplaySeq=10>
	  </cfif>
	<cfelse>
	  <cfif Arguments.insertMode><!--- add this nav as a sibling of the targetNavID --->
	    <cfquery name="qTargetNavInfo" datasource="#this.datasource#">
		  SELECT nm_navrel.parentNavID, nm_navrel.displaySeq, nm_nav.levelNum
		  FROM nm_navrel INNER JOIN nm_nav ON nm_navrel.navID=nm_nav.navID
		  WHERE nm_nav.navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.targetNavID#">
	    </cfquery>
	    <cfset levelNum=qTargetNavInfo.levelNum>
	    <cfset parentNavID=qTargetNavInfo.parentNavID>
	    <cfset newDisplaySeq=getNextDisplaySeq(Arguments.targetNavID)>
	  <cfelse><!--- add this nav as a child of the targetNavID --->
	    <cfset numLevels=getNumLevelsByNavGroupID(Arguments.navGroupID)>
	    <cfquery name="qTargetNavInfo" datasource="#this.datasource#">
		  SELECT nm_nav.levelNum
		  FROM nm_nav
		  WHERE nm_nav.navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.targetNavID#">
	    </cfquery>
		<cfif qTargetNavInfo.levelNum GTE numLevels>
		  <cfreturn false><!--- target nav is a leaf --->
		</cfif>
		<cfset levelNum=qTargetNavInfo.levelNum+1>
		<cfset parentNavID=Arguments.targetNavID>
		<cfquery name="qChildMaxDisplaySeq" datasource="#this.datasource#">
		  SELECT MAX(displaySeq) AS maxDisplaySeq
		  FROM nm_navrel
		  WHERE parentNavID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.targetNavID#">
		</cfquery>
		<cfif isNumeric(qChildMaxDisplaySeq.maxDisplaySeq)>
		  <cfset newDisplaySeq=qChildMaxDisplaySeq.maxDisplaySeq+10>
		<cfelse>
		  <cfset newDisplaySeq=10>
		</cfif>
	  </cfif>
	</cfif>
	
	<cfif Arguments.pageType IS "External Link" OR Arguments.pageType IS "Nothing">
	  <cfif Arguments.pageType IS "Nothing">
	    <cfset Arguments.externalLink = "">
		<cfset Arguments.iframe=0>
		<cfset Arguments.popup=0>
		<cfset resolvedURL="javascript:void(0)">
	  <cfelse>
	    <cfset resolvedURL="#Arguments.externalLink#">
	  </cfif>	
    <cfelse>
	  <cfset resolvedURL="index.cfm?#Arguments.URLQueryString#">
	</cfif>
	<cfquery datasource="#this.datasource#">
	  INSERT INTO nm_nav
		(navGroupID, navName, navImage, navImageOver, templateID, pageTitle, pageType, 
		 externalLink, iframe, popup, levelNum, published, SSLEnabled,
		 URLQueryString, resolvedURL, dateCreated, dateLastModified)
		values			
		(<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navGroupID#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.navName#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.navImage#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.navImageOver#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.templateID#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.pageTitle#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.pageType#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.externalLink#">,
		 <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.iframe#">,
		 <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.popup#">,
		 <cfqueryparam cfsqltype="cf_sql_integer" value="#levelNum#">,
		 <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
		 <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.SSLEnabled#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.URLQueryString#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#resolvedURL#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	</cfquery>	
	
	<cfquery name="qNewNavID" datasource="#this.datasource#">
	  SELECT MAX(navID) AS newNavID
	  FROM nm_nav
	</cfquery>

	<cfquery datasource="#this.datasource#">
	  INSERT INTO nm_navrel (parentNavID, navID, displaySeq, dateCreated, dateLastModified)
	  values (<cfqueryparam cfsqltype="cf_sql_integer" value="#parentNavID#">,
	  		  <cfqueryparam cfsqltype="cf_sql_integer" value="#qNewNavID.newNavID#">,
			  <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">,
			  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
			  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	</cfquery>  

	<cfreturn true>
  </cffunction>

  <cffunction name="editNav" access="public" output="false" returntype="void" displayname="addNav">
	<cfargument name="navID" type="numeric" required="true">
	<cfargument name="pageTitle" required="true" type="string">
    <cfargument name="navName" required="yes" type="string">
    <cfargument name="navImage" type="string" default="">
    <cfargument name="navImageOver" type="string" default="">
    <cfargument name="templateID" type="numeric" default="0">
	<cfargument name="published" type="boolean" default="0">
	<cfargument name="popup" type="boolean" default="0">
	<cfargument name="SSLEnabled" type="boolean" default="0">
	<cfargument name="pageType" required="true" type="string">
	<cfargument name="URLQueryString" required="true" type="string" default="">	
	<cfargument name="externalLink" type="string" default="">
	<cfargument name="iframe" type="boolean" default="0">

	<cfif Not Compare(Arguments.pageTitle, "")>
	  <cfset Arguments.pageTitle=Arguments.navName>
	</cfif>
	
	<cfif Arguments.pageType IS "External Link" OR Arguments.pageType IS "Nothing">
	  <cfif Arguments.pageType IS "Nothing">
	    <cfset Arguments.externalLink = "">
		<cfset Arguments.iframe=0>
		<cfset Arguments.popup=0>
		<cfset resolvedURL="javascript:void(0)">
	  <cfelse>
	    <cfset resolvedURL="#Arguments.externalLink#">
	  </cfif>	
    <cfelse>
	  <cfset resolvedURL="index.cfm?#Arguments.URLQueryString#">
	</cfif>
	<cfquery datasource="#this.datasource#">
	  UPDATE nm_nav
	  SET	navName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.navName#">,
      		navImage=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.navImage#">,
            navImageOver=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.navImageOver#">,
            templateID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.templateID#">,
	  		pageTitle=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.pageTitle#">,
	  		pageType=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.pageType#">,
	  		externalLink=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.externalLink#">,
	  		iframe=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.iframe#">,
	  		popup=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.popup#">,
	  		published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
	  		SSLEnabled=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.SSLEnabled#">,
	  		URLQueryString=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.URLQueryString#">,
	  		resolvedURL=<cfqueryparam cfsqltype="cf_sql_varchar" value="#resolvedURL#">,
	  		dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navID#">
	</cfquery>
  </cffunction>


  <cffunction name="deleteNav" access="public" returntype="boolean" output="false" displayname="deleteNav">
	<cfargument name="navID" required="true" type="numeric">
	
	<cfif Arguments.navID IS 0><cfreturn false></cfif>
	
	<cfquery name="qCheckChildNavs" datasource="#this.datasource#">
	  SELECT navID
	  FROM nm_navrel
	  WHERE parentNavID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navID#">
	</cfquery>
	<cfif qCheckChildNavs.recordcount GT 0>
	  <cfreturn false>
	</cfif>
    
    <cfquery name="qCheckTemplate" datasource="#this.datasource#">
      SELECT templateID
      FROM tm_template
      WHERE mainNavIDList='#Arguments.navID#' OR
      		mainNavIDList LIKE '#Arguments.navID#,%' OR
            mainNavIDList LIKE '%,#Arguments.navID#' OR
            mainNavIDList LIKE '%,#Arguments.navID#,%'
    </cfquery>
	<cfif qCheckTemplate.recordcount GT 0>
	  <cfreturn false>
	</cfif>
    
	<cfquery datasource="#this.datasource#">
	  DELETE FROM nm_navrel
	  WHERE navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navID#">
	</cfquery>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM nm_nav
	  WHERE navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navID#">
	</cfquery>
	
	<cfreturn true>
  </cffunction>

  <cffunction name="copyNav" access="public" returntype="numeric" output="false" displayname="copyNav">
	<cfargument name="navGroupID" required="true" type="numeric">
	<cfargument name="targetNavID" required="true" type="numeric"><!--- selected navID --->
	<cfargument name="navID" required="true" type="numeric"><!--- place it below this --->
	
	<cfset selectedNavDetail=getNavByNavID(Arguments.targetNavID)>
	<cfset destNavDetail=getNavByNavID(Arguments.navID)>
	
	<cfquery name="qMoreDestNavInfo" datasource="#this.datasource#">
	  SELECT *
	  FROM nm_navrel
	  WHERE navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navID#">
	</cfquery>
	
	<cfset newLevelNum=destNavDetail.levelNum>
	<cfset newParentNavID=qMoreDestNavInfo.parentNavID>
	<cfset newDisplaySeq=getNextDisplaySeq(Arguments.navID)>
	
	<cfquery datasource="#this.datasource#">
	  INSERT INTO nm_nav
		(navGroupID, published, navName, navImage, navImageOver, templateID, pageTitle, pageType, 
		 externalLink, iframe, popup, levelNum, SSLEnabled,
		 URLQueryString, resolvedURL, dateCreated, dateLastModified)
		values			
		(<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navGroupID#">,
		 <cfqueryparam cfsqltype="cf_sql_tinyint" value="#selectedNavDetail.published#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#selectedNavDetail.navName#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#selectedNavDetail.navImage#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#selectedNavDetail.navImageOver#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#selectedNavDetail.templateID#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#selectedNavDetail.pageTitle#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#selectedNavDetail.pageType#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#selectedNavDetail.externalLink#">,
		 <cfqueryparam cfsqltype="cf_sql_tinyint" value="#selectedNavDetail.iframe#">,
		 <cfqueryparam cfsqltype="cf_sql_tinyint" value="#selectedNavDetail.popup#">,
		 <cfqueryparam cfsqltype="cf_sql_integer" value="#newLevelNum#">,
		 <cfqueryparam cfsqltype="cf_sql_tinyint" value="#selectedNavDetail.SSLEnabled#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#selectedNavDetail.URLQueryString#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#selectedNavDetail.resolvedURL#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	</cfquery>

	<cfquery name="qNewNavID" datasource="#this.datasource#">
	  SELECT MAX(navID) AS newNavID
	  FROM nm_nav
	</cfquery>

	<cfquery datasource="#this.datasource#">
	  INSERT INTO nm_navrel (parentNavID, navID, displaySeq, dateCreated, dateLastModified)
	  values (<cfqueryparam cfsqltype="cf_sql_integer" value="#newParentNavID#">,
	  		  <cfqueryparam cfsqltype="cf_sql_integer" value="#qNewNavID.newNavID#">,
			  <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">,
			  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
			  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	</cfquery>
	
	<cfreturn qNewNavID.newNavID>
  </cffunction>

  <cffunction name="insertNav" access="public" returntype="numeric" output="false" displayname="insertNav">
	<cfargument name="navID" type="numeric" required="yes">
	<cfargument name="navGroupID" type="numeric" required="yes">
	
	<cfif Arguments.navID IS 0><!--- root element was selected --->
	  <cfquery name="qChildNavsInfo" datasource="#this.datasource#">
	    SELECT nm_nav.navID, nm_navrel.displaySeq
	    FROM nm_navrel INNER JOIN nm_nav ON nm_navrel.navID=nm_nav.navID
	    WHERE nm_navrel.parentNavID=0 AND nm_nav.navGroupID=<cfqueryparam cfsqltype="cf_sql_numeric" value="#Arguments.navGroupID#">
	    ORDER BY nm_navrel.displaySeq DESC
	  </cfquery>
	  <cfif qChildNavsInfo.recordcount GT 0>
	    <cfset Arguments.navID=qChildNavsInfo.navID>
	  </cfif>
	</cfif>
	
	<cfif Arguments.navID IS 0><!--- nothing under root element --->
		<cfset newLevelNum=1>
		<cfset newParentNavID=0>
		<cfset newDisplaySeq=10>
	<cfelse>
		<cfset selectedNavDetail=getNavByNavID(Arguments.navID)>
		<cfquery name="qDestNavInfo" datasource="#this.datasource#">
		  SELECT *
		  FROM nm_navrel
		  WHERE navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navID#">
		</cfquery>
		<cfset newLevelNum=selectedNavDetail.levelNum>
		<cfset newParentNavID=qDestNavInfo.parentNavID>
		<cfset newDisplaySeq=getNextDisplaySeq(Arguments.navID)>
	</cfif>
	
	<cfquery datasource="#this.datasource#">
	  INSERT INTO nm_nav
		(navGroupID, navName, pageTitle, pageType, 
		 levelNum, dateCreated, dateLastModified)
		values			
		(<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navGroupID#">,
		 'New Item',
		 '',
		 'Nothing',
		 <cfqueryparam cfsqltype="cf_sql_integer" value="#newLevelNum#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	</cfquery>
	
	<cfquery name="qNewNavID" datasource="#this.datasource#">
	  SELECT MAX(navID) AS newNavID
	  FROM nm_nav
	</cfquery>

	<cfquery datasource="#this.datasource#">
	  INSERT INTO nm_navrel (parentNavID, navID, displaySeq, dateCreated, dateLastModified)
	  values (<cfqueryparam cfsqltype="cf_sql_integer" value="#newParentNavID#">,
	  		  <cfqueryparam cfsqltype="cf_sql_integer" value="#qNewNavID.newNavID#">,
			  <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">,
			  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
			  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	</cfquery>
	
	<cfreturn qNewNavID.newNavID>	
  </cffunction>

  <cffunction name="moveUpNav" access="public" returntype="void" output="false" displayname="moveUpNav">
	<cfargument name="navID" type="numeric" required="yes">
	
	<cfif Arguments.navID IS 0><cfreturn></cfif>
	
	<cfquery name="qNavInfo" datasource="#this.datasource#">
	  SELECT nm_nav.navGroupID, nm_navrel.navRelID, nm_navrel.parentNavID, nm_navrel.displaySeq
	  FROM nm_nav INNER JOIN nm_navrel ON nm_nav.navID=nm_navrel.navID
	  WHERE	nm_nav.navID=<cfqueryparam value="#Arguments.navID#" cfsqltype="cf_sql_integer">  
	</cfquery>
	
	<cfquery name="qDisplaySeqs" datasource="#this.datasource#">
	  SELECT nm_navrel.navRelID, nm_navrel.displaySeq
	  FROM nm_navrel INNER JOIN nm_nav ON nm_navrel.navID=nm_nav.navID
	  WHERE	nm_nav.navGroupID=<cfqueryparam value="#qNavInfo.navGroupID#" cfsqltype="cf_sql_integer"> AND
	  	    nm_navrel.parentNavID=<cfqueryparam value="#qNavInfo.parentNavID#" cfsqltype="cf_sql_integer"> AND
            nm_navrel.navID<><cfqueryparam value="#Arguments.navID#" cfsqltype="cf_sql_integer"> AND
	  	    nm_navrel.displaySeq < <cfqueryparam cfsqltype="cf_sql_float" value="#qNavInfo.displaySeq#">
	  ORDER BY nm_navrel.displaySeq DESC
	</cfquery>
	
	<cfif qDisplaySeqs.recordcount GT 0>
	  <cfif qDisplaySeqs.recordcount GT 1>
		<cfset newDisplaySeq=(qDisplaySeqs.displaySeq[1] + qDisplaySeqs.displaySeq[2])/2>
	  <cfelse>
	    <cfset newDisplaySeq=qDisplaySeqs.displaySeq - 10>
	  </cfif>
	  
	  <cfquery datasource="#this.datasource#">
	    UPDATE nm_navrel
	    SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">
	    WHERE nm_navrel.navRelID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qNavInfo.navRelID#">
	  </cfquery>	  
	</cfif>
  </cffunction>

  <cffunction name="moveDownNav" access="public" returntype="void" output="false" displayname="moveUpNav">
	<cfargument name="navID" type="numeric" required="yes">
	
	<cfif Arguments.navID IS 0><cfreturn></cfif>
	
	<cfquery name="qNavInfo" datasource="#this.datasource#">
	  SELECT nm_nav.navGroupID, nm_navrel.navRelID, nm_navrel.parentNavID, nm_navrel.displaySeq
	  FROM nm_nav INNER JOIN nm_navrel ON nm_nav.navID=nm_navrel.navID
	  WHERE	nm_nav.navID=<cfqueryparam value="#Arguments.navID#" cfsqltype="cf_sql_integer">  
	</cfquery>
	
	<cfquery name="qDisplaySeqs" datasource="#this.datasource#">
	  SELECT nm_navrel.navRelID, nm_navrel.displaySeq
	  FROM nm_navrel INNER JOIN nm_nav ON nm_navrel.navID=nm_nav.navID
	  WHERE	nm_nav.navGroupID=<cfqueryparam value="#qNavInfo.navGroupID#" cfsqltype="cf_sql_integer"> AND
	  	    nm_navrel.parentNavID=<cfqueryparam value="#qNavInfo.parentNavID#" cfsqltype="cf_sql_integer"> AND
            nm_navrel.navID<><cfqueryparam value="#Arguments.navID#" cfsqltype="cf_sql_integer">  AND
	  	    nm_navrel.displaySeq > <cfqueryparam cfsqltype="cf_sql_float" value="#qNavInfo.displaySeq#">
	  ORDER BY nm_navrel.displaySeq ASC
	</cfquery>
	
	<cfif qDisplaySeqs.recordcount GT 0>
	  <cfif qDisplaySeqs.recordcount GT 1>
		<cfset newDisplaySeq=(qDisplaySeqs.displaySeq[1] + qDisplaySeqs.displaySeq[2])/2>
	  <cfelse>
	    <cfset newDisplaySeq=qDisplaySeqs.displaySeq + 10>
	  </cfif>
	  
	  <cfquery datasource="#this.datasource#">
	    UPDATE nm_navrel
	    SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">
	    WHERE nm_navrel.navRelID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qNavInfo.navRelID#">
	  </cfquery>	  
	</cfif>
  </cffunction>

  <cffunction name="resetDisplaySeq" access="public" output="false" displayname="resetDisplaySeq">
    <cfargument name="parentNavID" type="numeric" required="yes">
	<cfargument name="navGroupID" type="numeric" required="yes">
	<cfquery name="qDisplaySeqs" datasource="#this.datasource#">
	  SELECT nm_navrel.navRelID, nm_navrel.displaySeq
	  FROM nm_navrel INNER JOIN nm_nav ON nm_navrel.navID=nm_nav.navID
	  WHERE	nm_navrel.parentNavID=<cfqueryparam value="#Arguments.parentNavID#" cfsqltype="cf_sql_integer"> AND
		    nm_nav.navGroupID=<cfqueryparam value="#Arguments.navGroupID#" cfsqltype="cf_sql_integer">  
	  ORDER BY nm_navrel.displaySeq ASC
	</cfquery>
	
	<cfset idx = 0>
	<cfloop query="qDisplaySeqs">
	  <cfset idx = idx + 1>
	  <cfquery datasource="#this.datasource#">
	    UPDATE nm_navrel
		SET displaySeq=<cfqueryparam value="#idx#" cfsqltype="cf_sql_float">	  
		WHERE navRelID=<cfqueryparam value="#navRelID#" cfsqltype="cf_sql_integer">
	  </cfquery>	
	</cfloop>	
  </cffunction>

  <!--- Get a display sequence that will result greater than the target nav item but before the target nav item's following sibling --->
  <cffunction name="getNextDisplaySeq" access="public" returntype="numeric" output="false" displayname="getNextDisplaySeq">
	<cfargument name="navID" required="true" type="numeric">
	
	<cfquery name="qNavInfo" datasource="#this.datasource#">
	  SELECT nm_nav.navGroupID, nm_navrel.parentNavID, nm_navrel.displaySeq
	  FROM nm_nav INNER JOIN nm_navrel ON nm_nav.navID=nm_navrel.navID
	  WHERE	nm_nav.navID=<cfqueryparam value="#Arguments.navID#" cfsqltype="cf_sql_integer">  
	</cfquery>
	
	<cfquery name="qDisplaySeqs" datasource="#this.datasource#">
	  SELECT nm_navrel.navID, nm_navrel.displaySeq
	  FROM nm_navrel INNER JOIN nm_nav ON nm_navrel.navID=nm_nav.navID
	  WHERE	nm_nav.navGroupID=<cfqueryparam value="#qNavInfo.navGroupID#" cfsqltype="cf_sql_integer"> AND
	  	    nm_navrel.parentNavID=<cfqueryparam value="#qNavInfo.parentNavID#" cfsqltype="cf_sql_integer"> AND
            nm_navrel.navID<><cfqueryparam value="#Arguments.navID#" cfsqltype="cf_sql_integer"> AND
            nm_navrel.displaySeq > <cfqueryparam cfsqltype="cf_sql_float" value="#qNavInfo.displaySeq#">
	  ORDER BY nm_navrel.displaySeq ASC
	</cfquery>	  
	
	<cfif qDisplaySeqs.recordcount GT 0>
	  <cfreturn (qNavInfo.displaySeq + qDisplaySeqs.displaySeq)/2>
	<cfelse>
	  <cfreturn qNavInfo.displaySeq + 10>
	</cfif>  
  </cffunction>

  <!--- Get a display sequence that will result less than the target nav item but after the target nav item's previous sibling --->
  <cffunction name="getPrevDisplaySeq" access="public" returntype="numeric" output="false" displayname="getPrevDisplaySeq">
	<cfargument name="navID" required="true" type="numeric">
	
	<cfquery name="qNavInfo" datasource="#this.datasource#">
	  SELECT nm_nav.navGroupID, nm_navrel.parentNavID, nm_navrel.displaySeq
	  FROM nm_nav INNER JOIN nm_navrel ON nm_nav.navID=nm_navrel.navID
	  WHERE	nm_nav.navID=<cfqueryparam value="#Arguments.navID#" cfsqltype="cf_sql_integer">  
	</cfquery>
	
	<cfquery name="qDisplaySeqs" datasource="#this.datasource#">
	  SELECT nm_navrel.navID, nm_navrel.displaySeq
	  FROM nm_navrel INNER JOIN nm_nav ON nm_navrel.navID=nm_nav.navID
	  WHERE	nm_nav.navGroupID=<cfqueryparam value="#qNavInfo.navGroupID#" cfsqltype="cf_sql_integer"> AND
	  	    nm_navrel.parentNavID=<cfqueryparam value="#qNavInfo.parentNavID#" cfsqltype="cf_sql_integer"> AND
            nm_navrel.navID<><cfqueryparam value="#Arguments.navID#" cfsqltype="cf_sql_integer"> AND
	  	    nm_navrel.displaySeq < <cfqueryparam cfsqltype="cf_sql_float" value="#qNavInfo.displaySeq#">
	  ORDER BY nm_navrel.displaySeq DESC
	</cfquery>	  
	
	<cfif qDisplaySeqs.recordcount GT 0>
	  <cfreturn (qNavInfo.displaySeq + qDisplaySeqs.displaySeq)/2>
	<cfelse>
	  <cfreturn qNavInfo.displaySeq - 10>
	</cfif>  
  </cffunction>
  
  <cffunction name="getSubNavsByParentNavID" access="public" output="no" returntype="query">
	<cfargument name="navGroupID" type="numeric" required="yes">
	<cfargument name="parentNavID" type="numeric" default="0">
    
    <cfquery name="qNavs" datasource="#this.datasource#">
      SELECT nm_nav.navID, nm_nav.navName
      FROM nm_navrel INNER JOIN nm_nav ON nm_navrel.navID=nm_nav.navID
      WHERE nm_navrel.parentNavID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.parentNavID#"> AND
      		nm_nav.navGroupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navGroupID#">
      ORDER BY nm_navrel.displaySeq    
    </cfquery>
    
    <cfreturn qNavs>
  </cffunction>
</cfcomponent>