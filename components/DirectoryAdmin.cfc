<cfcomponent displayname="Directory Manager" extends="Base" output="false">
  
  <cffunction name="getAllCategories" access="public" output="no" returntype="query">
	<cfquery name="qAllCategories" datasource="#this.datasource#">
	  SELECT categoryID, categoryName
	  FROM dr_category
	  ORDER BY categoryName	
	</cfquery>	
    
    <cfreturn qAllCategories>  
  </cffunction>
  
  <cffunction name="getCategorySummary" access="public" output="no" returntype="query">
    <cfquery name="qCatSummary" datasource="#this.datasource#">
	  SELECT dr_category.categoryName,
      	  	 dr_category.categoryID,
			 COUNT(dr_item_category.itemID) AS numItems
	  FROM dr_category LEFT JOIN dr_item_category ON
	  	   dr_category.categoryID=dr_item_category.categoryID
	  GROUP by dr_category.categoryID
	  ORDER by dr_category.categoryName		
	</cfquery>
    
    <cfreturn qCatSummary>
  </cffunction>
  
  <cffunction name="getFirstCategoryID" access="public" output="no" returntype="numeric">
    <cfquery name="qCategoryID" datasource="#this.datasource#">
	  SELECT categoryID
	  FROM dr_category
      ORDER BY categoryName
	</cfquery>
    <cfif qCategoryID.recordcount GT 0>
	  <cfreturn qCategoryID.categoryID[1]>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="getCategory" access="public" output="no" returntype="query">
    <cfargument name="categoryID" required="yes" type="numeric">	
	<cfquery name="qCategory" datasource="#this.datasource#">
	  SELECT *
      FROM dr_category
      WHERE categoryID=<cfqueryparam value="#Arguments.categoryID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
	<cfreturn qCategory>
  </cffunction>
  
  <cffunction name="addCategory" access="public" output="no">
    <cfargument name="categoryName" type="string" required="yes">

    <cfquery datasource="#this.datasource#">
	  INSERT INTO dr_category
        (categoryName, dateCreated, dateLastModified)
	  VALUES
        (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.categoryName#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)	  
	</cfquery>  
  </cffunction>
  
  <cffunction name="editCategory" access="public" output="no">
    <cfargument name="categoryID" type="numeric" required="yes">
    <cfargument name="categoryName" type="string" required="yes">

    <cfquery datasource="#this.datasource#">
	  UPDATE dr_category
      SET categoryName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.categoryName#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">  
	</cfquery>  
  </cffunction>
    
  <cffunction name="deleteCategory" access="public" output="false" returntype="boolean">
    <cfargument name="categoryID" required="yes" type="numeric">
	
	<cfquery name="qItems" datasource="#this.datasource#">
	  SELECT itemID
	  FROM dr_item_category
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">			
	</cfquery>
	
	<cfif qItems.recordcount GT 0>
	  <cfreturn false>
	</cfif>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM dr_category
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
	</cfquery>
    
	<cfreturn true>
  </cffunction>
  
  <cffunction name="getItemsByCategoryID" access="public" output="false" returntype="struct">
    <cfargument name="categoryID" type="numeric" required="yes">
    <cfargument name="itemType" type="string" default="">
	<cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.items=QueryNew("itemID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qAllItemIDs" datasource="#this.datasource#">
	  SELECT COUNT(dr_item.itemID) AS numItems
	  FROM	 dr_item INNER JOIN dr_item_category ON
	  		 dr_item.itemID=dr_item_category.itemID
	  WHERE	 dr_item_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
      		 <cfif Compare(Arguments.itemType, "")>
             And itemType=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.itemType#">
             </cfif>
    </cfquery>
	<cfset resultStruct.numAllItems = qAllItemIDs.numItems>
	
	<cfquery name="qItems" datasource="#this.datasource#">
	  SELECT dr_item.itemID, dr_item.itemType, dr_item.published,
      		 dr_item.indivPrefix, dr_item.indivFirstName, dr_item.indivMiddleName, dr_item.indivLastName, dr_item.indivSuffix,
             dr_item.indivDivision, dr_item.indivJobTitle, dr_item.indivAddress, dr_item.indivCity, dr_item.indivState, dr_item.indivZIP,
             dr_item.indivEmail, dr_item.indivPhone, dr_item.indivFax, dr_item.indivPhotoFile,
             dr_item.orgName, dr_item.orgAddressPhysical, dr_item.orgCityPhysical, dr_item.orgStatePhysical, dr_item.orgZIPPhysical,
      		 dr_item.orgAddressMail, dr_item.orgCityMail, dr_item.orgStateMail, dr_item.orgZIPMail,
             dr_item.orgEmail, dr_item.orgPhone, dr_item.orgFax, dr_item.orgLogoFile, dr_item.orgWebAddress, dr_item.googleMapVerified, dr_item.latitude, dr_item.longitude,
             dr_item.mapID, dr_item.posX, dr_item.posY
	  FROM   dr_item INNER JOIN dr_item_category ON
	         dr_item.itemID=dr_item_category.itemID   
	  WHERE  dr_item_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
      		 <cfif Compare(Arguments.itemType, "")>
             And itemType=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.itemType#">
             </cfif>
      <cfif Compare(Arguments.itemType, "")>
        <cfif Not Compare(Arguments.itemType, "I")><!--- individual --->
	      ORDER  BY dr_item.indivLastName
        <cfelse><!--- organization --->
          ORDER  BY dr_item.orgName
        </cfif>
      <cfelse>
        ORDER  BY dr_item.itemType, dr_item.indivLastName, dr_item.orgName
      </cfif>
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
    <cfif qItems.recordcount GT 0>
      <cfquery name="qRelatedItems" datasource="#this.datasource#">
        SELECT dr_item.itemID, COUNT(dr_item_item_related.itemID2) AS numRelatedItems
        FROM dr_item LEFT JOIN dr_item_item_related ON dr_item.itemID=dr_item_item_related.itemID1        
        WHERE dr_item.itemID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qItems.itemID)#" list="yes">)
        GROUP BY dr_item.itemID
      </cfquery>
      
      <cfquery name="qItems" dbtype="query">
        SELECT *
        FROM qItems, qRelatedItems
        WHERE qItems.itemID=qRelatedItems.itemID
        <cfif Compare(Arguments.itemType, "")>
          <cfif Not Compare(Arguments.itemType, "I")><!--- individual --->
	        ORDER  BY qItems.indivLastName
          <cfelse><!--- organization --->
            ORDER  BY qItems.orgName
          </cfif>
        <cfelse>
          ORDER  BY qItems.itemType, qItems.indivLastName, qItems.orgName
        </cfif>
      </cfquery>    
    </cfif>   
    
    <cfif qItems.recordcount GT 0>            
      <cfset resultStruct.numDisplayedItems=qItems.recordcount>	
      <cfset resultStruct.items = qItems>
	<cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.items = QueryNew("itemID")>
    </cfif>
    
	<cfreturn resultStruct>
  </cffunction>  
  
  <cffunction name="getItemByID" access="public" output="no" returntype="query">
    <cfargument name="itemID" type="numeric" required="yes">
	<cfquery name="qItem" datasource="#this.datasource#">
	  SELECT *
	  FROM dr_item
      WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.itemID#">
	</cfquery>
    
	<cfreturn qItem>	
  </cffunction>
  
  <cffunction name="getCategoryIDListByItemID" access="public" output="no" returntype="string">
    <cfargument name="itemID" type="numeric" required="yes">
    
	<cfquery name="qCategories" datasource="#this.datasource#">
	  SELECT categoryID
      FROM dr_item_category
      WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.itemID#">
	</cfquery>
    
    <cfif qCategories.recordcount Is 0>
      <cfreturn "">
    <cfelse>
	  <cfreturn valueList(qCategories.categoryID)>	  
    </cfif>
  </cffunction>
  
  <cffunction name="getCategoryByID" access="public" output="no" returntype="query">
    <cfargument name="categoryID" required="yes" type="numeric">	
	<cfquery name="qCategory" datasource="#this.datasource#">
	  SELECT *
      FROM dr_category
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
	</cfquery>
	<cfreturn qCategory>
  </cffunction>

  <cffunction name="addItem" access="public" output="no" returntype="boolean">
    <cfargument name="categoryIDList" type="string" default="">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="itemType" type="string" default="I">
    <cfargument name="indivPrefix" type="string" default="">
    <cfargument name="indivFirstName" type="string" default="">
    <cfargument name="indivMiddleName" type="string" default="">
    <cfargument name="indivLastName" type="string" default="">
    <cfargument name="indivSuffix" type="string" default="">
    <cfargument name="indivDivision" type="string" default="">
    <cfargument name="indivJobTitle" type="string" default="">
    <cfargument name="indivAddress" type="string" default="">
    <cfargument name="indivCity" type="string" default="">
    <cfargument name="indivState" type="string" default="">
    <cfargument name="indivZIP" type="string" default="">
    <cfargument name="indivEmail" type="string" default="">
    <cfargument name="indivPhone" type="string" default="">
    <cfargument name="indivFax" type="string" default="">
    <cfargument name="indivPhotoFile" type="string" default="">
    <cfargument name="orgName" type="string" default="">
    <cfargument name="orgAddressPhysical" type="string" default="">
    <cfargument name="orgCityPhysical" type="string" default="">
    <cfargument name="orgStatePhysical" type="string" default="">
    <cfargument name="orgZIPPhysical" type="string" default="">
    <cfargument name="orgAddressMail" type="string" default="">
    <cfargument name="orgCityMail" type="string" default="">
    <cfargument name="orgStateMail" type="string" default="">
    <cfargument name="orgZIPMail" type="string" default="">
    <cfargument name="orgEmail" type="string" default="">
    <cfargument name="orgPhone" type="string" default="">
    <cfargument name="orgFax" type="string" default="">
    <cfargument name="orgDescription" type="string" default="">
    <cfargument name="orgLogoFile" type="string" default="">
    <cfargument name="orgWebAddress" type="string" default="">
    
    <cfif Not Compare(Arguments.categoryIDList, "")><cfreturn false></cfif>
    		
    <cfquery datasource="#this.datasource#">
	  INSERT INTO dr_item
        (itemType, published,
         indivPrefix, indivFirstName, indivMiddleName, indivLastName, indivSuffix,
         indivDivision, indivJobTitle, indivAddress, indivCity, indivState, indivZIP,
         indivEmail, indivPhone, indivFax, indivPhotoFile,
         orgName, orgAddressPhysical, orgCityPhysical, orgStatePhysical, orgZIPPhysical,
         orgAddressMail, orgCityMail, orgStateMail, orgZIPMail, orgEmail, orgPhone, orgFax,
         orgDescription, orgLogoFile, orgWebAddress, dateCreated, dateLastModified)
	  VALUES
	    (<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.itemType#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
         
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivPrefix#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivFirstName#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivMiddleName#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivLastName#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivSuffix#">,
         
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivDivision#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivJobTitle#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivAddress#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivCity#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivState#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivZIP#">,
         
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivEmail#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivPhone#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivFax#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivPhotoFile#">,
         
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgName#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgAddressPhysical#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgCityPhysical#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgStatePhysical#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgZIPPhysical#">,
         
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgAddressMail#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgCityMail#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgStateMail#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgZIPMail#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgEmail#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgPhone#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgFax#">,
         
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.orgDescription#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgLogoFile#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgWebAddress#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)		
	</cfquery>

	<cfquery name="qItemID" datasource="#this.datasource#">
	  SELECT MAX(itemID) AS newItemID
	  FROM dr_item
    </cfquery>
	<cfset Variables.itemID=qItemID.newItemID>	
    
    <cfloop index="categoryID" list="#Arguments.categoryIDList#">
      <cfquery datasource="#this.datasource#">
		INSERT INTO dr_item_category (itemID, categoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.itemID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>

  <cffunction name="editItem" access="public" output="no" returntype="boolean">
    <cfargument name="itemID" type="numeric" required="yes">
    <cfargument name="categoryIDList" type="string" default="">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="itemType" type="string" default="I">
    <cfargument name="indivPrefix" type="string" default="">
    <cfargument name="indivFirstName" type="string" default="">
    <cfargument name="indivMiddleName" type="string" default="">
    <cfargument name="indivLastName" type="string" default="">
    <cfargument name="indivSuffix" type="string" default="">
    <cfargument name="indivDivision" type="string" default="">
    <cfargument name="indivJobTitle" type="string" default="">
    <cfargument name="indivAddress" type="string" default="">
    <cfargument name="indivCity" type="string" default="">

    <cfargument name="indivState" type="string" default="">
    <cfargument name="indivZIP" type="string" default="">
    <cfargument name="indivEmail" type="string" default="">
    <cfargument name="indivPhone" type="string" default="">
    <cfargument name="indivFax" type="string" default="">
    <cfargument name="indivPhotoFile" type="string" default="">
    <cfargument name="orgName" type="string" default="">
    <cfargument name="orgAddressPhysical" type="string" default="">
    <cfargument name="orgCityPhysical" type="string" default="">
    <cfargument name="orgStatePhysical" type="string" default="">
    <cfargument name="orgZIPPhysical" type="string" default="">
    <cfargument name="orgAddressMail" type="string" default="">
    <cfargument name="orgCityMail" type="string" default="">
    <cfargument name="orgStateMail" type="string" default="">
    <cfargument name="orgZIPMail" type="string" default="">
    <cfargument name="orgEmail" type="string" default="">
    <cfargument name="orgPhone" type="string" default="">
    <cfargument name="orgFax" type="string" default="">
    <cfargument name="orgDescription" type="string" default="">
    <cfargument name="orgLogoFile" type="string" default="">
    <cfargument name="orgWebAddress" type="string" default="">
    
    <cfif Not Compare(Arguments.categoryIDList, "")><cfreturn false></cfif>
    
    <cfquery datasource="#this.datasource#">
	  UPDATE dr_item
      SET itemType=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.itemType#">,
          published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
          indivPrefix=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivPrefix#">,
          indivFirstName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivFirstName#">,
          indivMiddleName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivMiddleName#">,
          indivLastName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivLastName#">,
          indivSuffix=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivSuffix#">,
          
          indivDivision=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivDivision#">,
          indivJobTitle=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivJobTitle#">,
          indivAddress=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivAddress#">,
          indivCity=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivCity#">,
          indivState=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivState#">,
          indivZIP=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivZIP#">,
         
          indivEmail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivEmail#">,
          indivPhone=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivPhone#">,
          indivFax=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivFax#">,
          indivPhotoFile=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.indivPhotoFile#">,
         
          orgName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgName#">,
          orgAddressPhysical=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgAddressPhysical#">,
          orgCityPhysical=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgCityPhysical#">,
          orgStatePhysical=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgStatePhysical#">,
          orgZIPPhysical=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgZIPPhysical#">,
         
          orgAddressMail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgAddressMail#">,
          orgCityMail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgCityMail#">,
          orgStateMail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgStateMail#">,
          orgZIPMail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgZIPMail#">,
          orgEmail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgEmail#">,
          orgPhone=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgPhone#">,
          orgFax=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgFax#">,
         
          orgDescription=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.orgDescription#">,
          orgLogoFile=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgLogoFile#">,
          orgWebAddress=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orgWebAddress#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">
	</cfquery>

	<cfquery datasource="#this.datasource#">
      DELETE FROM dr_item_category
      WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">
    </cfquery>
    
    <cfloop index="categoryID" list="#Arguments.categoryIDList#">
      <cfquery datasource="#this.datasource#">
		INSERT INTO dr_item_category (itemID, categoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>
  
  <cffunction name="deleteItem" access="public" output="false" returntype="boolean">
    <cfargument name="itemID" required="yes" type="numeric">
    
    <cfquery name="qCourses" datasource="#this.datasource#">
      SELECT courseID
      FROM cl_course
      WHERE directoryItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">
    </cfquery>
    
    <cfif qCourses.recordcount GT 0>
      <cfreturn false>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM dr_item_category
      WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM dr_item
	  WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">
    </cfquery>
    
    <cfreturn true>
  </cffunction>
  
  <cffunction name="getItemsByKeyword" access="public" output="no" returntype="struct">
	<cfargument name="keyword" type="string" required="yes">
    <cfargument name="itemType" type="string" default="I">
    <cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.items=QueryNew("itemID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qAllItemIDs" datasource="#this.datasource#">
	  SELECT COUNT(itemID) AS numItems
	  FROM	 dr_item
      <cfif NOT Compare(Arguments.itemType, "I")>
	  WHERE	 indivFirstName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%"> OR
      		 indivMiddleName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%"> OR
             indivLastName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
      <cfelse>
      WHERE	 orgName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
      </cfif>      		 
    </cfquery>
	<cfset resultStruct.numAllItems = qAllItemIDs.numItems>
    
	<cfquery name="qItems" datasource="#this.datasource#">
	  SELECT itemID, itemType, published,
      		 indivPrefix, indivFirstName, indivMiddleName, indivLastName, indivSuffix,
             indivDivision, indivJobTitle, indivAddress, indivCity, indivState, indivZIP,
             indivEmail, indivPhone, indivFax, indivPhotoFile,
             orgName, orgAddressPhysical, orgCityPhysical, orgStatePhysical, orgZIPPhysical,
      		 orgAddressMail, orgCityMail, orgStateMail, orgZIPMail,
             orgEmail, orgPhone, orgFax, orgLogoFile, orgWebAddress, googleMapVerified, latitude, longitude,
             mapID, posX, posY
	  FROM dr_item
      <cfif NOT Compare(Arguments.itemType, "I")>
	  WHERE	 indivFirstName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%"> OR
      		 indivMiddleName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%"> OR
             indivLastName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
      <cfelse>
      WHERE	 orgName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
      </cfif>
	  ORDER BY itemType, indivLastName, orgName
      LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>	
    
    <cfif qItems.recordcount GT 0>
      <cfquery name="qRelatedItems" datasource="#this.datasource#">
        SELECT dr_item.itemID, COUNT(dr_item_item_related.itemID2) AS numRelatedItems
        FROM dr_item LEFT JOIN dr_item_item_related ON dr_item.itemID=dr_item_item_related.itemID1        
        WHERE dr_item.itemID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qItems.itemID)#" list="yes">)
        GROUP BY dr_item.itemID
      </cfquery>
      
      <cfquery name="qItems" dbtype="query">
        SELECT *
        FROM qItems, qRelatedItems
        WHERE qItems.itemID=qRelatedItems.itemID
        <cfif Compare(Arguments.itemType, "")>
          <cfif Not Compare(Arguments.itemType, "I")><!--- individual --->
	        ORDER  BY qItems.indivLastName
          <cfelse><!--- organization --->
            ORDER  BY qItems.orgName
          </cfif>
        <cfelse>
          ORDER  BY qItems.itemType, qItems.indivLastName, qItems.orgName
        </cfif>
      </cfquery>    
    </cfif>
    
    <cfif qItems.recordcount GT 0>      
      <cfset resultStruct.numDisplayedItems=qItems.recordcount>	
      <cfset resultStruct.items = qItems>
    <cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.items = QueryNew("itemID")>
	</cfif>

	<cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="getRelatedItems" access="public" output="no" returntype="query">
    <cfargument name="itemID" type="numeric" required="yes">
    
    <cfquery name="qItems" datasource="#this.datasource#">
      SELECT dr_item.itemID, dr_item.indivPrefix, indivFirstName, indivMiddleName, indivLastName, indivSuffix, orgName
      FROM dr_item_item_related INNER JOIN dr_item ON dr_item_item_related.itemID2=dr_item.itemID
      WHERE dr_item_item_related.itemID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">
      ORDER BY dr_item_item_related.displaySeq
    </cfquery>
    <cfreturn qItems>
  </cffunction>
  
  <cffunction name="addRelatedItems" access="public" output="no">
    <cfargument name="itemID" type="numeric" required="yes">
    <cfargument name="itemIDList" type="string" default="">
    
    <cfif Compare(Arguments.itemIDList,"")>
      <cfset relatedItems=getRelatedItems(Arguments.itemID)>
      <cfif relatedItems.recordcount GT 0>
        <cfset selectedItemIDList=ValueList(relatedItems.itemID)>
      <cfelse>
        <cfset selectedItemIDList="">
      </cfif>
      
      <cfquery name="qMaxSeq" datasource="#this.datasource#">
        SELECT MAX(displaySeq) AS seq
        FROM dr_item_item_related
        WHERE itemID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">
      </cfquery>
      <cfif IsNumeric(qMaxSeq.seq)>
        <cfset newSeq=qMaxSeq.seq+1>
      <cfelse>
        <cfset newSeq=0>
      </cfif>
        	  
      <cfloop index="newItemID" list="#Arguments.itemIDList#">
  		<cfif ListFind(selectedItemIDList, newItemID) Is 0>
          <cfset newSeq = newSeq + 1>
          <cfquery datasource="#this.datasource#">
            INSERT INTO dr_item_item_related (itemID1, itemID2, displaySeq)
            VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#">,
            		<cfqueryparam cfsqltype="cf_sql_integer" value="#newItemID#">,
                    <cfqueryparam cfsqltype="cf_sql_float" value="#newSeq#">)
          </cfquery>
        </cfif>
      </cfloop>
    </cfif>
  </cffunction>
  
  <cffunction name="deleteRelatedItems" access="public" output="no">
    <cfargument name="itemID" type="numeric" required="yes">
    <cfargument name="itemIDList" type="string" default="">
    
    <cfif Compare(Arguments.itemIDList,"")>
      <cfquery datasource="#this.datasource#">
        DELETE FROM dr_item_item_related
        WHERE itemID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#"> AND
      		  itemID2 IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.itemIDList#" list="yes">)
      </cfquery>
    </cfif>
  
  </cffunction>
  
  <cffunction name="moveRelatedItem" access="public" output="no">
    <cfargument name="itemID1" type="numeric" required="yes">
    <cfargument name="itemID2" type="numeric" required="yes">
    <cfargument name="direction" type="string" default="up">
    
    <cfquery name="qTargetDisplaySeq" datasource="#this.datasource#">
      SELECT displaySeq
      FROM dr_item_item_related
      WHERE itemID1=<cfqueryparam value="#Arguments.itemID1#" cfsqltype="cf_sql_integer"> AND
      		itemID2=<cfqueryparam value="#Arguments.itemID2#" cfsqltype="cf_sql_integer">
    </cfquery>
    <cfif qTargetDisplaySeq.recordcount IS 0><cfreturn></cfif>
    
    <cfset targetDisplaySeq=qTargetDisplaySeq.displaySeq>
    
    <cfif arguments.direction IS "up">
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT itemID2, displaySeq
        FROM dr_item_item_related
        WHERE itemID1=<cfqueryparam value="#Arguments.itemID1#" cfsqltype="cf_sql_integer"> AND
        	  displaySeq < <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq DESC
      </cfquery>
    <cfelse>
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT itemID2, displaySeq
        FROM dr_item_item_related
        WHERE itemID1=<cfqueryparam value="#Arguments.itemID1#" cfsqltype="cf_sql_integer"> AND
        	  displaySeq > <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq ASC
      </cfquery>
    </cfif>
    
    <cfif qNextDisplaySeq.recordcount Is 0><cfreturn></cfif>
    
    <cfset nextDisplaySeq=qNextDisplaySeq.displaySeq>
        
    <cfquery datasource="#this.datasource#">
      UPDATE dr_item_item_related
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#nextDisplaySeq#">
      WHERE itemID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID1#"> AND
			itemID2=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID2#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      UPDATE dr_item_item_related
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
      WHERE itemID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID1#"> AND
      		itemID2=<cfqueryparam cfsqltype="cf_sql_integer" value="#qNextDisplaySeq.itemID2#">
    </cfquery>  
  </cffunction>
  
  <cffunction name="getMapsByPageNum" access="public" output="no" returntype="struct">  
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="10">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.maps=QueryNew("mapID")>
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
    
    <cfquery name="qNumMaps" datasource="#this.datasource#">
	  SELECT COUNT(mapID) AS numItems
	  FROM	 dr_map
    </cfquery>
	<cfset resultStruct.numAllItems = qNumMaps.numItems>
    
    <cfquery name="qMaps" datasource="#this.datasource#">
      SELECT *
      FROM dr_map      
      ORDER BY mapName
      LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
    </cfquery>
    <cfset resultStruct.numDisplayedItems=qMaps.recordcount>	
    <cfset resultStruct.maps = qMaps>

	<cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="getMaps" access="public" output="no" returntype="query">  
    <cfquery name="qMaps" datasource="#this.datasource#">
      SELECT *
      FROM dr_map
      ORDER BY mapName
    </cfquery>
    <cfreturn qMaps>
  </cffunction>
  
  <cffunction name="getMap" access="public" output="no" returntype="query">
    <cfargument name="mapID" required="yes" type="numeric">
    
    <cfquery name="qMap" datasource="#this.datasource#">
      SELECT *
      FROM dr_map
      WHERE mapID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.mapID#">
    </cfquery>
    <cfreturn qMap>
  </cffunction>
  
  <cffunction name="addMap" access="public" output="no">
    <cfargument name="mapName" type="string" required="yes">
    <cfargument name="mapFile" type="string" required="yes">
    <cfargument name="featured" type="boolean" default="0">

    <cfquery datasource="#this.datasource#">
	  INSERT INTO dr_map
        (mapName, mapFile, featured, dateCreated, dateLastModified)
	  VALUES
        (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.mapName#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.mapFile#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.featured#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)	  
	</cfquery>  
  </cffunction>
  
  <cffunction name="editMap" access="public" output="no">
    <cfargument name="mapID" type="numeric" required="yes">
    <cfargument name="mapName" type="string" required="yes">
    <cfargument name="mapFile" type="string" required="yes">
    <cfargument name="featured" type="boolean" default="0">

    <cfquery datasource="#this.datasource#">
	  UPDATE dr_map
      SET mapName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.mapName#">,
      	  mapFile=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.mapFile#">,
          featured=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.featured#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE mapID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.mapID#">  
	</cfquery>  
  </cffunction>
    
  <cffunction name="deleteMap" access="public" output="false" returntype="boolean">
    <cfargument name="mapID" required="yes" type="numeric">
	
	<cfquery name="qItems" datasource="#this.datasource#">
	  SELECT itemID
	  FROM dr_item
      WHERE mapID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.mapID#">
	</cfquery>
	
	<cfif qItems.recordcount GT 0>
	  <cfreturn false>
	</cfif>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM dr_map
      WHERE mapID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.mapID#">
	</cfquery>
    <cfquery datasource="#this.datasource#">
	  UPDATE dr_item
      SET mapID=0, posX=0, posY=0,
      	  dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE mapID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.mapID#">
	</cfquery>
    
	<cfreturn true>
  </cffunction>
  
  <cffunction name="verifyGoogleMap" access="public" output="no">
    <cfargument name="itemID" required="yes" type="numeric">
    <cfargument name="googleMapVerified" type="boolean" default="0">
    <cfargument name="latitude" type="numeric" default="0">
    <cfargument name="longitude" type="numeric" default="0">
    
	<cfquery datasource="#this.datasource#">
	  UPDATE dr_item
      SET googleMapVerified=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.googleMapVerified#">,
      	  <cfif Arguments.googleMapVerified>
          latitude=<cfqueryparam cfsqltype="cf_sql_double" value="#Arguments.latitude#">,
          longitude=<cfqueryparam cfsqltype="cf_sql_double" value="#Arguments.longitude#">,
          <cfelse>
          latitude=0, longitude=0,
          </cfif>
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE itemID=<cfqueryparam value="#Arguments.itemID#" cfsqltype="cf_sql_integer">
	</cfquery>	
  </cffunction>
  
  <cffunction name="verifyCampusMap" access="public" output="no">
    <cfargument name="itemID" required="yes" type="numeric">
    <cfargument name="mapID" required="yes" type="numeric">
    <cfargument name="posX" type="numeric" default="0">
    <cfargument name="posY" type="numeric" default="0">
    
	<cfquery datasource="#this.datasource#">
	  UPDATE dr_item
      SET mapID=<cfqueryparam value="#Arguments.mapID#" cfsqltype="cf_sql_integer">,
          posX=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.posX#">,
          posY=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.posY#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE itemID=<cfqueryparam value="#Arguments.itemID#" cfsqltype="cf_sql_integer">
	</cfquery>	
  </cffunction>
</cfcomponent>