<cfcomponent displayname="Site Search Administrator" extends="Base" output="no">
  <cffunction name="getSiteContentCollectionName" access="public" output="no" returntype="string">
    <cfargument name="collectionType" type="string" default="db">
  
    <cfquery name="qCollectionName" datasource="#this.datasource#">
	  SELECT collectionName
	  FROM ss_collection
	  WHERE collectionType=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.collectionType#">
	</cfquery>
	<cfif qCollectionName.recordcount GT 0>
	  <cfreturn qCollectionName.collectionName[1]>
	<cfelse>
	  <cfreturn "">
	</cfif>
  </cffunction>
  
  <cffunction name="getSiteContentIndexStatus" access="public" output="no" returntype="boolean">
    <cfargument name="collectionType" type="string" default="db">
    <cfquery name="qStatus" datasource="#this.datasource#">
	  SELECT updateNeeded
	  FROM ss_collection
	  WHERE collectionType=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.collectionType#">
	</cfquery>
	<cfif qStatus.recordcount GT 0 AND qStatus.updateNeeded IS 1>
	  <cfreturn true>
	<cfelse>
	  <cfreturn false>
	</cfif>
  </cffunction>  
  
  <cffunction name="indexSiteContent" access="public" output="no">
	<cfargument name="directoryPath" type="string" required="yes">
	<cfargument name="urlPath" type="string" required="yes">
	<cfargument name="searchableFiles" type="string" required="yes">
	<cfif getSiteContentIndexStatus("db")>
	  <cfset indexDBContent()>
	</cfif>
    
    <cfif getSiteContentIndexStatus("class")>
	  <cfset indexClassContent()>
	</cfif>
    
    <cfif getSiteContentIndexStatus("directory")>
	  <cfset indexDirectoryContent()>
	</cfif>
	
	<cfif getSiteContentIndexStatus("file")>
	  <cfset indexFileContent(Arguments.directoryPath, Arguments.urlPath, Arguments.searchableFiles)>
	</cfif>  
  </cffunction>
  
  <cffunction name="indexDBContent" access="public" output="no">
	<cfset currentTime = now()>
    <cfset collectionName=getSiteContentCollectionName("db")>	

	<cfquery name="qSearchContent" datasource="#this.datasource#">
      <!--- PAGE BUILDER --->
	  SELECT CONCAT('a', CAST(pb_page.pageID AS char)) AS unique_id,
	  		 pb_page.pageID AS ID,
	  		 0 AS popup,
	         pb_page.pageName AS pageName,
			 'Page Builder' AS pageType,
             pb_pagecategory.pageCategoryName AS categoryName,		 
			 pb_page.pageContent AS fullStory,
			 '' AS secondaryStory
	  FROM  pb_page JOIN pb_pagecategory ON pb_page.pageCategoryID=pb_pagecategory.pageCategoryID
	  WHERE pb_page.isSearchable=1 AND pb_page.published=1

	  UNION
	  <!--- NEWSROOM --->
	  SELECT CONCAT('b', CAST(articleID AS char)) AS unique_id,
	  		 articleID AS ID,
	  		 0 AS popup,
	         headline AS pageName,
			 'Newsroom' AS pageType,
             'News' AS categoryName,		 
			 fullStory,
			 teaser AS secondaryStory
	  FROM 	 nr_article
	  WHERE  published=1 AND
			 publishdate <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#"> AND
			 unpublishdate > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
	  	  
	  UNION
	  <!--- PHOTOGALLERY --->
	  SELECT CONCAT('c', CAST(albumID AS char)) AS unique_id,
	  		 albumID AS ID,
	  		 0 AS popup,
	         albumTitle AS pageName,
			 'Photo Gallery' AS pageType,
             'Photo Gallery' AS categoryName,				 
			 description AS fullStory,
			 '' AS secondaryStory
	  FROM 	 pg_album
	  WHERE  published=1
      
      UNION
      <!--- DIRECTORY --->
      SELECT CONCAT('d', CAST(itemID AS char)) AS unique_id,
	  		 itemID AS ID,
	  		 0 AS popup,
             orgName AS pageName,
			 'Directory' AS pageType,
             'Directory' AS categoryName,				 
			 orgDescription AS fullStory,
			 '' AS secondaryStory
	  FROM 	 dr_item
	  WHERE  itemType='O' AND published=1    
      
      UNION
      <!--- DOCTOR --->
      SELECT CONCAT('e', CAST(dd_doctor.recordNum AS char)) AS unique_id,
	  		 dd_doctor.recordNum AS ID,
	  		 0 AS popup,
             CONCAT(dd_doctor.firstName, ' ', dd_doctor.lastName, ', ', dd_doctor.degree) AS pageName,
			 'Doctor' AS pageType,
             'Doctor Directory' AS categoryName,	 
			 dd_doctor_meta.bio AS fullStory,
			 CONCAT(dd_doctor.specialty, ' ', dd_doctor.officeName, ' ', dd_doctor.boardcert1, ' ', dd_doctor.boardcert2, ' ', dd_doctor.boardcert3) AS secondaryStory
	  FROM 	 dd_doctor LEFT JOIN dd_doctor_meta ON dd_doctor.recordNum=dd_doctor_meta.recordNum
      
      UNION
      <!--- EVENT  --->
      SELECT CONCAT('f', CAST(eventID AS char)) AS unique_id,
	  		 eventID AS ID,
	  		 0 AS popup,
             eventName AS pageName,
			 'Event' AS pageType,
             'Event' AS categoryName,	 
			 longDescription AS fullStory,
			 shortDescription AS secondaryStory
	  FROM 	 em_event
	  WHERE  published=1
      
      UNION
      <!--- COURSES --->
      SELECT CONCAT('g', CAST(courseID AS char)) AS unique_id,
	  		 courseID AS ID,
	  		 0 AS popup,
             courseTitle AS pageName,
			 'Class' AS pageType,
             'Class' AS categoryName,	 			 
			 longDescription AS fullStory,
			 shortDescription AS secondaryStory
	  FROM 	 cl_course
	  WHERE  published=1
      
      UNION
      <!--- FITNESS --->
      SELECT CONCAT('h', CAST(fitnessClassID AS char)) AS unique_id,
	  		 fitnessClassID AS ID,
	  		 0 AS popup,
             classTitle AS pageName,
			 'Fitness' AS pageType,
             'Fitness Club Class' AS categoryName,	 
			 description AS fullStory,
			 '' AS secondaryStory
	  FROM 	 cl_fitnessclass
	  WHERE  published=1
	</cfquery>   
    
	<cflock name="verity" timeout="600">
    <cfindex action="refresh"
			 collection="#collectionName#"
			 query="qSearchContent"			 
			 type="custom"
			 key="unique_id"
			 custom1="pageType"
			 custom2="popup"
             custom3="categoryName"
			 title="pageName"			 
			 body="pageName, fullStory, secondaryStory">   
    </cflock>
	
	<cfquery datasource="#this.datasource#">
	  UPDATE ss_collection
	  set updateNeeded=0, lastUpdate=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE collectionType='db'
	</cfquery>
  </cffunction>
  
  <cffunction name="indexClassContent" access="public" output="no">
	<cfset currentTime = now()>
    <cfset collectionName=getSiteContentCollectionName("class")>	

	<cfquery name="qSearchContent" datasource="#this.datasource#">      
      <!--- COURSES --->
      SELECT CONCAT('a', CAST(courseID AS char)) AS unique_id,
	  		 courseID AS ID,
	  		 0 AS popup,
             courseTitle AS pageName,
			 'Class' AS pageType,
             'Class' AS categoryName,	 			 
			 longDescription AS fullStory,
			 shortDescription AS secondaryStory
	  FROM 	 cl_course
	  WHERE  published=1
      
      UNION
      <!--- FITNESS --->
      SELECT CONCAT('b', CAST(fitnessClassID AS char)) AS unique_id,
	  		 fitnessClassID AS ID,
	  		 0 AS popup,
             classTitle AS pageName,
			 'Fitness' AS pageType,
             'Fitness Club Class' AS categoryName,	 
			 description AS fullStory,
			 '' AS secondaryStory
	  FROM 	 cl_fitnessclass
	  WHERE  published=1
	</cfquery>   

	<cflock name="verity" timeout="600">
    <cfindex action="refresh"
			 collection="#collectionName#"
			 query="qSearchContent"			 
			 type="custom"
			 key="unique_id"
			 custom1="pageType"
			 custom2="popup"
             custom3="secondaryStory"
			 title="pageName"			 
			 body="pageName, fullStory, secondaryStory">   
    </cflock>
	
	<cfquery datasource="#this.datasource#">
	  UPDATE ss_collection
	  set updateNeeded=0, lastUpdate=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE collectionType='class'
	</cfquery>
  </cffunction>
  
  <cffunction name="indexDirectoryContent" access="public" output="no">
	<cfset currentTime = now()>
    <cfset collectionName=getSiteContentCollectionName("directory")>	

	<cfquery name="qSearchContent" datasource="#this.datasource#">      
      SELECT CONCAT('a', CAST(itemID AS char)) AS unique_id,
	  		 itemID AS ID,
	  		 0 AS popup,
             orgName AS pageName,
			 'Directory' AS pageType,
             'Directory' AS categoryName,				 
			 orgDescription AS fullStory,
			 '' AS secondaryStory
	  FROM 	 dr_item
	  WHERE  itemType='O' AND published=1
	</cfquery>   

	<cflock name="verity" timeout="600">
    <cfindex action="refresh"
			 collection="#collectionName#"
			 query="qSearchContent"			 
			 type="custom"
			 key="unique_id"
			 custom1="pageType"
			 custom2="popup"
             custom3="secondaryStory"
			 title="pageName"			 
			 body="pageName, fullStory, secondaryStory">   
    </cflock>
	
	<cfquery datasource="#this.datasource#">
	  UPDATE ss_collection
	  set updateNeeded=0, lastUpdate=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE collectionType='directory'
	</cfquery>
  </cffunction>
  
  <cffunction name="indexFileContent" access="public" output="no">
    <cfargument name="directoryPath" type="string" required="yes">
	<cfargument name="urlPath" type="string" required="yes">
	<cfargument name="searchableFiles" type="string" required="yes">
  
    <cfset collectionName=getSiteContentCollectionName('file')>
    <cflock name="verity" timeout="600">
    <cfindex action="refresh"
			 collection="#collectionName#"
			 type="path"
			 key="#Arguments.directoryPath#"
			 urlPath="#Arguments.urlPath#"
			 recurse="yes"			 
			 extensions="#Arguments.searchableFiles#">
	</cflock> 
  
    <cfquery datasource="#this.datasource#">
	  UPDATE ss_collection
	  set updateNeeded=0, lastUpdate=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE collectionType='file'
	</cfquery>
  </cffunction>
  
  <cffunction name="updateSiteSearchIndexStatus" access="public" output="no">
    <cfargument name="md" type="string" default="">
	<cfargument name="task" type="string" default="">
	<cfargument name="dir" type="string" default="">
    <cfargument name="relativeDirectory" type="string" default="">
    <cfargument name="tmp" type="string" default="">
  
  	<cfswitch expression="#Arguments.md#">
	  <cfcase value="pagebuilder">	  
	    <cfif ListFindNoCase("addPage,editPage,deletePage,copyPage", Arguments.task) GT 0><cfset changeSiteSearchIndexStatus(1, "db")></cfif>
	  </cfcase>
	  <cfcase value="newsroom">
	    <cfif ListFindNoCase("addArticle,editArticle,deleteArticle,copyArticle", Arguments.task) GT 0><cfset changeSiteSearchIndexStatus(1, "db")></cfif>
	  </cfcase>	
	  <cfcase value="photogallery">
	    <cfif ListFindNoCase("addAlbum,editAlbum,deleteAlbum", Arguments.task) GT 0><cfset changeSiteSearchIndexStatus(1, "db")></cfif>
	  </cfcase>	
      <cfcase value="directory">
        <cfif ListFindNoCase("addItem,editItem,deleteItem", Arguments.task) GT 0><cfset changeSiteSearchIndexStatus(1, "db")><cfset changeSiteSearchIndexStatus(1, "directory")></cfif>
      </cfcase>
      <cfcase value="doctor">
        <cfif ListFindNoCase("importDoctorData,editDoctor", Arguments.task) GT 0><cfset changeSiteSearchIndexStatus(1, "db")></cfif>
      </cfcase>
      <cfcase value="event">
        <cfif ListFindNoCase("addEvent,editEvent,deleteEvent", Arguments.task) GT 0><cfset changeSiteSearchIndexStatus(1, "db")></cfif>
      </cfcase>
      <cfcase value="class">
        <cfif ListFindNoCase("addCourse,editCourse,deleteCourse,addService,editService,deleteService,addCurricula,editCurricula,deleteCurricula,addFitnessClass,editFitnessClass,deleteFitnessClass", Arguments.task) GT 0><cfset changeSiteSearchIndexStatus(1, "db")><cfset changeSiteSearchIndexStatus(1, "class")></cfif>
      </cfcase>
	  <cfcase value="file">
        <cfif (Not Compare(URL.tmp,"snip_upload_action") OR Not Compare(URL.task,"deleteFiles")) AND Find("docs", Arguments.dir) GT 0>
          <cfset changeSiteSearchIndexStatus(1, "file")>
        </cfif>
	  </cfcase>
	</cfswitch>  
  </cffunction>
  
  <cffunction name="changeSiteSearchIndexStatus" access="public" output="false">
    <cfargument name="updateNeeded" type="boolean" default="1">
	<cfargument name="collectionType" type="string" default="db">
	
	<cfquery datasource="#this.datasource#">
	  UPDATE ss_collection
	  SET updateNeeded=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.updateNeeded#">
	  WHERE collectionType=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.collectionType#">
	</cfquery>	
  </cffunction>
</cfcomponent>