<cfcomponent displayname="Template" extends="Base" output="false">
  <cffunction name="getTemplate" access="public" output="no" returntype="struct">
    <cfargument name="md" type="string" default="">
    <cfargument name="tmp" type="string" default="">
    <cfargument name="nid" type="numeric" default="0">
    <cfargument name="pnid" type="numeric" default="0">
    <cfargument name="tid" type="numeric" default="0">
        
    <cfset var tempStruct=StructNew()>
    
    <!--- tempStruct.navID, tempStruct.parentNavID, tempStruct.templateID will be determined --->
    <cfif Compare(Arguments.nid,0)>
      <!--- nid != 0 --->      
      <cfset tempStruct.navID=Arguments.nid>
      <cfif Compare(Arguments.pnid,0) And Compare(Arguments.tid,0)>
        <cfset tempStruct.parentNavID=Arguments.pnid>
        <cfset tempStruct.templateID=Arguments.tid>
      <cfelse>
    	<cfquery name="qNavInfo" datasource="#this.datasource#">
          SELECT nm_nav.templateID, nm_navrel.parentNavID
          FROM nm_nav INNER JOIN nm_navrel ON nm_nav.navID=nm_navrel.navID
          WHERE nm_nav.navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.nid#">
        </cfquery>
        <cfif qNavInfo.recordcount GT 0>
          <cfset tempStruct.parentNavID=qNavInfo.parentNavID>
          <cfset tempStruct.templateID=qNavInfo.templateID>
        <cfelse>
          <cfset tempStruct.parentNavID=0>
          <cfset tempStruct.templateID=Arguments.tid>
        </cfif>
      </cfif>      
    <cfelse>
      <!--- nid == 0 --->
      <!--- use md and tmp to find nid via reverse engineering --->
      <cfset Variables.URLQueryString="md=#Arguments.md#&amp;tmp=#Arguments.tmp#">
      <cfswitch expression="#Arguments.md#">
        <cfcase value="pagebuilder">
          <cfif IsDefined("Arguments.pid")>
            <cfset Variables.URLQueryString="#Variables.URLQueryString#&amp;pid=#Arguments.pid#">            
            <cfquery name="qPage" datasource="#this.datasource#">
              SELECT defaultTemplateID
              FROM pb_page
              WHERE pageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pid#">
            </cfquery>
            <cfif Compare(qPage.defaultTemplateID,0)>
              <cfset Variables.defaultTemplateID=qPage.defaultTemplateID>
            </cfif>
          </cfif>
        </cfcase>
        <cfcase value="newsroom">
          <cfif IsDefined("Arguments.catid")>
            <cfset Variables.URLQueryString="#Variables.URLQueryString#&amp;catid=#Arguments.catid#">
          </cfif>
        </cfcase>
        <cfcase value="homepage">
          <cfif IsDefined("Arguments.shpid")>
            <cfset Variables.URLQueryString="#Variables.URLQueryString#&amp;shpid=#Arguments.shpid#">
          </cfif>
        </cfcase>

        
        <!--- need mores work here --->       
      </cfswitch>
      
      <cfif IsDefined("Variables.defaultTemplateID")>
        <cfset tempStruct.navID=0>
        <cfset tempStruct.parentNavID=0>
        <cfset tempStruct.templateID=Variables.defaultTemplateID>
      <cfelse>
        <cfquery name="qNavInfo" datasource="#this.datasource#">
          SELECT nm_nav.navID, nm_nav.templateID, nm_navrel.parentNavID
          FROM nm_nav INNER JOIN nm_navrel ON nm_nav.navID=nm_navrel.navID
          WHERE nm_nav.URLQueryString=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Variables.URLQueryString#">
        </cfquery>
      
        <cfif qNavInfo.recordcount GT 0>
          <cfset tempStruct.navID=qNavInfo.navID>
          <cfset tempStruct.parentNavID=qNavInfo.parentNavID>
          <cfset tempStruct.templateID=qNavInfo.templateID>
        <cfelse>
          <cfset tempStruct.navID=0>
          <cfset tempStruct.parentNavID=0>
          <cfset tempStruct.templateID=Arguments.tid>
        </cfif> 
      </cfif>  
    </cfif>
    
    <!--- At this point, these have been defined: tempStruct.navID, tempStruct.parentNavID, tempStruct.templateID  --->
    
    <cfif Compare(Arguments.tid,0)>
      <!--- overwrite templatID if passed parameter tid is not zero --->
      <cfset tempStruct.templateID=Arguments.tid>
    </cfif>
    
    <cfif Compare(tempStruct.navID,0)>
      <cfset NV=CreateObject("component","com.Navigation").init()>
      <cfset tempStruct.topMostAscendantNavID=NV.getTopMostAscendantNavID(tempStruct.navID)>
    <cfelse>
      <cfset tempStruct.topMostAscendantNavID=0>
    </cfif>
   
    <cfif Compare(tempStruct.templateID, 0)>
      <cfquery name="qTemplate" datasource="#this.datasource#">
  	    SELECT topperStyle, sectionTitle, topperImage, mainNavIDList, themeImage
        FROM tm_template
        WHERE templateID=<cfqueryparam cfsqltype="cf_sql_integer" value="#tempStruct.templateID#">
      </cfquery>
    </cfif>

    <cfif Compare(tempStruct.templateID, 0) And qTemplate.recordcount GT 0>
      <cfset tempStruct.topperStyle=qTemplate.topperStyle>
      <cfset tempStruct.sectionTitle=qTemplate.sectionTitle>
      <cfset tempStruct.topperImage=qTemplate.topperImage>
      <cfset tempStruct.mainNavIDList=qTemplate.mainNavIDList>
      <cfset tempStruct.themeImage=qTemplate.themeImage>
    <cfelse>
      <!--- use md and tmp and other arguments to determine template properties --->
      <cfset tempStruct.topperStyle="ST">
      <cfset tempStruct.sectionTitle="">
      <cfset tempStruct.topperImage="">
      <cfset tempStruct.mainNavIDList="">
      <cfset tempStruct.themeImage="">
    </cfif>
    
    <cfreturn tempStruct>
  </cffunction>
</cfcomponent>