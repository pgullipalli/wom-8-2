<cfcomponent displayname="ADM_AccessAdmin" extends="Base" output="false">
  <cffunction name="getAllAdministrators" access="public" output="false" returntype="query" displayname="getAllAdministrators">
    <cfquery name="qAllAdmins" datasource="#this.datasource#">
	  select *
	  from am_administrator
	  where isMightyUser=0
	  order by username
	</cfquery>
	<cfreturn qAllAdmins>
  </cffunction>

  <cffunction name="addAdmin" access="public" output="false" returntype="struct">
    <cfargument name="isSuperUser" type="boolean" default="0">
    
    <cfset var returnedStruct=StructNew()>
    <cfset returnedStruct.SUCCESS=true>
    <cfset returnedStruct.MESSAGE="">
	
	<cfquery name="getUsername" datasource="#this.datasource#">
	  SELECT administratorID
	  FROM am_administrator
	  WHERE username=<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.username#">
	</cfquery>
    <cfif getUsername.recordcount Gt 0>
      <cfset returnedStruct.SUCCESS=false>
      <cfset returnedStruct.MESSAGE="The username ""#arguments.username#"" has been used. Please choose other username.">
      <cfreturn returnedStruct>
	</cfif>
	
	<cfif NOT arguments.isSuperUser AND NOT IsDefined("arguments.privilegeGroupID")>
      <cfset returnedStruct.SUCCESS=false>
      <cfset returnedStruct.MESSAGE="Please select at least a privilege group for this administrator.">
	  <cfreturn returnedStruct>
	</cfif>
  
    <cfquery datasource="#this.datasource#">
	  INSERT INTO am_administrator (firstName, lastName, username, password, email, isSuperUser, dateCreated, dateLastModified)
	  VALUES (
	    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.firstName#">,
	    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.lastName#">,
	    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.username#">,
	    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.password#">,
	    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#">,
	    <cfqueryparam cfsqltype="cf_sql_tinyint" value="#arguments.isSuperUser#">,
        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  )	
	</cfquery>
	
	<cfif NOT arguments.isSuperUser>
      <cfquery name="qAdminID" datasource="#this.datasource#">
	    SELECT MAX(administratorID) AS ID FROM am_administrator
	  </cfquery>
	  <cfset newAdminID=qAdminID.ID>
    
	  <cfset privilegeGroupIDArray=ArrayNew(1)>
	  <cfset privilegeGroupIDArray=ListToArray(arguments.privilegeGroupID)>
	  <cfloop index="idx1" from="1" to="#arrayLen(privilegeGroupIDArray)#">
	    <cfif Not IsDefined("arguments.privilegeID_#privilegeGroupIDArray[idx1]#")>
		  <cfset deleteAdmin(newAdminID)>
          <cfset returnedStruct.SUCCESS=false>
          <cfset returnedStruct.MESSAGE="Please select at least an access privilege for all access privilege groups you have selected for this administrator.">
	      <cfreturn returnedStruct>
		</cfif>
		<cfset privilegeIDList=Evaluate("arguments.privilegeID_#privilegeGroupIDArray[idx1]#")>
	    <cfset privilegeIDArray=ArrayNew(1)>
		<cfset privilegeIDArray=ListToArray(privilegeIDList)>
		<cfloop index="idx2" from="1" to="#arrayLen(privilegeIDArray)#">
		  <cfif Not IsDefined("arguments.categoryID_#privilegeGroupIDArray[idx1]#_#privilegeIDArray[idx2]#")>
		    <cfset arguments.categoryIDList="-1">
		  <cfelse>
		    <cfset arguments.categoryIDList=Evaluate("arguments.categoryID_#privilegeGroupIDArray[idx1]#_#privilegeIDArray[idx2]#")>
		  </cfif>
		  <cfif ListFind(arguments.categoryIDList, "-1") Is Not 0>
		    <cfset arguments.categoryIDList="-1">
		  </cfif>
		  <cfquery datasource="#this.datasource#">
		    INSERT INTO am_administrator_privilege
			(administratorID, privilegeGroupID, privilegeID, categoryIDList)
			VALUES (
			  <cfqueryparam cfsqltype="cf_sql_integer" value="#newAdminID#">,
			  <cfqueryparam cfsqltype="cf_sql_integer" value="#privilegeGroupIDArray[idx1]#">,
			  <cfqueryparam cfsqltype="cf_sql_integer" value="#privilegeIDArray[idx2]#">,
			  <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.categoryIDList#">
			)
		  </cfquery>		  
		</cfloop>
	  </cfloop>	
	</cfif> 
    <cfreturn returnedStruct>
  </cffunction>

  <cffunction name="editAdmin" access="public" output="false" returntype="struct">
    <cfargument name="administratorID" type="numeric" required="yes">
    <cfargument name="isSuperUser" type="boolean" default="0">
    
    <cfset var returnedStruct=StructNew()>
    <cfset returnedStruct.SUCCESS=true>
    <cfset returnedStruct.MESSAGE="">
	
	<cfquery name="getUsername" datasource="#this.datasource#">
	  SELECT administratorID
	  FROM am_administrator
	  WHERE username=<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.username#"> AND
	  		administratorID<><cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.administratorID#">
	</cfquery>
    <cfif getUsername.recordcount Gt 0>
      <cfset returnedStruct.SUCCESS=false>
      <cfset returnedStruct.MESSAGE="The username ""#arguments.username#"" has been used. Please choose other username.">
      <cfreturn returnedStruct>
	</cfif>
	
	<cfif NOT arguments.isSuperUser AND NOT IsDefined("arguments.privilegeGroupID")>
      <cfset returnedStruct.SUCCESS=false>
      <cfset returnedStruct.MESSAGE="Please select at least a privilege group for this administrator.">
	  <cfreturn returnedStruct>
	</cfif>
  
    <cfquery datasource="#this.datasource#">
	  UPDATE am_administrator
      SET firstName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.firstName#">,
      	  lastName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.lastName#">,
          username=<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.username#">,
          password=<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.password#">,
          email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#">,
          isSuperUser=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#arguments.isSuperUser#">, 
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE administratorID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.administratorID#">
	</cfquery>
    
    <cfquery datasource="#this.datasource#">
	  DELETE FROM am_administrator_privilege
	  WHERE administratorID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.administratorID#">
	</cfquery>
	
	<cfif NOT arguments.isSuperUser>
	  <cfset privilegeGroupIDArray=ArrayNew(1)>
	  <cfset privilegeGroupIDArray=ListToArray(arguments.privilegeGroupID)>
	  <cfloop index="idx1" from="1" to="#arrayLen(privilegeGroupIDArray)#">
	    <cfif Not IsDefined("arguments.privilegeID_#privilegeGroupIDArray[idx1]#")>
		  <cfset deleteAdmin(newAdminID)>
          <cfset returnedStruct.SUCCESS=false>
          <cfset returnedStruct.MESSAGE="Please select at least an access privilege for all access privilege groups you have selected for this administrator.">
	      <cfreturn returnedStruct>
		</cfif>
		<cfset privilegeIDList=Evaluate("arguments.privilegeID_#privilegeGroupIDArray[idx1]#")>
	    <cfset privilegeIDArray=ArrayNew(1)>
		<cfset privilegeIDArray=ListToArray(privilegeIDList)>
		<cfloop index="idx2" from="1" to="#arrayLen(privilegeIDArray)#">
		  <cfif Not IsDefined("arguments.categoryID_#privilegeGroupIDArray[idx1]#_#privilegeIDArray[idx2]#")>
		    <cfset arguments.categoryIDList="-1">
		  <cfelse>
		    <cfset arguments.categoryIDList=Evaluate("arguments.categoryID_#privilegeGroupIDArray[idx1]#_#privilegeIDArray[idx2]#")>
		  </cfif>
		  <cfif ListFind(arguments.categoryIDList, "-1") Is Not 0>
		    <cfset arguments.categoryIDList="-1">
		  </cfif>
		  <cfquery datasource="#this.datasource#">
		    INSERT INTO am_administrator_privilege
			(administratorID, privilegeGroupID, privilegeID, categoryIDList)
			VALUES (
			  <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.administratorID#">,
			  <cfqueryparam cfsqltype="cf_sql_integer" value="#privilegeGroupIDArray[idx1]#">,
			  <cfqueryparam cfsqltype="cf_sql_integer" value="#privilegeIDArray[idx2]#">,
			  <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.categoryIDList#">
			)
		  </cfquery>		  
		</cfloop>
	  </cfloop>	
	</cfif> 
    <cfreturn returnedStruct>
  </cffunction>

  <cffunction name="deleteAdmin" access="public" output="false">
    <cfargument name="administratorID" type="numeric" required="yes">
	<cfquery datasource="#this.datasource#">
	  DELETE FROM am_administrator
	  WHERE administratorID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.administratorID#">
	</cfquery>
	<cfquery datasource="#this.datasource#">
	  DELETE FROM am_administrator_privilege
	  WHERE administratorID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.administratorID#">
	</cfquery>
  </cffunction>
  
  <cffunction name="getAdministratorBasicInfo" access="public" output="false" returntype="query">
    <cfargument name="administratorID" type="numeric" required="yes">

    <cfquery name="qAdmin" datasource="#this.datasource#">
	  SELECT *
	  FROM am_administrator
	  WHERE administratorID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.administratorID#">
	</cfquery>
	<cfreturn qAdmin>
  </cffunction>
  
  <cffunction name="getAdministratorPrivilegeInfo" access="public" output="false" returntype="query">
    <cfargument name="administratorID" type="numeric" required="yes">
	<cfquery name="qPrivilege" datasource="#this.datasource#">
	  SELECT *
	  FROM am_administrator_privilege
	  WHERE administratorID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.administratorID#">
	  ORDER BY privilegeGroupID, privilegeID
	</cfquery>		
	<cfreturn qPrivilege>
  </cffunction>
</cfcomponent>