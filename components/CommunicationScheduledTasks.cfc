<cfcomponent displayname="Communication Manager" extends="Base">
  <cfscript>  
    if (IsDefined("Application.communication")) {
      this.errorNotificationEmail=Application.communication.errorNotificationEmail;
      this.numEmailsSentPerProcess=Application.communication.numEmailsSentPerProcess;
	  this.daysToKeepCampaignStats=Application.communication.daysToKeepCampaignStats;
	  this.hoursApartToResendBounceBackMails=Application.communication.hoursApartToResendBounceBackMails;
	  this.numBouncesToBeRemoved=Application.communication.numBouncesToBeRemoved;
	  this.bounceBackEmailDomain=Application.communication.bounceBackEmailDomain;
	  this.bounceBackEmailHost=Application.communication.bounceBackEmailHost;
	  this.bounceBackUsername=Application.communication.bounceBackUsername;
	  this.bounceBackPassword=Application.communication.bounceBackPassword;
	  this.mailLogDirectory=Application.communication.mailLogDirectory;
	  this.HTMLSpecialCharactersConversionPath=Application.communication.HTMLSpecialCharactersConversionPath;
	  this.emailDefaultFromAddress=Application.communication.emailDefaultFromAddress;
    }  
  </cfscript>
  
  
  <cffunction name="isProcessInProgress" access="public" output="no" returntype="boolean">
    <cfset currentTime=now()>
  
    <cfquery name="qInProgress" datasource="#this.datasource#">
	  SELECT scheduledTaskInProgress, lastUpdate
	  FROM cm_process_status
	</cfquery>
	<cfif DateDiff("s", qInProgress.lastUpdate[1], currentTime) GT 2000>
	  <cfset updateProcessStatus(0)>
	</cfif>
	
    <cfif qInProgress.scheduledTaskInProgress[1]>
	  <cfreturn true>
	<cfelse>
	  <cfreturn false>
	</cfif>
  </cffunction>
  
  <cffunction name="updateProcessStatus" access="public" output="no">
    <cfargument name="scheduledTaskInProgress" type="boolean" required="yes">
	
	<cfquery datasource="#this.datasource#">
	  UPDATE cm_process_status
	  SET scheduledTaskInProgress=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.scheduledTaskInProgress#">,
	  	  lastUpdate=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	</cfquery>
  </cffunction>  
  
  <cffunction name="scanAndUpdateCampaignStatus" access="public" output="false">
    <cfquery name="qCampaignInProgress" datasource="#this.datasource#">
	  SELECT messageID
	  FROM cm_message
	  WHERE status='Q'
	</cfquery>
	<cfloop query="qCampaignInProgress">
	  <cfquery name="qMailsInQueue" datasource="#this.datasource#">
	    SELECT emailQueueID
		FROM cm_emailqueue
		WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qCampaignInProgress.messageID#">
	  </cfquery>
	  <cfif qMailsInQueue.recordcount IS 0>
	    <cfquery datasource="#this.datasource#">
		  UPDATE cm_message
		  SET status='S'
		  WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qCampaignInProgress.messageID#">
		</cfquery>
	  </cfif>
	</cfloop>  
  </cffunction>  
  
  <cffunction name="processEmailQueue" access="public" output="no">
    <cfquery name="qEmails" datasource="#this.datasource#">
	  SELECT *
	  FROM cm_emailqueue
	  ORDER BY emailQueueID
	  LIMIT 0,1
	</cfquery>
	<cfif qEmails.recordcount GT 0>
	  <cfquery datasource="#this.datasource#">
	    DELETE FROM cm_emailqueue
		WHERE emailQueueID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qEmails.emailQueueID#">
	  </cfquery>
      
	  <cfset CM=CreateObject("component","com.CommunicationAdmin").init()>
      
	  <cfset messageInfo=CM.getMessage(qEmails.messageID)>
	  <cfif messageInfo.recordcount IS 0>
	    <cfset writeToLog(sendHistoryID=qEmails.sendHistoryID, logType="error", logText="Can not find specified message with ID: #qEmails.messageID#")>
	    <cfreturn>
	  </cfif>	  
	  
	  <cfif Compare(messageInfo.emailFromName, "")>
	    <cfset emailFrom="#messageInfo.emailFromName# <#messageInfo.emailFromAddress#>">
	  <cfelse>
	    <cfset emailFrom="#messageInfo.emailFromAddress#">
	  </cfif>
      
      <cfif Compare(messageInfo.emailReplyAddress, "")>
        <cfset emailReply=messageInfo.emailReplyAddress>
      <cfelse>
        <cfset emailReply=emailFrom>
      </cfif>
	  
	  <cfset emailPart=ArrayNew(1)>
	  <cfset Variables.logText = "">
	  <cfset emailCount = 0>
	  <cfloop index="email" list="#qEmails.emails#" delimiters=";">
	    <cftry>
	      <cfset emailPart=ListToArray(email, ",")>
		  <cfset emailID=emailPart[1]>
		  <cfset emailAddress=emailPart[2]>		
		  <cfset HTMLEmailContent=ReplaceNoCase(messageInfo.completeHTMLEmail, "##emailID##", "#URLEncodedFormat(CM.simpleEncrypt(emailID))#", "ALL")>
		  <cfset textEmailContent=messageInfo.emailBodyText & Chr(13) & Chr(10) & Chr(13) & Chr(10)& 
								"This e-mail is being sent to you at your request. " &
								"If you prefer not to receive future e-mail updates, " &
								"please cut and paste this entire link into your Internet browser: " &
								"#this.siteURLRoot#/index.cfm?md=emaillist&tmp=updateprofile&msgid=#qEmails.messageID#&uid=#URLEncodedFormat(CM.simpleEncrypt(emailID))#">
		
		  <cfset bounceAddress="bounce_" & qEmails.messageID & "_" & qEmails.sendHistoryID & "_" & emailID & "_@" & this.bounceBackEmailDomain>
		  <cfmail from="#emailFrom#" replyto="#emailReply#" to="#emailAddress#" subject="#messageInfo.emailSubject#" failto="#bounceAddress#" type="html">
		    <cfmailpart type="text">
#textEmailContent#
		    </cfmailpart>
		    <cfmailpart type="html" charset="utf-8">
			#HTMLEmailContent#
		    </cfmailpart>
		  </cfmail>
		  <cfset emailCount = emailCount + 1>
		  <cfset Variables.logText = Variables.logText & Chr(13) & Chr(10) & "#emailCount#. #emailAddress# - messageID=#qEmails.messageID# - sendHistoryID=#qEmails.sendHistoryID# - #DateFormat(now(), "mm/dd/yyyy")# #TimeFormat(now(), "hh:mm:ss tt")# Pushed to Mail Server">
		<cfcatch>
		  <cfset writeToLog(sendHistoryID=qEmails.sendHistoryID, logType="error", logText="ERROR: #cfcatch.Detail#")>
		</cfcatch>
		</cftry>
	  </cfloop>
	  <cfset writeToLog(sendHistoryID=qEmails.sendHistoryID, logType="delivery", logText="#VARIABLES.logText#")>
	  <cfset scanAndUpdateCampaignStatus()>
	</cfif>  
  </cffunction>
  
  <cffunction name="getFirstScheduledSendMessage" access="public" output="no" returntype="query">
    <cfquery name="qMessage" datasource="#this.datasource#">
	  SELECT messageID
	  FROM cm_message
	  WHERE sendMode='S' AND status='P' AND
	  		dateScheduledSend < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  LIMIT 0, 1
	</cfquery>
    <cfreturn qMessage>  
  </cffunction>
  
  <cffunction name="parseBounceBackEmails" access="public" output="no"> 
    <cftry>
	  <cfpop action="getall" server="#this.bounceBackEmailHost#" username="#this.bounceBackUsername#" password="#this.bounceBackPassword#" timeout="2000" startrow="1" maxrows="100" name="allbounces">
	<cfcatch>
	  <!--- <cfmail from="#this.errorNotificationEmail#" to="#this.errorNotificationEmail#" subject="Error when parse bounced emails - #this.siteURLRoot#">
	  #cfcatch.Message#
	  </cfmail> --->
	  <cfreturn>
	</cfcatch>
	</cftry>
		
	<cfset numBounces = 0>
	<cfset qBouncedHistory = QueryNew("sendHistoryID,emailID,bounceType")>
	<cfloop query="allbounces">  
	  <cfif Find("fatal errors", body) IS NOT "0" OR Find("No DNS", body) IS NOT "0" OR Find("bad address", body) IS NOT "0">
		<cfset Variables.bounceType="H">
	  <cfelse>
		<cfset Variables.bounceType="S">
	  </cfif>
	  
	  <cfset Variables.toAddress=allbounces.to>	  
	  
	  <cfset addrParts=ArrayNew(1)>
	  <cfset addrParts=ListToArray(Variables.toAddress, "_")>
	  <cfif ArrayLen(addrParts) EQ 5 AND IsNumeric(addrParts[4])>	    
	    <cfset Variables.messageID=addrParts[2]>
	    <cfset Variables.sendHistoryID=addrParts[3]>
		<cfset Variables.emailID=addrParts[4]>
		
        <cftry>
		<cfquery name="qBounceExisted" datasource="#this.datasource#">
		  SELECT emailID
		  FROM cm_bounce
		  WHERE emailID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.emailID#"> AND
		  		sendHistoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.sendHistoryID#"> AND
				messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.messageID#">
		</cfquery>
		
		<cfif qBounceExisted.recordcount IS 0>
			<cfset QueryAddRow(qBouncedHistory, 1)>
			<cfset numBounces = numBounces + 1>
			<cfset temp=QuerySetCell(qBouncedHistory, "sendHistoryID", "#Variables.sendHistoryID#")>
			<cfset temp=QuerySetCell(qBouncedHistory, "emailID", "#Variables.emailID#")>
			<cfset temp=QuerySetCell(qBouncedHistory, "bounceType", "#Variables.bounceType#")>
			
			<cfquery datasource="#this.datasource#">
			  INSERT INTO cm_bounce
				(emailID, messageID, sendHistoryID, bounceType, dateBounced, errorMessage)
			  VALUES
				(<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.emailID#">,
				 <cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.messageID#">,
				 <cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.sendHistoryID#">,
				 <cfqueryparam cfsqltype="cf_sql_char" value="#Variables.bounceType#">,
				 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
				 <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#allbounces.body#">)
			</cfquery>		
		</cfif>
        <cfcatch></cfcatch>
        </cftry>
	  </cfif>
	</cfloop>
	<cfquery name="qSendHistoryIDs" dbtype="query">
	  SELECT DISTINCT sendHistoryID
	  FROM qBouncedHistory
	</cfquery>
	
	<cfloop query="qSendHistoryIDs">
	  <cfquery name="qEmailIDs" dbtype="query">
	    SELECT emailID
		FROM qBouncedHistory
		WHERE sendHistoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qSendHistoryIDs.sendHistoryID#"> AND bounceType='H'
	  </cfquery>	
	  <cfset numHardBounces=qEmailIDs.recordcount>
	  <cfif numHardBounces GT 0>
	    <!--- Record hard bounces --->
	    <cfquery datasource="#this.datasource#">
	    UPDATE cm_email_list
	    SET numBouncedCampaigns=numBouncedCampaigns+1,
		    lastBouncedSendHistoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qSendHistoryIDs.sendHistoryID#">,
		    status='B'
	    WHERE emailID IN (#ValueList(qEmailIDs.emailID)#)	        
        </cfquery>
	  </cfif>
	  
	  <cfquery name="qEmailIDs" dbtype="query">
	    SELECT emailID
		FROM qBouncedHistory
		WHERE sendHistoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qSendHistoryIDs.sendHistoryID#"> AND bounceType='S'
	  </cfquery>
	  <cfset numSoftBounces=qEmailIDs.recordcount>
	  <cfif numSoftBounces GT 0>
	    <!--- Record soft bounces --->
	    <cfquery datasource="#this.datasource#">
	    UPDATE cm_email_list
	    SET numBouncedCampaigns=numBouncedCampaigns+1,
		    lastBouncedSendHistoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qSendHistoryIDs.sendHistoryID#">
	    WHERE emailID IN (#ValueList(qEmailIDs.emailID)#) AND
	  		  lastBouncedSendHistoryID<><cfqueryparam cfsqltype="cf_sql_integer" value="#qSendHistoryIDs.sendHistoryID#">
	    </cfquery>		
	  </cfif>
	  <cfset numBounces=numHardBounces+numSoftBounces>
	  <cfquery datasource="#this.datasource#">
	    UPDATE cm_sendhistory
	    SET numBounces=numBounces+#numBounces#
	    WHERE sendHistoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qSendHistoryIDs.sendHistoryID#">
	  </cfquery>	  
	</cfloop>
	
	<cfif allbounces.recordcount IS NOT "0">
	  <cfpop action="delete" server="#this.bounceBackEmailHost#" timeout="2000" username="#this.bounceBackUsername#" password="#this.bounceBackPassword#" uid="#ValueList(allbounces.uid)#">
	</cfif>
  </cffunction>  
  
  <cffunction name="resendBounceBackMails" access="public" output="no">
    <cfquery name="qResend" datasource="#this.datasource#">
	  SELECT scheduleID, sendHistoryID, messageID, resendNum
	  FROM cm_bouncebackresendschedule
	  WHERE dateToResend < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  LIMIT 0,1
	</cfquery>
		
    <cfif qResend.recordcount IS NOT 0><!--- a resend schedule was found --->	  
	  <cfquery datasource="#this.datasource#">
	    DELETE FROM cm_bouncebackresendschedule
		WHERE scheduleID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qResend.scheduleID#">
	  </cfquery>	    
	  
	  <!--- get all emails that need to be resent and then DELETE FROM the bounce table --->
	  <cfquery name="qEmails" datasource="#this.datasource#">
	    SELECT emailID
		FROM cm_bounce
		WHERE sendHistoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qResend.sendHistoryID#"> AND
			  messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qResend.messageID#"> AND
			  bounceType='S'
	  </cfquery>
	  
      <cfset CM=CreateObject("component","com.CommunicationAdmin").init()>
	  <cfset emailsList="">
	  <cfset idx = 0>
	  <cfloop query="qEmails">	    
		<cfset email = CM.getEmailByEmailID(emailID)>
		<cfif Compare(email, "")>
		  <cfset idx = idx + 1>		  
		  <cfif ListLen(emailsList, ";") IS 0>
		    <cfset emailsList=emailID & "," & email>
		  <cfelse>
		    <cfif idx GTE this.numEmailsSentPerProcess>
			  <cfset emailsList=emailsList & "|" & emailID & "," & email>
			  <cfset idx = 0>
			<cfelse>
		      <cfset emailsList=emailsList & ";" & emailID & "," & email>
			</cfif>
		  </cfif>
		</cfif>		
	  </cfloop>
	  <cfif ListLen(emailsList, ";") IS NOT 0 OR ListLen(emailsList, "|") IS NOT 0>
	    <!--- create new send history --->
	    <cfquery name="qAttemptNum" datasource="#this.datasource#">
		  SELECT MAX(attemptNum) as num
		  FROM cm_sendhistory
		  WHERE sendHistoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qResend.sendHistoryID#">
		</cfquery>
		<cfset newAttemptNum=qAttemptNum.num + 1>
	    <cfquery datasource="#this.datasource#">
		  INSERT INTO cm_sendhistory
		    (messageID, attemptNum, dateSent, numSent)
		  VALUES
		    (<cfqueryparam cfsqltype="cf_sql_integer" value="#qResend.messageID#">,
			 <cfqueryparam cfsqltype="cf_sql_integer" value="#newAttemptNum#">,
			 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
			 <cfqueryparam cfsqltype="cf_sql_integer" value="#qEmails.recordcount#">)
		</cfquery>		
		
		<cfquery name="getMaxID" datasource="#this.datasource#">
		  SELECT MAX(sendHistoryID) AS ID
		  FROM cm_sendhistory
		</cfquery>
		<cfset newSendHistoryID=getMaxID.ID>
				
		<cfset emailListArray=ArrayNew(1)>
		<cfset emailListArray=ListToArray(emailsList, "|")>
		
		<cfloop index="j" FROM="1" to="#ArrayLen(emailListArray)#">
		  <cfquery datasource="#this.datasource#">
		    INSERT INTO cm_emailqueue
		      (messageID, sendHistoryID, emails, dateCreated)
		    VALUES
		      (<cfqueryparam cfsqltype="cf_sql_integer" value="#qResend.messageID#">,
			   <cfqueryparam cfsqltype="cf_sql_integer" value="#newSendHistoryID#">,
			   <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#emailListArray[j]#">,
			   <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
		  </cfquery>
		</cfloop>
	
		<cfif qResend.resendNum IS "1">
		  <cfquery datasource="#this.datasource#">
		    UPDATE cm_bouncebackresendschedule
			SET sendHistoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#newSendHistoryID#">
			WHERE messageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qResend.messageID#"> AND
				  resendNum=2
		  </cfquery>		
		</cfif>		
	  </cfif>	
	</cfif>  
  </cffunction>  
  
  <cffunction name="updateStatusBasedOnBounceNumber" access="public" output="no">
    <cfquery datasource="#this.datasource#">
	  UPDATE cm_email_list
	  SET status='B'
	  WHERE numBouncedCampaigns >= <cfqueryparam cfsqltype="cf_sql_integer" value="#this.numBouncesToBeRemoved#"> 
	</cfquery>  
  </cffunction>  
  
  <cffunction name="purgeCampaignStats" access="public" output="no">
    <cfset dateToPurge=DateAdd("d", (-1 * this.daysToKeepCampaignStats), now())>
	
	<cfquery name="qMessageID" datasource="#this.datasource#">
	  SELECT MAX(messageID) AS messageID
	  FROM cm_message
	  WHERE dateFirstSend < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#dateToPurge#">
	</cfquery>
  
    <cfif isNumeric(qMessageID.messageID)>
  	  <cfquery datasource="#this.datasource#">
	    DELETE FROM cm_messageopen
	    WHERE messageID < <cfqueryparam cfsqltype="cf_sql_integer" value="#qMessageID.messageID#">
	  </cfquery>
	  <cfquery datasource="#this.datasource#">
	    DELETE FROM cm_messageclickthru
	    WHERE messageID < <cfqueryparam cfsqltype="cf_sql_integer" value="#qMessageID.messageID#">
	  </cfquery>
      <cfquery datasource="#this.datasource#">
	    DELETE FROM cm_messageforward
	    WHERE messageID < <cfqueryparam cfsqltype="cf_sql_integer" value="#qMessageID.messageID#">
	  </cfquery>
	</cfif>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cm_bounce
	  WHERE dateBounced < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#dateToPurge#">
	</cfquery>
  </cffunction>  
  
  
  <cffunction name="writeToLog" access="public" output="false">
    <cfargument name="sendHistoryID" type="numeric" required="yes">
	<cfargument name="logType" type="string" default="delivery">
	<cfargument name="logText" type="string" default="">
	
	<cftry>
	  <cfif NOT Compare(Arguments.logType, "delivery")>
	    <cfset fileName="#DateFormat(now(), "mm-dd-yy")#-delivery-#Arguments.sendHistoryID#.log">
		<cfif fileExists("#this.mailLogDirectory#/#fileName#")>
		  <cffile action="read" file="#this.mailLogDirectory#/#fileName#" variable="deliveryLog">
		<cfelse>
		  <cfset deliveryLog = "">
		</cfif>
		<cfset deliveryLog = deliveryLog & Chr(13) & Chr(10) & Arguments.logText>
		<cffile action="write" file="#this.mailLogDirectory#/#fileName#" output="#deliveryLog#" addnewline="no" nameconflict="overwrite">
	  <cfelse>
	    <cfset fileName="#DateFormat(now(), "mm-dd-yy")#-error-#Arguments.sendHistoryID#.log">
		<cfif fileExists("#this.mailLogDirectory#/#fileName#")>
		  <cffile action="read" file="#this.mailLogDirectory#/#fileName#" variable="errorLog">
		<cfelse>
		  <cfset errorLog = "">
		</cfif>
		<cfset errorLog = errorLog & Chr(13) & Chr(10) & Arguments.logText>
		<cffile action="write" file="#this.mailLogDirectory#/#fileName#" output="#errorLog#" addnewline="no" nameconflict="overwrite">
	  </cfif>  
	<cfcatch></cfcatch>
	</cftry>
  </cffunction>
</cfcomponent>