<cfcomponent displayname="Restricted Area Admin" extends="Base" output="false">
  <cffunction name="getAllGroups" access="public" output="no" returntype="query">
	<cfquery name="qAllGroups" datasource="#this.datasource#">
	  SELECT groupID, groupName
	  FROM pp_group
	  ORDER BY groupName	
	</cfquery>	
    
    <cfreturn qAllGroups>  
  </cffunction>
  
  <cffunction name="getGroupSummary" access="public" output="no" returntype="query">
    <cfquery name="qSummary" datasource="#this.datasource#">
	  SELECT pp_group.groupName,
      	  	 pp_group.groupID,
			 COUNT(pp_user_group.userID) AS numUsers
	  FROM pp_group LEFT JOIN pp_user_group ON
	  	   pp_group.groupID=pp_user_group.groupID
	  GROUP by pp_group.groupID
	  ORDER by pp_group.groupName		
	</cfquery>
    
    <cfreturn qSummary>
  </cffunction>
  
  <cffunction name="getFirstGroupID" access="public" output="no" returntype="numeric">
    <cfquery name="qGroupID" datasource="#this.datasource#">
	  SELECT groupID
	  FROM pp_group
      ORDER BY groupName
	</cfquery>
    <cfif qGroupID.recordcount GT 0>
	  <cfreturn qGroupID.groupID[1]>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="getGroup" access="public" output="no" returntype="query">
    <cfargument name="groupID" required="yes" type="numeric">	
	<cfquery name="qGroup" datasource="#this.datasource#">
	  SELECT *
      FROM pp_group
      WHERE groupID=<cfqueryparam value="#Arguments.groupID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
	<cfreturn qGroup>
  </cffunction>
  
  <cffunction name="addGroup" access="public" output="no">
    <cfargument name="groupName" type="string" required="yes">

    <cfquery datasource="#this.datasource#">
	  INSERT INTO pp_group
        (groupName, dateCreated, dateLastModified)
	  VALUES
        (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.groupName#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)	  
	</cfquery>  
  </cffunction>
  
  <cffunction name="editGroup" access="public" output="no">
    <cfargument name="groupID" type="numeric" required="yes">
    <cfargument name="groupName" type="string" required="yes">

    <cfquery datasource="#this.datasource#">
	  UPDATE pp_group
      SET groupName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.groupName#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">  
	</cfquery>  
  </cffunction>
    
  <cffunction name="deleteGroup" access="public" output="false" returntype="boolean">
    <cfargument name="groupID" required="yes" type="numeric">
	
	<cfquery name="qUsers" datasource="#this.datasource#">
	  SELECT userID
	  FROM pp_user_group
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">			
	</cfquery>
	
	<cfif qUsers.recordcount GT 0>
	  <cfreturn false>
	</cfif>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM pp_group
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
	</cfquery>
    
	<cfreturn true>
  </cffunction>
  
  <cffunction name="getUsersByGroupID" access="public" output="no" returntype="struct">
    <cfargument name="groupID" type="numeric" required="yes">
	<cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.users=QueryNew("userID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qAllUsers" datasource="#this.datasource#">
	  SELECT COUNT(pp_user.userID) AS numUsers
	  FROM	 pp_user INNER JOIN pp_user_group ON
	  		 pp_user.userID=pp_user_group.userID
	  WHERE	 pp_user_group.groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
    </cfquery>
	<cfset resultStruct.numAllItems = qAllUsers.numUsers>
	
	<cfquery name="qUsers" datasource="#this.datasource#">
	  SELECT pp_user.userID, pp_user.username, pp_user.password, pp_user.email, pp_user.published	 
	  FROM   pp_user INNER JOIN pp_user_group ON
	         pp_user.userID=pp_user_group.userID   
	  WHERE  pp_user_group.groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
	  ORDER  BY pp_user.username
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
    <cfif qUsers.recordcount GT 0>            
      <cfset resultStruct.numDisplayedItems=qUsers.recordcount>	
      <cfset resultStruct.users = qUsers>
	<cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.users = QueryNew("userID")>
    </cfif>
    
	<cfreturn resultStruct>
  </cffunction>  
  
  <cffunction name="getUserByID" access="public" output="no" returntype="query">
    <cfargument name="userID" type="numeric" required="yes">
	<cfquery name="qUser" datasource="#this.datasource#">
	  SELECT *
	  FROM pp_user
      WHERE userID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.userID#">
	</cfquery>
    
	<cfreturn qUser>	
  </cffunction>
  
  <cffunction name="getGroupIDListByUserID" access="public" output="no" returntype="string">
    <cfargument name="userID" type="numeric" required="yes">
    
	<cfquery name="qCategories" datasource="#this.datasource#">
	  SELECT groupID
      FROM pp_user_group
      WHERE userID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.userID#">
	</cfquery>
    
    <cfif qCategories.recordcount Is 0>
      <cfreturn "">
    <cfelse>
	  <cfreturn valueList(qCategories.groupID)>	  
    </cfif>
  </cffunction>
  
  <cffunction name="getGroupByID" access="public" output="no" returntype="query">
    <cfargument name="groupID" required="yes" type="numeric">	
	<cfquery name="qGroup" datasource="#this.datasource#">
	  SELECT *
      FROM pp_group
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
	</cfquery>
	<cfreturn qGroup>
  </cffunction>

  <cffunction name="addUser" access="public" output="no" returntype="boolean">
    <cfargument name="groupIDList" type="string" default="">
    <cfargument name="username" type="string" required="yes">
    <cfargument name="password" type="string" required="yes">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    
    <!--- <cfif Not Compare(Arguments.groupIDList, "")><cfreturn false></cfif> --->
    
    <cfquery name="qCheckExistingUsername" datasource="#this.datasource#">
      SELECT userID
      FROM pp_user
      WHERE username=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.username#">
    </cfquery>
    <cfif qCheckExistingUsername.recordcount GT 0><cfreturn false></cfif>
    		
    <cfquery datasource="#this.datasource#">
	  INSERT INTO pp_user
        (username, password, email, published, dateCreated, dateLastModified)
	  VALUES
	    (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.username#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.password#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)		
	</cfquery>

	<cfquery name="qUserID" datasource="#this.datasource#">
	  SELECT MAX(userID) AS newUserID
	  FROM pp_user
    </cfquery>
	<cfset Variables.userID=qUserID.newUserID>	
    
    <cfloop index="groupID" list="#Arguments.groupIDList#">      
      <cfquery datasource="#this.datasource#">
		INSERT INTO pp_user_group (userID, groupID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.userID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#groupID#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>

  <cffunction name="editUser" access="public" output="no" returntype="boolean">
    <cfargument name="userID" type="numeric" required="yes">
    <cfargument name="groupIDList" type="string" default="">
    <cfargument name="username" type="string" required="yes">
    <cfargument name="password" type="string" required="yes">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    
    <!--- <cfif Not Compare(Arguments.groupIDList, "")><cfreturn false></cfif> --->
    
    <cfquery name="qCheckExistingUsername" datasource="#this.datasource#">
      SELECT userID
      FROM pp_user
      WHERE userID<><cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.userID#"> AND
      		username=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.username#">
    </cfquery>
    <cfif qCheckExistingUsername.recordcount GT 0><cfreturn false></cfif>
    
    <cfquery datasource="#this.datasource#">
	  UPDATE pp_user
      SET published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
          username=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.username#">,
          password=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.password#">,
          email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE userID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.userID#">
	</cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM pp_user_group
      WHERE userID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.userID#">
    </cfquery>
    
    <cfloop index="groupID" list="#Arguments.groupIDList#">      
      <cfquery datasource="#this.datasource#">
		INSERT INTO pp_user_group (userID, groupID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.userID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#groupID#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>
  
  <cffunction name="deleteUser" access="public" output="false">
    <cfargument name="userID" required="yes" type="numeric">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM pp_user_group
      WHERE userID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.userID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM pp_user
	  WHERE userID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.userID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="getProtectedURLIndicatorListByGroupID" access="public" output="no" returntype="string">
    <cfargument name="groupID" type="numeric" required="yes">
  
    <cfquery name="qURLIndicators" datasource="#this.datasource#">
      SELECT CONCAT(module,'|',template,'|',keyName,'|',keyValue) AS URLIndicator
      FROM pp_page_group
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
    </cfquery>
  
    <cfif qURLIndicators.recordcount GT 0>
      <cfreturn ValueList(qURLIndicators.URLIndicator)>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>  
  
  <cffunction name="updateRestrictedPages" access="public" output="false">
    <cfargument name="groupID" type="numeric" required="yes">
    <cfargument name="pageURLIndicator" type="string" default="">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM pp_page_group
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
    </cfquery>
    
    <cfif Compare(Arguments.pageURLIndicator,"")>
      <cfset URLIndicator=ListToArray(Arguments.pageURLIndicator)>
      <cfloop index="idx" from="1" to="#ArrayLen(URLIndicator)#">
        <cfset items=ListToArray(URLIndicator[idx],"|")>
        <cfset module=items[1]>
        <cfset template=items[2]>
        <cfset keyName=items[3]>
        <cfset keyValue=items[4]>
        <cfquery datasource="#this.datasource#">
          INSERT INTO pp_page_group (groupID, module, template, keyName, keyValue, dateCreated)
          VALUES (
            <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#module#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#template#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#keyName#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#keyValue#">,
            <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
          )
        </cfquery>
      </cfloop>
    </cfif>
  </cffunction>
</cfcomponent>