<cfcomponent displayname="Promotion" extends="Base" output="false">
  <cffunction name="getAds" access="public" output="no" returntype="query">
    <cfargument name="placementID" type="numeric" default="0">
    <cfargument name="module" type="string" default="">
	<cfargument name="template" type="string" default="">
	<cfargument name="categoryID" type="numeric" default="0">
	<cfargument name="numAds" type="numeric" default="1">
	
	<cfset var seedIncrement=0.000001>
	<cfset var currentTime=now()>
	<cfset var currentDate=CreateDate(Year(currentTime),Month(currentTime),Day(currentTime))>
    	
	<cfquery name="qAds" datasource="#this.datasource#">
	  SELECT pm_promo_placement.promoPlacementID,
	  		 pm_promo.promoID, pm_promo.promoName, pm_promo.promoImage, pm_promo.promoImageOver, pm_promo.promoImageAltText,
	  		 pm_promo.pageID, pm_promo.destinationURL, pm_promo.popup
	  FROM (pm_placement INNER JOIN pm_promo_placement ON
	  	   pm_placement.placementID=pm_promo_placement.placementID) INNER JOIN pm_promo ON
		   pm_promo_placement.promoID=pm_promo.promoID
	  WHERE <cfif Compare(Arguments.placementID, 0)>
	  		pm_placement.placementID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.placementID#"> AND
	  		<cfelse>
	  		pm_placement.module=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.module#"> AND
	  		pm_placement.template=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.template#"> AND
			pm_promo_placement.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#"> AND
			</cfif>
			pm_promo.published=1 AND
			pm_promo.startDate <= <cfqueryparam cfsqltype="cf_sql_date" value="#currentDate#"> AND
			pm_promo.endDate >= <cfqueryparam cfsqltype="cf_sql_date" value="#currentDate#">
	  ORDER BY pm_promo_placement.rotationSeed
	  LIMIT 0,#Arguments.numAds#
	</cfquery>
    
    <cfif qAds.recordcount Is 0>
        <cfquery name="qAds" datasource="#this.datasource#">
          SELECT pm_promo_placement.promoPlacementID,
                 pm_promo.promoID, pm_promo.promoName, pm_promo.promoImage, pm_promo.promoImageOver, pm_promo.promoImageAltText,
                 pm_promo.pageID, pm_promo.destinationURL, pm_promo.popup
          FROM (pm_placement INNER JOIN pm_promo_placement ON
               pm_placement.placementID=pm_promo_placement.placementID) INNER JOIN pm_promo ON
               pm_promo_placement.promoID=pm_promo.promoID
          WHERE pm_placement.module=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.module#"> AND
                pm_placement.template='*' AND
                pm_promo_placement.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#"> AND
                pm_promo.published=1 AND
                pm_promo.startDate <= <cfqueryparam cfsqltype="cf_sql_date" value="#currentDate#"> AND
                pm_promo.endDate >= <cfqueryparam cfsqltype="cf_sql_date" value="#currentDate#">
          ORDER BY pm_promo_placement.rotationSeed
          LIMIT 0,#Arguments.numAds#
        </cfquery>
    </cfif>
    
    <cfif qAds.recordcount Is 0>
        <cfquery name="qAds" datasource="#this.datasource#">
          SELECT pm_promo_placement.promoPlacementID,
                 pm_promo.promoID, pm_promo.promoName, pm_promo.promoImage, pm_promo.promoImageOver, pm_promo.promoImageAltText,
                 pm_promo.pageID, pm_promo.destinationURL, pm_promo.popup
          FROM (pm_placement INNER JOIN pm_promo_placement ON
               pm_placement.placementID=pm_promo_placement.placementID) INNER JOIN pm_promo ON
               pm_promo_placement.promoID=pm_promo.promoID
          WHERE pm_placement.module='*' AND
                pm_placement.template='*' AND
                pm_promo_placement.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#"> AND
                pm_promo.published=1 AND
                pm_promo.startDate <= <cfqueryparam cfsqltype="cf_sql_date" value="#currentDate#"> AND
                pm_promo.endDate >= <cfqueryparam cfsqltype="cf_sql_date" value="#currentDate#">
          ORDER BY pm_promo_placement.rotationSeed
          LIMIT 0,#Arguments.numAds#
        </cfquery>
    </cfif>
	
	<cfif qAds.recordcount GT 0>
	  <cfquery datasource="#this.datasource#">
	    UPDATE pm_promo_placement
		SET rotationSeed=rotationSeed+#seedIncrement#
		WHERE promoPlacementID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qAds.promoPlacementID)#" list="yes">)
	  </cfquery>
	</cfif>
	
    <cfloop query="qAds">
      <cfset addViews(promoID)>
    </cfloop>
    
    <cfreturn qAds>
  </cffunction>

  <cffunction name="getAllPlacements" access="public" output="no" returntype="query">
    <cfquery name="qPlacements" datasource="#this.datasource#">
      SELECT *
      FROM pm_placement
      ORDER BY displaySeq
    </cfquery>
  
    <cfreturn qPlacements>
  </cffunction>

  <cffunction name="getFirstPlacementID" access="public" output="no" returntype="numeric">
    <cfquery name="qPlacement" datasource="#this.datasource#">
      SELECT placementID
      FROM pm_placement
      ORDER BY displaySeq
      LIMIT 0,1
    </cfquery>
    <cfreturn qPlacement.placementID>
  </cffunction>
  
  <cffunction name="getPlacementInfo" access="public" output="no" returntype="query">
    <cfargument name="placementID" type="numeric" required="yes">
    
    <cfquery name="qPlacementInfo" datasource="#this.datasource#">
      SELECT *
      FROM pm_placement
      WHERE placementID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.placementID#">
    </cfquery>
    
    <cfreturn qPlacementInfo>
  </cffunction>
  
  <cffunction name="getPromos" access="public" output="no" returntype="struct">
    <cfargument name="placementID" type="numeric" required="yes">
    <cfargument name="categoryID" type="numeric" required="yes">
	<cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
  
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.promo=QueryNew("promoID")>	
  
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
    
    <cfquery name="qAllPromos" datasource="#this.datasource#">
	  SELECT DISTINCT pm_promo.promoID
	  FROM	 pm_promo INNER JOIN pm_promo_placement ON
	  		 pm_promo.promoID=pm_promo_placement.promoID
	  WHERE	 pm_promo_placement.placementID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.placementID#">
      		 <cfif Compare(Arguments.categoryID,-1)> AND
      		 pm_promo_placement.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
             </cfif>
    </cfquery>
	<cfset resultStruct.numAllItems = qAllPromos.recordcount>
  
    <cfquery name="qPromos" datasource="#this.datasource#">
	  SELECT promoID, promoName, startDate, endDate, 
      		 promoImage, published, destinationURL     
	  FROM   pm_promo
	  WHERE	 <cfif qAllPromos.recordcount GT 0>
      		 promoID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qAllPromos.promoID)#" list="yes">)
      		 <cfelse>
      		 1=-1
      		 </cfif>
	  ORDER  BY startDate DESC
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
    <cfif qPromos.recordcount GT 0>            
      <cfset resultStruct.numDisplayedItems=qPromos.recordcount>	
      <cfset resultStruct.promos = qPromos>
	<cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.promos = QueryNew("promoID")>
    </cfif>
    
	<cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="addPromo" access="public" output="no">
    <cfargument name="promoName" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="startDate" type="string" required="yes">
    <cfargument name="endDate" type="string" required="yes">
    <cfargument name="promoImage" type="string" required="yes">
    <cfargument name="promoImageOver" type="string" default="">
    <cfargument name="descriptionText" type="string" required="yes">
    <cfargument name="destinationURL" type="string" required="yes">
    <cfargument name="pageID" type="numeric" default="0">
    <cfargument name="popup" type="boolean" default="0">
    <cfargument name="placementList" type="string" required="yes">
    
    <cfif Not Compare(Arguments.placementList,"")><cfreturn></cfif>
    
    <cfset Variables.startDate=DateAdd("d",0,Arguments.startDate)>
    <cfset Variables.endDate=DateAdd("d",0,Arguments.endDate)>
    <cfquery datasource="#this.datasource#">
      INSERT INTO pm_promo
      (promoName, startDate, endDate, promoImage, promoImageOver,
       descriptionText, pageID, destinationURL, popup, published,
       dateCreated, dateLastModified)
      VALUES
      (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.promoName#">,
       <cfqueryparam cfsqltype="cf_sql_date" value="#Variables.startDate#">,
       <cfqueryparam cfsqltype="cf_sql_date" value="#Variables.endDate#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.promoImage#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.promoImageOver#">,
       
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.descriptionText#">,
       <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageID#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.destinationURL#">,
       <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.popup#">,
       <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
       
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)       
    </cfquery>
    
    <cfquery name="qNewPromoID" datasource="#this.datasource#">
      SELECT MAX(promoID) AS promoID
      FROM pm_promo
    </cfquery>
    <cfset newPromoID=qNewPromoID.promoID>
    
    <cfloop index="placement" list="#Arguments.placementList#">
      <cfset items=ListToArray(placement,"|")>
      <cfset placementID=items[1]>
      <cfset categoryID=items[2]>
      
      <cfquery datasource="#this.datasource#">
        INSERT INTO pm_promo_placement
        (placementID, categoryID, promoID, rotationSeed, dateCreated)
        VALUES
        (<cfqueryparam cfsqltype="cf_sql_integer" value="#placementID#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#newPromoID#">,
         0,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
      </cfquery>
      <cfset resetRotationSeed(placementID)>
    </cfloop>   
  </cffunction>
  
  <cffunction name="editPromo" access="public" output="no">
    <cfargument name="promoID" type="numeric" required="yes">
    <cfargument name="promoName" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="startDate" type="string" required="yes">
    <cfargument name="endDate" type="string" required="yes">
    <cfargument name="promoImage" type="string" required="yes">
    <cfargument name="promoImageOver" type="string" default="">
    <cfargument name="descriptionText" type="string" required="yes">
    <cfargument name="destinationURL" type="string" required="yes">
    <cfargument name="pageID" type="numeric" default="0">
    <cfargument name="popup" type="boolean" default="0">
    <cfargument name="placementList" type="string" required="yes">
  
    <cfif Not Compare(Arguments.placementList,"")><cfreturn></cfif>
    
    <cfset Variables.startDate=DateAdd("d",0,Arguments.startDate)>
    <cfset Variables.endDate=DateAdd("d",0,Arguments.endDate)>
    <cfquery datasource="#this.datasource#">
      UPDATE pm_promo
      SET promoName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.promoName#">,
      	  startDate=<cfqueryparam cfsqltype="cf_sql_date" value="#Variables.startDate#">,
          endDate=<cfqueryparam cfsqltype="cf_sql_date" value="#Variables.endDate#">,
          promoImage=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.promoImage#">,
          promoImageOver=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.promoImageOver#">,
          descriptionText=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.descriptionText#">,
          pageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageID#">,
          destinationURL=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.destinationURL#">,
          popup=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.popup#">,
          published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery>
    
	<cfquery datasource="#this.datasource#">
      DELETE FROM pm_promo_placement
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery>
    
    <cfloop index="placement" list="#Arguments.placementList#">
      <cfset items=ListToArray(placement,"|")>
      <cfset placementID=items[1]>
      <cfset categoryID=items[2]>
      
      <cfquery datasource="#this.datasource#">
        INSERT INTO pm_promo_placement
        (placementID, categoryID, promoID, rotationSeed, dateCreated)
        VALUES
        (<cfqueryparam cfsqltype="cf_sql_integer" value="#placementID#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">,
         0,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
      </cfquery>
      <cfset resetRotationSeed(placementID)>
    </cfloop> 
  </cffunction>
  
  <cffunction name="deletePromo" access="public" output="no">
    <cfargument name="promoID" type="numeric" required="yes">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM pm_stats
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM pm_promo_placement
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM pm_promo
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery> 
  </cffunction>
  
  <cffunction name="getPromo" access="public" output="no" returntype="query">
    <cfargument name="promoID" type="numeric" required="yes">
  
    <cfquery name="qPromo" datasource="#this.datasource#">
      SELECT *
      FROM pm_promo
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery>
    
    <cfreturn qPromo>
  </cffunction>
  
  <cffunction name="getPromoPlacementInfo" access="public" output="no" returntype="query">
    <cfargument name="promoID" type="numeric" required="yes">
    
    <cfquery name="qPlacementInfo" datasource="#this.datasource#">
      SELECT placementID, categoryID, CONCAT(CAST(placementID AS CHAR),'|',CAST(categoryID AS CHAR)) AS placement
      FROM pm_promo_placement
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#">
    </cfquery>
    
    <cfreturn qPlacementInfo>
  </cffunction>

  <cffunction name="resetRotationSeed" access="public" output="no">
    <cfargument name="placementID" type="numeric" required="yes">
  
    <cfquery datasource="#this.datasource#">
      UPDATE pm_promo_placement
      SET rotationSeed=0
      WHERE placementID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.placementID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="addViews" access="public" output="no">
    <cfargument name="promoID" type="string" required="yes">
    
    <cfset currentTime=now()>
    <cfset todayDate=CreateDate(Year(currentTime),Month(currentTime),Day(currentTime))>
    <cfquery name="qStats" datasource="#this.datasource#">
      SELECT statsID
      FROM pm_stats
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#"> AND
      		statsDate=<cfqueryparam cfsqltype="cf_sql_date" value="#todayDate#">
    </cfquery>
  
    <cfif qStats.recordcount GT 0>
      <cfquery datasource="#this.datasource#">
        UPDATE pm_stats
        SET views=views+1
        WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#"> AND
      		  statsDate=<cfqueryparam cfsqltype="cf_sql_date" value="#todayDate#">
      </cfquery>
    <cfelse>
      <cfquery name="qName" datasource="#this.datasource#">
        INSERT INTO pm_stats (promoID, statsDate, views)
        VALUES (
          <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.promoID#">,
          <cfqueryparam cfsqltype="cf_sql_date" value="#todayDate#">,
          1
        )
      </cfquery>
    </cfif>
  </cffunction>  
  
  <cffunction name="addClickThru" access="public" output="no">
    <cfargument name="promoID" type="string" required="yes">
    
    <cfset currentTime=now()>
    <cfset todayDate=CreateDate(Year(currentTime),Month(currentTime),Day(currentTime))>
    <cfquery name="qStats" datasource="#this.datasource#">
      SELECT statsID
      FROM pm_stats
      WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#"> AND
      		statsDate=<cfqueryparam cfsqltype="cf_sql_date" value="#todayDate#">
    </cfquery>
  
    <cfif qStats.recordcount GT 0>
      <cfquery datasource="#this.datasource#">
        UPDATE pm_stats
        SET clickthrus=clickthrus+1
        WHERE promoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.promoID#"> AND
      		  statsDate=<cfqueryparam cfsqltype="cf_sql_date" value="#todayDate#">
      </cfquery>
    <cfelse>
      <cfquery name="qName" datasource="#this.datasource#">
        INSERT INTO pm_stats (promoID, statsDate, clickthrus)
        VALUES (
          <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.promoID#">,
          <cfqueryparam cfsqltype="cf_sql_date" value="#todayDate#">,
          1
        )
      </cfquery>
    </cfif>
  </cffunction>
</cfcomponent>