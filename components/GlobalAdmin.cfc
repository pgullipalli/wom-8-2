<cfcomponent displayname="Gloabl Properties Manager" extends="Base" output="false">
  <cffunction name="getGlobalProperties" access="public" output="no" returntype="query">
    <cfquery name="qGlobalProperties" datasource="#this.datasource#">
      SELECT *
      FROM gp_global_properties
    </cfquery>
    <cfreturn qGlobalProperties>
  </cffunction>

  <cffunction name="updateTitle" access="public" output="no">
	<cfquery datasource="#this.datasource#">
      UPDATE gp_global_properties
      SET title=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.title#">
    </cfquery>
  </cffunction>
  
  <cffunction name="updateDescription" access="public" output="no">
	<cfquery datasource="#this.datasource#">
      UPDATE gp_global_properties
      SET description=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.description#">
    </cfquery>
  </cffunction>
  
  <cffunction name="updateKeywords" access="public" output="no">
	<cfquery datasource="#this.datasource#">
      UPDATE gp_global_properties
      SET keywords=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.keywords#">
    </cfquery>
  </cffunction>
  
  <cffunction name="updateWebmasterEmail" access="public" output="no">
	<cfquery datasource="#this.datasource#">
      UPDATE gp_global_properties
      SET webmasterEmail=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.webmasterEmail#">
    </cfquery>
  </cffunction>  

  <cffunction name="updatePrimaryNavs" access="public" output="no">
    <cfargument name="primaryNavIDList" type="string" default="">
    
	<cfquery datasource="#this.datasource#">
      UPDATE gp_global_properties
      SET primaryNavIDList=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.primaryNavIDList#">
    </cfquery>
  </cffunction>

  <cffunction name="updateTopNavs" access="public" output="no">
    <cfargument name="topNavIDList" type="string" default="">
    
	<cfquery datasource="#this.datasource#">
      UPDATE gp_global_properties
      SET topNavIDList=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.topNavIDList#">
    </cfquery>
  </cffunction>
  
  <cffunction name="updateLeftSecondaryNavs" access="public" output="no">
    <cfargument name="leftSecondaryNavIDList" type="string" default="">
    
	<cfquery datasource="#this.datasource#">
      UPDATE gp_global_properties
      SET leftSecondaryNavIDList=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.leftSecondaryNavIDList#">
    </cfquery>
  </cffunction>
  
  <cffunction name="updateDonateNowCopy" access="public" output="no">
	<cfquery datasource="#this.datasource#">
      UPDATE gp_global_properties
      SET donateNowCopy=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.donateNowCopy#">
    </cfquery>
  </cffunction>
  
  <cffunction name="updateFooter" access="public" output="no">
	<cfquery datasource="#this.datasource#">
      UPDATE gp_global_properties
      SET footer=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.footer#">
    </cfquery>
  </cffunction>
</cfcomponent>