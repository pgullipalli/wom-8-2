<cfcomponent displayname="UAL Alias Manager" extends="Base" output="false">   
  <cffunction name="getAllAlias" access="public" output="false" returntype="query">
    <cfquery name="qAllAlias" datasource="#this.datasource#">
	  SELECT *
	  FROM ua_urlalias
	  ORDER BY alias
	</cfquery>
    <cfreturn qAllAlias>  
  </cffunction>
  
  <cffunction name="getAliasByID" access="public" output="false" returntype="query">
    <cfargument name="URLAliasID" required="true" type="numeric">
    <cfquery name="qALias" datasource="#this.datasource#">
	  SELECT *
	  FROM ua_urlalias
	  WHERE URLAliasID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.URLAliasID#">
	</cfquery>
    <cfreturn qALias>  
  </cffunction>
  
  <cffunction name="addAlias" access="public" output="false" returntype="struct">
    <cfargument name="aliasDirectoryRoot" type="string" required="yes">  
    <cfset var returnedStruct=StructNew()>
	<cfset returnedStruct.SUCCESS=false>
    <cfset returnedStruct.MESSAGE="">
  
    <cftry>
      <cfdirectory action="create" directory="#arguments.aliasDirectoryRoot#/#arguments.alias#">
	  <cfset pagecontent="<html><head><title>Redirect</title><meta http-equiv=""refresh"" content=""0;url=#arguments.resolvedURL#""></head><body></body></html>">
	  <cffile action="write" file="#arguments.aliasDirectoryRoot#/#arguments.alias#/index.html" output="#pagecontent#">
      <cfset returnedStruct.SUCCESS=true>
	<cfcatch>
      <cfset returnedStruct.MESSAGE="Alias could not be created. A file directory with the same name as the alias name may already exist. Please use other alias name and try again.">
	  <cfreturn returnedStruct>
	</cfcatch>
    </cftry>
	
	<cfquery datasource="#this.datasource#">
	  INSERT INTO ua_urlalias (aliasName, alias, resolvedURL, dateCreated, dateLastModified)
	  VALUES
	  ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.aliasName#">,
	    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alias#">,
	    <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#arguments.resolvedURL#">,
        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  )
	</cfquery>
	
	<cfreturn returnedStruct>
  </cffunction>
  
  <cffunction name="editAlias" access="public" output="false" returntype="struct">
    <cfargument name="aliasDirectoryRoot" type="string" required="yes">  
    <cfset var returnedStruct=StructNew()>
	<cfset returnedStruct.SUCCESS=false>
    <cfset returnedStruct.MESSAGE="">
  
    <cftry>
	  <cfset pagecontent="<html><head><title>Redirect</title><meta http-equiv=""refresh"" content=""0;url=#arguments.resolvedURL#""></head><body></body></html>">
	  <cffile action="write" file="#arguments.aliasDirectoryRoot#/#arguments.alias#/index.html" output="#pagecontent#">
      <cfset returnedStruct.SUCCESS=true>
    <cfcatch>
	  <cfset returnedStruct.MESSAGE="Server encountered an unexpected error.">
	  <cfreturn returnedStruct>
	</cfcatch>
	</cftry>  
  
	<cfquery datasource="#this.datasource#">
	  UPDATE ua_urlalias
	  SET aliasName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.aliasName#">,
      	  resolvedURL=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#arguments.resolvedURL#">
	  WHERE URLAliasID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.URLAliasID#">
	</cfquery>
	
	<cfreturn returnedStruct>
  </cffunction>

  <cffunction name="deleteAlias" access="public" output="false" returntype="struct">
    <cfargument name="URLAliasID" required="yes" type="numeric">
	<cfargument name="aliasDirectoryRoot" type="string" required="yes">
    <cfset var returnedStruct=StructNew()>
	<cfset returnedStruct.SUCCESS=false>
    <cfset returnedStruct.MESSAGE="">
	
    <cfquery name="qAlias" datasource="#this.datasource#">
	  SELECT *
	  FROM ua_urlalias
	  WHERE URLAliasID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.URLAliasID#">
	</cfquery>
  
    <cfif qAlias.recordcount IS 0>
      <cfset returnedStruct.SUCCESS=false>
      <cfset returnedStruct.MESSAGE="The URL alias that you tried to deleted doesn't exist.">
	  <cfreturn returnedStruct>
	</cfif>
	
    <cfquery datasource="#this.datasource#">
	  DELETE FROM ua_urlalias
	  WHERE URLAliasID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.URLAliasID#">
	</cfquery>	
	
	<cftry>
	  <cffile action="delete"  file="#arguments.aliasDirectoryRoot#/#qAlias.alias#/index.html">	
	  <cfdirectory action="delete" directory="#arguments.aliasDirectoryRoot#/#qAlias.alias#">	
      <cfset returnedStruct.SUCCESS=true>
	<cfcatch>	
      <cfset returnedStruct.SUCCESS=false>
	  <cfset returnedStruct.MESSAGE="Server encountered an unexpected error.">
	</cfcatch>
	</cftry>
    <cfreturn returnedStruct>
  </cffunction>   
</cfcomponent>
