<cfcomponent displayname="Page Builder" extends="Base" output="no">
  <cffunction name="getPage" access="public" output="no" returntype="query">
    <cfargument name="pageID" type="numeric" required="yes">
    
    <cfquery name="qPage" datasource="#this.datasource#">
      SELECT *
      FROM pb_page
      WHERE pageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageID#"> AND
      		published=1
    </cfquery>
    
    <cfreturn qPage>
  </cffunction>
  
  <cffunction name="getPageContent" access="public" output="no" returntype="string">
    <cfargument name="pageID" type="numeric" required="yes">
    
    <cfquery name="qPage" datasource="#this.datasource#">
      SELECT CASE WHEN (useHTML=1)
             THEN pageContentHTML
             ELSE pageContent
             END AS pageContent             
      FROM pb_page
      WHERE pageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageID#"> AND
      		published=1
    </cfquery>
    
    <cfif qPage.recordcount GT 0>
      <cfreturn qPage.pageContent>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="getPageName" access="public" output="no" returntype="string">
    <cfargument name="pageID" type="numeric" required="yes">
    
    <cfquery name="qPage" datasource="#this.datasource#">
      SELECT pageName
      FROM pb_page
      WHERE pageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageID#"> AND
      		published=1
    </cfquery>
    
    <cfif qPage.recordcount GT 0>
      <cfreturn qPage.pageName>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <!-- Added by Vipul on 23-May-2011 -->
  <cffunction name="getPageTitle" access="public" output="no" returntype="string">
    <cfargument name="pageID" type="numeric" required="yes">
    
    <cfquery name="qPage" datasource="#this.datasource#">
      SELECT pageTitle
      FROM pb_page
      WHERE pageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageID#"> AND
      		published=1
    </cfquery>
    
    <cfif qPage.recordcount GT 0>
      <cfreturn qPage.pageTitle>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="getPageCategoryNameByPageID" access="public" output="no" returntype="string">
    <cfargument name="pageID" type="numeric" required="yes">
    
    <cfquery name="qCategory" datasource="#this.datasource#">
      SELECT pb_pagecategory.pageCategoryName
      FROM pb_page INNER JOIN pb_pagecategory ON pb_page.pageCategoryID=pb_pagecategory.pageCategoryID
      WHERE pb_page.pageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageID#"> AND
      		pb_page.published=1
    </cfquery>
    
    <cfif qCategory.recordcount GT 0>
      <cfreturn qCategory.pageCategoryName>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="getPageCategoryIDByPageID" access="public" output="no" returntype="numeric">
    <cfargument name="pageID" type="numeric" required="yes">
    
    <cfquery name="qCategory" datasource="#this.datasource#">
      SELECT pb_pagecategory.pageCategoryID
      FROM pb_page INNER JOIN pb_pagecategory ON pb_page.pageCategoryID=pb_pagecategory.pageCategoryID
      WHERE pb_page.pageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageID#">
    </cfquery>
    
    <cfif qCategory.recordcount GT 0>
      <cfreturn qCategory.pageCategoryID>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
</cfcomponent>