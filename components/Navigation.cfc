<cfcomponent displayname="Navigation" extends="Base" output="no">
  <cffunction name="getSubNavsByParentNavID" access="public" output="no" returntype="query">
	<cfargument name="navGroupID" type="numeric" required="yes">
	<cfargument name="parentNavID" type="numeric" default="0">
    
    <cfquery name="qNavs" datasource="#this.datasource#">
      SELECT nm_nav.navID, nm_nav.templateID, nm_nav.navName, nm_nav.popup, nm_nav.navImage, nm_nav.navImageOver,
      		 CASE WHEN nm_nav.iframe=1
             THEN CONCAT('#this.siteURLRoot#/','index.cfm?md=navigation&amp;tmp=iframe&amp;pnid=', CAST(nm_navrel.parentNavID AS CHAR), '&amp;nid=', CAST(nm_nav.navID AS CHAR), '&amp;tid=', CAST(nm_nav.templateID AS CHAR))
             ELSE CASE WHEN (nm_nav.URLQueryString IS NULL OR nm_nav.URLQueryString='')
             	  THEN nm_nav.resolvedURL
                  ELSE CASE WHEN nm_nav.SSLEnabled=0
                       THEN CONCAT('#this.siteURLRoot#/', nm_nav.resolvedURL, '&amp;pnid=', CAST(nm_navrel.parentNavID AS CHAR), '&amp;nid=', CAST(nm_nav.navID AS CHAR), '&amp;tid=', CAST(nm_nav.templateID AS CHAR))
                       ELSE CONCAT('#this.siteURLRootSSL#/', nm_nav.resolvedURL, '&amp;pnid=', CAST(nm_navrel.parentNavID AS CHAR), '&amp;nid=', CAST(nm_nav.navID AS CHAR), '&amp;tid=', CAST(nm_nav.templateID AS CHAR))
                       END
                  END
             END AS href             
      FROM nm_navrel INNER JOIN nm_nav ON nm_navrel.navID=nm_nav.navID
      WHERE nm_navrel.parentNavID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.parentNavID#"> AND
      		nm_nav.navGroupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navGroupID#"> AND
			nm_nav.published=1
      ORDER BY nm_navrel.displaySeq    
    </cfquery>
    
    <cfreturn qNavs>
  </cffunction>
  
  <cffunction name="getNavsByNavIDList" access="public" output="no" returntype="query">
    <cfargument name="navIDList" type="string" required="yes">
    
    <cfif Compare(Arguments.navIDList,"")>  
        <cfquery name="qNavs" datasource="#this.datasource#">
          SELECT nm_nav.navID, nm_nav.templateID, nm_nav.navName, nm_nav.popup, nm_nav.navImage, nm_nav.navImageOver,
                 CASE WHEN nm_nav.iframe=1
                 THEN CONCAT('#this.siteURLRoot#/','index.cfm?md=navigation&amp;tmp=iframe&amp;pnid=', CAST(nm_navrel.parentNavID AS CHAR), '&amp;nid=', CAST(nm_nav.navID AS CHAR), '&amp;tid=', CAST(nm_nav.templateID AS CHAR))
                 ELSE CASE WHEN (nm_nav.URLQueryString IS NULL OR nm_nav.URLQueryString='')
                      THEN nm_nav.resolvedURL
                      ELSE CASE WHEN nm_nav.SSLEnabled=0
                      	   THEN CONCAT('#this.siteURLRoot#/',nm_nav.resolvedURL,'&amp;nid=', CAST(nm_nav.navID AS CHAR), '&amp;tid=', CAST(nm_nav.templateID AS CHAR))
                           ELSE CONCAT('#this.siteURLRootSSL#/',nm_nav.resolvedURL,'&amp;nid=', CAST(nm_nav.navID AS CHAR), '&amp;tid=', CAST(nm_nav.templateID AS CHAR))
                           END
                      END
                 END AS href             
          FROM nm_nav INNER JOIN nm_navrel ON nm_nav.navID=nm_navrel.navID
          WHERE nm_nav.navID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.navIDList#" list="yes">) AND
                nm_nav.published=1
          ORDER BY nm_navrel.displaySeq    
        </cfquery>
        
        <cfreturn qNavs>
    <cfelse>
    	<cfreturn QueryNew("navID")>
    </cfif>
  </cffunction>
  
  <cffunction name="getNavBranch" access="public" output="false" returntype="struct">
    <cfargument name="navGroupID" type="numeric" required="true">
    <cfargument name="navID" type="numeric" required="true">
    
    <cfset var nav = StructNew()>
    <cfset nav.numSubNavs=0>
    <cfset navTree=getNavTreeByNavGroupID(Arguments.navGroupID)>
    
    <cfloop index="idx1" from="1" to="#navTree.numSubNavs#">
      <cfif Not Compare(Arguments.navID, navTree.nav[idx1].navID)>
        <cfset nav=navTree.nav[idx1]>
      </cfif>
    </cfloop>
    
    <cfreturn nav>  
  </cffunction>

  <cffunction name="getNavTreeByNavGroupID" access="public" output="false" returntype="struct">
	<cfargument name="navGroupID" type="numeric" required="true">
		
	<cfset var nav = StructNew()>
	
    <cfquery name="qNumLevels" datasource="#this.datasource#">
	  SELECT numLevels
	  FROM nm_nav_group
	  WHERE navGroupID=<cfqueryparam value="#Arguments.navGroupID#" cfsqltype="cf_sql_integer">
	</cfquery>   
    
	<cfset nav.numLevels=qNumLevels.numLevels>
	
	<cfquery name="qAllNavs" datasource="#this.datasource#">
	  SELECT	nm_navrel.parentNavID,
	  			nm_nav.navID,
                nm_nav.templateID,
	  			nm_nav.navName,
                nm_nav.navImage,
                nm_nav.navImageOver,
                nm_nav.popup,			
				nm_nav.levelNum,
                CASE WHEN nm_nav.iframe=1
             	THEN CONCAT('#this.siteURLRoot#/','index.cfm?md=navigation&amp;tmp=iframe&amp;pnid=', CAST(nm_navrel.parentNavID AS CHAR), '&amp;nid=', CAST(nm_nav.navID AS CHAR), '&amp;tid=', CAST(nm_nav.templateID AS CHAR))
            	ELSE CASE WHEN (nm_nav.URLQueryString IS NULL OR nm_nav.URLQueryString='')
             		THEN nm_nav.resolvedURL
                	ELSE CASE WHEN nm_nav.SSLEnabled=0
                    	 THEN CONCAT('#this.siteURLRoot#/',nm_nav.resolvedURL, '&amp;pnid=', CAST(nm_navrel.parentNavID AS CHAR), '&amp;nid=', CAST(nm_nav.navID AS CHAR), '&amp;tid=', CAST(nm_nav.templateID AS CHAR))
                         ELSE CONCAT('#this.siteURLRootSSL#/',nm_nav.resolvedURL, '&amp;pnid=', CAST(nm_navrel.parentNavID AS CHAR), '&amp;nid=', CAST(nm_nav.navID AS CHAR), '&amp;tid=', CAST(nm_nav.templateID AS CHAR))
                         END
                	END
             	END AS href,
                nm_navrel.displaySeq				
	  FROM  nm_navrel JOIN nm_nav ON nm_navrel.navID = nm_nav.navID
	  WHERE nm_nav.navGroupID=<cfqueryparam value="#Arguments.navGroupID#" cfsqltype="cf_sql_integer"> AND
      		nm_nav.published=1	
	  ORDER BY nm_navrel.displaySeq
	</cfquery>    
    
	<cfloop index="idx" from="1" to="#nav.numLevels#">
	  <cfquery name="qLevel#idx#" dbtype="query">
	    SELECT *
	    FROM qAllNavs
	    WHERE levelNum=#idx#
	    ORDER BY displaySeq
	  </cfquery>	
	</cfloop>	

	<cfset nav.numSubNavs = 0>
	<cfset nav.nav = ArrayNew(1)>
	<cfset idx1=0>	
	<cfloop query="qLevel1"><!--- BEGIN: level 1 loop --->
	  <cfset idx1=idx1 + 1>
	  <cfset nav.numSubNavs=idx1>
	  <cfset nav.nav[idx1]=StructNew()>
	  <cfset nav.nav[idx1].parentNavID=parentNavID>
	  <cfset nav.nav[idx1].navID=navID>
      <cfset nav.nav[idx1].templateID=templateID>
	  <cfset nav.nav[idx1].navName=navName>
      <cfset nav.nav[idx1].navImage=navImage>
      <cfset nav.nav[idx1].navImageOver=navImageOver>
	  <cfset nav.nav[idx1].popup=popup>
      <cfset nav.nav[idx1].href=href>
	  <cfset nav.nav[idx1].numSubNavs=0>
	  
	  <cfif nav.numLevels GTE 2><!--- At least 2 levels --->
		<cfset nav.nav[idx1].nav=ArrayNew(1)>
		<cfset idx2=0>
		<cfloop query="qLevel2"><!--- BEGIN: level 2 loop --->
		  <cfif parentNavID IS nav.nav[idx1].navID>
			<cfset idx2=idx2 + 1>
			<cfset nav.nav[idx1].numSubNavs=idx2>
			<cfset nav.nav[idx1].nav[idx2]=StructNew()>
			<cfset nav.nav[idx1].nav[idx2].parentNavID=parentNavID>
			<cfset nav.nav[idx1].nav[idx2].navID=navID>
            <cfset nav.nav[idx1].nav[idx2].templateID=templateID>
			<cfset nav.nav[idx1].nav[idx2].navName=navName>
            <cfset nav.nav[idx1].nav[idx2].navImage=navImage>
      		<cfset nav.nav[idx1].nav[idx2].navImageOver=navImageOver>
            <cfset nav.nav[idx1].nav[idx2].popup=popup>
            <cfset nav.nav[idx1].nav[idx2].href=href>
		    <cfset nav.nav[idx1].nav[idx2].numSubNavs=0>
		    
		    <cfif nav.numLevels GTE 3><!--- BEGIN: At least 3 levels --->
			  <cfset nav.nav[idx1].nav[idx2].nav=ArrayNew(1)>
			  <cfset idx3=0>
			  <cfloop query="qLevel3"><!--- BEGIN: level 3 loop --->
				<cfif parentNavID IS nav.nav[idx1].nav[idx2].navID>
				  <cfset idx3=idx3 + 1>
				  <cfset nav.nav[idx1].nav[idx2].numSubNavs=idx3>
				  <cfset nav.nav[idx1].nav[idx2].nav[idx3]=StructNew()>
				  <cfset nav.nav[idx1].nav[idx2].nav[idx3].parentNavID=parentNavID>
				  <cfset nav.nav[idx1].nav[idx2].nav[idx3].navID=navID>
                  <cfset nav.nav[idx1].nav[idx2].nav[idx3].templateID=templateID>
				  <cfset nav.nav[idx1].nav[idx2].nav[idx3].navName=navName>
                  <cfset nav.nav[idx1].nav[idx2].nav[idx3].navImage=navImage>
      			  <cfset nav.nav[idx1].nav[idx2].nav[idx3].navImageOver=navImageOver>
                  <cfset nav.nav[idx1].nav[idx2].nav[idx3].popup=popup>
                  <cfset nav.nav[idx1].nav[idx2].nav[idx3].href=href>				  
				  <cfset nav.nav[idx1].nav[idx2].nav[idx3].numSubNavs=0>				  
				</cfif>
			  </cfloop><!--- END: level 3 loop --->
			</cfif><!--- END: At least 3 levels --->
		  </cfif>
		</cfloop><!--- END: level 2 loop --->
	  </cfif>
	</cfloop><!--- END: level 1 loop --->
	
    <cfreturn nav>
  </cffunction>


  <cffunction name="getExternalLinkByNavID" access="public" output="no" returntype="string">
    <cfargument name="navID" type="numeric" required="yes">
	
	<cfquery name="getLink" datasource="#this.datasource#">
	  SELECT externalLink
	  FROM nm_nav
	  WHERE navID=<cfqueryparam value="#Arguments.navID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
    <cfif getLink.recordcount IS 0>
	  <cfreturn "">
	<cfelse>
	  <cfreturn getLink.externalLink>
	</cfif>
  </cffunction> 
  
  <cffunction name="getPageTitleByNavID" access="public" output="no" returntype="string">
    <cfargument name="navID" type="numeric" required="yes">
  
    <cfquery name="qPageTitle" datasource="#this.datasource#">
      SELECT pageTitle
	  FROM nm_nav
	  WHERE navID=<cfqueryparam value="#Arguments.navID#" cfsqltype="cf_sql_integer">
    </cfquery>
    <cfif qPageTitle.recordcount GT 0>
      <cfreturn qPageTitle.pageTitle>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="getParentNavID" access="public" output="no" returntype="numeric">
    <cfargument name="navID" type="numeric" required="yes">
    
    <cfquery name="qParentNavID" datasource="#this.datasource#">
      SELECT parentNavID
      FROM nm_navrel
      WHERE navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navID#">
    </cfquery>
    <cfif qParentNavID.recordcount GT 0>
      <cfreturn qParentNavID.parentNavID>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="getTopMostAscendantNavID" access="public" output="no" returntype="numeric">
    <cfargument name="navID" type="numeric" required="yes">
    
    <cfif Compare(Arguments.navID,"0")>
      <cfquery name="qParentNavID" datasource="#this.datasource#">
        SELECT parentNavID
        FROM nm_navrel
        WHERE navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navID#">
      </cfquery>
      
      <cfif qParentNavID.recordcount Is 0>
        <cfreturn 0>
      <cfelse>
    	<cfif Compare(qParentNavID.parentNavID,"0")>
    	  <cfquery name="qGrandParentNavID" datasource="#this.datasource#">
            SELECT parentNavID
            FROM nm_navrel
            WHERE navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qParentNavID.parentNavID#">
          </cfquery>
          
          <cfif qGrandParentNavID.recordcount Is 0>
            <!--- Error: database inconsistency --->
            <cfreturn 0>
          <cfelse>
            <cfif Compare(qGrandParentNavID.parentNavID,"0")>
          	  <cfreturn qGrandParentNavID.parentNavID>
            <cfelse>
              <!--- grand parent navID is 0; parent nav is the Top Most Ascendant --->
              <cfreturn qParentNavID.parentNavID>
            </cfif>
          </cfif>
    	<cfelse>
          <!--- parent navID is 0; itself is the Top Most Ascendant --->
          <cfreturn Arguments.navID>
        </cfif>
      </cfif>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="getNavProperties" access="public" output="no" returntype="query">
    <cfargument name="navID" type="numeric" required="yes">
    
    <cfquery name="qNavInfo" datasource="#this.datasource#">
        SELECT navName, templateID, pageTitle
        FROM nm_nav
        WHERE navID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.navID#">
    </cfquery>
    
    <cfreturn qNavInfo>   
  </cffunction>
</cfcomponent>