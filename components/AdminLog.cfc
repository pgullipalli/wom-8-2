<cfcomponent displayname="Admin Log" extends="Base" output="false">
  <cffunction name="addLog" access="public" output="no" returntype="void">
    <cfargument name="adminID" type="numeric" default="0">
    <cfargument name="module" type="string" default=""><!--- modules --->
    <cfargument name="task" type="string" default=""><!--- template/view --->
    <cfargument name="actionURL" type="string" default="">
    
    <cfquery datasource="#this.datasource#">
  	  INSERT INTO admin_action_log (adminID, module, task, actionURL, dateOfActivity)
      VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.adminID#">,
      		  <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.module#">,
              <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.task#">,
              <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.actionURL#">,
              <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)              
    </cfquery>  
  </cffunction>
</cfcomponent>