<cfcomponent displayname="Homepage" extends="Base" output="false">
  <cffunction name="getHomepageInfo" access="public" output="no" returntype="query">
    <cfquery name="qHomepageInfo" datasource="#this.datasource#">
      SELECT * FROM hp_homepage
    </cfquery>
    <cfreturn qHomepageInfo>
  </cffunction>
  
  <cffunction name="getSubHomePage" access="public" output="no" returntype="query">
    <cfargument name="subHomePageID" type="numeric" required="yes">
    <cfquery name="qSubHomePage" datasource="#this.datasource#">
      SELECT *
      FROM hp_subhomepage
      WHERE subHomePageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.subHomePageID#">
    </cfquery>
    <cfreturn qSubHomePage>
  </cffunction>
</cfcomponent>