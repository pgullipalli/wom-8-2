<cfcomponent displayname="Page Builder Administrator" extends="Base" output="false">
  <cffunction name="getAllPageCategories" access="public" returntype="query">
	<cfquery name="qAllCategories" datasource="#this.datasource#">
	  SELECT *
	  FROM pb_pagecategory
	  ORDER BY pageCategoryName
	</cfquery>
	
	<cfreturn qAllCategories>
  </cffunction>

  <cffunction name="getPageCategorySummary" access="public" returntype="query">
	<cfquery name="qAllCategories" datasource="#this.datasource#">
	  SELECT pb_pagecategory.pageCategoryID, pb_pagecategory.pageCategoryName, pb_pagecategory.headerImage, COUNT(pb_page.pageID) AS numPages
	  FROM pb_pagecategory LEFT JOIN pb_page ON
	  	   pb_pagecategory.pageCategoryID=pb_page.pageCategoryID
	  GROUP BY pb_pagecategory.pageCategoryID
	  ORDER BY pb_pagecategory.pageCategoryName
	</cfquery>
	
	<cfreturn qAllCategories>
  </cffunction>

  <cffunction name="getFirstPageCategoryID" access="public" returntype="numeric" output="false" displayname="getFirstPageCategoryID">
	<cfquery name="qFirstPageCategoryID" datasource="#this.datasource#">
	  SELECT pageCategoryID
	  FROM pb_pagecategory
	  ORDER BY pageCategoryName
	  LIMIT 0,1
	</cfquery>
	<cfif qFirstPageCategoryID.recordcount GT 0>
	  <cfreturn qFirstPageCategoryID.pageCategoryID[1]>
	<cfelse>
	  <cfreturn 0>
	</cfif>
  </cffunction>

  <cffunction name="getPageCategory" access="public" returntype="query" output="false" displayname="getPageCategory">
    <cfargument name="pageCategoryID" required="true" type="numeric">
	
	<cfquery name="qPageCategory" datasource="#this.datasource#">
	  SELECT *
	  FROM pb_pagecategory
	  WHERE pageCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageCategoryID#">
	</cfquery>
	<cfreturn qPageCategory>
  </cffunction>

  <cffunction name="addPageCategory" access="public" returntype="Numeric" output="false" displayname="addPageCategory">
	<cfargument name="pageCategoryName" required="true" type="string">
    <cfargument name="headerImage" type="string" default="">
    
	<cfquery datasource="#this.datasource#">
	  INSERT INTO pb_pagecategory (pageCategoryName, headerImage, dateCreated, dateLastModified)
	  VALUES (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.pageCategoryName#">,
      		  <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.headerImage#">,
	  		  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
	  		  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	</cfquery>
	<cfquery name="qMaxCatID" datasource="#this.datasource#">
	  SELECT MAX(pageCategoryID) AS newPageCategoryID
	  FROM pb_pagecategory
	</cfquery>
	<cfreturn qMaxCatID.newPageCategoryID>
  </cffunction>

  <cffunction name="editPageCategory" access="public" returntype="Void" output="false" displayname="editPageCategory">
	<cfargument name="pageCategoryName" required="true" type="string">
    <cfargument name="headerImage" type="string" default="">
	<cfargument name="pageCategoryID" required="true" type="numeric">
    
	<cfquery datasource="#this.datasource#">
	  UPDATE pb_pagecategory
	  SET pageCategoryName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.pageCategoryName#">,
      	  headerImage=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.headerImage#">,
	      dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE pageCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageCategoryID#">
	</cfquery>
  </cffunction>

  <cffunction name="deletePageCategory" access="public" returntype="boolean" output="false" displayname="deletePageCategory">
	<cfargument name="pageCategoryID" required="true" type="numeric">
	<cfquery name="qPages" datasource="#this.datasource#">
	  SELECT pageID
	  FROM pb_page
	  WHERE pageCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageCategoryID#">
	  LIMIT 0,1
	</cfquery>
	<cfif qPages.recordcount GT 0>
	  <cfreturn false>
	<cfelse>
	  <cfquery datasource="#this.datasource#">
	    DELETE FROM pb_pagecategory
	    WHERE pageCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageCategoryID#">
	  </cfquery>
	  <cfreturn true>
	</cfif>
  </cffunction>

  <cffunction name="getPagesByCategoryID" access="public" returntype="struct" output="false" displayname="getPagesByCategoryID">
	<cfargument name="pageCategoryID" type="numeric" required="true">
	<cfargument name="pageNum" type="numeric" required="false" default="1">
	<cfargument name="startIndex" type="numeric" required="false" default="1">	
	<cfargument name="numItemsPerPage" type="numeric" required="false" default="20">
	
	<cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfset resultStruct=StructNew()>
	<cfset resultStruct.pageCategoryName="">
	<cfset resultStruct.numAllPages=0>
	<cfset resultStruct.numDisplayedPages=0>
	<cfset resultStruct.pages=QueryNew("pageID")>	
	
	<cfset categoryInfo=getPageCategory(Arguments.pageCategoryID)>
	<cfif categoryInfo.recordcount GT 0><cfset resultStruct.pageCategoryName=categoryInfo.pageCategoryName></cfif>
	
	<cfquery name="qAllPages" datasource="#this.datasource#">
	  SELECT COUNT(*) AS numAllPages
	  FROM pb_page
	  WHERE pageCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageCategoryID#">
	</cfquery>
	<cfset resultStruct.numAllPages=qAllPages.numAllPages>
	
	<cfquery name="qPages" datasource="#this.datasource#">
	  SELECT pageID, pageName, published, isSearchable
	  FROM pb_page
	  WHERE pageCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageCategoryID#">
	  ORDER BY pageName
	  LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
	
	<cfset resultStruct.numDisplayedPages=qPages.recordcount>
	<cfset resultStruct.pages=qPages>
	
	<cfreturn resultStruct>
  </cffunction>

  <cffunction name="getPagesByKeyword" access="public" returntype="struct" output="false" displayname="getPagesByKeyword">
	<cfargument name="keyword" type="string" required="true">
	<cfargument name="includePageContent" type="boolean" default="0">
	<cfargument name="pageNum" type="numeric" required="false" default="1">
	<cfargument name="startIndex" type="numeric" required="false" default="1">	
	<cfargument name="numItemsPerPage" type="numeric" required="false" default="20">
	
	<cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfset resultStruct=StructNew()>
	<cfset resultStruct.keyword=Arguments.keyword>
	<cfset resultStruct.numAllPages=0>
	<cfset resultStruct.numDisplayedPages=0>
	<cfset resultStruct.pages=QueryNew("pageID")>	
	
	<cfif Len(Arguments.keyword) LTE 1>
	  <cfreturn resultStruct>
	</cfif>
	
	<cfquery name="qAllPages" datasource="#this.datasource#">
	  SELECT COUNT(*) AS numAllPages
	  FROM pb_page
	  WHERE pageName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
	  		<cfif Arguments.includePageContent>
			OR pageContent LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
			</cfif>
	</cfquery>
	<cfset resultStruct.numAllPages=qAllPages.numAllPages>
	
	<cfquery name="qPages" datasource="#this.datasource#">
	  SELECT pb_page.pageID, pb_page.pageName, pb_page.published, pb_page.isSearchable,
	  	     pb_pagecategory.pageCategoryID, pb_pagecategory.pageCategoryName
	  FROM pb_page INNER JOIN pb_pagecategory ON
	  	   pb_page.pageCategoryID=pb_pagecategory.pageCategoryID
	  WHERE pb_page.pageName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
	  		<cfif Arguments.includePageContent>
			OR pb_page.pageContent LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
			</cfif>
	  ORDER BY pb_pagecategory.pageCategoryName, pb_page.pageName
	  LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
	
	<cfset resultStruct.numDisplayedPages=qPages.recordcount>
	<cfset resultStruct.pages=qPages>
	
	<cfreturn resultStruct>
  </cffunction>

  <cffunction name="getNumPagesByCategoryID" access="public" returntype="numeric" output="false" displayname="getNumPagesByCategoryID">
	<cfargument name="pageCategoryID" type="numeric">
	
	<cfquery name="qAllPages" datasource="#this.datasource#">
	  SELECT COUNT(*) AS numAllPages
	  FROM pb_page
	  WHERE pageCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageCategoryID#">
	</cfquery>
	<cfreturn qAllPages.numAllPages>	
  </cffunction>

  <cffunction name="addPage" access="public" returntype="void" output="false" displayname="addPage">
	<cfargument name="pageCategoryID" required="true" type="numeric">
	<cfargument name="pageName" required="true" type="string">
    <cfargument name="pageTitle" type="string">
	<cfargument name="pageContent" required="true" type="string">
    <cfargument name="pageContentHTML" required="true" type="string">
    <cfargument name="useHTML" required="false" type="boolean" default="0">
	<cfargument name="published" required="false" type="boolean" default="0">
	<cfargument name="isSearchable" required="false" type="boolean" default="0">
    <cfargument name="defaultTemplateID" required="false" type="numeric" default="0">
	
	<cfquery datasource="#this.datasource#">
	  INSERT INTO pb_page
      (pageCategoryID, pageName,pageTitle, pageContent, pageContentHTML, useHTML,
       published, isSearchable, defaultTemplateID, dateCreated, dateLastModified)
	  VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageCategoryID#">,
	  		  <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.pageName#">,
              <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.pageTitle#">,
	  		  <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.pageContent#">,
              <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.pageContentHTML#">,
              <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.useHTML#">,
	  		  <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
	  		  <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.isSearchable#">,
              <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.defaultTemplateID#">,
	  		  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
	  		  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	</cfquery>	
  </cffunction>

  <cffunction name="editPage" access="public" returntype="void" output="false" displayname="editPage">
	<cfargument name="pageID" required="true" type="numeric">
	<cfargument name="pageCategoryID" required="true" type="numeric">
	<cfargument name="pageName" required="true" type="string">
	<cfargument name="pageContent" required="true" type="string">
    <cfargument name="pageContentHTML" required="true" type="string">
    <cfargument name="useHTML" required="false" type="boolean" default="0">
	<cfargument name="published" required="false" type="boolean" default="0">
	<cfargument name="isSearchable" required="false" type="boolean" default="0">
    <cfargument name="defaultTemplateID" required="false" type="numeric" default="0">
	
	<cfquery datasource="#this.datasource#">
	  UPDATE pb_page
	  SET pageName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.pageName#">,
	      pageTitle=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.pageTitle#">,
	  	  pageContent=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.pageContent#">,
          pageContentHTML=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.pageContentHTML#">,
          useHTML=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.useHTML#">,              
	  	  pageCategoryID=<cfqueryparam cfsqltype="cf_sql_numeric" value="#Arguments.pageCategoryID#">,
	  	  published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
	  	  isSearchable=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.isSearchable#">,
          defaultTemplateID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.defaultTemplateID#">, 	  
	  	  dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE pageID=<cfqueryparam cfsqltype="cf_sql_numeric" value="#Arguments.pageID#">
	</cfquery>	
  </cffunction>

  <cffunction name="copyPage" access="public" returntype="void" output="false" displayname="copyPage">
	<cfargument name="pageID" required="true" type="numeric">
	<cfargument name="newPageName" required="true" type="string">
	<cfargument name="published" required="false" type="boolean" default="0">
	<cfargument name="isSearchable" required="false" type="boolean" default="0">
	
	<cfset pageInfo=getPageByPageID(Arguments.pageID)>
	
	<cfquery datasource="#this.datasource#">
	  INSERT INTO pb_page (pageCategoryID, pageName,pageTitle, pageContent, pageContentHTML, useHTML, published, isSearchable, defaultTemplateID, dateCreated, dateLastModified)
	  VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#pageInfo.pageCategoryID#">,
	  		  <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.newPageName#">,
              <cfqueryparam cfsqltype="cf_sql_varchar" value="#pageInfo.pageTitle#">,
	  		  <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#pageInfo.pageContent#">,
              <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#pageInfo.pageContentHTML#">,
              <cfqueryparam cfsqltype="cf_sql_tinyint" value="#pageInfo.useHTML#">,
	  		  <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
	  		  <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.isSearchable#">,
              <cfqueryparam cfsqltype="cf_sql_integer" value="#pageInfo.defaultTemplateID#">,
	  		  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
	  		  <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	</cfquery>
  </cffunction>

  <cffunction name="movePage" access="public" returntype="void" output="false" displayname="movePage">
	<cfargument name="pageID" required="true" type="numeric">
	<cfargument name="pageCategoryID" required="true" type="numeric">
	
	<cfquery datasource="#this.datasource#">
	  UPDATE pb_page
	  SET pageCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageCategoryID#">
	  WHERE pageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageID#">
	</cfquery>	   
  </cffunction>

  <!---<cffunction name="editPageContent" access="public" returntype="void" output="false" displayname="editPageContent">
	<cfargument name="pageID" required="true" type="numeric">
	
	<cfquery datasource="#this.datasource#">
	  UPDATE pb_page
	  SET pageContent=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.pageContent#">,	  	  
	  	  dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE pageID=<cfqueryparam cfsqltype="cf_sql_numeric" value="#Arguments.pageID#">
	</cfquery>
  </cffunction>--->

  <cffunction name="deletePage" access="public" returntype="void" output="false" displayname="deletePage">
	<cfargument name="pageID" required="true" type="numeric">
    <cfquery datasource="#this.datasource#">
	  DELETE FROM pb_page
	  WHERE pageID=<cfqueryparam cfsqltype="cf_sql_numeric" value="#Arguments.pageID#">
	</cfquery>
  </cffunction>

  <cffunction name="getPageByPageID" access="public" returntype="query" output="false" displayname="getPageByPageID">
	<cfargument name="pageID" required="true" type="numeric">
	
	<cfquery name="qPage" datasource="#this.datasource#">
	  SELECT *
	  FROM pb_page
	  WHERE pageID=<cfqueryparam cfsqltype="cf_sql_numeric" value="#Arguments.pageID#">
	</cfquery>
	<cfreturn qPage>
  </cffunction>

  <cffunction name="getAllPages" access="public" returntype="query" output="false" displayname="getAllPages">
	<cfquery name="qAllPages" datasource="#this.datasource#">
	  SELECT pb_page.pageID, pb_page.pageName,pb_page.pageTitle, pb_pagecategory.pageCategoryID, pb_pagecategory.pageCategoryName
	  FROM pb_page INNER JOIN pb_pagecategory ON pb_page.pageCategoryID=pb_pagecategory.pageCategoryID
	  ORDER BY pb_pagecategory.pageCategoryName, pb_page.pageName
	</cfquery>
	<cfreturn qAllPages>
  </cffunction>
</cfcomponent>