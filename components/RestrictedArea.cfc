﻿<cfcomponent displayname="Restricted Area" extends="Base" output="false">
  <cffunction name="getRestrictedGroupIDList" access="public" output="no" returntype="string">
    <cfargument name="URLQueryStuct" type="struct" required="yes">  
    <cfargument name="URLQueryString" type="string" required="yes">
    
    <cfset var args=Arguments.URLQueryStuct>
    <cfset var restrictedGroupIDList="">
           
    <cfquery name="qPages" datasource="#this.datasource#">
      SELECT groupID, template, keyName, keyValue
      FROM pp_page_group
      WHERE module=<cfqueryparam cfsqltype="cf_sql_varchar" value="#args.md#">
	</cfquery>
    
    <cfif qPages.recordcount GT 0>
      <cfswitch expression="#args.md#">
        <cfcase value="pagebuilder">
          <cfif IsDefined("args.pid") And IsNumeric(args.pid)>
            <cfquery name="qCatID" datasource="#this.datasource#">
              SELECT pageCategoryID
              FROM pb_page
              WHERE pageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#args.pid#">
            </cfquery>
            <cfif qCatID.recordcount GT 0>
              <cfset Arguments.URLQueryString=ListAppend(Arguments.URLQueryString,"tmp=home","&")>
              <cfset Arguments.URLQueryString=ListAppend(Arguments.URLQueryString,"catid=#qCatID.pageCategoryID#","&")>
            </cfif>
          </cfif>
        </cfcase>                
        <cfcase value="newsroom">
          <cfif Not Compare(args.tmp, "archives") And IsDefined("args.catid")>
            <cfset Arguments.URLQueryString=ListAppend(Arguments.URLQueryString,"tmp=category","&")>
          <cfelseif Not Compare(args.tmp, "detail") And IsDefined("args.articleID") And IsNumeric(args.articleID)>
            <cfquery name="qCatIDs" datasource="#this.datasource#">
              SELECT articleCategoryID
              FROM nr_article_articlecategory
              WHERE articleID=<cfqueryparam cfsqltype="cf_sql_integer" value="#args.articleID#">
            </cfquery>
            <cfif qCatIDs.recordcount GT 0>
              <cfset Arguments.URLQueryString=ListAppend(Arguments.URLQueryString,"tmp=category","&")>
              <cfloop query="qCatIDs">
                <cfset Arguments.URLQueryString=ListAppend(Arguments.URLQueryString,"catid=#qCatIDs.articleCategoryID#","&")>
              </cfloop>
            </cfif>
          </cfif>
        </cfcase>    
		<cfcase value="event">
          <cfif Not Compare(args.tmp, "detail") And IsDefined("args.eventid") And IsNumeric(args.eventid)>
            <cfquery name="qCatIDs" datasource="#this.datasource#">
              SELECT categoryID
              FROM em_event_category
              WHERE eventID=<cfqueryparam cfsqltype="cf_sql_integer" value="#args.eventID#">
            </cfquery>
            <cfif qCatIDs.recordcount GT 0>
              <cfset Arguments.URLQueryString=ListAppend(Arguments.URLQueryString,"tmp=category","&")>
              <cfloop query="qCatIDs">
                <cfset Arguments.URLQueryString=ListAppend(Arguments.URLQueryString,"catid=#qCatIDs.categoryID#","&")>
              </cfloop>
            </cfif>
          </cfif>
        </cfcase>    
      </cfswitch>
      
      <cfloop query="qPages">
        <cfif ListFindNoCase(Arguments.URLQueryString, "tmp=#template#", "&") GT 0 And ListFindNoCase(Arguments.URLQueryString, "#keyName#=#keyValue#", "&") GT 0>
          <cfset restrictedGroupIDList=ListAppend(restrictedGroupIDList,groupID,",")>
        </cfif>
      </cfloop>
	</cfif>
    
    <cfreturn restrictedGroupIDList>
  </cffunction>

  <cffunction name="login" access="public" output="no" returntype="boolean">
    <cfargument name="username" type="string" required="yes">
    <cfargument name="password" type="string" required="yes">
    
    <cfquery name="qUserGroupIDs" datasource="#this.datasource#">
      SELECT pp_user_group.groupID
      FROM pp_user INNER JOIN pp_user_group ON pp_user.userID=pp_user_group.userID
  	  WHERE pp_user.username=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.username#"> AND
      		pp_user.password=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.password#"> AND
            pp_user.published=1
    </cfquery>
    <cfif qUserGroupIDs.recordcount GT 0>
      <cfset newGroupIDList="">
      <cfif IsDefined("cookie.groupIDList")>
        <cfset newGroupIDList=cookie.groupIDList>
        <cfloop index="groupID" list="#ValueList(qUserGroupIDs.groupID)#">
    	  <cfif ListFind(newGroupIDList, groupID) Is 0>
            <cfset newGroupIDList=ListAppend(newGroupIDList, groupID)>
          </cfif>
        </cfloop>
      <cfelse>
        <cfset newGroupIDList="#ValueList(qUserGroupIDs.groupID)#">
      </cfif>
      <cfcookie name="groupIDList" value="#newGroupIDList#">
      <cfreturn true>
    <cfelse>
      <cfreturn false>
    </cfif>
  </cffunction>
</cfcomponent>