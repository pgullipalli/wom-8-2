<cfcomponent displayname="Class Admin" extends="Base" output="false">
  <cffunction name="getAllCategories" access="public" output="no" returntype="query">
	<cfquery name="qAllCategories" datasource="#this.datasource#">
	  SELECT categoryID, categoryName
	  FROM cl_category
	  ORDER BY displaySeq	
	</cfquery>	
    
    <cfreturn qAllCategories>  
  </cffunction>
  
  <cffunction name="getCategorySummary" access="public" output="no" returntype="query">   
    <cfquery name="qCatSummary" datasource="#this.datasource#">
	  SELECT categoryName, categoryID, featured, displaySeq
	  FROM cl_category	
	</cfquery>
    
    <cfquery name="qNumCourses" datasource="#this.datasource#">
	  SELECT cl_category.categoryID,
			 COUNT(cl_course_category.courseID) AS numCourses
	  FROM cl_category LEFT JOIN cl_course_category ON
	  	   cl_category.categoryID=cl_course_category.categoryID
	  GROUP BY cl_category.categoryID
	</cfquery>
    
    <cfquery name="qNumClasses" datasource="#this.datasource#">
	  SELECT cl_category.categoryID,			 
             COUNT(cl_class.classID) AS numClasses
	  FROM (cl_category LEFT JOIN cl_course_category ON
	  	   cl_category.categoryID=cl_course_category.categoryID) LEFT JOIN cl_class ON
           cl_course_category.courseID=cl_class.courseID
	  GROUP by cl_category.categoryID	
	</cfquery>
    
    <cfquery name="qNumServices" datasource="#this.datasource#">
	  SELECT cl_category.categoryID,
			 COUNT(cl_service_category.serviceID) AS numServices
	  FROM cl_category LEFT JOIN cl_service_category ON
	  	   cl_category.categoryID=cl_service_category.categoryID
	  GROUP BY cl_category.categoryID
	</cfquery>
    
    <cfquery name="qCatSummary" dbtype="query">
	  SELECT qCatSummary.categoryName, qCatSummary.categoryID, qCatSummary.featured, qCatSummary.displaySeq, qNumCourses.numCourses
	  FROM qCatSummary, qNumCourses
      WHERE qCatSummary.categoryID=qNumCourses.categoryID
	</cfquery>
    
    <cfquery name="qCatSummary" dbtype="query">
	  SELECT qCatSummary.categoryName, qCatSummary.categoryID, qCatSummary.featured, qCatSummary.numCourses, qCatSummary.displaySeq, qNumClasses.numClasses
	  FROM qCatSummary, qNumClasses
      WHERE qCatSummary.categoryID=qNumClasses.categoryID
	</cfquery>
    
    <cfquery name="qCatSummary" dbtype="query">
	  SELECT qCatSummary.categoryName, qCatSummary.categoryID, qCatSummary.featured, qCatSummary.numCourses, qCatSummary.displaySeq, qCatSummary.numClasses, qNumServices.numServices
	  FROM qCatSummary, qNumServices
      WHERE qCatSummary.categoryID=qNumServices.categoryID
      ORDER BY qCatSummary.displaySeq
	</cfquery>
    
    <cfreturn qCatSummary>
  </cffunction>
  
  <cffunction name="getFirstCategoryID" access="public" output="no" returntype="numeric">
    <cfquery name="qCategoryID" datasource="#this.datasource#">
	  SELECT categoryID
	  FROM cl_category
      ORDER BY displaySeq
	</cfquery>
    <cfif qCategoryID.recordcount GT 0>
	  <cfreturn qCategoryID.categoryID[1]>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="getCategory" access="public" output="no" returntype="query">
    <cfargument name="categoryID" required="yes" type="numeric">	
	<cfquery name="qCategory" datasource="#this.datasource#">
	  SELECT *
      FROM cl_category
      WHERE categoryID=<cfqueryparam value="#Arguments.categoryID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
	<cfreturn qCategory>
  </cffunction>
  
  <cffunction name="addCategory" access="public" output="no">
    <cfargument name="categoryName" type="string" required="yes">
    <cfargument name="featured" type="boolean" default="0">
    <cfargument name="description" type="string" default="">
    
    <cfquery name="qMaxSeq" datasource="#this.datasource#">
      SELECT MAX(displaySeq) AS maxSeq FROM cl_category
    </cfquery>
    <cfif IsNumeric(qMaxSeq.maxSeq)>
      <cfset newDisplaySeq=qMaxSeq.maxSeq + 1>
    <cfelse>
      <cfset newDisplaySeq=1>
    </cfif>

    <cfquery datasource="#this.datasource#">
	  INSERT INTO cl_category
        (categoryName, displaySeq, description, featured, dateCreated, dateLastModified)
	  VALUES
        (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.categoryName#">,
         <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">,         
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.description#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.featured#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)	  
	</cfquery>  
  </cffunction>
  
  <cffunction name="editCategory" access="public" output="no">
    <cfargument name="categoryID" type="numeric" required="yes">
    <cfargument name="categoryName" type="string" required="yes">
    <cfargument name="featured" type="boolean" default="0">
    <cfargument name="description" type="string" default="">

    <cfquery datasource="#this.datasource#">
	  UPDATE cl_category
      SET categoryName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.categoryName#">,
    	  featured=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.featured#">,
      	  description=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.description#">,          
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">  
	</cfquery>  
  </cffunction>
    
  <cffunction name="deleteCategory" access="public" output="false" returntype="boolean">
    <cfargument name="categoryID" required="yes" type="numeric">
	
	<cfquery name="qEvents" datasource="#this.datasource#">
	  SELECT eventID
	  FROM cl_event_category
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">			
	</cfquery>
	
	<cfif qEvents.recordcount GT 0>
	  <cfreturn false>
	</cfif>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cl_category
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
	</cfquery>
    
	<cfreturn true>
  </cffunction>
  
  <cffunction name="moveCategory" access="public" output="no">
    <cfargument name="categoryID" required="yes" type="numeric">
    <cfargument name="direction" required="yes" type="string">
    
    <cfquery name="qTargetDisplaySeq" datasource="#this.datasource#">
      SELECT displaySeq
      FROM cl_category
      WHERE categoryID=<cfqueryparam value="#Arguments.categoryID#" cfsqltype="cf_sql_integer">
    </cfquery>
    
    <cfset targetDisplaySeq=qTargetDisplaySeq.displaySeq>
    
    <cfif Arguments.direction IS "up">
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT categoryID, displaySeq
        FROM cl_category
        WHERE displaySeq < <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq DESC
      </cfquery>
    <cfelse>
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT categoryID, displaySeq
        FROM cl_category
        WHERE displaySeq > <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq ASC
      </cfquery>
    </cfif>
    
    <cfif qNextDisplaySeq.recordcount Is 0><cfreturn></cfif>
    
    <cfset nextDisplaySeq=qNextDisplaySeq.displaySeq>
        
    <cfquery datasource="#this.datasource#">
      UPDATE cl_category
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#nextDisplaySeq#">
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_category
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qNextDisplaySeq.categoryID#">
    </cfquery>
  </cffunction>
  
  
  <cffunction name="getLocations" access="public" output="no" returntype="query">
	<cfquery name="qAllLocations" datasource="#this.datasource#">
	  SELECT *
	  FROM cl_location
	  ORDER BY locationName	
	</cfquery>	
    
    <cfreturn qAllLocations>  
  </cffunction>
  
  <cffunction name="getLocation" access="public" output="no" returntype="query">
    <cfargument name="locationID" required="yes" type="numeric">	
	<cfquery name="qLocation" datasource="#this.datasource#">
	  SELECT *
      FROM cl_location
      WHERE locationID=<cfqueryparam value="#Arguments.locationID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
	<cfreturn qLocation>
  </cffunction>
  
  <cffunction name="addLocation" access="public" output="no">
    <cfargument name="locationName" type="string" required="yes">

    <cfquery datasource="#this.datasource#">
	  INSERT INTO cl_location
        (locationName, dateCreated, dateLastModified)
	  VALUES
        (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.locationName#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)	  
	</cfquery>  
  </cffunction>
  
  <cffunction name="editLocation" access="public" output="no">
    <cfargument name="locationID" type="numeric" required="yes">
    <cfargument name="locationName" type="string" required="yes">

    <cfquery datasource="#this.datasource#">
	  UPDATE cl_location
      SET locationName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.locationName#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE locationID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.locationID#">  
	</cfquery>  
  </cffunction>
    
  <cffunction name="deleteLocation" access="public" output="false" returntype="boolean">
    <cfargument name="locationID" required="yes" type="numeric">
	
	<cfquery name="qClassInstances" datasource="#this.datasource#">
	  SELECT classInstanceID
	  FROM cl_classinstance
      WHERE locationID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.locationID#">			
	</cfquery>
	
	<cfif qClassInstances.recordcount GT 0>
	  <cfreturn false>
	</cfif>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM cl_location
      WHERE locationID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.locationID#">
	</cfquery>
    
	<cfreturn true>
  </cffunction>  
  
  <cffunction name="getMaps" access="public" output="no" returntype="query">
    <cfquery name="qMaps" datasource="#this.datasource#">
      SELECT *
      FROM dr_map
      ORDER BY mapName
    </cfquery>
    <cfreturn qMaps>
  </cffunction>
  
  <cffunction name="getMap" access="public" output="no" returntype="query">
    <cfargument name="mapID" required="yes" type="numeric">
    
    <cfquery name="qMap" datasource="#this.datasource#">
      SELECT *
      FROM dr_map
      WHERE mapID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.mapID#">
    </cfquery>
    <cfreturn qMap>
  </cffunction>
  
  <cffunction name="verifyCampusMap" access="public" output="no">
    <cfargument name="locationID" required="yes" type="numeric">
    <cfargument name="mapID" required="yes" type="numeric">
    <cfargument name="posX" type="numeric" default="0">
    <cfargument name="posY" type="numeric" default="0">
    
	<cfquery datasource="#this.datasource#">
	  UPDATE cl_location
      SET mapID=<cfqueryparam value="#Arguments.mapID#" cfsqltype="cf_sql_integer">,
          posX=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.posX#">,
          posY=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.posY#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE locationID=<cfqueryparam value="#Arguments.locationID#" cfsqltype="cf_sql_integer">
	</cfquery>	
  </cffunction>

  <cffunction name="getCoursesByCategoryID" access="public" output="false" returntype="struct">
    <cfargument name="categoryID" type="numeric" required="yes">
	<cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.courses=QueryNew("courseID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qNumCourses" datasource="#this.datasource#">
	  SELECT COUNT(cl_course.courseID) AS numItems
	  FROM	 cl_course INNER JOIN cl_course_category ON
	  		 cl_course.courseID=cl_course_category.courseID
	  WHERE	 cl_course_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
    </cfquery>
	<cfset resultStruct.numAllItems = qNumCourses.numItems>
	
	<cfquery name="qCourses" datasource="#this.datasource#">
	  SELECT cl_course.courseID, cl_course.GLNumber, cl_course.published, cl_course.featuredOnHomepage, cl_course.courseTitle,      		 
             cl_course.feeRegular, cl_course.feeEmployee, cl_course.feeDoctor, cl_course.feeFitnessClub,
             cl_course.regLimit, cl_course.regMaximum, 
             cl_course.featuredOnClassHomePage, cl_course.registerOnline, cl_course.feePerCouple, cl_course.callForPrice
	  FROM   cl_course INNER JOIN cl_course_category ON
	         cl_course.courseID=cl_course_category.courseID   
	  WHERE  cl_course_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
	  ORDER  BY cl_course.courseTitle
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>

    <cfif qCourses.recordcount GT 0>    
      <cfquery name="qClasses" datasource="#this.datasource#">
        SELECT cl_course.courseID,
               COUNT(cl_class.classID) AS numClasses
        FROM   cl_course LEFT JOIN cl_class ON
               cl_course.courseID=cl_class.courseID
        WHERE  cl_course.courseID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#valueList(qCourses.courseID)#" list="yes">)
        GROUP BY cl_course.courseID	  		
      </cfquery>
      
      <cfquery name="qPrerequisites" datasource="#this.datasource#">
        SELECT cl_course.courseID,
               COUNT(cl_course_course_prerequired.courseID2) AS numPrerequisites
        FROM   cl_course LEFT JOIN cl_course_course_prerequired ON
               cl_course.courseID=cl_course_course_prerequired.courseID1
        WHERE  cl_course.courseID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#valueList(qCourses.courseID)#" list="yes">)
        GROUP BY cl_course.courseID	  		
      </cfquery>
      
      <cfquery name="qAssociatedCourses" datasource="#this.datasource#">
        SELECT cl_course.courseID,
               COUNT(cl_course_course_associated.courseID2) AS numAssociatedCourses
        FROM   cl_course LEFT JOIN cl_course_course_associated ON
               cl_course.courseID=cl_course_course_associated.courseID1
        WHERE  cl_course.courseID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#valueList(qCourses.courseID)#" list="yes">)
        GROUP BY cl_course.courseID	  		
      </cfquery>
      
      <cfquery name="qReviews" datasource="#this.datasource#">
        SELECT cl_course.courseID,
               COUNT(cl_coursereview.courseReviewID) AS numReviews
        FROM   cl_course LEFT JOIN cl_coursereview ON
               cl_course.courseID=cl_coursereview.courseID
        WHERE  cl_course.courseID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#valueList(qCourses.courseID)#" list="yes">)
        GROUP BY cl_course.courseID	  		
      </cfquery>
        
      <cfquery name="qCourses" dbtype="query">
        SELECT *
        FROM qCourses, qClasses
        WHERE qCourses.courseID=qClasses.courseID
      </cfquery>
      
      <cfquery name="qCourses" dbtype="query">
        SELECT *
        FROM qCourses, qPrerequisites
        WHERE qCourses.courseID=qPrerequisites.courseID
      </cfquery>
      
      <cfquery name="qCourses" dbtype="query">
        SELECT *
        FROM qCourses, qAssociatedCourses
        WHERE qCourses.courseID=qAssociatedCourses.courseID
      </cfquery>
      
      <cfquery name="qCourses" dbtype="query">
        SELECT *
        FROM qCourses, qReviews
        WHERE qCourses.courseID=qReviews.courseID
        ORDER BY courseTitle
      </cfquery>
            
      <cfset resultStruct.numDisplayedItems=qCourses.recordcount>	
      <cfset resultStruct.courses = qCourses>
	<cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.courses = QueryNew("courseID")>
    </cfif>
    
	<cfreturn resultStruct>
  </cffunction>
    
  <cffunction name="getCourse" access="public" output="no" returntype="query">
    <cfargument name="courseID" type="numeric" required="yes">
    
    <cfquery name="qCourse" datasource="#this.datasource#">
      SELECT *
      FROM cl_course
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
    
    <cfreturn qCourse>
  </cffunction>
  
  <cffunction name="getCategoryIDListByCourseID" access="public" output="no" returntype="string">
    <cfargument name="courseID" type="numeric" required="yes">
    
    <cfquery name="qCategoryIDs" datasource="#this.datasource#">
      SELECT categoryID
      FROM cl_course_category
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
    <cfif qCategoryIDs.recordcount GT 0>
      <cfreturn ValueList(qCategoryIDs.categoryID)>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="addCourse" access="public" output="no" returntype="boolean">
    <cfargument name="categoryIDList" type="string" default="">
    <cfargument name="GLNumber" type="string" default="">
    <cfargument name="courseTitle" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">  
    <cfargument name="featuredOnHomepage" type="boolean" default="0">
    <cfargument name="featuredOnClassHomePage" type="boolean" default="0">
    <cfargument name="registerOnline" type="boolean" default="0">
    <cfargument name="fitnessClubOnly" type="boolean" default="0">    
    <cfargument name="feeRegular" type="numeric" required="yes">
    <cfargument name="feeEmployee" type="numeric" required="yes">
    <cfargument name="feeDoctor" type="numeric" required="yes">
    <cfargument name="feeFitnessClub" type="numeric" required="yes">
    <cfargument name="callForPrice" type="boolean" default="0">
    <cfargument name="feePerCouple" type="boolean" default="0">
    <cfargument name="attendeeNameLabel" type="string" default="">
    <cfargument name="guestNameLabel" type="string" default="">
    <cfargument name="discountCode" type="string" default="">
    <cfargument name="discountType" type="string" default="flat rate">
    <cfargument name="contactName" type="string" default="">
    <cfargument name="contactEmail" type="string" default="">
    <cfargument name="contactPhone" type="string" default="">
    <cfargument name="courseImage" type="string" default="">
    <cfargument name="courseImageCaption" type="string" default="">
    <cfargument name="courseThumbnail" type="string" default="">
    <cfargument name="shortDescription" type="string" default="">
    <cfargument name="longDescription" type="string" default="">
    <cfargument name="specialInstructions" type="string" default="">
	<cfargument name="registrationURL" type="string" default="">
    <cfargument name="outsideEventURL" type="string" default="">
    <cfargument name="sendReminderEmail" type="boolean" default="0">
    <cfargument name="sendFollowUpEmailFeedback" type="boolean" default="0">
    <cfargument name="FollowUpEmailMissedClass" type="boolean" default="0">
       
    <cfif Not IsNumeric(Arguments.regLimit)>
      <cfset Arguments.regLimit=10000>
    </cfif>
    <cfif Not IsNumeric(Arguments.regMaximum)>
      <cfset Arguments.regMaximum=10000>
    </cfif>
    <cfif Not IsNumeric(Arguments.discountRate)>
      <cfset Arguments.discountRate=0>
    </cfif>
    <cfif Not IsNumeric(Arguments.directoryItemID)>
      <cfset Arguments.directoryItemID=0>
    </cfif>
    <cfif Not IsNumeric(Arguments.pageID)>
      <cfset Arguments.pageID=0>
    </cfif>
        
    <cfif Not Compare(Arguments.categoryIDList, "")><cfreturn false></cfif>
    
    <cfif Arguments.featuredOnClassHomePage>
      <cfquery datasource="#this.datasource#">
        UPDATE cl_course
        SET featuredOnClassHomePage=0
      </cfquery>
    </cfif>
    		
    <cfquery datasource="#this.datasource#">
	  INSERT INTO cl_course
        (GLNumber, courseTitle, published, featuredOnHomepage, featuredOnClassHomePage, registerOnline, fitnessClubOnly,
         feeRegular, feeEmployee, feeDoctor, feeFitnessClub,
         regLimit, regMaximum,
         callForPrice, feePerCouple, attendeeNameLabel, guestNameLabel,         
         discountCode, discountType, discountRate,         
         directoryItemID, contactName, contactEmail, contactPhone,         
         courseImage, courseImageCaption, courseThumbnail,        
         shortDescription, longDescription, specialInstructions,         
         registrationURL, outsideEventURL, pageID, sendReminderEmail, sendFollowUpEmailFeedback, FollowUpEmailMissedClass,         
         dateCreated, dateLastModified)
	  VALUES
	    (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.GLNumber#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.courseTitle#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.featuredOnHomepage#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.featuredOnClassHomePage#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.registerOnline#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.fitnessClubOnly#">,
         
         <cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeRegular#">,
         <cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeEmployee#">,
         <cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeDoctor#">,
         <cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeFitnessClub#">,  
         
         <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.regLimit#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.regMaximum#">,    

         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.callForPrice#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.feePerCouple#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.attendeeNameLabel#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.guestNameLabel#">,
         
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.discountCode#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.discountType#">,
         <cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.discountRate#">,
         
         <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.directoryItemID#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactName#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactEmail#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactPhone#">,
         
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.courseImage#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.courseImageCaption#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.courseThumbnail#">,
         
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.shortDescription#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.longDescription#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.specialInstructions#">,         
         
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.registrationURL#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.outsideEventURL#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageID#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.sendReminderEmail#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.sendFollowUpEmailFeedback#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.FollowUpEmailMissedClass#">,
         
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)		
	</cfquery>

	<cfquery name="qCourseID" datasource="#this.datasource#">
	  SELECT MAX(courseID) AS newCourseID
	  FROM cl_course
    </cfquery>
	<cfset Variables.courseID=qCourseID.newCourseID>	
    
    <cfloop index="categoryID" list="#Arguments.categoryIDList#">      
      <cfquery datasource="#this.datasource#">
		INSERT INTO cl_course_category (courseID, categoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.courseID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>

  <cffunction name="editCourse" access="public" output="no" returntype="boolean">
    <cfargument name="courseID" type="numeric" required="yes">
    <cfargument name="GLNumber" type="string" default="">
    <cfargument name="categoryIDList" type="string" default="">
    <cfargument name="courseTitle" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="featuredOnHomepage" type="boolean" default="0">
    <cfargument name="featuredOnClassHomePage" type="boolean" default="0">
    <cfargument name="registerOnline" type="boolean" default="0">
    <cfargument name="fitnessClubOnly" type="boolean" default="0"> 
    <cfargument name="feeRegular" type="numeric" required="yes">
    <cfargument name="feeEmployee" type="numeric" required="yes">
    <cfargument name="feeDoctor" type="numeric" required="yes">
    <cfargument name="feeFitnessClub" type="numeric" required="yes">
    <cfargument name="callForPrice" type="boolean" default="0">
    <cfargument name="feePerCouple" type="boolean" default="0">
    <cfargument name="attendeeNameLabel" type="string" default="">
    <cfargument name="guestNameLabel" type="string" default="">
    <cfargument name="discountCode" type="string" default="">
    <cfargument name="discountType" type="string" default="flat rate">
    <cfargument name="contactName" type="string" default="">
    <cfargument name="contactEmail" type="string" default="">
    <cfargument name="contactPhone" type="string" default="">
    <cfargument name="courseImage" type="string" default="">
    <cfargument name="courseImageCaption" type="string" default="">
    <cfargument name="courseThumbnail" type="string" default="">
    <cfargument name="shortDescription" type="string" default="">
    <cfargument name="longDescription" type="string" default="">
    <cfargument name="specialInstructions" type="string" default="">
	<cfargument name="registrationURL" type="string" default="">
    <cfargument name="outsideEventURL" type="string" default="">
    <cfargument name="sendReminderEmail" type="boolean" default="0">
    <cfargument name="sendFollowUpEmailFeedback" type="boolean" default="0">
    <cfargument name="FollowUpEmailMissedClass" type="boolean" default="0">
       
    <cfif Not IsNumeric(Arguments.regLimit)>
      <cfset Arguments.regLimit=10000>
    </cfif>
    <cfif Not IsNumeric(Arguments.regMaximum)>
      <cfset Arguments.regMaximum=10000>
    </cfif>
    <cfif Not IsNumeric(Arguments.discountRate)>
      <cfset Arguments.discountRate=0>
    </cfif>
    <cfif Not IsNumeric(Arguments.directoryItemID)>
      <cfset Arguments.directoryItemID=0>
    </cfif>
    <cfif Not IsNumeric(Arguments.pageID)>
      <cfset Arguments.pageID=0>
    </cfif>
        
    <cfif Not Compare(Arguments.categoryIDList, "")><cfreturn false></cfif>
    
    <cfif Arguments.featuredOnClassHomePage>
      <cfquery datasource="#this.datasource#">
        UPDATE cl_course
        SET featuredOnClassHomePage=0
      </cfquery>
    </cfif>

    <cfquery datasource="#this.datasource#">
	  UPDATE cl_course
      SET GLNumber=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.GLNumber#">,
      	  courseTitle=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.courseTitle#">,
      	  published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
          featuredOnHomepage=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.featuredOnHomepage#">,
          featuredOnClassHomePage=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.featuredOnClassHomePage#">,
          registerOnline=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.registerOnline#">,
          fitnessClubOnly=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.fitnessClubOnly#">,          
          
          feeRegular=<cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeRegular#">,
          feeEmployee=<cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeEmployee#">,
          feeDoctor=<cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeDoctor#">,
          feeFitnessClub=<cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeFitnessClub#">,
          
          regLimit=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.regLimit#">,
          regMaximum=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.regMaximum#">,   
         
          callForPrice=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.callForPrice#">,
          feePerCouple=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.feePerCouple#">,          
          attendeeNameLabel=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.attendeeNameLabel#">,
          guestNameLabel=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.guestNameLabel#">,
          
          discountCode=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.discountCode#">,
          discountType=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.discountType#">,
          discountRate=<cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.discountRate#">,
         
          directoryItemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.directoryItemID#">,
          contactName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactName#">,
          contactEmail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactEmail#">,
          contactPhone=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactPhone#">,
          
          courseImage=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.courseImage#">,
          courseImageCaption=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.courseImageCaption#">,
          courseThumbnail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.courseThumbnail#">,
         
          shortDescription=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.shortDescription#">,
          longDescription=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.longDescription#">,
          specialInstructions=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.specialInstructions#">,
          <!--- regLimit=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.regLimit#">,
          regMaximum=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.regMaximum#">, --->
         
          registrationURL=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.registrationURL#">,
          outsideEventURL=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.outsideEventURL#">,
          pageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.pageID#">,
          sendReminderEmail=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.sendReminderEmail#">,
          sendFollowUpEmailFeedback=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.sendFollowUpEmailFeedback#">,
          FollowUpEmailMissedClass=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.FollowUpEmailMissedClass#">,        
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
	</cfquery>    
        
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_course_category
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
    
    <cfloop index="categoryID" list="#Arguments.categoryIDList#">    
      <cfquery datasource="#this.datasource#">
		INSERT INTO cl_course_category (courseID, categoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>
  
  <cffunction name="deleteCourse" access="public" output="no" returntype="boolean">
    <cfargument name="courseID" required="yes" type="numeric">
    
    <!--- Need to verify if this course contains classes; return false if yes --->
    <cfquery name="qClasses" datasource="#this.datasource#">
      SELECT classID
      FROM cl_class
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
    <cfif qClasses.recordcount GT 0>
      <cfreturn false>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_course_category
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_course
	  WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_course_course_prerequired
      WHERE courseID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#"> OR
      		courseID2=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_course_course_associated
      WHERE courseID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#"> OR
      		courseID2=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_course_curricula
	  WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_course_service_associated
	  WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_coursereview
	  WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>   
        
    <cfreturn true>
  </cffunction>
  
  <cffunction name="getPrerequisites" access="public" output="no" returntype="query">
    <cfargument name="courseID" type="numeric" required="yes">
    
    <cfquery name="qPrerequisites" datasource="#this.datasource#">
      SELECT cl_course.courseID, cl_course.courseTitle
      FROM cl_course_course_prerequired INNER JOIN cl_course ON
      	   cl_course_course_prerequired.courseID2=cl_course.courseID
      WHERE cl_course_course_prerequired.courseID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
  
    <cfreturn qPrerequisites>
  </cffunction>
  
  <cffunction name="addPrerequisites" access="public" output="no">
    <cfargument name="courseID" type="numeric" required="yes">
    <cfargument name="courseIDList" type="string" default="">
    
    <cfif Compare(Arguments.courseIDList,"")>
      <cfset prerequisties=getPrerequisites(Arguments.courseID)>
      <cfif prerequisties.recordcount GT 0>
        <cfset selectedCourseIDList=ValueList(prerequisties.courseID)>
      <cfelse>
        <cfset selectedCourseIDList="">
      </cfif>
  	  
      <cfloop index="newCourseID" list="#Arguments.courseIDList#">
  		<cfif ListFind(selectedCourseIDList, newCourseID) Is 0>
          <cfquery datasource="#this.datasource#">
            INSERT INTO cl_course_course_prerequired (courseID1, courseID2)
            VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">,
            		<cfqueryparam cfsqltype="cf_sql_integer" value="#newCourseID#">)
          </cfquery>
        </cfif>
      </cfloop>
    </cfif>  
  </cffunction>
  
  <cffunction name="deletePrerequisites" access="public" output="no">
    <cfargument name="courseID" type="numeric" required="yes">
    <cfargument name="courseIDList" type="string" default="">
    
    <cfif Compare(Arguments.courseIDList,"")>
      <cfquery datasource="#this.datasource#">
        DELETE FROM cl_course_course_prerequired
        WHERE courseID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#"> AND
      		  courseID2 IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.courseIDList#" list="yes">)
      </cfquery>
    </cfif>
  </cffunction>
  
  <cffunction name="getAssociatedCourses" access="public" output="no" returntype="query">
    <cfargument name="courseID" type="numeric" required="yes">
    
    <cfquery name="qAssociatedCourses" datasource="#this.datasource#">
      SELECT cl_course.courseID, cl_course.courseTitle
      FROM cl_course_course_associated INNER JOIN cl_course ON
      	   cl_course_course_associated.courseID2=cl_course.courseID
      WHERE cl_course_course_associated.courseID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
  
    <cfreturn qAssociatedCourses>
  </cffunction>
  
  <cffunction name="addAssociatedCourses" access="public" output="no">
    <cfargument name="courseID" type="numeric" required="yes">
    <cfargument name="courseIDList" type="string" default="">
    
    <cfif Compare(Arguments.courseIDList,"")>
      <cfset associatedCourses=getAssociatedCourses(Arguments.courseID)>
      <cfif associatedCourses.recordcount GT 0>
        <cfset selectedCourseIDList=ValueList(associatedCourses.courseID)>
      <cfelse>
        <cfset selectedCourseIDList="">
      </cfif>
  	  
      <cfloop index="newCourseID" list="#Arguments.courseIDList#">
  		<cfif ListFind(selectedCourseIDList, newCourseID) Is 0>
          <cfquery datasource="#this.datasource#">
            INSERT INTO cl_course_course_associated (courseID1, courseID2)
            VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">,
            		<cfqueryparam cfsqltype="cf_sql_integer" value="#newCourseID#">)
          </cfquery>
        </cfif>
      </cfloop>
    </cfif>  
  </cffunction>
  
  <cffunction name="deleteAssociatedCourses" access="public" output="no">
    <cfargument name="courseID" type="numeric" required="yes">
    <cfargument name="courseIDList" type="string" default="">
    
    <cfif Compare(Arguments.courseIDList,"")>
      <cfquery datasource="#this.datasource#">
        DELETE FROM cl_course_course_associated
        WHERE courseID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#"> AND
      		  courseID2 IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.courseIDList#" list="yes">)
      </cfquery>
    </cfif>
  </cffunction>
  
  <cffunction name="getCourseReviews" access="public" output="no" returntype="query">
    <cfargument name="courseID" type="numeric" required="yes">
    
    <cfquery name="qCourseReviews" datasource="#this.datasource#">
      SELECT courseReviewID, published, fullName, review
      FROM cl_coursereview
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
  
    <cfreturn qCourseReviews>
  </cffunction>
  
  <cffunction name="addCourseReview" access="public" output="no">
    <cfargument name="courseID" type="numeric" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="fullName" type="string" default="">
    <cfargument name="review" type="string" default="">
    
    <cfquery datasource="#this.datasource#">
      INSERT INTO cl_coursereview
       (courseID, published, fullName, review, dateCreated, dateLastModified)
      VALUES
       (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">,
        <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.fullName#">,
        <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.review#">,
        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
    </cfquery> 
  </cffunction>
 
  <cffunction name="getCourseReview" access="public" output="no" returntype="query">
    <cfargument name="courseReviewID" type="numeric" required="yes">
    
    <cfquery name="qReview" datasource="#this.datasource#">
      SELECT *
      FROM cl_coursereview
      WHERE courseReviewID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseReviewID#">
    </cfquery>
    
    <cfreturn qReview>
  </cffunction>
  
  <cffunction name="editCourseReview" access="public" output="no">
    <cfargument name="courseReviewID" type="numeric" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="fullName" type="string" default="">
    <cfargument name="review" type="string" default="">
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_coursereview
      SET published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
      	  fullName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.fullName#">,
          review=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.review#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE courseReviewID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseReviewID#">
    </cfquery> 
  </cffunction>
  
  <cffunction name="deleteCourseReviews" access="public" output="no">
    <cfargument name="courseReviewIDList" type="string" default="">
    
    <cfif Compare(Arguments.courseReviewIDList,"")>
      <cfquery datasource="#this.datasource#">
        DELETE FROM cl_coursereview
        WHERE courseReviewID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.courseReviewIDList#" list="yes">)
      </cfquery>
    </cfif>
  </cffunction>
  
  <cffunction name="getServiceReviews" access="public" output="no" returntype="query">
    <cfargument name="serviceID" type="numeric" required="yes">
    
    <cfquery name="qServiceReviews" datasource="#this.datasource#">
      SELECT serviceReviewID, published, fullName, review
      FROM cl_servicereview
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
    </cfquery>
  
    <cfreturn qServiceReviews>
  </cffunction>
  
  <cffunction name="addServiceReview" access="public" output="no">
    <cfargument name="serviceID" type="numeric" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="fullName" type="string" default="">
    <cfargument name="review" type="string" default="">
    
    <cfquery datasource="#this.datasource#">
      INSERT INTO cl_servicereview
       (serviceID, published, fullName, review, dateCreated, dateLastModified)
      VALUES
       (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">,
        <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.fullName#">,
        <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.review#">,
        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
    </cfquery> 
  </cffunction>
 
  <cffunction name="getServiceReview" access="public" output="no" returntype="query">
    <cfargument name="serviceReviewID" type="numeric" required="yes">
    
    <cfquery name="qReview" datasource="#this.datasource#">
      SELECT *
      FROM cl_servicereview
      WHERE serviceReviewID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceReviewID#">
    </cfquery>
    
    <cfreturn qReview>
  </cffunction>
  
  <cffunction name="editServiceReview" access="public" output="no">
    <cfargument name="serviceReviewID" type="numeric" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="fullName" type="string" default="">
    <cfargument name="review" type="string" default="">
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_servicereview
      SET published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
      	  fullName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.fullName#">,
          review=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.review#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE serviceReviewID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceReviewID#">
    </cfquery> 
  </cffunction>
  
  <cffunction name="deleteServiceReviews" access="public" output="no">
    <cfargument name="serviceReviewIDList" type="string" default="">
    
    <cfif Compare(Arguments.serviceReviewIDList,"")>
      <cfquery datasource="#this.datasource#">
        DELETE FROM cl_servicereview
        WHERE serviceReviewID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.serviceReviewIDList#" list="yes">)
      </cfquery>
    </cfif>
  </cffunction>
 
  <cffunction name="getServicesByCategoryID" access="public" output="false" returntype="struct">
    <cfargument name="categoryID" type="numeric" required="yes">
	<cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.services=QueryNew("serviceID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qNumServices" datasource="#this.datasource#">
	  SELECT COUNT(cl_service.serviceID) AS numItems
	  FROM	 cl_service INNER JOIN cl_service_category ON
	  		 cl_service.serviceID=cl_service_category.serviceID
	  WHERE	 cl_service_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
    </cfquery>
	<cfset resultStruct.numAllItems = qNumServices.numItems>
	
	<cfquery name="qServices" datasource="#this.datasource#">
	  SELECT cl_service.serviceID, cl_service.GLNumber, cl_service.published, cl_service.serviceTitle,
             cl_service.feeRegular
	  FROM   cl_service INNER JOIN cl_service_category ON
	         cl_service.serviceID=cl_service_category.serviceID   
	  WHERE  cl_service_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
	  ORDER  BY cl_service.serviceTitle
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>

    <cfif qServices.recordcount GT 0>
      <cfquery name="qAssociatedCourses" datasource="#this.datasource#">
        SELECT cl_service.serviceID,
               COUNT(cl_service_course_associated.courseID) AS numAssociatedCourses
        FROM   cl_service LEFT JOIN cl_service_course_associated ON
               cl_service.serviceID=cl_service_course_associated.serviceID
        WHERE  cl_service.serviceID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#valueList(qServices.serviceID)#" list="yes">)
        GROUP BY cl_service.serviceID  		
      </cfquery>
      
      <cfquery name="qReviews" datasource="#this.datasource#">
        SELECT cl_service.serviceID,
               COUNT(cl_servicereview.serviceReviewID) AS numReviews
        FROM   cl_service LEFT JOIN cl_servicereview ON
               cl_service.serviceID=cl_servicereview.serviceID
        WHERE  cl_service.serviceID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qServices.serviceID)#" list="yes">)
        GROUP BY cl_service.serviceID	  		
      </cfquery>
      
      <cfquery name="qServiceParticipiants" datasource="#this.datasource#">
        SELECT cl_service.serviceID, COUNT(cl_student_service.studentServiceID) AS numParticipants
        FROM cl_service LEFT JOIN cl_student_service ON
      	     cl_service.serviceID=cl_student_service.serviceID
        WHERE cl_service.serviceID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#valueList(qServices.serviceID)#" list="yes">)
        GROUP BY cl_service.serviceID
        ORDER BY cl_service.serviceID
      </cfquery>
        
      <cfquery name="qServices" dbtype="query">
        SELECT *
        FROM qServices, qAssociatedCourses
        WHERE qServices.serviceID=qAssociatedCourses.serviceID
      </cfquery>
      
      <cfquery name="qServices" dbtype="query">
        SELECT *
        FROM qServices, qReviews
        WHERE qServices.serviceID=qReviews.serviceID
      </cfquery>
      
      <cfquery name="qServices" dbtype="query">
        SELECT *
        FROM qServices, qServiceParticipiants
        WHERE qServices.serviceID=qServiceParticipiants.serviceID
        ORDER BY serviceTitle
      </cfquery>
            
      <cfset resultStruct.numDisplayedItems=qServices.recordcount>	
      <cfset resultStruct.services = qServices>
	<cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.services = QueryNew("serviceID")>
    </cfif>
    
	<cfreturn resultStruct>
  </cffunction>
    
  <cffunction name="getService" access="public" output="no" returntype="query">
    <cfargument name="serviceID" type="numeric" required="yes">
    
    <cfquery name="qService" datasource="#this.datasource#">
      SELECT *
      FROM cl_service
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
    </cfquery>
    
    <cfreturn qService>
  </cffunction>
  
  <cffunction name="getCategoryIDListByServiceID" access="public" output="no" returntype="string">
    <cfargument name="serviceID" type="numeric" required="yes">
    
    <cfquery name="qCategoryIDs" datasource="#this.datasource#">
      SELECT categoryID
      FROM cl_service_category
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
    </cfquery>
    <cfif qCategoryIDs.recordcount GT 0>
      <cfreturn ValueList(qCategoryIDs.categoryID)>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="addService" access="public" output="no" returntype="boolean">
    <cfargument name="categoryIDList" type="string" default="">
    <cfargument name="GLNumber" type="string" default="">
    <cfargument name="serviceTitle" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="feeRegular" type="numeric" required="yes">
    <cfargument name="feeEmployee" type="numeric" required="yes">
    <cfargument name="feeDoctor" type="numeric" required="yes">
    <cfargument name="feeFitnessClub" type="numeric" required="yes">
	<cfargument name="locationID" type="numeric" default="0">
    <cfargument name="contactName" type="string" default="">
    <cfargument name="contactEmail" type="string" default="">
    <cfargument name="contactPhone" type="string" default="">
    <cfargument name="shortDescription" type="string" default="">
    <cfargument name="longDescription" type="string" default="">
    <cfargument name="specialInstructions" type="string" default="">
        
    <cfif Not Compare(Arguments.categoryIDList, "")><cfreturn false></cfif>
    		
    <cfquery datasource="#this.datasource#">
	  INSERT INTO cl_service
        (GLNumber, serviceTitle, published, locationID,
         feeRegular, feeEmployee, feeDoctor, feeFitnessClub,
         contactName, contactEmail, contactPhone,
         shortDescription, longDescription, specialInstructions,         
         dateCreated, dateLastModified)
	  VALUES
	    (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.GLNumber#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.serviceTitle#">,
         <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,         
         <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.locationID#">,
         
         <cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeRegular#">,
         <cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeEmployee#">,
         <cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeDoctor#">,
         <cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeFitnessClub#">,

         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactName#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactEmail#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactPhone#">,
         
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.shortDescription#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.longDescription#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.specialInstructions#">,
         
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)		
	</cfquery>

	<cfquery name="qServiceID" datasource="#this.datasource#">
	  SELECT MAX(serviceID) AS newServiceID
	  FROM cl_service
    </cfquery>
	<cfset Variables.serviceID=qServiceID.newServiceID>	
    
    <cfloop index="categoryID" list="#Arguments.categoryIDList#">      
      <cfquery datasource="#this.datasource#">
		INSERT INTO cl_service_category (serviceID, categoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.serviceID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>

  <cffunction name="editService" access="public" output="no" returntype="boolean">
    <cfargument name="serviceID" type="numeric" required="yes">
    <cfargument name="GLNumber" type="string" default="">
    <cfargument name="categoryIDList" type="string" default="">
    <cfargument name="serviceTitle" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="feeRegular" type="numeric" required="yes">
    <cfargument name="feeEmployee" type="numeric" required="yes">
    <cfargument name="feeDoctor" type="numeric" required="yes">
    <cfargument name="feeFitnessClub" type="numeric" required="yes">
	<cfargument name="locationID" type="numeric" default="0">
    <cfargument name="contactName" type="string" default="">
    <cfargument name="contactEmail" type="string" default="">
    <cfargument name="contactPhone" type="string" default="">
    <cfargument name="shortDescription" type="string" default="">
    <cfargument name="longDescription" type="string" default="">
    <cfargument name="specialInstructions" type="string" default="">
       
    <cfif Not Compare(Arguments.categoryIDList, "")><cfreturn false></cfif>

    <cfquery datasource="#this.datasource#">
	  UPDATE cl_service
      SET GLNumber=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.GLNumber#">,
      	  serviceTitle=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.serviceTitle#">,
          published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,          
          locationID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.locationID#">,
          
          feeRegular=<cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeRegular#">,
          feeEmployee=<cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeEmployee#">,
          feeDoctor=<cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeDoctor#">,
          feeFitnessClub=<cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.feeFitnessClub#">,

          contactName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactName#">,
          contactEmail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactEmail#">,
          contactPhone=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.contactPhone#">,
         
          shortDescription=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.shortDescription#">,
          longDescription=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.longDescription#">,
          specialInstructions=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.specialInstructions#">,        
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
	</cfquery>    
        
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_service_category
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
    </cfquery>
    
    <cfloop index="categoryID" list="#Arguments.categoryIDList#">    
      <cfquery datasource="#this.datasource#">
		INSERT INTO cl_service_category (serviceID, categoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryID#">)
	  </cfquery>
    </cfloop>

    <cfreturn true>
  </cffunction>
  
  <cffunction name="deleteService" access="public" output="no" returntype="boolean">
    <cfargument name="serviceID" required="yes" type="numeric">
    
    <!--- Need to verify if this service if registered by a student; return false if yes --->
    <cfquery name="qStudents" datasource="#this.datasource#">
      SELECT studentID
      FROM cl_student_service
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
    </cfquery>
    <cfif qStudents.recordcount GT 0>
      <cfreturn false>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_service_category
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_service
	  WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
    </cfquery>
        
    <cfreturn true>
  </cffunction>
  
  <cffunction name="getServiceAssociatedCourses" access="public" output="no" returntype="query">
    <cfargument name="serviceID" type="numeric" required="yes">
    
    <cfquery name="qCourses" datasource="#this.datasource#">
      SELECT cl_course.courseID, cl_course.courseTitle
      FROM cl_service_course_associated INNER JOIN cl_course ON
      	   cl_service_course_associated.courseID=cl_course.courseID
      WHERE cl_service_course_associated.serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
    </cfquery>
  
    <cfreturn qCourses>  
  </cffunction>
  
  <cffunction name="addServiceAssociatedCourses" access="public" output="no">
    <cfargument name="serviceID" type="numeric" required="yes">
    <cfargument name="courseIDList" type="string" default="">
    
    <cfif Compare(Arguments.courseIDList,"")>
      <cfset associatedCourses=getServiceAssociatedCourses(Arguments.serviceID)>
      <cfif associatedCourses.recordcount GT 0>
        <cfset selectedCourseIDList=ValueList(associatedCourses.courseID)>
      <cfelse>
        <cfset selectedCourseIDList="">
      </cfif>
  	  
      <cfloop index="newCourseID" list="#Arguments.courseIDList#">
  		<cfif ListFind(selectedCourseIDList, newCourseID) Is 0>
          <cfquery datasource="#this.datasource#">
            INSERT INTO cl_service_course_associated (serviceID, courseID)
            VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">,
            		<cfqueryparam cfsqltype="cf_sql_integer" value="#newCourseID#">)
          </cfquery>
        </cfif>
      </cfloop>
    </cfif>  
  </cffunction>
  
  <cffunction name="deleteServiceAssociatedCourses" access="public" output="no">
    <cfargument name="serviceID" type="numeric" required="yes">
    <cfargument name="courseIDList" type="string" default="">
    
    <cfif Compare(Arguments.courseIDList,"")>
      <cfquery datasource="#this.datasource#">
        DELETE FROM cl_service_course_associated
        WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#"> AND
      		  courseID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.courseIDList#" list="yes">)
      </cfquery>
    </cfif>
  </cffunction>  












  
  <cffunction name="getClassesByCourseID" access="public" output="no" returntype="struct">
    <cfargument name="courseID" type="numeric" required="yes">
    <cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.classInstances=QueryNew("classID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
    
    <cfquery name="qAllClasses" datasource="#this.datasource#">
      SELECT COUNT(classID) AS numClasses
      FROM cl_class
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
    </cfquery>
    
    <cfset resultStruct.numAllItems = qAllClasses.numClasses>    
    <cfif resultStruct.numAllItems Is 0><cfreturn resultStruct></cfif>
    
    <cfquery name="qClasses" datasource="#this.datasource#">
      SELECT cl_class.classID, view_cl_classinstance_summary.firstStartDateTime
      FROM cl_class INNER JOIN view_cl_classinstance_summary ON cl_class.classID=view_cl_classinstance_summary.classID
      WHERE cl_class.courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
      ORDER BY firstStartDateTime ASC
      LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
    </cfquery>
    
    <cfset resultStruct.numDisplayedItems = qClasses.recordcount>
    <cfif resultStruct.numDisplayedItems Is 0><cfreturn resultStruct></cfif>
    
    <cfset classIDList=ValueList(qClasses.classID)>
    
    <cfquery name="qClassParticipiants" datasource="#this.datasource#">
      SELECT cl_class.classID, cl_class.regLimit, cl_class.regMaximum, COUNT(cl_student_class.studentClassID) AS numParticipants, SUM(cl_student_class.fee) AS moneyCollected
      FROM cl_class LEFT JOIN cl_student_class ON
      	   cl_class.classID=cl_student_class.classID
      WHERE cl_class.classID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#classIDList#" list="yes">)
      GROUP BY cl_class.classID
      ORDER BY cl_class.classID
    </cfquery>

    
    <cfquery name="qClassInstances" datasource="#this.datasource#">
      SELECT cl_class.classID, cl_class.published, cl_class.publishDate, cl_class.unpublishDate,
      		 cl_classinstance.classInstanceID, cl_classinstance.startDateTime, cl_classinstance.endDateTime, cl_classinstance.dayOfWeekAsNumber
      FROM cl_class INNER JOIN cl_classinstance ON cl_class.classID=cl_classinstance.classID
      WHERE cl_class.classID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#classIDList#" list="yes">)
      ORDER BY cl_classinstance.classInstanceID
    </cfquery>
    

    <cfquery name="qClassAttendances" datasource="#this.datasource#">
      SELECT cl_classinstance.classInstanceID, COUNT(DISTINCT cl_classattendance.studentClassID) AS numAttendedStudents
      FROM cl_classinstance LEFT JOIN cl_classattendance ON
      	   cl_classinstance.classInstanceID=cl_classattendance.classInstanceID
      WHERE cl_classinstance.classID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#classIDList#" list="yes">)
      GROUP BY cl_classinstance.classInstanceID
      ORDER BY cl_classinstance.classInstanceID
    </cfquery>
    
    <cfquery name="qClasses" dbtype="query">
      SELECT *
      FROM qClasses, qClassParticipiants
      WHERE qClasses.classID=qClassParticipiants.classID
    </cfquery>
    
    <cfquery name="qClassInstances" dbtype="query">
      SELECT *
      FROM qClasses, qClassInstances
      WHERE qClasses.classID=qClassInstances.classID
    </cfquery>
    
    <cfquery name="qClassInstances" dbtype="query">
      SELECT *
      FROM qClassInstances, qClassAttendances
      WHERE qClassInstances.classInstanceID=qClassAttendances.classInstanceID
      ORDER BY qClassInstances.firstStartDateTime DESC, qClassInstances.startDateTime DESC
    </cfquery>
    
    <cfset resultStruct.classInstances=qClassInstances>
    <cfreturn resultStruct>
  </cffunction>








  
  <cffunction name="getAllClassStartDateTimeCourseID" access="public" output="no" returntype="query">
    <cfargument name="courseID" type="numeric" required="yes">
    
    <cfquery name="qClassDateTime" datasource="#this.datasource#">
      SELECT cl_class.classID, view_cl_classinstance_summary.firstStartDateTime
      FROM cl_class INNER JOIN view_cl_classinstance_summary ON cl_class.classID=view_cl_classinstance_summary.classID
      WHERE cl_class.courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">
      ORDER BY view_cl_classinstance_summary.firstStartDateTime ASC
    </cfquery>
  
    <cfreturn qClassDateTime>
  </cffunction>
  
  <cffunction name="getClassByClassID" access="public" output="no" returntype="query">
    <cfargument name="classID" type="numeric" required="yes">
  
    <cfquery name="qClass" datasource="#this.datasource#">
      SELECT *
      FROM cl_class
      WHERE classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
    </cfquery>
  
    <cfreturn qClass>
  </cffunction>  
  
  
  <cffunction name="getClassInstancesByClassID" access="public" output="no" returntype="query">
    <cfargument name="classID" type="numeric" required="yes">
  
    <cfquery name="qClassInstance" datasource="#this.datasource#">
      SELECT *
      FROM cl_classinstance
      WHERE classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
      ORDER BY startDateTime
    </cfquery>
  
    <cfreturn qClassInstance>
  </cffunction>
  
  <cffunction name="getClassInstanceByClassInstanceID" access="public" output="no" returntype="query">
    <cfargument name="classInstanceID" type="numeric" required="yes">
  
    <cfquery name="qClassInstance" datasource="#this.datasource#">
      SELECT *, locationName     
      FROM cl_classinstance LEFT JOIN cl_location ON cl_classinstance.locationID=cl_location.locationID
      WHERE classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">
    </cfquery>
  
    <cfreturn qClassInstance>
  </cffunction>

  <cffunction name="addClass" access="public" output="no">
	<cfargument name="courseID" type="numeric" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="publishDate" type="string" required="yes">
    <cfargument name="publishDate_hh" type="string" required="yes">
    <cfargument name="publishDate_mm" type="string" required="yes">
    <cfargument name="publishDate_ampm" type="string" required="yes">
    <cfargument name="unpublishDate" type="string" required="yes">
    <cfargument name="unpublishDate_hh" type="string" required="yes">
    <cfargument name="unpublishDate_mm" type="string" required="yes">
    <cfargument name="unpublishDate_ampm" type="string" required="yes">
    <cfargument name="notes" type="string" default="">
    <cfargument name="startDate" type="string" required="yes">
    <cfargument name="startDate_hh" type="string" required="yes">
    <cfargument name="startDate_mm" type="string" required="yes">
    <cfargument name="startDate_ampm" type="string" required="yes">   
    <cfargument name="endDate_hh" type="string" required="yes">
    <cfargument name="endDate_mm" type="string" required="yes">
    <cfargument name="endDate_ampm" type="string" required="yes">
    <cfargument name="locationID" type="numeric" required="yes">
    
    <cfset Util=CreateObject("component","Utility").init()>
    
    <cfif Arguments.publishDate IS "">
	  <cfset Variables.publishDate=now()>
	<cfelse>
   	  <cfset Variables.publishDate=Util.convertStringToDateTimeObject(Arguments.publishDate, Arguments.publishDate_hh, Arguments.publishDate_mm, 0, Arguments.publishDate_ampm)>
	</cfif>
	
    <cfif Arguments.unpublishDate IS "">
	  <cfset Variables.unpublishDate=CreateDateTime(3000,1,1,0,0,0)>
	<cfelse>
   	  <cfset Variables.unpublishDate=Util.convertStringToDateTimeObject(Arguments.unpublishDate, Arguments.unpublishDate_hh, Arguments.unpublishDate_mm, 0, Arguments.unpublishDate_ampm)>
	</cfif>
    
    <cfset Variables.startDateTime=Util.convertStringToDateTimeObject(Arguments.startDate, Arguments.startDate_hh, Arguments.startDate_mm, 0, Arguments.startDate_ampm)>
    <cfset Variables.endDateTime=Util.convertStringToDateTimeObject(Arguments.startDate, Arguments.endDate_hh, Arguments.endDate_mm, 0, Arguments.endDate_ampm)>
    
    <cfset Variables.dayOfWeekAsNumber=dayOfWeek(Variables.startDateTime)>
    <cfset Variables.classLengthInMinutes=DateDiff("n", Variables.startDateTime, Variables.endDateTime)>
    
    <cfif Not IsNumeric(Arguments.regLimit)>
      <cfset Arguments.regLimit=10000>
    </cfif>
    <cfif Not IsNumeric(Arguments.regMaximum)>
      <cfset Arguments.regMaximum=10000>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      INSERT INTO cl_class
      (courseID, published, publishDate, unpublishDate, regLimit, regMaximum, notes, dateCreated, dateLastModified)
      VALUES
      (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#">,
       <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.publishDate#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.unpublishDate#">,       
       <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.regLimit#">,
       <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.regMaximum#">,       
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.notes#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
    </cfquery>
    
    <cfquery name="qClassID" datasource="#this.datasource#">
      SELECT MAX(classID) AS classID FROM cl_class
    </cfquery>
    <cfset newClassID=qClassID.classID>
    
    
     <cfquery datasource="#this.datasource#">
      INSERT INTO cl_classinstance
      (classID, locationID, startDateTime, endDateTime, classLengthInMinutes, dayOfWeekAsNumber, dateCreated, dateLastModified)
      VALUES
      (<cfqueryparam cfsqltype="cf_sql_integer" value="#newClassID#">,
       <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.locationID#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDateTime#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDateTime#">,
       <cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.classLengthInMinutes#">,
       <cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.dayOfWeekAsNumber#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)      
     </cfquery>
  </cffunction>

  <cffunction name="editClass" access="public" output="no" returntype="boolean">
    <cfargument name="courseID" type="numeric" required="yes">
    <cfargument name="classID" type="numeric" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="publishDate" type="string" required="yes">
    <cfargument name="publishDate_hh" type="string" required="yes">
    <cfargument name="publishDate_mm" type="string" required="yes">
    <cfargument name="publishDate_ampm" type="string" required="yes">
    <cfargument name="unpublishDate" type="string" required="yes">
    <cfargument name="unpublishDate_hh" type="string" required="yes">
    <cfargument name="unpublishDate_mm" type="string" required="yes">
    <cfargument name="unpublishDate_ampm" type="string" required="yes">
    <cfargument name="notes" type="string" default="">
    <cfargument name="startDate" type="string" required="yes">
    <cfargument name="startDate_hh" type="string" required="yes">
    <cfargument name="startDate_mm" type="string" required="yes">
    <cfargument name="startDate_ampm" type="string" required="yes">   
    <cfargument name="endDate_hh" type="string" required="yes">
    <cfargument name="endDate_mm" type="string" required="yes">
    <cfargument name="endDate_ampm" type="string" required="yes">
    <cfargument name="locationID" type="numeric" required="yes">
    
    <cfset Util=CreateObject("component","Utility").init()>
    
    <cfif Arguments.publishDate IS "">
	  <cfset Variables.publishDate=now()>
	<cfelse>
   	  <cfset Variables.publishDate=Util.convertStringToDateTimeObject(Arguments.publishDate, Arguments.publishDate_hh, Arguments.publishDate_mm, 0, Arguments.publishDate_ampm)>
	</cfif>
	
    <cfif Arguments.unpublishDate IS "">
	  <cfset Variables.unpublishDate=CreateDateTime(3000,1,1,0,0,0)>
	<cfelse>
   	  <cfset Variables.unpublishDate=Util.convertStringToDateTimeObject(Arguments.unpublishDate, Arguments.unpublishDate_hh, Arguments.unpublishDate_mm, 0, Arguments.unpublishDate_ampm)>
	</cfif>
    
    <cfif Not IsNumeric(Arguments.regLimit)>
      <cfset Arguments.regLimit=10000>
    </cfif>
    <cfif Not IsNumeric(Arguments.regMaximum)>
      <cfset Arguments.regMaximum=10000>
    </cfif>
    
    <cfset classInstanceInfo=getClassInstancesByClassID(Arguments.classID)>
    
    <!--- BEGINI: check to see if there is at least one class instance --->
    <cfset hasInstance=false>
    <cfloop query="classInstanceInfo">
      <cfif Not IsDefined("Arguments.deleteClassInstance_#classInstanceID#")>
        <cfset hasInstance=true>
      </cfif>
    </cfloop>
    <cfif Compare(Arguments.startDate,"")><cfset hasInstance=true></cfif>
    <cfif Not hasInstance><cfreturn false></cfif>
    <!--- END: check to see if there is at least one class instance    --->
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_class
      SET published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
      	  publishDate=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.publishDate#">,
          unpublishDate=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.unpublishDate#">,
          regLimit=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.regLimit#">,
          regMaximum=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.regMaximum#">, 
          notes=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.notes#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
    </cfquery>
    
    <cfloop query="classInstanceInfo">
      <cfif IsDefined("Arguments.deleteClassInstance_#classInstanceID#")>
        <!--- delete this class instance --->
        <cfquery datasource="#this.datasource#">
          DELETE FROM cl_classattendance
          WHERE classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#classInstanceID#">
        </cfquery>
    	<cfquery datasource="#this.datasource#">
          DELETE FROM cl_classinstance
          WHERE classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#classInstanceID#">
        </cfquery>
      <cfelse>
        <!--- update this class instance --->
        <cfset Variables.startDateTime=
			   Util.convertStringToDateTimeObject(
			   		Arguments["startDate_#classInstanceID#"], Arguments["startDate_hh_#classInstanceID#"],
					Arguments["startDate_mm_#classInstanceID#"], 0, Arguments["startDate_ampm_#classInstanceID#"])>
    	<cfset Variables.endDateTime=
			   Util.convertStringToDateTimeObject(
			   		Arguments["startDate_#classInstanceID#"], Arguments["endDate_hh_#classInstanceID#"],
					Arguments["endDate_mm_#classInstanceID#"], 0, Arguments["endDate_ampm_#classInstanceID#"])>
    
    	<cfset Variables.dayOfWeekAsNumber=dayOfWeek(Variables.startDateTime)>
    	<cfset Variables.classLengthInMinutes=DateDiff("n", Variables.startDateTime, Variables.endDateTime)>
        
        <cfquery datasource="#this.datasource#">
          UPDATE cl_classinstance
          SET locationID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments["locationID_#classInstanceID#"]#">,
          	  startDateTime=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDateTime#">,
              endDateTime=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDateTime#">,
              classLengthInMinutes=<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.classLengthInMinutes#">,
              dayOfWeekAsNumber=<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.dayOfWeekAsNumber#">,
              dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
          WHERE classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#classInstanceID#">
        </cfquery>
      </cfif>      
    </cfloop>
    
    <cfif Compare(Arguments.startDate,"")>
      <!--- new class instance --->
      <cfset Variables.startDateTime=Util.convertStringToDateTimeObject(Arguments.startDate, Arguments.startDate_hh, Arguments.startDate_mm, 0, Arguments.startDate_ampm)>
      <cfset Variables.endDateTime=Util.convertStringToDateTimeObject(Arguments.startDate, Arguments.endDate_hh, Arguments.endDate_mm, 0, Arguments.endDate_ampm)>
    
      <cfset Variables.dayOfWeekAsNumber=dayOfWeek(Variables.startDateTime)>
      <cfset Variables.classLengthInMinutes=DateDiff("n", Variables.startDateTime, Variables.endDateTime)>
      
      <cfquery datasource="#this.datasource#">
        INSERT INTO cl_classinstance
        (classID, locationID, startDateTime, endDateTime, classLengthInMinutes, dayOfWeekAsNumber, dateCreated, dateLastModified)
        VALUES
        (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.locationID#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDateTime#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDateTime#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.classLengthInMinutes#">,
         <cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.dayOfWeekAsNumber#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)      
       </cfquery>
	</cfif>
    
	<cfreturn true>
  </cffunction>

  <cffunction name="deleteClass" access="public" output="no" returntype="boolean">
    <cfargument name="classID" type="numeric" required="yes">

	<cfquery name="qStudents" datasource="#this.datasource#">
      SELECT studentID
      FROM cl_student_class
      WHERE classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
    </cfquery>
    
    <cfif qStudents.recordcount GT 0>
      <cfreturn false>
    </cfif>
    
    <cfquery name="qClassInstances" datasource="#this.datasource#">
      SELECT classInstanceID
      FROM cl_classinstance
      WHERE classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
	</cfquery>
    
    <cfif qClassInstances.recordcount GT 0>
      <cfquery datasource="#this.datasource#">
        DELETE FROM cl_classattendance
        WHERE classInstanceID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qClassInstances.classInstanceID)#" list="yes">)
	  </cfquery>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_classinstance
      WHERE classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
	</cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_class
      WHERE classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
	</cfquery>
    
    <cfreturn true>
  </cffunction>
  
  <cffunction name="getClassAttendeesByClassID" access="public" output="no" returntype="query">
    <cfargument name="classID" type="numeric" required="yes">
    
    <cfquery name="qAttendees" datasource="#this.datasource#">
      SELECT cl_student_class.studentClassID, cl_student_class.studentID, cl_student_class.feePerCouple, cl_student_class.payWithCash, cl_student_class.fee,
      		 cl_student_class.attendeeName, cl_student_class.attendeeName AS attendeeLastName, cl_student_class.guestNames, cl_student_class.daytimePhone, cl_student_class.cellPhone,
             cl_student_class.address1, cl_student_class.address2, cl_student_class.city, cl_student_class.state, cl_student_class.zip,
             cl_student.studentType, cl_student.email
      FROM cl_student_class INNER JOIN cl_student ON
      	   cl_student_class.studentID=cl_student.studentID
      WHERE cl_student_class.classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
      ORDER BY cl_student_class.attendeeName
    </cfquery>
    
    <cfset names=ArrayNew(1)>
    <cfloop query="qAttendees">
      <cfset lastName=Trim(qAttendees.attendeeLastName)>
      <cfset commaIdx=Find(",",lastName)>
      <cfif commaIdx GT 0>
        <cfset lastName=Trim(Left(lastName,commaIdx-1))>
      <cfelse>
        <cfset names=ListToArray(lastName," ")>
        <cfif ArrayLen(names) GT 1>
          <cfset lastName=Trim(names[ArrayLen(names)])>
        </cfif>
      </cfif>
      
      <cfset qAttendees.attendeeLastName=lastName>
    </cfloop>
    
    <cfquery name="qAttendees" dbtype="query">
      SELECT *
      FROM qAttendees
      ORDER BY attendeeLastName
    </cfquery>
    
    <cfreturn qAttendees>
  </cffunction>
    
  <cffunction name="getServiceAttendeesByServiceID" access="public" output="no" returntype="query">
    <cfargument name="serviceID" type="numeric" required="yes">
    
    <cfquery name="qAttendees" datasource="#this.datasource#">
      SELECT cl_student_service.studentServiceID, cl_student_service.studentID, cl_student_service.feePerCouple,
      		 cl_student_service.attendeeName, cl_student_service.payWithCash, cl_student_service.dateCreated
      FROM cl_student_service INNER JOIN cl_student ON
      	   cl_student_service.studentID=cl_student.studentID
      WHERE cl_student_service.serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
      ORDER BY cl_student_service.attendeeName
    </cfquery>
    
    <cfreturn qAttendees>
  </cffunction>
  
  <cffunction name="getAttendedStudentClassIDListByClassInstanceID" access="public" output="no" returntype="string">
    <cfargument name="classInstanceID" type="numeric" required="yes">
    
    <cfquery name="qStudents" datasource="#this.datasource#">
      SELECT studentClassID
      FROM cl_classattendance
      WHERE classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">
    </cfquery>
    
    <cfif qStudents.recordcount GT 0>
      <cfreturn ValueList(qStudents.studentClassID)>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="getAttendedStudentServiceIDListByServiceID" access="public" output="no" returntype="string">
    <cfargument name="serviceID" type="numeric" required="yes">
    
    <cfquery name="qStudents" datasource="#this.datasource#">
      SELECT studentServiceID
      FROM cl_serviceattendance
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
    </cfquery>
    
    <cfif qStudents.recordcount GT 0>
      <cfreturn ValueList(qStudents.studentServiceID)>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="updateAttendance" access="public" output="no">
    <cfargument name="classInstanceID" type="numeric" required="yes">
    <cfargument name="attendedStudentClassIDList" type="string" default="">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_classattendance
      WHERE classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">
    </cfquery>
    
    <cfloop index="studentClassID" list="#Arguments.attendedStudentClassIDList#">
      <cfquery datasource="#this.datasource#">
        INSERT INTO cl_classattendance(studentClassID, classInstanceID)
        VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#studentClassID#">,
        		<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">)
      </cfquery>
    </cfloop>
  </cffunction>
  
  <cffunction name="updateServiceAttendance" access="public" output="no">
    <cfargument name="serviceID" type="numeric" required="yes">
    <cfargument name="attendedStudentServiceIDList" type="string" default="">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_serviceattendance
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
    </cfquery>
    
    <cfloop index="studentServiceID" list="#Arguments.attendedStudentServiceIDList#">
      <cfquery datasource="#this.datasource#">
        INSERT INTO cl_serviceattendance(studentServiceID, serviceID)
        VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#studentServiceID#">,
        		<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">)
      </cfquery>
    </cfloop>
  </cffunction>
  
  <cffunction name="deleteClassParticipants" access="public" output="no">
    <cfargument name="classID" type="numeric" required="yes">
    <cfargument name="studentClassIDList" type="string" default="">
    
    <cfif Compare(Arguments.studentClassIDList,"")>
      <cfquery datasource="#this.datasource#">
        DELETE FROM cl_student_class
        WHERE classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#"> AND
        	  studentClassID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.studentClassIDList#" list="yes">)
      </cfquery>
      
      <cfquery datasource="#this.datasource#">
        DELETE FROM cl_classattendance
        WHERE  studentClassID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.studentClassIDList#" list="yes">)
      </cfquery>
    </cfif>  
  </cffunction>
  
  <cffunction name="deleteServiceParticipants" access="public" output="no">
    <cfargument name="serviceID" type="numeric" required="yes">
    <cfargument name="studentServiceIDList" type="string" default="">
    
    <cfif Compare(Arguments.studentServiceIDList,"")>
      <cfquery datasource="#this.datasource#">
        DELETE FROM cl_student_service
        WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#"> AND
        	  studentServiceID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.studentServiceIDList#" list="yes">)
      </cfquery>
    </cfif>  
  </cffunction>
  
  <cffunction name="changeClassPayWithCashStatus" access="public" output="no">
    <cfargument name="studentClassID" type="numeric" required="yes">
    <cfargument name="payWithCash" type="boolean" required="yes">
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_student_class
      SET <cfif Arguments.payWithCash>payWithCash=0<cfelse>payWithCash=1</cfif>
      WHERE studentClassID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentClassID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="changeServicePayWithCashStatus" access="public" output="no">
    <cfargument name="studentServiceID" type="numeric" required="yes">
    <cfargument name="payWithCash" type="boolean" required="yes">
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_student_service
      SET <cfif Arguments.payWithCash>payWithCash=0<cfelse>payWithCash=1</cfif>
      WHERE studentServiceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentServiceID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="getIntroductionText" access="public" output="no" returntype="string">
    <cfargument name="sectionCode" type="string" required="yes">
    <cfquery name="qText" datasource="#this.datasource#">
  	  SELECT introductionText
      FROM cl_introductory_copy
      WHERE sectionCode=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sectionCode#">
    </cfquery>
    
    <cfif qText.recordcount GT 0>
      <cfreturn qText.introductionText>
    <cfelse>
      <cfquery datasource="#this.datasource#">
        INSERT INTO cl_introductory_copy(sectionCode, introductionText, dateLastModified)
        VALUES
         (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sectionCode#">,
          '',
          <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
      </cfquery>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="updateIntroductionText" access="public" output="no">
    <cfargument name="sectionCode" type="string" required="yes">
    <cfargument name="introductionText" type="string" required="yes">
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_introductory_copy
      SET introductionText=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.introductionText#">,
      	  dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE sectionCode=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sectionCode#">
    </cfquery>
  </cffunction>
  
  <cffunction name="getCurriculum" access="public" output="no" returntype="query">
    <cfquery name="qCurriculum" datasource="#this.datasource#">
      SELECT curriculaID, curriculaTitle, published
      FROM cl_curricula
    </cfquery>
        
    <cfquery name="qNumCourses" datasource="#this.datasource#">
      SELECT cl_curricula.curriculaID, COUNT(cl_course_curricula.courseID) AS numCourses
      FROM cl_curricula LEFT JOIN cl_course_curricula ON
      	   cl_curricula.curriculaID=cl_course_curricula.curriculaID
      GROUP BY cl_curricula.curriculaID
    </cfquery>
    
    <cfquery name="qNumServices" datasource="#this.datasource#">
      SELECT cl_curricula.curriculaID, COUNT(cl_service_curricula.serviceID) AS numServices
      FROM cl_curricula LEFT JOIN cl_service_curricula ON
           cl_curricula.curriculaID=cl_service_curricula.curriculaID
      GROUP BY cl_curricula.curriculaID
    </cfquery>
    
    <cfquery name="qCurriculum" dbtype="query">
      SELECT *
      FROM qCurriculum, qNumCourses
      WHERE qCurriculum.curriculaID=qNumCourses.curriculaID
    </cfquery>
    
    <cfquery name="qCurriculum" dbtype="query">
      SELECT *
      FROM qCurriculum, qNumServices
      WHERE qCurriculum.curriculaID=qNumServices.curriculaID
      ORDER BY curriculaTitle
    </cfquery>
    
    <cfreturn qCurriculum>
  </cffunction>
  
  <cffunction name="getCurricula" access="public" output="no" returntype="query">
    <cfargument name="curriculaID" type="numeric" required="yes">
    <cfquery name="qCurricula" datasource="#this.datasource#">
      SELECT *
      FROM cl_curricula
      WHERE curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#">
    </cfquery>
    <cfreturn qCurricula>
  </cffunction>
  
  <cffunction name="addCurricula" access="public" output="no">
    <cfargument name="curriculaTitle" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="shortDescription" type="string" default="">
    <cfargument name="longDescription" type="string" default="">
    
    <cfquery datasource="#this.datasource#">
      INSERT INTO cl_curricula
      (published, curriculaTitle, shortDescription, longDescription, dateCreated, dateLastModified)
      VALUES
      (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.curriculaTitle#">,
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.shortDescription#">,
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.longDescription#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
    </cfquery>  
  </cffunction>
  
  <cffunction name="editCurricula" access="public" output="no">
    <cfargument name="curriculaID" type="numeric" required="yes">
    <cfargument name="curriculaTitle" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="shortDescription" type="string" default="">
    <cfargument name="longDescription" type="string" default="">
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_curricula
      SET
      	published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
        curriculaTitle=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.curriculaTitle#">,
        shortDescription=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.shortDescription#">,
        longDescription=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.longDescription#">,
        dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#">
    </cfquery>  
  </cffunction>
  
  <cffunction name="deleteCurricula" access="public" output="no" returntype="boolean">
    <cfargument name="curriculaID" type="numeric" required="yes">
    
    <cfquery name="qCourses" datasource="#this.datasource#">
      SELECT courseID
      FROM cl_course_curricula
      WHERE curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#">
    </cfquery>
    
    <cfquery name="qServices" datasource="#this.datasource#">
      SELECT serviceID
      FROM cl_service_curricula
      WHERE curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#">
    </cfquery>
    
    <cfif qCourses.recordcount GT 0 OR qServices.recordcount GT 0>
      <cfreturn false>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_curricula
      WHERE curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#">
    </cfquery>
    
    <cfreturn true>
  </cffunction>
  
  <cffunction name="getCurriculaCourses" access="public" output="no" returntype="query">
    <cfargument name="curriculaID" type="numeric" required="yes">
    
    <cfquery name="qCourses" datasource="#this.datasource#">
      SELECT cl_course.courseID, cl_course.courseTitle, cl_course.published
      FROM cl_course_curricula INNER JOIN cl_course ON cl_course_curricula.courseID=cl_course.courseID
      WHERE cl_course_curricula.curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#">
      ORDER BY cl_course_curricula.displaySeq
    </cfquery>
    <cfreturn qCourses>
  </cffunction>

  <cffunction name="addCurriculaCourses" access="public" output="no">
    <cfargument name="curriculaID" type="numeric" required="yes">
    <cfargument name="courseIDList" type="string" default="">
    
    <cfset selectedCourses=getCurriculaCourses(Arguments.curriculaID)>
    <cfif selectedCourses.recordcount GT 0>
      <cfset selectedCourseIDList=ValueList(selectedCourses.courseID)>
    <cfelse>
      <cfset selectedCourseIDList="">
    </cfif>
    
    <cfquery name="qMaxSeq" datasource="#this.datasource#">
      SELECT MAX(displaySeq) AS displaySeq
      FROM cl_course_curricula
      WHERE curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#">
    </cfquery>
    <cfif IsNumeric(qMaxSeq.displaySeq)>
      <cfset newDisplaySeq=qMaxSeq.displaySeq>
    <cfelse>
      <cfset newDisplaySeq=0>
    </cfif>
    
    <cfloop index="newCourseID" list="#Arguments.courseIDList#">
      <cfif ListFind(selectedCourseIDList, newCourseID) IS 0>
        <cfset newDisplaySeq = newDisplaySeq + 1>
        <cfquery datasource="#this.datasource#">
          INSERT INTO cl_course_curricula
          (courseID, curriculaID, displaySeq)
          VALUES
          (<cfqueryparam cfsqltype="cf_sql_integer" value="#newCourseID#">,
           <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#">,
           <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">)
        </cfquery>
      </cfif>
    </cfloop>
  </cffunction>
  
  <cffunction name="deleteCurriculaCourses" access="public" output="no">
    <cfargument name="curriculaID" type="numeric" required="yes">
    <cfargument name="courseIDList" type="string" default="">
    
    <cfif Compare(Arguments.courseIDList,"")>
      <cfquery datasource="#this.datasource#">
        DELETE FROM cl_course_curricula
        WHERE curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#"> AND
        	  courseID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.courseIDList#" list="yes">)
      </cfquery>
    </cfif>
  </cffunction>

  <cffunction name="moveCurriculaCourse" access="public" output="no">
    <cfargument name="curriculaID" required="yes" type="numeric">
    <cfargument name="courseID" required="yes" type="numeric">
    <cfargument name="direction" required="yes" type="string">
    
    <cfquery name="qTargetDisplaySeq" datasource="#this.datasource#">
      SELECT displaySeq
      FROM cl_course_curricula
      WHERE courseID=<cfqueryparam value="#Arguments.courseID#" cfsqltype="cf_sql_integer"> AND
      		curriculaID=<cfqueryparam value="#Arguments.curriculaID#" cfsqltype="cf_sql_integer">
    </cfquery>
    <cfif qTargetDisplaySeq.recordcount Is 0><cfreturn></cfif>
    
    <cfset targetCurriculaID=Arguments.curriculaID>
    <cfset targetDisplaySeq=qTargetDisplaySeq.displaySeq>
    
    <cfif Arguments.direction IS "up">
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT courseID, displaySeq
        FROM cl_course_curricula
        WHERE curriculaID=<cfqueryparam value="#targetCurriculaID#" cfsqltype="cf_sql_integer"> AND
        	  displaySeq < <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq DESC
      </cfquery>
    <cfelse>
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT courseID, displaySeq
        FROM cl_course_curricula
        WHERE curriculaID=<cfqueryparam value="#targetCurriculaID#" cfsqltype="cf_sql_integer"> AND
        	  displaySeq > <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq ASC
      </cfquery>
    </cfif>
    
    <cfif qNextDisplaySeq.recordcount Is 0><cfreturn></cfif>
    
    <cfset nextDisplaySeq=qNextDisplaySeq.displaySeq>
        
    <cfquery datasource="#this.datasource#">
      UPDATE cl_course_curricula
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#nextDisplaySeq#">
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#"> AND
      		curriculaID=<cfqueryparam value="#targetCurriculaID#" cfsqltype="cf_sql_integer">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_course_curricula
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
      WHERE courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qNextDisplaySeq.courseID#"> AND
      		curriculaID=<cfqueryparam value="#targetCurriculaID#" cfsqltype="cf_sql_integer">
    </cfquery>
  </cffunction>

  <cffunction name="getCurriculaServices" access="public" output="no" returntype="query">
    <cfargument name="curriculaID" type="numeric" required="yes">
    
    <cfquery name="qServices" datasource="#this.datasource#">
      SELECT cl_service.serviceID, cl_service.serviceTitle, cl_service.published
      FROM cl_service_curricula INNER JOIN cl_service ON cl_service_curricula.serviceID=cl_service.serviceID
      WHERE cl_service_curricula.curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#">
      ORDER BY cl_service_curricula.displaySeq
    </cfquery>
    <cfreturn qServices>
  </cffunction>

  <cffunction name="addCurriculaServices" access="public" output="no">
    <cfargument name="curriculaID" type="numeric" required="yes">
    <cfargument name="serviceIDList" type="string" default="">
    
    <cfset selectedServices=getCurriculaServices(Arguments.curriculaID)>
    <cfif selectedServices.recordcount GT 0>
      <cfset selectedServiceIDList=ValueList(selectedServices.serviceID)>
    <cfelse>
      <cfset selectedServiceIDList="">
    </cfif>
    
    <cfquery name="qMaxSeq" datasource="#this.datasource#">
      SELECT MAX(displaySeq) AS displaySeq
      FROM cl_service_curricula
      WHERE curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#">
    </cfquery>
    <cfif IsNumeric(qMaxSeq.displaySeq)>
      <cfset newDisplaySeq=qMaxSeq.displaySeq>
    <cfelse>
      <cfset newDisplaySeq=0>
    </cfif>
    
    <cfloop index="newServiceID" list="#Arguments.serviceIDList#">
      <cfif ListFind(selectedServiceIDList, newServiceID) IS 0>
        <cfset newDisplaySeq = newDisplaySeq + 1>
        <cfquery datasource="#this.datasource#">
          INSERT INTO cl_service_curricula
          (serviceID, curriculaID, displaySeq)
          VALUES
          (<cfqueryparam cfsqltype="cf_sql_integer" value="#newServiceID#">,
           <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#">,
           <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">)
        </cfquery>
      </cfif>
    </cfloop>
  </cffunction>
  
  <cffunction name="deleteCurriculaServices" access="public" output="no">
    <cfargument name="curriculaID" type="numeric" required="yes">
    <cfargument name="serviceIDList" type="string" default="">
    
    <cfif Compare(Arguments.serviceIDList,"")>
      <cfquery datasource="#this.datasource#">
        DELETE FROM cl_service_curricula
        WHERE curriculaID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.curriculaID#"> AND
        	  serviceID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.serviceIDList#" list="yes">)
      </cfquery>
    </cfif>
  </cffunction>

  <cffunction name="moveCurriculaService" access="public" output="no">
    <cfargument name="curriculaID" required="yes" type="numeric">
    <cfargument name="serviceID" required="yes" type="numeric">
    <cfargument name="direction" required="yes" type="string">
    
    <cfquery name="qTargetDisplaySeq" datasource="#this.datasource#">
      SELECT displaySeq
      FROM cl_service_curricula
      WHERE serviceID=<cfqueryparam value="#Arguments.serviceID#" cfsqltype="cf_sql_integer"> AND
      		curriculaID=<cfqueryparam value="#Arguments.curriculaID#" cfsqltype="cf_sql_integer">
    </cfquery>
    <cfif qTargetDisplaySeq.recordcount Is 0><cfreturn></cfif>
    
    <cfset targetCurriculaID=Arguments.curriculaID>
    <cfset targetDisplaySeq=qTargetDisplaySeq.displaySeq>
    
    <cfif Arguments.direction IS "up">
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT serviceID, displaySeq
        FROM cl_service_curricula
        WHERE curriculaID=<cfqueryparam value="#targetCurriculaID#" cfsqltype="cf_sql_integer"> AND
        	  displaySeq < <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq DESC
      </cfquery>
    <cfelse>
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT serviceID, displaySeq
        FROM cl_service_curricula
        WHERE curriculaID=<cfqueryparam value="#targetCurriculaID#" cfsqltype="cf_sql_integer"> AND
        	  displaySeq > <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq ASC
      </cfquery>
    </cfif>
    
    <cfif qNextDisplaySeq.recordcount Is 0><cfreturn></cfif>
    
    <cfset nextDisplaySeq=qNextDisplaySeq.displaySeq>
        
    <cfquery datasource="#this.datasource#">
      UPDATE cl_service_curricula
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#nextDisplaySeq#">
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#"> AND
      		curriculaID=<cfqueryparam value="#targetCurriculaID#" cfsqltype="cf_sql_integer">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_service_curricula
      SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qNextDisplaySeq.serviceID#"> AND
      		curriculaID=<cfqueryparam value="#targetCurriculaID#" cfsqltype="cf_sql_integer">
    </cfquery>
  </cffunction>
  
  
  <cffunction name="addFitnessClass" access="public" output="no" returntype="boolean">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="classType" type="string" required="yes">
    <cfargument name="classTitle" type="string" required="yes">
    <cfargument name="instructorName" type="string" required="yes">
    <cfargument name="location" type="string" required="yes">
    <cfargument name="thumbnailImage" type="string" default="">
    <cfargument name="fullsizeImage" type="string" required="yes">
    <cfargument name="imageCaption" type="string" required="yes">
    <cfargument name="description" type="string" required="yes"> 
    <cfargument name="specialInstructions" type="string" required="yes">
    <cfargument name="moreInfoURL" type="string" required="yes">
    
    <cfset found=false>
    <cfloop index="idx" from="1" to="7">
      <cfif IsDefined("Arguments.dayOfWeekAsNumber_#idx#")><cfset found=true></cfif>
    </cfloop>
    <cfif Not found><cfreturn false></cfif>   
    
    		
    <cfquery datasource="#this.datasource#">
	  INSERT INTO cl_fitnessclass
        (published, classType, classTitle, instructorName, 
         location, thumbnailImage, fullsizeImage, imageCaption,
         description, specialInstructions, moreInfoURL,
         dateCreated, dateLastModified)
	  VALUES
	    (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.classType#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.classTitle#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.instructorName#">,
         
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.location#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.thumbnailImage#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.fullsizeImage#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.imageCaption#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.description#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.specialInstructions#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.moreInfoURL#">,
         
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
		 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)		
	</cfquery>
    
    <cfquery name="qNewClassID" datasource="#this.datasource#">
      SELECT MAX(fitnessClassID) AS fitnessClassID
      FROM cl_fitnessclass
    </cfquery>
    <cfset newFitnessClassID=qNewClassID.fitnessClassID>  
    
    <cfloop index="idx" from="1" to="7">
      <cfif IsDefined("Arguments.dayOfWeekAsNumber_#idx#")>
        <cfset startTime_hh=Arguments["startTime_hh_#idx#"]>
        <cfset startTime_mm=Arguments["startTime_mm_#idx#"]>
        <cfset startTime_ampm=Arguments["startTime_ampm_#idx#"]>
        <cfif Not Compare(startTime_hh,"12") AND Not Compare(startTime_ampm,"am")>
          <cfset startTime_hh="0">
        <cfelseif Not Compare(startTime_ampm,"pm") And Compare(startTime_hh,"12")>
          <cfset startTime_hh = 12 + startTime_hh>
        </cfif>
        <cfset startTime=CreateTime(startTime_hh,startTime_mm,1)>
        
        <cfset endTime_hh=Arguments["endTime_hh_#idx#"]>
        <cfset endTime_mm=Arguments["endTime_mm_#idx#"]>
        <cfset endTime_ampm=Arguments["endTime_ampm_#idx#"]>
        <cfif Not Compare(endTime_hh,"12") AND Not Compare(endTime_ampm,"am")>
          <cfset endTime_hh="0">
        <cfelseif Not Compare(endTime_ampm,"pm") And Compare(endTime_hh,"12")>
          <cfset endTime_hh = 12 + endTime_hh>
        </cfif>
        <cfset endTime=CreateTime(endTime_hh,endTime_mm,1)>      
      
        <cfquery datasource="#this.datasource#">
          INSERT INTO cl_fitnessclass_time
          (fitnessClassID, dayOfWeekAsNumber, startTime, endTime)
          VALUES
          (<cfqueryparam cfsqltype="cf_sql_integer" value="#newFitnessClassID#">,
           <cfqueryparam cfsqltype="cf_sql_integer" value="#idx#">,
           <cfqueryparam cfsqltype="cf_sql_time" value="#startTime#">,
           <cfqueryparam cfsqltype="cf_sql_time" value="#endTime#">)
        </cfquery>
      </cfif>
    </cfloop>
    
    <cfreturn true>
  </cffunction>
  
  <cffunction name="editFitnessClass" access="public" output="no" returntype="boolean">
    <cfargument name="fitnessClassID" type="numeric" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="classType" type="string" required="yes">
    <cfargument name="classTitle" type="string" required="yes">
    <cfargument name="instructorName" type="string" required="yes">
    <cfargument name="location" type="string" required="yes">
    <cfargument name="thumbnailImage" type="string" default="">
    <cfargument name="fullsizeImage" type="string" required="yes">
    <cfargument name="imageCaption" type="string" required="yes">
    <cfargument name="description" type="string" required="yes"> 
    <cfargument name="specialInstructions" type="string" required="yes">
    <cfargument name="moreInfoURL" type="string" required="yes">
    
    <cfset found=false>
    <cfloop index="idx" from="1" to="7">
      <cfif IsDefined("Arguments.dayOfWeekAsNumber_#idx#")><cfset found=true></cfif>
    </cfloop>
    <cfif Not found><cfreturn false></cfif> 	
    		
    <cfquery datasource="#this.datasource#">
	  UPDATE cl_fitnessclass
      SET
      	 published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
         classType=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.classType#">,
         classTitle=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.classTitle#">,
         instructorName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.instructorName#">,
         
         location=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.location#">,
         thumbnailImage=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.thumbnailImage#">,
         fullsizeImage=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.fullsizeImage#">,
         imageCaption=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.imageCaption#">,
         description=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.description#">,
         specialInstructions=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.specialInstructions#">,
         moreInfoURL=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.moreInfoURL#">,
		 dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE fitnessClassID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClassID#">	
	</cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_fitnessclass_time
      WHERE fitnessClassID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClassID#">	
    </cfquery>
    
    <cfloop index="idx" from="1" to="7">
      <cfif IsDefined("Arguments.dayOfWeekAsNumber_#idx#")>
        <cfset startTime_hh=Arguments["startTime_hh_#idx#"]>
        <cfset startTime_mm=Arguments["startTime_mm_#idx#"]>
        <cfset startTime_ampm=Arguments["startTime_ampm_#idx#"]>
        <cfif Not Compare(startTime_hh,"12") AND Not Compare(startTime_ampm,"am")>
          <cfset startTime_hh="0">
        <cfelseif Not Compare(startTime_ampm,"pm") And Compare(startTime_hh,"12")>
          <cfset startTime_hh = 12 + startTime_hh>
        </cfif>
        <cfset startTime=CreateTime(startTime_hh,startTime_mm,1)>
        
        <cfset endTime_hh=Arguments["endTime_hh_#idx#"]>
        <cfset endTime_mm=Arguments["endTime_mm_#idx#"]>
        <cfset endTime_ampm=Arguments["endTime_ampm_#idx#"]>
        <cfif Not Compare(endTime_hh,"12") AND Not Compare(endTime_ampm,"am")>
          <cfset endTime_hh="0">
        <cfelseif Not Compare(endTime_ampm,"pm") And Compare(endTime_hh,"12")>
          <cfset endTime_hh = 12 + endTime_hh>
        </cfif>
        <cfset endTime=CreateTime(endTime_hh,endTime_mm,1)>      
      
        <cfquery datasource="#this.datasource#">
          INSERT INTO cl_fitnessclass_time
          (fitnessClassID, dayOfWeekAsNumber, startTime, endTime)
          VALUES
          (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClassID#">,
           <cfqueryparam cfsqltype="cf_sql_integer" value="#idx#">,
           <cfqueryparam cfsqltype="cf_sql_time" value="#startTime#">,
           <cfqueryparam cfsqltype="cf_sql_time" value="#endTime#">)
        </cfquery>
      </cfif>
    </cfloop>
    
    <cfreturn true>
  </cffunction>
  
  <cffunction name="deleteFitnessClass" access="public" output="no">
    <cfargument name="fitnessClassID" type="numeric" required="yes">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_fitnessclass
      WHERE fitnessClassID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClassID#">	
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_fitnessclass_time
      WHERE fitnessClassID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClassID#">	
    </cfquery>
  </cffunction>
  
  <cffunction name="getFitnessClasses" access="public" output="false" returntype="struct">
    <cfargument name="classType" type="string" required="yes">
	<cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.classes=QueryNew("fitnessClassID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qNumClasses" datasource="#this.datasource#">
	  SELECT COUNT(fitnessClassID) AS numItems
	  FROM	 cl_fitnessclass
      WHERE classType=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.classType#">
    </cfquery>
	<cfset resultStruct.numAllItems = qNumClasses.numItems>
	
	<cfquery name="qClasses" datasource="#this.datasource#">
	  SELECT fitnessClassID, classType, classTitle, published, instructorName, location 
	  FROM   cl_fitnessclass 
      WHERE classType=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.classType#">
	  ORDER  BY classTitle
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
    <cfif qClasses.recordcount GT 0>            
      <cfset resultStruct.numDisplayedItems=qClasses.recordcount>	
      <cfset resultStruct.classes = qClasses>
	<cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.classes = QueryNew("fitnessClassID")>
    </cfif>
    
	<cfreturn resultStruct>
  </cffunction>
  
  <cffunction name="getFitnessClass" access="public" output="no" returntype="query">
    <cfargument name="fitnessClassID" type="numeric" required="yes">
    
    <cfquery name="qClass" datasource="#this.datasource#">
      SELECT *
      FROM cl_fitnessclass
      WHERE fitnessClassID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClassID#">	
    </cfquery>
    
    <cfreturn qClass>
  </cffunction>
  
  <cffunction name="getFitnessClassSchedule" access="public" output="no" returntype="query">
    <cfargument name="fitnessClassID" type="numeric" required="yes">
    
    <cfquery name="qClassTime" datasource="#this.datasource#">
      SELECT *
      FROM cl_fitnessclass_time
      WHERE fitnessClassID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClassID#">
      ORDER BY dayOfWeekAsNumber
    </cfquery>
    
    <cfreturn qClassTime>
  </cffunction>
  
  <cffunction name="getAllEmailSettings" access="public" output="no" returntype="query">
    <cfquery name="qEmailSettings" datasource="#this.datasource#">
      SELECT *
      FROM cl_emailsettings
      ORDER BY displaySeq
    </cfquery>
    
    <cfreturn qEmailSettings>
  </cffunction>  
  
  <cffunction name="updateEmailSettings" access="public" output="no" returntype="void">
    <cfargument name="emailCode" type="string" required="yes">
    <cfargument name="emailSubject" type="string" required="yes">
    <cfargument name="replyAddress" type="string" required="yes">
    <cfargument name="replyName" type="string" default="">
    <cfargument name="recipientAddresses" type="string" default="">
    <cfargument name="emailBody" type="string" required="yes">
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_emailsettings
      SET emailSubject=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailSubject#">,
      	  replyAddress=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.replyAddress#">,
          replyName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.replyName#">,
          recipientAddresses=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.recipientAddresses#">,
          emailBody=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.emailBody#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE emailCode=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailCode#">
    </cfquery>
    
  </cffunction>

  <cffunction name="getFollowUpStatus" access="public" output="no" returntype="query">
    <cfargument name="classInstanceID" type="numeric" required="yes">
    
    <cfquery name="qStatus" datasource="#this.datasource#">
      SELECT feedbackRequestSent, missedClassFollowUpSent
      FROM cl_classinstance
      WHERE classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">
    </cfquery>
    <cfreturn qStatus>
  </cffunction>
  
  <cffunction name="sendFeedbackRequest" access="public" output="no" returntype="void">
    <cfargument name="classInstanceID" type="string" required="yes">
  
    <cfquery datasource="#this.datasource#">
      UPDATE cl_classinstance
      SET feedbackRequestSent=1
      WHERE classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">
    </cfquery>
    
    <cfquery name="qEmailSetting" datasource="#this.datasource#">
      SELECT *
      FROM cl_emailsettings
      WHERE emailCode='feedback_request'
    </cfquery>
    
    <cfif Compare(qEmailSetting.replyName,"")>
      <cfset fromAddress="#qEmailSetting.replyName#<#qEmailSetting.replyAddress#>">
    <cfelse>
      <cfset fromAddress="#qEmailSetting.replyAddress#">
    </cfif>
    
    <cfquery name="qClassInfo" datasource="#this.datasource#">
      SELECT cl_course.courseTitle, cl_class.classID
      FROM (cl_classinstance INNER JOIN cl_class ON cl_classinstance.classID=cl_class.classID) INNER JOIN cl_course ON
      		cl_class.courseID=cl_course.courseID
      WHERE cl_classinstance.classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">
    </cfquery>
    
    <cfif qClassInfo.recordcount GT 0>    
      <cfquery name="qStudents" datasource="#this.datasource#">
        SELECT DISTINCT cl_student.studentID, cl_student.email
        FROM (cl_classattendance INNER JOIN cl_student_class ON cl_classattendance.studentClassID=cl_student_class.studentClassID) INNER JOIN cl_student ON
        	 cl_student_class.studentID=cl_student.studentID
        WHERE cl_classattendance.classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">
      </cfquery>
      
      <cfset Variables.courseTitleText=qClassInfo.courseTitle>
      <cfset Variables.courseTitleHTML="<span style=""font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:12px;font-weight:bold;color:##eb44a7;line-height:130%;"">#qClassInfo.courseTitle#</span>">
      
      <cfloop query="qStudents">
        <cfset feedbackFormURLText="#this.siteURLRoot#/review/?id=#qClassInfo.classID#&stid=#studentID#">
        <cfset feedbackFormURLHTML='<a href="#this.siteURLRoot#/review/?id=#qClassInfo.classID#&stid=#studentID#">#this.siteURLRoot#/review/?id=#qClassInfo.classID#&stid=#studentID#</a>'>
        <cfset finalEmailBodyText=ReplaceNoCase(qEmailSetting.emailBody, "$class", "#Variables.courseTitleText#", "All")>
        <cfset finalEmailBodyText=ReplaceNoCase(finalEmailBodyText, "$link", "#feedbackFormURLText#", "All")>
        <cfset finalEmailBodyHTML=ReplaceNoCase(qEmailSetting.emailBody, "$class", "#Variables.courseTitleHTML#", "All")>
        <cfset finalEmailBodyHTML=ReplaceNoCase(finalEmailBodyHTML, "$link", "#feedbackFormURLHTML#", "All")>
        <cfif IsValid("email", email)>
          <cfmail from="#fromAddress#" to="#email#" subject="#qEmailSetting.emailSubject#" type="html">
            <cfmailpart type="text" charset="utf-8">
#finalEmailBodyText#
			</cfmailpart>
            <cfmailpart type="html" charset="utf-8">
              <a href="#this.siteURLRoot#"><img src="#this.siteURLRoot#/images/imgLogoEmailNot.gif" alt="Woman's - exceptional care, centered on you" width="96" height="84" border="0" /></a>
              <br /><br /> 
              <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:12px;color:##333132;line-height:130%;">
              #Replace(finalEmailBodyHTML,Chr(10),"<br />","All")#
              </div>
              <br />
              <hr size="1" noshade="noshade" />
              <a href="#this.siteURLRoot#" style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;font-size:19px;color:##627d79;line-height:110%;text-decoration:none;"><b>WWW.WOMANS.ORG</b></a>
            </cfmailpart>
          </cfmail>
        </cfif>
        </cfloop>
    </cfif>
  </cffunction>
  
  <cffunction name="sendMissedClassFollowUp" access="public" output="no" returntype="void">
    <cfargument name="classInstanceID" type="string" required="yes">
  
    <cfquery datasource="#this.datasource#">
      UPDATE cl_classinstance
      SET missedClassFollowUpSent=1
      WHERE classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">
    </cfquery>
    
    <cfquery name="qEmailSetting" datasource="#this.datasource#">
      SELECT *
      FROM cl_emailsettings
      WHERE emailCode='missed_class'
    </cfquery>
    
    <cfif Compare(qEmailSetting.replyName,"")>
      <cfset fromAddress="#qEmailSetting.replyName#<#qEmailSetting.replyAddress#>">
    <cfelse>
      <cfset fromAddress="#qEmailSetting.replyAddress#">
    </cfif>
    
    <cfquery name="qClassInfo" datasource="#this.datasource#">
      SELECT cl_course.courseTitle
      FROM (cl_classinstance INNER JOIN cl_class ON cl_classinstance.classID=cl_class.classID) INNER JOIN cl_course ON
      		cl_class.courseID=cl_course.courseID
      WHERE cl_classinstance.classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">
    </cfquery>
        
    <cfif qClassInfo.recordcount GT 0>
      <cfset Variables.courseTitleText=qClassInfo.courseTitle>
      <cfset Variables.courseTitleHTML="<span style=""font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:12px;font-weight:bold;color:##eb44a7;line-height:130%;"">#qClassInfo.courseTitle#</span>">    
      <cfset finalEmailBodyText=ReplaceNoCase(qEmailSetting.emailBody, "$class", "#Variables.courseTitleText#", "All")>
      <cfset finalEmailBodyHTML=ReplaceNoCase(qEmailSetting.emailBody, "$class", "#Variables.courseTitleHTML#", "All")> 
      
      <!--- students who registered for this class instance --->
      <cfquery name="qRegisteredStudents" datasource="#this.datasource#">
        SELECT DISTINCT cl_student.studentID, cl_student.email
        FROM (cl_student_class INNER JOIN cl_classinstance ON
        	  cl_student_class.classID=cl_classinstance.classID) INNER JOIN cl_student ON
              cl_student_class.studentID=cl_student.studentID
        WHERE cl_classinstance.classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">
      </cfquery>
      
      <!--- students who attended this class instance --->
      <cfquery name="qAttendedStudents" datasource="#this.datasource#">
        SELECT DISTINCT cl_student.studentID
        FROM (cl_classattendance INNER JOIN cl_student_class ON cl_classattendance.studentClassID=cl_student_class.studentClassID) INNER JOIN cl_student ON
        	 cl_student_class.studentID=cl_student.studentID             
        WHERE cl_classattendance.classInstanceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classInstanceID#">
      </cfquery>
      <cfif qAttendedStudents.recordcount GT 0>
        <cfset studentIDList_Attended=ValueList(qAttendedStudents.studentID)>
      <cfelse>
        <cfset studentIDList_Attended="">
      </cfif>
      
      <cfloop query="qRegisteredStudents">
        <cfif ListFind(studentIDList_Attended, studentID) Is 0>
          <cfif IsValid("email", email)>
		    <cfmail from="#fromAddress#" to="#email#" subject="#qEmailSetting.emailSubject#" type="html">
              <cfmailpart type="text" charset="utf-8">
#finalEmailBodyText#
			  </cfmailpart>
              <cfmailpart type="html" charset="utf-8">
              <a href="#this.siteURLRoot#"><img src="#this.siteURLRoot#/images/imgLogoEmailNot.gif" alt="Woman's - exceptional care, centered on you" width="96" height="84" border="0" /></a>
              <br /><br /> 
              <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:12px;color:##333132;line-height:130%;">
              #Replace(finalEmailBodyHTML,Chr(10),"<br />","All")#
              </div>
              <br />
              <hr size="1" noshade="noshade" />
              <a href="#this.siteURLRoot#" style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;font-size:19px;color:##627d79;line-height:110%;text-decoration:none;"><b>WWW.WOMANS.ORG</b></a>
              </cfmailpart>
		    </cfmail>
          </cfif>
        </cfif>
      </cfloop>
    </cfif>
  </cffunction>
  
  <cffunction name="getStudentsByType" access="public" output="no" returntype="struct">
    <cfargument name="studentType" type="string" required="yes">
    <cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.courses=QueryNew("studentID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qNumStudents" datasource="#this.datasource#">
	  SELECT COUNT(studentID) AS numItems
	  FROM	 cl_student
	  WHERE	 studentType=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.studentType#">
    </cfquery>
	<cfset resultStruct.numAllItems = qNumStudents.numItems>
	
	<cfquery name="qStudents" datasource="#this.datasource#">
	  SELECT studentID, email, password, studentType
	  FROM   cl_student
	  WHERE  studentType=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.studentType#">
	  ORDER  BY email
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>

    <cfset resultStruct.numDisplayedItems=qStudents.recordcount>    
    <cfset resultStruct.students = qStudents>

    
	<cfreturn resultStruct>   
  </cffunction>
















<cffunction name="getStudentsByKeyword" access="public" output="no" returntype="struct">
    <cfargument name="keyword" type="string" required="yes">
    <cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.courses=QueryNew("studentID")>	
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qNumStudents" datasource="#this.datasource#">
	  SELECT DISTINCT COUNT(cl_student.studentID) AS numItems
	  FROM   cl_student LEFT OUTER JOIN cl_ordersummary ON cl_student.studentID=cl_ordersummary.studentID
	  WHERE  cl_student.email LIKE <cfqueryparam cfsqltype="cf_sql_char" value="%#Arguments.keyword#%"> OR
      		 cl_student.firstName LIKE <cfqueryparam cfsqltype="cf_sql_char" value="%#Arguments.keyword#%"> OR
      		 cl_student.middleName LIKE <cfqueryparam cfsqltype="cf_sql_char" value="%#Arguments.keyword#%"> OR
             cl_student.lastName LIKE <cfqueryparam cfsqltype="cf_sql_char" value="%#Arguments.keyword#%">
	</cfquery>
	<cfset resultStruct.numAllItems = qNumStudents.numItems>
	
	<cfquery name="qStudents" datasource="#this.datasource#">
	  SELECT DISTINCT cl_student.studentID, cl_student.email, cl_student.password, cl_student.studentType,
      		 cl_ordersummary.contactFirstName, cl_ordersummary.contactLastName
	  FROM   cl_student LEFT OUTER JOIN cl_ordersummary ON cl_student.studentID=cl_ordersummary.studentID
	  WHERE  cl_student.email LIKE <cfqueryparam cfsqltype="cf_sql_char" value="%#Arguments.keyword#%"> OR
      		 cl_student.firstName LIKE <cfqueryparam cfsqltype="cf_sql_char" value="%#Arguments.keyword#%"> OR
      		 cl_student.middleName LIKE <cfqueryparam cfsqltype="cf_sql_char" value="%#Arguments.keyword#%"> OR
             cl_student.lastName LIKE <cfqueryparam cfsqltype="cf_sql_char" value="%#Arguments.keyword#%">
	  ORDER  BY cl_student.email, cl_ordersummary.contactFirstName, cl_ordersummary.contactLastName
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>

    <cfset resultStruct.numDisplayedItems=qStudents.recordcount>    
    <cfset resultStruct.students = qStudents>
    
	<cfreturn resultStruct>   
  </cffunction>



  
  <cffunction name="getStudent" access="public" output="no" returntype="query">
    <cfargument name="studentID" type="numeric" required="yes">
    
    <cfquery name="qStudent" datasource="#this.datasource#">
      SELECT *
      FROM cl_student
      WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">
    </cfquery>
    
    <cfreturn qStudent>
  </cffunction>














  
  <cffunction name="addStudent" access="public" output="no" returntype="boolean">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="password" type="string" required="yes">
    <cfargument name="studentType" type="string" default="R">
    <cfargument name="sendReminderEmail" type="boolean" default="0">
    
    <cfquery name="qEmail" datasource="#this.datasource#">
      SELECT studentID
      FROM cl_student
      WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">
    </cfquery>
    
    <cfif qEmail.recordcount GT 0>
      <cfreturn false>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      INSERT INTO cl_student (email, password, studentType, sendReminderEmail, dateCreated, dateLastModified)
      VALUES (
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.password#">,
        <cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.studentType#">,
        <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.sendReminderEmail#">,
        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
    </cfquery>
  
    <cfreturn true>
  </cffunction>
  
  <cffunction name="editStudent" access="public" output="no" returntype="boolean">
    <cfargument name="studentID" type="numeric" required="yes">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="password" type="string" required="yes">
    <cfargument name="studentType" type="string" default="R">
    <cfargument name="sendReminderEmail" type="boolean" default="0">
    
    <cfquery name="qEmail" datasource="#this.datasource#">
      SELECT studentID
      FROM cl_student
      WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#"> AND
			studentID <> <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">
    </cfquery>
    <cfif qEmail.recordcount GT 0>
      <cfreturn false>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_student
      SET email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">,
      	  password=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.password#">,
          studentType=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.studentType#">,
          sendReminderEmail=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.sendReminderEmail#">
      WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">
    </cfquery>
    
    <cfreturn true>
  </cffunction>
  
  <cffunction name="deleteStudent" access="public" output="no" returntype="boolean">
    <cfargument name="studentID" type="numeric" required="yes">
    
    <cfquery name="qStudent1" datasource="#this.datasource#">
      SELECT studentID
      FROM cl_student_class
      WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">
    </cfquery>
    <cfquery name="qStudent2" datasource="#this.datasource#">
      SELECT studentID
      FROM cl_student_service
      WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">
    </cfquery>
    <cfquery name="qStudent3" datasource="#this.datasource#">
      SELECT cl_student_class.studentID
      FROM cl_classattendance INNER JOIN cl_student_class ON cl_classattendance.studentClassID=cl_student_class.studentClassID
      WHERE cl_student_class.studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">
    </cfquery>
    <cfquery name="qStudent4" datasource="#this.datasource#">
      SELECT studentID
      FROM cl_student_service
      WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">
    </cfquery>
  
  	<cfif qStudent1.recordcount GT 0 OR qStudent2.recordcount GT 0 OR qStudent3.recordcount GT 0 OR qStudent4.recordcount GT 0>
      <cfreturn false>
    <cfelse>
      <cfquery datasource="#this.datasource#">
        DELETE FROM cl_student
        WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">
      </cfquery>
      <cfreturn true>
    </cfif>
  </cffunction>
  
  <cffunction name="getRegisteredClassesByStudentID" access="public" output="no" returntype="array">
    <cfargument name="studentID" type="numeric" required="yes">
    <cfset var classes=ArrayNew(1)>    
    
    <cfquery name="qStudentClasses" datasource="#this.datasource#">
      SELECT cl_student_class.studentClassID, cl_student_class.classID, cl_student_class.feePerCouple,
      		 cl_student_class.attendeeName, cl_student_class.guestNames,
             cl_student_class.daytimePhone, cl_student_class.cellPhone,
             cl_student_class.address1, cl_student_class.address2,
             cl_student_class.city, cl_student_class.state, cl_student_class.zip,
             cl_student_class.payWithCash, cl_student_class.paymentPlan, cl_student_class.fee,
             cl_student_class.amountPaid, cl_student_class.discountAmount,
             cl_student_class.discountCode, view_cl_classinstance_summary.firstStartDateTime
      FROM cl_student_class INNER JOIN view_cl_classinstance_summary ON cl_student_class.classID=view_cl_classinstance_summary.classID
      WHERE cl_student_class.studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">
      ORDER BY view_cl_classinstance_summary.firstStartDateTime DESC
    </cfquery>
    
    <cfset idx = 0>
    <cfloop query="qStudentClasses">
      <cfset idx = idx + 1>
      <cfset classes[idx]=StructNew()>
      <cfset classes[idx].studentClassID=studentClassID>
      <cfset classes[idx].classID=classID>
      <cfset classes[idx].feePerCouple=feePerCouple>
      <cfset classes[idx].attendeeName=attendeeName>
      <cfset classes[idx].guestNames=guestNames>
      <cfset classes[idx].daytimePhone=daytimePhone>
      <cfset classes[idx].cellPhone=cellPhone>
      <cfset classes[idx].address1=address1>
      <cfset classes[idx].address2=address2>
      <cfset classes[idx].city=city>
      <cfset classes[idx].state=state>
      <cfset classes[idx].zip=zip>
      <cfset classes[idx].payWithCash=payWithCash>
      <cfset classes[idx].paymentPlan=paymentPlan>
      <cfset classes[idx].fee=fee>
      <cfset classes[idx].amountPaid=amountPaid>
      <cfset classes[idx].discountAmount=discountAmount>
      <cfset classes[idx].discountCode=discountCode>
      <cfset classes[idx].classObj=CreateObject("component","ClassEntity").getClassEntityByID(classID)>      
    </cfloop> 
  
    <cfreturn classes>
  </cffunction>
  
  <cffunction name="getRegisteredServicesByStudentID" access="public" output="no" returntype="query">
    <cfargument name="studentID" type="numeric" required="yes">
    <cfset var services=ArrayNew(1)>
    
    <cfquery name="qStudentServices" datasource="#this.datasource#">
      SELECT cl_student_service.studentServiceID, cl_student_service.studentID, cl_student_service.feePerCouple,
      		 cl_student_service.attendeeName, cl_student_service.guestNames,
             cl_student_service.daytimePhone, cl_student_service.cellPhone,
             cl_student_service.address1, cl_student_service.address2,
             cl_student_service.city, cl_student_service.state, cl_student_service.zip,              
             cl_student_service.payWithCash, cl_student_service.paymentPlan,
             cl_student_service.fee, cl_student_service.amountPaid,
             cl_student_service.discountAmount, cl_student_service.discountCode, cl_service.serviceTitle
      FROM cl_student_service INNER JOIN cl_service ON cl_student_service.serviceID=cl_service.serviceID
      WHERE cl_student_service.studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">
      ORDER BY cl_student_service.studentServiceID
    </cfquery>
    
    <cfreturn qStudentServices>  
  </cffunction>  
  
  <cffunction name="addClassToSchedule" access="public" output="no">
    <cfargument name="studentID" type="numeric" required="yes">
    <cfargument name="classID" type="numeric" required="yes">
    
    <cfquery name="qFeePerCouple" datasource="#this.datasource#">
      SELECT cl_course.feePerCouple
      FROM cl_class INNER JOIN cl_course ON cl_class.courseID=cl_course.courseID
      WHERE cl_class.classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
    </cfquery>
    
    <cfquery name="qName" datasource="#this.datasource#">
      INSERT INTO cl_student_class (studentID, classID, feePerCouple, dateCreated)
      VALUES (
        <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">,
        <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">,
        <cfqueryparam cfsqltype="cf_sql_tinyint" value="#qFeePerCouple.feePerCouple#">,
        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      )
    </cfquery>  
  </cffunction>
  
  <cffunction name="addServiceToSchedule" access="public" output="no">
    <cfargument name="studentID" type="numeric" required="yes">
    <cfargument name="serviceIDList" type="numeric" required="yes">
    
    <cfloop index="serviceID" list="#Arguments.serviceIDList#">
      <cfquery name="qName" datasource="#this.datasource#">
        INSERT INTO cl_student_service (studentID, serviceID, dateCreated)
        VALUES (
          <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#serviceID#">,
          <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
      </cfquery>
    </cfloop>
  </cffunction>
  
  <cffunction name="removeClassFromSchedule" access="public" output="no">
    <cfargument name="studentID" type="numeric" required="yes">
    <cfargument name="studentClassID" type="numeric" required="yes">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_student_class
      WHERE studentClassID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentClassID#"> AND
      		studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="removeServiceFromSchedule" access="public" output="no">
    <cfargument name="studentID" type="numeric" required="yes">
    <cfargument name="studentServiceID" type="numeric" required="yes">
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM cl_student_service
      WHERE studentServiceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentServiceID#"> AND
      		studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="editClassAttendeeInfo" access="public" output="no">
    <cfargument name="studentClassID" type="numeric" required="yes">
    <cfargument name="attendeeName" type="string" required="yes">
    <cfargument name="guestNames" type="string" required="yes">
    <cfargument name="payWithCash" type="boolean" default="0">
    <cfargument name="daytimePhone" type="string" required="yes">
    <cfargument name="cellPhone" type="string" required="yes">
    <cfargument name="address1" type="string" required="yes">
    <cfargument name="address2" type="string" required="yes">
    <cfargument name="city" type="string" required="yes">
    <cfargument name="state" type="string" required="yes">
    <cfargument name="zip" type="string" required="yes">
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_student_class
      SET attendeeName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.attendeeName#">,
      	  guestNames=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.guestNames#">,
          payWithCash=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.payWithCash#">,
          daytimePhone=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.daytimePhone#">,
          cellPhone=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.cellPhone#">,
          address1=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.address1#">,
          address2=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.address2#">,
          city=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.city#">,
          state=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.state#">,
          zip=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.zip#">
      WHERE studentClassID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentClassID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="getStudentClassInfo" access="public" output="no" returntype="query">
    <cfargument name="studentClassID" type="numeric" required="yes">
    
    <cfquery name="qStudentClassInfo" datasource="#this.datasource#">
      SELECT *
      FROM cl_student_class
      WHERE studentClassID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentClassID#">
    </cfquery>
    
    <cfreturn qStudentClassInfo>
  </cffunction>
  
  <cffunction name="getStudentServiceInfo" access="public" output="no" returntype="query">
    <cfargument name="studentServiceID" type="numeric" required="yes">
    
    <cfquery name="qStudentServiceInfo" datasource="#this.datasource#">
      SELECT *
      FROM cl_student_service
      WHERE studentServiceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentServiceID#">
    </cfquery>
    
    <cfreturn qStudentServiceInfo>
  </cffunction>
  
  
  <cffunction name="editServiceAttendeeInfo" access="public" output="no">
    <cfargument name="studentServiceID" type="numeric" required="yes">
    <cfargument name="attendeeName" type="string" required="yes">
    <cfargument name="guestNames" type="string" required="yes">
    <cfargument name="payWithCash" type="boolean" default="0">
    <cfargument name="daytimePhone" type="string" required="yes">
    <cfargument name="cellPhone" type="string" required="yes">
    <cfargument name="address1" type="string" required="yes">
    <cfargument name="address2" type="string" required="yes">
    <cfargument name="city" type="string" required="yes">
    <cfargument name="state" type="string" required="yes">
    <cfargument name="zip" type="string" required="yes">
    
    <cfquery datasource="#this.datasource#">
      UPDATE cl_student_service
      SET attendeeName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.attendeeName#">,
      	  guestNames=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.guestNames#">,
          payWithCash=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.payWithCash#">,
          daytimePhone=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.daytimePhone#">,
          cellPhone=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.cellPhone#">,
          address1=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.address1#">,
          address2=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.address2#">,
          city=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.city#">,
          state=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.state#">,
          zip=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.zip#">
      WHERE studentServiceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentServiceID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="getCourseProfitabilityReport" access="public" output="no" returntype="query"><!--- adhoc report --->
    <cfargument name="courseID" type="numeric" required="yes">
    <cfargument name="startDate" type="string" required="yes">
    <cfargument name="endDate" type="string" required="yes">   
    
    <cfset Variables.startDate=DateAdd("d", 0, Arguments.startDate)>
    <cfset Variables.endDate=DateAdd("d", 1, Arguments.endDate)>
    
    <cfquery name="qReport1" datasource="#this.datasource#">
      SELECT cl_class.classID, view_cl_classinstance_summary.firstStartDateTime,
      		 COUNT(cl_student_class.studentClassID) AS numParticipants, SUM(cl_student_class.fee) AS feeCollected, 0 AS numCouponUsed
      FROM (cl_class INNER JOIN view_cl_classinstance_summary ON cl_class.classID=view_cl_classinstance_summary.classID) LEFT JOIN cl_student_class ON
      		cl_class.classID=cl_student_class.classID
      WHERE cl_class.courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#"> AND
      		view_cl_classinstance_summary.firstStartDateTime >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDate#"> AND
            view_cl_classinstance_summary.firstStartDateTime < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDate#">
      GROUP BY cl_class.classID
    </cfquery>  
    
    <cfquery name="qReport2" datasource="#this.datasource#">
      SELECT cl_class.classID, COUNT(DISTINCT cl_classattendance.studentClassID) AS numAttends
      FROM ((cl_class INNER JOIN view_cl_classinstance_summary ON cl_class.classID=view_cl_classinstance_summary.classID) LEFT JOIN cl_student_class ON
      		cl_class.classID=cl_student_class.classID) LEFT JOIN cl_classattendance ON cl_student_class.studentClassID=cl_classattendance.studentClassID
      WHERE cl_class.courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#"> AND
      		view_cl_classinstance_summary.firstStartDateTime >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDate#"> AND
            view_cl_classinstance_summary.firstStartDateTime < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDate#">
      GROUP BY cl_class.classID
    </cfquery>
    
    <cfquery name="qReport" dbtype="query">
      SELECT *
      FROM qReport1, qReport2
      WHERE qReport1.classID=qReport2.classID
      ORDER BY qReport1.firstStartDateTime ASC
    </cfquery>   
    
    <cfquery name="qReport3" datasource="#this.datasource#">
      SELECT cl_class.classID, COUNT(cl_student_class.studentClassID) AS numCouponUsed
      FROM (cl_class INNER JOIN view_cl_classinstance_summary ON cl_class.classID=view_cl_classinstance_summary.classID) INNER JOIN cl_student_class ON
      		cl_class.classID=cl_student_class.classID
      WHERE cl_class.courseID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.courseID#"> AND
      		view_cl_classinstance_summary.firstStartDateTime >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDate#"> AND
            view_cl_classinstance_summary.firstStartDateTime < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDate#"> AND
            cl_student_class.discountCode <> ''
      GROUP BY cl_class.classID
    </cfquery> 
    
    <cfloop query="qReport">
      <cfset Variables.classIDTemp=qReport.classID>
      <cfset Variables.numCouponUsed=0>
      <cfloop query="qReport3">
        <cfif Not Compare(Variables.classIDTemp, qReport3.classID)>
      	  <cfset Variables.numCouponUsed=qReport3.numCouponUsed>
          <cfbreak>
        </cfif>
      </cfloop>
      <cfset qReport.numCouponUsed=Variables.numCouponUsed>
    </cfloop>

    <cfreturn qReport>
  </cffunction>
  
  <cffunction name="getCourseProfitabilityCannedReport" access="public" output="no" returntype="query"><!--- canned report --->
    <cfargument name="startDate" type="string" required="yes">
    <cfargument name="endDate" type="string" required="yes">   
    
    <cfset Variables.startDate=DateAdd("d", 0, Arguments.startDate)>
    <cfset Variables.endDate=DateAdd("d", 1, Arguments.endDate)>

    <cfquery name="qReport1" datasource="#this.datasource#">
      SELECT cl_course.courseID, cl_course.courseTitle, cl_course.feeRegular, cl_course.feeFitnessClub, cl_course.feeEmployee, cl_course.feeDoctor,
      		 COUNT(DISTINCT cl_student_class.studentClassID) AS numParticipants, SUM(cl_student_class.fee) AS feeCollected
      FROM  ((cl_course INNER JOIN cl_class ON cl_course.courseID=cl_class.courseID) INNER JOIN view_cl_classinstance_summary ON
      		 cl_class.classID=view_cl_classinstance_summary.classID) INNER JOIN cl_student_class ON
             cl_class.classID=cl_student_class.classID
      WHERE view_cl_classinstance_summary.firstStartDateTime >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDate#"> AND
            view_cl_classinstance_summary.firstStartDateTime < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDate#">
      GROUP BY cl_course.courseID
    </cfquery>
    
    
    <cfquery name="qReport2" datasource="#this.datasource#">
      SELECT cl_course.courseID, COUNT(cl_classattendance.studentClassID) AS numAttends
      FROM  (((cl_course INNER JOIN cl_class ON cl_course.courseID=cl_class.courseID) INNER JOIN view_cl_classinstance_summary ON
      		 cl_class.classID=view_cl_classinstance_summary.classID) INNER JOIN cl_student_class ON
             cl_class.classID=cl_student_class.classID) LEFT JOIN cl_classattendance ON
             cl_student_class.studentClassID=cl_classattendance.studentClassID
      WHERE view_cl_classinstance_summary.firstStartDateTime >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDate#"> AND
            view_cl_classinstance_summary.firstStartDateTime < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDate#">
      GROUP BY cl_course.courseID
    </cfquery>
    
    <cfquery name="qReport" dbtype="query">
      SELECT *
      FROM qReport1, qReport2
      WHERE qReport1.classID=qReport2.classID
      ORDER BY cl_course.courseTitle ASC
    </cfquery>     
    
    <cfreturn qReport>
  </cffunction>
  
  <cffunction name="getReachFrequencyReport" access="public" output="no" returntype="query"><!--- canned report --->
    <cfargument name="startDate" type="string" required="yes">
    <cfargument name="endDate" type="string" required="yes">  
    
    <cfset Variables.startDate=DateAdd("d", 0, Arguments.startDate)>
    <cfset Variables.endDate=DateAdd("d", 1, Arguments.endDate)>
    
    <cfquery name="qReport" datasource="#this.datasource#">
      SELECT cl_category.categoryID, cl_category.categoryName,
      		 COUNT(DISTINCT cl_student_class.studentID) AS numIndividuals,
             COUNT(cl_student_class.studentClassID) AS numRegistrants
      FROM ((((cl_category INNER JOIN cl_course_category ON cl_category.categoryID=cl_course_category.categoryID) INNER JOIN cl_course ON
      		cl_course_category.courseID=cl_course.courseID) INNER JOIN cl_class ON
            cl_course.courseID=cl_class.courseID) INNER JOIN view_cl_classinstance_summary ON
      		 cl_class.classID=view_cl_classinstance_summary.classID) INNER JOIN cl_student_class ON
             cl_class.classID=cl_student_class.classID      
      WHERE view_cl_classinstance_summary.firstStartDateTime >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDate#"> AND
            view_cl_classinstance_summary.firstStartDateTime < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDate#">
      GROUP BY cl_category.categoryID
      ORDER BY cl_category.categoryName
    </cfquery>  
  
    <cfreturn qReport>
  </cffunction>
  
  <cffunction name="getAllCoupons" access="public" output="no" returntype="query">
    <cfquery name="qCoupons" datasource="#this.datasource#">
      SELECT DISTINCT discountCode
      FROM cl_course
      WHERE discountCode<>''
      
      UNION
      
      SELECT DISTINCT discountCode
      FROM cl_orderdetail
      WHERE discountCode<>''
            
      
      ORDER BY discountCode
    </cfquery>
    
    <cfreturn qCoupons>
  </cffunction>
  
  <cffunction name="getCouponCodeReport" access="public" output="no" returntype="query">
    <cfargument name="discountCode" type="string" required="yes">
    <cfargument name="startDate" type="string" required="yes">
    <cfargument name="endDate" type="string" required="yes">   
    
    <cfset Variables.startDate=DateAdd("d", 0, Arguments.startDate)>
    <cfset Variables.endDate=DateAdd("d", 1, Arguments.endDate)>
    
    <cfquery name="qReport" datasource="#this.datasource#">
      SELECT cl_ordersummary.orderDate, cl_ordersummary.contactFirstName, cl_ordersummary.contactLastName,
      		 cl_orderdetail.amountPaid, cl_orderdetail.discountAmount, cl_course.courseTitle
      FROM ((cl_orderdetail INNER JOIN cl_ordersummary ON cl_orderdetail.orderID=cl_ordersummary.orderID) INNER JOIN cl_class ON
      		cl_orderdetail.classID=cl_class.classID) INNER JOIN cl_course ON cl_class.courseID=cl_course.courseID
      WHERE cl_orderdetail.discountCode=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.discountCode#"> AND
      		cl_ordersummary.orderDate >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.startDate#"> AND
            cl_ordersummary.orderDate < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.endDate#">
      ORDER BY cl_ordersummary.orderDate ASC
    </cfquery>
    
    <cfreturn qReport>
  </cffunction>
  
  
  <cffunction name="getLastImportFileTimestamp" access="public" output="no" returntype="string">
    <cfquery name="qTimestamp" datasource="#this.datasource#">
      SELECT importFileTimestamp
      FROM cl_fc_import_log
      ORDER BY logID DESC
      LIMIT 0,1
    </cfquery>
  
    <cfif qTimestamp.recordcount GT 0>
      <cfreturn qTimestamp.importFileTimestamp>
    <cfelse>
      <cfreturn now()>
    </cfif>
  </cffunction>
  
  <cffunction name="importFitnessClubMembers" access="public" output="no" returntype="string">
    <cfargument name="dataFilePath" type="string" default="">
    <cfargument name="targetAbsoluteDirectory" type="string" default="">
	<cfargument name="localFileFieldName" type="string" default="">
    
    <cfif Compare(Arguments.localFileFieldName,"")>
      <cffile action="upload"
	          destination="#Arguments.targetAbsoluteDirectory#"
	          nameconflict="overwrite"
	          filefield="#Arguments.localFileFieldName#">
      <cfset uploadedFilename=cffile.serverFile>
      <cfset  Arguments.dataFilePath="#Arguments.targetAbsoluteDirectory#/#uploadedFilename#">
      <cfset importFileTimestamp=now()>
    <cfelseif Compare(Arguments.dataFilePath,"")>
	  <cfset fileObj = CreateObject("java","java.io.File").init(Arguments.dataFilePath)>
      <cfset importFileTimestamp = CreateObject("java","java.util.Date").init(fileObj.lastModified())>
    </cfif>
    
    <cfset importLog="">
    <cfset newline=Chr(13) & Chr(10)>
    <cfset numRecordsimported=0>
    <cfset UT=CreateObject("component","Utility")>
    <cftry>
        <cfset lines=UT.CSVToArray(CSVFilePath="#Arguments.dataFilePath#")>
        <cfquery datasource="#this.datasource#">
          TRUNCATE TABLE cl_fitnessmember
        </cfquery>
        <cfloop index="row" from="2" to="#ArrayLen(lines)#">
          <cfif ArrayLen(lines[row]) LT 3>
            <cfset importLog=importLog & newline & "Line #row# has insufficient number of fields.">
          <cfelse>
            <cftry>
              <cfquery datasource="#this.datasource#">
                INSERT INTO cl_fitnessmember
                (memberID, memberName, memberEmail, dateLastModified)
                VALUES
                (<cfqueryparam cfsqltype="cf_sql_integer" value="#lines[row][1]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][2]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][3]#">,                 
                 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
              </cfquery>
              <cfset numRecordsImported=numRecordsImported+1>
            <cfcatch>
              <cfset importLog=importLog & newline & "ERROR: " & cfcatch.Message>
            </cfcatch>
            </cftry>  
          </cfif>
        </cfloop>
    
		<cfif Not Compare(importLog,"")>
          <cfset importLog="The data file was imported successfully.">
        </cfif>
   	<cfcatch>
    	<cfset importLog=importLog & newline & "ERROR: " & cfcatch.Message>
    </cfcatch>
    </cftry>
    
    <cfset importLog=importLog & newline & "Numer of records imported: #numRecordsImported#">    
    
    <cfquery datasource="#this.datasource#">
     INSERT INTO cl_fc_import_log
     (dateImport, importLog, importFileTimestamp)
     VALUES
     (<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
      <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#importLog#">,
      <cfqueryparam cfsqltype="cf_sql_timestamp" value="#importFileTimestamp#">)
    </cfquery>
    
    <cfreturn importLog>
  </cffunction>
</cfcomponent>