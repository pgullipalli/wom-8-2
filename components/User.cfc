<cfcomponent displayname="User" extends="Base" output="no" hint="Used for a session variable that store user's class sheduling info">
  <cfscript>
    this.isLoggedIn=false;
	this.studentID=0;
	this.email="";
	this.studentType="R";//Regular as default
	this.fitnessClubMemberID="0";
	this.sendReminderEmail=0;
	
	this.penciledInClasses=ArrayNew(1);//ClassEntity array
	this.penciledInServices=ArrayNew(1);	
	this.donationAmount=0;
	
	this.contactFirstName="";
	this.contactLastName="";
	this.contactAddress1="";
	this.contactAddress2="";
	this.contactCity="";
	this.contactState="";
	this.contactZip="";
	this.daytimePhone=""; this.daytimePhonePart1=""; this.daytimePhonePart2=""; this.daytimePhonePart3="";
	this.cellPhone=""; this.cellPhonePart1=""; this.cellPhonePart2=""; this.cellPhonePart3="";
	this.billingFirstName="";
	this.billingLastName="";
	this.billingAddress1="";
	this.billingAddress2="";
	this.billingCity="";
	this.billingState="";
	this.billingZip="";
	
	this.enrollPaymentPlan=false;//enroll payment for classes cost more than 750
	
	this.paymentMethod="Credit Card";//other options: Bank Account, Cash
	//credit card
	this.cardType="";
	this.nameOnCard="";
	this.cardNumber="";
	this.cardExpirationDate="";
	this.cardCVV2="";
	//back accout
	this.accountType="Checking";
	this.nameOnAccount="";
	this.accountNumber="";
	this.routingNumber="";
	
	this.payWithCash=0;//deprecated		
	this.message="";
	this.receiptHTML="";
  </cfscript>
   
  <cffunction name="login" access="public" output="no" returntype="boolean">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="password" type="string" required="yes">
    
    <cfquery name="qStudent" datasource="#this.datasource#">
      SELECT *
      FROM cl_student
      WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#"> AND
      		password=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.password#">
    </cfquery>
  
    <cfif qStudent.recordcount GT 0>
      <cfset this.isLoggedIn=true>
      <cfset this.studentID=qStudent.studentID>
      <cfset this.fitnessClubMemberID=qStudent.fitnessClubMemberID>
  	  <cfset this.email=qStudent.email>      
      
      <cfif Compare(this.studentType, qStudent.studentType)>       
        <cfif Not Compare(qStudent.studentType,"F")>
          <cfif Not verifyFitnessClubMembership()>
            <cfset this.studentType="R">
          <cfelse>
            <cfset this.studentType=qStudent.studentType>
          </cfif>
        <cfelse>
          <cfset this.studentType=qStudent.studentType>
        </cfif>
        
        <!--- adjust price for Penciled-In Classes --->
        <cfset resetPenciledInClassesPricing()>
        <!--- adjust price for Penciled-In Services --->
        <cfset resetPenciledInServicesPricing()> 	    
      </cfif>
      
      <cfset this.sendReminderEmail=qStudent.sendReminderEmail>
      
      <!--- pre-populate some fields for checkout --->
      <cfquery name="qBasicInfo" datasource="#this.datasource#">
        SELECT contactFirstName,contactLastName,contactAddress1,contactAddress2,contactCity,contactState,contactZIP,
        	   daytimePhone, cellPhone, billingFirstName, billingLastName, billingAddress1, billingAddress2,
               billingCity, billingState, billingZIP
        FROM cl_ordersummary
        WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qStudent.studentID#">
        ORDER BY orderDate DESC
      </cfquery>
      <cfif qBasicInfo.recordcount GT 0>
 		<cfset this.contactFirstName=qBasicInfo.contactFirstName>
        <cfset this.contactLastName=qBasicInfo.contactLastName>
        <cfset this.contactAddress1=qBasicInfo.contactAddress1>
        <cfset this.contactAddress2=qBasicInfo.contactAddress2>
        <cfset this.contactCity=qBasicInfo.contactCity>
        <cfset this.contactState=qBasicInfo.contactState>
        <cfset this.contactZip=qBasicInfo.contactZIP>
        <cfif Compare(qBasicInfo.daytimePhone,"")>
          <cfset phoneParts=ListToArray(qBasicInfo.daytimePhone,"-")>
          <cfif ArrayLen(phoneParts) Is 3 And Len(phoneParts[1]) Is 3 And Len(phoneParts[2]) Is 3 And Len(phoneParts[3]) Is 4>
            <cfset this.daytimePhone=qBasicInfo.daytimePhone>
            <cfset this.daytimePhonePart1=phoneParts[1]>
            <cfset this.daytimePhonePart2=phoneParts[2]>
            <cfset this.daytimePhonePart3=phoneParts[3]>
          </cfif>
        </cfif>
        <cfif Compare(qBasicInfo.cellPhone,"")>
          <cfset phoneParts=ListToArray(qBasicInfo.cellPhone,"-")>
          <cfif ArrayLen(phoneParts) Is 3 And Len(phoneParts[1]) Is 3 And Len(phoneParts[2]) Is 3 And Len(phoneParts[3]) Is 4>
            <cfset this.cellPhone=qBasicInfo.cellPhone>
            <cfset this.cellPhonePart1=phoneParts[1]>
            <cfset this.cellPhonePart2=phoneParts[2]>
            <cfset this.cellPhonePart3=phoneParts[3]>
          </cfif>
        </cfif>
        <cfset this.billingFirstName=qBasicInfo.billingFirstName>
        <cfset this.billingLastName=qBasicInfo.billingLastName>
        <cfset this.billingAddress1=qBasicInfo.billingAddress1>
        <cfset this.billingAddress2=qBasicInfo.billingAddress2>
        <cfset this.billingCity=qBasicInfo.billingCity>
        <cfset this.billingState=qBasicInfo.billingState>
        <cfset this.billingZip=qBasicInfo.billingZIP>
      </cfif>      
      
      <cfreturn true>
    <cfelse>
      <cfreturn false>    
    </cfif>
  </cffunction>
  
  <cffunction name="verifyLogin" access="public" output="no" returntype="boolean">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="password" type="string" required="yes">
    
    <cfif CompareNoCase(Arguments.email, this.email)>
      <cfreturn false>
    </cfif>
    <cfquery name="qStudent" datasource="#this.datasource#">
      SELECT *
      FROM cl_student
      WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#"> AND
      		password=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.password#">
    </cfquery>
    <cfif qStudent.recordcount GT 0>
      <cfreturn true>
    <cfelse>
      <cfreturn false>
    </cfif> 
  </cffunction>

  <cffunction name="verifyFitnessClubMembership" access="public" output="no" returntype="boolean">
    <cfquery name="qFitnessMemberID" datasource="#this.datasource#">
      SELECT fitnessClubMemberID
      FROM cl_student
      WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#"> AND studentType='F'
    </cfquery>   
    
    <cfif qFitnessMemberID.fitnessClubMemberID GT 0>
      <cfquery name="qMemberID" datasource="#this.datasource#">
        SELECT *
        FROM cl_fitnessmember
        WHERE memberID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qFitnessMemberID.fitnessClubMemberID#">
      </cfquery>
      <cfif qMemberID.recordcount GT 0>
        <cfreturn true>
      <cfelse>
        <cfreturn false>
      </cfif>
    <cfelse>
      <cfreturn false>
    </cfif>
  </cffunction>
  
  <cffunction name="updateProfile" access="public" output="no" returntype="boolean">
    <cfargument name="change" type="string" required="yes">
    <cfargument name="sendReminderEmail" type="boolean" default="0">
    
    <cfswitch expression="#Arguments.change#">
      <cfcase value="email">
        <cfquery name="qEmail" datasource="#this.datasource#">
          SELECT studentID
          FROM cl_student
          WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.newEmail#">
        </cfquery>
        <cfif qEmail.recordcount GT 0>
          <cfreturn false>
        <cfelse>
          <cfquery datasource="#this.datasource#">
            UPDATE cl_student
            SET email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.newEmail#">
            WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#this.email#">
          </cfquery>
          <cfset this.email=Arguments.newEmail>
          <cfreturn true>
        </cfif>
      </cfcase>
      
      <cfcase value="password">
        <cfquery datasource="#this.datasource#">
          UPDATE cl_student
          SET password=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.newPassword#">
          WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#this.email#">
        </cfquery>
        <cfreturn true>
      </cfcase>
      
      <cfcase value="reminder">
        <cfquery datasource="#this.datasource#">
          UPDATE cl_student
          SET sendReminderEmail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sendReminderEmail#">
          WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#this.email#">
        </cfquery>
        <cfset this.sendReminderEmail=Arguments.sendReminderEmail>
        <cfreturn true>
      </cfcase>
      
      <cfcase value="fitness">
        <cfquery name="qFitnessMemberNumber" datasource="#this.datasource#">
          SELECT *
          FROM cl_fitnessmember
          WHERE memberID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClubMemberID#">
        </cfquery>
        <cfif qFitnessMemberNumber.recordcount Is 0>
          <cfreturn false>
        <cfelse>
          <cfquery name="qStudentID" datasource="#this.datasource#">
            SELECT studentID
            FROM cl_student
            WHERE studentID<><cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#"> AND
            	  fitnessClubMemberID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClubMemberID#"> AND
                  studentType='F'
          </cfquery>
          <cfif qStudentID.recordcount GT 0>
            <cfreturn false>          
          <cfelse>  
            <cfquery datasource="#this.datasource#">
              UPDATE cl_student
              SET fitnessClubMemberID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.fitnessClubMemberID#">,
              	  studentType='F'
              WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#">
            </cfquery>
            <cfset this.fitnessClubMemberID=Arguments.fitnessClubMemberID>
            <cfset this.studentType="F">
            <!--- adjust price for Penciled-In Classes --->
			<cfset resetPenciledInClassesPricing()>
            <!--- adjust price for Penciled-In Services --->
            <cfset resetPenciledInServicesPricing()>
          </cfif>
        </cfif>        
      </cfcase>
    </cfswitch>
  
    <cfreturn true>
  </cffunction>
  
  <cffunction name="updateClassReminder" access="public" output="no" returntype="void">
    <cfargument name="sendReminderEmail" type="boolean" default="0">
    
    <cfif Arguments.sendReminderEmail>
      <cfquery datasource="#this.datasource#">
        UPDATE cl_student
        SET sendReminderEmail=1
        WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#">
      </cfquery>
      <cfset this.sendReminderEmail=1>
    </cfif>
  </cffunction>
  
  <cffunction name="emptyCart" access="public" output="no" returntype="void">
    <cfset ArrayClear(this.penciledInClasses)>
    <cfset ArrayClear(this.penciledInServices)>
    <cfset this.enrollPaymentPlan=false>
    <cfset this.donationAmount=0>
  </cffunction>
  





  <cffunction name="addClassToCart" access="public" output="no" returntype="void">
    <cfargument name="classID" type="numeric" required="yes">
    
    <cfset var idx = 1>
	<cfset var numClasses=ArrayLen(this.penciledInClasses)>
    <cfset var found=false>
    
    <!--- codes with validation: same class CAN NOT be added to the cart more than once --->
    <cfloop index="idx" from="1" to="#numClasses#">
      <cfif this.penciledInClasses[idx].classID Is Arguments.classID>
        <cfset found=true>
        <cfbreak>
      </cfif>
    </cfloop>
    

    <cfif Not found>
      <cfset newClassEntity=CreateObject("component","ClassEntity").getClassEntityByID(classID="#Arguments.classID#", studentType="#this.studentType#")>
      <cfset ArrayAppend(this.penciledInClasses,newClassEntity)>
    </cfif> 
    
    <!--- codes without validation: same class CAN be added to the cart more than once --->
    <!---
      <cfset newClassEntity=CreateObject("component","ClassEntity").getClassEntityByID(classID="#Arguments.classID#", studentType="#this.studentType#")>
      <cfset ArrayAppend(this.penciledInClasses,newClassEntity)> --->



  </cffunction>  





  
  <cffunction name="resetPenciledInClassesPricing" access="public" output="no" returntype="void">
    <cfset var CLObject=CreateObject("component","Class").init()>
    
    <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClasses)#">
      <cfset courseInfo=CLObject.getCourseByClassID(this.penciledInClasses[idx].classID)>
      <cfif courseInfo.recordcount GT 0>
        <cfswitch expression="#this.studentType#">
          <cfcase value="R"><cfset this.penciledInClasses[idx].cost=courseInfo.feeRegular></cfcase>
          <cfcase value="D"><cfset this.penciledInClasses[idx].cost=courseInfo.feeDoctor></cfcase>
          <cfcase value="E"><cfset this.penciledInClasses[idx].cost=courseInfo.feeEmployee></cfcase>
          <cfcase value="F"><cfset this.penciledInClasses[idx].cost=courseInfo.feeFitnessClub></cfcase>
        </cfswitch>  
        <cfif this.penciledInClasses[idx].cost GTE 750>
		  <cfset this.penciledInClasses[idx].paymentPlanEligible=true>
          <cfset this.penciledInClasses[idx].paymentPlanType="A">
          <cfset this.penciledInClasses[idx].firstInstallment=Ceiling(this.penciledInClasses[idx].cost/3) + this.penciledInClasses[idx].installmentFee>
          <cfset this.penciledInClasses[idx].remainingPayment=this.penciledInClasses[idx].cost - this.penciledInClasses[idx].firstInstallment + this.penciledInClasses[idx].installmentFee>
        <cfelseif this.penciledInClasses[idx].cost GTE 400 And this.penciledInClasses[idx].cost LT 750>
          <cfset this.penciledInClasses[idx].paymentPlanEligible=true>
          <cfset this.penciledInClasses[idx].paymentPlanType="B">
          <cfset this.penciledInClasses[idx].firstInstallment=Ceiling(this.penciledInClasses[idx].cost/2) + this.penciledInClasses[idx].installmentFee>
          <cfset this.penciledInClasses[idx].remainingPayment=this.penciledInClasses[idx].cost - this.penciledInClasses[idx].firstInstallment + this.penciledInClasses[idx].installmentFee>
        </cfif>     
      </cfif>
    </cfloop>
  </cffunction>
  
  <cffunction name="resetPenciledInServicesPricing" access="public" output="no" returntype="void">
    <cfset var CLObject=CreateObject("component","Class").init()>
    
    <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInServices)#">
      <cfset serviceInfo=CLObject.getServiceByID(this.penciledInServices[idx].serviceID)>
      <cfif serviceInfo.recordcount GT 0>
        <cfswitch expression="#this.studentType#">
          <cfcase value="R"><cfset this.penciledInServices[idx].cost=serviceInfo.feeRegular></cfcase>
          <cfcase value="D"><cfset this.penciledInServices[idx].cost=serviceInfo.feeDoctor></cfcase>
          <cfcase value="E"><cfset this.penciledInServices[idx].cost=serviceInfo.feeEmployee></cfcase>
          <cfcase value="F"><cfset this.penciledInServices[idx].cost=serviceInfo.feeFitnessClub></cfcase>
        </cfswitch>      
      </cfif>
    </cfloop>  
  </cffunction>
  
  <cffunction name="addServiceToCart" access="public" output="no" returntype="void">
    <cfargument name="serviceID" type="numeric" required="yes">
    
    <cfset var idx = 1>
    <cfset var numServices=ArrayLen(this.penciledInServices)>
    <cfset var found=false>
    
    <cfloop index="idx" from="1" to="#numServices#">
      <cfif this.penciledInServices[idx].serviceID Is Arguments.serviceID>
        <cfset found=true>
        <cfbreak>
      </cfif>
    </cfloop>
    
    <cfif Not found>
      <cfset newServiceEntity=CreateObject("component","ServiceEntity").getServiceEntityByID(serviceID="#Arguments.serviceID#", studentType="#this.studentType#")>
      <cfset ArrayAppend(this.penciledInServices, newServiceEntity)>    
    </cfif>    
  </cffunction>
  
  <cffunction name="addDiscountCodeToCart" access="public" output="no" returntype="boolean">
    <cfargument name="discountCode" type="string" required="yes">
    
    <cfset var idx = 1>
    <cfset var numClasses=ArrayLen(this.penciledInClasses)>
    <cfset var found=false>
    
    <cfloop index="idx" from="1" to="#numClasses#">
      <cfif this.penciledInClasses[idx].applyDiscountCode(Arguments.discountCode)>
        <cfset found=true>
      </cfif>
    </cfloop>
    <cfif found>
      <cfreturn true>
    <cfelse>
      <cfreturn false>
    </cfif>
  </cffunction>
  
  <cffunction name="removeClassesFromCart" access="public" output="no" returntype="void">
    <cfargument name="classIDList" type="string" default=""> 
    
    <cfset var idx = 1>
    
    <cfif Not Compare(Arguments.classIDList,"")><cfreturn></cfif>   
    
    <cfloop index="classID" list="#Arguments.classIDList#">
      <cfset removeClassFromCart(classID)>      
    </cfloop>
  </cffunction>
  
  <cffunction name="removeClassFromCart" access="public" output="no" returntype="void">
    <cfargument name="classID" type="numeric" required="yes"> 
    
    <cfset var idx = 1>
  
    <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClasses)#">
      <cfif Not Compare(this.penciledInClasses[idx].classID, Arguments.classID)>
        <cfset ArrayDeleteAt(this.penciledInClasses,idx)>
        <cfbreak>
      </cfif>
    </cfloop>
  </cffunction>
  
  <cffunction name="removeServicesFromCart" access="public" output="no" returntype="void">
    <cfargument name="serviceIDList" type="string" default=""> 
    
    <cfset var idx = 1>
    
    <cfif Not Compare(Arguments.serviceIDList,"")><cfreturn></cfif>  
    
    <cfloop index="serviceID" list="#Arguments.serviceIDList#">
      <cfset removeServiceFromCart(serviceID)>      
    </cfloop>
  </cffunction>
  
  <cffunction name="removeServiceFromCart" access="public" output="no" returntype="void">
    <cfargument name="serviceID" type="numeric" required="yes"> 
    
    <cfset var idx = 1>
  
    <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInServices)#">
      <cfif Not Compare(this.penciledInServices[idx].serviceID, Arguments.serviceID)>
        <cfset ArrayDeleteAt(this.penciledInServices,idx)>
        <cfbreak>
      </cfif>
    </cfloop>      
  </cffunction>   
  
  <cffunction name="addCurriculaClassesToCart" access="public" output="no">
    <!--- Form parameters have names like these: classID23, classID5, serviceID7, serviceID123 --->
    <cfset var classID="0">
    <cfset var serviceID="0">
    
    <cfloop collection="#Arguments#" item="key">
      <cfif FindNoCase("classID", key) GT 0>
        <cfset classID=Arguments[key]>
        <cfif Compare(classID, "0")>
          <cfset addClassToCart(classID)>
        </cfif>      
      <cfelseif FindNoCase("serviceID", key) GT 0>
        <cfset serviceID=Arguments[key]>
        <cfif Compare(serviceID, "0")>
          <cfset addServiceToCart(serviceID)>
        </cfif>      
      </cfif>
    </cfloop>
  </cffunction>
  
  <cffunction name="removeDiscountCodesFromCart" access="public" output="no" returntype="void">
    <cfargument name="discountCodeList" type="string" default=""> 
    
    <cfset var idx = 0>    
    
    <cfif Not Compare(Arguments.discountCodeList, "")><cfreturn></cfif>
    
    <cfloop index="discountCode" list="#Arguments.discountCodeList#">
      <cfset removeDiscountCodeFromCart(discountCode)>    
    </cfloop>  
  </cffunction>
  
  <cffunction name="removeDiscountCodeFromCart" access="public" output="no" returntype="void">
    <cfargument name="discountCode" type="string" required="yes">
    
    <cfset var idx = 0>
    
    <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClasses)#">
      <cfif this.penciledInClasses[idx].removeDiscountCode(Arguments.discountCode)>
        <cfbreak>
      </cfif>
    </cfloop>
  </cffunction>
  
  <cffunction name="addDonationToCart" access="public" output="no" returntype="void">
    <cfargument name="donationAmount" type="numeric" default="0">
    
    <cfset this.donationAmount=Arguments.donationAmount>
  </cffunction>
  
  <cffunction name="removeDonationFromCart" access="public" output="no" returntype="void">
    <cfargument name="removeDonation" type="boolean" default="0">
    
    <cfif Arguments.removeDonation>
      <cfset this.donationAmount=0>
    </cfif>
  </cffunction>  
  
  <cffunction name="getPenciledInClasses" access="public" output="no" returntype="array">    
    <cfreturn this.penciledInClasses>
  </cffunction>
  
  <cffunction name="getPenciledInServices" access="public" output="no" returntype="array">    
    <cfreturn this.penciledInServices>
  </cffunction>
  
  <cffunction name="getDonationAmount" access="public" output="no" returntype="numeric">
    <cfreturn this.donationAmount>
  </cffunction>
  
  <cffunction name="updatePaymentPlanOption" access="public" output="no" returntype="void">
    <cfargument name="paymentPlan" type="boolean" required="no">
    
	<cfset var idx = 0>
    
    <cfif IsDefined("Arguments.paymentPlan")>
      <cfset this.enrollPaymentPlan=false>
      <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClasses)#">
        <cfif this.penciledInClasses[idx].paymentPlanEligible>
          <cfset this.penciledInClasses[idx].enrollPaymentPlan=true>
          <cfset this.enrollPaymentPlan=true>
        </cfif>
      </cfloop>
    <cfelse>
      <cfset this.enrollPaymentPlan=false>
      <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClasses)#">
        <cfif this.penciledInClasses[idx].paymentPlanEligible>
          <cfset this.penciledInClasses[idx].enrollPaymentPlan=false>
        </cfif>
      </cfloop>
    </cfif>
  </cffunction>
  
  <cffunction name="getCartTotal" access="public" output="no" returntype="numeric">
    <cfset var idx1 = 0>
    <cfset var idx2 = 0>
    <cfset var totalCost = 0>
    
    <cfloop index="idx1" from="1" to="#ArrayLen(this.penciledInClasses)#">
      <cfif this.penciledInClasses[idx1].paymentPlanEligible And this.penciledInClasses[idx1].enrollPaymentPlan>
        <cfset totalCost = totalCost + this.penciledInClasses[idx1].firstInstallment>
      <cfelse>
        <cfset totalCost = totalCost + this.penciledInClasses[idx1].cost>
      </cfif>     
      <cfif this.penciledInClasses[idx1].discountApplied>
        <cfset totalCost = totalCost - this.penciledInClasses[idx1].discountAmount>
      </cfif>
    </cfloop>
    <cfloop index="idx1" from="1" to="#ArrayLen(this.penciledInServices)#">
      <cfset totalCost = totalCost + this.penciledInServices[idx1].cost>
    </cfloop>
    
    <cfset totalCost = totalCost+ this.donationAmount>
    
    <cfif totalCost LT 0>
      <cfreturn 0>
    <cfelse>
      <cfreturn totalCost>
    </cfif>
  </cffunction>
  
  <cffunction name="hasPaymentPlanEligibleClass" access="public" output="no" returntype="boolean">
    <cfset var idx = 0>
    <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClasses)#">
      <cfif this.penciledInClasses[idx].paymentPlanEligible><cfreturn true></cfif>
    </cfloop>
    <cfreturn false>
  </cffunction>
    
  <cffunction name="getUpcomingClasses" access="public" output="no" returntype="array">
    <cfset var currentTime=now()>
    <cfset var upcomingClasses=ArrayNew(1)>
    
    <cfquery name="qClassIDs" datasource="#this.datasource#">
      SELECT cl_student_class.classID, cl_student_class.studentClassID, cl_student_class.attendeeName,
      		 view_cl_classinstance_summary.firstStartDateTime      		 
      FROM  cl_student_class INNER JOIN view_cl_classinstance_summary ON
      		cl_student_class.classID=view_cl_classinstance_summary.classID
      WHERE cl_student_class.studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#"> AND
      		view_cl_classinstance_summary.lastStartDateTime >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
      ORDER BY view_cl_classinstance_summary.firstStartDateTime
    </cfquery>
  
    <cfif qClassIDs.recordcount GT 0>
      <cfloop index="idx" from="1" to="#qClassIDs.recordcount#">
        <cfset upcomingClasses[idx]=CreateObject("component","com.ClassEntity").getClassEntityByID(qClassIDs.classID[idx])>
        <cfset upcomingClasses[idx].studentClassID=qClassIDs.studentClassID[idx]>
        <cfset upcomingClasses[idx].attendeeName=qClassIDs.attendeeName[idx]>
      </cfloop>    
    </cfif>
    
    <cfreturn upcomingClasses>
  </cffunction>
  
  <cffunction name="getUpcomingServices" access="public" output="no" returntype="array">
    <cfset var currentTime=now()>
    <cfset var upcomingServices=ArrayNew(1)>
    
    <cfquery name="qServiceIDs" datasource="#this.datasource#">
      SELECT serviceID, attendeeName
      FROM  cl_student_service
      WHERE studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#">
      ORDER BY dateCreated DESC
    </cfquery>
    
    <cfquery name="qCompletedServiceIDs" datasource="#this.datasource#">
      SELECT cl_student_service.serviceID
      FROM  cl_student_service INNER JOIN cl_serviceattendance ON cl_student_service.studentServiceID=cl_serviceattendance.studentServiceID 
      WHERE cl_student_service.studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#">
    </cfquery>
    
    <cfif qCompletedServiceIDs.recordcount GT 0>
      <cfset completedServiceIDList=ValueList(qCompletedServiceIDs.serviceID)>
    <cfelse>
      <cfset completedServiceIDList="0">
    </cfif>
  
    <cfif qServiceIDs.recordcount GT 0>
      <cfloop index="idx" from="1" to="#qServiceIDs.recordcount#">
        <cfif ListFind(completedServiceIDList,qServiceIDs.serviceID[idx]) Is 0>
          <cfset upcomingServices[idx]=CreateObject("component","com.ServiceEntity").getServiceEntityByID(qServiceIDs.serviceID[idx])>
          <cfset upcomingServices[idx].attendeeName=qServiceIDs.attendeeName[idx]>
        </cfif>
      </cfloop>    
    </cfif>
    
    <cfreturn upcomingServices>
  </cffunction>
  
  <cffunction name="getCompletedClasses" access="public" output="no" returntype="array">
    <cfset var currentTime=now()>
    <cfset var completedClasses=ArrayNew(1)>
    
    <cfquery name="qClassIDs" datasource="#this.datasource#">
      SELECT cl_student_class.classID, cl_student_class.feedbackSent, view_cl_classinstance_summary.firstStartDateTime
      FROM  cl_student_class INNER JOIN view_cl_classinstance_summary ON
      		cl_student_class.classID=view_cl_classinstance_summary.classID
      WHERE cl_student_class.studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#"> AND
      		view_cl_classinstance_summary.lastStartDateTime < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#currentTime#">
      ORDER BY view_cl_classinstance_summary.firstStartDateTime DESC
    </cfquery>
  
    <cfif qClassIDs.recordcount GT 0>
      <cfloop index="idx" from="1" to="#qClassIDs.recordcount#">
        <cfset completedClasses[idx]=CreateObject("component","com.ClassEntity").getClassEntityByID(qClassIDs.classID[idx])>
        <cfset completedClasses[idx].feedbackSent=qClassIDs.feedbackSent[idx]>
      </cfloop>    
    </cfif>
    
    <cfreturn completedClasses>
  </cffunction>
  
  <cffunction name="getCompletedServices" access="public" output="no" returntype="array">
    <cfset var currentTime=now()>
    <cfset var completedServices=ArrayNew(1)>
    
	<cfquery name="qServiceIDs" datasource="#this.datasource#">
      SELECT cl_student_service.serviceID, cl_student_service.feedbackSent
      FROM  cl_student_service INNER JOIN cl_serviceattendance ON cl_student_service.studentServiceID=cl_serviceattendance.studentServiceID 
      WHERE cl_student_service.studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#">
    </cfquery>    
  
    <cfif qServiceIDs.recordcount GT 0>
      <cfloop index="idx" from="1" to="#qServiceIDs.recordcount#">
        <cfset completedServices[idx]=CreateObject("component","com.ServiceEntity").getServiceEntityByID(qServiceIDs.serviceID[idx])>
        <cfset completedServices[idx].feedbackSent=qServiceIDs.feedbackSent[idx]>
      </cfloop>    
    </cfif>
    
    <cfreturn completedServices>
  </cffunction>

  <cffunction name="getMessage" access="public" output="no" returntype="string">
    <cfset var tempMessage=this.message>
    <cfset this.message="">
    <cfreturn tempMessage>
  </cffunction>

  <cffunction name="updateBillingInfo" access="public" output="no" returntype="void">    
    <cfargument name="feePerCoupleClassIDList" type="string" default="">    
    <cfargument name="contactFirstName" type="string" required="yes">
    <cfargument name="contactLastName" type="string" required="yes">
    <cfargument name="contactAddress1" type="string" required="yes">
    <cfargument name="contactAddress2" type="string" default="">
    <cfargument name="contactCity" type="string" required="yes">
    <cfargument name="contactState" type="string" required="yes">
    <cfargument name="contactZip" type="string" required="yes">
    <cfargument name="daytimePhonePart1" type="string" required="yes">
    <cfargument name="daytimePhonePart2" type="string" required="yes">
    <cfargument name="daytimePhonePart3" type="string" required="yes">
    <cfargument name="cellPhonePart1" type="string" required="yes">
    <cfargument name="cellPhonePart2" type="string" required="yes">
    <cfargument name="cellPhonePart3" type="string" required="yes">
     
    <cfargument name="paymentMethod" type="string" default="Credit Card">
    <cfargument name="cardType" type="string" default="">
    <cfargument name="nameOnCard" type="string" default="">
    <cfargument name="cardNumber" type="string" default="">
    <cfargument name="cardExpirationDate" type="string" default="">
    <cfargument name="cardCVV2" type="string" default="">
    
    <cfargument name="nameOnAccount" type="string" default="">
    <cfargument name="accountNumber" type="string" default="">
    <cfargument name="routingNumber" type="string" default="">    
    
    <cfargument name="billingFirstName" type="string" default="">
    <cfargument name="billingLastName" type="string" default="">
    <cfargument name="billingAddress1" type="string" default="">
    <cfargument name="billingAddress2" type="string" default="">
    <cfargument name="billingCity" type="string" default="">
    <cfargument name="billingState" type="string" default="">
    <cfargument name="billingZip" type="string" default="">
    
    <cfset var i = 1>
    <cfset var j = 1>
    
    <cfscript>  
	  for (i=1; i LTE ArrayLen(this.penciledInClasses); i=i+1) {
	    this.penciledInClasses[i].attendeeName=Evaluate("Arguments.attendeeName#this.penciledInClasses[i].classID#_#i#");
		this.penciledInClasses[i].guestNames=Evaluate("Arguments.guestNames#this.penciledInClasses[i].classID#_#i#");	
	  }
	  for (i=1; i LTE ArrayLen(this.penciledInServices); i=i+1) {
	    this.penciledInServices[i].attendeeName=Evaluate("Arguments.serviceAttendeeName#this.penciledInServices[i].serviceID#_#i#");
		this.penciledInServices[i].guestNames=Evaluate("Arguments.serviceGuestNames#this.penciledInServices[i].serviceID#_#i#");	
	  }
	  
	  this.contactFirstName=Arguments.contactFirstName;
	  this.contactLastName=Arguments.contactLastName;
	  this.contactAddress1=Arguments.contactAddress1;
	  this.contactAddress2=Arguments.contactAddress2;
	  this.contactCity=Arguments.contactCity;
	  this.contactState=Arguments.contactState;
	  this.contactZip=Arguments.contactZip;
	  this.daytimePhonePart1=Arguments.daytimePhonePart1;
	  this.daytimePhonePart2=Arguments.daytimePhonePart2;
	  this.daytimePhonePart3=Arguments.daytimePhonePart3;  
	  this.daytimePhone=this.daytimePhonePart1 & "-" & this.daytimePhonePart2 & "-" & this.daytimePhonePart3;
	  this.daytimePhone=Replace(this.daytimePhone,"--","");
	  this.cellPhonePart1=Arguments.cellPhonePart1;
	  this.cellPhonePart2=Arguments.cellPhonePart2;
	  this.cellPhonePart3=Arguments.cellPhonePart3;
	  this.cellPhone=this.cellPhonePart1 & "-" & this.cellPhonePart2 & "-" & this.cellPhonePart3;
	  this.cellPhone=Replace(this.cellPhone,"--","");
	  this.paymentMethod=Arguments.paymentMethod;
	  if (Not CompareNoCase(this.paymentMethod,"Credit Card")) {
   		this.cardType=Arguments.cardType;
		this.nameOnCard=Arguments.nameOnCard;
		this.cardNumber=Arguments.cardNumber;
		this.cardExpirationDate=Arguments.cardExpirationDate;
		this.cardCVV2=Arguments.cardCVV2;
      } else if (Not CompareNoCase(this.paymentMethod,"Bank Account")) {
	    this.nameOnAccount=Arguments.nameOnAccount;
		this.accountNumber=Arguments.accountNumber;
		this.routingNumber=Arguments.routingNumber;
	  }
	  
	  this.billingFirstName=Arguments.billingFirstName;
	  this.billingLastName=Arguments.billingLastName;
	  this.billingAddress1=Arguments.billingAddress1;
	  this.billingAddress2=Arguments.billingAddress2;
	  this.billingCity=Arguments.billingCity;
	  this.billingState=Arguments.billingState;
	  this.billingZip=Arguments.billingZip;    
    </cfscript>
  </cffunction>
  
  
  <cffunction name="completeRegistration" access="public" output="no" returntype="struct">
    <cfargument name="signUpFormID" type="numeric" required="no"><!--- used for sending email sign-up invitation --->
    <cfargument name="donationFormID" type="numeric" default="0"><!--- used for adding donation record to donation submission table --->
    
    <!--- FOR ACH Check Payment
	UMcommand	 cc:sale, cc:authonly, check:sale

	UMrouting	 Check:Sale, Check:Credit	 Bank Routing number. Required when UMcommand is set to check or checkcredit.
	UMaccount	 Check:Sale, Check:Credit	 Checking Account Number. Required when UMcommand is set to check or checkcredit.
	UMaccounttype	 The type of account to debit/credit, either �Checking� or �Savings�.
	If omitted or left blank, it will default to �Checking� Only applies to the Check:Sale and Check:Credit commands.
	
	Test Check ACH Data
	The following list of test check information is made available for testing check processing functionality on our Sandbox system. The account data should not be used in production.
	Routing		Account	Amount		Response		Reason
	987654321	Any		Any			Error			Invalid routing Number
	Any			Any		5.99		Decline			Returned check for this account
	Any			Any		9999.99		ManagerApproval	Warning: You have exceeded your allocated monthly transaction volume
	* Any other combination of 9 digit routing number and account number will return an approval.
	--->
  
    <cfset var resultStruct=StructNew()>
    <cfset var idx = 0>
    <cfset var i = 0>
    <cfset var j = 0>
    <cfset resultStruct.success=true>
    <cfset resultStruct.creditCardApproved=false>    
    <cfset resultStruct.authCode="">
    <cfset resultStruct.message="">
    
    <cfset resultStruct.successDonation=true>
    <cfset resultStruct.creditCardApprovedDonation=false>
    <cfset resultStruct.authCodeDonation="">
    <cfset resultStruct.messageDonation="">
    
    <cfscript>
	  Variables.cartTotal=getCartTotal();
	  Variables.orderID=now().getTime();
	  Variables.clientIP=CGI.REMOTE_ADDR;
	  ePay=CreateObject("component", "com.USAePay").init();
	  if (CompareNoCase(this.paymentMethod,"Cash") And Variables.cartTotal GT 0) {//has balance and not pay with Cash	    
		//create line items array
		if ((Variables.cartTotal - this.donationAmount) GT 0) {//has registration fee to pay
		  lineItems=ArrayNew(1);
		  for (i=1; i LTE ArrayLen(this.penciledInClasses); i=i + 1) {
			if (this.penciledInClasses[i].cost GT 0) {
			  j = j + 1;
			  lineItems[j]=StructNew();
			  if (Compare(this.penciledInClasses[i].GLNumber,"")) {
				lineItems[j].SKU=this.penciledInClasses[i].GLNumber;
				lineItems[j].GLNumber=this.penciledInClasses[i].GLNumber;
			  } else {
				lineItems[j].SKU="Unspecified";
				lineItems[j].GLNumber="Unspecified";
			  }
			  lineItems[j].itemName=this.penciledInClasses[i].courseTitle;
			  lineItems[j].description=this.penciledInClasses[i].courseTitle;		  
			  
			  if (this.penciledInClasses[i].paymentPlanEligible And this.penciledInClasses[i].enrollPaymentPlan) {//cost more than $750 and enroll payment plan
				lineItems[j].cost=this.penciledInClasses[i].firstInstallment;
				if (this.penciledInClasses[i].paymentPlanType IS "A") {
				  lineItems[j].description=lineItems[j].description & " (3-Installment Payment Plan is selected. Remaining Balance: #DollarFormat(this.penciledInClasses[i].remainingPayment)#)";
				} else {
				  lineItems[j].description=lineItems[j].description & " (2-Installment Payment Plan is selected. Remaining Balance: #DollarFormat(this.penciledInClasses[i].remainingPayment)#)";
				}
			  } else {
				lineItems[j].cost=this.penciledInClasses[i].cost;
			  }
			  
			  if (this.penciledInClasses[i].cost GT 0 And this.penciledInClasses[i].discountApplied And this.penciledInClasses[i].discountAmount GT 0) {
				//price with discount
				lineItems[j].cost=lineItems[j].cost - this.penciledInClasses[i].discountAmount;
				lineItems[j].description=lineItems[j].description & " (Discount: #DollarFormat(this.penciledInClasses[i].discountAmount)# - #this.penciledInClasses[i].discountCode#)";
			  }  	
			}// cost > 0
	      }
		
		  for (i=1; i LTE ArrayLen(this.penciledInServices); i=i + 1) {
			if (this.penciledInServices[i].cost GT 0) {
			  j = j + 1;
			  lineItems[j]=StructNew();
			  if (Compare(this.penciledInServices[i].GLNumber,"")) {
				lineItems[j].SKU=this.penciledInServices[i].GLNumber;
				lineItems[j].GLNumber=this.penciledInServices[i].GLNumber;
			  } else {
				lineItems[j].SKU="Unspecified";
				lineItems[j].GLNumber="Unspecified";
			  }		  
			  lineItems[j].itemName=this.penciledInServices[i].serviceTitle;
			  lineItems[j].description=this.penciledInServices[i].serviceTitle;
			  lineItems[j].cost=this.penciledInServices[i].cost;
			}
		  }
		
		  /* donation will be considered as a separate transaction and will be submitted to a different USAePay account
		  if (this.donationAmount GT 0) {
		    j = j + 1;
		    lineItems[j]=StructNew();
		    lineItems[j].SKU="DONATION";
		    lineItems[j].GLNumber="DONATION";
		    lineItems[j].itemName="Donate to Woman's Hospital Foundation";
		    lineItems[j].description="Donate to Woman's Hospital Foundation";
		    lineItems[j].cost=this.donationAmount;
		  }
		  */	
		
		  if (Not CompareNoCase(this.paymentMethod,"Credit Card")) {
			Variables.expDateParts=ListToArray(this.cardExpirationDate,"/");
	    	Variables.expirationDate=Variables.expDateParts[1] & Right(Variables.expDateParts[2],2);
			gatewayReturnStruct=ePay.makePayment(
			  paymentMethod="#this.paymentMethod#",
			  command="cc:sale",
			  nameOnCard="#this.nameOnCard#",
			  cardNumber="#this.cardNumber#",
			  expirationDate="#Variables.expirationDate#",
			  CVV2="#this.cardCVV2#",
			  amount="#NumberFormat(Variables.cartTotal,'0.00')#",
			  address="#this.billingAddress1#",
			  zip="#this.billingZip#",
			  orderID="#Variables.orderID#",
			  clientIP="#Variables.clientIP#",
			  testMode="0",
			  contactPhone="#this.daytimePhone#",
			  billingFirstName="#this.billingFirstName#",
			  billingLastName="#this.billingLastName#",
			  billingAddress1="#this.billingAddress1#",
			  billingAddress2="#this.billingAddress2#",
			  billingCity="#this.billingCity#",
			  billingState="#this.billingState#",
			  billingZip="#this.billingZip#",
			  lineItems="#lineItems#",
			  splitPayments=1
			);
		  } else {//pay with Bank Account
			gatewayReturnStruct=ePay.makePayment(
			  paymentMethod="#this.paymentMethod#",
			  command="check:sale",		  
			  accountType="#this.accountType#",
			  nameOnAccount="#this.nameOnAccount#",
			  accountNumber="#this.accountNumber#",
			  routingNumber="#this.routingNumber#",			  
			  amount="#NumberFormat(Variables.cartTotal,'0.00')#",
			  address="#this.billingAddress1#",
			  zip="#this.billingZip#",
			  orderID="#Variables.orderID#",
			  clientIP="#Variables.clientIP#",
			  testMode="0",
			  contactPhone="#this.daytimePhone#",
			  billingFirstName="#this.billingFirstName#",
			  billingLastName="#this.billingLastName#",
			  billingAddress1="#this.billingAddress1#",
			  billingAddress2="#this.billingAddress2#",
			  billingCity="#this.billingCity#",
			  billingState="#this.billingState#",
			  billingZip="#this.billingZip#",
			  lineItems="#lineItems#",
			  splitPayments=1
			);	
		  }
		
		  if (gatewayReturnStruct.approved) {
		    resultStruct.creditCardApproved=true;
		    resultStruct.authCode=gatewayReturnStruct.authCode;		  
		  } else {
		    resultStruct.success=false;
		    resultStruct.creditCardApproved=false;
		    resultStruct.message=gatewayReturnStruct.error;
		  }	
		}
		
		//deal with donation here::
		if (this.donationAmount GT 0 And resultStruct.success) {
		  if (Not CompareNoCase(this.paymentMethod,"Credit Card")) {
			Variables.expDateParts=ListToArray(this.cardExpirationDate,"/");
	    	Variables.expirationDate=Variables.expDateParts[1] & Right(Variables.expDateParts[2],2);
			gatewayReturnStruct=ePay.makePayment(
			  paymentMethod="#this.paymentMethod#",
			  USAePayKey="#Application.USAePayKeyForDonation#",
			  command="cc:sale",
			  nameOnCard="#this.nameOnCard#",
			  cardNumber="#this.cardNumber#",
			  expirationDate="#Variables.expirationDate#",
			  CVV2="#this.cardCVV2#",
			  amount="#NumberFormat(this.donationAmount,'0.00')#",
			  address="#this.billingAddress1#",
			  zip="#this.billingZip#",
			  orderID="#Variables.orderID#",
			  description="Donate to Woman's Hospital Foundation",
			  clientIP="#Variables.clientIP#",
			  testMode="0",
			  contactPhone="#this.daytimePhone#",
			  billingFirstName="#this.billingFirstName#",
			  billingLastName="#this.billingLastName#",
			  billingAddress1="#this.billingAddress1#",
			  billingAddress2="#this.billingAddress2#",
			  billingCity="#this.billingCity#",
			  billingState="#this.billingState#",
			  billingZip="#this.billingZip#",
			  splitPayments=0
			);
		  } else {
			gatewayReturnStruct=ePay.makePayment(
			  paymentMethod="#this.paymentMethod#",
			  USAePayKey="#Application.USAePayKeyForDonation#",
			  command="check:sale",		  
			  accountType="#this.accountType#",
			  nameOnAccount="#this.nameOnAccount#",
			  accountNumber="#this.accountNumber#",
			  routingNumber="#this.routingNumber#",			  
			  amount="#NumberFormat(this.donationAmount,'0.00')#",
			  address="#this.billingAddress1#",
			  zip="#this.billingZip#",
			  orderID="#Variables.orderID#",
			  description="Donate to Woman's Hospital Foundation",
			  clientIP="#Variables.clientIP#",
			  testMode="0",
			  contactPhone="#this.daytimePhone#",
			  billingFirstName="#this.billingFirstName#",
			  billingLastName="#this.billingLastName#",
			  billingAddress1="#this.billingAddress1#",
			  billingAddress2="#this.billingAddress2#",
			  billingCity="#this.billingCity#",
			  billingState="#this.billingState#",
			  billingZip="#this.billingZip#",			  
			  splitPayments=0
			);		
		  }
		  
		  if (gatewayReturnStruct.approved) {
		    resultStruct.creditCardApprovedDonation=true;
		    resultStruct.authCodeDonation=gatewayReturnStruct.authCode;		  
		  } else {
		    resultStruct.successDonation=false;
		    resultStruct.creditCardApprovedDonation=false;
		    resultStruct.messageDonation=gatewayReturnStruct.error;
		  }		  
		}					
	  }//has balance and not pay with Cash
	</cfscript>
    
    <cfif resultStruct.success>
      <!--- card authorization is successfully if it was required --->
      <!--- now insert order information to database --->
      <cfquery datasource="#this.datasource#">
        INSERT INTO cl_ordersummary (
          orderID, studentID, orderDate, donationAmount, total,          
          contactFirstName, contactLastName,
          contactAddress1, contactAddress2, contactCity, contactState, contactZIP, daytimePhone, cellPhone,
          billingFirstName, billingLastName,
          billingAddress1, billingAddress2, billingCity, billingState, billingZIP, 
          payWithCash,
          authCode,
          dateLastModified)
        VALUES (
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#Variables.orderID#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#">,
          <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
          <cfqueryparam cfsqltype="cf_sql_float" value="#this.donationAmount#">,
          <cfqueryparam cfsqltype="cf_sql_float" value="#Variables.cartTotal#">,       
          
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactFirstName#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactLastName#">,
          
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactAddress1#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactAddress2#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactCity#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactState#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactZIP#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.daytimePhone#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.cellPhone#">,
          
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.billingFirstName#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.billingLastName#">,
          
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.billingAddress1#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.billingAddress2#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.billingCity#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.billingState#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.billingZIP#">,
          
          <cfqueryparam cfsqltype="cf_sql_tinyint" value="#this.payWithCash#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#resultStruct.authCode#">,
          <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
      </cfquery>
      
      <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClasses)#">
        <cfif this.penciledInClasses[idx].paymentPlanEligible And this.penciledInClasses[idx].enrollPaymentPlan>
          <cfset amountPaid=this.penciledInClasses[idx].firstInstallment>
        <cfelse>
          <cfset amountPaid=this.penciledInClasses[idx].cost>
        </cfif>
        
        <cfif this.penciledInClasses[idx].discountApplied AND this.penciledInClasses[idx].discountAmount GT 0>
          <cfset amountPaid=amountPaid - this.penciledInClasses[idx].discountAmount>
          <cfset netCost=this.penciledInClasses[idx].cost - this.penciledInClasses[idx].discountAmount>
        <cfelse>
          <cfset netCost=this.penciledInClasses[idx].cost>
		</cfif>
      
      
        <cfquery datasource="#this.datasource#">
          INSERT INTO cl_orderdetail (
            orderID, courseType, courseID, GLNumber, classID, courseTitle,
            cost, discountCode, discountAmount, paymentPlan, amountPaid)
          VALUES (
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#Variables.orderID#">,
            'R',
            <cfqueryparam cfsqltype="cf_sql_integer" value="#this.penciledInClasses[idx].courseID#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.penciledInClasses[idx].GLNumber#">,
            <cfqueryparam cfsqltype="cf_sql_integer" value="#this.penciledInClasses[idx].classID#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.penciledInClasses[idx].courseTitle#">,
            
            <cfqueryparam cfsqltype="cf_sql_float" value="#this.penciledInClasses[idx].cost#">,
            <cfif this.penciledInClasses[idx].discountApplied AND this.penciledInClasses[idx].discountAmount GT 0>
              <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.penciledInClasses[idx].discountCode#">,
              <cfqueryparam cfsqltype="cf_sql_float" value="#this.penciledInClasses[idx].discountAmount#">,
            <cfelse>
              '', 0,
            </cfif>
            <cfqueryparam cfsqltype="cf_sql_tinyint" value="#this.penciledInClasses[idx].enrollPaymentPlan#">,
            <cfqueryparam cfsqltype="cf_sql_float" value="#amountPaid#">
            )
        </cfquery>
        
        <cfquery datasource="#this.datasource#">
          INSERT INTO cl_student_class (
            studentID, classID, feePerCouple, attendeeName, guestNames, daytimePhone, cellPhone,
            address1, address2, city, state, zip,
            payWithCash, fee, discountCode, discountAmount, paymentPlan, amountPaid,  dateCreated)
          VALUES (
            <cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#">,
            <cfqueryparam cfsqltype="cf_sql_integer" value="#this.penciledInClasses[idx].classID#">,
            <cfqueryparam cfsqltype="cf_sql_tinyint" value="#this.penciledInClasses[idx].feePerCouple#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.penciledInClasses[idx].attendeeName#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.penciledInClasses[idx].guestNames#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.daytimePhone#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.cellPhone#">,
            
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactAddress1#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactAddress2#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactCity#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactState#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactZIP#">,
            
            <cfqueryparam cfsqltype="cf_sql_tinyint" value="#this.payWithCash#">,
            <cfqueryparam cfsqltype="cf_sql_float" value="#netCost#">,            
            <cfif this.penciledInClasses[idx].discountApplied AND this.penciledInClasses[idx].discountAmount GT 0>
              <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.penciledInClasses[idx].discountCode#">,
              <cfqueryparam cfsqltype="cf_sql_float" value="#this.penciledInClasses[idx].discountAmount#">,
            <cfelse>
              '', 0,
            </cfif>
            
            <cfqueryparam cfsqltype="cf_sql_tinyint" value="#this.penciledInClasses[idx].enrollPaymentPlan#">,
            <cfqueryparam cfsqltype="cf_sql_float" value="#amountPaid#">,
            
            <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
        </cfquery>           
      </cfloop>      
      
      <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInServices)#">
        <cfquery datasource="#this.datasource#">
          INSERT INTO cl_orderdetail (
            orderID, courseType, serviceID, serviceTitle, cost, amountPaid)
          VALUES (
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#Variables.orderID#">,
            'S',
            <cfqueryparam cfsqltype="cf_sql_integer" value="#this.penciledInServices[idx].serviceID#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.penciledInServices[idx].serviceTitle#">,            
            <cfqueryparam cfsqltype="cf_sql_float" value="#this.penciledInServices[idx].cost#">,
            <cfqueryparam cfsqltype="cf_sql_float" value="#this.penciledInServices[idx].cost#">)
        </cfquery>
        
        <cfquery datasource="#this.datasource#">
          INSERT INTO cl_student_service (
            studentID, serviceID, feePerCouple, attendeeName, guestNames, daytimePhone, cellPhone,
            address1, address2, city, state, zip,
            payWithCash, fee, amountPaid, dateCreated)
          VALUES (
            <cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#">,
            <cfqueryparam cfsqltype="cf_sql_integer" value="#this.penciledInServices[idx].serviceID#">,           
            <cfqueryparam cfsqltype="cf_sql_tinyint" value="#this.penciledInServices[idx].feePerCouple#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.penciledInServices[idx].attendeeName#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.penciledInServices[idx].guestNames#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.daytimePhone#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.cellPhone#">,
            
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactAddress1#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactAddress2#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactCity#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactState#">,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#this.contactZIP#">,
            
            <cfqueryparam cfsqltype="cf_sql_tinyint" value="#this.payWithCash#">,
            <cfqueryparam cfsqltype="cf_sql_float" value="#this.penciledInServices[idx].cost#">,
            <cfqueryparam cfsqltype="cf_sql_float" value="#this.penciledInServices[idx].cost#">,
            <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
        </cfquery>
      </cfloop>      

	  <!--- USED FOR FRONT-END RECEIPT FOR PRINTOUT --->
      <cfsavecontent variable="receiptHTMLForFrontEnd">      	  
          <div class="head">Your Class Registration Is Complete</div>
          
          <div class="intro">
          Thank you! You class registration is complete. A confirmation e-mail has been sent to <cfoutput>#this.email#</cfoutput>.
          </div>
        
          <cfoutput>
          <span class="section">Selected Classes</span>
          <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClasses)#">    
            <div class="itemizedContainer">
              <div class="itemizedClass">
                #this.penciledInClasses[idx].courseTitle#<br />
                <span class="date-range">
                  #DateFormat(this.penciledInClasses[idx].classInstance[1].startDateTime,"mm.dd.yy")# |
                  #LCase(TimeFormat(this.penciledInClasses[idx].classInstance[1].startDateTime,"h:mm tt"))# - #Lcase(TimeFormat(this.penciledInClasses[idx].classInstance[1].endDateTime,"h:mm tt"))#
                </span>
              </div>
              <div class="itemizedCost">
                <cfif this.penciledInClasses[idx].paymentPlanEligible>
				  <cfif this.penciledInClasses[idx].enrollPaymentPlan><!--- enroll payment plan --->
                    #DollarFormat(this.penciledInClasses[idx].firstInstallment)#<br />
                    <span class="accent01"><small>(Remaining Balance: #DollarFormat(this.penciledInClasses[idx].remainingPayment)#)</small></span>
                  <cfelse>
                    #DollarFormat(this.penciledInClasses[idx].cost)#*
                  </cfif>
                <cfelse>
                  #DollarFormat(this.penciledInClasses[idx].cost)#
                </cfif> 
              </div>
            </div>
            <cfif this.penciledInClasses[idx].discountApplied>
              <div class="itemizedContainer">
                <div class="itemizedClass">
                  Coupon Code - #this.penciledInClasses[idx].discountCode#
                </div>
                <div class="itemizedCost">-#DollarFormat(this.penciledInClasses[idx].discountAmount)#</div>
              </div>
            </cfif>  
          </cfloop>
          
          <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInServices)#">
            <div class="itemizedContainer">
              <div class="itemizedClass">#this.penciledInServices[idx].serviceTitle#</div>
              <div class="itemizedCost">#DollarFormat(this.penciledInServices[idx].cost)#</div>
            </div>
          </cfloop>
          <cfset donationAmount=getDonationAmount()>		  
          <cfif donationAmount GT 0>
            <div class="itemizedContainer">
              <div class="itemizedClass">Donate to Woman's Hospital Foundation</div>
              <div class="itemizedCost">#DollarFormat(donationAmount)#</div>
            </div>
          </cfif>
                    
          <div class="itemizedHdr">
            <div class="itemizedLabTotal">Total:</div>
            <div class="itemizedGrandTotal">#DollarFormat(getCartTotal())#</div>
          </div>
          
          <cfif hasPaymentPlanEligibleClass() And this.enrollPaymentPlan>
           <div class="itemizedHdr" style="margin:0px 0px 10px 0px;">
              <span class="accent03">
                * You have selected the Payment Plan.
                  A class that costs between $400 and $750 qualifies for the 2-Installment Payment Plan. The initial payment will be 1/2 the cost of the class plus $10.
            	  A class that costs $750 or more qualifies for the 3-Installment Payment Plan. The initial payment will be 1/3 the cost of the class plus $10.
              </span>
           </div>
          </cfif>
        
          <div class="floatLeftFullWdth">
            <div class="unit">
              <span class="section">Attendee Information</span>            
              <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInClasses)#">
                <div class="class-title">#this.penciledInClasses[idx].courseTitle#</div>
                <div class="singleField">
                  <label class="descrip">
                    <cfif Compare(this.penciledInClasses[idx].attendeeNameLabel,"")>#this.penciledInClasses[idx].attendeeNameLabel#<cfelse>Attendee Name</cfif>
                  </label>
                  <span class="formValue">#this.penciledInClasses[idx].attendeeName#</span>          
                </div>               
                <cfif this.penciledInClasses[idx].feePerCouple>            
                  <div class="singleField">
                    <label class="descrip">
                      <cfif Compare(this.penciledInClasses[idx].guestNameLabel,"")>#this.penciledInClasses[idx].guestNameLabel#<cfelse>Guest(s)</cfif>
                    </label>
                    <span class="formValue">#this.penciledInClasses[idx].guestNames#</span> 
                  </div>           
                </cfif>
              </cfloop>  
              <cfloop index="idx" from="1" to="#ArrayLen(this.penciledInServices)#">
                <div class="class-title">#this.penciledInServices[idx].serviceTitle#</div>
                <div class="singleField">
                  <label class="descrip">Attendee</label>
                  <span class="formValue">#this.penciledInServices[idx].attendeeName#</span>     
                </div>               
                <cfif this.penciledInServices[idx].feePerCouple>            
                  <div class="singleField">
                    <label class="descrip">Guest(s)</label>
                    <span class="formValue">#this.penciledInServices[idx].guestNames#</span> 
                  </div>           
                </cfif>
              </cfloop>
            </div>
                  
            <div class="unit">
              <span class="section">Contact Information</span>
              <div class="singleField">
                <label class="descrip">First Name </label>
                <span class="formValue">#this.contactFirstName#</span>
              </div>
              <div class="singleField">
                <label class="descrip">Last Name </label>
                <span class="formValue">#this.contactLastName#</span>
              </div>
              <div class="singleField">
                <label class="descrip">Address </label>
                <span class="formValue">
                  #this.contactAddress1#<br />
                  <cfif Compare(this.contactAddress2, "")>
                  #this.contactAddress2#<br />
                  </cfif>
                  #this.contactCity#, #this.contactState# #this.contactZip#
                </span>
              </div>
              <div class="singleField">
                <label class="descrip">Daytime Phone </label>
                <span class="formValue">#this.daytimePhone#</span>
              </div>
              <div class="singleField">
                <label class="descrip">Cell Phone </label>
                <span class="formValue">#this.cellPhone#&nbsp;</span>
              </div>
            </div>
            
            <cfif cartTotal GT 0>
              <div class="unit">
                <span class="section">Payment Information</span>        
                <cfif Not CompareNoCase(this.paymentMethod,"Credit Card")>
                  <div class="singleField">
                    <label for="creditCard" class="descrip">Payment Method</label>
                    <span class="formValue">Credit Card</span>
                  </div>
                  <div class="singleField">
                    <label for="creditCard" class="descrip">Card Type</label>
                    <span class="formValue">#this.cardType#</span>
                  </div>
                  <div class="singleField">
                    <label for="notifyNameFirst" class="descrip">Name as on Card</label>
                    <span class="formValue">#this.nameOnCard#</span>
                  </div>
                  <div class="singleField">
                    <label for="textfield5" class="descrip">Card Number</label>
                    <span class="formValue">************#Right(this.cardNumber,4)#</span>
                  </div>
                  <div class="singleField">
                    <label for="textfield6" class="descrip">Card Expiration Date</label>
                    <span class="formValue">#this.cardExpirationDate#</span>
                  </div>
                  <div class="singleField">
                    <label for="textfield7" class="descrip">Card ID <br />(CVV2/CID) Number</label>
                    <span class="formValue">...</span>          
                  </div>
                <cfelseif Not CompareNoCase(this.paymentMethod,"Bank Account")>
                  <div class="singleField">
                    <label for="creditCard" class="descrip">Payment Method</label>
                    <span class="formValue">Bank Check</span>
                  </div>
                  <div class="singleField">
                    <label for="notifyNameFirst" class="descrip">Name on Account</label>
                    <span class="formValue">#this.nameOnAccount#</span>
                  </div>
                  <div class="singleField">
                    <label for="textfield5" class="descrip">Checking Account Number</label>
                    <span class="formValue">************#Right(this.accountNumber,4)#</span>
                  </div>
                  <div class="singleField">
                    <label for="textfield6" class="descrip">Bank Routing Number</label>
                    <span class="formValue">************#Right(this.routingNumber,4)#</span>
                  </div>
                <cfelse>
                  <div class="singleField"><label for="anonymousYes4" class="descrip">Pay with check or cash in person.</label></div>
                </cfif>        
              </div>
              
              <div class="unit">
                <span class="section">Billing Information</span>        
                <div class="singleField">
                  <label for="billingNameFirst" class="descrip">First Name</label>
                  <span class="formValue">#this.billingFirstName#</span>
                </div>
                <div class="singleField">
                  <label for="billingNameLast" class="descrip">Last Name</label>
                  <span class="formValue">#this.billingLastName#</span>
                </div>
                <div class="singleField">
                  <label for="billingAddress" class="descrip">Address</label>
                  <span class="formValue">
                    #this.billingAddress1#<br />
                    <cfif Compare(this.billingAddress2, "")>
                    #this.billingAddress2#<br />
                    </cfif>
                    #this.billingCity#, #this.billingState# #this.billingZip#
                  </span>
                </div>
              </div>
            </cfif>      
          </div>
          </cfoutput>
      </cfsavecontent>
      <cfset this.receiptHTML=receiptHTMLForFrontEnd>
      

	  <!--- USED FOR NOTIFICATION/CONFIRMATION EMAILS --->      
      <cfsavecontent variable="registerClassesDetailText">
        <cfoutput>
<cfloop index="idx1" from="1" to="#ArrayLen(this.penciledInClasses)#">
#this.penciledInClasses[idx1].courseTitle#
<cfloop index="idx2" from="1" to="#this.penciledInClasses[idx1].numClassInstances#">
#DayOfWeekAsString(this.penciledInClasses[idx1].classInstance[idx2].dayOfWeekAsNumber)# #DateFormat(this.penciledInClasses[idx1].classInstance[idx2].startDateTime,"mm.dd.yy")# #LCase(TimeFormat(this.penciledInClasses[idx1].classInstance[idx2].startDateTime,"h:mm tt"))# - #LCase(TimeFormat(this.penciledInClasses[idx1].classInstance[idx2].endDateTime,"h:mm tt"))#
#this.penciledInClasses[idx1].classInstance[idx2].locationName#
<cfif this.penciledInClasses[idx1].paymentPlanEligible And this.penciledInClasses[idx1].enrollPaymentPlan>
#DollarFormat(this.penciledInClasses[idx1].firstInstallment - this.penciledInClasses[idx1].discountAmount)# (Remaining Balance: #DollarFormat(this.penciledInClasses[idx1].remainingPayment)#)*<cfelse>#DollarFormat(this.penciledInClasses[idx1].cost - this.penciledInClasses[idx1].discountAmount)#</cfif>
</cfloop>
</cfloop>

<cfloop index="idx1" from="1" to="#ArrayLen(this.penciledInServices)#">
#this.penciledInServices[idx1].serviceTitle# - #DollarFormat(this.penciledInServices[idx1].cost)#
</cfloop><cfif this.enrollPaymentPlan>

* You have selected the Payment Plan 
</cfif>
        </cfoutput>
      </cfsavecontent>
      
      <cfsavecontent variable="registerClassesDetailHTML">
        <cfoutput>
        <cfloop index="idx1" from="1" to="#ArrayLen(this.penciledInClasses)#">
          <span style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;color:##627d79;font-size:14px;">
            #this.penciledInClasses[idx1].courseTitle# -            
            <cfif this.penciledInClasses[idx1].paymentPlanEligible And this.penciledInClasses[idx1].enrollPaymentPlan>
              #DollarFormat(this.penciledInClasses[idx1].firstInstallment - this.penciledInClasses[idx1].discountAmount)#
              (Remaining Balance: #DollarFormat(this.penciledInClasses[idx1].remainingPayment)#)*
            <cfelseif this.penciledInClasses[idx1].cost GT 0>
              #DollarFormat(this.penciledInClasses[idx1].cost - this.penciledInClasses[idx1].discountAmount)#
            <cfelse>
              No Cost
            </cfif> 
          </span><br />
          <cfloop index="idx2" from="1" to="#this.penciledInClasses[idx1].numClassInstances#">
            <span style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:11px;color:##333132;line-height:130%;">
              <b>
              #DayOfWeekAsString(this.penciledInClasses[idx1].classInstance[idx2].dayOfWeekAsNumber)#
              #DateFormat(this.penciledInClasses[idx1].classInstance[idx2].startDateTime,"mm.dd.yy")# |</b>
              #LCase(TimeFormat(this.penciledInClasses[idx1].classInstance[idx2].startDateTime,"h:mm tt"))# -
              #LCase(TimeFormat(this.penciledInClasses[idx1].classInstance[idx2].endDateTime,"h:mm tt"))#
              <cfif Compare(this.penciledInClasses[idx1].classInstance[idx2].locationName,"")>| #this.penciledInClasses[idx1].classInstance[idx2].locationName#</cfif><br />
            </span>
          </cfloop>
          <span style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:11px;color:##333132;line-height:130%;">
          <cfif Compare(this.penciledInClasses[idx1].attendeeNameLabel,"")>#this.penciledInClasses[idx1].attendeeNameLabel#:<cfelse>Attendee:</cfif>
          #this.penciledInClasses[idx1].attendeeName#
          <cfif this.penciledInClasses[idx1].feePerCouple> |
            <cfif Compare(this.penciledInClasses[idx1].guestNameLabel,"")>#this.penciledInClasses[idx1].guestNameLabel#:<cfelse>Guest(s):</cfif>
            #this.penciledInClasses[idx1].guestNames#
          </cfif>
          <br />
          </span><br />          
        </cfloop>  
              
        <cfloop index="idx1" from="1" to="#ArrayLen(this.penciledInServices)#">
          <span style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;color:##627d79;font-size:14px;">
            #this.penciledInServices[idx1].serviceTitle# - 
            <cfif this.penciledInServices[idx1].cost GT 0>
              #DollarFormat(this.penciledInServices[idx1].cost)#
            <cfelse>
              No Cost
            </cfif>
          <br />
          </span>
          <span style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:11px;color:##333132;line-height:130%;">
          Attendee: #this.penciledInServices[idx1].attendeeName#<br /><br />  
          </span>
        </cfloop>        
        
        <cfif this.enrollPaymentPlan>
          <span style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:11px;color:##333132;line-height:130%;">
          * You have selected the Payment Plan<br /><br />
          </span>
        </cfif>               
        </cfoutput>
      </cfsavecontent>
      
      <!--- send notification email --->
      <cfquery name="qEmailSetting" datasource="#this.datasource#">
        SELECT *
        FROM cl_emailsettings
        WHERE emailCode='order_notification'
      </cfquery>
      
      <cfset finalEmailBodyText=ReplaceNocase(ReplaceNoCase(qEmailSetting.emailBody,"$orderid","#Variables.orderID#","All"), "$classes", "#registerClassesDetailText#", "All")>
      <cfset finalEmailBodyHTML=ReplaceNoCase(Replace(ReplaceNoCase(qEmailSetting.emailBody,"$orderid","#Variables.orderID#","All"), Chr(10), "<br />", "All"), "$classes", "#registerClassesDetailHTML#", "All")>
      <cfif Compare(qEmailSetting.replyName,"")>
        <cfset fromAddress="#qEmailSetting.replyName#<#qEmailSetting.replyAddress#>">
      <cfelse>
        <cfset fromAddress="#qEmailSetting.replyAddress#">
      </cfif>
      
      <cfmail from="#fromAddress#" to="#qEmailSetting.recipientAddresses#" subject="#qEmailSetting.emailSubject#" type="html">
        <cfmailpart type="text">
#finalEmailBodyText#
        </cfmailpart>        
        <cfmailpart type="html" charset="utf-8">          
          <a href="#this.siteURLRoot#"><img src="#this.siteURLRoot#/images/imgLogoEmailNot.gif" alt="Woman's - exceptional care, centered on you" width="96" height="84" border="0" /></a><br /><br />
          <div style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;font-size:19px;color:##eb44a7;line-height:110%;">
			#finalEmailBodyHTML#            
            <br />
            <hr size="1" noshade="noshade" />
            <a href="#this.siteURLRoot#" style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;font-size:19px;color:##627d79;line-height:110%;text-decoration:none;"><b>WWW.WOMANS.ORG</b></a> 
          </div>
        </cfmailpart>
      </cfmail>
            
      <!--- send confirmation email --->
      <cfquery name="qEmailSetting" datasource="#this.datasource#">
        SELECT *
        FROM cl_emailsettings
        WHERE emailCode='order_confirmation'
      </cfquery>
      
      <cfset finalEmailBodyText=ReplaceNoCase(ReplaceNoCase(qEmailSetting.emailBody,"$orderid","#Variables.orderID#","All"), "$name", "#this.contactFirstName#", "All")>
      <cfset finalEmailBodyHTML=ReplaceNoCase(ReplaceNoCase(qEmailSetting.emailBody,"$orderid","#Variables.orderID#","All"), "$name", "#this.contactFirstName#", "All")>
      
      <cfset finalEmailBodyText=ReplaceNoCase(finalEmailBodyText, "$classes", "#registerClassesDetailText#", "All")>
      <cfset finalEmailBodyHTML=ReplaceNoCase(Replace(finalEmailBodyHTML, Chr(10), "<br />", "All"), "$classes", "#registerClassesDetailHTML#", "All")>
      
      <cfif IsDefined("Arguments.signUpFormID")>
        <cfset signUpFormLink="#this.siteURLRoot#/r/?#URLEncodedFormat(Arguments.signUpFormID)#&#URLEncodedFormat(this.email)#">        
        
        <cfset finalEmailBodyText=ReplaceNoCase(finalEmailBodyText, "$signup", "#signUpFormLink#", "All")>
        <cfset signUpFormLink='<a href="#signUpFormLink#">#signUpFormLink#</a>'>
        <cfset finalEmailBodyHTML=ReplaceNoCase(finalEmailBodyHTML, "$signup", "#signUpFormLink#", "All")>
      </cfif>
      
      <cfif Compare(qEmailSetting.replyName,"")>
        <cfset fromAddress="#qEmailSetting.replyName#<#qEmailSetting.replyAddress#>">
      <cfelse>
        <cfset fromAddress="#qEmailSetting.replyAddress#">
      </cfif>
      
      <cfif IsValid("email", this.email)>
        <cfmail from="#fromAddress#" to="#this.email#" subject="#qEmailSetting.emailSubject#" type="html">
          <cfmailpart type="text">
#Trim(finalEmailBodyText)#
          </cfmailpart>        
          <cfmailpart type="html" charset="utf-8">
          <div style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;font-size:19px;color:##eb44a7;line-height:110%;">
            <a href="#this.siteURLRoot#"><img src="#this.siteURLRoot#/images/imgLogoEmailNot.gif" alt="Woman's - exceptional care, centered on you" width="96" height="84" border="0" /></a>
            <br /><br />          
			#finalEmailBodyHTML#            
            <br />
            <hr size="1" noshade="noshade" />
            <a href="#this.siteURLRoot#" style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;font-size:19px;color:##627d79;line-height:110%;text-decoration:none;"><b>WWW.WOMANS.ORG</b></a> 
          </div>
          </cfmailpart>
        </cfmail>
      </cfif>
      
      <!--- add donation info to donation submission table --->
	  <cfscript>
        if (CompareNoCase(this.paymentMethod,"Cash") And Variables.cartTotal GT 0 And this.donationAmount GT 0 And resultStruct.creditCardApprovedDonation) {
          argStruct=StructNew();
          CF=CreateObject("component","CustomForm").init();
          argStruct.authCode=resultStruct.authCodeDonation;
          argStruct.orderID=Variables.orderID;
          argStruct.clientIP=CGI.REMOTE_ADDR;
          argStruct.formID=Arguments.donationFormID;
          argStruct.sourceCode="CR";
          argStruct.firstName=this.contactFirstName;
          argStruct.lastName=this.contactLastName;
          argStruct.address1=this.contactAddress1;
          argStruct.address2=this.contactAddress1;
          argStruct.city=this.contactCity;
          argStruct.state=this.contactState;
          argStruct.zip=this.contactZip;
          argStruct.phonePart1=this.daytimePhonePart1;
          argStruct.phonePart2=this.daytimePhonePart2;
          argStruct.phonePart3=this.daytimePhonePart3;
          argStruct.emailAddress=this.email;
          argStruct.donationAmount=this.donationAmount;
              
          argStruct.creditCardType="";
          argStruct.nameOnCard="";
          argStruct.cardNumber="";
          argStruct.cardExpirationDate="";
          argStruct.cardCVV="";
          if (this.paymentMethod Is "Credit Card") {
            argStruct.creditCardType=this.cardType;
            argStruct.nameOnCard=this.nameOnCard;
            argStruct.cardNumber=this.cardNumber;
            argStruct.cardExpirationDate=this.cardExpirationDate;
            argStruct.cardCVV=this.cardCVV2;
          }
          argStruct.billingAddress1=this.billingAddress1;
          argStruct.billingAddress2=this.billingAddress2;
          argStruct.billingCity=this.billingCity;
          argStruct.billingState=this.billingState;
          argStruct.billingZip=this.billingZip;	  
    
          CF.submitDonationForm(argumentCollection=argStruct);
        }
      </cfscript>          
    </cfif><!--- resultStruct.success --->   
    
    <cfreturn resultStruct>
  </cffunction>
  
  
  <cffunction name="changeClassSchedule" access="public" output="no" returntype="boolean">
    <cfargument name="oldClassID" type="numeric" required="yes">
    <cfargument name="classID" type="numeric" required="yes">
    <cfargument name="studentClassID" type="numeric" required="yes">
    
    <!--- making sure oldClassID and classID has the same courseID --->
    <cfquery name="qCourseID" datasource="#this.datasource#">
      SELECT DISTINCT courseID
      FROM cl_class
      WHERE classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.oldClassID#"> OR
      		classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
    </cfquery>
    
    <cfif qCourseID.recordcount Is 1>
      <!--- lots of security checking here to prevent hacking --->
      <cfquery datasource="#this.datasource#">
        UPDATE cl_student_class
        SET classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
        WHERE studentClassID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.studentClassID#"> AND
        	  studentID=<cfqueryparam cfsqltype="cf_sql_integer" value="#this.studentID#"> AND
        	  classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.oldClassID#">
      </cfquery>
      <cfreturn true>
    <cfelse>
      <cfreturn false>
    </cfif>
  </cffunction>
  
  <cffunction name="register" access="public" output="no" returntype="boolean">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="password" type="string" required="yes">
    <cfargument name="studentType" type="string" default="R">
    <cfargument name="fitnessClubMemberID" type="string" default="">
    
    <cfif Compare(Arguments.fitnessClubMemberID,"") And IsNumeric(Arguments.fitnessClubMemberID)>
      <cfset Variables.fitnessClubMemberID=Arguments.fitnessClubMemberID>
    <cfelse>
      <cfset Variables.fitnessClubMemberID=0>
    </cfif>
    
    <cfquery name="qEmail" datasource="#this.datasource#">
      SELECT studentID
      FROM cl_student
      WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">
    </cfquery>
  
    <cfif qEmail.recordcount GT 0>
      <cfreturn false>
    <cfelse>
      <cfquery datasource="#this.datasource#">
        INSERT INTO cl_student (email, password, studentType, fitnessClubMemberID, dateCreated, dateLastModified)
        VALUES (
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">,
          <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.password#">,
          <cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.studentType#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.fitnessClubMemberID#">,
          <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
          <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)      
      </cfquery>
      
      <cfquery name="qMaxID" datasource="#this.datasource#">
        SELECT MAX(studentID) AS studentID
        FROM cl_student
      </cfquery>
      
      <cfset this.studentID=qMaxID.studentID>
      <cfset this.isLoggedIn=1>     
      <cfset this.email=Arguments.email>
      <cfset this.studentType=Arguments.studentType>
      
      <cfquery name="qEmailSetting" datasource="#this.datasource#">
        SELECT *
        FROM cl_emailsettings
        WHERE emailCode='register'
      </cfquery>      
      
      <cfif qEmailSetting.recordcount GT 0>
        <cfset finalEmailBodyText=ReplaceNoCase(qEmailSetting.emailBody, "$username", "#Arguments.email#", "All")>
        <cfset finalEmailBodyText=ReplaceNoCase(finalEmailBodyText, "$password", "#Arguments.password#", "All")>
        
        <cfset finalEmailBodyHTML=ReplaceNoCase(Replace(qEmailSetting.emailBody,Chr(10),"<br />","All"), "$username", "#Arguments.email#", "All")>
        <cfset finalEmailBodyHTML=ReplaceNoCase(finalEmailBodyHTML, "$password", "#Arguments.password#", "All")>     
        
        
        <cfif Compare(qEmailSetting.replyName,"")>
          <cfset fromAddress="#qEmailSetting.replyName#<#qEmailSetting.replyAddress#>">
        <cfelse>
          <cfset fromAddress="#qEmailSetting.replyAddress#">
        </cfif>
        
		<cfif IsValid("email", Arguments.email)>
          <cfmail from="#fromAddress#" to="#Arguments.email#" subject="#qEmailSetting.emailSubject#" type="html">
            <cfmailpart type="text" charset="utf-8">
#finalEmailBodyText#
			</cfmailpart>
            <cfmailpart type="html" charset="utf-8">
              <a href="#this.siteURLRoot#"><img src="#this.siteURLRoot#/images/imgLogoEmailNot.gif" alt="Woman's - exceptional care, centered on you" width="96" height="84" border="0" /></a>
              <br /><br /><br /> 
              <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:12px;color:##333132;line-height:130%;">
              #finalEmailBodyHTML#
              </div>
              <br />
              <hr size="1" noshade="noshade" />
              <a href="#this.siteURLRoot#" style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;font-size:19px;color:##627d79;line-height:110%;text-decoration:none;"><b>WWW.WOMANS.ORG</b></a>
            </cfmailpart>
          </cfmail>
		</cfif>		       
      </cfif>
      
      <!--- adjust price for Penciled-In Classes --->
	  <cfset resetPenciledInClassesPricing()>
      <!--- adjust price for Penciled-In Services --->
      <cfset resetPenciledInServicesPricing()>
      
      <cfreturn true>
    </cfif>
  </cffunction>
  
  <cffunction name="retrievePassword" access="public" output="no" returntype="boolean">
    <cfargument name="email" type="string" required="yes">
    
    <cfif Not IsValid("email", Arguments.email)><cfreturn false></cfif>
    
    <cfquery name="qPassword" datasource="#this.datasource#">
      SELECT *
      FROM cl_student
      WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">
    </cfquery>
    
    <cfif qPassword.recordcount GT 0>
      <cfquery name="qEmailSetting" datasource="#this.datasource#">
        SELECT *
        FROM cl_emailsettings
        WHERE emailCode='forgot_password'
      </cfquery>
      
      <cfset finalEmailBody=ReplaceNoCase(qEmailSetting.emailBody, "$password", "#qPassword.password#", "All")>        
      
      <cfif Compare(qEmailSetting.replyName,"")>
        <cfset fromAddress="#qEmailSetting.replyName#<#qEmailSetting.replyAddress#>">
      <cfelse>
        <cfset fromAddress="#qEmailSetting.replyAddress#">
      </cfif>
      
      <cfmail from="#fromAddress#" to="#Arguments.email#" subject="#qEmailSetting.emailSubject#">
#finalEmailBody#  
      </cfmail>    
      <cfreturn true>
    <cfelse>
  	  <cfreturn false>
    </cfif>
  </cffunction>
</cfcomponent>