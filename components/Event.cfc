<cfcomponent displayname="Calendar" extends="Base" output="false">
  <cffunction name="getCategory" access="public" output="no" returntype="query">
    <cfargument name="categoryID" required="yes" type="numeric">	
	<cfquery name="qCategory" datasource="#this.datasource#">
	  SELECT *
      FROM em_category
      WHERE categoryID=<cfqueryparam value="#Arguments.categoryID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
	<cfreturn qCategory>
  </cffunction>
  
  <cffunction name="getCategoryByID" access="public" output="no" returntype="query">
    <cfargument name="categoryID" required="yes" type="numeric">	
	<cfquery name="qCategory" datasource="#this.datasource#">
	  SELECT *
      FROM em_category
      WHERE categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#">
	</cfquery>
	<cfreturn qCategory>
  </cffunction>
  
  <cffunction name="getCategoryByEventID" access="public" output="no" returntype="query">
    <cfargument name="eventID" required="yes" type="numeric">	
	<cfquery name="qCategory" datasource="#this.datasource#">
	  SELECT em_category.categoryID, em_category.categoryName
      FROM em_event_category INNER JOIN em_category ON em_event_category.categoryID=em_category.categoryID
      WHERE em_event_category.eventID=<cfqueryparam value="#Arguments.eventID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
	<cfreturn qCategory>
  </cffunction>
  
  <cffunction name="getEventsByDateRange" access="public" output="false" returntype="query">
    <cfargument name="categoryID" type="numeric" required="yes">
    <cfargument name="fromDate" type="date" required="yes">
    <cfargument name="toDate" type="date" required="yes">
    
    <cfquery name="qEvents" datasource="#this.datasource#">
	  SELECT em_event.eventID, em_event.eventName,
      		 em_event.allDayEvent, em_event.startDateTime, em_event.endDateTime,
             em_event.eventLocation, em_event.eventURL, em_event.shortDescription    		  
	  FROM   em_event INNER JOIN em_event_category ON em_event.eventID=em_event_category.eventID
	  WHERE  em_event_category.categoryID=<cfqueryparam value="#Arguments.categoryID#" cfsqltype="cf_sql_integer"> AND
      		 em_event.published=1 AND
             em_event.startDateTime >= <cfqueryparam cfsqltype="cf_sql_date" value="#Arguments.fromDate#"> AND
             em_event.startDateTime < <cfqueryparam cfsqltype="cf_sql_date" value="#Arguments.toDate#">
	  ORDER  BY startDateTime
	</cfquery>
    
    <cfreturn qEvents>
  </cffunction>
  
  <cffunction name="getUpcomingEventsByCategoryID" access="public" output="false" returntype="struct">
    <cfargument name="categoryID" type="numeric" required="yes">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.events=QueryNew("eventID")>	
    
    <cfset currentDateTime=now()>
    <cfset currentDate=CreateDate(Year(currentDateTime),Month(currentDateTime),1)>
    	
	<cfquery name="qAllEventIDs" datasource="#this.datasource#">
	  SELECT DISTINCT em_event.eventID
	  FROM	 em_event INNER JOIN em_event_category ON
	  		 em_event.eventID=em_event_category.eventID
	  WHERE	 <cfif Compare(Arguments.categoryID,"0")>
      		 em_event_category.categoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.categoryID#"> AND
             </cfif>
             em_event.published=1 AND
             em_event.startDateTime >= <cfqueryparam cfsqltype="cf_sql_date" value="#currentDateTime#">
    </cfquery>
	<cfset resultStruct.numAllItems = qAllEventIDs.recordcount>
	
	<cfquery name="qEvents" datasource="#this.datasource#">
	  SELECT eventID, eventName,
      		 allDayEvent, startDateTime, endDateTime,
             eventLocation, eventURL, shortDescription    		  
	  FROM   em_event 
	  WHERE  <cfif qAllEventIDs.recordcount GT 0>
      		 eventID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ValueList(qAllEventIDs.eventID)#" list="yes">)
      		 <cfelse>
             1=-1
             </cfif>
	  ORDER  BY startDateTime
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
    <cfif qEvents.recordcount GT 0>            
      <cfset resultStruct.numDisplayedItems=qEvents.recordcount>	
      <cfset resultStruct.events = qEvents>
	<cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.events = QueryNew("eventID")>
    </cfif>
    
	<cfreturn resultStruct>
  </cffunction>  
  
  <cffunction name="getEventByID" access="public" output="no" returntype="query">
    <cfargument name="eventID" type="numeric" required="yes">
	<cfquery name="qEvent" datasource="#this.datasource#">
	  SELECT *
	  FROM em_event
      WHERE eventID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.eventID#"> AND
			published=1
	</cfquery>
    
	<cfreturn qEvent>	
  </cffunction>
 
  


</cfcomponent>