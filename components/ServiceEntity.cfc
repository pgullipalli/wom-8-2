<cfcomponent displayname="ServiceEntity" output="no">
  <cfif IsDefined("Application.datasource")>
    <cfset this.datasource=Application.datasource>
  </cfif>
  
  <cfset this.serviceID=0>
  <cfset this.GLNumber="">
  <cfset this.serviceTitle="">
  <cfset this.cost=0>
  <cfset this.locationID=0>
  <cfset this.locationName="">
  <cfset this.mapID=0>
  <cfset this.mapVerified=0><!--- map verification flag --->
  <cfset this.feedbackSent=0><!--- only used when this entity is owned by a user-specific --->
  <cfset this.feePerCouple=0><!--- place holder only --->
  <cfset this.attendeeNameLabel=""><!--- place holder only --->
  <cfset this.guestNameLabel=""><!--- place holder only --->
  <cfset this.attendeeName=""><!--- only used when this entity is owned by a user-specific --->
  <cfset this.guestNames=""><!--- only used when this entity is owned by a user-specific --->
  
  <cffunction name="init" access="public" output="no" returntype="ClassEntity">
    <cfargument name="serviceID" type="numeric" required="yes">
    <cfargument name="GLNumber" type="string" required="yes">
    <cfargument name="serviceTitle" type="string" required="yes">
    <cfargument name="cost" type="numeric" required="yes">
    <cfargument name="locationID" type="numeric" required="yes">
    <cfargument name="locationName" type="string" required="yes">
    <cfargument name="mapID" type="numeric" required="yes">
    <cfargument name="mapVerified" type="boolean" default="0">

    <cfset this.serviceID=Arguments.serviceID>
    <cfset this.GLNumber=Arguments.GLNumber>
    <cfset this.serviceTitle=Arguments.serviceTitle>
    <cfset this.cost=Arguments.cost>
    <cfset this.locationID=Arguments.locationID>
    <cfset this.locationName=Arguments.locationName>
    <cfset this.mapID=Arguments.mapID>
    <cfset this.mapVerified=Arguments.mapVerified>
    
    <cfreturn this>
  </cffunction>  
  
  <cffunction name="getServiceEntityByID" access="public" output="no" returntype="ServiceEntity">
    <cfargument name="serviceID" type="numeric" required="yes">
    <cfargument name="studentType" type="string" default="R"><!--- enum('R','D','E','F') --->
    
    <cfset var idx = 0>
    <cfset var argStruct=StructNew()>
    
    <cfquery name="qService" datasource="#this.datasource#">
      SELECT cl_service.serviceID, cl_service.GLNumber, cl_service.serviceTitle,
      		 cl_service.feeRegular, cl_service.feeEmployee, cl_service.feeDoctor, cl_service.feeFitnessClub,
             cl_service.locationID, cl_location.locationName, cl_location.mapID, cl_location.posX AS mapVerified             
      FROM cl_service LEFT JOIN cl_location ON
      	   cl_service.locationID=cl_location.locationID      		
      WHERE serviceID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.serviceID#">
    </cfquery>
    
    <cfset this.serviceID=Arguments.serviceID>
    <cfset this.GLNumber=qService.GLNumber>
    <cfset this.serviceTitle=qService.serviceTitle>
    <cfset this.locationID=qService.locationID>
    <cfset this.locationName=qService.locationName>
    <cfif IsNumeric(qService.mapID)>
      <cfset this.mapID=qService.mapID>
    <cfelse>
      <cfset this.mapID=0>
    </cfif>
    <cfif IsNumeric(qService.mapVerified)><cfset this.mapVerified=qService.mapVerified></cfif>
 	<cfswitch expression="#Arguments.studentType#">
      <cfcase value="R"><cfset this.cost=qService.feeRegular></cfcase>
      <cfcase value="D"><cfset this.cost=qService.feeDoctor></cfcase>
      <cfcase value="E"><cfset this.cost=qService.feeEmployee></cfcase>
      <cfcase value="F"><cfset this.cost=qService.feeFitnessClub></cfcase>
    </cfswitch>
    
    <cfreturn this>
  </cffunction>
</cfcomponent>