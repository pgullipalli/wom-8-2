<cfcomponent displayname="Utility" extends="Base" output="false">
  <cffunction name="getCategoriesByModuleName" access="public" returntype="query" output="false" displayname="getCategoriesByModuleName">
	<cfargument name="moduleName" type="string" required="true">
	
	<cfif IsDefined("Application.moduleCategoryInfoSQL.#Arguments.moduleName#")>
	  <cfquery name="qCatInfo" datasource="#this.datasource#">
		#Application.moduleCategoryInfoSQL["#Arguments.moduleName#"]#
	  </cfquery>
	  <cfreturn qCatInfo>
	<cfelse>
	  <cfreturn QueryNew("catID,catName")>
	</cfif>
  </cffunction>
  
  <cffunction name="createThumbnail" access="public" output="no" returntype="string">
    <cfargument name="absoluteFilePath" type="string" required="yes">
	<cfargument name="thumbnailWidth" type="numeric" required="yes">
    
	<cfset imageFullPath  = "#Arguments.absoluteFilePath#">
	<cfset fileName=GetFileFromPath(Arguments.absoluteFilePath)>
	<cfset thumbnailFileName = getThumbnailFileName(fileName)>
	<cfset thumbnailFullPath = "#GetDirectoryFromPath(Arguments.absoluteFilePath)##thumbnailFileName#">
    
    <cfif this.isColdfusion8>
      <cfif directoryExists("#this.siteDirectoryRoot#/assets_temp/")>
        <!--- this CF server has unpatched CFIMAGE tag; need special treatment --->
        <cfset fileNameTemp=Minute(now()) & Second(now()) & "_" & fileName>
        <cfset tempDir="#this.siteDirectoryRoot#/assets_temp/">
        <cfset imageFullPathTemp=tempDir & fileNameTemp>
        
        <cfdirectory action="list" directory="#tempDir#" name="allFiles" type="file">
        <cftry><cfloop query="allFiles"><cffile action="delete" file="#tempDir##name#"></cfloop><cfcatch></cfcatch></cftry>
        <cffile action="copy" source="#imageFullPath#" destination="#imageFullPathTemp#" nameconflict="overwrite">
        <cfimage action="resize" width="#Arguments.thumbnailWidth#" height="" source="#imageFullPathTemp#" destination="#thumbnailFullPath#" overwrite="yes">
      <cfelse>
        <cfimage action="resize" width="#Arguments.thumbnailWidth#" height="" source="#imageFullPathTemp#" destination="#thumbnailFullPath#" overwrite="yes">
      </cfif>      
    <cfelse>
	  <cfif FileExists(imageFullPath) AND NOT FileExists(thumbnailFullPath)>
	    <cfobject name="obj_inst" component="ImageUtility">
	    <cfinvoke component="#obj_inst#" method="load_image" in_file="#imageFullPath#">
	    <cfinvoke component="#obj_inst#" method="thumb_nail" edgeLength="#Arguments.thumbnailWidth#">
	    <cfinvoke component="#obj_inst#" method="output_image" out_file="#thumbnailFullPath#">			  		  
	  </cfif> 
    </cfif>
	<cfreturn Replace(thumbnailFullPath, "#this.siteDirectoryRoot#/", "")>
  </cffunction>
  
  <cffunction name="getThumbnailFileName" access="public" output="no" returntype="string">
    <cfargument name="fileName" type="string" required="yes">
	<cfset startIdx = Len(Arguments.fileName) - 4>
	<cfif startIdx LT 1><cfreturn "th_" & Arguments.fileName></cfif>
	<cfset dotPos=Find(".", Arguments.fileName, startIdx)>
	<cfreturn MID(Arguments.fileName, 1, dotPos-1) & "_th" & MID(Arguments.fileName, dotPos, Len(Arguments.fileName) - dotPos + 1)>  
  </cffunction>
  
  <cffunction name="getFileSize" access="public" output="no" returntype="string">
	<cfargument name="filePath" required="true">
	<cfset pathName=GetDirectoryFromPath(Arguments.filePath)>
	<cfset fileName=GetFileFromPath(Arguments.filePath)>
	<cflock name="Clock" type="ReadOnly" timeout="3">
	  <cfdirectory action="List"
   	    directory="#pathName#"
        name="fileInfo"
        filter="#fileName#"
	    sort="Type Asc, Name Asc">
	</cflock>
	<cfif IsDefined("fileInfo") AND fileInfo.recordcount is "1">
	  <cfreturn fileInfo.size>
	</cfif>
    <cfreturn "0">
  </cffunction>
  
  <cffunction name="convertStringToDateTimeObject" access="public" output="no" returntype="any">
    <cfargument name="date" type="string">
	<cfargument name="hours" type="string">
	<cfargument name="minutes" type="string">
	<cfargument name="seconds" type="string">
	<cfargument name="ampm" type="string">
	
    <cftry>
		<cfset dateParts=ArrayNew(1)>
        <cfset dateParts=ListToArray(Arguments.date, "/")>
        <cfset mm=dateParts[1]>
        <cfset dd=dateParts[2]>
        <cfset yy=dateParts[3]>
        <cfset hh=Arguments.hours>
        <cfset nn=Arguments.minutes>
        <cfset ss=Arguments.seconds>
        <cfif hh IS "">
          <cfset hh = 0>
        </cfif>
        <cfif nn IS "">
          <cfset nn = 0>
        </cfif>
        <cfif ss IS "">
          <cfset ss = 0>
        </cfif>
        <cfif Arguments.ampm IS "">
          <cfset Arguments.ampm = "am">
        </cfif>
    
        <cfif NOT CompareNoCase(Arguments.ampm, "am") AND NOT Compare(hh, "12")>
          <cfset hh = "0">
        <cfelseif NOT CompareNoCase(Arguments.ampm, "pm") AND Compare(hh, "12")>
          <cfset hh = hh + 12>
        </cfif>
        <cfreturn CreateDateTime(yy, mm, dd, hh, nn, ss)> 
    <cfcatch>
    	<cfreturn CreateDateTime(3000,1,1,0,0,0)>
    </cfcatch>
    </cftry> 
  </cffunction>
  
  <cffunction name="convertDateTimeObjectToString" access="public" output="no" returntype="struct">
    <cfargument name="dateTimeObj" required="yes">
	
	<cfset var resultStruct = StructNew()>
	<cfset resultStruct.date = DateFormat(Arguments.dateTimeObj, "mm/dd/yyyy")>
	
	<cfset time = TimeFormat(Arguments.dateTimeObj, "hh:mm tt")>
	<cfset resultStruct.hh = MID(time, 1, 2)>
    <cfif Left(resultStruct.hh,1) Is "0"><cfset resultStruct.hh=Right(resultStruct.hh,1)></cfif>
	<cfset resultStruct.mm = MID(time, 4, 2)>
    <cfif Left(resultStruct.mm,1) Is "0"><cfset resultStruct.mm=Right(resultStruct.mm,1)></cfif>
	<cfset resultStruct.ampm = LCASE(MID(time, 7, 2))>	
	
	<cfreturn resultStruct>  
  </cffunction>
  
  <cffunction name="CustomTimeFormat" access="public" output="no" returntype="string">
    <cfargument name="dateTimeObj" type="date" required="yes">
    <cfreturn LCase(TimeFormat(Arguments.dateTimeObj,"h:mmtt"))>
  </cffunction>
  
  <cffunction name="location" access="public" output="no"><!--- replicate cflocation tag --->
    <cfargument name="webURL" type="string" required="yes">
    <cflocation url="#Arguments.webURL#">
  </cffunction>
  
  <cffunction name="abort" access="public" output="no"><!--- replicate cfabort tag --->
    <cfabort>
  </cffunction>
  
  <cffunction name="CSVToArray" access="public" returntype="array" output="no" 
  			  hint="Takes a delimited text data file or chunk of delimited data and converts it to an array of arrays. (Author: Ben Nadel / Kinky Solutions)">
	<cfargument name="CSVData" type="string" required="false" default="" hint="This is the raw CSV data. This can be used if instead of a file path.">
	<cfargument name="CSVFilePath" type="string" required="false" default="" hint="This is the file path to a CSV data file. This can be used instead of a text data blob.">
	<cfargument name="Delimiter" type="string" required="false"	default=","	hint="The character that separate fields in the CSV.">
	<cfargument name="Qualifier" type="string" required="false"	default=""""
    		    hint="The field qualifier used in conjunction with fields that have delimiters (not used as delimiters ex: 1,344,343.00 where [,] is the delimiter).">

	<cfset var LOCAL = StructNew()><!--- Define the local scope. --->
 
	<!---
		Check to see if we are dealing with a file. If we are,
		then we will use the data from the file to overwrite
		any csv data blob that was passed in.
	--->
	<cfif (Len( ARGUMENTS.CSVFilePath ) AND FileExists( ARGUMENTS.CSVFilePath ) )>
		<!---
			Read the data file directly into the Arguments scope
			where it can override the blod data.
		--->
		<cffile	action="READ" file="#ARGUMENTS.CSVFilePath#" variable="ARGUMENTS.CSVData">
	</cfif>
 
	<!---
		ASSERT: At this point, whether we got the CSV data
		passed in as a data blob or we read it in from a
		file on the server, we now have our raw CSV data in
		the ARGUMENTS.CSVData variable.
	--->
 
	<!---
		Make sure that we only have a one character delimiter.
		I am not going traditional ColdFusion style here and
		allowing multiple delimiters. I am trying to keep
		it simple.
	--->
    
    <cfscript> 
	if (NOT Len( ARGUMENTS.Delimiter ) ) {
 	  /*  Since no delimiter was passed it, use thd default
		  delimiter which is the comma. */
	  ARGUMENTS.Delimiter = ",";
	} else if (Len( ARGUMENTS.Delimiter ) GT 1) {
	  /* Since multicharacter delimiter was passed, just
		 grab the first character as the true delimiter. */
	  ARGUMENTS.Delimiter = Left(ARGUMENTS.Delimiter, 1);
	} 
	
	/*	Make sure that we only have a one character qualifier.
		I am not going traditional ColdFusion style here and
		allowing multiple qualifiers. I am trying to keep
		it simple. */
		
	if (NOT Len( ARGUMENTS.Qualifier ) ) {
	  /* Since no qualifier was passed it, use thd default
		 qualifier which is the quote. */
	  ARGUMENTS.Qualifier = """";
	} else if (Len( ARGUMENTS.Qualifier ) GT 1) {
 	  /*	Since multicharacter qualifier was passed, just
			grab the first character as the true qualifier. */
	  ARGUMENTS.Qualifier = Left(ARGUMENTS.Qualifier, 1);
	}
 
 	// Create an array to handel the rows of data.
	LOCAL.Rows = ArrayNew( 1 );
 
	/*  Split the CSV data into rows of raw data. We are going
		to assume that each row is delimited by a return and
		/ or a new line character. */
	LOCAL.RawRows = ARGUMENTS.CSVData.Split("\r\n?");
 
 
	// Loop over the raw rows to parse out the data.
	for (LOCAL.RowIndex=1; LOCAL.RowIndex LTE ArrayLen( LOCAL.RawRows ); LOCAL.RowIndex = LOCAL.RowIndex + 1) {
	  // Create a new array for this row of data.
	  ArrayAppend( LOCAL.Rows, ArrayNew( 1 ) );
	  
	  // Get the raw data for this row.
	  LOCAL.RowData = LOCAL.RawRows[ LOCAL.RowIndex ];
 
	  /*	Replace out the double qualifiers. Two qualifiers in
			a row acts as a qualifier literal (OR an empty
			field). Replace these with a single character to
			make them easier to deal with. This is risky, but I
			figure that Chr( 1000 ) is something that no one
			is going to use (or is it????). */
			
	  LOCAL.RowData = LOCAL.RowData.ReplaceAll("[\#ARGUMENTS.Qualifier#]{2}", Chr( 1000 ));
	  
	  // Create a new string buffer to hold the value.
	  LOCAL.Value = CreateObject("java", "java.lang.StringBuffer").Init();
 
 	  /*	Set an initial flag to determine if we are in the
			middle of building a value that is contained within
			quotes. This will alter the way we handle
			delimiters - as delimiters or just character
			literals. */
	  LOCAL.IsInField = false;
	  
	  // Loop over all the characters in this row.
	  for (LOCAL.CharIndex=1; LOCAL.CharIndex LTE LOCAL.RowData.Length(); LOCAL.CharIndex = LOCAL.CharIndex + 1) {
 		/*		Get the current character. Remember, since Java
				is zero-based, we have to subtract one from out
				index when getting the character at a
				given position. */
	    LOCAL.ThisChar = LOCAL.RowData.CharAt(JavaCast( "int", (LOCAL.CharIndex - 1)) );
		
		/*		Check to see what character we are dealing with.
				We are interested in special characters. If we
				are not dealing with special characters, then we
				just want to add the char data to the ongoing
				value buffer. */
		if (LOCAL.ThisChar EQ ARGUMENTS.Delimiter) {
		  /*	Check to see if we are in the middle of
				building a value. If we are, then this is a
				character literal, not an actual delimiter.
				If we are NOT buildling a value, then this
				denotes the end of a value. */
		  if (LOCAL.IsInField) {
		    // Append char to current value.
			LOCAL.Value.Append(LOCAL.ThisChar.ToString() );
			/*	Check to see if we are dealing with an
				empty field. We will know this if the value
				in the field is equal to our "escaped"
				double field qualifier (see above). */
		  } else if ( (LOCAL.Value.Length() EQ 1) AND (LOCAL.Value.ToString() EQ Chr( 1000 )) ) {
		    /*	We are dealing with an empty field so
				just append an empty string directly to
				this row data. */
			ArrayAppend(LOCAL.Rows[ LOCAL.RowIndex ], "");
			/*	Start new value buffer for the next
				row value. */
			LOCAL.Value = CreateObject("java", "java.lang.StringBuffer").Init();
		  } else {
		    /*	Since we are not in the middle of
				building a value, we have reached the
				end of the field. Add the current value
				to row array and start a new value.
 				Be careful that when we add the new
				value, we replace out any "escaped"
				qualifiers with an actual qualifier
				character. */
			ArrayAppend(LOCAL.Rows[ LOCAL.RowIndex ], LOCAL.Value.ToString().ReplaceAll("#Chr( 1000 )#{1}",	ARGUMENTS.Qualifier));
			/*	Start new value buffer for the next
				row value. */
			LOCAL.Value = CreateObject("java", "java.lang.StringBuffer").Init();
		  }
		  
		  /*	Check to see if we are dealing with a field
				qualifier being used as a literal character.
				We just have to be careful that this is NOT
				an empty field (double qualifier). */
		} else if (LOCAL.ThisChar EQ ARGUMENTS.Qualifier) {
		  /*	Toggle the field flag. This will signal that
				future characters are part of a single value
				despite and delimiters that might show up. */
		  LOCAL.IsInField = (NOT LOCAL.IsInField);
		  
		  /*	We just have a non-special character. Add it
				to the current value buffer. */
		} else {
		  LOCAL.Value.Append(LOCAL.ThisChar.ToString());
		}
		
		/*	If we have no more characters left then we can't
			ignore the current value. We need to add this
			value to the row array. */
			
		if (LOCAL.CharIndex EQ LOCAL.RowData.Length()) {
		  /*	Check to see if the current value is equal
				to the empty field. If so, then we just
				want to add an empty string to the row. */
		  if ((LOCAL.Value.Length() EQ 1) AND (LOCAL.Value.ToString() EQ Chr( 1000 )) ) {
			/*	We are dealing with an empty field.
				Just add the empty string. */
		    ArrayAppend(LOCAL.Rows[ LOCAL.RowIndex ], "" );
		  } else {
 			/*	Nothing special about the value. Just
				add it to the row data. */
			ArrayAppend(LOCAL.Rows[ LOCAL.RowIndex ], LOCAL.Value.ToString().ReplaceAll("#Chr( 1000 )#{1}",	ARGUMENTS.Qualifier) );
		  }
 		}
	  }
	}
 
	// Return the row data.
	return LOCAL.Rows;
 	</cfscript>
  </cffunction>
  
  <cffunction name="displayDateTimePeriod" access="public" output="true" returntype="string">
    <cfargument name="startDateTime" type="date" required="yes">
	<cfargument name="endDateTime" type="date" required="yes">
	<cfargument name="allDayEvent" type="boolean" default="0">	
	<cfargument name="dateMask" type="string" default="mm/dd/yyyy">
	<cfset startDate_text=DateFormat(startDateTime, "#Arguments.dateMask#")>
	<cfset endDate_text=DateFormat(endDateTime, "#Arguments.dateMask#")>
    <cfsavecontent variable="datetimStr">
	<cfif Arguments.allDayEvent>
	  <cfif NOT Compare(startDate_text, endDate_text)><!--- same day --->
	    <cfoutput>#startDate_text#</cfoutput>	  
	  <cfelse>
	    <cfoutput>#startDate_text# - #endDate_text#</cfoutput>  
	  </cfif>
	<cfelse>
	  <cfif NOT Compare(startDate_text, endDate_text)><!--- same day --->
	    <cfoutput>#endDate_text#/#Replace(Replace(TimeFormat(startDateTime, "h:mmtt"), "AM", "am"), "PM", "pm")# - #Replace(Replace(TimeFormat(endDateTime, "h:mmtt"), "AM", "am"), "PM", "pm")#</cfoutput>
	  <cfelse>
	    <cfoutput>#startDate_text#/#Replace(Replace(TimeFormat(startDateTime, "h:mmtt"), "AM", "am"), "PM", "pm")# - #endDate_text#/#Replace(Replace(TimeFormat(endDateTime, "h:mmtt"), "AM", "am"), "PM", "pm")#</cfoutput>
	  </cfif>
	</cfif>  
    </cfsavecontent>
    <cfreturn datetimStr>
  </cffunction>
  
  <cffunction name="getGMT" access="public" output="no" returntype="string">
    <cfargument name="localDateTime" type="date" required="yes">
    <!--- Gets GMT date/time (GMT) from local time --->
    <cfset TZ=getTimeZoneInfo()>
    <cfset GDT=dateAdd('s',TZ.utcTotalOffset,localDateTime)>
    <cfset GMTDt=DateFormat(GDT,'ddd, dd mmm yyyy')>
    <cfset GMTTm=TimeFormat(GDT,'HH:mm:ss')>
    <cfreturn "#GMTDt# #GMTTm# GMT">
  </cffunction>
  
  <cffunction name="ConvertBadCharacters" access="public" output="no" returntype="string">
    <cfargument name="inputString" type="string" required="yes">
    <cfset Arguments.inputString = Replace(Arguments.inputString, "�", "&lsquo;", "ALL")>
    <cfset Arguments.inputString = Replace(Arguments.inputString, "�", "&rsquo;", "ALL")>
    <cfset Arguments.inputString = Replace(Arguments.inputString, "�", "&ldquo;", "ALL")>
    <cfset Arguments.inputString = Replace(Arguments.inputString, "�", "&rdquo;", "ALL")>
    <cfset Arguments.inputString = Replace(Arguments.inputString, "�", "&mdash;", "ALL")>
    <cfset Arguments.inputString = Replace(Arguments.inputString, "�", "&ndash;", "ALL")>
    <cfset Arguments.inputString = Replace(Arguments.inputString, "", "'", "ALL")>
    <cfset Arguments.inputString = Replace(Arguments.inputString, "", " ", "ALL")><!--- very tricky invisible one; don't change --->
    <cfset Arguments.inputString = Replace(Arguments.inputString, "", "-", "ALL")>  
  
    <cfreturn Arguments.inputString>
  </cffunction>
  
  
  
  <!--- Site Specific --->
  <cffunction name="displayNextPreviousLinks" access="public" output="no" returntype="string">
    <cfargument name="pageURL" type="string" required="yes">    
	<cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="15">	
	<cfargument name="numDisplayedItems" type="numeric" required="yes">
	<cfargument name="numAllItems" type="numeric" required="yes">
	<cfargument name="itemName" type="string" required="yes">

    <cfsavecontent variable="outputString">
	<cfoutput>
    <center>
    <div class="displayTools">
	  <div class="leftInfo">
		<cfif Arguments.startIndex GT Arguments.numItemsPerPage>
		  <cfset sIdx = Arguments.startIndex - Arguments.numItemsPerPage>
		  <a href="#Arguments.pageURL#&amp;startIndex=#sIdx#">&lt; Previous #Arguments.numItemsPerPage#</a>
		<cfelse>
		  &nbsp;
		</cfif>
	  </div>
	  <div class="rightInfo">
        <cfset nextSIdx = Arguments.startIndex + Arguments.numItemsPerPage>
		<cfif nextSIdx LTE Arguments.numAllItems>
		  <cfif (Arguments.numAllItems-nextSIdx+1) LT Arguments.numItemsPerPage>
		    <cfset nextNumItems=Arguments.numAllItems-nextSIdx+1>
		  <cfelse>
		    <cfset nextNumItems=Arguments.numItemsPerPage>
		  </cfif>
          <a href="#Arguments.pageURL#&amp;startIndex=#nextSIdx#">Next #nextNumItems#  &gt;</a>
        <cfelse>
		  &nbsp;
		</cfif>
      </div>
      <div class="centerInfo">
        <cfset eIdx=Arguments.startIndex + Arguments.numDisplayedItems - 1>
		<cfif eIdx GT Arguments.numAllItems><cfset eIdx = Arguments.numAllItems></cfif>
        <span class="accent03"><b>#Arguments.startIndex# - #eIdx# of #Arguments.numAllItems# #Arguments.itemName# Currently Displayed</b></span>
      </div>
    </div>
    </center>
	</cfoutput>
    </cfsavecontent>
    <cfreturn outputString>
  </cffunction>
  
  <cffunction name="getOptionList" access="public" output="false" returntype="string">
    <cfargument name="selectMenuType" type="string" required="yes">
	<cfargument name="selectedOptionList" type="string" default="">
	
	<cfsavecontent variable="optionList">
	<cfoutput>
	<cfswitch expression="#selectMenuType#">
	  <cfcase value="States">
	    <cfset stateList="Alabama,Alaska,Arizona,Arkansas,California,Colorado,Connecticut,Delaware,District of Columbia,Florida,">
	    <cfset stateList=stateList & "Georgia,Hawaii,Idaho,Illinois,Indiana,Iowa,Kansas,Kentucky,Louisiana,Maine,">
	    <cfset stateList=stateList & "Maryland,Massachusetts,Michigan,Minnesota,Mississippi,Missouri,Montana,Nebraska,Nevada,New Hampshire,">
	    <cfset stateList=stateList & "New Jersey,New Mexico,New York,North Carolina,North Dakota,Ohio,Oklahoma,Oregon,Pennsylvania,Rhode Island,">
	    <cfset stateList=stateList & "South Carolina,South Dakota,Tennessee,Texas,Utah,Vermont,Virginia,Washington,West Virginia,Wisconsin,">
	    <cfset stateList=stateList & "Wyoming"> 
	    <cfif arguments.selectedOptionList IS "">
		  <cfloop index="state" list="#stateList#" delimiters=",">
		  <option value="#state#">#state#</option>
		  </cfloop>
		<cfelse>
		  <cfloop index="state" list="#stateList#" delimiters=",">
		  <option value="#state#"<cfif ListFind(arguments.selectedOptionList, "#state#")> selected</cfif>>#state#</option>
		  </cfloop>
		</cfif> 
	  </cfcase>
	  <cfcase value="Countries">
	    <cfset countryList="United States;Afghanistan;Albania;Algeria;American Samoa;Andorra;Anguilla;Antarctica;Antigua And Barbuda;">
		<cfset countryList=countryList & "Agentina;Armenia;Aruba;Australia;Austria;Azerbaijan;Bahamas;Bahrain;Bangladesh;">
		<cfset countryList=countryList & "Barbados;Belarus;Belgium;Belize;Benin;Bermuda;Bhutan;Bolivia;Bosnia and Herzegovina;">
		<cfset countryList=countryList & "Botswana;Bouvet Island;Brazil;British Indian Ocean Territory;Brunei Darussalam;">
		<cfset countryList=countryList & "Bulgaria;Burkina Faso;Burundi;Cambodia;Cameroon;Canada;Cape Verde;Cayman Islands;">
		<cfset countryList=countryList & "Central African Republic;Chad;Chile;China;Christmas Island;Cocos (Keeling) Islands;">
		<cfset countryList=countryList & "Colombia;Comoros;Congo;Congo, the Democratic Republic of the;Cook Islands;Costa Rica;">
		<cfset countryList=countryList & "Cote d'Ivoire;Croatia;Cyprus;Czech Republic;Denmark;Djibouti;Dominica;Dominican Republic;">
		<cfset countryList=countryList & "East Timor;Ecuador;Egypt;El Salvador;England;Equatorial Guinea;Eritrea;Espana;Estonia;">
		<cfset countryList=countryList & "Ethiopia;Falkland Islands;Faroe Islands;Fiji;Finland;France;French Guiana;French Polynesia;">
		<cfset countryList=countryList & "French Southern Territories;Gabon;Gambia;Georgia;Germany;Ghana;Gibraltar;Great Britain;">
		<cfset countryList=countryList & "Greece;Greenland;Grenada;Guadeloupe;Guam;Guatemala;Guinea;Guinea-Bissau;Guyana;">
		<cfset countryList=countryList & "Haiti;Heard and Mc Donald Islands;Honduras;Hong Kong;Hungary;Iceland;India;">
		<cfset countryList=countryList & "Indonesia;Ireland;Israel;Italy;Jamaica;Japan;Jordan;Kazakhstan;Kenya;Kiribati;">
		<cfset countryList=countryList & "Korea, Republic of;Korea (South);Kuwait;Kyrgyzstan;Lao People's Democratic Republic;">
		<cfset countryList=countryList & "Latvia;Lebanon;Lesotho;Liberia;Libya;Liechtenstein;Lithuania;Luxembourg;Macau;">
		<cfset countryList=countryList & "Macedonia;Madagascar;Malawi;Malaysia;Maldives;Mali;Malta;Marshall Islands;Martinique;">
		<cfset countryList=countryList & "Mauritania;Mauritius;Mayotte;Mexico;Micronesia, Federated States of;Moldova, Republic of;">
		<cfset countryList=countryList & "Monaco;Mongolia;Montserrat;Morocco;Mozambique;Myanmar;Namibia;Nauru;Nepal;Netherlands;">
		<cfset countryList=countryList & "Netherlands Antilles;New Caledonia;New Zealand;Nicaragua;Niger;Nigeria;Niue;Norfolk Island;">
		<cfset countryList=countryList & "Northern Ireland;Northern Mariana Islands;Norway;Oman;Pakistan;Palau;Panama;Papua New Guinea;">
		<cfset countryList=countryList & "Paraguay;Peru;Philippines;Pitcairn;Poland;Portugal;Puerto Rico;Qatar;Reunion;Romania;">
		<cfset countryList=countryList & "Russia;Russian Federation;Rwanda;Saint Kitts and Nevis;Saint Lucia;Saint Vincent and the Grenadines;">
		<cfset countryList=countryList & "Samoa (Independent);San Marino;Sao Tome and Principe;Saudi Arabia;Scotland;Senegal;Serbia and Montenegro;">
		<cfset countryList=countryList & "Seychelles;Sierra Leone;Singapore;Slovakia;Slovenia;Solomon Islands;Somalia;South Africa;South Georgia and the South Sandwich Islands;">
		<cfset countryList=countryList & "South Korea;Spain;Sri Lanka;St. Helena;St. Pierre and Miquelon;Suriname;Svalbard and Jan Mayen Islands;">
		<cfset countryList=countryList & "Swaziland;Sweden;Switzerland;Taiwan;Tajikistan;Tanzania;Thailand;Togo;Tokelau;Tonga;Trinidad;">
		<cfset countryList=countryList & "Trinidad and Tobago;Tunisia;Turkey;Turkmenistan;Turks and Caicos Islands;Tuvalu;Uganda;Ukraine;">
		<cfset countryList=countryList & "United Arab Emirates;United Kingdom;United States;United States Minor Outlying Islands;Uruguay;">
		<cfset countryList=countryList & "USA;Uzbekistan;Vanuatu;Vatican City State (Holy See);Venezuela;Viet Nam;Virgin Islands (British);">
		<cfset countryList=countryList & "Virgin Islands (U.S.);Wales;Wallis and Futuna Islands;Western Sahara;Yemen;Zambia;Zimbabwe">
		<cfif arguments.selectedOptionList IS "">
		  <cfloop index="country" list="#countryList#" delimiters=";">
		  <option value="#country#">#country#</option>
		  </cfloop>
		<cfelse>
		  <cfloop index="country" list="#country#" delimiters=";">
		  <option value="#country#"<cfif ListFind(arguments.selectedOptionList, "#country#")> selected</cfif>>#country#</option>
		  </cfloop>
		</cfif> 
	  </cfcase>
	  <cfcase value="Months">  
	    <cfif arguments.selectedOptionList IS "">
		  <cfloop index="idx" from="1" to="12">
		  <option value="#MonthAsString(idx)#">#MonthAsString(idx)#</option>
		  </cfloop>
		<cfelse>
 		  <cfloop index="idx" from="1" to="12">
		  <option value="#MonthAsString(idx)#"<cfif ListFind(arguments.selectedOptionList, "#MonthAsString(idx)#")> selected</cfif>>#MonthAsString(idx)#</option>
		  </cfloop>
		</cfif>
	  </cfcase>
	  <cfcase value="Days">
	    <cfif arguments.selectedOptionList IS "">
		  <cfloop index="idx" from="1" to="31">
		  <option value="#idx#">#idx#</option>
		  </cfloop>
		<cfelse>
		  <cfloop index="idx" from="1" to="31">
		  <option value="#idx#"<cfif ListFind(arguments.selectedOptionList, "#idx#")> selected</cfif>>#idx#</option>
		  </cfloop>
		</cfif>
	  </cfcase>
	  <cfdefaultcase></cfdefaultcase>
    </cfswitch>
	</cfoutput>
	</cfsavecontent>
	
	<cfreturn optionList>
  </cffunction>
  
  <cffunction name="createiCalendarString" access="public" output="no" returntype="string">
    <cfargument name="structEvent" type="struct" required="yes">
    
    <cfset var stEvent=Arguments.structEvent>
    <!---
		stEvent
			.name
			.location
			.description
			.startDateTime
			.endDateTime
			.recurring (1/0)
	--->
    <cfscript>
     var vCal = "";
     var newLine = chr(13) & chr(10);
     var currentTime = now();
	 
	 vCal="BEGIN:VCALENDAR" & newLine &
	 	  "VERSION:2.0" & newLine &
		  "PRODID:-//Covalent Logic//Content Management System//EN" & newLine &
		  "BEGIN:VEVENT";
	 vCal=vCal & newLine &
		  "UID:" & currentTime.getTime() & ".COVALENTLOGIC.COM" & newLine &
		  "DTSTART:" & DateFormat(stEvent.startDateTime,"yyyymmdd") & "T" & TimeFormat(stEvent.startDateTime, "HHmmss") & newLine &
		  "DTEND:" & DateFormat(stEvent.endDateTime,"yyyymmdd") & "T" & TimeFormat(stEvent.endDateTime, "HHmmss");
	 
	 if (IsDefined("stEvent.recurring") And stEvent.recurring) {
		switch ("#DayOfWeek(stEvent.startDateTime)#") {
		  case "1": byDay="SU"; break;
		  case "2": byDay="MO"; break;
		  case "3": byDay="TU"; break;
		  case "4": byDay="WE"; break;
		  case "5": byDay="TH"; break;
		  case "6": byDay="FR"; break;
		  case "7": byDay="SA"; break;
		  default: byDay=""; break;
		}
		vCal=vCal & newLine &
			 "RRULE:FREQ=WEEKLY;BYDAY=" & byDay;
	  }
	  vCal=vCal & newLine &
	  	   "SUMMARY:" & stEvent.name & newLine &
		   "LOCATION:" & stEvent.location & newLine &
		   "DESCRIPTION:" & stEvent.description & newLine &
		   "PRIORITY:5";
	  vCal=vCal & newLine &
	  	   "END:VEVENT" & newLine &
		   "END:VCALENDAR"; 
		   
	  return Trim(vCal);
    </cfscript>
  </cffunction>
  
  <cffunction name="iCalUS" access="public" output="no" returntype="string">
    <cfargument name="structEvent" type="struct" required="yes">
    
    <cfset var stEvent=Arguments.structEvent>
    
	<cfscript>
    /**
    * Pass a formatted structure containing the event properties and get back a string in the iCalendar format (correctly offset for daylight savings time in U.S.) that can be saved to a file or returned to the browser with MIME type=&quot;text/calendar&quot;.
    * v2 updated by Dan Russel
    * 
    * @param stEvent      Structure of data for the event. (Required)
    * @return Returns a string. 
    * @author Troy Pullis (tpullis@yahoo.com) 
    * @version 2, December 18, 2008 
    */
        var vCal = "";
        var CRLF=chr(13)&chr(10);
        var date_now = Now();
            
        if (NOT IsDefined("stEvent.organizerName"))
            stEvent.organizerName = "Organizer Name";
            
        if (NOT IsDefined("stEvent.organizerEmail"))
            stEvent.organizerEmail = "Organizer_Name@CFLIB.ORG";
                    
        if (NOT IsDefined("stEvent.subject"))
            stEvent.subject = "Event subject goes here";
            
        if (NOT IsDefined("stEvent.location"))
            stEvent.location = "Event location goes here";
        
        if (NOT IsDefined("stEvent.description"))
            stEvent.description = "";
            
        if (NOT IsDefined("stEvent.startTime")) // This value must be in Central time!!!
            stEvent.startTime = ParseDateTime("3/21/2008 14:30"); // Example start time is 21-March-2008 2:30 PM Central
        
        if (NOT IsDefined("stEvent.endTime"))
            stEvent.endTime = ParseDateTime("3/21/2008 15:30"); // Example end time is 21-March-2008 3:30 PM Central
            
        if (NOT IsDefined("stEvent.priority"))
            stEvent.priority = "5";
			
		if (IsDefined("stEvent.recurring") And stEvent.recurring) {
		  switch ("#DayOfWeek(stEvent.startTime)#") {
		    case "1": ByDay="SU"; break;
			case "2": ByDay="MO"; break;
			case "3": ByDay="TU"; break;
			case "4": ByDay="WE"; break;
			case "5": ByDay="TH"; break;
			case "6": ByDay="FR"; break;
			case "7": ByDay="SA"; break;
			default: break;
		  }
		}
            
        vCal = "BEGIN:VCALENDAR" & CRLF;
        vCal = vCal & "PRODID: -//CFLIB.ORG//iCalUS()//EN" & CRLF;
        vCal = vCal & "VERSION:2.0" & CRLF;
        vCal = vCal & "METHOD:REQUEST" & CRLF;
        vCal = vCal & "BEGIN:VTIMEZONE" & CRLF;
        vCal = vCal & "TZID:Central" & CRLF;
        vCal = vCal & "BEGIN:STANDARD" & CRLF;
        vCal = vCal & "DTSTART:20061101T020000" & CRLF;
        vCal = vCal & "RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=1SU;BYMONTH=11" & CRLF;
        vCal = vCal & "TZOFFSETFROM:-0500" & CRLF;
        vCal = vCal & "TZOFFSETTO:-0600" & CRLF;
        vCal = vCal & "TZNAME:Standard Time" & CRLF;
        vCal = vCal & "END:STANDARD" & CRLF;
        vCal = vCal & "BEGIN:DAYLIGHT" & CRLF;
        vCal = vCal & "DTSTART:20060301T020000" & CRLF;
        vCal = vCal & "RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=2SU;BYMONTH=3" & CRLF;
        vCal = vCal & "TZOFFSETFROM:-0600" & CRLF;
        vCal = vCal & "TZOFFSETTO:-0500" & CRLF;
        vCal = vCal & "TZNAME:Daylight Savings Time" & CRLF;
        vCal = vCal & "END:DAYLIGHT" & CRLF;
        vCal = vCal & "END:VTIMEZONE" & CRLF;
        vCal = vCal & "BEGIN:VEVENT" & CRLF;
        vCal = vCal & "UID:#date_now.getTime()#.CFLIB.ORG" & CRLF; // creates a unique identifier
        vCal = vCal & "ORGANIZER;CN=#stEvent.organizerName#:MAILTO:#stEvent.organizerEmail#" & CRLF;
        vCal = vCal & "DTSTAMP:" & 
                DateFormat(date_now,"yyyymmdd") & "T" & 
                TimeFormat(date_now, "HHmmss") & CRLF;
        vCal = vCal & "DTSTART;TZID=Central:" & 
                DateFormat(stEvent.startTime,"yyyymmdd") & "T" & 
                TimeFormat(stEvent.startTime, "HHmmss") & CRLF;
        vCal = vCal & "DTEND;TZID=Central:" & 
                DateFormat(stEvent.endTime,"yyyymmdd") & "T" & 
                TimeFormat(stEvent.endTime, "HHmmss") & CRLF;
        vCal = vCal & "SUMMARY:#stEvent.subject#" & CRLF;
        vCal = vCal & "LOCATION:#stEvent.location#" & CRLF;
        vCal = vCal & "DESCRIPTION:#stEvent.description#" & CRLF;
        vCal = vCal & "PRIORITY:#stEvent.priority#" & CRLF;
		if (IsDefined("stEvent.recurring") And stEvent.recurring)
			vCal = vCal & "RRULE:FREQ=WEEKLY;BYDAY=#ByDay#" & CRLF;		
        vCal = vCal & "TRANSP:OPAQUE" & CRLF;
        vCal = vCal & "CLASS:PUBLIC" & CRLF;
        vCal = vCal & "BEGIN:VALARM" & CRLF;
        vCal = vCal & "TRIGGER:-PT30M" & CRLF; // alert user 30 minutes before meeting begins
        vCal = vCal & "ACTION:DISPLAY" & CRLF;
        vCal = vCal & "DESCRIPTION:Reminder" & CRLF;
        vCal = vCal & "END:VALARM" & CRLF;
        vCal = vCal & "END:VEVENT" & CRLF;
        vCal = vCal & "END:VCALENDAR";
        return Trim(vCal);
    </cfscript>  
  </cffunction>
  
  <cffunction name="getRandomSubList" access="public" output="no" returntype="string">
    <!--- get a random sub-list from a list --->
    <cfargument name="sourceList" type="string" required="yes">
    <cfargument name="numItems" type="numeric" required="yes">
    <cfset var subList="">
    <cfset var i = 1>
    <cfset var j = 0>
    
    <cfscript>
    if (ListLen(Arguments.sourceList) LTE Arguments.numItems) return Arguments.sourceList;
    for (i=1; i LTE Arguments.numItems; i=i+1) {
      if (ListLen(Arguments.sourceList) GT 0) {
        j = RandRange(1,ListLen(Arguments.sourceList));
        subList=ListAppend(subList, ListGetAt(Arguments.sourceList, j));
        Arguments.sourceList=ListDeleteAt(Arguments.sourceList,j);
      }
    }
    </cfscript>
    
    <cfreturn subList>
  </cffunction>
</cfcomponent>