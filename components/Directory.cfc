<cfcomponent displayname="Directory" extends="Base" output="false">
  <cffunction name="getAllMaps" access="public" output="no" returntype="query">
    <cfquery name="qMaps" datasource="#this.datasource#">
      SELECT mapID, mapName, mapFile
      FROM dr_map
      ORDER BY mapID
    </cfquery>
    <cfreturn qMaps>
  </cffunction>
  
  <cffunction name="getAllFeaturedMaps" access="public" output="no" returntype="query">
    <cfquery name="qMaps" datasource="#this.datasource#">
      SELECT mapID, mapName, mapFile
      FROM dr_map
      WHERE featured=1
      ORDER BY mapID
    </cfquery>
    <cfreturn qMaps>
  </cffunction>
  
  <cffunction name="getMapByID" access="public" output="no" returntype="query">
    <cfargument name="mapID" required="yes" type="numeric">
    
    <cfquery name="qMap" datasource="#this.datasource#">
      SELECT mapName, mapFile
      FROM dr_map
      WHERE mapID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.mapID#">
    </cfquery>
    <cfreturn qMap>
  </cffunction>
  
  <cffunction name="getItem" access="public" output="no" returntype="query">
    <cfargument name="itemID" type="numeric" required="yes">
    <cfargument name="itemType" type="string" default="O">
    <cfargument name="publishedOnly" type="boolean" default="1">
	<cfquery name="qItem" datasource="#this.datasource#">
	  SELECT *
	  FROM dr_item
      WHERE itemID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.itemID#"> AND
      		dr_item.itemType=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.itemType#">
            <cfif Arguments.publishedOnly> AND
      		published=1
            </cfif>
	</cfquery>
    
	<cfreturn qItem>	
  </cffunction>

  <cffunction name="getRelatedItems" access="public" output="no" returntype="query">
    <cfargument name="itemID" type="numeric" required="yes">
    <cfargument name="itemType" type="string" default="I">
    
    <cfquery name="qItems" datasource="#this.datasource#">
      SELECT dr_item.itemID, dr_item.indivPrefix, dr_item.indivFirstName, dr_item.indivMiddleName, dr_item.indivLastName, dr_item.indivSuffix,
      		 dr_item.indivDivision, dr_item.indivJobTitle, dr_item.indivAddress, dr_item.indivCity, dr_item.indivState, dr_item.indivZIP,
             dr_item.indivEmail, dr_item.indivPhone, dr_item.indivFax, dr_item.indivPhotoFile     
      FROM dr_item_item_related INNER JOIN dr_item ON dr_item_item_related.itemID2=dr_item.itemID
      WHERE dr_item_item_related.itemID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.itemID#"> AND
      		dr_item.published=1 AND
            dr_item.itemType=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.itemType#">
      ORDER BY dr_item_item_related.displaySeq
    </cfquery>
    <cfreturn qItems>
  </cffunction>

  <cffunction name="getDirectorySummary" access="public" output="no" returntype="query">
    <cfquery name="qItems" datasource="#this.datasource#">
      SELECT dr_item.itemID, dr_item.orgName, dr_item.orgAddressPhysical, dr_item.orgCityPhysical,
      		 dr_item.orgStatePhysical, dr_item.orgZIPPhysical,
             dr_item.orgPhone, dr_item.orgFax, dr_item.orgWebAddress,
             dr_category.categoryID, dr_category.categoryName
      FROM (dr_item INNER JOIN dr_item_category ON dr_item.itemID=dr_item_category.itemID) INNER JOIN dr_category ON
      		dr_item_category.categoryID=dr_category.categoryID
      WHERE dr_item.published=1 AND dr_item.itemType='O'
      ORDER BY dr_category.categoryName, dr_item.orgName
    </cfquery>

    <cfreturn qItems>
  </cffunction>
  
  <cffunction name="contactDepartment" access="public" output="no">
    <cfargument name="itemID" type="numeric" required="yes">
    <cfargument name="firstName" type="string" required="yes">
    <cfargument name="lastName" type="string" required="yes">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="comments" type="string" required="yes">
    
    <cfset itemInfo=getItem(Arguments.itemID)>
    <cfif Compare(itemInfo.orgEmail,"")>
      <cfset recipientEmail=itemInfo.orgEmail>
    <cfelse>
      <cfset GL=CreateObject("component","com.Global").init()>
      <cfset recipientEmail=GL.getWebmasterEmail()>
    </cfif>
    
    <cftry>
      <cfmail from="DoNotReply@womans.org" to="#recipientEmail#" subject="Contact Form Submission - Department Directory" type="html">
        <cfmailpart type="text" charset="utf-8">
Department: #itemInfo.orgName#

Name: #Arguments.firstName# #Arguments.lastName#

Email Address: #Arguments.email#
    
Comments/Questions:
#Arguments.comments#
		</cfmailpart>
        <cfmailpart type="html" charset="utf-8">
          <a href="#this.siteURLRoot#"><img src="#this.siteURLRoot#/images/imgLogoEmailNot.gif" alt="Woman's - exceptional care, centered on you" width="96" height="84" border="0" /></a>
          <br /><br /><br /> 
          <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:12px;color:##333132;">
            <b>Department:</b> #itemInfo.orgName#<br /><br />
            <b>Name:</b> #Arguments.firstName# #Arguments.lastName#<br /><br />         
            <b>Email Address:</b> #Arguments.email#<br /><br />             
            <b>Comments/Questions:</b><br />
            #Replace(Arguments.comments,Chr(10),"<br />","All")#
          </div>
          <br />
          <hr size="1" noshade="noshade" />
          <a href="#this.siteURLRoot#" style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;font-size:19px;color:##627d79;line-height:110%;text-decoration:none;"><b>WWW.WOMANS.ORG</b></a>
        </cfmailpart>
      </cfmail>
    <cfcatch></cfcatch>
    </cftry>
  </cffunction>
  
  <cffunction name="getAllOrganizationNames" access="public" output="no" returntype="query">
    <cfquery name="qOrgNames" datasource="#this.datasource#">
      SELECT orgName
      FROM dr_item
      WHERE itemType='O' AND
      		published=1
      ORDER BY orgName
    </cfquery>
    
    <cfreturn qOrgNames>
  </cffunction>
</cfcomponent>