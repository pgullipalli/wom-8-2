<cfcomponent displayname="ClassEntity" output="no">
  <cfif IsDefined("Application.datasource")>
    <cfset this.datasource=Application.datasource>
  </cfif>
  
  <cfset this.classID=0>
  <cfset this.courseID=0>
  <cfset this.GLNumber="">
  <cfset this.courseTitle="">
  <cfset this.cost=0>
  <cfset this.paymentPlanEligible=false>
  <cfset this.paymentPlanType="A"><!--- A: $750+ 3 payments, 1/3 cost + $10; B: $400 - $749.99 2 payments, 1/2 cost + $10 --->
  <cfset this.firstInstallment=0><!--- only used when paymentPlanEligible=true --->
  <cfset this.installmentFee=10><!--- $10.00 added to the first paymnet if payment plan is selected --->
  <cfset this.remainingPayment=0><!--- remaining balance after the first payment --->
  <cfset this.discountCode="">
  <cfset this.discountType="">
  <cfset this.discountRate=0>
  <cfset this.discountApplied=0>
  <cfset this.discountAmount=0>
  <cfset this.feePerCouple=0>
  <cfset this.attendeeNameLabel="">
  <cfset this.guestNameLabel="">
  <cfset this.numClassInstances=1>
  <cfset this.classInstance=ArrayNew(1)><!--- array of ClassInstanceEntity objects --->
  
  <cfset this.studentClassID=0><!--- primary key of 'cl_student_class' table; only used when this entity is owned by a user-specific --->
  <cfset this.feedbackSent=0><!--- only used when this entity is owned by a user-specific --->
  <cfset this.attendeeName=""><!--- only used when this entity is owned by a user-specific --->
  <cfset this.guestNames=""><!--- only used when this entity is owned by a user-specific --->
  <cfset this.enrollPaymentPlan=false><!--- only used when this entity is owned by a user-specific --->
  
  
  <cffunction name="init" access="public" output="no" returntype="ClassEntity">
    <cfargument name="classID" type="numeric" required="yes">
    <cfargument name="GLNumber" type="string" required="yes">
    <cfargument name="courseID" type="numeric" required="yes">
    <cfargument name="courseTitle" type="string" required="yes">
    <cfargument name="cost" type="numeric" required="yes">
    <cfargument name="discountCode" type="string" default="">
    <cfargument name="discountType" type="string" default="">
    <cfargument name="discountRate" type="numeric" default="0">
    <cfargument name="feePerCouple" type="feePerCouple" default="0">
    <cfargument name="attendeeNameLabel" type="string" default="">
    <cfargument name="guestNameLabel" type="string" default="">
    <cfargument name="numClassInstances" type="numeric" required="yes">
    <cfargument name="classInstance" type="array" required="yes">

    <cfset this.classID=Arguments.classID>
    <cfset this.courseID=Arguments.courseID>
    <cfset this.courseTitle=Arguments.courseTitle>
    <cfset this.cost=Arguments.cost>
    <cfset this.discountCode=Arguments.discountCode>
    <cfset this.discountType=Arguments.discountType>
    <cfset this.discountRate=Arguments.discountRate>
    <cfset this.feePerCouple=Arguments.feePerCouple>
    <cfset this.attendeeNameLabel=Arguments.attendeeNameLabel>
    <cfset this.guestNameLabel=Arguments.guestNameLabel>
    <cfset this.numClassInstances=Arguments.numClassInstances>
    <cfset this.classInstance=Arguments.classInstance>
    
    <cfif this.cost GTE 750>
	  <cfset this.paymentPlanEligible=true>
      <cfset this.paymentPlanType="A">
      <cfset this.firstInstallment=Ceiling(this.cost/3) + this.installmentFee>
      <cfset this.remainingPayment=this.cost - this.firstInstallment + this.installmentFee>
    <cfelseif this.cost GTE 400 And this.cost LT 750>
      <cfset this.paymentPlanEligible=true>
      <cfset this.paymentPlanType="B">
      <cfset this.firstInstallment=Ceiling(this.cost/2) + this.installmentFee>
      <cfset this.remainingPayment=this.cost - this.firstInstallment + this.installmentFee>
    </cfif>

    <cfreturn this>
  </cffunction>  
  
  <cffunction name="getClassEntityByID" access="public" output="no" returntype="ClassEntity">
    <cfargument name="classID" type="numeric" required="yes">
    <cfargument name="studentType" type="string" default="R"><!--- enum('R','D','E','F') --->
    
    <cfset var idx = 0>
    <cfset var argStruct=StructNew()>
    
    <cfquery name="qClass" datasource="#this.datasource#">
      SELECT cl_course.courseID, cl_course.GLNumber, cl_course.courseTitle, cl_course.regMaximum,
      		 cl_course.feeRegular, cl_course.feeEmployee, cl_course.feeDoctor, cl_course.feeFitnessClub,
             cl_course.feePerCouple, cl_course.attendeeNameLabel, cl_course.guestNameLabel,
             cl_course.discountCode, cl_course.discountType,cl_course.discountRate
      FROM cl_class INNER JOIN cl_course ON cl_class.courseID=cl_course.courseID
      WHERE cl_class.classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
    </cfquery>
    
    <cfset this.classID=Arguments.classID>
    <cfset this.courseID=qClass.courseID>
    <cfset this.GLNumber=qClass.GLNumber>
    <cfset this.courseTitle=qClass.courseTitle>
    
    <cfswitch expression="#Arguments.studentType#">
      <cfcase value="R"><cfset this.cost=qClass.feeRegular></cfcase>
      <cfcase value="D"><cfset this.cost=qClass.feeDoctor></cfcase>
      <cfcase value="E"><cfset this.cost=qClass.feeEmployee></cfcase>
      <cfcase value="F"><cfset this.cost=qClass.feeFitnessClub></cfcase>
    </cfswitch>
    <cfif this.cost GTE 750>
	  <cfset this.paymentPlanEligible=true>
      <cfset this.paymentPlanType="A">
      <cfset this.firstInstallment=Ceiling(this.cost/3) + this.installmentFee>
      <cfset this.remainingPayment=this.cost - this.firstInstallment + this.installmentFee>
    <cfelseif this.cost GTE 400 And this.cost LT 750>
      <cfset this.paymentPlanEligible=true>
      <cfset this.paymentPlanType="B">
      <cfset this.firstInstallment=Ceiling(this.cost/2) + this.installmentFee>
      <cfset this.remainingPayment=this.cost - this.firstInstallment + this.installmentFee>
    </cfif>   
    
    <cfset this.discountCode=qClass.discountCode>
    <cfset this.discountType=qClass.discountType>
    <cfset this.discountRate=qClass.discountRate>
    <cfset this.feePerCouple=qClass.feePerCouple>
    <cfset this.attendeeNameLabel=qClass.attendeeNameLabel>
    <cfset this.guestNameLabel=qClass.guestNameLabel>
    
    <cfquery name="qClassInstance" datasource="#this.datasource#">
      SELECT cl_classinstance.classInstanceID, cl_classinstance.locationID,
      		 cl_classinstance.startDateTime, cl_classinstance.endDateTime,
      		 cl_classinstance.dayOfWeekAsNumber, cl_location.locationName, cl_location.mapID, cl_location.posX AS mapVerified
      FROM cl_classinstance LEFT JOIN cl_location ON
      	   cl_classinstance.locationID=cl_location.locationID
      WHERE cl_classinstance.classID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classID#">
      ORDER BY cl_classinstance.startDateTime
    </cfquery>

    <cfset this.numClassInstances=qClassInstance.recordcount>    
    <cfset ArrayClear(this.classInstance)><!--- clear the array --->
    <cfloop query="qClassInstance">
      <cfset idx = idx + 1>
      <cfset argStruct.classInstanceID=classInstanceID>
      <cfset argStruct.classID=Arguments.classID>
      <cfset argStruct.startDateTime=qClassInstance.startDateTime>
      <cfset argStruct.endDateTime=qClassInstance.endDateTime>
      <cfset argStruct.dayOfWeekAsNumber=qClassInstance.dayOfWeekAsNumber>
      <cfset argStruct.locationID=qClassInstance.locationID>
      <cfset argStruct.locationName=qClassInstance.locationName>
      <cfif IsNumeric(qClassInstance.mapID)>
        <cfset argStruct.mapID=qClassInstance.mapID>
      <cfelse>
        <cfset argStruct.mapID=0>
      </cfif>
      <cfif IsNumeric(qClassInstance.mapVerified)><cfset argStruct.mapVerified=qClassInstance.mapVerified></cfif>
      <cfset this.classInstance[idx]=CreateObject("component","ClassInstanceEntity").init(argumentCollection=argStruct)>
    </cfloop>
     
    <cfreturn this>
  </cffunction>
  
  <cffunction name="applyDiscountCode" access="public" output="no" returntype="boolean">
    <cfargument name="discountCode" type="string" required="yes">
    
    <cfif Not Compare(this.discountCode,"") Or Not Compare(Arguments.discountCode, "")>
      <cfreturn false>
    </cfif>
  
    <cfif Not CompareNoCase(this.discountCode, Arguments.discountCode)>
      <cfset this.discountApplied=1>
      <cfif Not CompareNoCase(this.discountType, "percentage")>
        <cfset this.discountAmount=NumberFormat((this.discountRate * this.cost) / 100, ".99")>
      <cfelse>
        <cfset this.discountAmount=this.discountRate>
      </cfif>    
      <cfreturn true>
    <cfelse>
      <cfreturn false>
    </cfif>
  </cffunction>
  
  <cffunction name="removeDiscountCode" access="public" output="no" returntype="boolean">
    <cfargument name="discountCode" type="string" required="yes">
    
    <cfif Not Compare(this.discountCode,"") Or Not Compare(Arguments.discountCode, "")>
      <cfreturn false>
    </cfif>
  
    <cfif Not CompareNoCase(this.discountCode, Arguments.discountCode)>
      <cfset this.discountApplied=0>
      <cfset this.discountAmount=0>
      <cfreturn true>
    <cfelse>
      <cfreturn false>
    </cfif>
  </cffunction>
</cfcomponent>