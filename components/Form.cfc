<cfcomponent displayname="Form Manager" extends="Base" output="false">

  <cffunction name="getGroupInfoByFormID" access="public" output="no" returntype="query">
    <cfargument name="formID" required="yes" type="numeric">
    
	<cfquery name="qGroupInfo" datasource="#this.datasource#">
	  SELECT groupID, formID, displaySeq
	  FROM fm_group
	  WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
	  ORDER BY displaySeq
	</cfquery>
	<cfreturn qGroupInfo>
  </cffunction>

  <cffunction name="getFormProperties" access="public" output="no" returntype="query">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="publishedOnly" type="boolean" default="1">
    
    <cfquery name="qForm" datasource="#this.datasource#">
      SELECT *
      FROM fm_form
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
      		<cfif Arguments.publishedOnly>
              AND published=1
            </cfif>
    </cfquery>
    
    <cfreturn qForm>    
  </cffunction>
  
  <cffunction name="getFormItemInfoByGroupID" access="public" output="no" returntype="query">
    <cfargument name="groupID" type="numeric" required="yes">
    
    <cfquery name="qFormItemInfo" datasource="#this.datasource#">
      SELECT *
      FROM  fm_formitem
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#"> AND
            active=1     
      ORDER BY displaySeq
    </cfquery>
    
    <cfreturn qFormItemInfo>    
  </cffunction>
  
  <cffunction name="getFormItemInfoByFormID" access="public" output="no" returntype="query">
    <cfargument name="formID" type="numeric" required="yes">
    
    <cfquery name="qFormItemInfo" datasource="#this.datasource#">
      SELECT *
      FROM  fm_formitem JOIN fm_group ON fm_formitem.groupID=fm_group.groupID
      WHERE fm_formitem.formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
            fm_formitem.active=1     
      ORDER BY fm_group.displaySeq, fm_formitem.displaySeq
    </cfquery>
    
    <cfreturn qFormItemInfo>    
  </cffunction>
  
  <cffunction name="getFormIDByGroupID" access="public" output="no" returntype="numeric">
    <cfargument name="groupID" type="numeric" required="yes">
    
    <cfquery name="qFormID" datasource="#this.datasource#">
      SELECT formID
      FROM fm_group
      WHERE groupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.groupID#">
    </cfquery>
    <cfif qFormID.recordcount GT 0>
      <cfreturn qFormID.formID>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>

  <cffunction name="getFormBodyHTML" access="public" output="no" returntype="string">
    <cfargument name="formID" type="numeric" required="false">
    <cfargument name="groupID" type="numeric" required="false">
    <cfargument name="submissionID" type="numeric" default="0"><!--- Populate field value for submitted data --->
    <cfargument name="retrieveSavedData" type="boolean" default="0"><!--- Retrieve from save data --->
    
    <cfscript>
    if (IsDefined("Arguments.groupID")) {
      // display form items of a group
      formInfo=getFormProperties(getFormIDByGroupID(Arguments.groupID));
      itemLabelPlacement=formInfo.itemLabelPlacement;
      formItemInfo=getFormItemInfoByGroupID(Arguments.groupID);
    } else if (IsDefined("Arguments.formID")) {  
      // display all form items of the entire form
      formInfo=getFormProperties(Arguments.formID);
      itemLabelPlacement=formInfo.itemLabelPlacement;
      formItemInfo=getFormItemInfoByFormID(Arguments.formID);
    } else {
      return "";
    }
    
    // set up form field default values for submitted data
    hasSubmittedData=false;
    if (Compare(Arguments.submissionID,"0")) {
      if (Arguments.retrieveSavedData) {
        submissionBasicInfo=getSubmissionBasicInfoInSavedTable(Arguments.submissionID);
        submittedData=getSubmittedDataInSavedTable(Arguments.submissionID);
      } else {
        submissionBasicInfo=getSubmissionBasicInfo(Arguments.submissionID);
        submittedData=getSubmittedData(Arguments.submissionID);
      }
      
      if (submittedData.recordcount GT 0 And submissionBasicInfo.recordcount GT 0) {
        Variables.userEmail=submissionBasicInfo.userEmail;
        Variables.userFirstName=submissionBasicInfo.userFirstName;
        Variables.userLastName=submissionBasicInfo.userLastName;
        hasSubmittedData=true;
        submittedValue=StructNew();
        for (idx=1; idx LTE formItemInfo.recordcount; idx = idx + 1) {
      	  if (Compare(formItemInfo.fieldType[idx],"Text Only")) {
            submittedValue["f#formItemInfo.formItemID[idx]#"]="";
          }
        }
		for (idx=1; idx LTE submittedData.recordcount; idx = idx + 1) {
		  submittedValue["f#submittedData.formItemID[idx]#"]=submittedData.valueText[idx];
		}
      }
	}    
    </cfscript>
    
    <cfset newline="#Chr(13)##Chr(10)#">
    <cfsavecontent variable="HTMLCodes">
      <cfoutput query="formItemInfo">
      <cfif applyValidation OR mandatory>
      <div class="row validate" validate="f#formItemID#,#mandatory#,#applyValidation#,#fieldType#,#fieldContext#,#dataType#,#numRequiredOptions#" label="#HTMLEditFormat(itemLabel)#">
      <cfelse>
      <div class="row">
      </cfif>
      <cfif fieldType Is "Text Only">
        <div class="col-whole-left">#instructionText#</div>
      <cfelse>
        <cfif itemLabelPlacement Is "Top">
          <div class="col-whole-left">
            <cfif Compare(itemLabel,"")><label for="label#formItemID#"><cfif boldItemLabel><b>#itemLabel#</b><cfelse>#itemLabel#</cfif></label><br /></cfif>
        <cfelseif itemLabelPlacement Is "Left">
          <div class="col-first-left"><label for="label#formItemID#"><cfif Compare(itemLabel,"")><cfif boldItemLabel><b>#itemLabel#</b><cfelse>#itemLabel#</cfif><cfelse>&nbsp;</cfif></label></div>
          <div class="col-second-left">
        <cfelse>
          <div class="col-first-right"><label for="label#formItemID#"><cfif Compare(itemLabel,"")><cfif boldItemLabel><b>#itemLabel#</b><cfelse>#itemLabel#</cfif><cfelse>&nbsp;</cfif></label></div>
          <div class="col-second-left">
        </cfif>
      
        <cfswitch expression="#fieldType#">
          <cfcase value="Short Text">
            <cfif fieldContext Is "Email">
              <input type="hidden" name="emailFieldID" value="#formItemID#">
              <input type="hidden" name="f#formItemID#" value="">
              <cfif hasSubmittedData>
              <input type="text" name="email" id="label#formItemID#" class="#fieldSize#" value="#HTMLEditFormat(Variables.userEmail)#" />
              <cfelse>
              <input type="text" name="email" id="label#formItemID#" class="#fieldSize#" value="#HTMLEditFormat(shortTextDefault)#" />
              </cfif>
            <cfelseif fieldContext Is "Full Name">
              <input type="hidden" name="nameFieldID" value="#formItemID#">
              <input type="hidden" name="f#formItemID#" value="">
              <div class="col-name">
                <cfif hasSubmittedData>
                <input type="text" name="firstName" id="label#formItemID#" class="Small" value="#HTMLEditFormat(Variables.userFirstName)#" /><br />
                <cfelse>
                <input type="text" name="firstName" id="label#formItemID#" class="Small" value="#HTMLEditFormat(shortTextDefault)#" /><br />
                </cfif>
                <small>(First Name)</small></div>
              <div class="col-name">
                <cfif hasSubmittedData>
                <input type="text" name="lastName" id="label#formItemID#" class="Small" value="#HTMLEditFormat(Variables.userLastName)#" /><br />
                <cfelse>
                <input type="text" name="lastName" id="label#formItemID#" class="Small" value="#HTMLEditFormat(shortTextDefault)#" /><br />
                </cfif>
                <small>(Last Name)</small></div>
            <cfelseif fieldContext Is "Date">
              <cfif hasSubmittedData>
              <input type="text" name="f#formItemID#" id="label#formItemID#" class="Small JQueryDate" value="#HTMLEditFormat(submittedValue["f#formItemID#"])#" />
              <cfelse>
              <input type="text" name="f#formItemID#" id="label#formItemID#" class="Small JQueryDate" value="#HTMLEditFormat(shortTextDefault)#" />
              </cfif>
            <cfelse>
              <cfif hasSubmittedData>
              <input type="text" name="f#formItemID#" id="label#formItemID#" class="#fieldSize#" value="#HTMLEditFormat(submittedValue["f#formItemID#"])#" />
              <cfelse>
              <input type="text" name="f#formItemID#" id="label#formItemID#" class="#fieldSize#" value="#HTMLEditFormat(shortTextDefault)#" />
              </cfif>
            </cfif>
            <cfif Compare(itemNotes, "")><br /><small>(#itemNotes#)</small></cfif>
          </cfcase>
          <cfcase value="Long Text">
            <cfif hasSubmittedData>
            <textarea name="f#formItemID#" id="label#formItemID#" class="#fieldSize#">#HTMLEditFormat(submittedValue["f#formItemID#"])#</textarea>
            <cfelse>
            <textarea name="f#formItemID#" id="label#formItemID#" class="#fieldSize#">#HTMLEditFormat(longTextDefault)#</textarea>
            </cfif>
            <cfif Compare(itemNotes, "")><br /><small>(#itemNotes#)</small></cfif>
          </cfcase>
          <cfcase value="Radio Group">
            <cfif hasSubmittedData>
              <cfloop index="option" list="#optionList#" delimiters="#newline#">
              <input type="radio" name="f#formItemID#" class="radio" value="#HTMLEditFormat(option)#" <cfif Not Compare(submittedValue["f#formItemID#"], option)>checked</cfif> /> #HTMLEditFormat(option)#
              </cfloop>
            <cfelse>
              <cfloop index="option" list="#optionList#" delimiters="#newline#">
              <input type="radio" name="f#formItemID#" class="radio" value="#HTMLEditFormat(option)#" <cfif Not Compare(shortTextDefault, option)>checked</cfif> /> #HTMLEditFormat(option)#
              </cfloop>
            </cfif>
            <cfif Compare(itemNotes, "")><small>(#itemNotes#)</small></cfif>
          </cfcase>
          <cfcase value="Check Boxes">
            <cfif hasSubmittedData>
              <cfloop index="option" list="#optionList#" delimiters="#newline#">
              <input type="checkbox" name="f#formItemID#" class="checkbox" value="#HTMLEditFormat(option)#" <cfif ListFind(submittedValue["f#formItemID#"], option, ",") GT 0>checked</cfif> />
              #HTMLEditFormat(option)#<br />
              </cfloop>
            <cfelse>
              <cfloop index="option" list="#optionList#" delimiters="#newline#">
              <input type="checkbox" name="f#formItemID#" class="checkbox" value="#HTMLEditFormat(option)#" <cfif ListFind(longTextDefault, option, "#newline#") GT 0>checked</cfif> />
              #HTMLEditFormat(option)#<br />
              </cfloop>
            </cfif>
            <cfif Compare(itemNotes, "")><small>(#itemNotes#)</small></cfif>
          </cfcase>
          <cfcase value="Drop-Down Menu">
            <select name="f#formItemID#">
              <option value=""></option>
              <cfif hasSubmittedData>
                <cfloop index="option" list="#optionList#" delimiters="#newline#">
                <option value="#HTMLEditFormat(option)#" <cfif Not Compare(submittedValue["f#formItemID#"], option)>selected</cfif>>#HTMLEditFormat(option)#</option>
                </cfloop>
              <cfelse>
                <cfloop index="option" list="#optionList#" delimiters="#newline#">
                <option value="#HTMLEditFormat(option)#" <cfif Not Compare(shortTextDefault, option)>selected</cfif>>#HTMLEditFormat(option)#</option>
                </cfloop>
              </cfif>
            </select>
            <cfif Compare(itemNotes, "")><br /><small>(#itemNotes#)</small></cfif>
          </cfcase>
          <cfdefaultcase></cfdefaultcase>
        </cfswitch>
        </div>
      </cfif>
      </div>
      </cfoutput>
    </cfsavecontent>

    <cfreturn HTMLCodes>
  </cffunction>
  
  <cffunction name="hasEmailField" access="public" output="no" returntype="boolean">
    <cfargument name="formID" type="numeric" required="yes">
    
    <cfquery name="qEmailField" datasource="#this.datasource#">
      SELECT formItemID
      FROM fm_formitem
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
      		fieldContext='Email' AND
            active=1
    </cfquery>
    <cfif qEmailField.recordcount GT 0>
      <cfreturn true>
    <cfelse>
      <cfreturn false>
    </cfif>
  </cffunction>
  
  <cffunction name="hasNameField" access="public" output="no" returntype="boolean">
    <cfargument name="formID" type="numeric" required="yes">
    
    <cfquery name="qNameField" datasource="#this.datasource#">
      SELECT formItemID
      FROM fm_formitem
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
      		fieldContext='Full Name' AND
            active=1
    </cfquery>
    <cfif qNameField.recordcount GT 0>
      <cfreturn true>
    <cfelse>
      <cfreturn false>
    </cfif>
  </cffunction>

  <cffunction name="submitForm" access="public" output="no" returntype="numeric">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="firstName" type="string" default="">
    <cfargument name="lastName" type="string" default="">
    <cfargument name="email" type="string" default="">
    <cfargument name="sendReceiptToUser" type="boolean" default="0">
    <cfargument name="userIPAddress" type="string" default="">
    <cfargument name="submittedByAdmin" type="boolean" default="0">
    
    <cfset formInfo = getFormProperties(arguments.formID)>
	<cfset formItemsInfo = getFormItemInfoByFormID(arguments.formID)>
  	<cfset Variables.dateSubmitted=now()>

    
	<!--- BEGIN: Insert New Submission --->
	<cfquery datasource="#this.datasource#">
	  INSERT INTO fm_submission
	   (formID, userEmail, userFirstName, userLastName, userIPAddress,
	    dateSubmitted, dateLastModified)
	  VALUES (
	    <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.firstName#">,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.lastName#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.userIPAddress#">,
		
		<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.dateSubmitted#">,
		<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.dateSubmitted#">	
	  )
	</cfquery>
    
	<cfquery name="qSubmission" datasource="#this.datasource#">
	  SELECT MAX(submissionID) AS submissionID
	  FROM fm_submission
	</cfquery>
	<cfset Variables.submissionID=qSubmission.submissionID>
    
	<!--- END: Insert New Submission --->
	<cfset newline=Chr(13) & Chr(10)>
    
	<cfset initialSubmittedHTMLContent='<table border="0" width="100%" cellpadding="5" cellspacing="1" bgcolor="##eeeeee" style="font-family:Arial;font-size:11px;">'>
	<cfset initialSubmittedTextContent = "">
    
	<cfloop query="formItemsInfo">
	  <cfif Compare(fieldType, "Text Only")>
	    <cfif IsDefined("Arguments.f#formItemID#")>
		  <cfset Variables.valueText=Arguments["f#formItemID#"]>
		  <cfquery datasource="#this.datasource#">
		    INSERT INTO fm_submitteddata
			  (formID, submissionID, formItemID, valueText)
			VALUES (
			   <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">,
			   <cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.submissionID#">,
			   <cfqueryparam cfsqltype="cf_sql_integer" value="#formItemID#">,
			   <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Variables.valueText#">
			 )			  
		  </cfquery>
        <cfelse><!--- empty field --->
          <cfset Variables.valueText="">       	
		</cfif>
        
        <cfset initialSubmittedTextContent = initialSubmittedTextContent & formItemsInfo.itemLabel & ": " & Variables.valueText & newline>
        
        <cfif Compare(Variables.valueText,"")>
          <cfset Variables.valueText=HTMLEditFormat(Variables.valueText)>
        <cfelse>
		  <cfset Variables.valueText="&nbsp;">
		</cfif>
        <cfif Compare(formItemsInfo.itemLabel,"")>
		  <cfset Variables.itemLabel=HTMLEditFormat(formItemsInfo.itemLabel)>
		<cfelse>
		  <cfset Variables.itemLabel="&nbsp;">
		</cfif>
        
        <cfset initialSubmittedHTMLContent = initialSubmittedHTMLContent & newline &
		  									 '<tr valign="top" bgcolor="##ffffff">' &
		  									 '  <td width="35%" align="right">' & Variables.itemLabel & '</td>' &
											 '  <td width="65%">' & Variables.valueText & '</td>' &
											 '</tr>'>
      <cfelse><!--- Text Only (instructional text) --->
        <cfset initialSubmittedHTMLContent = initialSubmittedHTMLContent & newline &
		  									 '<tr valign="top" bgcolor="##ffffff">' &
		  									 '  <td colspan="2">' & instructionText & '</td>' &
											 '</tr>'>
        <cfset initialSubmittedTextContent = initialSubmittedTextContent & instructionText & newline>
	  </cfif>	
	</cfloop>
	<cfset initialSubmittedHTMLContent = initialSubmittedHTMLContent & Chr(13) & Chr(10) & "</table>">
  
    <cfquery datasource="#this.datasource#">
	  UPDATE fm_submission
	  SET initialSubmittedHTMLContent=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#initialSubmittedHTMLContent#">,
      	  initialSubmittedtextContent=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#initialSubmittedtextContent#">
	  WHERE submissionID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.submissionID#">
	</cfquery> 
	
	<cfif Not Arguments.submittedByAdmin And formInfo.sendNotificationEmail And Compare(formInfo.notificationEmailTo, "")>
      <!--- send notification email to form admin --->
	  <cfif formInfo.useUserEmailAsSender And Compare(Arguments.email, "")>
	    <cfset Variables.senderEmailAddress=Arguments.email>
	  <cfelse>
	    <cfset Variables.senderEmailAddress=formInfo.notificationEmailFrom>
	  </cfif>
      
      <cfif Compare(Variables.senderEmailAddress,"")>
        <!--- <cftry> --->  
        <cfmail from="#Variables.senderEmailAddress#" to="#formInfo.notificationEmailTo#" subject="#formInfo.notificationEmailSubject#" type="html">
          <cfmailpart type="text">
#initialSubmittedTextContent#	
          </cfmailpart>
          <cfmailpart type="html" charset="utf-8">
          <table border="0" cellpadding="0" cellspacing="0" width="600" style="font-family:Arial;font-size:11px;"><tr valign="top"><td>
            #initialSubmittedHTMLContent#
          </td></tr></table>
          </cfmailpart>	
        </cfmail>
        <!--- <cfcatch></cfcatch>
        </cftry> --->
      </cfif>
	</cfif>
    
    <cfif Arguments.sendReceiptToUser And Compare(formInfo.receiptEmailFrom,"") And Compare(Arguments.email, "")>
      <!--- send confirmation email to user --->
      <!--- <cftry> --->
      <cfmail from="#formInfo.receiptEmailFrom#" to="#Arguments.email#" subject="#formInfo.receiptEmailSubject#" type="html">
        <cfmailpart type="text">
		  <cfif Compare(formInfo.headerMessageInReceipt,"")>
#formInfo.headerMessageInReceipt#

		  </cfif>
		  <cfif formInfo.includeSubmissionInReciept>
#initialSubmittedTextContent#
		  </cfif>
        </cfmailpart>
        <cfmailpart type="html" charset="utf-8">
          <table border="0" cellpadding="0" cellspacing="0" width="600" style="font-family:Arial;font-size:11px;"><tr valign="top"><td>
          <cfif Compare(formInfo.headerMessageInReceipt,"")>
            #formInfo.headerMessageInReceipt#<br /><br />
          </cfif>
          <cfif formInfo.includeSubmissionInReciept>
            #initialSubmittedHTMLContent#
          </cfif>
          </td></tr></table>
        </cfmailpart>
      </cfmail>
      <!--- <cfcatch></cfcatch>
      </cftry> --->
    </cfif>
	  
    <cfreturn Variables.submissionID>    
  </cffunction>
  
  <cffunction name="saveForm" access="public" output="no" returntype="numeric">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="firstName" type="string" default="">
    <cfargument name="lastName" type="string" default="">
    <cfargument name="email" type="string" default="">
    <cfargument name="userIPAddress" type="string" default="">
    <cfargument name="emailForSavedData" type="string" default="">
    <cfargument name="passwordForSavedData" type="string" default="">
    
    <cfset formInfo = getFormProperties(arguments.formID)>
	<cfset formItemsInfo = getFormItemInfoByFormID(arguments.formID)>
  	<cfset Variables.dateSubmitted=now()>
    
    <!--- BEGIN: delete old saved data --->
    <cfquery name="qOldSumissionIDs" datasource="#this.datasource#">
      SELECT submissionID
      FROM fm_submission_saved
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
      		emailForSavedData=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailForSavedData#"> AND
            passwordForSavedData=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.passwordForSavedData#">
    </cfquery>
    <cfif qOldSumissionIDs.recordcount GT 0>
      <cfquery datasource="#this.datasource#">
        DELETE from fm_submission_saved
        WHERE submissionID IN (#ValueList(qOldSumissionIDs.submissionID)#)
      </cfquery>
      <cfquery datasource="#this.datasource#">
        DELETE FROM fm_submitteddata_saved
        WHERE submissionID IN (#ValueList(qOldSumissionIDs.submissionID)#)
      </cfquery>
    </cfif>
    <!--- END: delete old saved data --->

    
	<!--- BEGIN: Insert New Submission --->
	<cfquery datasource="#this.datasource#">
	  INSERT INTO fm_submission_saved
	   (formID, userEmail, userFirstName, userLastName, userIPAddress, emailForSavedData, passwordForSavedData,
	    dateSubmitted, dateLastModified)
	  VALUES (
	    <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.firstName#">,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.lastName#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.userIPAddress#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailForSavedData#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.passwordForSavedData#">,
		
		<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.dateSubmitted#">,
		<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Variables.dateSubmitted#">	
	  )
	</cfquery>
    
	<cfquery name="qSubmission" datasource="#this.datasource#">
	  SELECT MAX(submissionID) AS submissionID
	  FROM fm_submission_saved
	</cfquery>
	<cfset Variables.submissionID=qSubmission.submissionID>
    
	<!--- END: Insert New Submission --->
	<cfset newline=Chr(13) & Chr(10)>
    
	<cfloop query="formItemsInfo">
	  <cfif Compare(fieldType, "Text Only")>
	    <cfif IsDefined("Arguments.f#formItemID#")>
		  <cfset Variables.valueText=Arguments["f#formItemID#"]>
		  <cfquery datasource="#this.datasource#">
		    INSERT INTO fm_submitteddata_saved
			  (formID, submissionID, formItemID, valueText)
			VALUES (
			   <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">,
			   <cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.submissionID#">,
			   <cfqueryparam cfsqltype="cf_sql_integer" value="#formItemID#">,
			   <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Variables.valueText#">
			 )			  
		  </cfquery>   	
		</cfif>
      </cfif>
	</cfloop>
	  
    <cfreturn Variables.submissionID>    
  </cffunction>
  
  <cffunction name="getSubmissionBasicInfo" access="public" output="no" returntype="query">
    <cfargument name="submissionID" type="string" required="yes">
    
    <cfquery name="qSubmission" datasource="#this.datasource#">
      SELECT *
      FROM fm_submission
      WHERE submissionID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.submissionID#">
    </cfquery>
    
    <cfreturn qSubmission>
  </cffunction>
  
  <cffunction name="getSubmissionBasicInfoInSavedTable" access="public" output="no" returntype="query">
    <cfargument name="submissionID" type="string" required="yes">
    
    <cfquery name="qSubmission" datasource="#this.datasource#">
      SELECT *
      FROM fm_submission_saved
      WHERE submissionID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.submissionID#">
    </cfquery>
    
    <cfreturn qSubmission>
  </cffunction>
  
  <cffunction name="getSubmittedData" access="public" output="no" returntype="query">
    <cfargument name="submissionID" type="string" required="yes">
    
    <cfquery name="qSubmission" datasource="#this.datasource#">
      SELECT formItemID, valueText
      FROM fm_submitteddata
      WHERE submissionID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.submissionID#">
    </cfquery>
    
    <cfreturn qSubmission>
  </cffunction>
  
  <cffunction name="getSubmittedDataInSavedTable" access="public" output="no" returntype="query">
    <cfargument name="submissionID" type="string" required="yes">
    
    <cfquery name="qSubmission" datasource="#this.datasource#">
      SELECT formItemID, valueText
      FROM fm_submitteddata_saved
      WHERE submissionID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.submissionID#">
    </cfquery>
    
    <cfreturn qSubmission>
  </cffunction>
  
  <cffunction name="sendConfirmationEmailForRetrievingData" access="public" output="no">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="emailForSavedData" type="string" required="yes">
    <cfargument name="passwordForSavedData" type="string" required="yes">
    <cfargument name="formURL" type="string" required="yes">
    <cfargument name="dataRetrievalURL" type="string" required="yes">
    
    <cfset formInfo=getFormProperties(Arguments.formID)>
    
    <cfmail from="#formInfo.dataRetrievalSenderEmailAddress#" to="#Arguments.emailForSavedData#" subject="#formInfo.formName# - your data has been saved" type="html">
    <cfmailpart type="text">
The information you entered to the form "#formInfo.formName#" has been saved. You can retrieve your saved data by visiting the following URL:<br><br>

#dataRetrievalURL#

You can also go to the form and enter the e-mail address and retrieve code to retrieve your saved data:

Form URL: #formURL#
E-mail address: #Arguments.emailForSavedData#
Retrieve Code: #Arguments.passwordForSavedData#
	</cfmailpart>
    <cfmailpart type="html">
    <div style="font-family:Arial;font-size:12px">
    The information you entered to the form "#formInfo.formName#" has been saved. You can retrieve your saved data by clicking on the following URL:<br><br>
    
    <a href="#dataRetrievalURL#">#dataRetrievalURL#</a><br><br>
    
    You can also go to the form and enter the e-mail address and retrieve code to retrieve your saved data:<br><br>
    
    Form URL: #formURL#<br>
    E-mail address: #Arguments.emailForSavedData#<br>
    Retrieve Code: #Arguments.passwordForSavedData#    
    </div>
    </cfmailpart>
    </cfmail>  
  </cffunction>
  
  <cffunction name="retrieveSavedSubmissionID" access="public" output="no" returntype="numeric">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="emailForSavedData" type="string" required="yes">
    <cfargument name="passwordForSavedData" type="string" required="yes">
    
    <cfquery name="qSavedSubmissionID" datasource="#this.datasource#">
      SELECT submissionID
      FROM fm_submission_saved
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
      		emailForSavedData=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailForSavedData#"> AND
            passwordForSavedData=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.passwordForSavedData#">
      ORDER BY dateSubmitted DESC
    </cfquery>
    
    <cfif qSavedSubmissionID.recordcount GT 0>
      <cfreturn qSavedSubmissionID.submissionID>
    <cfelse>
      <cfreturn 0>
    </cfif>
  </cffunction>
  
  <cffunction name="retrieveRetrieveCode" access="public" output="false" returntype="boolean">
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="emailForSavedData" type="string" required="yes">
    
    <cfquery name="qRetrieveCode" datasource="#this.datasource#">
      SELECT passwordForSavedData
      FROM fm_submission_saved
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#"> AND
      		emailForSavedData=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailForSavedData#">
      ORDER BY dateSubmitted DESC
    </cfquery>
    
    <cfif qRetrieveCode.recordcount GT 0>
      <cfset formInfo=getFormProperties(Arguments.formID)>
    
      <cfmail from="#formInfo.dataRetrievalSenderEmailAddress#" to="#Arguments.emailForSavedData#" subject="#formInfo.formName# - your data retrieve code" type="html">
        <cfmailpart type="text">
The e-mail address and code you need to retrieve your saved form data is:<br><br>

E-mail address: #Arguments.emailForSavedData#
Retrieve Code: #qRetrieveCode.passwordForSavedData[1]#
        </cfmailpart>
        <cfmailpart type="html">
        <div style="font-family:Arial;font-size:12px">
        The e-mail address and code you need to retrieve your saved form data is:<br><br>
        
        E-mail address: #Arguments.emailForSavedData#<br>
        Retrieve Code: #qRetrieveCode.passwordForSavedData[1]#
        </div>
        </cfmailpart>
      </cfmail>    
      <cfreturn true>
    <cfelse>
      <cfreturn false>
    </cfif> 
  </cffunction>
</cfcomponent>