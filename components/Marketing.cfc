<cfcomponent displayname="Viral Marketing" extends="Base" output="no">
  <cffunction name="getStats" access="public" output="no" returntype="query">
    <cfargument name="reportYear" type="numeric" required="yes">

    <cfquery name="qStats" datasource="#this.datasource#">
      SELECT *
      FROM vm_stats
      WHERE reportYear = <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.reportYear#">
      ORDER BY reportMonth DESC
    </cfquery>
    <cfreturn qStats>
  </cffunction>

  <cffunction name="addCount" access="public" output="no">
    <cfargument name="countType" type="string" required="yes">
    
    <cfset reportYear=Year(now())>
    <cfset reportMonth=Month(now())>
    
    <cfquery name="qReport" datasource="#this.datasource#">
      SELECT *
      FROM vm_stats
      WHERE reportYear=<cfqueryparam cfsqltype="cf_sql_integer" value="#reportYear#"> AND
      		reportMonth=<cfqueryparam cfsqltype="cf_sql_integer" value="#reportMonth#"> 
    </cfquery>
    
    <cfif qReport.recordcount GT 0>
      <cfquery datasource="#this.datasource#">
        UPDATE vm_stats
        <cfif Not CompareNoCase(Arguments.countType,"Email")>
        SET numPagesEmailed = numPagesEmailed + 1
        <cfelse>
        SET numPagesPrinted = numPagesPrinted + 1
        </cfif>
        WHERE reportYear=<cfqueryparam cfsqltype="cf_sql_integer" value="#reportYear#"> AND
      		  reportMonth=<cfqueryparam cfsqltype="cf_sql_integer" value="#reportMonth#"> 
      </cfquery>
    <cfelse>
      <cfif Not CompareNoCase(Arguments.countType,"Email")>
        <cfset numPagesEmailed = 1>
        <cfset numPagesPrinted = 0>
      <cfelse>
        <cfset numPagesEmailed = 0>
        <cfset numPagesPrinted = 1>
      </cfif>
        
      <cfquery datasource="#this.datasource#">
        INSERT INTO vm_stats (numPagesEmailed, numPagesPrinted, reportYear, reportMonth)
        VALUES (
          <cfqueryparam cfsqltype="cf_sql_integer" value="#numPagesEmailed#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#numPagesPrinted#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#reportYear#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#reportMonth#">
        )
      </cfquery>
    </cfif>
  </cffunction>
  
  <cffunction name="getEmailMessage" access="public" output="no" returntype="string">
    <cfquery name="qMessage" datasource="#this.datasource#">
      SELECT message
      FROM vm_message 
    </cfquery>
    
    <cfif qMessage.recordcount GT 0>
      <cfreturn qMessage.message>
    <cfelse>
      <cfreturn "">
    </cfif>
  </cffunction>
  
  <cffunction name="updateEmailMessage" access="public" output="no" returntype="string">
    <cfargument name="message" type="string" required="yes">
    
    <cfquery datasource="#this.datasource#">
      UPDATE vm_message
      SET message=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.message#"> 
    </cfquery>
  </cffunction>
</cfcomponent>