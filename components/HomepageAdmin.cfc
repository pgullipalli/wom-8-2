<cfcomponent displayname="Homepage Admin" extends="Base" output="false">
  <cffunction name="getHomepageInfo" access="public" output="no" returntype="query">
    <cfquery name="qHomepageInfo" datasource="#this.datasource#">
      SELECT * FROM hp_homepage
    </cfquery>
    <cfreturn qHomepageInfo>
  </cffunction>
  
  <cffunction name="updateHomepageInfo" access="public" output="no">
    <cfargument name="podImage1" type="string" default="">
    <cfargument name="podImageOver1" type="string" default="">
    <cfargument name="podImage2" type="string" default="">
    <cfargument name="podImageOver2" type="string" default="">
    <cfargument name="podImage3" type="string" default="">
    <cfargument name="podImageOver3" type="string" default="">
    <cfargument name="podImageAltText1" type="string" default="">
    <cfargument name="podImageAltText2" type="string" default="">
    <cfargument name="podImageAltText3" type="string" default="">
    <cfargument name="subHomePageID1" type="numeric" required="yes">
    <cfargument name="subHomePageID2" type="numeric" required="yes">
    <cfargument name="subHomePageID3" type="numeric" required="yes">  
    <cfargument name="classCategoryID1" type="numeric" required="yes">
    <cfargument name="classCategoryID2" type="numeric" required="yes">
    <cfargument name="classCategoryID3" type="numeric" required="yes">
    <cfargument name="articleCategoryID1" type="numeric" required="yes">
    <cfargument name="articleCategoryID2" type="numeric" required="yes">
    <cfargument name="articleCategoryID3" type="numeric" required="yes">    
    
    <cfquery datasource="#this.datasource#">
      UPDATE hp_homepage
      SET podImage1=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.podImage1#">,
      	  podImageOver1=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.podImageOver1#">,
          podImage2=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.podImage2#">,
          podImageOver2=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.podImageOver2#">,
          podImage3=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.podImage3#">,
          podImageOver3=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.podImageOver3#">,
          podImageAltText1=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.podImageAltText1#">,
          podImageAltText2=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.podImageAltText2#">,
          podImageAltText3=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.podImageAltText3#">,
      	  subHomePageID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.subHomePageID1#">,
          subHomePageID2=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.subHomePageID2#">,
          subHomePageID3=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.subHomePageID3#">,
      	  classCategoryID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classCategoryID1#">,
      	  classCategoryID2=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classCategoryID2#">,
          classCategoryID3=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classCategoryID3#">,
          articleCategoryID1=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID1#">,
      	  articleCategoryID2=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID2#">,
          articleCategoryID3=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID3#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
    </cfquery>
  </cffunction>
  
  <cffunction name="getAllSubHomePages" access="public" output="no" returntype="query">
    <cfquery name="qAllPages" datasource="#this.datasource#">
      SELECT subHomePageID, published, subHomePageName
      FROM hp_subhomepage
      ORDER BY subHomePageName
    </cfquery>
    <cfreturn qAllPages> 
  </cffunction>
  
  <cffunction name="getSubHomepage" access="public" output="no" returntype="query">
    <cfargument name="subHomePageID" type="numeric" required="yes">
    <cfquery name="qSubHomePage" datasource="#this.datasource#">
      SELECT *
      FROM hp_subhomepage
      WHERE subHomePageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.subHomePageID#">
    </cfquery>
    <cfreturn qSubHomePage>
  </cffunction>
  
  <cffunction name="addSubHomePage" access="public" output="no">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="subHomePageName" type="string" required="yes">
    <cfargument name="welcomeMessage" type="string" default="">
    <cfargument name="articleCategoryID" type="numeric" default="0">
    <cfargument name="showFeatureClass" type="boolean" default="0">
    <cfargument name="featureClassIntro" type="string" default="">
    <cfargument name="classCategoryID" type="numeric" default="0">
    <cfargument name="showfeatureAlbum" type="boolean" default="0">
    <cfargument name="albumCategoryID" type="numeric" default="0">
    
    <cfquery datasource="#this.datasource#">
      INSERT INTO hp_subhomepage
      (published, subHomePageName, welcomeMessage, articleCategoryID, showFeatureClass, featureClassIntro,
       classCategoryID, showfeatureAlbum, albumCategoryID, dateCreated, dateLastModified)
      VALUES
      (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.subHomePageName#">,
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.welcomeMessage#">,
       <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID#">,
       <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.showFeatureClass#">,
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.featureClassIntro#">,
          
       <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classCategoryID#">,
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.showfeatureAlbum#">,
       <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumCategoryID#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)       
    </cfquery>
  </cffunction>
  
  <cffunction name="editSubHomePage" access="public" output="no">
    <cfargument name="subHomePageID" type="numeric" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="subHomePageName" type="string" required="yes">
    <cfargument name="welcomeMessage" type="string" default="">
    <cfargument name="articleCategoryID" type="numeric" default="0">
    <cfargument name="showFeatureClass" type="boolean" default="0">
    <cfargument name="featureClassIntro" type="string" default="">
    <cfargument name="classCategoryID" type="numeric" default="0">
    <cfargument name="showfeatureAlbum" type="boolean" default="0">
    <cfargument name="albumCategoryID" type="numeric" default="0">
    
    <cfquery datasource="#this.datasource#">
      UPDATE hp_subhomepage
      SET published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
      	  subHomePageName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.subHomePageName#">,
          welcomeMessage=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.welcomeMessage#">,
          articleCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.articleCategoryID#">,
          showFeatureClass=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.showFeatureClass#">,          
          featureClassIntro=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.featureClassIntro#">,          
          classCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.classCategoryID#">,
          showfeatureAlbum=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.showfeatureAlbum#">,
          albumCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumCategoryID#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE subHomePageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.subHomePageID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="deleteSubHomePage" access="public" output="no">
    <cfquery datasource="#this.datasource#">
      DELETE FROM hp_subhomepage
      WHERE subHomePageID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.subHomePageID#">
    </cfquery>
  </cffunction>
  
  
</cfcomponent>