<cfcomponent hint="Used for administration session" extends="Base" output="false">
  <cfparam name="this.isLoggedIn" type="boolean" default="0">
  <cfparam name="this.message" type="string" default="">  
  <cfparam name="this.name" type="string" default="">
  <cfparam name="this.username" type="string" default="">
  <cfparam name="this.administratorID" type="numeric" default="0">
  <cfparam name="this.isSuperUser" type="boolean" default="0">
  <cfparam name="this.isMightyUser" type="boolean" default="0">
  <!---<cfparam name="this.privilegeXMLObj" type="xml">--->
  <!---<cfparam name="this.adminPrivilegeXMLObj" type="xml">--->
  
  <cffunction name="login" access="public" output="false" returntype="boolean">
    <cfargument name="username" type="string" required="yes">
	<cfargument name="password" type="string" required="yes">
	
	<cfquery name="getLogin" datasource="#this.datasource#">
	  select administratorID, firstName, lastName, username, isSuperUser, isMightyUser
	  from am_administrator
	  where username=<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.username#"> AND
	  		password=<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.password#">
	</cfquery>
	
	<cfif getLogin.recordcount IS 0>
	  <cfreturn false>
	</cfif>
	
	<cfset this.name = getLogin.firstName & " " & getLogin.lastName>
	<cfset this.administratorID = getLogin.administratorID>
	<cfset this.isSuperUser = getLogin.isSuperUser>
	<cfset this.isMightyUser = getLogin.isMightyUser>
	
	<cfif Not this.isSuperUser AND Not this.isMightyUser>
	  <cfset this.adminPrivilegeXMLObj=getAdminPrivilege()>
	</cfif>	
	
	<cfset this.isLoggedIn = 1>

	<cfreturn true>
  </cffunction>

  <cffunction name="authenticate" access="public" output="false" returntype="struct">
    <cfargument name="md" type="string" default="">
	<cfargument name="tmp" type="string" default="">
	<cfargument name="task" type="string" default="">	
	<cfargument name="catID" type="numeric" default="0">
	
	<cfset var returnedStruct=StructNew()>
	<cfset returnedStruct.success=false>
	<cfset returnedStruct.message="">
		
	<cfif this.isSuperUser Or this.isMightyUser Or Arguments.md Is "">
	  <cfset returnedStruct.success=true>
      <cfreturn returnedStruct>
	</cfif>
    
    <cfif Not IsDefined("this.adminPrivilegeXMLObj") OR Not this.isLoggedIn>
	  <cfset returnedStruct.message='Your session has expired. Please <a href="##" onclick="goToLogin();"><b>log in</b></a> again to renew your session.'>
	  <cfreturn returnedStruct>
	</cfif>
	
	<cfset docRoot=this.adminPrivilegeXMLObj.XmlRoot>
	
	<!--- check 'accessible-to-all' tag --->
	<cfif IsDefined("docRoot.accessible-to-all")>
	  <cfset moduleArray=docRoot["accessible-to-all"].module>
	  <cfloop index="idx" from="1" to="#ArrayLen(moduleArray)#">
	    <cfset moduleObj=moduleArray[idx]>
		<cfif moduleObj.XmlAttributes.name Is Arguments.md>
		  <cfif IsDefined("moduleObj.template")>
		    <cfset templateArray=moduleObj.template>
		    <cfloop index="idx2" from="1" to="#ArrayLen(templateArray)#">
		      <cfif templateArray[idx2].XmlText Is Arguments.tmp>
			    <cfset returnedStruct.success=true>
			    <cfreturn returnedStruct>
			  </cfif>
		    </cfloop>
		  </cfif>
		  <cfif IsDefined("moduleObj.action")>
			<cfset actionArray=moduleObj.action>
		    <cfloop index="idx2" from="1" to="#ArrayLen(actionArray)#">
		      <cfif actionArray[idx2].XmlText Is Arguments.task>
			    <cfset returnedStruct.success=true>
			    <cfreturn returnedStruct>
			  </cfif>
		    </cfloop>			  
		  </cfif>
		</cfif>
      </cfloop>
	</cfif>
	
	<cfset grant=false>
	<cfif IsDefined("docRoot.privilege-group")>
	  <cfset privilegeGroupArray=docRoot["privilege-group"]>
	  <cfloop index="idx1" from="1" to="#ArrayLen(privilegeGroupArray)#">
	    <cfset privilegeGroupObj=privilegeGroupArray[idx1]>
	    <cfset privilegeArray=privilegeGroupObj.privilege>
	    <cfloop index="idx2" from="1" to="#ArrayLen(privilegeArray)#">
	      <cfset privilegeObj=privilegeArray[idx2]>
          <cfset moduleArray=privilegeObj.module>
	      <cfloop index="idx3" from="1" to="#ArrayLen(moduleArray)#">
		    <cfset moduleObj=moduleArray[idx3]>
		    <cfif moduleObj.XmlAttributes.name Is Arguments.md>
		      <cfif IsDefined("moduleObj.XmlAttributes.grantall") And moduleObj.XmlAttributes.grantall>
			    <!--- grant access to all templates and actions of this module --->
		        <cfset returnedStruct.success=true>
		        <cfreturn returnedStruct>		    
		      <cfelse>		        
		        <cfif IsDefined("moduleObj.deny")>	          
		          <cfif IsDefined("moduleObj.deny.template")>		            
			        <cfset templateArray=moduleObj.deny.template>
			        <cfloop index="idx4" from="1" to="#ArrayLen(templateArray)#">
				      <cfset templateObj=templateArray[idx4]>
				      <cfif templateObj.XmlText Is Arguments.tmp>
					    <cfset returnedStruct.success=false>
					    <cfset returnedStruct.message="Sorry, but your request has been denied.">
			    		<cfreturn returnedStruct>
					  </cfif>
				    </cfloop>
				  </cfif>
			      <cfif IsDefined("moduleObj.deny.action")>
			        <cfset actionArray=moduleObj.deny.action>
			        <cfloop index="idx4" from="1" to="#ArrayLen(actionArray)#">
				      <cfset actionObj=actionArray[idx4]>
				      <cfif actionObj.XmlText Is Arguments.task>
					    <cfset returnedStruct.success=false>
					    <cfset returnedStruct.message="Sorry, but your request has been denied.">
			    		<cfreturn returnedStruct>
					  </cfif>
				    </cfloop>
			      </cfif>
			      <cfset grant=true><!--- access has not been denied yet --->		      
		        <cfelseif IsDefined("moduleObj.grant")>
		          <cfif IsDefined("moduleObj.grant.template")>		            
			        <cfset templateArray=moduleObj.grant.template>
			        <cfloop index="idx4" from="1" to="#ArrayLen(templateArray)#">
				      <cfset templateObj=templateArray[idx4]>
				      <cfif templateObj.XmlText Is Arguments.tmp>
					    <cfset grant=true>
					    <cfbreak>
					  </cfif>
				    </cfloop>
				  </cfif>
			      <cfif IsDefined("moduleObj.grant.action")>
			        <cfset actionArray=moduleObj.grant.action>
			        <cfloop index="idx4" from="1" to="#ArrayLen(actionArray)#">
				      <cfset actionObj=actionArray[idx4]>
				      <cfif actionObj.XmlText Is Arguments.task>
					    <cfset grant=true>
					    <cfbreak>
					  </cfif>
				    </cfloop>
			      </cfif>
		        </cfif>
		        
		        <cfif Not grant>
		          <cfset returnedStruct.success=false>
				  <cfset returnedStruct.message="Sorry, but your request has been denied.">
			      <cfreturn returnedStruct>
		        <cfelse>
		          <!--- check category restriction --->
		          <cfif Compare(Arguments.catID,"0") And IsDefined("moduleObj.XmlAttributes.categoryrestriction") And moduleObj.XmlAttributes.categoryrestriction>
		        	<cfif IsDefined("moduleObj.category-id-list") And Compare(moduleObj["category-id-list"].XmlText,"-1")>
			          <cfif ListFind(moduleObj["category-id-list"].XmlText, Arguments.catID) Is 0>
						<cfset returnedStruct.success=false>
					    <cfset returnedStruct.message="You do not have permission to access this category.">
			    		<cfreturn returnedStruct>
					  </cfif>
			        </cfif>
		          </cfif>		          
		        </cfif>
		      </cfif>
		    </cfif>
	      </cfloop>
	    </cfloop>
	  </cfloop>
	</cfif>
	
	<cfif Not grant>
	  <cfset returnedStruct.success=false>
	  <cfset returnedStruct.message="Sorry, but your request has been denied.">
	  <cfreturn returnedStruct>
	<cfelse>
	  <cfset returnedStruct.success=true>
      <cfreturn returnedStruct>
	</cfif>
  </cffunction>

  <cffunction name="getAdminPrivilege" access="public" output="false" returntype="xml">	
	<cfset var resultXMLObj=Duplicate(this.privilegeXMLObj)>
	
	<cfset docRoot=resultXMLObj.XmlRoot>
	<cfset privilegeGroupArray=docRoot["privilege-group"]>
	
	<cfloop index="idx1" from="#ArrayLen(privilegeGroupArray)#" to="1" step="-1">
	  <cfset privilegeGroupID=privilegeGroupArray[idx1].XmlAttributes.id>
	  <cfquery name="qAllPrivilegeGroups" datasource="#this.datasource#">
	    SELECT privilegeGroupID, privilegeID, categoryIDList
	    FROM am_administrator_privilege
	    WHERE administratorID=<cfqueryparam cfsqltype="cf_sql_integer" value="#this.administratorID#"> AND
			  privilegeGroupID=<cfqueryparam cfsqltype="cf_sql_integer" value="#privilegeGroupID#">
	    ORDER by privilegeID
	  </cfquery>
	  <cfif qAllPrivilegeGroups.recordcount IS 0>
	    <!--- NO such privilege group --->
	    <cfset ArrayDeleteAt(privilegeGroupArray, idx1)>
	  <cfelse>
	    <cfset privilegeArray=ArrayNew(1)>
	    <cfset privilegeArray=privilegeGroupArray[idx1].privilege>
	    <cfloop index="idx2" from="#ArrayLen(privilegeArray)#" to="1" step="-1">
	      <cfset privilege=privilegeArray[idx2]>
		  <cfset privilegeID=privilege.XmlAttributes.id>
		  <cfquery name="qPrivilege" dbtype="query">
		    select privilegeID, categoryIDList
			from qAllPrivilegeGroups
			where privilegeID=#privilegeID#
		  </cfquery>
		  <cfif qPrivilege.recordcount IS 0>
		    <!--- No such privilege --->
			<cfset ArrayDeleteAt(privilegeArray, idx2)>
		  <cfelse>
		    <cfset moduleArray=privilege.module>
		    <cfloop index="idx3" from="1" to="#ArrayLen(moduleArray)#">
			  <cfset module=moduleArray[idx3]>
			  <cfif IsDefined("module.XmlAttributes.categoryrestriction")>
			    <cfset module["category-id-list"]=XmlElemNew(resultXMLObj, "category-id-list")>
			    <cfset module["category-id-list"].XmlText=qPrivilege.categoryIDList>		  
			  </cfif>
		    </cfloop>	  					
		  </cfif>  
	    </cfloop>	  
	  </cfif>
	</cfloop>
  
    <cfreturn resultXMLObj>
  </cffunction>



  <cffunction name="getAccessibleModules" access="public" output="false" returntype="string">
    <cfset var moduleList="x">
	<cfset var idx1 = 0>
	<cfset var idx2 = 0>
	<cfset var idx3 = 0>
	<cfif this.isSuperUser><cfreturn ""></cfif>
    
    <cfif IsDefined("this.adminPrivilegeXMLObj")>
	  <cfset docRoot=this.adminPrivilegeXMLObj.XmlRoot>
      <cfset privilegeGroupArray=docRoot["privilege-group"]>
      <cfloop index="idx1" from="1" to="#ArrayLen(privilegeGroupArray)#">
        <cfset privilegeArray=privilegeGroupArray[idx1].privilege>
        <cfloop index="idx2" from="1" to="#ArrayLen(privilegeArray)#">
          <cfset privilegeObj=privilegeArray[idx2]>
          <cfset moduleArray=privilegeObj.module>
          <cfloop index="idx3" from="1" to="#ArrayLen(moduleArray)#">
            <cfset moduleList=moduleList & "," & moduleArray[idx3].XMLAttributes.name>
          </cfloop>		
        </cfloop>
      </cfloop>
    </cfif>
    <cfreturn moduleList>  
  </cffunction>
  
  <cffunction name="setMessage" access="public" output="false">
    <cfargument name="message" type="string" required="yes">
	<cfset this.message = Arguments.message>
  </cffunction>
  
  <cffunction name="getMessage" access="public" output="false" returntype="string">
    <cfargument name="resetMessage" type="numeric" default="0">
	<cfset var msg="">
	<cfset msg=this.message>
	<cfif Arguments.resetMessage>  
	  <cfset this.message="">
	</cfif>   
	<cfreturn msg> 
  </cffunction>
</cfcomponent>