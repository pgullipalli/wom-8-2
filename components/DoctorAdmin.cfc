<cfcomponent displayname="Doctor Directory Admin" extends="Base" output="no">
  <cffunction name="getLastImportFileTimestamp" access="public" output="no" returntype="string">
    <cfquery name="qTimestamp" datasource="#this.datasource#">
      SELECT importFileTimestamp
      FROM dd_import_log
      ORDER BY logID DESC
      LIMIT 0,1
    </cfquery>
  
    <cfif qTimestamp.recordcount GT 0>
      <cfreturn qTimestamp.importFileTimestamp>
    <cfelse>
      <cfreturn now()>
    </cfif>
  </cffunction>
  
  <cffunction name="importDoctorData" access="public" output="no" returntype="string">
    <cfargument name="dataFilePath" type="string" default="">
    <cfargument name="targetAbsoluteDirectory" type="string" default="">
	<cfargument name="localFileFieldName" type="string" default="">
    
    <cfif Compare(Arguments.localFileFieldName,"")>
      <cffile action="upload"
	          destination="#Arguments.targetAbsoluteDirectory#"
	          nameconflict="overwrite"
	          filefield="#Arguments.localFileFieldName#">
      <cfset uploadedFilename=cffile.serverFile>
      <cfset  Arguments.dataFilePath="#Arguments.targetAbsoluteDirectory#/#uploadedFilename#">
      <cfset importFileTimestamp=now()>
    <cfelse>
      <cfset fileObj = CreateObject("java","java.io.File").init(Arguments.dataFilePath)>
      <cfset importFileTimestamp = CreateObject("java","java.util.Date").init(fileObj.lastModified())>
    </cfif>
    
    <cfset importLog="">
    <cfset newline=Chr(13) & Chr(10)>
    <cfset numRecordsimported=0>
    <cfset UT=CreateObject("component","Utility")>
    <cftry>
        <cfset lines=UT.CSVToArray(CSVFilePath="#Arguments.dataFilePath#")>
        <cfquery datasource="#this.datasource#">
          TRUNCATE TABLE dd_doctor
        </cfquery>
        <cfloop index="row" from="2" to="#ArrayLen(lines)#">
          <cfif ArrayLen(lines[row]) LT 20>
            <cfset importLog=importLog & newline & "Line #row# has insufficient number of fields.">
          <cfelse>
            <cftry>
              <cfquery datasource="#this.datasource#">
                INSERT INTO dd_doctor
                (recordNum, lastName, firstName, middleName, degree,
                 languages, officeName, address1, address2, citya,
                 statea, zipa, phone, fax, answering,
                 specialty, boardcert1, boardcert2, boardcert3, officeWebsite,
                 dateLastModified)
                VALUES
                (<cfqueryparam cfsqltype="cf_sql_integer" value="#lines[row][1]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][2]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][3]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][4]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][5]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][6]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][7]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][8]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][9]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][10]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][11]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][12]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][13]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][14]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][15]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][16]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][17]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][18]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][19]#">,
                 <cfqueryparam cfsqltype="cf_sql_varchar" value="#lines[row][20]#">,
                 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
              </cfquery>
              <cfset numRecordsImported=numRecordsImported+1>
            <cfcatch>
              <cfset importLog=importLog & newline & "ERROR: " & cfcatch.Message>
            </cfcatch>
            </cftry>  
          </cfif>
        </cfloop>
    
		<cfif Not Compare(importLog,"")>
          <cfset importLog="The data file was imported successfully.">
          <cfset updateSpecialtyTable()>
        </cfif>
   	<cfcatch>
    	<cfset importLog=importLog & newline & "ERROR: " & cfcatch.Message>
    </cfcatch>
    </cftry>
    
    <cfset importLog=importLog & newline & "Numer of records imported: #numRecordsImported#">    
    
    <cfquery datasource="#this.datasource#">
     INSERT INTO dd_import_log
     (dateImport, importLog, importFileTimestamp)
     VALUES
     (<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
      <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#importLog#">,
      <cfqueryparam cfsqltype="cf_sql_timestamp" value="#importFileTimestamp#">)
    </cfquery>
    
    <cfreturn importLog>
  </cffunction>
  
  <cffunction name="updateSpecialtyTable" access="public" output="no">
    <cfquery datasource="#this.datasource#">
      TRUNCATE TABLE dd_specialty
    </cfquery>
    
    <cfquery name="qSpecialties" datasource="#this.datasource#">
      SELECT DISTINCT specialty
      FROM dd_doctor
      ORDER BY specialty
    </cfquery>
    
    <cfloop query="qSpecialties">
      <cfquery datasource="#this.datasource#">
        INSERT INTO dd_specialty (specialty) VALUES (<cfqueryparam cfsqltype="cf_sql_varchar" value="#specialty#">)
      </cfquery>
    </cfloop>
  </cffunction>
  
  <cffunction name="getAllOfficeNames" access="public" output="no" returntype="query">
    <cfquery name="qOfficeNames" datasource="#this.datasource#">
      SELECT distinct officeName
      FROM dd_doctor
      ORDER BY officeName
    </cfquery>
    <cfreturn qOfficeNames>
  </cffunction>
  
  <cffunction name="getDoctorsByKeyword" access="public" output="no" returntype="struct">
    <cfargument name="keyword" type="string" default="">
    <cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="50">
    
    <cfset var resultStruct = StructNew()>
	<cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.doctors=QueryNew("recordNum")>	
    
	<cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qAllItemIDs" datasource="#this.datasource#">
	  SELECT COUNT(dd_doctor.recordNum) AS numItems
	  FROM	 dd_doctor
      <cfif Compare(Arguments.keyword, "")>
	  WHERE	CONCAT(dd_doctor.firstName,' ',dd_doctor.lastName) LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">     		 
      </cfif>
    </cfquery>
	<cfset resultStruct.numAllItems = qAllItemIDs.numItems>
	
	<cfquery name="qDoctors" datasource="#this.datasource#">
	  SELECT dd_doctor.recordNum, dd_doctor.firstName, dd_doctor.middleName, dd_doctor.lastName, dd_doctor.officeName,
      		 dd_doctor.degree, dd_doctor.specialty, dd_doctor_meta.photo
	  FROM   dd_doctor LEFT JOIN dd_doctor_meta ON
	         dd_doctor.recordNum=dd_doctor_meta.recordNum   
	  <cfif Compare(Arguments.keyword, "")>
	  WHERE	CONCAT(dd_doctor.firstName,' ',dd_doctor.lastName) LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
      </cfif>
      ORDER BY lastName, middleName, firstName
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
    <cfif qDoctors.recordcount GT 0>            
      <cfset resultStruct.numDisplayedItems=qDoctors.recordcount>	
      <cfset resultStruct.doctors = qDoctors>
	<cfelse>
      <cfset resultStruct.numDisplayedItems=0>	
      <cfset resultStruct.doctors = QueryNew("recordNum")>
    </cfif>
    
	<cfreturn resultStruct>    
  </cffunction>
  
  <cffunction name="getDoctorInfo" access="public" output="no" returntype="query">
    <cfargument name="recordNum" type="numeric" required="yes">
    
    <cfquery name="qDoctor" datasource="#this.datasource#">
	  SELECT *
	  FROM   dd_doctor LEFT JOIN dd_doctor_meta ON
	         dd_doctor.recordNum=dd_doctor_meta.recordNum
      WHERE dd_doctor.recordNum=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.recordNum#">
    </cfquery>
    <cfreturn qDoctor> 
  </cffunction>
  
  <cffunction name="getBoardCertList" access="public" output="no" returntype="string">
    <cfargument name="recordNum" type="numeric" required="yes">
    <cfquery name="qCerts" datasource="#this.datasource#">
	  SELECT boardcert1, boardcert2, boardcert3
	  FROM   dd_doctor
      WHERE dd_doctor.recordNum=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.recordNum#">
    </cfquery>
    
	<cfif qCerts.recordcount Is 0><cfreturn ""></cfif>
    
    <cfset boardCertList="">
    
    <cfif Compare(qCerts.boardcert1,"")>
      <cfloop index="cert" list="#qCerts.boardcert1#">
        <cfset cert=Trim(cert)>
    	<cfif Compare(cert,"") And ListFind(boardCertList, cert) IS 0>
          <cfset boardCertList=listAppend(boardCertList,cert)>
        </cfif>
      </cfloop>
    </cfif>
    <cfif Compare(qCerts.boardcert2,"")>
      <cfloop index="cert" list="#qCerts.boardcert2#">
        <cfset cert=Trim(cert)>
    	<cfif Compare(cert,"") And ListFind(boardCertList, cert) IS 0>
          <cfset boardCertList=listAppend(boardCertList,cert)>
        </cfif>
      </cfloop>
    </cfif>
    <cfif Compare(qCerts.boardcert3,"")>
      <cfloop index="cert" list="#qCerts.boardcert3#">
        <cfset cert=Trim(cert)>
    	<cfif Compare(cert,"") And ListFind(boardCertList, cert) IS 0>
          <cfset boardCertList=listAppend(boardCertList,cert)>
        </cfif>
      </cfloop>
    </cfif>
    
    <cfreturn boardCertList>
  </cffunction>
  
  <cffunction name="editDoctor" access="public" output="no">
    <cfargument name="recordNum" type="numeric" required="yes">
    
    <cfquery name="qDoctor" datasource="#this.datasource#">
      SELECT recordNum
      FROM dd_doctor_meta
      WHERE recordNum=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.recordNum#">
    </cfquery>
    
    <cfif qDoctor.recordcount Is 0>
      <cfquery datasource="#this.datasource#">
        INSERT INTO dd_doctor_meta
        (recordNum, photo, email, groupLogo, bio, philosophy, staff,
         address1Additional, address2Additional, cityAdditional, stateAdditional, ZIPAdditional,
         dateCreated, dateLastModified)
        VALUES
        (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.recordNum#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.photo#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.groupLogo#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.bio#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.philosophy#">,
         <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.staff#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.address1Additional#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.address2Additional#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.cityAdditional#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.stateAdditional#">,
         <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.ZIPAdditional#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
      	 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
      </cfquery>
    <cfelse>
      <cfquery datasource="#this.datasource#">
        UPDATE dd_doctor_meta
        SET  photo=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.photo#">,
             email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.email#">,
             groupLogo=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.groupLogo#">,
             bio=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.bio#">,
             philosophy=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.philosophy#">,
             staff=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.staff#">,
             address1Additional=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.address1Additional#">,
         	 address2Additional=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.address2Additional#">,
        	 cityAdditional=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.cityAdditional#">,
        	 stateAdditional=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.stateAdditional#">,
        	 ZIPAdditional=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.ZIPAdditional#">,
             dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
        WHERE recordNum=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.recordNum#">
      </cfquery>
    </cfif>
  </cffunction>
  
  <cffunction name="getIntroductionText" access="public" output="no" returntype="string">
    <cfquery name="qText" datasource="#this.datasource#">
      SELECT introductionText
      FROM dd_introduction
    </cfquery>
    
    <cfif qText.recordcount GT 0>
	  <cfreturn qText.introductionText>
	<cfelse>
      <cfquery name="qName" datasource="#this.datasource#">
        INSERT INTO dd_introduction (introductionText)
        VALUES ('')
      </cfquery>
      <cfreturn "">
	</cfif>
  </cffunction>
  
  <cffunction name="updateIntroductionText" access="public" output="no" returntype="void">
    <cfargument name="introductionText" type="string" required="yes">
    
    <cfquery name="qName" datasource="#this.datasource#">
      UPDATE dd_introduction
      SET introductionText=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.introductionText#">
    </cfquery>    
  </cffunction>
</cfcomponent>