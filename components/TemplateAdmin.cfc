<cfcomponent displayname="Template Admin" extends="Base" output="false">
  <cffunction name="getTemplates" access="public" output="no" returntype="query">
    <cfquery name="qTemplates" datasource="#this.datasource#">
  	  SELECT *
      FROM tm_template
      ORDER BY templateName
    </cfquery>
    <cfreturn qTemplates>
  </cffunction>
  
  <cffunction name="getTemplate" access="public" output="no" returntype="query">
    <cfargument name="templateID" type="numeric" required="yes">
    
    <cfquery name="qTemplate" datasource="#this.datasource#">
  	  SELECT *
      FROM tm_template
      WHERE templateID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.templateID#">
    </cfquery>
    <cfreturn qTemplate>
  </cffunction>
  
  
  <cffunction name="addTemplate" access="public" output="no">
    <cfargument name="templateName" type="string" required="yes">
    <cfargument name="isDefault" type="boolean" default="0">
    <cfargument name="topperStyle" type="string" default="ST">
    <cfargument name="sectionTitle" type="string" default="">
    <cfargument name="topperImage" type="string" default="">
    <cfargument name="mainNavIDList" type="string" default="">
    <cfargument name="themeImage" type="string" default="">
    <cfargument name="description" type="string" default="">
    
    <cfif Arguments.isDefault>
      <cfquery datasource="#this.datasource#">
        UPDATE tm_template
        SET isDefault=0
      </cfquery>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      INSERT INTO tm_template
      (templateName, isDefault, topperStyle, sectionTitle, topperImage,
       mainNavIDList, themeImage, description, dateCreated, dateLastModified)
      VALUES
      (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.templateName#">,
       <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.isDefault#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.topperStyle#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sectionTitle#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.topperImage#">,
       
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.mainNavIDList#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.themeImage#">,
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.description#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)   
    </cfquery>
  </cffunction>
  
  <cffunction name="editTemplate" access="public" output="no">
    <cfargument name="templateID" type="numeric" required="yes">
    <cfargument name="templateName" type="string" required="yes">
    <cfargument name="isDefault" type="boolean" default="0">
    <cfargument name="topperStyle" type="string" default="ST">
    <cfargument name="sectionTitle" type="string" default="">
    <cfargument name="topperImage" type="string" default="">
    <cfargument name="mainNavIDList" type="string" default="">
    <cfargument name="themeImage" type="string" default="">
    <cfargument name="description" type="string" default="">
    
    <cfif Arguments.isDefault>
      <cfquery datasource="#this.datasource#">
        UPDATE tm_template
        SET isDefault=0
      </cfquery>
    </cfif>  
    
    <cfquery datasource="#this.datasource#">
      UPDATE tm_template
      SET  templateName=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.templateName#">,
           isDefault=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.isDefault#">,
           topperStyle=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.topperStyle#">,
           sectionTitle=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sectionTitle#">,
           topperImage=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.topperImage#">,           
           mainNavIDList=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.mainNavIDList#">,
           themeImage=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.themeImage#">,
           description=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.description#">,
           dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
  	  WHERE templateID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.templateID#">
    </cfquery>
  </cffunction>
  
  <cffunction name="deleteTemplate" access="public" output="no" returntype="boolean">
    <cfargument name="templateID" type="numeric" required="yes">
    
    <cfquery name="qNavs" datasource="#this.datasource#">
      SELECT navID
      FROM nm_nav
      WHERE templateID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.templateID#">
    </cfquery>
  
    <cfif qNavs.recordcount GT 0><cfreturn false></cfif>
    <cfquery datasource="#this.datasource#">
      DELETE FROM tm_template
      WHERE templateID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.templateID#">
    </cfquery>
    <cfreturn true>
  </cffunction>
</cfcomponent>