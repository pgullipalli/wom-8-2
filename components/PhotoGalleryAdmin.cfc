<cfcomponent displayname="Photo Gallery Administrator" extends="Base" output="No">
  <cfif IsDefined("Application.photogallery")>
    <cfset this.thumbnailWidth=Application.photogallery.thumbnailWidth>
  <cfelse>
    <cfset this.thumbnailWidth="100">
  </cfif>

  <cffunction name="getAllAlbumCategories" access="public" output="no" returntype="query">
	<cfquery name="qAlbumCategories" datasource="#this.datasource#">
	  SELECT albumCategoryID, albumCategory
	  FROM pg_albumcategory
	  ORDER BY albumCategory	
	</cfquery>	
    
    <cfreturn qAlbumCategories>  
  </cffunction>
  
  <cffunction name="getAlbumCategorySummary" access="public" output="no" returntype="query">
    <cfquery name="qCatSummary" datasource="#this.datasource#">
	  SELECT pg_albumcategory.albumCategory,
      	  	 pg_albumcategory.albumCategoryID,
			 COUNT(pg_album_albumcategory.albumID) AS numAlbums
	  FROM pg_albumcategory LEFT JOIN pg_album_albumcategory ON
	  	   pg_albumcategory.albumCategoryID=pg_album_albumcategory.albumCategoryID
	  GROUP by pg_albumcategory.albumCategoryID
	  ORDER by pg_albumcategory.albumCategory		
	</cfquery>
    
    <cfreturn qCatSummary>
  </cffunction>
  
  <cffunction name="getFirstAlbumCategoryID" access="public" output="no" returntype="numeric">
    <cfquery name="qAlbumCategoryID" datasource="#this.datasource#">
	  SELECT albumCategoryID
	  FROM pg_albumcategory
      ORDER BY albumCategory
	</cfquery>
    <cfif qAlbumCategoryID.recordcount Is 0>
      <cfreturn 0>
    <cfelse>
	  <cfreturn qAlbumCategoryID.albumCategoryID[1]>
    </cfif>
  </cffunction>
  
  <cffunction name="getAlbumCategory" access="public" output="no" returntype="query">
    <cfargument name="albumCategoryID" required="yes" type="numeric">	
	<cfquery name="qAlbumCategory" datasource="#this.datasource#">
	  SELECT *
      FROM pg_albumcategory
      WHERE albumCategoryID=<cfqueryparam value="#Arguments.albumCategoryID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
	<cfreturn qAlbumCategory>
  </cffunction>
  
  <cffunction name="addAlbumCategory" access="public" output="no">
    <cfargument name="albumCategory" type="string" required="yes">

    <cfquery datasource="#this.datasource#">
	  INSERT INTO pg_albumcategory
        (albumCategory, dateCreated, dateLastModified)
	  VALUES
        (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.albumCategory#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
         <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)	  
	</cfquery>  
  </cffunction>
  
  <cffunction name="editAlbumCategory" access="public" output="no">
    <cfargument name="albumCategoryID" type="numeric" required="yes">
    <cfargument name="albumCategory" type="string" required="yes">
    
    <cfquery datasource="#this.datasource#">
	  UPDATE pg_albumcategory
      SET albumCategory=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.albumCategory#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE albumCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumCategoryID#">  
	</cfquery>  
  </cffunction>
    
  <cffunction name="deleteAlbumCategory" access="public" output="false" returntype="boolean">
    <cfargument name="albumCategoryID" required="yes" type="numeric">
	
	<cfquery name="qAlbums" datasource="#this.datasource#">
	  SELECT albumID
	  FROM pg_album_albumcategory
      WHERE albumCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumCategoryID#">			
	</cfquery>
	
	<cfif qAlbums.recordcount GT 0>
	  <cfreturn false>
	</cfif>
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM pg_albumcategory
      WHERE albumCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumCategoryID#">
	</cfquery>
    
	<cfreturn true>
  </cffunction>



  <cffunction name="getAlbumsByCategoryID" access="public" output="no" returntype="struct">
    <cfargument name="albumCategoryID" type="numeric" required="yes">
    <cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
	<cfset var resultStruct = StructNew()>
    
    <cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.albums=QueryNew("albumID")>
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
  
	<cfquery name="qNumAlbums" datasource="#this.datasource#">
	  SELECT COUNT(pg_album.albumID) AS numAlbums
	  FROM pg_album INNER JOIN pg_album_albumcategory ON
	  	   pg_album.albumID=pg_album_albumcategory.albumID
      WHERE pg_album_albumcategory.albumCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumCategoryID#">
	</cfquery> 
	<cfset resultStruct.numAllItems=qNumAlbums.numAlbums>
    
    <cfquery name="qAlbums" datasource="#this.datasource#">
	  SELECT pg_album.albumID, pg_album.coverImage, pg_album.albumTitle, pg_album.albumDate,
      		 pg_album.published, pg_album.featuredOnHomepage, pg_album.displaySeq			 
	  FROM   pg_album INNER JOIN pg_album_albumcategory ON
	         pg_album.albumID=pg_album_albumcategory.albumID   
	  WHERE  pg_album_albumcategory.albumCategoryID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumCategoryID#">
	  ORDER  BY pg_album.albumDate DESC
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
    <cfif qAlbums.recordcount GT 0>
        <cfquery name="qPhotos" datasource="#this.datasource#">
          SELECT pg_album.albumID,
                 COUNT(pg_photo.photoID) AS numPhotos
          FROM   pg_album LEFT JOIN pg_photo ON pg_album.albumID=pg_photo.albumID
          WHERE  pg_album.albumID IN (#valueList(qAlbums.albumID)#)
          GROUP BY pg_album.albumID	  		
        </cfquery>
        
        <cfquery name="qAlbums" dbtype="query">
          SELECT *
          FROM qAlbums, qPhotos
          WHERE qAlbums.albumID=qPhotos.albumID
          ORDER BY albumDate DESC, albumID DESC
        </cfquery>
            
        <cfset resultStruct.numDisplayedItems=qAlbums.recordcount>	
        <cfset resultStruct.albums = qAlbums>
	<cfelse>
        <cfset resultStruct.numDisplayedItems=0>	
        <cfset resultStruct.albums = QueryNew("albumID")>
    </cfif>
    
	<cfset resultStruct.numDisplayedItems=qAlbums.recordcount>
	<cfset resultStruct.albums = qAlbums>
		
	<cfreturn resultStruct>  
  </cffunction>
  
  <cffunction name="getAlbumsByKeyword" access="public" output="no" returntype="struct">
    <cfargument name="keyword" type="string" required="yes">
    <cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
	<cfset var resultStruct = StructNew()>
    
    <cfset resultStruct.numAllItems=0>
	<cfset resultStruct.numDisplayedItems=0>
	<cfset resultStruct.albums=QueryNew("albumID")>
    
    <cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
  
	<cfquery name="qNumAlbums" datasource="#this.datasource#">
	  SELECT COUNT(pg_album.albumID) AS numAlbums
	  FROM pg_album
      WHERE pg_album.albumTitle LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
	</cfquery> 
	<cfset resultStruct.numAllItems=qNumAlbums.numAlbums>
    
    <cfquery name="qAlbums" datasource="#this.datasource#">
	  SELECT pg_album.albumID, pg_album.coverImage, pg_album.albumTitle, pg_album.albumDate,
      		 pg_album.published, pg_album.featuredOnHomepage, pg_album.displaySeq		 
	  FROM   pg_album  
	  WHERE pg_album.albumTitle LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#Arguments.keyword#%">
	  ORDER  BY pg_album.albumDate DESC, pg_album.albumTitle ASC
	  LIMIT  #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>
    
    <cfif qAlbums.recordcount GT 0>
        <cfquery name="qPhotos" datasource="#this.datasource#">
          SELECT pg_album.albumID,
                 COUNT(pg_photo.photoID) AS numPhotos
          FROM   pg_album LEFT JOIN pg_photo ON pg_album.albumID=pg_photo.albumID
          WHERE  pg_album.albumID IN (#valueList(qAlbums.albumID)#)
          GROUP BY pg_album.albumID	  		
        </cfquery>       
        
        <cfquery name="qAlbums" dbtype="query">
          SELECT *
          FROM qAlbums, qPhotos
          WHERE qAlbums.albumID=qPhotos.albumID
          ORDER BY albumDate DESC, albumTitle ASC
        </cfquery>
            
        <cfset resultStruct.numDisplayedItems=qAlbums.recordcount>	
        <cfset resultStruct.albums = qAlbums>
	<cfelse>
        <cfset resultStruct.numDisplayedItems=0>	
        <cfset resultStruct.albums = QueryNew("albumID")>
    </cfif>
    		
	<cfreturn resultStruct>  
  </cffunction>
  
  <cffunction name="getAllAlbumsBasicInfo" access="public" output="NO" returntype="query">
    <cfquery name="qAlbums" datasource="#this.datasource#">
	  SELECT albumID, albumTitle from pg_album
	  ORDER BY albumDate DESC
	</cfquery>
	<cfreturn qAlbums>
  </cffunction>
  
  <cffunction name="getFirstAlbumID" access="public" output="no" returntype="numeric">
    <cfquery name="qAlbumID" datasource="#this.datasource#">
	  SELECT albumID
	  FROM pg_album
	  ORDER BY albumDate DESC
	</cfquery>
    
    <cfif qAlbumID.recordcount Is 0>
      <cfreturn 0>
    <cfelse>
	  <cfreturn qAlbumID.albumID[1]>
    </cfif>
  </cffunction>
  
  <cffunction name="addAlbum" access="public" output="no" returntype="boolean">	
    <cfargument name="albumCategoryIDList" type="string" default="">
    <cfargument name="coverImage" type="string" default="">
    <cfargument name="albumTitle" type="string" required="yes">
    <cfargument name="albumDate" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="featuredOnHomepage" type="boolean" default="0">    
    <cfargument name="description" type="string" default="">
    
    <cfif Not Compare(Arguments.albumCategoryIDList, "")><cfreturn false></cfif>
    
    <cftry>
      <cfset albumDateObj=DateAdd("d", 0, arguments.albumDate)>
    <cfcatch>
      <cfset albumDateObj=now()>
    </cfcatch>
    </cftry>    
    
    <cfquery name="qDisplaySeq" datasource="#this.datasource#">
      SELECT MAX(displaySeq) AS displaySeq
      FROM pg_album
    </cfquery>
    
    <cfif Not IsNumeric(qDisplaySeq.displaySeq)>
      <cfset newDisplaySeq=1>
    <cfelse>
      <cfset newDisplaySeq=qDisplaySeq.displaySeq + 1>
    </cfif>
    
	<cfquery datasource="#this.datasource#">
	  INSERT INTO pg_album
	  (coverImage, albumTitle, albumDate, published, featuredOnHomepage, displaySeq, description, dateCreated, dateLastModified)
	  VALUES
	  (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.coverImage#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.albumTitle#">,
	   <cfqueryparam cfsqltype="cf_sql_date" value="#albumDateObj#">,
	   <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
       <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.featuredOnHomepage#">,
	   <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">,
	   <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.description#">,
	   <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
	   <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)
	</cfquery>
    
    <cfquery name="qAlbumID" datasource="#this.datasource#">
	  SELECT MAX(albumID) AS newAlbumID
	  FROM pg_album
    </cfquery>
	<cfset Variables.albumID=qAlbumID.newAlbumID>	
    
    <cfloop index="albumCategoryID" list="#Arguments.albumCategoryIDList#">
      <cfquery datasource="#this.datasource#">
		INSERT INTO pg_album_albumcategory (albumID, albumCategoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Variables.albumID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#albumCategoryID#">)
	  </cfquery>
    </cfloop>
    
    <cfreturn true>
  </cffunction>
  
  <cffunction name="editAlbum" access="public" output="no" returntype="boolean">	
    <cfargument name="albumID" type="numeric" required="yes">
    <cfargument name="albumCategoryIDList" type="string" default="">
    <cfargument name="coverImage" type="string" default="">
    <cfargument name="albumTitle" type="string" required="yes">
    <cfargument name="albumDate" type="string" required="yes">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="featuredOnHomepage" type="boolean" default="0">  
    <cfargument name="description" type="string" default="">
    
    <cfif Not Compare(Arguments.albumCategoryIDList, "")><cfreturn false></cfif>
    
    <cftry>
      <cfset albumDateObj=DateAdd("d", 0, Arguments.albumDate)>
    <cfcatch>
      <cfset albumDateObj=now()>
    </cfcatch>
    </cftry>    
        
	<cfquery datasource="#this.datasource#">
	  UPDATE pg_album
	  SET coverImage=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.coverImage#">,
      	  albumTitle=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.albumTitle#">,
          albumDate=<cfqueryparam cfsqltype="cf_sql_date" value="#albumDateObj#">,
          published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
          featuredOnHomepage=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.featuredOnHomepage#">,
          description=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.description#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
	  WHERE albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#">
	</cfquery>
    
    <cfquery datasource="#this.datasource#">
      DELETE FROM pg_album_albumcategory
      WHERE albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#">
    </cfquery>
    
    <cfloop index="albumCategoryID" list="#Arguments.albumCategoryIDList#">
      <cfquery datasource="#this.datasource#">
		INSERT INTO pg_album_albumcategory (albumID, albumCategoryID)
		VALUES
		  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#">,
		   <cfqueryparam cfsqltype="cf_sql_integer" value="#albumCategoryID#">)
	  </cfquery>
    </cfloop>
    
    <cfreturn true>
  </cffunction>
  
  <cffunction name="getAlbumByID" access="public" output="no" returntype="query">
    <cfargument name="albumID" type="numeric" required="yes">
	
	<cfquery name="qAlbum" datasource="#this.datasource#">
	  SELECT *
	  FROM pg_album
	  WHERE albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#">
	</cfquery> 
	<cfreturn qAlbum>
  </cffunction>  
  
  <cffunction name="getAlbumCategoryIDListByAlbumID" access="public" output="no" returntype="string">
    <cfargument name="albumID" type="numeric" required="yes">
    
	<cfquery name="qCategories" datasource="#this.datasource#">
	  SELECT albumCategoryID
      FROM pg_album_albumcategory
      WHERE albumID=<cfqueryparam value="#arguments.albumID#" cfsqltype="cf_sql_integer">
	</cfquery>
    
    <cfif qCategories.recordcount Is 0>
      <cfreturn "">
    <cfelse>
	  <cfreturn valueList(qCategories.albumCategoryID)>	  
    </cfif>
  </cffunction>
  
  <cffunction name="deleteAlbum" access="public" output="no" returntype="boolean">
    <cfargument name="albumID" type="numeric" required="yes">
	
	<cfquery name="qPhotos" datasource="#this.datasource#">
	  SELECT photoID
	  FROM pg_photo
	  WHERE albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#">
	</cfquery>
	
	<cfif qPhotos.recordcount GT 0><cfreturn false></cfif>
  
    <cfquery datasource="#this.datasource#">
	  DELETE FROM pg_album
	  WHERE albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#">
	</cfquery>
    
    <cfquery datasource="#this.datasource#">
	  DELETE FROM pg_album_albumcategory
	  WHERE albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#">
	</cfquery>
     
    <cfreturn true>  
  </cffunction>
  
  <cffunction name="getPhotosByAlbumID" access="public" output="no" returntype="struct">
    <cfargument name="albumID" type="numeric" required="yes">
	<cfargument name="pageNum" type="numeric" default="1">
    <cfargument name="startIndex" type="numeric" default="1">
	<cfargument name="numItemsPerPage" type="numeric" default="20">
    
	<cfset var resultStruct = StructNew()>
    
	<cfif Arguments.pageNum GT 1>
	  <cfset Arguments.startIndex=(Arguments.pageNum - 1) * Arguments.numItemsPerPage + 1>
	</cfif>
	
	<cfquery name="qAllPhotos" datasource="#this.datasource#">
	  SELECT photoID
	  FROM pg_photo
	  WHERE albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#">
    </cfquery>
	<cfset resultStruct.numAllItems = qAllPhotos.recordcount>
	
	<cfquery name="qPhotos" datasource="#this.datasource#">
	  SELECT *	 
	  FROM pg_photo
	  WHERE albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#">
	  ORDER BY displaySeq
	  LIMIT #Arguments.startIndex-1#, #Arguments.numItemsPerPage#
	</cfquery>	
	
	<cfset resultStruct.numDisplayedItems=qPhotos.recordcount>	
	<cfset resultStruct.photos = qPhotos>
		
	<cfreturn resultStruct> 
  </cffunction>
  
  <cffunction name="getPhotoByID" access="public" output="no" returntype="query">
    <cfargument name="photoID" type="numeric" required="yes">
	
	<cfquery name="qPhoto" datasource="#this.datasource#">
	  SELECT *
	  FROM pg_photo
	  WHERE photoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.photoID#">
	</cfquery>
	<cfreturn qPhoto>  
  </cffunction>
  
  <cffunction name="addPhoto" access="public" output="false">
    <cfargument name="albumID" type="numeric" required="yes">
    <cfargument name="title" type="string" default="">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="photoFile" type="string" required="yes">
    <cfargument name="orientation" type="string" default="H">
    <cfargument name="description" type="string" default="">
    <cfargument name="author" type="string" default="">
    <cfargument name="photoThumbnail" type="string" default="">
    <cfargument name="resizeForThumbnail" type="boolean" default="0">
    
    <cfset Util=CreateObject("component","Utility").init()>
    <cftry>
	  <cfif Arguments.resizeForThumbnail And Compare(Arguments.photoFile, "")>
	    <cfset Arguments.photoThumbnail=Util.createThumbnail("#this.siteDirectoryRoot#/#Arguments.photoFile#", this.thumbnailWidth)>
	  </cfif>		
	<cfcatch></cfcatch>
    </cftry>
	
	<cfquery name="qDisplaySeq" datasource="#this.datasource#">
	  SELECT MAX(displaySeq) AS displaySeq
	  FROM pg_photo
	  WHERE albumID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#">
	</cfquery>
	<cfif Not IsNumeric(qDisplaySeq.displaySeq)>
	  <cfset newDisplaySeq=1>
	<cfelse>
	  <cfset newDisplaySeq=qDisplaySeq.displaySeq + 1>
	</cfif>
    <cfset Util=CreateObject("component","Utility").init()>
    <cfset fileSize=Util.getFileSize(this.siteDirectoryRoot & "/" & Arguments.photoFile)>
    
    <cfif fileSize LT 1000>
      <cfset fileSize = fileSize & " bytes">
    <cfelseif fileSize GTE 1000 AND fileSize LTE 1000000>
      <cfset fileSize = NumberFormat(fileSize/1000, "9.9") & " KB">
    <cfelse>
      <cfset fileSize = NumberFormat(fileSize/1000000, "9.999") & " MB">
    </cfif>
	
	<cfquery datasource="#this.datasource#">
	  INSERT INTO pg_photo
	  (albumID, title, published, photoFile,
	   displaySeq, author, fileSize, description,
       orientation, photoThumbnail,
       dateCreated, dateLastModified)
	  values
	  (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.albumID#">,
	   <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.title#">,
	   <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
	   <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.photoFile#">,
       
	   <cfqueryparam cfsqltype="cf_sql_float" value="#newDisplaySeq#">,
	   <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.author#">,
	   <cfqueryparam cfsqltype="cf_sql_varchar" value="#fileSize#">,
	   <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.description#">,
       
       <cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.orientation#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.photoThumbnail#">,
	   <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
	   <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">)	   
	</cfquery>  
  </cffunction>
  
  <cffunction name="editPhoto" access="public" output="false">
    <cfargument name="photoID" type="numeric" required="yes">
    <cfargument name="title" type="string" default="">
    <cfargument name="published" type="boolean" default="0">
    <cfargument name="photoFile" type="string" required="yes">
    <cfargument name="orientation" type="string" default="H">
    <cfargument name="description" type="string" default="">
    <cfargument name="author" type="string" default="">
	<cfargument name="photoThumbnail" type="string" default="">
    <cfargument name="resizeForThumbnail" type="boolean" default="0">
    
    <cfset Util=CreateObject("component","Utility").init()>
    <cftry>
	  <cfif Arguments.resizeForThumbnail And Compare(Arguments.photoFile, "")>
	    <cfset Arguments.photoThumbnail=Util.createThumbnail("#this.siteDirectoryRoot#/#Arguments.photoFile#", this.thumbnailWidth)>
	  </cfif>		
	<cfcatch></cfcatch>
    </cftry>
    
    <cfset Util=CreateObject("component","Utility").init()>
    <cfset fileSize=Util.getFileSize(this.siteDirectoryRoot & "/" & Arguments.photoFile)>
    
    <cfif fileSize LT 1000>
      <cfset fileSize = fileSize & " bytes">
    <cfelseif fileSize GTE 1000 AND fileSize LTE 1000000>
      <cfset fileSize = NumberFormat(fileSize/1000, "9.9") & " KB">
    <cfelse>
      <cfset fileSize = NumberFormat(fileSize/1000000, "9.999") & " MB">
    </cfif>
	
	<cfquery datasource="#this.datasource#">
	  UPDATE pg_photo
	  SET title=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.title#">,
          published=<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.published#">,
          photoFile=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.photoFile#">,
          author=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.author#">,
          fileSize=<cfqueryparam cfsqltype="cf_sql_varchar" value="#fileSize#">,
          orientation=<cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.orientation#">,
          photoThumbnail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.photoThumbnail#">,
          description=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#Arguments.description#">,
          dateLastModified=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
      WHERE photoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.photoID#">
	</cfquery>  
  </cffunction>
  
  <cffunction name="deletePhoto" access="public" output="no">
    <cfargument name="photoID" type="numeric" required="yes">
	
	<cfquery datasource="#this.datasource#">
	  DELETE FROM pg_photo
	  WHERE photoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.photoID#">
	</cfquery>  
  </cffunction>
  
  <cffunction name="movePhoto" access="public" output="no">
    <cfargument name="photoID" required="yes" type="numeric">
    <cfargument name="direction" required="yes" type="string">
    
    <cfquery name="qTargetDisplaySeq" datasource="#this.datasource#">
      SELECT albumID, displaySeq
      FROM pg_photo WHERE photoID=<cfqueryparam value="#Arguments.photoID#" cfsqltype="cf_sql_integer">
    </cfquery>
    
    <cfset targetAlbumID=qTargetDisplaySeq.albumID>
    <cfset targetDisplaySeq=qTargetDisplaySeq.displaySeq>
    
    <cfif Arguments.direction IS "up">
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT photoID, displaySeq
        FROM pg_photo
        WHERE albumID=<cfqueryparam value="#targetAlbumID#" cfsqltype="cf_sql_integer"> AND
        	  displaySeq < <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq DESC
      </cfquery>
    <cfelse>
      <cfquery name="qNextDisplaySeq" datasource="#this.datasource#">
        SELECT photoID, displaySeq
        FROM pg_photo
        WHERE albumID=<cfqueryparam value="#targetAlbumID#" cfsqltype="cf_sql_integer"> AND
        	  displaySeq > <cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
        ORDER BY displaySeq ASC
      </cfquery>
    </cfif>
    
    <cfif qNextDisplaySeq.recordcount Is 0><cfreturn></cfif>
    
    <cfset nextDisplaySeq=qNextDisplaySeq.displaySeq>
        
    <cfquery datasource="#this.datasource#">
      UPDATE pg_photo SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#nextDisplaySeq#">
      WHERE photoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.photoID#">
    </cfquery>
    
    <cfquery datasource="#this.datasource#">
      UPDATE pg_photo SET displaySeq=<cfqueryparam cfsqltype="cf_sql_float" value="#targetDisplaySeq#">
      WHERE photoID=<cfqueryparam cfsqltype="cf_sql_integer" value="#qNextDisplaySeq.photoID#">
    </cfquery>
  </cffunction>
</cfcomponent>