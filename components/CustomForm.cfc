<cfcomponent displayname="Custom Form" extends="Base" output="false">
  <cffunction name="getFormInfo" access="public" output="no" returntype="query">
    <cfargument name="formID" type="numeric" required="yes">
    
    <cfquery name="qFormInfo" datasource="#this.datasource#">
      SELECT *
      FROM fm_cf_form
      WHERE formID=<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">
    </cfquery>
    
    <cfreturn qFormInfo>
  </cffunction>
  
  <cffunction name="submitDonationForm" access="public" output="no" returntype="numeric">
    <cfargument name="authCode" type="string" required="yes">
    <cfargument name="orderID" type="string" required="yes">
    <cfargument name="clientIP" type="string" required="yes">   
    <cfargument name="formID" type="numeric" required="yes">
    <cfargument name="sourceCode" type="string" default="">
    <!--- form fields from Form 1 --->
    <cfargument name="contactTitle" type="string" default="">
    <cfargument name="firstName" type="string" required="yes">
    <cfargument name="middleName" type="string" default="">
    <cfargument name="lastName" type="string" required="yes">
    <cfargument name="address1" type="string" required="yes">
    <cfargument name="address2" type="string" default="">
    <cfargument name="city" type="string" required="yes">
    <cfargument name="state" type="string" required="yes">
    <cfargument name="zip" type="string" required="yes">
    <cfargument name="phonePart1" type="string" required="yes">
    <cfargument name="phonePart2" type="string" required="yes">
    <cfargument name="phonePart3" type="string" required="yes">
    <cfargument name="emailAddress" type="string" required="yes"><!--- ***** --->
    <cfargument name="dateOfBirthPart1" type="string" default="">
    <cfargument name="dateOfBirthPart2" type="string" default="">
    <cfargument name="dateOfBirthPart3" type="string" default="">
    <cfargument name="displayName" type="string" default="">
    <cfargument name="preferAnonymous" type="string" default="No">
    <cfargument name="donationAmount" type="numeric" required="yes"><!--- ***** --->
    <cfargument name="dedicate" type="string" default="No">
    <cfargument name="inHonorOf" type="string" default="">
    <cfargument name="inMemoryOf" type="string" default="">
    <cfargument name="notification" type="string" default="No">
    <cfargument name="notifyTitle" type="string" default="">
    <cfargument name="notifyFirstName" type="string" default="">
    <cfargument name="notifyMiddleInitial" type="string" default="">
    <cfargument name="notifyLastName" type="string" default="">
    <cfargument name="notifyAddress1" type="string" default="">
    <cfargument name="notifyAddress2" type="string" default="">
    <cfargument name="notifyCity" type="string" default="">
    <cfargument name="notifyState" type="string" default="">
    <cfargument name="notifyZip" type="string" default="">
    <cfargument name="notifyPhonePart1" type="string" default="">
    <cfargument name="notifyPhonePart2" type="string" default="">
    <cfargument name="notifyPhonePart3" type="string" default="">
    <cfargument name="donatedBy" type="string" default="">
    <cfargument name="matchGifts" type="string" default="No">
    <cfargument name="matchCompany" type="string" default="">
    <cfargument name="donorStory" type="string" default="">
    <!--- form fields from Form 2 --->
    <cfargument name="creditCardType" type="string" required="yes">
    <cfargument name="nameOnCard" type="string" required="yes">
    <cfargument name="billingAddress1" type="string" required="yes">
    <cfargument name="billingAddress2" type="string" default="">
    <cfargument name="billingCity" type="string" required="yes">
    <cfargument name="billingState" type="string" required="yes">
    <cfargument name="billingZip" type="string" required="yes">
    <cfargument name="cardNumber" type="string" required="yes">
    <cfargument name="cardExpirationDate" type="string" required="yes">
    <cfargument name="cardCVV" type="string" required="yes">
    <cfargument name="shippingCompanyName" type="string" default="">
    <cfargument name="shippingFirstName" type="string" default="">
    <cfargument name="shippingLastName" type="string" default="">
    <cfargument name="shippingAddress1" type="string" default="">
    <cfargument name="shippingAddress2" type="string" default="">
    <cfargument name="shippingCity" type="string" default="">
    <cfargument name="shippingState" type="string" default="">
    <cfargument name="shippingZip" type="string" default="">
    <cfargument name="shippingCountry" type="string" default="">
    <cfargument name="shippingPhonePart1" type="string" default="">
    <cfargument name="shippingPhonePart2" type="string" default="">
    <cfargument name="shippingPhonePart3" type="string" default="">
    
    <cfif Compare(Arguments.sourceCode,"")>
      <cfquery name="qSourceCode" datasource="#this.datasource#">
        SELECT sourceName
        FROM fm_cf_source
        WHERE sourceCode=<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sourceCode#">
      </cfquery>
      <cfif qSourceCode.recordcount GT 0>
        <cfset Variables.source=qSourceCode.sourceName>
      <cfelse>
        <cfset Variables.source="">
      </cfif>
    <cfelse>
      <cfset Variables.source="">
    </cfif>
    
    <cfsavecontent variable="dataHTML">
    <cfoutput>
    <p style="font-family:Arial;font-size:20px;color:##16459A;" align="left">Woman's Hospital Foundation Online Donation</p>
    <cfif Compare(Variables.source,"")>
    <p style="font-family:Arial;font-size:15px;color:##16459A;" align="left">Source: #Variables.source#</p>
	</cfif>
    <p style="font-family:Arial;font-size:13px;color:##16459A;font-weight:bold;" align="left">
      Donation ID: #Arguments.orderID# &nbsp;&nbsp;
      Approval Code: #Arguments.authCode# &nbsp;&nbsp;
      Donor IP: #Arguments.clientIP#
    </p>
    <table border="0" cellpadding="3" cellspacing="0" width="500" style="font-family:Arial;font-size:12px;color:##80858E;">    
      <tr valign="top"><td colspan="2" style="color:##5E8ADA;font-weight:bold;">Contact Information</td></tr>
      <tr valign="top">
        <td width="150" align="right">Title:</td>
        <td width="350" style="color:##16459A;">#Arguments.contactTitle#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">First Name:</td>
        <td style="color:##16459A;">#Arguments.firstName#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Middle Name/Initial:</td>
        <td style="color:##16459A;">#Arguments.middleName#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Last Name:</td>
        <td style="color:##16459A;">#Arguments.lastName#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Address:</td>
        <td style="color:##16459A;">#Arguments.address1#<cfif Compare(Arguments.address2,"")><br />#Arguments.address2#</cfif>&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">City:</td>
        <td style="color:##16459A;">#Arguments.city#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">State:</td>
        <td style="color:##16459A;">#Arguments.state#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">ZIP:</td>
        <td style="color:##16459A;">#Arguments.zip#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Phone:</td>
        <td style="color:##16459A;">#Arguments.phonePart1#-#Arguments.phonePart2#-#Arguments.phonePart3#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">E-Mail Address:</td>
        <td style="color:##16459A;">#Arguments.emailAddress#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Birth Date:</td>
        <td style="color:##16459A;">#Arguments.dateOfBirthPart1#/#dateOfBirthPart2#/#dateOfBirthPart3#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Your name as you wish it to appear for donor recognition:</td>
        <td style="color:##16459A;">#Arguments.displayName#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">I prefer to remain anonymous:</td>
        <td style="color:##16459A;">#Arguments.preferAnonymous#&nbsp;</td>
      </tr>
      
      <tr valign="top"><td colspan="2" style="color:##5E8ADA;font-weight:bold;">Donation Amount</td></tr>
      <tr valign="top">
        <td align="right">I wish to donate:</td>
        <td style="color:##16459A;">#DollarFormat(Arguments.donationAmount)#&nbsp;</td>
      </tr>
      
      <tr valign="top"><td colspan="2" style="color:##5E8ADA;font-weight:bold;">Memorium/Honorarium</td></tr>
      <tr valign="top">
        <td align="right">Would you like to dedicate your gift in memory or honor of someone special?</td>
        <td style="color:##16459A;">#Arguments.dedicate#&nbsp;</td>
      </tr>
      <cfif Arguments.dedicate Is "Yes">
      <tr valign="top">
        <td align="right">In Honor Of:</td>
        <td style="color:##16459A;">#Arguments.inHonorOf#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">In Memory Of:</td>
        <td style="color:##16459A;">#Arguments.inMemoryOf#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Is there someone other than you to whom you wish to notify of this gift?</td>
        <td style="color:##16459A;">#Arguments.notification#&nbsp;</td>
      </tr>
      <cfif Arguments.notification Is "Yes">
      <tr valign="top">
        <td align="right"><b>Individual to notify:</b></td>
        <td style="color:##16459A;">&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Title:</td>
        <td style="color:##16459A;">#Arguments.notifyTitle#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">First Name:</td>
        <td style="color:##16459A;">#Arguments.notifyFirstName#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Middle Name/Initial:</td>
        <td style="color:##16459A;">#Arguments.notifyMiddleInitial#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Last Name:</td>
        <td style="color:##16459A;">#Arguments.notifyLastName#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Address:</td>
        <td style="color:##16459A;">#Arguments.notifyAddress1#<cfif Compare(Arguments.notifyAddress2,"")><br />#Arguments.notifyAddress2#</cfif>&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">City:</td>
        <td style="color:##16459A;">#Arguments.notifyCity#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">State:</td>
        <td style="color:##16459A;">#Arguments.notifyState#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">ZIP:</td>
        <td style="color:##16459A;">#Arguments.notifyZip#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Phone:</td>
        <td style="color:##16459A;">#Arguments.notifyPhonePart1#-#Arguments.notifyPhonePart2#-#Arguments.notifyPhonePart3#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Donation given by: (if different than previously entered above)</td>
        <td style="color:##16459A;">#Arguments.donatedBy#&nbsp;</td>
      </tr>
      </cfif>
      </cfif>
      <tr valign="top"><td colspan="2" style="color:##5E8ADA;font-weight:bold;">Matching Gift</td></tr>      
      <tr valign="top">
        <td align="right">Do you work for or are retired from a company that matches gifts?</td>
        <td style="color:##16459A;">#Arguments.matchGifts#&nbsp;</td>
      </tr>
      <cfif Arguments.matchGifts Is "Yes">
      <tr valign="top">
        <td align="right">If Yes, What is the company's name?</td>
        <td style="color:##16459A;">#Arguments.matchCompany#&nbsp;</td>
      </tr>
      </cfif>
      <tr valign="top"><td colspan="2" style="color:##5E8ADA;font-weight:bold;">Please Tell Us Your Story</td></tr>      
      <tr valign="top">
        <td align="right">&nbsp;</td>
        <td style="color:##16459A;">#Replace(Arguments.donorStory,Chr(13) & Chr(10), "<br />", "All")#&nbsp;</td>
      </tr>
      
      <tr valign="top"><td colspan="2" style="color:##5E8ADA;font-weight:bold;">Credit Card Information</td></tr>
      <tr valign="top">
        <td align="right">Card Type:</td>
        <td style="color:##16459A;">#Arguments.creditCardType#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Name as on Card:</td>
        <td style="color:##16459A;">#Arguments.nameOnCard#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Card Billing Address:</td>
        <td style="color:##16459A;">#Arguments.billingAddress1#<cfif Compare(Arguments.billingAddress2,"")><br />#Arguments.billingAddress2#</cfif>&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">City:</td>
        <td style="color:##16459A;">#Arguments.billingCity#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">State:</td>
        <td style="color:##16459A;">#Arguments.billingState#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">ZIP:</td>
        <td style="color:##16459A;">#Arguments.billingZip#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Card Number:</td>
        <td style="color:##16459A;"><cfif Len(Arguments.cardNumber) GT 4>************#Right(Arguments.cardNumber,4)#</cfif>&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Card Expiration Date:</td>
        <td style="color:##16459A;">#Arguments.cardExpirationDate#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">CVV2/CID:</td>
        <td style="color:##16459A;">***</td>
      </tr>
      
      
      <tr valign="top"><td colspan="2" style="color:##5E8ADA;font-weight:bold;">Mailing Information</td></tr>
      <tr valign="top">
        <td align="right">Company Name:</td>
        <td style="color:##16459A;">#Arguments.shippingCompanyName#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">First Name:</td>
        <td style="color:##16459A;">#Arguments.shippingFirstName#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Last Name:</td>
        <td style="color:##16459A;">#Arguments.shippingLastName#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Address:</td>
        <td style="color:##16459A;">#Arguments.shippingAddress1#<cfif Compare(Arguments.shippingAddress2,"")><br />#Arguments.shippingAddress2#</cfif>&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">City:</td>
        <td style="color:##16459A;">#Arguments.shippingCity#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">State:</td>
        <td style="color:##16459A;">#Arguments.shippingState#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">ZIP:</td>
        <td style="color:##16459A;">#Arguments.shippingZip#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Country:</td>
        <td style="color:##16459A;">#Arguments.shippingCountry#&nbsp;</td>
      </tr>
      <tr valign="top">
        <td align="right">Phone:</td>
        <td style="color:##16459A;">#Arguments.shippingPhonePart1#-#Arguments.shippingPhonePart2#-#Arguments.shippingPhonePart3#&nbsp;</td>
      </tr>
    </table>    
    </cfoutput>
    </cfsavecontent>

    <cfsavecontent variable="dataText"><cfoutput>
WOMAN'S HOSPITAL FOUNDATION ONLINE DONATION

<cfif Compare(Variables.source,"")>Source: #Variables.source#</cfif>

Donation ID: #Arguments.orderID#    Approval Code: #Arguments.authCode#    Donor IP: #Arguments.clientIP#

--------------------------------

CONTACT INFORMATION

Title: #Arguments.contactTitle#

First Name: #Arguments.firstName#

Middle Name/Initial: #Arguments.middleName#

Last Name: #Arguments.lastName#

Address: #Arguments.address1#<cfif Compare(Arguments.address2,"")>, #Arguments.address2#</cfif>

City: #Arguments.city#

State: #Arguments.state#

ZIP: #Arguments.zip#

Phone: #Arguments.phonePart1#-#Arguments.phonePart2#-#Arguments.phonePart3#

E-Mail Address: #Arguments.emailAddress#

Birth Date: #Arguments.dateOfBirthPart1#/#dateOfBirthPart2#/#dateOfBirthPart3#

Your name as you wish it to appear for donor recognition: #Arguments.displayName#

I prefer to remain anonymous: #Arguments.preferAnonymous#

--------------------------------

DONATION AMOUNT

I wish to donate: #DollarFormat(Arguments.donationAmount)#

--------------------------------

MEMORIUM/HONORARIUM

Would you like to dedicate your gift in memory or honor of someone special? #Arguments.dedicate#
<cfif Arguments.dedicate Is "Yes">
In Honor Of: #Arguments.inHonorOf#

In Memory Of: #Arguments.inMemoryOf#

Is there someone other than you to whom you wish to notify of this gift? #Arguments.notification#
<cfif Arguments.notification Is "Yes">
Individual to notify:

Title: #Arguments.notifyTitle#

First Name: #Arguments.notifyFirstName#

Middle Name/Initial: #Arguments.notifyMiddleInitial#

Last Name: #Arguments.notifyLastName#

Address: #Arguments.notifyAddress1#<cfif Compare(Arguments.notifyAddress2,"")>, #Arguments.notifyAddress2#</cfif>

City: #Arguments.notifyCity#

State: #Arguments.notifyState#

ZIP: #Arguments.notifyZip#

Phone: #Arguments.notifyPhonePart1#-#Arguments.notifyPhonePart2#-#Arguments.notifyPhonePart3#

Donation given by: (if different than previously entered above) #Arguments.donatedBy#
</cfif>
</cfif>

--------------------------------

MATCHING GIFT

Do you work for or are retired from a company that matches gifts? #Arguments.matchGifts#
<cfif Arguments.matchGifts Is "Yes">
If Yes, What is the company's name? #Arguments.matchCompany#
</cfif>

Please Tell Us Your Story:
#Replace(Arguments.donorStory,Chr(13) & Chr(10), "<br />", "All")#

--------------------------------

CREDIT CARD INFORMATION

Card Type: #Arguments.creditCardType#

Name as on Card: #Arguments.nameOnCard#

Card Billing Address #Arguments.billingAddress1#<cfif Compare(Arguments.billingAddress2,"")>, #Arguments.billingAddress2#</cfif>

City: #Arguments.billingCity#

State: #Arguments.billingState#

ZIP: #Arguments.billingZip#

Card Number: <cfif Len(Arguments.cardNumber) GT 4>************#Right(Arguments.cardNumber,4)#</cfif>

Card Expiration Date: #Arguments.cardExpirationDate#

CVV2/CID: ***

--------------------------------

MAILING INFORMATION

Company Name: #Arguments.shippingCompanyName#

First Name: #Arguments.shippingFirstName#

Last Name: #Arguments.shippingLastName#

Address: #Arguments.shippingAddress1#<cfif Compare(Arguments.shippingAddress2,"")>, #Arguments.shippingAddress2#</cfif>

City: #Arguments.shippingCity#

State: #Arguments.shippingState#

ZIP: #Arguments.shippingZip#

Country: #Arguments.shippingCountry#

Phone: #Arguments.shippingPhonePart1#-#Arguments.shippingPhonePart2#-#Arguments.shippingPhonePart3#
     </cfoutput>
    </cfsavecontent>
    
    <cfset Variables.address="#Arguments.address1#">
	<cfif Compare(Arguments.address2,"")>
      <cfset Variables.address=Variables.address & ", " & Arguments.address2>
    </cfif>
    
    <cfquery datasource="#this.datasource#">
      INSERT INTO fm_cf_submission
      (formID, submissionDate, amount, firstName, lastName, address,
       phonePrimary, email, submissionDataHTML, submissionDataText, sourceCode, orderID, authCode, ip)
      VALUES
      (<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.formID#">,
       <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
       <cfqueryparam cfsqltype="cf_sql_float" value="#Arguments.donationAmount#">,       
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.firstName#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.lastName#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Variables.address#">,
       
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.phonePart1#-#Arguments.phonePart2#-#Arguments.phonePart3#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.emailAddress#">,
       
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#dataHTML#">,
       <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#dataText#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.sourceCode#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.orderID#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.authCode#">,
       <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.clientIP#">)
    </cfquery>
    
    <cfquery name="qSubmissionID" datasource="#this.datasource#">
      SELECT MAX(submissionID) AS submissionID
      FROM fm_cf_submission
    </cfquery>
    
    <cfset Variables.submissionID=qSubmissionID.submissionID>
        
    <cfset formInfo=getFormInfo(Arguments.formID)> 
    
    
    <cfset Variables.notificationEmailRecipients="test@yahoo.com">
    
    <!--- Notification Email (to admin) --->
    <cfmail from="#formInfo.notificationEmailReplyAddress#" to="#formInfo.notificationEmailRecipients#" subject="#formInfo.notificationEmailSubject#" type="html">
      <cfmailpart type="text" charset="utf-8">
#dataText#
      </cfmailpart>
      <cfmailpart type="html" charset="utf-8">
        <a href="#this.siteURLRoot#"><img src="#this.siteURLRoot#/images/imgLogoEmailNot.gif" alt="Woman's - exceptional care, centered on you" width="96" height="84" border="0" /></a>
        <br /><br /><br /> 
        <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:12px;color:##333132;line-height:130%;">
		  #dataHTML#
        </div>
        <br />
        <hr size="1" noshade="noshade" />
        <a href="#this.siteURLRoot#" style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;font-size:19px;color:##627d79;line-height:110%;text-decoration:none;"><b>WWW.WOMANS.ORG</b></a>
      </cfmailpart>
    </cfmail>
        
    <!--- Thank You Email (to client) --->
    <cfmail from="#formInfo.thankyouEmailReplyAddress#" to="#Arguments.emailAddress#" subject="#formInfo.thankyouEmailSubject#" type="html">
      <cfmailpart type="text" charset="utf-8">
#formInfo.thankyouMessage#
      </cfmailpart>
      <cfmailpart type="html" charset="utf-8">
        <a href="#this.siteURLRoot#"><img src="#this.siteURLRoot#/images/imgLogoEmailNot.gif" alt="Woman's - exceptional care, centered on you" width="96" height="84" border="0" /></a>
        <br /><br /><br /> 
        <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;font-size:12px;color:##333132;line-height:130%;">
		  #Replace(formInfo.thankyouMessage, Chr(10), "<br />", "All")#
        </div>
        <br />
        <hr size="1" noshade="noshade" />
        <a href="#this.siteURLRoot#" style="font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;font-size:19px;color:##627d79;line-height:110%;text-decoration:none;"><b>WWW.WOMANS.ORG</b></a>
      </cfmailpart>
    </cfmail>
  
    <cfreturn Variables.submissionID>
  </cffunction>
</cfcomponent>