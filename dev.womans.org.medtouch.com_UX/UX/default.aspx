<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">

    <title>MedTouch UX Demo Site</title>

	<meta name="title" content="">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<!--meta name="google-site-verification" content=""-->

	<meta name="Copyright" content="">
	
	<!-- Mobile Device Scale/Zoom -->	
    <!--meta name="viewport" content="width=device-width"-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <!-- Fav Icons -->
	<link rel="shortcut icon" href="/assets/images/favicon.ico">
	<link rel="apple-touch-icon" href="/assets/images/apple-touch-icon.png">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="/ux/_includes/ux-style.css">

	<!-- <head> loaded scripts -->
	<script src="/assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>		
	<script src="/assets/js/prefixfree.min.js"></script>
</head>
<body>
    <header>
        <div class="top grid">
			<section class="top">
				<div class="logo">
					<a title="MedTouch" href="#"><img src="/UX/_includes/logo.png" alt="MedTouch" class="screen-logo" /></a>
				</div>
			</section>
        </div>
    </header>
	<section class="wrapper">
		 <div class="content grid">
				<h1>Demo Site</h1>
				<!--------==========================TABS JQUERY==============================-->
                <div class="responsive-tabs">
					<h2>HTML</h2>
					<div>
						<h3>Main Site Templates</h3>
						<div class="row">
							<article><a href="/UX/HTML/template.html" target="_blank">Template</a></article>
							<article class="desc">Base of all pages</article>
						</div>				
						<div class="row">
							<article><a href="/UX/HTML/index.aspx" target="_blank">Home</a></article>
							<article class="desc">Homepage template</article>
						</div>
						<div class="row">
							<article><a href="#" target="_blank">Home Page 2</a></article>
							<article class="desc">Home 2</article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/inner.aspx" target="_blank">3-Columns</a></article>
							<article class="desc">Inner</article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/inner-wr.aspx" target="_blank">2-Columns-WR</a></article>
							<article class="desc">Wide Right</article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/inner-wl.aspx" target="_blank">2-Columns-WL</a></article>
							<article class="desc">Wide Left</article>
						</div>						
					</div>
					<h2>Modules</h2>
					<div>
						<h3>Physician Directory</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/PhysicianDirectory/PhysicianDirectoryLanding.aspx" target="_blank">Physician Landing</a></article>
							<article class="desc">Doctor Page</article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/PhysicianDirectory/PhysicianDirectoryResults.aspx" target="_blank">Physician Listing</a></article>
							<article class="desc">Doctor List</article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/modules/PhysicianDirectory/PhysicianDirectoryDetail.aspx" target="_blank">Physician Detail</a></article>
							<article class="desc">Doctor Detail/Bio</article>
						</div>
						
						<h3>News Module</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/News/NewsLanding.aspx" target="_blank">News Landing</a></article>
							<article class="desc">News Page</article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/News/NewsResults.aspx" target="_blank">News Listing</a></article>
							<article class="desc">News List</article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/modules/News/NewsDetail.aspx" target="_blank">News Detail</a></article>
							<article class="desc">News Detail</article>
						</div>		


						<h3>Locations Module</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/LocationDirectory/LocationLanding.aspx" target="_blank">Locations Landing</a></article>
							<article class="desc">Location Page</article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/LocationDirectory/LocationResults.aspx" target="_blank">Locations Listing</a></article>
							<article class="desc">Listing</article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/modules/LocationDirectory/LocationDetail.aspx" target="_blank">Locations Profile</a></article>
							<article class="desc">Locale Detail</article>
						</div>	
						
						<h3>Search Module</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/SiteSearch/SiteSearchResults.aspx" target="_blank">Search Results</a></article>
							<article class="desc">Location Page</article>
						</div>						
						<div class="row">
							<article><a href="/UX/HTML/modules/SiteSearch/SiteSearchResultsCombined.aspx" target="_blank">Search Results Combined</a></article>
							<article class="desc">Location Page</article>
						</div>
						
						
						<h3>Services Module</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/Services/ServicesLanding.aspx" target="_blank">Services Landing</a></article>
							<article class="desc">Services Page</article>
						</div>					

						<h3>Calendar Module</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/Calendar/CalendarLanding.aspx" target="_blank">Calendar Landing</a></article>
							<article class="desc">Calendar Page</article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/Calendar/CalendarResults.aspx" target="_blank">Calendar Results</a></article>
							<article class="desc">Listing</article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/modules/Calendar/CalendarDetail.aspx" target="_blank">Calendar Detail</a></article>
							<article class="desc">Calendar Detail</article>
						</div>	

						<h3>FAQ Module</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/FAQ/FAQLanding.aspx" target="_blank">FAQ Search</a></article>
							<article class="desc">FAQ Landing</article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/FAQ/FAQResults.aspx" target="_blank">FAQ Results</a></article>
							<article class="desc">Results</article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/modules/FAQ/FAQDetail.aspx" target="_blank">FAQ Detail</a></article>
							<article class="desc">FAQ Detail</article>
						</div>	

						<h3>Greeting Cards Module</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/GreetingCards/GreetingCardsLanding.aspx" target="_blank">Greeting Card Category</a></article>
							<article class="desc"></article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/GreetingCards/GreetingCardsForm.aspx" target="_blank">Greeting Card Form</a></article>
							<article class="desc"></article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/modules/GreetingCards/GreetingCardsPreview.aspx" target="_blank">Greeting Card Preview</a></article>
							<article class="desc"></article>
						</div>

						<h3>Clinical Trials Module</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/ClinicalTrials/ClinicalTrialsLanding.aspx" target="_blank">CT Landing</a></article>
							<article class="desc"></article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/ClinicalTrials/ClinicalTrialsResults.aspx" target="_blank">CT Results</a></article>
							<article class="desc"></article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/modules/ClinicalTrials/ClinicalTrialsDetail.aspx" target="_blank">CT Detail</a></article>
							<article class="desc"></article>
						</div>	

						<h3>Publications Module</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/Publications/PublicationsLanding.aspx" target="_blank">Publications Search</a></article>
							<article class="desc"></article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/Publications/PublicationsResults.aspx" target="_blank">Publications Results</a></article>
							<article class="desc"></article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/modules/Publications/PublicationsDetail.aspx" target="_blank">Publications Detail</a></article>
							<article class="desc"></article>
						</div>	

                        <h3>StayWell Health Library Module</h3>
						
						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellCategorizedSearch.aspx" target="_blank">StayWell Categorized Search</a></article>
							<article class="desc"></article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellCollection.aspx" target="_blank">StayWell Collection</a></article>
							<article class="desc"></article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellCollectionFiltered.aspx" target="_blank">StayWell Collection Filtered</a></article>
							<article class="desc"></article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellContentTypeContents.aspx" target="_blank">StayWell Content Type Contents</a></article>
							<article class="desc"></article>
						</div>						
						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellDrugDescription.aspx" target="_blank">StayWell Drug Description</a></article>
							<article class="desc"></article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellDrugDescription.aspx" target="_blank">StayWell Drug Interaction</a></article>
							<article class="desc"></article>
						</div>						
						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellDrugSearch.aspx" target="_blank">StayWell Drug Search</a></article>
							<article class="desc"></article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellDynamicCollection.aspx" target="_blank">StayWell Dynamic Collection</a></article>
							<article class="desc"></article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellHealthNews.aspx" target="_blank">StayWell Health News</a></article>
							<article class="desc"></article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellLatestNewsResult.aspx" target="_blank">StayWell Latest News Result</a></article>
							<article class="desc"></article>
						</div>						
						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellLibrary.aspx" target="_blank">StayWell Library</a></article>
							<article class="desc"></article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellLocalContent.aspx" target="_blank">StayWell Local Content</a></article>
							<article class="desc"></article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellLocalContentwithAlpha.aspx" target="_blank">StayWell Local Content with Alpha</a></article>
							<article class="desc"></article>
						</div>                               
 						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellLocalContentwithSearch.aspx" target="_blank">StayWell Local Content with Search</a></article>
							<article class="desc"></article>
						</div>
 						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellLocalContentwithAlpha.aspx" target="_blank">StayWell Local Content with Alpha</a></article>
							<article class="desc"></article>
						</div>						
  						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellRelatedContentListing.aspx" target="_blank">StayWell Related Content Listing</a></article>
							<article class="desc"></article>
						</div>	 
   						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellSearchResults.aspx" target="_blank">StayWell Search Results</a></article>
							<article class="desc"></article>
						</div>	                                                                                             
   						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellSymptomChecker.aspx" target="_blank">StayWell Symptom Checker</a></article>
							<article class="desc"></article>
						</div>	
   						<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellVideoPlayer.aspx" target="_blank">StayWell Video Player</a></article>
							<article class="desc"></article>
						</div>
    					<div class="row">
							<article><a href="/UX/HTML/modules/StayWell/StayWellWSContent.aspx" target="_blank">StayWell WS Content</a></article>
							<article class="desc"></article>
						</div> 

						<h3>Gallery Module</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/PhotoGallery/GalleriaGalleryOneCol.aspx" target="_blank">Gallery (1-col)</a></article>
							<article class="desc">FAQ Landing</article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/PhotoGallery/GalleriaGalleryTwoColWideRight.aspx" target="_blank">Gallery (2-col)</a></article>
							<article class="desc">Results</article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/modules/PhotoGallery/GalleriaGalleryThreeColumn.aspx" target="_blank">Gallery (3-col)</a></article>
							<article class="desc">FAQ Detail</article>
						</div>						
					
						<h3>Blog Module</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/Blog/BlogLanding.aspx" target="_blank">Blog</a></article>
							<article class="desc">Blog Landing</article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/Blog/BlogPost.aspx" target="_blank">Blog Post</a></article>
							<article class="desc">Posts</article>
						</div>	

						<h3>Policy Module</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/PolicyProcedures/PolicyProceduresLanding.aspx" target="_blank">Policy Procedures Landing</a></article>
							<article class="desc">Policy Landing</article>
						</div>
						<div class="row">
							<article><a href="/UX/HTML/modules/PolicyProcedures/PolicyProceduresResults.aspx" target="_blank">Policy Procedures Results</a></article>
							<article class="desc"></article>
						</div>						
						<div class="row">
							<article><a href="/UX/HTML/modules/PolicyProcedures/PolicyProceduresDetail.aspx" target="_blank">Policy Procedures Detail</a></article>
							<article class="desc"></article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/modules/PolicyProcedures/UpcomingPolicyProceduresDetail.aspx" target="_blank">Upcoming Policy Procedures Detail</a></article>
							<article class="desc"></article>
						</div>							

						<h3>Form</h3>
						<div class="row">
							<article><a href="/UX/HTML/modules/ContactUs/ContactUsLanding.aspx" target="_blank">Contact Us</a></article>
							<article class="desc">Base Form</article>
						</div>	
						<div class="row">
							<article><a href="/UX/HTML/modules/OnlineDonations/DonationsLanding.aspx" target="_blank">Online Donation</a></article>
							<article class="desc">Online Donation</article>
						</div>							
					</div>
					<h2>Frames</h2>
					<div>
						<div class="row">
							<article><a href="#" target="_blank">Home Frame</a></article>
							<article class="desc">Homepage template</article>
						</div>
						<div class="row">
							<article><a href="#" target="_blank">Home Frame 2</a></article>
							<article class="desc">Home 2</article>
						</div>					
					</div>			
					<h2>Designs</h2>
					<div>
						<div class="row">
							<article><a href="HTML/Designs/home/Home-Expanded-Quicklinks-v5.jpg" target="_blank">Homepage Expanded</a></article>
						</div>
						<div class="row">
							<article><a href="HTML/Designs/home/ipad-portrait-home.jpg" target="_blank">Homepage iPad portrait</a></article>
						</div>
						<div class="row">
							<article><a href="HTML/Designs/home/ipad-portrait-quicklinks.jpg" target="_blank">Homepage iPad portait quicklinks</a></article>
						</div>
						<div class="row">
							<article><a href="HTML/Designs/home/mobile-portrait-homepage.jpg" target="_blank">Homepage iPhone portrait</a></article>
						</div>	
						<div class="row">
							<article><a href="HTML/Designs/interior/ipad-portrait-interior.jpg" target="_blank">Interior iPad portrait</a></article>
						</div>
						<div class="row">
							<article><a href="HTML/Designs/interior/mobile-portrait-interior.jpg" target="_blank">Interior mobile portrait</a></article>
						</div>
						<div class="row">
							<article><a href="HTML/Designs/interior/Womans-Interior-3-Column.jpg" target="_blank">Interior 3 col</a></article>
						</div>
						<div class="row">
							<article><a href="HTML/Designs/interior/Womans-Interior-Wide-Left.jpg" target="_blank">Interior Wide Left</a></article>
						</div>
						<div class="row">
							<article><a href="HTML/Designs/interior/Womans-Physician-Detail.jpg" target="_blank">Physician Detail</a></article>
						</div>	
						<div class="row">
							<article><a href="HTML/Designs/services-landing/ipad-portrait-services-landing.jpg" target="_blank">Services Landing iPad portrait</a></article>
						</div>
						<div class="row">
							<article><a href="HTML/Designs/services-landing/mobile-portrait-services.jpg" target="_blank">Services mobile portrait</a></article>
						</div>
						<div class="row">
							<article><a href="HTML/Designs/services-landing/Womans-Service-Landing.jpg" target="_blank">Woman's Services Landing</a></article>
						</div>
						<div class="row">
							<article><a href="HTML/Designs/services-landing/Womans-Service-Landing-Content-Expanded.jpg" target="_blank">Woman's Services Landing Content Expanded</a></article>
						</div>
						<div class="row">
							<article><a href="HTML/Designs/services-landing/Womans-Service-Landing-Service-Menu-Expanded.jpg" target="_blank">Woman's Services Landing Service Menu Expanded</a></article>
						</div>					
					</div>			

				</div>

			</div>
	</section>
	<footer>
		<div>
			<div class="logo"><a href="http://www.medtouch.com"><img src="/assets/images/footer-logo.png" alt="MedTouch" /></a></div>
			<div class="copy">
				&copy; 2014 <a href="http://www.medtouch.com">MedTouch</a>. All Rights Reserved.
			</div>
		</div>
	</footer>

    <!-- Scripts -->
	<!-- Grab Google CDN jQuery. fall back to local if necessary -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>!window.jQuery && document.write('<script src="/assets/js/jquery-1.9.1.min.js"><\/script>')</script>
	<!-- Grab Google CDN jQuery UI. fall back to local if necessary -->
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script>!window.jQuery.ui && document.write('<script src="/assets/js/jquery-ui-1.10.4.min.js"><\/script>')</script>
	<!-- Local Scripts -->
	<script src="/assets/js/plugins.js"></script>
	<script src="/assets/js/script.js"></script>

</body>
</html>
