﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWR.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/locations/LocationDetail.ascx" tagname="locationdetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/locations/LocationListing.ascx" tagname="locationlisting" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/locations/LocationQuickSearch.ascx" tagname="locationquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/locations/LocationSearch.ascx" tagname="locationsearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/locations/LocationSearchAgain.ascx" tagname="locationsearchagain" tagprefix="module" %>


<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
    <module:locationsearchagain ID="locationsearchagain" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">
	<article>
		<h1>Find a Location</h1>
	</article>
	<module:locationlisting ID="locationresults" runat="server" />
</asp:Content>
