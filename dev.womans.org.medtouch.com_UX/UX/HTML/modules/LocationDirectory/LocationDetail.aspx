﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/locations/LocationDetail.ascx" tagname="locationdetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/locations/LocationListing.ascx" tagname="locationlisting" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/locations/LocationQuickSearch.ascx" tagname="locationquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/locations/LocationSearch.ascx" tagname="locationsearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/locations/LocationSearchAgain.ascx" tagname="locationsearchagain" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
	<module:locationdetail ID="locationdetail" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
    <module:locationquicksearch ID="locationquicksearch" runat="server" />
</asp:Content>
