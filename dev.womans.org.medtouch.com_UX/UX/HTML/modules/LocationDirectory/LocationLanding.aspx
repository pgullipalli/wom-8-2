﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/locations/LocationDetail.ascx" tagname="locationdetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/locations/LocationListing.ascx" tagname="locationlisting" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/locations/LocationQuickSearch.ascx" tagname="locationquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/locations/LocationSearch.ascx" tagname="locationsearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/locations/LocationSearchAgain.ascx" tagname="locationsearchagain" tagprefix="module" %>
<%@ Register TagPrefix="uc" TagName="callout" Src="/UX/HTML/_includes/callout.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
    <!--<div class="breadcrumbs">
		<a href="index.aspx">Home</a><span class="separator"> &gt; </span>
		Find a Location
	</div>-->
	<div class="content">
	<article>
		<h1>Find a Location</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu accumsan felis. Proin rutrum massa et felis suscipit pulvinar. Maecenas iaculis lorem elit, vel sagittis risus. Vivamus tristique ipsum at dolor rhoncus sed hendrerit est faucibus.</p>
	</article>

	<module:locationsearch ID="locationsearch" runat="server" />
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
    <uc:callout ID="callout" runat="server" />
</asp:Content>
