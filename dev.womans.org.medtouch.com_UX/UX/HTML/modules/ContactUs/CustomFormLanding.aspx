﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWR.master" AutoEventWireup="true" %>

<%@ Register TagPrefix="uc" TagName="prox" Src="/UX/HTML/_includes/prox.ascx" %>
<%@ Register src="/ux/html/_includes/modules/contact_us/CustomForm.ascx" tagname="customform" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
    <uc:prox id="uxProx" runat="server"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">
	<div class="content">	
	<article>
		<h1>Send Us a Message</h1>
		<p>Thank you for your interest in Providence Health & Services. Please use this form for non-urgent, non-medical questions or comments. Messages sent via this form are not secure and should not contain personal health information. Call 911 if you have a life-threatening emergency.</p>
    </article>	
    <module:customform ID="customform" runat="server" />
	</div>
</asp:Content>
