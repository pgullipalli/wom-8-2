﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master"
    AutoEventWireup="true" %>

<%@ Register Src="/ux/html/_includes/modules/blog/BlogListing.ascx" TagName="bloglisting" TagPrefix="module" %>
<%@ Register Src="/ux/html/_includes/modules/blog/BlogSearch.ascx" TagName="blogsearch" TagPrefix="module" %>
<%@ Register Src="/ux/html/_includes/modules/blog/BlogCategories.ascx" TagName="blogcategories" TagPrefix="module" %>
<%@ Register Src="/ux/html/_includes/modules/blog/BlogTopics.ascx" TagName="blogtopics" TagPrefix="module" %>
<%@ Register Src="/ux/html/_includes/modules/blog/BlogArchive.ascx" TagName="blogarchive" TagPrefix="module" %>
<%@ Register Src="/ux/html/_includes/modules/blog/BlogPostLogin.ascx" TagName="blogpostlogin" TagPrefix="module" %>

<%@ Register TagPrefix="uc" TagName="breadcrumb" Src="/UX/HTML/_includes/breadcrumb.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
    <uc:breadcrumb ID="Breadcrumb1" UD="breadcrumb" runat="server" />
    
    <module:blogpostlogin ID="blogpostlogin" runat="server" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
    <module:blogsearch ID="blogserach" runat="server" />
    <module:blogcategories ID="blogcategories" runat="server" />
    <module:blogtopics ID="blogtopics" runat="server" />
    <module:blogarchive ID="blogarchive" runat="server" />
</asp:Content>
