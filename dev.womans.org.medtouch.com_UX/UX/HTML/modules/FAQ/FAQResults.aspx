﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWR.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/faq/FAQCategories.ascx" tagname="faqcategories" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FAQDetail.ascx" tagname="faqdetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FAQListing.ascx" tagname="faqlisting" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FAQQuickSearch.ascx" tagname="faqquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FAQSearch.ascx" tagname="faqsearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FeaturedFAQ.ascx" tagname="featuredfaq" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/RelatedFAQ.ascx" tagname="relatedfaq" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
    <module:faqquicksearch ID="faqquicksearch" runat="server" />
	<module:faqcategories ID="faqcategories" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">
	<article>
		<h1>FAQ Results</h1>
	</article>
	<module:faqlisting ID="faqlisting" runat="server" />
</asp:Content>
