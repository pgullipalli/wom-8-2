﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWR.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/physicians/PhysicianSearchAgain.ascx" tagname="searchagain" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/physicians/PhysicianListing.ascx" tagname="physicianresults" tagprefix="module" %>


<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
	<module:searchagain ID="searchagain" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">

	<article>
		<h1>Find a Doctor Results</h1>
	</article>	
    <module:physicianresults ID="physicianresults" runat="server" />
</asp:Content>