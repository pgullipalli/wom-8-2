﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true" %>


<%@ Register src="/ux/html/_includes/modules/patient_testimonials/PatientTestimonialsListing.ascx" tagname="testimoniallisting" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/patient_testimonials/PatientTestimonialsRotator.ascx" tagname="testimonialrotator" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/patient_testimonials/RelatedPatientTestimonials.ascx" tagname="relatedtestimonials" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
    <div class="breadcrumbs">
			<a href="index.aspx">Home</a><span class="separator"> &gt; </span>
			Patient Testimonials 
	</div>
    <article>
		<h1>Patient Testimonials (h1)</h1>
		<p>Lorem ipsum dolor sit amet, <b>bold text (b)</b>consectetur adipiscing elit. Curabitur <strong>bold text (strong)</strong>condimentum blandit diam 
		pharetra viverra. <i>Italic text (i)</i>Cras rutrum. <em>Italic text (em)</em>Urna eu consequat tempor, massa lacus. Ipsum lorem dolor amet sit aliquet 
		eget scelerisque eo consequat.</p>
    </article>	
	<module:testimoniallisting ID="testimoniallisting" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
    <module:testimonialrotator ID="testimonialrotator" runat="server" />
	<module:relatedtestimonials ID="relatedtestimonials" runat="server" />
</asp:Content>
