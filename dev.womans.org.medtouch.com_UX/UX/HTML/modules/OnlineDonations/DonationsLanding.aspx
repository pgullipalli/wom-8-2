﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWR.master" AutoEventWireup="true" %>

<%@ Register TagPrefix="uc" TagName="prox" Src="/UX/HTML/_includes/prox.ascx" %>
<%@ Register src="/ux/html/_includes/modules/onlinedonations/Form.ascx" tagname="onlinedonations" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
    <uc:prox id="uxProx" runat="server"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">
    <link type="text/css" rel="stylesheet" href="/sitecore%20modules/shell/Web%20Forms%20for%20Marketers/themes/MedTouch.css">

	<div class="breadcrumbs">
			<a href="index.aspx">Home</a><span class="separator"> &gt; </span>
			<a href="#">Lorem Allerium Section</a><span class="separator"> &gt; </span>
			Online Donations
	</div>
	<article>
		<h1>Online Donations (h1)</h1>
		<p>Lorem ipsum dolor sit amet, <b>bold text (b)</b>consectetur adipiscing elit. Curabitur <strong>bold text (strong)</strong>condimentum blandit diam 
		pharetra viverra. <i>Italic text (i)</i>Cras rutrum. <em>Italic text (em)</em>Urna eu consequat tempor, massa lacus. Ipsum lorem dolor amet sit aliquet 
		eget scelerisque eo consequat.</p>
    </article>	
    <module:onlinedonations ID="onlinedonations" runat="server" />
</asp:Content>
