﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/policyprocedures/FeaturedPolicyProcedure.ascx" tagname="featuredpolicyprocedure" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/LatestPolicyProcedure.ascx" tagname="latestpolicyprocedure" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/PolicyProcedureDetail.ascx" tagname="policyproceduredetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/PolicyProcedureListing.ascx" tagname="policyprocedurelisting" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/PolicyProcedureSearch.ascx" tagname="policyproceduresearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/PolicyProcedureQuickSearch.ascx" tagname="policyprocedurequicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/PolicyProcedureSitemap.ascx" tagname="policyproceduresitemap" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/RelatedPolicyProcedure.ascx" tagname="relatedpolicyprocedure" tagprefix="module" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
			<module:policyproceduredetail ID="policyproceduredetail" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
		<module:policyprocedurequicksearch ID="policyprocedurequicksearch" runat="server" />
		<module:relatedpolicyprocedure ID="relatedpolicyprocedure" runat="server" />
</asp:Content>
