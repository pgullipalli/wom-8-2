﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWR.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/publications/LatestPublications.ascx" tagname="latestpublications" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/PublicationArchive.ascx" tagname="publicationarchive" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/PublicationCategories.ascx" tagname="publicationcategories" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/PublicationDetail.ascx" tagname="publicationdetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/PublicationListing.ascx" tagname="publicationlisting" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/PublicationSearch.ascx" tagname="publicationsearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/PublicationQuickSearch.ascx" tagname="publicationquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/RelatedPublications.ascx" tagname="relatedpublications" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/FeaturedPublications.ascx" tagname="featuredpublications" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
    <module:publicationquicksearch ID="publicationquicksearch" runat="server"  />
    <module:publicationarchive ID="publicationarchive" runat="server"  />
    <module:publicationcategories ID="publicationcategories" runat="server"  />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">
    <div class="breadcrumbs">
			<a href="index.aspx">Home</a><span class="separator"> &gt; </span>
			<a href="#">Publications</a><span class="separator"> &gt; </span>
			Results
	</div>
	<article>
		<h1>Lorem Ipsum Dolar Amet (h1)</h1>
	</article>
    <module:publicationlisting ID="publicationlisting" runat="server"  />
</asp:Content>
