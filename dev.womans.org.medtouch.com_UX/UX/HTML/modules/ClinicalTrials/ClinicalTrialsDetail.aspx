﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true"  %>

<%@ Register src="/ux/html/_includes/modules/clinical_trials/CTDetail.ascx" tagname="ctdetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/clinical_trials/CTSearchBox.ascx" tagname="ctsearchbox" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/clinical_trials/CTQuickSearchBox.ascx" tagname="ctquicksearchbox" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/clinical_trials/CTSearchResults.ascx" tagname="ctsearchresults" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/clinical_trials/RelatedClinicalTrials.ascx" tagname="relatedclinicaltrials" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/clinical_trials/RelatedClinicalTrialsCallout.ascx" tagname="relatedclinicaltrialscallout" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
			<div class="breadcrumbs">
					<a href="index.aspx">Home</a><span class="separator"> &gt; </span>
					<a href="#">Clinical Trials</a><span class="separator"> &gt; </span>
					Clinical Trial A
			</div>

            <module:ctdetail ID="ctdetail"  runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
	<module:relatedclinicaltrialscallout ID="relatedclinicaltrialscallout" runat="server" />
</asp:Content>
