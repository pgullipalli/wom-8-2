﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/ThreeColumn.master" AutoEventWireup="true" %>
<%@ Register TagPrefix="uc" TagName="prox" Src="/UX/HTML/_includes/prox.ascx" %>
<%@ Register src="/ux/html/_includes/callout.ascx" tagname="callout" tagprefix="module" %>

<%@ Register src="/ux/html/_includes/modules/staywell/StayWellDrugDescriptionIFrame.ascx" tagname="StayWellDrugDescriptionIFrame" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellDrugInteractionIFrame.ascx" tagname="StayWellDrugInteractionIFrame" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellDrugSearchIFrame.ascx" tagname="StayWellDrugSearchIFrame" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetCollectionContentFilteredDisplay.ascx" tagname="StayWellGetCollectionContentFilteredDisplay" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetCollectionContents.ascx" tagname="StayWellGetCollectionContents" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetContentByID.ascx" tagname="StayWellGetContentByID" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetDynamicCollections.ascx" tagname="StayWellGetDynamicCollections" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetLatestNewsResults.ascx" tagname="StayWellGetLatestNewsResults" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetLatestNewsSearchBox.ascx" tagname="StayWellGetLatestNewsSearchBox" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetRelatedContentById.ascx" tagname="StayWellGetRelatedContentById" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetRelatedContentByMeSHCode.ascx" tagname="StayWellGetRelatedContentByMeSHCode" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetRelatedContentPaginated.ascx" tagname="StayWellGetRelatedContentPaginated" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetRelatedNews.ascx" tagname="StayWellGetRelatedNews" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellListByAlphabetForm.ascx" tagname="StayWellListByAlphabetForm" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellListContentByContentType.ascx" tagname="StayWellListContentByContentType" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSearchBox.ascx" tagname="StayWellSearchBox" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSearchByAlphabet.ascx" tagname="StayWellSearchByAlphabet" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSearchByCategory.ascx" tagname="StayWellSearchByCategory" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSearchResults.ascx" tagname="StayWellSearchResults" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSymptomCheckerIFrame.ascx" tagname="StayWellSymptomCheckerIFrame" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellVideoPlayer.ascx" tagname="StayWellVideoPlayer" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
    <uc:prox ID="prox" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">

    <div class="health-library-results listing grid">
	<h1>Wellness Library</h1>
		<div class="listing-item">
				<div class="module-thumbnail two columns">
					<img src="http://placehold.it/50x50" alt="photo_of_event_award_gala" width="100" />
				</div>
				<div class="teaser-copy ten columns">
					<a href="/health-library/wellness-library/at-work/">At Work</a>
					<div class="listing-item-teaser">
						<p>You spend much of your weekday on the job. Find tips here on how to make the most of your opportunities at work.</p>
					</div>
				</div>	
		</div>
		
		<div class="listing-item">
				<div class="module-thumbnail two columns">
					<img src="http://placehold.it/50x50" alt="photo_of_event_award_gala" width="100" />
				</div>
				<div class="teaser-copy ten columns">
					<a href="/health-library/wellness-library/at-work/">At Work</a>
					<div class="listing-item-teaser">
						<p>You spend much of your weekday on the job. Find tips here on how to make the most of your opportunities at work.</p>
					</div>
				</div>	
		</div>

		<div class="listing-item">
				<div class="module-thumbnail two columns">
					<img src="http://placehold.it/50x50" alt="photo_of_event_award_gala" width="100" />
				</div>
				<div class="teaser-copy ten columns">
					<a href="/health-library/wellness-library/at-work/">At Work</a>
					<div class="listing-item-teaser">
						<p>You spend much of your weekday on the job. Find tips here on how to make the most of your opportunities at work.</p>
					</div>
				</div>	
		</div>


		<div class="listing-item">
				<div class="module-thumbnail two columns">
					<img src="http://placehold.it/50x50" alt="photo_of_event_award_gala" width="100" />
				</div>
				<div class="teaser-copy ten columns">
					<a href="/health-library/wellness-library/at-work/">At Work</a>
					<div class="listing-item-teaser">
						<p>You spend much of your weekday on the job. Find tips here on how to make the most of your opportunities at work.</p>
					</div>
				</div>	
		</div>	
		<div class="listing-item">
				<div class="module-thumbnail two columns">
					<img src="http://placehold.it/50x50" alt="photo_of_event_award_gala" width="100" />
				</div>
				<div class="teaser-copy ten columns">
					<a href="/health-library/wellness-library/at-work/">At Work</a>
					<div class="listing-item-teaser">
						<p>You spend much of your weekday on the job. Find tips here on how to make the most of your opportunities at work.</p>
					</div>
				</div>	
		</div>		
	</div>

<module:StayWellSearchBox ID="StayWellSearchBox" runat="server" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightPanel" runat="server">
    <module:callout ID="callout" runat="server" />
</asp:Content>
