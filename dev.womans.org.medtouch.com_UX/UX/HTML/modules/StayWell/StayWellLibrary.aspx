﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/ThreeColumn.master" AutoEventWireup="true" %>
<%@ Register TagPrefix="uc" TagName="prox" Src="/UX/HTML/_includes/prox.ascx" %>
<%@ Register src="/ux/html/_includes/callout.ascx" tagname="callout" tagprefix="module" %>

<%@ Register src="/ux/html/_includes/modules/staywell/StayWellDrugDescriptionIFrame.ascx" tagname="StayWellDrugDescriptionIFrame" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellDrugInteractionIFrame.ascx" tagname="StayWellDrugInteractionIFrame" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellDrugSearchIFrame.ascx" tagname="StayWellDrugSearchIFrame" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetCollectionContentFilteredDisplay.ascx" tagname="StayWellGetCollectionContentFilteredDisplay" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetCollectionContents.ascx" tagname="StayWellGetCollectionContents" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetContentByID.ascx" tagname="StayWellGetContentByID" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetDynamicCollections.ascx" tagname="StayWellGetDynamicCollections" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetLatestNewsResults.ascx" tagname="StayWellGetLatestNewsResults" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetLatestNewsSearchBox.ascx" tagname="StayWellGetLatestNewsSearchBox" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetRelatedContentById.ascx" tagname="StayWellGetRelatedContentById" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetRelatedContentByMeSHCode.ascx" tagname="StayWellGetRelatedContentByMeSHCode" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetRelatedContentPaginated.ascx" tagname="StayWellGetRelatedContentPaginated" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetRelatedNews.ascx" tagname="StayWellGetRelatedNews" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellListByAlphabetForm.ascx" tagname="StayWellListByAlphabetForm" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellListContentByContentType.ascx" tagname="StayWellListContentByContentType" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSearchBox.ascx" tagname="StayWellSearchBox" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSearchByAlphabet.ascx" tagname="StayWellSearchByAlphabet" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSearchByCategory.ascx" tagname="StayWellSearchByCategory" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSearchResults.ascx" tagname="StayWellSearchResults" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSymptomCheckerIFrame.ascx" tagname="StayWellSymptomCheckerIFrame" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellVideoPlayer.ascx" tagname="StayWellVideoPlayer" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
    <uc:prox ID="prox" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">
    <h1>Health Library</h1>    <p>The&nbsp;health library&nbsp;provides information about wellness, disease conditions, procedures and medications.&nbsp;Whether your goal is to get in shape or manage a disease,&nbsp;the library&nbsp;provides information to help you reach that goal.&nbsp;</p>
    <div class="divider"></div>
    <div class="div_section grid">
        <h2>Interactive Tools &amp; Multimedia</h2>
        <!--<table style="margin: -7px 0px 5px; background: url(assets/images/shadow-items-area.gif) no-repeat 50% 0px;">
            <tbody>
                <tr>
                    <td align="center" style="width: 10%;border: 0px;"><a href="/health-library/multimedia-room/video-library/"><img alt="" style="margin-bottom: 3px;" src="~/media/Images/Modules/StayWell/multimedia room/teaser vod.gif" /><br />
                    Video Library</a>&nbsp;</td>
                    <td align="center" style="width: 10%;border: 0px;"><a href="/health-library/interactive-tools/calculators/"><img alt="" style="margin-bottom: 3px;" src="~/media/Images/Modules/StayWell/Interactive Tools/teaser calc.png" /><br />
                    Calculators</a></td>
                    <td align="center" style="width: 10%;border: 0px;"><a href="/health-library/interactive-tools/quizzes/"><img alt="" style="margin-bottom: 3px;" src="~/media/Images/Modules/StayWell/Interactive Tools/teaser quiz.png" /><br />
                    Quizzes</a></td>
                    <td align="center" style="width: 10%;border: 0px;"><a href="/health-library/interactive-tools/risk-assessments/"><img alt="" style="margin-bottom: 3px;" src="~/media/Images/Modules/StayWell/Interactive Tools/teaser assess.png" /><br />
                    Risk Assessment</a></td>
                </tr>
                <tr>
                    <td align="center" style="width: 10%;border: 0px;"><a href="/health-library/multimedia-room/animations/"><img alt="" style="margin-bottom: 3px;" src="~/media/Images/Modules/StayWell/multimedia room/teaser animations.gif" /><br />
                    Animations</a></td>
                    <td align="center" style="width: 10%;border: 0px;"><a href="/health-library/multimedia-room/clinical-wizards/"><img alt="" style="margin-bottom: 3px;" src="~/media/Images/Modules/StayWell/multimedia room/teaser wizards.gif" /><br />
                    Clinical Wizards</a>&nbsp;</td>
                    <td align="center" style="width: 10%;border: 0px;"><a href="/health-library/multimedia-room/podcasts/"><img alt="" style="margin-bottom: 3px;" src="~/media/Images/Modules/StayWell/multimedia room/teaser pod.gif" /><br />
                    Podcasts</a></td>
                </tr>
            </tbody>
        </table>-->
		
		<!--NEW HTML AND CLASS=======================================================================-->
		<div class="health-library grid">
			<div class="three columns">
				<a href="/health-library/multimedia-room/video-library/">
				<img src="http://placehold.it/50x50" alt=""><br />
				Title Subject</a> 
			</div>
			<div class="three columns">
				<a href="/health-library/multimedia-room/video-library/">
				<img src="http://placehold.it/50x50" alt=""><br />
				Title Subject</a> 
			</div>
			<div class="three columns">
				<a href="/health-library/multimedia-room/video-library/">
				<img src="http://placehold.it/50x50" alt=""><br />
				Title Subject</a> 
			</div>
			<div class="three columns">
				<a href="/health-library/multimedia-room/video-library/">
				<img src="http://placehold.it/50x50" alt=""><br />
				Title Subject</a> 
			</div>
			<div class="three columns">
				<a href="/health-library/multimedia-room/video-library/">
				<img src="http://placehold.it/50x50" alt=""><br />
				Title Subject</a> 
			</div>
			<div class="three columns">
				<a href="/health-library/multimedia-room/video-library/">
				<img src="http://placehold.it/50x50" alt=""><br />
				Title Subject</a> 
			</div>
			<div class="three columns">
				<a href="/health-library/multimedia-room/video-library/">
				<img src="http://placehold.it/50x50" alt=""><br />
				Title Subject</a> 
			</div>		
		</div>    </div>
    <div class="div_section">
        <h2>Resources</h2>
			<div class="health-library grid">
				<div class="three columns">
					<a href="/health-library/multimedia-room/video-library/">
					<img src="http://placehold.it/50x50" alt=""><br />
					Title Subject</a> 
				</div>
				<div class="three columns">
					<a href="/health-library/multimedia-room/video-library/">
					<img src="http://placehold.it/50x50" alt=""><br />
					Title Subject</a> 
				</div>
				<div class="three columns">
					<a href="/health-library/multimedia-room/video-library/">
					<img src="http://placehold.it/50x50" alt=""><br />
					Title Subject</a> 
				</div>
				<div class="three columns">
					<a href="/health-library/multimedia-room/video-library/">
					<img src="http://placehold.it/50x50" alt=""><br />
					Title Subject</a> 
				</div>
			</div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightPanel" runat="server">
    <module:callout ID="callout" runat="server" />
</asp:Content>
