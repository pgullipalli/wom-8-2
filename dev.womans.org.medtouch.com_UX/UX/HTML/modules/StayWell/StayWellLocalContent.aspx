﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/ThreeColumn.master" AutoEventWireup="true" %>
<%@ Register TagPrefix="uc" TagName="prox" Src="/UX/HTML/_includes/prox.ascx" %>
<%@ Register src="/ux/html/_includes/callout.ascx" tagname="callout" tagprefix="module" %>

<%@ Register src="/ux/html/_includes/modules/staywell/StayWellDrugDescriptionIFrame.ascx" tagname="StayWellDrugDescriptionIFrame" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellDrugInteractionIFrame.ascx" tagname="StayWellDrugInteractionIFrame" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellDrugSearchIFrame.ascx" tagname="StayWellDrugSearchIFrame" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetCollectionContentFilteredDisplay.ascx" tagname="StayWellGetCollectionContentFilteredDisplay" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetCollectionContents.ascx" tagname="StayWellGetCollectionContents" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetContentByID.ascx" tagname="StayWellGetContentByID" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetDynamicCollections.ascx" tagname="StayWellGetDynamicCollections" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetLatestNewsResults.ascx" tagname="StayWellGetLatestNewsResults" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetLatestNewsSearchBox.ascx" tagname="StayWellGetLatestNewsSearchBox" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetRelatedContentById.ascx" tagname="StayWellGetRelatedContentById" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetRelatedContentByMeSHCode.ascx" tagname="StayWellGetRelatedContentByMeSHCode" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetRelatedContentPaginated.ascx" tagname="StayWellGetRelatedContentPaginated" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellGetRelatedNews.ascx" tagname="StayWellGetRelatedNews" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellListByAlphabetForm.ascx" tagname="StayWellListByAlphabetForm" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellListContentByContentType.ascx" tagname="StayWellListContentByContentType" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSearchBox.ascx" tagname="StayWellSearchBox" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSearchByAlphabet.ascx" tagname="StayWellSearchByAlphabet" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSearchByCategory.ascx" tagname="StayWellSearchByCategory" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSearchResults.ascx" tagname="StayWellSearchResults" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellSymptomCheckerIFrame.ascx" tagname="StayWellSymptomCheckerIFrame" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/staywell/StayWellVideoPlayer.ascx" tagname="StayWellVideoPlayer" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
    <uc:prox ID="prox" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">
    <h1>
        Prevention Guidelines for Women 18-39
    </h1><p>Here are the screening tests and immunizations that most women age 18-39 need. Although you and your healthcare provider may decide that a different schedule is best for you, this plan can guide your discussion. Click to see prevention plans for men and women in different age groups and learn more about each screening.</p>

<div class="twelve columns">
<table class="data-grid" summary="data">
        <thead>
		<tr>
            <th>
            <p style="text-align: center;"><strong>Screening</strong></p>
            </th>
            <th>
            <p style="text-align: center;"><strong>Who needs it</strong></p>
            </th>
            <th>
            <p style="text-align: center;"><strong>How often</strong></p>
            </th>
        </tr>
		</thead>
		<tbody>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Alcohol misuse</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>All adults and pregnant women</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>At routine exams</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Anemia - Iron Deficiency</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>All pregnant women</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>At prenatal visits, especially the first</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Asymptomatic Bacteriuria (with urine culture)</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>All pregnant women</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>At 12-16 weeks' gestation or the first prenatal visit, if later</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Blood pressure</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>All adults</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Joint National Committee on Prevention, Detection, Evaluation, and Treatment of High Blood Pressure recommends the following screening schedules:</p>
            <p>Every 2 years - blood pressure reading &lt; 120/80 mm Hg, and</p>
            <p>Yearly - systolic blood pressure reading of 120 to 139 mm Hg or diastolic blood pressure</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Breast cancer</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Women age 20 years and older*</p>
            <p>Women under the age of 20 years, talk to your healthcare provider and make an informed decision about performing monthly breast self-exams based on your family history, current medical condition, and personal values.</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Clinical breast exam every 3 years*</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Cervical cancer</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>An update to this recommendation is currently in progress and being reviewed by the USPSTF. The recommendation below may contain information that is out of date. Please consult your healthcare provider.</p>
            <p>Women age 21 and older who have been sexually active and have a cervix</p>
            <p>For women age 20 years and younger, please consult your healthcare provider.</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Please discuss with your healthcare provider.</p>
            <p>The American Cancer Society (ACS) recommends a pap test every 2 years.</p>
            <p>The American Congress of Obstetricians and Gynecologists (ACOG) recommends that women age 30 and older get a pap test once every 3 years, and women with certain risk factors (or at increased risk) may need more frequent screening.**</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Chlamydia</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">Sexually active women age 24 and younger, pregnant women age 24 and younger, and women at increased risk for infection</td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>At routine exams</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Depression</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>All adults in clinical practices that have staff and systems in place to assure accurate diagnosis, effective treatment, and follow-up</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>At routine exams</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Diabetes Mellitus, type 2</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Adults who are asymptomatic with sustained blood pressure (either treated or untreated) greater than 135/80 mm Hg</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>At routine exams</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Gonorrhea</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Sexually active women at increased risk for infection, and pregnant women</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>At routine exams</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Hepatitis B virus</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>All pregnant women</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>At first prenatal visit</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>HIV</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Anyone at increased risk for infection, and pregnant women</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>At routine checkups</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Obesity</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>All adults</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>At routine checkups</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Preeclampsia</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Given the availability of new evidence, USPSTF has decided to update this recommendation. The recommendation below may contain information that is out of date. Please consult your healthcare provider.</p>
            <p>All pregnant women</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>This recommendation below may contain information that is out of date. Please consult your healthcare provider.</p>
            <p>At routine checkups</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>h (D) Incompatibility</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>All pregnant women</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>First prenatal visit</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Rubella</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Given the availability of new evidence, USPSTF has decided to update this recommendation. The recommendation below may contain information that is out of date. Please consult your healthcare provider.</p>
            <p>All pregnant women</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>The recommendation below may contain information that is out of date. Please consult your healthcare provider.</p>
            <p>At routine checkups</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Syphilis</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Women at increased risk for infection, and all pregnant women</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>At routine exams</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Tuberculosis</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Anyone at increased risk for infection</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Check with your healthcare provider</p>
            </td>
        </tr>
        <tr>
            <td style="background-color: #c6d9f0;">
            <p style="text-align: center;"><strong>Counseling</strong></p>
            </td>
            <td style="background-color: #c6d9f0;">
            <p style="text-align: center;"><strong>Who needs it</strong></p>
            </td>
            <td style="background-color: #c6d9f0;">
            <p style="text-align: center;"><strong>How often</strong></p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Breast cancer, chemoprevention</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>An update to this recommendation is currently in progress and being reviewed by the USPSTF. The recommendation below may contain information that is out of date. Please consult your healthcare provider.</p>
            <p>Women with high risk</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>The recommendation below may contain information that is out of date. Please consult your healthcare provider.</p>
            <p>When risk is identified</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>BRCA mutation testing for breast and ovarian cancer susceptibility</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Women with increased risk</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>When risk is identified</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Breastfeeding</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>All pregnant women</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>During pregnancy and after delivery</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Diet, behavioral counseling</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Adults with hyperlipidemia and other known risk factors for cardiovascular and diet-related chronic disease</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>When diagnosed</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Tobacco use and Tobacco-Cause Disease</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>All adults</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Every visit</p>
            </td>
        </tr>
        <tr>
            <td style="background-color: #c6d9f0;">
            <p style="text-align: center;"><strong>Immunization</strong></p>
            </td>
            <td style="background-color: #c6d9f0;">
            <p style="text-align: center;"><strong>Who needs it</strong></p>
            </td>
            <td style="background-color: #c6d9f0;">
            <p style="text-align: center;"><strong>How often</strong></p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Human papillomavirus (HPV)</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Recommended for all females age 11 to 26 years, and who lack evidence of immunity</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Three doses</p>
            <p>The second dose should be given one to two months after the first dose, and the third dose should be given six months after the first dose.</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Tetanus/diphtheria/pertussis (Td/Tdap) booster</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>All adults</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Td: Every 10 years</p>
            <p>Tdap: Substitute a one-time dose of Tdap for a Td booster - Once after age 18</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Chickenpox (varicella)</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>All adults age 19 to 49 years and who lack evidence of immunity (i.e., no documentation of prior infection or vaccinations)</p>
            <p>Pregnant women should be assessed for evidence of immunity.</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Two doses. The second dose should be administered 4 to 8 weeks after the first dose.</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Measles, mumps, rubella (MMR) vaccine</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>All adults age 19 to 49 years and who lack evidence of immunity (i.e., no documentation of prior infection or vaccinations)</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>One or two doses</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Flu vaccine (seasonal)</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>People at risk***</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Yearly</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Hepatitis A vaccine</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>People at risk***</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Two doses.</p>
            <p>Schedule:</p>
            <p>Zero and 6 to 12 months (Havrix), OR</p>
            <p>Zero and 6 to 18 months schedule (Vaqta)</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Hepatitis B vaccine</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>People at risk***</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>Three doses. Second dose should be administered 1 month after the first dose; the third dose should be administered at least 2 months after the second dose (and at least 4 months after the first dose)</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>Meningococcal</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>People at risk***</p>
            </td>
            <td class="chartcolor1" style="background-color: #f2f2f2;">
            <p>One or more doses</p>
            </td>
        </tr>
        <tr>
            <td class="chartcolor2" valign="top">
            <p>Pneumococcal (polysaccharide)</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>People at risk***</p>
            </td>
            <td class="chartcolor2" valign="top">
            <p>One or two doses</p>
            </td>
        </tr>
		</tbody>
</table>
</div>
<p>*According to the American Cancer Society, women age 20 to 39 years should have a clinical breast exam as part of their routine health exam every three years, and monthly breast self-exams should begin at the age of 20 years.</p>
<p>**According to the American Congress of Obstetricians and Gynecologists, women 30 and older who have had three consecutive negative Pap or liquid-based cytology tests may be screened every three years.</p>
<p>***Exceptions may exist, please discuss with your healthcare provider</p>
<p>Other Guidelines are from the U.S. Preventive Services Task Force (USPSTF)</p>
<p>Immunization schedule from the Centers for Disease Control and Prevention (CDC)</p>
<p><strong>Online Medical Reviewer:</strong>&nbsp;Mukamal, Kenneth MD&nbsp;<br />
<strong>Online Medical Reviewer:</strong>&nbsp;Oken, Emily MD&nbsp;</p>
<div><strong>Last Review Date:</strong>&nbsp;2/8/2010&nbsp;<br />
</div>
<div id="MainTemplate_ctl11_pnlReviewDate"><br />
</div>
<div id="MainTemplate_ctl11_pnlCopyrightStatement">&copy; 2000-2011 Krames StayWell, 780 Township Line Road, Yardley, PA 19067. All rights reserved. This information is not intended as a substitute for professional medical care. Always follow your healthcare professional's instructions.</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightPanel" runat="server">
    <module:callout ID="callout" runat="server" />
</asp:Content>
