﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWR.master" AutoEventWireup="true"  %>
<%@ Register src="/ux/html/_includes/modules/photogallery/GalleriaGallery.ascx" tagname="galleriagallery" tagprefix="module" %>
<%@ Register TagPrefix="uc" TagName="prox" Src="/UX/HTML/_includes/prox.ascx" %>
<%@ Register TagPrefix="uc" TagName="callout" Src="/UX/HTML/_includes/callout.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="LeftPanel" runat="server">
    <uc:prox ID="prox" runat="server"/>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
	

	<div class="breadcrumbs">
			<a href="index.aspx">Home</a><span class="separator"> &gt; </span>
			Simple Photo Gallery 
	</div>
	<div class="content">
		<h1>Simple Photo Gallery</h1>
		<p>Lorem ipsum dolor sit amet, <b>bold text (b)</b>consectetur adipiscing elit. Curabitur <strong>bold text (strong)</strong>condimentum blandit diam 
		pharetra viverra. <i>Italic text (i)</i>Cras rutrum.</p>
	</div>
    <module:galleriagallery ID="gallerificgallery"  runat="server" />    			
</asp:Content>
