<!--==================== Module: Publications: Featured Publications =============================-->
<div class="module-pb-feature article">
	
    
        <h4><span>Featured Publications</span></h4>
    
    <div class="listing">
		
        
                <div class="listing-item">
			
                    <h5>
                        <a href="/publications/2012/11/test-publication/">Test Publication</a></h5>
                    <div class="module-date">
				
                        November 22, 2012
			</div>
                    
                    <div class="listing-item-teaser">
				
                        <p>Test Publication Content</p>
                    
			</div>
                    <div class="listing-item-more-link">
				
                        <a href="/publications/2012/11/test-publication/">Read More</a>
                    
			</div>
                
		</div>
            
    
	</div>
    

</div>
<!--==================== /Module: Publications: Featured Publications =============================-->
