<!--==================== Module: Publications: Publication Categories =============================-->
<div class="module-pb-categories core-categories callout collapse-for-mobile">
    <div class="reg-callout grid">
		<h3>View by Category</h3>
        <ul class="module-pb-filters-list core-list">
			<li><a class="aspNetDisabled">All</a></li>
			<li><a href="/publications/publication-search-results/?category=category+1">Category 1</a></li>
		</ul>
    </div>
</div>
<!--==================== /Module: Publications: Publication Categories =============================-->