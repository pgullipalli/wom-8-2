<!--==================== Module: Publications: Publication Search =============================-->
<div class="module-pb-search core-search grid">
    <div class="six columns">
        <label class="label">Keyword</label>
        <input type="text" placeholder="enter keyword(s)" class="textbox">
	</div>
    <div class="six columns">
        <label  class="label">Date</label>
		<div class="grid">
			<input type="date" placeholder="Date From" class="dp_input six columns textbox">
			<input type="date" placeholder="Date To" class="dp_input six columns textbox">
		</div>
	</div>
    <div class="twelve columns search-option-submit">
		<input type="submit" value="Search" class="button">
    </div>
</div>
<!--==================== /Module: Publications: Publication Search =============================-->
