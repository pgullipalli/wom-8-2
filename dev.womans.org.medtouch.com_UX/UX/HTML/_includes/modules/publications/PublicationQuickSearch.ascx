<!--==================== Module: Publications: Publication Quick Search =============================-->
<div class="module-pb-search core-search callout collapse-for-mobile">
    <div class="reg-callout">
		<h3>Publication Search</h3>
        <div class="twelve columns">
            <label class="label">Keyword</label>
            <input type="text" placeholder="Enter keyword(s)" class="textbox">
		</div>
        <div class="twelve columns">
        	<label>Filter by Category</label>
 			<div class="selectbox">
				<select class="selectboxdiv">
					<option value="">All Categories</option>
					<option value="category 1">Category 1</option>
					<option value="category 2">Category 2</option>
					<option value="category 3">Category 3</option>
				</select>
				<div class="out"></div>
			</div>
		</div>
        <div class="twelve columns">
            <label class="label">Date</label>
            <input type="date" placeholder="Date From" class="dp_input twelve columns">
            <input type="date" placeholder="Date To" class="dp_input twelve columns">
		</div>
		<div class="twelve columns">
			<input type="submit" value="Search" class="button">
		</div>        
    </div>
</div>
<!--==================== /Module: Publications: Publication Quick Search =============================-->
