<!--==================== /Module: Publications: Related Publications =============================-->
<div class="module-pb-related core-related callout collapse-for-mobile">
	<div class="reg-callout grid">
		<h3>Related Publications</h3>
		<ul class="core-list">
			<li class="listing-item">
				<h5><a href="/publications/2012/11/test-publication/">Test Publication</a></h5>
				<div class="date">November 22, 2012</div>
				<div class="teaser">
					<p>Test Publication Content</p>
				</div>
			</li>
		</ul>
	</div>
</div>
<!--==================== /Module: Publications: Related Publications =============================-->
