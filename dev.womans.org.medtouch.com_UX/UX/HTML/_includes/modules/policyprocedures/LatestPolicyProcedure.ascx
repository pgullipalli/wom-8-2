﻿<!--==================== Module: Policy Procedure: Latest Policy Procedure =============================-->
<div class="module-pp-latest callout">
    <div class="reg-callout">
        <h4>
            <span>Latest Policies</span></h4>
        <div class="listing">
            <div class="listing-item">
                <h5>
                    <a href="/en/policy-procedure/policy-folder/policy-test/penni-test-policy-1/">
                        Penni Test Policy 1</a></h5>
                <div class="listing-item-more-link">
                    <a href="/en/policy-procedure/policy-folder/policy-test/penni-test-policy-1/">
                        Read More</a>
                </div>
            </div>
            <div class="listing-item">
                <h5>
                    <a href="/en/policy-procedure/policy-folder/policy-test/policy-test-b1/">
                        Policy Test B1</a></h5>
                <div class="listing-item-teaser">
                    <p>
                        Version 2</p>
                </div>
                <div class="listing-item-more-link">
                    <a href="/en/policy-procedure/policy-folder/policy-test/policy-test-b1/">
                        Read More</a>
                </div>
            </div>
            <div class="listing-item">
                <h5>
                    <a href="/en/policy-procedure/policy-folder/policy-test/policy-test-a1/">
                        Policy Test A1 Version 3 test</a></h5>
                <div class="listing-item-more-link">
                    <a href="/en/policy-procedure/policy-folder/policy-test/policy-test-a1/">
                        Read More</a>
                </div>
            </div>
        </div>
        <div class="module-pp-view-all">
            <a href="/en/policy-procedure/policyprocedure-listing-page/">
                View All</a>
        </div>
    </div>
</div>
<!--==================== /Module: Policy Procedure: Latest Policy Procedure =============================-->
