﻿<!--==================== Module: Policy Procedure: Related Policy Procedure =============================-->
<div class="module-pp-related core-related callout collapse-for-mobile">
    <div class="reg-callout">
        <h3>Related Policies</h3>
        <ul class="module-pp-related-list core-list">
            <li class="core-li">
                <h5><a href="/en/policy-procedure/policy-folder/policy-test/policy-test-b1/">Policy Test B1</a></h5>
                <div class="teaser">
                    <p>Version 2</p>
                </div>
                <div class="more-link">
                    <a href="/en/policy-procedure/policy-folder/policy-test/policy-test-b1/">Read More</a>
                </div>
            </li>
            <li class="core-li">
                <h5><a href="/en/policy-procedure/policy-folder/policy-surat-test-folder/surat-test-policy-a-by-ce/">Surat Test Policy A by CE</a></h5>
                <div class="date">March 13, 2013</div>
                <div class="teaser">
                    <p>Test Version 2</p>
                </div>
                <div class="listing-item-more-link">
                    <a href="/en/policy-procedure/policy-folder/policy-surat-test-folder/surat-test-policy-a-by-ce/">Read More</a>
                </div>
            </li>
            <li class="core-li">
                <h5><a href="/en/policy-procedure/policy-folder/policy-test/policy-test-c1/">Policy Test C1</a></h5>
                <div class="date">February 12, 2013</div>
                <div class="teaser">
                    <p>v2</p>
                </div>
                <div class="more-link">
                    <a href="/en/policy-procedure/policy-folder/policy-test/policy-test-c1/">Read More</a>
                </div>
            </li>
        </ul>
    </div>
</div>
<!--==================== /Module: Policy Procedure: Related Policy Procedure =============================-->
