<!--==================== Module: Patient Testimonials: Related Testimonials =============================-->
<div class="module-pt-related callout">
    <div class="reg-callout">
        <h4>
            <span>Related Patient Testimonials</span></h4>
        <div class="listing">
            <div class="listing-item">
                <h5>
                    <a href="/patient-testimonials/sample-testimonials/sample-testimonial/">
                        Sample Testimonial</a></h5>
                <div class="listing-item-teaser">
                    <p>
                        This is the body copy of my sample testimonial</p>
                </div>
                <div class="listing-item-more-link">
                    <a href="/patient-testimonials/sample-testimonials/sample-testimonial/">
                        Read More</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--==================== /Module: Patient Testimonials: Related Testimonials =============================-->
