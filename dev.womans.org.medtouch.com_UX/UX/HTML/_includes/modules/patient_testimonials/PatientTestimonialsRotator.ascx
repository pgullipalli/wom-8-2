<!--==================== Module: Patient Testimonials: Rotator =============================-->
<div class="module-pt-rotator callout">
    <div class="reg-callout">
        <h4><span>Patient Testimonials Rotator</span></h4>
    
        <div class="slides">
		    <div class="slides_container">
                <div class="feature_content">
			        <h5><a href="#">Sample Testimonial</a></h5>
                    <div class="listing-item-teaser">
				        <p>This is the body copy of my sample testimonial</p>
                    </div>
                    <div class="listing-item-more-link">
				        <a href="#">Read More</a>
                    </div>
                </div> 
                <div class="feature_content">
			        <h5><a href="#">Sample Testimonial 2</a></h5>
                    <div class="listing-item-teaser">
				        <p>This is the body copy of my sample testimonial</p>
                    </div>
                    <div class="listing-item-more-link">
				        <a href="#">Read More</a>
                    </div>
                </div> 
                <div class="feature_content">
			        <h5><a href="#">Sample Testimonial 3</a></h5>
                    <div class="listing-item-teaser">
				        <p>This is the body copy of my sample testimonial</p>
                    </div>
                    <div class="listing-item-more-link">
				        <a href="#">Read More</a>
                    </div>
                </div>                                 
                 
            </div>    
	    </div>
    </div>
</div>
<!--==================== /Module: Patient Testimonials: Rotator =============================-->
