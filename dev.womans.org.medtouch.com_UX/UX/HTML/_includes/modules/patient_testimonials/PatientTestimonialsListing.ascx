<!--==================== Module: Patient Testimonials: Listing =============================-->
<div class="module-pt-results">
	
    
    <div class="module-pg-wrapper">
		
    
    <div class="module-pg-info">
			
        Viewing Page 1 of 1 | Showing Results 1 of 1
    
		</div>
    <div class="clear">
    </div>

	</div>

    <div class="listing">
		<div class="listing-item">
        
                <h4>
                    <a href="/patient-testimonials/sample-testimonials/sample-testimonial/">Sample Testimonial</a></h4>
                
                <div class="listing-item-teaser">
			
                    <p>This is the body copy of my sample testimonial</p>
                
		</div>
                <div class="listing-item-more-link">
			
                    <a href="/patient-testimonials/sample-testimonials/sample-testimonial/">Read More</a>
                
		</div>
          </div>  
    
	</div>
    <div class="module-pg-wrapper">
		
     <div class="module-pg-info">
			
        Viewing Page 1 of 1 | Showing Results 1 of 1
    
		</div>
    
    <div class="clear">
    </div>

	</div>


</div>
<!--==================== /Module: Patient Testimonials: Listing =============================-->
