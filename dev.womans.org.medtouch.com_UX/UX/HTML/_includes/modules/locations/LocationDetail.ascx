<!--==================== Module: Locations: Location Detail =============================-->
<div class="module-lc-profile core-profile grid">
    <div class="twelve columns return">
        <a href="#">Return to Search</a>
    </div>
    <div class="module-lc-profile-top twelve columns">
        <h1>MedTouch Medical Center Cambridge</h1>
    </div>
    <div class="twelve columns grid">
        <div class="module-lc-image">
            <img src="http://placehold.it/300x300" class="float-right" alt="cambridge">
        </div>
        
        <div>
            <h4>Office Information</h4>
        </div>
        <div class="department">Department of Medicine, General Surgery</div>
        <div class="address">84 Sherman St<br/>Cambridge, MA 02140</div>
        <div class="phones">
            <div>Office Phone: 617-621-8670</div>
            <div>Alternate Phone: 617-111-1111</div>
            <div>Fax: 888-770-5082</div>
            <div>Appointment Phone: 800-800-8888</div>
        </div>
        <div class="new-patients">Accepting New Patients</div>
        <div class="links">
            <a href="http://maps.google.com/?q=84+Sherman+St%2c+Cambridge+%2c+MA+02140" target="_blank">Maps & Directions</a>
            <span> | </span>
            <a target="_blank" href="http://www.medtouch.com">Website</a>
        </div>
        <div class="email-link">
            <a href="mailto:solutions@medtouch.com">Email</a>
        </div>
        <div class="module-lc-hours">
            <h4>Office Hours</h4>
            <div>
                <div>Monday 9-5</div>
                <div>Tuesday 8-4</div>
                <div>Wedneday - closed</div>
            </div>
        </div>
        <div class="twelve columns">
            <p>Phasellus vulputate tempor dictum. Mauris odio sapien, blandit sed commodo mattis, rutrum eu justo. Suspendisse potenti. Sed fringilla vestibulum mauris sit amet fringilla. Aliquam erat volutpat. Sed vulputate lectus vel neque tincidunt consequat. Maecenas eros risus, placerat sed sollicitudin non, viverra non nibh. Nulla eu diam placerat ligula malesuada aliquam bibendum in sem. Sed ac congue est. Fusce pellentesque aliquet sapien, ut gravida lorem fringilla ut. Proin dignissim pharetra lobortis. In hac habitasse platea dictumst. Cras ut tempus ante. </p>
            <p>Morbi accumsan eros eget leo consectetur vel aliquet metus sollicitudin. Phasellus sem nibh, aliquam ut iaculis a, egestas id lacus. Suspendisse porta dui et neque pharetra vestibulum. Vivamus ac nisi ac urna egestas luctus. Aliquam luctus, sem in eleifend hendrerit, ante sem ultrices massa, ut luctus lacus velit ac sapien. Curabitur nulla leo, fermentum sit amet vehicula ornare, condimentum id erat. In hac habitasse platea dictumst. Vivamus vel dolor vel est aliquam vestibulum. Phasellus ut tellus elit. Mauris dui est, scelerisque sit amet dignissim vel, dictum non ante. Curabitur bibendum, tellus non viverra aliquet, justo nisi rutrum turpis, vel scelerisque nunc diam gravida nisi. Quisque eleifend mi malesuada turpis aliquam eget egestas leo ultrices. Suspendisse lacinia, massa in condimentum tristique, sem nunc fringilla nisl, sit amet aliquet lacus quam quis sem.</p>
        </div>
    </div>
    <div class="module-pd-tabs responsive-tabs twelve columns">
        <h2>Services</h2>
        <div>
            <div>
                <ul>
                    <li><a>Service 1</a></li>
                    <li><a>Service 2 With Longer Name</a></li>
                    <li><a>Service 3 With Name that Wraps because of space</a></li>	
                </ul>
            </div>
        </div>
        <h2>Media</h2>	
        <div>
            <div>	
                <iframe width="420" height="315" src="http://www.youtube.com/embed/ERJPv5RteSQ" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
<!--==================== /Module: Locations: Location Detail =============================-->
