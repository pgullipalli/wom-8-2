<!--==================== Module: Locations: Location Listing =============================-->
<div class="module-lc-results core-results twelve columns grid">
    <div class="module-pg-wrapper twelve columns grid">
        <div class="module-pg-info">
            Viewing Page 1 of 1 | Showing Results 1 - 3 of 3
        </div>
    </div>
    <div class="listing twelve columns grid">
        <div class="listing-item">
            <div class="three columns">
                <div class="module-lc-thumbnail core-thumbnail">
                    <a href="/location-directory/locations/cambridge/">
                        <img src="http://placehold.it/200x150" alt="cambridge" width="200" height="150">
                    </a>
                </div>
                <div class="listing-item-more-link">
                    <a class="button read-more" href="/location-directory/locations/cedar-rapids/">Read More</a>
                </div>
            </div>
            <div class="nine columns">
                <div class="module-lc-info twelve columns">
                    <h4><a href="/location-directory/locations/cedar-rapids/">MedTouch Medical Center Cedar Rapids</a></h4>
                    <div class="address">1005 Longfellow Drive<br />Hiawatha, IA 52233</div>
                    <div class="phones">
                        <div>Office Phone: 319-512-7475</div>
                        <div>Fax: 319-512-7475</div>
                    </div>
                    <div class="links">
                        <a href="http://maps.google.com/?q=84+Sherman+St%2c+Cambridge+%2c+MA+02140" target="_blank">Maps & Directions</a>
                    </div>
                </div>
                <div class="module-lc-services twelve columns"></div>
            </div>
        </div>
        <div class="listing-item">
             <div class="three columns">
                <div class="module-lc-thumbnail core-thumbnail">
                    <a href="/location-directory/locations/cambridge/">
                        <img src="http://placehold.it/200x150" alt="cambridge" width="200" height="150">
                    </a>
                </div>
                <div class="listing-item-more-link">
                    <a class="button read-more" href="/location-directory/locations/houston/">Read More</a>
                </div>
            </div>
            <div class="nine columns">
                <div class="module-lc-info twelve columns">
                    <h4><a href="/location-directory/locations/houston/">MedTouch Medical Center Houston</a></h4>
                    <div class="address">14511 Old Katy Road<br />Old Katy, TX 77079</div>
                    <div class="phones">
                        <div>Office Phone: 281-652-5622</div>
                        <div>Fax: 281-652-5622</div>
                    </div>
                    <div class="links">
                        <a href="http://maps.google.com/?q=84+Sherman+St%2c+Cambridge+%2c+MA+02140" target="_blank">Maps & Directions</a>
                        <span> | </span>
                        <a target="_blank" href="http://www.medtouch.com">Website</a>
                    </div>
                </div>
                <div class="module-lc-services twelve columns"></div>
            </div>
        </div>
        <div class="listing-item">
             <div class="three columns">
                <div class="module-lc-thumbnail">
                    <a href="/location-directory/locations/cambridge/">
                        <img src="http://placehold.it/200x150" alt="cambridge" width="200" height="150">
                    </a>
                </div>
                <div class="listing-item-more-link">
                    <a class="button read-more" href="/location-directory/locations/cambridge/">Read More</a>
                </div>
            </div>
            <div class="nine columns">
                <div class="module-lc-info twelve columns">
                    <h4><a href="/location-directory/locations/cambridge/">MedTouch Medical Center Cambridge</a></h4>
                    <div class="departments">Department of Medicine, General Surgery</div>
                    <div class="address">84 Sherman St<br />Cambridge, MA 02140</div>
                    <div class="phones">
                        <div>Office Phone: 617-621-8670</div>
                        <div>Alternate Phone: 617-111-1111</div>
                        <div>Fax: 617-621-8670</div>
                        <div>Appointment Phone: 800-800-8888</div>
                    </div>
                    <div class="links">
                        <a href="http://maps.google.com/?q=84+Sherman+St%2c+Cambridge+%2c+MA+02140" target="_blank">Maps &amp; Directions</a>
                        <span> | </span>
                        <a target="_blank" href="http://www.medtouch.com">Website</a>
                    </div>
                </div>
    			<div class="module-lc-services twelve columns">
                    <a href="#">View Services</a>
                </div>
            </div>
        </div>
    </div>
    <div class="module-pg-wrapper twelve columns grid">
        <div class="module-pg-info">
            Viewing Page 1 of 1 | Showing Results 1 - 3 of 3
        </div>
    </div>
</div>
<!--==================== /Module: Locations: Location Listing =============================-->
