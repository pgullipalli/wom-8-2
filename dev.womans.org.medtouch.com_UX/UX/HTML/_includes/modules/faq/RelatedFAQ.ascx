<!--==================== Module: FAQ: Related FAQs =============================-->
<div class="module-faq-related callout">
	
    <div class="reg-callout">
        
            <h4><span>Related FAQs</span></h4>
        
        <div class="listing">
		
            
                    <div class="listing-item">
			
                        <h5><a href="/faqs/question-2/">Question 2</a></h5>
                        
                        <div>
				
                            <a href="/faqs/question-2/">Read More</a>
                        
			</div>
                    
		</div>
                
                    <div class="listing-item">
			
                        <h5><a href="/faqs/question-1/">Question 1</a></h5>
                        <p>This is my sample answer</p>
                        <div>
				
                            <a href="/faqs/question-1/">Read More</a>
                        
			</div>
                    
		</div>
                
        
	</div>
        
    </div>

</div>

<!--==================== /Module: FAQ: Related FAQs =============================-->
