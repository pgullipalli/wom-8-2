<!--==================== Module: FAQ: FAQ Quick Search =============================-->
<div class="module-faq-quick-search core-quick-search core-search callout collapse-for-mobile">
    <div class="reg-callout grid">
        <h3>FAQ Search</h3>
		<div class="twelve columns">
            <label class="label">Keyword</label>
            <input type="text" placeholder="enter keyword(s)" class="textbox">
		</div>
		<div class="twelve columns">
            <label class="label">Filter by Category</label>
			<div class="selectbox">  
				<select class="selectboxdiv">
					<option value="">All Categories</option>
					<option value="cardiac-care">Cardiac Care</option>
					<option value="cancer">Cancer</option>
					<option value="maternity">Maternity</option>
				</select>
				<div class="out"></div>
			</div>			
		</div>
		<div class="twelve columns">
			<input type="submit" value="Search" class="button">
		</div>
	</div>
</div>
<!--==================== /Module: FAQ: FAQ Quick Search =============================-->
