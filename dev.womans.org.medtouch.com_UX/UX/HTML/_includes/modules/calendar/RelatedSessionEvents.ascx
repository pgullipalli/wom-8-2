<!--==================== Module: Calendar: Related Session Events =============================-->
<div class="module-ce-session-related callout">
    <div class="reg-callout">
			<div class="header"><h3> <span>Upcoming Events</span></h3></div>	
			
			<section class="inner">
			<div class="gray-bg">	
				<div class="listing">
					<div class="listing-item">
						<div class="module-thumbnail">
							<img src="/ux/html/_includes/images/filler-profile.jpg" width="50"
								alt="stockphoto14394161newbornbabygirl" />
						</div>
						<h5>
							<a href="/calendar/test-calendar/cesarean-birth-class/?esk=sample+session+2012+12+01">
								Cesarean Birth Class</a></h5>
						<div class="listing-item-teaser">
							<p><span class="module-date">08-18-13</span>For couples who are expecting to deliver by c-section. <a href="#" class="arrow">More</a></p>
						</div>
						<!--<div class="module-date">
							December 01, 2012 05:00 PM &ndash; 06:00 PM
						</div>-->
						<div class="listing-item-more-link">
							<a href="/calendar/test-calendar/cesarean-birth-class/?esk=sample+session+2012+12+01">
								Read More</a> <span>&nbsp;|&nbsp;</span>
							<a href="/calendar/event-registration/?esid=AB53DD3D056A4C9F9074D99CC35A71DB">
								Register</a>
						</div>
					</div>
					<div class="listing-item">
						<h5>
							<a href="/calendar/test-calendar/breastfeeding-class/?esk=test+session+2012+12+02">
								Breastfeeding Class</a></h5>
						<div class="listing-item-teaser">
							<p><span class="module-date">08-18-13</span>This class will teach you the basics of breastfeeding. Lorem ipsum dolor sit amet,
								consectetur adipiscing elit. Fusce fermentum posuere eros et. <a href="#" class="arrow">More</a></p>
						</div>
						<!--<div class="module-date">
							December 02, 2012 08:01 AM &ndash; 08:01 AM
						</div>-->
						<div class="listing-item-more-link">
							<a href="/calendar/test-calendar/breastfeeding-class/?esk=test+session+2012+12+02">
								Read More</a> <span>&nbsp;|&nbsp;</span>
							<a href="/calendar/event-registration/?esid=9893B9C3FC1F456FB28AF297FA457914">
								Register</a>
						</div>
					</div>
				</div>
				<div class="module-ce-view-all">
					<a href="/?specialties=dermatology%7cobstetrics" class="read-more read-arrow"> View All</a>
				</div>
		</div>  	
		</section> 				
    </div>
</div>
<!--==================== /Module: Calendar: Related Session Events =============================-->
