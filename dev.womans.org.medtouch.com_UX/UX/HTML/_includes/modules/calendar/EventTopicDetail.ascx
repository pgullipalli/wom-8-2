<!--==================== Module: Calendar: Event Topic Detail =============================-->
<div class="module-ce-topic-profile grid">
	<!-- TODO: NEED TO ADD RETURN LINK HERE -->
	<div class="return">
		<a href="#">� Return to Search</a>
	</div>
    <div>
        <h1>Breastfeeding Class</h1>
	</div>
	
    <div>
		
        <p>This class will teach you the basics of breastfeeding.</p>
		
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum posuere eros et aliquet. Fusce ligula enim, accumsan a elementum quis, tempus quis neque. Nunc sollicitudin luctus aliquet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquet eleifend ullamcorper. Sed sit amet orci id enim venenatis dignissim. Curabitur posuere mattis ligula quis consequat.</p>

		<p>Phasellus aliquet faucibus dolor, id porttitor metus semper eget. Donec rhoncus ullamcorper eros, nec placerat justo posuere vel. Sed ut enim quam, sed sollicitudin tellus. Aenean id arcu non nisl dictum venenatis. Suspendisse at augue augue, sit amet commodo purus. Duis mollis, eros a porta elementum, augue ante rhoncus urna, et scelerisque risus mi eget risus. Pellentesque est ligula, mattis id pharetra in, porttitor nec dolor. Donec non iaculis mauris. Vestibulum dapibus lectus vel erat tincidunt elementum. Nulla lobortis vehicula lorem, a convallis mi pharetra eget. Suspendisse vel felis risus, sit amet tincidunt eros.</p>
	</div>
	
    <div class="module-ce-contact">
        <h3>Contact</h3>
        <div><a href="mailto:solutions@medtouch.com">Email</a></div>
	</div>
    
    
    <div class="module-ce-offerdates listing">

        <h3>Dates Offered:</h3>
    
    
            <div class="module-ce-offerdates-item highlight listing-item">
                <div class="module-date">
                    <span class="date">December 02, 2012 Sunday</span>
                    08:01 AM
                    &ndash;
                    08:01 AM
                </div>
                
                <div class="cal-location-name">
					<div>Cedar Rapids Office</div>
					<div>
					<a href="http://maps.google.com/?q=" target="_blank">Maps & Direction</a>  
					</div>
				</div>
                <div class="event-register">
                    <a class="button" href="/calendar/event-registration/?esid=9893B9C3FC1F456FB28AF297FA457914">Register</a>
				</div>
			</div>
        
            <div class="module-ce-offerdates-item highlight listing-item">
                <div class="module-date">
                    <span class="date">December 02, 2012 Sunday</span>
                    08:01 AM
                    &ndash;
                    08:01 AM
                </div>
                
                <div class="cal-location-name">
					<div>Cedar Rapids Office</div>
					<div>
					<a href="http://maps.google.com/?q=" target="_blank">Maps & Direction</a>  
					</div>
				</div>
                <div class="event-register">
                    <a class="button" href="/calendar/event-registration/?esid=9893B9C3FC1F456FB28AF297FA457914">Register</a>
				</div>
			</div>
        
            <div class="module-ce-offerdates-item highlight listing-item">
                <div class="module-date">
                    <span class="date">December 02, 2012 Sunday</span>
                    08:01 AM
                    &ndash;
                    08:01 AM
                </div>
                
                <div class="cal-location-name">
					<div>Cedar Rapids Office</div>
					<div>
					<a href="http://maps.google.com/?q=" target="_blank">Maps & Direction</a>  
					</div>
				</div>
                <div class="event-register">
                    <a class="button" href="/calendar/event-registration/?esid=9893B9C3FC1F456FB28AF297FA457914">Register</a>
				</div>
			</div>
        
            <div class="module-ce-offerdates-item highlight listing-item">
                <div class="module-date">
                    <span class="date">December 02, 2012 Sunday</span>
                    08:01 AM
                    &ndash;
                    08:01 AM
                </div>
                
                <div class="cal-location-name">
					<div>Cedar Rapids Office</div>
					<div>
					<a href="http://maps.google.com/?q=" target="_blank">Maps & Direction</a>  
					</div>
				</div>
                <div class="event-register">
                    <a class="button" href="/calendar/event-registration/?esid=9893B9C3FC1F456FB28AF297FA457914">Register</a>
				</div>
			</div>
        
            <div class="module-ce-offerdates-item highlight listing-item">
                <div class="module-date">
                    <span class="date">December 02, 2012 Sunday</span>
                    08:01 AM
                    &ndash;
                    08:01 AM
                </div>
                
                <div class="cal-location-name">
					<div>Cedar Rapids Office</div>
					<div>
					<a href="http://maps.google.com/?q=" target="_blank">Maps & Direction</a>  
					</div>
				</div>
                <div class="event-register">
                    <a class="button" href="/calendar/event-registration/?esid=9893B9C3FC1F456FB28AF297FA457914">Register</a>
				</div>
			</div>
        
            <div class="module-ce-offerdates-item highlight listing-item">
                <div class="module-date">
                    <span class="date">December 02, 2012 Sunday</span>
                    08:01 AM
                    &ndash;
                    08:01 AM
                </div>
                
                <div class="cal-location-name">
					<div>Cedar Rapids Office</div>
					<div>
					<a href="http://maps.google.com/?q=" target="_blank">Maps & Direction</a>  
					</div>
				</div>
                <div class="event-register">
                    <a class="button" href="/calendar/event-registration/?esid=9893B9C3FC1F456FB28AF297FA457914">Register</a>
				</div>
			</div>
        
            <div class="module-ce-offerdates-item highlight listing-item">
                <div class="module-date">
                    <span class="date">December 02, 2012 Sunday</span>
                    08:01 AM
                    &ndash;
                    08:01 AM
                </div>
                
                <div class="cal-location-name">
					<div>Cedar Rapids Office</div>
					<div>
					<a href="http://maps.google.com/?q=" target="_blank">Maps & Direction</a>  
					</div>
				</div>
                <div class="event-register">
                    <a class="button" href="/calendar/event-registration/?esid=9893B9C3FC1F456FB28AF297FA457914">Register</a>
				</div>
			</div>
        
            <div class="module-ce-offerdates-item highlight listing-item">
                <div class="module-date">
                    <span class="date">December 02, 2012 Sunday</span>
                    08:01 AM
                    &ndash;
                    08:01 AM
                </div>
                
                <div class="cal-location-name">
					<div>Cedar Rapids Office</div>
					<div>
					<a href="http://maps.google.com/?q=" target="_blank">Maps & Direction</a>  
					</div>
				</div>
                <div class="event-register">
                    <a class="button" href="/calendar/event-registration/?esid=9893B9C3FC1F456FB28AF297FA457914">Register</a>
				</div>
			</div>
        
            <div class="module-ce-offerdates-item highlight listing-item">
                <div class="module-date">
                    <span class="date">December 02, 2012 Sunday</span>
                    08:01 AM
                    &ndash;
                    08:01 AM
                </div>
                
                <div class="cal-location-name">
					<div>Cedar Rapids Office</div>
					<div>
					<a href="http://maps.google.com/?q=" target="_blank">Maps & Direction</a>  
					</div>
				</div>
                <div class="event-register">
                    <a class="button" href="/calendar/event-registration/?esid=9893B9C3FC1F456FB28AF297FA457914">Register</a>
				</div>
			</div>
        
            <div class="module-ce-offerdates-item highlight listing-item">
                <div class="module-date">
                    <span class="date">December 02, 2012 Sunday</span>
                    08:01 AM
                    &ndash;
                    08:01 AM
                </div>
                
                <div class="cal-location-name">
					<div>Cedar Rapids Office</div>
					<div>
					<a href="http://maps.google.com/?q=" target="_blank">Maps & Direction</a>  
					</div>
				</div>
                <div class="event-register">
                    <a class="button" href="/calendar/event-registration/?esid=9893B9C3FC1F456FB28AF297FA457914">Register</a>
				</div>
			</div>
        
            <div class="module-ce-offerdates-item highlight listing-item">
                <div class="module-date">
                    <span class="date">December 02, 2012 Sunday</span>
                    08:01 AM
                    &ndash;
                    08:01 AM
                </div>
                
                <div class="cal-location-name">
					<div>Cedar Rapids Office</div>
					<div>
					<a href="http://maps.google.com/?q=" target="_blank">Maps & Direction</a>  
					</div>
				</div>
                <div class="event-register">
                    <a class="button" href="/calendar/event-registration/?esid=9893B9C3FC1F456FB28AF297FA457914">Register</a>
				</div>
			</div>
        
                        <div class="module-ce-offerdates-item highlight listing-item">
                <div class="module-date">
                    <span class="date">December 02, 2012 Sunday</span>
                    08:01 AM
                    &ndash;
                    08:01 AM
                </div>
                
                <div class="cal-location-name">
					<div>Cedar Rapids Office</div>
					<div>
					<a href="http://maps.google.com/?q=" target="_blank">Maps & Direction</a>  
					</div>
				</div>
                <div class="event-register">
                    <a class="button" href="/calendar/event-registration/?esid=9893B9C3FC1F456FB28AF297FA457914">Register</a>
				</div>
			</div>
	</div>


</div>
<!--==================== /Module: Calendar: Event Topic Detail =============================-->
