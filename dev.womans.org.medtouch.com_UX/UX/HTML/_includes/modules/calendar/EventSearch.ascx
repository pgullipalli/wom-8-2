<!--==================== Module: Calendar: Event Search =============================-->
<div class="module-ce-search core-search twelve columns grid">
    <div class="twelve columns">
        <label class="label">Keyword</label>
        <input type="text" placeholder="enter keyword(s)" class="textbox">
	</div>
	<div class="grid">
		<div class="six columns">
			<label class="label">All Categories</label>
			<div class="selectbox">  
				<select class="selectboxdiv">
					<option value="">All Categories</option>
					<option value="birthing-classes">Birthing Classes</option>
				</select>
				<div class="out"></div>
			</div>			
		</div>
		<div class="six columns">
			<label class="label">Date</label>
			<div class="grid">
				<input type="date" placeholder="Date From" class="dp_input six columns">
				<input type="date" placeholder="Date To" class="dp_input six columns">
			</div>
		</div>
	</div>
	<div class="twelve columns search-option-submit">
		<input type="submit" value="Search" class="button">
	</div>
</div>
<!--==================== /Module: Calendar: Event Search =============================-->
