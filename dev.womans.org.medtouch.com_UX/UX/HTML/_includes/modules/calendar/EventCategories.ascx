<!--==================== Module: Calendar: Event Categories =============================-->
<div class="module-ce-categories callout collapse-for-mobile">
    <div class="reg-callout grid">
        <h3>Browse by Category</h3>	
		<ul class="module-ce-filters-list">
			<li><a href="/calendar/event-search-results/?category=Family+Birthing+Classes">Birthing Classes</a></li>
			<li><a href="/calendar/event-search-results/?category=CPR+First+Aid+Classes">CPR and First Aid Classes</a></li>
		</ul>
    </div>
</div>
<!--==================== /Module: Calendar: Event Categories =============================-->
