<!--==================== Module: Calendar: Related Topic Events =============================-->
<div class="module-ce-topic-related callout">
    <div class="reg-callout">
        <div class="header"><h3> <span>Related Classes &amp; Events</span></h3></div>	

		<section class="inner">
		<div class="gray-bg">	
			<div class="listing">
				<div class="listing-item">
					<div class="module-thumbnail">
						<img src="/ux/html/_includes/images/filler-profile.jpg" width="50"
							alt="stockphoto17676717mother" />
					</div>
					<h5>
						<a href="/calendar/test-calendar/breastfeeding-class/">
							Breastfeeding Class</a></h5>
					<div class="listing-item-teaser">
						<p>
							This class will teach you the basics of breastfeeding. Lorem ipsum dolor sit amet,
							consectetur adipiscing elit. Fusce fermentum posuere eros et...</p>
					</div>
					<div class="listing-item-more-link">
						<a href="/calendar/test-calendar/breastfeeding-class/">
							Read More</a>
					</div>
				</div>
				<div class="listing-item">
					<h5>
						<a href="/calendar/test-calendar/cesarean-birth-class/">
							Cesarean Birth Class</a></h5>
					<div class="listing-item-teaser">
						<p>
							For couples who are expecting to deliver by c-section.</p>
					</div>
					<div class="listing-item-more-link">
						<a href="/calendar/test-calendar/cesarean-birth-class/">
							Read More</a>
					</div>
				</div>
			</div>
		</div>  	
		</section> 				
    </div>
</div>
<!--==================== Module: Calendar: Related Topic Events =============================-->
