﻿<!--==================== Module: Calendar: Upcoming Session Events =============================-->
<div class="module-ce-session-upcoming grid">
        <h4>Upcoming Classes &amp; Events</h4>
    
    <div class="listing twelve columns">
		 <div class="listing-item">
			<h5><a href="#">Cesarean Birth Class</a></h5>
                 <div class="listing-item-teaser twelve columns">
						<p>For couples who are expecting to deliver by c-section.</p>
                 </div>
                 <div class="module-date twelve columns">
                        December 01, 2012
                        05:00 PM
                        &ndash;
                        06:00 PM
				</div>
				<div class="listing-item-more-link twelve columns">
					 <a href="#">Read More</a>
					 <span>|</span>
					 <a href="#">Register</a>    
				</div>    
		</div>
            
        <div class="listing-item">
			<div class="module-thumbnail two columns">
				<img src="http://placehold.it/100x100" alt="photo" />				
			</div>
            
			<div class="ten columns">
				<h5><a href="#">Breastfeeding Class</a></h5>
				<div class="listing-item-teaser">
					<p>This class will teach you the basics of breastfeeding. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum posuere eros et...</p>     
				</div>
				<div class="module-date">
					December 02, 2012
					08:01 AM
					&ndash;
					08:01 AM  
				</div>
			   <div class="listing-item-more-link">
					<a href="#">Read More</a>
					<span>|</span>
					<a href="#">Register</a>   
				</div>
			</div>  
		</div>
            
         <div class="listing-item">
			<div class="module-thumbnail two columns">
				<img src="http://placehold.it/100x100" alt="photo" width="100" />				
			</div>
			<div class="ten columns">
				<h5><a href="#">CPR for infants</a></h5>
                <div class="listing-item-teaser">
					<p>Learn to provide CPR for infants. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi est nunc, lobortis at ornare ut, gravida vitae...</p>
                </div>
                <div class="module-date">
					December 09, 2012
					08:15 AM
					&ndash;
					08:15 AM
                </div>
                <div class="listing-item-more-link">
                        <a href="#">Read More</a>    
				</div>
			</div>   
		</div>
            
         <div class="listing-item">
            <div class="module-thumbnail two columns">
				<img src="http://placehold.it/100x100" alt="photo" width="100" />				
			</div>
			<div class="ten columns">
               <h5><a href="#">Cesarean Birth Class</a></h5>
                <div class="listing-item-teaser">
				<p>For couples who are expecting to deliver by c-section.</p>
				</div>
               <div class="module-date">
					January 01, 2013
					05:00 PM
					&ndash;
					06:00 PM
				</div>
                 <div class="listing-item-more-link">
                        <a href="#">Read More</a>
                        <span>|</span>
                        <a href="#">Register</a>
                </div>
			</div>                
		</div>
	</div>
    <div class="module-view-all  twelve columns">
        <a href="/">View All</a>
	</div>

</div>
<!--==================== /Module: Calendar: Upcoming Session Events =============================-->
