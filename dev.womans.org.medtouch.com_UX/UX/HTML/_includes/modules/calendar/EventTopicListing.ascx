<!--==================== Module: Calendar: Event Topic Listing =============================-->
<div class="module-ce-topic-results grid">
	<div class="module-pg-wrapper">
		<div class="module-pg-info">
		Viewing Page 1 of 1 | Showing Results 1 - 3 of 3
		</div>
	</div>

    <div class="listing twelve columns">
		<div class="listing-item">
            <h4><a href="#">Breastfeeding Class</a></h4>
			<div class="module-thumbnail two columns">
				<a href="#"><img src="http://placehold.it/100x100" alt="photo_of_event_award_gala" /></a>
			</div>	
			<div class="teaser-copy ten columns">
				<div class="listing-item-teaser">
					<p>This class will teach you the basics of breastfeeding. 
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum posuere eros et...</p>
				</div>
				<div class="listing-item-more-link">
					<a href="#">Read More</a>
				</div>
			</div> 
		</div>
            
        <div class="listing-item">
			<h4><a href="#">Cesarean Birth Class</a></h4>
            <div class="module-thumbnail two columns">
				<a href="#"><img src="http://placehold.it/100x100" alt="photo_of_event_award_gala" /></a>
			</div>	
            <div class="teaser-copy ten columns">
                <div class="listing-item-teaser">
				<p>For couples who are expecting to deliver by c-section.</p>
                </div>
                <div class="listing-item-more-link">
				<a href="#">Read More</a>
                </div>
            </div>   
		</div>
	</div>
    <div class="module-pg-wrapper">
	</div>
</div>
<!--==================== /Module: Calendar: Event Topic Listing =============================-->
