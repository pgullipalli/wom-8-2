<!--==================== Module: Calendar: Event Quick Search =============================-->
<div class="module-ce-search core-search callout collapse-for-mobile">
	<div class="reg-callout grid">
		<h3>Event Search</h3>	
		<div class="twelve columns">
			<label class="label">Keyword</label>
			<input type="text" placeholder="enter keyword(s)" class="textbox">
		</div>
		<div class="twelve columns">
			<label class="label">All Categories</label>
			<div class="selectbox">  
				<select class="selectboxdiv">
					<option value="">All Categories</option>
					<option value="birthing-classes">Birthing Classes</option>
				</select>
				<div class="out"></div>
			</div>			
		</div>
		<div class="twelve columns">
			<label class="label">Date</label>
			<input type="date" placeholder="Date From" class="dp_input twelve columns">
			<input type="date" placeholder="Date To" class="dp_input twelve columns">
		</div>
		<div class="twelve columns">
			<input type="submit" value="Search" class="button">
		</div>
	</div>
</div>
<!--==================== /Module: Calendar: Event Quick Search =============================-->
