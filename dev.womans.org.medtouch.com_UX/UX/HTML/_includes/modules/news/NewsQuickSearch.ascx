<!--==================== Module: News: News Quick Search =============================-->
<div class="module-nw-quick-search core-quick-search core-search callout collapse-for-mobile">
    <div class="reg-callout grid">
	    <h3>News Search</h3>
		<div class="twelve columns">
			<label class="label">Keyword</label>
			<input type="text" placeholder="enter keyword(s)" class="textbox">
		</div>
		<div class="twelve columns">
			<label class="label">Filter by Category</label>
			<div class="selectbox">
				<select class="selectboxdiv">
					<option value="">All Categories</option>
					<option value="hospital news">Hospital News</option>
					<option value="special events">Special Events</option>
				</select>
				<div class="out">Filter by Category</div>
			</div>
		</div>
		<div class="twelve columns grid">
			<label class="label">Date</label>
			<input type="date" placeholder="Date From" class="dp_input twelve columns">
			<input type="date" placeholder="Date To" class="dp_input twelve columns">    
		</div>
		<div class="twelve columns">
			<input type="submit" value="Search" class="button">
		</div>
    </div>
</div>
<!--==================== /Module: News: News Quick Search =============================-->
