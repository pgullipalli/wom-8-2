<!--==================== Module: News: Latest News =============================-->
<div class="module-nw-latest grid">
	
    
        <h4>
            Latest News</h4>
    
    <div class="listing twelve columns">
		
                <div class="listing-item">
                    <div class="module-thumbnail two columns">
				
                        <img src="http://placehold.it/100x100" alt="photo_of_event_award_gala" width="100" height="63" />
                    
			</div>
                    <div class="teaser-copy ten columns">
                        <h5><a href="/news/2012/10/gala-event/">Gala Event</a></h5>
                        <div class="module-date">
				
                            October 27, 2012
                        
			</div>
                        <div class="listing-item-teaser">
				
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel lacus odio. Aliquam felis...</p>
                        
			</div>
                        <div class="listing-item-more-link">
				
                            <a href="/news/2012/10/gala-event/">Read More</a>
                        
			</div>
                    </div>
                
		</div>
            
                <div class="listing-item">
			
                    <div class="module-thumbnail  two columns">
				
                        <img src="http://placehold.it/100x100" alt="logo" width="100" height="27" />
                    
			</div>
                    <div class="teaser-copy ten columns">
                        <h5><a href="/news/2012/07/25-million-pledged-for-new-heart-center-at-the-medtouch-medical-center/">$25 Million Pledged for New Heart Center at the MedTouch Medical Center</a></h5>
                        <div class="module-date">
				
                            July 20, 2012
                        
			</div>
                        <div class="listing-item-teaser">
				
                            <p>HOUSTON, Tex.&ndash; Cardiologists and surgeons who will work in a heart care facility just now...</p>
                        
			</div>
                        <div class="listing-item-more-link">
				
                            <a href="/news/2012/07/25-million-pledged-for-new-heart-center-at-the-medtouch-medical-center/">Read More</a>
                        
			</div>
                    </div>
                
		</div>
            
    
	</div>
    

</div>
<!--==================== /Module: News: Latest News =============================-->
