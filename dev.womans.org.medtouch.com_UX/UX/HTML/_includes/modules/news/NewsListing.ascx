<!--==================== Module: News: News Listing =============================-->
<div class="module-nw-results">
	
    
    <div class="module-pg-wrapper grid">
		
    
    <div class="module-pg-info">
			
        Viewing Page 1 of 1 | Showing Results 1 - 2 of 2
    
		</div>

	</div>
    <div class="listing">
		
        
                <div class="listing-item">
			
                    <h4><a href="/news/2012/10/gala-event/">Gala Event</a></h4>
                    <div class="module-thumbnail two columns">
                        <img src="http://placehold.it/100x100" alt="photo_of_event_award_gala" width="100" />
                    </div>
                    <div class="teaser-copy ten columns">
                        <div class="module-date">October 27, 2012</div>
                        <div class="listing-item-teaser">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel lacus odio. Aliquam felis...</p>
                        </div>
                        <div class="listing-item-more-link">
                            <a href="/news/2012/10/gala-event/">Read More</a>
						</div>
                    </div>
                
		</div>
            
                <div class="listing-item">
			
                    <h4><a href="/news/2012/07/25-million-pledged-for-new-heart-center-at-the-medtouch-medical-center/">$25 Million Pledged for New Heart Center at the MedTouch Medical Center</a></h4>
                    <div class="module-thumbnail two columns">
				
                        <img src="http://placehold.it/100x100" alt="logo" width="100" />
                    
			</div>
                    <div class="teaser-copy ten columns">
                        <div class="module-date">
				
                            July 20, 2012
                        
			</div>
                        <div class="listing-item-teaser">
				
                            <p>HOUSTON, Tex.&ndash; Cardiologists and surgeons who will work in a heart care facility just now...</p>
                        
			</div>
                        <div class="listing-item-more-link">
				
                            <a href="/news/2012/07/25-million-pledged-for-new-heart-center-at-the-medtouch-medical-center/">Read More</a>
                        
			</div>
                    </div>
                
		</div>
            
    
	</div>
    <div class="module-pg-wrapper">
		
    <div class="module-pg-info">
			
        Viewing Page 1 of 1 | Showing Results 1 - 2 of 2
    
		</div>

	</div>

</div>
<!--==================== /Module: News: News Listing =============================-->
