<!--==================== Module: News: News Detail =============================-->
<div class="module-nw-detail article">
	<div class="return">
		<a href="#">� Return to Search</a>
	</div>
    
    <h1>$25 Million Pledged for New Heart Center at the MedTouch Medical Center</h1>
    
    <div class="date">  
        July 20, 2012
    </div>
    
    <div class="grid">
        <div class="nine columns">
            <p>HOUSTON, Tex.&ndash; Cardiologists and surgeons who will work in a heart care facility just now breaking ground in Houston&rsquo;s world renown medical center pledged $25 million for their new home last year.</p>
            <p>This 542,000-square-foot facility is expected to be completed in the Fall of 2013. Members of the cardiologists of Mid-America Cardiology Associates Inc. and the cardiothoracic surgeons of MidAmerica Thoracic &amp; Cardiovascular Surgeons Inc. have offered their blessings and have contributed significant donations to the soon-to-be state of the art MTMC Heart Center.&nbsp; The cardiologists and vascular surgeons of the MedTouch Medical Center have contributed many hours of planning and discussion with the MTMC board council for the project and are working collaboratively with the Gruene Architecture Company to design a sustainable facility that is ISO 100 certified.</p>
        </div>

        <div class="three columns">
            <img src="http://placehold.it/150x150" alt="logo" width="150" />
        </div>
    </div>
</div>
<!--==================== /Module: News: News Detail =============================-->
