<!--==================== Module: Blog: Blog Post =============================-->
<div id="main_0_contentpanel_1_pnlDetail" class="module-bg-detail article">
    <h1>
        Test Blog Post
    </h1>
    <div id="main_0_contentpanel_1_pnlDate" class="module-bg-date">
        January 03, 2014 09:08:00 AM
    </div>
    <div id="main_0_contentpanel_1_pnlAuthors" class="module-bg-detail-authors">
        <b>By:</b> <a href="/blog/authors/test-author/">Test Author</a>
    </div>
    <div id="main_0_contentpanel_1_UpdatePanel1">
        <div class="module-bg-detail-like">
            <a id="main_0_contentpanel_1_lkbVoteLikeUnlike" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("main_0$contentpanel_1$lkbVoteLikeUnlike", "", true, "", "", false, true))'>
                Like (0)</a>
        </div>
    </div>
    <div id="main_0_contentpanel_1_pnlImage" class="module-bg-detail-image left">
        <img src="/~/media/Images/Modules/Blog/Posts/feature1.jpg?mw=300" alt="feature1"
            width="152" height="101">
    </div>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam purus velit, aliquet
    in sagittis vitae, imperdiet id elit. Vivamus viverra, lorem et pharetra porta,
    dui mi pellentesque purus, at tincidunt nulla nulla at risus. Aenean porttitor non
    turpis eu faucibus. Curabitur viverra ultricies adipiscing. Nunc varius convallis
    velit, eget sollicitudin dolor scelerisque a. Aliquam quis porttitor nunc. Nullam
    auctor arcu neque, sit amet scelerisque sem dictum vel.<br>
    <br>
    Suspendisse ac ante quam. Morbi elementum enim nisl, vel condimentum quam auctor
    ac. In hac habitasse platea dictumst. Etiam tempor lorem quis risus facilisis, ut
    blandit augue feugiat. Proin rutrum eget dolor nec malesuada. In condimentum pulvinar
    tortor, at faucibus tortor. Suspendisse potenti. Nulla mattis dapibus eros sed pulvinar.
    Aliquam sollicitudin pharetra lacus nec mollis.<br>
    <br>
    Phasellus tincidunt, arcu porttitor adipiscing tincidunt, nibh lectus malesuada
    turpis, et feugiat nunc eros et risus. Vestibulum mattis magna libero, id dapibus
    ante gravida nec. Quisque viverra tellus ut libero convallis, eget rhoncus odio
    tristique. Nullam feugiat elementum aliquam. Quisque viverra at nunc quis porttitor.
    Etiam semper egestas orci, eget porttitor dolor facilisis vel. Morbi sed tortor
    sit amet odio vulputate volutpat at vitae odio.<br>
    <br>
    Vivamus sit amet est dolor. Duis consectetur mauris vitae nisi ullamcorper cursus.
    Aenean id tempor augue, vitae facilisis mi. Proin auctor sodales sollicitudin. Sed
    iaculis eu dolor quis sagittis. Donec vehicula nisi id nibh blandit convallis. Sed
    quis interdum metus, vel sodales nibh. Etiam accumsan, dui eget scelerisque bibendum,
    lorem velit bibendum metus, in blandit elit nibh sodales ligula. Donec suscipit
    vulputate nibh sed accumsan. Aliquam a ipsum commodo, posuere enim eget, aliquet
    felis. Fusce nec nisi vel odio dictum fringilla ut sit amet tortor. Vestibulum ante
    ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br>
    <br>
    Nulla tristique laoreet pulvinar. Donec quis adipiscing sem. Cras tincidunt bibendum
    pellentesque. Pellentesque tristique vestibulum adipiscing. Vivamus pulvinar quam
    eu sapien ornare sagittis. Quisque quis vestibulum mauris. Nunc libero velit, elementum
    id tortor et, fringilla ultrices purus. In sed ultricies nisi, quis tincidunt justo.
    Donec mollis elementum est, id malesuada lectus pulvinar in. Praesent a dictum lacus,
    porta mollis risus.
    <div id="main_0_contentpanel_1_pnlCategories" class="module-bg-detail-categories">
        Categories: <a href="/blog/categories/test-category/">Test Category</a>
    </div>
    <div id="main_0_contentpanel_1_pnlTopics" class="module-bg-detail-topics">
        Topics: <a href="/blog/topics/test-topic/">Test Topic</a>
    </div>
</div>
<div id="main_0_contentpanel_2_pnlCommentListing" class="module-bg-comments-listing">
    <h3>
        <div id="Comments">
            Comments
        </div>
    </h3>
    <div id="main_0_contentpanel_2_UpdatePanel1">
        <div id="main_0_contentpanel_2_lvCommentListing_pnlResult_0" class="listing-item">
            <div id="main_0_contentpanel_2_lvCommentListing_pnlUser_0" class="module-bg-comment-byline">
                <span id="main_0_contentpanel_2_lvCommentListing_lbCommenter_0">Dan Persson - January
                    03, 2014 12:53:42 PM</span>
            </div>
            <div id="main_0_contentpanel_2_lvCommentListing_pnlComment_0" class="module-bg-comment-content">
                <span id="main_0_contentpanel_2_lvCommentListing_lbComment_0">This is my test comment</span>
            </div>
        </div>
    </div>
    <span id="main_0_contentpanel_2_timerComment" style="visibility: hidden; display: none;">
    </span>
</div>
<span id="main_0_contentpanel_2_lblMonitoredComment" class="errortext"></span>
<div id="main_0_contentpanel_3_pnlCommentForm" class="module-bg-comments-form">
    

</div>
<div class="module-bg-login">
<!--==================== RHS: Blogs: User Login =============================-->
<script type="text/javascript">

    function ToggleForgetPassword() {
        $('#' + 'main_0_contentpanel_3_ctl00_pnlForgetPassword').show();
        $('#' + 'main_0_contentpanel_3_ctl00_pnlForgetUsername').hide();
        return true;
    }
    function ToggleForgetUserName() {
        $('#' + 'main_0_contentpanel_3_ctl00_pnlForgetUsername').show();
        $('#' + 'main_0_contentpanel_3_ctl00_pnlForgetPassword').hide();
        return true;
    }
</script>
        <div class="pnlLogin">
        <h4>Login to Comment</h4>
        
        <div class="search-option">
            <label for="main_0_contentpanel_3_ctl00_blogLogin_UserName" id="main_0_contentpanel_3_ctl00_blogLogin_lblUserName" class="label">Username: </label>
            <input name="main_0$contentpanel_3$ctl00$blogLogin$UserName" type="text" id="main_0_contentpanel_3_ctl00_blogLogin_UserName" class="textbox">            
        </div>
        <div class="search-option">
              <label for="main_0_contentpanel_3_ctl00_blogLogin_Password" id="main_0_contentpanel_3_ctl00_blogLogin_lblPassword" class="label">Password:</label>
            <input name="main_0$contentpanel_3$ctl00$blogLogin$Password" type="password" id="main_0_contentpanel_3_ctl00_blogLogin_Password" class="textbox">

        </div>
        <div class="search-option-submit">
            <input type="submit" name="main_0$contentpanel_3$ctl00$blogLogin$Login" value="Log In" id="main_0_contentpanel_3_ctl00_blogLogin_Login" class="button">
            <input type="submit" name="main_0$contentpanel_3$ctl00$blogLogin$btnRegister" value="Create a Free Account" id="main_0_contentpanel_3_ctl00_blogLogin_btnRegister" class="button" style="">
        </div>

        </div>
        
        <div id="main_0_contentpanel_3_ctl00_Panel1" class="reset-panel">
		
            <a href="javascript: void(0);" id="lnkForgetPassword" onclick="return ToggleForgetPassword();">
                Forgot Password</a>&nbsp;&nbsp;
            <a href="javascript: void(0);" id="lnkForgetUserName" onclick="return ToggleForgetUserName();">
                Forgot Username</a>

	    </div>
<span class="errortext">
    <span id="main_0_contentpanel_3_ctl00_lblMessage" class="label"></span>
    <span id="main_0_contentpanel_3_ctl00_revEmail" style="display:none;">Please enter a valid email address</span>
</span>

<div id="main_0_contentpanel_3_ctl00_vsMessage" style="display:none;">

	</div>

<div id="main_0_contentpanel_3_ctl00_pnlForgetPassword" style="display: none;">
        <h4>Retrieve Password</h4>
        <div class="search-option">
            <label for="main_0$contentpanel_3$ctl00$txtEmailPassword" id="Label1" class="label">Email Address:</label>
            <input name="main_0$contentpanel_3$ctl00$txtEmailPassword" type="text" id="main_0$contentpanel_3$ctl00$txtEmailPassword" class="textbox">
        </div>		
        <span id="main_0_contentpanel_3_ctl00_rfvEmail" style="display:none;"></span>
        <div class="search-option-submit">
            <input type="submit" name="main_0$contentpanel_3$ctl00$btnSendPassword" value="Send Email" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;main_0$contentpanel_3$ctl00$btnSendPassword&quot;, &quot;&quot;, true, &quot;Forget&quot;, &quot;&quot;, false, false))" id="main_0_contentpanel_3_ctl00_btnSendPassword" class="button">

	    </div>
</div>

<div id="main_0_contentpanel_3_ctl00_pnlForgetUsername" style="display: none;">
        <h4>Retrieve Username</h4>
        <div class="search-option">
            <label for="main_0$contentpanel_3$ctl00$txtEmailPassword" id="Label3" class="label">Email Address:</label>
            <input name="main_0$contentpanel_3$ctl00$txtEmailPassword" type="text" id="Submit1" class="textbox">
        </div>		
        <span id="Span1" style="display:none;"></span>
        <div class="search-option-submit">
            <input type="submit" name="main_0$contentpanel_3$ctl00$btnSendPassword" value="Send Email" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;main_0$contentpanel_3$ctl00$btnSendPassword&quot;, &quot;&quot;, true, &quot;Forget&quot;, &quot;&quot;, false, false))" id="Submit4" class="button">

	    </div>
</div>  
    

<!--==================== /RHS: Blogs: User Login =============================-->    

</div>
<!--==================== /Module: Blog: Blog Post =============================-->
