<!--==================== Module: Physician Directory: Physician Search Again =============================-->
<div class="module-pd-search-again core-search-again collapse-for-mobile">
	<div class="reg-callout grid">
		<h3>Filter Your Results</h3>	
		<div class="module-alphabet-list-sm alpha-list columns">
			<ul class="module-alphabet-list">
				<li class="one columns"><a class="aspNetDisabled">A</a></li>
				<li class="one columns"><a class="aspNetDisabled">B</a></li>
				<li class="one columns"><a class="aspNetDisabled">C</a></li>
				<li class="one columns"><a class="aspNetDisabled">D</a></li>
				<li class="one columns"><a class="aspNetDisabled">E</a></li>
				<li class="one columns"><a href="/physician-directory/search-results/?letter=f">F</a></li>
				<li class="one columns"><a class="aspNetDisabled">G</a></li>
				<li class="one columns"><a class="aspNetDisabled">H</a></li>
				<li class="one columns"><a class="aspNetDisabled">I</a></li>
				<li class="one columns"><a class="aspNetDisabled">J</a></li>
				<li class="one columns"><a class="aspNetDisabled">K</a></li>
				<li class="one columns"><a class="aspNetDisabled">L</a></li>
				<li class="one columns"><a class="aspNetDisabled">M</a></li>
				<li class="one columns"><a class="aspNetDisabled">N</a></li>
				<li class="one columns"><a class="aspNetDisabled">O</a></li>
				<li class="one columns"><a class="aspNetDisabled">P</a></li>
				<li class="one columns"><a class="aspNetDisabled">Q</a></li>
				<li class="one columns"><a class="aspNetDisabled">R</a></li>
				<li class="one columns"><a class="aspNetDisabled">S</a></li>
				<li class="one columns"><a class="aspNetDisabled">T</a></li>
				<li class="one columns"><a class="aspNetDisabled">U</a></li>
				<li class="one columns"><a class="aspNetDisabled">V</a></li>
				<li class="one columns"><a class="aspNetDisabled">W</a></li>
				<li class="one columns"><a class="aspNetDisabled">X</a></li>
				<li class="one columns"><a class="aspNetDisabled">Y</a></li>
				<li class="one columns"><a class="aspNetDisabled">Z</a></li>
			</ul>
		</div>
		<div class="search-form">	
			<div class="twelve columns">
				<label class="label">First Name</label>
				<input type="text" class="textbox" placeholder="Enter a First Name" />
			</div>
			<div class="twelve columns">
				<label class="label">Last Name</label>
				<input type="text" class="textbox" placeholder="Enter a Last Name" />
			</div>
			<div class="twelve columns">
				<label class="label">Specialty</label>
				<div class="selectbox">  
					<select class="selectboxdiv">
						<option value="">Select a Specialty</option>
						<option value="dermatology">Dermatology</option>
						<option value="family general medicine">Family-General Medicine</option>
					</select>
					<div class="out"></div>
				</div>
			</div>
			<div class="twelve columns">
				<label class="label">Department</label>
				<div class="selectbox">  
					<select class="selectboxdiv">
						<option value="">Select a Department</option>
						<option value="department of medicine">Department of Medicine</option>
						<option value="emergency">Emergency</option>
						<option value="general surgery">General Surgery</option>
						<option value="human resources">Human Resources</option>
						<option value="maternity">Maternity</option>
						<option value="oncology">Oncology</option>
						<option value="orthopaedics">Orthopaedics</option>
						<option value="pain management">Pain Management</option>
					</select>
					<div class="out"></div>
				</div>			
			</div>
			<div class="twelve columns">
				<label class="label">Language</label>
				<div class="selectbox">  
					<select class="selectboxdiv">
						<option value="">Select a Language</option>
						<option value="english">English</option>
						<option value="spanish">Spanish</option>
					</select>
					<div class="out"></div>
				</div>			
			</div>
			<div class="twelve columns">
				<label class="label">Gender</label>
				<div class="selectbox">  
					<select class="selectboxdiv">
						<option value="">Select a Gender</option>
						<option value="female">Female</option>
					</select>
					<div class="out"></div>
				</div>			
			</div>
			<div class="twelve columns">
				<label class="label">City</label>
				<div class="selectbox">  
					<select class="selectboxdiv">
						<option value="">Select a City</option>
						<option value="Cambridge, MA">Cambridge, MA</option>
						<option value="Hiawatha, IA">Hiawatha, IA</option>
						<option value="Old Katy, TX">Old Katy, TX</option>
					</select>
					<div class="out"></div>
				</div>			
			</div>
			<div class="twelve columns">
				<label class="label">Zip</label>
				<input type="text" class="textbox" placeholder="Enter a Zip Code" />
				<span class="errortext" style="display:none;">Zip code must be 5 digits.</span>
				<span class="errortext" style="display:none;">Please enter zip code.</span>
			</div>			
			<div class="twelve columns">
				<label class="label">Within</label>
				<div class="selectbox">  
					<select class="selectboxdiv">
					<option value="">Select a Radius</option>
					</select>
					<div class="out"></div>
				</div>			
			</div>
			<div class="twelve columns">
				<input type="submit" value="Search" class="button" />
			</div>
		</div>
	</div>
</div>
<!--==================== /Module: Physician Directory: Physician Search Again =============================-->