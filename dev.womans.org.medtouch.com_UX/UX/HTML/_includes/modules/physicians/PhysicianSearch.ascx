<!--==================== Module: Physician Directory: Physician Search =============================-->
<div class="module-pd-search core-search twelve columns grid">
    <div class="six columns">	
        <label class="label">Name</label>
        <input type="text" class="textbox" placeholder="Enter a First or Last Name" />
	</div>
    <div class="six columns">
		<label class="label">Specialty</label>
		<div class="selectbox">  
			<select class="selectboxdiv">
			<option value="">Select a Specialty</option>
			<option value="dermatology">Dermatology</option>
			<option value="family general medicine">Family-General Medicine</option>
			</select>
			<div class="out"></div>
		</div>		
	</div>
	<section class="form-toggle">
		<div class="six columns">
			<label class="label">Last Name</label>
			<input type="text" class="textbox" placeholder="Enter a Last Name">
		</div>
		<div class="six columns">
		  <label class="label">Language</label>
		  <div class="selectbox">  
			  <select class="selectboxdiv">
				<option value="">Select a Language</option>
				<option value="english">English</option>
				<option value="spanish">Spanish</option>
			  </select>
			  <div class="out"></div>
		  </div>		
		</div>
		<div class="six columns">
			<label class="label">Gender</label>
			<div class="selectbox">  
				  <select class="selectboxdiv">
					<option value="">Select a Gender</option>
					<option value="male">Male</option>
					<option value="female">Female</option>
				  </select>
				  <div class="out"></div>
			</div>		
		</div>
		<div class="six columns">
			<label class="label">City</label>
			<div class="selectbox">  
				<select class="selectboxdiv">
				<option value="">Select a City</option>
				<option value="Cambridge , MA">Cambridge , MA</option>
				<option value="Hiawatha, IA">Hiawatha, IA</option>
				<option value="Old Katy, TX">Old Katy, TX</option>
				</select>
				<div class="out"></div>
			</div>			
		</div>
		<div class="six columns">
			<div class="six columns">
				<label class="label">Zip</label>
				<input type="text" class="textbox" placeholder="Zip Code"  />
				<span class="errortext" style="display:none;">Zip code must be 5 digits.</span>
				<span class="errortext" style="display:none;">Please enter zip code.</span>
			</div>
			<div class="six columns">
				<label class="label">Within</label>
				<div class="selectbox">  
					<select class="selectboxdiv">
					<option value="">Radius</option>
					</select>
					<div class="out"></div>
				</div>					
			</div>
		</div>
	</section> 
	<div class="grid twelve columns toggle-hide-show"> 
		<a href="#"><span class="show">Advanced Search +</span></a>
	</div>	
	<div class="search-option-submit twelve columns">
		<input type="submit" value="Search" class="button">
	</div>   
</div>
<!--==================== Module: Physician Directory: Physician Search =============================-->