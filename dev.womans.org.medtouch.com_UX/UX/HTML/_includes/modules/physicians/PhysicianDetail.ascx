<!--==================== Module: Physician Directory: Physician Detail =============================-->
<div class="module-pd-profile core-profile">
  
    <div class="module-pd-profile-top grid">

        <div class="new-patients">
        	<span></span>
        	<p>Jada Armstrong is Currently Accepting New Patients</p>
        </div>

        <h2>Bio Introduction ipsum dolor sit amet, consectetur adipiscing elit. Praesent laoreet odio ut sollicitudin pharetra. Vestibulum ut tellus eget ac sit </h2>
		
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent laoreet odio ut sollicitudin pharetra. Vestibulum ut tellus eget risus tempor dictum ac sit amet ipsum. Nunc nec ante euismod, congue velit quis, blandit lorem. Suspendisse ac mi hendrerit, auctor enim at, tincidunt massa. Curabitur eu magna a orci placerat cursus et dapibus nunc. Pellentesque rhoncus ac eros ac cursus. Duis iaculis egestas mi, sit amet vehicula tortor dapibus et. Cras in bibendum nisl.</p>

		<p>Sed pulvinar nunc vitae dignissim hendrerit. Suspendisse iaculis, ipsum nec viverra gravida, lorem erat imperdiet arcu, posuere tristique nisl diam in mauris. Nunc in aliquam sem, sit amet eleifend nunc. Morbi interdum elit et eros molestie ullamcorper. Vestibulum luctus mattis eros at laoreet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor, est at sollicitudin lacinia, ante tellus accumsan nisl, eget ultrices libero dolor eu ante.</p>
	</div>
	
	
   
	<!--------==========================TABS JQUERY==============================-->
	<div class="module-pd-tabs responsive-tabs">
			<h2>Overview</h2>
			<div id="pnlOverview">
				<div class="module-pd-tab-label">
					<h2>Biography</h2>
				</div>
				<div id="main_1_contentpanel_1_ctl04_pnlTabContentCopy">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis, neque laoreet
						laoreet placerat, tellus purus pretium enim, at egestas nisi elit sed sem. Nullam
						et sapien vitae risus semper vehicula non in orci. Nulla sed egestas massa. Aenean
						sit amet tincidunt enim. Sed et justo ac diam rhoncus dapibus id vitae justo. Donec
						ac lacus vitae enim facilisis eleifend non at est. Nullam pulvinar elementum augue,
						at porttitor erat posuere eu. Sed at lobortis lacus. Donec rutrum leo a lectus fermentum
						posuere. Aliquam id purus lorem, vel tristique ante. Praesent dapibus lobortis bibendum.
					</p>
				</div>
				<div class="module-pd-tab-label">
					<h2>Areas of expertise</h2>
				</div>
				<div><p>Family Medicine</p></div>

				<div class="module-pd-tab-label">
					<h2>Education</h2>
				</div>
				<div>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis, neque laoreet
						laoreet placerat, tellus purus pretium enim, at egestas nisi elit sed sem. Nullam
						et sapien vitae risus semper vehicula non in orci. Nulla sed egestas massa. Aenean
						sit amet tincidunt enim. Sed et justo ac diam rhoncus dapibus id vitae justo. Donec
						ac lacus vitae enim facilisis eleifend non at est. Nullam pulvinar elementum augue,
						at porttitor erat posuere eu. Sed at lobortis lacus. Donec rutrum leo a lectus fermentum
						posuere. Aliquam id purus lorem, vel tristique ante. Praesent dapibus lobortis bibendum.
					</p>
				</div>
				
				<div id="main_1_contentpanel_1_ctl04_pnlTabLanguages">
					<div class="module-pd-tab-label">
						<h2>Languages</h2>
					</div>
					<div class="module-pd-attribute">
						<ul>
							<li>English</li><li>Spanish</li>
						</ul>
					</div>
				</div>
			</div>
			
			<h2>Achievements</h2>
			<div id="pnlAchievements">
				<div id="main_1_contentpanel_1_ctl04_pnlTabAwards">
					<div class="module-pd-tab-label">
						Awards</div>
					<div class="module-pd-attribute">
						<ul>
							<li>Award 1</li>
							<li>Award 2</li>
						</ul>
					</div>
					<div class="clear">
					</div>
				</div>
				<div id="main_1_contentpanel_1_ctl04_pnlTabPublications">
					<div class="module-pd-tab-label">
						Publications</div>
					<div class="module-pd-attribute">
						<ul>
							<li>Pub 1</li>
							<li>Pub 2</li>
						</ul>
					</div>
					<div class="clear">
					</div>
				</div>
				<div id="main_1_contentpanel_1_ctl04_pnlTabCommunity">
					<div class="module-pd-tab-label">
						Community</div>
					<div class="module-pd-attribute">
						<ul>
							<li>Comm 1</li>
							<li>Comm 2</li>
						</ul>
					</div>
				</div>
			</div>

			<h2>Media</h2>
			<div id="pnlMedia">
				<div id="main_1_contentpanel_1_ctl04_pnlTabMedia">
					<iframe width="420" height="315" src="http://www.youtube.com/embed/ERJPv5RteSQ" frameborder="0"
						allowfullscreen></iframe>
				</div>
			</div>
			<h2>Insurance</h2>
			<div>
				<div id="main_1_contentpanel_1_ctl04_pnlTabSpecialties">
					<div class="module-pd-tab-label">Specialties</div>
					<div class="module-pd-attribute">
						<ul>
							<li>Blue Cross and Blue Shield</li>
							<li>Tufts Health Plan</li>
						</ul>
					</div>
				</div>
				<div>
					<div class="module-pd-tab-label">Conditions Treated</div>
					<div class="module-pd-attribute">
						<ul>
							<li><a href="#">Family-General Medicine</a></li>
							<li><a href="#">Dermatology</a></li>
						</ul>
					</div>
				</div>
				<div>
					<div class="module-pd-tab-label">Services Provided</div>
					<div class="module-pd-attribute">
						<ul>
							<li><a href="#">Family-General Medicine</a></li>
							<li><a href="#">Dermatology</a></li>
						</ul>
					</div>
				</div>
			</div>

	</div>
    <!--------==========================TABS JQUERY==============================-->
</div>
<!--==================== /Module: Physician Directory: Physician Detail =============================-->
