<!--==================== Module: Physician Directory: Physician Listing =============================-->
<div class="module-pd-results">
    <div class="module-pg-wrapper">
        <div class="module-pg-nav">
			<ul class="pagination">
				<li class="arrow no-link"><a href="">&laquo;</a></li>
				<li class="active"><a href="">1</a></li>
				<li><a href="">2</a></li>
				<li><a href="">3</a></li>
				<li><a href="">4</a></li>
				<li class="arrow"><a href="">&raquo;</a></li>
			</ul>			
		</div>
     	<div class="module-pg-info">Page 1 of 210 | Results 1 - 10 of 2093</div>
	</div>
	<!--THE RESULTS=================================================-->
    <div class="listing grid">
        <div class="listing-item">
            <div class="three columns">
                <div class="module-pd-thumbnail core-thumbnail">
                    <a href="/physician-directory/f/fancher-sandra/"><img src="http://placehold.it/150x150" alt="" /></a>
                </div>
                <div class="listing-item-more-link">
                    <a class="button read-more" href="/physician-directory/f/fancher-sandra/">View Profile</a>
                </div>
            </div>
            <div class="nine columns">
                <div class="module-pd-listing-info twelve columns">
				<h3><a href="/physician-directory/f/fancher-sandra/">Mike Johnson, <span>MD, DDS</span></a></h3>
                    <div class="module-pd-specialty-title">Family Nurse Practitioner</div>
                    <div id="" class="module-pd-specialty-list">
						Area of Expertise:
						<div class="list">Family Medicine, Ophthalmology, Diagnostic Radiology</div>
                    </div>					
                </div>
				<!--LIST OF OFFICES===============================================-->
                <div class="module-pd-office-listing grid">
                    <div class="module-pd-offices">
                        <div class="module-pd-office-item six columns">
                            <section class="inside">
								<div>
									<h5>Eastern Oregon Correctional Institution</h5>
									<label
										id="main_1_contentpanel_2_lvSearchResults_ctl00_0_rptOffices_0_lblPrimary_0">
									</label>
								</div>
								<div>
									2500 Westgate <br />
									Pendleton, OR 97801 
								</div>
								<div class="miles">
									<span class="icon"></span>Approx. .35 miles away 
								</div>								
								<div class="phones">
									<div>
										<div class="callout-phone"><a href="tel:503-574-6595">503-574-6595</a></div>
									</div>
								</div>			
							</section>
                        </div>
                        
                        <div class="module-pd-office-item six columns">
                            <section class="inside">
								<div>
									<h5>Eastern Oregon Correctional Institution</h5>
									<label
										id="main_1_contentpanel_2_lvSearchResults_ctl00_0_rptOffices_0_lblPrimary_0">
									</label>
								</div>
								<div>
									2500 Westgate <br />
									Pendleton, OR 97801 
								</div>
								<div class="miles">
									<span class="icon"></span>Approx. .35 miles away 
								</div>								
								<div class="phones">
									<div>
										<div class="callout-phone"><a href="tel:503-574-6595">503-574-6595</a></div>
									</div>
								</div>			
							</section>
                        </div>
					</div>
					<div class="module-pd-offices ">
                        <div class="module-pd-office-item six columns">
                            <section class="inside">
								<div>
									<h5>Eastern Oregon Correctional Institution</h5>
									<label
										id="main_1_contentpanel_2_lvSearchResults_ctl00_0_rptOffices_0_lblPrimary_0">
									</label>
								</div>
								<div>
									2500 Westgate <br />
									Pendleton, OR 97801 
								</div>
								<div class="miles">
									<span class="icon"></span>Approx. .35 miles away 
								</div>								
								<div class="phones">
									<div>
										<div class="callout-phone"><a href="tel:503-574-6595">503-574-6595</a></div>
									</div>
								</div>			
							</section>
                        </div>
                        
                        <div class="module-pd-office-item six columns">
                            <section class="inside">
								<div>
									<h5>Eastern Oregon Correctional Institution</h5>
									<label
										id="main_1_contentpanel_2_lvSearchResults_ctl00_0_rptOffices_0_lblPrimary_0">
									</label>
								</div>
								<div>
									2500 Westgate <br />
									Pendleton, OR 97801 
								</div>
								<div class="miles">
									<span class="icon"></span>Approx. .35 miles away 
								</div>								
								<div class="phones">
									<div>
										<div class="callout-phone"><a href="tel:503-574-6595">503-574-6595</a></div>
									</div>
								</div>			
							</section>
                        </div>
					</div>
                </div>
            </div>
        </div>
    </div>
	
    <div class="listing grid">
        <div class="listing-item">
            <div class="three columns">
                <div class="module-pd-thumbnail">
                    <a href="/physician-directory/f/fancher-sandra/">
                        <img src="http://placehold.it/150x150" alt="slide image"></a>
                </div>
                <div class="listing-item-more-link">
                    <a class="button read-more" href="/physician-directory/f/fancher-sandra/">View Profile</a>
                </div>
            </div>
            <div class="nine columns">
                <div class="module-pd-listing-info twelve columns">
				<!--<section class="pd-info">
					<div class="mychart"><a href="#">MyChart</a></div>
					<div class="accepting">Accepting New Patients</div>
				</section>-->
				<h3><a href="/physician-directory/f/fancher-sandra/">Mike Johnson, <span>MD, DDS</span></a></h3>
                    <div class="module-pd-specialty-title">
						Family Nurse Practitioner 
                    </div>
                    <div id="" class="module-pd-specialty-list">
						Area of Expertise:
							<div class="list">Family Medicine, Ophthalmology, Diagnostic Radiology</div>
                    </div>					
                </div>

				<!--LIST OF OFFICES===============================================-->
                <div class="module-pd-office-listing grid">
                    <div class="module-pd-offices">
                        <div class="module-pd-office-item six columns">
                            <section class="inside">
								<div>
									<h5>Eastern Oregon Correctional Institution</h5>
									<label
										id="main_1_contentpanel_2_lvSearchResults_ctl00_0_rptOffices_0_lblPrimary_0">
									</label>
								</div>
								<div>
									2500 Westgate <br />
									Pendleton, OR 97801 
								</div>
								<div class="miles">
									<span class="icon"></span>Approx. .35 miles away 
								</div>								
								<div class="phones">
									<div>
										<div class="callout-phone"><a href="tel:503-574-6595">503-574-6595</a></div>
									</div>
								</div>			
							</section>
                        </div>
                        
                        <div class="module-pd-office-item six columns">
                            <section class="inside">
								<div>
									<h5>Eastern Oregon Correctional Institution</h5>
									<label
										id="main_1_contentpanel_2_lvSearchResults_ctl00_0_rptOffices_0_lblPrimary_0">
									</label>
								</div>
								<div>
									2500 Westgate <br />
									Pendleton, OR 97801 
								</div>
								<div class="miles">
									<span class="icon"></span>Approx. .35 miles away 
								</div>								
								<div class="phones">
									<div>
										<div class="callout-phone"><a href="tel:503-574-6595">503-574-6595</a></div>
									</div>
								</div>			
							</section>
                        </div>
					</div>
					<div class="module-pd-offices ">
                        <div class="module-pd-office-item six columns">
                            <section class="inside">
								<div>
									<h5>Eastern Oregon Correctional Institution</h5>
									<label
										id="main_1_contentpanel_2_lvSearchResults_ctl00_0_rptOffices_0_lblPrimary_0">
									</label>
								</div>
								<div>
									2500 Westgate <br />
									Pendleton, OR 97801 
								</div>
								<div class="miles">
									<span class="icon"></span>Approx. .35 miles away 
								</div>								
								<div class="phones">
									<div>
										<div class="callout-phone"><a href="tel:503-574-6595">503-574-6595</a></div>
									</div>
								</div>			
							</section>
                        </div>
                        
                        <div class="module-pd-office-item six columns">
                            <section class="inside">
								<div>
									<h5>Eastern Oregon Correctional Institution</h5>
									<label
										id="main_1_contentpanel_2_lvSearchResults_ctl00_0_rptOffices_0_lblPrimary_0">
									</label>
								</div>
								<div>
									2500 Westgate <br />
									Pendleton, OR 97801 
								</div>
								<div class="miles">
									<span class="icon"></span>Approx. .35 miles away 
								</div>								
								<div class="phones">
									<div>
										<div class="callout-phone"><a href="tel:503-574-6595">503-574-6595</a></div>
									</div>
								</div>			
							</section>
                        </div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--==================== /Module: Physician Directory: Physician Listing =============================-->
