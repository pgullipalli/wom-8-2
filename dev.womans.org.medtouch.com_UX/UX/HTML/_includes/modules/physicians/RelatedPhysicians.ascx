<!--==================== Module: Physician Directory: Related Physicians =============================-->
<div class="module-pd-related core-related callout collapse-for-mobile">
    <div class="reg-callout">
        <h3>Related Physicians</h3>
        <ul class="core-list">
            <li class="core-li grid">
                <div class="three columns">
                    <img src="http://placehold.it/50x50" alt="photo_of_event_award_gala" />
                </div>
                <div class="nine columns">
                    <h5><a href="/physician-directory/f/fancher-sandra/">Sandra Fancher MD, PhD</a></h5>
                    <div>
                        <ul>
                            <li>Family-General Medicine</li>
                            <li>Dermatology</li>
                        </ul>
                    </div>
                    <div class="listing-item-more-link">
                        <a href="/physician-directory/f/fancher-sandra/">View Profile</a>
                    </div>
                </div>
            </li>
            <li class="core-li grid">
                <div class="twelve columns">
                    <h5><a href="/physician-directory/f/fancher-sandra/">Sandra Fancher MD, PhD</a></h5>
                    <div>
                        <ul>
                            <li>Family-General Medicine</li>
                            <li>Dermatology</li>
                        </ul>
                    </div>
                    <div class="listing-item-more-link">
                        <a href="/physician-directory/f/fancher-sandra/">View Profile</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
<!--==================== /Module: Physician Directory: Related Physicians =============================-->
