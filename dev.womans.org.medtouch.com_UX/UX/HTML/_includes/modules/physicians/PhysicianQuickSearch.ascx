<!--==================== Module: Physician Directory: Physician Quick Search =============================-->
<div class="module-pd-quick-search core-quick-search core-search callout collapse-for-mobile">
    <div class="reg-callout grid">
        <h3>Find a Doctor</h3>
        <div class="twelve columns">
            <label class="label">Last Name</label>
            <input type="text" placeholder="Enter a Last Name" class="textbox">
		</div>
        <div class="twelve columns">
            <label class="label">Specialty</label>
			<div class="selectbox">  
				<select class="selectboxdiv">
					<option value="">Select a Specialty</option>
					<option value="dermatology">Dermatology</option>
					<option value="family general medicine">Family-General Medicine</option>
				</select>
				<div class="out"></div>
			</div>			
		</div>
		<div class="twelve columns">
			<a href="/physician-directory/">Advanced Search</a>
		</div>
		<div class="twelve columns">
			<input type="submit" value="Search" class="button">
		</div>
    </div>
</div>
<!--==================== /Module: Physician Directory: Physician Quick Search =============================-->
