<!--==================== Module: Site Search: Search Again =============================-->
<div class="module-ss-search-again core-search-again core-search grid">
    <div class="six columns">
        <label>Keyword</label>
        <input type="text" placeholder="enter keyword(s)" class="textbox">
	</div>
    <div class="six columns">
        <label>Filter by Category</label>
        <div class="selectbox">
	        <select class="selectboxdiv">
				<option value="">All Categories</option>
				<option value="doctors">Doctors</option>
			</select>
			<div class="out"></div>
    	</div>
	</div>
    <div class="twelve columns">
    	<input type="submit" value="Search" class="button">
    </div>
</div>
<!--==================== /Module: Site Search: Search Again =============================-->
