<!--==================== Module: StayWell: StayWell Get Collection Contents =============================-->
<div class="module-sw-getcollection">
	
    
            <div class="listing">
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=95&amp;contentID=P10601&amp;language=en">Podcast: Fighting Breast Cancer: The Latest Treatment Techniques		</a></h4>
                <div class="listing-item-teaser">
                    Women diagnosed with breast cancer today have more treatment options available to them than ever before. And scientists continue to make advancements.
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=95&amp;contentID=P10580&amp;language=en">Podcast: Strong Social Ties May Help Women Survive Cancer	</a></h4>
                <div class="listing-item-teaser">
                    Learning you have breast cancer can be overwhelming. Many women face hard decisions about their care. A new study indicates that having a strong social network may help women better cope with a breast cancer diagnosis. 
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=95&amp;contentID=P10562&amp;language=en">Podcast: Patient Service Helps Spot Cancer Early</a></h4>
                <div class="listing-item-teaser">
                    Early diagnosis is crucial in fighting breast cancer. It often leads to faster treatment and a better chance of survival. That�s where a service called "patient navigation" may fit in. A recent study shows this service may shorten the time to diagnos
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=95&amp;contentID=P10538&amp;language=en">Podcast: Mammography Pluses Top Any Harms	</a></h4>
                <div class="listing-item-teaser">
                    For older women, the benefits of getting a mammogram every two years outweigh potential harms, researchers say. 
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=95&amp;contentID=P10525&amp;language=en">Podcast: Study Suggests Change in Radiation Guidelines in Older Women	</a></h4>
                <div class="listing-item-teaser">
                    An older woman who has radiation therapy after a lumpectomy may lower her need for a mastectomy later on, a new study says. Yet current guidelines recommend that older breast cancer patients not have radiation.
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=95&amp;contentID=P10501&amp;language=en">Podcast: Do Larger Infants Raise Breast Cancer Risk?</a></h4>
                <div class="listing-item-teaser">
                    Women who give birth to large infants may be 2.5 times more likely to develop breast cancer than women who give birth to the smallest babies. 
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=95&amp;contentID=P10488&amp;language=en">Podcast: Radiation Treatment in Childhood Boosts Breast Cancer Risk	</a></h4>
                <div class="listing-item-teaser">
                    Girls who get radiation therapy to the chest to treat cancer are at higher risk for breast cancer by the time they turn 50, a new study says. 
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=95&amp;contentID=P10470&amp;language=en">Podcast: Moving Toward a Blood Test for Breast Cancer</a></h4>
                <div class="listing-item-teaser">
                    Researchers are looking closer at a blood test that assesses a certain gene's DNA. The test may one day be able to predict who�s at risk for breast cancer years before it develops. 
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=95&amp;contentID=P10452&amp;language=en">Podcast: Side Effects Linger After Breast Cancer Treatment</a></h4>
                <div class="listing-item-teaser">
                    In a study that followed breast cancer patients after treatment, more than 60 percent had at least one treatment-related complication up to six years after diagnosis. Thirty percent had at least two complications. 
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=95&amp;contentID=P10433&amp;language=en">Podcast: Cadmium Exposure May Boost Breast Cancer Risk</a></h4>
                <div class="listing-item-teaser">
                    Cadmium can find its way into the diet via fruits and vegetables grown in soils fertilized with products containing the toxic metal. In the body, cadmium may mimic the effects of estrogen, raising the risk for certain breast cancers.
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=95&amp;contentID=P10415&amp;language=en">Podcast: Breast Cancer Drug May Increase Bone Loss</a></h4>
                <div class="listing-item-teaser">
                    A drug that can cut the risk for breast cancer has a serious down side: Aromasin appears to cause bone loss in postmenopausal women. 
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=95&amp;contentID=P10397&amp;language=en">Podcast: Do Parabens Affect Breast Cancer Risk?</a></h4>
                <div class="listing-item-teaser">
                    Some personal hygiene products contain preservatives called parabens, which have estrogen-like properties. Do these chemicals raise the risk for breast cancer? Experts say more research is needed to know for sure. 
                </div>
            </div>
        
            </div>
        

</div>
<!--==================== Module: StayWell: StayWell Get Collection Contents  =============================-->
