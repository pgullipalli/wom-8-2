<!--==================== Module: StayWell: StayWell Search By Category  =============================-->
<div class="module-sw-searchbycategory core-search twelve columns grid">
	
    <!--<div class="six columns">
		<label class="label"></label>
		<input type="text" placeholder="enter keyword(s)" class="textbox">
	</div>-->
	
    <div class="six columns">
        <label></label>
		<div class="selectbox">
			<select class="selectboxdiv">
				<option selected="selected" value="">ALL</option>
				<option value="15125">Baby Foods</option>
				<option value="15126">Baked Products</option>
				<option value="15127">Beef Products</option>
				<option value="15128">Beverages</option>
				<option value="15129">Breakfast Cereals</option>
				<option value="15130">Cereal Grains and Pasta</option>
				<option value="15131">Dairy and Egg Products</option>
				<option value="15132">Fast Foods</option>
				<option value="15133">Fats and Oils</option>
				<option value="15134">Finfish and Shellfish Products</option>
				<option value="15135">Fruits and Fruit Juices</option>
				<option value="15136">Lamb, Veal, and Game Products</option>
				<option value="15137">Legumes and Legume Products</option>
				<option value="15138">Meals, Entrees, and Sidedishes</option>
				<option value="15139">Nut and Seed Products</option>
				<option value="15140">Pork Products</option>
				<option value="15141">Poultry Products</option>
				<option value="15142">Sausages and Luncheon Meats</option>
				<option value="15143">Snacks</option>
				<option value="15144">Soups, Sauces, and Gravies</option>
				<option value="15145">Spices and Herbs</option>
				<option value="15146">Sweets</option>
				<option value="15147">Vegetables and Vegetable Products</option>
			</select>
			<div class="out">ALL</div>
		</div>		
	</div>
	
	<div class="search-option-submit twelve columns">
		<input type="submit" value="Search" class="button" />
	</div>

</div>
<!--==================== /Module: StayWell: StayWell Search By Category  =============================-->
