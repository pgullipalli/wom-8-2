<!--==================== Module: StayWell: StayWell Search Box  =============================-->
<div class="module-sw-searchbox core-search twelve columns grid">
	
    <h4>Search</h4>
    <div class="six columns">
        <label></label>
       <input type="text" placeholder="enter keyword(s)" class="textbox">
        <span style="color:Red;display:none;">*</span>
    
	</div>
    <div class="search-option-submit twelve columns">
		<input type="submit" value="Search" />
	</div>
</div>
<!--==================== /Module: StayWell: StayWell Search Box  =============================-->
