<!--==================== Module: Clinical Trials: CTQuickSearchBox  =============================-->
<div class="module-ct-quick-search core-quick-search core-search callout collapse-for-mobile">
    <div class="reg-callout grid">
        <h3>Clinical Trial Search</h3>
		<div class="twelve columns">
            <label class="label">Search By Keyword</label>
            <input type="text" placeholder="enter keyword(s)" class="textbox">
		</div>
		<div class="twelve columns">
            <label class="label">Search by Condition/Disease</label>
			<div class="selectbox">  
				<select class="selectboxdiv">
					<option value="">Select a Condition/Disease</option>
					<option value="breast-cancer">Breast Cancer</option>
				</select>
				<div class="out"></div>
			</div>			
		</div>
		<div class="twelve columns">
			<input type="submit" value="Search" class="button">
		</div>
	</div>
</div>
<!--==================== /Module: Clinical Trials: CTQuickSearchBox  =============================-->
