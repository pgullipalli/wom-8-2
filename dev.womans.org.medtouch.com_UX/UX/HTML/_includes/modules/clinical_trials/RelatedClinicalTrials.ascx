<!--==================== Module: Clinical Trials: RelatedClinicalTrials  =============================-->
<div class="module-ct-related article">
	
    
        <h4><span>Related Clinical Trials</span></h4>
    
    <div class="listing">
		
        
                <div class="listing-item">
			
                    <h5><a href="/clinical-trials/a/a-trial/">A Trial</a></h5>
                    <div class="listing-item-teaser">
				
                        <p>This is my content copy</p>
                    
			</div>
                    <div class="listing-item-more-link">
				
                        <a href="/clinical-trials/a/a-trial/">Read More</a>
                    
			</div>
                
		</div>
            
    
	</div>
    

</div>
<!--==================== /Module: Clinical Trials: RelatedClinicalTrials  =============================-->
