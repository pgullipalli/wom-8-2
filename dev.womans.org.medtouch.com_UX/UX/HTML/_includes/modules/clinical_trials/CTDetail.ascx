<!--==================== Module: Clinical Trials: CTDetail  =============================-->
<div class="module-ct-profile article">
	<!-- TODO: NEED TO ADD RETURN LINK HERE -->
	<div class="return">
		<a href="#">� Return to Search</a>
	</div>
    <h1>A Trial</h1>
    <div>
		
        <label>Description:</label>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer arcu sem, tempor at accumsan< sit amet, interdum et nisl. Quisque eu nisi id sem faucibus pulvinar. Quisque in ornare tellus. Pellentesque et arcu at magna dictum lacinia pretium vitae est. Vivamus eros nibh, tristique in feugiat eget, pretium vel metus. Curabitur vulputate faucibus eleifend. Cras massa massa, semper sit amet gravida non, egestas in mi. In lacinia volutpat neque, vel consequat augue varius quis. Suspendisse quis urna dignissim diam congue fermentum. Maecenas consequat, turpis quis semper ullamcorper, felis ante cursus dui, et rhoncus dui ipsum vel leo. Vivamus eleifend blandit arcu sit amet rhoncus. Nulla nec magna sed leo luctus mollis. Phasellus aliquet mollis tempor.</p>
    
	</div>
    <div class="single-line-listing">
		
        <label>Active Date:</label>
        November 23, 2012
    
	</div>
    <div class="single-line-listing">
		
        <label>Start Date:</label>
        November 23, 2012
    
	</div>
    <div class="single-line-listing">
		
        <label>Expire Date:</label>
        November 23, 2012
    
	</div>
    <div class="single-line-listing">
		
        <label>IRB Number:</label>
        123456789
    
	</div>
    <div class="single-line-listing">
		
        <label>Condition/Disease:</label>
        
                Breast Cancer
    
	</div>
    <div class="single-line-listing">
		
        <label>Lead Researcher:</label>
        Lead Research Name
    
	</div>
    <div>
		
        <label>Location:</label>
        <p>Location Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer arcu sem, tempor at accumsan sit amet, interdum et nisl. Quisque eu nisi id sem faucibus pulvinar. Quisque in ornare tellus. Pellentesque et arcu at magna dictum lacinia pretium vitae est. Vivamus eros nibh, tristique in feugiat eget, pretium vel metus. Curabitur vulputate faucibus eleifend. Cras massa massa, semper sit amet gravida non, egestas in mi. In lacinia volutpat neque, vel consequat augue varius quis. Suspendisse quis urna dignissim diam congue fermentum. Maecenas consequat, turpis quis semper ullamcorper, felis ante cursus dui, et rhoncus dui ipsum vel leo. Vivamus eleifend blandit arcu sit amet rhoncus. Nulla nec magna sed leo luctus mollis. Phasellus aliquet mollis tempor.</p>
    
	</div>
    <div>
		
        <label>Purpose:</label>
        <p>Purpose Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer arcu sem, tempor at accumsan sit amet, interdum et nisl. Quisque eu nisi id sem faucibus pulvinar. Quisque in ornare tellus. Pellentesque et arcu at magna dictum lacinia pretium vitae est. Vivamus eros nibh, tristique in feugiat eget, pretium vel metus. Curabitur vulputate faucibus eleifend. Cras massa massa, semper sit amet gravida non, egestas in mi. In lacinia volutpat neque, vel consequat augue varius quis. Suspendisse quis urna dignissim diam congue fermentum. Maecenas consequat, turpis quis semper ullamcorper, felis ante cursus dui, et rhoncus dui ipsum vel leo. Vivamus eleifend blandit arcu sit amet rhoncus. Nulla nec magna sed leo luctus mollis. Phasellus aliquet mollis tempor.</p>
    
	</div>
    <div>
		
        <label>Eligibility:</label>
        <p>Eligibility Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer arcu sem, tempor at accumsan sit amet, interdum et nisl. Quisque eu nisi id sem faucibus pulvinar. Quisque in ornare tellus. Pellentesque et arcu at magna dictum lacinia pretium vitae est. Vivamus eros nibh, tristique in feugiat eget, pretium vel metus. Curabitur vulputate faucibus eleifend. Cras massa massa, semper sit amet gravida non, egestas in mi. In lacinia volutpat neque, vel consequat augue varius quis. Suspendisse quis urna dignissim diam congue fermentum. Maecenas consequat, turpis quis semper ullamcorper, felis ante cursus dui, et rhoncus dui ipsum vel leo. Vivamus eleifend blandit arcu sit amet rhoncus. Nulla nec magna sed leo luctus mollis. Phasellus aliquet mollis tempor.</p>
    
	</div>
    <div>
		
        <label>Time Commitment:</label>
        <p>Time Commitment Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer arcu sem, tempor at accumsan sit amet, interdum et nisl. Quisque eu nisi id sem faucibus pulvinar. Quisque in ornare tellus. Pellentesque et arcu at magna dictum lacinia pretium vitae est. Vivamus eros nibh, tristique in feugiat eget, pretium vel metus. Curabitur vulputate faucibus eleifend. Cras massa massa, semper sit amet gravida non, egestas in mi. In lacinia volutpat neque, vel consequat augue varius quis. Suspendisse quis urna dignissim diam congue fermentum. Maecenas consequat, turpis quis semper ullamcorper, felis ante cursus dui, et rhoncus dui ipsum vel leo. Vivamus eleifend blandit arcu sit amet rhoncus. Nulla nec magna sed leo luctus mollis. Phasellus aliquet mollis tempor.</p>
    
	</div>
    <div>
		
        <label>Benefits:</label>
        <p>Benefits Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer arcu sem, tempor at accumsan sit amet, interdum et nisl. Quisque eu nisi id sem faucibus pulvinar. Quisque in ornare tellus. Pellentesque et arcu at magna dictum lacinia pretium vitae est. Vivamus eros nibh, tristique in feugiat eget, pretium vel metus. Curabitur vulputate faucibus eleifend. Cras massa massa, semper sit amet gravida non, egestas in mi. In lacinia volutpat neque, vel consequat augue varius quis. Suspendisse quis urna dignissim diam congue fermentum. Maecenas consequat, turpis quis semper ullamcorper, felis ante cursus dui, et rhoncus dui ipsum vel leo. Vivamus eleifend blandit arcu sit amet rhoncus. Nulla nec magna sed leo luctus mollis. Phasellus aliquet mollis tempor.</p>
    
	</div>
    <div>
		
        <label>Compensation:</label>
        <p>Compensation Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer arcu sem, tempor at accumsan sit amet, interdum et nisl. Quisque eu nisi id sem faucibus pulvinar. Quisque in ornare tellus. Pellentesque et arcu at magna dictum lacinia pretium vitae est. Vivamus eros nibh, tristique in feugiat eget, pretium vel metus. Curabitur vulputate faucibus eleifend. Cras massa massa, semper sit amet gravida non, egestas in mi. In lacinia volutpat neque, vel consequat augue varius quis. Suspendisse quis urna dignissim diam congue fermentum. Maecenas consequat, turpis quis semper ullamcorper, felis ante cursus dui, et rhoncus dui ipsum vel leo. Vivamus eleifend blandit arcu sit amet rhoncus. Nulla nec magna sed leo luctus mollis. Phasellus aliquet mollis tempor.</p>
    
	</div>
    <div class="single-line-listing">
		
        <label>Status:</label>
        Accepting Participants
    
	</div>
    <div class="single-line-listing">
		
        <label>Current Trial Type:</label>
        Type
    
	</div>
    <div class="single-line-listing">
		
        <label>Phase:</label>
        II/III
    
	</div>
    <div class="single-line-listing">
		
        <label>Contact Name:</label>
        Dan Persson
    
	</div>
    <div class="single-line-listing">
		
        <label>Phone:</label>
        617-621-8670 x305
    
	</div>
    <div class="single-line-listing">
		
        <label>Fax:</label>
        617-621-8670
    
	</div>
    <div class="single-line-listing">
		
        <label>Alternate Phone:</label>
        617-621-8670
    
	</div>
    <div class="single-line-listing">
		
        <a href="mailto:dpersson@medtouch.com">Email</a>
    
	</div>

</div>
<!--==================== /Module: Clinical Trials: CTDetail  =============================-->
