<!--==================== Module: Services: Service Search =============================-->
<div class="module-sv-search core-search grid">
    <div class="twelve columns">
        <label>Search by Keyword</label>
        <input type="text" placeholder="enter keyword(s)" class="textbox">
    </div>
    <div class="twelve columns search-option-submit">
		<input type="submit" value="Search" class="button">
    </div>
</div>
<!--==================== /Module: Services: Service Search =============================-->
