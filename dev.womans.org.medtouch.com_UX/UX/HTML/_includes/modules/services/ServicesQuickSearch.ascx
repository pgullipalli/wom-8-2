<!--==================== Module: Services: Service Quick Search =============================-->
<div class="module-sv-quick-search core-quick-search core-search callout collapse-for-mobile">
	<div class="reg-callout grid">
		<h3>Services Search</h3>
		<div class="twelve columns">
			<label>Search by Keyword</label>
			<input type="text" placeholder="enter keyword(s)" class="textbox">
		</div>
		<div class="search-option-submit twelve columns">
			<input type="submit" value="Search" class="button">
		</div>
	</div>
</div>
<!--==================== /Module: Services: Service Quick Search =============================-->
