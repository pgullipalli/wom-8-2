﻿<!--==================== Module: Photo Gallery =============================-->
<div class="module-photo-gallery">
    <div class="stage">
        <div class="image-wrap"></div>
		<div class="image-overlay">
			<div class="image-count"><span class="current"></span> of <span class="total"></span></div>
			<div class="image-details">
				<h6 class="image-title"></h6>
				<div class="image-desc"></div>
			</div>
		</div>
    </div>
    <div class="thumbs">
        <img data-title="Locomotives Roundhouse" data-description="Steam locomotives of the Chicago &amp; North Western Railway." data-src="http://upload.wikimedia.org/wikipedia/commons/3/34/Locomotives-Roundhouse2.jpg" src="/assets/images/fpo-gallery/fpo-gallery-thumb-001.jpg">
        <img data-title="Icebergs in the High Arctic" data-description="The debris loading isn't particularly extensive, but the color is usual." data-src="http://upload.wikimedia.org/wikipedia/commons/3/36/Icebergs_in_the_High_Arctic_-_20050907.jpg" src="/assets/images/fpo-gallery/fpo-gallery-thumb-002.jpg">
        <img data-title="Araña" data-description="Xysticus cristatus, A Estrada, Galicia, Spain" data-src="http://upload.wikimedia.org/wikipedia/commons/f/fe/Ara%C3%B1a._A_Estrada%2C_Galiza._02.jpg" src="/assets/images/fpo-gallery/fpo-gallery-thumb-003.jpg">
        <img data-title="Museo storia naturale" data-src="http://upload.wikimedia.org/wikipedia/commons/2/2d/9104_-_Milano_-_Museo_storia_naturale_-_Fluorite_-_Foto_Giovanni_Dall%27Orto_22-Apr-2007.jpg" src="/assets/images/fpo-gallery/fpo-gallery-thumb-004.jpg">
        <img data-title="Grjótagjá caves in summer 2009" data-id="railroad-car" data-src="http://upload.wikimedia.org/wikipedia/commons/1/15/Grj%C3%B3tagj%C3%A1_caves_in_summer_2009_%282%29.jpg" src="/assets/images/fpo-gallery/fpo-gallery-thumb-005.jpg">
        <img data-title="Thermes" data-description="Xanthi hot-spa springs, Xanthi Prefecture, Greece" data-src="http://upload.wikimedia.org/wikipedia/commons/9/90/20091128_Loutra_Thermes_Xanthi_Thrace_Greece_2.jpg" src="/assets/images/fpo-gallery/fpo-gallery-thumb-006.jpg">
        <img data-title="Polish Army Kołobrzeg" data-description="A display of the Polish Army. Both the soldier, and the vehicle belong to the 7th Pomeranian Coastal Defence Brigade, a part of the Szczecin-based 12th Mechanized Division &quot;Bolesław Krzywousty&quote;" data-src="http://upload.wikimedia.org/wikipedia/commons/5/58/Polish_Army_Ko%C5%82obrzeg_077.JPG" src="/assets/images/fpo-gallery/fpo-gallery-thumb-007.jpg">
        <img data-title="Zlatograd Bulgaria" data-src="http://upload.wikimedia.org/wikipedia/commons/2/2d/20100213_Zlatograd_Bulgaria_3.jpg" src="/assets/images/fpo-gallery/fpo-gallery-thumb-008.jpg">
        <img data-title="09-28-2001 in New York City" data-description="New York, NY, September 28, 2001 -- Debris on surrounding roofs at the site of the World Trade Center. Photo by Andrea Booher/ FEMA News Photo" data-src="http://upload.wikimedia.org/wikipedia/commons/b/b5/FEMA_-_5399_-_Photograph_by_Andrea_Booher_taken_on_09-28-2001_in_New_York.jpg" src="/assets/images/fpo-gallery/fpo-gallery-thumb-009.jpg">
        <img data-src="http://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/India_Karnataka_location_map.svg/500px-India_Karnataka_location_map.svg.png" src="/assets/images/fpo-gallery/fpo-gallery-thumb-010.png">
    </div>
</div>
<!--==================== Module: Photo Gallery =============================-->