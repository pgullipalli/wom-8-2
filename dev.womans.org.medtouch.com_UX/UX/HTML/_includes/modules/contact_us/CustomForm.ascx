<div class="customform">
	
    <!--start required fields-->
    <h3 class="subhead"><span class="required">*</span> Required fields</h3>
    <fieldset>
        <div class="grid-full">
            <label><span class="required">*</span> Name:</label>
            <input type="text" class="textfield">
            <span class="warning">Name is Required</span>
		</div>
         <div class="grid-full">
            <label><span class="required">*</span> E-mail address:</label>
            <input type="text" class="textfield">
            <span class="warning">Email address is Required</span>
            <span class="warning">Enter a valid Email Address</span>
	   </div>
         <div class="grid-full">
            <label><span class="required">*</span> ZIP code:</label>
            <input type="text" class="textfield">
            <span class="warning">Zip code is Required</span>
		</div>
         <div class="grid-full">
            <label><span class="required">*</span> Question or comment:</label>
            <textarea rows="2" cols="20"></textarea>
			<span class="character">Write your question within <em>1000</em> characters.</span>
            <span class="warning">Question is Required</span>
		</div>
    </fieldset>
    <!--end required fields-->
	<!--start optional fields-->
    <h3 class="subhead">&nbsp; Optional fields</h3>
    <fieldset>
         <div class="grid-full">			
            <label>Providence Health Plan member ID:
<span class="php_member_info">(entering this information will help us respond to you more quickly)</span></label>
            <input type="text" class="textfield">
		</div>
        <div class="grid-full">
            <label>Address:</label>
            <input type="text" class="textfield">
		</div>
		<div class="grid-full">
            <label>City:</label>
            <input type="text" class="textfield">
		</div>
        <div class="grid-full">
            <label>State:</label>
            <select class="filter_select styled">
		<option value="-- Select State --">-- Select State --</option>
		<option value="AK">Alaska</option>
		<option value="AL">Alabama</option>
		<option value="AR">Arkansas</option>
		<option value="AZ">Arizona</option>
		<option value="CA">California</option>
		<option value="CO">Colorado</option>
		<option value="CT">Connecticut</option>
		<option value="DC">District of Columbia</option>
		<option value="DE">Delaware</option>
		<option value="FL">Florida</option>
		<option value="GA">Georgia</option>
		<option value="HI">Hawaii</option>
		<option value="IA">Iowa</option>
		<option value="ID">Idaho</option>
		<option value="IL">Illinois</option>
		<option value="IN">Indiana</option>
		<option value="KS">Kansas</option>
		<option value="KY">Kentucky</option>
		<option value="LA">Louisiana</option>
		<option value="MA">Massachusetts</option>
		<option value="MD">Maryland</option>
		<option value="ME">Maine</option>
		<option value="MI">Michigan</option>
		<option value="MN">Minnesota</option>
		<option value="MO">Missouri</option>
		<option value="MS">Mississippi</option>
		<option value="MT">Montana</option>
		<option value="NC">North Carolina</option>
		<option value="ND">North Dakota</option>
		<option value="NE">Nebraska</option>
		<option value="NH">New Hampshire</option>
		<option value="NJ">New Jersey</option>
		<option value="NM">New Mexico</option>
		<option value="NV">Nevada</option>
		<option value="NY">New York</option>
		<option value="OH">Ohio</option>
		<option value="OK">Oklahoma</option>
		<option value="OR">Oregon</option>
		<option value="PA">Pennsylvania</option>
		<option value="RI">Rhode Island</option>
		<option value="SC">South Carolina</option>
		<option value="SD">South Dakota</option>
		<option value="TN">Tennessee</option>
		<option value="TX">Texas</option>
		<option value="UT">Utah</option>
		<option value="VA">Virginia</option>
		<option value="VT">Vermont</option>
		<option value="WA">Washington</option>
		<option value="WI">Wisconsin</option>
		<option value="WV">West Virginia</option>
		<option value="WY">Wyoming</option>

	</select>
        </div>
        <div class="grid-full">
            <label>Telephone:</label>
            <input type="text" class="textfield">
            <span style="color:Red;visibility:hidden;">Enter a valid Telephone</span>
		</div>
    </fieldset>
    <!--end optional fields-->
     <div class="grid-full">
        <input type="submit" value="Send Message" class="button">
	</div>

</div>