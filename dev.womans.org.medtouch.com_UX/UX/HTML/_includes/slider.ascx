			<section class="hero home grid">
				<div class="decoration"></div>
				<div class="inner">
					<!--<img src="/assets/images/hero-fpo.jpg" />-->
					<div class="mtslide">
					  <ul class="slides">
						<li>		 
						<span data-picture data-alt="Testing images">
							<span data-src="/assets/images/homepage/image1-mobile.jpg"></span>
							<span data-src="/assets/images/homepage/demo-image-one-tablet.jpg"  data-media="(min-width: 400px)"></span>
							<span data-src="/assets/images/homepage/demo-image-one.jpg"  data-media="(min-width: 800px)"></span>

							<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
							<noscript>
								<img src="assets/images/image1-mobile.jpg" alt="image" />
							</noscript>
						</span>						 
						  <div class="core">
							  <div class="slide-content">
								<h1>
									<a href="#">
										Have a vision of the best possible 
										<span>YOU? So do we</span>
										<span class="arrow"></span>
									</a>
								</h1>
								<div class="copy-left">
									<p>Lorem Ipsum Dole salit.</p>
									<a href="#" class="more">Learn more ></a>
								</div>
							  </div>
						  </div>
						</li>
						<li> 
						<span data-picture data-alt="Testing images">
							<span data-src="/assets/images/homepage/image2-mobile.jpg"></span>
							<span data-src="/assets/images/homepage/demo-image-two-tablet.jpg"  data-media="(min-width: 400px)"></span>
							<span data-src="/assets/images/homepage/demo-image-two.jpg"  data-media="(min-width: 800px)"></span>

							<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
							<noscript>
								<img src="assets/images/image2-mobile.jpg" alt="image">
							</noscript>
						</span>	
                        <div class="core">
							  <div class="slide-content">
								<h1>
									<a href="#">
										Slide 2
										<span>Lorem Ipsum dolor</span>
										<span class="arrow"></span>
									</a>
								</h1>
								<div class="copy-left">
									<p>Lorem Ipsum Dole salit.</p>
									<a href="#" class="more">Learn more ></a>
								</div>
							  </div>
						  </div>
                         </li>
									
					  </ul>
					</div>		

					<div class="homepage-services-slider-full-width">
						<ul>
							<li class="baby first">
								<a href="#">
									<span class="popup-content">
										<img src="/assets/images/landing-jump.jpg" style="width: 100%;">
										<span class="popup-text">I am interested in learning more about what Woman's has to offer for Bones and Joints</span>
										<button class="default-button black" type="button">Learn More</button>									
									</span>
									<span class="icon"></span>
									Baby
								</a>
							</li>
							<li class="children-and-men second">
								<a href="#">
									<span class="popup-content">
										<img src="/assets/images/landing-jump.jpg" style="width: 100%;">
										<span class="popup-text">I am interested in learning more about what Woman's has to offer for Bones and Joints</span>
										<button class="default-button black" type="button">Learn More</button>									
									</span>
									<span class="icon"></span>
									Children<br> &amp; Men
								</a>
							</li>
							<li class="gynecology third">
								<a href="#">
									<span class="popup-content">
										<img src="/assets/images/landing-jump.jpg" style="width: 100%;">
										<span class="popup-text">I am interested in learning more about what Woman's has to offer for Bones and Joints</span>
										<button class="default-button black" type="button">Learn More</button>									
									</span>
									<span class="icon"></span>
									Gynecology
								</a>
							</li>
							<li class="wellness-and-nutrition fourth">
								<a href="#">
									<span class="popup-content">
										<img src="/assets/images/landing-jump.jpg" style="width: 100%;">
										<span class="popup-text">I am interested in learning more about what Woman's has to offer for Bones and Joints</span>
										<button class="default-button black" type="button">Learn More</button>									
									</span>
									<span class="icon"></span>
									Wellness<br> &amp; Nutrition
								</a>
							</li>
							<li class="bone-and-joint fifth">
								<a href="#">
									<span class="popup-content">
										<img src="/assets/images/landing-jump.jpg" style="width: 100%;">
										<span class="popup-text">I am interested in learning more about what Woman's has to offer for Bones and Joints</span>
										<button class="default-button black" type="button">Learn More</button>									
									</span>
									<span class="icon"></span>
									Bone <br>&amp;  Joint
								</a>
							</li>
							<li class="cancer sixth">
								<a href="#">
									<span class="popup-content">
										<img src="/assets/images/landing-jump.jpg" style="width: 100%;">
										<span class="popup-text">I am interested in learning more about what Woman's has to offer for Bones and Joints</span>
										<button class="default-button black" type="button">Learn More</button>									
									</span>
									<span class="icon"></span>
									Cancer
								</a>
							</li>
							<li class="surgery last">
								<a href="#">
									<span class="popup-content">
										<img src="/assets/images/landing-jump.jpg" style="width: 100%;">
										<span class="popup-text">I am interested in learning more about what Woman's has to offer for Bones and Joints</span>
										<button class="default-button black" type="button">Learn More</button>									
									</span>
									<span class="icon"></span>
									Surgery
								</a>
							</li>
						</ul>
					</div>	


					<div class="homepage-services-slider-mobile">
						<div class="owl-carousel-homepage">
							<ul>
								<li class="baby">
									<a href="#">
										<span class="icon"></span>
										Baby
									</a>
								</li>
								<li class="children-and-men">
									<a href="#">
										<span class="icon"></span>
										Children<br> &amp; Men
									</a>
								</li>
								<li class="gynecology">
									<a href="#">
										<span class="icon"></span>
										Gynecology
									</a>
								</li>
								<li class="wellness-and-nutrition">
									<a href="#">
										<span class="icon"></span>
										Wellness<br> &amp; Nutrition
									</a>
								</li>
								<li class="bone-and-joint">
									<a href="#">
										<span class="icon"></span>
										Bone <br>&amp;  Joint
									</a>
								</li>
								<li class="cancer">
									<a href="#">
										<span class="icon"></span>
										Cancer
									</a>
								</li>
								<li class="surgery">
									<a href="#">
										<span class="icon"></span>
										Surgery
									</a>
								</li>
							</ul>
						</div>					
					</div>
				</div>			
			</section> 