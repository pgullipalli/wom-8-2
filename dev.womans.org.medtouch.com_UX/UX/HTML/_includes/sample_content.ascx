<article>
	
	
	<h2>We strive to help each mother to have the birth she desires.</h2>
	
	<div class="highlight-box right">
		<h2>Contact Us Today</h2>
		<p>We offer breast cancer detection and treatment in three convenient locations.</p>
		<h3>
			<a href="tel:225-215-7981">225-215-7981</a>
		</h3>
	</div>
	
	<p>Lorem ipsum dolor sit amet, <b>bold text (b)</b> consectetur adipiscing elit. Curabitur <strong>bold text (strong)</strong> condimentum blandit diam 
	pharetra viverra. <i>Italic text (i)</i>Cras rutrum. <em>Italic text (em)</em>Urna eu consequat tempor, massa lacus. Ipsum lorem dolor amet sit aliquet 
	eget scelerisque eo consequat. Maecenas eu quam risus, et vehicula orci. <a href="#">Pellentesque</a> fringilla lacinia dolor, at gravida nibh suscipit ac. Sed auctor 
	orci a justo venenatis eu rutrum nunc sodales. Quisque sit amet dolor a purus euismod condimentum. Donec tempor posuere felis, sed tempor risus iaculis et. 
	Integer a nunc sed velit convallis ultrices. Fusce vestibulum mauris nib.</p>

	<p>Lorem ipsum dolor sit amet, <b>bold text (b)</b>consectetur adipiscing elit. Curabitur <strong>bold text (strong)</strong>condimentum blandit diam 
	pharetra viverra. <i>Italic text (i)</i>Cras rutrum.</p>
	<ul class="custom-list">
		<li>Lorem ipsum dolor sit amet</li>
		<li>Quisque hendrerit scelerisque interdum. Curabitur a nisi convallis quam posuere auraesent sapien odio, laoreet at lacinia 
			<ul>
			<li>Lorem ipsum dolor sit amet</li>
			<li>Consectetur adipiscing elit </li>
			<li>Curabitur condimentum blandit diam pharetra</li>
			</ul>
		</li>
		<li>Curabitur condimentum blandit diam pharetra</li>
	</ul>
							
	<p>Curabitur condimentum blandit diam pharetra viverra. <a href="#">This is a link text</a> Cras rutrum. Urna eu consequat tempor, massa lacus. Ipsum lorem dolor amet sit aliquet eget scelerisque leo consequat. Maecenas eu quam risus, et vehicula orci. Pellentesque fringilla lacinia dolor, at gravida nibh suscipit ac.</p>	
							
	<h2>Lorem Ipsum Subheading Forat forli Abstergo(h2)</h2>
	<!-- 	<img src="http://placehold.it/150x150" alt="" class="left" />  -->	
	<p>Donec tempor posuere felis, sed tempor risus iaculis et. Integer a nunc sed velit convallis ultrices. Fusce vestibulum mauris nibh. 
	Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							
	<h3>Lorem Ipsum Subheading (h3)</h3>
	<p>Ut odio ipsum, iaculis eu eleifend at, feugiat eu dui. In aliquam velit a nisl pretium id
	condimentum arcu euismod. Vestibulum varius posuere neque sit amet semper.</p>	
							
	<!-- <div class="widget right">
		<div class="widget-content">
		<div class="img-holder">
		<img src="http://placehold.it/150x150" alt="" />
		</div>
		<span class="title"><a href="#">This is an Example of a Content Callout Box</a></span>
		<p>Lorem ipsum dolor sit amet, sectetur adipiscing edolor sit amet, dolor sit amet. </p>
		</div>
	</div> -->
							
	<h4>Lorem Ipsum Subheading (h4)</h4>
	<p>Donec tempor posuere felis, sed tempor risus iaculis et. Integer a nunc sed velit convallis ultrices. Fusce vestibulum mauris nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>	
							
	<ol>
		<li>Lorem ipsum dolor sit amet</li>
		<li>Lorem ipsum dolor sit amet</li>
		<li>Lorem ipsum dolor sit amet</li>
	</ol>

	<h5>Table Styling (h5)</h5>
	<p>Donec tempor posuere felis, sed tempor risus iaculis et. Integer a nunc sed velit convallis ultrices. Fusce vestibulum mauris nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>	
		
		<h5>Basic Table Style</h5>
		<table class="data-grid" summary="data">
			<thead>
				<tr>
					<th>#</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Username</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Mark</td>
					<td>Otto</td>
					<td>@mdo</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Jacob</td>
					<td>Thornton</td>
					<td>@fact</td>
				</tr>
				<tr>
					<td>3</td>
					<td>Larry</td>
					<td>the Bird</td>
					<td>@twitter</td>
				</tr>
			</tbody>
		</table>
		
		<h5>Striped Table Style: Add class "striped"</h5>
		<table class="data-grid striped">	
			<thead>
				<tr>
					<th>#</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Username</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Mark</td>
					<td>Otto</td>
					<td>@mdo</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Jacob</td>
					<td>Thornton</td>
					<td>@fact</td>
				</tr>
				<tr>
					<td>3</td>
					<td>Larry</td>
					<td>the Bird</td>
					<td>@twitter</td>
				</tr>
			</tbody>
        </table>		

        <h5>Bordered Table Style: Add class "bordered"</h5>
		<table class="data-grid bordered">	
			<thead>
				<tr>
					<th>#</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Username</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Mark</td>
					<td>Otto</td>
					<td>@mdo</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Jacob</td>
					<td>Thornton</td>
					<td>@fact</td>
				</tr>
				<tr>
					<td>3</td>
					<td>Larry</td>
					<td>the Bird</td>
					<td>@twitter</td>
				</tr>
			</tbody>
        </table>		


        <!--------==========================Accordion JQUERY==============================-->
		<div class="accordion">
			<h3><a href="#">Accordion Panel One</a></h3>
			<div class="accordian_copy">
			<p>Nam lectus neque, bibendum eg faibus et lacus et augue gravida dapibus. Teger faculis enim etu amet graon. Menius temu luctus met, consectetur adipiscing elit. Integer iaculis ulla facilisi ullam at nulla <a href="#">vitae velit rhoncus pellentesque</a>. Nam lectus neque, biliquet lacus et augue gravida dapibus done allerium cras ponex</p>
			</div>

			<h3><a href="#">Accordion Panel Two</a></h3>
			<div class="accordian_copy">
			<p>AccordionContent2</p>
			</div>

			<h3><a href="#">Accordion Panel Three</a></h3>
			<div class="accordian_copy">
			<p>AccordionContent2</p>
			</div>
		</div>
		<!--------==========================Accordion JQUERY==============================-->	
        					
        <!--------==========================TABS JQUERY==============================-->
        <div class="responsive-tabs">
            <h2>Tab Tittle 1</h2>
            <div>
                <h4>Test Title (h4)</h4>
                <p>
                    Nam lectus neque, bibendum eg faibus et lacus et augue gravida dapibus. Teger faculis
                    enim etu amet graon. Menius temu luctus met, consectetur adipiscing elit. Integer
                    iaculis ulla facilisi ullam at nulla <a href="#">vitae velit rhoncus pellentesque</a>.
                    Nam lectus neque, biliquet lacus et augue gravida dapibus done allerium cras ponex</p>
            </div>
            <h2>Tab Title 2</h2>
            <div>
                <ul>
                    <li>Lorem ipsum dolor sit amet</li>
                    <li>Consectetur adipiscing elit </li>
                    <li>Curabitur condimentum blandit diam pharetra</li>
                </ul>
            </div>
        </div>
        <!--------==========================TABS JQUERY==============================-->							
							
</article>
