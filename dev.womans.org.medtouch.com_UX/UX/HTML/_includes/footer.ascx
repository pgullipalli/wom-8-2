	<footer>
		<div class="back-to-top grid">
			<a href="#top"></a>
		</div>
		<div class="latest-tweet grid">
				<!-- Placeholder, twitter will generate latest tweet here. -->
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc convallis ut. <a href="#">felis vel placerat.</a></p>
		</div>
		<div class="footer-header grid">
			<div class="eight columns newsletter">
				<div class="border-wrapper">
					<div class="eight columns">
						<h3>My Woman's Email Newsletter</h3>
						<p>SIGN UP and get the latest on weight loss, nutrition, healthy living, fitness, motherhood, breast health and more every month!</p>
					</div>
					<div class="four columns">
						<div class="sign-up-form">
							<input class="text-input" type="text" placeholder="Full Name">
							<input class="text-input" type="text" placeholder="Email Address">
							<div class="submit-option">
								<input class="default-button black" type="submit" value="Sign Me Up!">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="four columns footer-donate">
				<div class="border-wrapper">
					<h3>Support Woman's</h3>
					<p>Your gift provides critical services and programs for women and babies in our community.</p>
					<button class="default-button black" type="button">Donate Now</button>
				</div>
			</div>
			
		</div>
		<div class="footer-main grid">
				<div class="four columns connect-with-womans">
					<h4>
						<span class="icon"></span>
						<span class="plus-minus"></span>
						Connect with Woman's
					</h4>
					<div class="footer-content">
						<ul class="footer-blog-links">
							<li class="mommy-go-round">
								<span class="icon"></span>
								<a href="#">The Mommy Go-Round Blog</a>
							</li>
							<li class="weight-loss-surgery">
								<span class="icon"></span>
								<a href="#">A New You: Weight Loss Surgery Blog</a>
							</li>
							<li class="living-with-cancer">
								<span class="icon"></span>
								<a href="#">Living With Cancer Blog</a>
							</li>
							<li class="healthy-self">
								<span class="icon"></span>
								<a href="#">Healthy Self Blog</a>
							</li>
						</ul>
						<ul class="social-links">
							<li class="twitter-link">
								<a href="#">Twitter</a>
							</li>
							<li class="wp-link">
								<a href="#">WordPress</a>
							</li>
							<li class="facebook-link">
								<a href="#">Facebook</a>
							</li>
							<li class="pinterest-link">
								<a href="#">Pinterest</a>
							</li>
							<li class="youtube-link">
								<a href="#">Youtube</a>
							</li>
							<li class="instagram-link">
								<a class="last" href="#">Instagram</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="four columns womans-footer-nav">
					<h4>						
						<span class="icon"></span>
						<span class="plus-minus"></span>
						Woman's For
					</h4>
					<div class="footer-content">
						<ul>
							<li>
								<a href="#">Patients &amp; Visitors</a>
							</li>
							<li>
								<a href="#">Health Professionals</a>
							</li>
							<li>
								<a href="#">Employees</a>
							</li>
							<li>
								<a href="#">Volunteers</a>
							</li>
							<li>
								<a href="#">Board of Directors</a>
							</li>
							<li>
								<a href="#">Children &amp; Men</a>
							</li>
							<li>
								<a href="#">Community Health</a>
							</li>
							<li>
								<a href="#">Job Applicants</a>
							</li>
							<li>
								<a href="#">Board of Directors</a>
							</li>
						</ul>
					</div>
				</div>
				
				<div class="four columns locations-and-maps">
					
					<h4>						
						<span class="icon"></span>
						<span class="plus-minus"></span>
						Locations &amp; Map
					</h4>

					<div class="footer-content">
						<address>
							<span>Woman's Main Campus</span><br>
							100 Woman's Way<br>
							Baton Rouge, LA 70817
						</address>

						<p>
							Main Number: 
							<a class="phone-number" href="225-927-1300">(225) 927-1300</a>
						</p>
						<p>
							Patient Rooms: 
							<a class="phone-number" href="225-927-1300">(225) 231-5[+]</a><br>
							<span class="ital">[last 3 digits of room number]</span>
						</p>
						<p>
							Patient Info: 
							<a class="phone-number" href="225-927-1300">(225) 924-8157</a>
						</p>

						<button type="button" class="default-button pink">View More</button>
					</div>
				</div>

		</div>

		<div class="footer-utility-links">
			<ul>
				<li>
					<a href="#">About Woman's</a>
				</li>
				<li>
					<a href="#">Careers</a>
				</li>
				<li>
					<a href="#">Contact Us</a>
				</li>
				<li>
					<a href="#">Donate</a>
				</li>
				<li>
					<a href="#">Your Bill</a>
				</li>
			</ul>

			<br>

			<ul>
				<li>
					<span>Copyright 2014</span>
				</li>
				<li>
					<a href="#">Terms &amp; Conditions</a>
				</li>
				<li>
					<a href="#">Privacy Policy</a>
				</li>
				<li>
					<a href="#">by MedTouch</a>
				</li>
			</ul>
		</div>
	</footer>