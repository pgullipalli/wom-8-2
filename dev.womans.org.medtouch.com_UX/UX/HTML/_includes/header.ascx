	<%@ Register TagPrefix="uc" TagName="nav" Src="/UX/HTML/_includes/nav.ascx" %>

	<div class="right-utility-A">
		<div class="text-resizer"></div>
		<ul>
			<li>
				<a href="#" class="print-button">Print</a>
			</li>
			<li>
				<a href="http://www.addthis.com/bookmark.php" class="email-button addthis_button share">Email</a>
			</li>
		</ul>
	</div>
	<div class="right-utility-B">
		<ul class="social-links">
			<li class="twitter-link">
				<a href="#">Twitter</a>
			</li>
			<li class="wp-link">
				<a href="#">WordPress</a>
			</li>
			<li class="facebook-link">
				<a href="#">Facebook</a>
			</li>
			<li class="pinterest-link">
				<a href="#">Pinterest</a>
			</li>
			<li class="youtube-link">
				<a href="#">Youtube</a>
			</li>
			<li class="instagram-link">
				<a class="last" href="#">Instagram</a>
			</li>
		</ul>
	</div>
	
	<header role="banner">
		
		<section class="dropdown-panel">
			<div class="grid">
				<div class="eight columns quick-links-nav">
					<div class="inner">
						<div class="three columns">
							<h4>Woman's For...</h4>
							<ul>
								<li>
									<a href="#">Patients</a>
								</li>
								<li>
									<a href="#">Visitors</a>
								</li>
								<li>
									<a href="#">Employees</a>
								</li>
								<li>
									<a href="#">Health Professionals</a>
								</li>
								<li>
									<a href="#">Board of Directors</a>
								</li>
								<li>
									<a href="#">Volunteers</a>
								</li>
								<li>
									<a href="#">Community Health</a>
								</li>
								<li>
									<a href="#">Children</a>
								</li>
								<li>
									<a href="#">Men</a>
								</li>
							</ul>
						</div>
						<div class="three columns">
							<h4>Visiting Woman's</h4>
							<ul>
								<li>
									<a href="#">Parking</a>
								</li>
								<li>
									<a href="#">Visiting Hours</a>
								</li>
								<li>
									<a href="#">Picking Up a Patient</a>
								</li>
								<li>
									<a href="#">Gift Shop</a>
								</li>
								<li>
									<a href="#">Dining</a>
								</li>
								<li>
									<a href="#">Where to Stay</a>
								</li>
							</ul>
						</div>
						<div class="three columns">
							<h4>Popular Links</h4>
							<ul>
								<li>
									<a href="#">Bill Pay</a>
								</li>
								<li>
									<a href="#">Birthplace Photos</a>
								</li>
								<li>
									<a href="#">Careers</a>
								</li>
								<li>
									<a href="#">News &amp; Videos</a>
								</li>
								<li>
									<a href="#">Find a Doctor</a>
								</li>
								<li>
									<a href="#">Classes</a>
								</li>
								<li>
									<a href="#">About Woman's</a>
								</li>
								<li>
									<a href="#">Make a Donation</a>
								</li>
								<li>
									<a href="#">Contact Us</a>
								</li>
							</ul>
						</div>
						<div class="three columns">
							<h4>Patient Services</h4>
							<ul>
								<li>
									<a href="#">Find a Doctor</a>
								</li>
								<li>
									<a href="#">Our Services</a>
								</li>
								<li>
									<a href="#">Registration</a>
								</li>
								<li>
									<a href="#">Amenities</a>
								</li>
								<li>
									<a href="#">Pay Your Bill</a>
								</li>
								<li>
									<a href="#">Your Rights</a>
								</li>
								<li>
									<a href="#">Your Responsibilities</a>
								</li>
								<li>
									<a href="#">Privacy Policy</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="four columns dropdown-feature">
					<div class="inner">
						<img class="dropdown-feature-image" src="/assets/images/footer/hands.png">
						<h4>Giving Opportunities</h4>
						<p>
							There are many ways to support Woman's Hospital. Your gift provides critical services and programs for women and babies in our community. 
							<a href="#">Read More</a>
						</p>
					</div>
				</div>
			</div>

		</section>

		<section class="top grid">
			
			<div class="mobile-dropdown-nav">
				<ul>
					<li class="mobile-menu-button">
						<a href="#">
							<span></span>
							Menu
						</a>
					</li>
					<li class="mobile-search-button">
						<a href="#">
							<span></span>
							Search
						</a>
					</li>
					<li class="mobile-call-button">
						<a href="#">
							<span></span>
							Call
						</a>
					</li>
				</ul>
			</div>
			
			<div class="logo">
				<a title="MedTouch" href="#">
				<img src="/assets/images/logo.svg" alt="MedTouch" class="screen-logo" width="160" height="80">
				<img src="/assets/images/logo.jpg" alt="MedTouch" class="ie-print">
				
				<span>exceptional care, centered on you</span>
				<!-- <img src="/assets/images/logo.png" alt="MedTouch"> -->
				</a>
			</div>

			<a class="donate" href="#">Donate</a>

			<div class="header-utility">
				<div class="header-utility-contents">
					<table>
						<tr>
							<!-- <td>
								<a class="donate" href="#">Donate</a>
							</td> -->
							<td>
								<div class="search-table-wrapper">
									<table>
										<tr>
											<td>
												<a class="quick-links" href="#">
													Quick Links
													<span class="arrow"></span>
												</a>
											</td>
										</tr>
										<tr>
											<!-- <td>
												<input type="search" class="main-search-input" placeholder="Search Term">
											</td> -->
										</tr>
									</table>
								</div>
							</td>
							<td>
							<a class="main-search" href="#"></a>
						</tr>
					</table>
				</div>
				

			</div>
			
			

			<uc:nav ID="ucNav" runat="server" />

			<!-- Mobile CODE: added .mobile-menu -->
			<section class="mobile-menu visible-mobile">
				<a href="#" class="toggle2"><span>In This Section</span></a>
			</section>
			<!-- / END Mobile CODE -->
			
			
		</section>	
	 </header>