﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/ThreeColumn.Master" AutoEventWireup="true" %>
<%@ Register TagPrefix="uc" TagName="samplecontent" Src="/UX/HTML/_includes/sample_content.ascx" %>
<%@ Register TagPrefix="uc" TagName="prox" Src="/UX/HTML/_includes/prox.ascx" %>
<%@ Register TagPrefix="uc" TagName="callout" Src="/UX/HTML/_includes/callout.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="LeftPanel" runat="server">
    <uc:prox ID="prox" runat="server"/>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
	<uc:samplecontent ID="samplecontent" runat="server"/>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="RightPanel" runat="server">
    <uc:callout ID="callout" runat="server"/>
</asp:Content>
