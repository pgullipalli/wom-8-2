<cfparam name="Attributes.formName" type="string">
<cfparam name="Attributes.fieldName" type="string">
<cfparam name="Attributes.previewID" type="string" default="">
<cfparam name="Attributes.windowWidth" type="numeric" default="800">
<cfparam name="Attributes.windowHeight" type="numeric" default="600">

<cfoutput>
<cfif Attributes.previewID Is "">
  <cfset Variables.previewID="preview_" & Attributes.formName & "_" & Attributes.fieldName>
<cfelse>
  <cfset Variables.previewID=Attributes.previewID>
</cfif>
<input type="button" value="Select Page" onclick="xsite.openWindow('index.cfm?md=pagebuilder&tmp=pageselector&formName=#Attributes.formName#&fieldName=#Attributes.fieldName#&previewID=#previewID#&wrap=1','pageselector',#Attributes.windowWidth#,#Attributes.windowHeight#)" />

<input type="button" value="Clear" onclick="xsite.clearFormField('#Attributes.formName#', '#Attributes.fieldName#');xsite.clearTextNode('#Variables.previewID#')" />
<cfif Attributes.previewID Is ""><br />
<div id="#Variables.previewID#" style="margin:5px 0px;"></div>
</cfif>

</cfoutput>
