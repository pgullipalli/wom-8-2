<!--- For TinyMCE Window--->
<cfparam name="Attributes.tinyMCEInstallationURL" type="string">
<cfparam name="Attributes.siteURLRoot" type="string">
<cfparam name="Attributes.stylesheetURL" type="string">
<cfparam name="Attributes.assetRelativeDirectory" type="string">
<cfparam name="Attributes.currentRelativeDirectory" type="string" default="">
<cfparam name="Attributes.templateListURL" type="string" default="">
<cfparam name="Attributes.useAbsoluteURL" type="boolean" default="true">
<cfparam name="Attributes.forceBrNewlines" type="boolean" default="false">
<cfparam name="Attributes.replaceTokens" type="string" default="">
<cfparam name="Attributes.replaceValues" type="string" default="">

<!--- For Text Area --->
<cfparam name="Attributes.formName" type="string">
<cfparam name="Attributes.fieldName" type="string">
<cfparam name="Attributes.HTMLContent" type="string" default="">
<cfparam name="Attributes.cols" type="numeric" default="72">
<cfparam name="Attributes.rows" type="numeric" default="20">	
<cfparam name="Attributes.editorHeader" type="string" default="">
<cfparam name="Attributes.displayHTMLSource" type="boolean" default="0">
<cfparam name="Attributes.buttonText" type="string" default="Open WYSIWYG Editor">
<cfparam name="Attributes.buttonStyle" type="string" default="">

<cfif Attributes.displayHTMLSource>
  <cfset displayTextArea="block">
<cfelse>
  <cfset displayTextArea="none">
</cfif>
<cfoutput>
<input type="button" value="#Attributes.buttonText#" <cfif Compare(Attributes.buttonStyle,"")>class="#Attributes.buttonStyle#"</cfif> onclick="xsite.openWindow('#Attributes.tinyMCEInstallationURL#/extension/editcontent.cfm?siteURLRoot=#Attributes.siteURLRoot#&tinyMCEInstallationURL=#URLEncodedFormat(Attributes.tinyMCEInstallationURL)#&useAbsoluteURL=#Attributes.useAbsoluteURL#&forceBrNewlines=#Attributes.forceBrNewlines#&templateListURL=#Attributes.templateListURL#&formName=#Attributes.formName#&fieldName=#Attributes.fieldName#&assetRelativeDirectory=#URLEncodedFormat(Attributes.assetRelativeDirectory)#&currentRelativeDirectory=#URLEncodedFormat(Attributes.currentRelativeDirectory)#&stylesheetURL=#URLEncodedFormat(Attributes.stylesheetURL)#&editorHeader=#URLEncodedFormat(Attributes.editorHeader)#','tinymce_edit#Attributes.fieldName#',850,700)">&nbsp;&nbsp;
<a href="javascript:xsite.openWindow('#Attributes.tinyMCEInstallationURL#/extension/previewcontent.cfm?formName=#Attributes.formName#&fieldName=#Attributes.fieldName#&stylesheetURL=#URLEncodedFormat(Attributes.stylesheetURL)#','view#Attributes.fieldName#',800,700)">&raquo; Preview</a> &nbsp;&nbsp;
<a href="##" onclick="xsite.toggle('divContent_#Attributes.formName#_#Attributes.fieldName#')">&raquo; Source</a><br>
<div ID="divContent_#Attributes.formName#_#Attributes.fieldName#" STYLE="margin-left:0;display:#displayTextArea#;">
  <textarea name="#Attributes.fieldName#" cols="#Attributes.cols#" rows="#Attributes.rows#">#HTMLEditFormat(Attributes.HTMLContent)#</textarea>
</div> 
</cfoutput>