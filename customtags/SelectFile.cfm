<cfparam name="Attributes.formName" type="string">
<cfparam name="Attributes.fieldName" type="string">
<cfparam name="Attributes.previewID" type="string" default="">
<cfparam name="Attributes.windowWidth" type="numeric" default="800">
<cfparam name="Attributes.windowHeight" type="numeric" default="600">
<cfparam name="Attributes.previewWidth" type="string" default="">
<cfparam name="Attributes.previewHeight" type="string" default="">

<cfoutput>
<cfif Attributes.previewID Is "">
  <cfset Variables.previewID="preview_" & Attributes.formName & "_" & Attributes.fieldName>
<cfelse>
  <cfset Variables.previewID=Attributes.previewID>
</cfif>
<input type="button" value="Select File" onclick="xsite.openWindow('index.cfm?md=file&tmp=fileselector&formName=#Attributes.formName#&fieldName=#Attributes.fieldName#&previewID=#previewID#&previewWidth=#Attributes.previewWidth#&previewHeight=#Attributes.previewHeight#&wrap=1','fileSelector',#Attributes.windowWidth#,#Attributes.windowHeight#)" />

<input type="button" value="Clear" onclick="xsite.clearFormField('#Attributes.formName#', '#Attributes.fieldName#');xsite.clearTextNode('#Variables.previewID#')" />
<cfif Attributes.previewID Is ""><br />
<div id="#Variables.previewID#" style="margin:5px 0px;"></div>
</cfif>

</cfoutput>
