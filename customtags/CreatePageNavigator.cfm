<cfparam name="Attributes.numTotalItems" type="numeric">
<cfparam name="Attributes.navigateBy" type="string" default="startIndex"><!--- either by pageNum or startIndex --->
<cfparam name="Attributes.navigateJSFunction" type="string" default="goToPage">
<cfparam name="Attributes.pageURL" type="string" default=""><!--- required if navigateBy=startIndex --->
<cfparam name="Attributes.startIndex" type="numeric" default="1"><!--- required if navigateBy=startIndex --->
<cfparam name="Attributes.pageNum" type="numeric" default="1"><!--- required if navigateBy=pageNum --->
<cfparam name="Attributes.numItemsPerPage" type="numeric" default="20">
<cfparam name="Attributes.linkStyleClassName" type="string" default="">

<cfset numPages=Ceiling(Attributes.numTotalItems/Attributes.numItemsPerPage)>
<cfif Attributes.navigateBy IS "pageNum">
  <cfset currentPageNum=Attributes.pageNum>
<cfelse>
  <cfset currentPageNum=Ceiling(Attributes.startIndex/Attributes.numItemsPerPage)>
</cfif>

<cfif numPages GT 1>
<cfoutput>
<p align="center">Page
  <cfloop index="idx" from="1" to="#numPages#">	  
    <cfif idx IS currentPageNum>
      #idx#
    <cfelse>
      <cfset sidx=Attributes.numItemsPerPage * (idx-1) + 1>	
	  <cfif Attributes.navigateBy IS "pageNum">
	    <a href="javascript:#Attributes.navigateJSFunction#(#idx#)"<cfif Attributes.linkStyleClassName IS NOT ""> class="#Attributes.linkStyleClassName#"</cfif>>#idx#</a>
	  <cfelse>
      <a href="#Attributes.pageURL#&startIndex=#sidx#&numItemsPerPage=#Attributes.numItemsPerPage#"<cfif Attributes.linkStyleClassName IS NOT ""> class="#Attributes.linkStyleClassName#"</cfif>>#idx#</a>
	  </cfif>
    </cfif>
  </cfloop>
</p>
</cfoutput>
</cfif>