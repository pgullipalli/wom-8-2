<div class="callout-wrapper">
	<div class="callout">
		<h4>
			<span>Upcoming Events</span>
		</h4>
		
		<div class="callout-event">
			<div class="event-header">
				<a href="#">
					<span class="event-date">22 Jan</span>
					Woman's Ideal Protein Information
				</a>
			</div>

			<p class="callout-event-preview">Hinc disputationi ei nam, mei etudoctus tamquam suscipit te. Enim mediocritatem vel eine, in qui falli minimum. <a href="#" class="read-more">Read More</a> </p>
		</div>

		<div class="callout-event">
			<div class="event-header">
				<a href="#">
					<span class="event-date">22 Jan</span>
					Woman's Ideal Protein Information
				</a>
			</div>

			<p class="callout-event-preview">Hinc disputationi ei nam, mei etudoctus tamquam suscipit te. Enim mediocritatem vel eine, in qui falli minimum. <a href="#" class="read-more">Read More</a> </p>
		</div>

		<button type="button" class="default-button black inset">View All Events</button>

	</div>
			
	 
	<div class="callout">
		<h4>
			<span>Patient Stories</span>
		</h4>
		
		<div class="grid">
			<div class="patient-stories">
				<div class="six columns patient-stories-callout-image">
					<img src="/assets/images/woman-ribbon.png">
				</div>	

				<div class="six columns">
					<div class="patient-story-text">We knew this was a winning combination from the moment we started working with Woman's. This is a dream come true.</div>
					<div class="patient-name">Susan Allen, <br>Cedar Rapids, IA</div>
				</div>	
			</div>
		</div>
	</div>

	<div class="callout">
		<h4>
			<span>Feature Story</span>
		</h4>

		<div class="feature-story-callout">
			<a href="#">
				<img src="/assets/images/landing-jump.jpg" style="width: 100%;">
			
			<div class="feature-story-title">
				<a href="#">I am newly diagnosed</a>
			</div>
			</a>
		</div>
	</div>
</div>