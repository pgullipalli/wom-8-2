<html>
<head>
    <title>Physician Directory</title>
    <link href="/assets/PDF/pdf.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="wrapper">
        <p class="cover">
            <%--<span class="document-name">Drexel Medicine</span><br/>--%>
            <span class="document-logo"><img src="/assets/images/logo.jpg" alt="logo"/></span><br/>
            <span class="document-title">Physician Directory</span><br/>
            <span class="document-caption">This Directory is Current as of May 8, 2012</span>
            <!-- <span class="page-header">
                <table class="ptop">
                    <tr>
                        <td class="topleft">
                            Physician Directory
                        </td>
                        <td class="topright">
                            Drexel Medicine
                        </td>
                    </tr>
                </table>
            </span> -->
        </p>
        <div class="listing">
            <table>
                <tr>
                    <td rowspan="2" class="col-photo">
                        <div class="doc-photo"><img src="http://dev.drexelmedicine.org.medtouch.com/assets/default/images/fpo_results.jpg" width="105" height="120" alt="" /></div>
                    </td>
                    <td colspan="2" class="col-name">
                        <h3>Darrin M. Breaux<span class="doc-small">MD, FACC</span></h3>
                        <div class="doc-position">Assistant Professor</div>
                    </td>
                </tr>
                <tr>
                    <td class="col-location">
                        <div class="location">
                            <h4>Baton Rouge Cardiology Center</h4>
                            <div>5231 Brittany Drive<br />
                            Baton Rouge, LA 70808</div>
                            <div><b>Phone:</b> (225)-769-0933<br>
                                 <b>Fax:</b> (225)-769-6255
                            </div>
                        </div>
                    </td>
                    <td class="col-info">
                        <h4>
                            Specialties:</h4>
                        <div class="content">
                            Cardiology</div>
                        <h4>
                            Education:</h4>
                        <div class="content">
                            D.O., Kirksville College of Osteopathic Medicine, Kirksville, Missouri, 1973</div>
                        <h4>
                            Residency:</h4>
                        <div class="content">
                            Surgery and Ob/Gyn, Metropolitan Hospital, Philadelphia, PA, 1975<br />
                            Thomas Jefferson University Hospital, Philadelphia, PA, 1978</div>
                    </td>
                </tr>
            </table>
            <hr/>
            <table>
                <tr>
                    <td rowspan="2" class="col-photo">
                        <div class="doc-photo"><img src="http://dev.drexelmedicine.org.medtouch.com/assets/default/images/fpo_results.jpg" width="105" height="120" alt="" /></div>
                    </td>
                    <td colspan="2" class="col-name">
                        <h3>Crystal Bianchi</h3>
                        <div class="doc-position">Certified Nurse Practitioner</div>
                    </td>
                </tr>
                <tr>
                    <td class="col-location">
                        <div class="location">
                            <h4>Cardiovascular Institute of the South</h4>
                            <div>7941 Picardy Avenue<br />
                            Philadelphia, PA 19102</div>
                            <div><b>Phone:</b> 215-762-3600</div>
                        </div>                       
                    </td>
                    <td class="col-info">
                        <h4>
                            Specialties:</h4>
                        <div class="content">
                            Gynecology
                        </div>
                        <h4>
                            Education:</h4>
                        <div class="content">
                            MSN, CRNP, University of Rochester, NY</div>
                    </td>
                </tr>
            </table>
            <hr/>
            <table>
                <tr>
                    <td rowspan="2" class="col-photo">
                        <div class="doc-photo"><img src="http://dev.drexelmedicine.org.medtouch.com/assets/default/images/fpo_results.jpg" width="105" height="120" alt="" /></div>
                    </td>
                    <td colspan="2" class="col-name">
                        <h3>Owen Montgomery<span class="doc-small">MD, PhD</span></h3>
                        <div class="doc-position">Professor, Chair of Gynecology Department</div>
                    </td>
                </tr>
                <tr>
                    <td class="col-location">
                        <div class="location">
                            <h4>Owen Montgomery</h4>
                            <div>255 S. 17th Street<br />
                            9th Floor<br />
                            Philadelphia, PA 19103</div>
                            <div><b>Phone:</b> 215-546-6475</div>
                            <div><b>Fax:</b> 215-546-6476</div>
                        </div>
                        <div class="location">
                            <h4>Baton Rouge Cardiology Center</h4>
                            <div>5231 Brittany Drive<br />
                            Baton Rouge, LA 70808</div>
                            <div><b>Phone:</b> (225)-769-0933<br>
                                 <b>Fax:</b> (225)-769-6255
                            </div>
                        </div>
                        
                    </td>
                    <td class="col-info">
                        <h4>
                            Specialties:</h4>
                        <div class="content">
                            Oncology, Cardiology</div>
                        <h4>
                            Education:</h4>
                        <div class="content">
                            D.O., Kirksville College of Osteopathic Medicine, Kirksville, Missouri, 1973</div>
                        <h4>
                            Residency:</h4>
                        <div class="content">
                            Surgery and Ob/Gyn, Metropolitan Hospital, Philadelphia, PA, 1975<br />
                            Thomas Jefferson University Hospital, Philadelphia, PA, 1978</div>
                            <h4>
                            Fellowship:</h4>
                        <div class="content">
                            Jefferson Medical College, Philadelphia, PA</div>
                    </td>
                </tr>
            </table>
            <hr/>
            <table>
                <tr>
                    <td rowspan="2" class="col-photo">
                        <div class="doc-photo"><img src="http://dev.drexelmedicine.org.medtouch.com/assets/default/images/fpo_results.jpg" width="105" height="120" alt="" /></div>
                    </td>
                    <td colspan="2" class="col-name">
                        <h3>Jack NoEducationData<span class="doc-small">MD, PhD</span></h3>
                        <div class="doc-position">Professor, Chair of Gynecology Department</div>
                    </td>
                </tr>
                <tr>
                    <td class="col-location">
                        <div class="location">
                            <h4>Owen Montgomery</h4>
                            <div>255 S. 17th Street<br />
                            9th Floor<br />
                            Philadelphia, PA 19103</div>
                            <div><b>Phone:</b> 215-546-6475</div>
                            <div><b>Fax:</b> 215-546-6476</div>
                        </div>
                        <div class="location">
                            <h4>Drexel Clinic</h4>
                            1234 Sunshine Road<br />
                            Philadelphia, PA 19102<br />
                            <span class="phone">Phone: 215-762-0000</span>
                        </div>
                        <div class="location">
                            <h4>Drexel Research Center</h4>
                            999 Sunset Ave<br />
                            Philadelphia, PA 19102<br />
                            <span class="phone">Phone: 215-762-0000</span>
                        </div>
                    </td>
                    <td class="col-info">
                        <h4>
                            Specialties:</h4>
                        <div class="content">
                            Oncology, Cardiology</div>
                    </td>
                </tr>
            </table>
            <hr/>
            <table>
                <tr>
                    <td rowspan="2" class="col-photo">
                        <div class="doc-photo"><img src="http://dev.drexelmedicine.org.medtouch.com/assets/default/images/fpo_results.jpg" width="105" height="120" alt="" /></div>
                    </td>
                    <td colspan="2" class="col-name">
                        <h3>Joe NoLocationData<span class="doc-small">MD, PhD</span></h3>
                        <div class="doc-position">Professor, Chair of Gynecology Department</div>
                    </td>
                </tr>
                <tr>
                    <td class="col-location">
                        <div class="location">
                            <h4>Owen Montgomery</h4>
                            <div>255 S. 17th Street<br />
                            9th Floor<br />
                            Philadelphia, PA 19103</div>
                            <div><b>Phone:</b> 215-546-6475</div>
                            <div><b>Fax:</b> 215-546-6476</div>
                        </div>
                        <div class="location">
                            <h4>Drexel Clinic</h4>
                            1234 Sunshine Road<br />
                            Philadelphia, PA 19102<br />
                            <span class="phone">Phone: 215-762-0000</span>
                        </div>
                        <div class="location">
                            <h4>Drexel Research Center</h4>
                            999 Sunset Ave<br />
                            Philadelphia, PA 19102<br />
                            <span class="phone">Phone: 215-762-0000</span>
                        </div>
                    </td>
                    <td class="col-info">
                        <h4>
                            Specialties:</h4>
                        <div class="content">
                            Oncology, Cardiology</div>
                        <h4>
                            Education:</h4>
                        <div class="content">
                            D.O., Kirksville College of Osteopathic Medicine, Kirksville, Missouri, 1973</div>
                        <h4>
                            Residency:</h4>
                        <div class="content">
                            Surgery and Ob/Gyn, Metropolitan Hospital, Philadelphia, PA, 1975<br />
                            Thomas Jefferson University Hospital, Philadelphia, PA, 1978</div>
                            <h4>
                            Fellowship:</h4>
                        <div class="content">
                            Jefferson Medical College, Philadelphia, PA</div>
                    </td>
                </tr>
            </table>
            <hr/>
            <table>
                <tr>
                    <td rowspan="2" class="col-photo">
                        
                    </td>
                    <td colspan="2" class="col-name">
                        <h3>Jill NoPhoto</h3>
                        <div class="doc-position">Certified Nurse Practitioner</div>
                    </td>
                </tr>
                <tr>
                    <td class="col-location">
                        <div class="location">
                            <h4>Drexel Ob/Gyn</h4>
                            <div>216 N. Broad Street<br />
                            Feinstein Building, 4th Floor<br />
                            Philadelphia, PA 19102</div>
                            <div><b>Phone:</b> 215-762-3600</div>
                        </div>                       
                    </td>
                    <td class="col-info">
                        <h4>
                            Specialties:</h4>
                        <div class="content">
                            Gynecology
                        </div>
                        <h4>
                            Education:</h4>
                        <div class="content">
                            MSN, CRNP, University of Rochester, NY</div>
                    </td>
                </tr>
            </table>
            <hr/>
        </div>
    </div>
</body>
</html>
