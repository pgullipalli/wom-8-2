			<section class="hero home grid">
				<div class="decoration"></div>
				<div class="inner">
					<!--<img src="/assets/images/hero-fpo.jpg" />-->
					<div class="mtslide">
					  <ul class="slides">
						<li>		 
						  <img src="/assets/images/homepage/demo-image-one.jpg" alt="image" />
						  <div class="core">
							  <div class="slide-content">
								<h1>
									<a href="#">
										Have a vision of the best possible 
										<span>YOU? So do we</span>
										<span class="arrow"></span>
									</a>
								</h1>
								<div class="copy-left">
									<p>Lorem Ipsum Dole salit.</p>
									<a href="#" class="more">Learn more ></a>
								</div>
							  </div>
						  </div>
						</li>
						<li> 
						  <img src="/assets/images/homepage/demo-image-two.jpg" alt="image" />
                        	<div class="core">
							  <div class="slide-content">
								<h1>
									<a href="#">
										Slide 2
										<span>Lorem Ipsum dolor</span>
										<span class="arrow"></span>
									</a>
								</h1>
								<div class="copy-left">
									<p>Lorem Ipsum Dole salit.</p>
									<a href="#" class="more">Learn more ></a>
								</div>
							  </div>
						  </div>
                         </li>
									
					  </ul>
					</div>		

					

					<div class="homepage-services-slider-mobile">
						<div class="owl-carousel-homepage">
							<ul>
								<li class="baby">
									<a href="#">
										<span class="icon"></span>
										Baby
									</a>
								</li>
								<li class="children-and-men">
									<a href="#">
										<span class="icon"></span>
										Children<br> &amp; Men
									</a>
								</li>
								<li class="gynecology">
									<a href="#">
										<span class="icon"></span>
										Gynecology
									</a>
								</li>
								<li class="wellness-and-nutrition">
									<a href="#">
										<span class="icon"></span>
										Wellness<br> &amp; Nutrition
									</a>
								</li>
								<li class="bone-and-joint">
									<a href="#">
										<span class="icon"></span>
										Bone <br>&amp;  Joint
									</a>
								</li>
								<li class="cancer">
									<a href="#">
										<span class="icon"></span>
										Cancer
									</a>
								</li>
								<li class="surgery">
									<a href="#">
										<span class="icon"></span>
										Surgery
									</a>
								</li>
							</ul>
						</div>					
					</div>
				</div>			
			</section> 