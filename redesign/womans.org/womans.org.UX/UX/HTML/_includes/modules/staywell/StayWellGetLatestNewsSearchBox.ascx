<!--==================== Module: StayWell: StayWell Get Latest News Search Box  =============================-->
<div class="module-sw-newssearch core-search twelve columns grid">
	
    <h4>Search News</h4>
    <div class="six columns">
		<label class="label">Keyword</label>
		<input type="text" placeholder="enter keyword(s)" class="textbox">
	</div>
	
	<div class="search-option-submit twelve columns">
		<input type="submit" value="Search" class="button">
	</div>

</div>
<!--==================== /Module: StayWell: StayWell Get Latest News Search Box  =============================-->
