<!--==================== Module: StayWell: StayWell Get Collection Content Filtered Display  =============================-->
<div class="module-sw-getcollectionfiltered">
	
    
            <div class="listing">
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=1&amp;contentID=1477&amp;language=en">Why Quit Smoking?</a></h4>
                <div class="listing-item-teaser">
                    You know you should quit smoking. But you just haven't gotten around to it yet. Here are some reasons to help you commit to quitting.
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=85&amp;contentID=P00242&amp;language=en">Smoking and Cardiovascular Disease</a></h4>
                <div class="listing-item-teaser">
                    Smokers not only have increased risk of lung disease, including lung cancer and emphysema, but also have increased risk of heart disease, stroke, and oral cancer.
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=1&amp;contentID=164&amp;language=en">Smoking and Gum Disease</a></h4>
                <div class="listing-item-teaser">
                    Do you have healthy gums? You may kiss them goodbye if you're a smoker.
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=85&amp;contentID=P00716&amp;language=en">Oral Cancer</a></h4>
                <div class="listing-item-teaser">
                    Ninety percent of oral cancer cases are caused by tobacco use. Oral cancer can affect the lips, teeth, gums, and lining of the mouth.
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=1&amp;contentID=154&amp;language=en">Smoking Adds Another Wrinkle to Aging</a></h4>
                <div class="listing-item-teaser">
                    Everybody knows smoking is bad for your health. Now here's something you may not know: Smoking is bad for your looks. It's true.
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=85&amp;contentID=P01239&amp;language=en">Smoking and Pregnancy</a></h4>
                <div class="listing-item-teaser">
                    Don't smoke during your pregnancy and limit how much time you spend in environments where there is secondhand smoke.
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=85&amp;contentID=P01331&amp;language=en">Smoking and Respiratory Diseases</a></h4>
                <div class="listing-item-teaser">
                    Smoking is directly responsible for the majority of lung cancer cases (87 percent), emphysema cases, and chronic bronchitis cases.
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=1&amp;contentID=2465&amp;language=en">Smoking and Asthma Don't Mix</a></h4>
                <div class="listing-item-teaser">
                    One of the major triggers for asthma attacks is cigarette smoke. Cigarette, pipe or cigar smoke is especially harmful to people with asthma because it damages the cells in the lungs that make the protective coating lining the bronchial tubes.
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=1&amp;contentID=4167&amp;language=en">The High Cost of Smoking</a></h4>
                <div class="listing-item-teaser">
                    When people consider the cost of smoking, they usually focus on the cost of the cigarettes alone. But that's only the first step.
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=1&amp;contentID=1427&amp;language=en">Up in Smoke: Cigars and Your Health</a></h4>
                <div class="listing-item-teaser">
                    Most people realize that cigarettes can cause lung cancer and heart disease. But many people erroneously believe that cigars aren't harmful.
                </div>
            </div>
        
            <div class="listing-item">
                <h4><a href="/health-library/content/?contentTypeID=85&amp;contentID=P00900&amp;language=en">Oral Cancer and Tobacco</a></h4>
                <div class="listing-item-teaser">
                    All tobacco products, from cigarettes to snuff, contain toxins, carcinogens, and nicotine, an addictive substance.
                </div>
            </div>
        
            </div>
        

</div>
<!--==================== /Module: StayWell: StayWell Get Collection Content Filtered Display  =============================-->
