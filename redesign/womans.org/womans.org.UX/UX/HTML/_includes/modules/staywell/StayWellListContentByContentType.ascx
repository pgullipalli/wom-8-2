<!--==================== Module: StayWell: StayWell List Content by Content Type  =============================-->
<div class="module-sw-contentbycontenttype">
	
    <div class="module-sw-paginationlist">
		
    <div class="module-pg-wrapper">
			
        
        <div class="module-pg-info">
				
            Viewing Page 1 of 1 | Showing Results 1 - 9 of 9
        
			</div>
        <div class="clear">
        </div>
    
		</div>

	</div>

        <div class="listing">
    
        <div class="listing-item">
            <h4><a href="/health-library/content/?contentTypeID=41&amp;contentID=BMICalc">Adult BMI Calculator</a></h4>
            <div class="listing-item-teaser"></div>
            Experts are increasingly urging people to know their BMI, a figure that takes into account not just weight but also height to indicate body fat.
        </div>
    
        <div class="listing-item">
            <h4><a href="/health-library/content/?contentTypeID=41&amp;contentID=AsthmaZoneCalc">Asthma Zone Calculator</a></h4>
            <div class="listing-item-teaser"></div>
            You need to know your personal best peak flow number to help control your asthma. This calculator will determine your Green, Yellow, and Red Zones, based on your personal best peak flow number. 
        </div>
    
        <div class="listing-item">
            <h4><a href="/health-library/content/?contentTypeID=41&amp;contentID=CalorieBurnCalc">Calorie Burn Rate Calculator</a></h4>
            <div class="listing-item-teaser"></div>
            The more active you are, the more calories you burn. Running or jogging, for instance, burns more calories than bowling.
        </div>
    
        <div class="listing-item">
            <h4><a href="/health-library/content/?contentTypeID=41&amp;contentID=ChildBMICalc">Children's BMI Calculator</a></h4>
            <div class="listing-item-teaser"></div>
            BMI, or body mass index, uses weight and height to calculate weight status for adults. BMI for children and teens also takes into account gender and age because healthy body fatness differs between boys and girls and changes as they grow. This BMI calculator will help you determine if your child is at a healthy weight.
        </div>
    
        <div class="listing-item">
            <h4><a href="/health-library/content/?contentTypeID=41&amp;contentID=CostOfDrinkingCalc">Cost of Drinking Calculator</a></h4>
            <div class="listing-item-teaser"></div>
            Drinking can be an expensive habit. While you may not notice a dollar here or two dollars there, consider how much you spend per week and per year on alcohol.
        </div>
    
        <div class="listing-item">
            <h4><a href="/health-library/content/?contentTypeID=41&amp;contentID=CostOfSmokingCalc">Cost of Smoking Calculator</a></h4>
            <div class="listing-item-teaser"></div>
            There isn't much good that can be said about smoking. Now, on a positive note, do you know how much money you can save if you quit smoking today?
        </div>
    
        <div class="listing-item">
            <h4><a href="/health-library/content/?contentTypeID=41&amp;contentID=DueDateCalc">Due Date Calculator</a></h4>
            <div class="listing-item-teaser"></div>
            It is important to know your estimated due date in order to help plan for the baby's birth.
        </div>
    
        <div class="listing-item">
            <h4><a href="/health-library/content/?contentTypeID=41&amp;contentID=OvulationCalc">Ovulation Date Calculator</a></h4>
            <div class="listing-item-teaser"></div>
            When a person is attempting to have a baby, fertile times become an important factor.  Using information about your cycles will help to predict your more fertile times and when you will ovulate.
        </div>
    
        <div class="listing-item">
            <h4><a href="/health-library/content/?contentTypeID=41&amp;contentID=TargetHeartCalc">Target Heart Rate Calculator</a></h4>
            <div class="listing-item-teaser"></div>
            Your target heart rate is the range at which sustained physical activity - running, cycling, swimming laps, or any other aerobic exercise - is considered safe and effective.
        </div>
    
        </div>
    
<div class="module-sw-paginationlist">
		
    <div class="module-pg-wrapper">
			
        
        <div class="module-pg-info">
				
            Viewing Page 1 of 1 | Showing Results 1 - 9 of 9
        
			</div>
        <div class="clear">
        </div>
    
		</div>

	</div>


</div>
<!--==================== /Module: StayWell: StayWell List Content by Content Type  =============================-->
