<!--==================== Module: StayWell: StayWell Get Content By Id  =============================-->
<div class="module-sw-contentbyid grid">
	
    
        <h1>About Arthritis and Other Rheumatic Diseases</h1>
        <h2>What is the difference between arthritis and other rheumatic diseases?</h2>
        <p>Arthritis, itself a group of more than 100 different diseases, is one category of rheumatic diseases. Rheumatic diseases may cause pain, stiffness, and swelling in the joints and other supporting body structures, such as muscles, tendons, ligaments, and bones. However, rheumatic diseases can affect other areas of the body, including internal organs. Some rheumatic diseases involve connective tissues (called connective tissue diseases), while others may be caused by an autoimmune disorder, which means the body's immune system attacks its own healthy cells and tissues.</p>
        <h2>Who treats arthritis and other rheumatic diseases?</h2>
        <p>Arthritis and other rheumatic diseases may be treated by your doctor and/or other medical specialists and health care providers. Several doctors from different medical specialties may be involved in the treatment at the same time. This multidisciplinary team approach is particularly important in managing the symptoms of a rheumatic disease, especially as many symptoms are chronic and change in severity over time.</p>
        <p>
          <img alt="Photo of elderly woman being examined by doctor" class="right" src="http://placehold.it/300x300">
        </p>
        <p>Some of the more common medical professionals involved in the treatment of arthritis and other rheumatic diseases may include the following:</p>
        <ul>
          <li>
            <p>
              <b>Primary care doctor. </b>A primary care doctor is one who has specialized education and training in general internal medicine, family practice, or another first-level-of-care area. Primary care doctors are those who provide patients with any or all of the following:</p>
            <ul>
              <li>
                <p>Routine health care (including annual physical examinations and immunizations)</p>
              </li>
              <li>
                <p>Treatment for acute medical conditions</p>
              </li>
              <li>
                <p>Initial care for conditions that may become more serious or chronic in nature</p>
              </li>
            </ul>
            <p>While your primary care doctor may treat and/or diagnose your disease, he or she may refer you to a specialist.</p>
          </li>
          <li>
            <p>
              <b>Rheumatologist. </b>A rheumatologist is a doctor who specializes in the treatment of arthritis and other rheumatic diseases that may affect joints, muscles, bones, skin, and other tissues. Most rheumatologists have a background in internal medicine or pediatrics and have received additional training in the field of rheumatology. Rheumatologists are specially trained to identify many types of rheumatic diseases in their earliest stages, including arthritis, many types of autoimmune diseases, musculoskeletal pain, disorders of the musculoskeletal system, and osteoporosis. In addition to four years of medical school and three years of specialized training in internal medicine or pediatrics, a rheumatologist has had an additional two or three years of specialized training in the field of rheumatology. A rheumatologist may also be board-certified by the American Board of Internal Medicine.</p>
          </li>
          <li>
            <p>
              <b>Orthopedic surgeon. </b>The doctor who specializes in orthopedic surgery is called an orthopedic surgeon, or sometimes, simply, an orthopedist. Orthopedists are educated in the workings of the musculoskeletal system, which includes (but is not limited to) diagnosing a condition or disorder, identifying and treating an injury, providing rehabilitation to an affected area or function, and establishing prevention protocol to inhibit further damage to a diseased area or component of the musculoskeletal system.</p>
            <p>The orthopedist may have completed up to 14 years of formal education. After becoming licensed to practice medicine, the orthopedic surgeon may become board-certified by passing both oral and written examinations given by the American Board of Orthopaedic Surgery.</p>
            <p>Many orthopedic surgeons choose to practice general orthopedics, while others specialize in certain areas of the body (for example, foot, hand, shoulder, spine, hip, or knee), or in a specialized area of orthopedic care (for example, sports medicine or trauma medicine). Some orthopedists may specialize in several areas and may collaborate with other specialists, such as neurosurgeons, rheumatologists, or physiatrists in caring for patients.</p>
          </li>
          <li>
            <p>
              <b>Physical therapist. </b>Physical therapy is the health profession that focuses on the neuromuscular, musculoskeletal, and cardiopulmonary systems of the human body as these systems relate to human motion and function.</p>
            <p>Physical therapists, or PTs, are very important members of the health care team. They evaluate and provide treatment for people with health problems resulting from injury, disease, or overuse of muscles or tendons.</p>
            <p>Physical therapists have an undergraduate degree in physical therapy, and many have a master's degree. In order to practice, all graduates must be licensed by their state by passing a national certification examination.</p>
            <p>Physical therapists may practice in a variety of settings, including:</p>
            <ul>
              <li>
                <p>Hospitals</p>
              </li>
              <li>
                <p>Rehabilitation centers</p>
              </li>
              <li>
                <p>Home health agencies</p>
              </li>
              <li>
                <p>Schools</p>
              </li>
              <li>
                <p>Sports facilities</p>
              </li>
              <li>
                <p>Community health centers</p>
              </li>
              <li>
                <p>Private practice</p>
              </li>
            </ul>
            <p>As related to arthritis and other rheumatic diseases, physical therapists provide comprehensive training that includes, but is not limited to, the following:</p>
            <ul>
              <li>
                <p>Functional mobility</p>
              </li>
              <li>
                <p>Balance and gait retraining</p>
              </li>
              <li>
                <p>Soft-tissue mobilization</p>
              </li>
              <li>
                <p>Body mechanics education</p>
              </li>
              <li>
                <p>Casting and splinting</p>
              </li>
              <li>
                <p>Wheelchair safety and management</p>
              </li>
              <li>
                <p>Neuromuscular re-education</p>
              </li>
              <li>
                <p>Exercise programming</p>
              </li>
              <li>
                <p>Family education and training</p>
              </li>
              <li>
                <p>Assistance with pain relief and management</p>
              </li>
              <li>
                <p>Instruction in safe ambulation</p>
              </li>
            </ul>
          </li>
          <li>
            <p>
              <b>Occupational therapist. </b>Occupational therapy is a health care profession that uses <i>occupation</i>, or purposeful activity, to help people with physical, developmental, or emotional disabilities lead independent, productive, and satisfying lives.<br><br>An occupational therapist often coordinates the following in the care for the individual with a debilitating condition, such as an orthopedic condition:</p>
            <ul>
              <li>
                <p>Evaluating children and adults with developmental, neuromuscular problems in order to plan treatment activities that will help them grow mentally, socially, and physically</p>
              </li>
              <li>
                <p>Assisting children and adults in learning how to carry out daily tasks</p>
              </li>
              <li>
                <p>Conducting group or individual treatment to help children and adults in a mental health center learn to cope with daily activities</p>
              </li>
              <li>
                <p>Recommending changes in layout and design of the home or school to allow children and adults with injuries or disabilities greater access and mobility</p>
              </li>
            </ul>
            <p>Occupational therapists work in a variety of different settings, including the following:</p>
            <ul>
              <li>
                <p>Hospitals</p>
              </li>
              <li>
                <p>Rehabilitation centers</p>
              </li>
              <li>
                <p>Schools</p>
              </li>
              <li>
                <p>Home care agencies</p>
              </li>
              <li>
                <p>Private practice</p>
              </li>
              <li>
                <p>Government agencies</p>
              </li>
            </ul>
          </li>
          <li>
            <p>
              <b>Podiatrist. </b>A podiatrist specializes in foot care and is licensed to prescribe medication and perform surgery. For example, people who suffer from arthritis in the feet may see a podiatrist for special supportive shoes.</p>
          </li>
          <li>
            <p>
              <b>Nurses. </b>Nurses specialized in the care of rheumatic diseases may assist your doctor in providing care. In addition, these nurses may help you learn about your treatment plan and can answer many of your questions.</p>
          </li>
        </ul>
        <h2>Who is affected by arthritis and other rheumatic diseases?</h2>
        <p>Arthritis and rheumatic diseases can affect anyone, at any age, or of any race. However, certain diseases are more common in certain populations, including the following:</p>
        <ul>
          <li>
            <p>Osteoarthritis is more common among the elderly.</p>
          </li>
          <li>
            <p>Seventy percent of people with rheumatoid arthritis are women.</p>
          </li>
          <li>
            <p>Fibromyalgia affects 2 percent of the U.S. population.</p>
          </li>
          <li>
            <p>Gout is more common in men.</p>
          </li>
          <li>
            <p>Scleroderma is more common in women.</p>
          </li>
          <li>
            <p>Lupus affects women about eight to 10 times as often as men.</p>
          </li>
          <li>
            <p>Ankylosing spondylitis is more common in men.</p>
          </li>
        </ul>
        <h2>What causes arthritis and other rheumatic diseases?</h2>
        <p>The cause of most types of rheumatic diseases remains unknown and, in many cases, varies depending on the type of rheumatic disease present. However, researchers believe that some or all of the following may play a role in the development or aggravation of one or more types of rheumatic diseases:</p>
        <ul>
          <li>
            <p>Genetics and family history (for example, inherited cartilage weakness)</p>
          </li>
          <li>
            <p>Lifestyle choices (such as being overweight)</p>
          </li>
          <li>
            <p>Trauma</p>
          </li>
          <li>
            <p>Infection</p>
          </li>
          <li>
            <p>Neurogenic disturbances</p>
          </li>
          <li>
            <p>Metabolic disturbances</p>
          </li>
          <li>
            <p>Excessive wear and tear and stress on a joint(s)</p>
          </li>
          <li>
            <p>Environmental triggers</p>
          </li>
          <li>
            <p>The influence of certain hormones on the body</p>
          </li>
        </ul>
        <h2>What are the symptoms of arthritis and other rheumatic diseases?</h2>
        <p>The following are the most common symptoms of arthritis and other rheumatic diseases. However, each individual may experience symptoms differently, and different types of rheumatic diseases present different symptoms. In general, symptoms may include:</p>
        <ul>
          <li>
            <p>Joint pain</p>
          </li>
          <li>
            <p>Swelling in the joint(s)</p>
          </li>
          <li>
            <p>Joint stiffness that lasts for at least one hour in the early morning</p>
          </li>
          <li>
            <p>Chronic pain or tenderness in the joint(s)</p>
          </li>
          <li>
            <p>Warmth and redness in the joint area</p>
          </li>
          <li>
            <p>Limited movement in the affected joint(s)</p>
          </li>
          <li>
            <p>Fatigue</p>
          </li>
        </ul>
        <p>The symptoms of arthritis and other rheumatic diseases may resemble other medical conditions and problems. Always consult your doctor for a diagnosis.</p>
      

</div>
<!--==================== /Module: StayWell: StayWell Get Content By Id  =============================-->
