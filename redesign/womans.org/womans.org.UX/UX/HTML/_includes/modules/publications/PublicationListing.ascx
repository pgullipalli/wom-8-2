<!--==================== Module: Publications: Publication Listing =============================-->
<div class="module-pb-results">	
    <div>
        <h3>Search results for <span class="module-search-keyword">pub</span></h3>
	</div>
    
    <div class="listing">
		<div class="listing-item results">
			<div class="two columns">
	           <img src="http://placehold.it/100x100" alt="photo_of_event_award_gala">
            </div>
            <div class="ten columns">
				<h4><a href="/publications/2012/11/test-publication/">Test Publication</a></h4>
				<div class="module-date">November 22, 2012</div>
	                    
				<div class="listing-item-teaser">	
					<p>Test Publication Content</p>       
				</div>
				<div class="listing-item-more-link">
					<a href="#">Read More</a>
				</div>
			</div>
        </div>       
	</div>
</div>
<!--==================== /Module: Publications: Publication Listing =============================-->