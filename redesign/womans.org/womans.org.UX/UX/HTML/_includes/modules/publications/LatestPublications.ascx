<!--==================== Module: Publications: Latest Publications =============================-->
<div class="module-pb-latest">
    <h3 class="module-heading">Latest Publications</h3>

    <div class="listing">
        <div class="listing-item grid">
            <div class="two columns">
	           <img src="http://placehold.it/100x100" alt="photo_of_event_award_gala">
            </div>
            <div class="ten columns">
                <h5>
                    <a href="/publications/2012/11/test-publication/">Test Publication</a>
                </h5>
                <div class="module-date">
                    November 22, 2012
    	         </div>
                
                <div class="listing-item-teaser">
                    <p>Test Publication Content</p>
    	        </div>
                <div class="listing-item-more-link">
                    <a href="/publications/2012/11/test-publication/">Read More</a>
    	        </div>
            </div>
        
		</div>
	</div>
</div>
<!--==================== /Module: Publications: Latest Publications =============================-->
