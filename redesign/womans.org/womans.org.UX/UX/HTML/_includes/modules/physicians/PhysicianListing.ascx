<!--==================== Module: Physician Directory: Physician Listing =============================-->
<div class="module-pd-results">
    <div class="module-pg-wrapper">
        <div class="module-pg-nav">
			<ul class="pagination">
				<li class="arrow no-link"><a href="">&laquo;</a></li>
				<li class="active"><a href="">1</a></li>
				<li><a href="">2</a></li>
				<li><a href="">3</a></li>
				<li><a href="">4</a></li>
				<li class="arrow"><a href="">&raquo;</a></li>
			</ul>			
		</div>
     	<div class="module-pg-info">Page 1 of 210 | Results 1 - 10 of 2093</div>
	</div>
	<!--THE RESULTS=================================================-->
    <div class="listing grid">
        <div class="listing-item">
            <div class="three columns">
                <div class="module-pd-thumbnail core-thumbnail">
                    <a href="#"><img src="http://placehold.it/148x148" alt="" /></a>
                </div>
                <div class="listing-item-more-link">
                    <a class="button read-more" href="#">View Profile</a>
                </div>
            </div>
            <div class="nine columns">
                <div class="module-pd-listing-info twelve columns">
				<h3><a href="#">First Last, <span>MD</span></a></h3>
                    
                    <div id="" class="module-pd-specialty-list">
						<div class="list">Family Medicine, Ophthalmology, Diagnostic Radiology</div>
                    </div>

                    <!--LIST OF OFFICES===============================================-->
                    <div class="module-pd-office-listing grid">
                        <div class="module-pd-offices">
                            <ul>
                                <li>Children's Internation Medical  Group of Denham Springs</li>
                                <li>West Feliciana Parish Hospital Physicians Clinic</li>
                            </ul>
					    </div>
                    </div>

                    <div class="new-patients">
        				<span></span>
        				<p>Jada Armstrong is Currently Accepting New Patients</p>
        			</div>					
                </div>
			
            </div>
        </div>
				
                
        <div class="listing-item">
            <div class="three columns">
                <div class="module-pd-thumbnail core-thumbnail">
                    <a href="#"><img src="http://placehold.it/148x148" alt="" /></a>
                </div>
                <div class="listing-item-more-link">
                    <a class="button read-more" href="#">View Profile</a>
                </div>
            </div>
            <div class="nine columns">
                <div class="module-pd-listing-info twelve columns">
				<h3><a href="#">First Last, <span>MD</span></a></h3>
                    <div id="Div1" class="module-pd-specialty-list">
						<div class="list">Family Medicine, Ophthalmology, Diagnostic Radiology</div>
                    </div>
                    
                    <!--LIST OF OFFICES===============================================-->
                    <div class="module-pd-office-listing grid">
                        <div class="module-pd-offices">
                            <ul>
                                <li>Children's Internation Medical  Group of Denham Springs</li>
                                <li>West Feliciana Parish Hospital Physicians Clinic</li>
                            </ul>
					    </div>
                    </div>

                    <div class="new-patients">
        				<span></span>
        				<p>Jada Armstrong is Currently Accepting New Patients</p>
        			</div>					
                </div>
            </div>
        </div>

    </div>
</div>
<!--==================== /Module: Physician Directory: Physician Listing =============================-->
