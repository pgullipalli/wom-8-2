<!--==================== Module: Physician Directory: Physician Detail =============================-->
<div class="module-pd-profile core-profile">
  
    <div class="module-pd-profile-top grid">

        <div class="new-patients">
        	<span></span>
        	<p>Jada L. Armstrong, MD is Currently Accepting New Patients</p>
        </div>
        
        <h2>Bio Introduction ipsum dolor sit amet, consectetur adipiscing elit. Praesent laoreet odio ut sollicitudin pharetra. Vestibulum ut tellus eget ac sit </h2>
		
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent laoreet odio ut sollicitudin pharetra. Vestibulum ut tellus eget risus tempor dictum ac sit amet ipsum. Nunc nec ante euismod, congue velit quis, blandit lorem. Suspendisse ac mi hendrerit, auctor enim at, tincidunt massa. Curabitur eu magna a orci placerat cursus et dapibus nunc. Pellentesque rhoncus ac eros ac cursus. Duis iaculis egestas mi, sit amet vehicula tortor dapibus et. Cras in bibendum nisl.</p>

		<p>Sed pulvinar nunc vitae dignissim hendrerit. Suspendisse iaculis, ipsum nec viverra gravida, lorem erat imperdiet arcu, posuere tristique nisl diam in mauris. Nunc in aliquam sem, sit amet eleifend nunc. Morbi interdum elit et eros molestie ullamcorper. Vestibulum luctus mattis eros at laoreet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tempor, est at sollicitudin lacinia, ante tellus accumsan nisl, eget ultrices libero dolor eu ante.</p>
	</div>
	
	
   
	<!--------==========================TABS JQUERY==============================-->
	<div class="module-pd-tabs responsive-tabs">
			<h2>Overview</h2>
            <div id="pnlOverview">
                <div class="module-pd-tab-label">
                 <h2>Specialties</h2>
                </div>
                <ul><li>Allergy & Immunology</li></ul>
            
            <div class="module-pd-tab-label">
            <h2>Physician Groups</h2>
            </div>
            <ul>
                <li>Children's Internation Medical  Group of Denham Springs - (225)-922-5224</li>
                <li>West Feliciana Parish Hospital Physicians Clinic - (225)-922-5224</li>
            </ul>
            <div class="module-pd-tab-label">
            <h2>Website</h2>
            </div>
            <p><a href="#">jadaarmstrong.com</a></p>
        
            <div class="module-pd-tab-label">
             <h2>Languages Spoken</h2>
            </div>
             <ul><li>English</li><li>French</li></ul>
         </div>

            
            <h2>Credentials</h2>
			<div id="pnlCredentials">
				<div class="module-pd-tab-label">
					<h2>Certifications</h2>
				</div>
				<div><ul><li>Pediatrics: American Board of Pediatrics</li></ul></div>
                
                <div class="module-pd-tab-label">
					<h2>Licenses</h2>
				</div>
				<div><ul><li>License 1</li><li>License 2</li></ul></div>
                
                <div class="module-pd-tab-label">
					<h2>Position Titles</h2>
				</div>
				<div><ul><li>Title 1</li><li>Title 2</li></ul></div>

				<div class="module-pd-tab-label">
					<h2>Expertise</h2>
				</div>
				<div><ul><li>Expertise 1</li><li>Expertise 2</li></ul></div>

			</div>
			
			<h2>Education</h2>
			<div id="pnlEducation">
			    
                <div class="module-pd-tab-label">
					<h2>Medical School</h2>
				</div>
				<div><p>Medical School Name, YEAR</p></div>
                
                <div class="module-pd-tab-label">
					<h2>Internship</h2>
				</div>
				<div><p>Institution Name, YEAR</p></div>
                
                <div class="module-pd-tab-label">
					<h2>Residency</h2>
				</div>
				<div><p>Institution Name, YEAR</p></div>
                
                <div class="module-pd-tab-label">
					<h2>Fellowship</h2>
				</div>
				<div><p>Institution Name, YEAR</p></div>

			</div>


			<h2>Achievements</h2>
			<div>
                <div class="module-pd-tab-label">
					<h2>Awards/Recognition</h2>
				</div>
				<div><ul><li>Award 1</li><li>Award 2</li></ul></div>
                
                <div class="module-pd-tab-label">
					<h2>Publications</h2>
				</div>
				<div><ul><li>Publication 1</li><li>Publication 2</li></ul></div>
                
                <div class="module-pd-tab-label">
					<h2>Community</h2>
				</div>
				<div><ul><li>Community 1 1</li><li>Community 2</li></ul></div>
			</div>

	</div>
    <!--------==========================TABS JQUERY==============================-->
</div>
<!--==================== /Module: Physician Directory: Physician Detail =============================-->
