<!--==================== Module: Greeting Cards: Greeting Card Form =============================-->
<div class="scfForm"><input type="hidden" value="form_C8D25433B4EE456DA61A002261D30730"><h1 class="scfTitleBorder">

</h1>
<div class="scfIntroBorder">

</div>
<div class="scfValidationSummary" style="display:none;">

</div>
<div class="scfSubmitSummary" style="color:Red;">

</div>
<div>
	<div class="scfSectionBorder">
		<fieldset class="scfSectionBorderAsFieldSet">
			<legend class="scfSectionLegend">
				Image
			</legend><div class="scfSectionContent">
				<div class=" fieldid.%7bB4E2B89E-CF3C-4B8C-89FA-5B5F3DA28BA9%7d name.Image">
					<img src="/ux/html/_includes/images/filler-profile.jpg" alt="baby boy" style="height:360px;width:450px;"><span style="display:none;"></span>
				</div>
			</div>
		</fieldset>
	</div><div>
		<fieldset class="scfSectionBorderAsFieldSet">
			<legend class="scfSectionLegend">
				Contact Information
			</legend><div class="scfSectionContent">
				<div class="scfSingleLineTextBorder fieldid.%7b8B86A1D4-A368-42E8-A490-699B15CFD843%7d name.First+Name">
					<label class="scfSingleLineTextLabel">First Name</label><div class="scfSingleLineGeneralPanel">
						<input type="text" maxlength="256" class="scfSingleLineTextBox"><span class="scfSingleLineTextUsefulInfo" style="display:none;"></span><span class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7b8B86A1D4-A368-42E8-A490-699B15CFD843%7d inner.1" style="display:none;">First Name must have at least 0 and no more than 256 characters.</span><span class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b8B86A1D4-A368-42E8-A490-699B15CFD843%7d inner.1" style="display:none;">The value of the First Name field is not valid.</span>
					</div><span class="scfRequired">*</span>
				</div><div class="scfSingleLineTextBorder fieldid.%7bDC1BA4F0-CA50-4FF3-A005-6626A6897347%7d name.Last+Name">
					<label class="scfSingleLineTextLabel">Last Name</label><div class="scfSingleLineGeneralPanel">
						<input type="text" maxlength="256" class="scfSingleLineTextBox"><span class="scfSingleLineTextUsefulInfo" style="display:none;"></span><span class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bDC1BA4F0-CA50-4FF3-A005-6626A6897347%7d inner.1" style="display:none;">Last Name must have at least 0 and no more than 256 characters.</span><span class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bDC1BA4F0-CA50-4FF3-A005-6626A6897347%7d inner.1" style="display:none;">The value of the Last Name field is not valid.</span>
					</div><span class="scfRequired">*</span>
				</div><div class="scfNumberBorder fieldid.%7b5EBF5FFD-B36D-48C0-93B0-CBB44A8FBA04%7d name.Room+Number">
					<label class="scfNumberLabel">Room Number</label><div class="scfNumberGeneralPanel">
						<input type="text" class="scfNumberTextBox"><span class="scfNumberUsefulInfo" style="display:none;"></span><span class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b5EBF5FFD-B36D-48C0-93B0-CBB44A8FBA04%7d inner.1" style="display:none;">Enter a number.</span><span class="minimum.0 maximum.2147483647 scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7b5EBF5FFD-B36D-48C0-93B0-CBB44A8FBA04%7d inner.1" style="display:none;">The number in Room Number must be at least 0 and no more than 2147483647.</span>
					</div><span class="scfRequired">*</span>
				</div>
			</div>
		</fieldset>
	</div><div>
		<fieldset class="scfSectionBorderAsFieldSet">
			<legend class="scfSectionLegend">
				Sender Information
			</legend><div class="scfSectionContent">
				<div class="scfTelephoneBorder fieldid.%7b6F13785C-7D74-44E7-A266-6EC6DEE5073B%7d name.Phone">
					<label class="scfTelephoneLabel">Phone</label><div class="scfTelephoneGeneralPanel">
						<input type="text" class="scfTelephoneTextBox"><span class="scfTelephoneUsefulInfo" style="display:none;"></span><span class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b6F13785C-7D74-44E7-A266-6EC6DEE5073B%7d inner.1" style="display:none;">Enter a valid telephone number.</span>
					</div>
				</div><div class="scfSingleLineTextBorder fieldid.%7bCAE5BECC-C14B-4403-BEAA-1024247E34AB%7d name.Sender+First+Name">
					<label class="scfSingleLineTextLabel">Sender First Name</label><div class="scfSingleLineGeneralPanel">
						<input type="text" maxlength="256" class="scfSingleLineTextBox"><span class="scfSingleLineTextUsefulInfo" style="display:none;"></span><span class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bCAE5BECC-C14B-4403-BEAA-1024247E34AB%7d inner.1" style="display:none;">Sender First Name must have at least 0 and no more than 256 characters.</span><span class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bCAE5BECC-C14B-4403-BEAA-1024247E34AB%7d inner.1" style="display:none;">The value of the Sender First Name field is not valid.</span>
					</div><span class="scfRequired">*</span>
				</div><div class="scfSingleLineTextBorder fieldid.%7b570CE8E4-0156-4680-87EA-2421CA3E7F07%7d name.Sender+Last+Name">
					<label class="scfSingleLineTextLabel">Sender Last Name</label><div class="scfSingleLineGeneralPanel">
						<input type="text" maxlength="256" class="scfSingleLineTextBox"><span class="scfSingleLineTextUsefulInfo" style="display:none;"></span><span class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7b570CE8E4-0156-4680-87EA-2421CA3E7F07%7d inner.1" style="display:none;">Sender Last Name must have at least 0 and no more than 256 characters.</span><span class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b570CE8E4-0156-4680-87EA-2421CA3E7F07%7d inner.1" style="display:none;">The value of the Sender Last Name field is not valid.</span>
					</div><span class="scfRequired">*</span>
				</div><div class="scfMultipleLineTextBorder fieldid.%7bA57A80D3-FFEA-4990-9D05-29BCF9684B4E%7d name.Message">
					<label class="scfMultipleLineTextLabel">Message</label><div class="scfMultipleLineGeneralPanel">
						<textarea rows="4" cols="20" class="scfMultipleLineTextBox"></textarea><span class="scfMultipleLineTextUsefulInfo" style="display:none;"></span><span class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bA57A80D3-FFEA-4990-9D05-29BCF9684B4E%7d inner.1" style="display:none;">Message must have at least 0 and no more than 1000 characters.</span><span class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bA57A80D3-FFEA-4990-9D05-29BCF9684B4E%7d inner.1" style="display:none;">The value of the Message field is not valid.</span>
					</div>
				</div><div class="scfDropListBorder fieldid.%7b4A92C160-65E8-45CB-9E18-80607C61ABE6%7d name.Send+To">
					<label class="scfDropListLabel">Send To</label><div class="scfDropListGeneralPanel">
						<select class="ddlSendTo styled" onchange="hideShowDropDownTextBox_SendTo();">
							<option value="pwest@medtouch.com;pkwest1@live.com">Hospital Greeting Department</option>
							<option value="">Personal Email Recipient</option>

						</select><br><span class="lblSendTo" style="display:none;"></span><textarea rows="2" cols="20" class="txtSendTo" style="display:none;"></textarea><span class="scfDropListUsefulInfo" style="display:none;"></span>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="scfFooterBorder">

</div>
<div class="scfSubmitButtonBorder">
	<input type="submit" value="Submit" onclick="$scw.webform.lastSubmit = this.id;WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;main_0$form_C8D25433B4EE456DA61A002261D30730$form_C8D25433B4EE456DA61A002261D30730_submit&quot;, &quot;&quot;, true, &quot;form_C8D25433B4EE456DA61A002261D30730_submit&quot;, &quot;&quot;, false, false));$scw.webform.validators.setFocusToFirstNotValid('form_C8D25433B4EE456DA61A002261D30730_submit')">
</div>
 
<input type="hidden"></div>

<!--==================== /Module: Greeting Cards: Greeting Card Form =============================-->