<!--==================== Module: Greeting Cards: Preview =============================-->

        <div class="module-gc-greetingcardpreview">
	
            
                    <ul>
                
                        <li>
                            
                            <img src="/ux/html/_includes/images/filler-profile.jpg" width="450" height="360" alt="birthday princess" />
                        </li>
                
                        <li>
                            <span>First Name</span>:
                            Contact First
                        </li>
                
                        <li>
                            <span>Last Name</span>:
                            Contact Last
                        </li>
                
                        <li>
                            <span>Room Number</span>:
                            1243
                        </li>
                
                        <li>
                            <span>Phone</span>:
                            6176218670
                        </li>
                
                        <li>
                            <span>Sender First Name</span>:
                            Dan
                        </li>
                
                        <li>
                            <span>Sender Last Name</span>:
                            Persson
                        </li>
                
                        <li>
                            <span>Message</span>:
                            This is my message
                        </li>
                
                        
                
                    </ul>
                
            <input type="submit" value="Submit" />
        
</div>
    
<!--==================== /Module: Greeting Cards: Preview =============================-->
