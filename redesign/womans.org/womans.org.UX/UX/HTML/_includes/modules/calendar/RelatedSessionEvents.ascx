<!--==================== Module: Calendar: Related Session Events =============================-->
<div class="module-ce-session-related module-ce-related collapse-for-mobile">
   <div class="grid reg-callout">
		<h3 class="module-heading">Related Classes &amp; Events</h3>		
		<div class="listing-wrap twelve columns">
			<ul class="core-list">
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Cesarean Birth Class</a></h5>
					<div class="list-item-image">
						<a href="#"><img src="http://placehold.it/100x100" alt="photo_of_event_award_gala"></a>
					</div>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>For couples who are expecting to deliver by c-section.</p>
						</div>
                        <div class="module-date">
                            May 26, 2014 10:00 AM - 11:00 AM 
                        </div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
							<span>&nbsp;|&nbsp;</span>
							<a href="#" class="register">Register</a>
						</div>
					</div>
				</li>
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Breastfeeding Class</a></h5>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>This class will teach you the basics of breastfeeding. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum posuere eros et.</p>
						</div>
                        <div class="module-date">
                            May 26, 2014 10:00 AM - 11:00 AM 
                        </div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
							<span>&nbsp;|&nbsp;</span>
							<a href="#" class="register">Register</a>
						</div>
					</div>
				</li>
			</ul>
		</div>
		<div class="module-ce-view-all core-view-all twelve columns">
			<a href="#" class="read-more read-arrow">View All</a>
		</div>
	</div>  			
</div>
<!--==================== /Module: Calendar: Related Session Events =============================-->
