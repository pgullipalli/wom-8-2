<!--==================== Module: Calendar: Event Topic Detail =============================-->
<div class="module-ce-topic-profile grid">
    <div class="system-message success nine columns centered">
        Class has been added to your shopping cart.
    </div>
    <div class="system-message error nine columns centered">
        There was an error adding the class to your shopping cart.
    </div>
    <div class="twelve columns">
        <h1>Breastfeeding Class</h1>
    </div>
    <div class="twelve columns">
        <img src="http://placehold.it/150x150" alt="" class="right">
        <p>
            This class will teach you the basics of breastfeeding.
        </p>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum posuere eros et aliquet. Fusce ligula enim, accumsan a elementum quis, tempus quis neque. Nunc sollicitudin luctus aliquet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquet eleifend ullamcorper. Sed sit amet orci id enim venenatis dignissim. Curabitur posuere mattis ligula quis consequat.
        </p>
        <p>
            Phasellus aliquet faucibus dolor, id porttitor metus semper eget. Donec rhoncus ullamcorper eros, nec placerat justo posuere vel. Sed ut enim quam, sed sollicitudin tellus. Aenean id arcu non nisl dictum venenatis. Suspendisse at augue augue, sit amet commodo purus. Duis mollis, eros a porta elementum, augue ante rhoncus urna, et scelerisque risus mi eget risus. Pellentesque est ligula, mattis id pharetra in, porttitor nec dolor. Donec non iaculis mauris. Vestibulum dapibus lectus vel erat tincidunt elementum. Nulla lobortis vehicula lorem, a convallis mi pharetra eget. Suspendisse vel felis risus, sit amet tincidunt eros.
        </p>
    </div>
   
    <div class="module-ce-reviews twelve columns">
        <h3>Class Reviews:</h3>
        <h5 class="review-title">Review Title</h5>
        <p class="review-text">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum non mi a tortor malesuada bibendum eu ut magna. Integer non nisi quis odio tincidunt lobortis. Proin felis augue, pellentesque vel lectus a, consectetur eleifend nulla. Suspendisse fringilla pharetra mauris tempor consectetur. Suspendisse a consequat urna. Aliquam ut scelerisque velit. Morbi eget nibh leo. Quisque aliquet justo neque, vel posuere turpis ultrices sed. Suspendisse condimentum lobortis enim quis placerat. Cras dignissim risus non erat molestie, quis fringilla dolor imperdiet. Nunc urna sapien, dignissim vitae consectetur non, mollis eget ante. Vestibulum a leo vel diam euismod pharetra. Vestibulum molestie hendrerit rhoncus.
        </p>
        <h5 class="review-title">Review Title</h5>
        <p class="review-text">
            Proin vel augue ut metus porttitor commodo. Curabitur sit amet metus pellentesque, laoreet velit at, tempus augue. Vivamus non vehicula est, eget dictum lacus. Fusce quis nunc dapibus, ornare lorem et, viverra dui. Pellentesque sed nibh convallis, auctor libero nec, imperdiet quam. Donec lectus ligula, sagittis ut tristique ac, porta at est. Etiam varius, magna vitae dignissim aliquam, dolor enim rhoncus justo, sit amet rutrum justo enim id ipsum. Etiam accumsan, sapien quis sollicitudin aliquam, sapien nulla suscipit tellus, ac vehicula urna metus vel elit. Etiam scelerisque est a dictum accumsan. Morbi consequat consectetur quam quis tempus. Sed luctus purus et condimentum sodales.<br>
            <br>
            Donec sed pretium leo, eu viverra diam. Nam id sapien hendrerit, tristique diam in, sagittis lectus. Fusce elit massa, hendrerit vel dictum vitae, aliquam in eros. Ut sed lacus ante. Nulla commodo, risus vitae porttitor rutrum, justo tortor volutpat velit, ac tempus est risus a tellus. Nam sodales enim quam, eu sagittis turpis vulputate nec. Sed fermentum faucibus ligula et pretium. Nullam et commodo tortor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum aliquet leo interdum nisi porta, vitae tempor lacus vulputate. Quisque viverra mauris eu lectus sodales, sed condimentum purus blandit. Mauris vitae arcu massa. Etiam id imperdiet leo, ut auctor augue. Donec sit amet ullamcorper sem. Donec non nibh arcu.
        </p>
        <h5 class="review-title">Review Title</h5>
        <p class="review-text">
            Mauris pulvinar faucibus diam vel egestas. Donec sit amet feugiat tortor. Suspendisse blandit volutpat interdum. Donec aliquam, magna et interdum bibendum, dui lacus varius nisl, vel interdum nisl justo quis arcu. Sed vel ornare justo, vitae aliquet nisi. Mauris vitae convallis orci. Praesent condimentum imperdiet pulvinar. Mauris cursus lobortis sapien sit amet auctor. Curabitur euismod egestas suscipit. Proin bibendum nibh feugiat mollis fermentum. Proin cursus placerat arcu, ac elementum dui commodo vitae. Curabitur molestie suscipit venenatis. Praesent sed velit quis purus condimentum sodales. Curabitur sed lorem ac lacus rhoncus iaculis quis quis enim. Donec dictum iaculis mi, sit amet viverra tortor eleifend sed.
        </p>
        <h5 class="review-title">Review Title</h5>
        <p class="review-text">
            Nam dignissim leo enim. Vivamus congue tempor suscipit. Suspendisse potenti. In lacinia convallis tristique. Phasellus lacinia neque at massa bibendum mollis. Nunc vehicula volutpat massa et tincidunt. Curabitur vel nisl vel erat viverra luctus ac a augue. Curabitur quam eros, consectetur ac sem sed, congue tristique est. Cras elit sem, interdum at volutpat vel, consequat non mauris. Vivamus mauris erat, iaculis commodo risus at, pulvinar hendrerit massa.
        </p>
    </div>
    <div class="module-ce-register twelve columns">
        <h3>Registration Instructions:</h3>
        <p>Registration Instructions</p>
    </div>
    <div class="module-ce-fee twelve columns grid">
        <h3>Fee:</h3>
        <div class="fee-type selectbox">
            <select class="selectboxdiv">
                <option value="fee1">Test Fee 1: $25.00 </option>
                <option selected="selected" value="fee2">Test Fee Default: $50.00 </option>
            </select>
			<div class="out"></div>
        </div>
        <div class="fixed-fee">$50.00</div>
    </div>

     <div class="module-ce-contact twelve columns">
        <h3>Contact:</h3>
        <div>Event Contact Name </div>
        <div>Event Phone</div>
        <div><a href="mailto:pwest@medtouch.com">Email</a></div>
    </div>
    <div class="module-ce-offerdates listing twelve columns grid">
        <h3>Dates Offered:</h3>
        <div class="module-ce-offerdates-item list-item twelve columns grid highlight">
            <div class="event-lpanel six columns grid">
                <div class="module-date twelve columns">
                    <span class="date">5/20/2014 6:30:00 AM</span> 
                    6:30 AM - 8:30 AM
                </div>
                <div class="event-attendinginfo twelve columns">Attending dates information</div>
                <div class="cal-location-name twelve columns">
                    <div><a href="/contact-us">Cedar Rapids</a></div>
                    <div>800 1st Avenue NE<br>Suite 001<br>Cedar Rapids, IA 52402</div>
                    <div><a href="http://maps.google.com/?q=800+1st+Avenue+NE%2c+Suite+001%2c+Cedar+Rapids%2c+IA+52402" target="_blank">Maps &amp; Direction</a></div>
                </div>
                <!-- 5/19 - UX - Added module-ce-addthisevent class and moved into event-leftpanel <div> -->
                <div class="module-ce-addthisevent twelve columns">
                    <a href="http://example.com/link-to-your-event" title="" class="addthisevent">
                        Add To My Calendar
                        <span class="_start" style="display: none;">5/20/2014 6:30:00 AM</span>
                        <span class="_end" style="display: none;">5/20/2014 8:30:00 AM</span>
                        <span class="_zonecode" style="display: none;"></span>
                        <span class="_summary" style="display: none;">Sample Event Summary</span>
                        <span class="_description" style="display: none;">Sample Event Description</span>
                        <span class="_location" style="display: none;">Cedar Rapids, 800 1st Avenue NE Suite 001 Cedar Rapids, IA 52402</span>
                        <span class="_organizer" style="display: none;">Organizer Name</span>
                        <span class="_organizer_email" style="display: none;">organizer@medtouch.com</span>
                        <span class="_all_day_event" style="display: none;">false</span>
                        <span class="_date_format" style="display: none;">MM/DD/YYYY</span>
                    </a>
                </div>
            </div>
            <div class="event-rpanel six columns grid">
                <div class="event-register twelve columns">
                    <div class="event-button"><a class="button pink">Add to Cart</a></div>
                    <div class="event-availability">Seats Available: 53</div>
                </div>
            </div>
        </div>
        <div class="module-ce-offerdates-item list-item twelve columns grid">
            <div class="event-lpanel six columns grid">
                <div class="module-date twelve columns">
                    <span class="date">5/21/2014 6:30:00 AM</span> 
                    6:30 AM - 8:30 AM
                </div>
                <div class="event-attendinginfo twelve columns">Attending dates information</div>
                <div class="cal-location-name twelve columns">
                    <div><a href="/contact-us">Cedar Rapids</a></div>
                    <div>800 1st Avenue NE<br>Suite 001<br>Cedar Rapids, IA 52402</div>
                    <div><a href="http://maps.google.com/?q=800+1st+Avenue+NE%2c+Suite+001%2c+Cedar+Rapids%2c+IA+52402" target="_blank">Maps &amp; Direction</a></div>
                </div>
                <!-- 5/19 - UX - Added module-ce-addthisevent class and moved into event-leftpanel <div> -->
                <div class="module-ce-addthisevent twelve columns">
                    <a href="http://example.com/link-to-your-event" title="" class="addthisevent">
                        Add To My Calendar
                        <span class="_start" style="display: none;">5/21/2014 6:30:00 AM</span>
                        <span class="_end" style="display: none;">5/21/2014 8:30:00 AM</span>
                        <span class="_zonecode" style="display: none;"></span>
                        <span class="_summary" style="display: none;">Sample Event Summary</span>
                        <span class="_description" style="display: none;">Sample Event Description</span>
                        <span class="_location" style="display: none;">Cedar Rapids, 800 1st Avenue NE Suite 001 Cedar Rapids, IA 52402</span>
                        <span class="_organizer" style="display: none;">Organizer Name</span>
                        <span class="_organizer_email" style="display: none;">organizer@medtouch.com</span>
                        <span class="_all_day_event" style="display: none;">false</span>
                        <span class="_date_format" style="display: none;">MM/DD/YYYY</span>
                    </a>
                </div>
            </div>
            <div class="event-rpanel six columns grid">
                <div class="event-register twelve columns">
                    <div class="event-button"><a class="button pink">Add to Cart</a></div>
                    <div class="event-availability">Seats Available: 22</div>
                </div>
                   
            </div>
        </div>
        <div class="module-ce-offerdates-item list-item twelve columns grid">
            <div class="event-lpanel six columns grid">
                <div class="module-date twelve columns">
                    <span class="date">5/27/2014 6:30:00 AM</span> 
                    6:30 AM - 8:30 AM
                </div>
                <div class="event-attendinginfo twelve columns">Attending dates information</div>
                <div class="cal-location-name twelve columns">
                    <div><a href="/contact-us">Cedar Rapids</a></div>
                    <div>800 1st Avenue NE<br>Suite 001<br>Cedar Rapids, IA 52402</div>
                    <div><a href="http://maps.google.com/?q=800+1st+Avenue+NE%2c+Suite+001%2c+Cedar+Rapids%2c+IA+52402" target="_blank">Maps &amp; Direction</a></div>
                </div>
                <!-- 5/19 - UX - Added module-ce-addthisevent class and moved into event-leftpanel <div> -->
                <div class="module-ce-addthisevent twelve columns">
                    <a href="http://example.com/link-to-your-event" title="" class="addthisevent">
                        Add To My Calendar
                        <span class="_start" style="display: none;">5/27/2014 6:30:00 AM</span>
                        <span class="_end" style="display: none;">5/27/2014 8:30:00 AM</span>
                        <span class="_zonecode" style="display: none;"></span>
                        <span class="_summary" style="display: none;">Sample Event Summary</span>
                        <span class="_description" style="display: none;">Sample Event Description</span>
                        <span class="_location" style="display: none;">Cedar Rapids, 800 1st Avenue NE Suite 001 Cedar Rapids, IA 52402</span>
                        <span class="_organizer" style="display: none;">Organizer Name</span>
                        <span class="_organizer_email" style="display: none;">organizer@medtouch.com</span>
                        <span class="_all_day_event" style="display: none;">false</span>
                        <span class="_date_format" style="display: none;">MM/DD/YYYY</span>
                    </a>
                </div>
            </div>
            <div class="event-rpanel six columns grid">
                <div class="event-register twelve columns">
                    <div class="event-button"><a class="button pink">Add to Cart</a></div>
                    <div class="event-availability">Seats Available: 3</div>
                </div>
                <div class="module-ce-instructors-listing twelve columns grid">
                        <h5 class="twelve columns">Instructor(s):</h5>
                    <div class="toggle-hide-show twelve columns">
                        <a href="javascript:void(0);"><span>+</span>&nbsp;Jonathan Lipkowicz</a>
                    </div>
                    <div class="instructor-item hide-onload twelve columns" style="display: none;">
                        <div class="thumbnail"><img src="http://placehold.it/50x50" alt="Jonathan" width="50" height="50"></div>
                        <div class="results">
                            <div>Professor Emeritus, Harvard University</div>
                            <div>This is lots of descriptive info about an instructor.</div>
                            <div>360-111-2222</div>
                            <div><a href="mailto:blah@blah.com">Email</a></div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
        <div class="module-ce-offerdates-item list-item twelve columns grid">
            <div class="event-lpanel six columns grid">
                <div class="module-date twelve columns">
                    <span class="date">5/30/2014 6:30:00 AM</span> 
                    6:30 AM - 8:30 AM
                </div>
                <div class="event-attendinginfo twelve columns">Attending dates information</div>
                <div class="cal-location-name twelve columns">
                    <div><a href="/contact-us">Cedar Rapids</a></div>
                    <div>800 1st Avenue NE<br>Suite 001<br>Cedar Rapids, IA 52402</div>
                    <div><a href="http://maps.google.com/?q=800+1st+Avenue+NE%2c+Suite+001%2c+Cedar+Rapids%2c+IA+52402" target="_blank">Maps &amp; Direction</a></div>
                </div>
                <!-- 5/19 - UX - Added module-ce-addthisevent class and moved into event-leftpanel <div> -->
                <div class="module-ce-addthisevent twelve columns">
                    <a href="http://example.com/link-to-your-event" title="" class="addthisevent">
                        Add To My Calendar
                        <span class="_start" style="display: none;">5/30/2014 6:30:00 AM</span>
                        <span class="_end" style="display: none;">5/30/2014 8:30:00 AM</span>
                        <span class="_zonecode" style="display: none;"></span>
                        <span class="_summary" style="display: none;">Sample Event Summary</span>
                        <span class="_description" style="display: none;">Sample Event Description</span>
                        <span class="_location" style="display: none;">Cedar Rapids, 800 1st Avenue NE Suite 001 Cedar Rapids, IA 52402</span>
                        <span class="_organizer" style="display: none;">Organizer Name</span>
                        <span class="_organizer_email" style="display: none;">organizer@medtouch.com</span>
                        <span class="_all_day_event" style="display: none;">false</span>
                        <span class="_date_format" style="display: none;">MM/DD/YYYY</span>
                    </a>
                </div>
            </div>
            <div class="event-rpanel six columns grid">
                <div class="event-register twelve columns">
                    <div class="event-button"><a class="button pink">Add to Cart</a></div>
                    <div class="event-availability">Seats Available: 53</div>
                </div>
                <div class="module-ce-instructors-listing twelve columns grid">
                    <h5 class="twelve columns">Instructor(s):</h5>
                    <div class="toggle-hide-show twelve columns">
                        <a href="javascript:void(0);"><span>+</span>&nbsp;Albus Dumbledore</a>
                    </div>
                    <div class="instructor-item hide-onload twelve columns" style="display: none;">
                        <div class="thumbnail"><img src="http://placehold.it/50x50" alt="Dumbledore" width="46" height="50"></div>
                        <div class="results">
                            <div>Professor, Hogwarts University</div>
                        </div>
                    </div>
                    <div class="toggle-hide-show twelve columns">
                        <a href="javascript:void(0);"><span>+</span>&nbsp;Jane Goodall</a>
                    </div>
                    <div class="instructor-item hide-onload twelve columns" style="display: none;">
                        <div class="thumbnail"><img src="http://placehold.it/50x50" alt="Mary Kay BoitanoNelson" width="50" height="50"></div>
                        <div class="results">
                            <div>Gorilla Expert</div>
                            <div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum posuere eros et aliquet. Fusce ligula enim, accumsan a elementum quis, tempus quis neque. Nunc sollicitudin luctus aliquet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquet eleifend ullamcorper. Sed sit amet orci id enim venenatis dignissim. Curabitur posuere mattis ligula quis consequat.</p>
                            </div>
                            <div><a href="mailto:mymail@mymail.com">Email</a></div>
                        </div>
                    </div>
                    <div class="toggle-hide-show twelve columns">
                        <a href="javascript:void(0);"><span>+</span>&nbsp;George Saunders</a>
                    </div>
                    <div class="instructor-item hide-onload twelve columns" style="display: none;">
                        <div class="thumbnail"><img src="http://placehold.it/50x50" alt="paul" width="50" height="50"></div>
                        <div class="results">
                            <div>Professor, Syracuse University</div>
                            <div>360-555-1515</div>
                        </div>
                    </div>
                    <div class="toggle-hide-show twelve columns">
                        <a href="javascript:void(0);"><span>+</span>&nbsp;Jonathan Lipkowicz</a>
                    </div>
                    <div class="instructor-item hide-onload twelve columns" style="display: none;">
                        <div class="thumbnail"><img src="http://placehold.it/50x50" alt="Jonathan" width="50" height="50"></div>
                        <div class="results">
                            <div>Professor Emeritus, Harvard University</div>
                            <div>This is lots of descriptive info about an instructor.</div>
                            <div>360-111-2222</div>
                            <div><a href="mailto:blah@blah.com">Email</a></div>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
<!-- AddThisEvent -->
<script type="text/javascript" src="//js.addthisevent.com/atemay.js"></script>
<!-- AddThisEvent Settings -->
<script type="text/javascript">
    addthisevent.settings({
        license   : "aao8iuet5zp9iqw5sm9z",
        mouse     : false,
        css       : true,
        outlook   : {show:true, text:"Outlook Calendar"},
        google    : {show:true, text:"Google Calendar"},
        yahoo     : {show:true, text:"Yahoo Calendar"},
        hotmail   : {show:true, text:"Hotmail Calendar"},
        ical      : {show:true, text:"iCal Calendar"},
        facebook  : {show:true, text:"Facebook Event"},
        dropdown  : {order:"google,ical,outlook"},
        callback  : ""
    });
</script>
<!--==================== /Module: Calendar: Event Topic Detail =============================-->