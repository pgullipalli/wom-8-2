<!--==================== Module: Calendar: Event Topic Listing =============================-->
<div class="module-ce-topic-results module-ce-results core-results">
	<div class="module-pg-wrapper">
		<div class="module-pg-info">Viewing Page 1 of 1 | Showing Results 1 - 3 of 3</div>
	</div>
	<div class="grid">
		<div class="listing-wrap twelve columns">
			<ul class="core-list">
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Cesarean Birth Class</a></h5>
					<div class="list-item-image">
						<a href="#"><img src="http://placehold.it/100x100" alt="photo_of_event_award_gala"></a>
					</div>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>For couples who are expecting to deliver by c-section.</p>
						</div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
						</div>
					</div>
				</li>
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Breastfeeding Class</a></h5>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>This class will teach you the basics of breastfeeding. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum posuere eros et.</p>
						</div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
						</div>
					</div>
				</li>
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Cesarean Birth Class</a></h5>
					<div class="list-item-image">
						<a href="#"><img src="http://placehold.it/100x100" alt="photo_of_event_award_gala"></a>
					</div>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>For couples who are expecting to deliver by c-section.</p>
						</div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>  			
	<div class="module-pg-wrapper">
		<div class="module-pg-info">Viewing Page 1 of 1 | Showing Results 1 - 3 of 3</div>
	</div>
</div>
<!--==================== /Module: Calendar: Event Topic Listing =============================-->