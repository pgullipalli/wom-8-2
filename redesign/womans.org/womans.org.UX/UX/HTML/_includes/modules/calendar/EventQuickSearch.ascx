
<div class="calendar-side-wrapper">
<!--==================== Module: Calendar: Event Quick Search =============================-->
<div class="module-ce-search core-search right-desktop-search">
	<div class="grid reg-callout">
		<h3 class="module-heading">Event Search</h3>		
		<div class="twelve columns">
			<label class="label">Keyword</label>
			<input type="text" placeholder="Enter Keyword(s)" class="textbox">
		</div>
		<div class="twelve columns">
			<label class="label">All Categories</label>
			<div class="selectbox">  
				<select class="selectboxdiv">
					<option value="">All Categories</option>
					<option value="birthing-classes">Birthing Classes</option>
				</select>
				<div class="out"></div>
			</div>			
		</div>
	    <!--<div class="twelve columns">
	        <label class="label">Instructor</label>
			<div class="selectbox">  
				<select class="selectboxdiv">
					<option value="">All Instructors</option>
					<option value="albus dumbledore">Albus Dumbledore</option>
					<option value="jane goodall">Jane Goodall</option>
					<option value="jonathan lipkowicz">Jonathan Lipkowicz</option>
					<option value="george saunders">George Saunders</option>
				</select>
				<div class="out"></div>
			</div>			
		</div>-->
		<div class="twelve columns grid">
			<label class="label">Date</label>
			<label class="noshow">Date From</label>
			<input type="date" placeholder="Date From" class="dp_input twelve columns date-to">
			<label class="noshow">Date To</label>
			<input type="date" placeholder="Date To" class="dp_input twelve columns date-from">    
		</div>
		<div class="twelve columns">
			<input type="submit" value="Search" class="button pink default_button">
			<input type="button" value="Reset" class="button reset-button default_button">
		</div>
	</div>
</div>
<!--==================== /Module: Calendar: Event Quick Search =============================-->
</div>