<!--==================== Module: Calendar: Shopping Cart (Top Variant) =============================-->
<div class="cart-header-wrap grid">
	<h1 class="module-ce-checkout-header nine columns">Calendar Event Shopping Cart</h1>
	<div class="cart-empty three columns"><a href="#">Empty Cart</a></div>
</div>
<div class="module-ce-session-shoppingcart top grid">
    
    <div class="cart-contents twelve columns grid">
		<!--==================== Module: Calendar: Shopping Cart Content =============================-->
		<div class="twelve columns">
            <ul class="cart-listing">
				<li class="cart-item">
					<div class="item-summary">
						<h5 class="name"><a href="/calendar-sessions/test-calendar/birthing-class">Birthing Class</a></h5>
					</div>
					<div class="item-details">
						<div class="item-actions-remove"><a href="#" title="Remove">Remove</a></div>
						<div class="desc">07/10/2014 3:00PM - 5:00PM</div>
						<div class="item-price">$20.00</div>
					</div>
					<div class="item-details">
						<div class="item-actions-remove"><a href="#" title="Remove">Remove</a></div>					
						<div class="desc">08/17/2014 3:00PM - 5:00PM</div>
						<div class="item-price">$20.00</div>
					</div>
				</li>
				<li class="cart-item">
					<div class="item-summary">
						<h5 class="name"><a href="/calendar-sessions/test-calendar/first-aid-class">First Aid Class</a></h5>
					</div>
					<div class="item-details">
						<div class="item-actions-remove"><a href="#" title="Remove">Remove</a></div>
						<div class="desc">09/15/2014 10:00AM - 3:00PM (when wrapping happens)</div>
						<div class="item-price">$45.00</div>
					</div>
				</li>
            </ul>
		</div>

		<div class="cart-actions twelve columns grid">
		
		<div class="cart-buttons twelve columns grid">
    	    <div class="cart-register twelve columns"><a class="button" href="/calendar-premium/event-registration-premium">Register Now</a></div>
    	</div>
		
		
        <div class="cart-promos grid first">
            <!--First panel------------------------------>
			<div class="add-promo-panel twelve columns grid">
				<div class="add-promo-link twelve columns"><a href="#">Add Fitness Member Number or Employee Number</a></div>
                <div class="eight columns">
					<input id="hdnPromoCodesAvail" type="hidden" value="Test Promo-Code Percent,Test Promo Percent">
					<label>Promo</label>
					<input type="text" class="textbox promo-input nine columns first" placeholder="Enter Fitness or Employee Number">
					<input type="submit" value="Apply" class="button disabled pink">
				</div>
				<div class="twelve columns">
				<span class="error" data-reason="invalid">Invalid Fitness Member Number or Employee Number. If you do not qualify for fitness member or employee pricing, please update the event with applicable pricing. </span>
				</div>
			</div>

            <!--First panel------------------------------>
			<div class="add-promo-panel twelve columns grid">
				<div class="add-promo-link twelve columns"><a href="#">Add Promo Code</a></div>
                <div class="eight columns">
					<input id="hdnPromoCodesAvail" type="hidden" value="Test Promo-Code Percent,Test Promo Percent">
					<label>Promo</label>
					 <input type="text" class="textbox promo-input nine columns first" placeholder="Enter Promo Code">
					<input type="submit" value="Apply" class="button disabled pink">
				</div>
				<div class="twelve columns">
				<span class="error" data-reason="invalid">Promo code invalid. Please enter a valid promo code or proceed to checkout</span>
				</div>
			</div>	
            		
		</div>



        <div class="cart-total twelve columns">
            <h5><span>Total </span>&nbsp;$200.00</h5>
		</div>
	</div>
		<!--==================== /Module: Calendar: Shopping Cart Content =============================-->
		<div class="cart-calculations">
			<div class="cart-subtotal">
				<h5><span>Subtotal</span>&nbsp;$250.00</h5>
			</div>
			<div class="cart-promos">
				<div class="applied-promos">
					<div>Promo Code Applied<span>$50.00-</span></div>
				</div>
			</div>
	        <div class="cart-total twelve columns">
	            <h5><span>Total </span>&nbsp;$200.00</h5>
			</div>
		</div>
	</div>
</div>
<!--==================== /Module: Calendar: Shopping Cart (Top Variant) =============================-->
