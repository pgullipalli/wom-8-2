<!--==================== Module: Calendar: Event Day Picker =============================-->
<div class="module-ce-eventdaypicker core-daypicker collapse-for-mobile">
	<div class="grid reg-callout">
		<h3>Browse by Date</h3>	
	    <div class="module-ce-daypicker twelve columns"></div>
	    <!-- Do Not Remove These Hidden Fields -->
	    <input type="hidden" id="hdnResultPageURL" class="hdnResultPageURL" value="/calendar/event-search-results/" />
	    <input type="hidden" id="hdnEventsDays" class="hdnEventsDays" value="12/01/2013,12/02/2013,12/09/2013,01/01/2014,01/06/2014,01/13/2014,02/01/2014,02/03/2014,02/10/2014,03/01/2014,03/03/2014,03/10/2014,04/01/2014,04/07/2014,04/14/2014,05/01/2014,05/05/2014,05/12/2014,06/01/2014,06/02/2014,06/09/2014,07/01/2014,07/07/2014,07/14/2014,08/01/2014,08/04/2014,08/11/2014,09/01/2014,09/01/2014,09/08/2014,10/01/2014,10/06/2014,10/13/2014,11/01/2014,11/03/2014,11/10/2014," />
	</div>
</div>
<!--==================== /Module: Calendar: Event Day Picker =============================-->
