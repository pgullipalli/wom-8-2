<!--==================== Module: Calendar: Shopping Cart Checkout =============================-->
<div class="cart-header-wrap grid">
	<h1 class="module-ce-checkout-header nine columns">Calendar Event Shopping Cart</h1>
	<div class="cart-empty three columns"><a href="#">Empty Cart</a></div>
</div>
<div class="module-ce-session-shoppingcart grid">
    <div class="cart-contents twelve columns grid">
	<!--==================== Module: Calendar: Shopping Cart Content =============================-->
		<div class="twelve columns">
            <ul class="cart-listing">
				<li class="cart-item">
					<div class="item-summary">
						<h5 class="name"><a href="/calendar-sessions/test-calendar/birthing-class">Birthing Class</a></h5>
					</div>
					<div class="item-details">
						<div class="item-actions-remove"><a href="#" title="Remove">Remove</a></div>
						<div class="desc">07/10/2014 3:00PM - 5:00PM</div>
						<div class="item-price">$20.00</div>
					</div>
					<div class="item-details">
						<div class="item-actions-remove"><a href="#" title="Remove">Remove</a></div>					
						<div class="desc">08/17/2014 3:00PM - 5:00PM</div>
						<div class="item-price">$20.00</div>
					</div>
				</li>
				<li class="cart-item">
					<div class="item-summary">
						<h5 class="name"><a href="/calendar-sessions/test-calendar/first-aid-class">First Aid Class</a></h5>
					</div>
					<div class="item-details">
						<div class="item-actions-remove"><a href="#" title="Remove">Remove</a></div>
						<div class="desc">09/15/2014 10:00AM - 3:00PM (when wrapping happens)</div>
						<div class="item-price">$45.00</div>
					</div>
				</li>
            </ul>
		<div>
	</div>
    <div class="cart-actions twelve columns grid">
    	<div class="six columns grid first">
	        <div class="cart-promos twelve columns grid first">
				<div class="add-promo-link twelve columns">
					<a href="#">Add Promo Code...</a>
					
				</div>
	            <div class="add-promo-panel twelve columns grid">
	                <input id="hdnPromoCodesAvail" type="hidden" value="Test Promo-Code Percent,Test Promo Percent">
	                <label>Promo</label>
	                <input type="text" class="textbox promo-input nine columns first" placeholder="Enter Promo Code">
	                <!--<a href="#" class="button pink">Apply</a>-->
					<input type="submit" value="Apply" class="button pink default_button">
					<span class="error" data-reason="blank" style="display:none;">Please enter a promo code.</span>
					<span class="error" data-reason="invalid" style="display:none;">Invalid Promo Code.</span>
				</div>

			</div>
			<div class="fitness-club-member-number twelve columns">
				<div class="add-promo-link twelve columns">
					<a href="#">Add Fitness Club Member Number...</a>
				</div>
				<div class="add-promo-panel twelve columns grid">
					<label>Fitness Club Member Number</label>
					<input type="text" placeholder="Fitness Club Member Number" class="textbox nine columns first">
	                <!--<a href="#" class="button pink">Apply</a>-->
					<input type="submit" value="Apply" class="button pink default_button">
					<div class="error" style="display:none;">Invalid Fitness Member Number. If you do not qualify for fitness member pricing, please update the event with applicable pricing.</div>
				</div>
			</div>
		</div>
		<div class="cart-calculations six columns grid">
			<div class="cart-subtotal twelve columns">
				<h5><span>Subtotal</span>&nbsp;$250.00</h5>
			</div>
			<div class="cart-promos twelve columns">
				<div class="applied-promos">
					<div>Promo Code Applied<span>$50.00-</span></div>
				</div>
			</div>
	        <div class="cart-total twelve columns">
	            <h5><span>Total </span>&nbsp;$200.00</h5>
			</div>
		</div>
		<div class="cart-buttons twelve columns grid">
    	    <div class="cart-register six columns push_six"><a class="button" href="/calendar-premium/event-registration-premium">Register Now</a></div>
    	</div>
	</div>
</div>
<!--==================== /Module: Calendar: Shopping Cart Content =============================-->