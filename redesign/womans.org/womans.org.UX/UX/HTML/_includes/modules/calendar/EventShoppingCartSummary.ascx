<div class="callout-wrapper module-ce-session-shoppingcart module-ce-session-shoppingcart-summary">
	<div class="callout">
		<h3 class="module-heading">Shopping Cart</h3>
		

        <div class="grid reg-callout">



		<div class="cart-summary twelve columns">
			<p><span>Cart Total: $20.00</span></p>
		</div>

		<div class="cart-actions grid twelve columns">
			<div class="cart-edit six columns">
				<a class="button pink" href="/calendar-premium/shopping-cart-checkout">Edit Cart</a>
			</div>
			<div class="cart-checkout six columns">
				<a class="button pink" href="/calendar-premium/shopping-cart-checkout">Checkout</a>
			</div>
		</div>
                </div>
	</div>
</div>