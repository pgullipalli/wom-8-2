<!--==================== Module: Calendar: Event Search =============================-->
<div class="module-ce-search core-search">
	<div class="grid">
	    <div class="six columns">
	        <label class="label">Keyword</label>
	        <input type="text" placeholder="Enter Keyword(s)" class="textbox">
		</div>
		<div class="six columns">
			<label class="label">All Categories</label>
			<div class="selectbox">  
				<select class="selectboxdiv">
					<option value="">All Categories</option>
					<option value="birthing-classes">Birthing Classes</option>
				</select>
				<div class="out"></div>
			</div>			
		</div>
	   <!-- <div class="six columns">
	        <label class="label">Instructor</label>
			<div class="selectbox">  
				<select class="selectboxdiv">
					<option value="">All Instructors</option>
					<option value="albus dumbledore">Albus Dumbledore</option>
					<option value="jane goodall">Jane Goodall</option>
					<option value="jonathan lipkowicz">Jonathan Lipkowicz</option>
					<option value="george saunders">George Saunders</option>
				</select>
				<div class="out"></div>
			</div>			
		</div>-->
		<div class="six columns">
			<label class="label">Date</label>

			<div class="grid">
				<input type="date" placeholder="Date From" class="dp_input six columns textbox">
				<input type="date" placeholder="Date To" class="dp_input six columns textbox">
			</div>			
		</div>
		<div class="twelve columns search-option-submit">

			<input type="submit" value="Search" class="default_button pink button">
		</div>
	</div>
</div>
<!--==================== /Module: Calendar: Event Search =============================-->
