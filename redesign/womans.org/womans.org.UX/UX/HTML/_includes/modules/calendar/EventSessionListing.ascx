<!--==================== Module: Calendar: Event Session Listing =============================-->
<div class="module-ce-session-results module-ce-results core-results">
    <div class="module-pg-wrapper">
        <div class="module-pg-nav">
			<ul class="pagination">
				<li class="arrow no-link"><a href="">&laquo;</a></li>
				<li class="active"><a href="">1</a></li>
				<li><a href="">2</a></li>
				<li><a href="">3</a></li>
				<li><a href="">4</a></li>
				<li class="arrow"><a href="">&raquo;</a></li>
			</ul>			
		</div>
     	<div class="module-pg-info">Page 1 of 4 | Results 1 - 10 of 38</div>
	</div>
	<div class="grid">
		<div class="listing-wrap twelve columns">
			<ul class="core-list">
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Cesarean Birth Class</a></h5>
					<div class="list-item-image">
						<a href="#"><img src="http://placehold.it/100x100" alt="photo_of_event_award_gala"></a>
					</div>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>For couples who are expecting to deliver by c-section.</p>
						</div>
                        <div class="module-date">
                            May 26, 2014 10:00 AM - 11:00 AM 
                        </div>
                        <div class="module-instructors">
                        	<span class="module-instructors-label">Instructor(s)</span>
                        	Albus Dumbledor, Blank Profinto, Jane Goodall, John Doe
                        </div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
							<span>&nbsp;|&nbsp;</span>
							<a href="#" class="register">Register</a>
						</div>
					</div>
				</li>
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Breastfeeding Class</a></h5>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>This class will teach you the basics of breastfeeding. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum posuere eros et.</p>
						</div>
                        <div class="module-date">
                            May 26, 2014 10:00 AM - 11:00 AM 
                        </div>
                        <div class="module-instructors">
                        	<span class="module-instructors-label">Instructor(s)</span>
                        	George Saunders, Jonathan Lipkowicz, Name Only
                        </div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
							<span>&nbsp;|&nbsp;</span>
							<a href="#" class="register">Register</a>
						</div>
					</div>
				</li>
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Cesarean Birth Class</a></h5>
					<div class="list-item-image">
						<a href="#"><img src="http://placehold.it/100x100" alt="photo_of_event_award_gala"></a>
					</div>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>For couples who are expecting to deliver by c-section.</p>
						</div>
                        <div class="module-date">
                            May 26, 2014 10:00 AM - 11:00 AM 
                        </div>
                        <div class="module-instructors">
                        	<span class="module-instructors-label">Instructor(s)</span>
                        	Albus Dumbledor, Blank Profinto, Jane Goodall, John Doe
                        </div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
							<span>&nbsp;|&nbsp;</span>
							<a href="#" class="register">Register</a>
						</div>
					</div>
				</li>
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Breastfeeding Class</a></h5>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>This class will teach you the basics of breastfeeding. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum posuere eros et.</p>
						</div>
                        <div class="module-date">
                            May 26, 2014 10:00 AM - 11:00 AM 
                        </div>
                        <div class="module-instructors">
                        	<span class="module-instructors-label">Instructor(s)</span>
                        	George Saunders, Jonathan Lipkowicz, Name Only
                        </div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
							<span>&nbsp;|&nbsp;</span>
							<a href="#" class="register">Register</a>
						</div>
					</div>
				</li>
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Cesarean Birth Class</a></h5>
					<div class="list-item-image">
						<a href="#"><img src="http://placehold.it/100x100" alt="photo_of_event_award_gala"></a>
					</div>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>For couples who are expecting to deliver by c-section.</p>
						</div>
                        <div class="module-date">
                            May 26, 2014 10:00 AM - 11:00 AM 
                        </div>
                        <div class="module-instructors">
                        	<span class="module-instructors-label">Instructor(s)</span>
                        	Albus Dumbledor, Blank Profinto, Jane Goodall, John Doe
                        </div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
							<span>&nbsp;|&nbsp;</span>
							<a href="#" class="register">Register</a>
						</div>
					</div>
				</li>
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Breastfeeding Class</a></h5>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>This class will teach you the basics of breastfeeding. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum posuere eros et.</p>
						</div>
                        <div class="module-date">
                            May 26, 2014 10:00 AM - 11:00 AM 
                        </div>
                        <div class="module-instructors">
                        	<span class="module-instructors-label">Instructor(s)</span>
                        	George Saunders, Jonathan Lipkowicz, Name Only
                        </div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
							<span>&nbsp;|&nbsp;</span>
							<a href="#" class="register">Register</a>
						</div>
					</div>
				</li>
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Cesarean Birth Class</a></h5>
					<div class="list-item-image">
						<a href="#"><img src="http://placehold.it/100x100" alt="photo_of_event_award_gala"></a>
					</div>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>For couples who are expecting to deliver by c-section.</p>
						</div>
                        <div class="module-date">
                            May 26, 2014 10:00 AM - 11:00 AM 
                        </div>
                        <div class="module-instructors">
                        	<span class="module-instructors-label">Instructor(s)</span>
                        	Albus Dumbledor, Blank Profinto, Jane Goodall, John Doe
                        </div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
							<span>&nbsp;|&nbsp;</span>
							<a href="#" class="register">Register</a>
						</div>
					</div>
				</li>
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Breastfeeding Class</a></h5>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>This class will teach you the basics of breastfeeding. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum posuere eros et.</p>
						</div>
                        <div class="module-date">
                            May 26, 2014 10:00 AM - 11:00 AM 
                        </div>
                        <div class="module-instructors">
                        	<span class="module-instructors-label">Instructor(s)</span>
                        	George Saunders, Jonathan Lipkowicz, Name Only
                        </div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
							<span>&nbsp;|&nbsp;</span>
							<a href="#" class="register">Register</a>
						</div>
					</div>
				</li>
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Cesarean Birth Class</a></h5>
					<div class="list-item-image">
						<a href="#"><img src="http://placehold.it/100x100" alt="photo_of_event_award_gala"></a>
					</div>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>For couples who are expecting to deliver by c-section.</p>
						</div>
                        <div class="module-date">
                            May 26, 2014 10:00 AM - 11:00 AM 
                        </div>
                        <div class="module-instructors">
                        	<span class="module-instructors-label">Instructor(s)</span>
                        	Albus Dumbledor, Blank Profinto, Jane Goodall, John Doe
                        </div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
							<span>&nbsp;|&nbsp;</span>
							<a href="#" class="register">Register</a>
						</div>
					</div>
				</li>
				<li class="core-li grid">
					<h5 class="list-item-header"><a href="#">Breastfeeding Class</a></h5>
					<div class="list-item-copy">
						<div class="list-item-teaser">
							<p>This class will teach you the basics of breastfeeding. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum posuere eros et.</p>
						</div>
                        <div class="module-date">
                            May 26, 2014 10:00 AM - 11:00 AM 
                        </div>
                        <div class="module-instructors">
                        	<span class="module-instructors-label">Instructor(s)</span>
                        	George Saunders, Jonathan Lipkowicz, Name Only
                        </div>
 						<div class="list-item-links">
							<a href="#" class="read-more">Read More</a>
							<span>&nbsp;|&nbsp;</span>
							<a href="#" class="register">Register</a>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>  			
    <div class="module-pg-wrapper">
        <div class="module-pg-nav">
			<ul class="pagination">
				<li class="arrow no-link"><a href="">&laquo;</a></li>
				<li class="active"><a href="">1</a></li>
				<li><a href="">2</a></li>
				<li><a href="">3</a></li>
				<li><a href="">4</a></li>
				<li class="arrow"><a href="">&raquo;</a></li>
			</ul>			
		</div>
     	<div class="module-pg-info">Page 1 of 4 | Results 1 - 10 of 38</div>
	</div>
</div>
<!--==================== /Module: Calendar: Event Session Listing =============================-->