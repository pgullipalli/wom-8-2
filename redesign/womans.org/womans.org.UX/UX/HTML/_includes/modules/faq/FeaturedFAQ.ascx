<!--==================== Module: FAQ: Featured FAQ =============================-->
<div class="module-faq-featured article">
	
    
        <h3 class="module-heading">Featured FAQs</h3>
    
    <div class="listing">
		
        
                <div class="listing-item">
			
                    <h4><a href="/faqs/question-1/">Question 1</a></h4>
                    <p>This is my sample answer</p>
                    <div>
				
                        <a href="/faqs/question-1/">Read More</a>
                    
			</div>
                
		</div>
            
                <div class="listing-item">
			
                    <h4><a href="/faqs/question-2/">Question 2</a></h4>
                    
                    <div>
				
                        <a href="/faqs/question-2/">Read More</a>
                    
			</div>
                
		</div>
            
    
	</div>
    

</div>
<!--==================== /Module: FAQ: Featured FAQ =============================-->
