<!--==================== Module: FAQ: FAQ Search =============================-->
<div class="module-faq-search core-search grid twelve columns">
    <div class="six columns">
        <label class="label">Keyword</label>
        <input type="text" placeholder="enter keyword(s)" class="textbox">
	</div>
    <div class="six columns">
        <label class="label">Filter by Category</label>
		<div class="selectbox">  
			<select class="selectboxdiv">
				<option value="">All Categories</option>
				<option value="cardiac-care">Cardiac Care</option>
				<option value="cancer">Cancer</option>
				<option value="maternity">Maternity</option>
			</select>
			<div class="out"></div>
		</div>			
	</div>
    <div class="twelve columns search-option-submit">
		<input type="submit" value="Search" class="button">
    </div>      
</div>
<!--==================== /Module: FAQ: FAQ Search =============================-->
