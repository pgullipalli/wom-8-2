<!--==================== Module: News: News Categories =============================-->
<div class="">
	<div class="module-nw-categories core-categories callout collapse-for-mobile">
		<div class="reg-callout grid">
			<h3>View by Category</h3>
			<ul class="module-nw-filters-list core-list">
				<li><a class="aspNetDisabled">All</a></li><li><a href="/news/search-results/?category=hospital+news">Hospital News</a></li>
				<li><a href="/news/search-results/?category=special+events">Special Events</a></li>
			</ul>
		</div>
	</div>
</div>
<!--==================== /Module: News: News Categories =============================-->
