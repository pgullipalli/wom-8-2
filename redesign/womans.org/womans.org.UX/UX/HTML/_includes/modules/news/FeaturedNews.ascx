<!--==================== Module: News: Featured News =============================-->

<div class="module-nw-feature grid reg-callout">
	
    
        <h3 class="module-heading">
            Featured News</h3>
    
    <div class="listing">
		
        
                <div class="listing-item">
			
                    <div class="module-thumbnail">
				
                        <img src="http://placehold.it/100x100" alt="logo" width="100" height="100" />
                    
			</div>
                    <div class="teaser-copy">
                        <h5><a href="/news/2012/07/25-million-pledged-for-new-heart-center-at-the-medtouch-medical-center/">$25 Million Pledged for New Heart Center at the MedTouch Medical Center</a></h5>
                        <div class="date">
				
                            July 20, 2012
                        
			</div>
                        <div class="listing-item-teaser">
				
                            <p>HOUSTON, Tex.&ndash; Cardiologists and surgeons who will work in a heart care facility just now...</p>
                        
			</div>
                        <div class="listing-item-more-link">
				
                            <a href="/news/2012/07/25-million-pledged-for-new-heart-center-at-the-medtouch-medical-center/">Read More</a>
                        
			</div>
                    </div>
                
		</div>
		
		
		<div class="listing-item">
                    <div class="teaser-copy">
                        <h5><a href="/news/2012/07/25-million-pledged-for-new-heart-center-at-the-medtouch-medical-center/">$25 Million Pledged for New Heart Center at the MedTouch Medical Center</a></h5>
                        <div class="date">
				
                            July 20, 2012
                        
			</div>
                        <div class="listing-item-teaser">
				
                            <p>HOUSTON, Tex.&ndash; Cardiologists and surgeons who will work in a heart care facility just now...</p>
                        
			</div>
                        <div class="listing-item-more-link">
				
                            <a href="/news/2012/07/25-million-pledged-for-new-heart-center-at-the-medtouch-medical-center/">Read More</a>
                        
			</div>
                    </div>
                
		</div>
            
    
	</div>
    

</div>
<!--==================== /Module: News: Featured News =============================-->
