<!--==================== Module: News: Related News =============================-->
<div class="module-nw-related core-related callout collapse-for-mobile">
    <div class="reg-callout grid">
		<h3 class="module-heading">Related News</h3>		
		<ul class="core-list">
			<li class="core-li grid">
				<div class="list-item-image">
					<a href="/news/2012/10/gala-event/">
						<img src="http://placehold.it/100x100" alt="photo_of_event_award_gala" />
					</a>
				</div>
				<div class="list-item-copy">
					<h5><a href="/news/2012/10/gala-event/">Gala Event</a></h5>
					<div class="date">October 27, 2012</div>
					<div class="listing-item-more-link">
						<a href="/news/2012/10/gala-event/">Read More</a>
					</div>
				</div>
			</li>
			<li class="core-li grid">
				<div class="list-item-copy">
					<h5><a href="/news/2012/10/gala-event/">Gala Event</a></h5>
					<div class="date">October 27, 2012</div>
					<div class="listing-item-more-link">
						<a href="/news/2012/10/gala-event/">Read More</a>
					</div>
				</div>
			</li>
		</ul>
    </div>
</div>
<!--==================== /Module: News: Related News =============================-->
