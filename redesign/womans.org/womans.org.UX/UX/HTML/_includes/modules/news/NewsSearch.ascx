<!--==================== Module: News: News Search =============================-->
<div class="module-nw-search core-search twelve columns grid">
	<div class="six columns">
		<label class="label">Keyword</label>
		<input type="text" placeholder="enter keyword(s)" class="textbox">
	</div>
	<div class="six columns">
		<label class="label">Filter by Category</label>
		<div class="selectbox">
			<select class="selectboxdiv">
				<option value="">All Categories</option>
				<option value="hospital news">Hospital News</option>
				<option value="special events">Special Events</option>
			</select>
			<div class="out">Filter By Category</div>
		</div>
	</div>
	<div class="six columns">	
		<label class="label">Date</label>
		<div class="grid">
			<input type="date" placeholder="Date From" class="dp_input six columns">
			<input type="date" placeholder="Date To" class="dp_input six columns">
		</div>
	</div>
	<div class="search-option-submit twelve columns">
		<input type="submit" value="Search" class="button pink default_button">
	</div>
</div>
<!--==================== /Module: News: News Search =============================-->
