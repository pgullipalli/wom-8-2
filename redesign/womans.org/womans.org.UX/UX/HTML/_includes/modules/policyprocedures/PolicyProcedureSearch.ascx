﻿<!--==================== Module: Policy Procedure: Policy Procedure Search =============================-->
<div class="module-pp-search core-search twelve columns grid">
    <div class="grid">
		<div class="six columns">
			<label class="label">Search By Keyword</label>
			<input type="text" class="textbox"  placeholder="enter keyword(s)">
		</div>
		<div class="six columns">
			<label class="label">Filter By Category</label>
			<div class="selectbox">
				<select class="selectboxdiv">
					<option value="">All Categories</option>
					<option value="policy category a">Policy Category A</option>
					<option value="policy category b">Policy Category B</option>
				</select>
				<div class="out"></div>
			</div>
		</div>
		<div class="search-option-submit twelve columns">
			<input type="submit" value="Search" class="button">
		</div>
	</div>
</div>
<!--==================== /Module: Policy Procedure: Policy Procedure Search =============================-->
