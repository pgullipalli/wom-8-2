﻿<!--==================== Module: Policy Procedure: Policy Procedure Detail =============================-->
<div class="module-pp-profile grid">
    <h1>Policy C</h1>
    
	<div class="twelve columns">
		<div class="module-upcoming">
			<a href="/policy-procedure/policy-folder/policy-c/?upid={53248DA1-FFAF-447F-BF69-549C1AB73BB8}">Upcoming Policy</a>
		</div>
		<div class="module-file twelve download columns">
			<a href="/~/media/Files/Modules/Policy%20Procedure/Document%20C.pdf"
				target="_blank">Download</a>
		</div>
	
	
	<section class="grid policy-content">
		<div><b>PolicyId:</b> 123456789</div>
		<div>
			<div class="date">Next Review Date: March 29, 2013</div>
		</div>
		<div>
			<div class="date">
				Last Reviewed Date: March 20, 2013
			</div>
		</div>
		<div>
			<b>Maintained By: </b><a href="mailto:test@medtouch.com">
			Surat Thairdthai</a>
		</div>
		<div>
			<b>Category:</b>
			<ul>
				<li>Policy Category A </li>
			</ul>
		</div>
		<div>
			<b>Regulatory Type:</b>
			<ul>
				<li>Regulatory Type A </li>
			</ul>
		</div>
		<div>
			<b>Hospital/Department: </b>
			<ul>
				<li>Policy Department A </li>
			</ul>
		</div>
		<div>
			Policy Approved Version 1
		</div>
		<div>
			<div><b>Attachments: </b></div>
			This is test attachmendt<br />
			<a href="~/media/49BC4D3BD36546F793E567F1A756B7B3.pdf">File one </a>
		</div>
		<div>
			<b>Related Policies &amp; Procedures: </b>
			<ul>
				<li><a href="/policy-procedure/policy-folder/policy-a/">Policy A</a> </li>
				<li><a href="/policy-procedure/policy-folder/upcoming-policy-b/">Upcoming Policy B</a> </li>
			</ul>
		</div>
		<div>
			<div><b>Regulation References: </b></div>
			Regulation reference to <a href="~/link.aspx?_id=94A5C7CE669A484F8F418F695A9C0064&amp;_z=z">this item</a>
		</div>
		<div>
			<div><b>Changed Notices: </b></div>
			This is a changed <strong>Noticed</strong> Field
		</div>
	</section>
	</div>
</div>
<!--==================== /Module: Policy Procedure: Policy Procedure Detail =============================-->
