﻿<!--==================== Module: Policy Procedure: Policy Procedure Detail =============================-->
<div class="module-pp-profile core-profile grid">
    <h1>Policy C</h1>
    <div class="twelve columns">
		<div>
			<a href="/policy-procedure/policy-folder/policy-c/">Return to Approved Policy</a>
		</div>
		<div class="module-previewupcoming">Upcoming Policy - For preview purposes only</div>
		<div class="module-file">
			<a href="http://local.mtmc2.medtouch.com/sitecore%20modules/Web/MedTouch.PolicyProcedure/UpcomingDocumentHandler.ashx?upid=53248da1-ffaf-447f-bf69-549c1ab73bb8" target="_blank">Download</a>
		</div>


	
		<section class="grid policy-content">
		
		<div><b>PolicyId: </b>123456789</div>
		<div>
			<div class="module-date">
				Next Review Date: July 18, 2013
			</div>
		</div>
		<div>
			<div class="module-date">
				Last Reviewed Date: March 13, 2013
			</div>
		</div>
		<div>
			<b>Maintained By: </b><a href="mailto:sthairdthai@medtouch.com">
				Surat Thairdthai</a>
		</div>
		<div>
			<b>Category: </b>
			<ul>
				<li>Policy Category A </li>
				<li>Policy Category B </li>
			</ul>
			<div class="clear">
			</div>
		</div>
		<div>
			<b>Regulatory Type: </b>
			<ul>
				<li>Regulatory Type A </li>
				<li>Regulatory Type B </li>
			</ul>
			<div class="clear">
			</div>
		</div>
		<div>
			<b>Hospital/Department: </b>
			<ul>
				<li>Policy Department A </li>
				<li>Policy Department B </li>
			</ul>
			<div class="clear">
			</div>
		</div>
		<div>
			Policy&nbsp;Upcoming Version 2
		</div>
		<div>
			<div>
				<b>Attachments: </b>
			</div>
			This is test attachmendt<br />
			<a href="~/media/49BC4D3BD36546F793E567F1A756B7B3.pdf">File one </a>
			<br />
			upcoming
		</div>
		<div>
			<b>Related Policies & Procedures: </b>
			<ul>
				<li><a href="/policy-procedure/policy-folder/policy-a/">
					Policy A</a> </li>
				<li><a href="/policy-procedure/policy-folder/upcoming-policy-b/">
					Upcoming Policy B</a> </li>
				<li><a href="/policy-procedure/policy-folder/policy-d-added-version/">
					Policy D added Version</a> </li>
			</ul>
			<div class="clear">
			</div>
		</div>
		<div>
			<div>
				<b>Regulation References: </b>
			</div>
			<p>
				Regulation reference to <a href="~/link.aspx?_id=94A5C7CE669A484F8F418F695A9C0064&amp;_z=z">
					this item</a><br />
				upcoming</p>
		</div>
		<div>
			<div>
				<b>Changed Notices: </b>
			</div>
			This is a changed <strong>Noticed</strong> Field upcoming
		</div>
		</section>
	</div>
</div>
<!--==================== /Module: Policy Procedure: Policy Procedure Detail =============================-->
