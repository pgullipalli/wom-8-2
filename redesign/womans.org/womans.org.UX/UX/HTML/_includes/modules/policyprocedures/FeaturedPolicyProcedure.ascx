﻿<!--==================== Module: Policy Procedure: Featured Policy Procedure =============================-->
<div class="module-pp-feature callout">
    <div class="reg-callout">
        <h3 class="module-heading">Featured Policies</h3>
        <div class="listing">
            <div class="listing-item">
                <h5>
                    <a href="/en/policy-procedure/policy-folder/policy-surat-test-folder/surat-non-version-doc-policy-c-by-ce/">
                        Surat Non Version Doc Policy C by CE</a></h5>
                <div class="module-pp-date">
                    March 20, 2013
                </div>
                <div class="listing-item-more-link">
                    <a href="/en/policy-procedure/policy-folder/policy-surat-test-folder/surat-non-version-doc-policy-c-by-ce/">
                        Read More</a>
                </div>
            </div>
            <div class="listing-item">
                <h5>
                    <a href="/en/policy-procedure/policy-folder/policy-surat-test-folder/surat-non-version-doc-policy-d-by-am/">
                        Surat Non Version Doc Policy D by AM</a></h5>
                <div class="module-pp-date">
                    March 20, 2013
                </div>
                <div class="listing-item-more-link">
                    <a href="/en/policy-procedure/policy-folder/policy-surat-test-folder/surat-non-version-doc-policy-d-by-am/">
                        Read More</a>
                </div>
            </div>
            <div class="listing-item">
                <h5>
                    <a href="/en/policy-procedure/policy-folder/policy-surat-test-folder/surat-test-policy-a-by-ce/">
                        Surat Test Policy A by CE</a></h5>
                <div class="module-pp-date">
                    March 13, 2013
                </div>
                <div class="listing-item-teaser">
                    <p>
                        Test Version 2</p>
                </div>
                <div class="listing-item-more-link">
                    <a href="/en/policy-procedure/policy-folder/policy-surat-test-folder/surat-test-policy-a-by-ce/">
                        Read More</a>
                </div>
            </div>
        </div>
        <div class="module-pp-view-all">
            <a href="/en/policy-procedure/policyprocedure-listing-page/">
                View All</a>
        </div>
    </div>
</div>
<!--==================== /Module: Policy Procedure: Featured Policy Procedure =============================-->
