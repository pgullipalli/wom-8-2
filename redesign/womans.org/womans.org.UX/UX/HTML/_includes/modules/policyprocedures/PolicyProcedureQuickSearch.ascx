﻿<!--==================== Module: Policy Procedure: Policy Procedure Quick Search =============================-->
<div class="module-pp-search core-search callout collapse-for-mobile">
    <div class="reg-callout grid">
        <h3>Policy Procedure Search</h3>
        <div class="twelve columns">
            <label class="label">Search By Keyword</label>
            <input type="text" placeholder="enter keyword(s)" class="textbox">
        </div>
        <div class="twelve columns">
            <label class="label">Filter by Category</label>
			<div class="selectbox">
				<select class="selectboxdiv">
					<option value="">All Categories</option>
					<option value="hospital news">Hospital News</option>
					<option value="special events">Special Events</option>
				</select>
				<div class="out"></div>
			</div>
        </div>
        <div class="twelve columns">
			<input type="submit" value="Search" class="button">
		</div>
    </div>
</div>
<!--==================== /Module: Policy Procedure: Policy Procedure Quick Search =============================-->
