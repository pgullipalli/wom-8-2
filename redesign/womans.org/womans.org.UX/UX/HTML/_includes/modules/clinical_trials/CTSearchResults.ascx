<!--==================== Module: Clinical Trials: CTSearchResults  =============================-->
<div class="module-ct-results article">
	
    
    <div class="module-pg-wrapper">
		
    
    <div class="module-pg-info">
			
        Viewing Page 1 of 1 | Showing Results 1 of 1
    
		</div>
    <div class="clear">
    </div>

	</div>

    <div class="listing">
		
        
                <div class="listing-item">
			
                    <h4><a href="/clinical-trials/a/a-trial/">A Trial</a></h4>
                    <div class="listing-item-teaser">
				
                        <p>This is my content copy</p>
                    
			</div>
                    <div>
				
                        <label>Lead Researcher:</label>
                        Dan Persson
                    
			</div>
                    <div>
				
                        <label>Location:</label>
                        Location
                    
			</div>
                    <div>
				
                        <label>Status:</label>
                        Accepting Participants
                    
			</div>
                    <div class="listing-item-more-link">
				
                        <a href="/clinical-trials/a/a-trial/">Read More</a>
                    
			</div>
                
		</div>
            
    
	</div>
    <div class="module-pg-wrapper">
		
    
    
    <div class="clear">
    </div>

	</div>


</div>
<!--==================== /Module: Clinical Trials: CTSearchResults  =============================-->
