<!--==================== Module: Clinical Trials: CTSearchBox  =============================-->
<div class="module-ct-search core-search grid twelve columns">
    <div class="six columns">
        <label class="label">Search By Keyword</label>
        <input type="text" placeholder="enter keyword(s)" class="textbox">
	</div>
    <div class="six columns">
        <label class="label">Search by Condition/Disease</label>
		<div class="selectbox">  
			<select class="selectboxdiv">
				<option value="">Select a Condition/Disease</option>
				<option value="breast-cancer">Breast Cancer</option>
			</select>
			<div class="out"></div>
		</div>			
	</div>
    <div class="twelve columns search-option-submit">
		<input type="submit" value="Search" class="button">
    </div>
</div>
<!--==================== /Module: Clinical Trials: CTSearchBox  =============================-->
