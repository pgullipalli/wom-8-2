<!--==================== Module: Clinical Trials: RelatedClinicalTrialsCallout  =============================-->
<div class="module-ct-related callout">
	
    <div class="reg-callout">
         
            <h3 class="module-heading">Related Clinical Trials</h3>
        
        <div class="listing">
		
            
                    <div class="listing-item">
			
                        <h5><a href="/clinical-trials/a/a-trial/">A Trial</a></h5>
                        <div class="listing-item-teaser">
				
                            <p>This is my content copy</p>
                        
			</div>
                        <div class="listing-item-more-link">
				
                            <a href="/clinical-trials/a/a-trial/">Read More</a>
                        
			</div>
                    
		</div>
                
        
	</div>
        
    </div>

</div>
<!--==================== /Module: Clinical Trials: RelatedClinicalTrialsCallout  =============================-->
