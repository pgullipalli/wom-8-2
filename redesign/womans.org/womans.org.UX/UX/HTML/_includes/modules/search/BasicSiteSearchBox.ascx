<!--==================== Module: Site Search: Basic Site Search Box =============================-->
<div class="module-ss-search-box core-search-box core-search grid">
    <div class="twelve columns">
		<label>Keywords</label>
        <input type="text" placeholder="enter keyword(s)" class="textbox">
        <input type="submit" value="Search" class="button">
	</div>
</div>
<!--==================== /Module: Site Search: Basic Site Search Box =============================-->