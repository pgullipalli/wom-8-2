<!--==================== Module: Site Search: Basic Search Results =============================-->
<div class="module-ss-results article">
	
    <div class="module-ss-results-message">
		
        <h3>
            Search results for <span class="module-ss-current-keyword">test</span></h3>
    
	</div>
    <div class="module-pg-wrapper">
		
    
    <div class="module-pg-info">
			
        Viewing Page 1 of 1 | Showing Results 1 - 5 of 5
    
		</div>
    <div class="clear">
    </div>

	</div>
    <div class="listing">
		
        
                <div class="listing-item">
			
                    <h4>
                        <a href="/patient-testimonials/sample-testimonials/sample-testimonial/">Sample Testimonial</a></h4>
                    <div class="listing-item-teaser">
				<p>This is the body copy of my sample testimonial</p>
			</div>
                    <div class="listing-item-more-link">
				
                        <a href="/patient-testimonials/sample-testimonials/sample-testimonial/">Read More</a>
                    
			</div>
                
		</div>
            
                <div class="listing-item">
			
                    <h4>
                        <a href="/patient-testimonials/">Patient Testimonials</a></h4>
                    
                    <div class="listing-item-more-link">
				
                        <a href="/patient-testimonials/">Read More</a>
                    
			</div>
                
		</div>
            
                <div class="listing-item">
			
                    <h4>
                        <a href="/publications/2012/11/test-publication/">Test Publication</a></h4>
                    <div class="listing-item-teaser">
				<p>Test Publication Content</p>
			</div>
                    <div class="listing-item-more-link">
				
                        <a href="/publications/2012/11/test-publication/">Read More</a>
                    
			</div>
                
		</div>
            
                <div class="listing-item">
			
                    <h4>
                        <a href="/about-us/careers/">Careers</a></h4>
                    <div class="listing-item-teaser">
				<p>Do you have what it takes to grow a company? At MedTouch, we have vision and opportunity in abundance - now we're looking for world-class talent to...</p>
			</div>
                    <div class="listing-item-more-link">
				
                        <a href="/about-us/careers/">Read More</a>
                    
			</div>
                
		</div>
            
                <div class="listing-item">
			
                    <h4>
                        <a href="/about-us/">Welcome to the MedTouch Demo Site</a></h4>
                    <div class="listing-item-teaser">
				<p>Welcome to the MedTouch Demo Site. The goal of this website is to let you test drive Sitecore&rsquo;s extensive features and get a taste of...</p>
			</div>
                    <div class="listing-item-more-link">
				
                        <a href="/about-us/">Read More</a>
                    
			</div>
                
		</div>
            
    
	</div>
    <div class="module-pg-wrapper">
		
    
    
    <div class="clear">
    </div>

	</div>

</div>
<!--==================== /Module: Site Search: Basic Search Results =============================-->
