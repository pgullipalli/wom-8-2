<!--==================== Module: Site Search: Search Filters =============================-->
<div class="module-ss-filters core-filters callout collapse-for-mobile">
    <div class="reg-callout grid">
		<h3>Filter Results</h3>	
		<ul class="module-ss-filters-list core-list">
			<li><a class="aspNetDisabled">All (3)</a></li>
			<li><a href="/search-results/?keyword=sandra&amp;category=doctors">Doctors (1)</a></li>
			<li><a href="/search-results/?keyword=sandra&amp;category=locations">Locations (1)</a></li>
			<li><a href="/search-results/?keyword=sandra&amp;category=sevices">Services (1)</a></li>
		</ul>
    </div>
</div>
<!--==================== /Module: Site Search: Search Filters =============================-->
