﻿<!--==================== Module: Services: Related Services =============================-->
<div class="module-sv-related core-related callout collapse-for-mobile">
    <div class="reg-callout grid">
	    <h3>Related Services</h3>
		<ul class="core-list twelve columns">
            <li class="core-li">
                <h4><a href="/global/list-manager/services/fitness-and-rehabilitation/">Fitness and Rehabilitation</a></h4>
                <div class="more-link">
                    <a href="/global/list-manager/services/fitness-and-rehabilitation/">Read More</a>
                </div>
            </li>
            <li class="core-li">
                <h4><a href="/global/list-manager/services/brain/">Brain</a></h4>
                <div class="more-link">
                    <a href="/global/list-manager/services/brain/">Read More</a>
                </div>
            </li>
            <li class="core-li">
                <h4><a href="/global/list-manager/services/maternity/">Maternity &amp; % + </a>
                </h4>
                <div class="more-link">
                    <a href="/global/list-manager/services/maternity/">Read More</a>
                </div>
            </li>
            <li class="core-li">
                <h4><a href="/global/list-manager/services/new-service-external-link/">New Service External Link Name change &amp; Test</a></h4>
                <div class="more-link">
                    <a href="/global/list-manager/services/new-service-external-link/">Read More</a>
                </div>
            </li>
            <li class="core-li">
                <h4>
                    <a href="/global/list-manager/services/copy-of-brain/">
                        Brain 2</a></h4>
                <div class="more-link">
                    <a href="/global/list-manager/services/copy-of-brain/">Read More</a>
                </div>
            </li>
        </ul>
        <div class="module-sv-view-all core-view-all twelve columns">
            <a href="/medical-services/?services=copy+of+brain%7cbrain%7cfitness+and+rehabilitation%7cnew+service+external+link%7cmaternity%7cnew+service+page+content%7cnew+service+link+to+file">View All</a>
        </div>
    </div>
</div>
<!--==================== /Module: Services: Related Services =============================-->
