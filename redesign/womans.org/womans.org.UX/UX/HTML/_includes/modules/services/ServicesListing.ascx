<!--==================== Module: Services: Services Listing =============================-->
<div class="module-sv-results module-core-results">
    <a></a>
    <div class="module-pg-wrapper">
        <div class="module-pg-info">
            Viewing Page 1 of 1 | Showing Results 1 - 4 of 4
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="grid">
        <div class="twelve columns">
            <h4>
                <a href="http://www.medtouch.com" target="_blank">New Service External Link Name change
                </a>
            </h4>
            <div class="teaser-copy">
                <div class="listing-item-teaser">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed elit non justo
                    aliquam pretium ut ac justo. In mi leo, lacinia non ultrices ut, t
                </div>
            </div>
        </div>
        <div class="twelve columns">
            <h4>
                <a href="/~/media/Files/installation_guide_sc65.pdf">New Service Link to File</a></h4>
            <div class="teaser-copy">
                <div class="listing-item-teaser">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed elit non justo
                    aliquam pretium ut ac justo. In mi leo, lacinia non ultrices ut, t
                </div>
            </div>
        </div>
        <div class="twelve columns">
            <h4>
                <a href="/medical-services/">New Service Page Content</a></h4>
            <div class="teaser-copy">
                <div class="listing-item-teaser">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed elit non justo
                    aliquam pretium ut ac justo. In mi leo, lacinia non ultrices ut, t
                </div>
            </div>
        </div>
        <div class="twelve columns">
            <h4>
                <a>Not related Service</a></h4>
            <div class="teaser-copy">
                <div class="listing-item-teaser">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed elit non justo
                    aliquam pretium ut ac justo. In mi leo, lacinia non ultrices ut, t
                </div>
            </div>
        </div>
    </div>
    <div class="module-pg-wrapper">
        <div class="clear">
        </div>
    </div>
</div>
<!--==================== /Module: Services: Services Listing =============================-->
