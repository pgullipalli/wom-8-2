<!--==================== Module: Blog: Blog Categories =============================-->
<div id="main_0_rightpanel_1_pnlBrowseByCategory" class="module-bg-categories callout">
	
    <div class="reg-callout">
        
            <h4>
                View by Category</h4>
        
        
                <ul class="module-bg-filters-list">
            
                <li>
                    <a id="main_0_rightpanel_1_rptCategories_hlCategory_0" href="/blog/categories/test-category/">Test Category</a>
                </li>
            
                </ul>
            
        <div class="clear">
        </div>
    </div>

</div>
<!--==================== /Module: Blog: Blog Categories =============================-->
