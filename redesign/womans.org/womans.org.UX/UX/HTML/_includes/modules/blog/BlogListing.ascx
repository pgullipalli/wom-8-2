<!--==================== Module: Blog: Blog Listing =============================-->
<div id="main_0_contentpanel_2_pnlResults" class="module-bg-results grid">

    <div id="main_0_contentpanel_2_ucPagingTop_pnlPagination" class="module-pg-wrapper">
		<div id="main_0_contentpanel_2_ucPagingTop_pnlPaginationInfo" class="module-pg-info">
			Viewing Page 1 of 1 | Showing Results 1 - 2 of 2
		</div>
	</div>
	
    <div id="main_0_contentpanel_2_pnlListing" class="listing twelve columns">
        
		<div id="main_0_contentpanel_2_lvSearchResults_pnlResult_0" class="listing-item">
			<div id="main_0_contentpanel_2_lvSearchResults_pnlThumbnail_0" class="module-thumbnail two columns">
				<a id="main_0_contentpanel_2_lvSearchResults_hlThumbnail_0" href="/blog/2014/01/test-blog-post-2/">
				<img src="http://placehold.it/100x100" alt="photo_of_event_award_gala">
				</a>
			</div>
			
			<div class="ten columns">
				<h4><a id="main_0_contentpanel_2_lvSearchResults_hlItem_0" href="/blog/2014/01/test-blog-post-2/">Test Blog Post 2</a></h4>
				<div id="main_0_contentpanel_2_lvSearchResults_pnlDate_0" class="module-date"> January 18, 2014</div>
				<div id="main_0_contentpanel_2_lvSearchResults_pnlByLine_0" class="module-bg-authors">
					<b>by: </b><a href="/blog/authors/test-author/">Test Author</a>
				</div>
				<div id="main_0_contentpanel_2_lvSearchResults_pnlTeaser_0" class="listing-item-teaser">
			
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu accumsan felis. Proin rutrum massa et felis suscipit pulvinar. Maecenas iaculis...</p>
				</div>
				
				<div id="main_0_contentpanel_2_lvSearchResults_pnlMoreLink_0" class="listing-item-more-link">
					<a id="main_0_contentpanel_2_lvSearchResults_hlComments_0" href="/blog/2014/01/test-blog-post-2/#Comments">0 comment(s)</a><span class="pipe">|</span><a id="main_0_contentpanel_2_lvSearchResults_hlMoreLink_0" href="/blog/2014/01/test-blog-post-2/">Read More</a>
				</div>
			</div>
		</div>
				
            
		<div id="main_0_contentpanel_2_lvSearchResults_pnlResult_0" class="listing-item">
			<div id="main_0_contentpanel_2_lvSearchResults_pnlThumbnail_0" class="module-thumbnail two columns">
				<a id="main_0_contentpanel_2_lvSearchResults_hlThumbnail_0" href="/blog/2014/01/test-blog-post-2/">
				<img src="http://placehold.it/100x100" alt="photo_of_event_award_gala">
				</a>
			</div>
			
			<div class="ten columns">
				<h4><a id="main_0_contentpanel_2_lvSearchResults_hlItem_0" href="/blog/2014/01/test-blog-post-2/">Test Blog Post 2</a></h4>
				<div id="main_0_contentpanel_2_lvSearchResults_pnlDate_0" class="module-date"> January 18, 2014</div>
				<div id="main_0_contentpanel_2_lvSearchResults_pnlByLine_0" class="module-bg-authors">
					<b>by: </b><a href="/blog/authors/test-author/">Test Author</a>
				</div>
				<div id="main_0_contentpanel_2_lvSearchResults_pnlTeaser_0" class="listing-item-teaser">
			
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu accumsan felis. Proin rutrum massa et felis suscipit pulvinar. Maecenas iaculis...</p>
				</div>
				
				<div id="main_0_contentpanel_2_lvSearchResults_pnlMoreLink_0" class="listing-item-more-link">
					<a id="main_0_contentpanel_2_lvSearchResults_hlComments_0" href="/blog/2014/01/test-blog-post-2/#Comments">0 comment(s)</a><span class="pipe">|</span><a id="main_0_contentpanel_2_lvSearchResults_hlMoreLink_0" href="/blog/2014/01/test-blog-post-2/">Read More</a>
				</div>
			</div>
		</div>
            
	</div>
	
    <div id="main_0_contentpanel_2_ucPagingBottom_pnlPagination" class="module-pg-wrapper">
	</div>

</div>
<!--==================== /Module: Blog: Blog Listing =============================-->
