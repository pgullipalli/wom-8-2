<!--==================== Module: Blog: Blog Search =============================-->
<div class="module-bg-search-box core-search callout collapse-for-mobile">
    <div class="reg-callout grid">
		<h3>Blog Search</h3>
        <div class="twelve columns">
            <label>Keywords</label>
            <input type="text" placeholder="Enter Keyword(s)" class="textbox">
		</div>
        <input type="submit" value="Search" class="button">
    </div>
</div>
<!--==================== /Module: Blog: Blog Search =============================-->
