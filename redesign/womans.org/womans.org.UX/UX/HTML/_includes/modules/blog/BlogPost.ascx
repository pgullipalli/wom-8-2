    
    <!--==================== Module: Blog: Blog Post =============================-->
<div id="main_0_contentpanel_1_pnlDetail" class="module-bg-detail grid">
    <h1>Test Blog Post</h1>
    <div id="main_0_contentpanel_1_pnlDate" class="module-bg-date">
        January 03, 2014 09:08:00 AM
    </div>
    <div id="main_0_contentpanel_1_pnlAuthors" class="module-bg-detail-authors">
        <b>By:</b> <a href="/blog/authors/test-author/">Test Author</a>
    </div>
    <div id="main_0_contentpanel_1_UpdatePanel1">
        <div class="module-bg-detail-like">
            <a id="main_0_contentpanel_1_lkbVoteLikeUnlike" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("main_0$contentpanel_1$lkbVoteLikeUnlike", "", true, "", "", false, true))'>
                Like (0)</a>
        </div>
    </div>
	
	<section class="post twelve columns">
    <div id="main_0_contentpanel_1_pnlImage" class="module-bg-detail-image left">
        <img src="http://placehold.it/150x150" alt="photo_of_event_award_gala">
    </div>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam purus velit, aliquet
    in sagittis vitae, imperdiet id elit. Vivamus viverra, lorem et pharetra porta,
    dui mi pellentesque purus, at tincidunt nulla nulla at risus. Aenean porttitor non
    turpis eu faucibus. Curabitur viverra ultricies adipiscing. Nunc varius convallis
    velit, eget sollicitudin dolor scelerisque a. Aliquam quis porttitor nunc. Nullam
    auctor arcu neque, sit amet scelerisque sem dictum vel.<br>
    <br>
    Suspendisse ac ante quam. Morbi elementum enim nisl, vel condimentum quam auctor
    ac. In hac habitasse platea dictumst. Etiam tempor lorem quis risus facilisis, ut
    blandit augue feugiat. Proin rutrum eget dolor nec malesuada. In condimentum pulvinar
    tortor, at faucibus tortor. Suspendisse potenti. Nulla mattis dapibus eros sed pulvinar.
    Aliquam sollicitudin pharetra lacus nec mollis.<br>
    <br>
    Phasellus tincidunt, arcu porttitor adipiscing tincidunt, nibh lectus malesuada
    turpis, et feugiat nunc eros et risus. Vestibulum mattis magna libero, id dapibus
    ante gravida nec. Quisque viverra tellus ut libero convallis, eget rhoncus odio
    tristique. Nullam feugiat elementum aliquam. Quisque viverra at nunc quis porttitor.
    Etiam semper egestas orci, eget porttitor dolor facilisis vel. Morbi sed tortor
    sit amet odio vulputate volutpat at vitae odio.<br>
    <br>
    Vivamus sit amet est dolor. Duis consectetur mauris vitae nisi ullamcorper cursus.
    Aenean id tempor augue, vitae facilisis mi. Proin auctor sodales sollicitudin. Sed
    iaculis eu dolor quis sagittis. Donec vehicula nisi id nibh blandit convallis. Sed
    quis interdum metus, vel sodales nibh. Etiam accumsan, dui eget scelerisque bibendum,
    lorem velit bibendum metus, in blandit elit nibh sodales ligula. Donec suscipit
    vulputate nibh sed accumsan. Aliquam a ipsum commodo, posuere enim eget, aliquet
    felis. Fusce nec nisi vel odio dictum fringilla ut sit amet tortor. Vestibulum ante
    ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br>
    <br>
    Nulla tristique laoreet pulvinar. Donec quis adipiscing sem. Cras tincidunt bibendum
    pellentesque. Pellentesque tristique vestibulum adipiscing. Vivamus pulvinar quam
    eu sapien ornare sagittis. Quisque quis vestibulum mauris. Nunc libero velit, elementum
    id tortor et, fringilla ultrices purus. In sed ultricies nisi, quis tincidunt justo.
    Donec mollis elementum est, id malesuada lectus pulvinar in. Praesent a dictum lacus,
    porta mollis risus.
    <div id="main_0_contentpanel_1_pnlCategories" class="module-bg-detail-categories">
        Categories: <a href="/blog/categories/test-category/">Test Category</a>
    </div>
    <div id="main_0_contentpanel_1_pnlTopics" class="module-bg-detail-topics">
        Topics: <a href="/blog/topics/test-topic/">Test Topic</a>
    </div>
	</section>
</div>
<div id="main_0_contentpanel_2_pnlCommentListing" class="module-bg-comments-listing">
    <h3>
        <div id="Comments">
            Comments
        </div>
    </h3>
    <div id="main_0_contentpanel_2_UpdatePanel1">
        <div id="main_0_contentpanel_2_lvCommentListing_pnlResult_0" class="listing-item">
            <div id="main_0_contentpanel_2_lvCommentListing_pnlUser_0" class="module-bg-comment-byline">
                <span id="main_0_contentpanel_2_lvCommentListing_lbCommenter_0">Dan Persson - January
                    03, 2014 12:53:42 PM</span>
            </div>
            <div id="main_0_contentpanel_2_lvCommentListing_pnlComment_0" class="module-bg-comment-content">
                <span id="main_0_contentpanel_2_lvCommentListing_lbComment_0">This is my test comment</span>
            </div>
        </div>
    </div>
    <span id="main_0_contentpanel_2_timerComment" style="visibility: hidden; display: none;">
    </span>
</div>
<span id="main_0_contentpanel_2_lblMonitoredComment" class="errortext"></span>
<div id="main_0_contentpanel_3_pnlCommentForm" class="module-bg-comments-form">
  <div class="logged-in-user"><p>Comments will display as from <strong>Dan Persson</strong></p></div>  
  <div class="scfForm" id="form_E5274D1E8858413587441DEA3FFE92CE">
        <input type="hidden" name="main_0$contentpanel_3$form_E5274D1E8858413587441DEA3FFE92CE$formreference" id="main_0_contentpanel_3_form_E5274D1E8858413587441DEA3FFE92CE_formreference" value="form_E5274D1E8858413587441DEA3FFE92CE"><input type="hidden" name="main_0$contentpanel_3$form_E5274D1E8858413587441DEA3FFE92CE$form_E5274D1E8858413587441DEA3FFE92CE_eventcount" id="main_0_contentpanel_3_form_E5274D1E8858413587441DEA3FFE92CE_form_E5274D1E8858413587441DEA3FFE92CE_eventcount"><h1 class="scfTitleBorder">
                </h1>
        <div class="scfIntroBorder">
        </div>
        <div id="main_0_contentpanel_3_form_E5274D1E8858413587441DEA3FFE92CE__summary" class="scfValidationSummary" style="display: none;">
        </div>
        <div id="main_0_contentpanel_3_form_E5274D1E8858413587441DEA3FFE92CE__submitSummary" class="scfSubmitSummary" style="color: Red;">
        </div>
        <div onkeypress="javascript:return WebForm_FireDefaultButton(event, 'main_0_contentpanel_3_form_E5274D1E8858413587441DEA3FFE92CE_form_E5274D1E8858413587441DEA3FFE92CE_submit')">
            <div class="scfSectionBorder">
                <fieldset class="scfSectionBorderAsFieldSet">
                    <div class="scfSectionContent">
                        
                        
                        <div id="main_0_contentpanel_3_form_E5274D1E8858413587441DEA3FFE92CE_field_0613A7FB0208442EA6F1A19A5D99FFCB_scope" class="scfMultipleLineTextBorder fieldid.%7b0613A7FB-0208-442E-A6F1-A19A5D99FFCB%7d name.Comment">
                            <label for="main_0_contentpanel_3_form_E5274D1E8858413587441DEA3FFE92CE_field_0613A7FB0208442EA6F1A19A5D99FFCB" id="main_0_contentpanel_3_form_E5274D1E8858413587441DEA3FFE92CE_field_0613A7FB0208442EA6F1A19A5D99FFCB_text" class="scfMultipleLineTextLabel">
                                Comment:</label><div class="scfMultipleLineGeneralPanel">
                                    <textarea name="main_0$contentpanel_3$form_E5274D1E8858413587441DEA3FFE92CE$field_0613A7FB0208442EA6F1A19A5D99FFCB" rows="4" cols="20" id="main_0_contentpanel_3_form_E5274D1E8858413587441DEA3FFE92CE_field_0613A7FB0208442EA6F1A19A5D99FFCB" class="scfMultipleLineTextBox"></textarea><span class="scfMultipleLineTextUsefulInfo" style="display: none;"></span><span id="main_0_contentpanel_3_form_E5274D1E8858413587441DEA3FFE92CE_field_0613A7FB0208442EA6F1A19A5D99FFCB6ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7b0613A7FB-0208-442E-A6F1-A19A5D99FFCB%7d inner.1" style="display: none;">Comment: must have at least 0 and no more than 512 characters.</span><span id="main_0_contentpanel_3_form_E5274D1E8858413587441DEA3FFE92CE_field_0613A7FB0208442EA6F1A19A5D99FFCB070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b0613A7FB-0208-442E-A6F1-A19A5D99FFCB%7d inner.1" style="display: none;">The value of the Comment: field is not valid.</span>
                                </div>
                            <span class="scfRequired">*</span>
                        </div>
                        
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="scfFooterBorder">
        </div>
        <div>
            <input type="submit" name="main_0$contentpanel_3$form_E5274D1E8858413587441DEA3FFE92CE$form_E5274D1E8858413587441DEA3FFE92CE_submit" value="Submit" onclick="$scw.webform.lastSubmit = this.id;WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;main_0$contentpanel_3$form_E5274D1E8858413587441DEA3FFE92CE$form_E5274D1E8858413587441DEA3FFE92CE_submit&quot;, &quot;&quot;, true, &quot;form_E5274D1E8858413587441DEA3FFE92CE_submit&quot;, &quot;&quot;, false, false));$scw.webform.validators.setFocusToFirstNotValid('form_E5274D1E8858413587441DEA3FFE92CE_submit')" id="main_0_contentpanel_3_form_E5274D1E8858413587441DEA3FFE92CE_form_E5274D1E8858413587441DEA3FFE92CE_submit" class="scfSubmitButton">
        </div>
    </div>
</div>
<!--==================== /Module: Blog: Blog Post =============================-->

