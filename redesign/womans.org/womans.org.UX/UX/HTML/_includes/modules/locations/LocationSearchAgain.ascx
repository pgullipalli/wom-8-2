<!--==================== Module: Locations: Location Search Again =============================-->
<div class="module-lc-search core-search callout collapse-for-mobile">
    <div class="reg-callout grid">
        <h3>Filter Your Results</h3>	
		<div class="twelve columns">
			<label class="label">Location Name</label>
			<input type="text" class="textbox">
		</div>
		<div class="twelve columns">
			<label class="label">City</label>
			<div class="selectbox">
				<select class="selectboxdiv">
					<option value="">Select a City</option>
					<option value="Cambridge, MA">Cambridge, MA</option>
					<option value="Hiawatha, IA">Hiawatha, IA</option>
					<option value="Old Katy, TX">Old Katy, TX</option>
				</select>    
				<div class="out"></div>
			</div>
		</div>
		<div class="twelve columns">
			<label class="label">Services</label>
			<div class="selectbox">
				<select class="selectboxdiv">
					<option value="">Select a Service</option>
				</select>
				<div class="out"></div>
			</div>
		</div>
		<div class="twelve columns">
			<label class="label">Location Type</label>
			<div class="selectbox">
				<select class="selectboxdiv">
					<option value="">Select a Location Type</option>
					<option value="clinic">Clinic</option>
					<option value="hospital">Hospital</option>
				</select>
				<div class="out"></div>
			</div>
		</div>
		<div class="twelve columns">
			<label class="label">Zip Code</label>
			<input type="text" placeholder="Enter a Zip Code" class="textbox">
			<span class="errortext" style="display:none;">Zip code must be 5 digits.</span>
			<span class="errortext" style="display:none;">Please enter zip code.</span>
		</div>
		<div class="twelve columns">
			<label class="label">Within</label>
			<div class="selectbox">
				<select class="selectboxdiv">
					<option value="">Select a Radius</option>
					<option value="1">1 mile</option>
					<option value="5">5 miles</option>
					<option value="10">10 miles</option>
				</select> 
				<div class="out"></div>
			</div>
		</div>
		<div class="twelve columns">
			<input type="submit" value="Search" class="button" />
		</div>
	</div>
</div>
<!--==================== /Module: Locations: Location Search Again =============================-->
