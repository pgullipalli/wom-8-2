<!--==================== Module: Locations: Location Quick Search =============================-->
<div class="module-lc-quick-search core-quick-search core-search callout collapse-for-mobile">
    <div class="reg-callout grid">
		<h3>Find a Location</h3>
		<div class="twelve columns">
			<label class="label">Location Name</label>
			<input type="text" placeholder="Enter a Location Name" class="textbox">
		</div>
		<div class="twelve columns">
			<label class="label">Service</label>
			<div class="selectbox">
				<select class="selectboxdiv">
					<option value="">Select a Service</option>
				</select>
				<div class="out"></div>
			</div>
		</div>
		<div class="twelve columns">
			<a href="/location-directory/">Advanced Search</a>
		</div>
		<div class="twelve columns">
			<input type="submit" value="Search" class="button"> 
		</div>
    </div>
</div>
<!--==================== /Module: Locations: Location Quick Search =============================-->
