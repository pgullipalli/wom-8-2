<div class="services-menu grid hide-on-phones">
	<ul>
		<li class="baby">
			<a href="#" original-title="Baby">Baby</a>
		</li>
		<li class="children-and-men">
			<a href="#" original-title="Children &amp; Men">Children &amp; Men</a>
		</li>
		<li class="gynecology">
			<a href="#" original-title="Gynecology">Gynecology</a>
		</li>
		<li class="wellness-and-nutrition">
			<a href="#" original-title="Wellness &amp; Nutrition">Wellness &amp; Nutrition</a>
		</li>
		<li class="bone-and-joint">
			<a href="#" original-title="Bone &amp; Joint">Bone &amp; Joint</a>
		</li>
		<li class="cancer">
			<a href="#" original-title="Cancer">Cancer</a>
		</li>
		<li class="surgery">
			<a href="#" original-title="Surgery">Surgery</a>
		</li>
	</ul>

	<div class="services-menu-toggle toggle-button"></div>
</div>

<div class="services-menu grid tablet-display">
	<ul>
		<li class="baby">
			<a href="#" original-title="Baby">Baby</a>
		</li>
		<li class="children-and-men">
			<a href="#" original-title="Children &amp; Men">Children &amp; Men</a>
		</li>
		<li class="gynecology">
			<a href="#" original-title="Gynecology">Gynecology</a>
		</li>
		<li class="wellness-and-nutrition">
			<a href="#" original-title="Wellness &amp; Nutrition">Wellness &amp; Nutrition</a>
		</li>
		<li class="bone-and-joint">
			<a href="#" original-title="Bone &amp; Joint">Bone &amp; Joint</a>
		</li>
		<li class="cancer">
			<a href="#" original-title="Cancer">Cancer</a>
		</li>
		<li class="surgery">
			<a href="#" original-title="Surgery">Surgery</a>
		</li>
	</ul>

	<div class="services-menu-toggle tablet"></div>
</div>