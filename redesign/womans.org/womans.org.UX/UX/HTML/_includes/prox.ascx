<!-- Mobile CODE: added .mobile-menu -->
<div class="its">
    <a href="#" class="prox"><span class="icon"></span>In This Section</a>
</div>
<nav class="left-nav nav-collapse collapse" role="navigation">
	<div class="leftnav-title"><a href="#">Your Birth Experience</a></div>
	<div class="level-1 nav-item"><a href="#">Types of Deliveries</a></div>
	<div class="expanded"><div class="level-1 nav-item on"><a href="#">Labor Options</a></div>
		<div class="level-2 active nav-item">Cesarean Birth</div>
		<div class="level-2 nav-item"><a href="#">Natural Birthing Options</a></div>
		<div class="level-2 nav-item"><a href="#">Vaginal Delivery</a></div>
		<div class="level-2 nav-item"><a href="#">Scheduled Induction</a></div>
	</div>
	<div class="level-1 nav-item"><a href="#">Labor and Delivery</a></div>
	<div class="level-1 nav-item"><a href="#">Managing Your Pain</a></div>
	<div class="level-1 nav-item"><a href="#">Amenities</a></div>
</nav>		