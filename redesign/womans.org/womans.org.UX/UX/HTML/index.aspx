<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/MainSiteHome.Master" AutoEventWireup="true" %>

<%@ Register Src="~/UX/HTML/_includes/slider.ascx" TagName="slider" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageContent" runat="server"> 
	<uc:slider ID="slider" runat="server" />
	<section class="main-content">
		<div class="grid home-content">
		   <div class="four columns homepage-callout-slider-wrapper">
		   		<h2>Woman's is now on Pinterest!</h2>
		   		<div class="homepage-callout-slider">
					<div>
						<div>
							<img src="/assets/images/homepage/pinterest-baby-belly-demo-images.png">
						</div>
						<div>
							<img src="/assets/images/homepage/pinterest-baby-belly-demo-images2.png">
						</div>
					</div>					
				</div>
				<div class="slider-caption">
					<p>Maternity Picture Ideas</p>
					<button class="default-button red" type="button">Pin It</button>
				</div>
				<div class="pinterest-info">
					<ul>
						<li>24 Boards</li>
						<li>358 Pins</li>
						<li>176 Followers</li>
					</ul>
					<a href="http://www.pinterest.com/womanshospital/" class="default-button red" type="button" target="_new">Follow Us On <span class="pinterest-logo">Pinterest</span></a>
				</div>
		   </div>
		   <div class="four columns center-callout-col">
		   		<h2 class="home-accordion-handle">
		   			Latest News
		   			<span class="toggle-icon"></span>
		   		</h2>
		   		<div class="home-accordion-content">
			   		<div class="callout home-callout">
						<div class="callout-event">
							<a href="#">
				
								<span class="news-title">Woman's Hospital named in "Best Places to Work in Healthcare"</span>
							</a>
						</div>

						<div class="callout-event">
							<a href="#">
				
								<span class="news-title">Woman's named 2014 Family Favorite Birthing Hospital </span>
							</a>
						</div>

						<div class="callout-event">
							<a href="#">
				
								<span class="news-title">Woman's Announces The Foundation for Woman's</span>
							</a>
						</div>

						<button type="button" class="default-button pink inset">View More</button>
					</div>	
				</div>
		   </div>
		   <div class="four columns">
		   		<h2 class="home-accordion-handle">
		   			Upcoming Classes &amp; Events
		   			<span class="toggle-icon"></span>
		   		</h2>
		   		<div class="home-accordion-content">
					<div class="callout">
						<div class="callout-event">
							<div class="event-header">
								<a href="#">
									<span class="event-date">22 Jan</span>
									Woman's Ideal Protein Information
								</a>
							</div>

							<p class="callout-event-preview">Hinc disputationi ei nam, mei etudoctus tamquam suscipit te. Enim mediocritatem vel eine, in qui falli minimum. <a href="#" class="read-more">Read More</a> </p>
						</div>

						<div class="callout-event">
							<div class="event-header">
								<a href="#">
									<span class="event-date">22 Jan</span>
									Woman's Ideal Protein Information
								</a>
							</div>

							<p class="callout-event-preview">Hinc disputationi ei nam, mei etudoctus tamquam suscipit te. Enim mediocritatem vel eine, in qui falli minimum. <a href="#" class="read-more">Read More</a> </p>
						</div>

						<button type="button" class="default-button pink inset">View More</button>

					</div>
				</div>
		   </div>

		   <div class="homepage-mobile-container"></div>
		</div>
	</section>	
</asp:Content>