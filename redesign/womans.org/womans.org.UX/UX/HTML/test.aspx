

<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>

<!-- GOOGLE FONTS -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css" /><meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<!--============================================= MedTouch.Base: Include: SEO Data ========================================-->


<title>
	Woman&#39;s | Womans.org
</title>
<meta id="ctl07_hmTitle" content="Woman&amp;#39;s | Womans.org" name="title" />





<!--============================================= /MedTouch.Base: Include: SEO Data ========================================--><meta name="google-site-verification" content="0cAq1Rtvm4CpM1rXKh9yLVUomUAcQc0-d4NuAJfz0_o" />



	
	<!-- Mobile Device Scale/Zoom -->	
    <!--meta name="viewport" content="width=device-width"-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" /><meta name="Copyright" />
	
	<!-- Mobile Device Scale/Zoom -->	
    <!--meta name="viewport" content="width=device-width"-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

    <!-- Fav Icons -->
	<link rel="shortcut icon" href="/assets/images/favicon.ico?v=2" />
	

	<!-- Stylesheets -->
    <link rel="stylesheet" href="/assets/css/styles.css" /><link rel="stylesheet" href="/assets/css/modules.css" /><link rel="stylesheet" href="/assets/css/mobile.css" />	
	

	<!--<script src="/assets/js/prefixfree.min.js"></script>-->
        <!--[if lt IE 9]>
           <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>	
        <![endif]-->

	
        

    
<!--============================================= MedTouch.Base: Include: User Edited Script =============================================-->

<!--============================================= /MedTouch.Base: Include: User Edited Script =============================================-->
<link href="/sitecore%20modules/shell/Web%20Forms%20for%20Marketers/themes/MedTouch.css?v=17072012" rel="stylesheet" type="text/css" /><link href="/sitecore%20modules/shell/Web%20Forms%20for%20Marketers/themes/colors//jquery-ui.custom.Default.css" rel="stylesheet" type="text/css" /><link href="/sitecore%20modules/shell/Web%20Forms%20for%20Marketers/themes/colors/Default.css" rel="stylesheet" type="text/css" /><link href="/sitecore%20modules/shell/Web%20Forms%20for%20Marketers/themes/Custom.css" rel="stylesheet" type="text/css" /><link href="/layouts/system/visitoridentificationextension.aspx" rel="stylesheet" type="text/css" /><title>

</title></head>
<body>
    
<!--============================================= MedTouch.Base: Include: User Edited Script =============================================-->

<!--============================================= /MedTouch.Base: Include: User Edited Script =============================================-->

    <form method="post" action="/" onsubmit="javascript:return WebForm_OnSubmit();" id="mainform">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTc3NjI4NDk4Mg9kFgQCAQ9kFgICBQ9kFgJmD2QWAgIFDxYGHgdjb250ZW50BRhXb21hbiYjMzk7cyB8IFdvbWFucy5vcmceBG5hbWUFBXRpdGxlHgdWaXNpYmxlZ2QCBRBkZBYEAgMPZBYCZg9kFgJmD2QWEGYPZBYCZg9kFgICAQ8WAh4LXyFJdGVtQ291bnQCBhYMZg9kFgQCAQ8WAh4EVGV4dAUaPGxpIGNsYXNzPSJmYWNlYm9vay1saW5rIj5kAgUPFgIfBAUFPC9saT5kAgEPZBYEAgEPFgIfBAUZPGxpIGNsYXNzPSJ0d2l0dGVyLWxpbmsiPmQCBQ8WAh8EBQU8L2xpPmQCAg9kFgQCAQ8WAh8EBRk8bGkgY2xhc3M9InlvdXR1YmUtbGluayI+ZAIFDxYCHwQFBTwvbGk+ZAIDD2QWBAIBDxYCHwQFFDxsaSBjbGFzcz0id3AtbGluayI+ZAIFDxYCHwQFBTwvbGk+ZAIED2QWBAIBDxYCHwQFGzxsaSBjbGFzcz0icGludGVyZXN0LWxpbmsiPmQCBQ8WAh8EBQU8L2xpPmQCBQ9kFgYCAQ8WAh8EBRs8bGkgY2xhc3M9Imluc3RhZ3JhbS1saW5rIj5kAgMPD2QWAh4FY2xhc3MFBGxhc3RkAgUPFgIfBAUFPC9saT5kAgEPZBYCZg9kFgwCAQ9kFgJmD2QWAmYPZBYGAgEPZBYCZg9kFgICAQ8PFgQfBGUfAmhkZAIDDw9kFgQeB29uZm9jdXMFMWlmICh0aGlzLnZhbHVlID09ICdTZWFyY2ggVGVybScpIHRoaXMudmFsdWUgPSAnJzseBm9uYmx1cgUxaWYgKHRoaXMudmFsdWUgPT0gJycpIHRoaXMudmFsdWUgPSAnU2VhcmNoIFRlcm0nO2QCBQ8PFgIfBAUCR09kZAIDDxYCHwMCBBYIZg9kFgYCAQ8WAh8DAgUWCgIBD2QWAmYPFQINL291ci1zZXJ2aWNlcwxPdXIgU2VydmljZXNkAgIPZBYCZg8VAhQvcGh5c2ljaWFuLWRpcmVjdG9yeQ1GaW5kIGEgRG9jdG9yZAIDD2QWAmYPFQITL2NsYXNzZXMtYW5kLWV2ZW50cxRDbGFzc2VzICZhbXA7IEV2ZW50c2QCBA9kFgJmDxUCEy9sb2NhdGlvbnMtYW5kLW1hcHMUTG9jYXRpb25zICZhbXA7IE1hcHNkAgUPZBYCZg8VAhYvcGF0aWVudHMtYW5kLXZpc2l0b3JzF1BhdGllbnRzICZhbXA7IFZpc2l0b3JzZAICDxUBEldvbWFuJiMzOTtzIEZvci4uLmQCAw8WAh8DAgUWCmYPZBYCZg8VAhMvZm9yLWpvYi1hcHBsaWNhbnRzDkpvYiBBcHBsaWNhbnRzZAIBD2QWAmYPFQIWL3BhdGllbnRzLWFuZC12aXNpdG9ycxdQYXRpZW50cyAmYW1wOyBWaXNpdG9yc2QCAg9kFgJmDxUCDi9mb3ItZW1wbG95ZWVzCUVtcGxveWVlc2QCAw9kFgJmDxUCFy9mb3ItYm9hcmQtb2YtZGlyZWN0b3JzEkJvYXJkIG9mIERpcmVjdG9yc2QCBA9kFgJmDxUCHS9mb3Itam9iLWFwcGxpY2FudHMvdm9sdW50ZWVyClZvbHVudGVlcnNkAgEPZBYGAgEPFgQfA2YfAmhkAgIPFQEUVmlzaXRpbmcgV29tYW4mIzM5O3NkAgMPFgIfAwIFFgpmD2QWAmYPFQITL2xvY2F0aW9ucy1hbmQtbWFwcwxHZXR0aW5nIEhlcmVkAgEPZBYCZg8VAhsvbG9jYXRpb25zLWFuZC1tYXBzL3BhcmtpbmcHUGFya2luZ2QCAg9kFgJmDxUCMi9wYXRpZW50cy1hbmQtdmlzaXRvcnMvdmlzaXRpbmctaG91cnMtYW5kLXBvbGljaWVzDlZpc2l0aW5nIEhvdXJzZAIDD2QWAmYPFQIdL3BhdGllbnRzLWFuZC12aXNpdG9ycy9kaW5pbmcGRGluaW5nZAIED2QWAmYPFQJHL291ci1zZXJ2aWNlcy9icmVhc3QtY2FyZS9icmVhc3QtaW1hZ2luZy1zZXJ2aWNlcy9tYW1tb2dyYXBoeS1sb2NhdGlvbnMTTWFtbW9ncmFtIExvY2F0aW9uc2QCAg9kFgYCAQ8WBB8DZh8CaGQCAg8VAQ1Qb3B1bGFyIExpbmtzZAIDDxYCHwMCBRYKZg9kFgJmDxUCDS9hYm91dC13b21hbnMRQWJvdXQgV29tYW4mIzM5O3NkAgEPZBYCZg8VAigvcGF0aWVudHMtYW5kLXZpc2l0b3JzL2NvbnRhY3QtYS1wYXRpZW50FENvbnRhY3RpbmcgYSBQYXRpZW50ZAICD2QWAmYPFQIQL2NvbnRhY3QtdXMtZm9ybQpDb250YWN0IFVzZAIDD2QWAmYPFQJ7L291ci1zZXJ2aWNlcy93ZWxsbmVzcy1hbmQtbnV0cml0aW9uL2ZpdG5lc3MvcGVyc29uYWwtdHJhaW5pbmcvc21hbGwtZ3JvdXAtdHJhaW5pbmcvcHJlLWFuZC1wb3N0LXByZWduYW5jeS1maXRuZXNzLXByb2dyYW1zDVByZW5hdGFsIFlvZ2FkAgQPZBYCZg8VAhMvZm9yLWpvYi1hcHBsaWNhbnRzDUpvaW4gT3VyIFRlYW1kAgMPZBYGAgEPFgQfA2YfAmhkAgIPFQEQUGF0aWVudCBTZXJ2aWNlc2QCAw8WAh8DAgUWCmYPZBYCZg8VAjsvcGF0aWVudHMtYW5kLXZpc2l0b3JzL2JpbGxpbmctYW5kLWluc3VyYW5jZS9iaWxsLXBheS1sb2dpbg1QYXkgWW91ciBCaWxsZAIBD2QWAmYPFQIlL3BhdGllbnRzLWFuZC12aXNpdG9ycy9wYXRpZW50LXBvcnRhbBdNeUhlYWx0aCBQYXRpZW50IFBvcnRhbGQCAg9kFgJmDxUCIy9wYXRpZW50cy1hbmQtdmlzaXRvcnMvcmVnaXN0cmF0aW9uDFJlZ2lzdHJhdGlvbmQCAw9kFgJmDxUCMS9wYXRpZW50cy1hbmQtdmlzaXRvcnMvYnJvY2h1cmVzLWFuZC1wdWJsaWNhdGlvbnMcQnJvY2h1cmVzICZhbXA7IFB1YmxpY2F0aW9uc2QCBA9kFgJmDxUCMy9wYXRpZW50cy1hbmQtdmlzaXRvcnMvcGF0aWVudC1ndWlkZS9wYXRpZW50LXJpZ2h0cwtZb3VyIFJpZ2h0c2QCBQ8PFgIeCEltYWdlVXJsBVgvfi9tZWRpYS9pbWFnZXMvd29tYW5zL2RldmVsb3BtZW50L2RldmVsb3BtZW50IGxhbmRpbmcgaW1hZ2VzL3ByZW1pZV9oYW5kc18zNTMgeCAxNzAuanBnZGQCBw8WAh8EBRRHaXZpbmcgT3Bwb3J0dW5pdGllc2QCCQ8WAh8EBYkBVGhlcmUgYXJlIG1hbnkgd2F5cyB0byBzdXBwb3J0IFdvbWFuJ3MgSG9zcGl0YWwuIFlvdXIgZ2lmdCBwcm92aWRlcyBjcml0aWNhbCBzZXJ2aWNlcyBhbmQgcHJvZ3JhbXMgZm9yIHdvbWVuIGFuZCBiYWJpZXMgaW4gb3VyIGNvbW11bml0eS5kAgsPDxYEHgtOYXZpZ2F0ZVVybAUYL2dpdmluZy1hbmQtdm9sdW50ZWVyaW5nHwQFC1JlYWQgTW9yZSA+ZGQCAg8WAh8EBQRNZW51ZAIDDw8WAh8CZ2QWAgIBDw8WBB8JBQEvHgdUb29sVGlwBQtXb21hbiYjMzk7c2QWAgIBDxYCHwJoZAIFD2QWAmYPZBYCAgEPZBYGAgEPZBYCAgEPDxYEHwRlHwJoZGQCAw8PZBYEHwYFMWlmICh0aGlzLnZhbHVlID09ICdTZWFyY2ggVGVybScpIHRoaXMudmFsdWUgPSAnJzsfBwUxaWYgKHRoaXMudmFsdWUgPT0gJycpIHRoaXMudmFsdWUgPSAnU2VhcmNoIFRlcm0nO2QCBQ8PFgIfBAUCR09kZAIGD2QWAmYPZBYCAgEPFgIfAwIFFgpmD2QWAgIBD2QWAgIBDxYCHgRocmVmBQ0vb3VyLXNlcnZpY2VzFgJmDxYCHwQFDE91ciBTZXJ2aWNlc2QCAQ9kFgICAQ9kFgICAQ8WAh8LBRQvcGh5c2ljaWFuLWRpcmVjdG9yeRYCZg8WAh8EBQ1GaW5kIGEgRG9jdG9yZAICD2QWAgIBD2QWAgIBDxYCHwsFEy9jbGFzc2VzLWFuZC1ldmVudHMWAmYPFgIfBAUUQ2xhc3NlcyAmYW1wOyBFdmVudHNkAgMPZBYCAgEPZBYCAgEPFgIfCwUTL2xvY2F0aW9ucy1hbmQtbWFwcxYCZg8WAh8EBRRMb2NhdGlvbnMgJmFtcDsgTWFwc2QCBA9kFgICAQ9kFgICAQ8WAh8LBRYvcGF0aWVudHMtYW5kLXZpc2l0b3JzFgJmDxYCHwQFF1BhdGllbnRzICZhbXA7IFZpc2l0b3JzZAIID2QWAmYPZBYCZg9kFgICAQ8WAh8DAgcWDmYPZBYCZg8VBhRib25lLWFuZC1qb2ludCBmaWZ0aBwvb3VyLXNlcnZpY2VzL2JvbmUtYW5kLWpvaW50lgE8aW1nIHNyYz0iL34vbWVkaWEvaW1hZ2VzL3dvbWFucy9ib25lIGFuZCBqb2ludC9ob3Zlci93YWxraW5nX3JvYWRfMzEweDIzMy5qcGc/aD0yMzMmYW1wO3c9MzEwIiBhbHQ9IndhbGtpbmdfcm9hZF8zMTB4MjMzIiB3aWR0aD0iMzEwIiBoZWlnaHQ9IjIzMyIgLz57PHA+V2hldGhlciB5b3UncmUgYW4gYXRobGV0ZSwgd2Vla2VuZCB3YXJyaW9yIG9yIG1hbmFnaW5nIG9zdGVvcG9yb3Npcywgd2UgY2FuIGtlZXAgeW91IG1vdmluZyB3aXRoIHRoZXJhcHkgb3Igc3VyZ2VyeS48L3A+CVJlYWQgbW9yZRBCb25lICZhbXA7IEpvaW50ZAIBD2QWAmYPFQYMY2FuY2VyIHNpeHRoFC9vdXItc2VydmljZXMvY2FuY2VypAE8aW1nIHNyYz0iL34vbWVkaWEvaW1hZ2VzL3dvbWFucy9jYW5jZXIvaG92ZXIvY2FuY2VyX2JhbGRfYmxhY2tfNDBzXzI4MCB4IDIwMC5qcGc/aD0yMzMmYW1wO3c9MzEwIiBhbHQ9ImNhbmNlcl9iYWxkX2JsYWNrXzQwc18yODAgeCAyMDAiIHdpZHRoPSIzMTAiIGhlaWdodD0iMjMzIiAvPnw8cD5Gcm9tJm5ic3A7c2NyZWVuaW5nIGFuZCBkaWFnbm9zaXMgdG8gdHJlYXRtZW50IGFuZCBzdXJnZXJ5LCB3ZSdyZSBoZXJlIHRvIHN1cHBvcnQgeW91IHRocm91Z2hvdXQgeW91ciBjYW5jZXIgam91cm5leS48L3A+CVJlYWQgbW9yZQZDYW5jZXJkAgIPZBYCZg8VBhZjaGlsZHJlbi1hbmQtbWVuIHRoaXJkHi9vdXItc2VydmljZXMvY2hpbGRyZW4tYW5kLW1lbpoBPGltZyBzcmM9Ii9+L21lZGlhL2ltYWdlcy93b21hbnMvbWVuIGFuZCBjaGlsZHJlbi9tYW5fYmFieV9oYW5kc18zMTAgeCAyMzMuanBnP2g9MjMzJmFtcDt3PTMxMCIgYWx0PSJtYW5fYmFieV9oYW5kc18zMTAgeCAyMzMiIHdpZHRoPSIzMTAiIGhlaWdodD0iMjMzIiAvPowBPHA+TGV0IG91ciBmYW1pbHkgY2FyZSBmb3IgeW91cnMgd2l0aCBpbWFnaW5nIGFuZCBsYWIgc2VydmljZXMsIHNwZWNpYWxpemVkIG1lZGljYWwgY2xpbmljcyBhbmQgaGVhbHRoIHNjcmVlbmluZ3MgZm9yIG1lbiBhbmQgY2hpbGRyZW4uIDwvcD4JUmVhZCBtb3JlEkNoaWxkcmVuICZhbXA7IE1lbmQCAw9kFgJmDxUGEGd5bmVjb2xvZ3kgZmlyc3QYL291ci1zZXJ2aWNlcy9neW5lY29sb2d5igE8aW1nIHNyYz0iL34vbWVkaWEvaW1hZ2VzL3dvbWFucy9neW5lY29sb2d5L3N0ZXRoZXNjb3BlXzMxMHgyMzMuanBnP2g9MjMzJmFtcDt3PTMxMCIgYWx0PSJzdGV0aGVzY29wZV8zMTB4MjMzIiB3aWR0aD0iMzEwIiBoZWlnaHQ9IjIzMyIgLz5xPHA+V29tYW4ncyBrbm93cyBob3cgdG8gY2FyZSBmb3Igd29tZW4sIGZyb20geW91ciBmaXJzdCBneW5lY29sb2dpY2FsIHZpc2l0IHRvIG1lbm9wYXVzZSB0byBhZHZhbmNlZCBzdXJnZXJ5LjwvcD4JUmVhZCBNb3JlCkd5bmVjb2xvZ3lkAgQPZBYCZg8VBgtiYWJ5IHNlY29uZB0vb3VyLXNlcnZpY2VzL21vdGhlci1hbmQtYmFieYYBPGltZyBzcmM9Ii9+L21lZGlhL2ltYWdlcy93b21hbnMvYmFieS9ob3Zlci9iYWJ5X2J1bXBfMzEweDIzMy5qcGc/aD0yMzMmYW1wO3c9MzEwIiBhbHQ9ImJhYnlfYnVtcF8zMTB4MjMzIiB3aWR0aD0iMzEwIiBoZWlnaHQ9IjIzMyIgLz6EATxwPkV2ZXJ5IGRlbGl2ZXJ5IGlzIGEgc3BlY2lhbCBkZWxpdmVyeSBhdCBXb21hbidzLiBXZSB1bmRlcnN0YW5kIHlvdXIgbmVlZHMsIGZyb20gZmVydGlsaXR5IHRvIGJyZWFzdGZlZWRpbmcgYW5kIG5ld2Jvcm4gY2FyZS4gPC9wPglSZWFkIG1vcmURTW90aGVyICZhbXA7IEJhYnlkAgUPZBYCZg8VBgxzdXJnZXJ5IGxhc3QVL291ci1zZXJ2aWNlcy9zdXJnZXJ5jwE8aW1nIHNyYz0iL34vbWVkaWEvaW1hZ2VzL3dvbWFucy9zdXJnZXJ5L2hvdmVyL3N1cmdlcnlfbWFza18zMTB4MjMzLmpwZz9oPTIzMyZhbXA7dz0zMTAiIGFsdD0ic3VyZ2VyeV9tYXNrXzMxMHgyMzMiIHdpZHRoPSIzMTAiIGhlaWdodD0iMjMzIiAvPoUBPHA+Q2hvb3NlIFdvbWFuJ3MgZm9yIHlvdXIgZ2VuZXJhbCBzdXJnZXJ5IG9yIHNwZWNpYWxpemVkIGd5bmVjb2xvZ2ljYWwsIHVyb2xvZ2ljYWwsIG9ydGhvcGVkaWMsIHdlaWdodCBsb3NzIG9yIGNvc21ldGljIHN1cmdlcnkuPC9wPglSZWFkIG1vcmUHU3VyZ2VyeWQCBg9kFgJmDxUGHXdlbGxuZXNzLWFuZC1udXRyaXRpb24gZm91cnRoJC9vdXItc2VydmljZXMvd2VsbG5lc3MtYW5kLW51dHJpdGlvbr4BPGltZyBzcmM9Ii9+L21lZGlhL2ltYWdlcy93b21hbnMvd2VsbG5lc3MgYW5kIG51dHJpdGlvbi9ob3Zlci93ZWlnaHRzX2FwcGxlX3RhcGVtZWFzdXJlMl8zMTAgeCAyMzMuanBnP2g9MjMzJmFtcDt3PTMxMCIgYWx0PSJ3ZWlnaHRzX2FwcGxlX3RhcGVtZWFzdXJlMl8zMTAgeCAyMzMiIHdpZHRoPSIzMTAiIGhlaWdodD0iMjMzIiAvPms8cD5CdWlsZCBhIGhlYWx0aHkgYm9keSB0aHJvdWdoIGZpdG5lc3MsIG51dHJpdGlvbiwgdGhlcmFweSwgaGVhbHRoIHNjcmVlbmluZ3MgYW5kIHJlbGF4YXRpb24gc2VydmljZXMuPC9wPglSZWFkIG1vcmUYV2VsbG5lc3MgJmFtcDsgTnV0cml0aW9uZAIKD2QWAmYPZBYCZg9kFgoCAQ8WAh8DAgUWCmYPZBYEZg8VAZwBPGltZyBzcmM9Ii9+L21lZGlhL2ltYWdlcy93b21hbnMvaG9tZXBhZ2Uvc2xpZGVyIGltYWdlcy93YWxraW5nX3BhdGhfMTIxMHg0OTIuanBnP2g9NDkyJmFtcDt3PTEyMTAiIGFsdD0id2Fsa2luZ19wYXRoXzEyMTB4NDkyIiB3aWR0aD0iMTIxMCIgaGVpZ2h0PSI0OTIiIC8+ZAIBD2QWAmYPFQQkL291ci1zZXJ2aWNlcy93ZWxsbmVzcy1hbmQtbnV0cml0aW9uGFdlbGxuZXNzICZhbXA7IE51dHJpdGlvbiFFeGNlcHRpb25hbCBDYXJlLCBDZW50ZXJlZCBvbiBZb3VJPGEgaHJlZj0iL291ci1zZXJ2aWNlcy93ZWxsbmVzcy1hbmQtbnV0cml0aW9uIj5XZWxsbmVzcyBhbmQgTnV0cml0aW9uPC9hPmQCAQ9kFgRmDxUBrgE8aW1nIHNyYz0iL34vbWVkaWEvaW1hZ2VzL3dvbWFucy9ob21lcGFnZS9zbGlkZXIgaW1hZ2VzL3ByZWduYW50X2JhbmFuYWxlYXZlc18xMjEweDQ5Mi5qcGc/aD00OTImYW1wO3c9MTIxMCIgYWx0PSJwcmVnbmFudF9iYW5hbmFsZWF2ZXNfMTIxMHg0OTIiIHdpZHRoPSIxMjEwIiBoZWlnaHQ9IjQ5MiIgLz5kAgEPZBYCZg8VBB0vb3VyLXNlcnZpY2VzL21vdGhlci1hbmQtYmFieQlQcmVnbmFuY3keV2UgU3RheSBGb2N1c2VkIG9uIHRoZSBEZXRhaWxzOzxhIGhyZWY9Ii9vdXItc2VydmljZXMvbW90aGVyLWFuZC1iYWJ5Ij5Nb3RoZXIgYW5kIEJhYnk8L2E+ZAICD2QWBGYPFQGcATxpbWcgc3JjPSIvfi9tZWRpYS9pbWFnZXMvd29tYW5zL2hvbWVwYWdlL3NsaWRlciBpbWFnZXMvd29tYW5fY29mZmVlXzEyMTB4NDkyLmpwZz9oPTQ5MiZhbXA7dz0xMjEwIiBhbHQ9IndvbWFuX2NvZmZlZV8xMjEweDQ5MiIgd2lkdGg9IjEyMTAiIGhlaWdodD0iNDkyIiAvPmQCAQ9kFgJmDxUEFS9vdXItc2VydmljZXMvc3VyZ2VyeQdTdXJnZXJ5JEJlY2F1c2UgWW91IEtub3cgVXMsIGFuZCBXZSBLbm93IFlvdSs8YSBocmVmPSIvb3VyLXNlcnZpY2VzL3N1cmdlcnkiPlN1cmdlcnk8L2E+ZAIDD2QWBGYPFQGsATxpbWcgc3JjPSIvfi9tZWRpYS9pbWFnZXMvd29tYW5zL2hvbWVwYWdlL3NsaWRlciBpbWFnZXMvaG9tZV90ZW1wbGF0ZV90b191c2ViYWJ5X3RvZXMuanBnP2g9NDkyJmFtcDt3PTEyMTAiIGFsdD0iSE9NRV90ZW1wbGF0ZV90b191c2ViYWJ5X3RvZXMiIHdpZHRoPSIxMjEwIiBoZWlnaHQ9IjQ5MiIgLz5kAgEPZBYCZg8VBCovb3VyLXNlcnZpY2VzL21vdGhlci1hbmQtYmFieS9uZXdib3JuLWNhcmUJQmFieSBDYXJlHkxpdHRsZSBUb3VjaGVzIFlvdSdsbCBSZW1lbWJlckU8YSBocmVmPSIvb3VyLXNlcnZpY2VzL21vdGhlci1hbmQtYmFieS9uZXdib3JuLWNhcmUiPk5ld2Jvcm4gQ2FyZTwvYT5kAgQPZBYEZg8VAbABPGltZyBzcmM9Ii9+L21lZGlhL2ltYWdlcy93b21hbnMvaG9tZXBhZ2Uvc2xpZGVyIGltYWdlcy9ob21lX3RlbXBsYXRlX3RvX3VzZV9waW5rX3NjYXJmLmpwZz9oPTQ5MiZhbXA7dz0xMjEwIiBhbHQ9IkhPTUVfdGVtcGxhdGVfdG9fdXNlX3Bpbmtfc2NhcmYiIHdpZHRoPSIxMjEwIiBoZWlnaHQ9IjQ5MiIgLz5kAgEPZBYCZg8VBBkvb3VyLXNlcnZpY2VzL2JyZWFzdC1jYXJlC0JyZWFzdCBDYXJlJUV2ZXJ5IE1vbnRoIGlzIEFib3V0IENhbmNlciBBd2FyZW5lc3MzPGEgaHJlZj0iL291ci1zZXJ2aWNlcy9icmVhc3QtY2FyZSI+QnJlYXN0IENhcmU8L2E+ZAIDDxYCHwMCBxYOZg9kFgJmDxUDFGJvbmUtYW5kLWpvaW50IGZpZnRoHC9vdXItc2VydmljZXMvYm9uZS1hbmQtam9pbnQQQm9uZSAmYW1wOyBKb2ludGQCAQ9kFgJmDxUDDGNhbmNlciBzaXh0aBQvb3VyLXNlcnZpY2VzL2NhbmNlcgZDYW5jZXJkAgIPZBYCZg8VAxZjaGlsZHJlbi1hbmQtbWVuIHRoaXJkHi9vdXItc2VydmljZXMvY2hpbGRyZW4tYW5kLW1lbhJDaGlsZHJlbiAmYW1wOyBNZW5kAgMPZBYCZg8VAxBneW5lY29sb2d5IGZpcnN0GC9vdXItc2VydmljZXMvZ3luZWNvbG9neQpHeW5lY29sb2d5ZAIED2QWAmYPFQMLYmFieSBzZWNvbmQdL291ci1zZXJ2aWNlcy9tb3RoZXItYW5kLWJhYnkRTW90aGVyICZhbXA7IEJhYnlkAgUPZBYCZg8VAwxzdXJnZXJ5IGxhc3QVL291ci1zZXJ2aWNlcy9zdXJnZXJ5B1N1cmdlcnlkAgYPZBYCZg8VAx13ZWxsbmVzcy1hbmQtbnV0cml0aW9uIGZvdXJ0aCQvb3VyLXNlcnZpY2VzL3dlbGxuZXNzLWFuZC1udXRyaXRpb24YV2VsbG5lc3MgJmFtcDsgTnV0cml0aW9uZAIFD2QWAmYPZBYCZg9kFgYCAQ8WAh8EZWQCAw8WAh8EBfIBPGgzIHN0eWxlPSJ0ZXh0LWFsaWduOiBjZW50ZXI7Ij5Xb21hbidzIFN1cmdlcnk6CjxiciAvPgpCZWNhdXNlIFlvdSBBbHJlYWR5IEtub3cgVXMsIGFuZCBXZSBLbm93IFlvdTwvaDM+CjxpZnJhbWUgd2lkdGg9IjQwMCIgaGVpZ2h0PSIyMjUiIGZyYW1lYm9yZGVyPSIwIiBzcmM9Ii8vd3d3LnlvdXR1YmUuY29tL2VtYmVkLzNzR3ViSS1renhzP3JlbD0wJmFtcDthdXRvaGlkZT0xJmFtcDtzaG93aW5mbz0wIj48L2lmcmFtZT5kAgUPDxYEHwllHwRlZGQCBw9kFgJmD2QWAmYPZBYGAgEPFgIfBAULTGF0ZXN0IE5ld3NkAgMPFCsAAg8WBB4LXyFEYXRhQm91bmRnHwMCA2RkFgZmD2QWAgIBDw8WAh8JBS8vbmV3cy8yMDE0LzA4L3ByZXNzLXJlbGVhc2UtYmVzdC1wbGFjZXMtdG8td29ya2QWAgIBDxYCHwQFQ1dvbWFu4oCZcyBIb3NwaXRhbCBuYW1lZCBpbiDigJxCZXN0IFBsYWNlcyB0byBXb3JrIGluIEhlYWx0aGNhcmXigJ1kAgEPZBYCAgEPDxYCHwkFKy9uZXdzLzIwMTQvMDgvcHJlc3MtcmVsZWFzZS1mYW1pbHktZmF2b3JpdGVkFgICAQ8WAh8EBTdXb21hbuKAmXMgbmFtZWQgMjAxNCBGYW1pbHkgRmF2b3JpdGUgQmlydGhpbmcgSG9zcGl0YWwgZAICD2QWAgIBDw8WAh8JBTEvbmV3cy8yMDE0LzA4L3ByZXNzLXJlbGVhc2UtZm91bmRhdGlvbi1mb3Itd29tYW5zZBYCAgEPFgIfBAUyV29tYW4mIzM5O3MgQW5ub3VuY2VzIFRoZSBGb3VuZGF0aW9uIGZvciBXb21hbuKAmXNkAgUPDxYCHwJnZBYCAgEPDxYEHgtQb3N0QmFja1VybAUFL25ld3MfBAUJVmlldyBNb3JlZGQCCQ9kFgJmD2QWAmYPZBYEAgEPFgIfBAUZVXBjb21pbmcgQ2xhc3NlcyAmIEV2ZW50c2QCAw9kFgICAQ8WAh8DAgIWBgIBD2QWBgIBDw8WBB8EBVM8c3BhbiBjbGFzcz0nZXZlbnQtZGF0ZSc+MDI8YnIvPk9jdDwvc3Bhbj5XZWlnaHQgTG9zcyBTdXJnZXJ5OiBXaGF0IFlvdSBTaG91bGQgS25vdx8JBUkvY2xhc3Nlcy1hbmQtZXZlbnRzL3N1cmdlcnkvd2VpZ2h0LWxvc3Mvd2VpZ2h0LWxvc3Mtc3VyZ2VyeT9lc2s9b2N0XzJfd2xzZGQCAw8WAh8EBWJKb2luIHVzIHRvIGxlYXJuIG1vcmUgYWJvdXQgdGhlIGRpZmZlcmVudCB0eXBlcyBvZiB3ZWlnaHQgbG9zcyBzdXJnZXJpZXMgYW5kIGdldCB5b3VyIHF1ZXN0aW9ucy4uLmQCBQ8PFgQfBAUJUmVhZCBNb3JlHwkFSS9jbGFzc2VzLWFuZC1ldmVudHMvc3VyZ2VyeS93ZWlnaHQtbG9zcy93ZWlnaHQtbG9zcy1zdXJnZXJ5P2Vzaz1vY3RfMl93bHNkZAICD2QWBgIBDw8WBB8EBU88c3BhbiBjbGFzcz0nZXZlbnQtZGF0ZSc+MDM8YnIvPk9jdDwvc3Bhbj5CcmVhc3RmZWVkaW5nIFRpcHMgZm9yIE1vbXMgT24tdGhlLUdvHwkFXC9jbGFzc2VzLWFuZC1ldmVudHMvYnJlYXN0ZmVlZGluZy9icmVhc3RmZWVkaW5nLXRpcHMtZm9yLW1vbXMtb24tdGhlLWdvP2Vzaz1vY3RvYmVyXzNfYmZ0aXBzZGQCAw8WAh8EBWNMZWFybiB0aGUgInRyaWNrcyBvZiB0aGUgdHJhZGUiIHRvIGVuc3VyZSB5b3VyIGJhYnkgZ2V0cyB0aGUgYmVuZWZpdHMgb2YgYnJlYXN0IG1pbGsgd2hlbiB5b3UncmUuLi5kAgUPDxYEHwQFCVJlYWQgTW9yZR8JBVwvY2xhc3Nlcy1hbmQtZXZlbnRzL2JyZWFzdGZlZWRpbmcvYnJlYXN0ZmVlZGluZy10aXBzLWZvci1tb21zLW9uLXRoZS1nbz9lc2s9b2N0b2Jlcl8zX2JmdGlwc2RkAgMPZBYCAgEPDxYGHw0FEy9jbGFzc2VzLWFuZC1ldmVudHMfBAUJVmlldyBNb3JlHwJnZGQCBw9kFgJmD2QWAmYPZBYWAgEPFgIfBAWICDxkaXY+PHA+IkFib3V0IDUwJSBvZiB2YWdpbmFsIGNhbmNlciBjYXNlcyBhcmUgZm91bmQgaW4gd29tZW4gNzArLiA8YSBocmVmPSdodHRwOi8vc2VhcmNoLnR3aXR0ZXIuY29tL3NlYXJjaD9xPSUyM0d5bmVjb2xvZ2ljQ2FuY2VyQXdhcmVuZXNzTW9udGgnIHRhcmdldD0nX2JsYW5rJz4jR3luZWNvbG9naWNDYW5jZXJBd2FyZW5lc3NNb250aDwvYT4iPC9wPjwvZGl2PjxkaXY+PHA+IlN1cGVydmlzZSB5b3VyIGJhYnkgZHVyaW5nIHdhbGtlciB1c2UuIDxhIGhyZWY9J2h0dHA6Ly9zZWFyY2gudHdpdHRlci5jb20vc2VhcmNoP3E9JTIzQmFieVNhZmV0eU1vbnRoJyB0YXJnZXQ9J19ibGFuayc+I0JhYnlTYWZldHlNb250aDwvYT4iPC9wPjwvZGl2PjxkaXY+PHA+IkNob29zZSBzb2Z0IHRveXMgZm9yIGNhciByaWRlcyB0aGF0IHdvdWxkbuKAmXQgaHVydCB5b3VyIGJhYnkgaWYgaW52b2x2ZWQgaW4gYSBjcmFzaC4gPGEgaHJlZj0naHR0cDovL3NlYXJjaC50d2l0dGVyLmNvbS9zZWFyY2g/cT0lMjNCYWJ5U2FmZXR5TW9udGgnIHRhcmdldD0nX2JsYW5rJz4jQmFieVNhZmV0eU1vbnRoPC9hPiI8L3A+PC9kaXY+PGRpdj48cD4iQWx3YXlzIHVzZSBzdHJhcHMgd2hlbiB1c2luZyBhIHN0cm9sbGVyLCBzd2luZywgaW5mYW50IGNhcnJpZXIgb3IgaGlnaCBjaGFpci4gPGEgaHJlZj0naHR0cDovL3NlYXJjaC50d2l0dGVyLmNvbS9zZWFyY2g/cT0lMjNCYWJ5U2FmZXR5TW9udGgnIHRhcmdldD0nX2JsYW5rJz4jQmFieVNhZmV0eU1vbnRoPC9hPiI8L3A+PC9kaXY+PGRpdj48cD4iVGhlIGVtb3Rpb25hbCBzdXBwb3J0IG9mIGZhbWlseSwgZnJpZW5kcyBhbmQgb3RoZXJzIGNhbiBoZWxwIHRob3NlIHdobyBhcmUgZGVhbGluZyB3aXRoIGNhbmNlciB0cmVhdG1lbnRzLiA8YSBocmVmPSdodHRwOi8vc2VhcmNoLnR3aXR0ZXIuY29tL3NlYXJjaD9xPSUyM0d5bmVjb2xvZ2ljQ2FuY2VyQXdhcmVuZXNzTW9udGgnIHRhcmdldD0nX2JsYW5rJz4jR3luZWNvbG9naWNDYW5jZXJBd2FyZW5lc3NNb250aDwvYT4iPC9wPjwvZGl2PmQCAw8WAh8EBaoBPGgzPk1ZIFdPTUFOJ1MgRU1BSUwgTkVXU0xFVFRFUjwvaDM+CjxwPlNJR04gVVAgYW5kIGdldCB0aGUgbGF0ZXN0IG9uIHdlaWdodCBsb3NzLCBudXRyaXRpb24sIGhlYWx0aHkgbGl2aW5nLCBmaXRuZXNzLCBtb3RoZXJob29kLCBicmVhc3QgaGVhbHRoIGFuZCBtb3JlIGV2ZXJ5IG1vbnRoITwvcD5kAgUPZBYCZg8PFgIeCHJlbmRlcmVkBtgTlzvMpNGIZBYKAgIPD2QWAh8FBQ5zY2ZUaXRsZUJvcmRlcmQCAw8PZBYCHwUFDnNjZkludHJvQm9yZGVyZAIGD2QWAmYPZBYCZg9kFgJmD2QWBGYPZBYCZg9kFgJmDw8WBB4IQ3NzQ2xhc3MFWXNjZlNpbmdsZUxpbmVUZXh0Qm9yZGVyIGZpZWxkaWQuJTdiRDg5NkE0NUQtNjQxQy00QTQ5LUJEMEUtQjY0QzRBRjQyOTBGJTdkIG5hbWUuRnVsbCtOYW1lHgRfIVNCAgJkFgICAQ9kFgICAg8PFgQfBAU/RnVsbCBOYW1lIG11c3QgaGF2ZSBhdCBsZWFzdCAwIGFuZCBubyBtb3JlIHRoYW4gMjU2IGNoYXJhY3RlcnMuHgxFcnJvck1lc3NhZ2UFP0Z1bGwgTmFtZSBtdXN0IGhhdmUgYXQgbGVhc3QgMCBhbmQgbm8gbW9yZSB0aGFuIDI1NiBjaGFyYWN0ZXJzLmRkAgEPZBYCZg9kFgJmDw8WBB8PBVRzY2ZFbWFpbEJvcmRlciBmaWVsZGlkLiU3YjE4MjBFQzdDLTdCRUUtNDMyMi05Qjg2LUEwNzUzRjEyMjE0NCU3ZCBuYW1lLkVtYWlsK0FkZHJlc3MfEAICZGQCBw8PZBYCHwUFD3NjZkZvb3RlckJvcmRlcmQCCA9kFgJmDw8WCB8EBQtTSUdOIE1FIFVQIR8PBQ9zY2ZTdWJtaXRCdXR0b24eDU9uQ2xpZW50Q2xpY2sFIiRzY3cud2ViZm9ybS5sYXN0U3VibWl0ID0gdGhpcy5pZDsfEAICZGQCBw8WAh8EBeEBPGgzPlNVUFBPUlQgV09NQU4nUzwvaDM+CjxwPllvdXIgZ2lmdCBwcm92aWRlcyBjcml0aWNhbCBzZXJ2aWNlcyBhbmQgcHJvZ3JhbXMgZm9yIHdvbWVuIGFuZCBiYWJpZXMgaW4gb3VyIGNvbW11bml0eS48L3A+CjxhIGhyZWY9Ii9naXZpbmctYW5kLXZvbHVudGVlcmluZy93YXlzLXRvLWdpdmUiPjxzcGFuIGNsYXNzPSJkZWZhdWx0LWJ1dHRvbiBibGFjayI+RE9OQVRFIE5PVzwvc3Bhbj48L2E+ZAIJDxYCHwQF9wY8aDQ+CjxzcGFuIGNsYXNzPSJpY29uIj48L3NwYW4+CjxzcGFuIGNsYXNzPSJwbHVzLW1pbnVzIj48L3NwYW4+CkNvbm5lY3Qgd2l0aCBXb21hbidzCjwvaDQ+CjxkaXYgY2xhc3M9ImZvb3Rlci1jb250ZW50Ij4KPHVsIGNsYXNzPSJmb290ZXItYmxvZy1saW5rcyI+CiAgICA8bGkgY2xhc3M9Im1vbW15LWdvLXJvdW5kIj4KICAgIDxzcGFuIGNsYXNzPSJpY29uIj48L3NwYW4+CiAgICA8YSB0aXRsZT0iVGhlIE1vbW15IEdvLVJvdW5kIEJsb2ciIHRhcmdldD0iX2JsYW5rIiBocmVmPSJodHRwOi8vdGhlbW9tbXlnb3JvdW5kLndvbWFuc2Jsb2dzLm9yZy8iPlRoZSBNb21teS1Hby1Sb3VuZCBCbG9nPC9hPgogICAgPC9saT4KICAgIDxsaSBjbGFzcz0id2VpZ2h0LWxvc3Mtc3VyZ2VyeSI+CiAgICA8c3BhbiBjbGFzcz0iaWNvbiI+PC9zcGFuPgogICAgPGEgdGFyZ2V0PSJfYmxhbmsiIGhyZWY9Imh0dHA6Ly93ZWlnaHRsb3Nzc3VyZ2VyeS53b21hbnNibG9ncy5vcmcvIj5BIE5ldyBZb3U6IFdlaWdodCBMb3NzIFN1cmdlcnkgQmxvZzwvYT4KICAgIDwvbGk+CiAgICA8bGkgY2xhc3M9ImxpdmluZy13aXRoLWNhbmNlciI+CiAgICA8c3BhbiBjbGFzcz0iaWNvbiI+PC9zcGFuPgogICAgPGEgdGFyZ2V0PSJfYmxhbmsiIGhyZWY9Imh0dHA6Ly9saXZpbmd3aXRoY2FuY2VyLndvbWFuc2Jsb2dzLm9yZy8iPkxpdmluZyBXaXRoIENhbmNlciBCbG9nPC9hPgogICAgPC9saT4KICAgIDxsaSBjbGFzcz0iaGVhbHRoeS1zZWxmIj4KICAgIDxzcGFuIGNsYXNzPSJpY29uIj48L3NwYW4+CiAgICA8YSB0YXJnZXQ9Il9ibGFuayIgaHJlZj0iaHR0cDovL2hlYWx0aHlzZWxmLndvbWFuc2Jsb2dzLm9yZy8iPkhlYWx0aHkgU2VsZiBCbG9nPC9hPjwvbGk+CjwvdWw+CjwvZGl2PmQCCw9kFgJmD2QWAgIBDxYCHwMCBhYMZg9kFgQCAQ8WAh8EBRo8bGkgY2xhc3M9ImZhY2Vib29rLWxpbmsiPmQCBQ8WAh8EBQU8L2xpPmQCAQ9kFgQCAQ8WAh8EBRk8bGkgY2xhc3M9InR3aXR0ZXItbGluayI+ZAIFDxYCHwQFBTwvbGk+ZAICD2QWBAIBDxYCHwQFGTxsaSBjbGFzcz0ieW91dHViZS1saW5rIj5kAgUPFgIfBAUFPC9saT5kAgMPZBYEAgEPFgIfBAUUPGxpIGNsYXNzPSJ3cC1saW5rIj5kAgUPFgIfBAUFPC9saT5kAgQPZBYEAgEPFgIfBAUbPGxpIGNsYXNzPSJwaW50ZXJlc3QtbGluayI+ZAIFDxYCHwQFBTwvbGk+ZAIFD2QWBgIBDxYCHwQFGzxsaSBjbGFzcz0iaW5zdGFncmFtLWxpbmsiPmQCAw8PZBYCHwUFBGxhc3RkAgUPFgIfBAUFPC9saT5kAg0PFgIfBAWqBTxoND48c3BhbiBjbGFzcz0iaWNvbiI+PC9zcGFuPjxzcGFuIGNsYXNzPSJwbHVzLW1pbnVzIj48L3NwYW4+V29tYW4ncyBGb3IgPC9oND4KPGRpdiBjbGFzcz0iZm9vdGVyLWNvbnRlbnQiPgo8dWw+CiAgICA8bGk+PGEgaHJlZj0iL3BhdGllbnRzLWFuZC12aXNpdG9ycyI+UGF0aWVudHMgJmFtcDsgVmlzaXRvcnMgPC9hPjwvbGk+CiAgICA8bGk+PGEgaHJlZj0iL2Zvci1oZWFsdGgtcHJvZmVzc2lvbmFscyI+SGVhbHRoIFByb2Zlc3Npb25hbHM8L2E+IDwvbGk+CiAgICA8bGk+PGEgaHJlZj0iL2Zvci1lbXBsb3llZXMiPkVtcGxveWVlczwvYT4gPC9saT4KICAgIDxsaT48YSBocmVmPSIvZ2l2aW5nLWFuZC12b2x1bnRlZXJpbmcvdm9sdW50ZWVyIj5Wb2x1bnRlZXI8L2E+PC9saT4KICAgIDxsaT48YSBocmVmPSIvZm9yLWJvYXJkLW9mLWRpcmVjdG9ycyI+Qm9hcmQgb2YgRGlyZWN0b3JzPC9hPiA8L2xpPgogICAgPGxpPjxhIGhyZWY9Ii9vdXItc2VydmljZXMvY2hpbGRyZW4tYW5kLW1lbiI+Q2hpbGRyZW4gJmFtcDsgTWVuIDwvYT48L2xpPgogICAgPGxpPjxhIGhyZWY9Ii9hYm91dC13b21hbnMvY29tbXVuaXR5Ij5Db21tdW5pdHkgSGVhbHRoPC9hPiA8L2xpPgogICAgPGxpPjxhIGhyZWY9Ii9mb3Itam9iLWFwcGxpY2FudHMiPkpvYiBBcHBsaWNhbnRzIDwvYT48L2xpPgo8L3VsPgo8L2Rpdj5kAg8PFgIfBAX8AzxoND4KPHNwYW4gY2xhc3M9Imljb24iPjwvc3Bhbj4KPHNwYW4gY2xhc3M9InBsdXMtbWludXMiPjwvc3Bhbj4KTG9jYXRpb25zICZhbXA7IE1hcHMKPC9oND4KPGRpdiBjbGFzcz0iZm9vdGVyLWNvbnRlbnQiPgo8YWRkcmVzcz4KPHNwYW4+V29tYW4ncyBIb3NwaXRhbDwvc3Bhbj48YnIgLz4KMTAwIFdvbWFuJ3MgV2F5PGJyIC8+CkJhdG9uIFJvdWdlLCBMQSA3MDgxNwo8L2FkZHJlc3M+CjxwPgpNYWluIE51bWJlcjoKKDIyNSkgOTI3LTEzMDAKPC9wPgo8cD4KUGF0aWVudCBSb29tczoKKDIyNSkgMjMxLTVbK108YnIgLz4KPHNwYW4gY2xhc3M9Iml0YWwiPltsYXN0IDMgZGlnaXRzIG9mIHJvb20gbnVtYmVyXTwvc3Bhbj4KPC9wPgo8cD4KUGF0aWVudCBJbmZvOgooMjI1KSA5MjQtODE1Nwo8L3A+CjxhIGhyZWY9Ii9sb2NhdGlvbnMtYW5kLW1hcHMiPjxidXR0b24gY2xhc3M9ImRlZmF1bHQtYnV0dG9uIHBpbmsiIHR5cGU9ImJ1dHRvbiI+VmlldyBNb3JlPC9idXR0b24+PC9hPgo8L2Rpdj5kAhEPFgIfBAWDAzx1bD4KICAgIDxsaT4KICAgIDxhIGhyZWY9Ii9hYm91dC13b21hbnMiPkFib3V0IFdvbWFuJ3M8L2E+CiAgICA8L2xpPgogICAgPGxpPgogICAgPGEgaHJlZj0iL2Zvci1qb2ItYXBwbGljYW50cy9jYXJlZXJzIj5DYXJlZXJzPC9hPgogICAgPC9saT4KICAgIDxsaT4KICAgIDxhIGhyZWY9Ii9jb250YWN0LXVzLWZvcm0iPkNvbnRhY3QgVXM8L2E+CiAgICA8L2xpPgogICAgPGxpPgogICAgPGEgaHJlZj0iL2dpdmluZy1hbmQtdm9sdW50ZWVyaW5nL3dheXMtdG8tZ2l2ZSI+RG9uYXRlPC9hPgogICAgPC9saT4KICAgIDxsaT4KICAgIDxhIGhyZWY9Ii9wYXRpZW50cy1hbmQtdmlzaXRvcnMvYmlsbGluZy1hbmQtaW5zdXJhbmNlIj5Zb3VyIEJpbGw8L2E+CiAgICA8L2xpPgo8L3VsPmQCEw8WAh8EBQ5Db3B5cmlnaHQgMjAxNGQCFQ8WAh8EBcsBPGxpPgo8YSBocmVmPSIvdGVybXMtYW5kLWNvbmRpdGlvbnMiPlRlcm1zICZhbXA7IENvbmRpdGlvbnM8L2E+CjwvbGk+CjxsaT4KPGEgaHJlZj0iL3ByaXZhY3ktcG9saWN5Ij5Qcml2YWN5IFBvbGljeTwvYT4KPC9saT4KPGxpPgo8YSBocmVmPSJodHRwOi8vd3d3Lm1lZHRvdWNoLmNvbS8iIHRhcmdldD0iX2JsYW5rIj5ieSBNZWRUb3VjaDwvYT4KPC9saT5kGAEFK3RvcF8wJGdyaWRfMCRjb250ZW50cGFuZWxfMCRsdlNlYXJjaFJlc3VsdHMPFCsADmRkZGRkZGQUKwADZGRkAgNkZGRmAv////8PZL+eUBmuZJYlJClU//El7vajOMid6mR+rZoIuyb7K4S1" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['mainform'];
if (!theForm) {
    theForm = document.mainform;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=vdFPQt6ZDAMRQzVSBhHZfuCu0rmKkDtV66hksZmM7X1RCuwsgY673yb2GwYw7-1kwGBcdVLrIOHxFnSuaOykgkGWrkxk57uRKMN7wKv9A0k1&amp;t=635216952300000000" type="text/javascript"></script>


<script src="/sitecore modules/web/web forms for marketers/scripts/jquery.js" type="text/javascript"></script>
<script src="/sitecore modules/web/web forms for marketers/scripts/jquery-ui.min.js" type="text/javascript"></script>
<script src="/sitecore modules/web/web forms for marketers/scripts/jquery-ui-i18n.js" type="text/javascript"></script>
<script src="/sitecore modules/web/web forms for marketers/scripts/json2.min.js" type="text/javascript"></script>
<script src="/sitecore modules/web/web forms for marketers/scripts/head.load.min.js" type="text/javascript"></script>
<script src="/sitecore modules/web/web forms for marketers/scripts/sc.webform.js?v=17072012" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
$scw(document).ready(function() {$scw('#form_E182B8B829CC4F76B8C6F092A1C1BC88').webform({formId:"{E182B8B8-29CC-4F76-B8C6-F092A1C1BC88}"})});//]]>
</script>

<script src="/ScriptResource.axd?d=tH_lLTZ3Jz8JJQ7bJtBy7BojwUfi3Ux8B6X25qAnpyiAGiOmEeNwCwBRrm5NKy0d5ObBxGjueJMQCGu8jZDtQwgs_iP3WzzXsMsd4XkS07dhTD1EXoBH1FzENLLnhke7axC9l031u3kAnstmRZODMPIpIDZDy1ciGlLoScFlN7s1&amp;t=6165b15a" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=k34LFGfF9c07WwwNk3pJWwWW8Babe0DG8QuGDWqlWy_d0UpnLV4xL5ueNTUlH5duAG2Ka9GL7hSnIkPbtCsO_fS_fPkzkLW1X7lAJ6V-lQB3I9ALQLaCsfzn3QRgPGvDbv7W4x-QCrvzT2roGPLVDE7qLs_Cr3DeITQEOTw55H81&amp;t=6119e399" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=qBj84yfROV0e7vGiGiIMzvfedsXPEZSZJtxi8lL4vr9vMFQfAWwgOfroe7bLmBFw9A7MLj5UkID088gT5vtQVSaQ_AumBRHLpGnMN59Ejp57FPPQNszJ_SrbX_EuOBrXTR-i1LU3PinX0BEPMi9E7FVn-oZF4N5Yrex_VzhUh-6PJoHF1IlBURIX1wkY9L7a0&amp;t=6119e399" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
if (typeof(ValidatorOnSubmit) == "function" && ValidatorOnSubmit() == false) return false;$scw('#form_E182B8B829CC4F76B8C6F092A1C1BC88').webform('updateSubmitData', 'form_E182B8B829CC4F76B8C6F092A1C1BC88');
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__PREVIOUSPAGE" id="__PREVIOUSPAGE" value="hqEHzqNtkK4m0R3hvfHmmqZtX7he6MZsb8p7wMyc8mzLk6UVWpUntB8eBlCUwcSCcwnhpbLo7sCC5hEBjAs9IQ2" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAwyoii8eX8r6K1v0kDwBUeRtSkKL1dELeyGt3Cd5jfwixyqbFJ+JXoJUTJ5g3/H1UliiLGWOouNwRrkp38xMYWUmCJHHZhhZels1WiPRMxaTEABP/F3eSu0espbYLH9DRoTU47/sPVhR3H0gvAJ3r4JY7G9kongIXmUw4cHZRm5zMhTELPzUz4ExfrzLB02X6BTvfpuXo5nCV+EmcqlA6oU+rJkhb/pvMG2i0IjKGKIY6i/VhgIKfTirHu+r/N6qOOMncNe8HVoqv+NplO1TbNs6mqJVlVrKhb0XSmBFFYTNQ==" />
</div>    
        <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('spListScriptManager', 'mainform', [], [], [], 90, '');
//]]>
</script>


        
<!--============================================= MedTouch.Base: Common: Header =============================================--> 

	<div class="right-utility-A">
		<div class="text-resizer">
			<a class="zoom_out resetFont" href="">A</a>
			<a class="zoom_in increaseFont" href="">A</a>
			<a class="zoom_big increaseFont-more" href="">A</a>
		</div>
		<ul>
			<li>
				<a href="#" onclick="window.print()" class="print-button">Print</a>
			</li>
			<li>
			    <a href="http://www.addthis.com/bookmark.php" class="email-button addthis_button share">Email</a>
			</li>
		</ul>
	</div>

    <div class="right-utility-B">
        

    
		<ul class="social-links">
            
                      <li class="facebook-link">
                        <a href="https://www.facebook.com/womanshospitalbr ">
                           Facebook
                        </a>
                    </li>
                
                      <li class="twitter-link">
                        <a href="https://twitter.com/WomansHospital ">
                           Twitter
                        </a>
                    </li>
                
                      <li class="youtube-link">
                        <a href="https://www.youtube.com/user/WomansHospitalBR">
                           Youtube
                        </a>
                    </li>
                
                      <li class="wp-link">
                        <a href="http://womansblogs.org/ ">
                           Wordpress
                        </a>
                    </li>
                
                      <li class="pinterest-link">
                        <a href="http://www.pinterest.com/Womanshospital ">
                           Pinterest
                        </a>
                    </li>
                
                      <li class="instagram-link">
                        <a class="last" href="http://instagram.com/womanshospital">
                           Instagram
                        </a>
                    </li>
                
		</ul>

    </div>
	
<header role="banner">				
		<!-- quick links panel -->
        

<section class="dropdown-panel">
			<div class="grid">
				<div class="eight columns quick-links-nav">
			   <div id="top_0_ctl01_ctl00_pnlSearch" class="mobile-search-wrapper" onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;top_0_ctl01_ctl00_btnSubmit&#39;)">
	
<div class="mobile-search-inner">
	<table>
		<tr>
			<td>
                
                <input name="top_0$ctl01$ctl00$txtKeyword" type="text" value="Search Term" id="top_0_ctl01_ctl00_txtKeyword" onfocus="if (this.value == &#39;Search Term&#39;) this.value = &#39;&#39;;" onblur="if (this.value == &#39;&#39;) this.value = &#39;Search Term&#39;;" />
			</td>
			<td>
                <input type="submit" name="top_0$ctl01$ctl00$btnSubmit" value="GO" id="top_0_ctl01_ctl00_btnSubmit" class="black button" />
                
			</td>
		</tr>
	</table>
</div>
    


</div>

					<div class="inner">
							
                            
                                    <div class="three columns">

                                            
                                                    <div class="home-nav">
                                                
                                                    <a href="/our-services">Our Services</a>
                                                
                                                    <a href="/physician-directory">Find a Doctor</a>
                                                
                                                    <a href="/classes-and-events">Classes &amp; Events</a>
                                                
                                                    <a href="/locations-and-maps">Locations &amp; Maps</a>
                                                
                                                    <a href="/patients-and-visitors">Patients &amp; Visitors</a>
                                                
                                                    </div>
                                                
								            
                                        <h4 class="accordion-handle">
								        Woman&#39;s For...
								<span class="toggle-icon"></span>
							</h4>
							<ul>
                                
                                        <li><a href="/for-job-applicants">Job Applicants</a></li>    
                                    
                                        <li><a href="/patients-and-visitors">Patients &amp; Visitors</a></li>    
                                    
                                        <li><a href="/for-employees">Employees</a></li>    
                                    
                                        <li><a href="/for-board-of-directors">Board of Directors</a></li>    
                                    
                                        <li><a href="/for-job-applicants/volunteer">Volunteers</a></li>    
                                    

							</ul>
                                </div>
                                
                                    <div class="three columns">

                                            
								            
                                        <h4 class="accordion-handle">
								        Visiting Woman&#39;s
								<span class="toggle-icon"></span>
							</h4>
							<ul>
                                
                                        <li><a href="/locations-and-maps">Getting Here</a></li>    
                                    
                                        <li><a href="/locations-and-maps/parking">Parking</a></li>    
                                    
                                        <li><a href="/patients-and-visitors/visiting-hours-and-policies">Visiting Hours</a></li>    
                                    
                                        <li><a href="/patients-and-visitors/dining">Dining</a></li>    
                                    
                                        <li><a href="/our-services/breast-care/breast-imaging-services/mammography-locations">Mammogram Locations</a></li>    
                                    

							</ul>
                                </div>
                                
                                    <div class="three columns">

                                            
								            
                                        <h4 class="accordion-handle">
								        Popular Links
								<span class="toggle-icon"></span>
							</h4>
							<ul>
                                
                                        <li><a href="/about-womans">About Woman&#39;s</a></li>    
                                    
                                        <li><a href="/patients-and-visitors/contact-a-patient">Contacting a Patient</a></li>    
                                    
                                        <li><a href="/contact-us-form">Contact Us</a></li>    
                                    
                                        <li><a href="/our-services/wellness-and-nutrition/fitness/personal-training/small-group-training/pre-and-post-pregnancy-fitness-programs">Prenatal Yoga</a></li>    
                                    
                                        <li><a href="/for-job-applicants">Join Our Team</a></li>    
                                    

							</ul>
                                </div>
                                
                                    <div class="three columns">

                                            
								            
                                        <h4 class="accordion-handle">
								        Patient Services
								<span class="toggle-icon"></span>
							</h4>
							<ul>
                                
                                        <li><a href="/patients-and-visitors/billing-and-insurance/bill-pay-login">Pay Your Bill</a></li>    
                                    
                                        <li><a href="/patients-and-visitors/patient-portal">MyHealth Patient Portal</a></li>    
                                    
                                        <li><a href="/patients-and-visitors/registration">Registration</a></li>    
                                    
                                        <li><a href="/patients-and-visitors/brochures-and-publications">Brochures &amp; Publications</a></li>    
                                    
                                        <li><a href="/patients-and-visitors/patient-guide/patient-rights">Your Rights</a></li>    
                                    

							</ul>
                                </div>
                                


						</div>
				</div>
				<div class="four columns dropdown-feature">
					<div class="inner">
                        <img id="top_0_ctl01_imgQuickLinks" class="dropdown-feature-image" src="http://prod.womans.org.medtouch.com/~/media/images/womans/development/development%20landing%20images/premie_hands_353%20x%20170.jpg" />
                        
						<h4>Giving Opportunities</h4>
						<p>
							There are many ways to support Woman's Hospital. Your gift provides critical services and programs for women and babies in our community.
							<a id="top_0_ctl01_hlLink" href="/giving-and-volunteering">Read More ></a>
						</p>
					</div>
				</div>
			</div>
            
		</section>

        

	<section class="top grid">
		
        <!-- Mobile CODE: added .mobile-menu -->
		<div class="mobile-dropdown-nav">
				<ul>
					<li class="mobile-menu-button">
						<a href="#">
							<span></span>
							Menu
						</a>
					</li>
					<li class="mobile-search-button">
						<a href="#">
							<span></span>
							Search
						</a>
					</li>
					<li class="mobile-call-button">
						<a href="#">
							<span></span>
							Call
						</a>
					</li>
				</ul>
			</div>
        
        <!-- /Mobile CODE: -->


        <!-- Header Logo -->
        <div id="top_0_pnlLogo" class="logo">
	
			<a id="top_0_hplLogo" title="Woman&amp;#39;s" href="/"><img src="http://prod.womans.org.medtouch.com/~/media/images/womans/logo2.svg?mw=200" class="screen-logo" alt="Womans" /><img src="http://prod.womans.org.medtouch.com/~/media/images/womans/logo.png?h=234&amp;w=251" class="ie-print" alt="Womans" width="251" height="234" /></a>
		
</div>
        <!-- /Header Logo -->

        
        	
            <a href="/giving-and-volunteering" class="donate">Donate</a>
			<div class="header-utility">
				<div class="header-utility-contents">
					<table>
						<tr>
							<!-- <td>
								<a class="donate" href="#">Donate</a>
							</td> -->
							<td>
								<div class="search-table-wrapper">
									<table>
										<tr>
											<td>
												<a class="quick-links" href="#">
													Quick Links
													<span class="arrow"></span>
												</a>
                                                
<!--==================== Module: Site Search: Basic Site Search Box =============================-->

<div id="top_0_ctl02_pnlSearch" class="main-search-input-wrapper" onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;top_0_ctl02_btnSubmit&#39;)">
	
    
        
        <input name="top_0$ctl02$txtKeyword" type="text" value="Search Term" id="top_0_ctl02_txtKeyword" class="main-search-input" onkeydown="ispagenotfound(event, this)" onfocus="if (this.value == &#39;Search Term&#39;) this.value = &#39;&#39;;" onblur="if (this.value == &#39;&#39;) this.value = &#39;Search Term&#39;;" />
        <input type="submit" name="top_0$ctl02$btnSubmit" value="GO" id="top_0_ctl02_btnSubmit" class="main-search-submit black button" />
        
        
    
    


</div>
<!--==================== /Module: Site Search: Basic Site Search Box =============================-->
											</td>
										</tr>
										
									</table>
								</div>
							</td>
							<td>
							<a class="main-search" href="#"></a>
                            </td>
						</tr>
					</table>
				</div>
				

			</div>
		


        <!-- Header Nav -->
        
<!--============================================= MedTouch.Base: Common: Primary Navigation =============================================--> 

<nav class="main-nav" role="navigation">
    <div class="grid">
        <ul class="twelve columns spread">  
            
                   <li id="top_0_ctl03_rptPrimaryNav_liNav_0">
                        <a href="/our-services" id="top_0_ctl03_rptPrimaryNav_lnkNav_0">Our Services</a>
                        
                    </li>
                
                   <li id="top_0_ctl03_rptPrimaryNav_liNav_1">
                        <a href="/physician-directory" id="top_0_ctl03_rptPrimaryNav_lnkNav_1">Find a Doctor</a>
                        
                    </li>
                
                   <li id="top_0_ctl03_rptPrimaryNav_liNav_2">
                        <a href="/classes-and-events" id="top_0_ctl03_rptPrimaryNav_lnkNav_2">Classes &amp; Events</a>
                        
                    </li>
                
                   <li id="top_0_ctl03_rptPrimaryNav_liNav_3">
                        <a href="/locations-and-maps" id="top_0_ctl03_rptPrimaryNav_lnkNav_3">Locations &amp; Maps</a>
                        
                    </li>
                
                   <li id="top_0_ctl03_rptPrimaryNav_liNav_4">
                        <a href="/patients-and-visitors" id="top_0_ctl03_rptPrimaryNav_lnkNav_4">Patients &amp; Visitors</a>
                        
                    </li>
                
        </ul>
    </div>
</nav>
<!--============================================= /MedTouch.Base: Common: Primary Navigation =============================================-->
        <!-- /Header Nav -->

        <!-- Mobile CODE: added .mobile-menu -->
        
			<!-- / END Mobile CODE -->

	</section>
    

<div class="homepage-services-slider-full-width">
    <ul>
        
            <li class='bone-and-joint fifth'><a href='/our-services/bone-and-joint'><span class="popup-content">
                <img src="http://prod.womans.org.medtouch.com/~/media/images/womans/bone and joint/hover/walking_road_310x233.jpg?h=233&amp;w=310" alt="walking_road_310x233" width="310" height="233" />
                <span class="popup-text">
                    <p>Whether you're an athlete, weekend warrior or managing osteoporosis, we can keep you moving with therapy or surgery.</p></span>
                <button class="default-button black" type="button">
                    Read more</button>
            </span><span class="icon"></span>
                Bone &amp; Joint
            </a></li>
            
            <li class='cancer sixth'><a href='/our-services/cancer'><span class="popup-content">
                <img src="http://prod.womans.org.medtouch.com/~/media/images/womans/cancer/hover/cancer_bald_black_40s_280 x 200.jpg?h=233&amp;w=310" alt="cancer_bald_black_40s_280 x 200" width="310" height="233" />
                <span class="popup-text">
                    <p>From&nbsp;screening and diagnosis to treatment and surgery, we're here to support you throughout your cancer journey.</p></span>
                <button class="default-button black" type="button">
                    Read more</button>
            </span><span class="icon"></span>
                Cancer
            </a></li>
            
            <li class='children-and-men third'><a href='/our-services/children-and-men'><span class="popup-content">
                <img src="http://prod.womans.org.medtouch.com/~/media/images/womans/men and children/man_baby_hands_310 x 233.jpg?h=233&amp;w=310" alt="man_baby_hands_310 x 233" width="310" height="233" />
                <span class="popup-text">
                    <p>Let our family care for yours with imaging and lab services, specialized medical clinics and health screenings for men and children. </p></span>
                <button class="default-button black" type="button">
                    Read more</button>
            </span><span class="icon"></span>
                Children &amp; Men
            </a></li>
            
            <li class='gynecology first'><a href='/our-services/gynecology'><span class="popup-content">
                <img src="http://prod.womans.org.medtouch.com/~/media/images/womans/gynecology/stethescope_310x233.jpg?h=233&amp;w=310" alt="stethescope_310x233" width="310" height="233" />
                <span class="popup-text">
                    <p>Woman's knows how to care for women, from your first gynecological visit to menopause to advanced surgery.</p></span>
                <button class="default-button black" type="button">
                    Read More</button>
            </span><span class="icon"></span>
                Gynecology
            </a></li>
            
            <li class='baby second'><a href='/our-services/mother-and-baby'><span class="popup-content">
                <img src="http://prod.womans.org.medtouch.com/~/media/images/womans/baby/hover/baby_bump_310x233.jpg?h=233&amp;w=310" alt="baby_bump_310x233" width="310" height="233" />
                <span class="popup-text">
                    <p>Every delivery is a special delivery at Woman's. We understand your needs, from fertility to breastfeeding and newborn care. </p></span>
                <button class="default-button black" type="button">
                    Read more</button>
            </span><span class="icon"></span>
                Mother &amp; Baby
            </a></li>
            
            <li class='surgery last'><a href='/our-services/surgery'><span class="popup-content">
                <img src="http://prod.womans.org.medtouch.com/~/media/images/womans/surgery/hover/surgery_mask_310x233.jpg?h=233&amp;w=310" alt="surgery_mask_310x233" width="310" height="233" />
                <span class="popup-text">
                    <p>Choose Woman's for your general surgery or specialized gynecological, urological, orthopedic, weight loss or cosmetic surgery.</p></span>
                <button class="default-button black" type="button">
                    Read more</button>
            </span><span class="icon"></span>
                Surgery
            </a></li>
            
            <li class='wellness-and-nutrition fourth'><a href='/our-services/wellness-and-nutrition'><span class="popup-content">
                <img src="http://prod.womans.org.medtouch.com/~/media/images/womans/wellness and nutrition/hover/weights_apple_tapemeasure2_310 x 233.jpg?h=233&amp;w=310" alt="weights_apple_tapemeasure2_310 x 233" width="310" height="233" />
                <span class="popup-text">
                    <p>Build a healthy body through fitness, nutrition, therapy, health screenings and relaxation services.</p></span>
                <button class="default-button black" type="button">
                    Read more</button>
            </span><span class="icon"></span>
                Wellness &amp; Nutrition
            </a></li>
            
    </ul>
</div>

</header>

<!--============================================= MedTouch.Base: Common: Notification Box =============================================--> 

<!--============================================= /MedTouch.Base: Common: Notification Box =============================================--> 
	



<section class="hero home grid">
				<div class="decoration"></div>
				<div class="inner">
					<div class="mtslide">
					  <ul class="slides">
						
                      
                            <li>	
                            <img src="http://prod.womans.org.medtouch.com/~/media/images/womans/homepage/slider images/walking_path_1210x492.jpg?h=492&amp;w=1210" alt="walking_path_1210x492" width="1210" height="492" />	 
						  
                          <div id="top_0_grid_0_rptSlides_pnlCore_0" class="core">
	
							  <div class="slide-content">
								<h1>
									<a href='/our-services/wellness-and-nutrition'>
										Wellness &amp; Nutrition
                                        <span>Exceptional Care, Centered on You</span>
										<span class="arrow"></span>
									</a>
								</h1>
								<div class="copy-left">
									<a href="/our-services/wellness-and-nutrition">Wellness and Nutrition</a>
								</div>
							  </div>
                            
</div>
						  
						</li>
                        
                            <li>	
                            <img src="http://prod.womans.org.medtouch.com/~/media/images/womans/homepage/slider images/pregnant_bananaleaves_1210x492.jpg?h=492&amp;w=1210" alt="pregnant_bananaleaves_1210x492" width="1210" height="492" />	 
						  
                          <div id="top_0_grid_0_rptSlides_pnlCore_1" class="core">
	
							  <div class="slide-content">
								<h1>
									<a href='/our-services/mother-and-baby'>
										Pregnancy
                                        <span>We Stay Focused on the Details</span>
										<span class="arrow"></span>
									</a>
								</h1>
								<div class="copy-left">
									<a href="/our-services/mother-and-baby">Mother and Baby</a>
								</div>
							  </div>
                            
</div>
						  
						</li>
                        
                            <li>	
                            <img src="http://prod.womans.org.medtouch.com/~/media/images/womans/homepage/slider images/woman_coffee_1210x492.jpg?h=492&amp;w=1210" alt="woman_coffee_1210x492" width="1210" height="492" />	 
						  
                          <div id="top_0_grid_0_rptSlides_pnlCore_2" class="core">
	
							  <div class="slide-content">
								<h1>
									<a href='/our-services/surgery'>
										Surgery
                                        <span>Because You Know Us, and We Know You</span>
										<span class="arrow"></span>
									</a>
								</h1>
								<div class="copy-left">
									<a href="/our-services/surgery">Surgery</a>
								</div>
							  </div>
                            
</div>
						  
						</li>
                        
                            <li>	
                            <img src="http://prod.womans.org.medtouch.com/~/media/images/womans/homepage/slider images/home_template_to_usebaby_toes.jpg?h=492&amp;w=1210" alt="HOME_template_to_usebaby_toes" width="1210" height="492" />	 
						  
                          <div id="top_0_grid_0_rptSlides_pnlCore_3" class="core">
	
							  <div class="slide-content">
								<h1>
									<a href='/our-services/mother-and-baby/newborn-care'>
										Baby Care
                                        <span>Little Touches You'll Remember</span>
										<span class="arrow"></span>
									</a>
								</h1>
								<div class="copy-left">
									<a href="/our-services/mother-and-baby/newborn-care">Newborn Care</a>
								</div>
							  </div>
                            
</div>
						  
						</li>
                        
                            <li>	
                            <img src="http://prod.womans.org.medtouch.com/~/media/images/womans/homepage/slider images/home_template_to_use_pink_scarf.jpg?h=492&amp;w=1210" alt="HOME_template_to_use_pink_scarf" width="1210" height="492" />	 
						  
                          <div id="top_0_grid_0_rptSlides_pnlCore_4" class="core">
	
							  <div class="slide-content">
								<h1>
									<a href='/our-services/breast-care'>
										Breast Care
                                        <span>Every Month is About Cancer Awareness</span>
										<span class="arrow"></span>
									</a>
								</h1>
								<div class="copy-left">
									<a href="/our-services/breast-care">Breast Care</a>
								</div>
							  </div>
                            
</div>
						  
						</li>
                        
                      </ul>
					</div>		
                      
                      
						
					

					<div class="homepage-services-slider-mobile">
						<div class="owl-carousel-homepage">
							<ul>
                                
                                        <li class='bone-and-joint fifth'>
									<a href='/our-services/bone-and-joint'>
										<span class="icon"></span>
										Bone &amp; Joint
									</a>
								</li>
								    
                                    
                                        <li class='cancer sixth'>
									<a href='/our-services/cancer'>
										<span class="icon"></span>
										Cancer
									</a>
								</li>
								    
                                    
                                        <li class='children-and-men third'>
									<a href='/our-services/children-and-men'>
										<span class="icon"></span>
										Children &amp; Men
									</a>
								</li>
								    
                                    
                                        <li class='gynecology first'>
									<a href='/our-services/gynecology'>
										<span class="icon"></span>
										Gynecology
									</a>
								</li>
								    
                                    
                                        <li class='baby second'>
									<a href='/our-services/mother-and-baby'>
										<span class="icon"></span>
										Mother &amp; Baby
									</a>
								</li>
								    
                                    
                                        <li class='surgery last'>
									<a href='/our-services/surgery'>
										<span class="icon"></span>
										Surgery
									</a>
								</li>
								    
                                    
                                        <li class='wellness-and-nutrition fourth'>
									<a href='/our-services/wellness-and-nutrition'>
										<span class="icon"></span>
										Wellness &amp; Nutrition
									</a>
								</li>
								    
                                    
								
							</ul>
						</div>					
					</div>
				</div>		
                
			</section>


            <section class="main-content">
		<div class="grid home-content">
		   <div class="four columns homepage-callout-slider-wrapper">
		   		
<div class="callout-wrapper">
<div class="callout">
    <h4>
        <span>
            </span>
    </h4>
    <div class="feature-story-callout" style="border-top:0px;">
        <h3 style="text-align: center;">Woman's Surgery:
<br />
Because You Already Know Us, and We Know You</h3>
<iframe width="400" height="225" frameborder="0" src="//www.youtube.com/embed/3sGubI-kzxs?rel=0&amp;autohide=1&amp;showinfo=0"></iframe>
        <h3>
            <a id="top_0_grid_0_leftpanel_0_hlLink"></a>
        </h3>
    </div>
</div>
</div>
		   </div>
		   <div class="four columns center-callout-col">
		   		

<!--==================== Women: News: Latest News Home Page (CUSTOM) =============================-->
<h2 class="home-accordion-handle">
    
		   			Latest News
		   			<span class="toggle-icon"></span>
		   		</h2>
		   		<div class="home-accordion-content">
			   		<div class="callout home-callout">
						
                                <div class="callout-event">
							        <a id="top_0_grid_0_contentpanel_0_lvSearchResults_hlMoreLink_0" href="/news/2014/08/press-release-best-places-to-work">
								        
								        <span class="news-title">Woman’s Hospital named in “Best Places to Work in Healthcare”</span>
								        
							        </a>
        			            </div>
                            
                                <div class="callout-event">
							        <a id="top_0_grid_0_contentpanel_0_lvSearchResults_hlMoreLink_1" href="/news/2014/08/press-release-family-favorite">
								        
								        <span class="news-title">Woman’s named 2014 Family Favorite Birthing Hospital </span>
								        
							        </a>
        			            </div>
                            
                                <div class="callout-event">
							        <a id="top_0_grid_0_contentpanel_0_lvSearchResults_hlMoreLink_2" href="/news/2014/08/press-release-foundation-for-womans">
								        
								        <span class="news-title">Woman&#39;s Announces The Foundation for Woman’s</span>
								        
							        </a>
        			            </div>
                            
                        
                        
                       
				</div>
                 <div id="top_0_grid_0_contentpanel_0_pnlViewAll" class="module-nw-view-all">
	
                    <input type="submit" name="top_0$grid_0$contentpanel_0$btnViewAll" value="View More" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;top_0$grid_0$contentpanel_0$btnViewAll&quot;, &quot;&quot;, false, &quot;&quot;, &quot;/news&quot;, false, false))" id="top_0_grid_0_contentpanel_0_btnViewAll" class="default-button pink inset" />
                
</div>
            </div>
<!--==================== /Women: News: Latest News Home Page (CUSTOM) =============================-->   


		   </div>
		   <div class="four columns">
		   		
<!--==================== Module: Calendar: Upcoming Session Events =============================-->
<h2 class="home-accordion-handle">
    Upcoming Classes & Events
    <span class="toggle-icon"></span>
</h2>
<div id="top_0_grid_0_rightpanel_0_pnlListing" class="home-accordion-content">
	
    
            <div class="callout-wrapper">
                <div class="callout">
        
			<div class="callout-event">
                <div class="event-header">
                    <a id="top_0_grid_0_rightpanel_0_lvSearchResults_hlItem_0" href="/classes-and-events/surgery/weight-loss/weight-loss-surgery?esk=oct_2_wls"><span class='event-date'>02<br/>Oct</span>Weight Loss Surgery: What You Should Know</a>
                </div>
                <p class="callout-event-preview">
                    Join us to learn more about the different types of weight loss surgeries and get your questions...
                    <a id="top_0_grid_0_rightpanel_0_lvSearchResults_hlMoreLink_0" class="read-more" href="/classes-and-events/surgery/weight-loss/weight-loss-surgery?esk=oct_2_wls">Read More</a>
                </p>
            </div>
        
			<div class="callout-event">
                <div class="event-header">
                    <a id="top_0_grid_0_rightpanel_0_lvSearchResults_hlItem_1" href="/classes-and-events/breastfeeding/breastfeeding-tips-for-moms-on-the-go?esk=october_3_bftips"><span class='event-date'>03<br/>Oct</span>Breastfeeding Tips for Moms On-the-Go</a>
                </div>
                <p class="callout-event-preview">
                    Learn the "tricks of the trade" to ensure your baby gets the benefits of breast milk when you're...
                    <a id="top_0_grid_0_rightpanel_0_lvSearchResults_hlMoreLink_1" class="read-more" href="/classes-and-events/breastfeeding/breastfeeding-tips-for-moms-on-the-go?esk=october_3_bftips">Read More</a>
                </p>
            </div>
        
            <input type="submit" name="top_0$grid_0$rightpanel_0$lvSearchResults$ctl03$btnViewMore" value="View More" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;top_0$grid_0$rightpanel_0$lvSearchResults$ctl03$btnViewMore&quot;, &quot;&quot;, false, &quot;&quot;, &quot;/classes-and-events&quot;, false, false))" id="top_0_grid_0_rightpanel_0_lvSearchResults_btnViewMore" class="default-button pink inset" />
            </div>
            </div>
        

</div>
<!--==================== /Module: Calendar: Upcoming Session Events =============================-->
		   </div>

		   <div class="homepage-mobile-container"></div>
		</div>
        </section>
	
		
<!--============================================= /MedTouch.Base: Common: Header =============================================-->	



        

        


<style>

footer .scfSingleLineTextLabel
{
    display:none !important;
}

footer  .scfEmailLabel
{
    display:none !important;
}

footer .scfSingleLineTextBox
{
    margin-bottom: 5px !important;
    width: 100% !important;
}

footer .scfEmailTextBox
{
    margin-bottom: 5px !important;
    width: 100% !important;
}

footer .scfValidationSummary
{
    
    color:White !important;
}

footer .scfValidator
{
    color:White !important;
}

footer .scfSubmitButton 
{
     font-size: 0.9em;
    text-transform: uppercase;
    font-family: 'Open Sans', sans-serif;
    font-weight: 400;
    border: 0px;
    color: white;
     transition: .25s;
    -moz-transition: .25s;
    -webkit-transtion: .25s;
    -o-transition: .25s;
    -ms-transition: .25s;background-color: #464646;
    border: 1px solid #464646;
    float:right;
}

footer .scfSubmitButton:hover,
input[type="submit"].black:hover,
button.black:hover{
    background-color: #4e4e4e;
}

</style>


<!--============================================= MedTouch.Base: Common: Footer =============================================--> 
<footer>

		<div class="back-to-top grid">
			<a href="#top"></a>
		</div>
		<div class="latest-tweet grid">
				<!-- Placeholder, twitter will generate latest tweet here. -->
                <div><p>"About 50% of vaginal cancer cases are found in women 70+. <a href='http://search.twitter.com/search?q=%23GynecologicCancerAwarenessMonth' target='_blank'>#GynecologicCancerAwarenessMonth</a>"</p></div><div><p>"Supervise your baby during walker use. <a href='http://search.twitter.com/search?q=%23BabySafetyMonth' target='_blank'>#BabySafetyMonth</a>"</p></div><div><p>"Choose soft toys for car rides that wouldn’t hurt your baby if involved in a crash. <a href='http://search.twitter.com/search?q=%23BabySafetyMonth' target='_blank'>#BabySafetyMonth</a>"</p></div><div><p>"Always use straps when using a stroller, swing, infant carrier or high chair. <a href='http://search.twitter.com/search?q=%23BabySafetyMonth' target='_blank'>#BabySafetyMonth</a>"</p></div><div><p>"The emotional support of family, friends and others can help those who are dealing with cancer treatments. <a href='http://search.twitter.com/search?q=%23GynecologicCancerAwarenessMonth' target='_blank'>#GynecologicCancerAwarenessMonth</a>"</p></div>
		</div>
		<div class="footer-header grid">
			<div class="eight columns newsletter">
				<div class="border-wrapper">
					<div class="seven columns">
                        <h3>MY WOMAN'S EMAIL NEWSLETTER</h3>
<p>SIGN UP and get the latest on weight loss, nutrition, healthy living, fitness, motherhood, breast health and more every month!</p>
					</div>
					<div class="five columns signup">
                        <div class='scfForm' id="form_E182B8B829CC4F76B8C6F092A1C1BC88"><input type="hidden" name="bottom_0$form_E182B8B829CC4F76B8C6F092A1C1BC88$formreference" id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_formreference" value="form_E182B8B829CC4F76B8C6F092A1C1BC88" /><input type="hidden" name="bottom_0$form_E182B8B829CC4F76B8C6F092A1C1BC88$form_E182B8B829CC4F76B8C6F092A1C1BC88_eventcount" id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_form_E182B8B829CC4F76B8C6F092A1C1BC88_eventcount" /><h1 class="scfTitleBorder">

</h1><div class="scfIntroBorder">

</div><div id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88__summary" class="scfValidationSummary" style="display:none;">

</div><div id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88__submitSummary" class="scfSubmitSummary" style="color:Red;">

</div><div onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_form_E182B8B829CC4F76B8C6F092A1C1BC88_submit&#39;)">
	<div class="scfSectionBorder">
		<div class="scfSectionBorder">
			<div class="scfSectionContent">
				<div id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F_scope" class="scfSingleLineTextBorder fieldid.%7bD896A45D-641C-4A49-BD0E-B64C4AF4290F%7d name.Full+Name">
					<label for="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F" id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F_text" class="scfSingleLineTextLabel">Full Name</label><div class="scfSingleLineGeneralPanel">
						<input name="bottom_0$form_E182B8B829CC4F76B8C6F092A1C1BC88$field_D896A45D641C4A49BD0EB64C4AF4290F" type="text" value="Full Name" maxlength="256" id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F" class="scfSingleLineTextBox" /><span class="scfSingleLineTextUsefulInfo" style="display:none;"></span><span id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F6ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bD896A45D-641C-4A49-BD0E-B64C4AF4290F%7d inner.1" style="display:none;">Full Name must have at least 0 and no more than 256 characters.</span><span id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bD896A45D-641C-4A49-BD0E-B64C4AF4290F%7d inner.1" style="display:none;">The value of the Full Name field is not valid.</span>
					</div><span class="scfRequired">*</span>
				</div><div id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F122144_scope" class="scfEmailBorder fieldid.%7b1820EC7C-7BEE-4322-9B86-A0753F122144%7d name.Email+Address">
					<label for="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F122144" id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F122144_text" class="scfEmailLabel">Email Address</label><div class="scfEmailGeneralPanel">
						<input name="bottom_0$form_E182B8B829CC4F76B8C6F092A1C1BC88$field_1820EC7C7BEE43229B86A0753F122144" type="text" value="Email Address" id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F122144" class="scfEmailTextBox" /><span class="scfEmailUsefulInfo" style="display:none;"></span><span id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F1221445D10AF7533054C39908EB25E8CB4ABDC_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b1820EC7C-7BEE-4322-9B86-A0753F122144%7d inner.1" style="display:none;">Enter a valid e-mail address.</span>
					</div><span class="scfRequired">*</span>
				</div>
			</div>
		</div>
	</div>
</div><div class="scfFooterBorder">

</div><div>
	<input type="submit" name="bottom_0$form_E182B8B829CC4F76B8C6F092A1C1BC88$form_E182B8B829CC4F76B8C6F092A1C1BC88_submit" value="SIGN ME UP!" onclick="$scw.webform.lastSubmit = this.id;WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;bottom_0$form_E182B8B829CC4F76B8C6F092A1C1BC88$form_E182B8B829CC4F76B8C6F092A1C1BC88_submit&quot;, &quot;&quot;, true, &quot;form_E182B8B829CC4F76B8C6F092A1C1BC88_submit&quot;, &quot;&quot;, false, false));$scw.webform.validators.setFocusToFirstNotValid(&#39;form_E182B8B829CC4F76B8C6F092A1C1BC88_submit&#39;)" id="bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_form_E182B8B829CC4F76B8C6F092A1C1BC88_submit" class="scfSubmitButton" />
</div></div>
					</div>
				</div>
			</div>
			<div class="four columns footer-donate">
				<div class="border-wrapper">
                    <h3>SUPPORT WOMAN'S</h3>
<p>Your gift provides critical services and programs for women and babies in our community.</p>
<a href="/giving-and-volunteering/ways-to-give"><span class="default-button black">DONATE NOW</span></a>
                    
				</div>
			</div>
			
		</div>
		<div class="footer-main grid">
				<div class="four columns connect-with-womans">
                    <h4>
<span class="icon"></span>
<span class="plus-minus"></span>
Connect with Woman's
</h4>
<div class="footer-content">
<ul class="footer-blog-links">
    <li class="mommy-go-round">
    <span class="icon"></span>
    <a title="The Mommy Go-Round Blog" target="_blank" href="http://themommygoround.womansblogs.org/">The Mommy-Go-Round Blog</a>
    </li>
    <li class="weight-loss-surgery">
    <span class="icon"></span>
    <a target="_blank" href="http://weightlosssurgery.womansblogs.org/">A New You: Weight Loss Surgery Blog</a>
    </li>
    <li class="living-with-cancer">
    <span class="icon"></span>
    <a target="_blank" href="http://livingwithcancer.womansblogs.org/">Living With Cancer Blog</a>
    </li>
    <li class="healthy-self">
    <span class="icon"></span>
    <a target="_blank" href="http://healthyself.womansblogs.org/">Healthy Self Blog</a></li>
</ul>
</div>

                    <div class="footer-content">
                    

    
		<ul class="social-links">
            
                      <li class="facebook-link">
                        <a href="https://www.facebook.com/womanshospitalbr ">
                           Facebook
                        </a>
                    </li>
                
                      <li class="twitter-link">
                        <a href="https://twitter.com/WomansHospital ">
                           Twitter
                        </a>
                    </li>
                
                      <li class="youtube-link">
                        <a href="https://www.youtube.com/user/WomansHospitalBR">
                           Youtube
                        </a>
                    </li>
                
                      <li class="wp-link">
                        <a href="http://womansblogs.org/ ">
                           Wordpress
                        </a>
                    </li>
                
                      <li class="pinterest-link">
                        <a href="http://www.pinterest.com/Womanshospital ">
                           Pinterest
                        </a>
                    </li>
                
                      <li class="instagram-link">
                        <a class="last" href="http://instagram.com/womanshospital">
                           Instagram
                        </a>
                    </li>
                
		</ul>

                    </div>
                </div>		
		    
                <div class="four columns womans-footer-nav">
                    <h4><span class="icon"></span><span class="plus-minus"></span>Woman's For </h4>
<div class="footer-content">
<ul>
    <li><a href="/patients-and-visitors">Patients &amp; Visitors </a></li>
    <li><a href="/for-health-professionals">Health Professionals</a> </li>
    <li><a href="/for-employees">Employees</a> </li>
    <li><a href="/giving-and-volunteering/volunteer">Volunteer</a></li>
    <li><a href="/for-board-of-directors">Board of Directors</a> </li>
    <li><a href="/our-services/children-and-men">Children &amp; Men </a></li>
    <li><a href="/about-womans/community">Community Health</a> </li>
    <li><a href="/for-job-applicants">Job Applicants </a></li>
</ul>
</div>
                </div>		
		
				<div class="four columns locations-and-maps">   
                    <h4>
<span class="icon"></span>
<span class="plus-minus"></span>
Locations &amp; Maps
</h4>
<div class="footer-content">
<address>
<span>Woman's Hospital</span><br />
100 Woman's Way<br />
Baton Rouge, LA 70817
</address>
<p>
Main Number:
(225) 927-1300
</p>
<p>
Patient Rooms:
(225) 231-5[+]<br />
<span class="ital">[last 3 digits of room number]</span>
</p>
<p>
Patient Info:
(225) 924-8157
</p>
<a href="/locations-and-maps"><button class="default-button pink" type="button">View More</button></a>
</div>	
				</div>
            </div>
      
	
	
        <div class="footer-utility-links">
             <ul>
    <li>
    <a href="/about-womans">About Woman's</a>
    </li>
    <li>
    <a href="/for-job-applicants/careers">Careers</a>
    </li>
    <li>
    <a href="/contact-us-form">Contact Us</a>
    </li>
    <li>
    <a href="/giving-and-volunteering/ways-to-give">Donate</a>
    </li>
    <li>
    <a href="/patients-and-visitors/billing-and-insurance">Your Bill</a>
    </li>
</ul>
        <br />
        
        <ul>
            <li><a href="#">Copyright 2014</a></li>
             <li>
<a href="/terms-and-conditions">Terms &amp; Conditions</a>
</li>
<li>
<a href="/privacy-policy">Privacy Policy</a>
</li>
<li>
<a href="http://www.medtouch.com/" target="_blank">by MedTouch</a>
</li>
        </ul>

		</div>
	
	
    
</footer>
<!--============================================= /MedTouch.Base: Common: Footer =============================================--> 


        
    
<script type="text/javascript">
//<![CDATA[
var Page_ValidationSummaries =  new Array(document.getElementById("bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88__summary"));
var Page_Validators =  new Array(document.getElementById("bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F6ADFFAE3DADB451AB530D89A2FD0307B_validator"), document.getElementById("bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F070FCA141E9A45D78611EA650F20FE77_validator"), document.getElementById("bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F1221445D10AF7533054C39908EB25E8CB4ABDC_validator"));
//]]>
</script>

<script type="text/javascript">
//<![CDATA[
var bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88__summary = document.all ? document.all["bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88__summary"] : document.getElementById("bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88__summary");
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88__summary.validationGroup = "form_E182B8B829CC4F76B8C6F092A1C1BC88_submit";
var bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F6ADFFAE3DADB451AB530D89A2FD0307B_validator = document.all ? document.all["bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F6ADFFAE3DADB451AB530D89A2FD0307B_validator"] : document.getElementById("bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F6ADFFAE3DADB451AB530D89A2FD0307B_validator");
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F6ADFFAE3DADB451AB530D89A2FD0307B_validator.controltovalidate = "bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F6ADFFAE3DADB451AB530D89A2FD0307B_validator.errormessage = "Full Name must have at least 0 and no more than 256 characters.";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F6ADFFAE3DADB451AB530D89A2FD0307B_validator.display = "Dynamic";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F6ADFFAE3DADB451AB530D89A2FD0307B_validator.validationGroup = "form_E182B8B829CC4F76B8C6F092A1C1BC88_submit";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F6ADFFAE3DADB451AB530D89A2FD0307B_validator.evaluationfunction = "RegularExpressionValidatorEvaluateIsValid";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F6ADFFAE3DADB451AB530D89A2FD0307B_validator.validationexpression = "(.|\\n){0,256}$";
var bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F070FCA141E9A45D78611EA650F20FE77_validator = document.all ? document.all["bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F070FCA141E9A45D78611EA650F20FE77_validator"] : document.getElementById("bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F070FCA141E9A45D78611EA650F20FE77_validator");
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F070FCA141E9A45D78611EA650F20FE77_validator.controltovalidate = "bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F070FCA141E9A45D78611EA650F20FE77_validator.errormessage = "The value of the Full Name field is not valid.";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F070FCA141E9A45D78611EA650F20FE77_validator.display = "Dynamic";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F070FCA141E9A45D78611EA650F20FE77_validator.validationGroup = "form_E182B8B829CC4F76B8C6F092A1C1BC88_submit";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F070FCA141E9A45D78611EA650F20FE77_validator.evaluationfunction = "RegularExpressionValidatorEvaluateIsValid";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F070FCA141E9A45D78611EA650F20FE77_validator.validationexpression = "^(.|\\n)*$";
var bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F1221445D10AF7533054C39908EB25E8CB4ABDC_validator = document.all ? document.all["bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F1221445D10AF7533054C39908EB25E8CB4ABDC_validator"] : document.getElementById("bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F1221445D10AF7533054C39908EB25E8CB4ABDC_validator");
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F1221445D10AF7533054C39908EB25E8CB4ABDC_validator.controltovalidate = "bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F122144";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F1221445D10AF7533054C39908EB25E8CB4ABDC_validator.errormessage = "Email Address contains an invalid address.";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F1221445D10AF7533054C39908EB25E8CB4ABDC_validator.display = "Dynamic";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F1221445D10AF7533054C39908EB25E8CB4ABDC_validator.validationGroup = "form_E182B8B829CC4F76B8C6F092A1C1BC88_submit";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F1221445D10AF7533054C39908EB25E8CB4ABDC_validator.evaluationfunction = "RegularExpressionValidatorEvaluateIsValid";
bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F1221445D10AF7533054C39908EB25E8CB4ABDC_validator.validationexpression = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
//]]>
</script>


<script type="text/javascript">
//<![CDATA[

(function(id) {
    var e = document.getElementById(id);
    if (e) {
        e.dispose = function() {
            Array.remove(Page_ValidationSummaries, document.getElementById(id));
        }
        e = null;
    }
})('bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88__summary');

var Page_ValidationActive = false;
if (typeof(ValidatorOnLoad) == "function") {
    ValidatorOnLoad();
}

function ValidatorOnSubmit() {
    if (Page_ValidationActive) {
        return ValidatorCommonOnSubmit();
    }
    else {
        return true;
    }
}
        
document.getElementById('bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F6ADFFAE3DADB451AB530D89A2FD0307B_validator').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F6ADFFAE3DADB451AB530D89A2FD0307B_validator'));
}

document.getElementById('bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F070FCA141E9A45D78611EA650F20FE77_validator').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_D896A45D641C4A49BD0EB64C4AF4290F070FCA141E9A45D78611EA650F20FE77_validator'));
}

document.getElementById('bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F1221445D10AF7533054C39908EB25E8CB4ABDC_validator').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('bottom_0_form_E182B8B829CC4F76B8C6F092A1C1BC88_field_1820EC7C7BEE43229B86A0753F1221445D10AF7533054C39908EB25E8CB4ABDC_validator'));
}
//]]>
</script>
</form>
    
	<script src="/assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>		
	<!-- Grab Google CDN jQuery. fall back to local if necessary -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>	    !window.jQuery && document.write('<script src="/assets/js/jquery-1.9.1.min.js"><\/script>')</script>
	<!-- Grab Google CDN jQuery UI. fall back to local if necessary -->
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script>	    !window.jQuery.ui && document.write('<script src="/assets/js/jquery-ui-1.10.4.min.js"><\/script>')</script>
    <script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>
	<!-- Local Scripts -->
	<script src="/assets/js/plugins.js"></script>
	<script src="/assets/js/tipsy.js"></script>
	<script src="/assets/js/script.js"></script>

    
    
<!--============================================= MedTouch.Base: Include: User Edited Script =============================================-->

<!--============================================= /MedTouch.Base: Include: User Edited Script =============================================-->

    
</body>
</html>