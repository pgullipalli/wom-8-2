<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/MainSite.Master" AutoEventWireup="true" %>

<%@ Register Src="~/UX/HTML/_includes/services-menulp.ascx" TagName="servicesmenu" TagPrefix="uc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="PageContent" runat="server"> 
    <uc:servicesmenu ID="servicesmenu" runat="server" />
	<section class="main-content grid landing-page">
		<img class="landing-image" src="/assets/images/girl-in-grass.jpg">

	    <div class="main-quote-mobile-wrapper"></div>
		<div class="grid">
		   <div class="quote">
		   		<div class="main-quote-desktop-wrapper">
			   		<div class="main-quote">
			   			<p>
			   				<a href="#">
			   					Once you choose <span>hope,</span> anything is possible"
			   					<span class="arrow"></span>
			   				</a>
			   			</p>
			   		</div>
			   	</div>
		   </div>

		   <div class="landing-jumps">
		   		<div class="main-heading-wrapper">		   		
		   			<h1>Breast Care at Woman's :)</h1>
				</div>
		   		<div class="grid">
		   			<div class="landing-jump-wrapper">
						<div class="landing-jump left">
						  <div class="landing-jump-image-wrapper">
						  	<a href="#">
						  		<img src="/assets/images/landing-jump.jpg">
						  	</a>
						  </div>
						  <p>
						  	<a href="#">I am newly diagnosed</a>
						  </p>
						  <a href="#" class="hover-overlay hide-on-phones">
						  	<span class="overlay-heading">I am Newly Diagnosed</span>
						 	<span class="overlay-description">The more you know, the more empowered you will be</span>					  	
						 	<span class="default-button black">Learn More</span>
						  </a>
						</div>
						<div class="landing-jump">
							<div class="landing-jump-image-wrapper">
						  		<a href="#">
						  			<img src="/assets/images/landing-jump.jpg">
						  		</a>
						  	</div>					  	
						  	<p>
						  		<a href="#">
						  			I need advanced treatment options
						  		</a>
						  	</p>
						  	<a href="#" class="hover-overlay hide-on-phones">
							  	<span class="overlay-heading">I need advanced treatment options</span>
							 	<span class="overlay-description">The more you know, the more empowered you will be</span>					  	
							 	<span class="default-button black">Learn More</span>
							</a>
						</div>
					</div>
				</div>	
				<div class="grid">
					<div class="landing-jump-wrapper">
						<div class="landing-jump left">
						  <div class="landing-jump-image-wrapper">
						  	<a href="#">
						  		<img src="/assets/images/landing-jump.jpg">
						  	</a>
						  </div>
						  <p>
						  	<a href="#">
						  		I am Interested in Prevention &amp; Detection
						  	</a>
						  </p>
						  <a href="#" class="hover-overlay hide-on-phones">
						  	<span class="overlay-heading">I am Interested in Prevention &amp; Detection</span>
						 	<span class="overlay-description">The more you know, the more empowered you will be</span>					  	
						 	<span class="default-button black">Learn More</span>
						  </a>
						</div>
						<div class="landing-jump">
						  <div class="landing-jump-image-wrapper">
						  	<a href="#">
						  		<img src="/assets/images/landing-jump.jpg">
						  	</a>
						  </div>
						  <p>
						  	<a href="#">
						  		I would like more Breast Cancer Information
						  	</a>
						  </p>
						  <a href="#" class="hover-overlay hide-on-phones">
						  	<span class="overlay-heading">I would like more Breast Cancer Information</span>
						 	<span class="overlay-description">The more you know, the more empowered you will be</span>					  	
						 	<span class="default-button black">Learn More</span>
						  </a>
						</div>
					</div>
				</div>

				<div class="grid">
					<div class="six columns">

					</div>

					<div class="six columns toggle">
						<div>
							<p class="all-services-toggle">
								<span></span>
								All Breast Care Services
							</p>
						</div>
					</div>
				</div>	
		   </div>
		   <div style="clear:both"></div>
		</div>	
		<div class="grid dropdown-content">
			<div class="nine columns">
				<div class="header1"></div>
				<h2>Every month, week and day of the year, our mission is focused on one goal: helping women become survivors and return to living healthy, active lives.</h2>
				<p>As the region's leader on comprehensive breast care, we commit our skills and resources daily to aiding women in their fight against breast cancer. From a dedicated breast patient navigator on staff, to an imaging department specializing in mammography, to advanced diagnosis, treatment, therapy, education and supportive care, our commitment is reflected in the breadth of services we provide. </p>

				<div class="grid">
					<div class="four columns mobile-accordion">
						<h3 class="accordion-handle">
							Prevention &amp; Detection
							<span class="toggle-icon"></span>
						</h3>
						<div class="accordion-content">
							<p>When detected early, more than 95 percent of breast cancer cases are treated successfully. Woman's offers advanced imaging, diagnosis, and education.</p>

							<ul>
								<li>
									<a href="#">About Mammography</a>
								</li>
								<li>
									<a href="#">Schedule a Mammogram</a>
								</li>
								<li>
									<a href="#">Mobile Screening Schedule</a>
								</li>
								<li>
									<a href="#">Breast Self Exam</a>
								</li>
								<li>
									<a href="#">Signs and Symptoms</a>
								</li>
								<li>
									<a href="#">Diagnosis and Staging</a>
								</li>
								<li>
									<a href="#">Genetic Testing</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="four columns mobile-accordion">
						<h3 class="accordion-handle">
							Treatment &amp; Support
							<span class="toggle-icon"></span>
						</h3>
						<div class="accordion-content">
							<p>Woman's is dedicated to meeting the unique needs of women who have been diagnosed with breast cancer. Our staff will guide our patients step-by-step.</p>

							<ul>
								<li>
									<a href="#">Types of Cancer</a>
								</li>
								<li>
									<a href="#">Treatment</a>
									<ul>
										<li>
											<a href="#">Lorem Ipsum</a>
										</li>
										<li>
											<a href="#">Dolor Ectum ei nam</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="#">Genetic Testing</a>
								</li>
								<li>
									<a href="#">Breast Reconstruction</a>
								</li>
								<li>
									<a href="#">Lymphedma Therapy</a>
								</li>
								<li>
									<a href="#">Accreditations</a>
								</li>
								<li>
									<a href="#">Navigator &amp; Support</a>
								</li>
								<li>
									<a href="#">Our Blog: Living with Cancer</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="four columns hide-on-mobile">
						<div class="video-container">
							<img src="/assets/images/video-still.jpg">
						</div>
						<h3>Woman's Video Title</h3>
						<p>Remind the special woman in your life to schedule an annual mammogram at Woman's</p>

						<button class="default-button black" type="button">Send the Email Now</button>
					</div>
				</div>
                        <!--------==========================Accordion JQUERY==============================-->
		<div class="accordion">
			<h3><a href="#">Accordion Panel One</a></h3>
			<div class="accordian_copy">
			<h3>Heading</h3>
			<p>Nam lectus neque, bibendum eg faibus et lacus et augue gravida dapibus. Teger faculis enim etu amet graon. Menius temu luctus met, consectetur adipiscing elit. Integer iaculis ulla facilisi ullam at nulla <a href="#">vitae velit rhoncus pellentesque</a>. Nam lectus neque, biliquet lacus et augue gravida dapibus done allerium cras ponex</p>
			</div>

			<h3><a href="#">Accordion Panel Two</a></h3>
			<div class="accordian_copy">
			<p>AccordionContent2</p>
            <h3>Sample Test</h3>
            <p>Blah Blah Blah</p>
			</div>

			<h3><a href="#">Accordion Panel Three</a></h3>
			<div class="accordian_copy">
			<p>AccordionContent2</p>
			</div>
		</div>
		<!--------==========================Accordion JQUERY==============================-->	
			</div>
			<div class="three columns">
				<div class="highlight-box right">
					<h2>Contact Us Today</h2>
					<p>We offer breast cancer detection and treatment in three convenient locations.</p>
					<h3>
						<a href="tel:+2252157981">225-215-7981</a>
					</h3>
				</div>

				<div class="mobile-gallery-wrapper"></div>

				<div class="callout">
					<h4>
						<span>Upcoming Events</span>
					</h4>
					
					<div class="callout-event-wrapper">
						<div class="callout-event">
							<div class="event-header">
								<a href="#">
									<span class="event-date">22 Jan</span>
									Woman's Ideal Protein Information
								</a>
							</div>

							<p class="callout-event-preview">Hinc disputationi ei nam, mei etudoctus tamquam suscipit te. Enim mediocritatem vel eine, in qui falli minimum. <a href="#" class="read-more">Read More</a> </p>
						</div>

						<div class="callout-event">
							<div class="event-header">
								<a href="#">
									<span class="event-date">22 Jan</span>
									Woman's Ideal Protein Information
								</a>
							</div>

							<p class="callout-event-preview">Hinc disputationi ei nam, mei etudoctus tamquam suscipit te. Enim mediocritatem vel eine, in qui falli minimum. <a href="#" class="read-more">Read More</a> </p>
						</div>
					</div>

					<button type="button" class="default-button black">View All Events</button>

				</div>
			</div>

			<div class="grid features hide-on-mobile">
				<!-- ################################## DESTOP ONLY ################################## -->
				<div class="three columns">
					<div class="feature-image-wrapper">
						<a href="#">
							<img src="/assets/images/landing-jump.jpg">
						</a>
					</div>
					<div class="feature-content">
						<div class="inner">
							<h4>
								<span>Headline</span>
							</h4>
							<p>
								<a href="#">It is time to schedule an annual mammogram at Woman's</a>
							</p>
						</div>
					</div>
				</div>
				<div class="three columns">
					<div class="feature-image-wrapper">
						<a href="#">
							<img src="/assets/images/landing-jump.jpg">
						</a>
					</div>
					<div class="feature-content">
						<div class="inner">
							<h4>
								<span>Headline</span>
							</h4>
							<p>
								<a href="#">It is time to schedule an annual mammogram at Woman's</a>
							</p>						
						</div>
					</div>
				</div>
				<div class="three columns">
					<div class="feature-image-wrapper">
						<a href="#">
							<img src="/assets/images/landing-jump.jpg">
						</a>
					</div>
					<div class="feature-content">
						<div class="inner">
							<h4>
								<span>Headline</span>
							</h4>
							<p>
								<a href="#">It is time to schedule an annual mammogram at Woman's</a>
							</p>						
						</div>
					</div>
				</div>
				<div class="three columns">
					<div class="feature-image-wrapper">
						<a href="#">
							<img src="/assets/images/landing-jump.jpg">
						</a>
					</div>
					<div class="feature-content">
						<div class="inner">
							<h4>
								<span>Headline</span>
							</h4>
							<p>
								<a href="#">It is time to schedule an annual mammogram at Woman's</a>
							</p>						
						</div>
					</div>
				</div>
				<!-- ################################## END DESKTOP ONLY ##################################-->

			</div>

				<!-- ################################## MOBILE ONLY SLIDER ##################################-->
				
				<div class="owl-carousel hide-on-desktop" >
				  <div>
				  	<div>
					  	<div class="feature-image-wrapper">
							<a href="#">
								<img src="/assets/images/landing-jump.jpg">
							</a>
						</div>
						<div class="feature-content">
							<div class="inner">
								<h4>
									<span>Headline</span>
								</h4>
								<p>
									<a href="#">It is time to schedule an annual mammogram at Woman's</a>
								</p>								
							</div>
						</div>
					</div>
				  </div>
				  
				  <div>
				  	<div>
					  	<div class="feature-image-wrapper">
							<a href="#">
								<img src="/assets/images/landing-jump.jpg">
							</a>
						</div>
						<div class="feature-content">
							<div class="inner">
								<h4>
									<span>Headline</span>
								</h4>
								<p>
									<a href="#">It is time to schedule an annual mammogram at Woman's</a>
								</p>								
							</div>
						</div>
					</div>
				  </div>
				  
				  <div>
				  	<div>
					  	<div class="feature-image-wrapper">
							<a href="#">
								<img src="/assets/images/landing-jump.jpg">
							</a>
						</div>
						<div class="feature-content">
							<div class="inner">
								<h4>
									<span>Headline</span>
								</h4>
								<p>
									<a href="#">It is time to schedule an annual mammogram at Woman's</a>
								</p>								
							</div>
						</div>
					</div>
				  </div>
				  
				  <div>
				  	<div>
					  	<div class="feature-image-wrapper">
							<a href="#">
								<img src="/assets/images/landing-jump.jpg">
							</a>
						</div>
						<div class="feature-content">
							<div class="inner">
								<h4>
									<span>Headline</span>
								</h4>
								<p>
									<a href="#">It is time to schedule an annual mammogram at Woman's</a>
								</p>								
							</div>
						</div>
					</div>
				  </div>

				</div>

				<!-- ################################## END MOBILE ONLY SLIDER ##################################-->
		</div>	
	</section>	
</asp:Content>