<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/MainSite.Master" AutoEventWireup="true" %>

<%@ Register Src="~/UX/HTML/_includes/services-menulp.ascx" TagName="servicesmenu" TagPrefix="uc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="PageContent" runat="server"> 
    <uc:servicesmenu ID="servicesmenu" runat="server" />
	<section class="main-content grid landing-pageb">

        <div class="content-copy nine columns">
            <h1><span>get</span>ready</h1>
            <div class="twelve columns">
                <div class="core-list">
                    <div class="copy">
                        <div class="copy-image"><img src="/assets/images/fpolist_01.jpg" class="left" alt="" /></div>
                        <div class="copy-headline"><h2>Pregnancy To Do</h2></div>
                    </div>
                    <div class="copy-overlay">
                        <div class="copy-overlay-image"><img src="/assets/images/fpolist_01b.jpg" class="left" alt="" /></div>
                        <div class="copy-overlay-text">Learn what is going on and important checklists for every step of your pregnancy journey!</div>
                    </div>
                </div> 
                <div class="core-list">
                    <div class="copy">
                        <div class="copy-image"><img src="/assets/images/fpolist_01.jpg" class="left" alt="" /></div>
                        <div class="copy-headline"><h2>Expecting Education</h2></div>
                    </div>
                    <div class="copy-overlay">
                        <div class="copy-overlay-image"><img src="/assets/images/fpolist_01b.jpg" class="left" alt="" /></div>
                        <div class="copy-overlay-text">Learn what is going on and important checklists for every step of your pregnancy journey!</div>
                    </div>
                </div> 
                <div class="core-list">
                    <div class="copy">
                        <div class="copy-image"><img src="/assets/images/fpolist_01.jpg" class="left" alt="" /></div>
                        <div class="copy-headline"><h2>Birth Option</h2></div>
                    </div>
                    <div class="copy-overlay">
                        <div class="copy-overlay-image"><img src="/assets/images/fpolist_01b.jpg" class="left" alt="" /></div>
                        <div class="copy-overlay-text">Learn what is going on and important checklists for every step of your pregnancy journey!</div>
                    </div>
                </div> 
                 <div class="core-list">
                    <div class="copy">
                        <div class="copy-image"><img src="/assets/images/fpolist_01.jpg" class="left" alt="" /></div>
                        <div class="copy-headline"><h2>Breastfeeding</h2></div>
                    </div>
                    <div class="copy-overlay">
                        <div class="copy-overlay-image"><img src="/assets/images/fpolist_01b.jpg" class="left" alt="" /></div>
                        <div class="copy-overlay-text">Learn what is going on and important checklists for every step of your pregnancy journey!</div>
                    </div>
                </div> 
                 <div class="core-list">
                    <div class="copy">
                        <div class="copy-image"><img src="/assets/images/fpolist_01.jpg" class="left" alt="" /></div>
                        <div class="copy-headline"><h2>Learn what is going on and important checklists for every step</h2></div>
                    </div>
                    <div class="copy-overlay">
                        <div class="copy-overlay-image"><img src="/assets/images/fpolist_01b.jpg" class="left" alt="" /></div>
                        <div class="copy-overlay-text">Learn what is going on and important checklists for every step of your pregnancy journey!</div>
                    </div>
                </div> 
                
            </div>
        </div>
        
        <!--BGIMAGE------------------------->
        <div class="landing-fpo"><img src="/assets/images/fpo_01.jpg" alt="fpo" class="" /></div>
        
        <!----------------------------PTA----------------------------->
        
        <div class="pta twelve columns" style="background-image: url('/assets/images/cta_01.jpg');">
            <div class="copy eight columns">
                 <div class="content">
                <q>Thank you for offering the 
                Understanding Birth e-class.
                My partner and I Iearned so much,
                that matched our style</q>
                </div>
            </div>
        </div>
         <div class="pta twelve columns" style="background-image: url('/assets/images/cta_02.jpg');">
             <div class="copyb ten columns">
                 <div class="six columns">
                     <div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/dVIhFL-n8R0" frameborder="0" allowfullscreen></iframe></div>
                </div>            
                 <div class="six columns">
                     <div class="content">
                    <q>Reduce the risk sleep-related causes of infant death. Do not share the bed with your baby</q>
                     </div>
                </div>
            </div>
        </div>       
	
	</section>	
</asp:Content>