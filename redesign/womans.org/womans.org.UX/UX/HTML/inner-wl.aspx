﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.Master" AutoEventWireup="true" %>
<%@ Register TagPrefix="uc" TagName="headline" Src="/UX/HTML/_includes/headline.ascx" %>
<%@ Register TagPrefix="uc" TagName="samplecontent" Src="/UX/HTML/_includes/sample_content.ascx" %>
<%@ Register TagPrefix="uc" TagName="callout" Src="/UX/HTML/_includes/callout.ascx" %>
<%@ Register TagPrefix="uc" TagName="prox" Src="/UX/HTML/_includes/prox.ascx" %>


<asp:Content ID="Content3" ContentPlaceHolderID="TopPanel" runat="server">
	<div class="headline">
		<table>
			<tr>
				<td class="headline-main-heading">
					<div>
						<uc:headline ID="headline" runat="server"/>
					</div>
				</td>
				<td>
					<div class="headline-nav">
			    		<uc:prox ID="prox" runat="server"/>
			    	</div>
				</td>
			</tr>
		</table>
		
		
	</div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
	<uc:samplecontent ID="samplecontent" runat="server"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
    <uc:callout ID="callout" runat="server"/>
</asp:Content>
