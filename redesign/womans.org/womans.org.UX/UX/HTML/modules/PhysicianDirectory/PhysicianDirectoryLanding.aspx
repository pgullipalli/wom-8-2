﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true"  %>
<%@ Register src="/ux/html/_includes/modules/physicians/PhysicianSearch.ascx" tagname="physiciansearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/physicians/PhysicianAtoZ.ascx" tagname="physicianaz" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/physicians/DirectoryPDF.ascx" tagname="directorypdf" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/callout.ascx" tagname="callout" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">

	<article>
		<h1>Find a Doctor</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu accumsan felis. Proin rutrum massa et felis suscipit pulvinar. Maecenas iaculis lorem elit, vel sagittis risus. Vivamus tristique ipsum at dolor rhoncus sed hendrerit est faucibus.</p>
	</article>
    <%--<module:directorypdf ID="directorypdf" runat="server" />--%>
	<module:physiciansearch ID="physiciansearch" runat="server" />
	<module:physicianaz ID="physicianaz" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
   <module:callout ID="callout" runat="server" />
</asp:Content>