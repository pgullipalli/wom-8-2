﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true" %>
<%@ Register TagPrefix="uc" TagName="headline" Src="/UX/HTML/_includes/headline.ascx" %>
<%@ Register src="/ux/html/_includes/modules/physicians/PhysicianDetail.ascx" tagname="physicianprofile" tagprefix="module" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainHeading" runat="server">
	<uc:headline ID="headline" runat="server"/>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
	<module:physicianprofile ID="physicianprofile" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
	<div class="callout-wrapper">
		<div class="callout">
			<div class="physician-profile-pic">
				<img class="profile-pic" src="/assets/images/physician-detail-profile-pic.jpg">
			</div>
			<div class="physician-profile-vid">
				<!--<img src="/assets/images/physician-video.png">-->
				<iframe width="560" height="315" src="//www.youtube.com/embed/FCt2l5Yl-wY" frameborder="0"></iframe>
			</div>
            <div class="address-callout"><a href="#">View Profile</a></div>
			<div class="physician-contact callout">
				<h4>
					<span>Contact</span>
				</h4>

				<div class="location-map">
					<img src="/assets/images/map.jpg">
					<a href="#" class="button black">Get Directions</a>
				</div>
				<div class="address-callout">
					<address>
						The Allergy Asthma & Health Clinic LLC<br/>
                        8786 Goodwood Blvd<br>
						Suite 109<br>
						Baton Rouge, LA 70806
					</address>
					<p>
						PH: 
						<a href="tel:225-922-5224">(225)-922-5224</a>
					</p>
					<p>
						FX: 
						<a href="225-922-5229">(225)-922-5229</a>
					</p>
				</div>
				
			</div>
            <div class="physician-contact callout">
				<h4>
					<span>Contact</span>
				</h4>

				<div class="location-map">
					<img src="/assets/images/map.jpg">
				</div>
				<div class="address-callout">
					<address>
						Manually entered physician group<br/>
                        8786 Goodwood Blvd<br>
						Suite 109<br>
						Baton Rouge, LA 70806
					</address>
					<p>
						PH: 
						<a href="tel:225-922-5224">(225)-922-5224</a>
					</p>
					<p>
						FX: 
						<a href="225-922-5229">(225)-922-5229</a>
					</p>
				</div>
				
			</div>
		</div>
	</div>
</asp:Content>