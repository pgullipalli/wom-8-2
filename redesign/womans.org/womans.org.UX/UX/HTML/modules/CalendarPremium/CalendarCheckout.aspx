<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWR.master" AutoEventWireup="true"  %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventCategories.ascx" tagname="eventcategories" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventCheckoutForm.ascx" tagname="eventcheckoutform" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventQuickSearch.ascx" tagname="eventquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventShoppingCartSummary.ascx" tagname="eventshoppingcart" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
	<module:eventshoppingcart ID="eventshoppingcart" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">
	<h1 class="module-ce-checkout-header">Event Registration Checkout</h1>
	<module:eventcheckoutform ID="eventcheckoutform" runat="server" />	
</asp:Content>