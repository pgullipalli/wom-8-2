﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/calendar/EventCategories.ascx" tagname="eventcategories" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventDayPicker.ascx" tagname="eventdaypicker" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventSearch.ascx" tagname="eventsearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventQuickSearch.ascx" tagname="eventquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventTopicDetail.ascx" tagname="eventtopicdetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventTopicListing.ascx" tagname="eventtopiclist" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventSessionListing.ascx" tagname="eventsessionlist" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventShoppingCartSummary.ascx" tagname="eventshoppingcart" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/RelatedTopicEvents.ascx" tagname="relatedtopicevents" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/RelatedSessionEvents.ascx" tagname="relatedsessionevents" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/UpcomingTopicEvents.ascx" tagname="upcomingtopicevents" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/UpcomingSessionEvents.ascx" tagname="upcomingsessionevents" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
	<module:eventtopicdetail ID="eventtopicdetail" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
<div class="callout">
	<module:eventshoppingcart ID="eventshoppingcart" runat="server" />
	<module:eventquicksearch ID="eventquicksearch" runat="server" />
	<module:relatedtopicevents ID="relatedtopicevents" runat="server" />
	<module:relatedsessionevents ID="relatedsessionevents" runat="server" />
</div>
</asp:Content>
