<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true"  %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventCategories.ascx" tagname="eventcategories" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventQuickSearch.ascx" tagname="eventquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventShoppingCartTop.ascx" tagname="eventshoppingcarttop" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="RightPanel" runat="server">
	<module:eventquicksearch ID="eventquicksearch" runat="server" />
	<module:eventcategories ID="eventcategories" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">
	<module:eventshoppingcarttop ID="eventshoppingcarttop" runat="server" />
</asp:Content>
