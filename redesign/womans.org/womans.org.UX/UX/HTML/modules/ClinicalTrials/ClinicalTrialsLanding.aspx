﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true" %>
<%@ Register src="/ux/html/_includes/modules/clinical_trials/CTDetail.ascx" tagname="ctdetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/clinical_trials/CTSearchBox.ascx" tagname="ctsearchbox" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/clinical_trials/CTQuickSearchBox.ascx" tagname="ctquicksearchbox" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/clinical_trials/CTSearchResults.ascx" tagname="ctsearchresults" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/clinical_trials/RelatedClinicalTrials.ascx" tagname="relatedclinicaltrials" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/clinical_trials/RelatedClinicalTrialsCallout.ascx" tagname="relatedclinicaltrialscallout" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/callout.ascx" tagname="callout" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
	<div class="breadcrumbs">
			<a href="index.aspx">Home</a><span class="separator"> &gt; </span>
			Clinical Trials 
	</div>
	<article>
		<h1>Clinical Trial Search</h1>						
	</article>
    <module:ctsearchbox ID="ctsearchbox" runat="server"  />
    <module:relatedclinicaltrials ID="relatedclinicaltrials" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
	<module:callout ID="callout" runat="server" />
</asp:Content>
