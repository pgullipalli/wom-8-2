﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master"
    AutoEventWireup="true" %>

<%@ Register Src="/ux/html/_includes/modules/blog/BlogListing.ascx" TagName="bloglisting" TagPrefix="module" %>
<%@ Register Src="/ux/html/_includes/modules/blog/BlogSearch.ascx" TagName="blogsearch" TagPrefix="module" %>
<%@ Register Src="/ux/html/_includes/modules/blog/BlogCategories.ascx" TagName="blogcategories" TagPrefix="module" %>
<%@ Register Src="/ux/html/_includes/modules/blog/BlogTopics.ascx" TagName="blogtopics" TagPrefix="module" %>
<%@ Register Src="/ux/html/_includes/modules/blog/BlogArchive.ascx" TagName="blogarchive" TagPrefix="module" %>


<%@ Register TagPrefix="uc" TagName="breadcrumb" Src="/UX/HTML/_includes/breadcrumb.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">

    <article>
        <h1>Blog Landing Page</h1>
        <h2>This is Content Display sublayout</h2>
        <p>This content comes from Content Copy field of the blog landing item</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu accumsan felis. Proin rutrum massa et felis suscipit pulvinar. Maecenas iaculis lorem elit, vel sagittis risus. Vivamus tristique ipsum at dolor rhoncus sed hendrerit est faucibus. Aliquam luctus aliquet viverra. Fusce ligula nisi, luctus ac ornare vel, tincidunt quis velit. </p>
    </article>


    <module:bloglisting ID="bloglisting" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
    <module:blogsearch ID="blogserach" runat="server" />
    <module:blogcategories ID="blogcategories" runat="server" />
    <module:blogtopics ID="blogtopics" runat="server" />
    <module:blogarchive ID="blogarchive" runat="server" />
</asp:Content>
