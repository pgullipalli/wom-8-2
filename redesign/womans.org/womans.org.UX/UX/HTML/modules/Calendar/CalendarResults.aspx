﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWR.master" AutoEventWireup="true"  %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventCategories.ascx" tagname="eventcategories" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventDayPicker.ascx" tagname="eventdaypicker" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventSearch.ascx" tagname="eventsearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventQuickSearch.ascx" tagname="eventquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventTopicDetail.ascx" tagname="eventtopicdetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventTopicListing.ascx" tagname="eventtopiclist" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/EventSessionListing.ascx" tagname="eventsessionlist" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/RelatedTopicEvents.ascx" tagname="relatedtopicevents" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/RelatedSessionEvents.ascx" tagname="relatedsessionevents" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/UpcomingTopicEvents.ascx" tagname="upcomingtopicevents" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/calendar/UpcomingSessionEvents.ascx" tagname="upcomingsessionevents" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
	<module:eventquicksearch ID="eventquicksearch" runat="server" />
	<module:eventdaypicker ID="eventdaypicker" runat="server" />
	<module:eventcategories ID="eventcategories" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">
<div class="content">
	<article>
		<h1>Calendar (h1)</h1>
		<p>Lorem ipsum dolor sit amet, <b>bold text (b)</b>consectetur adipiscing elit. Curabitur <strong>bold text (strong)</strong>condimentum blandit diam 
		pharetra viverra. <i>Italic text (i)</i>Cras rutrum. <em>Italic text (em)</em>Urna eu consequat tempor, massa lacus. Ipsum lorem dolor amet sit aliquet 
		eget scelerisque eo consequat.</p>
    </article>	
    <module:eventtopiclist ID="eventtopiclist" runat="server" />
    <module:eventsessionlist ID="eventsessionlist" runat="server" />
	
</div>	
</asp:Content>
