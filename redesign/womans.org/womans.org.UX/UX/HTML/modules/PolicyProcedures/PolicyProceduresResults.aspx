﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWR.master" AutoEventWireup="true"  %>

<%@ Register src="/ux/html/_includes/modules/policyprocedures/FeaturedPolicyProcedure.ascx" tagname="featuredpolicyprocedure" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/LatestPolicyProcedure.ascx" tagname="latestpolicyprocedure" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/PolicyProcedureDetail.ascx" tagname="policyproceduredetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/PolicyProcedureListing.ascx" tagname="policyprocedurelisting" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/PolicyProcedureSearch.ascx" tagname="policyproceduresearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/PolicyProcedureQuickSearch.ascx" tagname="policyprocedurequicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/PolicyProcedureSitemap.ascx" tagname="policyproceduresitemap" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/policyprocedures/RelatedPolicyProcedure.ascx" tagname="relatedpolicyprocedure" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
	<module:policyprocedurequicksearch ID="policyprocedurequicksearch" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">
	<article>
		<h1>Policy Procedure (h1)</h1>
		<p>Lorem ipsum dolor sit amet, <b>bold text (b)</b>consectetur adipiscing elit. Curabitur <strong>bold text (strong)</strong>condimentum blandit diam 
		pharetra viverra. <i>Italic text (i)</i>Cras rutrum. <em>Italic text (em)</em>Urna eu consequat tempor, massa lacus. Ipsum lorem dolor amet sit aliquet 
		eget scelerisque eo consequat.</p>
    </article>	
    <module:policyprocedurelisting ID="policyprocedurelisting" runat="server" />
</asp:Content>
