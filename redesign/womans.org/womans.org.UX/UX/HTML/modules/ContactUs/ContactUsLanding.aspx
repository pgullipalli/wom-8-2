﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWR.master" AutoEventWireup="true" %>

<%--<%@ Register TagPrefix="uc" TagName="prox" Src="/UX/HTML/_includes/prox.ascx" %>--%>
<%@ Register src="/ux/html/_includes/modules/contact_us/Form.ascx" tagname="contactus" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
    <%--<uc:prox id="uxProx" runat="server"/>--%>
	Prox
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">
    <link type="text/css" rel="stylesheet" href="/sitecore%20modules/shell/Web%20Forms%20for%20Marketers/themes/MedTouch.css">
	<div class="content">	
	<article>
		<h1>Contact Us (h1)</h1>
		<p>Lorem ipsum dolor sit amet, <b>bold text (b)</b>consectetur adipiscing elit. Curabitur <strong>bold text (strong)</strong>condimentum blandit diam 
		pharetra viverra. <i>Italic text (i)</i>Cras rutrum. <em>Italic text (em)</em>Urna eu consequat tempor, massa lacus. Ipsum lorem dolor amet sit aliquet 
		eget scelerisque eo consequat.</p>
    </article>	
    <module:contactus ID="contactus" runat="server" />
	</div>
</asp:Content>
