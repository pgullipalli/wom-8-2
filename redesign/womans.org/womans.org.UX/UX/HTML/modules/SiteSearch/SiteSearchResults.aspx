﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/search/AdvancedSearchResults.ascx" tagname="advancedsearchresults" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/search/AdvancedSearchResultsWithPriority.ascx" tagname="advancedsearchresultspriority" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/search/BasicSiteSearchBox.ascx" tagname="basicsitesearchbox" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/search/BasicSearchResults.ascx" tagname="basicsearchresults" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/search/SearchAgain.ascx" tagname="searchagain" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/search/SearchFilter.ascx" tagname="searchfilter" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
    
	<article>
		<h1>Search Results</h1>
	</article>
	<module:searchagain ID="searchagain" runat="server" />
	<module:advancedsearchresultspriority ID="advancedsearchresultspriority" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
    <module:searchfilter ID="searchfilter" runat="server" />
</asp:Content>
