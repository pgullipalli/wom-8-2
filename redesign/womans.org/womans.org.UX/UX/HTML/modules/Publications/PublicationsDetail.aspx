﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/publications/LatestPublications.ascx" tagname="latestpublications" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/PublicationArchive.ascx" tagname="publicationarchive" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/PublicationCategories.ascx" tagname="publicationcategories" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/PublicationDetail.ascx" tagname="publicationdetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/PublicationListing.ascx" tagname="publicationlisting" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/PublicationSearch.ascx" tagname="publicationsearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/PublicationQuickSearch.ascx" tagname="publicationquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/RelatedPublications.ascx" tagname="relatedpublications" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/publications/FeaturedPublications.ascx" tagname="featuredpublications" tagprefix="module" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
    

    <module:publicationdetail ID="publicationdetail"  runat="server" />
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
    <module:relatedpublications ID="relatedpublications" runat="server" />
</asp:Content>
