﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/news/NewsSearch.ascx" tagname="newssearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/news/NewsQuickSearch.ascx" tagname="newsquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/news/NewsCategories.ascx" tagname="newscat" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/news/NewsArchive.ascx" tagname="newsarchive" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/news/FeaturedNews.ascx" tagname="featurednews" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/news/LatestNews.ascx" tagname="latestnews" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/news/NewsListing.ascx" tagname="newslisting" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/news/NewsDetail.ascx" tagname="newsdetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/news/RelatedNews.ascx" tagname="relatednews" tagprefix="module" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
<div class="content"> 
	<module:newsdetail ID="newsdetail" runat="server" />
</div> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
    <module:relatednews ID="relatednews" runat="server" />
    <module:featurednews ID="featurednews" runat="server" />
    <module:latestnews ID="latestnews" runat="server" />
</asp:Content>
