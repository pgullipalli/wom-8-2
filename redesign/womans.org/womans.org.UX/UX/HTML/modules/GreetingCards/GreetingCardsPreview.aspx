﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWR.master" AutoEventWireup="true" %>

<%@ Register TagPrefix="uc" TagName="prox" Src="/UX/HTML/_includes/prox.ascx" %>
<%@ Register src="/ux/html/_includes/modules/greeting_cards/GreetingCardCategories.ascx" tagname="greetingcardcategories" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/greeting_cards/GreetingCardForm.ascx" tagname="greetingcardform" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/greeting_cards/GreetingCardPreview.ascx" tagname="greetingcardpreview" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" runat="server">
    <uc:prox ID="prox" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPanel" runat="server">
    <div class="breadcrumbs">
		<a href="index.aspx">Home</a><span class="separator"> &gt; </span>
		Greeting Card Preview
	</div>
	<article>
		<h1>Greeting Card Preview</h1>					
	</article>
	<module:greetingcardpreview ID="greetingcardpreview" runat="server" />
</asp:Content>
