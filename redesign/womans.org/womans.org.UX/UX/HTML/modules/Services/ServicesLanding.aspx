﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/services/RelatedServices.ascx" tagname="relatedservices" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/services/ServicesAlphaCustom.ascx" tagname="servicesalpha" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/services/ServicesListing.ascx" tagname="serviceslisting" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/services/ServicesQuickSearch.ascx" tagname="servicesquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/services/ServicesSearch.ascx" tagname="servicessearch" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
	<div class="content">
	<article>
		<h1>Services</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu accumsan felis. Proin rutrum massa et felis suscipit pulvinar. Maecenas iaculis lorem elit, vel sagittis risus. Vivamus tristique ipsum at dolor rhoncus sed hendrerit est faucibus.</p>
	</article>

	<module:servicesalpha ID="servicesalpha" runat="server" />
	<module:servicessearch ID="servicessearch" runat="server" />
    <module:serviceslisting ID="serviceslisting" runat="server"/>
	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
    <module:relatedservices ID="relatedservices" runat="server" />
</asp:Content>
