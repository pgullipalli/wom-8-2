﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/faq/FAQCategories.ascx" tagname="faqcategories" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FAQDetail.ascx" tagname="faqdetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FAQListing.ascx" tagname="faqlisting" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FAQQuickSearch.ascx" tagname="faqquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FAQSearch.ascx" tagname="faqsearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FeaturedFAQ.ascx" tagname="featuredfaq" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/RelatedFAQ.ascx" tagname="relatedfaq" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
	<article>
		<h1>FAQ Search</h1>
							
		<p>Lorem ipsum dolor sit amet, <b>bold text (b)</b>consectetur adipiscing elit. Curabitur <strong>bold text (strong)</strong>condimentum blandit diam 
		pharetra viverra. <i>Italic text (i)</i>Cras rutrum. <em>Italic text (em)</em>Urna eu consequat tempor.</p>

	</article>	
						
	<module:faqsearch ID="faqsearch" runat="server" />
	<module:featuredfaq ID="featuredfaq" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
    <module:faqcategories ID="faqcategories" runat="server" />
</asp:Content>
