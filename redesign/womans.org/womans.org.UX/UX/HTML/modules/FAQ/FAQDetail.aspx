﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UX/HTML/masters/TwoColumnWL.master" AutoEventWireup="true" %>

<%@ Register src="/ux/html/_includes/modules/faq/FAQCategories.ascx" tagname="faqcategories" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FAQDetail.ascx" tagname="faqdetail" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FAQListing.ascx" tagname="faqlisting" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FAQQuickSearch.ascx" tagname="faqquicksearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FAQSearch.ascx" tagname="faqsearch" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/FeaturedFAQ.ascx" tagname="featuredfaq" tagprefix="module" %>
<%@ Register src="/ux/html/_includes/modules/faq/RelatedFAQ.ascx" tagname="relatedfaq" tagprefix="module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPanel" runat="server">
									
	<module:faqdetail ID="faqdetail" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RightPanel" runat="server">
    <module:relatedfaq ID="relatedfaq" runat="server" />
</asp:Content>
