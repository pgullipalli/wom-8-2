$(function () {
    {
        var formElement = $('div.scfForm');

        if (formElement.length > 0) {
            var copyAddress = $(".disableBox input");
            // Load form elements to be copied
            var PAddress = jQuery(".custom_wffm_billing").find("label:contains('Address')").next().find("input:text"),
                    PCity = jQuery(".custom_wffm_billing").find("label:contains('City')").next().find("input:text"),
                    PState = jQuery(".custom_wffm_billing").find("label:contains('State')").next().find("select"),
                    PZipCode = jQuery(".custom_wffm_billing").find("label:contains('Zip Code')").next().find("input:text"),
                    enableCopyCheck = function () {
                        var complete = true;
                        switch (true) {
                            case PAddress.val() === "": complete = false;
                            case PCity.val() === "": complete = false;
                            case PState.find(":selected").val() === "": complete = false;
                            case PZipCode.val() === "": complete = false;
                        }
                        if (complete) copyAddress.prop("disabled", false);
                        else copyAddress.prop("disabled", true)
                    },
                    updateBillingAddress = function () {
                        // Set Copy Values Values
                        var addressSource = PAddress.val() || false,
                            citySource = PCity.val() || false,
                            stateSource = PState.find(":selected").val() || false,
                            zipcodeSource = PZipCode.val() || false;

                        // Load form elements to copy into
                        var BAddress = jQuery("label:contains('Billing Address')").next().find("input:text"),
                            BCity = jQuery("label:contains('Billing City')").next().find("input:text"),
                            BState = jQuery("label:contains('Billing State')").next().find("select"),
                            BZipCode = jQuery("label:contains('Billing Zip Code')").next().find("input:text");

                        // Perform action based on copyAddress check status
                        if (copyAddress.is(':checked')) {
                            // Checked: copy patient address to billing address 
                            BAddress.val(PAddress.val()).attr("readonly", true).toggleClass('disable');
                            BCity.val(PCity.val()).attr("readonly", true).toggleClass('disable');
                            BState.val(PState.find(":selected").val()).attr("readonly", true).trigger("change").toggleClass('disable');
                            BZipCode.val(PZipCode.val()).attr("readonly", true).toggleClass('disable');
                        } else {
                            // Unchecked: clear info from billing address
                            BAddress.attr("readonly", false).toggleClass('disable');
                            BCity.attr("readonly", false).toggleClass('disable');
                            BState.attr("readonly", false).toggleClass('disable');
                            BZipCode.attr("readonly", false).toggleClass('disable');
                        }
                    };


            // Initially disable Address Copy Checkbox
            enableCopyCheck();

            // Watch for changes to address elements and enable/disbale copy button as required
            PAddress.on("change blur", enableCopyCheck);
            PCity.on("change blur", enableCopyCheck);
            PState.on("change blur", enableCopyCheck);
            PZipCode.on("change blur", enableCopyCheck);

            // Check if box is already on, if so trigger change event
            if (copyAddress.is(':checked')) updateBillingAddress;

            // copyAddress checkbox event handler
            copyAddress.on("change", updateBillingAddress);
        }
        // End of form Check
    }

    //making total amount field read only. 
    $('.totalAmount input').prop('readonly', true);

    //populating the total amount field in wellness gift card form
    //grab the value of the radio button
    var deliveryValue = $('input[type=radio]:checked', '.deliveryOption').val();


    var deliverGift = function () {

        $('.wellnessGCAmount input').on('change', function () {
            if (deliveryValue === "Ship FedEx Ground") {
                //grab the value of the wellness amount
                var giftValue = $('input[type=text]', '.wellnessGCAmount').val();
                if (Number(giftValue)) {
                    var totalAmount = Number(giftValue) + 5;
                    $('input[type=text]', '.totalAmount').val(totalAmount.toFixed(2));

                }
            }
        });
    };

    var deliverGiftWithNoUpdate = function () {
        if (deliveryValue === "Ship FedEx Ground") {
            //grab the value of the wellness amount
            var giftValue = $('input[type=text]', '.wellnessGCAmount').val();
            if (Number(giftValue)) {
                var totalAmount = Number(giftValue) + 5;
                $('input[type=text]', '.totalAmount').val(totalAmount.toFixed(2));

            }
        }

    };

    var deliverPlain = function () {

        //grab the value of the wellness amount
        var plainGiftValue = $('input[type=text]', '.wellnessGCAmount').val();
        var originalVal = Number(plainGiftValue);
        $('input[type=text]', '.totalAmount').val(originalVal.toFixed(2));

    };

    $('.wellnessGCAmount input').on('change', function () {
        if (deliveryValue !== "Ship FedEx Ground") {
            deliverPlain();
        }
    });



    $('.deliveryOption input').change(function () {

        deliveryValue = $('input[type=radio]:checked', '.deliveryOption').val();

        if (deliveryValue !== "Ship FedEx Ground") {

            deliverPlain();

        } else {

            deliverGiftWithNoUpdate();
        }

    });

    deliverGift();
});
