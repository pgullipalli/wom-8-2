// Avoid console errors in browsers that lack a console (cough... legacy IE... cough...)
;(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

/*
 * jQuery Extra Psuedo Selectors
 */
;(function($) {
        function getNthIndex(cur, dir) {
                var t = cur, idx = 0;
                while (cur = cur[dir] ) {
                        if (t.tagName == cur.tagName) {
                                idx++;
                        }
                }
                return idx;
        }

        function isNthOf(elm, pattern, dir) {
                var position = getNthIndex(elm, dir), loop;
                if (pattern == "odd" || pattern == "even") {
                        loop = 2;
                        position -= !(pattern == "odd");
                } else {
                        var nth = pattern.indexOf("n");
                        if (nth > -1) {
                                loop = parseInt(pattern, 10) || parseInt(pattern.substring(0, nth) + "1", 10);
                                position -= (parseInt(pattern.substring(nth + 1), 10) || 0) - 1;
                        } else {
                                loop = position + 1;
                                position -= parseInt(pattern, 10) - 1;
                        }
                }
                return (loop<0 ? position<=0 : position >= 0) && position % loop == 0
        }

        var pseudos = {
                "first-of-type": function(elm) {
                        return getNthIndex(elm, "previousSibling") == 0;
                },
                "last-of-type": function(elm) { 
                        return getNthIndex(elm, "nextSibling") == 0;
                },
                "only-of-type": function(elm) { 
                        return pseudos["first-of-type"](elm) && pseudos["last-of-type"](elm);
                },
                "nth-of-type": function(elm, i, match) {
                        return isNthOf(elm, match[3], "previousSibling");
                },
                "nth-last-of-type": function(elm, i, match) {
                        return isNthOf(elm, match[3], "nextSibling");
                }                
        }
        $.extend($.expr[':'], pseudos);
}(jQuery));

/*
 * imagesLoaded v3.1.4
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */
;(function(){function e(){}function t(e,t){for(var n=e.length;n--;)if(e[n].listener===t)return n;return-1}function n(e){return function(){return this[e].apply(this,arguments)}}var i=e.prototype,r=this,o=r.EventEmitter;i.getListeners=function(e){var t,n,i=this._getEvents();if("object"==typeof e){t={};for(n in i)i.hasOwnProperty(n)&&e.test(n)&&(t[n]=i[n])}else t=i[e]||(i[e]=[]);return t},i.flattenListeners=function(e){var t,n=[];for(t=0;e.length>t;t+=1)n.push(e[t].listener);return n},i.getListenersAsObject=function(e){var t,n=this.getListeners(e);return n instanceof Array&&(t={},t[e]=n),t||n},i.addListener=function(e,n){var i,r=this.getListenersAsObject(e),o="object"==typeof n;for(i in r)r.hasOwnProperty(i)&&-1===t(r[i],n)&&r[i].push(o?n:{listener:n,once:!1});return this},i.on=n("addListener"),i.addOnceListener=function(e,t){return this.addListener(e,{listener:t,once:!0})},i.once=n("addOnceListener"),i.defineEvent=function(e){return this.getListeners(e),this},i.defineEvents=function(e){for(var t=0;e.length>t;t+=1)this.defineEvent(e[t]);return this},i.removeListener=function(e,n){var i,r,o=this.getListenersAsObject(e);for(r in o)o.hasOwnProperty(r)&&(i=t(o[r],n),-1!==i&&o[r].splice(i,1));return this},i.off=n("removeListener"),i.addListeners=function(e,t){return this.manipulateListeners(!1,e,t)},i.removeListeners=function(e,t){return this.manipulateListeners(!0,e,t)},i.manipulateListeners=function(e,t,n){var i,r,o=e?this.removeListener:this.addListener,s=e?this.removeListeners:this.addListeners;if("object"!=typeof t||t instanceof RegExp)for(i=n.length;i--;)o.call(this,t,n[i]);else for(i in t)t.hasOwnProperty(i)&&(r=t[i])&&("function"==typeof r?o.call(this,i,r):s.call(this,i,r));return this},i.removeEvent=function(e){var t,n=typeof e,i=this._getEvents();if("string"===n)delete i[e];else if("object"===n)for(t in i)i.hasOwnProperty(t)&&e.test(t)&&delete i[t];else delete this._events;return this},i.removeAllListeners=n("removeEvent"),i.emitEvent=function(e,t){var n,i,r,o,s=this.getListenersAsObject(e);for(r in s)if(s.hasOwnProperty(r))for(i=s[r].length;i--;)n=s[r][i],n.once===!0&&this.removeListener(e,n.listener),o=n.listener.apply(this,t||[]),o===this._getOnceReturnValue()&&this.removeListener(e,n.listener);return this},i.trigger=n("emitEvent"),i.emit=function(e){var t=Array.prototype.slice.call(arguments,1);return this.emitEvent(e,t)},i.setOnceReturnValue=function(e){return this._onceReturnValue=e,this},i._getOnceReturnValue=function(){return this.hasOwnProperty("_onceReturnValue")?this._onceReturnValue:!0},i._getEvents=function(){return this._events||(this._events={})},e.noConflict=function(){return r.EventEmitter=o,e},"function"==typeof define&&define.amd?define("eventEmitter/EventEmitter",[],function(){return e}):"object"==typeof module&&module.exports?module.exports=e:this.EventEmitter=e}).call(this),function(e){function t(t){var n=e.event;return n.target=n.target||n.srcElement||t,n}var n=document.documentElement,i=function(){};n.addEventListener?i=function(e,t,n){e.addEventListener(t,n,!1)}:n.attachEvent&&(i=function(e,n,i){e[n+i]=i.handleEvent?function(){var n=t(e);i.handleEvent.call(i,n)}:function(){var n=t(e);i.call(e,n)},e.attachEvent("on"+n,e[n+i])});var r=function(){};n.removeEventListener?r=function(e,t,n){e.removeEventListener(t,n,!1)}:n.detachEvent&&(r=function(e,t,n){e.detachEvent("on"+t,e[t+n]);try{delete e[t+n]}catch(i){e[t+n]=void 0}});var o={bind:i,unbind:r};"function"==typeof define&&define.amd?define("eventie/eventie",o):e.eventie=o}(this),function(e,t){"function"==typeof define&&define.amd?define(["eventEmitter/EventEmitter","eventie/eventie"],function(n,i){return t(e,n,i)}):"object"==typeof exports?module.exports=t(e,require("eventEmitter"),require("eventie")):e.imagesLoaded=t(e,e.EventEmitter,e.eventie)}(this,function(e,t,n){function i(e,t){for(var n in t)e[n]=t[n];return e}function r(e){return"[object Array]"===d.call(e)}function o(e){var t=[];if(r(e))t=e;else if("number"==typeof e.length)for(var n=0,i=e.length;i>n;n++)t.push(e[n]);else t.push(e);return t}function s(e,t,n){if(!(this instanceof s))return new s(e,t);"string"==typeof e&&(e=document.querySelectorAll(e)),this.elements=o(e),this.options=i({},this.options),"function"==typeof t?n=t:i(this.options,t),n&&this.on("always",n),this.getImages(),a&&(this.jqDeferred=new a.Deferred);var r=this;setTimeout(function(){r.check()})}function c(e){this.img=e}function f(e){this.src=e,v[e]=this}var a=e.jQuery,u=e.console,h=u!==void 0,d=Object.prototype.toString;s.prototype=new t,s.prototype.options={},s.prototype.getImages=function(){this.images=[];for(var e=0,t=this.elements.length;t>e;e++){var n=this.elements[e];"IMG"===n.nodeName&&this.addImage(n);for(var i=n.querySelectorAll("img"),r=0,o=i.length;o>r;r++){var s=i[r];this.addImage(s)}}},s.prototype.addImage=function(e){var t=new c(e);this.images.push(t)},s.prototype.check=function(){function e(e,r){return t.options.debug&&h&&u.log("confirm",e,r),t.progress(e),n++,n===i&&t.complete(),!0}var t=this,n=0,i=this.images.length;if(this.hasAnyBroken=!1,!i)return this.complete(),void 0;for(var r=0;i>r;r++){var o=this.images[r];o.on("confirm",e),o.check()}},s.prototype.progress=function(e){this.hasAnyBroken=this.hasAnyBroken||!e.isLoaded;var t=this;setTimeout(function(){t.emit("progress",t,e),t.jqDeferred&&t.jqDeferred.notify&&t.jqDeferred.notify(t,e)})},s.prototype.complete=function(){var e=this.hasAnyBroken?"fail":"done";this.isComplete=!0;var t=this;setTimeout(function(){if(t.emit(e,t),t.emit("always",t),t.jqDeferred){var n=t.hasAnyBroken?"reject":"resolve";t.jqDeferred[n](t)}})},a&&(a.fn.imagesLoaded=function(e,t){var n=new s(this,e,t);return n.jqDeferred.promise(a(this))}),c.prototype=new t,c.prototype.check=function(){var e=v[this.img.src]||new f(this.img.src);if(e.isConfirmed)return this.confirm(e.isLoaded,"cached was confirmed"),void 0;if(this.img.complete&&void 0!==this.img.naturalWidth)return this.confirm(0!==this.img.naturalWidth,"naturalWidth"),void 0;var t=this;e.on("confirm",function(e,n){return t.confirm(e.isLoaded,n),!0}),e.check()},c.prototype.confirm=function(e,t){this.isLoaded=e,this.emit("confirm",this,t)};var v={};return f.prototype=new t,f.prototype.check=function(){if(!this.isChecked){var e=new Image;n.bind(e,"load",this),n.bind(e,"error",this),e.src=this.src,this.isChecked=!0}},f.prototype.handleEvent=function(e){var t="on"+e.type;this[t]&&this[t](e)},f.prototype.onload=function(e){this.confirm(!0,"onload"),this.unbindProxyEvents(e)},f.prototype.onerror=function(e){this.confirm(!1,"onerror"),this.unbindProxyEvents(e)},f.prototype.confirm=function(e,t){this.isConfirmed=!0,this.isLoaded=e,this.emit("confirm",this,t)},f.prototype.unbindProxyEvents=function(e){n.unbind(e.target,"load",this),n.unbind(e.target,"error",this)},s});

/*
 * Collapsible Callouts
 */
;(function ($, window, document) {
    $.fn.mtMobileCallouts = function(options) {
        // Default Settings
        var defaults = {
            // Collapsible Header Insertion
            insertBefore: ".reg-callout", // the parent container to insert mobile header before
            // Collapsible Header Wrapper Opts
            wrapperElem: "div",
            wrapperAttr: {
                cssClass: "mobile-callout-header"
            }, // the class to assign the wrapper
            activeClass: "open", // the class that will be assigned to the header when the callout is open
            calloutHeader: "h3", // the header element of the toggle that contains the header text 
            // Misc Opts
            toggleSpeed: 400, // the speed of the collapsible menu toggle animation
            // Callbacks
            onCalloutOpen: function() {}, // Fired after callout is opened
            onCalloutClose: function() {} // Fired after callout is closed
        };

        // Apply passed options and clone nav
        var opts = $.extend( {}, defaults, options),
            $callouts = $(this);
        
        if ($callouts.length > 0) {
            // Iterate over callouts and insert mobile headers
            $callouts.each( function(index, element){
                $(this).find(opts.insertBefore).before('<' + opts.wrapperElem + ' class="' + opts.wrapperAttr.cssClass + '">' + $(this).find(opts.calloutHeader).text() + '</' + opts.wrapperElem + '>');
            });
            // Attach header click event
            $(opts.wrapperElem + "." + opts.wrapperAttr.cssClass).on("click", function (e) {
                e.preventDefault();
                // Get current state
                var state = $(this).hasClass(opts.activeClass) ? "close" : "open";
                // Change state and fire appropriate callback
                $(this).toggleClass(opts.activeClass).next().slideToggle(opts.toggleSpeed, function (e){
                    if (state === "open") opts.onCalloutOpen.call(this);
                    else opts.onCalloutClose.call(this);
                });
                return false;
            });
        }
    };
})(jQuery, window, document);

/*
 * Gallery Plugin
 */
;(function ($, window, document) {
    $.fn.mtGallery = function(options) {
        // Default Settings
        var defaults = {
            // CSS Namespace
            namespace: "mt-",
            // Gallery Stage Settings
            stageWrap: ".stage",
            imageWrap: ".image-wrap",
            imageTitle: ".image-title",
            imageDesc: ".image-desc",
            animation: "fade", // fade, slide
            preload: 5,
            staticWidth: true,
            staticHeight: true,
            animSpeed: 400,
            attachDetails: true,
            // Gallery Navigation Settings
            navWrap: ".thumbs",
            navElem: "img",
            activeClass: "active",
            navType: "flat", // carousel, flat, custom
            // Navigation Control Options
            addControls: true,
            useControls: true,
            nextButton: "next",
            prevButton: "prev",
            controlsAppend: false,
            // Gallery Behaviors


            // Callbacks
            onInit: function() {},
            onThumbClick: function() {},
            onNextClick: function() {},
            onPrevClick: function() {}
        };

        // Setiup gallery var and return false if it is not on the page
        var $gallery = $(this);
        if ($gallery.length == 0) return false;

        // Apply passed options, get hash, setup vars 
        var opts = $.extend( {}, defaults, options),
            deeplink = _getHash(),
            startSlide;

        // Load applicable elements
        var elements = {},
            current;

        // Initialize gallery
        init();

        // Functions
        function init() {
            // Initialize the Navigation
            elements.nav = _initNavigation();
            // Initialize the Gallery
            elements.gallery = _initGallery(elements.nav);
            // Setup Stage
            _updateStage(startSlide);
        }

        // 
        function _getHash() {
            if (window.location.hash) {
                var hash = (window.location.hash).replace("#","");
                if (!isNaN(parseFloat(hash)) && isFinite(hash)) return Math.floor(Math.abs(hash));
                else return hash;
            } else return false;
        }

        // Set the startSlide variable from the URL hash
        function _setStartSlide(nav) {
            // Set startSlide value from deeplink hash (or 0 by default)
            if (deeplink) {
                switch (typeof deeplink) {
                    case "number": if (nav.items.eq(deeplink).length > 0) startSlide = deeplink; break;
                    case "string":
                        if (nav.wrapper.find("[data-id='" + deeplink + "']") && nav.wrapper.find("[data-id='" + deeplink + "']").index() >= 0) {
                            startSlide = nav.wrapper.find("[data-id='" + deeplink + "']").index();
                        }
                        break;
                    default: startSlide = 0;
                }
            } else startSlide = 0;
            return startSlide;
        }

        function _initGallery(nav) {
            var stage = {};
            stage.wrapper = $gallery.find(opts.stageWrap);
            stage.imgWrap = stage.wrapper.find(opts.imageWrap);
            // Set image total
            stage.wrapper.find("span.total").text(nav.total);
            // Image Positioning
            var wCSS;
            if (opts.animation === "fade") wCSS = "position:absolute;top:0;left:0;";
            else if (opts.animation === "slide") wCSS = "position:absolute;top:0;";
            // Add image elements
            elements.nav.items.each( function(index, element) {
                if ((startSlide === 0 && index < opts.preload) || index === startSlide || (index > (startSlide-opts.preload) && index < (startSlide+opts.preload))) {
                    stage.imgWrap.append('<div class="' + opts.namespace + 'item-wrap" style="' + (index != startSlide ? "display:none;" : "") + wCSS + '"><div><img src="' + ($(this).data("src") ? $(this).data("src") : (element.src !== undefined ? element.src : "")) + '"></div></div>');
                    _imageSize(stage.imgWrap.children().eq(index));
                } else stage.imgWrap.append('<div class="' + opts.namespace + 'item-wrap" style="display:none;' + wCSS + '"><div><img></div></div>');
            });
            // Load images into an object
            stage.items = stage.imgWrap.find("div." + opts.namespace + "item-wrap");
            // Add active class to start slide
            stage.items.eq(startSlide).addClass(opts.activeClass);
            // Setup Image Wrap
            stage.imgWrap.css({'position':'relative'});
            // Return stage objects
            return stage;
        }

        // Initialize the gallery thumbnails
        function _initNavigation() {
            var nav = {};
            nav.wrapper = $gallery.find(opts.navWrap);
            nav.items = nav.wrapper.find(opts.navElem);
            // Setup Nav
            switch (opts.navType) {
                case "carousel":
                    nav.items.css({ "float": "left" }).wrapAll('<div class="' + opts.namespace + 'carousel-inner"></div>');
                    nav.wrapper.find("." + opts.namespace + "carousel-inner").css({ "position": "relative", "width": (nav.items.outerWidth(true) * nav.items.length) }).wrap('<div class="' + opts.namespace + 'carousel-outer"></div>').find("." + opts.namespace + "carousel-outer").css({ "overflow": "hidden" });
                    nav.move = {
                        el: $("." + opts.namespace + "carousel-inner"),
                        dist: nav.items.outerWidth(true)
                    };
                    break;
                case "flat":
                    nav.items.css({ "float": "left" });
                    break;
            }
            // Add controls
            if (opts.addControls !== false && opts.useControls === true) {
                var attachTo =  (typeof opts.controlsAppend == "string" && $(opts.controlsAppend).length > 0) ? $(opts.controlsAppend) : nav.wrapper;
                attachTo.append(
                    $("<div></div>", { 'class': opts.namespace + "gallery-paging" }).append(
                        $("<a>", { 'class': opts.namespace + opts.prevButton + " disabled", text: "Previous" }),
                        $("<a>", { 'class': opts.namespace + opts.nextButton + (nav.items.length < 2 ? " disabled" : ""), text: "Next" })
                    )
                );
            }
            // Bind control actions
            if (opts.useControls === true) {
                // Bind Previous Button Action
                $("." + opts.namespace + opts.prevButton).on("click", gotoPrevious);
                // Bind Next Button Action
                $("." + opts.namespace + opts.nextButton).on("click", gotoNext);
            }
            nav.total = nav.items.length;
            nav.items.on("click", gotoItem);
            // Set appropriate thumbnail class to active on init
            nav.items.eq(_setStartSlide(nav)).addClass(opts.activeClass);
            // Return nav objects
            return nav;
        }

        

        // Move to a specified gallery item
        function gotoItem() {
            var item = _gotoSupport($(this).index());
			//var item = _gotoSupport($(this).index('.thumbs > img'));
            if (item.cIndex !== item.nIndex) {
                if (opts.navType === "carousel") {
                    var move = {};
                    move.direction = item.cIndex < item.nIndex ? "-=" : "+=";
                    move.distance = (Math.abs(item.cIndex - item.nIndex))*elements.nav.move.dist;
                    // Move Carousel
                    elements.nav.move.el.animate({ "left": move.direction + move.distance + "px" });
                }
                // Update Navigation Item Classes
                _navClassUpdate(item.cEl, item.nEl);
                // Update Stage
                _updateStage(item.nIndex);
            }
            // Next Event Callback
            opts.onThumbClick.call(this);
            return false;
        }

        // Move to the previous gallery item
        function gotoPrevious() {
            var item = _gotoSupport("prev");
            // Move to Next Gallery Image
            if (item.cIndex > 0) {
                // Update Navigatiom
                if (opts.navType === "carousel") elements.nav.move.el.animate({ "left": "+=" + elements.nav.move.dist + "px" });
                // Update Navigation Item Classes
                console.log("remove: " + item.cEl + " add: " + item.nEl);
                _navClassUpdate(item.cEl, item.nEl);
                // Update Stage
                _updateStage(item.nIndex);
            }
            // Prev Event Callback
            opts.onPrevClick.call(this);
            return false;
        }
        
        // Move to the next gallery item
        function gotoNext() {
            var item = _gotoSupport("next");
            // Move to Next Gallery Image
            if (item.cIndex < (elements.nav.items.length-1)) {
                // Update Navigatiom
                if (opts.navType === "carousel") elements.nav.move.el.animate({ "left": "-=" + elements.nav.move.dist + "px" });
                // Update Navigation Item Classes
                _navClassUpdate(item.cEl, item.nEl);
                // Update Stage
                _updateStage(item.nIndex);
            }
            // Next Event Callback
            opts.onNextClick.call(this);
            return false;
        }

        // Return Indexes and Elements for current and next gallery items
        function _gotoSupport(item) {
            var data = {};
            data.cIndex = elements.nav.items.index($(opts.navWrap + " > ." + opts.activeClass));
            data.cEl = elements.nav.items.eq(data.cIndex);
            data.nIndex = item === "prev" ? data.cIndex - 1 : (item === "next" ? data.cIndex + 1 : item);
            data.nEl = elements.nav.items.eq(data.nIndex);
            _getImageSrc(data.nIndex);
            return data;
        }

        // Update the active class on the navigation items
        function _navClassUpdate(currentEl, nextEl) {
            nextEl.addClass(opts.activeClass);
			//nextEl.next('.module-photo-item').find('img').addClass(opts.activeClass);
            currentEl.removeClass(opts.activeClass);
            //currentEl.next('.module-photo-item').find('img').removeClass(opts.activeClass);
        }

        function _updateStage(index) {
            // If current/next are the same, exit
            if (current === index) return false;
            // Start image preload
            _preLoad(index);
            // Set variables
            var $item = elements.nav.items.eq(index),
                $current = elements.gallery.items.eq(current),
                $next = elements.gallery.items.eq(index),
                title = $item.data("title") !== undefined ? $item.data("title") : "",
                description = $item.data("description") !== undefined ? $item.data("description") : "";
            // Size Image
            _imageSize($next);
            // Update title, description, and image count
            if (opts.attachDetails === true) $next.find("img").parent().css({ "position": "relative" }).append($(".image-overlay"));
            $(opts.imageTitle).html(title);
            $(opts.imageDesc).html(description);
            $(opts.stageWrap).find("span.current").text(index+1);
            // Animate Slides
            switch (opts.animation) {
                case "fade":
                    $next.fadeIn(opts.animSpeed).addClass(opts.activeClass);
                    $current.fadeOut(opts.animSpeed).removeClass(opts.activeClass);
                    break;
                case "push":
                    if (index < elements.nav.items.index("." + opts.activeClass)) {
                        var nCSS = { "left": "100%", "right": "auto", "display": "block" },
                            nAnim = { "left": "0", "right": "auto", "display": "block" },
                            cAnim = { "right": "100%", "left": "auto", "display": "none" };
                    } else {
                        var nCSS = { "right": "100%", "right": "auto", "display": "block" },
                            nAnim = { "right": "0", "right": "auto", "display": "block" },
                            cAnim = { "left": "100%", "left": "auto", "display": "none" };
                    }
                    $next.css(nCSS).animate(nAnim).addClass(opts.activeClass);
                    $current.animate(cAnim).removeClass(opts.activeClass);
                    break;
            }
            //if (opts.staticWidth && opts.staticHeight) $gallery.find(".image-overlay").css({ "width": size.width + "px" });
            current = index;
        }

        function _scaleImage(img,orig) {
            var size = {};
            // Set new image size
            switch (true) {
                case opts.staticWidth && opts.staticHeight:
                    var wS = elements.gallery.imgWrap.width()/orig.width,
                        hS = elements.gallery.imgWrap.height()/orig.height,
                        ratio = wS < hS ? wS : hS;
                    size.width = Math.round(orig.width*ratio);
                    size.height = Math.round(orig.height*ratio);
                    img.css({ "width": size.width + "px", "height": size.height + "px" }).parent().css({ "width": size.width + "px", "height": size.height + "px" });
                    break;
                case opts.staticWidth:
                    size.width = elements.gallery.imgWrap.width();
                    size.height = Math.round(orig.ratio*size.width);
                    img.css({ "width": size.width + "px", "height": size.height + "px" }).parent().css({ "height": size.height + "px" });
                    break;
                case opts.staticHeight:
                    var ratio = orig.width/orig.height;
                    size.height = elements.gallery.imgWrap.height();
                    size.width = Math.round(ratio*size.height);
                    img.css({ "width": size.width + "px", "height": size.height + "px" }).parent().css({ "width": size.width + "px" });
                    break;
                default:
                    size.width = orig.width;
                    size.height = orig.height;
                    img.css({ "width": size.width + "px", "height": size.height + "px" });
            }
            return size;
        }

        function _imageSize(el) {
            var orig = {},
                $img = el.find("img"),
                test = new Image();
            // Get Image Size and Determine Ratio
            switch (true) {
                case (el.data("processed") == true && el.data("ratio") > 0):
                    orig.width = el.data("width");
                    orig.height = el.data("height");
                    orig.ratio = el.data("ratio");
                    return _scaleImage($img,orig);
                default:
                    test.src = $img.attr("src");
                    imagesLoaded( test, function() {
                        orig.width = test.width;
                        orig.height = test.height;
                        orig.ratio = orig.height/orig.width;
                        el.attr({ "data-ratio": orig.ratio, "data-width": orig.width, "data-height": orig.height, "data-processed": true });
                        return _scaleImage($img,orig);
                    });
            }
        }

        function _getImageSrc(index) {
            return elements.nav.items.eq(index).data("src") !== undefined ? elements.nav.items.eq(index).data("src") : elements.nav.items.eq(index).attr("src");
        }
        
        function _preLoad(index,init) {
           // Check if image source is loaded for selected image, if not load
            if (elements.gallery.items.eq(index).find("img").attr("src") === undefined) {
                elements.gallery.items.eq(index).find("img").attr("src", _getImageSrc(index));
                _imageSize(elements.gallery.items.eq(index));
            }
            // Determine appropriate starting image index for pre-loading
            var plStart;
            switch (true) {
                case index === 0: plStart = 0; break;
                case (index-(Math.ceil(opts.preload/2))) < 0: plStart = 0; break;
                default: plStart = (index-(Math.ceil(opts.preload/2)));
            }
            var i = plStart,
                l = i + opts.preload;
            // Check if image is loaded, if not load it
            for (; i<l; i++) {
                //console.log("Executing Preload for Index: " + i + "/" + l);
                if (elements.gallery.items.eq(i).find("img").attr("src") === undefined) {
                    elements.gallery.items.eq(i).find("img").attr("src", _getImageSrc(i));
                    _imageSize(elements.gallery.items.eq(i));
                } else l++;
            }
        }

    };
})(jQuery, window, document);

/*
 * Toggle Functions
 * Will setup toggles on all passed selectors per options passed. This function relies on the selector 
 * tag passed to have an attribute of data-toggle naming the selector to toggle.
 */
;(function ($, window, document) {
    $.fn.attachToggle = function(options) {
        var defaults = {
            activeClass: 'active',
            animation: 'toggle', // slideToggle, toggle or custom
            animationOpts: 200,  // animation speed as integer for toggle and slideToggle, or property object for animate
            attachEvent: 'click', // any valid event (i.e. click, hover, focus)
            customFunction: function() {}
        };

        var opts = $.extend( {}, defaults, options),
            $this = $(this);

        if (opts.animateEl !== null) {
            return $this.each( function(index, element) {
                var $t = $(this);
                switch ($t.data("animation") ? $t.data("animation") : opts.animation) {
                    case "custom":
                        $(this).on(opts.attachEvent, opts.customFunction);
                        break;
                    default:
                        $(this).on(opts.attachEvent, function(e) {
                            e.preventDefault;
                            $t.toggleClass(opts.activeClass);
                            switch ($t.data("animation") ? $t.data("animation") : opts.animation) {
                                case "slideToggle":
                                    $($t.data("toggle")).slideToggle($t.data("speed") ? $t.data("speed") : opts.animateOpts);
                                    break;
                                default:
                                    $($t.data("toggle")).toggle($t.data("speed") ? $t.data("speed") : opts.animateOpts);
                            }
                            return false;
                        });
                }
            });
        }
    }
})(jQuery, window, document);

/*
 * List Alphabetizer
 * Works on most elements containing <li>'s
 *
 * Usage: $('ul.selector, ol.selector').sortList();
 *      Optionally pass the arguemnt 'true' to sort in reverse order
 */
;(function ($, window, document) {
    $.fn.sortList = function(sortDescending) {
        var $lists = $(this);
        // Only proceed if there is a list
        if ($lists.length > 0) {
            return $lists.each( function(index, element) {
                var $list = $(this);
                // Get the list items and setup an array for sorting
                var $lis = $list.find("li"),
                    vals = [];

                // Populate the array
                $lis.each( function(index, element) {
                    var $this = $(this);
                    vals.push({ sort: $this.text(), html: $this });
                });

                // Sort it
                vals.sort( function(a,b) {
                    if (a.sort > b.sort) return 1;
                    if (a.sort < b.sort) return -1;
                    // a must be equal to b
                    return 0;
                });

                // Sometimes you gotta DESC
                if(sortDescending) vals.reverse();

                // Empty existing data from UL
                $list.html("");

                // Change the list on the page
                for (var i=0,l=vals.length; i<l; i++) {
                    vals[i].html.appendTo($list);
                }
            });
        } else return false;
    }
})(jQuery, window, document);

/*
 * MedTouch Slider
 */

(function (e, window, document) {
    e.mtslider = function (t, n) {
        var r = e(t);
        r.vars = e.extend({}, e.mtslider.defaults, n);
        var i = r.vars.namespace,
            s = window.navigator && window.navigator.msPointerEnabled && window.MSGesture,
            o = ("ontouchstart" in window || s || window.DocumentTouch && document instanceof DocumentTouch) && r.vars.touch,
            u = "click touchend MSPointerUp",
            a = "",
            f, l = r.vars.direction === "vertical",
            c = r.vars.reverse,
            h = r.vars.itemWidth > 0,
            p = r.vars.animation === "fade",
            d = r.vars.asNavFor !== "",
            v = {}, m = !0;
        e.data(t, "mtslider", r);
        v = {
            init: function () {
                r.animating = !1;
                r.currentSlide = parseInt(r.vars.startAt ? r.vars.startAt : 0);
                isNaN(r.currentSlide) && (r.currentSlide = 0);
                r.animatingTo = r.currentSlide;
                r.atEnd = r.currentSlide === 0 || r.currentSlide === r.last;
                r.containerSelector = r.vars.selector.substr(0, r.vars.selector.search(" "));
                r.slides = e(r.vars.selector, r);
                r.container = e(r.containerSelector, r);
                r.count = r.slides.length;
                r.syncExists = e(r.vars.sync).length > 0;
                r.vars.animation === "slide" && (r.vars.animation = "swing");
                r.prop = l ? "top" : "marginLeft";
                r.args = {};
                r.manualPause = !1;
                r.stopped = !1;
                r.started = !1;
                r.startTimeout = null;
                r.transitions = !r.vars.video && !p && r.vars.useCSS && function () {
                    var e = document.createElement("div"),
                        t = ["perspectiveProperty", "WebkitPerspective", "MozPerspective", "OPerspective", "msPerspective"];
                    for (var n in t)
                        if (e.style[t[n]] !== undefined) {
                            r.pfx = t[n].replace("Perspective", "").toLowerCase();
                            r.prop = "-" + r.pfx + "-transform";
                            return !0
                        }
                    return !1
                }();
                r.vars.controlsContainer !== "" && (r.controlsContainer = e(r.vars.controlsContainer).length > 0 && e(r.vars.controlsContainer));
                r.vars.manualControls !== "" && (r.manualControls = e(r.vars.manualControls).length > 0 && e(r.vars.manualControls));
                if (r.vars.randomize) {
                    r.slides.sort(function () {
                        return Math.round(Math.random()) - .5
                    });
                    r.container.empty().append(r.slides)
                }
                r.doMath();
                r.setup("init");
                r.vars.controlNav && v.controlNav.setup();
                r.vars.directionNav && v.directionNav.setup();
                r.vars.keyboard && (e(r.containerSelector).length === 1 || r.vars.multipleKeyboard) && e(document).bind("keyup", function (e) {
                    var t = e.keyCode;
                    if (!r.animating && (t === 39 || t === 37)) {
                        var n = t === 39 ? r.getTarget("next") : t === 37 ? r.getTarget("prev") : !1;
                        r.mtAnimate(n, r.vars.pauseOnAction)
                    }
                });
                r.vars.mousewheel && r.bind("mousewheel", function (e, t, n, i) {
                    e.preventDefault();
                    var s = t < 0 ? r.getTarget("next") : r.getTarget("prev");
                    r.mtAnimate(s, r.vars.pauseOnAction)
                });
                r.vars.pausePlay && v.pausePlay.setup();
                r.vars.slideshow && r.vars.pauseInvisible && v.pauseInvisible.init();
                if (r.vars.slideshow) {
                    r.vars.pauseOnHover && r.hover(function () {
                        !r.manualPlay && !r.manualPause && r.pause()
                    }, function () {
                        !r.manualPause && !r.manualPlay && !r.stopped && r.play()
                    });
                    if (!r.vars.pauseInvisible || !v.pauseInvisible.isHidden()) r.vars.initDelay > 0 ? r.startTimeout = setTimeout(r.play, r.vars.initDelay) : r.play()
                }
                d && v.asNav.setup();
                o && r.vars.touch && v.touch();
                (!p || p && r.vars.smoothHeight) && e(window).bind("resize orientationchange focus", v.resize);
                r.find("img").attr("draggable", "false");
                setTimeout(function () {
                    r.vars.start(r)
                }, 200)
            },
            asNav: {
                setup: function () {
                    r.asNav = !0;
                    r.animatingTo = Math.floor(r.currentSlide / r.move);
                    r.currentItem = r.currentSlide;
                    r.slides.removeClass(i + "active-slide").eq(r.currentItem).addClass(i + "active-slide");
                    if (!s) r.slides.click(function (t) {
                        t.preventDefault();
                        var n = e(this),
                            s = n.index(),
                            o = n.offset().left - e(r).scrollLeft();
                        if (o <= 0 && n.hasClass(i + "active-slide")) r.mtAnimate(r.getTarget("prev"), !0);
                        else if (!e(r.vars.asNavFor).data("mtslider").animating && !n.hasClass(i + "active-slide")) {
                            r.direction = r.currentItem < s ? "next" : "prev";
                            r.mtAnimate(s, r.vars.pauseOnAction, !1, !0, !0)
                        }
                    });
                    else {
                        t._slider = r;
                        r.slides.each(function () {
                            var t = this;
                            t._gesture = new MSGesture;
                            t._gesture.target = t;
                            t.addEventListener("MSPointerDown", function (e) {
                                e.preventDefault();
                                e.currentTarget._gesture && e.currentTarget._gesture.addPointer(e.pointerId)
                            }, !1);
                            t.addEventListener("MSGestureTap", function (t) {
                                t.preventDefault();
                                var n = e(this),
                                    i = n.index();
                                if (!e(r.vars.asNavFor).data("mtslider").animating && !n.hasClass("active")) {
                                    r.direction = r.currentItem < i ? "next" : "prev";
                                    r.mtAnimate(i, r.vars.pauseOnAction, !1, !0, !0)
                                }
                            })
                        })
                    }
                }
            },
            controlNav: {
                setup: function () {
                    r.manualControls ? v.controlNav.setupManual() : v.controlNav.setupPaging()
                },
                setupPaging: function () {
                    var t = r.vars.controlNav === "thumbnails" ? "control-thumbs" : "control-paging",
                        n = 1,
                        s, o;
                    r.controlNavScaffold = e('<ol class="' + i + "control-nav " + i + t + '"></ol>');
                    if (r.pagingCount > 1)
                        for (var f = 0; f < r.pagingCount; f++) {
                            o = r.slides.eq(f);
                            s = r.vars.controlNav === "thumbnails" ? '<img src="' + o.attr("data-thumb") + '"/>' : "<a>" + n + "</a>";
                            if ("thumbnails" === r.vars.controlNav && !0 === r.vars.thumbCaptions) {
                                var l = o.attr("data-thumbcaption");
                                "" != l && undefined != l && (s += '<span class="' + i + 'caption">' + l + "</span>")
                            }
                            r.controlNavScaffold.append("<li>" + s + "</li>");
                            n++
                        }
                    r.controlsContainer ? e(r.controlsContainer).append(r.controlNavScaffold) : r.append(r.controlNavScaffold);
                    v.controlNav.set();
                    v.controlNav.active();
                    r.controlNavScaffold.delegate("a, img", u, function (t) {
                        t.preventDefault();
                        if (a === "" || a === t.type) {
                            var n = e(this),
                                s = r.controlNav.index(n);
                            if (!n.hasClass(i + "active")) {
                                r.direction = s > r.currentSlide ? "next" : "prev";
                                r.mtAnimate(s, r.vars.pauseOnAction)
                            }
                        }
                        a === "" && (a = t.type);
                        v.setToClearWatchedEvent()
                    })
                },
                setupManual: function () {
                    r.controlNav = r.manualControls;
                    v.controlNav.active();
                    r.controlNav.bind(u, function (t) {
                        t.preventDefault();
                        if (a === "" || a === t.type) {
                            var n = e(this),
                                s = r.controlNav.index(n);
                            if (!n.hasClass(i + "active")) {
                                s > r.currentSlide ? r.direction = "next" : r.direction = "prev";
                                r.mtAnimate(s, r.vars.pauseOnAction)
                            }
                        }
                        a === "" && (a = t.type);
                        v.setToClearWatchedEvent()
                    })
                },
                set: function () {
                    var t = r.vars.controlNav === "thumbnails" ? "img" : "a";
                    r.controlNav = e("." + i + "control-nav li " + t, r.controlsContainer ? r.controlsContainer : r)
                },
                active: function () {
                    r.controlNav.removeClass(i + "active").eq(r.animatingTo).addClass(i + "active")
                },
                update: function (t, n) {
                    r.pagingCount > 1 && t === "add" ? r.controlNavScaffold.append(e("<li><a>" + r.count + "</a></li>")) : r.pagingCount === 1 ? r.controlNavScaffold.find("li").remove() : r.controlNav.eq(n).closest("li").remove();
                    v.controlNav.set();
                    r.pagingCount > 1 && r.pagingCount !== r.controlNav.length ? r.update(n, t) : v.controlNav.active()
                }
            },
            directionNav: {
                setup: function () {
                    var t = e('<ul class="' + i + 'direction-nav"><li><a class="' + i + 'prev" href="#">' + r.vars.prevText + '</a></li><li><a class="' + i + 'next" href="#">' + r.vars.nextText + "</a></li></ul>");
                    if (r.controlsContainer) {
                        e(r.controlsContainer).append(t);
                        r.directionNav = e("." + i + "direction-nav li a", r.controlsContainer)
                    } else {
                        r.append(t);
                        r.directionNav = e("." + i + "direction-nav li a", r)
                    }
                    v.directionNav.update();
                    r.directionNav.bind(u, function (t) {
                        t.preventDefault();
                        var n;
                        if (a === "" || a === t.type) {
                            n = e(this).hasClass(i + "next") ? r.getTarget("next") : r.getTarget("prev");
                            r.mtAnimate(n, r.vars.pauseOnAction)
                        }
                        a === "" && (a = t.type);
                        v.setToClearWatchedEvent()
                    })
                },
                update: function () {
                    var e = i + "disabled";
                    r.pagingCount === 1 ? r.directionNav.addClass(e).attr("tabindex", "-1") : r.vars.animationLoop ? r.directionNav.removeClass(e).removeAttr("tabindex") : r.animatingTo === 0 ? r.directionNav.removeClass(e).filter("." + i + "prev").addClass(e).attr("tabindex", "-1") : r.animatingTo === r.last ? r.directionNav.removeClass(e).filter("." + i + "next").addClass(e).attr("tabindex", "-1") : r.directionNav.removeClass(e).removeAttr("tabindex")
                }
            },
            pausePlay: {
                setup: function () {
                    var t = e('<div class="' + i + 'pauseplay"><a></a></div>');
                    if (r.controlsContainer) {
                        r.controlsContainer.append(t);
                        r.pausePlay = e("." + i + "pauseplay a", r.controlsContainer)
                    } else {
                        r.append(t);
                        r.pausePlay = e("." + i + "pauseplay a", r)
                    }
                    v.pausePlay.update(r.vars.slideshow ? i + "pause" : i + "play");
                    r.pausePlay.bind(u, function (t) {
                        t.preventDefault();
                        if (a === "" || a === t.type)
                            if (e(this).hasClass(i + "pause")) {
                                r.manualPause = !0;
                                r.manualPlay = !1;
                                r.pause()
                            } else {
                                r.manualPause = !1;
                                r.manualPlay = !0;
                                r.play()
                            }
                        a === "" && (a = t.type);
                        v.setToClearWatchedEvent()
                    })
                },
                update: function (e) {
                    e === "play" ? r.pausePlay.removeClass(i + "pause").addClass(i + "play").html(r.vars.playText) : r.pausePlay.removeClass(i + "play").addClass(i + "pause").html(r.vars.pauseText)
                }
            },
            touch: function () {
                var e, n, i, o, u, a, f = !1,
                    d = 0,
                    v = 0,
                    m = 0;
                if (!s) {
                    t.addEventListener("touchstart", g, !1);

                    function g(s) {
                        if (r.animating) s.preventDefault();
                        else if (window.navigator.msPointerEnabled || s.touches.length === 1) {
                            r.pause();
                            o = l ? r.h : r.w;
                            a = Number(new Date);
                            d = s.touches[0].pageX;
                            v = s.touches[0].pageY;
                            i = h && c && r.animatingTo === r.last ? 0 : h && c ? r.limit - (r.itemW + r.vars.itemMargin) * r.move * r.animatingTo : h && r.currentSlide === r.last ? r.limit : h ? (r.itemW + r.vars.itemMargin) * r.move * r.currentSlide : c ? (r.last - r.currentSlide + r.cloneOffset) * o : (r.currentSlide + r.cloneOffset) * o;
                            e = l ? v : d;
                            n = l ? d : v;
                            t.addEventListener("touchmove", y, !1);
                            t.addEventListener("touchend", b, !1)
                        }
                    }

                    function y(t) {
                        d = t.touches[0].pageX;
                        v = t.touches[0].pageY;
                        u = l ? e - v : e - d;
                        f = l ? Math.abs(u) < Math.abs(d - n) : Math.abs(u) < Math.abs(v - n);
                        var s = 500;
                        if (!f || Number(new Date) - a > s) {
                            t.preventDefault();
                            if (!p && r.transitions) {
                                r.vars.animationLoop || (u /= r.currentSlide === 0 && u < 0 || r.currentSlide === r.last && u > 0 ? Math.abs(u) / o + 2 : 1);
                                r.setProps(i + u, "setTouch")
                            }
                        }
                    }

                    function b(s) {
                        t.removeEventListener("touchmove", y, !1);
                        if (r.animatingTo === r.currentSlide && !f && u !== null) {
                            var l = c ? -u : u,
                                h = l > 0 ? r.getTarget("next") : r.getTarget("prev");
                            r.canAdvance(h) && (Number(new Date) - a < 550 && Math.abs(l) > 50 || Math.abs(l) > o / 2) ? r.mtAnimate(h, r.vars.pauseOnAction) : p || r.mtAnimate(r.currentSlide, r.vars.pauseOnAction, !0)
                        }
                        t.removeEventListener("touchend", b, !1);
                        e = null;
                        n = null;
                        u = null;
                        i = null
                    }
                } else {
                    t.style.msTouchAction = "none";
                    t._gesture = new MSGesture;
                    t._gesture.target = t;
                    t.addEventListener("MSPointerDown", w, !1);
                    t._slider = r;
                    t.addEventListener("MSGestureChange", E, !1);
                    t.addEventListener("MSGestureEnd", S, !1);

                    function w(e) {
                        e.stopPropagation();
                        if (r.animating) e.preventDefault();
                        else {
                            r.pause();
                            t._gesture.addPointer(e.pointerId);
                            m = 0;
                            o = l ? r.h : r.w;
                            a = Number(new Date);
                            i = h && c && r.animatingTo === r.last ? 0 : h && c ? r.limit - (r.itemW + r.vars.itemMargin) * r.move * r.animatingTo : h && r.currentSlide === r.last ? r.limit : h ? (r.itemW + r.vars.itemMargin) * r.move * r.currentSlide : c ? (r.last - r.currentSlide + r.cloneOffset) * o : (r.currentSlide + r.cloneOffset) * o
                        }
                    }

                    function E(e) {
                        e.stopPropagation();
                        var n = e.target._slider;
                        if (!n) return;
                        var r = -e.translationX,
                            s = -e.translationY;
                        m += l ? s : r;
                        u = m;
                        f = l ? Math.abs(m) < Math.abs(-r) : Math.abs(m) < Math.abs(-s);
                        if (e.detail === e.MSGESTURE_FLAG_INERTIA) {
                            setImmediate(function () {
                                t._gesture.stop()
                            });
                            return
                        }
                        if (!f || Number(new Date) - a > 500) {
                            e.preventDefault();
                            if (!p && n.transitions) {
                                n.vars.animationLoop || (u = m / (n.currentSlide === 0 && m < 0 || n.currentSlide === n.last && m > 0 ? Math.abs(m) / o + 2 : 1));
                                n.setProps(i + u, "setTouch")
                            }
                        }
                    }

                    function S(t) {
                        t.stopPropagation();
                        var r = t.target._slider;
                        if (!r) return;
                        if (r.animatingTo === r.currentSlide && !f && u !== null) {
                            var s = c ? -u : u,
                                l = s > 0 ? r.getTarget("next") : r.getTarget("prev");
                            r.canAdvance(l) && (Number(new Date) - a < 550 && Math.abs(s) > 50 || Math.abs(s) > o / 2) ? r.mtAnimate(l, r.vars.pauseOnAction) : p || r.mtAnimate(r.currentSlide, r.vars.pauseOnAction, !0)
                        }
                        e = null;
                        n = null;
                        u = null;
                        i = null;
                        m = 0
                    }
                }
            },
            resize: function () {
                if (!r.animating && r.is(":visible")) {
                    h || r.doMath();
                    if (p) v.smoothHeight();
                    else if (h) {
                        r.slides.width(r.computedW);
                        r.update(r.pagingCount);
                        r.setProps()
                    } else if (l) {
                        r.viewport.height(r.h);
                        r.setProps(r.h, "setTotal")
                    } else {
                        r.vars.smoothHeight && v.smoothHeight();
                        r.newSlides.width(r.computedW);
                        r.setProps(r.computedW, "setTotal")
                    }
                }
            },
            smoothHeight: function (e) {
                if (!l || p) {
                    var t = p ? r : r.viewport;
                    e ? t.animate({
                        height: r.slides.eq(r.animatingTo).height()
                    }, e) : t.height(r.slides.eq(r.animatingTo).height())
                }
            },
            sync: function (t) {
                var n = e(r.vars.sync).data("mtslider"),
                    i = r.animatingTo;
                switch (t) {
                case "animate":
                    n.mtAnimate(i, r.vars.pauseOnAction, !1, !0);
                    break;
                case "play":
                    !n.playing && !n.asNav && n.play();
                    break;
                case "pause":
                    n.pause()
                }
            },
            pauseInvisible: {
                visProp: null,
                init: function () {
                    var e = ["webkit", "moz", "ms", "o"];
                    if ("hidden" in document) return "hidden";
                    for (var t = 0; t < e.length; t++) e[t] + "Hidden" in document && (v.pauseInvisible.visProp = e[t] + "Hidden");
                    if (v.pauseInvisible.visProp) {
                        var n = v.pauseInvisible.visProp.replace(/[H|h]idden/, "") + "visibilitychange";
                        document.addEventListener(n, function () {
                            v.pauseInvisible.isHidden() ? r.startTimeout ? clearTimeout(r.startTimeout) : r.pause() : r.started ? r.play() : r.vars.initDelay > 0 ? setTimeout(r.play, r.vars.initDelay) : r.play()
                        })
                    }
                },
                isHidden: function () {
                    return document[v.pauseInvisible.visProp] || !1
                }
            },
            setToClearWatchedEvent: function () {
                clearTimeout(f);
                f = setTimeout(function () {
                    a = ""
                }, 3e3)
            }
        };
        r.mtAnimate = function (t, n, s, u, a) {
            !r.vars.animationLoop && t !== r.currentSlide && (r.direction = t > r.currentSlide ? "next" : "prev");
            d && r.pagingCount === 1 && (r.direction = r.currentItem < t ? "next" : "prev");
            if (!r.animating && (r.canAdvance(t, a) || s) && r.is(":visible")) {
                if (d && u) {
                    var f = e(r.vars.asNavFor).data("mtslider");
                    r.atEnd = t === 0 || t === r.count - 1;
                    f.mtAnimate(t, !0, !1, !0, a);
                    r.direction = r.currentItem < t ? "next" : "prev";
                    f.direction = r.direction;
                    if (Math.ceil((t + 1) / r.visible) - 1 === r.currentSlide || t === 0) {
                        r.currentItem = t;
                        r.slides.removeClass(i + "active-slide").eq(t).addClass(i + "active-slide");
                        return !1
                    }
                    r.currentItem = t;
                    r.slides.removeClass(i + "active-slide").eq(t).addClass(i + "active-slide");
                    t = Math.floor(t / r.visible)
                }
                r.animating = !0;
                r.animatingTo = t;
                n && r.pause();
                r.vars.before(r);
                r.syncExists && !a && v.sync("animate");
                r.vars.controlNav && v.controlNav.active();
                h || r.slides.removeClass(i + "active-slide").eq(t).addClass(i + "active-slide");
                r.atEnd = t === 0 || t === r.last;
                r.vars.directionNav && v.directionNav.update();
                if (t === r.last) {
                    r.vars.end(r);
                    r.vars.animationLoop || r.pause()
                }
                if (!p) {
                    var m = l ? r.slides.filter(":first").height() : r.computedW,
                        g, y, b;
                    if (h) {
                        g = r.vars.itemMargin;
                        b = (r.itemW + g) * r.move * r.animatingTo;
                        y = b > r.limit && r.visible !== 1 ? r.limit : b
                    } else r.currentSlide === 0 && t === r.count - 1 && r.vars.animationLoop && r.direction !== "next" ? y = c ? (r.count + r.cloneOffset) * m : 0 : r.currentSlide === r.last && t === 0 && r.vars.animationLoop && r.direction !== "prev" ? y = c ? 0 : (r.count + 1) * m : y = c ? (r.count - 1 - t + r.cloneOffset) * m : (t + r.cloneOffset) * m;
                    r.setProps(y, "", r.vars.animationSpeed);
                    if (r.transitions) {
                        if (!r.vars.animationLoop || !r.atEnd) {
                            r.animating = !1;
                            r.currentSlide = r.animatingTo
                        }
                        r.container.unbind("webkitTransitionEnd transitionend");
                        r.container.bind("webkitTransitionEnd transitionend", function () {
                            r.wrapup(m)
                        })
                    } else r.container.animate(r.args, r.vars.animationSpeed, r.vars.easing, function () {
                        r.wrapup(m)
                    })
                } else if (!o) {
                    r.slides.eq(r.currentSlide).css({
                        zIndex: 1
                    }).animate({
                        opacity: 0
                    }, r.vars.animationSpeed, r.vars.easing);
                    r.slides.eq(t).css({
                        zIndex: 2
                    }).animate({
                        opacity: 1
                    }, r.vars.animationSpeed, r.vars.easing, r.wrapup)
                } else {
                    r.slides.eq(r.currentSlide).css({
                        opacity: 0,
                        zIndex: 1
                    });
                    r.slides.eq(t).css({
                        opacity: 1,
                        zIndex: 2
                    });
                    r.wrapup(m)
                }
                r.vars.smoothHeight && v.smoothHeight(r.vars.animationSpeed)
            }
        };
        r.wrapup = function (e) {
            !p && !h && (r.currentSlide === 0 && r.animatingTo === r.last && r.vars.animationLoop ? r.setProps(e, "jumpEnd") : r.currentSlide === r.last && r.animatingTo === 0 && r.vars.animationLoop && r.setProps(e, "jumpStart"));
            r.animating = !1;
            r.currentSlide = r.animatingTo;
            r.vars.after(r)
        };
        r.animateSlides = function () {
            !r.animating && m && r.mtAnimate(r.getTarget("next"))
        };
        r.pause = function () {
            clearInterval(r.animatedSlides);
            r.animatedSlides = null;
            r.playing = !1;
            r.vars.pausePlay && v.pausePlay.update("play");
            r.syncExists && v.sync("pause")
        };
        r.play = function () {
            r.playing && clearInterval(r.animatedSlides);
            r.animatedSlides = r.animatedSlides || setInterval(r.animateSlides, r.vars.slideshowSpeed);
            r.started = r.playing = !0;
            r.vars.pausePlay && v.pausePlay.update("pause");
            r.syncExists && v.sync("play")
        };
        r.stop = function () {
            r.pause();
            r.stopped = !0
        };
        r.canAdvance = function (e, t) {
            var n = d ? r.pagingCount - 1 : r.last;
            return t ? !0 : d && r.currentItem === r.count - 1 && e === 0 && r.direction === "prev" ? !0 : d && r.currentItem === 0 && e === r.pagingCount - 1 && r.direction !== "next" ? !1 : e === r.currentSlide && !d ? !1 : r.vars.animationLoop ? !0 : r.atEnd && r.currentSlide === 0 && e === n && r.direction !== "next" ? !1 : r.atEnd && r.currentSlide === n && e === 0 && r.direction === "next" ? !1 : !0
        };
        r.getTarget = function (e) {
            r.direction = e;
            return e === "next" ? r.currentSlide === r.last ? 0 : r.currentSlide + 1 : r.currentSlide === 0 ? r.last : r.currentSlide - 1
        };
        r.setProps = function (e, t, n) {
            var i = function () {
                var n = e ? e : (r.itemW + r.vars.itemMargin) * r.move * r.animatingTo,
                    i = function () {
                        if (h) return t === "setTouch" ? e : c && r.animatingTo === r.last ? 0 : c ? r.limit - (r.itemW + r.vars.itemMargin) * r.move * r.animatingTo : r.animatingTo === r.last ? r.limit : n;
                        switch (t) {
                        case "setTotal":
                            return c ? (r.count - 1 - r.currentSlide + r.cloneOffset) * e : (r.currentSlide + r.cloneOffset) * e;
                        case "setTouch":
                            return c ? e : e;
                        case "jumpEnd":
                            return c ? e : r.count * e;
                        case "jumpStart":
                            return c ? r.count * e : e;
                        default:
                            return e
                        }
                    }();
                return i * -1 + "px"
            }();
            if (r.transitions) {
                i = l ? "translate3d(0," + i + ",0)" : "translate3d(" + i + ",0,0)";
                n = n !== undefined ? n / 1e3 + "s" : "0s";
                r.container.css("-" + r.pfx + "-transition-duration", n)
            }
            r.args[r.prop] = i;
            (r.transitions || n === undefined) && r.container.css(r.args)
        };
        r.setup = function (t) {
            if (!p) {
                var n, s;
                if (t === "init") {
                    r.viewport = e('<div class="' + i + 'viewport"></div>').css({
                        overflow: "hidden",
                        position: "relative"
                    }).appendTo(r).append(r.container);
                    r.cloneCount = 0;
                    r.cloneOffset = 0;
                    if (c) {
                        s = e.makeArray(r.slides).reverse();
                        r.slides = e(s);
                        r.container.empty().append(r.slides)
                    }
                }
                if (r.vars.animationLoop && !h) {
                    r.cloneCount = 2;
                    r.cloneOffset = 1;
                    t !== "init" && r.container.find(".clone").remove();
                    r.container.append(r.slides.first().clone().addClass("clone").attr("aria-hidden", "true")).prepend(r.slides.last().clone().addClass("clone").attr("aria-hidden", "true"))
                }
                r.newSlides = e(r.vars.selector, r);
                n = c ? r.count - 1 - r.currentSlide + r.cloneOffset : r.currentSlide + r.cloneOffset;
                if (l && !h) {
                    r.container.height((r.count + r.cloneCount) * 200 + "%").css("position", "absolute").width("100%");
                    setTimeout(function () {
                        r.newSlides.css({
                            display: "block"
                        });
                        r.doMath();
                        r.viewport.height(r.h);
                        r.setProps(n * r.h, "init")
                    }, t === "init" ? 100 : 0)
                } else {
                    r.container.width((r.count + r.cloneCount) * 200 + "%");
                    r.setProps(n * r.computedW, "init");
                    setTimeout(function () {
                        r.doMath();
                        r.newSlides.css({
                            width: r.computedW,
                            "float": "left",
                            display: "block"
                        });
                        r.vars.smoothHeight && v.smoothHeight()
                    }, t === "init" ? 100 : 0)
                }
            } else {
                r.slides.css({
                    width: "100%",
                    "float": "left",
                    marginRight: "-100%",
                    position: "relative"
                });
                t === "init" && (o ? r.slides.css({
                    opacity: 0,
                    display: "block",
                    webkitTransition: "opacity " + r.vars.animationSpeed / 1e3 + "s ease",
                    zIndex: 1
                }).eq(r.currentSlide).css({
                    opacity: 1,
                    zIndex: 2
                }) : r.slides.css({
                    opacity: 0,
                    display: "block",
                    zIndex: 1
                }).eq(r.currentSlide).css({
                    zIndex: 2
                }).animate({
                    opacity: 1
                }, r.vars.animationSpeed, r.vars.easing));
                r.vars.smoothHeight && v.smoothHeight()
            }
            h || r.slides.removeClass(i + "active-slide").eq(r.currentSlide).addClass(i + "active-slide")
        };
        r.doMath = function () {
            var e = r.slides.first(),
                t = r.vars.itemMargin,
                n = r.vars.minItems,
                i = r.vars.maxItems;
            r.w = r.viewport === undefined ? r.width() : r.viewport.width();
            r.h = e.height();
            r.boxPadding = e.outerWidth() - e.width();
            if (h) {
                r.itemT = r.vars.itemWidth + t;
                r.minW = n ? n * r.itemT : r.w;
                r.maxW = i ? i * r.itemT - t : r.w;
                r.itemW = r.minW > r.w ? (r.w - t * (n - 1)) / n : r.maxW < r.w ? (r.w - t * (i - 1)) / i : r.vars.itemWidth > r.w ? r.w : r.vars.itemWidth;
                r.visible = Math.floor(r.w / r.itemW);
                r.move = r.vars.move > 0 && r.vars.move < r.visible ? r.vars.move : r.visible;
                r.pagingCount = Math.ceil((r.count - r.visible) / r.move + 1);
                r.last = r.pagingCount - 1;
                r.limit = r.pagingCount === 1 ? 0 : r.vars.itemWidth > r.w ? r.itemW * (r.count - 1) + t * (r.count - 1) : (r.itemW + t) * r.count - r.w - t
            } else {
                r.itemW = r.w;
                r.pagingCount = r.count;
                r.last = r.count - 1
            }
            r.computedW = r.itemW - r.boxPadding
        };
        r.update = function (e, t) {
            r.doMath();
            if (!h) {
                e < r.currentSlide ? r.currentSlide += 1 : e <= r.currentSlide && e !== 0 && (r.currentSlide -= 1);
                r.animatingTo = r.currentSlide
            }
            if (r.vars.controlNav && !r.manualControls)
                if (t === "add" && !h || r.pagingCount > r.controlNav.length) v.controlNav.update("add");
                else if (t === "remove" && !h || r.pagingCount < r.controlNav.length) {
                if (h && r.currentSlide > r.last) {
                    r.currentSlide -= 1;
                    r.animatingTo -= 1
                }
                v.controlNav.update("remove", r.last)
            }
            r.vars.directionNav && v.directionNav.update()
        };
        r.addSlide = function (t, n) {
            var i = e(t);
            r.count += 1;
            r.last = r.count - 1;
            l && c ? n !== undefined ? r.slides.eq(r.count - n).after(i) : r.container.prepend(i) : n !== undefined ? r.slides.eq(n).before(i) : r.container.append(i);
            r.update(n, "add");
            r.slides = e(r.vars.selector + ":not(.clone)", r);
            r.setup();
            r.vars.added(r)
        };
        r.removeSlide = function (t) {
            var n = isNaN(t) ? r.slides.index(e(t)) : t;
            r.count -= 1;
            r.last = r.count - 1;
            isNaN(t) ? e(t, r.slides).remove() : l && c ? r.slides.eq(r.last).remove() : r.slides.eq(t).remove();
            r.doMath();
            r.update(n, "remove");
            r.slides = e(r.vars.selector + ":not(.clone)", r);
            r.setup();
            r.vars.removed(r)
        };
        v.init()
    };
    e(window).blur(function (e) {
        focused = !1
    }).focus(function (e) {
        focused = !0
    });
    e.mtslider.defaults = {
        namespace: "mt-",
        selector: ".slides > li",
        animation: "fade",
        easing: "swing",
        direction: "horizontal",
        reverse: !1,
        animationLoop: !0,
        smoothHeight: !1,
        startAt: 0,
        slideshow: !0,
        slideshowSpeed: 7e3,
        animationSpeed: 600,
        initDelay: 0,
        randomize: !1,
        thumbCaptions: !1,
        pauseOnAction: !0,
        pauseOnHover: !1,
        pauseInvisible: !0,
        useCSS: !0,
        touch: !0,
        video: !1,
        controlNav: !0,
        directionNav: !0,
        prevText: "Previous",
        nextText: "Next",
        keyboard: !0,
        multipleKeyboard: !1,
        mousewheel: !1,
        pausePlay: !1,
        pauseText: "Pause",
        playText: "Play",
        controlsContainer: "",
        manualControls: "",
        sync: "",
        asNavFor: "",
        itemWidth: 0,
        itemMargin: 0,
        minItems: 1,
        maxItems: 0,
        move: 0,
        allowOneSlide: !0,
        start: function () {},
        before: function () {},
        after: function () {},
        end: function () {},
        added: function () {},
        removed: function () {}
    };
    e.fn.mtslider = function (t) {
        t === undefined && (t = {});
        if (typeof t == "object") return this.each(function () {
            var n = e(this),
                r = t.selector ? t.selector : ".slides > li",
                i = n.find(r);
            if (i.length === 1 && t.allowOneSlide === !0 || i.length === 0) {
                i.fadeIn(400);
                t.start && t.start(n)
            } else n.data("mtslider") === undefined && new e.mtslider(this, t)
        });
        var n = e(this).data("mtslider");
        switch (t) {
        case "play":
            n.play();
            break;
        case "pause":
            n.pause();
            break;
        case "stop":
            n.stop();
            break;
        case "next":
            n.mtAnimate(n.getTarget("next"), !0);
            break;
        case "prev":
        case "previous":
            n.mtAnimate(n.getTarget("prev"), !0);
            break;
        default:
            typeof t == "number" && n.mtAnimate(t, !0)
        }
    }
})(jQuery, window, document);

/*
 * Mobile Menu
 */
;(function ($, window, document) {
    $.fn.mtMobileNav = function ( options ) {
        // Default Settings
        var defaults = {
            // Menu Positioning
            position: "right", // right, left, top
            action: "slide", // slide or push
            // Menu Insertion
            attachment: "append", // append, before, after
            container: "header", // the container to append to or insert before/after
            // Menu Wrapper Opts
            wrapperAttr: {
                cssClass: "mobile-nav"
            }, // the class to assign the wrapper
            // Menu Toggle
            toggleBtn: "section.mobile-menu .toggle",
            activeClass: "active",
            // Misc Opts
            overlayColor: "rgba(0,0,0,.8)", // the overlay color to insert over the site content (slide only)
            topToggleMode: "slide", // slide, showhide, or a custom function
            topToggleSpeed: 0, // the speed of the slide or showhide transition for top positioned menu
            appendSearch: null, // set to the selector for the search form if you would like it appended to the menu
            prependSearch: null, // set to the selector for the search form if you would like it prepended to the menu
            // Callbacks
            onAttach: function() {},
            onMenuOpen: function() {},
            onMenuClose: function() {}
        };

        // Apply passed options and clone nav
        var opts = $.extend( {}, defaults, options);

        
        // Setup Menu Toggle and related vars
        var init = init(this),
            $toggleBtn = $(opts.toggleBtn),
            $menu = init.menu,
            menuWidth = init.menuWidth;

        // Append Overlay for Slide Nav
        if (opts.action == "slide") {
            $("body").append('<div class="mtMobileNavOverlay" style="background:'+ opts.overlayColor +';position:absolute;top:0;left:0;width:100%;height:' + $(document).height() + 'px;display:none;z-index:999998;"></div>');
            $toggleBtn.css({"z-index": 999999});
            $overlay = $(".mtMobileNavOverlay");
            $overlay.on("click", menuToggle);
        }

        // Attach menu toggle event
        $toggleBtn.on("click", menuToggle);

        // Return the mobile menu for chainability
        return $menu;

        // Private functions
        function attach (nav) {
            // Attach Nav per Opts
            switch (opts.attachment) {
                case "append": $(opts.container).append('<section class="' + opts.wrapperAttr.cssClass + '" role="navigation">' + nav.html() + '</section>'); break;
                case "before": $(opts.container).before('<section class="' + opts.wrapperAttr.cssClass + '" role="navigation">' + nav.html() + '</section>'); break;
                case "after":  $(opts.container).after('<section class="' + opts.wrapperAttr.cssClass + '" role="navigation">' + nav.html() + '</section>'); break;
            }
            // If we are attaching the site search
            switch (true) {
                case (opts.appendSearch != null && $(opts.appendSearch).length > 0): $("." + opts.wrapperAttr.cssClass).append($(opts.appendSearch).clone()); break;
                case (opts.prependSearch != null && $(opts.prependSearch).length > 0): $("." + opts.wrapperAttr.cssClass).prepend($(opts.prependSearch).clone()); break;
            }
            // Fire Callback
            opts.onAttach.call(this);
        }

        function init(nav) {
            // Clone Nav and attach
            var $nav = nav.clone().unwrap();
            attach($nav);

            // Get inserted nav and set positioning
            var $mobileNav = $("." + opts.wrapperAttr.cssClass),
                 mnavWidth = $mobileNav.outerWidth();

            position($mobileNav, mnavWidth);

            // Return objects for variable assignment
            return {
                menu: $mobileNav,
                menuWidth: mnavWidth
            }
        }

        function position(menu, width) {
            switch (opts.position) {
                case "left":
                    menu.css({ position: "fixed", height: "100%", top: 0, left: 0, marginLeft: "-" + width + "px", zIndex: 999999 });
                    break;
                case "right":
                    menu.css({ position: "fixed", height: "100%", top: 0, right: 0, marginRight: "-" + width + "px", zIndex: 999999 });
                    break;
                case "top":
                    menu.css({ display: "none" });
                    break;
            }
        }

        function slideMenu() {
            // Get Current Menu State
            if ($menu.css("margin-" + opts.position) >= "0px") {
                // menu is OPEN - now CLOSE it
                var anim = function() {
                    if (opts.position == "left") return { "margin-left": "-=" + menuWidth };
                    else if (opts.position == "right") return { "margin-right": "-=" + menuWidth };
                    else console.log("The position passed is not valid for use with this method.");
                }
                if (opts.action == "slide") { 
                    $overlay.fadeOut('fast'); 
                    $toggleBtn.animate(anim());
                }
                else if (opts.action == "push") { 
                    var moveBody = function() {
                        if (opts.position == "left") return { "left": 0 };
                        else if (opts.position == "right") return { "right": 0 };
                    }
                    $("body").animate(moveBody(), function() {
                        $(this).css({ "position": "static" });
                    }); 
                }
                $menu.animate(anim());
                // Fire callback
                opts.onMenuClose.call(this);
            } 
            else {
                // menu is CLOSED - now OPEN it
                var anim = function() {
                    if (opts.position == "left") return { "margin-left": "+=" + menuWidth };
                    else if (opts.position == "right") return { "margin-right": "+=" + menuWidth };
                    else console.log("The position passed, " +  + " , is not valid for use with this method.")
                }
                if (opts.action == "slide") { 
                    $overlay.fadeIn('fast'); 
                    $toggleBtn.animate(anim());
                }
                else if (opts.action == "push") {
                    var moveBody = function() {
                        if (opts.position == "left") return { "left": menuWidth + "px" };
                        else if (opts.position == "right") return { "right": menuWidth + "px" };
                    }
                    $("body").css({ "position": "relative" }).animate(moveBody()); 
                }
                $menu.animate(anim());
                // Fire callback
                opts.onMenuOpen.call(this);
            }
        }

        function menuToggle() {
            switch (opts.position) {
                case "top": 
                    // Use specified top menu display method
                    if (opts.topToggleMode == "showhide") $menu.toggle(opts.topToggleSpeed);
                    else if ($.isFunction(opts.topToggleMode)) opts.topToggleMode.call(this);
                    else $menu.slideToggle(opts.topToggleSpeed);
                    // Check toggleBtn class and fire appropriate callback
                    if ($toggleBtn.hasClass(opts.activeClass)) opts.onMenuClose.call(this);
                    else opts.onMenuOpen.call(this);
                    break;
                default: slideMenu(); break;
            }
            // Toggle Class on Button
            $toggleBtn.toggleClass(opts.activeClass);
            return false;
        }
    }
})(jQuery, window, document);

/*
 * Placeholder Support form elements for Older Browsers
 */
;(function ($, window, document) {
    $.fn.placehold = function (placeholderClassName) {
        var placeholderClassName = placeholderClassName || "placeholder",
            supported = $.fn.placehold.is_supported();

        function toggle() {
            for (i = 0; i < arguments.length; i++) {
                arguments[i].toggle();
            }
        }

        return supported ? this : this.each(function () {
            var $elem = $(this),
                placeholder_attr = $elem.attr("placeholder");

            if (placeholder_attr) {
                if ($elem.val() === "" || $elem.val() == placeholder_attr) {
                    $elem.addClass(placeholderClassName).val(placeholder_attr);
                }

                if ($elem.is(":password")) {
                    var $pwd_shiv = $("<input />", {
                        "class": $elem.attr("class") + " " + placeholderClassName,
                        "value": placeholder_attr
                    });

                    $pwd_shiv.bind("focus.placehold", function () {
                        toggle($elem, $pwd_shiv);
                        $elem.focus();
                    });

                    $elem.bind("blur.placehold", function () {
                        if ($elem.val() === "") {
                            toggle($elem, $pwd_shiv);
                        }
                    });

                    $elem.hide().after($pwd_shiv);
                }

                $elem.bind({
                    "focus.placehold": function () {
                        if ($elem.val() == placeholder_attr) {
                            $elem.removeClass(placeholderClassName).val("");
                        }
                    },
                    "blur.placehold": function () {
                        if ($elem.val() === "") {
                            $elem.addClass(placeholderClassName).val(placeholder_attr);
                        }
                    }
                });

                $elem.closest("form").bind("submit.placehold", function () {
                    if ($elem.val() == placeholder_attr) {
                        $elem.val("");
                    }

                    return true;
                });
            }
        });
    };

    $.fn.placehold.is_supported = function () {
        return "placeholder" in document.createElement("input");
    };
})(jQuery, window, document);

/*
 * Responsive Tables
 */
$(function() {
  var switched = false;
  var updateTables = function() {
    if (($(window).width() < 767) && !switched ){
      switched = true;
      $("table.restable").each(function(i, element) {
        splitTable($(element));
      });
      return true;
    }
    else if (switched && ($(window).width() > 767)) {
      switched = false;
      $("table.restable").each(function(i, element) {
        unsplitTable($(element));
      });
    }
  };
   
  $(window).load(updateTables);
  $(window).on("redraw",function(){switched=false;updateTables();}); // An event to listen for
  $(window).on("resize", updateTables);
   
    
    function splitTable(original) {
        original.wrap("<div class='table-wrapper' />");
        
        var copy = original.clone();
        copy.find("td:not(:first-child), th:not(:first-child)").css("display", "none");
        copy.removeClass("restable");
        
        original.closest(".table-wrapper").append(copy);
        copy.wrap("<div class='pinned' />");
        original.wrap("<div class='scrollable' />");

    setCellHeights(original, copy);
    }
    
    function unsplitTable(original) {
    original.closest(".table-wrapper").find(".pinned").remove();
    original.unwrap();
    original.unwrap();
    }

  function setCellHeights(original, copy) {
    var tr = original.find('tr'),
        tr_copy = copy.find('tr'),
        heights = [];

    tr.each(function (index) {
      var self = $(this),
          tx = self.find('th, td');

      tx.each(function () {
        var height = $(this).outerHeight(true);
        heights[index] = heights[index] || 0;
        if (height > heights[index]) heights[index] = height;
      });

    });

    tr_copy.each(function (index) {
      $(this).height(heights[index]);
    });
  }
});

/*
 * MedTouch Helper Utilities
 */
var mtUtilities = {
    /*
     * Shopping Cart Promo Handling
     */
    calendarModule: function() {
        var $cePromoMap = $("#hdnPromoCodesAvail"),
            $instructorListing = $(".module-ce-instructors-listing"),
            $checkoutHeader = $(".module-ce-checkout-header"),
            $cartSummary = $(".module-ce-session-shoppingcart-summary");

        if ($cePromoMap.length > 0) {

            var cePromoMap = $cePromoMap.val().toUpperCase().split(","),
                flag = false,
                $promoInput = $("input.promo-input"),
                $promoBtn = $("div.add-promo-panel").find("input[type=submit]");

            $(".add-promo-link").children("a").on("click", function (e) {
                e.preventDefault();
                // var $this = $(this);
                // $this.toggleClass("active").parent().next().find("span.error").hide().parent().slideToggle(400);
                // if ($this.hasClass("active")) $promoInput.val("").focus();
                // return false;
            });

            $promoBtn.on("click", function (e) {
                if (flag === false) {
                    e.preventDefault();
                    switch (true) {
                        case $promoInput.val() === "" || $promoInput.val() === "Enter Promo Code":
                            $("span[data-reason='blank']").show();
                            return false;
                            break;
                        case $.inArray($promoInput.val().toUpperCase(), cePromoMap) === -1:
                            $("span[data-reason='invalid']").show();
                            return false;
                            break;
                        default:
                            flag = true;
                            break;
                    }
                    if (flag === true) $(this).click();
                }
            });

            $promoInput.keyup(function (e) {
                var $this = $(this);
                // If the invalid code message is showing, hide on typing
                if ($.inArray(e.keyCode, [37, 38, 39, 40, 16]) === -1 && ($this.val() === "" || $this.val().length === 1)) $("span[data-reason='invalid']:visible").hide();
                // Add enable/disable class to Apply link        
                if ($.inArray($this.val().toUpperCase(), cePromoMap) === -1) $promoBtn.addClass("disabled");
                else {
                    $promoBtn.removeClass("disabled");
                    $("span[data-reason='invalid']:visible").hide();
                }
                // If the blank error is up and text has been entered, hide it
                if ($this.val() !== "") $("span[data-reason='blank']:visible").hide();
                // If the user pressed the enter key, trigger a click on the Apply button
                if (e.keyCode == 13) {
                    $promoBtn.click();
                }
            });
        }

        if ($instructorListing.length > 0) {
            if ($instructorListing.length > 0) {
                $(".hide-onload").hide();
                $instructorListing.each( function (i, el) {
                        var $this = $(this),
                                $toggleBtn = $this.find(".toggle-hide-show"),
                                $togglePanel = $this.find(".instructor-item");

                        if ($toggleBtn.length > 0) {
                            $togglePanel.eq(0).show().prev(".toggle-hide-show").children("a").addClass("open").children("span").html("-");
                        }
                });
                $(".toggle-hide-show a").unbind("click").on("click", function (e) {
                        e.preventDefault();
                        var $this = $(this).hasClass("toggle-hide-show") ? $(this).children("a") : $(this);
                        if ($this.hasClass("open")) {
                            $this.toggleClass("open").children("span").html("+").end().parent().next(".instructor-item").slideToggle();
                        }
                        else {
                            $this.parents(".module-ce-instructors-listing").find("a.open").click();
                            $this.toggleClass("open").children("span").html("-").end().parent().next(".instructor-item").slideToggle();
                        }
                }); 
            }
        }

        if ($checkoutHeader.length > 0 && $cartSummary.length > 0) {
            $cartSummary.find(".cart-checkout").hide();
        }
    },

    /*
     * Search Keyword Autocomplete
     */
    correctComplete: function () {
        var oldFn = $.ui.autocomplete.prototype._renderItem;

        $.ui.autocomplete.prototype._renderItem = function(ul, item) {
            var re = new RegExp("\\b" + this.term, "i"),
                t = item.label.replace(re,"<span style='font-weight:bold'>" + this.term + "</span>");
            
            return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + t + "</a>" )
                .appendTo( ul )
				
        };
    },

    /*
    * Custom Select
    */
    customSelect: function () {
        var $select = $("select:not('[data-processed=true]')").not(".no-custom-select"),
            cs = function () {
                return $("select.selectboxdiv");
            },
            selected = function (el) {
                var el = el.tagName === "SELECT" ? $(el) : $(el).find("select");
                value = el.children(":selected").length > 0 ? el.children(":selected").text() : el.children().eq(0).text();
                return value !== "" ? value : "\xA0";
            },
            setupSelect = function (el, settings) {
                var $this = $(el),
                    opts = {},
                    settings = settings || {},
                    value = selected(el);

                // Check if select has alread been processed
                if ( $this.not("[data-processed=true]") ) {
                    // Setup settings
                    opts.selectClass = settings.selectClass ? " " + settings.selectClass : "";
                    opts.wrapperClass = settings.wrapperClass ? " " + settings.wrapperClass : "";

                    // Check for <select> wrapper <div>, add if not found and append output <div>
                    if ( !$this.parent(".selectbox").length ) {
                        $this.wrap('<div class="selectbox' + opts.wrapperClass + '" />');
                    }

                    // Check for output <div> and add if it does not exist
                    if ( !$this.siblings("div.out").length ) {
                        $this.parent(".selectbox").append('<div class="out" />');
                    }
                    
                    // Add class on <select> and set processed attribute to true 
                    $this
                        .addClass("selectboxdiv")
                        .attr("data-processed", "true");
                }

                // Set select value
                updateSelect(el);
            },
            updateSelect = function (el) {
                $(el).next(".out").text(selected(el));
            };

        // Initialize <select> elements on page
        if ($select.length) {
            $select.each( function (i, el) {
                var $this = $(this);

                switch (true) {
                    case $this.parents(".scfDateSelectorGeneralPanel").length > 0:
                        var wffmClass = $this.attr("class") + "Wrap shortDateSelect";
                        setupSelect( el, { wrapperClass: wffmClass } );
                        break;
                    default:
                        setupSelect( el );
                }
            });
        }

        // <select> change handler
        cs().on("change keyup", function (e) {
            updateSelect(this);
        });

        // Reset Buttons
        $('.reset').click(function () {
            setTimeout(function () {
                $cs().trigger('change').trigger('update');
            }, 1);
        });
    },
	
	

    isPageEditor: function () {
        if (typeof Sitecore == "undefined") {
            return false;
        }
        if (typeof Sitecore.PageModes == "undefined" || Sitecore.PageModes == null) {
            return false;
        }
        return Sitecore.PageModes.PageEditor != null;
    },


    /*
     * Responsive Tabs
     */
    responsiveTabs: function () {
        var $tabSets = $('.responsive-tabs');

        if (!$tabSets.hasClass('responsive-tabs--enabled')) {   // if we haven't already called this function and enabled tabs
            $tabSets.addClass('responsive-tabs--enabled'); 

            //loop through all sets of tabs on the page
            var tablistcount = 1;

            $tabSets.each(function() {

                var $tabs = $(this);

                // add tab heading and tab panel classes
                $tabs.children('h1,h2,h3,h4,h5,h6').addClass('responsive-tabs__heading');
                $tabs.children('div').addClass('responsive-tabs__panel');

                // determine if markup already identifies the active tab panel for this set of tabs
                // if not then set first heading and tab to be the active one
                var $activePanel = $tabs.find('.responsive-tabs__panel--active');
                if(!$activePanel.length) {
                    var $activePanel = $tabs.find('.responsive-tabs__panel').first().addClass('responsive-tabs__panel--active');
                }

                $tabs.find('.responsive-tabs__panel').not('.responsive-tabs__panel--active').hide().attr('aria-hidden','true'); //hide all except active panel
                $activePanel.attr('aria-hidden', 'false');
                /* make active tab panel hidden for mobile */
                $activePanel.addClass('responsive-tabs__panel--closed-accordion-only');

                // wrap tabs in container - to be dynamically resized to help prevent page jump
                var $tabsWrapper = $('<div/>', {Class: 'responsive-tabs-wrapper' });
                $tabs.wrap($tabsWrapper);

                var highestHeight = 0;

                // determine height of tallest tab panel. Used later to prevent page jump when tabs are clicked
                $tabs.find('.responsive-tabs__panel').each(function() {
                    var tabHeight = $(this).height();
                    if (tabHeight > highestHeight) {
                        highestHeight = tabHeight;
                    }
                })

                //create the tab list
                var $tabList = $('<ul/>', { Class: 'responsive-tabs__list', 'role': 'tablist' });

                //loop through each heading in set
                var tabcount = 1;
                $tabs.find('.responsive-tabs__heading').each(function() {

                    var $tabHeading = $(this);
                    var $tabPanel = $(this).next();

                    $tabHeading.attr('tabindex', 0);

                    // CREATE TAB ITEMS (VISIBLE ON DESKTOP)
                    //create tab list item from heading
                    //associate tab list item with tab panel
                    var $tabListItem = $('<li/>', { 
                        Class: 'responsive-tabs__list__item',
                        id: 'tablist' + tablistcount + '-tab' + tabcount,
                        'aria-controls': 'tablist' + tablistcount +'-panel' + tabcount,
                        'role': 'tab',
                        tabindex: 0,
                        text: $tabHeading.text(),
                        keydown: function (objEvent) {
                            if (objEvent.keyCode == 13) { // if user presses 'enter'
                                $tabListItem.click();
                            }
                        },
                        click: function() {
                            //Show associated panel

                            //set height of tab container to highest panel height to avoid page jump
                            $tabsWrapper.css('height', highestHeight);

                            // remove hidden mobile class from any other panel as we'll want that panel to be open at mobile size
                            $tabs.find('.responsive-tabs__panel--closed-accordion-only').removeClass('responsive-tabs__panel--closed-accordion-only');
                            
                            // close current panel and remove active state from its (hidden on desktop) heading
                            $tabs.find('.responsive-tabs__panel--active').toggle().removeClass('responsive-tabs__panel--active').attr('aria-hidden','true').prev().removeClass('responsive-tabs__heading--active');
                            
                            //make this tab panel active
                            $tabPanel.toggle().addClass('responsive-tabs__panel--active').attr('aria-hidden','false');

                            //make the hidden heading active
                            $tabHeading.addClass('responsive-tabs__heading--active');

                            //remove active state from currently active tab list item
                            $tabList.find('.responsive-tabs__list__item--active').removeClass('responsive-tabs__list__item--active');

                            //make this tab active
                            $tabListItem.addClass('responsive-tabs__list__item--active');

                            //reset height of tab panels to auto
                            $tabsWrapper.css('height', 'auto');
                        }
                    });
                    
                    //associate tab panel with tab list item
                    $tabPanel.attr({
                        'role': 'tabpanel',
                        'aria-labelledby': $tabListItem.attr('id'),
                        id: 'tablist' + tablistcount + '-panel' + tabcount
                    });

                    // if this is the active panel then make it the active tab item
                    if($tabPanel.hasClass('responsive-tabs__panel--active')) {
                        $tabListItem.addClass('responsive-tabs__list__item--active');
                    }

                    // add tab item
                    $tabList.append($tabListItem);

                    
                    // TAB HEADINGS (VISIBLE ON MOBILE)
                    // if user presses 'enter' on tab heading trigger the click event
                    $tabHeading.keydown(function(objEvent) {
                        if (objEvent.keyCode == 13) {
                             $tabHeading.click();
                        }
                    })

                    //toggle tab panel if click heading (on mobile)
                    $tabHeading.click(function() {

                        // remove any hidden mobile class
                        $tabs.find('.responsive-tabs__panel--closed-accordion-only').removeClass('responsive-tabs__panel--closed-accordion-only');

                        // if this isn't currently active
                        if (!$tabHeading.hasClass('responsive-tabs__heading--active')){
                            //get position of active heading 
                            if($('.responsive-tabs__heading--active').length) {
                                var oldActivePos = $('.responsive-tabs__heading--active').offset().top;
                            }
                            
                            // close currently active panel and remove active state from any hidden heading
                            $tabs.find('.responsive-tabs__panel--active').slideToggle().removeClass('responsive-tabs__panel--active').prev().removeClass('responsive-tabs__heading--active');
                            
                            //close all tabs
                            $tabs.find('.responsive-tabs__panel').hide().attr('aria-hidden','true');

                            //open this panel
                            $tabPanel.slideToggle().addClass('responsive-tabs__panel--active').attr('aria-hidden','false');

                            // make this heading active
                            $tabHeading.addClass('responsive-tabs__heading--active');

                            var $currentActive = $tabs.find('.responsive-tabs__list__item--active');

                            //set the active tab list item (for desktop)
                            $currentActive.removeClass('responsive-tabs__list__item--active');
                            var panelId = $tabPanel.attr('id');
                            var tabId = panelId.replace('panel','tab');
                            $('#' + tabId).addClass('responsive-tabs__list__item--active');

                            //scroll to active heading only if it is below previous one
                            var tabsPos = $('.responsive-tabs').offset().top;
                            var newActivePos = $('.responsive-tabs__heading--active').offset().top;
                            if(oldActivePos < newActivePos) {
                                $('html, body').animate({ scrollTop: tabsPos }, 0).animate({ scrollTop: newActivePos }, 400);
                            }
                            
                        }

                        // if this tab panel is already active
                        else {

                            // hide panel but give it special responsive-tabs__panel--closed-accordion-only class so that it can be visible at desktop size
                            $tabPanel.removeClass('responsive-tabs__panel--active').slideToggle(function () { $(this).addClass('responsive-tabs__panel--closed-accordion-only') });

                            //remove active heading class
                            $tabHeading.removeClass('responsive-tabs__heading--active');

                            //don't alter classes on tabs as we want it active if put back to desktop size
                        }
                        
                    })

                    tabcount ++;

                })

                // add finished tab list to its container
                $tabs.prepend($tabList);

                // next set of tabs on page
                tablistcount ++;
            })
        }
    }
}


!function($) {
  $.fn.tree = function(options) {
    if(!options) options = {};
    var default_options = {
      open_char                     : '-',
      close_char                    : '+',
      default_expanded_paths_string : '',
      only_one                      : false,
      animation                     : 'slow'
    };
    var o = {};
    $.extend(o, default_options, options);

    // Get the expanded paths from the current state of tree
    $.fn.save_paths = function() {
      var paths = [],
          path = [],
          open_nodes = $(this).find('li span.open'),
          last_depth = null;
      for(var i = 0; i < open_nodes.length; i++) {
        var depth = $(open_nodes[i]).parents('ul').length-1;
        if((last_depth == null && depth > 0) || (depth > last_depth && depth-last_depth > 1))
          continue;
        var pos = $(open_nodes[i]).parent().prevAll().length;
        if(last_depth == null) {
          path = [pos];
        } else if(depth < last_depth) {
          paths.push(path.join('/'));
          var diff = last_depth - depth;
          path.splice(path.length-diff-1, diff+1);
          path.push(pos);
        } else if(depth == last_depth) {
          paths.push(path.join('/'));
          path.splice(path.length-1, 1);
          path.push(pos);
        } else if(depth > last_depth) {
          path.push(pos);
        }
        last_depth = depth;
      }
      paths.push(path.join('/'));
      // Save state to cookie
      var cookie_key = this.data('cookie');
      if(!cookie_key) cookie_key = 'jtree-cookie';
      try { $.cookie(cookie_key, paths.join(',')); }
      catch(e) {}
    };

    // This function expand the tree with 'path'
    $.fn.restore_paths = function() {
      var paths_string = null;
      var cookie_key = this.data('cookie');
      if(!cookie_key) cookie_key = 'jtree-cookie';
      try { paths_string = $.cookie(cookie_key); }
      catch(e) {}
      if(!paths_string) paths_string = o.default_expanded_paths_string;
      if(paths_string == 'all') {
        $(this).find('span.jtree-arrow').open();
      } else {
        var paths = paths_string.split(',');
        for(var i = 0; i < paths.length; i++) {
          var path = paths[i].split('/'),
              obj = $(this);
          for(var j = 0; j < path.length; j++) {
            obj = $(obj.children('li')[path[j]]);
            obj.children('span.jtree-arrow').open();
            obj = obj.children('ul')
          }
        }
      }
    };

    // Expand the tree (animation bug fixed - @stecb)
    $.fn.expand = function(animation) {
      animation = (animation == 'none') ? undefined : (!animation) ? o.animation : animation;
      // find each :first-child .jtree-arrow.close element (we don't need to care about open elements, they're already open...)
      elems = $(this).find('.jtree-arrow.close:first-child');
      // their childs .jtree-arrow.close need to be opened without animation (!IMPORTANT!)
      elems.siblings('ul').find('.jtree-arrow.close').open(undefined);
      // the :first-child elements now can be opened with animation (height is known now)
      elems.open(animation);
    };

    // Collapse the tree
    $.fn.collapse = function(animation) {
      animation = (animation == 'none') ? undefined : (!animation) ? o.animation : animation;
      $(this).find('.jtree-arrow').close(animation);
    };

    // Open a child
    $.fn.open = function(animation) {
      if($(this).hasClass('jtree-arrow')) {
        $(this).parent().children('ul').show(animation);
        $(this).removeClass('close').addClass('open').html(o.open_char);
      }
    };

    // Close a child
    $.fn.close = function(animation) {
      if($(this).hasClass('jtree-arrow')) {
        $(this).parent().children('ul').hide(animation);
        $(this).removeClass('open').addClass('close').html(o.close_char);
      }
    };

    // Click event on <span class="jtree-arrow"></span>
    $.fn.click_event = function() {
      var button = $(this);
      if(button.hasClass('jtree-arrow')) {
        button[button.hasClass('open') ? 'close' : 'open'](o.animation);
        if(o.only_one) button.closest('li').siblings().children('span.jtree-arrow').close(o.animation);
      }
    };

    for(var i = 0; i < this.length; i++) {
      if(this[i].tagName === 'UL') {
        // Make a tree
        $(this[i]).find('li').has('ul').prepend('<span class="jtree-arrow close" style="cursor:pointer;">' + o.close_char + '</span>');
        $(this[i]).find('ul').hide();
        // Restore cookie expand path
        $(this[i]).restore_paths();
        // Click event for arrow
        $(this[i]).find('li > span.jtree-arrow').on('click', {tree : this[i]}, function(e) {
          $(this).click_event();
          $(e.data.tree).save_paths();
        });
        // Click event for 'jtree-button'
        $(this[i]).find('li .jtree-button').on('click', {tree : this[i]}, function(e) {
          var arrow = $(this).closest('li').children('span.jtree-arrow');
          arrow.click_event();
          $(e.data.tree).save_paths();
        });
      }
    }
  };
}(jQuery);

jQuery.cookie = function(name, value, options) {
  if (typeof value != 'undefined') { // name and value given, set cookie
    options = options || {};
    if (value === null) {
      value = '';
      options.expires = -1;
    }
    var expires = '';
    if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
      var date;
      if (typeof options.expires == 'number') {
        date = new Date();
        date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
      } else {
        date = options.expires;
      }
      expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
    }
    // CAUTION: Needed to parenthesize options.path and options.domain
    // in the following expressions, otherwise they evaluate to undefined
    // in the packed version for some reason...
    var path = options.path ? '; path=' + (options.path) : '';
    var domain = options.domain ? '; domain=' + (options.domain) : '';
    var secure = options.secure ? '; secure' : '';
    document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
  } else { // only name given, get cookie
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) == (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }
};

;(function(w){
    // Enable strict mode
    "use strict";

    w.picturefill = function() {
        var ps = w.document.getElementsByTagName( "span" );

        // Loop the pictures
        for( var i = 0, il = ps.length; i < il; i++ ){
            if( ps[ i ].getAttribute( "data-picture" ) !== null ){

                var sources = ps[ i ].getElementsByTagName( "span" ),
                    matches = [];

                // See if which sources match
                for( var j = 0, jl = sources.length; j < jl; j++ ){
                    var media = sources[ j ].getAttribute( "data-media" );
                    // if there's no media specified, OR w.matchMedia is supported 
                    if( !media || ( w.matchMedia && w.matchMedia( media ).matches ) ){
                        matches.push( sources[ j ] );
                    }
                }

            // Find any existing img element in the picture element
            var picImg = ps[ i ].getElementsByTagName( "img" )[ 0 ];

            if( matches.length ){
                var matchedEl = matches.pop();
                if( !picImg || picImg.parentNode.nodeName === "NOSCRIPT" ){
                    picImg = w.document.createElement( "img" );
                    picImg.alt = ps[ i ].getAttribute( "data-alt" );
                }
                else if( matchedEl === picImg.parentNode ){
                    // Skip further actions if the correct image is already in place
                    continue;
                }

                picImg.src =  matchedEl.getAttribute( "data-src" );
                matchedEl.appendChild( picImg );
                picImg.removeAttribute("width");
                picImg.removeAttribute("height");
            }
            else if( picImg ){
                picImg.parentNode.removeChild( picImg );
            }
        }
        }
    };

    // Run on resize and domready (w.load as a fallback)
    if( w.addEventListener ){
        w.addEventListener( "resize", w.picturefill, false );
        w.addEventListener( "DOMContentLoaded", function(){
            w.picturefill();
            // Run once only
            w.removeEventListener( "load", w.picturefill, false );
        }, false );
        w.addEventListener( "load", w.picturefill, false );
    }
    else if( w.attachEvent ){
        w.attachEvent( "onload", w.picturefill );
    }
}(this));


//
// Crazy Fixes for things CSS Just Can't do...
//

/*
 * Main Nav Dropdown Menu Right Alignment
 */
$(function() {
    var $rightmenus = $("nav.main-nav").find("div.drop-down.right");

    // Determine Width for Right Aligned Drop Downs and Position
    $rightmenus.each( function() {
        var $this = $(this),
            width = $this.width();

        $this.css({ "width": width + "px", "right": 0 }).parent().css({ "position": "relative" });
    });
});


$(function() {
    var $dates = $("input[type=date]");

    if (Modernizr && (Modernizr.touch && Modernizr.inputtypes.date) && $dates.length > 0) {
        $dates.each( function (index, element) {
            $(this).removeClass("dp_input").addClass("nodp_input").attr("type","text").on({
                focus: function (e) { 
                    $(this).attr("type","date"); 
                },
                blur: function (e) { 
                    if ($(this).val() === "") $(this).attr("type","text");
                }
            });
        });
    }
    else if ($dates.length > 0) {
        $dates.each( function (index, element) {
           $(this).attr("type","text").addClass("textbox").datepicker();
        });
    }
});


/*
 * Global Form Reset Button
 */
$(function() {
    var $resetBtn = $(".button.reset-button, .lt-ie10 .reset");

    $resetBtn.on("click", function (e) {
        e.preventDefault();

        var $this = $(this);
        $this.parents(".core-search,.core-search-again").find(":input").not(":button, :submit, :reset, :hidden").val("").removeAttr("checked").removeAttr("selected").trigger("change").trigger("blur");
    });
});


//
// Misc Other Functions
//
//function mtInit(opts) {
//
//}
