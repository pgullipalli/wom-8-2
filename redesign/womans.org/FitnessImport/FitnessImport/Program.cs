﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;

namespace FitnessImport
{
    class Program
    {
        public static string log = string.Empty;

        public static void Main(string[] args)
        {
            ImportWrapper();
        }

        static void ImportWrapper()
        {
            string emailMessage = string.Empty;
            string destinationTableName = ConfigurationManager.AppSettings["medtouch.data.import.tablename"].ToString();

            log += "Calling FitnessImport [START]";
            //log += string.Format("There are total of {0} physicians in the xml.", physicians.Count);
            emailMessage += string.Format("{0} {1}- Import process started\r\n", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
            Stopwatch sw = Stopwatch.StartNew();
            DateTime now = DateTime.Now;
            
            string connectionString = ConfigurationManager.ConnectionStrings["customDB"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            SqlConnection conDelete = new SqlConnection(connectionString);
            string filepath = ConfigurationManager.AppSettings["medtouch.fitness.import.csv.filepath"].ToString(); 
            StreamReader sr = new StreamReader(filepath);
            string line = sr.ReadLine();
            string[] value = line.Split(',');
            DataTable dt = new DataTable();
            DataRow row;
            foreach (string dc in value)
            {
                dt.Columns.Add(new DataColumn(dc));
            }

            while ( !sr.EndOfStream )
            {
                value = sr.ReadLine().Split(',');
                if(value.Length == dt.Columns.Count)
                {
                    row = dt.NewRow();
                    row.ItemArray = value;
                    dt.Rows.Add(row);
                }
            }
            

            if (dt.Rows.Count > 0)
            {
                string sql = @"DELETE FROM " + destinationTableName + ";";
                conDelete.Open();
                SqlCommand cmd = new SqlCommand(sql, conDelete);
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }

            SqlBulkCopy bc = new SqlBulkCopy(con.ConnectionString, SqlBulkCopyOptions.TableLock);
            bc.DestinationTableName = destinationTableName;
            bc.BatchSize = dt.Rows.Count;
            con.Open();
            bc.WriteToServer(dt);
            bc.Close();
            con.Close();

            sw.Stop();

            log += "Calling FitnessImport [END]";
            log += String.Format("Total Time Spent: {0:00}:{1:00}:{2:00} ", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds);
            emailMessage += string.Format(" {0} {1} - Import process finished", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString()) + System.Environment.NewLine;
            emailMessage += "\r\nSummary:" + System.Environment.NewLine;
            emailMessage += string.Format("Number of fitness center members imported/updated: {0} ", dt.Rows.Count) + System.Environment.NewLine;
            emailMessage += String.Format("Time processed: {0:00}:{1:00}:{2:00} Log file: ", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds) + System.Environment.NewLine;
            string logFilePath = ConfigurationManager.AppSettings["medtouch.data.import.logging.filepath"] + "log_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            emailMessage += logFilePath;

            LogToFile(log);
            SendEmailNotification(emailMessage);
        }

        public static void LogToFile(string logMessage)
        {
            if (ConfigurationManager.AppSettings["medtouch.data.import.logging.enabled"].Equals("true"))
            {
                string logFilePath = ConfigurationManager.AppSettings["medtouch.data.import.logging.filepath"] + "log_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";

                if (!string.IsNullOrEmpty(logFilePath))
                {
                    using (StreamWriter w = File.AppendText(logFilePath))
                    {
                        w.Write("\r\nLog Entry : ");
                        w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                        w.WriteLine("{0}", logMessage);
                        w.Flush();
                        w.Close();
                    }
                }
            }
        }

        
        
        /// <summary>
        /// Sends the email notification.
        /// </summary>
        public static void SendEmailNotification(string message)
        {
            try
            {
                SendEmail(message);
            }
            catch (Exception ex)
            {
                log += string.Format("Exception at SendEmailNOtification.\r\nExcpetion Message: {0}\r\nStack Trace: {1}", ex.Message, ex.StackTrace);
            }

        }

        public static void SendEmail(string strMessage)
        {
            MailAddress from = new MailAddress(ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.from"]);
            string[] toAddress = ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.to"].Split(';');
            foreach (string emailTo in toAddress)
            {
                MailAddress to = new MailAddress(emailTo);
                MailMessage message = new MailMessage(from, to);
                message.Subject = ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.subject"] + string.Format("{0}/{1}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString());
                message.Body = strMessage;

                SmtpClient client = new SmtpClient();
                client.Host = ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.server"];
                client.Port = Int32.Parse(ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.server.port"]);
                NetworkCredential credentials = new NetworkCredential(
                    ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.server.user.name"],
                    ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.server.password"]);
                client.Credentials = credentials;

                try
                {
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    log += string.Format("Error on sending notification email.\r\nExcpetion Message: {0}\r\nStack Trace: {1}", ex.Message, ex.StackTrace);
                }
            }
        }
    }
}