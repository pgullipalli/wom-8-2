﻿
using MedTouch.Calendar.Entities;
using MedTouch.Common.Helpers.Security.Encryption;
using MedTouch.Common.Helpers.Security.Encryption.Base;
using MedTouch.Common.Helpers.Utilities.Base;
using MedTouch.Calendar.Security;

namespace womans.org.BusinessLogic.Entities {
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// a) adds Calendar_EventRegistration to Interface (for project customization)
    /// b) handles encryption/decryption for specific fields 
    /// </summary>
  
    //TODO: figure out how to not copy this from Calendar module
    public partial class Calendar_EventRegistration : MedTouch.Calendar.Entities.Calendar_EventRegistration
    {

        private static readonly IEncryptorFactory Factory =
            EncryptorFactory.CreateNewEncryptorFactory(MTEncryptionSettings.Current);

        private static readonly IEncryptor Encryptor = Factory.CreateNewEncryptor();

        //public virtual string Firstname_Secure {
        //    get { return Decrypt(Firstname); }
        //    set { Firstname = Encrypt(value); }
        //}

        //public virtual string Lastname_Secure {
        //    get { return Decrypt(Lastname); }
        //    set { Lastname = Encrypt(value); }
        //}

        //public virtual string Address_Secure {
        //    get { return Decrypt(Address); }
        //    set { Address = Encrypt(value); }
        //}

        //public virtual string Address2_Secure {
        //    get { return Decrypt(Address2); }
        //    set { Address2 = Encrypt(value); }
        //}

        //public virtual string City_Secure {
        //    get { return Decrypt(City); }
        //    set { City = Encrypt(value); }
        //}

        //public virtual string State_Secure {
        //    get { return Decrypt(State); }
        //    set { State = Encrypt(value); }
        //}

        //public virtual string ZipCode_Secure {
        //    get { return Decrypt(ZipCode); }
        //    set { ZipCode = Encrypt(value); }
        //}

        //public virtual string PhoneNumber_Secure {
        //    get { return Decrypt(PhoneNumber); }
        //    set { PhoneNumber = Encrypt(value); }
        //}

        //public virtual string Email_Secure {
        //    get { return Decrypt(Email); }
        //    set { Email = Encrypt(value); }
        //}

        public virtual string PaymentTransactionID_Secure
        {
            get
            {
                return Decrypt(PaymentTransactionID);
            }
            set
            {
                PaymentTransactionID = Encrypt(value);
            }
        }

        //public virtual string PaymentAuthorizationCode_Secure {
        //    get { return Decrypt(PaymentAuthorizationCode); }
        //    set { PaymentAuthorizationCode = Encrypt(value); }
        //}

        public static string Encrypt(string value)
        {
            return Encryptor.Encrypt(value);
        }

        public static string Decrypt(string value)
        {
            return Encryptor.Decrypt(value);
        }
    }
}
