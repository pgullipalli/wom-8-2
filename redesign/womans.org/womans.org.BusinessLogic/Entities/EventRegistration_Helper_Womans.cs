﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.Pkcs;
using System.Web;
using MedTouch.Calendar.Entities;
using Sitecore.Diagnostics;
using womans.org.BusinessLogic.Entities;

namespace womans.org.BusinessLogic.Entities {

    public class EventRegistration_Helper_Womans : ICalendar_EventRegistration_Helper {

        /// <summary>
        /// Get list of all calendar event registrations in database
        /// </summary>
        /// <returns></returns>
        public virtual List<ICalendar_EventRegistration> GetEventRegistrations() {
            var result = new List<ICalendar_EventRegistration>();

            try {
                var dbCtx = new CustomEntities_Womans();
                var reglist = dbCtx.Calendar_EventRegistration.ToList();
                result.AddRange(reglist);
            }
            catch (Exception ex) {
                Log.Error("GetEventRegistrations (all) failed.", ex, HttpContext.Current.Session);

            }
            return result;
        }

        /// <summary>
        /// Get list of registrations for this event.
        /// </summary>
        /// <param name="eventSessionGuid"></param>
        /// <param name="customDBcontext"></param>
        /// <returns></returns>
        public virtual List<ICalendar_EventRegistration> GetEventRegistrations(Guid eventSessionGuid)
        {
            var result = new List<ICalendar_EventRegistration>();
            var dbCtx = new CustomEntities_Womans();

            try
            {
                var reglist = dbCtx.Calendar_EventRegistration.Where(i => i.SessionID.Equals(eventSessionGuid)).ToList();
                result.AddRange(reglist);
            }
            catch (Exception ex)
            {
                Log.Error("GetEventRegistrations (for session) failed.", ex, HttpContext.Current.Session);

            }
            return result;
        }

        /// <summary>
        /// Get a specific registrant (by transaction ID)
        /// </summary>
        /// <param name="registrationTID"></param>
        /// <param name="customDBcontext"></param>
        /// <returns></returns>
        public virtual ICalendar_EventRegistration GetRegistrationByTID(int registrationTID) {
            var dbCtx = new CustomEntities_Womans();
            try {
                return dbCtx.Calendar_EventRegistration.FirstOrDefault(i => i.TransactionID == registrationTID);
            }
            catch (Exception ex) {
                Log.Error("GetRegistrationByTID failed.", ex, HttpContext.Current.Session);
            }
            return null;
        }


        /// <summary>
        /// Get a specific registrant (by transaction ID), using a specific context (needed for updating records)
        /// </summary>
        /// <param name="registrationTID"></param>
        /// <param name="customDBcontext"></param>
        /// <returns></returns>
        public virtual ICalendar_EventRegistration GetRegistrationByTID(int registrationTID, out ICustomEntities dbCtx) {
            dbCtx = new CustomEntities_Womans();
            try {
                var womansCtx = dbCtx as CustomEntities_Womans;
                return womansCtx.Calendar_EventRegistration.FirstOrDefault(i => i.TransactionID == registrationTID);
            }
            catch (Exception ex) {
                Log.Error("GetRegistrationByTID failed.", ex, HttpContext.Current.Session);
            }
            return null;
        }

    }
}