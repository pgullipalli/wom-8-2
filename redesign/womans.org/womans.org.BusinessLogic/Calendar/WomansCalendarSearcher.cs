//using MedTouch.Common.Helpers.Module.SiteSearch;

namespace womans.org.BusinessLogic.Calendar
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using MedTouch.Calendar.Helpers;
    using MedTouch.Calendar.Search;
    using MedTouch.Common.Helpers;

    using Sitecore.Collections;
    using Sitecore.Data.Items;
    using Sitecore.Diagnostics;
    using Sitecore.Search;
    using Sitecore.Sharedsource.Data.Comparers.ItemComparers;
    using Sitecore.SharedSource.Search.Parameters;
    using Sitecore.SharedSource.Search.Utilities;

    public class WomansCalendarSearcher { //: INonSiteSearchItemRemover {


        // SURAT: This is the new faster topic filtering method
        // Just use new common lib & remove the comment on INonSiteSearchItemRemover
        /// <summary>
        /// INonSiteSearchItemRemover interface: Remove any topics "found" if they don't have sessions available from today, forward (or ongoing)
        /// </summary>
        /// <param name="items"></param>
        public virtual void FilterItems(IList<Item> items) {
            foreach (Item itm in items) {
                if (itm.TemplateName == ItemMapper.EventTopic.TemplateName) {
                    if (!ModuleHelper.HasFutureSessions(itm))
                        items.Remove(itm);
                }
            }
        }

        /// <summary>
        /// Assumption: Don't return any event that has start date prior to DateTime.Now.
        /// </summary>
        /// <param name="calendarSearchParam"></param>
        /// <returns></returns>
        public virtual List<Item> ExecuteSearch(CalendarSearchParam calendarSearchParam, bool showAsTopics = false)
        {
            List<SkinnyItem> datedSkinnyItems = null;
            List<SkinnyItem> ongoingSkinnyItems = null;
            List<Item> outItems = new List<Item>();
            List<Item> ongoingItems = new List<Item>();

            switch (calendarSearchParam.ListType)
            {
                // only outgoing events, include dated outgoing events
                case EventListType.Ongoing:
                    ongoingSkinnyItems = this.SearchOngoingEvents(calendarSearchParam, true);   
                    break;

                // typical: dated list
                default:
                    // first: get dated events
                    datedSkinnyItems = this.SearchDatedEvents(calendarSearchParam); 
                    // second: get outgoing events if included, but exclude dated events (already in other list)
                    if (calendarSearchParam.IncludeOngoing.HasValue && calendarSearchParam.IncludeOngoing.Value)
                    {
                        //ongoingSkinnyItems = SearchOngoingEvents(calendarSearchParam, false);
                        // 6/25 - TRY THIS: Include ALL ongoing events (dated or not at end of dated list)
                        ongoingSkinnyItems = this.SearchOngoingEvents(calendarSearchParam, true);   
                    }
                    break;
            }

            // Convert skinny item lists to sitecore Event Session item lists, selecting out "featured" events if needed
            if (datedSkinnyItems != null)
            {
                outItems = calendarSearchParam.IsFeatured
                    ? this.SelectFeaturedEvents(datedSkinnyItems)
                    : SearchHelper.GetItemListFromInformationCollection(datedSkinnyItems);
                outItems = this.SortEventSessionItemsByDate(outItems);
            }
            if (ongoingSkinnyItems != null)
            {
                ongoingItems = calendarSearchParam.IsFeatured
                    ? this.SelectFeaturedEvents(ongoingSkinnyItems)
                    : SearchHelper.GetItemListFromInformationCollection(ongoingSkinnyItems);
                ongoingItems = this.SortEventSessionItemsByHeadline(ongoingItems);
            }

            // 6/25: TRY THISConsolidate Event Session Item lists at this point: First Dated non-outgoing events, then ALL outgoing events that qualify
            if (ongoingItems.Any())
            {
                List<DateRangeSearchParam.DateRangeField> dateRangeFields = this.GetDateRanges(calendarSearchParam.DateFrom, calendarSearchParam.DateTo);
                // this will always be true...if empty date range, it will be today through +10 years
                if (dateRangeFields.Count >= 1) {
                    foreach (Item itm in ongoingItems.ToList()) {
                        if (!ModuleHelper.SessionExpired(itm, dateRangeFields[0].StartDate, dateRangeFields[0].EndDate))
                            outItems.Add(itm);
                    }
                }
            }

            // OLD: Consolidate Event Session Item lists (outItems already contains dated ongoing events)
            //if (outItems.Any())
            //{
            //    if (ongoingItems.Any())
            //        outItems.AddRange(ongoingItems);
            //}
            //else
            //{
            //    // If ongoing items only, need to remove any dated items where start/end not between date ranges
            //    List<DateRangeSearchParam.DateRangeField> dateRangeFields = GetDateRanges(calendarSearchParam.DateFrom, calendarSearchParam.DateTo);
            //    if (dateRangeFields.Count >= 1){
            //        foreach (Item itm in ongoingItems.ToList())
            //        {
            //            if (!SessionExpired(itm, dateRangeFields[0].StartDate, dateRangeFields[0].EndDate))
            //                outItems.Add(itm);
            //        }
            //    }
            //}


            // Distill into Topic list if necessary
            if (calendarSearchParam.SearchType == CalendarSearchType.EventTopic || showAsTopics)
                outItems = this.GetEventTopicItemsFromEventSessionItems(outItems);         

            return outItems;
        }

        /// <summary>
        /// Search for Events that DO have a start date, based on search criteria
        /// </summary>
        /// <param name="calendarSearchParam"></param>
        /// <returns></returns>
        protected virtual List<SkinnyItem> SearchDatedEvents(CalendarSearchParam calendarSearchParam)
        {
            // Handle all Search criteria by EventSession.
            string templateGuid = ModuleHelper.Config.ItemGuid.EventSessionTemplate;

            // Refinement Search Param
            SafeDictionary<string> refinements = this.GetCalendarSearchRefinement(calendarSearchParam);

            // Date Range Search Param
            List<DateRangeSearchParam.DateRangeField> dateRangeFields = this.GetDateRanges(
                calendarSearchParam.DateFrom, calendarSearchParam.DateTo);

            // Search
            List<SkinnyItem> skinnyItems = SitecoreSearchHelper.GetItems(
                MedTouch.Calendar.Helpers.Common.GetModuleIndexName(),
                Sitecore.Context.Language.CultureInfo.TwoLetterISOLanguageName,
                templateGuid,
                MedTouch.Calendar.Helpers.Common.GetCalendarSearchRootGuid(),
                string.Empty,
                string.Empty,
                refinements,
                dateRangeFields,
                QueryOccurance.Must);

            return skinnyItems;
        }

        /// <summary>
        /// Search for Ongoing Events only, if !bIncludeDated, includ only ongoing events with a null start date
        /// </summary>
        /// <param name="calendarSearchParam"></param>
        /// <param name="bIncludeDated"></param>
        /// <returns></returns>
        protected virtual List<SkinnyItem> SearchOngoingEvents(CalendarSearchParam calendarSearchParam, bool bIncludeDated) {
            // Handle all Search criteria by EventSession.
            string templateGuid = ModuleHelper.Config.ItemGuid.EventSessionTemplate;

            // Refinement Search Param
            SafeDictionary<string> refinements = this.GetCalendarSearchRefinement(calendarSearchParam);
            
            // Make sure ONLY ongoing events are selected (i.e., don't select non-dated non-ongoing events)
            if (refinements.ContainsKey("_calendar_eventsessionisongoing"))
                refinements["_calendar_eventsessionisongoing"] = "true";
            else 
                refinements.Add("_calendar_eventsessionisongoing", "true");

            // Exclude dated events, if specified
            if (!bIncludeDated)
                refinements.Add("_calendar_eventsessionisdated", "false");

            // Search
            List<SkinnyItem> skinnyItems = SitecoreSearchHelper.GetItems(
                MedTouch.Calendar.Helpers.Common.GetModuleIndexName(),
                Sitecore.Context.Language.CultureInfo.TwoLetterISOLanguageName,
                templateGuid,
                MedTouch.Calendar.Helpers.Common.GetCalendarSearchRootGuid(),
                string.Empty,
                refinements,
                QueryOccurance.Must);

            return skinnyItems;
        }

        /// <summary>
        /// After initial search performed, create Item list from skinny items by selecting out only the Featured items.
        /// Ugly code alert: 
        /// The initial calendar search always gets a list of SESSIONS (not TOPICS).
        /// We can't select "isfeatured" in the initial search, because an un-featured session needs to be included if it's parent TOPIC is "featured"
        /// So, here is where we weed out the non-featured sessions/topics.
        /// Goal is to include ALL sessions from the search where the session itself or it's topic is "featured"
        /// </summary>
        /// <param name="skinnyItems"></param>
        /// <returns></returns>
        protected virtual List<Item> SelectFeaturedEvents(List<SkinnyItem> skinnyItems)
        {
            if (skinnyItems == null)
                return null;

            List<Item> items = new List<Item>();
            List<Item> sessions =
                SearchHelper.GetItemListFromInformationCollection(skinnyItems)
                    .ToList()
                    .OrderBy(e => e.Parent.Paths.Path)
                    .ToList();
            string curParent = string.Empty;
            string parentFeatured = string.Empty;
            string isFeatured = string.Empty;
            foreach (Item session in sessions) {
                isFeatured = ItemHelper.GetFieldRawValue(session, MedTouch.Calendar.Helpers.ObjectModel.EventSession.FieldName.IsFeatured);
                if (isFeatured != "1") {
                    if (session.Parent.Paths.Path != curParent) {
                        curParent = session.Parent.Paths.Path;
                        parentFeatured = string.Empty;
                        var eventTopicItem = ModuleHelper.GetEventTopicFromEventSession(session);
                        if (eventTopicItem != null)
                            parentFeatured = ItemHelper.GetFieldRawValue(eventTopicItem, ItemMapper.EventTopic.FieldName.IsFeatured);
                    }
                    if (parentFeatured != "1")
                        continue;
                }
                items.Add(session);
            }
            return items;
        }


        /// <summary>
        /// Create Calendar session event search criteria
        /// </summary>
        /// <param name="calendarSearchParam"></param>
        /// <returns></returns>
        protected virtual SafeDictionary<string> GetCalendarSearchRefinement(CalendarSearchParam calendarSearchParam)
        {
            var refinements = new SafeDictionary<string>();

            // Search keyword base on custom index field below.
            if (!string.IsNullOrEmpty(calendarSearchParam.Keyword))
            {
                refinements.Add("_calendar_eventtopicdata", calendarSearchParam.Keyword);
            }

            // Specialties tagging search
            if (!string.IsNullOrEmpty(calendarSearchParam.Specialties))
            {
                string specialties = IdHelper.ProcessGUIDs(calendarSearchParam.Specialties);
                refinements.Add("_calendar_eventtopicspecialtiestag", specialties);
            }

            // Locations tagging search
            if (!string.IsNullOrEmpty(calendarSearchParam.Locations))
            {
                string locations = IdHelper.ProcessGUIDs(calendarSearchParam.Locations);
                refinements.Add("_calendar_eventtopiclocationstag", locations);
            }

            // Taxonomy Tagging
            if (!string.IsNullOrEmpty(calendarSearchParam.Taxonomy))
            {
                string taxonoyGuids = IdHelper.ProcessGUIDs(calendarSearchParam.Taxonomy);
                refinements.Add("_calendar_eventtopictaxonomytermstag", taxonoyGuids);
            }

            // Event Category
            if (!string.IsNullOrEmpty(calendarSearchParam.Category))
            {
                string strCategoryGuids = ItemHelper.GetItemGuidFromItemName(
                    calendarSearchParam.Category,
                    ModuleHelper.Config.ItemGuid.EventCategoryFolder,
                    ModuleHelper.Config.ItemGuid.EventCategoryTemplate);

                refinements.Add("_calendar_eventsessioncategory", strCategoryGuids);
            }

            // Event Instructor
            if (!string.IsNullOrEmpty(calendarSearchParam.Instructor)) {
                string strInstructorGuids = ItemHelper.GetItemGuidFromItemName(
                    calendarSearchParam.Instructor,
                    ModuleHelper.Config.ItemGuid.EventInstructorFolder,
                    ModuleHelper.Config.ItemGuid.EventInstructorTemplate);

                refinements.Add("_calendar_eventsessioninstructor", strInstructorGuids);
            }

            // Ongoing (or not) events only
            if (calendarSearchParam.ListType == EventListType.Ongoing)
                refinements.Add("_calendar_eventsessionisongoing", "true");
            //else if (calendarSearchParam.IncludeOngoing.HasValue && !calendarSearchParam.IncludeOngoing.Value)
            //    refinements.Add("_calendar_eventsessionisongoing", "false");
            // else: we want to include all types of events in this search, so no refinement based on IsOngoing field
            // 6/25 - TRY THIS: No ongoing events in the dated list... if "includeongoing" for EventListType.Dated, list ALL ongoing (dated or not) 
            // at the end of the list - don't merge ongoing dated events into dated list because the date range selection doesn't respect end date
            else
                refinements.Add("_calendar_eventsessionisongoing", "false");

            return refinements;
        }

        /// <summary>
        /// Build topic list from session list
        /// </summary>
        /// <param name="eventSessionItems"></param>
        /// <returns></returns>
        internal virtual List<Item> GetEventTopicItemsFromEventSessionItems(List<Item> eventSessionItems)
        {
            var resultItems = new List<Item>();
            foreach (var eventSessionItem in eventSessionItems)
            {
                var eventTopicItem = ModuleHelper.GetEventTopicFromEventSession(eventSessionItem);

                // Create non duplicate list of Event Topic, the order is based on Event sesseion item list that sort by most recent start date.
                if (eventTopicItem != null && !resultItems.Exists(item => item.ID == eventTopicItem.ID))
                {
                    resultItems.Add(eventTopicItem);
                }
            }
            return resultItems;
        }

        /// <summary>
        /// Sort by start date: dated events
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        protected virtual List<Item> SortEventSessionItemsByDate(List<Item> items)
        {
            if (items == null || !items.Any())
            {
                return items;
            }
            items = ItemHelper.RemoveNullItemsFromList(items);
            var comparer = new FieldValueComparer(MedTouch.Calendar.Helpers.ObjectModel.EventSession.FieldName.SessionStartDate);
            items.Sort(comparer);

            return items;
        }

        /// <summary>
        /// Sort by topic headline: ongoing events
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        protected virtual List<Item> SortEventSessionItemsByHeadline(List<Item> items) {
            if (items == null || !items.Any()) {
                return items;
            }

			items = ItemHelper.RemoveNullItemsFromList(items);
            var comparer = new SessionHeadlineComparer();
            items.Sort(comparer);

            return items;
        }


        //----------------------- MISC CALENDAR SEARCH INDEX SUPPORT --------------------------------------------------------------------------------------------

        public class SessionHeadlineComparer : Sitecore.Data.Comparers.Comparer
        {
            protected override int DoCompare(Item item1, Item item2) {
                Assert.IsNotNull(item1, "invalid session item for headline sorting");
                Assert.IsNotNull(item2, "invalid session item for headline sorting");
                Item topic1 = ModuleHelper.GetEventTopicFromEventSession(item1);
                Item topic2 = ModuleHelper.GetEventTopicFromEventSession(item2);
                if (topic1.ID.ToString().Equals(topic2.ID.ToString()))
                    return 0;
                return string.Compare(ItemHelper.GetFieldRawValue(topic1, ItemMapper.EventTopic.FieldName.Headline),
                    ItemHelper.GetFieldRawValue(topic2, ItemMapper.EventTopic.FieldName.Headline),
                    StringComparison.CurrentCultureIgnoreCase);
            }
        }

        /// <summary>
        /// If dateFrom invalid or prior to DateTime.Now, use dateFrom = DateTime.Now.
        /// If DateTo invalid, assume DateTo + 10 years.
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        protected virtual List<DateRangeSearchParam.DateRangeField> GetDateRanges(string dateFrom, string dateTo) {
            var dateRanges = new List<DateRangeSearchParam.DateRangeField>();

            // DateFrom
            DateTime dtDateFrom;
            if (!DateTime.TryParseExact(dateFrom, ModuleHelper.Config.DefaultDateFormat(), new CultureInfo("en-US"), DateTimeStyles.None, out dtDateFrom)) {
                dtDateFrom = DateTime.Today;
            }

            if (dtDateFrom < DateTime.Today) {
                dtDateFrom = DateTime.Today;
            }

            // DateTo. Limit to next 10 years
            DateTime dtDateTo;
            if (!DateTime.TryParseExact(dateTo, ModuleHelper.Config.DefaultDateFormat(), new CultureInfo("en-US"), DateTimeStyles.None, out dtDateTo)) {
                dtDateTo = DateTime.Today.AddYears(10);
            }

            dateRanges.Add(
                new DateRangeSearchParam.DateRangeField(
                    MedTouch.Calendar.Helpers.ObjectModel.EventSession.FieldName.SessionStartDate, dtDateFrom, dtDateTo));
            // TODO: Need to change sitecore.search to support an OR between 2 date ranges
            // So... we need start date between (now & 10 years from now) OR end date between (now & 10 years from now... if it's non-null)
            // UGH!!
            //dateRanges.Add(new DateRangeSearchParam.DateRangeField(
            //        Helpers.ObjectModel.EventSession.FieldName.SessionEndDate, dtDateFrom, dtDateTo));

            return dateRanges;
        }

        /// <summary>
        /// For EventTopicOfferDates: Get Event Session Items that Session Start Date haven't pass System.DateTime.Now yet.
        /// Includes non-dated ongoing events at end of list 
        /// </summary>
        /// <param name="eventTopicGuid"></param>
        /// <returns></returns>
        public virtual List<Item> GetEventSessionItems(string eventTopicGuid) {

            List<Item> outItems = new List<Item>();
            // Get dated events for this topic - default range search = Now to 10 yrs. from now
            List<DateRangeSearchParam.DateRangeField> dateRangeFields = this.GetDateRanges(string.Empty, string.Empty);
            List<SkinnyItem> eventSessionSkinnyItems = SitecoreSearchHelper.GetItems(
                MedTouch.Calendar.Helpers.Common.GetModuleIndexName(),
                Sitecore.Context.Language.CultureInfo.TwoLetterISOLanguageName,
                ModuleHelper.Config.ItemGuid.EventSessionTemplate,
                eventTopicGuid,
                string.Empty,
                dateRangeFields);
            if (eventSessionSkinnyItems.Any())
            {
                outItems = SearchHelper.GetItemListFromInformationCollection(eventSessionSkinnyItems);
                outItems = this.SortEventSessionItemsByDate(outItems);
            }

            // Add undated ongoing sessions
            var refinements = new SafeDictionary<string>();
            refinements.Add("_calendar_eventsessionisongoing", "true");
            refinements.Add("_calendar_eventsessionisdated", "false");
            List<SkinnyItem> ongoingNondatedSkinnyItems = SitecoreSearchHelper.GetItems(
                MedTouch.Calendar.Helpers.Common.GetModuleIndexName(),
                Sitecore.Context.Language.CultureInfo.TwoLetterISOLanguageName,
                ModuleHelper.Config.ItemGuid.EventSessionTemplate,
                eventTopicGuid,
                string.Empty,
                refinements,
                QueryOccurance.Must);
            if (ongoingNondatedSkinnyItems.Any())
            {
                outItems.AddRange(SearchHelper.GetItemListFromInformationCollection(ongoingNondatedSkinnyItems));
            }

            return outItems;
        }

        /// <summary>
        /// Get Category Items that has Event Topic item associated.
        /// </summary>
        /// <param name="categoryFolderGuid"></param>
        /// <returns></returns>
        public virtual List<Item> GetAssociatedCategoryItems(string categoryFolderGuid) {
            var skinnyItems = new List<SkinnyItem>();

            // Get EventCategory Items
            List<SkinnyItem> categorySkinnyItems = SitecoreSearchHelper.GetItems(
                MedTouch.Calendar.Helpers.Common.GetModuleIndexName(),
                Sitecore.Context.Language.CultureInfo.TwoLetterISOLanguageName,
                ModuleHelper.Config.ItemGuid.EventCategoryTemplate,
                categoryFolderGuid,
                string.Empty);

            foreach (var categorySkinnyItem in categorySkinnyItems) {
                // Search Event base on System.DateTime.Now as start date to find if any category has any associate event. 
                // Don't include event that start date time already passed.
                var eventSearchParam = new CalendarSearchParam() {
                    Keyword = string.Empty,
                    Category = categorySkinnyItem.GetItem().Name,
                    DateFrom = System.DateTime.Now.ToString(ModuleHelper.Config.DefaultDateFormat()),
                    DateTo = string.Empty,
                    SearchType = CalendarSearchType.EventSession,
                    IncludeOngoing = true
                };

                var calendarSearcher = new CalendarSearcher();
                List<Item> associatedEventSkinnyItems = calendarSearcher.ExecuteSearch(eventSearchParam);

                // Add associated category into result List
                if (associatedEventSkinnyItems.Any()) {
                    skinnyItems.Add(categorySkinnyItem);
                }
            }

            List<Item> resultItems = SearchHelper.GetItemListFromInformationCollection(skinnyItems);

            // Sort Category Items
            var comparer = new FieldValueComparer(ItemMapper.EventCategory.FieldName.CategoryName);
            resultItems.Sort(comparer);

            return resultItems;
        }

    }
}