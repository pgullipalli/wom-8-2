﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.BusinessLogic.Calendar
{
    using System.Configuration;
    using System.Data.SqlClient;

    using MedTouch.Calendar.Entities;

    public class WomansRegistrationHelper
    {

        public string GetInternalNotes(int transactionId)
        {
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["customDB"].ConnectionString))
            {
                con.Open();
                using (
                    var cmd =
                        new SqlCommand(
                            "SELECT InternalNotes FROM Calendar_EventRegistration WHERE TransactionID='" + transactionId
                            + "'", con))
                {
                    var encryptedValue = cmd.ExecuteScalar().ToString();
                    return Calendar_EventRegistration.Decrypt(encryptedValue);
                }
            }
        }

        public void SetInternalNotes(int transactionId, string notes)
        {
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["customDB"].ConnectionString))
            {
                con.Open();
                using (
                    var cmd =
                        new SqlCommand(
                            string.Format("UPDATE Calendar_EventRegistration SET InternalNotes='{0}' WHERE TransactionID='{1}'", Calendar_EventRegistration.Encrypt(notes), transactionId), con))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
