﻿using System;

namespace womans.org.BusinessLogic.Calendar
{
    using System.Collections.Generic;

    using MedTouch.Calendar.Entities;
    using MedTouch.Calendar.Helpers;
    using MedTouch.Calendar.Helpers.ObjectModel;
    using MedTouch.Common.Helpers;

    using Sitecore;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Diagnostics;
    using Sitecore.Sharedsource.Data.Comparers.ItemComparers;
    using Sitecore.Web.UI.HtmlControls;
    using Sitecore.Web.UI.Pages;
    using Sitecore.Web.UI.Sheer;

    public class WomansRegistrationMoveControl : DialogForm
    {
        protected Edit txtInternalNotes;
        protected Literal ltStatus;
        protected Literal ltFirstName;
        protected Literal ltLastName;
        private Literal ltTransactionID;
        private Combobox cmbSession;
        private string _RegistrationTID;
        private ICalendar_EventRegistration _EventRegistration;
        private ICustomEntities _CustomDBcontext;

        protected string RegistrationTID
        {
            get
            {
                if (this._RegistrationTID == null)
                    this._RegistrationTID = Context.Request.QueryString["RegistrationTID"];
                return this._RegistrationTID;
            }
        }

        protected ICalendar_EventRegistration EventRegistration
        {
            get
            {
                if (this._EventRegistration == null)
                {
                    int result;
                    int.TryParse(this.RegistrationTID, out result);
                    this._EventRegistration = ModuleHelper.CustomRegistrationHelper.GetRegistrationByTID(result, out this._CustomDBcontext);
                }
                return this._EventRegistration;
            }
        }

        protected ICustomEntities CustomDBcontext
        {
            get
            {
                return this._CustomDBcontext;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (Context.ClientPage.IsEvent || string.IsNullOrEmpty(this.RegistrationTID))
                return;
            this.UpdateMoveUIFromRegistrationItem(this.EventRegistration);
            this.FillMoveToSessions();
        }


        protected void UpdateMoveUIFromRegistrationItem(ICalendar_EventRegistration eventRegistration)
        {
            ltStatus.Text = MedTouch.Calendar.Helpers.ObjectModel.EventRegistrationStatus.GetStatusString(new Guid?(eventRegistration.RegistrationStatus));
            ltFirstName.Text = eventRegistration.Firstname_Secure;
            ltLastName.Text = eventRegistration.Lastname_Secure;
            var regHelper = new WomansRegistrationHelper();
            this.txtInternalNotes.Value = regHelper.GetInternalNotes(eventRegistration.TransactionID);
        }

        protected override void OnOK(object sender, EventArgs args)
        {
            try
            {
                var regHelper = new WomansRegistrationHelper();
                int result;
                int.TryParse(this.RegistrationTID, out result);
                regHelper.SetInternalNotes(result, this.txtInternalNotes.Value);
                ID gResult;
                if (!ID.TryParse(this.cmbSession.Value, out gResult))
                    return;
                if (gResult.Guid.Equals(this.EventRegistration.SessionID))
                    SheerResponse.Alert("Same session selected. Registration was not moved.");
                else if (RegistrationHelper.CheckRegistrationCapacity(result.ToString()) != eEventCapacity.EnrollmentAvailable)
                {
                    SheerResponse.Alert("Selected session is full. Registration was not moved.");
                }
                else
                {
                    this.EventRegistration.SessionID = gResult.Guid;
                    this.EventRegistration.Updated = DateTime.Now;
                    this.CustomDBcontext.SaveChanges();
                    SheerResponse.Alert("Registration moved to new session.");
                    SheerResponse.SetDialogValue("yes");
                    base.OnOK(sender, args);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Unexpected error attempting to move registrant.", (object)ex);
                throw;
            }
        }

        protected void FillMoveToSessions()
        {
            this.cmbSession.Controls.Clear();
            try
            {
                List<Item> availableSessions = ModuleHelper.GetTopicAvailableSessions(this.EventRegistration.TopicID.ToString());
                FieldValueComparer fieldValueComparer = new FieldValueComparer(EventSession.FieldName.SessionStartDate);
                availableSessions.Sort((IComparer<Item>)fieldValueComparer);
                if (availableSessions.Count <= 0)
                    return;
                foreach (Item sessionItem in availableSessions)
                {
                    string sessionDateTimeString = RegistrationHelper.GetSessionDateTimeString(sessionItem, true, "", "");
                    string fieldRawValue = ItemHelper.GetFieldRawValue(sessionItem, EventSession.FieldName.AttendingDatesInformation);
                    if (!string.IsNullOrEmpty(fieldRawValue))
                        sessionDateTimeString += string.IsNullOrEmpty(sessionDateTimeString) ? fieldRawValue : ": " + fieldRawValue;
                    ListItem listItem1 = new ListItem();
                    listItem1.ID = Sitecore.Web.UI.HtmlControls.Control.GetUniqueID("ListItem");
                    listItem1.Header = sessionDateTimeString;
                    listItem1.Value = sessionItem.ID.ToString();
                    ListItem listItem2 = listItem1;
                    if (sessionItem.ID.Guid.Equals(this.EventRegistration.SessionID))
                        listItem2.Selected = true;
                    this.cmbSession.Controls.Add((System.Web.UI.Control)listItem2);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error determining available move-to sessions", (object)ex);
                throw;
            }
        }
    }
}
