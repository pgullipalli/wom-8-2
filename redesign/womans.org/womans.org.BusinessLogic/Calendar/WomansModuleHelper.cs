﻿
#if PREMIUM
using MedTouch.Calendar.Helpers.ShoppingCart;
#endif

namespace womans.org.BusinessLogic.Calendar
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;

    using MedTouch.Calendar.Entities;
    using MedTouch.Calendar.Helpers;
    using MedTouch.Calendar.Helpers.ObjectModel;
    using MedTouch.Calendar.Search;
    using MedTouch.Common.Helpers;
    using MedTouch.Common.Helpers.Logging;
    using MedTouch.Common.Helpers.Logging.Base;

    using Sitecore;
    using Sitecore.Collections;
    using Sitecore.Configuration;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Diagnostics;
    using Sitecore.Web;

    using Convert = System.Convert;

    public sealed class WomansModuleHelper
    {
        private static ILogger logger = Logger.Create();

        #region Config

        public class Config
        {
            public static bool Debug()
            {
                return Settings.GetBoolSetting("MedTouch.Calendar.Debug", false);
            }

            public static int FilterCacheLifeTime
            {
                get
                {
                    int intLifeTime;
                    string strLifetime = Settings.GetSetting("MedTouch.Calendar.FilterCacheLifeTime", "0");
                    return Int32.TryParse(strLifetime, out intLifeTime) ? intLifeTime : 0;
                }
            }

            /// <summary>
            /// Default DateFormat in QueryString of DateTime Value
            /// </summary>
            /// <returns></returns>
            public static string DefaultDateFormat()
            {
                return Settings.GetSetting("MedTouch.Calendar.DateTimeFormatQuerystring", "yyyyMMddTHHmmss");
            }

            public class ItemGuid
            {
                #region Template

                public static string EventSessionTemplate
                {
                    get { return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Event Session Template"); }
                }

                public static string EventSessionFolderTemplate
                {
                    get { return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Event Session Folder Template"); }
                }

                public static string EventTopicTemplate
                {
                    get { return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Event Topic Template"); }
                }

                public static string EventCategoryTemplate
                {
                    get { return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Event Category Template"); }
                }

                public static string EventInstructorTemplate {
                    get { return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Event Instructor Template"); }
                }

                public static string EventLocationTemplate
                {
                    get { return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Event Location Template"); }
                }

                #endregion Template

                #region Folder

                public static string CalendarFolders
                {
                    get { return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Calendar Folder"); }
                }

                public static string EventCategoryFolder
                {
                    get { return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Event Category Folder"); }
                }

                public static string EventInstructorFolder {
                    get { return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Event Instructor Folder"); }
                }

                public static string EventLocationFolder
                {
                    get { return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Event Location Folder"); }
                }

                public static string PromoCodeFolder {
                    get { return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Promo Code Folder"); }
                }

                public static string EventRegistrationStatusFolder {
                    get
                    {
                        Database db = CalendarGetCurrentDatabase();
                        return ItemHelper.GetFieldRawValue(Common.GetModuleItem(db), "Event Registration Status Folder");
                    }
                }

                public static string AutoEmailFolder {
                    get
                    {
                        Database db = Factory.GetDatabase("master");
                        return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Auto Email Folder");
                    }
                }

                public static string PaymentMethodFolder {
                    get { return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Payment Method Folder"); }
                }

                #endregion Folder
            }

            public static class QueryString
            {
                /// <summary>
                /// Get Query string from "Category Querystring" field of Module Setting
                /// </summary>
                /// <returns></returns>
                public static string GetCategoryQuerystring() {
                    return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Category Querystring");
                }

                /// <summary>
                /// Get Query string from "Instructor Querystring" field of Module Setting
                /// </summary>
                /// <returns></returns>
                public static string GetInstructorQuerystring() {
                    return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Instructor Querystring");
                }

                /// <summary>
                /// Get Query string from "DateFrom Querystring" field of Module Setting
                /// </summary>
                /// <returns></returns>
                public static string GetDateFromQuerystring()
                {
                    return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "DateFrom Querystring");
                }

                /// <summary>
                /// Get Query string from "DateTo Querystring" field of Module Setting
                /// </summary>
                /// <returns></returns>
                public static string GetDateToQuerystring()
                {
                    return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "DateTo Querystring");
                }

                /// <summary>
                /// Get Query string from "Event Session ID Querystring" field of Module Setting
                /// </summary>
                /// <returns></returns>
                public static string GetEventSessionIDQuerystring()
                {
                    return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Event Session ID Querystring");
                }

                /// <summary>
                /// Get Query string from "Payment Method ID Querystring" field of Module Setting
                /// </summary>
                /// <returns></returns>
                public static string GetPaymentMethodIDQuerystring() {
                    return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Payment Method ID Querystring");
                }

                /// <summary>
                /// Get Query string from "Event Session ID Querystring" field of Module Setting
                /// </summary>
                /// <returns></returns>
                public static string GetEventSessionKeyQuerystring()
                {
                    return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Event Session Key Querystring");
                }

                /// <summary>
                /// Get Query string from "Keyword Querystring" field of Module Setting
                /// </summary>
                /// <returns></returns>
                public static string GetKeywordQuerystring()
                {
                    return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Keyword Querystring");
                }

                /// <summary>
                /// Get Query string from "Page Querystring" field of Module Setting
                /// </summary>
                /// <returns></returns>
                public static string GetPageQuerystring()
                {
                    return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Page Querystring");
                }

                /// <summary>
                /// Get Query string from "Include Ongoing Querystring" field of Module Setting
                /// 1 = include ongoing events
                /// </summary>
                /// <returns></returns>
                public static string GetIncludeOngoingQuerystring() {
                    return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Include Ongoing Querystring");
                }

                /// <summary>
                /// Get Query string from "Event Type" field of Module Setting
                /// value = "ongoing": MedTouch.Calendar.Search.EventListType.Ongoing
                /// any other value: MedTouch.Calendar.Search.EventListType.Dated
                /// </summary>
                /// <returns></returns>
                public static string GetEventTypeQuerystring() {
                    return ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Event Type Querystring");
                }
            }

            public static class ModuleSettings
            {

                public static string EventRosterPage {
                    get {
                        Database db = Factory.GetDatabase("master");
                        return ItemHelper.GetFieldRawValue(Common.GetModuleItem(db), "Event Roster Page");
                    }
                }

                /// <summary>
                /// Get Max # shopping cart items
                /// </summary>
                
                public static int MaximumShoppingCartItems
                {
                    get
                    {
                        string maxItemsStr = ItemHelper.GetFieldRawValue(Common.GetModuleItem(), "Maximum Shopping Cart Items");
                        if (!String.IsNullOrEmpty(maxItemsStr))
                        {
                            int maxItems;
                            Int32.TryParse(maxItemsStr, out maxItems);
                            return maxItems;
                        }
                        return 999; // allow any number of items
                    }
                }
              
            }

        }

        #endregion Config

        public static ICalendar_EventRegistration_Helper CustomRegistrationHelper {
            get {
                var helper = Factory.CreateObject("calendarEventRegistrationHelperFactory", false) as ICalendar_EventRegistration_Helper;
                Assert.IsNotNull(helper, "calendarEventRegistrationHelper factory must be set in config file");
                return helper;
            }
        }

#if PREMIUM
        /// <summary>
        /// Gets or creates the current shopping cart.
        /// Default cart = Cart (see Cart.cs); Projects can implement instances of ICart and configure factory in MedTouch.Calendar.[Project].config
        /// (see woman's project for example)
        /// </summary>
        public static ICart ShoppingCart {
            get {
                IStorageMedium storageMedium = SessionStorageMedium.Create(HttpContext.Current.Session);
                var cart = storageMedium.Get<ICart>("CalendarShoppingCart");
                if (cart == null)
                {
                    cart = Factory.CreateObject("cartFactory", false) as ICart;
                    Assert.IsNotNull(cart, "Cart factory must be set in config file");
                    storageMedium.Save("CalendarShoppingCart", cart);
                }
                return cart;
            }
            set
            {
                IStorageMedium storageMedium = SessionStorageMedium.Create(HttpContext.Current.Session);
                storageMedium.Save("CalendarShoppingCart", value);
            }
        }
#endif

        /// <summary>
        /// Add a string to a list of strings, if the new string is unique
        /// (for promo code list management)
        /// </summary>
        /// <param name="curCodes"></param>
        /// <param name="addCode"></param>
        public static void AddUniqueCode(List<string> curCodes, string addCode) {
            if (!String.IsNullOrEmpty(addCode) && !curCodes.Contains(addCode))
                curCodes.Add(addCode);
        }

        public static string TruncateString(string value, int maxLength) {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static Database CalendarGetCurrentDatabase()
        {
            Database db = (Context.ContentDatabase ?? Context.Database);
            // returns null when running tasks (i.e, sendreminders)
            if (db == null){
                db = Factory.GetDatabase("master");                
            }
            return db;
        }

        public static Item CalendarGetStartItem()
        {
            Database db = CalendarGetCurrentDatabase();
            return (db.GetItem(Context.Site.StartPath));
        }

        /// <summary>
        /// Setup SendMail SMTP client for non-form based email sending out of Calendar module
        /// </summary>
        /// <returns></returns>
        public static SmtpClient CalendarSMTPClient() {
            try {
                var client = new SmtpClient(Settings.GetSetting("MailServer"));
                client.Port = Convert.ToInt16(Settings.GetSetting("MailServerPort"));
                client.Credentials =
                    new NetworkCredential(Settings.GetSetting("MailServerUserName"),
                        Settings.GetSetting("MailServerPassword"));
                // Note: client.DeliveryMethod auto-setup from web.config (see <system.net>, <mailSettings>, <smtp deliveryMethod>
                return client;
            }
            catch (Exception ex) {
                logger.Error("Error setting up Mail SMTP Client", ex, HttpContext.Current.Session);
                throw ex;
            }
        }

        public static bool SendAutoEmail(Item autoEmailItem, ICalendar_EventRegistration registration)
        {

            Assert.IsNotNull(autoEmailItem, "Calendar auto email message: autoEmail item null");

            MailMessage email = new MailMessage();
            Assert.IsNotNull(email, "Calendar auto email: MailMessage");

            SmtpClient smtpClient = CalendarSMTPClient();
            Assert.IsNotNull(smtpClient, "Calendar auto email: SMTPClient");

            var sessionItem = Client.ContentDatabase.GetItem(registration.SessionID.ToString());
            Assert.IsNotNull(sessionItem, "Calendar auto email: invalid session ID");
            var topicItem = GetEventTopicFromEventSession(sessionItem);

            var siteStartItem = CalendarGetStartItem();
            Assert.IsNotNull(siteStartItem, "Send auto-email: site start item null.");

            // Setup basic mail message from autoemail content
            email.Body = FormHelper.ExpandCustomTokens(ItemHelper.GetFieldRawValue(autoEmailItem, ItemMapper.AutoEmail.FieldName.EmailBody), topicItem, sessionItem, siteStartItem);
            email.IsBodyHtml = true;
            email.Subject = FormHelper.ExpandCustomTokens(ItemHelper.GetFieldRawValue(autoEmailItem, ItemMapper.AutoEmail.FieldName.EmailSubject), topicItem, sessionItem, siteStartItem);
            email.From = new MailAddress(ItemHelper.GetFieldRawValue(siteStartItem, ItemMapper.CalendarModuleSiteSettings.FieldName.AutoEmailFromAddress));
            email.To.Add(new MailAddress(registration.Email_Secure));
            if (string.IsNullOrEmpty(email.Body) || string.IsNullOrEmpty(email.Subject) || string.IsNullOrEmpty(email.From.Address) || string.IsNullOrEmpty(registration.Email_Secure))
            {
                logger.Error("Invalid Auto Email: Email Subject or Body is empty OR Calendar setting for 'auto email from address' has not been setup", HttpContext.Current.Session);
                return false;
            }

            try
            {
                smtpClient.Send(email);
            }
            catch (Exception ex)
            {
                Log.Error("Send Auto Email Failed.", ex, HttpContext.Current.Session);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get Category Items that has Event Topic item associated.
        /// </summary>
        /// <param name="categoryFolderGuid">If this is blank, it will use Event Category Folder GUID from Module Setting as reference.</param>
        /// <returns></returns>
        public static List<Item> GetAssociatedCategoryItems(string categoryFolderGuid)
        {
            if (String.IsNullOrEmpty(categoryFolderGuid))
            {
                categoryFolderGuid = Config.ItemGuid.EventCategoryFolder;
            }

            var calendarSearcher = new CalendarSearcher();

            string strCacheKey = String.Concat(
                "cache_ce_GetAssociatedCategoryItems",
                categoryFolderGuid,
                Common.GetModuleIndexName(),
                Context.Language.CultureInfo.TwoLetterISOLanguageName,
                Common.GetCalendarSearchRootGuid(),
                ItemHelper.GetStartItemGuid());

            var resultItems = new List<Item>();

            if (!CacheHelper.Get(strCacheKey, out resultItems))
            {
                resultItems = calendarSearcher.GetAssociatedCategoryItems(categoryFolderGuid);

                if (Config.FilterCacheLifeTime != 0)
                {
                    CacheHelper.Add(resultItems, strCacheKey, Config.FilterCacheLifeTime);
                }
            }

            return resultItems;
        }

        /// <summary>
        /// Get Sitecore date field as a nullable DateTime since start/end dates can be null
        /// </summary>
        /// <param name="itm"></param>
        /// <param name="fldname"></param>
        /// <returns></returns>
        public static DateTime? GetSitecoreDateField(Item itm, string fldname) {
            DateTime? outDate = null;
            if (itm != null) {
                string strDate = ItemHelper.GetFieldRawValue(itm, fldname);
                if (!String.IsNullOrEmpty(strDate))
                    outDate = ItemHelper.GetDateFromSitecoreIsoDate(strDate);
            }
            return outDate;
        }

        /// <summary>
        /// Get Date Picker input format
        /// </summary>
        /// <param name="dateArg"></param>
        /// <returns></returns>
        public static string GetDatePickerInputFormat(string dateArg)
        {
            if (String.IsNullOrEmpty(dateArg)) return String.Empty;

            string result = String.Empty;

            DateTime dtDateTime;
            if (DateTime.TryParseExact(dateArg, Config.DefaultDateFormat(), new CultureInfo("en-US"),
                DateTimeStyles.None, out dtDateTime))
            {
                string dateFormat = Settings.GetSetting("MedTouch.Calendar.DatePickerInputFormat", "MM/dd/yyyy");
                result = dtDateTime.ToString(dateFormat);
            }

            return result;
        }

        public static string GetEventTopicLinkUrl(Item eventTopic, Item eventSession)
        {
            if (eventTopic == null || eventSession == null) return String.Empty;

            return String.Format("{0}?{1}={2}", ItemHelper.GetItemUrl(eventTopic),
                Config.QueryString.GetEventSessionKeyQuerystring(), StringHelper.EncodeString(eventSession.Key));
        }

        /// <summary>
        /// Get Event Topic From Event Session
        /// </summary>
        /// <param name="eventSession"></param>
        /// <returns></returns>
        public static Item GetEventTopicFromEventSession(Item eventSession)
        {
            return ItemHelper.GetFirstAncestorByTemplate(eventSession, ItemMapper.EventTopic.TemplateName);
        }

        /// <summary>
        /// Get Event Fee from Event Session by Short ID
        /// </summary>
        /// <param name="shortID"></param>
        /// <returns></returns>
        public static string GetEventFeeFromEventSession(string shortID)
        {
            Item eventSessionItem = ItemHelper.GetItemFromShortID(shortID);
            if (eventSessionItem == null) return String.Empty;

            Item eventTopicItem = GetEventTopicFromEventSession(eventSessionItem);
            if (eventTopicItem == null) return String.Empty;                // Fee from Event Topic

            decimal fee = 0;
            string feeString = ItemHelper.GetFieldRawValue(eventTopicItem, ItemMapper.EventTopic.FieldName.Fee);
            if (decimal.TryParse(feeString, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out fee))
                return string.Format("{0:c}", fee);
            return feeString;
        }

        /// <summary>
        /// Determine if specific session has expired:
        /// (a) start/end date range before today
        /// (b) else: isongoing
        ///  </summary>
        /// <param name="sessionItem"></param>
        /// <param name="EarliestDate"></param>
        /// <param name="LatestDate"></param>
        /// <returns></returns>
        public static bool SessionExpired(Item sessionItem, DateTime EarliestDate, DateTime LatestDate) {
            if (sessionItem != null) {
                DateTime? startDate = GetSitecoreDateField(sessionItem, MedTouch.Calendar.Helpers.ObjectModel.EventSession.FieldName.SessionStartDate);
                if (startDate.HasValue) {
                    DateTime? endDate = GetSitecoreDateField(sessionItem, MedTouch.Calendar.Helpers.ObjectModel.EventSession.FieldName.SessionEndDate);
                    if (endDate.HasValue) {
                        if (endDate.Value.CompareTo(EarliestDate) < 0 || startDate.Value.CompareTo(LatestDate) > 0)
                            return true; // end date exists - session either ends before search start date or starts after search end date
                    }
                    else if (startDate.Value.CompareTo(EarliestDate) < 0 || startDate.Value.CompareTo(LatestDate) > 0) {
                        return true; // no end date; session date is before search start date or after search end date
                    }
                    return false; // date tests pass - event is within earliest->latest
                }
                // non-dated event - check if ongoing
                if (ItemHelper.GetFieldRawValue(sessionItem, MedTouch.Calendar.Helpers.ObjectModel.EventSession.FieldName.IsOngoing) == "1")
                    return false; // ongoing events never expire
                return true; // no start date & not ongoing - don't include
            }
            return true; // invalid session
        }

        public static string EscapePathForQuery(string inPath)
        {
            return Regex.Replace(inPath, @"([^/]+)", "#$1#").Replace("#*#", "*");
        }

        /// <summary>
        /// returns whether or not there are sessions available for start item (usually an event topic):
        /// (a) has start/end date in range of today to +10 years
        /// (b) ongoing event without a date range
        /// (see SessionExpired() helper function)
        /// </summary>
        /// <param name="startItem"></param>
        /// <returns></returns>
        public static bool HasFutureSessions(Item startItem = null)
        {
            Database db = CalendarGetCurrentDatabase();
            if (startItem == null) {
                startItem = db.GetItem(Context.Site.StartPath);
            }
            if (startItem != null)
            {
                string scQuery = EscapePathForQuery(startItem.Paths.Path) + "//*[@@TemplateName='" + MedTouch.Calendar.Helpers.ObjectModel.EventSession.TemplateName + "']";
                var sessionsList = db.SelectItems("fast:" + scQuery).ToList().ToList();
                foreach (Item sessionItem in sessionsList)
                    if (!SessionExpired(sessionItem, DateTime.Today, DateTime.Today.AddYears(10)))
                        return true;
            }
            return false;
        }

        /// <summary>
        /// Utility method to get sessions below a specific node (i.e., topic, calendar, or entire site), 
        /// ensuring that we get the sessions from the appropriate Database ("master" for admin features, "web" for site features) 
        /// because the sessions need to come from the current content database (master vs. web); Calendar_Search only uses Web database
        /// </summary>
        /// <param name="startItem"></param>
        /// <returns></returns>
        public static List<Item> GetFutureSessions(Item startItem = null) {
            var sessionList = new List<Item>();
            Database db = CalendarGetCurrentDatabase();
            if (startItem == null) {
                startItem = db.GetItem(Context.Site.StartPath);
            }
            if (startItem != null) {
                string scQuery = EscapePathForQuery(startItem.Paths.Path) + "//*[@@TemplateName='" + MedTouch.Calendar.Helpers.ObjectModel.EventSession.TemplateName + "']";
                var allSessionsList = db.SelectItems("fast:" + scQuery).ToList().OrderBy(e => e.Parent.Paths.Path).ToList();
                foreach (Item sessionItem in allSessionsList)
                {
                    if (!SessionExpired(sessionItem, DateTime.Today, DateTime.Today.AddYears(10)))
                        sessionList.Add(sessionItem);
                }
            }
            return sessionList;
        }

        /// <summary>
        /// Returns list of sessions with actual capacity (null or capacity not reached yet)
        /// Does NOT include waitlist capacity
        /// </summary>
        /// <param name="eventTopicGuid"></param>
        /// <returns></returns>
        public static List<Item> GetTopicAvailableSessions(string eventTopicGuid) {
            var sessionList = new List<Item>();
            Item topic = ItemHelper.GetItemFromGuid(eventTopicGuid, CalendarGetCurrentDatabase());
            if (topic != null) {
                sessionList = GetFutureSessions(topic)
                        .Where(s => RegistrationHelper.CheckRegistrationCapacity(s.ID.ToString()) == eEventCapacity.EnrollmentAvailable)
                        .ToList();
            }
            return sessionList;
        }

        /// <summary>
        /// If parameter Item is blank, use value from the homepage item.  If rendering parameter is set, use the rendering parameter
        /// </summary>
        /// <param name="renderingSearchResultPage"></param>
        /// <returns></returns>
        public static Item GetSearchResultPageItem(Item renderingSearchResultPage)
        {
            if (renderingSearchResultPage == null)
            {
                Item siteStartItem = ItemHelper.GetStartItem();
                Item globalModuleSiteSearchResultPage = ItemHelper.GetItemFromReferenceField(siteStartItem,
                    ItemMapper.CalendarModuleSiteSettings.FieldName.EventSearchResultsPage);
                return globalModuleSiteSearchResultPage;
            }

            return renderingSearchResultPage;
        }

        /// <summary>
        /// Get Search Result URL with Query string appended.
        /// </summary>
        /// <param name="calendarSearchParam"></param>
        /// <param name="searchResultPageItem"></param>
        /// <returns></returns>
        public static string GetSearchResultsUrl(CalendarSearchParam calendarSearchParam, Item searchResultPageItem)
        {
            string retVal = "/";
            string searchResultsItemUrl = "/";
            if (searchResultPageItem != null)
            {
                searchResultsItemUrl = ItemHelper.GetItemUrl(searchResultPageItem);
            }

            SafeDictionary<string> safeDictionary = new SafeDictionary<string>();
            if (!String.IsNullOrEmpty(calendarSearchParam.Keyword))
                safeDictionary.Add(Config.QueryString.GetKeywordQuerystring(), calendarSearchParam.Keyword);
            if (!String.IsNullOrEmpty(calendarSearchParam.Category))
                safeDictionary.Add(Config.QueryString.GetCategoryQuerystring(), calendarSearchParam.Category);
            if (!String.IsNullOrEmpty(calendarSearchParam.Instructor))
                safeDictionary.Add(Config.QueryString.GetInstructorQuerystring(), calendarSearchParam.Instructor);
            if (!String.IsNullOrEmpty(calendarSearchParam.DateFrom))
                safeDictionary.Add(Config.QueryString.GetDateFromQuerystring(), calendarSearchParam.DateFrom);
            if (!String.IsNullOrEmpty(calendarSearchParam.DateTo))
                safeDictionary.Add(Config.QueryString.GetDateToQuerystring(), calendarSearchParam.DateTo);

            //Tagging
            if (!String.IsNullOrEmpty(calendarSearchParam.Specialties))
                safeDictionary.Add(BaseModuleHelper.SpecialtiesQuerystring(), calendarSearchParam.Specialties);
            if (!String.IsNullOrEmpty(calendarSearchParam.Locations))
                safeDictionary.Add(BaseModuleHelper.LocationsQuerystring(), calendarSearchParam.Locations);
            if (!String.IsNullOrEmpty(calendarSearchParam.Taxonomy))
            {
                safeDictionary.Add(BaseModuleHelper.TaxonomyQuerystring(), calendarSearchParam.Taxonomy);

                string taxonomyRecursivelySearchValue = calendarSearchParam.IsTaxonomyRecursive ? "true" : "false";
                safeDictionary.Add(BaseModuleHelper.TaxonomyRecursivelySearchQuerystring(),
                    taxonomyRecursivelySearchValue);
            }

            // Ongoing events
            if (calendarSearchParam.ListType == EventListType.Ongoing)
                safeDictionary.Add(Config.QueryString.GetEventTypeQuerystring(), "Ongoing");
            else
            {
                if (calendarSearchParam.IncludeOngoing.HasValue)
                    safeDictionary.Add(Config.QueryString.GetIncludeOngoingQuerystring(), calendarSearchParam.IncludeOngoing.Value ? "true" : "false");
            }

            StringBuilder sbQuerystring = new StringBuilder();

            foreach (KeyValuePair<string, string> keyValuePair in safeDictionary)
            {
                sbQuerystring.Append(sbQuerystring.Length > 0
                    ? String.Format("&{0}={1}", keyValuePair.Key, StringHelper.EncodeString(keyValuePair.Value))
                    : String.Format("{0}={1}", keyValuePair.Key, StringHelper.EncodeString(keyValuePair.Value)));
            }

            retVal = sbQuerystring.Length > 0
                ? String.Format("{0}?{1}", searchResultsItemUrl, sbQuerystring.ToString())
                : searchResultsItemUrl;

            return retVal;
        }

        public static void CalendarSearch(CalendarSearchParam calendarSearchParam, string strCurrentPage,
            int numberOfDisplayedItems, int numberOfDisplayedPages, bool showAsTopics, out int startItem, out int endItem,
            out int totalCount, out List<Item> items)
        {
            // initialize pagination
            int totalPage = 0;
            int currentPage = 0;
            int startPage = 0;
            int endPage = 0;
            totalCount = 0;
            startItem = 0;
            endItem = 0;

            WomansCalendarSearcher calendarSearcher = new WomansCalendarSearcher();
            items = calendarSearcher.ExecuteSearch(calendarSearchParam, showAsTopics);


            if (items != null && items.Any())
            {
                // pagination 
                totalCount = items.Count;
                MedTouch.Common.Controls.Navigation.Pagination.GetPaginationData(strCurrentPage, totalCount,
                    numberOfDisplayedItems, numberOfDisplayedPages,
                    out totalPage, out startItem, out endItem, out currentPage, out startPage,
                    out endPage);
                // end pagination
                if (startItem > 0 && endItem > 0)
                    items = items.GetRange(startItem - 1, endItem - startItem + 1);
            }
        }

        /// <summary>
        /// Submit search and Redirect to Search Result Page
        /// </summary>
        /// <param name="calendarSearchParam"></param>
        /// <param name="searchResultsPage"></param>
        public static void SubmitSearch(CalendarSearchParam calendarSearchParam, Item searchResultsPage)
        {
            WebUtil.Redirect(GetSearchResultsUrl(calendarSearchParam, searchResultsPage));
        }
    }
}