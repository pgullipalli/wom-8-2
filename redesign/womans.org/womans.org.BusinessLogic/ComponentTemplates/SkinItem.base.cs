using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class SkinItem : CustomItem
{

public static readonly string TemplateId = "{B1701564-0230-49D7-AFF9-5B17AA1ACC0E}";


#region Boilerplate CustomItem Code

public SkinItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator SkinItem(Item innerItem)
{
	return innerItem != null ? new SkinItem(innerItem) : null;
}

public static implicit operator Item(SkinItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField FileSystemFolderName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["File System Folder Name"]);
	}
}


#endregion //Field Instance Methods
}
}