using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class NavigationItem 
{

    public string LinkToSelf
    {
        get
        {
            if (InnerItem.Fields["Destination"] != null)
            {
                LinkField link = InnerItem.Fields["Destination"];

                if (link != null && link.IsInternal)
                {
                    return Sitecore.Links.LinkManager.GetItemUrl(link.TargetItem);
                }
            }
            
            return Sitecore.Links.LinkManager.GetItemUrl(InnerItem);
        }
    }



}
}