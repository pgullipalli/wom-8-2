using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class MapServiceItem : CustomItem
{

public static readonly string TemplateId = "{1D44572D-3DA4-490F-ABA8-A8FB97EA0646}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public MapServiceItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator MapServiceItem(Item innerItem)
{
	return innerItem != null ? new MapServiceItem(innerItem) : null;
}

public static implicit operator Item(MapServiceItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField URLService
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["URL Service"]);
	}
}


#endregion //Field Instance Methods
}
}