using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class LanguageItem : CustomItem
{

public static readonly string TemplateId = "{93075E7D-ABC3-4020-853B-D30625CC31B1}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }
private readonly DataImportItem _DataImportItem;
public DataImportItem DataImport { get { return _DataImportItem; } }

#endregion

#region Boilerplate CustomItem Code

public LanguageItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);
	_DataImportItem = new DataImportItem(innerItem);

}

public static implicit operator LanguageItem(Item innerItem)
{
	return innerItem != null ? new LanguageItem(innerItem) : null;
}

public static implicit operator Item(LanguageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField LanguageName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Language Name"]);
	}
}


public CustomTextField LanguageCode
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Language Code"]);
	}
}


#endregion //Field Instance Methods
}
}