using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class SendEmailContactSettingItem : CustomItem
{

public static readonly string TemplateId = "{60597EC7-B2F2-4344-AEEB-6CC8858C7B0D}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public SendEmailContactSettingItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator SendEmailContactSettingItem(Item innerItem)
{
	return innerItem != null ? new SendEmailContactSettingItem(innerItem) : null;
}

public static implicit operator Item(SendEmailContactSettingItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomLookupField WFFMFormItem
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["WFFM Form Item"]);
	}
}


public CustomLookupField SubjectFieldItem
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Subject Field Item"]);
	}
}


public CustomLookupField ContactSubjectFolder
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Contact Subject Folder"]);
	}
}


#endregion //Field Instance Methods
}
}