using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class CountryItem : CustomItem
{

public static readonly string TemplateId = "{DBDA669B-D26F-478E-8250-F293AA89548D}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public CountryItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator CountryItem(Item innerItem)
{
	return innerItem != null ? new CountryItem(innerItem) : null;
}

public static implicit operator Item(CountryItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Abbreviation
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Abbreviation"]);
	}
}


public CustomTextField CountryName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Country Name"]);
	}
}


#endregion //Field Instance Methods
}
}