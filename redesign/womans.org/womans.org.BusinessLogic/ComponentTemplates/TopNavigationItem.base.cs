using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class TopNavigationItem : CustomItem
{

public static readonly string TemplateId = "{40A30B85-69D3-41D4-A175-A1B68D9D0BCD}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public TopNavigationItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator TopNavigationItem(Item innerItem)
{
	return innerItem != null ? new TopNavigationItem(innerItem) : null;
}

public static implicit operator Item(TopNavigationItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField OverrideNavigationTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Override Navigation Title"]);
	}
}


public CustomGeneralLinkField Destination
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Destination"]);
	}
}


public CustomTextField OptionalContentBlock
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Optional Content Block"]);
	}
}


#endregion //Field Instance Methods
}
}