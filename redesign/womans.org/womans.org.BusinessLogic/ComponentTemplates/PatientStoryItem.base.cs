using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class PatientStoryItem : CustomItem
{

public static readonly string TemplateId = "{4552C4C3-B39C-4B3A-BE30-31398DE0AC0B}";


#region Boilerplate CustomItem Code

public PatientStoryItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator PatientStoryItem(Item innerItem)
{
	return innerItem != null ? new PatientStoryItem(innerItem) : null;
}

public static implicit operator Item(PatientStoryItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Headline
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Headline"]);
	}
}


public CustomImageField Image
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Image"]);
	}
}


public CustomTextField Quote
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Quote"]);
	}
}


public CustomTextField NameandPlace
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Name and Place"]);
	}
}


#endregion //Field Instance Methods
}
}