using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates.Services
{
public partial class ServiceItem : CustomItem
{

public static readonly string TemplateId = "{24E5C576-661A-483A-BABE-3D011F9051F7}";


#region Boilerplate CustomItem Code

public ServiceItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator ServiceItem(Item innerItem)
{
	return innerItem != null ? new ServiceItem(innerItem) : null;
}

public static implicit operator Item(ServiceItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


//Could not find Field Type for Alternative Service Name


public CustomTextField Name
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Name"]);
	}
}


public CustomTextField Teaser
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Teaser"]);
	}
}


public CustomGeneralLinkField Link
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link"]);
	}
}


public CustomImageField ServiceImage
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Service Image"]);
	}
}


#endregion //Field Instance Methods
}
}