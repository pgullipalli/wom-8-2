using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class SliderCalloutItem : CustomItem
{

public static readonly string TemplateId = "{B198134B-380C-4103-A167-6C5D082D1C35}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public SliderCalloutItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator SliderCalloutItem(Item innerItem)
{
	return innerItem != null ? new SliderCalloutItem(innerItem) : null;
}

public static implicit operator Item(SliderCalloutItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTreeListField CalloutSliderImages
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Callout Slider Images"]);
	}
}


public CustomTextField CalloutTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Callout Title"]);
	}
}


public CustomTextField CalloutCopy
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Callout Copy"]);
	}
}


public CustomGeneralLinkField CalloutLink
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Callout Link"]);
	}
}


public CustomTextField CalloutLinkButtonText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Callout Link Button Text"]);
	}
}


public CustomTextField CalloutSliderCaption
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Callout Slider Caption"]);
	}
}


#endregion //Field Instance Methods
}
}