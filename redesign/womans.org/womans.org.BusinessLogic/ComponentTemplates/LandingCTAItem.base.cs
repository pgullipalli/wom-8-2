using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class LandingCTAItem : CustomItem
{

public static readonly string TemplateId = "{181FBFE9-870D-4793-8B18-F27C4942C96A}";


#region Boilerplate CustomItem Code

public LandingCTAItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator LandingCTAItem(Item innerItem)
{
	return innerItem != null ? new LandingCTAItem(innerItem) : null;
}

public static implicit operator Item(LandingCTAItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField HeadingBottomLeft
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Heading Bottom Left"]);
	}
}


public CustomTextField HeadingBottomRight
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Heading Bottom Right"]);
	}
}


public CustomTextField HeadingTopRight
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Heading Top Right"]);
	}
}


public CustomTextField BodyTextBottomLeft
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Body Text Bottom Left"]);
	}
}


public CustomTextField BodyTextBottomRight
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Body Text Bottom Right"]);
	}
}


public CustomTextField BodyTextTopRight
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Body Text Top Right"]);
	}
}


public CustomTextField HeadingTopLeft
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Heading Top Left"]);
	}
}


public CustomImageField BackgroundImageBottomLeft
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Background Image Bottom Left"]);
	}
}


public CustomImageField BackgroundImageBottomRight
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Background Image Bottom Right"]);
	}
}


public CustomImageField BackgroundImageTopRight
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Background Image Top Right"]);
	}
}


public CustomTextField BodyTextTopLeft
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Body Text Top Left"]);
	}
}


public CustomImageField BackgroundImageTopLeft
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Background Image Top Left"]);
	}
}


public CustomTextField ButtonTextBottomLeft
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Button Text Bottom Left"]);
	}
}


public CustomTextField ButtonTextBottomRight
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Button Text Bottom Right"]);
	}
}


public CustomTextField ButtonTextTopRight
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Button Text Top Right"]);
	}
}


public CustomTextField ButtonTextTopLeft
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Button Text Top Left"]);
	}
}


public CustomGeneralLinkField LinkBottomLeft
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link Bottom Left"]);
	}
}


public CustomGeneralLinkField LinkBottomRight
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link Bottom Right"]);
	}
}


public CustomGeneralLinkField LinkTopRight
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link Top Right"]);
	}
}


public CustomGeneralLinkField LinkTopLeft
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link Top Left"]);
	}
}


#endregion //Field Instance Methods
}
}