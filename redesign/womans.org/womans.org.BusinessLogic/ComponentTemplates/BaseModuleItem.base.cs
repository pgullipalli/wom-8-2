using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class BaseModuleItem : CustomItem
{

public static readonly string TemplateId = "{2D17512B-8FAE-4DFE-BF9B-710B253698F9}";


#region Boilerplate CustomItem Code

public BaseModuleItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator BaseModuleItem(Item innerItem)
{
	return innerItem != null ? new BaseModuleItem(innerItem) : null;
}

public static implicit operator Item(BaseModuleItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomLookupField MapService
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Map Service"]);
	}
}


public CustomLookupField SpecialtiesFolder
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Specialties Folder"]);
	}
}


public CustomTextField SpecialtiesQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Specialties Querystring"]);
	}
}


public CustomLookupField SpecialtyTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Specialty Template"]);
	}
}


public CustomLookupField LocationTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Location Template"]);
	}
}


public CustomTreeListField LocationsFolder
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Locations Folder"]);
	}
}


public CustomTextField LocationsQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Locations Querystring"]);
	}
}


public CustomTextField RangeQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Range Querystring"]);
	}
}


public CustomLookupField RangeTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Range Template"]);
	}
}


public CustomLookupField ServicesFolder
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Services Folder"]);
	}
}


//Could not find Field Type for Service Template


public CustomTextField TaxonomyQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Taxonomy Querystring"]);
	}
}


public CustomTextField TaxonomyRecursivelySearchQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Taxonomy Recursively Search Querystring"]);
	}
}


public CustomTextField IncludedTemplatesQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Included Templates Querystring"]);
	}
}


public CustomTextField ServicesQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Services Querystring"]);
	}
}


#endregion //Field Instance Methods
}
}