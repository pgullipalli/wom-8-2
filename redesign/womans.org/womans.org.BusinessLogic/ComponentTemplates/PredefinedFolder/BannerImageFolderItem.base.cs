using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates.PredefinedFolder
{
public partial class BannerImageFolderItem : CustomItem
{

public static readonly string TemplateId = "{7BA1F5FC-E2B2-4B1A-A956-5B321FF6C80B}";


#region Boilerplate CustomItem Code

public BannerImageFolderItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator BannerImageFolderItem(Item innerItem)
{
	return innerItem != null ? new BannerImageFolderItem(innerItem) : null;
}

public static implicit operator Item(BannerImageFolderItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}