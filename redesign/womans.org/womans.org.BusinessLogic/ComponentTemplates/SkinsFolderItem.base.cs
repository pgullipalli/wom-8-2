using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class SkinsFolderItem : CustomItem
{

public static readonly string TemplateId = "{D1D8E3FE-BD8D-4259-9400-88B5F6083666}";


#region Boilerplate CustomItem Code

public SkinsFolderItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator SkinsFolderItem(Item innerItem)
{
	return innerItem != null ? new SkinsFolderItem(innerItem) : null;
}

public static implicit operator Item(SkinsFolderItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}