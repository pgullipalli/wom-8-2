using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class NavigationSectionItem : CustomItem
{

public static readonly string TemplateId = "{B72C737C-8855-4C48-A424-3DB23ACFA986}";


#region Boilerplate CustomItem Code

public NavigationSectionItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator NavigationSectionItem(Item innerItem)
{
	return innerItem != null ? new NavigationSectionItem(innerItem) : null;
}

public static implicit operator Item(NavigationSectionItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField SectionName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Section Name"]);
	}
}


#endregion //Field Instance Methods
}
}