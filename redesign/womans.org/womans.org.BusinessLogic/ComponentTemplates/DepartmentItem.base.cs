using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class DepartmentItem : CustomItem
{

public static readonly string TemplateId = "{03886F3B-EE53-4BB5-A879-B7CB8723780C}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }
private readonly DataImportItem _DataImportItem;
public DataImportItem DataImport { get { return _DataImportItem; } }

#endregion

#region Boilerplate CustomItem Code

public DepartmentItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);
	_DataImportItem = new DataImportItem(innerItem);

}

public static implicit operator DepartmentItem(Item innerItem)
{
	return innerItem != null ? new DepartmentItem(innerItem) : null;
}

public static implicit operator Item(DepartmentItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField DepartmentName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Department Name"]);
	}
}


public CustomTextField DepartmentCode
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Department Code"]);
	}
}


#endregion //Field Instance Methods
}
}