using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class ImageCalloutItem : CustomItem
{

public static readonly string TemplateId = "{D8EF51A8-EEF2-4B92-9B31-6B613F92ED38}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public ImageCalloutItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator ImageCalloutItem(Item innerItem)
{
	return innerItem != null ? new ImageCalloutItem(innerItem) : null;
}

public static implicit operator Item(ImageCalloutItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomImageField CalloutImage
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Callout Image"]);
	}
}


public CustomGeneralLinkField CalloutLink
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Callout Link"]);
	}
}

public CustomTextField CalloutHeader
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["Callout Header"]);
    }
}

public CustomTextField CalloutText
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["Callout Text"]);
    }
}



#endregion //Field Instance Methods
}
}