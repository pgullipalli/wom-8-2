using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class HomepageSlideItem : CustomItem
{

public static readonly string TemplateId = "{9B6FEC22-4047-4F47-813D-FF470A14969C}";


#region Boilerplate CustomItem Code

public HomepageSlideItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator HomepageSlideItem(Item innerItem)
{
	return innerItem != null ? new HomepageSlideItem(innerItem) : null;
}

public static implicit operator Item(HomepageSlideItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField SlideTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Slide Title"]);
	}
}


public CustomImageField SlideImage
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Slide Image"]);
	}
}


public CustomTextField SlideContent
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Slide Content"]);
	}
}


public CustomGeneralLinkField SlideLink
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Slide Link"]);
	}
}


#endregion //Field Instance Methods
}
}