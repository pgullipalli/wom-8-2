using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class RangeItem : CustomItem
{

public static readonly string TemplateId = "{A1B48585-5659-4FE5-BDCE-D4472CA1A01E}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public RangeItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator RangeItem(Item innerItem)
{
	return innerItem != null ? new RangeItem(innerItem) : null;
}

public static implicit operator Item(RangeItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField DisplayedText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Displayed Text"]);
	}
}


public CustomIntegerField Value
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["Value"]);
	}
}


#endregion //Field Instance Methods
}
}