using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class StateItem : CustomItem
{

public static readonly string TemplateId = "{30E388E1-43C6-44A6-BA66-1BE60E498BE1}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }
private readonly DataImportItem _DataImportItem;
public DataImportItem DataImport { get { return _DataImportItem; } }

#endregion

#region Boilerplate CustomItem Code

public StateItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);
	_DataImportItem = new DataImportItem(innerItem);

}

public static implicit operator StateItem(Item innerItem)
{
	return innerItem != null ? new StateItem(innerItem) : null;
}

public static implicit operator Item(StateItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Abbreviation
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Abbreviation"]);
	}
}


public CustomTextField StateName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["State Name"]);
	}
}


#endregion //Field Instance Methods
}
}