using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class FooterFeatureItem : CustomItem
{

public static readonly string TemplateId = "{04772A6C-D088-4B43-A22B-D7516A56E1AD}";


#region Boilerplate CustomItem Code

public FooterFeatureItem(Item innerItem)
    : base(innerItem)
{

}

public static implicit operator FooterFeatureItem(Item innerItem)
{
    return innerItem != null ? new FooterFeatureItem(innerItem) : null;
}

public static implicit operator Item(FooterFeatureItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Headline
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Headline"]);
	}
}


public CustomTextField Content
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Content"]);
	}
}


public CustomImageField Image
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Image"]);
	}
}


public CustomGeneralLinkField Link
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link"]);
	}
}


#endregion //Field Instance Methods
}
}