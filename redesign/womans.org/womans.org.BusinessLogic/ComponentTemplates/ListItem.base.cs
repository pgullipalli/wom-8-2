using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class ListItem : CustomItem
{

public static readonly string TemplateId = "{9C65CE2B-7E32-4B1C-B6F8-C2BFD8703C48}";


#region Boilerplate CustomItem Code

public ListItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator ListItem(Item innerItem)
{
	return innerItem != null ? new ListItem(innerItem) : null;
}

public static implicit operator Item(ListItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Name
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Name"]);
	}
}


public CustomTextField Value
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Value"]);
	}
}


#endregion //Field Instance Methods
}
}