using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class TaxonomyTermItem : CustomItem
{

public static readonly string TemplateId = "{6EB63B62-5FED-42AF-8BC3-5900CED7A41D}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public TaxonomyTermItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator TaxonomyTermItem(Item innerItem)
{
	return innerItem != null ? new TaxonomyTermItem(innerItem) : null;
}

public static implicit operator Item(TaxonomyTermItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField TaxonomyTerm
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Taxonomy Term"]);
	}
}


public CustomTextField TaxonomyID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Taxonomy ID"]);
	}
}


#endregion //Field Instance Methods
}
}