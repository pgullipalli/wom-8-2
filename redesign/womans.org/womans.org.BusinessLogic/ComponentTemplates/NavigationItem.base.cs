using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class NavigationItem : CustomItem
{

public static readonly string TemplateId = "{F8C860FE-ECA7-4E42-AD3A-061F2992A8C2}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public NavigationItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator NavigationItem(Item innerItem)
{
	return innerItem != null ? new NavigationItem(innerItem) : null;
}

public static implicit operator Item(NavigationItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomImageField HoverImage
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Hover Image"]);
	}
}


public CustomTextField OverrideNavigationTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Override Navigation Title"]);
	}
}

public CustomTextField CSSClass
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["CSS Class"]);
    }
}


public CustomGeneralLinkField Destination
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Destination"]);
	}
}


public CustomTextField HoverContentCopy
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Hover Content Copy"]);
	}
}


public CustomTextField HoverButtonCopy
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Hover Button Copy"]);
	}
}


#endregion //Field Instance Methods
}
}