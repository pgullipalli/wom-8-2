using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class CalloutItem : CustomItem
{

public static readonly string TemplateId = "{FAE173F3-7379-41B6-963A-8B275BF9CE06}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public CalloutItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator CalloutItem(Item innerItem)
{
	return innerItem != null ? new CalloutItem(innerItem) : null;
}

public static implicit operator Item(CalloutItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTreeListField CalloutSliderImages
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Callout Slider Images"]);
	}
}


public CustomTextField CalloutTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Callout Title"]);
	}
}


public CustomTextField CalloutCopy
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Callout Copy"]);
	}
}


public CustomTextField CalloutSliderCaption
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Callout Slider Caption"]);
	}
}


public CustomGeneralLinkField CalloutLink
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Callout Link"]);
	}
}


public CustomTextField CalloutSliderButtonText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Callout Slider Button Text"]);
	}
}


public CustomTextField CalloutLinkButtonText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Callout Link Button Text"]);
	}
}


public CustomGeneralLinkField CalloutSliderLink
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Callout Slider Link"]);
	}
}


#endregion //Field Instance Methods
}
}