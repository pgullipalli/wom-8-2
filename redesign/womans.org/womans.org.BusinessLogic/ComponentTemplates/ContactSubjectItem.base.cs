using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class ContactSubjectItem : CustomItem
{

public static readonly string TemplateId = "{34E69B7E-36A7-46BE-94F4-D93B2B9627CC}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public ContactSubjectItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator ContactSubjectItem(Item innerItem)
{
	return innerItem != null ? new ContactSubjectItem(innerItem) : null;
}

public static implicit operator Item(ContactSubjectItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Subject
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Subject"]);
	}
}


public CustomTextField Email
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Email"]);
	}
}


#endregion //Field Instance Methods
}
}