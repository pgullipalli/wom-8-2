using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class SpecialtyRelationshipItem : CustomItem
{

public static readonly string TemplateId = "{F13CD9DF-B762-4E72-8706-5D325EB1F223}";


#region Boilerplate CustomItem Code

public SpecialtyRelationshipItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator SpecialtyRelationshipItem(Item innerItem)
{
	return innerItem != null ? new SpecialtyRelationshipItem(innerItem) : null;
}

public static implicit operator Item(SpecialtyRelationshipItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField SpecialtyName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Specialty Name"]);
	}
}


public CustomMultiListField Specialties
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["Specialties"]);
	}
}


#endregion //Field Instance Methods
}
}