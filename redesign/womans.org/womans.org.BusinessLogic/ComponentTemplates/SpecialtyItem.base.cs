using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates
{
public partial class SpecialtyItem : CustomItem
{

public static readonly string TemplateId = "{AD052481-F20F-47DA-84B7-04F7B63206CC}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }
private readonly DataImportItem _DataImportItem;
public DataImportItem DataImport { get { return _DataImportItem; } }

#endregion

#region Boilerplate CustomItem Code

public SpecialtyItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);
	_DataImportItem = new DataImportItem(innerItem);

}

public static implicit operator SpecialtyItem(Item innerItem)
{
	return innerItem != null ? new SpecialtyItem(innerItem) : null;
}

public static implicit operator Item(SpecialtyItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomCheckboxField AllowtoMakeanAppointment
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Allow to Make an Appointment"]);
	}
}


public CustomTextField SpecialtyName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Specialty Name"]);
	}
}


public CustomTextField SpecialtyCode
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Specialty Code"]);
	}
}


#endregion //Field Instance Methods
}
}