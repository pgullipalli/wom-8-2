using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates.PredefinedFolders
{
public partial class SpecialtyFolderItem : CustomItem
{

public static readonly string TemplateId = "{2E481388-6031-4776-92F2-C23ED2D5287C}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public SpecialtyFolderItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator SpecialtyFolderItem(Item innerItem)
{
	return innerItem != null ? new SpecialtyFolderItem(innerItem) : null;
}

public static implicit operator Item(SpecialtyFolderItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}