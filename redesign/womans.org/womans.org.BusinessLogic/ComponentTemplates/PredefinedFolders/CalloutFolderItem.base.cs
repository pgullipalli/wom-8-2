using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates.PredefinedFolders
{
public partial class CalloutFolderItem : CustomItem
{

public static readonly string TemplateId = "{FA1FC8C4-0D72-4777-9EC9-C4EA310075A6}";


#region Boilerplate CustomItem Code

public CalloutFolderItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator CalloutFolderItem(Item innerItem)
{
	return innerItem != null ? new CalloutFolderItem(innerItem) : null;
}

public static implicit operator Item(CalloutFolderItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}