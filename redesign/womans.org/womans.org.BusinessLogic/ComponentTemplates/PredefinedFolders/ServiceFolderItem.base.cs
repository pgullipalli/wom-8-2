using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates.PredefinedFolders
{
public partial class ServiceFolderItem : CustomItem
{

public static readonly string TemplateId = "{6F1CF3BC-FA2F-4A90-9A4C-1FAAFFFF6201}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public ServiceFolderItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator ServiceFolderItem(Item innerItem)
{
	return innerItem != null ? new ServiceFolderItem(innerItem) : null;
}

public static implicit operator Item(ServiceFolderItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}