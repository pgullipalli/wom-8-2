using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.ComponentTemplates.PredefinedFolders
{
public partial class NotificationFolderItem : CustomItem
{

public static readonly string TemplateId = "{5E02BE42-A907-4774-8657-9998E2A39BBD}";


#region Boilerplate CustomItem Code

public NotificationFolderItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator NotificationFolderItem(Item innerItem)
{
	return innerItem != null ? new NotificationFolderItem(innerItem) : null;
}

public static implicit operator Item(NotificationFolderItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}