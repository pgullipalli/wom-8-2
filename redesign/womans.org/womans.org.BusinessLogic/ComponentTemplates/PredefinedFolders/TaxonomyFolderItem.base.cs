using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.ComponentTemplates.PredefinedFolders
{
public partial class TaxonomyFolderItem : CustomItem
{

public static readonly string TemplateId = "{8DF38EA4-3163-4B28-A496-BCA7CD481790}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public TaxonomyFolderItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator TaxonomyFolderItem(Item innerItem)
{
	return innerItem != null ? new TaxonomyFolderItem(innerItem) : null;
}

public static implicit operator Item(TaxonomyFolderItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}