using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.Messages.InnerContent
{
public partial class FooterItem : CustomItem
{

    public static readonly string TemplateId = "{35BD8877-8E03-4B63-A768-DD2939038590}";


#region Boilerplate CustomItem Code

public FooterItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator FooterItem(Item innerItem)
{
	return innerItem != null ? new FooterItem(innerItem) : null;
}

public static implicit operator Item(FooterItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField FooterLinks
{
	get
	{
        return new CustomTextField(InnerItem, InnerItem.Fields["Footer Links"]);
	}
}

public CustomTextField CopyrightText
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["Copyright Text"]);
    }
}

#endregion //Field Instance Methods
}
}