using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.Messages.InnerContent.Newsletter
{
public partial class HalfWidthHeaderLeftItem : CustomItem
{

public static readonly string TemplateId = "{9479A4C2-DB06-4594-A71A-9DDBEFED694F}";


#region Boilerplate CustomItem Code

public HalfWidthHeaderLeftItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator HalfWidthHeaderLeftItem(Item innerItem)
{
	return innerItem != null ? new HalfWidthHeaderLeftItem(innerItem) : null;
}

public static implicit operator Item(HalfWidthHeaderLeftItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Title
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Title"]);
	}
}


public CustomTextField Content
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Content"]);
	}
}


public CustomGeneralLinkField Link
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link"]);
	}
}


public CustomImageField Image
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Image"]);
	}
}


#endregion //Field Instance Methods
}
}