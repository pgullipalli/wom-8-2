﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Sitecore.Data.Items;

namespace womans.org.BusinessLogic.Messages.InnerContent.Newsletter
{
    public class TwoImagesTwoLinksItem : CustomItem
    {
        public static readonly string TemplateId = "{9086554C-1A50-43E9-9DE7-7505E408768F}";

        #region Boilerplate CustomItem Code
        public TwoImagesTwoLinksItem(Item innerItem)
            : base(innerItem)
        {

        }

        public static implicit operator TwoImagesTwoLinksItem(Item innerItem)
        {
            return innerItem != null ? new TwoImagesTwoLinksItem(innerItem) : null;
        }

        public static implicit operator Item(TwoImagesTwoLinksItem customItem)
        {
	        return customItem != null ? customItem.InnerItem : null;
        }
        #endregion Boilerplate CustomItem Code

        #region Field Instance Methods
        public CustomTextField Title
        {
            get
            {
                return new CustomTextField(InnerItem, InnerItem.Fields["Title"]);
            }
        }

        public CustomImageField ImageLeft
        {
            get
            {
                return new CustomImageField(InnerItem, InnerItem.Fields["Image Left"]);
            }
        }

        public CustomGeneralLinkField LinkLeft
        {
            get
            {
                return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link Left"]);
            }
        }

        public CustomImageField ImageRight
        {
            get
            {
                return new CustomImageField(InnerItem, InnerItem.Fields["Image Right"]);
            }
        }

        public CustomGeneralLinkField LinkRight
        {
            get
            {
                return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link Right"]);
            }
        }
        #endregion Field Instance Methods
    }
}
