﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Data.Items;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace womans.org.BusinessLogic.Messages.InnerContent.Newsletter
{
    public class RelatedContentItem : CustomItem
    {
        public static readonly string TemplateId = "{8F882F00-4028-47A4-A82E-DA2B3F4F7692}";

        #region Inherited Base Templates

        private readonly TaggingItem _TaggingItem;
        public TaggingItem Tagging { get { return _TaggingItem; } }

        #endregion

        #region Boilerplate CustomItem Code
        public RelatedContentItem(Item innerItem)
            : base(innerItem)
        {
            _TaggingItem = new TaggingItem(innerItem);
        }

        public static implicit operator RelatedContentItem(Item innerItem)
        {
            return innerItem != null ? new RelatedContentItem(innerItem) : null;
        }

        public static implicit operator Item(RelatedContentItem customItem)
        {
	        return customItem != null ? customItem.InnerItem : null;
        }
        #endregion //Boilerplate CustomItem Code


        #region Field Instance Methods
        public CustomTextField Title
        {
	        get
	        {
		        return new CustomTextField(InnerItem, InnerItem.Fields["Title"]);
	        }
        }       
        #endregion //Field Instance Methods
    }
}
