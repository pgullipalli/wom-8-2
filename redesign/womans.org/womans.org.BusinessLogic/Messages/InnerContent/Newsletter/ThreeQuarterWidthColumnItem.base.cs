using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.Messages.InnerContent.Newsletter
{
public partial class ThreeQuarterWidthColumnItem : CustomItem
{

public static readonly string TemplateId = "{6E42183A-A583-4BE6-A08B-1CFFA4373E25}";


#region Boilerplate CustomItem Code

public ThreeQuarterWidthColumnItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator ThreeQuarterWidthColumnItem(Item innerItem)
{
	return innerItem != null ? new ThreeQuarterWidthColumnItem(innerItem) : null;
}

public static implicit operator Item(ThreeQuarterWidthColumnItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField ContentColumn2
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Content Column 2"]);
	}
}


public CustomTextField Title
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Title"]);
	}
}


public CustomTextField ContentColumn1
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Content Column 1"]);
	}
}


#endregion //Field Instance Methods
}
}