using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.Messages.InnerContent.Newsletter
{
public partial class HeaderItem : CustomItem
{

public static readonly string TemplateId = "{545FA681-19A5-488B-9F2B-AECE411BD7E0}";


#region Boilerplate CustomItem Code

public HeaderItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator HeaderItem(Item innerItem)
{
	return innerItem != null ? new HeaderItem(innerItem) : null;
}

public static implicit operator Item(HeaderItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Text
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Text"]);
	}
}


public CustomTextField LinkText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Link Text"]);
	}
}


public CustomCheckboxField LinktoSelf
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Link to Self"]);
	}
}


public CustomGeneralLinkField Destination
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Destination"]);
	}
}


//public CustomTextField SocialIcons
//{
//    get
//    {
//        return new CustomTextField(InnerItem, InnerItem.Fields["Social Icons"]);
//    }
//}

    public CustomGeneralLinkField TwitterLink
    {
        get
        {
            return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Twitter Link"]);
        }
    }
    public CustomImageField TwitterIcon
    {
        get
        {
            return new CustomImageField(InnerItem, InnerItem.Fields["Twitter Icon"]);
        }
    }

    public CustomGeneralLinkField BlogLink
    {
        get
        {
            return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Blog Link"]);
        }
    }
    public CustomImageField BlogIcon
    {
        get
        {
            return new CustomImageField(InnerItem, InnerItem.Fields["Blog Icon"]);
        }
    }

    public CustomGeneralLinkField FacebookLink
    {
        get
        {
            return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Facebook Link"]);
        }
    }
    public CustomImageField FacebookIcon
    {
        get
        {
            return new CustomImageField(InnerItem, InnerItem.Fields["Facebook Icon"]);
        }
    }

    public CustomGeneralLinkField PinterestLink
    {
        get
        {
            return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Pinterest Link"]);
        }
    }
    public CustomImageField PinterestIcon
    {
        get
        {
            return new CustomImageField(InnerItem, InnerItem.Fields["Pinterest Icon"]);
        }
    }

    public CustomGeneralLinkField YoutubeLink
    {
        get
        {
            return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Youtube Link"]);
        }
    }
    public CustomImageField YoutubeIcon
    {
        get
        {
            return new CustomImageField(InnerItem, InnerItem.Fields["Youtube Icon"]);
        }
    }

    public CustomGeneralLinkField InstagramLink
    {
        get
        {
            return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Instagram Link"]);
        }
    }
    public CustomImageField InstagramIcon
    {
        get
        {
            return new CustomImageField(InnerItem, InnerItem.Fields["Instagram Icon"]);
        }
    }
    #endregion //Field Instance Methods
}
}