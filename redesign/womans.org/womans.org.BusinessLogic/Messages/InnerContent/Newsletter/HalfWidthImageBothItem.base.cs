using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.Messages.InnerContent.Newsletter
{
public partial class HalfWidthImageBothItem : CustomItem
{

public static readonly string TemplateId = "{BC3FD731-863A-4BC1-9260-B14F9C4CD7F9}";


#region Boilerplate CustomItem Code

public HalfWidthImageBothItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator HalfWidthImageBothItem(Item innerItem)
{
	return innerItem != null ? new HalfWidthImageBothItem(innerItem) : null;
}

public static implicit operator Item(HalfWidthImageBothItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomImageField ImageLeft
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Image Left"]);
	}
}


public CustomImageField ImageRight
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Image Right"]);
	}
}


public CustomTextField ImageLeftHeader
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Image Left Header"]);
	}
}


public CustomTextField ImageRightHeader
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Image Right Header"]);
	}
}


public CustomTextField ContentLeft
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Content Left"]);
	}
}


public CustomTextField ContentRight
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Content Right"]);
	}
}


public CustomGeneralLinkField LinkLeft
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link Left"]);
	}
}


public CustomGeneralLinkField LinkRight
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link Right"]);
	}
}


#endregion //Field Instance Methods
}
}