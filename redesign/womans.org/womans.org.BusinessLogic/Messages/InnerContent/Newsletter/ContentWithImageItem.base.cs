using System;
using System.Collections.Generic;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;

namespace Womans.CustomItems.Messages.InnerContent.Newsletter
{
public partial class ContentWithImageItem : CustomItem
{

public static readonly string TemplateId = "{2713F9B8-A4A7-414E-A21D-A76408063FF8}";


#region Boilerplate CustomItem Code

public ContentWithImageItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator ContentWithImageItem(Item innerItem)
{
	return innerItem != null ? new ContentWithImageItem(innerItem) : null;
}

public static implicit operator Item(ContentWithImageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Title
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Title"]);
	}
}


public CustomTextField Content
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Content"]);
	}
}


public CustomGeneralLinkField Link
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link"]);
	}
}


public CustomImageField Image
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Image"]);
	}
}


#endregion //Field Instance Methods
}
}