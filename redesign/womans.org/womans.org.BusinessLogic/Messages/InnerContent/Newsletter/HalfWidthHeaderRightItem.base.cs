using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.Messages.InnerContent.Newsletter
{
public partial class HalfWidthHeaderRightItem : CustomItem
{

public static readonly string TemplateId = "{BEE0B0A4-D46F-462A-9A42-94936362CA7A}";


#region Boilerplate CustomItem Code

public HalfWidthHeaderRightItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator HalfWidthHeaderRightItem(Item innerItem)
{
	return innerItem != null ? new HalfWidthHeaderRightItem(innerItem) : null;
}

public static implicit operator Item(HalfWidthHeaderRightItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Title
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Title"]);
	}
}


public CustomTextField Content
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Content"]);
	}
}


public CustomGeneralLinkField Link
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link"]);
	}
}


public CustomImageField Image
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Image"]);
	}
}


#endregion //Field Instance Methods
}
}