using System;
using System.Collections.Generic;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;

namespace Womans.CustomItems.Messages.InnerContent.Newsletter
{
public partial class LinkWithImageItem : CustomItem
{

public static readonly string TemplateId = "{0236BAF6-CD46-4EAC-8731-603803D9CCF9}";


#region Boilerplate CustomItem Code

public LinkWithImageItem(Item innerItem)
    : base(innerItem)
{

}

public static implicit operator LinkWithImageItem(Item innerItem)
{
    return innerItem != null ? new LinkWithImageItem(innerItem) : null;
}

public static implicit operator Item(LinkWithImageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomGeneralLinkField Link
{
	get
	{
        return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Link"]);
	}
}


public CustomImageField Image
{
	get
	{
        return new CustomImageField(InnerItem, InnerItem.Fields["Image"]);
	}
}


#endregion //Field Instance Methods
}
}