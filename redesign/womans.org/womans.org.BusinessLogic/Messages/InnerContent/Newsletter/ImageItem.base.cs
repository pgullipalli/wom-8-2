using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.Messages.InnerContent.Newsletter
{
public partial class ImageItem : CustomItem
{

public static readonly string TemplateId = "{0E5C0114-6917-4C36-B77A-B60AA9BFF4E5}";


#region Boilerplate CustomItem Code

public ImageItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator ImageItem(Item innerItem)
{
	return innerItem != null ? new ImageItem(innerItem) : null;
}

public static implicit operator Item(ImageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomImageField Image
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Image"]);
	}
}


#endregion //Field Instance Methods
}
}