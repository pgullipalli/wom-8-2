using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.Messages.InnerContent.Newsletter
{
public partial class MainSectionItem : CustomItem
{

public static readonly string TemplateId = "{7E6F2940-D810-4960-B452-E0D4076D4572}";


#region Boilerplate CustomItem Code

public MainSectionItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator MainSectionItem(Item innerItem)
{
	return innerItem != null ? new MainSectionItem(innerItem) : null;
}

public static implicit operator Item(MainSectionItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


//Could not find Field Type for Headline


public CustomImageField Image
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Image"]);
	}
}

public CustomTextField Headline
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["Headline"]);
    }
}
#endregion //Field Instance Methods
}
}