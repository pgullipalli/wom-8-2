﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Sitecore.Data.Items;

namespace womans.org.BusinessLogic.Messages.InnerContent.Newsletter
{
    public class TwoImagesTwoContentsItem : CustomItem
    {
        public static readonly string TemplateId = "{7CF7B167-781E-4432-8B35-A63BCBE49772}";

        #region Boilerplate CustomItem Code
        public TwoImagesTwoContentsItem(Item innerItem)
            : base(innerItem)
        {

        }

        public static implicit operator TwoImagesTwoContentsItem(Item innerItem)
        {
            return innerItem != null ? new TwoImagesTwoContentsItem(innerItem) : null;
        }

        public static implicit operator Item(TwoImagesTwoContentsItem customItem)
        {
	        return customItem != null ? customItem.InnerItem : null;
        }
        #endregion Boilerplate CustomItem Code

        #region Field Instance Methods
        public CustomTextField Title
        {
            get
            {
                return new CustomTextField(InnerItem, InnerItem.Fields["Title"]);
            }
        }

        public CustomImageField ImageLeft
        {
            get
            {
                return new CustomImageField(InnerItem, InnerItem.Fields["Image Left"]);
            }
        }

        public CustomTextField LinkLeft
        {
            get
            {
                return new CustomTextField(InnerItem, InnerItem.Fields["Content Left"]);
            }
        }

        public CustomImageField ImageRight
        {
            get
            {
                return new CustomImageField(InnerItem, InnerItem.Fields["Image Right"]);
            }
        }

        public CustomTextField LinkRight
        {
            get
            {
                return new CustomTextField(InnerItem, InnerItem.Fields["Content Right"]);
            }
        }
        #endregion Field Instance Methods
    }
}
