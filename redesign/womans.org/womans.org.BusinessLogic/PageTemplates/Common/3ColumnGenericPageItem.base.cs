using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PageTemplates.Common
{
public partial class ThreeColumnGenericPageItem : CustomItem
{

public static readonly string TemplateId = "{E4890007-DA93-4881-BCF3-BCF9645E2F5E}";

#region Inherited Base Templates

private readonly BaseInnerPageItem _BaseInnerPageItem;
public BaseInnerPageItem BaseInnerPage { get { return _BaseInnerPageItem; } }
private readonly AccordionItem _AccordionItem;
public AccordionItem Accordion { get { return _AccordionItem; } }
private readonly TabsItem _TabsItem;
public TabsItem Tabs { get { return _TabsItem; } }

#endregion

#region Boilerplate CustomItem Code

public ThreeColumnGenericPageItem(Item innerItem) : base(innerItem)
{
	_BaseInnerPageItem = new BaseInnerPageItem(innerItem);
	_AccordionItem = new AccordionItem(innerItem);
	_TabsItem = new TabsItem(innerItem);

}

public static implicit operator ThreeColumnGenericPageItem(Item innerItem)
{
	return innerItem != null ? new ThreeColumnGenericPageItem(innerItem) : null;
}

public static implicit operator Item(ThreeColumnGenericPageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}