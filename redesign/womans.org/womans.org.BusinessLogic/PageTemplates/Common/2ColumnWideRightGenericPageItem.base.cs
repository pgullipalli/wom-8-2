using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PageTemplates.Common
{
public partial class TwoColumnWideRightGenericPageItem : CustomItem
{

public static readonly string TemplateId = "{860D6D54-F9CE-4DDC-A9FD-7A39FF18A9ED}";

#region Inherited Base Templates

private readonly BaseInnerPageItem _BaseInnerPageItem;
public BaseInnerPageItem BaseInnerPage { get { return _BaseInnerPageItem; } }
private readonly AccordionItem _AccordionItem;
public AccordionItem Accordion { get { return _AccordionItem; } }
private readonly TabsItem _TabsItem;
public TabsItem Tabs { get { return _TabsItem; } }

#endregion

#region Boilerplate CustomItem Code

public TwoColumnWideRightGenericPageItem(Item innerItem) : base(innerItem)
{
	_BaseInnerPageItem = new BaseInnerPageItem(innerItem);
	_AccordionItem = new AccordionItem(innerItem);
	_TabsItem = new TabsItem(innerItem);

}

public static implicit operator TwoColumnWideRightGenericPageItem(Item innerItem)
{
	return innerItem != null ? new TwoColumnWideRightGenericPageItem(innerItem) : null;
}

public static implicit operator Item(TwoColumnWideRightGenericPageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}