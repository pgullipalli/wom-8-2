using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PageTemplates.Common
{
public partial class FormPageItem : CustomItem
{

public static readonly string TemplateId = "{1C7AE697-8321-4468-85F1-7323727F13DC}";

#region Inherited Base Templates

private readonly BaseInnerPageItem _BaseInnerPageItem;
public BaseInnerPageItem BaseInnerPage { get { return _BaseInnerPageItem; } }

#endregion

#region Boilerplate CustomItem Code

public FormPageItem(Item innerItem) : base(innerItem)
{
	_BaseInnerPageItem = new BaseInnerPageItem(innerItem);

}

public static implicit operator FormPageItem(Item innerItem)
{
	return innerItem != null ? new FormPageItem(innerItem) : null;
}

public static implicit operator Item(FormPageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}