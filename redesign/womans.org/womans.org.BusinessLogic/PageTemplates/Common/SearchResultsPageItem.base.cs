using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PageTemplates.Common
{
public partial class SearchResultsPageItem : CustomItem
{

public static readonly string TemplateId = "{0C859AFB-CA4A-4EB6-8D4D-F1AF4E4335C0}";

#region Inherited Base Templates

private readonly BaseInnerPageItem _BaseInnerPageItem;
public BaseInnerPageItem BaseInnerPage { get { return _BaseInnerPageItem; } }

#endregion

#region Boilerplate CustomItem Code

public SearchResultsPageItem(Item innerItem) : base(innerItem)
{
	_BaseInnerPageItem = new BaseInnerPageItem(innerItem);

}

public static implicit operator SearchResultsPageItem(Item innerItem)
{
	return innerItem != null ? new SearchResultsPageItem(innerItem) : null;
}

public static implicit operator Item(SearchResultsPageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}