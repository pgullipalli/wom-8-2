using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PageTemplates.Common
{
public partial class TwoColumnWideLeftGenericPageItem : CustomItem
{

public static readonly string TemplateId = "{215F3E99-61DF-486A-93C1-326FE9BA32BE}";

#region Inherited Base Templates

private readonly BaseInnerPageItem _BaseInnerPageItem;
public BaseInnerPageItem BaseInnerPage { get { return _BaseInnerPageItem; } }
private readonly AccordionItem _AccordionItem;
public AccordionItem Accordion { get { return _AccordionItem; } }
private readonly TabsItem _TabsItem;
public TabsItem Tabs { get { return _TabsItem; } }

#endregion

#region Boilerplate CustomItem Code

public TwoColumnWideLeftGenericPageItem(Item innerItem) : base(innerItem)
{
	_BaseInnerPageItem = new BaseInnerPageItem(innerItem);
	_AccordionItem = new AccordionItem(innerItem);
	_TabsItem = new TabsItem(innerItem);

}

public static implicit operator TwoColumnWideLeftGenericPageItem(Item innerItem)
{
	return innerItem != null ? new TwoColumnWideLeftGenericPageItem(innerItem) : null;
}

public static implicit operator Item(TwoColumnWideLeftGenericPageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


//public CustomTextField SectionHeadline
//{
//    get
//    {
//        return new CustomTextField(InnerItem, InnerItem.Fields["Section Headline"]);
//    }
//}


#endregion //Field Instance Methods
}
}