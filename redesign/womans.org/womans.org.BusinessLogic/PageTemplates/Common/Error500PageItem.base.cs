using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PageTemplates.Common
{
public partial class Error500PageItem : CustomItem
{

public static readonly string TemplateId = "{897A80F9-1E56-4501-9A3B-97AAF138B1AF}";

#region Inherited Base Templates

private readonly BaseInnerPageItem _BaseInnerPageItem;
public BaseInnerPageItem BaseInnerPage { get { return _BaseInnerPageItem; } }

#endregion

#region Boilerplate CustomItem Code

public Error500PageItem(Item innerItem) : base(innerItem)
{
	_BaseInnerPageItem = new BaseInnerPageItem(innerItem);

}

public static implicit operator Error500PageItem(Item innerItem)
{
	return innerItem != null ? new Error500PageItem(innerItem) : null;
}

public static implicit operator Item(Error500PageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}