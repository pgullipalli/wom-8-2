using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.ComponentTemplates;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PageTemplates.Common
{
public partial class LandingPageItem : CustomItem
{

public static readonly string TemplateId = "{2BA0A451-9F5E-4CC3-98DB-70FA423F631C}";

#region Inherited Base Templates

private readonly LandingCTAItem _LandingCTAItem;
public LandingCTAItem LandingCTA { get { return _LandingCTAItem; } }
private readonly BaseInnerPageItem _BaseInnerPageItem;
public BaseInnerPageItem BaseInnerPage { get { return _BaseInnerPageItem; } }
private readonly AccordionItem _AccordionItem;
public AccordionItem Accordion { get { return _AccordionItem; } }
private readonly TabsItem _TabsItem;
public TabsItem Tabs { get { return _TabsItem; } }

#endregion

#region Boilerplate CustomItem Code

public LandingPageItem(Item innerItem) : base(innerItem)
{
	_LandingCTAItem = new LandingCTAItem(innerItem);
	_BaseInnerPageItem = new BaseInnerPageItem(innerItem);
	_AccordionItem = new AccordionItem(innerItem);
	_TabsItem = new TabsItem(innerItem);

}

public static implicit operator LandingPageItem(Item innerItem)
{
	return innerItem != null ? new LandingPageItem(innerItem) : null;
}

public static implicit operator Item(LandingPageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomImageField BackgroundImage
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Background Image"]);
	}
}


public CustomTextField ExpandTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Expand Title"]);
	}
}


public CustomTreeListField FooterFeatures
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Footer Features"]);
	}
}


public CustomTextField PinkCalloutHeader
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Pink Callout Header"]);
	}
}


public CustomTextField Quote
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Quote"]);
	}
}


public CustomTextField PinkCalloutBody
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Pink Callout Body"]);
	}
}


public CustomGeneralLinkField PinkCalloutLink
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Pink Callout Link"]);
	}
}


public CustomTextField PinkCalloutLinkText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Pink Callout Link Text"]);
	}
}


#endregion //Field Instance Methods
}
}