using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PageTemplates.Common
{
public partial class SectionPageItem : CustomItem
{

public static readonly string TemplateId = "{EB9123F6-9A29-4D07-907F-E83286182DAC}";

#region Inherited Base Templates

private readonly BaseSectionPageItem _BaseSectionPageItem;
public BaseSectionPageItem BaseSectionPage { get { return _BaseSectionPageItem; } }
private readonly AccordionItem _AccordionItem;
public AccordionItem Accordion { get { return _AccordionItem; } }
private readonly TabsItem _TabsItem;
public TabsItem Tabs { get { return _TabsItem; } }

#endregion

#region Boilerplate CustomItem Code

public SectionPageItem(Item innerItem) : base(innerItem)
{
	_BaseSectionPageItem = new BaseSectionPageItem(innerItem);
	_AccordionItem = new AccordionItem(innerItem);
	_TabsItem = new TabsItem(innerItem);

}

public static implicit operator SectionPageItem(Item innerItem)
{
	return innerItem != null ? new SectionPageItem(innerItem) : null;
}

public static implicit operator Item(SectionPageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}