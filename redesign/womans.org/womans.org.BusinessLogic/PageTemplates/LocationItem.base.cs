using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PageTemplates
{
public partial class LocationItem : CustomItem
{

public static readonly string TemplateId = "{BFE5CC2B-F045-4A05-8B9A-1BC4B5F49CB6}";

#region Inherited Base Templates

private readonly BaseInnerPageItem _BaseInnerPageItem;
public BaseInnerPageItem BaseInnerPage { get { return _BaseInnerPageItem; } }
private readonly DataImportItem _DataImportItem;
public DataImportItem DataImport { get { return _DataImportItem; } }

#endregion

#region Boilerplate CustomItem Code

public LocationItem(Item innerItem) : base(innerItem)
{
	_BaseInnerPageItem = new BaseInnerPageItem(innerItem);
	_DataImportItem = new DataImportItem(innerItem);

}

public static implicit operator LocationItem(Item innerItem)
{
	return innerItem != null ? new LocationItem(innerItem) : null;
}

public static implicit operator Item(LocationItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField LocationID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Location ID"]);
	}
}


public CustomTextField LocationName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Location Name"]);
	}
}


public CustomTextField Address1
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Address 1"]);
	}
}


public CustomGeneralLinkField Map
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Map"]);
	}
}


public CustomTextField Address2
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Address 2"]);
	}
}


public CustomTextField MediaTabContent
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Media Tab Content"]);
	}
}


public CustomTextField City
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["City"]);
	}
}


public CustomLookupField State
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["State"]);
	}
}


public CustomTextField Zip
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Zip"]);
	}
}


public CustomImageField Photo
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Photo"]);
	}
}


public CustomTextField MainPhone
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Main Phone"]);
	}
}


public CustomTextField AlternatePhone
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Alternate Phone"]);
	}
}


public CustomTextField MainFax
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Main Fax"]);
	}
}


public CustomTextField AppointmentPhone
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Appointment Phone"]);
	}
}


public CustomGeneralLinkField Website
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Website"]);
	}
}


public CustomTextField EmailAddress
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Email Address"]);
	}
}


public CustomCheckboxField AcceptingNewPatients
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Accepting New Patients"]);
	}
}


public CustomTextField OfficeHours
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Office Hours"]);
	}
}


public CustomTreeListField Department
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Department"]);
	}
}


public CustomTreeListField LocationType
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Location Type"]);
	}
}


//Could not find Field Type for Latitude


//Could not find Field Type for Longitude


public CustomCheckboxField ShowinLocationListings
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Show in Location Listings"]);
	}
}


public CustomCheckboxField DisplayPhysiciansAtThisLocation
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Display Physicians At This Location"]);
	}
}


#endregion //Field Instance Methods
}
}