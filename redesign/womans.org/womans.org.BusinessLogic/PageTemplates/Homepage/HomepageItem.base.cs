using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;


namespace Womans.CustomItems.PageTemplates.Homepage
{
public partial class HomepageItem : CustomItem
{

public static readonly string TemplateId = "{CC6CC6FB-6E54-440A-913C-737EDC8C5E18}";

#region Inherited Base Templates

private readonly SiteSettingsItem _SiteSettingsItem;
public SiteSettingsItem SiteSettings { get { return _SiteSettingsItem; } }
private readonly SiteRootItem _SiteRootItem;
public SiteRootItem SiteRoot { get { return _SiteRootItem; } }
private readonly BasePresentationItem _BasePresentationItem;
public BasePresentationItem BasePresentation { get { return _BasePresentationItem; } }

#endregion

#region Boilerplate CustomItem Code

public HomepageItem(Item innerItem) : base(innerItem)
{
	_SiteSettingsItem = new SiteSettingsItem(innerItem);
	_SiteRootItem = new SiteRootItem(innerItem);
	_BasePresentationItem = new BasePresentationItem(innerItem);
	

}

public static implicit operator HomepageItem(Item innerItem)
{
	return innerItem != null ? new HomepageItem(innerItem) : null;
}

public static implicit operator Item(HomepageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField ContainerPublicID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Container Public ID"]);
	}
}


public CustomTextField LeftTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Left Title"]);
	}
}


public CustomCheckboxField ShowPeelbackTab
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Show Peelback Tab"]);
	}
}


public CustomTextField LeftContent
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Left Content"]);
	}
}


public CustomTextField RightTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Right Title"]);
	}
}


public CustomTextField RightContent
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Right Content"]);
	}
}


public CustomTextField RightLinkButtonText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Right Link Button Text"]);
	}
}


public CustomGeneralLinkField RightLink
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Right Link"]);
	}
}


#endregion //Field Instance Methods
}
}