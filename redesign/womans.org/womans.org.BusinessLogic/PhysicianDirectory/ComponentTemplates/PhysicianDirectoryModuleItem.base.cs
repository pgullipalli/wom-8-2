using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PhysicianDirectory.ComponentTemplates
{
public partial class PhysicianDirectoryModuleItem : CustomItem
{

public static readonly string TemplateId = "{F594DE15-6390-4513-9016-3A875826FA1D}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public PhysicianDirectoryModuleItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator PhysicianDirectoryModuleItem(Item innerItem)
{
	return innerItem != null ? new PhysicianDirectoryModuleItem(innerItem) : null;
}

public static implicit operator Item(PhysicianDirectoryModuleItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Introduction
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Introduction"]);
	}
}


public CustomTextField KeywordQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Keyword Querystring"]);
	}
}


public CustomTextField PhysicianBrowserTitleFormat
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Physician Browser Title Format"]);
	}
}


public CustomLookupField PhysicianTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Physician Template"]);
	}
}


public CustomLookupField EducationTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Education Template"]);
	}
}


public CustomLookupField OfficeTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Office Template"]);
	}
}


public CustomTextField PageQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Page Querystring"]);
	}
}


public CustomTextField PhysicianMetaKeywordsFormat
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Physician Meta Keywords Format"]);
	}
}


public CustomTextField FirstNameQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["First Name Querystring"]);
	}
}


public CustomLookupField LanguageTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Language Template"]);
	}
}


public CustomLookupField LanguagesFolder
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Languages Folder"]);
	}
}


public CustomTextField PhysicianMetaDescriptionFormat
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Physician Meta Description Format"]);
	}
}


public CustomLookupField DepartmentTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Department Template"]);
	}
}


public CustomTextField LastNameQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Last Name Querystring"]);
	}
}


public CustomLookupField RangesFolder
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Ranges Folder"]);
	}
}


public CustomLookupField GendersFolder
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Genders Folder"]);
	}
}


public CustomLookupField InsuranceTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Insurance Template"]);
	}
}


public CustomLookupField DepartmentsFolder
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Departments Folder"]);
	}
}


public CustomTextField LanguagesQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Languages Querystring"]);
	}
}


public CustomLookupField DegreeTitlesFolder
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Degree Titles Folder"]);
	}
}


public CustomTextField GenderQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Gender Querystring"]);
	}
}


public CustomTreeListField LocationsFolder
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Locations Folder"]);
	}
}


public CustomTextField ZipQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Zip Querystring"]);
	}
}


public CustomLookupField GenderTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Gender Template"]);
	}
}


public CustomTextField DepartmentsQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Departments Querystring"]);
	}
}


public CustomLookupField FellowshipTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Fellowship Template"]);
	}
}


public CustomTextField FirstInitialLastNameQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["First Initial Last Name Querystring"]);
	}
}


public CustomLookupField InternshipTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Internship Template"]);
	}
}


public CustomTextField CityQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["City Querystring"]);
	}
}


public CustomLookupField ResidencyTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Residency Template"]);
	}
}


public CustomTextField FullNameQuerystring
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["FullName Querystring"]);
	}
}


public CustomLookupField LocationTemplate
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Location Template"]);
	}
}


#endregion //Field Instance Methods
}
}