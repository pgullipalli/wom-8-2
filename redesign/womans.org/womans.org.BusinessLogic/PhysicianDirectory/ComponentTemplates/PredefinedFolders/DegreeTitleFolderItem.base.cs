using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PhysicianDirectory.ComponentTemplates.PredefinedFolders
{
public partial class DegreeTitleFolderItem : CustomItem
{

public static readonly string TemplateId = "{6AFD38E1-0D05-4420-958B-B9F1906ED62A}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public DegreeTitleFolderItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator DegreeTitleFolderItem(Item innerItem)
{
	return innerItem != null ? new DegreeTitleFolderItem(innerItem) : null;
}

public static implicit operator Item(DegreeTitleFolderItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}