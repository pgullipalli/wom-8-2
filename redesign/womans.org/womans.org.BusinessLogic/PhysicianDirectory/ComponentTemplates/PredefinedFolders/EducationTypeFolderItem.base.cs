using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PhysicianDirectory.ComponentTemplates.PredefinedFolders
{
public partial class EducationTypeFolderItem : CustomItem
{

public static readonly string TemplateId = "{E4F9C79A-1B1F-43A7-B23E-8A449EA15E94}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }

#endregion

#region Boilerplate CustomItem Code

public EducationTypeFolderItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);

}

public static implicit operator EducationTypeFolderItem(Item innerItem)
{
	return innerItem != null ? new EducationTypeFolderItem(innerItem) : null;
}

public static implicit operator Item(EducationTypeFolderItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}