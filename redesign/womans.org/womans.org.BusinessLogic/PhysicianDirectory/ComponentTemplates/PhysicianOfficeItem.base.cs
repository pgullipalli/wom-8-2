using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PhysicianDirectory.ComponentTemplates
{
public partial class PhysicianOfficeItem : CustomItem
{

public static readonly string TemplateId = "{032E64D5-1433-460D-B2AD-3B50191CDAE1}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }
private readonly DataImportItem _DataImportItem;
public DataImportItem DataImport { get { return _DataImportItem; } }

#endregion

#region Boilerplate CustomItem Code

public PhysicianOfficeItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);
	_DataImportItem = new DataImportItem(innerItem);

}

public static implicit operator PhysicianOfficeItem(Item innerItem)
{
	return innerItem != null ? new PhysicianOfficeItem(innerItem) : null;
}

public static implicit operator Item(PhysicianOfficeItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField OfficeID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Office ID"]);
	}
}


public CustomTextField OfficeName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Office Name"]);
	}
}


public CustomCheckboxField AcceptingNewPatients
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Accepting New Patients"]);
	}
}


public CustomLookupField Location
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Location"]);
	}
}


public CustomTextField OfficePhone
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Office Phone"]);
	}
}


public CustomTextField OfficeFax
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Office Fax"]);
	}
}


public CustomCheckboxField PrimaryOffice
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Primary Office"]);
	}
}


public CustomTextField OfficeHours
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Office Hours"]);
	}
}


public CustomTextField EmailAddress
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Email Address"]);
	}
}


public CustomTextField OfficeAnsweringPhone
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Office Answering Phone"]);
	}
}


#endregion //Field Instance Methods
}
}