using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PhysicianDirectory.ComponentTemplates
{
public partial class EducationItem : CustomItem
{

public static readonly string TemplateId = "{83CC76FC-8EAD-4FA1-A2D5-955B03FE0D55}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }
private readonly DataImportItem _DataImportItem;
public DataImportItem DataImport { get { return _DataImportItem; } }

#endregion

#region Boilerplate CustomItem Code

public EducationItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);
	_DataImportItem = new DataImportItem(innerItem);

}

public static implicit operator EducationItem(Item innerItem)
{
	return innerItem != null ? new EducationItem(innerItem) : null;
}

public static implicit operator Item(EducationItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomLookupField EducationType
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Education Type"]);
	}
}


public CustomTextField InstitutionName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Institution Name"]);
	}
}


public CustomTextField Year
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Year"]);
	}
}


#endregion //Field Instance Methods
}
}