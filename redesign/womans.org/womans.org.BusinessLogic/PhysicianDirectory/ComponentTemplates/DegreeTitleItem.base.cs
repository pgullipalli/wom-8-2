using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PhysicianDirectory.ComponentTemplates
{
public partial class DegreeTitleItem : CustomItem
{

public static readonly string TemplateId = "{C4410DC2-5076-43EE-B842-149572D1A323}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }
private readonly DataImportItem _DataImportItem;
public DataImportItem DataImport { get { return _DataImportItem; } }

#endregion

#region Boilerplate CustomItem Code

public DegreeTitleItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);
	_DataImportItem = new DataImportItem(innerItem);

}

public static implicit operator DegreeTitleItem(Item innerItem)
{
	return innerItem != null ? new DegreeTitleItem(innerItem) : null;
}

public static implicit operator Item(DegreeTitleItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField DegreeTitleName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Degree Title Name"]);
	}
}


public CustomTextField DegreeTitleCode
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Degree Title Code"]);
	}
}


#endregion //Field Instance Methods
}
}