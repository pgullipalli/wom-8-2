using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PhysicianDirectory.ComponentTemplates
{
public partial class InternshipItem : CustomItem
{

public static readonly string TemplateId = "{83085D40-FAFC-479B-868F-4116D3562C62}";

#region Inherited Base Templates

private readonly BaseComponentItem _BaseComponentItem;
public BaseComponentItem BaseComponent { get { return _BaseComponentItem; } }
private readonly DataImportItem _DataImportItem;
public DataImportItem DataImport { get { return _DataImportItem; } }

#endregion

#region Boilerplate CustomItem Code

public InternshipItem(Item innerItem) : base(innerItem)
{
	_BaseComponentItem = new BaseComponentItem(innerItem);
	_DataImportItem = new DataImportItem(innerItem);

}

public static implicit operator InternshipItem(Item innerItem)
{
	return innerItem != null ? new InternshipItem(innerItem) : null;
}

public static implicit operator Item(InternshipItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField InstitutionName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Institution Name"]);
	}
}


public CustomTextField Year
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Year"]);
	}
}


#endregion //Field Instance Methods
}
}