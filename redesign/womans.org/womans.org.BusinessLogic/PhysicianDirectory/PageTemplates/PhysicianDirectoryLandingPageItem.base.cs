using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PhysicianDirectory.PageTemplates
{
public partial class PhysicianDirectoryLandingPageItem : CustomItem
{

public static readonly string TemplateId = "{1C543D1D-A049-4A00-B7B9-C66BC2ADB875}";

#region Inherited Base Templates

private readonly BaseSectionPageItem _BaseSectionPageItem;
public BaseSectionPageItem BaseSectionPage { get { return _BaseSectionPageItem; } }
private readonly ModuleRootItem _ModuleRootItem;
public ModuleRootItem ModuleRoot { get { return _ModuleRootItem; } }

#endregion

#region Boilerplate CustomItem Code

public PhysicianDirectoryLandingPageItem(Item innerItem) : base(innerItem)
{
	_BaseSectionPageItem = new BaseSectionPageItem(innerItem);
	_ModuleRootItem = new ModuleRootItem(innerItem);

}

public static implicit operator PhysicianDirectoryLandingPageItem(Item innerItem)
{
	return innerItem != null ? new PhysicianDirectoryLandingPageItem(innerItem) : null;
}

public static implicit operator Item(PhysicianDirectoryLandingPageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}