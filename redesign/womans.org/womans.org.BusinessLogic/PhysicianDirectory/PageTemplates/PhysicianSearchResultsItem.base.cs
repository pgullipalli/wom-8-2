using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.PhysicianDirectory.PageTemplates
{
public partial class PhysicianSearchResultsItem : CustomItem
{

public static readonly string TemplateId = "{2CDDDA0A-04A3-4F36-9AF8-CA504A8558E9}";

#region Inherited Base Templates

private readonly BaseInnerPageItem _BaseInnerPageItem;
public BaseInnerPageItem BaseInnerPage { get { return _BaseInnerPageItem; } }

#endregion

#region Boilerplate CustomItem Code

public PhysicianSearchResultsItem(Item innerItem) : base(innerItem)
{
	_BaseInnerPageItem = new BaseInnerPageItem(innerItem);

}

public static implicit operator PhysicianSearchResultsItem(Item innerItem)
{
	return innerItem != null ? new PhysicianSearchResultsItem(innerItem) : null;
}

public static implicit operator Item(PhysicianSearchResultsItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}