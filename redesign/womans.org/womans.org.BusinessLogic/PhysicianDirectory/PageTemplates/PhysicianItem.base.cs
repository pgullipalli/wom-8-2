using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;
using Womans.CustomItems.PhysicianDirectory.BaseTemplates;

namespace Womans.CustomItems.PhysicianDirectory.PageTemplates
{
public partial class PhysicianItem : CustomItem
{

public static readonly string TemplateId = "{5F74A738-83CB-4C97-B327-2F6FA22DDB70}";

#region Inherited Base Templates

private readonly BaseInnerPageItem _BaseInnerPageItem;
public BaseInnerPageItem BaseInnerPage { get { return _BaseInnerPageItem; } }
private readonly DataImportItem _DataImportItem;
public DataImportItem DataImport { get { return _DataImportItem; } }
private readonly GeolocationDataItem _GeolocationDataItem;
public GeolocationDataItem GeolocationData { get { return _GeolocationDataItem; } }

#endregion

#region Boilerplate CustomItem Code

public PhysicianItem(Item innerItem) : base(innerItem)
{
	_BaseInnerPageItem = new BaseInnerPageItem(innerItem);
	_DataImportItem = new DataImportItem(innerItem);
	_GeolocationDataItem = new GeolocationDataItem(innerItem);

}

public static implicit operator PhysicianItem(Item innerItem)
{
	return innerItem != null ? new PhysicianItem(innerItem) : null;
}

public static implicit operator Item(PhysicianItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Awards
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Awards"]);
	}
}


public CustomTextField FirstName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["First Name"]);
	}
}


public CustomTextField OverrideBrowserTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Override Browser Title"]);
	}
}


public CustomFileField Profile
{
	get
	{
		return new CustomFileField(InnerItem, InnerItem.Fields["Profile"]);
	}
}


public CustomTextField Certifications
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Certifications"]);
	}
}


public CustomTextField MediaTabContent
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Media Tab Content"]);
	}
}


public CustomTextField MiddleInitial
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Middle Initial"]);
	}
}


public CustomTextField OverrideMetaKeywords
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Override Meta Keywords"]);
	}
}


public CustomTextField Publications
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Publications"]);
	}
}


public CustomTextField Community
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Community"]);
	}
}


public CustomTextField LastName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Last Name"]);
	}
}


public CustomTextField Licenses
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Licenses"]);
	}
}


public CustomTextField OverrideMetaDescription
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Override Meta Description"]);
	}
}


//Could not find Field Type for Alternative Last Names


public CustomTextField Expertise
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Expertise"]);
	}
}


public CustomTextField Suffix
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Suffix"]);
	}
}


public CustomMultiListField Gender
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["Gender"]);
	}
}


public CustomTreeListField PositionTitles
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Position Titles"]);
	}
}


public CustomImageField Photo
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Photo"]);
	}
}


public CustomTreeListField DegreeTitles
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Degree Titles"]);
	}
}


public CustomTreeListField Departments
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Departments"]);
	}
}


public CustomTreeListField InsuranceAccepted
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Insurance Accepted"]);
	}
}


public CustomTreeListField LanguagesSpoken
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Languages Spoken"]);
	}
}


public CustomGeneralLinkField Website
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Website"]);
	}
}


public CustomTextField InternalHospitalID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Internal Hospital ID"]);
	}
}


#endregion //Field Instance Methods
}
}