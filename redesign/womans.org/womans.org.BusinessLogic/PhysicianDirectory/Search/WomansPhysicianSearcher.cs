﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MedTouch.Common.Helpers;
using MedTouch.PhysicianDirectory.Search;
using Sitecore;
using Sitecore.Collections;
using MedTouch.PhysicianDirectory.Helpers;
using Sitecore.Data.Items;
using Sitecore.SharedSource.Search.Parameters;
using Sitecore.SharedSource.Search.Utilities;
using Sitecore.Search;
using womans.org.BusinessLogic.Helpers;

namespace womans.org.BusinessLogic.PhysicianDirectory.Search
{
    public class WomansPhysicianSearcher : PhysicianSearcher
    {
        protected override void AddInjectedRefinement(PhysicianSearchParam physicianSearchParam, SafeDictionary<string> refinement)
        {
            WomansPhysicianSearchParam womansPhysicianSearchParam = (WomansPhysicianSearchParam) physicianSearchParam;
            if (womansPhysicianSearchParam != null)
            {
                if (!string.IsNullOrEmpty(womansPhysicianSearchParam.Group))
                {
                    string locationGuids = string.Empty;

                    SafeDictionary<string> refinements = new SafeDictionary<string>();
                    List<DateRangeSearchParam.DateRangeField> ranges = new List<DateRangeSearchParam.DateRangeField>();

                    refinements.Add("Location Name", GetLucenePhraseTermSearch(womansPhysicianSearchParam.Group));

                    List<Item> locationItems = SearchHelper.GetItemListFromInformationCollection(SitecoreSearchHelper.GetItems(ModuleHelper.SiteSearchIndexName(), Context.Language.CultureInfo.TwoLetterISOLanguageName, PhysicianHelper.GroupTemplateGuid(), PhysicianHelper.GroupFolder().ID.ToString(), string.Empty, string.Empty, refinements, ranges, QueryOccurance.Must));
                    foreach (var locationItem in locationItems)
                    {
                        if (
                            ItemHelper.GetFieldRawValue(locationItem, "Location Name")
                                .Equals(womansPhysicianSearchParam.Group, StringComparison.OrdinalIgnoreCase))
                        {
                            //locationGuids = string.IsNullOrWhiteSpace(locationGuids)
                            //    ? locationItem.ID.ToString()
                            //    : locationGuids + "|" + locationItem.ID.ToString();

                            locationGuids = locationGuids +
                                            (string.IsNullOrWhiteSpace(locationGuids)
                                                ? locationItem.ID.ToString()
                                                : ("|" + locationItem.ID.ToString()));
                        }

                    }

                    if (!string.IsNullOrWhiteSpace(locationGuids))
                    {
                        string str2 = IdHelper.ProcessGUIDs(locationGuids);
                        refinement.Add("locations", str2);
                    }
                }
            }
        }
    }
}
