﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MedTouch.PhysicianDirectory.Search;

namespace womans.org.BusinessLogic.PhysicianDirectory.Search
{
    public class WomansPhysicianSearchParam : PhysicianSearchParam
    {
        public WomansPhysicianSearchParam() : base()
        {
            this.Group = string.Empty;
        }

        public string Group { get; set; }
    }
}
