﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MedTouch.Common.Helpers;
using MedTouch.Common.Helpers.Utilities;
using MedTouch.Common.Helpers.Utilities.Base;
using MedTouch.PhysicianDirectory.Helpers;
using MedTouch.PhysicianDirectory.Helpers.Links;
using Sitecore.Collections;
using Sitecore.Links;
using Sitecore.Web;
using womans.org.BusinessLogic.Helpers;

namespace womans.org.BusinessLogic.PhysicianDirectory.Search
{
    public class WomansLinkHelper : ILinkHelper
    {
        private readonly IAssertor assertor = Assertor.Create();

        public string GetSearchResultsUrl(MedTouch.PhysicianDirectory.Search.PhysicianSearchParam physicianSearchParam, Sitecore.Data.Items.Item searchResultPageItem)
        {
            var womansPhysicianSearchParam = (WomansPhysicianSearchParam)physicianSearchParam;
            this.assertor.ArgumentNotNull(womansPhysicianSearchParam, "womansPhysicianSearchParam");
            
            string itemUrl = "/";
            if (searchResultPageItem != null)
            {
                itemUrl = LinkManager.GetItemUrl(searchResultPageItem);
            }
            SafeDictionary<string> dictionary = new SafeDictionary<string>();
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.Keyword))
            {
                dictionary.Add(ModuleHelper.KeywordQuerystring(), womansPhysicianSearchParam.Keyword);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.FirstName))
            {
                dictionary.Add(ModuleHelper.FirstNameQuerystring(), womansPhysicianSearchParam.FirstName);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.LastName))
            {
                dictionary.Add(ModuleHelper.LastNameQuerystring(), womansPhysicianSearchParam.LastName);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.FirstInitialLastName))
            {
                dictionary.Add(ModuleHelper.FirstInitLastNameQuerystring(), womansPhysicianSearchParam.FirstInitialLastName);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.FullName))
            {
                dictionary.Add(ModuleHelper.FullNameQuerystring(), womansPhysicianSearchParam.FullName);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.Zip))
            {
                dictionary.Add(ModuleHelper.ZipQuerystring(), womansPhysicianSearchParam.Zip);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.Departments))
            {
                dictionary.Add(ModuleHelper.DepartmentsQuerystring(), womansPhysicianSearchParam.Departments);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.Languages))
            {
                dictionary.Add(ModuleHelper.LanguagesQuerystring(), womansPhysicianSearchParam.Languages);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.Gender))
            {
                dictionary.Add(ModuleHelper.GenderQuerystring(), womansPhysicianSearchParam.Gender);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.Range))
            {
                dictionary.Add(BaseModuleHelper.RangeQuerystring(), womansPhysicianSearchParam.Range);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.Cities))
            {
                dictionary.Add(ModuleHelper.CityQuerystring(), womansPhysicianSearchParam.Cities);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.Taxonomies))
            {
                dictionary.Add(BaseModuleHelper.TaxonomyQuerystring(), womansPhysicianSearchParam.Taxonomies);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.Locations))
            {
                dictionary.Add(BaseModuleHelper.LocationsQuerystring(), womansPhysicianSearchParam.Locations);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.Specialties))
            {
                dictionary.Add(BaseModuleHelper.SpecialtiesQuerystring(), womansPhysicianSearchParam.Specialties);
            }
            if (!string.IsNullOrEmpty(womansPhysicianSearchParam.Group))
            {
                dictionary.Add(PhysicianHelper.GroupQuerystring(), womansPhysicianSearchParam.Group);
            }
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            using (System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<string, string>> enumerator = (System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<string, string>>)dictionary.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    System.Collections.Generic.KeyValuePair<string, string> current = enumerator.Current;
                    builder.Append((builder.Length > 0) ? string.Format("&{0}={1}", current.Key, StringHelper.EncodeString(current.Value)) : string.Format("{0}={1}", current.Key, StringHelper.EncodeString(current.Value)));
                }
            }
            return ((builder.Length > 0) ? string.Format("{0}?{1}", itemUrl, builder.ToString()) : itemUrl);
        }

        public void SubmitSearch(MedTouch.PhysicianDirectory.Search.PhysicianSearchParam physicianSearchParam, Sitecore.Data.Items.Item searchResultsPage)
        {
            WebUtil.Redirect(this.GetSearchResultsUrl(physicianSearchParam, searchResultsPage));
        }
    }
}
