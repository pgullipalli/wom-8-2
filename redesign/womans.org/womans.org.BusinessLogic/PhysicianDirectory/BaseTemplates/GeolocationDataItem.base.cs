using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.PhysicianDirectory.BaseTemplates
{
public partial class GeolocationDataItem : CustomItem
{

public static readonly string TemplateId = "{404B3973-9F9C-4250-9ECD-0541D278942B}";


#region Boilerplate CustomItem Code

public GeolocationDataItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator GeolocationDataItem(Item innerItem)
{
	return innerItem != null ? new GeolocationDataItem(innerItem) : null;
}

public static implicit operator Item(GeolocationDataItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


//Could not find Field Type for Max Latitude


//Could not find Field Type for Min Latitude


//Could not find Field Type for Max Longitude


//Could not find Field Type for Min Longitude


#endregion //Field Instance Methods
}
}