﻿namespace womans.org.BusinessLogic.UI.EventHandler
{
    using System;
    using System.Linq;
    using System.Net.Mail;
    using System.Text.RegularExpressions;
    using Sitecore.Links;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Diagnostics;
    using Sitecore.Form.Core.Client.Data.Submit;
    using Sitecore.Form.Core.Configuration;
    using Sitecore.Form.Core.Controls.Data;
    using Sitecore.Form.Core.Utility;
    using Sitecore.Form.Submit;
    using Sitecore.Forms.Core.Data;
    using MedTouch.Common.Helpers;
    using MedTouch.Calendar.Helpers;
    using MedTouch.OnlinePayments.SitecoreExtensions.WFFM.Actions;


    /// <summary>
    /// The purpose of this Save Action is to notify the recipients of all session registrations for each topic in a shopping cart transaction.
    /// 1 email is generated per topic, and recipient should normally be {registrar email}, to ensure the email gets sent to the event topic's contact person.
    /// Registrar will get a summary of all sessions registered for if {event topic summary} custom token is used in the mail body
    /// </summary>
    //public class SendShoppingCartRegistrationEmails : SendMail
    public class SendShoppingCartRegistrationEmails : SendOnlinePaymentsEmailMessage
    {
        private string _bodyResolved;
        private string _subjectResolved;
        private string _toResolved;
        private string _fromResolved;
        private string _ccResolved;
        private string _bccResolved;

        protected override void ExecuteMail(ID formID, AdaptedResultList fields)
        {
            

            var cart = ModuleHelper.ShoppingCart;
            if (cart != null)
            {

                // Initialize SMTP settings
                Item siteStartItem = ItemHelper.GetStartItem();
                var smtpClient = ModuleHelper.CalendarSMTPClient();
                _bodyResolved = this.FormatMail(base.Mail, fields);
                _subjectResolved = this.FormatMail(base.Subject, fields);
                _toResolved = this.FormatMail(base.To, fields);
                _ccResolved = this.FormatMail(base.CC, fields);
                _bccResolved = this.FormatMail(base.BCC, fields);
                foreach (var topic in cart.CartTopics.Where(t => t.CartSessions.Count > 0))
                {
                    try
                    {
                        if (!cart.HasSessions)
                        {
                            throw new InvalidOperationException("The cart does not have any sessions.");
                        }

                        if (!string.IsNullOrEmpty(_toResolved) && !string.IsNullOrEmpty(_bodyResolved)) {
                            Item eventTopicItem = ItemHelper.GetItemFromGuid(topic.TopicID);

                            //MailMessage mail = new MailMessage();
                            //mail.From = new MailAddress(base.From.Replace(";", ","));
                            //mail.Body = cart.ExpandCartTokens(_bodyResolved, topic);
                            //mail.Body = FormHelper.ExpandCustomTokens(mail.Body, eventTopicItem);
                            //mail.IsBodyHtml = true;
                            //mail.Subject = FormHelper.ExpandCustomTokens(_subjectResolved, eventTopicItem);
                            //mail.To.Add(new MailAddress(FormHelper.ExpandCustomTokens(_toResolved, eventTopicItem).Replace(";", ",")));
                            //if (!string.IsNullOrEmpty(_ccResolved))
                            //    mail.CC.Add(new MailAddress(FormHelper.ExpandCustomTokens(_ccResolved, eventTopicItem).Replace(";", ",")));
                            //if (!string.IsNullOrEmpty(_bccResolved))
                            //    mail.Bcc.Add(new MailAddress(FormHelper.ExpandCustomTokens(_bccResolved, eventTopicItem).Replace(";", ",")));
                            //mail.Body = Regex.Replace(mail.Body, "href=\"/",
                            //    string.Join(string.Empty, new string[] { "href=\"", Sitecore.Web.WebUtil.GetServerUrl(), "/" }));
                            //smtpClient.Send(mail);

                            Mail = cart.ExpandCartTokens(_bodyResolved, topic);
                            Mail = FormHelper.ExpandCustomTokens(Mail, eventTopicItem);
                            To = FormHelper.ExpandCustomTokens(_toResolved, eventTopicItem).Replace(";", ",");
                            if (!string.IsNullOrEmpty(_ccResolved)) CC = FormHelper.ExpandCustomTokens(_ccResolved, eventTopicItem).Replace(";", ",");
                            if (!string.IsNullOrEmpty(_bccResolved)) CC = FormHelper.ExpandCustomTokens(_bccResolved, eventTopicItem).Replace(";", ",");
                            Subject = FormHelper.ExpandCustomTokens(_subjectResolved, eventTopicItem);

                            // Make link URL's external
                            string serverURL = Sitecore.Web.WebUtil.GetServerUrl();
                            Mail = Regex.Replace(Mail, "href=\"/", string.Join(string.Empty, new string[] { "href=\"", serverURL, "/" }));
                            base.ExecuteMail(formID, fields);

                        }
                     }
                    catch (Exception exception) {
                        Exception innerException = exception;
                        if (exception.InnerException != null) {
                            innerException = exception.InnerException;
                        }
                        Log.Error("MedTouch.Calendar.layouts.modules.Calendar.WFFM.SaveAction.SendShoppingCartRegistrationEmails:Send Mail failed: to:" + this.To + " from:" + this.From, innerException, innerException);
                        throw innerException;
                    }
                }
            }
        }

        /// <summary>
        /// Expand WFFM Tag inside "[]" tag.
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        private string FormatMail(string mail, AdaptedResultList fields)
        {
            
            mail = LinkManager.ExpandDynamicLinks(mail, true);
            if (string.IsNullOrEmpty(mail))
                return "";

            //Replace Token from Standard WFFM Fields
            foreach (AdaptedControlResult result in fields)
            {
            //    FieldItem field = new FieldItem(StaticSettings.ContextDatabase.GetItem(result.FieldID));
            //    string str = result.Value;
            //    str =
            //        Regex.Replace(
            //            Regex.Replace(
            //                Regex.Replace(
            //                    FieldReflectionUtil.GetAdaptedValue(field, str),
            //                    "src=\"/sitecore/shell/themes/standard/~",
            //                    string.Join(string.Empty, new string[] { "src=\"", Sitecore.Web.WebUtil.GetServerUrl(), "/~" })),
            //                "href=\"/sitecore/shell/themes/standard/~",
            //                string.Join(string.Empty, new string[] { "href=\"", Sitecore.Web.WebUtil.GetServerUrl(), "/~" })),
            //            "on\\w*=\".*?\"",
            //            string.Empty);

            //    mail =
            //        mail.Replace(
            //            string.Concat(new object[] { "[<label id=\"", field.ID, "\">", field.Name, "</label>]" }),
            //            str ?? string.Empty);

            //    base.To = base.To.Replace(
            //        string.Join(string.Empty, new string[] { "[", result.FieldName, "]" }), result.Value);

            //    base.CC = base.CC.Replace(
            //        string.Join(string.Empty, new string[] { "[", result.FieldName, "]" }), result.Value);

            //    base.Subject = base.Subject.Replace(
            //        string.Join(string.Empty, new string[] { "[", result.FieldName, "]" }), result.Value);
            }
            return mail;
        }

    }
}
