﻿namespace womans.org.BusinessLogic.UI.EventHandler
{
    using System;
    using System.Text.RegularExpressions;
    using MedTouch.Calendar.Helpers;
    using MedTouch.Calendar.Helpers.ShoppingCart;
    using MedTouch.OnlinePayments.SitecoreExtensions.WFFM.Actions;
    using Sitecore.Data;
    using Sitecore.Form.Core.Client.Data.Submit;
    using Sitecore.Links;

    using womans.org.BusinessLogic.Helpers;

    using FormHelper = MedTouch.Calendar.Helpers.FormHelper;

    /// <summary>
    /// The purpose of this Save Action is to notify recipient that a shopping cart transaction has been processed
    /// (generally sent to user ([Email] token) and finance contact (using custom {finance email} token).
    /// 1 email is generated for this action, so {shopping cart summary} token should be used to put a full summary of the cart in the email body
    /// </summary>
    public class SendShoppingCartSummaryEmail : SendOnlinePaymentsEmailMessage
    {
        protected override void ExecuteMail(ID formId, AdaptedResultList adaptedResultList)
        {
            var cart = ModuleHelper.ShoppingCart;
            if (!cart.HasSessions)
            {
                throw new InvalidOperationException("The cart does not have any sessions.");
            }
            Mail = LinkManager.ExpandDynamicLinks(Mail, true);

            // kludge for the finance notification email: in this case, show each topic's internal code in cart summary
            // see Cart.cs (GetEventTopicSummary())
            if (FormHelper.IsFinanceEmail(To))
                cart.IncludeTopicCodeInCartSummary = true;

            var womanCart = ModuleHelper.ShoppingCart as WomansCart;
            Mail = womanCart.ExpandCartTokens(Mail);
            cart.IncludeTopicCodeInCartSummary = false;

            Mail = FormHelper.ExpandCustomTokens(Mail);
            To = FormHelper.ExpandCustomTokens(To);
            Subject = FormHelper.ExpandCustomTokens(Subject);

            // Make link URL's external
            string serverURL = Sitecore.Web.WebUtil.GetServerUrl();
            Mail = Regex.Replace(Mail, "href=\"/", string.Join(string.Empty, new string[] { "href=\"", serverURL, "/" }));
            base.ExecuteMail(formId, adaptedResultList);

        }

    }
}
