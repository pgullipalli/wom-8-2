﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MedTouch.Calendar.Entities;
using Sitecore.Web.UI.HtmlControls;

namespace womans.org.BusinessLogic.UI {
    using System.Globalization;

    using MedTouch.Calendar.Helpers.ExtensionMethods;
    using MedTouch.Common.Helpers;

    using Sitecore;

    using womans.org.BusinessLogic.Calendar;

    public class RegistrationAddEditControl : MedTouch.Calendar.UI.RegistrationAddEditControl {
        #region Controls
        protected Literal ltFitnessMemberID;
        protected Edit editFirstName;
        protected Edit editLastName;
        protected Edit editAddress;
        protected Edit editAddress2;
        protected Edit editCity;
        protected Edit editState;
        protected Edit editZipCode;
        protected Edit editPhoneNumber;
        protected Edit editEmail;
        protected Edit editPartnerName;
        protected Edit editPhysician;
        protected Edit txtInternalNotes;
        #endregion


        protected Literal ltFeeType;

        protected Literal ltPromoCodes;
        protected Literal ltPromoAmount ;

        protected override void UpdateEditUIFromRegistrationItem(ICalendar_EventRegistration eventRegistration)
        {
            this.ltTransactionID.Text = eventRegistration.TransactionID.ToString((IFormatProvider)CultureInfo.InvariantCulture);
            this.editFirstName.Value = eventRegistration.Firstname_Secure;
            this.editLastName.Value = eventRegistration.Lastname_Secure;
            this.editAddress.Value = eventRegistration.Address_Secure;
            this.editAddress2.Value = eventRegistration.Address2_Secure;
            this.editCity.Value = eventRegistration.City_Secure;
            this.editState.Value = eventRegistration.State_Secure;
            this.editZipCode.Value = eventRegistration.ZipCode_Secure;
            this.editPhoneNumber.Value = eventRegistration.PhoneNumber_Secure;
            this.editEmail.Value = eventRegistration.Email_Secure;
            this.txtFee.Value = CurrencyExtensions.GetUSCurrencyDisplay(eventRegistration.Fee);
            this.ltPaymentMethod.Text = eventRegistration.PaymentMethod;
            this.ltUpdatedDate.Text = eventRegistration.Updated.ToString((IFormatProvider)CultureInfo.InvariantCulture);
            this.ltSubmitedDate.Text = eventRegistration.Created.ToString((IFormatProvider)CultureInfo.InvariantCulture);
            this.ltPaymentTransactionID.Text = eventRegistration.PaymentTransactionID_Secure;
            this.ltPaymentAuthorizationCode.Text = eventRegistration.PaymentAuthorizationCode_Secure;
            this.UpdateEditUIFromRegistrationItem_Custom(eventRegistration);
            this.ltFeeType.Text = eventRegistration.FeeType;
            this.ltPromoCodes.Text = eventRegistration.PromoCodes;
            this.ltPromoAmount.Text = CurrencyExtensions.GetUSCurrencyDisplay(eventRegistration.PromoAmount);
            var womansReg = eventRegistration as womans.org.BusinessLogic.Entities.Calendar_EventRegistration;
            ltFitnessMemberID.Value = womansReg.FitnessMbrNumber;

            txtFee.Value = string.Format("{0:0.00}", eventRegistration.Fee);

            var regHelper = new WomansRegistrationHelper();
            this.txtInternalNotes.Value = regHelper.GetInternalNotes(eventRegistration.TransactionID);
        }

        protected override void UpdateEditUIFromRegistrationItem_Custom(ICalendar_EventRegistration eventRegistration)
        {
            Calendar_EventRegistration eventRegistration1 = eventRegistration as Calendar_EventRegistration;
            this.editPartnerName.Value= eventRegistration1.PartnerName_Secure;
            this.editPhysician.Value= eventRegistration1.Physician_Secure;
            this.ltDueDate.Text = eventRegistration1.DueDate_Secure;
            if (!string.IsNullOrEmpty(this.ltDueDate.Text) && DateUtil.IsIsoDate(this.ltDueDate.Text))
                this.ltDueDate.Text = ItemHelper.GetDateFromSitecoreIsoDate(this.ltDueDate.Text).ToString("d");
            this.ltOtherChildren.Text = eventRegistration1.OtherChildren_Secure;
            this.ltInsurance.Text = eventRegistration1.Insurance_Secure;
            this.ltHowHeard.Text = eventRegistration1.HowHeard_Secure;
        }

        protected override void OnOK(object sender, EventArgs args)
        {
            var eventReg = EventRegistration as Calendar_EventRegistration;
            eventReg.Firstname_Secure = this.editFirstName.Value;
            eventReg.Lastname_Secure = this.editLastName.Value;
            eventReg.Email_Secure = this.editEmail.Value;
            eventReg.Address_Secure = this.editAddress.Value;
            eventReg.Address2_Secure = this.editAddress2.Value;
            eventReg.City_Secure = this.editCity.Value;
            eventReg.State_Secure = this.editState.Value;
            eventReg.ZipCode_Secure = this.editZipCode.Value;
            eventReg.PhoneNumber_Secure = this.editZipCode.Value;
            eventReg.PartnerName_Secure = this.editPartnerName.Value;
            eventReg.Physician_Secure = this.editPhysician.Value;
            eventReg.Updated = DateTime.Now;
            this.CustomDBcontext.SaveChanges();
            base.OnOK(sender,args);
        }
    }
}
