﻿namespace womans.org.BusinessLogic.UI
{
    #region Usings

    using System;
    using System.ComponentModel;
    using System.Web.UI;
    using Sitecore.Form.Core.Attributes;
    using Sitecore.Form.Core.Visual;

    using Sitecore;
    using Sitecore.Form.Core.Attributes;
    using Sitecore.Form.Core.Visual;
    using Sitecore.Forms.Data;

    #endregion

    [ValidationProperty("Text")]
    public class DatePicker : Sitecore.Form.Web.UI.Controls.DatePicker
    {
        // Methods
        public DatePicker()
            : this(HtmlTextWriterTag.Div)
        {
        }

        public DatePicker(HtmlTextWriterTag tag)
            : base(tag)
        {
            base.classAtributes["DateFormat"] = "MM-dd-yyyy";
            base.classAtributes["startDate"] = "20000101T120000";
            base.classAtributes["endDate"] = DateUtil.ToIsoDate(DateTime.Now.AddYears(1).Date);
            this.Text = DateUtil.ToIsoDate(DateTime.Now);
        }

        #region Public Properties

        [VisualProperty("Display Format:", 50)]
        [DefaultValue("MM-dd-yyyy")]
        [VisualFieldType(typeof(DateFormatField))]
        public new string DateFormat
        {
            get
            {
                return this.classAtributes["DateFormat"];
            }
            set
            {
                this.classAtributes["DateFormat"] = value;
            }
        }

        #endregion
    }
}