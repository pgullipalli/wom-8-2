﻿namespace womans.org.BusinessLogic.Pipelines.DispatchNewsletter
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using MedTouch.Common.Helpers;

    using Sitecore.Configuration;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Modules.EmailCampaign.Core.Pipelines.DispatchNewsletter;
    using Sitecore.Publishing;
    using Sitecore.SecurityModel;

    public class PublishSentMessage
    {
        public void Process(DispatchNewsletterArgs args)
        {
            PublishMessage msg = new PublishMessage();
            msg.Process(args);
        }
    }
}
