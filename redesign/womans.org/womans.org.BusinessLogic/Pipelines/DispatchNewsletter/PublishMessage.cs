﻿namespace womans.org.BusinessLogic.Pipelines.DispatchNewsletter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using MedTouch.Common.Helpers;

    using Sitecore.Configuration;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Modules.EmailCampaign.Core.Pipelines.DispatchNewsletter;
    using Sitecore.Publishing;
    using Sitecore.SecurityModel;

    public class PublishMessage
    {
        public void Process(DispatchNewsletterArgs args)
        {
            Database masterDb = Factory.GetDatabase("master");
            Item publishingTargetsItem = masterDb.GetItem("/sitecore/system/Publishing targets");
            Item managerRoot = args.Message.ManagerRoot.InnerItem;

            foreach (Item publishingTargetItem in publishingTargetsItem.Children)
            {
                string database = ItemHelper.GetFieldRawValue(publishingTargetItem, "Target database");
                if (string.IsNullOrEmpty(database))
                {
                    continue;
                }

                this.PublishItem(args, string.Format("{0}/Messages", managerRoot.Paths.FullPath), database);
                this.PublishItem(args, string.Format("{0}/Recipient Lists", managerRoot.Paths.FullPath), database);
                this.PublishItem(args, "/sitecore/media library/Images/Newsletter", database);
                this.PublishItem(args, "/sitecore/system/Marketing Center/Campaigns/Emails", database);
                this.PublishItem(args, "/sitecore/system/Marketing Center/Engagement Plans/Email Campaign/Emails", database);
            }

        }

        private void PublishItem(DispatchNewsletterArgs args, string path, string database)
        {
            using (new SecurityDisabler())
            {
                PublishOptions options = new PublishOptions(args.Message.InnerItem.Database, Factory.GetDatabase(database), PublishMode.SingleItem, args.Message.InnerItem.Language, DateTime.UtcNow)
                {
                    Deep = true,
                };

                Publisher publisher = new Publisher(options);
                Item publishItem = args.Message.InnerItem.Database.GetItem(path);
                if (publishItem == null)
                {
                    return;
                }

                options.RootItem = publishItem;
                publisher.Publish();
            }
        }
    }
}
