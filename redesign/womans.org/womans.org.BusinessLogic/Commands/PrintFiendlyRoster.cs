﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PrinterFriendlyRoster.cs" company="MedTouch">
//   MedTouch
// </copyright>
// --------------------------------------------------------------------------------------------------------------------



namespace womans.org.BusinessLogic.Commands
{
    using System;
    using System.Linq;
    using System.Web;

    using MedTouch.Calendar.Entities;
    using MedTouch.Calendar.Helpers;
    using MedTouch.Calendar.Helpers.ObjectModel;
    using MedTouch.Calendar.UI;
    using MedTouch.Common.Helpers;

    using Sitecore;
    using Sitecore.Data.Items;
    using Sitecore.Diagnostics;
    using Sitecore.Links;
    using Sitecore.Shell.Framework.Commands;
    using Sitecore.StringExtensions;
    using Sitecore.Web.UI.Sheer;
    using Sitecore.Web.UI.WebControls;
    using Sitecore.Web.UI.XamlSharp.Continuations;


    /// <summary>
    ///     The printer friendly roster.
    /// </summary>
    public class PrinterFriendlyRoster : Command, ISupportsContinuation
    {
        #region Constants

        /// <summary>
        ///     The pdf file name prefix.
        /// </summary>
        private const string PdfFileNamePrefix = "EventRoster";

        #endregion

        #region Fields

        /// <summary>
        /// The custom form.
        /// </summary>
        protected bool CustomForm;

        /// <summary>
        ///     The custom d bcontext.
        /// </summary>
        private CustomEntities customDBcontext;

        /// <summary>
        /// Whether the form should show contact details or not
        /// </summary>
        protected bool NoContactDetails;

        /// <summary>
        /// The ID of the Form
        /// </summary>
        protected string CustomFormID;

        /// <summary>
        ///     The event roster url.
        /// </summary>
        private string eventRosterUrl = string.Empty;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the custom d bcontext.
        /// </summary>
        protected CustomEntities CustomDBcontext
        {
            get
            {
                return this.customDBcontext ?? (this.customDBcontext = new CustomEntities());
            }
        }

        /// <summary>
        ///     Gets the session item id.
        /// </summary>
        protected string SessionItemId { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public override void Execute(CommandContext context)
        {
            Assert.IsNotNull(context, "CommandContext context is null");

            // get the mandatory session ID
            if (string.IsNullOrEmpty(this.SessionItemId))
            {
                return;
            }

            this.eventRosterUrl = string.Empty;
            bool.TryParse(context.Parameters["CustomForm"], out this.CustomForm);
            bool.TryParse(context.Parameters["NoContactDetails"], out this.NoContactDetails);
            this.CustomFormID = context.Parameters["CustomFormID"];

            var clientArgs = new ClientPipelineArgs();
            clientArgs.Parameters.Add(SitecoreUIConstants.AttributeName.EventSessionItemGuid, this.SessionItemId);

            Assert.IsNotNull(ContinuationManager.Current, "The current continuation manager is null.");

            this.DownloadPdf();
        }

        /// <summary>
        /// The query state.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="CommandState"/>.
        /// </returns>
        public override CommandState QueryState(CommandContext context)
        {
            Error.AssertObject(context, "context");

            // get the mandatory session ID
            this.SessionItemId = context.Parameters[SitecoreUIConstants.AttributeName.EventSessionItemGuid];
            return string.IsNullOrEmpty(this.SessionItemId) ? CommandState.Disabled : CommandState.Enabled;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The download pdf.
        /// </summary>
        /// <param name="sessionItemId">
        ///     The session item id.
        /// </param>
        protected void DownloadPdf()
        {
            var strRosterPage = ModuleHelper.Config.ModuleSettings.EventRosterPage;
            var eventRosterPage = ItemHelper.GetItemFromGuid(strRosterPage, ModuleHelper.CalendarGetCurrentDatabase());
            Assert.IsNotNull(eventRosterPage, "Printer Friendly event roster page configuration missing");

            // This will temporarily set the site to the real website so site prefix for links will be correct
            // When in Sitecore manager, site = "shell", and we need links relative to "website"
            var oldSiteName = Context.GetSiteName();
            Context.SetActiveSite("website");

            var eventSession = new EventSession(ItemHelper.GetItemFromGuid(this.SessionItemId));
            var outFileName = string.Format("{0} {1}", PdfFileNamePrefix, eventSession.Name);

            this.SetEventRosterUrl(eventSession, eventRosterPage);

            this.eventRosterUrl += "?esid=" + this.SessionItemId;
            var sitePrefix = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host;
            var fullURL = sitePrefix + this.eventRosterUrl;

            var siteStartItem = ItemHelper.GetStartItem();
            var headerText = ItemHelper.GetFieldHtmlValue(
                siteStartItem,
                ItemMapper.CalendarModuleSiteSettings.FieldName.ReportHeadline);
            var footerText = "This roster is current as of " + DateTime.Now.ToString("MMMM dd, yyyy");

            Context.SetActiveSite(oldSiteName);

            // TESTING
            // string tmpURL =
            // "http://local.sc660rev130529.medtouch.com/calendar-premium/printer-friendly-roster-page?esid={B26DC547-5510-49FD-B29F-A387E5DD8304}";
            var exportData = HTMLPDFHelper.GetPdfDownloadBytes(
                fullURL + "&PDF=true",
                headerText,
                footerText,
                true,
                true);

            SheerResponse.Eval(SheerUiHelper.CreateSheerUiFileDownload(exportData, outFileName, "application/pdf", this.SessionItemId));
        }

        /// <summary>
        /// The set event roster url.
        /// </summary>
        /// <param name="eventSession">
        /// The event session.
        /// </param>
        /// <param name="eventRosterPage">
        /// The event roster page.
        /// </param>
        private void SetEventRosterUrl(Item eventSession, Item eventRosterPage)
        {
            if (this.CustomForm)
            {
                if (!string.IsNullOrEmpty(this.CustomFormID))
                {
                    var renderingPage = ItemHelper.GetItemFromGuid(this.CustomFormID);
                    if (renderingPage != null)
                    {
                        var url = ItemHelper.GetItemUrl(renderingPage);

                        if (!url.IsNullOrEmpty())
                        {
                            this.eventRosterUrl = url;
                            return;
                        }
                    }
                }
                var parentEvent = ItemHelper.GetFirstAncestorByTemplate(eventSession, "Event Topic");

                var additionalForm = ItemHelper.GetItemFromReferenceField(
                    parentEvent,
                    ItemMapper.EventTopic.FieldName.AdditionalForm);

                if (additionalForm != null)
                {
                    var renderingPageItem =
                        additionalForm.Children.FirstOrDefault(
                            x => x.TemplateName == ItemMapper.CustomFormsPdfRenderingPageParameter.TemplateName);
                    if (renderingPageItem != null)
                    {
                        var renderingPage = ItemHelper.GetItemFromGuid(ItemHelper.GetFieldRawValue(
                            renderingPageItem,
                            ItemMapper.CustomFormsPdfRenderingPageParameter.FieldName.RenderingPage));

                        var url = ItemHelper.GetItemUrl(renderingPage);

                        if (!url.IsNullOrEmpty())
                        {
                            this.eventRosterUrl = url;
                        }
                    }
                }
                else
                {
                    SheerResponse.Alert(
                        "There is no custom form associated with this event, the default roster will be printed instead.");
                }
            }

            if (this.eventRosterUrl.IsNullOrEmpty())
            {
                this.eventRosterUrl = LinkManager.GetItemUrl(eventRosterPage);
            }
        }

        #endregion
    }
}
