﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using MedTouch.Calendar.Commands;
using MedTouch.Calendar.Entities;
using MedTouch.Calendar.Helpers;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
namespace womans.org.BusinessLogic.Commands {
    using womans.org.BusinessLogic.Calendar;

    class RegistrationExportToExcel : MedTouch.Calendar.Commands.RegistrationExportToExcel {

        /// <summary>
        ///     Write column headings for the Womans project
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected override void WriteColumnNames_Custom() {
            AddComma("Fitness Member ID");
            AddComma("Attendee Name");
            AddComma("Guest Name");
            AddComma("Internal notes");
        }

        /// <summary>
        ///     Write rows for the DEMO base module custom fields (supporting the Additional Forms feature)
        ///     Projects should override this method to write row data for project-specific Calendar_EventRegistration fields
        ///     If no project-specific fields, override as empty method
        /// </summary>
        /// <param name="eventRegistration">
        /// </param>
        protected override void WriteEventRegistrationInfo_Custom(ICalendar_EventRegistration eventRegistration) {
            var womansReg = eventRegistration as womans.org.BusinessLogic.Entities.Calendar_EventRegistration;
            AddComma(womansReg.FitnessMbrNumber.ToString(CultureInfo.InvariantCulture));

            var projectReg = eventRegistration as Calendar_EventRegistration; // Use entity type for project here
            this.AddComma(projectReg.PartnerName_Secure.ToString(CultureInfo.InvariantCulture));
            this.AddComma(projectReg.Physician_Secure.ToString(CultureInfo.InvariantCulture));
            var regHelper = new WomansRegistrationHelper();
            this.AddComma(regHelper.GetInternalNotes(eventRegistration.TransactionID));
        }
    }

}
