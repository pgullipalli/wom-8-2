﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Sitecore.IO;
using Sitecore.Modules.EmailCampaign;
using Sitecore.Modules.EmailCampaign.Core;
using Sitecore.Modules.EmailCampaign.UI.Controls;
using Sitecore.Modules.EmailCampaign.UI.HtmlControls;
using Sitecore.Resources;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.Sheer;

namespace womans.org.BusinessLogic.ECM
{
  public class ImportUsersWizard : Sitecore.Modules.EmailCampaign.UI.Dialogs.ImportUsersWizard
  {
    // Fields

    private const string DeleteImgSrc = "Applications/16x16/delete2.png";
    private const string DelSectionPrefix = "DelSection";

    private const int DomainThreshold = 15;
    private List<string> fieldList;
    private const string FieldPrefix = "Field";

    private Dictionary<string, string> mappedProperties;

    private Control parentControl;
    private List<string> propertyList;
    private const string PropertyPrefix = "Property";

    private readonly ITargetAudienceRepository targetAudienceRepository = Factory.Instance.GetTargetAudienceRepository();


    protected override bool TargetAudiencesCompleted()
    {
      ImportOptions options = new ImportOptions
      {
        Filename = FileUtil.MapPath("/temp/" + FileUtil.GetFileName(this.Filename.Value)),
        MappedProperties = this.MappedProperties,
        Root = this.Root,
        DomainName = this.DomainInput.Value
      };
      string str = Sitecore.Context.ClientPage.ClientRequest.Form[this.SkipUser.Name];
      if (string.IsNullOrEmpty(str) || str.Equals(this.SkipUser.Value))
      {
        options.ConflictOption = ImportOptions.ConflictOptions.SkipUser;
      }
      else
      {
        options.ConflictOption = str.Equals(this.OverwriteProperties.Value) ? ImportOptions.ConflictOptions.OverwriteProperties : ImportOptions.ConflictOptions.KeepProperties;
      }
      List<string> roles = new List<string>();
      try
      {
        this.AddOptInRoles(roles);
        this.AddOptOutRoles(roles);
        this.AddAdvancedRoles(roles);
      }
      catch (Exception exception)
      {
        SheerResponse.Alert(exception.Message, new string[0]);
        return false;
      }
      options.Roles = roles.ToArray();

      JobHelper.StartJob("Import Users", "PerformImport", new ECM.RecipientImporter(), new object[] { options });
      this.CheckImport();
      return true;
    }

    private void AddAdvancedRoles(List<string> roles)
    {
      if (this.AdvancedOptions.Checked)
      {
        foreach (string str in this.RoleList.Value.Split(new char[] { '|' }))
        {
          if (!((str.Length <= 0) || roles.Contains(str)))
          {
            roles.Add(str);
          }
        }
      }
    }

    private void AddDelSection(string idPostfix)
    {
      ImageBuilder builder = new ImageBuilder
      {
        Src = "Applications/16x16/delete2.png",
        Width = 0x10,
        Height = 0x10,
        Style = "cursor: pointer"
      };
      Sitecore.Web.UI.HtmlControls.Literal child = new Sitecore.Web.UI.HtmlControls.Literal
      {
        Text = builder.ToString()
      };
      Border border = new Border
      {
        ID = "DelSection" + idPostfix,
        Click = "DelSection_Click"
      };
      border.Controls.Add(child);
      this.ParentControl.Controls.Add(border);
    }

    private void AddOptOutRoles(List<string> roles)
    {
      foreach (ListviewItem item in this.OptOutOf.Items)
      {
        TargetAudience targetAudience = this.targetAudienceRepository.GetTargetAudience(item.Value);
        if (targetAudience != null)
        {
          string domain = this.DomainCombobox.Value;
          string str2 = targetAudience.Storage.CreateRoleToExtraOptOut(domain);
          if (!string.IsNullOrEmpty(str2))
          {
            roles.Add(str2);
          }
        }
      }
    }


    private void AddFieldCombobox(string idPostfix, string defValue)
    {
      TraceableCombobox child = new TraceableCombobox
      {
        ID = "Field" + idPostfix,
        Width = new Unit(100.0, UnitType.Percentage),
        SelectOnly = true
      };
      child.SetList(this.FieldList);
      child.Change = "FieldChanged";
      if (!string.IsNullOrEmpty(defValue))
      {
        foreach (string str in this.FieldList)
        {
          if (defValue.Equals(str, StringComparison.OrdinalIgnoreCase))
          {
            child.Value = str;
            break;
          }
        }
      }
      if (string.IsNullOrEmpty(child.Value))
      {
        child.Value = this.FormatDefaultText(EcmTexts.Localize("select to add field", new object[0]));
      }
      this.ParentControl.Controls.Add(child);
    }

    private void AddOptInRoles(List<string> roles)
    {
      foreach (ListviewItem item in this.SubscribeTo.Items)
      {
        TargetAudience targetAudience = this.targetAudienceRepository.GetTargetAudience(item.Value);
        if (targetAudience != null)
        {
          string domain = this.DomainCombobox.Value;
          string str2 = targetAudience.Storage.CreateRoleToExtraOptIn(domain);
          if (!string.IsNullOrEmpty(str2))
          {
            roles.Add(str2);
          }
        }
      }
    }


    private void AddPropertyCombobox(string idPostfix, string defValue)
    {
      TraceableCombobox child = new TraceableCombobox
      {
        ID = "Property" + idPostfix,
        Width = new Unit(100.0, UnitType.Percentage),
        SelectOnly = true
      };
      child.SetList(this.PropertyList);
      if (string.IsNullOrEmpty(defValue))
      {
        child.Value = this.FormatDefaultText(EcmTexts.Localize("Select property", new object[0]));
      }
      else
      {
        foreach (string str in this.PropertyList)
        {
          if (defValue.Equals(str, StringComparison.OrdinalIgnoreCase))
          {
            child.Value = str;
            break;
          }
        }
      }
      this.ParentControl.Controls.Add(child);
    }

    private string AddRow(string prevIDPostfix)
    {
      return this.AddRow(prevIDPostfix, string.Empty);
    }

    private string AddRow(string prevIDPostfix, string defProperty)
    {
      if (this.ParentControl == null)
      {
        return string.Empty;
      }
      string uniqueID = Control.GetUniqueID(string.Empty);
      if (!string.IsNullOrEmpty(prevIDPostfix))
      {
        this.AddDelSection(prevIDPostfix);
      }
      this.AddFieldCombobox(uniqueID, defProperty);
      this.AddPropertyCombobox(uniqueID, defProperty);
      return uniqueID;
    }




    private void FillDomains()
    {
      List<Sitecore.Security.Domains.Domain> list = new List<Sitecore.Security.Domains.Domain>(Sitecore.Context.User.Delegation.GetManagedDomains());
      if (list.Count <= 15)
      {
        this.DomainCombobox.Visible = true;
        this.DomainCombobox.SetList(list.ConvertAll<string>(d => d.Name.ToString()));
      }
      else
      {
        this.DomainPanel.Visible = true;
      }
    }

    private void FillTargetAudiences()
    {
      ListviewItemMod mod;
      if (this.CurrentRecipientListId != null)
      {
        TargetAudience targetAudience = this.targetAudienceRepository.GetTargetAudience(this.CurrentRecipientListId);
        mod = new ListviewItemMod
        {
          ID = Control.GetUniqueID("I"),
          Icon = targetAudience.Icon,
          Header = targetAudience.Name,
          Value = targetAudience.ID
        };
        if (this.FillOptIn)
        {
          this.SubscribeTo.Controls.Add(mod);
        }
        else
        {
          this.OptOutOf.Controls.Add(mod);
        }
      }
      else
      {
        foreach (TargetAudience audience in this.Root.GetTargetAudiences())
        {
          mod = new ListviewItemMod
          {
            ID = Control.GetUniqueID("I"),
            Icon = audience.Icon,
            Header = audience.Name,
            Value = audience.ID
          };
          this.AlltargetAudiences.Controls.Add(mod);
        }
      }
    }


    private string FormatDefaultText(string text)
    {
      return HttpUtility.HtmlEncode("<" + text + ">");
    }

    private void InitializeFields()
    {
      if (this.ParentControl != null)
      {
        for (int i = this.ParentControl.Controls.Count - 1; i >= 0; i--)
        {
          System.Web.UI.Control control = this.ParentControl.Controls[i];
          if (!string.IsNullOrEmpty(control.ID) && ((control.ID.StartsWith("Field") || control.ID.StartsWith("Property")) || control.ID.StartsWith("DelSection")))
          {
            this.ParentControl.Controls.Remove(control);
          }
        }
        string prevIDPostfix = this.AddRow(string.Empty, "Email");
        foreach (string str2 in this.FieldList)
        {
          if (!"Email".Equals(str2, StringComparison.OrdinalIgnoreCase))
          {
            foreach (string str3 in this.PropertyList)
            {
              if (str2.Equals(str3, StringComparison.OrdinalIgnoreCase))
              {
                prevIDPostfix = this.AddRow(prevIDPostfix, str3);
              }
            }
          }
        }
        this.AddRow(prevIDPostfix);
        SheerResponse.Refresh(this.FieldsSection);
      }
    }

    private void UpdateForm(string results)
    {
      if (!string.IsNullOrEmpty(results))
      {
        string[] strArray = results.Split(new char[] { '|' });
        if (strArray.Length >= 4)
        {
          this.NumImported.Text = strArray[0];
          this.NumEmailExists.Text = strArray[1];
          this.NumNoEmail.Text = strArray[2];
          this.NumBroken.Text = strArray[3];
          SheerResponse.Refresh(this.Results);
        }
      }
    }
  }
}