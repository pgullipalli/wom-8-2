﻿using System;
using System.Web.Security;
using Sitecore.Diagnostics;
using Sitecore.Modules.EmailCampaign;
using Sitecore.Modules.EmailCampaign.Core;
using Sitecore.Security.Accounts;
using Sitecore.SecurityModel.Cryptography;

namespace womans.org.BusinessLogic.ECM
{
    public class TypeResolver : Sitecore.Modules.EmailCampaign.Core.TypeResolver
    {
        public override Contact GetCorrectAnonymouseFromEmail(string email, ManagerRoot root)
        {
            Contact contact = null;
            if ((root != null) && Util.IsValidEmail(email))
            {
                string commonDomain = root.Settings.CommonDomain;
                Assert.IsNotNullOrEmpty(commonDomain, EcmTexts.Localize("The Common Domain setting is not set.", new object[0]));
                MembershipUserCollection users = Membership.FindUsersByEmail(email);
                foreach (MembershipUser user in users)
                {
                    Contact contact2 = Contact.FromName(user.UserName);
                    if (contact2.IsAnonymousSubscriber)
                    {
                        contact = contact2;
                        if (contact2.InnerUser.Domain.Name.Equals(commonDomain, StringComparison.OrdinalIgnoreCase))
                        {
                            break;
                        }
                    }
                }
                if (contact == null)
                {
                    contact = CreateAnonymous(email, root);
                }
            }
            return contact;
        }

        protected static Contact CreateAnonymous(string localName, ManagerRoot root)
        {
            Contact contact = null;
            if ((root != null) && !string.IsNullOrEmpty(localName))
            {
                string commonDomain = root.Settings.CommonDomain;
                Assert.IsNotNullOrEmpty(commonDomain, EcmTexts.Localize("The Common Domain setting is not set.", new object[0]));
                string username = commonDomain + @"\" + Util.AddressToUserName(localName);
                while (User.Exists(username))
                {
                    username = username + "_";
                }
                string password = new PasswordGenerator { MinimumCharacters = 14 }.Generate() + Sitecore.Configuration.Settings.GetSetting("custom.default.password", "!w6h");
                Membership.CreateUser(username, password, localName);
                contact = Contact.FromName(username);
                contact.Profile.SetCustomProperty("IsAnonymousSubscriber", "true");
                contact.Profile.ProfileItemId = root.Settings.SubscriberProfile;
                contact.Profile.Save();
            }
            return contact;
        }
    }
}
