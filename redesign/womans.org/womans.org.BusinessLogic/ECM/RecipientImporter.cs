﻿
using System;
using System.Collections.Generic;
using System.Web.Security;
using Sitecore.Diagnostics;
using Sitecore.Modules.EmailCampaign;
using Sitecore.Modules.EmailCampaign.Core;
using Sitecore.Security.Accounts;
using System.Linq;
using System.Text.RegularExpressions;

namespace womans.org.BusinessLogic.ECM
{
  public class RecipientImporter : Sitecore.Modules.EmailCampaign.Core.RecipientImporter
  {
    public const string EmailKey = "Email";
    protected const string FullnameKey = "Fullname";
    protected const string NameKey = "Name";
    protected const string PhoneKey = "Phone";

    private void AddUserToRoles(ImportOptions options, string userName)
    {
      string[] assignedRoles = Roles.GetRolesForUser(userName);
      List<string> list = (from role in options.Roles
                           where !assignedRoles.Any<string>(r => (r == role))
                           select role).ToList<string>();
      if (list.Count > 0)
      {
        Roles.AddUserToRoles(userName, list.ToArray());
      }
    }

    private int GetEmailColumnIndex(ImportOptions options, List<string> headers)
    {
      int num = -1;
      for (int i = 0; i < headers.Count; i++)
      {
        if (options.MappedProperties["Email"].Equals(headers[i]))
        {
          num = i;
          options.MappedProperties.Remove("Email");
          return num;
        }
      }
      return num;
    }

    public override string PerformImport(ImportOptions options)
    {
      Assert.ArgumentNotNull(options, "options");
      int num = 0;
      int num2 = 0;
      int num3 = 0;
      int num4 = 0;
      CsvFile file = new CsvFile(options.Filename);
      try
      {
        List<string> headers = file.ReadLine();
        int emailColumnIndex = this.GetEmailColumnIndex(options, headers);
        if (emailColumnIndex < 0)
        {
          return string.Empty;
        }
        List<string> list2 = file.ReadLine();
        while (list2 != null)
        {
          bool flag = false;
          if (headers.Count < list2.Count)
          {
            num4++;
            list2 = file.ReadLine();
          }
          else
          {
            Contact contactFromName;
            string str = list2[emailColumnIndex];
            if (!Util.IsValidEmail(str))
            {
              str = this.TryFindEmail(str);
              if (string.IsNullOrEmpty(str))
              {
                num3++;
                list2 = file.ReadLine();
                continue;
              }
            }
            string username = options.DomainName + @"\" + Util.AddressToUserName(str);
            if (User.Exists(username))
            {
              if (options.ConflictOption == ImportOptions.ConflictOptions.SkipUser)
              {
                this.AddUserToRoles(options, username);
                num2++;
                list2 = file.ReadLine();
                continue;
              }
              flag = options.ConflictOption == ImportOptions.ConflictOptions.KeepProperties;
              contactFromName = Factory.GetContactFromName(username);
            }
            else
            {
              string password = username + Sitecore.Configuration.Settings.GetSetting("custom.default.password","!w6h" );
              var user = Membership.CreateUser(username, password, str);
              Log.Info("428826 ECM user import: New user is created from the CSV file. The user name is " + user.UserName + " The password is " + password, this);
              contactFromName = Factory.GetContactFromName(username);
              contactFromName.Profile.ProfileItemId = options.Root.Settings.SubscriberProfile;
              contactFromName.Profile.Save();
            }
            if (!flag)
            {
              contactFromName.Profile.Email = str;
              contactFromName.Profile.SetCustomProperty("IsAnonymousSubscriber", "true");
            }
            foreach (string str3 in options.MappedProperties.Keys)
            {
              for (int i = 0; i < headers.Count; i++)
              {
                if (options.MappedProperties[str3].Equals(headers[i]))
                {
                  if (!(flag && !string.IsNullOrEmpty(contactFromName.Profile[str3])))
                  {
                    contactFromName.Profile[str3] = list2[i];
                  }
                  break;
                }
              }
            }
            contactFromName.Profile.Save();
            this.AddUserToRoles(options, username);
            num++;
            list2 = file.ReadLine();
          }
        }
      }
      catch (Exception exception)
      {
        Logging.LogError(exception);
      }
      finally
      {
        if (file != null)
        {
          file.Dispose();
        }
      }
      return string.Concat(new object[] { num, "|", num2, "|", num3, "|", num4 });
    }

    private string TryFindEmail(string text)
    {
      int num4;
      char ch;
      if (string.IsNullOrEmpty(text))
      {
        return string.Empty;
      }
      int index = text.IndexOf('@');
      if ((index < 1) || (index > (text.Length - 2)))
      {
        return string.Empty;
      }
      int startIndex = 0;
      int num3 = text.Length - 1;
      for (num4 = index - 1; num4 >= 0; num4--)
      {
        ch = text[num4];
        if ((!char.IsLetterOrDigit(ch) && (ch != '-')) && (ch != '_'))
        {
          startIndex = num4 + 1;
          break;
        }
      }
      for (num4 = index + 1; num4 < text.Length; num4++)
      {
        ch = text[num4];
        if (((!char.IsLetterOrDigit(ch) && (ch != '.')) && (ch != '-')) && (ch != '_'))
        {
          num3 = num4 - 1;
          break;
        }
      }
      string str = text.Substring(startIndex, (num3 - startIndex) + 1);
      return (Util.IsValidEmail(str) ? str : string.Empty);
    }
  }
}
