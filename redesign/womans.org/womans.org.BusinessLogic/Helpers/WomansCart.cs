﻿namespace womans.org.BusinessLogic.Helpers
{
    #region

    using System;
    using System.Data.Entity.Validation;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    using MedTouch.Calendar.Entities;
    using MedTouch.Calendar.Helpers;
    using MedTouch.Calendar.Helpers.ExtensionMethods;
    using MedTouch.Common.Helpers.ExtensionMethods;
    using MedTouch.Calendar.Helpers.ObjectModel;
    using MedTouch.Calendar.Helpers.ShoppingCart;
    using MedTouch.Common.Helpers;

    using Sitecore.Data.Items;
    using Sitecore.Diagnostics;
    using Sitecore.Form.Core.Client.Data.Submit;

    using womans.org.BusinessLogic.Entities;

    using Calendar_EventRegistration = womans.org.BusinessLogic.Entities.Calendar_EventRegistration;
    

    #endregion

    public class WomansCart : Cart
    {
        #region Constants

        private const string lineender = "<br/>";

        #endregion

        #region Fields

        private string _fitnessMemberNumber = "";

        #endregion

        #region Public Properties

        public string FitnessMemberNumber
        {
            get
            {
                return this._fitnessMemberNumber;
            }
            set
            {
                this._fitnessMemberNumber = value;
                this.UpdateCart();
            }
        }

        public bool HasFitnessPricing
        {
            get
            {
                foreach (var t in this.CartTopics)
                {
                    if (t.CartSessions.Count > 0 && !string.IsNullOrEmpty(t.CurEventFeeID))
                    {
                        var feetype = ItemHelper.GetItemFromGuid(t.CurEventFeeID);
                        if (feetype != null)
                        {
                            if (ItemHelper.GetCheckBoxValueByFieldName(feetype, "Requires Fitness Membership"))
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Clear the cart and remove from session state
        /// </summary>
        public override void EmptyCart()
        {
            this.FitnessMemberNumber = string.Empty;
            base.EmptyCart();
        }
        
        public override string ExpandCartTokens(string inText, CartTopic topic = null)
        {
            //            string outText = base.ExpandCartTokens(inText, topic);
            var outText = inText;

            // Shopping Cart Summary
            if (outText.Contains("{shopping cart summary}"))
            {
                var shoppingCartSummary = this.GetShoppingCartSummary();
                outText = outText.Replace(
                    string.Concat(new object[] { "{shopping cart summary}" }), shoppingCartSummary ?? string.Empty);
            }

            // Topic sessions Summary
            if (topic != null && outText.Contains("{event topic summary}"))
            {
                var eventTopicSummary = base.GetEventTopicSummary(topic);
                outText = outText.Replace(
                    string.Concat(new object[] { "{event topic summary}" }), eventTopicSummary ?? string.Empty);
            }

            // Fitness membership number
            if (outText.Contains("{fitness member number}"))
            {
                outText = outText.Replace(string.Concat(new object[] { "{fitness member number}" }), this._fitnessMemberNumber ?? string.Empty);
            }
            // Fitness membership number
            if (outText.Contains(" {Transaction ID}"))
            {
                outText = outText.Replace(string.Concat(new object[] { " {Transaction ID}" }),"@@@@@@@@@@@@"?? string.Empty);
            }
            return outText;
        }

        public override string GetEventTopicSummary(CartTopic topic)
        {
            var lineender = "<br/>";
            var strOut = new StringBuilder();
            if (topic != null && topic.CartSessions.Any())
            {
                var eventTopicItem = ItemHelper.GetItemFromGuid(topic.TopicID);
                if (eventTopicItem != null)
                {
                    strOut.Append(
                        "<strong>"
                        + ItemHelper.GetFieldRawValue(eventTopicItem, ItemMapper.EventTopic.FieldName.Headline)
                        + "</strong>" + lineender);
                    strOut.Append(
                        ItemHelper.GetFieldRawValue(eventTopicItem, ItemMapper.EventTopic.FieldName.Introduction));
                    //                    // 7/2014: Penni requested these not be added to the emails (for privacy purposes)
                    //                    //          keeping conditional compile in case this feature is needed for specific projects.
                    //#if EMAILCUSTOMFIELDS
                    //                    foreach (var f in topic.CustomFields) {
                    //                        if (string.IsNullOrEmpty(f.Value))
                    //                            continue;
                    //                        if (Sitecore.DateUtil.IsIsoDate(f.Value)) {
                    //                            DateTime outDate = ItemHelper.GetDateFromSitecoreIsoDate(f.Value);
                    //                            strOut.Append(String.Format("{0}: {1}{2}", f.Name, outDate.ToString("d"), lineender));
                    //                            continue;
                    //                        }
                    //                        strOut.Append(String.Format("{0}: {1}{2}", f.Name, f.Value, lineender));
                    //                    }
                    //                    if (topic.CustomFields.Any())
                    //                        strOut.Append(lineender);
                    //#endif
                    strOut.Append(lineender);
                    strOut.Append(lineender);
                    var contactLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.Calendar.ShoppingCartCheckout.SummaryContactLabel");
                    strOut.Append(contactLabel + lineender);
                    strOut.Append(
                        String.Format(
                            "{0}{1}",
                            ItemHelper.GetFieldHtmlValue(
                                eventTopicItem, ItemMapper.EventTopic.FieldName.EventContactName),
                            lineender));
                    strOut.Append(
                        String.Format(
                            "{0}{1}",
                            ItemHelper.GetFieldHtmlValue(
                                eventTopicItem, ItemMapper.EventTopic.FieldName.EventContactPhone),
                            lineender));
                    strOut.Append(
                        String.Format(
                            "{0}{1}",
                            ItemHelper.GetFieldHtmlValue(
                                eventTopicItem, ItemMapper.EventTopic.FieldName.EventContactEmail),
                            lineender));

                    //adding topic instruction here
                    strOut.Append(this.GetEventTopicInstruction(eventTopicItem) + lineender);

                    var curTopicFee = topic.FeeValue.GetUSCurrencyDisplay();
                    foreach (var session in topic.CartSessions)
                    {
                        var eventSessionItem = ItemHelper.GetItemFromGuid(session.SessionID);
                        if (eventSessionItem != null)
                        {
                            strOut.Append(
                                ItemHelper.GetFieldRawValue(
                                    eventSessionItem, EventSession.FieldName.AttendingDatesInformation));
                            var strEventDateTime = RegistrationHelper.GetSessionDateTimeString(eventSessionItem);
                            if (!string.IsNullOrEmpty(strEventDateTime))
                            {
                                strOut.Append(strEventDateTime + lineender + lineender);
                            }
                            strOut.Append(this.GetLocation(eventSessionItem) + lineender);

                            if (session.IsWaitList)
                            {
                                strOut.Append(
                                    String.Format(
                                        "{0}{1}",
                                        CultureHelper.GetDictionaryTranslation(
                                            "Modules.Calendar.ShoppingCartCheckout.WaitlistItemStatus"),
                                        lineender));
                            }
                            else
                            {
                                var feeLabel =
                                    CultureHelper.GetDictionaryTranslation(
                                        "Modules.Calendar.ShoppingCartCheckout.SummaryFeeLabel");
                                strOut.Append(String.Format("{0} {1}{2}", feeLabel, curTopicFee, lineender));
                                if (this.IncludeTopicCodeInCartSummary)
                                {
                                    var codeLabel =
                                        CultureHelper.GetDictionaryTranslation(
                                            "Modules.Calendar.ShoppingCartCheckout.SummaryCodeLabel");
                                    strOut.Append(
                                        String.Format(
                                            "{0} {1}{2}",
                                            codeLabel,
                                            ItemHelper.GetFieldHtmlValue(
                                                eventTopicItem, ItemMapper.EventTopic.FieldName.InternalCode),
                                            lineender));
                                }
                            }
                        }
                    }
                    strOut.Append(lineender);
                }
            }
            return strOut.ToString();
        }

        /// <summary>
        ///     Generate Html Shopping cart summary - all topics (with sessions) in the current shopping cart, with sub-total, promotion values, and final total
        /// </summary>
        /// <returns>string: generated html for current shopping cart</returns>
        public virtual string GetShoppingCartSummary()
        {
            var cartSummary = new StringBuilder();
            var cart = ModuleHelper.ShoppingCart;

            if (cart != null && cart.CartTopics != null)
            {
                var bHasWaitlistItems = false;
                foreach (var topic in cart.CartTopics.Where(t => t.CartSessions.Count > 0))
                {
                    var outTopic = this.GetEventTopicSummary(topic);
                    if (!string.IsNullOrEmpty(outTopic))
                    {
                        cartSummary.Append(outTopic);
                        if (!bHasWaitlistItems)
                        {
                            bHasWaitlistItems = topic.CartSessions.Where(s => s.IsWaitList).Any();
                        }
                    }
                }
                // Calculate totals for all sessions in the cart
                var shoppingCartSubtotal = cart.ShoppingCartSessionTotal;
                var promoTotal = cart.ShoppingCartPromoTotal; // also sets up each promo code's value
                var shoppingCartTotal = shoppingCartSubtotal - promoTotal;

                // Setup promo code totals panel
                if (cart.CartPromosApplied.Count > 0 && promoTotal > 0.0)
                {
                    var subtotalLabel =
                        CultureHelper.GetDictionaryTranslation("Modules.Calendar.ShoppingCartCheckout.SubtotalLabel");
                    cartSummary.Append(
                        String.Format("<b>{0}:</b> {1:c}{2}", subtotalLabel, shoppingCartSubtotal, lineender));
                    //var promocodeLabel =
                    //    CultureHelper.GetDictionaryTranslation("Modules.Calendar.ShoppingCartCheckout.PromoCodeLabel");
                    //foreach (var cartPromo in cart.CartPromosApplied)
                    //{
                    //    cartSummary.Append(
                    //        String.Format(
                    //            "{0}: {1}: {2:c}- {3}",
                    //            promocodeLabel,
                    //            cartPromo.PromoCodeName,
                    //            cartPromo.TotalValue,
                    //            lineender));
                    //}
                }
                var totalLabel =
                    CultureHelper.GetDictionaryTranslation("Modules.Calendar.ShoppingCartCheckout.TotalLabel");
                cartSummary.Append(
                    String.Format("<b>{0}:</b> {1:c}{2}", lineender + totalLabel, shoppingCartTotal, lineender));
                if (bHasWaitlistItems)
                {
                    cartSummary.Append(
                        String.Format(
                            "{0}<i>{1}</i>{2}",
                            lineender,
                            CultureHelper.GetDictionaryTranslation(
                                "Modules.Calendar.ShoppingCartCheckout.WaitlistInCartMessage"),
                            lineender));
                }
            }
            return cartSummary.ToString();
        }

        public override bool RegisterEvent(
            ICalendar_EventRegistration regObj, AdaptedResultList fields, CartTopic topic)
        {
            using (var customDbContext = new CustomEntities_Womans())
            {
                Log.Info("Inserting event: " + regObj.TopicID, this);
                try
                {
                    var womansReg = regObj as Calendar_EventRegistration;
                    womansReg.FitnessMbrNumber = this.FitnessMemberNumber;

                    if (topic.CustomFields.Any())
                    {
                        foreach (var f in topic.CustomFields)
                        {
                            if (!string.IsNullOrEmpty(f.Value))
                            {
                                switch (f.Name)
                                {
                                    case "Partner Name":
                                        womansReg.PartnerName = f.Value;
                                        break;
                                    case "Physician":
                                        womansReg.Physician = f.Value;
                                        break;
                                }
                            }
                        }
                    }

                    customDbContext.Calendar_EventRegistration.Add(womansReg);
                    customDbContext.SaveChanges();
                    return true;
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        var errInfo = "Save Calendar_EventRegistration - Entity validation error(s):"
                                      + eve.Entry.Entity.GetType().Name + "-" + eve.Entry.State;
                        foreach (var ve in eve.ValidationErrors)
                        {
                            var err = string.Format(
                                "- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                            errInfo += Environment.NewLine + err;
                        }
                        Log.Error(errInfo, e);
                        ModuleHelper.SendErrorEmail(regObj.SerializeToJson(), e);
                        throw e;
                    }
                }
                catch (Exception exInsert)
                {
                    Log.Error("InsertEventRegistration failed.", exInsert, exInsert);
                    ModuleHelper.SendErrorEmail(regObj.SerializeToJson(), exInsert);
                    throw exInsert;
                }
            }
            return false;
        }

        #endregion

        #region Methods

        private string GetEventTopicInstruction(Item eventTopicItem)
        {
            var lineender = "<br/>";
            var outText = new StringBuilder();
            var instructionLabel =
                CultureHelper.GetDictionaryTranslation(
                    "Modules.Calendar.ShoppingCartCheckout.RegistrationInstructionLabel");

            outText.Append(lineender + instructionLabel + lineender);
            if (!string.IsNullOrEmpty(ItemHelper.GetFieldRawValue(eventTopicItem, "Registration Instruction")))
            {
                outText.Append(ItemHelper.GetFieldRawValue(eventTopicItem, "Registration Instruction") + lineender);
            }
            return outText.ToString();
        }

        #endregion
    }
}