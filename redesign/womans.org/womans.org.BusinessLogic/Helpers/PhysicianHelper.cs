﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using MedTouch.PhysicianDirectory.Helpers;
using Sitecore.Search;
using Sitecore.SharedSource.Search.Parameters;
using Sitecore.Collections;
using Sitecore.SharedSource.Search.Utilities;
using Sitecore.Sharedsource.Data.Comparers.ItemComparers;
using MedTouch.PhysicianDirectory.Helpers.Links;
using womans.org.BusinessLogic.PhysicianDirectory.Search;
using MedTouch.PhysicianDirectory.Search;

namespace womans.org.BusinessLogic.Helpers
{
    public class PhysicianHelper
    {
        public static ILinkHelper GetLinkHelper()
        {
            return new WomansLinkHelper();
        }

        public static IPhysicianSearcher GetPhysicianSearcher()
        {
            return new WomansPhysicianSearcher();
        }

        public static string GroupQuerystring()
        {
            return ItemHelper.GetFieldRawValue(ModuleHelper.GetModuleItem(), "Group Querystring");
        }

        public static Item GroupFolder()
        {
            return ItemHelper.GetItemFromGuid(ItemHelper.GetFieldRawValue(ModuleHelper.GetModuleItem(), "Locations Folder"));
        }

        public static string GroupTemplateGuid()
        {
            return ItemHelper.GetFieldRawValue(ModuleHelper.GetModuleItem(), "Location Template");
        }

        public static bool BindGroupDropDown(DropDownList dropDownList, string currentFilter)
        {
            string displayNameField = "Location Name";
            string allFilterString =
                CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianSearch.AllLocationsText");

            List<Item> list = new List<Item>();
            string key = string.Concat((string[])new string[] { "cache_pd_dropdown_", currentFilter, GroupTemplateGuid(), GroupFolder().ID.ToString(), displayNameField });

            SafeDictionary<string> refinements = new SafeDictionary<string>();
            List<DateRangeSearchParam.DateRangeField> ranges = new List<DateRangeSearchParam.DateRangeField>();

            dropDownList.Items.Clear();
            dropDownList.Items.Add(new ListItem(allFilterString, string.Empty));
            if (!CacheHelper.Get<List<Item>>(key, out list))
            {
                if (list == null)
                {
                    list = new List<Item>();
                }
                List<Item> itemListFromInformationCollection = SearchHelper.GetItemListFromInformationCollection(SitecoreSearchHelper.GetItems(ModuleHelper.SiteSearchIndexName(), Context.Language.CultureInfo.TwoLetterISOLanguageName, GroupTemplateGuid(), GroupFolder().ID.ToString(), string.Empty, string.Empty, refinements, ranges, QueryOccurance.Must));
                itemListFromInformationCollection = ItemHelper.SortItemsByAscending(itemListFromInformationCollection, displayNameField);
                foreach (Item item in itemListFromInformationCollection)
                {
                    if (Enumerable.Any<SkinnyItem>(SitecoreSearchHelper.GetItems(SitecoreSearchHelper.SiteSearchIndexName(), Context.Language.CultureInfo.TwoLetterISOLanguageName, ModuleHelper.PhysicianTemplateGuid(), ItemHelper.GetStartItemGuid(), string.Empty, item.ID.ToString(), refinements, ranges, QueryOccurance.Must)))
                    {
                        if (
                            !list.Exists(
                                //i => i.Fields[displayNameField].Value.Equals(item.Fields[displayNameField].Value, StringComparison.CurrentCulture)))
                                i => ItemHelper.GetFieldRawValue(i, displayNameField).Equals(ItemHelper.GetFieldRawValue(item, displayNameField), StringComparison.OrdinalIgnoreCase)))
                        {
                            list.Add(item);
                        }
                    }
                }
                if (Configurations.FilterCacheLifeTime != 0)
                {
                    CacheHelper.Add<System.Collections.Generic.List<Item>>(list, key, Configurations.FilterCacheLifeTime);
                }
            }
            if (Enumerable.Any<Item>(list))
            {
                foreach (Item item2 in list)
                {
                    ListItem item3 = new ListItem(ItemHelper.GetFieldRawValue(item2, displayNameField), ItemHelper.GetFieldRawValue(item2, displayNameField));
                    if (ItemHelper.GetFieldRawValue(item2, displayNameField).Equals(currentFilter, StringComparison.OrdinalIgnoreCase))
                    {
                        item3.Selected = true;
                    }
                    dropDownList.Items.Add(item3);
                }
            }

            return (bool)(dropDownList.Items.Count > 1);
        }

        public static string SpecialtySearchFolderName
        {
            get
            {
                return "{A7D5609C-6FF0-432F-B6D6-5836199EED92}";
            }
        }

        public static string SpecialtySearchTemplateGuid
        {
            get
            {
                return "{F13CD9DF-B762-4E72-8706-5D325EB1F223}";
            }
        }

        public static bool BindSpecialtySearchDropDown(DropDownList dropDownList, string currentFilter)
        {
            string displayNameField = "Specialty Name";
            string allFilterString =
                CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianSearch.AllSpecialtiesText");

            List<Item> list = new List<Item>();
            string key = string.Concat((string[])new string[] { "cache_pd_dropdown_", currentFilter, SpecialtySearchTemplateGuid, SpecialtySearchFolderName, displayNameField });

            dropDownList.Items.Clear();
            dropDownList.Items.Add(new ListItem(allFilterString, string.Empty));
            if (!CacheHelper.Get<List<Item>>(key, out list))
            {
                if (list == null)
                {
                    list = new List<Item>();
                }
                Item specialtySearchItem = Sitecore.Context.Database.GetItem(SpecialtySearchFolderName);
                foreach (Item itm in specialtySearchItem.GetChildren())
                {
                    if (itm.TemplateID.ToString().Equals(SpecialtySearchTemplateGuid, StringComparison.OrdinalIgnoreCase))
                    {
                        list.Add(itm);
                    }
                }
                if (Configurations.FilterCacheLifeTime != 0)
                {
                    CacheHelper.Add<System.Collections.Generic.List<Item>>(list, key, Configurations.FilterCacheLifeTime);
                }
            }
            if (Enumerable.Any(list))
            {
                foreach (Item item2 in list)
                {
                    string specialties = string.Empty;

                    MultilistField mfSpecialties = (MultilistField)(item2.Fields["Specialties"]);
                    foreach (Item i in mfSpecialties.GetItems())
                    {
                        if (string.IsNullOrWhiteSpace(specialties))
                        {
                            specialties = i.Name;
                        }
                        else
                        {
                            specialties += "|" + i.Name;
                        }
                    }

                    ListItem li = new ListItem(item2.Fields[displayNameField].Value, specialties);
                    if (li.Value.Equals(currentFilter, StringComparison.OrdinalIgnoreCase))
                    {
                        li.Selected = true;
                    }
                    dropDownList.Items.Add(li);
                }
            }

            return (bool)(dropDownList.Items.Count > 1);

            //Item item = Sitecore.Context.Database.GetItem(SpecialtySearchFolderName);
            //ListItemCollection listItems = new ListItemCollection();
            //int counter = 1;
            //int selectedIndex = 0;

            //if (item != null)
            //{
            //    foreach (Item itm in item.GetChildren())
            //    {
            //        string specialties = string.Empty;
            //        if (itm.TemplateID.ToString() == SpecialtySearchTemplateGuid)
            //        {
            //            MultilistField mfSpecialties = (MultilistField)(itm.Fields["Specialties"]);
            //            foreach (Item i in mfSpecialties.GetItems())
            //            {
            //                if (string.IsNullOrWhiteSpace(specialties))
            //                {
            //                    specialties = i.Name;
            //                }
            //                else
            //                {
            //                    specialties += "|" + i.Name;
            //                }
            //            }

            //            ListItem li = new ListItem(itm.Fields[displayNameField].Value, specialties);
            //            listItems.Add(li);
            //            if (li.Value == currentFilter)
            //            {
            //                selectedIndex = counter;
            //            }
            //            counter++;
            //        }
            //    }
            //}

            ////ListItem listItem = new ListItem(CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianSearch.AllSpecialtiesText"), "");
            ////listItems.Insert(0, listItem);

            //dropDownList.DataSource = listItems;
            //dropDownList.DataBind();
            //dropDownList.SelectedIndex = selectedIndex;
        }
    }
}
