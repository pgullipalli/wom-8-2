﻿namespace womans.org.BusinessLogic.Helpers
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using MedTouch.Calendar.Helpers;
    using MedTouch.Calendar.Helpers.ObjectModel;
    using MedTouch.Common.Helpers;

    using Sitecore;
    using Sitecore.Data.Items;
    using Sitecore.Diagnostics;

    using womans.org.BusinessLogic.Entities;

    using Convert = System.Convert;

    #endregion

    public class CalendarHelper
    {
        #region Public Methods and Operators

        public static bool CheckFitnessMember(string MbrNumber)
        {
            if (string.IsNullOrEmpty(MbrNumber))
            {
                return false;
            }
            var customDBcontext = new CustomEntities_Womans();
            try
            {
                var x = customDBcontext.Fitness_Center.FirstOrDefault(i => i.MbrNumber == MbrNumber);
                if (x != null)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Get Fitness Center Membership failed.", ex, HttpContext.Current);
            }
            return (false);
        }

        public static List<Item> GetFutureSessions(Item startItem = null)
        {
            var list = new List<Item>();
            var database = ModuleHelper.CalendarGetCurrentDatabase();
            if (startItem == null)
            {
                startItem = database.GetItem(Context.Site.StartPath);
            }
            if (startItem != null)
            {
                var str = ModuleHelper.EscapePathForQuery(startItem.Paths.Path) + "//*[@@TemplateName='"
                          + EventSession.TemplateName + "']";
                foreach (var item in
                    (from e in database.SelectItems(str).ToList() orderby e.Parent.Paths.Path select e).ToList<Item>())
                {
                    var isOngoingEvent = (ItemHelper.GetFieldRawValue(item, EventSession.FieldName.IsOngoing) == "1");
                    var sessionStartTime = ModuleHelper.GetSitecoreDateField(
                        item, EventSession.FieldName.SessionStartDate);
                    var sessionEndTime = ModuleHelper.GetSitecoreDateField(item, EventSession.FieldName.SessionEndDate);

                    //on going events
                    if (isOngoingEvent)
                    {
                        //no end date always show
                        if (!sessionEndTime.HasValue)
                        {
                            list.Add(item);
                        }
                        else
                        {
                            //before session end date will show
                            if (Convert.ToDateTime(DateTime.Now) < Convert.ToDateTime(sessionEndTime))
                            {
                                list.Add(item);
                            }
                        }
                    }
                    else
                    {
                        //Get all available session from 12am today
                        if (!ModuleHelper.SessionExpired(item, DateTime.Today, DateTime.Today.AddYears(10)))
                        {
                            //check start time is past or not. When time not time been enterned, it will default back to 12am.
                            if (sessionStartTime.HasValue && Convert.ToDateTime(DateTime.Now) < Convert.ToDateTime(sessionStartTime))
                            {
                                list.Add(item);
                            }
                        }
                    }
                }
            }
            return list;
        }

        #endregion
    }
}