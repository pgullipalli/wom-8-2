﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using Womans.CustomItems.PageTemplates.Homepage;
using Womans.CustomItems.ComponentTemplates;
using Sitecore.Data.Fields;

namespace womans.org.BusinessLogic.Helpers
{
    public class NavigationSection
    {
        private string _sectionTitle = string.Empty;
        private List<Navigation> _subNav = new List<Navigation>();

        internal NavigationSection(Item item)
        {
            _sectionTitle = FieldRenderer.Render(item, "Section Name");
            _subNav = Navigation.GetChildNavigation(item);
        }

        public string SectionTitle
        {
            get { return _sectionTitle; }
            set { _sectionTitle = value; }
        }


        public List<Navigation> SubNav
        {
            get { return _subNav; }
            set { _subNav = value; }
        }



        public static List<NavigationSection> GetQuickLinks()
        {
            List<NavigationSection> sections = new List<NavigationSection>();

            Item itm = ItemHelper.GetStartItem();
            HomepageItem home = itm;
            Item item = home.SiteSettings.QuickLinksRoot;

            foreach (Item i in ItemHelper.GetChildItems(item))
            {
                NavigationSection section = new NavigationSection(i);
                sections.Add(section);
            }

            return sections;
        }

    }
    public class Navigation
    {
        private string _navigationTitle = string.Empty;
        private string _linkToSelf = string.Empty;
        private string _scId = string.Empty;
        private string _target = string.Empty;
        private string _cssClass = string.Empty;
        private List<Navigation> _subNav = new List<Navigation>();

        internal Navigation(Item item)
        {

            _linkToSelf = Sitecore.Links.LinkManager.GetItemUrl(item);
            _scId = item.ID.ToString();

            try
            {
                _cssClass = item.Fields["CSS Class"].Value;
            }
            catch
            {
                _cssClass = string.Empty;
            }

            if (item.Fields["Destination"] != null)
            {
                LinkField link = item.Fields["Destination"];
                _navigationTitle = FieldRenderer.Render(item, "Override Navigation Title");

                if (link != null && link.IsInternal && link.TargetItem != null)
                {
                    _linkToSelf = Sitecore.Links.LinkManager.GetItemUrl(link.TargetItem);
                    if (String.IsNullOrEmpty(_navigationTitle))
                    {
                        _navigationTitle = FieldRenderer.Render(link.TargetItem, "Navigation Title");
                    }
                }

                if (link != null && !link.IsInternal)
                {
                    _linkToSelf = link.Url;
                    _target = "_blank";
                }
               
            }
            else
            {
                _navigationTitle = FieldRenderer.Render(item, "Navigation Title");
            }
            
            if (_target == string.Empty)
            {
                _target = "_self";
            }

            //if (item.ID.ToString() == Sitecore.Context.Item.ID.ToString())
            //{
            //    _cssClass = "active";
            //}
        }

        
        public Navigation()
        {
            LinkToSelf = string.Empty;
            NavigationTitle = string.Empty;
            ScId = string.Empty;
            _target = string.Empty;
            _cssClass = string.Empty;
        }

        public string NavigationTitle
        {
            get { return _navigationTitle; }
            set { _navigationTitle = value; }
        }

       
        public string Target
        {
            get { return _target; }
            set { _target = value; }
        }

        public string CssClass
        {
            get { return _cssClass; }
            set { _cssClass = value; }
        }

        public string LinkToSelf
        {
            get { return _linkToSelf; }
            set { _linkToSelf = value; }
        }

        public string ScId
        {
            get { return _scId; }
            set { _scId = value; }
        }

        public static List<Navigation> GetPrimaryNavigation()
        {
            List<Navigation> navs = new List<Navigation>();
            Item itm = ItemHelper.GetStartItem();
            HomepageItem home = itm;


            foreach (Item i in ItemHelper.GetChildItems(home.SiteSettings.PrimaryNavigation.Item))
            {
                Navigation nav = new Navigation(i);
                if (Sitecore.Context.Item.Axes.IsDecendantOf(Sitecore.Context.Database.GetItem(nav.ScId)))
                {
                    nav.CssClass += " active";
                }
                navs.Add(nav);
            }

            return navs;
        }

        public static string GetNavigationActiveClass(string scId)
        {
            try
            {
                Item itm = Sitecore.Context.Database.GetItem(scId);
                LinkField lf = itm.Fields["Destination"];

                if (lf.IsInternal)
                {
                    if (Sitecore.Context.Item.Paths.LongID.Contains(lf.TargetItem.Paths.LongID))
                    {
                        return "active";
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
            }
            catch
            {
                return string.Empty;
            }

            return string.Empty;
        }

        public static List<Navigation> GetBreadcrumb(Item item)
        {
            List<Navigation> navs = new List<Navigation>();
            Item itm = ItemHelper.GetStartItem();
            HomepageItem home = itm;

            Navigation nav = new Navigation(item);
            navs.Add(nav);

            while (item.ID.ToString() != home.ID.ToString())
            {
                item = item.Parent;
                nav = new Navigation(item);
                navs.Insert(0, nav);
            }

            return navs;
        }

        public static List<Navigation> GetFooterNavigation()
        {
            List<Navigation> navs = new List<Navigation>();
            Item itm = ItemHelper.GetStartItem();
            HomepageItem home = itm;

            foreach (Item i in ItemHelper.GetChildItems(home.SiteSettings.FooterNavigation.Item))
            {
                Navigation nav = new Navigation(i);
                navs.Add(nav);
            }

            return navs;
        }

        public static List<Navigation> GetCopyrightNavigation()
        {
            List<Navigation> navs = new List<Navigation>();
            Item itm = ItemHelper.GetStartItem();
            HomepageItem home = itm;

            foreach (Item i in ItemHelper.GetChildItems(home.SiteSettings.CopyrightNavigation.Item))
            {
                Navigation nav = new Navigation(i);
                navs.Add(nav);
            }

            return navs;
        }
        
        public static List<Navigation> GetSecondaryNavigation()
        {
            List<Navigation> navs = new List<Navigation>();
            Item itm = ItemHelper.GetStartItem();
            HomepageItem home = itm;


            foreach (Item i in ItemHelper.GetChildItems(home.SiteSettings.SecondaryNavigation.Item))
            {
                Navigation nav = new Navigation(i);
                if (nav.LinkToSelf != string.Empty)
                {
                    navs.Add(nav);
                }
            }

            return navs;
        }

        public static List<NavigationItem> GetSecondaryHomePageNavigation()
        {
            List<NavigationItem> navs = new List<NavigationItem>();
            Item itm = ItemHelper.GetStartItem();
            HomepageItem home = itm;


            foreach (Item i in ItemHelper.GetChildItems(home.SiteSettings.SecondaryNavigation.Item))
            {
                NavigationItem nav = i;
                navs.Add(nav);
            }

            return navs;
        }

        public static List<Navigation> GetLeftNavigaiton(Item item)
        {
            List<Navigation> navs = new List<Navigation>();
            
            foreach (Item i in ItemHelper.GetChildItems(item))
            {
                Navigation nav = new Navigation(i);
                if (Sitecore.Context.Item.ID.ToString() == item.ID.ToString())
                {
                    nav.CssClass = "level-1 nav-item on";
                }
                else
                {
                    nav.CssClass = "level-1 nav-item";
                }
                navs.Add(nav);
            }

            return navs;
        }

        public static List<Navigation> GetChildNavigation(Item item)
        {
            List<Navigation> navs = new List<Navigation>();
            Item itm = ItemHelper.GetStartItem();
            HomepageItem home = itm;

            
            foreach (Item i in ItemHelper.GetChildItems(item))
            {
                Navigation nav = new Navigation(i);
                navs.Add(nav);
            }

            return navs;
        }
       

        public static List<Navigation> GetQuickLinks(Item item)
        {
            var related = new List<Navigation>();
            List<Navigation> quicklinks = new List<Navigation>();
            Item itm = ItemHelper.GetStartItem();
            HomepageItem home = itm;
            
            Item QuickLinksRoot = home.SiteSettings.QuickLinksRoot.Item;

            foreach (Item i in ItemHelper.GetChildItems(QuickLinksRoot))
            {
                Navigation nav = new Navigation(i);
                quicklinks.Add(nav);
            }

            return quicklinks;
        }

        public static List<Navigation> GetSocial()
        {
            List<Navigation> social = new List<Navigation>();
            Item itm = ItemHelper.GetStartItem();
            HomepageItem home = itm;

            Item SocialLinksRoot = home.SiteSettings.SocialRoot.Item;

            foreach (Item i in ItemHelper.GetChildItems(SocialLinksRoot))
            {
                Navigation nav = new Navigation(i);
                social.Add(nav);
            }

            return social;
        }
    }
}






