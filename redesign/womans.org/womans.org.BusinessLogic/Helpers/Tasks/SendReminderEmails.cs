﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using MedTouch.Calendar.Entities;
using MedTouch.Calendar.Helpers.ObjectModel;
using MedTouch.Common.Helpers;
using MedTouch.Common.Helpers.Logging;
using MedTouch.Common.Helpers.Logging.Base;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace womans.org.BusinessLogic.Helpers.Tasks
{
    using MedTouch.Calendar.Helpers;

    using FormHelper = womans.org.BusinessLogic.Helpers.FormHelper;

    public class SendReminderEmailsTask {

        private ILogger logger = Logger.Create();

        /// <summary>
        /// Send reminder emails for:
        /// 1) Any Event where reminder #days before event date before or on today & reminder hasn't already been sent
        ///     2) All Registrants with a status flagged for "send reminder"
        /// </summary>
        public void SendEventReminderEmails(Sitecore.Data.Items.Item[] items, Sitecore.Tasks.CommandItem command, Sitecore.Tasks.ScheduleItem schedule)
        {

            try {
                logger.Info("Start 'Calendar - Send Event Reminder Emails'", this);
            
                // Make sure we are always operating in master database
                Item siteStartItem = (items.Length > 0) ? items[0] : Sitecore.Context.Item;
                if (siteStartItem == null) {
                    logger.Error("Error getting Site start item for calendar: please Initialize the 'Items' field to this site's home page in the Schedule for this task", this);
                    return;
                }

                // Get the Auto-email body to be sent as a reminder
                var reminderAutoEmail = ItemHelper.GetItemFromReferenceField(siteStartItem, ItemMapper.CalendarModuleSiteSettings.FieldName.ReminderEmail);
                if (reminderAutoEmail == null)
                {
                    logger.Error("Error getting reminder email from Site start item", this);
                    return;
                }
                string emailBody = ItemHelper.GetFieldRawValue(reminderAutoEmail, ItemMapper.AutoEmail.FieldName.EmailBody);
                string emailSubject = ItemHelper.GetFieldRawValue(reminderAutoEmail, ItemMapper.AutoEmail.FieldName.EmailSubject);
                string emailFrom = ItemHelper.GetFieldRawValue(siteStartItem, ItemMapper.CalendarModuleSiteSettings.FieldName.AutoEmailFromAddress);
                if (string.IsNullOrEmpty(emailBody) || string.IsNullOrEmpty(emailSubject) || string.IsNullOrEmpty(emailFrom)) {
                    logger.Error("Invalid Reminder Email: Email Subject or Body is empty OR Calendar setting for 'auto email from address' has not been setup", this);
                    return;
                }

                // Initialize SMTP settings
                var smtpClient = ModuleHelper.CalendarSMTPClient();
                if (smtpClient == null || string.IsNullOrEmpty(emailFrom)) {
                    logger.Error("Invalid Reminder Email: Email Subject or Body text is empty", this);
                    return;
                }

                // Get the list of statuses that need reminders sent
                List<Guid> sendEmailStatusList = EventRegistrationStatus.GetStatusList(bSendReminderOnly: true);
                if (!sendEmailStatusList.Any())
                {
                    logger.Error("Reminder emails not sent: No registration statuses are flagged for sending reminders", this);
                    return;
                }

                // Find ALL sessions (for all topics), order by the parent path, so sessions can be grouped by parent topic
                var eventSessionItems = ModuleHelper.GetFutureSessions(siteStartItem);
                if (!eventSessionItems.Any())
                    return;

                // Process all sessions in Parent Topic order
                ID curParentID = null;
                int daysBeforeSend = 0;
                Item curTopic = null;
                foreach (Item session in eventSessionItems)
                {
                    // Get Topic information (if days before reminder 0 or null, no reminder is to be sent for any sessions in this topic)
                    if (!session.ParentID.Equals(curParentID))
                    {
                        curParentID = session.ParentID;
                        curTopic = ModuleHelper.GetEventTopicFromEventSession(session);
                        if (curTopic != null)
                        {
                            daysBeforeSend = 0;
                            Int32.TryParse(
                                ItemHelper.GetFieldRawValue(curTopic, ItemMapper.EventTopic.FieldName.SendReminderDaysBefore),
                                out daysBeforeSend);
                        }
                    }

                    // Process each session, checking if reminder should be sent now:
                    // a) session date minus days-before-send is today or >today
                    // b) reminder hasn't already been sent
                    if (daysBeforeSend <= 0)
                        continue;
                    DateTime? startDate = ModuleHelper.GetSitecoreDateField(session,MedTouch.Calendar.Helpers.ObjectModel.EventSession.FieldName.SessionStartDate);
                    if (!startDate.HasValue)
                        continue;
                    TimeSpan diff = startDate.Value.Date - System.DateTime.Today;
                    if (diff.Days >= daysBeforeSend)
                        continue;
                    DateTime? sentReminderDate = ModuleHelper.GetSitecoreDateField(session, MedTouch.Calendar.Helpers.ObjectModel.EventSession.FieldName.ReminderSentDate);
                    if (!sentReminderDate.HasValue) {
                        // At this point, we know a reminder needs to be sent
                        bool bSuccess = SendSessionReminderEmails(session, smtpClient, sendEmailStatusList, emailFrom, 
                            FormHelper.ExpandCustomTokens(emailSubject, curTopic, session, siteStartItem), 
                            FormHelper.ExpandCustomTokens(emailBody, curTopic, session, siteStartItem));
                        if (bSuccess)
                        {
                            // Set reminder-sent date for this session, so reminder won't be sent again
                            using (new Sitecore.Data.Items.EditContext(session)) {
                                var isoDate = Sitecore.DateUtil.ToIsoDate(DateTime.Now);
                                session.Fields[MedTouch.Calendar.Helpers.ObjectModel.EventSession.FieldName.ReminderSentDate].Value = isoDate;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex, this);
            }
            finally
            {
                logger.Info("End 'Calendar - Send Event Reminder Emails'", this);
            }

        }


        private bool SendSessionReminderEmails(Item session, SmtpClient smtpClient, List<Guid> sendEmailStatusList, string emailFrom, string emailSubject, string emailBody)
        {
            int remindersSent = 0;
            int remindersTotal = 0;
            Guid eventsessionGuid = session.ID.ToGuid();
            // Make link URL's external
            emailBody = Regex.Replace(emailBody, "href=\"/", string.Join(string.Empty, new string[] { "href=\"", Sitecore.Web.WebUtil.GetServerUrl(), "/" }));
            using (var customDBcontext = new CustomEntities())
            {
                var sessionReg = customDBcontext.Calendar_EventRegistration
                    .Where(
                        q =>
                            sendEmailStatusList.Contains(q.RegistrationStatus) &&
                            q.SessionID.Equals(eventsessionGuid));
                if (!sessionReg.Any())
                    return true;
                remindersTotal = sessionReg.Count();
                remindersSent = 0;
                foreach (Calendar_EventRegistration reg in sessionReg)
                {
                    try
                    {
                        if (string.IsNullOrEmpty(reg.Email_Secure))
                            continue;
                        MailMessage mail = new MailMessage(emailFrom, reg.Email_Secure, emailSubject, emailBody);
                        mail.IsBodyHtml = true;
                        smtpClient.Send(mail);
                        remindersSent++;
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Error sending reminder email to: " + reg.Email_Secure, ex, this);
                    }
                }
                logger.Info(String.Format("Calendar reminders sent for event: {0}, total to send: {1}, total sent: {2}", session.ID, remindersTotal, remindersSent), this);
            }
            return true;
        }

    }
}