using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.BaseTemplates
{
public partial class BaseComponentItem : CustomItem
{

public static readonly string TemplateId = "{EC8DDD4A-AC67-4738-B24D-97B3C864967A}";


#region Boilerplate CustomItem Code

public BaseComponentItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator BaseComponentItem(Item innerItem)
{
	return innerItem != null ? new BaseComponentItem(innerItem) : null;
}

public static implicit operator Item(BaseComponentItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}