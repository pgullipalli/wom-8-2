using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.BaseTemplates
{
public partial class NavigationPropertiesItem : CustomItem
{

public static readonly string TemplateId = "{9A351BEE-C03E-4769-9435-A185FB00765F}";


#region Boilerplate CustomItem Code

public NavigationPropertiesItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator NavigationPropertiesItem(Item innerItem)
{
	return innerItem != null ? new NavigationPropertiesItem(innerItem) : null;
}

public static implicit operator Item(NavigationPropertiesItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField NavigationTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Navigation Title"]);
	}
}


public CustomGeneralLinkField RedirectLink
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["Redirect Link"]);
	}
}


public CustomCheckboxField ShowonBreadcrumb
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Show on Breadcrumb"]);
	}
}


public CustomCheckboxField ShowonSiteMap
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Show on Site Map"]);
	}
}


public CustomCheckboxField ShowonNavigation
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Show on Navigation"]);
	}
}


public CustomCheckboxField HideSiblings
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Hide Siblings"]);
	}
}


#endregion //Field Instance Methods
}
}