using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.BaseTemplates
{
public partial class AccordionItem : CustomItem
{

public static readonly string TemplateId = "{96845A48-7AB8-41E7-9617-BBD2B4963A02}";


#region Boilerplate CustomItem Code

public AccordionItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator AccordionItem(Item innerItem)
{
	return innerItem != null ? new AccordionItem(innerItem) : null;
}

public static implicit operator Item(AccordionItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField AccordionTitle1
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Title 1"]);
	}
}


public CustomTextField AccordionContent1
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Content 1"]);
	}
}


public CustomTextField AccordionTitle2
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Title 2"]);
	}
}


public CustomTextField AccordionContent2
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Content 2"]);
	}
}


public CustomTextField AccordionTitle3
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Title 3"]);
	}
}


public CustomTextField AccordionContent3
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Content 3"]);
	}
}


public CustomTextField AccordionTitle4
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Title 4"]);
	}
}


public CustomTextField AccordionContent4
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Content 4"]);
	}
}


public CustomTextField AccordionTitle5
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Title 5"]);
	}
}


public CustomTextField AccordionContent5
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Content 5"]);
	}
}


public CustomTextField AccordionTitle6
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Title 6"]);
	}
}


public CustomTextField AccordionContent6
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Content 6"]);
	}
}


public CustomTextField AccordionTitle7
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Title 7"]);
	}
}


public CustomTextField AccordionContent7
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Content 7"]);
	}
}


public CustomTextField AccordionTitle8
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Title 8"]);
	}
}


public CustomTextField AccordionContent8
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Content 8"]);
	}
}


public CustomTextField AccordionTitle9
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Title 9"]);
	}
}


public CustomTextField AccordionContent9
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Content 9"]);
	}
}


public CustomTextField AccordionTitle10
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Title 10"]);
	}
}


public CustomTextField AccordionContent10
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Accordion Content 10"]);
	}
}


#endregion //Field Instance Methods
}
}