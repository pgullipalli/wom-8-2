using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.BaseTemplates
{
public partial class SiteSkinItem : CustomItem
{

public static readonly string TemplateId = "{C640485B-7CE5-4E78-92E6-F6F16A9F6C45}";


#region Boilerplate CustomItem Code

public SiteSkinItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator SiteSkinItem(Item innerItem)
{
	return innerItem != null ? new SiteSkinItem(innerItem) : null;
}

public static implicit operator Item(SiteSkinItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomLookupField Skin
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Skin"]);
	}
}


#endregion //Field Instance Methods
}
}