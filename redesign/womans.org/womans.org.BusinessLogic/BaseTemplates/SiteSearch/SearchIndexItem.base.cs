using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.BaseTemplates.SiteSearch
{
public partial class SearchIndexItem : CustomItem
{

public static readonly string TemplateId = "{0538E67C-20BC-497E-92EF-E64937C8FA5C}";


#region Boilerplate CustomItem Code

public SearchIndexItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator SearchIndexItem(Item innerItem)
{
	return innerItem != null ? new SearchIndexItem(innerItem) : null;
}

public static implicit operator Item(SearchIndexItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField IndexName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Index Name"]);
	}
}


#endregion //Field Instance Methods
}
}