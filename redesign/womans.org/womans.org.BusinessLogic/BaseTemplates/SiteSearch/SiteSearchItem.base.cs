using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.BaseTemplates.SiteSearch
{
public partial class SiteSearchItem : CustomItem
{

public static readonly string TemplateId = "{3696BF89-0E29-4E1C-BB4D-F5B0A5BF6A4C}";


#region Boilerplate CustomItem Code

public SiteSearchItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator SiteSearchItem(Item innerItem)
{
	return innerItem != null ? new SiteSearchItem(innerItem) : null;
}

public static implicit operator Item(SiteSearchItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomCheckboxField ShowinSiteSearchResults
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Show in Site Search Results"]);
	}
}


public CustomTextField OverrideSiteSearchTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Override Site Search Title"]);
	}
}


public CustomTextField PriorityKeywords
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Priority Keywords"]);
	}
}


#endregion //Field Instance Methods
}
}