using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.BaseTemplates
{
public partial class ModuleRootItem : CustomItem
{

public static readonly string TemplateId = "{74F24580-EEBC-4114-AF77-68ADEE031A58}";


#region Boilerplate CustomItem Code

public ModuleRootItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator ModuleRootItem(Item innerItem)
{
	return innerItem != null ? new ModuleRootItem(innerItem) : null;
}

public static implicit operator Item(ModuleRootItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}