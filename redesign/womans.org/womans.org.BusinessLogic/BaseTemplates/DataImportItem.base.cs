using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.BaseTemplates
{
public partial class DataImportItem : CustomItem
{

public static readonly string TemplateId = "{2D934EFD-D7D0-40FF-9B43-78FFAB7764A2}";


#region Boilerplate CustomItem Code

public DataImportItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator DataImportItem(Item innerItem)
{
	return innerItem != null ? new DataImportItem(innerItem) : null;
}

public static implicit operator Item(DataImportItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField ContextSensitiveKey
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["ContextSensitiveKey"]);
	}
}


public CustomTextField AllUpdateableFieldsHash
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["AllUpdateableFieldsHash"]);
	}
}


#endregion //Field Instance Methods
}
}