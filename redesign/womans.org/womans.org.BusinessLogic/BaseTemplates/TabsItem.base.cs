using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.BaseTemplates
{
public partial class TabsItem : CustomItem
{

public static readonly string TemplateId = "{8E3201B9-0D2F-42E8-B766-86FCF43E6132}";


#region Boilerplate CustomItem Code

public TabsItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator TabsItem(Item innerItem)
{
	return innerItem != null ? new TabsItem(innerItem) : null;
}

public static implicit operator Item(TabsItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField TabTitle1
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Title 1"]);
	}
}


public CustomTextField TabContent1
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Content 1"]);
	}
}


public CustomTextField TabTitle2
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Title 2"]);
	}
}


public CustomTextField TabContent2
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Content 2"]);
	}
}


public CustomTextField TabTitle3
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Title 3"]);
	}
}


public CustomTextField TabContent3
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Content 3"]);
	}
}


public CustomTextField TabTitle4
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Title 4"]);
	}
}


public CustomTextField TabContent4
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Content 4"]);
	}
}


public CustomTextField TabTitle5
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Title 5"]);
	}
}


public CustomTextField TabContent5
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Content 5"]);
	}
}


public CustomTextField TabTitle6
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Title 6"]);
	}
}


public CustomTextField TabContent6
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Content 6"]);
	}
}


public CustomTextField TabTitle7
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Title 7"]);
	}
}


public CustomTextField TabContent7
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Content 7"]);
	}
}


public CustomTextField TabTitle8
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Title 8"]);
	}
}


public CustomTextField TabContent8
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Content 8"]);
	}
}


public CustomTextField TabTitle9
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Title 9"]);
	}
}


public CustomTextField TabContent9
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Content 9"]);
	}
}


public CustomTextField TabTitle10
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Title 10"]);
	}
}


public CustomTextField TabContent10
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Tab Content 10"]);
	}
}


#endregion //Field Instance Methods
}
}