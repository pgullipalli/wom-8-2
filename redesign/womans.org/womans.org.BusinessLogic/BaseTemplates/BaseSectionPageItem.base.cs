using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.BaseTemplates
{
public partial class BaseSectionPageItem : CustomItem
{

public static readonly string TemplateId = "{591BD905-E419-49FE-BBDE-3432B954F9FF}";

#region Inherited Base Templates

private readonly PageDataItem _PageDataItem;
public PageDataItem PageData { get { return _PageDataItem; } }
private readonly BasePresentationItem _BasePresentationItem;
public BasePresentationItem BasePresentation { get { return _BasePresentationItem; } }

#endregion

#region Boilerplate CustomItem Code

public BaseSectionPageItem(Item innerItem) : base(innerItem)
{
	_PageDataItem = new PageDataItem(innerItem);
	_BasePresentationItem = new BasePresentationItem(innerItem);

}

public static implicit operator BaseSectionPageItem(Item innerItem)
{
	return innerItem != null ? new BaseSectionPageItem(innerItem) : null;
}

public static implicit operator Item(BaseSectionPageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}