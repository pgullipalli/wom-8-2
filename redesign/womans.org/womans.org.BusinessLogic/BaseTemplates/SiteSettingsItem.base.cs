using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.BaseTemplates
{
public partial class SiteSettingsItem : CustomItem
{

public static readonly string TemplateId = "{441344C4-C932-472C-A280-D330C0227AFD}";


#region Boilerplate CustomItem Code

public SiteSettingsItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator SiteSettingsItem(Item innerItem)
{
	return innerItem != null ? new SiteSettingsItem(innerItem) : null;
}

public static implicit operator Item(SiteSettingsItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomImageField SiteLogo
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Site Logo"]);
	}
}

public CustomImageField SiteLogoFallback
{
    get
    {
        return new CustomImageField(InnerItem, InnerItem.Fields["Site Logo Fallback"]);
    }
}


public CustomTextField FooterColumn1
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Footer Column 1"]);
	}
}


public CustomMultiListField HomepageSliders
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["Homepage Sliders"]);
	}
}


public CustomLookupField PrimaryNavigation
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Primary Navigation"]);
	}
}


public CustomLookupField CopyrightNavigation
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["Copyright Navigation"]);
    }
}

    
    public CustomLookupField SocialRoot
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["Social Root"]);
    }

}


    public CustomLookupField QuickLinksRoot
    {
        get
        {
            return new CustomLookupField(InnerItem, InnerItem.Fields["QuickLinks Root"]);
        }

    }


public CustomTextField FooterColumn2
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Footer Column 2"]);
	}
}


public CustomLookupField SecondaryNavigation
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Secondary Navigation"]);
	}
}


public CustomTextField FooterColumn3
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Footer Column 3"]);
	}
}


public CustomLookupField FooterNavigation
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Footer Navigation"]);
	}
}


public CustomTextField CopyrightText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Copyright Text"]);
	}
}


public CustomTextField BrowserTitleFormat
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Browser Title Format"]);
	}
}


#endregion //Field Instance Methods
}
}