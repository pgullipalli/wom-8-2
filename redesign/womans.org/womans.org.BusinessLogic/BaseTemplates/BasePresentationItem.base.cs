using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.BaseTemplates
{
public partial class BasePresentationItem : CustomItem
{

public static readonly string TemplateId = "{3A647540-3BC8-4490-BF00-A1D562327863}";

#region Inherited Base Templates

private readonly BasePageItem _BasePageItem;
public BasePageItem BasePage { get { return _BasePageItem; } }
private readonly SEODataItem _SEODataItem;
public SEODataItem SEOData { get { return _SEODataItem; } }
private readonly NavigationPropertiesItem _NavigationPropertiesItem;
public NavigationPropertiesItem NavigationProperties { get { return _NavigationPropertiesItem; } }

#endregion

#region Boilerplate CustomItem Code

public BasePresentationItem(Item innerItem) : base(innerItem)
{
	_BasePageItem = new BasePageItem(innerItem);
	_SEODataItem = new SEODataItem(innerItem);
	_NavigationPropertiesItem = new NavigationPropertiesItem(innerItem);

}

public static implicit operator BasePresentationItem(Item innerItem)
{
	return innerItem != null ? new BasePresentationItem(innerItem) : null;
}

public static implicit operator Item(BasePresentationItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}