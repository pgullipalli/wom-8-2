using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.BaseTemplates
{
public partial class BannerImageItem : CustomItem
{

public static readonly string TemplateId = "{AA71DF30-479F-47ED-A7D9-EAACF5DFF75D}";


#region Boilerplate CustomItem Code

public BannerImageItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator BannerImageItem(Item innerItem)
{
	return innerItem != null ? new BannerImageItem(innerItem) : null;
}

public static implicit operator Item(BannerImageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField BannerTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Banner Title"]);
	}
}


public CustomImageField BannerImage
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["Banner Image"]);
	}
}


public CustomTextField BannerContent
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Banner Content"]);
	}
}


#endregion //Field Instance Methods
}
}