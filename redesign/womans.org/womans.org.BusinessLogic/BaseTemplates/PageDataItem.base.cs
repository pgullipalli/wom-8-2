using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;

namespace Womans.CustomItems.BaseTemplates
{
public partial class PageDataItem : CustomItem
{

public static readonly string TemplateId = "{B7376899-E7CB-4557-9654-45DF914904E9}";

#region Inherited Base Templates

private readonly TaggingItem _TaggingItem;
public TaggingItem Tagging { get { return _TaggingItem; } }

#endregion

#region Boilerplate CustomItem Code

public PageDataItem(Item innerItem) : base(innerItem)
{
	_TaggingItem = new TaggingItem(innerItem);

}

public static implicit operator PageDataItem(Item innerItem)
{
	return innerItem != null ? new PageDataItem(innerItem) : null;
}

public static implicit operator Item(PageDataItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods

    public CustomTextField Headline
    {
	    get
	    {
		    return new CustomTextField(InnerItem, InnerItem.Fields["Headline"]);
	    }
    }

    public CustomTextField SubHeadline
    {
        get
        {
            return new CustomTextField(InnerItem, InnerItem.Fields["Sub Headline"]);
        }
    }



    public CustomTextField Introduction
    {
	    get
	    {
		    return new CustomTextField(InnerItem, InnerItem.Fields["Introduction"]);
	    }
    }


    public CustomTextField ContentCopy
    {
	    get
	    {
		    return new CustomTextField(InnerItem, InnerItem.Fields["Content Copy"]);
	    }
    }

#endregion //Field Instance Methods
}
}