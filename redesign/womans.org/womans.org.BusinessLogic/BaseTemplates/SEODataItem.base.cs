using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.BaseTemplates
{
public partial class SEODataItem : CustomItem
{

public static readonly string TemplateId = "{91491034-D6F9-4DCA-95F7-9B265FA3E722}";


#region Boilerplate CustomItem Code

public SEODataItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator SEODataItem(Item innerItem)
{
	return innerItem != null ? new SEODataItem(innerItem) : null;
}

public static implicit operator Item(SEODataItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField BrowserTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Browser Title"]);
	}
}


public CustomTextField MetaKeywords
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Meta Keywords"]);
	}
}


public CustomTextField MetaDescription
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["Meta Description"]);
	}
}


public CustomCheckboxField MetaRobotsNOINDEX
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Meta Robots NOINDEX"]);
	}
}


public CustomCheckboxField MetaRobotsNOFOLLOW
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["Meta Robots NOFOLLOW"]);
	}
}


public CustomLookupField CanonicalURL
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["Canonical URL"]);
	}
}


#endregion //Field Instance Methods
}
}