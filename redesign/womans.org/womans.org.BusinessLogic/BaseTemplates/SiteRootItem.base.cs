using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
using Womans.CustomItems.BaseTemplates;
using Womans.CustomItems.BaseTemplates.SiteSearch;

namespace Womans.CustomItems.BaseTemplates
{
public partial class SiteRootItem : CustomItem
{

public static readonly string TemplateId = "{2C597407-2EBD-4B47-B277-A41B890A8ADF}";

#region Inherited Base Templates

private readonly BasePageItem _BasePageItem;
public BasePageItem BasePage { get { return _BasePageItem; } }
private readonly NavigationPropertiesItem _NavigationPropertiesItem;
public NavigationPropertiesItem NavigationProperties { get { return _NavigationPropertiesItem; } }
private readonly SEODataItem _SEODataItem;
public SEODataItem SEOData { get { return _SEODataItem; } }
private readonly SearchIndexItem _SearchIndexItem;
public SearchIndexItem SearchIndex { get { return _SearchIndexItem; } }

#endregion

#region Boilerplate CustomItem Code

public SiteRootItem(Item innerItem) : base(innerItem)
{
	_BasePageItem = new BasePageItem(innerItem);
	_NavigationPropertiesItem = new NavigationPropertiesItem(innerItem);
	_SEODataItem = new SEODataItem(innerItem);
	_SearchIndexItem = new SearchIndexItem(innerItem);

}

public static implicit operator SiteRootItem(Item innerItem)
{
	return innerItem != null ? new SiteRootItem(innerItem) : null;
}

public static implicit operator Item(SiteRootItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}