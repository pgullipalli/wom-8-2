using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace Womans.CustomItems.BaseTemplates
{
public partial class TaggingItem : CustomItem
{

public static readonly string TemplateId = "{F7FFAF84-28EC-49CE-83F2-89B78E8B3ED5}";


#region Boilerplate CustomItem Code

public TaggingItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator TaggingItem(Item innerItem)
{
	return innerItem != null ? new TaggingItem(innerItem) : null;
}

public static implicit operator Item(TaggingItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTreeListField TaxonomyTerms
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Taxonomy Terms"]);
	}
}


public CustomTreeListField Specialties
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Specialties"]);
	}
}


public CustomTreeListField Locations
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Locations"]);
	}
}


public CustomTreeListField Services
{
	get
	{
		return new CustomTreeListField(InnerItem, InnerItem.Fields["Services"]);
	}
}


#endregion //Field Instance Methods
}
}