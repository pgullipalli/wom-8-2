using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

using Womans.CustomItems.BaseTemplates.SiteSearch;

namespace Womans.CustomItems.BaseTemplates
{
public partial class BasePageItem : CustomItem
{

public static readonly string TemplateId = "{EA71A374-F8B2-4889-A3AF-2BFE73C0C6F5}";

#region Inherited Base Templates

private readonly SiteSearchItem _SiteSearchItem;
public SiteSearchItem SiteSearch { get { return _SiteSearchItem; } }

#endregion

#region Boilerplate CustomItem Code

public BasePageItem(Item innerItem) : base(innerItem)
{
	_SiteSearchItem = new SiteSearchItem(innerItem);

}

public static implicit operator BasePageItem(Item innerItem)
{
	return innerItem != null ? new BasePageItem(innerItem) : null;
}

public static implicit operator Item(BasePageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


#endregion //Field Instance Methods
}
}