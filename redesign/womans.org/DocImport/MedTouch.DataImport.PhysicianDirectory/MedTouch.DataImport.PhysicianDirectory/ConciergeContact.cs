﻿using System.Text;
using System;
using System.Xml.Serialization;
using MedTouch.DataImport.SitecoreFieldModels.ValueFields;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;


namespace MedTouch.PhysicianDirectory.DataImport
{
    public class ConciergeContact : IInsertable
    {
        [ValueField("Contact Type")]
        public string Contact_Type { get; set; }

        [ValueField("Contact Name")]
        public string Contact_Name { get; set; }

        [ValueField("Contact Phone")]
        public string Contact_Phone { get; set; }

        [ValueField("Extension")]
        public string Extension { get; set; }

        [XmlIgnore]
        public bool ShouldDelete { get; set; }

        public string GetName()
        {
            return String.Format("{0} {1}", this.Contact_Type, this.Contact_Phone);
        }

        public string GetTemplate()
        {
            return "Modules/Physician Directory/Component Templates/Concierge Contact";
        }


        public string GetContextSensitiveKey()
        {
            return Contact_Type + Contact_Phone;
        }

        public string GetInterveningParentFolder()
        {
            return "Concierge Contacts";
        }

        public string GetBranchTemplateToInsert()
        {
            return null;
        }

        public bool IsDeltaDeleteEligible()
        {
            return false;
        }
    }
}
