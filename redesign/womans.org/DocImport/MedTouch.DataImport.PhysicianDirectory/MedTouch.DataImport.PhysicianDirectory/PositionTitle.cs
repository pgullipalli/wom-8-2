﻿using System.Text;
using System.Xml.Serialization;
using MedTouch.DataImport.SitecoreFieldModels.ValueFields;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;

namespace MedTouch.PhysicianDirectory.DataImport
{
    public class PositionTitle : IInsertable
    {

        [ValueField("Title")]
        public string Title { get; set; }

        [ValueField("__Sortorder")]
        public string Sortorder { get; set; }

        [XmlIgnore]
        public bool ShouldDelete { get; set; }

        public string GetName()
        {
            return Title != null ? Title.Trim() : Title;
        }

        //we could make these interface properties annotations on the class, though it might be hard to generate their values thru functions
        public string GetTemplate()
        {
            return "MedTouch/Modules/Physicians/Position Title";
        }

        //maybe make context sensitive defined by attributes, 
        public string GetContextSensitiveKey() //generating this key the same way for the DB data -- if only there was a way to pass this function to our code...
        {
            //problem -- here we are generating the key using the value and in the webservice we are generating it using the guid for any reference field
            return Title != null ? Title.Trim() : Title;
        }

        public string GetInterveningParentFolder()
        {
            return "Titles";
        }

        public string GetBranchTemplateToInsert()
        {
            return null;
        }

        public bool IsDeltaDeleteEligible()
        {
            return false;
        }
    }
}
