﻿using System;
using System.Xml.Serialization;

using MedTouch.DataImport.SitecoreFieldModels.ReferenceFields;
using MedTouch.DataImport.SitecoreFieldModels.ValueFields;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;

namespace MedTouch.PhysicianDirectory.DataImport
{
    //sitecore field values are stored as strings, so that's why all data types are string...
    //the idea is that you have to make the values of each of the properties sitecore compatible before you assign them to the property
    //TODO: in another iteration, we could use sitecore datatypes instead...
    
    //XmlElement is for Xml deserialization and serialization
    //ValueField and ReferenceField are for mapping the Properties to sitecore fields

    public class Physician : IInsertable
    {
        [ValueField("First Name")] 
        public string First_Name { get; set; }

        [ValueField("Middle Initial")]
        public string Middle_Initial { get; set; }

        [ValueField("Last Name")]
        public string Last_Name { get; set; }

        //we need the Attribute XmlElement on all --ARRAYS--, otherwise it will assume that Degree_Title is the root element and that there are elements underneath it of type <string> -- so deserialization, serialization, and xsd creation will be screwed up (in the xsd you'd see stuff like type=arrayofstring instead of maxOccurs=unbounded which is what you want
        [ReferenceField("Degree Titles", "/sitecore/content/Global/Physician Directory/Degree Titles"), XmlElement] //perhaps we should do a compile time check on this just to keep people from making errors...
        public string[] Degree_Title { get; set; }
        
        [ReferenceField("Languages Spoken", "/sitecore/content/Global/List Manager/Languages"), XmlElement]
        public string[] Languages_Spoken { get; set; }

        [ValueField("Website")]
        public string Website { get; set; }

        [ValueField("Internal Hospital ID")]
        public string Internal_Hospital_ID { get; set; }

        [ReferenceField("Specialties", "/sitecore/content/Global/List Manager/Specialties"), XmlElement]
        public string[] Specialties { get; set; }

        [ValueField("Locations")]
        public string Locations { get; set; }

        [XmlIgnore]
        public Location[] LocationList { get; set; }

        [ValueField("Certifications")]
        public string Certifications { get; set; }

        [XmlElement]
        public PhysicianOffice[] PhysicianOffice { get; set; }

        [XmlIgnore]
        public bool ShouldDelete { get; set; }

        //probably make this an interface and use it here and on education, titles etc. -- though implementing an interface will 
        //the key 'guarantees' uniqueness in a specific context (forexample, for education items, the key is 'guaranteed' to be unique under a specific physician item - but that physician item isnt part of the key)
        public string GetContextSensitiveKey()
        {
            return this.Internal_Hospital_ID;
        }

        //TODO: its possible that the name has invalid characters in it----like - or parenthesis -- we should probably just let the webservice deal with removing that stuff...
        //InsertableItem is handling that stuff
        //what to do if 2 physicians have same last first and middle initial?
        public string GetName()
        {
            return String.Format("{0} {1} {2}", Last_Name, First_Name, Middle_Initial).Trim();
        }

        public string GetTemplate()
        {
            return "Modules/Physician Directory/Page Templates/Physician";
        }

        public string GetInterveningParentFolder()
        {
            return Last_Name[0].ToString().ToLower();
        }

        //this is missing from mtmc-2.0
        public string GetBranchTemplateToInsert()
        {
            return "/sitecore/templates/Branches/Modules/Physician Directory/Physician";
        }

        public bool IsDeltaDeleteEligible()
        {
            return true;
        }
    }
}
