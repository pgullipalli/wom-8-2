﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MedTouch.DataImport.SitecoreFieldModels.ValueFields;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;
using System.Text.RegularExpressions;

namespace MedTouch.PhysicianDirectory.DataImport
{
    public class Location : IInsertable
    {
        [ValueField("Headline")]
        public string Headline { get; set; }

        [ValueField("Navigation Title")]
        public string Navigation_Title { get; set; }

        [ValueField("Browser Title")]
        public string Browser_Title { get; set; }

        [ValueField("Location Name")]
        public string Location_Name { get; set; }

        [ValueField("Address 1")]
        public string Address_1 { get; set; }

        [ValueField("Address 2")]
        public string Address_2 { get; set; }

        [ValueField("City")]
        public string City { get; set; }

        [ValueField("State")] 
        public string State { get; set; }

        [ValueField("Zip")]
        public string Zip { get; set; }

        [ValueField("Main Phone")]
        public string Main_Phone { get; set; }

        [ValueField("Main Fax")]
        public string Main_Fax { get; set; }

        [ValueField("Website")]
        public string Website { get; set; }

        [ValueField("Latitude")]
        public string Latitude { get; set; }

        [ValueField("Longitude")]
        public string Longitude { get; set; }

        [ValueField("Show in Location Listings")]
        public string Show_In_Location_Listings { get; set; }

        [ValueField("Location ID")]
        public string Location_ID { get; set; }

        [XmlIgnore]
        public bool ShouldDelete { get; set; }

        public string DisplayState { get; set; }

        public string GetName()
        {
            return GeneralizeString(String.Format("{0} {1}", this.Location_Name, this.Address_1), ' ', false).Trim();
        }

        public string GetTemplate()
        {
            return "Global/Page Templates/Location";
        }

        public string GetContextSensitiveKey()
        {
            return String.Format("{0} {1} {2} {3} {4} {5} {6} {7}", this.Location_Name, this.Address_1, this.Address_2, this.City, this.Zip,this.DisplayState, this.Main_Phone, this.Main_Fax).Trim();
        }

        public string GetInterveningParentFolder()
        {
            return "";
        }

        public string GetBranchTemplateToInsert()
        {
            return null;
        }

        public bool IsDeltaDeleteEligible()
        {
            return false;
        }

        // remove special characters, eliminate extra whitespaces, and put in a replacement
        private static string GeneralizeString(string words, char replacement, bool makeLowercase)
        {
            string newString = String.Empty;
            if (!String.IsNullOrEmpty(words))
            {
                newString = Regex.Replace(words, @"[^\w]", " "); // replace special characters with space
                newString = Regex.Replace(newString, @"\s+", " "); // replace multiple space with a single space
                newString = Regex.Replace(newString, @"^[ \t]+|[ \t]+$", ""); // remove leading and trailing space
            }
            return (makeLowercase) ? newString.ToLower() : newString;
        }
    }
}
