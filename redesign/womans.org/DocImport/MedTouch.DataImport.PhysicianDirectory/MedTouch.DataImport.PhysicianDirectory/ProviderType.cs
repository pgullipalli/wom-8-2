﻿using System.Text;
using System;
using System.Xml.Serialization;
using MedTouch.DataImport.SitecoreFieldModels.ValueFields;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;

namespace MedTouch.PhysicianDirectory.DataImport
{
    public class ProviderType : IInsertable
    {
        [ValueField("Provider Type Name")]
        public string Provider_Type_Name { get; set; }

        [ValueField("Provider Type Code")]
        public string Provider_Type_Code { get; set; }

        [XmlIgnore]
        public bool ShouldDelete { get; set; }
        
        public string GetName()
        {
            return Provider_Type_Name;
        }

        public string GetTemplate()
        {
            return "Modules/Physician Directory/Component Templates/Provider Type";
        }

        public string GetContextSensitiveKey()
        {
            return string.Format("{0} {1}", Provider_Type_Name, Provider_Type_Code);
        }

        public string GetInterveningParentFolder()
        {
            return "Provider Types";
        }

        public string GetBranchTemplateToInsert()
        {
            return null;
        }

        public bool IsDeltaDeleteEligible()
        {
            return false;
        }
    }
}
