﻿using System.Text;
using System;
using System.Xml.Serialization;
using MedTouch.DataImport.SitecoreFieldModels.ReferenceFields;
using MedTouch.DataImport.SitecoreFieldModels.ValueFields;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;

namespace MedTouch.PhysicianDirectory.DataImport
{
    public class Education : IInsertable
    {

        [ValueField("Education Type")]
        public string Education_Type { get; set; }

        [ValueField("Institution Name")]
        public string Institution_Name { get; set; }
        
        //[ValueField("Year")]
        //public string Year { get; set; }

        [ValueField("__Sortorder")]
        public string Sortorder { get; set; }

        [XmlIgnore]
        public bool ShouldDelete { get; set; }

        public string GetName()
        {
            return String.Format("{0} {1}", this.Institution_Name, this.Sortorder);
        }

        //we could make these interface properties annotations on the class, though it might be hard to generate their values thru functions
        public string GetTemplate()
        {
            return "Modules/Physician Directory/Component Templates/Education";
        }

        //maybe make context sensitive defined by attributes, 
        public string GetContextSensitiveKey() //generating this key the same way for the DB data -- if only there was a way to pass this function to our code...
        {
            //problem -- here we are generating the key using the value and in the webservice we are generating it using the guid for any reference field
            //return Education_Type + Institution_Name + Year; //right now this is all done by convention...we really need a key builder on both client and server
            return Institution_Name + Sortorder;
        }

        public string GetInterveningParentFolder()
        {
            return "Education";
        }

        public string GetBranchTemplateToInsert()
        {
            return null;
        }

        public bool IsDeltaDeleteEligible()
        {
            return false;
        }
    }
}
