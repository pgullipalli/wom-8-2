﻿using System.Text;
using System;
using System.Xml.Serialization;
using MedTouch.DataImport.SitecoreFieldModels.ValueFields;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;

namespace MedTouch.PhysicianDirectory.DataImport
{
    public class Specialty : IInsertable
    {
        [ValueField("Specialty Name")]
        public string Specialty_Name { get; set; }

        [ValueField("Specialty Code")]
        public string Specialty_Code { get; set; }

        [XmlIgnore]
        public bool ShouldDelete { get; set; }
        
        public string GetName()
        {
            return Specialty_Name;
        }

        public string GetTemplate()
        {
            return "Global/Component Templates/Specialty";
        }

        public string GetContextSensitiveKey()
        {
            return string.Format("{0} {1}", Specialty_Name, Specialty_Code);
        }

        public string GetInterveningParentFolder()
        {
            return "Specialties";
        }

        public string GetBranchTemplateToInsert()
        {
            return null;
        }

        public bool IsDeltaDeleteEligible()
        {
            return false;
        }
    }
}
