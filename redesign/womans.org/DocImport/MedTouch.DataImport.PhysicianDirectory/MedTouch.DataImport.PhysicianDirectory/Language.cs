﻿using System.Text;
using System;
using System.Xml.Serialization;
using MedTouch.DataImport.SitecoreFieldModels.ValueFields;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;

namespace MedTouch.PhysicianDirectory.DataImport
{
    public class Language : IInsertable
    {
        [ValueField("Language Name")]
        public string Language_Name { get; set; }

        [ValueField("Language Code")]
        public string Language_Code { get; set; }

        [XmlIgnore]
        public bool ShouldDelete { get; set; }
        
        public string GetName()
        {
            return Language_Name;
        }

        public string GetTemplate()
        {
            return "Global/Component Templates/Language";
        }

        public string GetContextSensitiveKey()
        {
            return string.Format("{0} {1}", Language_Name, Language_Code);
        }

        public string GetInterveningParentFolder()
        {
            return "Languagea";
        }

        public string GetBranchTemplateToInsert()
        {
            return null;
        }

        public bool IsDeltaDeleteEligible()
        {
            return false;
        }
    }
}
