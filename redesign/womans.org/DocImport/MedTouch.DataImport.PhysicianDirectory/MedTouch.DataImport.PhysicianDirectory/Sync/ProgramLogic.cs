﻿using System;
using System.Collections.Generic;
using System.Linq;
using MedTouch.DataImport.DataValidationAndExceptionHandling;
using MedTouch.DataImport.DatabaseCallProviders;
using MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice;
using MedTouch.DataImport.SitecoreFieldModels;
using MedTouch.DataImport.SitecoreFieldModels.ReferenceFields;
using MedTouch.DataImport.SitecoreFieldModels.ReferenceFields.ValueToGuidResolutionAndSync;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;
using MedTouch.DataImport.Sync.Contracts.SlaveToMaster;
using MedTouch.DataImport.Sync.Mappings;
using MedTouch.DataImport._Class.Global;
using System.Diagnostics;
using MedTouch.DataImport.Sync;
using MedTouch.PhysicianDirectory.DataImport;
//using Sitecore.Diagnostics;

namespace MedTouch.DataImport.PhysicianDirectory.Sync
{
    ///The key we use on the client side is Tuple<ContextParentItemPath,Item.Key,Template>
    ///We use that key to do lookups by field in the DB and then to sync those lookups with items on the client
    ///To do DB lookups we need to pass that Tuple as well as a map provides the DB webservice with the fields nessary for forming a key given a certain template
    ///After that sync occurs, we simply use Guid
 
    public class ProgramLogic
    {
        public bool IsFullImport { get; set; }
        
        public DateTime UpdatedSince { get; set; }

        public ProgramLogic()
        {
        }

        public ProgramLogic(bool isFullImport)
        {
            SetIsFullImport(isFullImport);
        }

        private void SetIsFullImport(bool isFullImport)
        {
            IsFullImport = isFullImport;
        }

        private void SyncReferenceFieldTranslator(ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer, DatabaseWebserviceClient webserviceCallProvider, List<IInsertable> iInsertables)
        {
            referenceFieldValueToGuidResolverAndSyncer.Sync(webserviceCallProvider, iInsertables);
            
        }

        //We support one level of nesting

        private ReferenceFieldValueToGuidResolverAndSyncer BuildReferenceFieldTranslatorForItemAndChildrenAndSyncDatasources(List<IInsertable> iInsertables, DatabaseWebserviceClient webserviceCallProvider)
        {
            ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer = new ReferenceFieldValueToGuidResolverAndSyncer();

            iInsertables
                .ForEach(iInsertable =>                      
                    BuildReferenceFieldValueToGuidResolverAndSyncer(referenceFieldValueToGuidResolverAndSyncer, iInsertable)
                );

            SyncReferenceFieldTranslator(referenceFieldValueToGuidResolverAndSyncer, webserviceCallProvider, iInsertables);

 
            return referenceFieldValueToGuidResolverAndSyncer;
        }

        private static ReferenceFieldValueToGuidResolverAndSyncer BuildReferenceFieldValueToGuidResolverAndSyncer(List<IInsertable> iInsertables)
        {
            ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer = new ReferenceFieldValueToGuidResolverAndSyncer();

            foreach (var iInsertable in iInsertables)
            {
                BuildReferenceFieldValueToGuidResolverAndSyncer(referenceFieldValueToGuidResolverAndSyncer, iInsertable);
            }

            return referenceFieldValueToGuidResolverAndSyncer;

        }

        private static void BuildReferenceFieldValueToGuidResolverAndSyncer(ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer, IInsertable iInsertable)
        {
            var datasourceAndReferenceFieldValuePairs = ReferenceFieldDatasourcePathValuePairBuilder.GetReferenceFieldValuesForTranslationAndSync(iInsertable);

            foreach (var datasourceAndReferenceFieldValuePair in datasourceAndReferenceFieldValuePairs)
            {
                var datasourcePath = datasourceAndReferenceFieldValuePair.Fieldname;
                var referenceFieldValuePair = datasourceAndReferenceFieldValuePair.Value;


                referenceFieldValueToGuidResolverAndSyncer.Add(datasourcePath, referenceFieldValuePair);
            }

            foreach (var iInsertableChild in IInsertableChildrenListBuilder.BuildListOfIInsertableChildren(iInsertable))
            {
                BuildReferenceFieldValueToGuidResolverAndSyncer(referenceFieldValueToGuidResolverAndSyncer, iInsertableChild);
            }
        }

        public void RunImportWrapper(out int physiciansCount)
        {
            List<string> physiciansIds = null;
            if (IsFullImport)
            {
                physiciansIds = DatabaseWebserviceClient.Instance.GetPhysiciansWithChildLocationFolder();
            }
            else
            {
                physiciansIds = DatabaseWebserviceClient.Instance.GetPhysicianListUpdatedSince(UpdatedSince);
            }

            physiciansCount = physiciansIds.Count;
            int counter = 0;
            Console.WriteLine(string.Format("PhysicianImport@Total of imported physicians: {0}", physiciansCount));
            Utility.log(string.Format("PhysicianImport@Total of imported physicians: {0}", physiciansCount));
            foreach (string physiciansId in physiciansIds)
            {
                counter++;

                Console.WriteLine(String.Format("Start Updating Physician #: {0} of {1}", counter, physiciansCount));
                Utility.log(String.Format("Start Updating Physician #: {0} of {1} - guid {2}", counter, physiciansCount, physiciansId));
                Stopwatch sw = Stopwatch.StartNew();
                //Console.WriteLine(String.Format("Updating Clone Destinations, locations, and offices for physician: {0}", physiciansId));                
                //Utility.log(String.Format("Updating Clone Destinations, locations, and offices for physician: {0}", physiciansId));  
              
                DatabaseWebserviceClient.Instance.PhysicianImportWrapper(physiciansId);
                sw.Stop();


                Console.WriteLine(String.Format("End Updating Physician #: {0} of {1}, total time {2}", counter, physiciansCount, sw.Elapsed.TotalSeconds));
                Utility.log(String.Format("End Updating Physician #: {0} of {1}, total time {2}", counter, physiciansCount, sw.Elapsed.TotalSeconds));
                Console.WriteLine("");
            }
        }

        //TODO: IInsertable only gets us so far, it won't verify that the annotations used for reference fields are used properly
        //- that's why gethash and getkeyvaluepairs arent defined -- if we want to verify that the annotations are used properly 
        //we can create a wrapper class which takes an IIdentifiable, verifies that the annotations are good, and creates 
        //Insertable Items from that -- or we just say its up to the programmer to make sure the annotationsa are used properly
        public void run(RootItem rootItem) 
        {
            //if(ConfigurationManager.AppSettings["medtouch.bata..import.logging.enabled"].Equals("true"))
            //    Log.Info("PhysicianImport@BuildReferenceFieldTranslatorForItemAndChildrenAndSyncDatasources", "INFO");

            Stopwatch sw = Stopwatch.StartNew();
            Console.WriteLine("Build ReferenceFieldValueToGuidResolverAndSyncer");
            ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer = BuildReferenceFieldTranslatorForItemAndChildrenAndSyncDatasources(rootItem.Children, DatabaseWebserviceClient.Instance);
            Utility.log("PhysicianImport@BuildInsertableItemsAndTheirChildren");
            sw.Stop();            
            Console.WriteLine("Build ReferenceFieldValueToGuidResolverAndSyncer Complete (" + sw.Elapsed.TotalSeconds + ")");
            Utility.log("Build ReferenceFieldValueToGuidResolverAndSyncer Complete (" + sw.Elapsed.TotalSeconds + ")");

            //do the same thing for locations
            //pass locations into the insertable items creation

            #region PhysicianClean
            //convert physicians back into full objects (hack!)
            List<Physician> physicians = new List<Physician>();
            foreach (IInsertable item in rootItem.Children)
            {
                physicians.Add((Physician)item);
            }

            //build mappings
            Dictionary<string, Guid> stateMapping = buildStateMapping();
            Dictionary<string, Guid> educationTypeMapping = null;
            Dictionary<string, Guid> cloneDestinationMapping = null;

            //build locations, clean, sync to global
            sw = Stopwatch.StartNew();
            Dictionary<ContextSensitiveKeyTemplateTuple, IInsertable> locations = BuildLocationsList(physicians, stateMapping);
            RootItem locationRoot = new RootItem();
            //TODO: Make this a config param
            locationRoot.Path = "/sitecore/content/global/list manager/locations/";
            locationRoot.Children = locations.Values.Cast<IInsertable>().ToList();
            Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableLocationItems = BuildInsertableItemsAndTheirChildren(locationRoot.Children, referenceFieldValueToGuidResolverAndSyncer);
            Guid locationContextRootNodeGuid = DatabaseWebserviceClient.Instance.GetGuidFromPath(locationRoot.Path);
            List<ContextSensitiveKeyTemplateTupleToIDMapping> locationMapping = SyncChildrenOfLocationContextRootItem(insertableLocationItems, locationContextRootNodeGuid);
            sw.Stop();
            Console.WriteLine("Clean and Sync Locations Complete (" + sw.Elapsed.TotalSeconds + ")");
            Utility.log("Clean and Sync Locations Complete (" + sw.Elapsed.TotalSeconds + ")");


            //convert result into a dictionary for quick lookups
            Dictionary<string, Guid> locationContextSensitiveKeyGuidMap = new Dictionary<string, Guid>();
            foreach (ContextSensitiveKeyTemplateTupleToIDMapping m in locationMapping)
            {
                locationContextSensitiveKeyGuidMap.Add(m.ContextSensitiveKeyTemplateTuple.Key, m.ID);
            }

            //clean physicians
            physicians = cleanUpPhysicianItems(physicians, locationContextSensitiveKeyGuidMap, educationTypeMapping, cloneDestinationMapping);
            rootItem.Children = physicians.Cast<IInsertable>().ToList();
            #endregion

            sw = Stopwatch.StartNew();
            Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableItems = BuildInsertableItemsAndTheirChildren(rootItem.Children, referenceFieldValueToGuidResolverAndSyncer);
            sw.Stop();
            Console.WriteLine("BuildInsertableItemsAndTheirChildren for Physicians Complete (" + sw.Elapsed.TotalSeconds + ")");
            Utility.log("BuildInsertableItemsAndTheirChildren for Physicians Complete (" + sw.Elapsed.TotalSeconds + ")");

            Guid iInsertableContextRootNodeGuid = DatabaseWebserviceClient.Instance.GetGuidFromPath(rootItem.Path);

            
            sw = Stopwatch.StartNew();
            SyncChildrenOfIInsertableContextRootItem(insertableItems, iInsertableContextRootNodeGuid);
            sw.Stop();
            Console.WriteLine("SyncChildrenOfIInsertableContextRootItem for Physicians Complete (" + sw.Elapsed.TotalSeconds + ")");
            Utility.log("SyncChildrenOfIInsertableContextRootItem for Physicians Complete (" + sw.Elapsed.TotalSeconds + ")");

            //We handle this by calling web services to handle physician one by one to above web service time out.

            List<string> physiciansIds = null;

            //DEP - changing to call the import wrapper on every physician
            physiciansIds = DatabaseWebserviceClient.Instance.GetPhysicianList();

            //if (physiciansIds != null && physiciansIds.Any())
            //{
            //    DatabaseWebserviceClient.Instance.UpdateCloningDestinationForPhysicians(physiciansIds);
            //}

            int counter = 0;

            foreach (string physiciansId in physiciansIds)
            {
                counter++;

                Console.WriteLine(String.Format("Start Wrapper on Physician #: {0} of {1}", counter, physiciansIds.Count));
                Utility.log(String.Format("Start Wrapper on Physician #: {0} of {1}", counter, physiciansIds.Count));
               
                DatabaseWebserviceClient.Instance.PhysicianImportWrapper(physiciansId);

                Console.WriteLine(String.Format("End Wrapper on Physician #: {0} of {1}", counter, physiciansIds.Count));
                Utility.log(String.Format("End Wrapper on Physician #: {0} of {1}", counter, physiciansIds.Count));
            }

            Utility.log("AddToPublishingQueue");
            DatabaseWebserviceClient.Instance.AddToPublishingQueue();
        }

        private Dictionary<string, Guid> buildStateMapping()
        {
            Dictionary<string, Guid> mapping = new Dictionary<string, Guid>();
            Dictionary<ContextSensitiveKeyTemplateTuple, HashAndID > stateItems = DatabaseWebserviceClient.Instance.GetChildren(new Guid("{F58A137A-D889-456C-BC07-5F76BBAE0C87}"));
            foreach (ContextSensitiveKeyTemplateTuple tuple in stateItems.Keys)
            {
                if (stateItems.ContainsKey(tuple))
                    mapping.Add(tuple.Key, stateItems[tuple].ID);
            }
            return mapping;
        }

        private Dictionary<string, Guid> buildEducationTypeMapping()
        {
            Dictionary<string, Guid> mapping = new Dictionary<string, Guid>();
            Dictionary<ContextSensitiveKeyTemplateTuple, HashAndID> educationTypeItems = DatabaseWebserviceClient.Instance.GetChildren(new Guid("{94FD383C-4254-4253-A0B2-C027C389F6A1}"));
            foreach (ContextSensitiveKeyTemplateTuple tuple in educationTypeItems.Keys)
            {
                if (educationTypeItems.ContainsKey(tuple))
                    mapping.Add(tuple.Key, educationTypeItems[tuple].ID);
            }
            return mapping;
        }

        private List<Physician> cleanUpPhysicianItems(List<Physician> physicians, Dictionary<string, Guid> locationMapping, Dictionary<string, Guid> educationTypeMapping, Dictionary<string, Guid> cloneDestinationMapping)
        {

            foreach(Physician p in physicians) {
                List<string> locationGuids = new List<string>();
                if (p.PhysicianOffice != null)
                {
                    foreach (PhysicianOffice o in p.PhysicianOffice)
                    {
                        if (locationMapping.ContainsKey(o.Location))
                        {
                            o.Location = locationMapping[o.Location].ToString("B").ToUpper();
                            locationGuids.Add(o.Location);
                        }
                    }
                    p.Locations = string.Join("|", locationGuids);
                    p.LocationList = new Location[0];
                }

                //if (p.Education != null)
                //{
                //    foreach (Education e in p.Education)
                //    {
                //        if (educationTypeMapping.ContainsKey(e.Education_Type))
                //            e.Education_Type = educationTypeMapping[e.Education_Type].ToString("B").ToUpper();
                //    }

                //}
                //List<string> cloneDest = new List<string>();
                //if (p.Affiliations.Any())
                //{
                //    if (cloneDestinationMapping.ContainsKey("California"))
                //        cloneDest.Add(cloneDestinationMapping["California"].ToString("B").ToUpper());
                //}
                //foreach (string a in p.Affiliations)
                //{
                //    if (cloneDestinationMapping.ContainsKey(a))
                //        cloneDest.Add(cloneDestinationMapping[a].ToString("B").ToUpper());
                //}
                //p.CloneDestinations = string.Join("|", cloneDest);
                
                

            }
            return physicians;

        }

        private Dictionary<ContextSensitiveKeyTemplateTuple, IInsertable> BuildLocationsList(List<Physician> physicians, Dictionary<string, Guid> stateMapping)
        {
            Dictionary<ContextSensitiveKeyTemplateTuple, IInsertable> locationList = new Dictionary<ContextSensitiveKeyTemplateTuple, IInsertable>();
            List<string> processedKeys = new List<string>();

            foreach(Physician p in physicians) {
                if (p.LocationList.Any())
                {
                    foreach (Location l in p.LocationList)
                    {
                        if (stateMapping.ContainsKey(l.State))
                        {
                            l.State = stateMapping[l.State].ToString("B").ToUpper();  
                        }
                        if (!processedKeys.Contains(l.GetContextSensitiveKey()))
                        {
                            
                            locationList.Add(new ContextSensitiveKeyTemplateTuple(l.GetContextSensitiveKey(), l.GetTemplate()), (IInsertable)l);
                            processedKeys.Add(l.GetContextSensitiveKey());
                        }                  
                    }
                }
            }



            return locationList;
        }

        private List<ContextSensitiveKeyTemplateTupleToIDMapping> SyncChildrenOfLocationContextRootItem(Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableItems, Guid iInsertableContextRootNodeGuid)
        {
            Console.WriteLine("SyncChildrenOfIInsertableContextRootItem: " + iInsertableContextRootNodeGuid.ToString());
            List<ContextSensitiveKeyTemplateTupleToIDMapping> keyIDPairs = null;
            keyIDPairs =
                Syncer.SyncChildrenOfIInsertableContextRootItem(
                        insertableItems,
                        iInsertableContextRootNodeGuid, //has to be guid because path is NOT unique (2 physicians could have the same name for example)
                        DatabaseWebserviceClient.Instance,
                        Syncer.HowToDealWithUnusedItemsOnSlave.Keep //this policy does not apply to the reference items, they are handled differently
            );

            List<ContextSensitiveKeyTemplateTupleToIDMapping> templateTupleList = keyIDPairs
                .ToList()
                .Where(contextSensitiveKeyTemplateTupleToIDMapping =>
                    insertableItems[contextSensitiveKeyTemplateTupleToIDMapping.ContextSensitiveKeyTemplateTuple].Children != null &&
                    insertableItems[contextSensitiveKeyTemplateTupleToIDMapping.ContextSensitiveKeyTemplateTuple].Children.Keys.Any()) //this makes sure there is at least 1 element in the list, though that element could be null and it will return true
                .ToList();

            return keyIDPairs;
        }

        private void SyncChildrenOfIInsertableContextRootItem(Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableItems, Guid iInsertableContextRootNodeGuid)
        {
            Console.WriteLine("SyncChildrenOfIInsertableContextRootItem: " + iInsertableContextRootNodeGuid.ToString());
            List<ContextSensitiveKeyTemplateTupleToIDMapping> keyIDPairs = null;
            if (AreDeltaDeleteEligible(insertableItems))
            {
                insertableItems = DeleteShouldDeleteItems(DatabaseWebserviceClient.Instance, insertableItems, DatabaseWebserviceClient.Instance.GetChildren(iInsertableContextRootNodeGuid));

                keyIDPairs =
                    Syncer.SyncChildrenOfIInsertableContextRootItem(
                            insertableItems,
                            iInsertableContextRootNodeGuid, //has to be guid because path is NOT unique (2 physicians could have the same name for example)
                            DatabaseWebserviceClient.Instance,
                            Syncer.HowToDealWithUnusedItemsOnSlave.Keep //this policy does not apply to the reference items, they are handled differently
                );
            }
            else
            {
                keyIDPairs =
                    Syncer.SyncChildrenOfIInsertableContextRootItem(
                            insertableItems,
                            iInsertableContextRootNodeGuid, //has to be guid because path is NOT unique (2 physicians could have the same name for example)
                            DatabaseWebserviceClient.Instance,
                            Syncer.HowToDealWithUnusedItemsOnSlave.Discard //this policy does not apply to the reference items, they are handled differently
                );
            }
            
            
            List<ContextSensitiveKeyTemplateTupleToIDMapping> templateTupleList = keyIDPairs
                .ToList()
                .Where(contextSensitiveKeyTemplateTupleToIDMapping =>
                    insertableItems[contextSensitiveKeyTemplateTupleToIDMapping.ContextSensitiveKeyTemplateTuple].Children != null &&
                    insertableItems[contextSensitiveKeyTemplateTupleToIDMapping.ContextSensitiveKeyTemplateTuple].Children.Keys.Any()) //this makes sure there is at least 1 element in the list, though that element could be null and it will return true
                .ToList();

                foreach (ContextSensitiveKeyTemplateTupleToIDMapping contextSensitiveKeyTemplateTupleToIDMapping in templateTupleList) {
                    SyncChildrenOfIInsertableContextRootItem(
                        insertableItems[contextSensitiveKeyTemplateTupleToIDMapping.ContextSensitiveKeyTemplateTuple].Children, //TODO: this could be null or empty list...what to do then.
                        contextSensitiveKeyTemplateTupleToIDMapping.ID);  
                }
        }

        private bool AreDeltaDeleteEligible(Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableItems)
        {
            if (IsFullImport)
            {
                return false;
            }

            foreach (ContextSensitiveKeyTemplateTuple key in insertableItems.Keys)
            {
                InsertableItem insertableItem = insertableItems[key];
                if (!insertableItem.IsDeltaDeleteEligible)
                {
                    return false;
                }
            }

            return true;
        }

        private Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> DeleteShouldDeleteItems(IDatabaseCallProvider slaveCallProvider, Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableItems, Dictionary<ContextSensitiveKeyTemplateTuple, HashAndID> itemsInSitecore)
        {
            Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> notDeletedInsertableItems = new Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem>();
            var items = (from key in insertableItems.Keys
                                                   let insertableItem = insertableItems[key]
                                                   select new
                                                       {
                                                           Key = key,
                                                           InsertableItem = insertableItem,
                                                           ShouldDelete = insertableItem.IsDeltaDeleteEligible && insertableItem.ShouldDelete
                                                       }).ToList();

            foreach (var item in items)
            {
                HashAndID hashAndID;
                if (item.ShouldDelete && itemsInSitecore.TryGetValue(item.Key, out hashAndID))
                {
                        slaveCallProvider.DeleteItem(hashAndID.ID);
                        Utility.log(String.Format("Deleted physician (Sitecore ID: {0}, Key: {1})", hashAndID.ID, item.InsertableItem.Key));
                }
                else if (!item.ShouldDelete)
                {
                    notDeletedInsertableItems.Add(item.Key, item.InsertableItem);
                }
            }

            return notDeletedInsertableItems;
        }

        //call to handle unlimited levels of nesting

        private static Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> BuildInsertableLocationItemsAndTheirChildren(List<IInsertable> iInsertables, ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer)
        {
            if (iInsertables == null || iInsertables.Count() == 0) //recursive base case
            {
                return null;
            }

            if (iInsertables.Where(iInsertable => String.IsNullOrWhiteSpace(iInsertable.GetContextSensitiveKey())).Any()) 
            {
                Console.WriteLine(String.Format("Context sensitive key is null/empty, skipping")); //if we wanna get the parent, we gotta go up in the stack trace
                return null;
                //throw new Exception("Context sensitive key is null/empty"); //how should we handle this?
            }

            Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableItems = new Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem>();

            //TODO: the problem here is duplicate keys...what to do if we encounter duplicate keys
            //TODO: this is where we should try to catch as many exceptions as possible

            foreach (var iInsertable in iInsertables) {
                try
                {
                    if(iInsertable.ToString().Contains("MedTouch.Beta.Physicians.Import.Location"))
                    {
                        var insertableItem = new InsertableItem(
                             iInsertable.GetName(),
                             iInsertable.GetTemplate(),
                             iInsertable.GetBranchTemplateToInsert(),
                             iInsertable.GetContextSensitiveKey(),
                             iInsertable.GetInterveningParentFolder(),
                             BuildInsertableLocationItemsAndTheirChildren(IInsertableChildrenListBuilder.BuildListOfIInsertableChildren(iInsertable), referenceFieldValueToGuidResolverAndSyncer), //we call this 2wice, once here, and once in BuildReferenceFieldTranslatorForItemAndChildrenAndSyncDatasources
                             FieldnameValuePairBuilder.BuildFieldnameValuePairs(iInsertable, referenceFieldValueToGuidResolverAndSyncer)
                             );

                        insertableItem.IsDeltaDeleteEligible = iInsertable.IsDeltaDeleteEligible();
                        insertableItem.ShouldDelete = iInsertable.ShouldDelete;
                        insertableItems.Add(new ContextSensitiveKeyTemplateTuple(iInsertable.GetContextSensitiveKey(), iInsertable.GetTemplate()), insertableItem);
                    }
                }
                catch (InvalidKeyException)
                {
                    System.Console.WriteLine("Invalid Key"); //TODO: catch different types of errors + report them differntly -- the idea is to do the field validation in InsertableItem and throw a different error based on which field is being accessed (i.e. InvalidKeyError ...)
                }
                catch (ArgumentException)
                {
                    System.Console.WriteLine(String.Format("An item with the same key has already been added {0}", iInsertable.GetName())); //TODO: better error msg
                }
                catch
                {
                    System.Console.WriteLine("An error has occured in creating an insertableItem");
                }
            }
    
            return insertableItems;
        }
        private static Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> BuildInsertableItemsAndTheirChildren(List<IInsertable> iInsertables, ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer)
        {
            if (iInsertables == null || iInsertables.Count() == 0) //recursive base case
            {
                return null;
            }

            if (iInsertables.Where(iInsertable => String.IsNullOrWhiteSpace(iInsertable.GetContextSensitiveKey())).Any()) 
            {
                Console.WriteLine(String.Format("Context sensitive key is null/empty, skipping")); //if we wanna get the parent, we gotta go up in the stack trace
                return null;
                //throw new Exception("Context sensitive key is null/empty"); //how should we handle this?
            }

            Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableItems = new Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem>();

            //TODO: the problem here is duplicate keys...what to do if we encounter duplicate keys
            //TODO: this is where we should try to catch as many exceptions as possible

            foreach (var iInsertable in iInsertables) {
                try
                {

                    var insertableItem = new InsertableItem(
                         iInsertable.GetName(),
                         iInsertable.GetTemplate(),
                         iInsertable.GetBranchTemplateToInsert(),
                         iInsertable.GetContextSensitiveKey(),
                         iInsertable.GetInterveningParentFolder(),
                         BuildInsertableItemsAndTheirChildren(IInsertableChildrenListBuilder.BuildListOfIInsertableChildren(iInsertable), referenceFieldValueToGuidResolverAndSyncer), //we call this 2wice, once here, and once in BuildReferenceFieldTranslatorForItemAndChildrenAndSyncDatasources
                         FieldnameValuePairBuilder.BuildFieldnameValuePairs(iInsertable, referenceFieldValueToGuidResolverAndSyncer)
                         );

                    insertableItem.IsDeltaDeleteEligible = iInsertable.IsDeltaDeleteEligible();
                    insertableItem.ShouldDelete = iInsertable.ShouldDelete;
                    insertableItems.Add(new ContextSensitiveKeyTemplateTuple(iInsertable.GetContextSensitiveKey(), iInsertable.GetTemplate()), insertableItem);
                }
                catch (InvalidKeyException)
                {
                    System.Console.WriteLine("Invalid Key"); //TODO: catch different types of errors + report them differntly -- the idea is to do the field validation in InsertableItem and throw a different error based on which field is being accessed (i.e. InvalidKeyError ...)
                }
                catch (ArgumentException)
                {
                    System.Console.WriteLine(String.Format("An item with the same key has already been added {0}", iInsertable.GetName())); //TODO: better error msg
                }
                catch
                {
                    System.Console.WriteLine("An error has occured in creating an insertableItem");
                }
            }
    
            return insertableItems;
        }

        
    }

}
