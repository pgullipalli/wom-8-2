﻿using System.Text;
using System;
using System.Xml.Serialization;
using MedTouch.DataImport.SitecoreFieldModels.ValueFields;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;

namespace MedTouch.PhysicianDirectory.DataImport
{
    public class Affiliation : IInsertable
    {
        [ValueField("Affiliation Name")]
        public string Affiliation_Name { get; set; }

        [ValueField("Affiliation Name")]
        public string Affiliation_Code { get; set; }

        [XmlIgnore]
        public bool ShouldDelete { get; set; }
        
        public string GetName()
        {
            return Affiliation_Name;
        }

        public string GetTemplate()
        {
            return "Modules/Physician Directory/Component Templates/Affiliation";
        }

        public string GetContextSensitiveKey()
        {
            return string.Format("{0} {1}", Affiliation_Name, Affiliation_Code);
        }

        public string GetInterveningParentFolder()
        {
            return "Affiliations";
        }

        public string GetBranchTemplateToInsert()
        {
            return null;
        }

        public bool IsDeltaDeleteEligible()
        {
            return false;
        }
    }
}
