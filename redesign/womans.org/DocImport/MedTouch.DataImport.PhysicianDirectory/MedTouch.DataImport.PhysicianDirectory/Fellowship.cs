﻿using System.Text;
using System;
using System.Xml.Serialization;
using MedTouch.DataImport.SitecoreFieldModels.ValueFields;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;

namespace MedTouch.PhysicianDirectory.DataImport
{
    public class Fellowship : IInsertable
    {
        [ValueField("Institution Name")]
        public string Institution_Name { get; set; }
        
        //[ValueField("Year")]
        //public string Year { get; set; }

        [XmlIgnore]
        public bool ShouldDelete { get; set; }

        public string GetName()
        {
            //return String.Format("{0} {1}", this.Institution_Name, this.Year);
            return this.Institution_Name;
        }

        public string GetTemplate()
        {
            return "Modules/Physician Directory/Component Templates/Fellowship";
        }


        public string GetContextSensitiveKey()
        {
            //return String.Format("{0} {1}", this.Institution_Name, this.Year);
            return this.Institution_Name;
        }

        public string GetInterveningParentFolder()
        {
            return "Fellowship";
        }

        public string GetBranchTemplateToInsert()
        {
            return null;
        }

        public bool IsDeltaDeleteEligible()
        {
            return false;
        }
    }
}
