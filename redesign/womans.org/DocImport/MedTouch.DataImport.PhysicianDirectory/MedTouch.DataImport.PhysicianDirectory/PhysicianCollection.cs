﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;


namespace MedTouch.PhysicianDirectory.DataImport
{
    [XmlRootAttribute("Physicians")]
    public class PhysicianCollection
    {
        [XmlElement("Physician")]
        public IInsertable[] Physicians { get; set; }
    }
}
