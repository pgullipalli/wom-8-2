﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MedTouch.DataImport.SitecoreFieldModels.ReferenceFields;
using MedTouch.DataImport.SitecoreFieldModels.ValueFields;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;

//TODO: Cool idea-->Autogenerate this C# class file from sitecore templates/dummy items

namespace MedTouch.PhysicianDirectory.DataImport
{
    public class PhysicianOffice : IInsertable
    {
        
        [ValueField("Office Id")]
        public string Office_Id { get; set; }

        [ValueField("Office Name")]
        public string Office_Name { get; set; }

        [ValueField("Location")]
        public string Location { get; set; }

        [ValueField("Office Phone")]
        public string Office_Phone { get; set; }

        [ValueField("Office Fax")]
        public string Office_Fax { get; set; }

        [ValueField("Office Answering Phone")]
        public string Answering_Phone { get; set; }

        //[ValueField("Office Hours")]
        //public string Office_Hours { get; set; }

        //[ValueField("Email Address")]
        //public string Email_Address { get; set; }

        //[ValueField("Accepting New Patients")]
        //public string Accepting_New_Patients { get; set; }

        //[ValueField("Primary Office")]
        //public string Primary_Office { get; set; }

        //[ValueField("Address 2")]
        //public string Address_2 { get; set; }

        //[ValueField("Mass Transit Hours")]
        //public string Mass_Transit_Hours { get; set; }

        //[ValueField("Wheel Chair Accessible")]
        //public string Wheel_Chair_Accessible { get; set; }

        //[ValueField("Has Weekend Hours")]
        //public string Has_Weekend_Hours { get; set; }

        //[ValueField("Has Evening Hours")]
        //public string Has_Evening_Hours { get; set; }

        [XmlIgnore]
        public bool ShouldDelete { get; set; }
        
        //TODO: How are we determining the name?
        //What if office name is blank? -- then we can't use office name to determine the name...
        public string GetName()
        {
            return String.Format("{0}", this.Office_Id).Trim();
        }

        public string GetTemplate()
        {
            return "Modules/Physician Directory/Component Templates/Physician Office";
        }

        public string GetContextSensitiveKey()
        {
            return String.Format("{0} {1} {2} {3} ", this.Office_Name, this.Office_Id, this.Office_Phone, this.Office_Fax);
        }

        public string GetInterveningParentFolder()
        {
            return "Offices";
        }

        public string GetBranchTemplateToInsert()
        {
            return null;
        }

        public bool IsDeltaDeleteEligible()
        {
            return false;
        }
    }
}
