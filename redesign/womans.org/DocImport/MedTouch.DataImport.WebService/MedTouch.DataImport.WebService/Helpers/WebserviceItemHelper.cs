﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using MedTouch.Common.Helpers;
using MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice.Webservice.Entities;
using Sitecore.Configuration;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Security.Accounts;
using Sitecore.SecurityModel;
using Sitecore.Exceptions;
using Sitecore.Diagnostics;
using MedTouch.DataImport.Webservice._Class.Global;
using Sitecore.Globalization;
using Sitecore.Publishing;
using Sitecore.Text;
using System.Diagnostics;


namespace MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice.Webservice.Helpers
{
    public class WebserviceItemHelper
    {
        private const string Pipe = "|";

        private static Database masterDB = Factory.GetDatabase("master");
        private static readonly string SpecialtySubspecialtyDelimiter = Settings.GetSetting("MedTouch.DataImport.WebService.Miscellaneous.SpecialtySubspecialtyDelimiter");
        private static readonly ID GlobalSpecialtiesFolderGUId = ParseID(Settings.GetSetting("MedTouch.DataImport.WebService.GlobalSpecialtiesFolderGUID"));
        private static readonly TemplateItem SpecialtyTemplateItem = CreateNewTemplateItem(Settings.GetSetting("MedTouch.DataImport.WebService.TemplateId.Specialty"));
        private static readonly TemplateItem SubspecialtyTemplateItem = CreateNewTemplateItem(Settings.GetSetting("MedTouch.DataImport.WebService.TemplateId.Subspecialty"));
        private static readonly string SpecialtiesFieldName = Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameSpecialties");
        private static readonly string SpecialtyNameField = Settings.GetSetting("MedTouch.DataImport.WebService.SpecialtyNameField");
        private static readonly string SubSpecialtyNameField = Settings.GetSetting("MedTouch.DataImport.WebService.SubSpecialtyNameField");
        private static readonly string CaliforniaMainSite = Settings.GetSetting("MedTouch.DataImport.WebService.Miscellaneous.CaliforniaMainSite");
        
        public static Item TryCreateSitecoreItemUsingBranch(Guid parentItemID, string branchPath, string itemName)
        {
            // Guid to be returned
            Item newItem = null;

            // get location of item to create
            var locationItem = TryGetItem(parentItemID);
            var branchItem = TryGetBranchItem(branchPath);

            User user = TryGetServiceUser();
            using (new UserSwitcher(user))
            {
                //TODO: what to do if item name is invalid?
                newItem = TryAddItem(itemName, branchItem, locationItem);
                Utility.log(string.Format("Added new item using branch: {0}", newItem.Name));
            }

            return newItem;
        }

        private static string GetStateGuidFromValue(string value)
        {

            try
            {
                User user = TryGetServiceUser();
                using (new UserSwitcher(user))
                {

                    Item statesFolderItem = masterDB.GetItem(new ID(Settings.GetSetting("MedTouch.DataImport.WebService.GlobalStatesGUID")));
                    if (statesFolderItem != null)
                    {
                        List<Item> states =
                            statesFolderItem.Axes.GetDescendants().Where(
                                i =>
                                i.Template.Name.Equals(
                                    Settings.GetSetting("MedTouch.DataImport.WebService.GenericNameState"),
                                    StringComparison.OrdinalIgnoreCase)).ToList();
                        foreach (Item state in states)
                        {
                            if (state.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameAbbreviation")].Value == value.Trim())
                                return state.ID.ToString();
                        }
                    }
                }
            }
            catch
            {
                Utility.log(String.Format("GetStateGuidFromValue {0}", value));
                throw new Exception(String.Format("GetStateGuidFromValue {0}", value));
            }
            return null;
        }
        public static void TryUpdateEducationItem(Item itemEducation)
        {
            User user = TryGetServiceUser();

            using (new UserSwitcher(user))
            {
                itemEducation.Editing.BeginEdit();

                try
                {
                    var iField = itemEducation.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericFieldNameEducationType")];
                    if (iField != null)
                    {
                        Item educationItem = masterDB.GetItem(new ID(itemEducation.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericFieldNameEducationType")].Value));
                        if(educationItem != null)
                        {
                            iField.SetValue(educationItem.ID.ToString(), true);
                            //iField.Item = educationItem;
                            //iField.SetValue(educationItem.na);
                            //iField.SetValue(educationItem.ID.ToString(), true);                            
                        }
                    }
                    itemEducation.Editing.EndEdit();
                }
                catch (Exception ex)
                {
                    Utility.log(string.Format("Failed to update location stage: {0}|{1}", itemEducation.Name, itemEducation.ID));
                    itemEducation.Editing.CancelEdit();
                }
            }            
        }
        public static void TryUpdateLocationItem(Item itemLocation)
        {
            Utility.log(string.Format("Updating location -> State: {0}|{1}", itemLocation.Name, itemLocation.ID));
            string strState = itemLocation.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericNameState")].Value;
            string strStateGUID = GetStateGuidFromValue(strState);
            if (strStateGUID == null)
                return;
            User user = TryGetServiceUser();

            using (new UserSwitcher(user))
            {
                itemLocation.Editing.BeginEdit();

                try
                {
                    var iField = itemLocation.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericNameState")];
                    if (iField != null)
                    {
                        iField.SetValue(strStateGUID, true);
                    }
                    itemLocation.Editing.EndEdit();
                }
                catch (Exception ex)
                {
                    Utility.log(string.Format("Failed to update location stage: {0}|{1}", itemLocation.Name,itemLocation.ID));
                    itemLocation.Editing.CancelEdit();
                }
            }
        }

        public static void TryUpdateSitecoreItem(Item itemToUpdate, string field, string value)
        {
            User user = TryGetServiceUser();

            using (new UserSwitcher(user))
            {
                itemToUpdate.Editing.BeginEdit();

                try
                {
                    var iField = itemToUpdate.Fields[field];
                    if (iField != null)
                    {
                        if (!TryParseImportItems(iField, value))
                        {    
                            var val = HttpContext.Current.Server.HtmlDecode(value);
                            if (field.Equals("Content Copy", StringComparison.CurrentCultureIgnoreCase) || field.Equals("Certifications", StringComparison.CurrentCultureIgnoreCase))
                            {
                                val = val.Replace("&", "&amp;");
                            }
                            iField.SetValue(val, true);    
                        }
                    }
                    
                    if (string.Equals(field, "%Folder%", StringComparison.CurrentCultureIgnoreCase))
                    {
                        TryParseImportFolder(itemToUpdate, value);    
                    }

                    itemToUpdate.Editing.EndEdit();
                }
                catch (Exception ex)
                {
                    //TODO: Log this error
                    Utility.log(string.Format("Failed to update item: {0} | {1} | {2}", itemToUpdate.Name, field, value));
                    itemToUpdate.Editing.CancelEdit();
                }
            }
        }

        public static void TryUpdateAllFieldsSitecoreItem(Item itemToUpdate, Dictionary<string, string> fieldValues) 
        {
            Sitecore.Diagnostics.Log.Warn("TryUpdateAllFieldsSitecoreItem: " + itemToUpdate.ID + " - START",new object());
            Stopwatch sw = Stopwatch.StartNew();
            User user = TryGetServiceUser();

            using (new UserSwitcher(user))
            {
                itemToUpdate.Editing.BeginEdit();

                try
                {
                    foreach (string field in fieldValues.Keys)
                    {

                        string value = fieldValues[field];
                        var iField = itemToUpdate.Fields[field];
                        if (iField != null)
                        {
                            
                            if (!TryParseImportItems(iField, value))
                            {
                                var val = HttpContext.Current.Server.HtmlDecode(value);
                                if (field.Equals("Content Copy", StringComparison.CurrentCultureIgnoreCase) || field.Equals("Certifications", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    val = val.Replace("&", "&amp;");
                                }
                                iField.SetValue(val, true);
                            }
                        }

                        if (string.Equals(field, "%Folder%", StringComparison.CurrentCultureIgnoreCase))
                        {
                            TryParseImportFolder(itemToUpdate, value);
                        }
                    }
                    sw.Stop();
                    Sitecore.Diagnostics.Log.Warn("TryUpdateAllFieldsSitecoreItem: " + sw.Elapsed.TotalSeconds + " sec to set fields", new object());
                    sw = Stopwatch.StartNew();
                    itemToUpdate.Editing.EndEdit();
                    sw.Stop();
                    Sitecore.Diagnostics.Log.Warn("TryUpdateAllFieldsSitecoreItem: " + sw.Elapsed.TotalSeconds + " sec to save (cloning)", new object());
                }
                catch (Exception ex)
                {
                    //TODO: Log this error
                    Utility.log(string.Format("Failed to update item: {0} | {1}", itemToUpdate.Name, fieldValues.ToString()));
                    itemToUpdate.Editing.CancelEdit();
                }
            }
        }

        private static bool TryCreateSpecialtiesSubspecialties(string fieldName, string fieldValues, out string guids)
        {
            guids = string.Empty;

            if (string.IsNullOrWhiteSpace(fieldName) || !string.Equals(fieldName, SpecialtiesFieldName, StringComparison.CurrentCultureIgnoreCase))
            {
                return false;
            }

            Item specialtiesFolder = TryGetItemFromGuid(GlobalSpecialtiesFolderGUId.Guid);
            if (specialtiesFolder == null)
            {
                return false;
            }

            List<string> guidList = new List<string>();
            foreach (string fieldValue in fieldValues.Split(Pipe.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList().Select(degree => degree.Trim()).ToList())
            {
                List<string> specialtySubspecialties = fieldValue.Split(SpecialtySubspecialtyDelimiter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList().Select(degree => degree.Trim()).ToList();
                if (specialtySubspecialties.Count < 1)
                {
                    continue;
                }
                
                Item specialty = (from child in specialtiesFolder.GetChildren()
                                  where string.Equals(child.Name, ProposeValidItemName(specialtySubspecialties[0]), StringComparison.CurrentCultureIgnoreCase)
                                  select child).FirstOrDefault();

                if (specialty == null)
                {
                    specialty = TryAddItem(ProposeValidItemName(specialtySubspecialties[0]), SpecialtyTemplateItem, specialtiesFolder);
                    specialty.Editing.BeginEdit();
                    specialty.Fields[SpecialtyNameField].SetValue(specialtySubspecialties[0], true);
                    specialty.Editing.EndEdit();
                }

                if(specialtySubspecialties.Count == 1)
                {
                    guidList.Add(ToGuidString(specialty.ID.Guid));
                    continue;
                }

                Item subspecialty = (from child in specialty.GetChildren()
                                     where string.Equals(child.Name, ProposeValidItemName(specialtySubspecialties[1]), StringComparison.CurrentCultureIgnoreCase)
                                     select child).FirstOrDefault();

                if (subspecialty == null)
                {
                    subspecialty = TryAddItem(ProposeValidItemName(specialtySubspecialties[1]), SubspecialtyTemplateItem, specialty);
                    subspecialty.Editing.BeginEdit();
                    subspecialty.Fields[SubSpecialtyNameField].SetValue(specialtySubspecialties[1], true);
                    subspecialty.Editing.EndEdit();
                }

                guidList.Add(ToGuidString(specialty.ID.Guid));
                guidList.Add(ToGuidString(subspecialty.ID.Guid));
            }
            guids = string.Join(Pipe, guidList.Distinct());
            return true;
        }

        private static string ProposeValidItemName(string itemName)
        {
            return ItemUtil.ProposeValidItemName(itemName);
        }
            

        private static string ToGuidString(Guid guid)
        {
            if (guid != Guid.Empty)
            {
                return guid.ToString("B");
            }

            return string.Empty;
        }

        public static void TryUpdateSitecoreItem(Guid itemID, string field, string value)
        {
            
            Item itemToUpdate = TryGetItem(itemID);

            TryUpdateSitecoreItem(itemToUpdate, field, value);

        }

        public static void TryUpdateAllFieldsSitecoreItem(Guid itemID, Dictionary<string,string> fieldValues)
        {

            Item itemToUpdate = TryGetItem(itemID);

            TryUpdateAllFieldsSitecoreItem(itemToUpdate, fieldValues);

        }

        private static void updataOfficeItem(Item physician, Item office)
        {
            //find match location items
            Item matchedLocationItem = MatchOfficeToLocationItem(physician, office);
            if (matchedLocationItem != null)
            {
                Item existingLocation = IsExistingLocationItemInGlobal(matchedLocationItem);
                if (existingLocation == null)
                {
                    TryMoveLocationItemToGlobal(matchedLocationItem);
                    UpdateOfficeLocationField(office, matchedLocationItem.ID.ToString());
                    UpdatePhysicianLocationField(physician, matchedLocationItem.ID.ToString());
					//only update when newly add location item
                	TriggleOnSaveLocation(matchedLocationItem);
                }
                else
                {
                    UpdateOfficeLocationField(office, existingLocation.ID.ToString());
                    UpdatePhysicianLocationField(physician, existingLocation.ID.ToString());
                }
            }

        }

		private static void TriggleOnSaveLocation(Item locationItem)
		{
			using (new SecurityDisabler())
			{
				if(locationItem != null)
				{
					string field = "Address 1";
					string strVal = locationItem.Fields[field].Value;
					// add trailing space
					locationItem.Editing.BeginEdit();
					locationItem.Fields[field].Value = strVal + " ";
					locationItem.Editing.EndEdit();

					// trim
					string strValNew = locationItem.Fields[field].Value;
					locationItem.Editing.BeginEdit();
					locationItem.Fields[field].Value = strValNew.Trim();
					locationItem.Editing.EndEdit();

					System.Threading.Thread.Sleep(12000);
				}

			}
		}

        public static void PhysicianImportWrapper(string physicianId)
        {
            User user = TryGetServiceUser();
    
            try
            {
                using (new UserSwitcher(user))
                {
                    Item physician = masterDB.GetItem(new ID(physicianId));

                    if (physician == null)
                        return;
                    
                    //just calling save on the physician with a trivial change to make sure cloning and save action worked properly.
                     using (new SecurityDisabler())
                     {
                         physician.Editing.BeginEdit();
                         // trick to change something in the item, otherwise Sitecore won't save
                         physician.Fields[FieldIDs.Updated].Reset();
                         physician.Editing.EndEdit();
                     }
                }

            }
            catch (Exception ex)
            {
                Utility.log(String.Format("Error in PhysicianImportWrapper for physician: {0}", physicianId));
                throw new Exception(String.Format("Error in PhysicianImportWrapper for physician: {0}", physicianId), ex);
            }

        }

        private static void DeletedLocationFolder(Item itemToDelete)
        {
            User user = TryGetServiceUser();

            try
            {
                using (new UserSwitcher(user))
                {
                    itemToDelete.Delete();
                }
            }
            catch
            {
                Utility.log(String.Format("Error deleting item {0}", itemToDelete.ID.ToString()));
                throw new Exception(String.Format("Error deleting item {0}", itemToDelete.ID.ToString()));
            }            
        }

        private static void UpdateOfficeLocationField(Item office, string locationId)
        {
            User user = TryGetServiceUser();

            using (new UserSwitcher(user))
            {
                office.Editing.BeginEdit();

                try
                {
                    var iField = office.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameLocation")];
                    if (iField != null)
                    {
                        iField.SetValue(locationId, true);
                    }
                    office.Editing.EndEdit();
                }
                catch (Exception ex)
                {
                    office.Editing.CancelEdit();
                    Utility.log(String.Format("Error upadte office location field {0}", office.ID.ToString()));
                    throw new Exception(String.Format("Error upadte office location field {0}", office.ID.ToString()), ex); 
                }
            }
        }
        private static void UpdatePhysicianLocationField(Item physician, string locationId)
        {
            User user = TryGetServiceUser();

            using (new UserSwitcher(user))
            {
                try
                {
                    physician.Editing.BeginEdit();
                    MultilistField multilistField = physician.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameLocations")];
                    if (multilistField != null)
                    {
                        multilistField.Add(locationId);
                    }
                    physician.Editing.EndEdit();
                }
                catch (Exception ex)
                {
                    physician.Editing.CancelEdit();
                    Utility.log(String.Format("Error upadte office location field {0}", physician.ID.ToString()));
                    throw new Exception(String.Format("Error upadte office location field {0}", physician.ID.ToString()), ex);
                }
            }            
        }

        private static Item IsExistingLocationItemInGlobal(Item item)
        {
            User user = TryGetServiceUser();
            try
            {
                using (new UserSwitcher(user))
                {
                    Item folderItemMoveTo = masterDB.GetItem(Settings.GetSetting("MedTouch.DataImport.WebService.GlobalLocationsPath"));
                    List<Item> locations = folderItemMoveTo.Axes.GetDescendants()
                        .Where(i => i.Template.Name.Equals(Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameLocation"), StringComparison.OrdinalIgnoreCase))
                        .ToList();
                    foreach (Item location in locations)
                    {
                        if (location.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameAllUpdateableFieldsHash")].Value == item.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameAllUpdateableFieldsHash")].Value)
                        {
                            return location;
                        }
                    }
                    return null;
                }
            }
            catch
            {
                Utility.log(String.Format("IsExistingLocationItemInGlobal {0}", item));
                throw new Exception(String.Format("IsExistingLocationItemInGlobal {0}", item));
            }
        }

        private static void TryMoveLocationItemToGlobal(Item item)
        {
            User user = TryGetServiceUser();
            try
            {
                using (new UserSwitcher(user))
                {
                    Item folderItemMoveTo = masterDB.GetItem(Settings.GetSetting("MedTouch.DataImport.WebService.GlobalLocationsPath"));
                    item.MoveTo(folderItemMoveTo);
                }
            }
            catch
            {
                Utility.log(String.Format("Error moving item {0}", item));
                throw new Exception(String.Format("Error moving item {0}", item));
            }
        }

        private static Item MatchOfficeToLocationItem(Item physician, Item office)
        {
            
            List<Item> locations = physician.Axes.GetDescendants()
                .Where(i => i.Template.Name.Equals(Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameLocation"), StringComparison.OrdinalIgnoreCase))
                .ToList();
            foreach (Item location in locations)
            {
                if ((location.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameLocationName")].Value == office.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameOfficeName")].Value)
                    &&
                (location.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameLocationId")].Value == office.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameOfficeId")].Value))
                {
                    return location;
                }
            }
            return null;
        }

        public static void UpdateSitecoreItem(Item itemToUpdate, Dictionary<string, string> keyValuePairsForUpdate)
        {
            User user = TryGetServiceUser();

            using (new UserSwitcher(user))
            {
                itemToUpdate.Editing.BeginEdit();

                try
                {
                    foreach (var keyValue in keyValuePairsForUpdate)
                    {
                        var iField = itemToUpdate.Fields[keyValue.Key];
                        if (iField != null)
                        {
                            var val = HttpContext.Current.Server.HtmlDecode(keyValue.Value); //TODO: what is going on here
                            iField.SetValue(val, true);
                        }
                    }

                    itemToUpdate.Editing.EndEdit();
                }
                catch (Exception ex)
                {
                    Utility.log(string.Format("Failed to upadte item: {0}", itemToUpdate.Name));
                    itemToUpdate.Editing.CancelEdit();
                }
            }

        }

        public static Item TryGetItemFromGuid(Guid itemID) //returns null if item isn't found
        {
            User user = TryGetServiceUser();
            try
            {
                using (new UserSwitcher(user))
                {
                    return masterDB.GetItem(new ID(itemID));
                }
            }
            catch
            {
                Utility.log(String.Format("TryGetItemFromGuid {0}", itemID));
                throw new Exception(String.Format("TryGetItemFromGuid {0}", itemID));
            }
        }

        public static Item TryGetItemByPath(String itemPath)
        {
            try
            {
                var items = masterDB.SelectItems(itemPath);
                if (items.Length == 1)
                {
                    return items[0];
                }
                else if (items.Length == 0) 
                {
                    Utility.log(String.Format("No items with path {0}", itemPath));
                    throw new Exception(String.Format("No items with path {0}", itemPath));
                }
                else 
                {
                    Utility.log(String.Format("More than 1 item with path {0}", itemPath));
                    throw new Exception(String.Format("More than 1 item with path {0}", itemPath));
                }
            }
            catch
            {
                Utility.log(String.Format("Error getting item with path {0}", itemPath));
                throw new Exception(String.Format("Error getting item with path {0}", itemPath));
            }
        }

        public static IEnumerable<Item> TryGetItemsFromMaster(string query)
        {
            return TryGetItems(masterDB, query);
        }

        public static IEnumerable<Item> TryGetItems(Database database, string query)
        {
            try
            {
                return database.SelectItems(query);
            }
            catch
            {
                string error = string.Format("Error getting items with query: {0} in database: {1}", query, database);
                Utility.log(error);
                throw new Exception(error);
            }
        }

        public static string GeneralizeName(string name)
        {
            string newName = Regex.Replace(name, @"\.", String.Empty); //remove periods (ex: M.D. becomes MD);
            newName = Regex.Replace(newName, @"[^\w]", " "); // replace special characters with space
            newName = Regex.Replace(newName, @"\s+", " "); // replace multiple space with a single space            
            newName = Regex.Replace(newName, @"^[ \t]+|[ \t]+$", ""); // remove leading and trailing space

            return newName; //the toLower() and dashes will be provided by URL helper
        }

        public static string UploadPhysicianImapge(string physicianImageFolderPath, string strItemName, string webPath)
        {
            string imgFieldValue = "";

            var itemName = GeneralizeName(strItemName);

            var mediaItm = SaveToMediaItemFromUrl(physicianImageFolderPath, itemName, webPath);

            if (mediaItm != null)
            {
                TryUpdateSitecoreItem(mediaItm, "Alt", Regex.Replace(strItemName, @"\s+", " "));
                imgFieldValue = string.Format("<image mediaid=\"{0}\" mediapath=\"{1}\" src=\"{2}\" />",
                    mediaItm.ID,
                    mediaItm.MediaPath,
                    "~/media" + mediaItm.MediaPath + "." + mediaItm.Extension);
                ItemHelper.PublishItem(mediaItm, false);
            }
            return imgFieldValue;           
        }

		//START: Customize from MedTouch.Common, rewrite to prevent memory leak issue

		public static MediaItem SaveToMediaItemFromUrl(string mediaPath, string mediaName, string url)
		{
			if (string.IsNullOrEmpty(url))
			{
                return null;
			}

            MediaItem mediaItem = null;
			string contentType = string.Empty;
			try
			{
				using (Stream stream = GetStreamFromURL(url, ref contentType))
				{
                    mediaItem = masterDB.GetItem(string.Concat(mediaPath, "/", mediaName));
                    if (ShouldUpdateMedia(mediaItem, stream))
				    {
				        string fileExt = GetExtensionFromContentType(contentType);

					    if (stream.Length > 0)
					    {
                            mediaItem = MediaHelper.SaveToMediaItem(mediaPath, fileExt, mediaName, stream);
                            if (mediaItem != null)
                            {
                                ItemHelper.UpdateSitecoreItem(mediaItem, "Alt", mediaName);
                            }
					    }    
				    }
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex.Message, ex, null);
			}

            return mediaItem;
		}

		private static string GetExtensionFromContentType(string contentType)
		{
			string retVal = string.Empty;
			switch (contentType.ToLower())
			{
				case "image/jpeg":
					retVal = "jpg";
					break;
				case "image/jpg":
					retVal = "jpg";
					break;
				case "image/gif":
					retVal = "gif";
					break;
				case "image/x-png":
					retVal = "png";
					break;
				case "image/tiff":
					retVal = "tiff";
					break;
				case "application/pdf":
					retVal = "pdf";
					break;
				case "application/x-pdf":
					retVal = "pdf";
					break;
			}
			return retVal;
		}

		public static Stream GetStreamFromURL(string url, ref string contentType)
		{
			byte[] result = new byte[0];
			byte[] buffer = new byte[4096];

			WebRequest wr = WebRequest.Create(url);

			try
			{
				using (WebResponse response = wr.GetResponse())
				{
					contentType = response.ContentType;
					using (Stream responseStream = response.GetResponseStream())
					{
						if (responseStream != null)
						{
							using (MemoryStream memoryStream = new MemoryStream())
							{
								int count = 0;
								do
								{
									count = responseStream.Read(buffer, 0, buffer.Length);
									memoryStream.Write(buffer, 0, count);

								} while (count != 0);

								result = memoryStream.ToArray();
								memoryStream.Close();
							}
							responseStream.Close();
						}
					}
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex.Message, ex, null);
			}

			return new MemoryStream(result);
		}

        private static bool ShouldUpdateMedia(MediaItem mediaItem, Stream newMedia)
        {
            if (mediaItem == null || newMedia == null)
            {
                return true;
            }

            string oldHash = string.Empty;
            using (Stream oldMedia = mediaItem.GetMediaStream())
            {
                oldHash = ComputeHash(oldMedia);
            }
                
            string newdHash = ComputeHash(newMedia);
            return !string.Equals(oldHash, newdHash, StringComparison.CurrentCultureIgnoreCase);
        }

        private static string ComputeHash(Stream stream)
        {
            byte[] data = new byte[stream.Length];
            int bytesRead = 0;
            int numBytesToRead = (int)stream.Length;

            while (numBytesToRead > 0)
            {
                int read = stream.Read(data, bytesRead, numBytesToRead);
                if (read == 0)
                {
                    break;
                }
                numBytesToRead -= read;
                bytesRead += read;
            }

            return ComputeHash(data);
        }

        private static string ComputeHash(byte[] data)
        {
            if (data == null || !data.Any())
            {
                return string.Empty;
            }

            string result = string.Empty;
            using (MD5 md5 = MD5.Create())
            {
                byte[] byteHashed = md5.ComputeHash(data);
                var s = new StringBuilder();
                foreach (byte b in byteHashed)
                {
                    s.Append(b.ToString("x2").ToLower());
                }
                    
                result = s.ToString();
            }

            return result;
        }

		//END: Customize from MedTouch.Common, rewrite to prevent memory leak issue

        public static void TryRemoveImportedItems(Item physicianRoot)
        {
            User user = TryGetServiceUser();
            try
            {
                using (new UserSwitcher(user))
                {
                    physicianRoot.Axes.GetDescendants()
                        .Where(i => i.Template.Name.Equals(Settings.GetSetting("MedTouch.DataImport.WebService.GenericNamePhysicianFolder")))
                        .ToList()
                        .ForEach(physician => TryGetItemFromGuid(Guid.Parse(physician.ID.ToString())).Delete());
                }
            }
            catch
            {
                Utility.log(String.Format("TryRemoveImportedItems {0}", physicianRoot.ID));
                throw new Exception(String.Format("TryRemoveImportedItems {0}", physicianRoot.ID));
            }

        }

        public static void TryDeleteSitecoreItem(Guid itemID)
        {

            Item itemToDelete = TryGetItem(itemID);
            User user = TryGetServiceUser();

            try
            {
                using (new UserSwitcher(user))
                {
                    itemToDelete.Delete(); // this will delete the item and its descendants
                }
            }
            catch
            {
                Utility.log(String.Format("Error deleting item {0}", itemID));
                throw new Exception(String.Format("Error deleting item {0}", itemID));
            }

        }

        public static Item TryCreateSitecoreItemUsingTemplate(Guid parentItemID, string templatename, string itemName)
        {
            Item newItem = null;

            // get location of item to create
            var locationItem = TryGetItem(parentItemID);
            var templateItem = TryGetTemplateItem(templatename);

            // if location and template are valid, create an item

            User user = TryGetServiceUser();

            using (new UserSwitcher(user))
            {
                newItem = TryAddItem(itemName, templateItem, locationItem);
                Utility.log(string.Format("Added new item using template: {0} | {1} | {2}", parentItemID.ToString(), templatename, itemName));
            }

            return newItem;

        }

        public static BranchItem TryGetBranchItem(string branchItemPath)
        {
            try
            {
                User user = TryGetServiceUser();
                using (new UserSwitcher(user))
                {
                    var branchItem = (BranchItem)masterDB.GetItem(branchItemPath);
                    if (branchItem == null)
                    {
                        throw new Exception();
                    }
                    return branchItem;
                }
            }
            catch
            {
                Utility.log(String.Format("TryGetBranchItem:Error fetcnhing Item {0}, perhaps it is not a branch item", branchItemPath));
                throw new Exception(String.Format("TryGetBranchItem:Error fetcnhing Item {0}, perhaps it is not a branch item", branchItemPath));
            }

        }

       

        private static Item TryGetItem(Guid itemID)
        {
            try
            {
                User user = TryGetServiceUser();
                using (new UserSwitcher(user))
                {
                    if (masterDB == null)
                    {
                        Utility.log("Master Db is null");
                        throw new Exception("Master Db is null");
                    }
                    var item = masterDB.GetItem(new ID(itemID));

                    if (item == null)
                    {
                        Utility.log("Item is null");
                        throw new Exception();
                    }
                    return item;
                }
            }
            catch
            {
                Utility.log(String.Format("TryGetItem: Error fetching item {0}...perhaps it does not exist", itemID));
                throw new Exception(String.Format("TryGetItem: Error fetching item {0}...perhaps it does not exist", itemID));
            }
        }

        private static TemplateItem TryGetTemplateItem(string templateFullName)
        {
            try
            {
                var template = masterDB.GetTemplate(templateFullName);
                if (template == null)
                {
                    throw new Exception();
                }
                return template;
            }
            catch
            {
                Utility.log(String.Format("TryGetTemplateItem: Error fetching template {0}...perhaps it is not a branch item", templateFullName));
                throw new Exception(String.Format("TryGetTemplateItem: Error fetching template {0}...perhaps it is not a branch item", templateFullName));
            }

        }

        private static Item TryAddItem(string itemName, BranchItem branchItem, Item parentItem) 
        {
            try 
            {
                return parentItem.Add(itemName, branchItem);
            }
            catch (InvalidItemNameException e)
            {
                Utility.log(String.Format("Item {0} with template {1} with parent Item {2} has an invalid name. Reason: {3}", itemName, branchItem.Name, parentItem.ID, e.Message));
                throw new Exception(String.Format("Item {0} with template {1} with parent Item {2} has an invalid name. Reason: {3}", itemName, branchItem.Name, parentItem.ID, e.Message));
            }
            catch 
            {
                Utility.log(String.Format("Error adding item {0} with branch {1} with parent Item {2}", itemName, branchItem.Name, parentItem.ID));
                throw new Exception(String.Format("Error adding item {0} with branch {1} with parent Item {2}",itemName,branchItem.Name,parentItem.ID));
            }
        }

        private static Item TryAddItem(string itemName, TemplateItem templateItem, Item parentItem)
        {
            try
            {
                return parentItem.Add(itemName, templateItem);
            }
            catch (InvalidItemNameException e)
            {
                Utility.log(String.Format("Item {0} with template {1} with parent Item {2} has an invalid name. Reason: {3}", itemName, templateItem.FullName, parentItem.ID, e.Message));
                throw new Exception(String.Format("Item {0} with template {1} with parent Item {2} has an invalid name. Reason: {3}", itemName, templateItem.FullName, parentItem.ID, e.Message));
            }
            catch
            {
                Utility.log(String.Format("Error adding item {0} with template {1} with parent Item {2}", itemName, templateItem.FullName, parentItem.ID));
                throw new Exception(String.Format("Error adding item {0} with template {1} with parent Item {2}", itemName, templateItem.FullName, parentItem.ID));
            }
        }

        private static User TryGetServiceUser()
        {
            User user = null;
            string domainUser = @"sitecore\DataImporter";
            if (User.Exists(domainUser))
            {
                user = User.FromName(domainUser, false);
            }
            else
            {
                Utility.log(String.Format("User {0} does not exist", domainUser));
                throw new Exception(String.Format("User {0} does not exist", domainUser));
            }
            return user;
        }

		public static void PublishItem(Item targetItem, bool withSubItem)
		{
			DateTime now = DateTime.Now;
			Database sourceDatabase = Factory.GetDatabase("master");
			Item item = targetItem;
			foreach (Item item3 in sourceDatabase.GetItem("/sitecore/system/publishing targets").Children)
			{
				string name = item3["target database"];
				Database database = Factory.GetDatabase(name);
				foreach (Language language in sourceDatabase.Languages)
				{
					using (new SecurityDisabler())
					{

						PublishOptions options = new PublishOptions(sourceDatabase, database, PublishMode.Full, language, now)
						{
							RootItem = item,
							Deep = withSubItem
						};
						new Publisher(options).PublishAsync();
					}
				}
			}
		}

        private static TemplateItem CreateNewTemplateItem(string guid)
        {
            ID id = ParseID(guid);
            if (!ID.IsNullOrEmpty(id))
            {
                return masterDB.GetItem(id);
            }

            return null;
        }

        private static ID ParseID(string guid)
        {
            ID id;
            if (ID.TryParse(guid, out id))
            {
                return id;
            }

            return ID.Null;
        }

        private static bool TryParseImportItems(Field field, string value)
        {
            // No JSON or XML field in the source, no need to parse, set value as is
            return false;
            //string itemIds = string.Empty;
            //if (!TryParseImportItems(value, out itemIds))
            //{
            //    return false;
            //}

            //field.Value = itemIds;
            //return true;
        }

        private static bool TryParseImportItems(string value, out string itemIds)
        {
            ImportItems importItems = ImportItems.JsonToObjectOfType(value);
            if (importItems == null)
            {
                itemIds = string.Empty;
                return false;
            }
            
            List<string> ids = new List<string>();
            foreach (ImportItem importItem in importItems.Items)
            {
                Item item = importItem.SitecoreItem;
                if (item == null)
                {
                    item = TryAddItem(importItem.SitecoreItemName, importItem.TemplateItem, importItem.ParentSitecoreItem);
                }
                
                if(AreAnyFieldValuesDifferent(item, importItem))
                {
                   UpdateFieldValues(item, importItem);
                }
                
                ids.Add(item.ID.ToString());
            }

            itemIds = string.Join(Pipe, ids.Distinct());
            return true;
        }

        private static bool AreAnyFieldValuesDifferent(Item item, ImportItem importItem)
        {
            if (importItem.Fields == null || !importItem.Fields.Any())
            {
                return false;
            }

            foreach (ImportField importField in importItem.Fields)
            {
                Field field = item.Fields[importField.Name];
                if (field != null && !string.Equals(importField.Value, field.Value))
                {
                    return true;
                }
            }

            return false;
        }

        private static void UpdateFieldValues(Item item, ImportItem importItem)
        {
            item.Editing.BeginEdit();
            foreach (ImportField importField in importItem.Fields)
            {
                Field field = item.Fields[importField.Name];
                if (field != null && !string.Equals(importField.Value, field.Value))
                {
                    field.Value = importField.Value;
                }
            }

            item.Editing.EndEdit();
        }

        private static bool TryParseImportFolder(Item parent, string value)
        {
            ImportFolder importFolder = ImportFolder.JsonToObjectOfType(value);
            if (importFolder == null)
            {
                return false;
            }

            Item folderItem = GetOrCreateChild(parent, importFolder.Folder);
            foreach (ImportItem importItem in importFolder.Items)
            {
                UpdateFieldValues(GetOrCreateChild(folderItem, importItem), importItem);
            }
            
            return true;
        }

        private static Item GetOrCreateChild(Item parent, ImportItem child)
        {
            string query = string.Format("{0}/*[@@name='{1}']", parent.Paths.FullPath, child.SitecoreItemName);
            Item childItem = parent.Database.SelectSingleItem(query);
            if (childItem != null)
            {
                return childItem;
            }

            return TryAddItem(child.SitecoreItemName, child.TemplateItem, parent);
        }

        private static void TryUpdatePhysicianWorkflow(Item physician)
        {
            string physicianApprovalWorkflowID = Settings.GetSetting("MedTouch.DataImport.WebService.WorkflowID.PhysicianApprovalWorkflow");
            string approvedStateID = Settings.GetSetting("MedTouch.DataImport.WebService.WorkflowID.ApprovedState");

            using (new SecurityDisabler())
            {
                physician.Editing.BeginEdit();
                // update workflow field and the workflow state field
                physician.Fields[FieldIDs.Workflow].Value = physicianApprovalWorkflowID;
                physician.Fields[FieldIDs.WorkflowState].Value = approvedStateID;
                physician.Editing.EndEdit();
            }
        }

        private static List<Item> _cloneDestinationsList;
        public static List<Item> CloneDestinationsList
        {
            get
            {
                if (_cloneDestinationsList == null || _cloneDestinationsList.Count == 0)
                {
                    Item cloningDestinationFolder = ItemHelper.GetItemByPath(Settings.GetSetting("MedTouch.DataImport.WebService.CloneDestinationsPath"), masterDB);
                    _cloneDestinationsList = ItemHelper.GetChildItems(cloningDestinationFolder);
                }

                return _cloneDestinationsList;
            }
        }

        private static void TryUpdateCloningDestination(Item physicianItem)
        {
                        
            try
            {
                if (physicianItem != null)
                {
                    List<Item> affiliations = ItemHelper.GetItemsFromMultilistField(Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameAffiliations"), physicianItem);
                    if (affiliations != null && affiliations.Any())
                    {
                        Item parentFolder = physicianItem.Parent;
                        if (parentFolder.Template.Name.Equals(Settings.GetSetting("MedTouch.DataImport.WebService.GenericNamePhysicianFolder"), StringComparison.OrdinalIgnoreCase))
                        {
                            UpdateCloningDestination(parentFolder, affiliations);
                        }

                        UpdateCloningDestination(physicianItem, affiliations);
                    }

                }
            }
            catch (Exception ex)
            {
                //TODO: Log this error
                Utility.log(string.Format("TryUpdateCloningDestination: Failed to Update Cloning Destination on physician item: {0}", physicianItem.Paths.FullPath));
                throw new Exception(string.Format("TryUpdateCloningDestination: Failed to Update Cloning Destination on physician item: {0}", physicianItem.Paths.FullPath, ex));
            }
        }

        private static void UpdateCloningDestination(Item itemToUpdate, List<Item> affiliations)
        {
            User user = TryGetServiceUser();

            using (new UserSwitcher(user))
            {
                try
                {
                    if (itemToUpdate.Template.BaseTemplates.Where(baseTemplate => baseTemplate.Name.Equals(Settings.GetSetting("MedTouch.DataImport.WebService.GenericNameTemplatesCloneDestinations"), StringComparison.OrdinalIgnoreCase)).Any())
                    {
                        itemToUpdate.Editing.BeginEdit();
                        
                        //List<Item> cloneDestinations = ItemHelper.GetItemsFromMultilistField(Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameCloneDestinations"), itemToUpdate);
                        MultilistField cloneDestinationsField = (MultilistField)itemToUpdate.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameCloneDestinations")];
                        if (cloneDestinationsField != null)
                        {
                            Item CaliforniaMainSiteItem = CloneDestinationsList.FirstOrDefault(item => item.Name.Equals(CaliforniaMainSite, StringComparison.OrdinalIgnoreCase));
                            //if (!cloneDestinationsField.GetItems().Any(item => item.Name.Equals(CaliforniaMainSite, StringComparison.OrdinalIgnoreCase)))
                            if (CaliforniaMainSiteItem != null && !cloneDestinationsField.TargetIDs.Any(id => id.Equals(CaliforniaMainSiteItem.ID)))
                            {
                                cloneDestinationsField.Add(CaliforniaMainSiteItem.ID.ToString());
                            }


                            foreach (Item affiliationItem in affiliations)
                            {
                                string affiliationItemName = ItemHelper.GetFieldRawValue(affiliationItem, Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameAffiliationName"));
                                Item cloningDestinationItem = CloneDestinationsList.FirstOrDefault(item => item.Name.Equals(affiliationItemName, StringComparison.OrdinalIgnoreCase));
                                if (cloningDestinationItem != null && !cloneDestinationsField.TargetIDs.Any(id => id.Equals(cloningDestinationItem.ID)))
                                {
                                    cloneDestinationsField.Add(cloningDestinationItem.ID.ToString());
                                }
                            }
                        }

                        //ensures this is a change
                        itemToUpdate.Fields[FieldIDs.Updated].Reset();

                        itemToUpdate.Editing.EndEdit();
                    }
                }
                catch (Exception ex)
                {
                    //TODO: Log this error
                    itemToUpdate.Editing.CancelEdit();
                    Utility.log(string.Format("UpdateCloningDestination: Failed to Update Cloning Destination on item: {0}", itemToUpdate.Paths.FullPath));
                    throw new Exception(string.Format("UpdateCloningDestination: Failed to Update Cloning Destination on item: {0}", itemToUpdate.Paths.FullPath), ex);
                }
            }

        }
    }
}