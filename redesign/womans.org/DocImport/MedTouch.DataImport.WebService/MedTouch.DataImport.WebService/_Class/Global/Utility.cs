﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace MedTouch.DataImport.Webservice._Class.Global
{
    public class Utility
    {
        /// <summary>
        /// Logs the specified log message.
        /// </summary>
        /// <param name="logMessage">The log message.</param>
        public static void log(string logMessage)
        {
            if (false)
            {
                using (StreamWriter w = File.AppendText(@"C:\_000\wslog.txt"))
                {
                    w.WriteLine("\r\n{0}", logMessage);
                    w.Flush();
                }
            }
        }
    }
}
