﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Sitecore.Data.Items;
using Sitecore.Data;
using System.Text;
using Sitecore.Data.Templates;




namespace MedTouch.DataImport.AnnotatedClassGeneration {
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AnnotatedClassGeneratorTemplate : System.Web.Services.WebService
    {

        [WebMethod]
        public string GenerateAnnotatedClassFromTemplate(string templateFullName, string namespaceToUse, List<string> namesOfFieldsToIgnore)
        {
            StringBuilder sb = new StringBuilder();

            Database master = Sitecore.Configuration.Factory.GetDatabase("master");
            TemplateItem template = master.GetTemplate(templateFullName);

            BuildUsings(sb);
            OpenNamespace(namespaceToUse,sb);
            OpenClass(template.Name,sb);
            AddProperties(template, namesOfFieldsToIgnore, sb);
            AddInterfaceMethods(sb);
            CloseClass(sb);
            CloseNamespace(sb);

            return sb.ToString();

        }

        private void CloseNamespace(StringBuilder sb)
        {
            sb.Append("}");
        }


        private void CloseClass(StringBuilder sb)
        {
            sb.Append("\t}\n");
        }

        private void AddInterfaceMethods(StringBuilder sb)
        {
            List<string> interfaceMethods = new List<string>();

            interfaceMethods.Add("GetName");
            interfaceMethods.Add("GetTemplate");
            interfaceMethods.Add("GetContextSensitiveKey");
            interfaceMethods.Add("GetInterveningParentFolder");
            interfaceMethods.Add("GetBranchTemplateToInsert");

            foreach (var interfaceMethod in interfaceMethods)
            {
                sb.AppendFormat("\t\tpublic string {0}()\n\t\t{\n\t\t\tthrow new NotImplementedException();\n\t\t}\n", interfaceMethod);
            }

        }

        private string GeneralizeName(string s) 
        {
            return s.Replace("-",String.Empty).Replace(" ","_").Trim();
        }

        public void AddProperties(TemplateItem template, List<string> namesOfFieldsToIgnore, StringBuilder sb)
        {
            HashSet<string> valueTypes = new HashSet<string>() { "Single-Line Text", "Multi-Line Text", "Date", "Rich Text" };
            HashSet<string> referenceTypes = new HashSet<string>() { "Multilist", "Treelist", "TreelistEx" };

            foreach (var field in template.Fields.Where(field => !namesOfFieldsToIgnore.Contains(field.Name)))
            {
                if (valueTypes.Contains(field.Type))
                {
                    sb.AppendFormat("[ValueField({0})]\n", field.Name);
                }
                else if (referenceTypes.Contains(field.Type))
                {
                    sb.AppendFormat("[Reference Field({0},\"{1}\" )]\n", field.Name, field.Source);
                }
                else
                {
                    throw new Exception(String.Format("Need to add {0} either to ReferenceTypes or ValueTypes", field.Type));
                }
                sb.AppendFormat("public string {0} { get; set; }\n");
                sb.AppendFormat("\n");
            }
        }

        public void OpenClass(string templateName, StringBuilder sb)
        {
            sb.AppendFormat("\tclass {0} : IInsertable\n", GeneralizeName(templateName));
        }

        public void OpenNamespace(string namespaceToUse, StringBuilder sb)
        {
            sb.AppendFormat("namespace {0} {\n", namespaceToUse);
        }

        public void BuildUsings(StringBuilder sb)
        {
            List<string> usings = new List<string>();


            usings.Add("System.Text");
            usings.Add("MedTouch.DataImport");
            usings.Add("MedTouch.DataImport.SitecoreFields.ReferenceFields");
            usings.Add("MedTouch.DataImport.Sync.Contracts.MasterToSlave");
            usings.Add("System.Collections.Generic");
            usings.Add("MedTouch.DataImport.SitecoreFields.ValueFields");
            usings.Add("System");

            foreach (var usingstatement in usings)
            {
                sb.AppendFormat("using {0};\n", usingstatement);
            }
        }


     


    }
}
