﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MedTouch.DataImport.Webservice._Class.Global;
using MedTouch.Common.Helpers;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Security.Accounts;
using Sitecore.SecurityModel;
using Sitecore.Data.Items;
using MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice.Webservice.DataContract;
using MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice.Webservice.Helpers;
using MedTouch.Common;
using Newtonsoft.Json;

namespace MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice.Webservice
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]      
    public class InteractWithSitecore : System.Web.Services.WebService
    {
        public enum InsertIfDoesntExist
        {
            YES,
            NO
        }

        //will return a list of strings which contain pipe dilimeted Id, hash, template, and key
        //can't handle nested folders
        //must use Common/Folder
        //should be in ItemHelper or some other place

        [WebMethod]
        public Guid GetGuidOfSingleChildOfTemplateWhereNameEquals(Guid contextParentID, string templateName, string name,InsertIfDoesntExist insertIfDoesntExist)
        {
            
            var children = WebserviceItemHelper.TryGetItemFromGuid(contextParentID).Children.Where(i => i.Name.Equals(name));
            
            switch (children.Count())
            {
                case 0:
                    if (insertIfDoesntExist == InsertIfDoesntExist.YES)
                    {
                        return WebserviceItemHelper.TryCreateSitecoreItemUsingTemplate(contextParentID, templateName, name).ID.ToGuid();
                    }
                    else 
                    {
                        throw new Exception(String.Format("Item with name {0} doesn't exist", name));
                    }
                    
                case 1:
                    return children.First().ID.ToGuid();
                    
                default:
                    throw new Exception(String.Format("More than 1 child with name {0}", name));               

            }
        }


        //TODO: instead of using pipe dilimited string, use a class
        [WebMethod]
        public List<IDKeyHashTemplate> GetChildren(Guid contextParentID, Boolean drillDownOneLevelIntoFolders)
        //public List<IDKeyHashTemplate> GetChildren()
        {
            //Guid contextParentID = new Guid("{c92491ae-e39f-4d69-9a53-a5ed02663786}");
            //Boolean drillDownOneLevelIntoFolders = true;
            var children = new List<IDKeyHashTemplate>();

            Item parentItem = WebserviceItemHelper.TryGetItemFromGuid(contextParentID);

            Utility.log(string.Format("Parent ID:{0} Total Children: {1}", contextParentID, parentItem.Children.Count));

            foreach (Item child in parentItem.Children)
            {
                if(!child.Key.Equals("search results") && child.TemplateID != new ID(Settings.GetSetting("MedTouch.DataImport.WebService.AugmentedOfficeFolderGuid")))
                {
                    
                    //TODO: need to check with Chait or dan on how to properly identify folders
                    if (drillDownOneLevelIntoFolders &&
                        (child.Template.FullName.Equals(Settings.GetSetting("MedTouch.DataImport.WebService.GenericNameCommonFolder"))
                            || child.Template.BaseTemplates.Where(baseTemplate => baseTemplate.FullName.Equals(Settings.GetSetting("MedTouch.DataImport.WebService.GenericNameSystemTemplatesTemplateFolder"))).Any()
                            || child.Template.FullName.Equals(Settings.GetSetting("MedTouch.DataImport.WebService.PhysicianFolderPath"))
                        ))
                    {
                        Console.WriteLine("Loop through children");
                        foreach (Item grandchild in child.Children)
                        {//TODO: should we wrap field access in try-catch? if the fieldname doesnt exist we'll definately get an exception...
                            //Use an extension method for this???
                            children.Add(new IDKeyHashTemplate(
                                    grandchild.ID.ToGuid(),
                                    grandchild.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameContextSensitiveKey")].Value,
                                    grandchild.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameAllUpdateableFieldsHash")].Value,
                                    grandchild.Template.FullName
                                )
                            );
                        }
                    }
                    else if (child.Template.FullName.Equals(Settings.GetSetting("MedTouch.DataImport.WebService.AugmentedLocationsFolderPath")))
                    {
                        continue;
                    }
                    else
                    {
                        Utility.log(string.Format("Add chilld template name {0}", child.Template.FullName));
                        Console.WriteLine("Add chilld template name {0}", child.Template.FullName);
                        //TODO: need to verify that AllUpdateableFieldsHash and ContextSensitiveKey are actual fields, 
                        //otherwise will error out with cryptic message : child.Fields["AllUpdateableFieldsHash"].Value
                        children.Add(new IDKeyHashTemplate(child.ID.ToGuid(),
                            child.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameContextSensitiveKey")].Value,
                            child.Fields[Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameAllUpdateableFieldsHash")].Value,
                            child.Template.FullName));
                    }
                }
            }
            Utility.log(string.Format("Total number of children: {0}", children.Count));
            return children;

        }

        [WebMethod]
        public Guid InsertItemUsingBranch(string name, string branchItemPath, string contextSensitiveKey, Guid contextParentID)
        {
            Item item = WebserviceItemHelper.TryCreateSitecoreItemUsingBranch(contextParentID, branchItemPath, name);

            WebserviceItemHelper.TryUpdateSitecoreItem(item, Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameContextSensitiveKey"), contextSensitiveKey);

            return item.ID.ToGuid();
        }

        //the idea is to go to the context parent, if the item is of template education, we insert under the education folder etc.
        //hopefully there aren't any required fields that would make the insert bomb

        //we insert the item and add the key right after just in case the sync errors out after, we will not have an item in the DB without a key
        //its hash will be null so it will get updated on the next update
        //TODO: think about what might happen in the above case --- how can we know on the frontend when a physician hasn't been fully created

        [WebMethod]
        public Guid InsertItem(string name, string templatename, string contextSensitiveKey, Guid contextParentID)
        {
            Item item = WebserviceItemHelper.TryCreateSitecoreItemUsingTemplate(contextParentID, templatename, name);

            WebserviceItemHelper.TryUpdateSitecoreItem(item, Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameContextSensitiveKey"), contextSensitiveKey);

            return item.ID.ToGuid();

        }

        [WebMethod]
        public void DeleteItem(Guid g)
        {
            WebserviceItemHelper.TryDeleteSitecoreItem(g);
        }


        //TODO: Need to look into batching here...
        [WebMethod]
        public void UpdateFieldOnItem(Guid id, string field, string value)
        {
            WebserviceItemHelper.TryUpdateSitecoreItem(id, field, value);    
        }


        //TODO: Need to look into batching here...
        [WebMethod]
        public void UpdateAllFieldsOnItem(Guid id, string jsonFieldValues)
        {
            //deserialize the json 
            Dictionary<string, string> fieldValues = JsonConvert.DeserializeObject<Dictionary<string,string>>(jsonFieldValues);
            
            WebserviceItemHelper.TryUpdateAllFieldsSitecoreItem(id, fieldValues);
        }


        [WebMethod]
        public void UpdateHashOnItem(Guid id, string value)
        {
            WebserviceItemHelper.TryUpdateSitecoreItem(id, Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameAllUpdateableFieldsHash"), value);
        }


        [WebMethod]
        public List<string> GetPhysicianList()
        {
            List<string> physicians = new List<string>();
            Guid findADoctor = new Guid(Settings.GetSetting("MedTouch.DataImport.WebService.HomeFindADoctorItemGUID"));
            Item physicianRoot = WebserviceItemHelper.TryGetItemFromGuid(findADoctor);
            if (physicianRoot != null)
            {
                physicianRoot.Axes.GetDescendants()
                    .Where(i => i.Template.Name.Equals(Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNamePhysician"), StringComparison.OrdinalIgnoreCase))
                    .ToList()
                    .ForEach(i => physicians.Add(i.ID.ToString()));
            }
            Utility.log(string.Format("physicians total: {0}",physicians.Count));
            return physicians;
        }
        [WebMethod]
        public List<string> GetPhysiciansWithChildLocationFolder()
        {
            Guid findADoctor = new Guid(Settings.GetSetting("MedTouch.DataImport.WebService.HomeFindADoctorItemGUID"));
            Item physicianRoot = WebserviceItemHelper.TryGetItemFromGuid(findADoctor);
            List<string> physicianGuids = new List<string>();

            if (physicianRoot != null)
            {

            IEnumerable<Item> locationFolders = physicianRoot.Database.SelectItems(String.Concat("fast:", physicianRoot.Paths.FullPath, "//*[@@name='", Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameLocations"), "' and @@templatename='Folder']"));

                string test = String.Concat("fast:", physicianRoot.Paths.FullPath, "//*[@@name='", Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNameLocation"), "' and @@templatename='Folder']");

                foreach (Item item in locationFolders)
                {
                    physicianGuids.Add(item.ParentID.ToString());
                }
            }
            return physicianGuids;
        }

        [WebMethod]
        public List<string> GetPhysicianListUpdatedSince(DateTime updatedSince)
        {
            if(updatedSince == DateTime.MinValue)
            {
                return GetPhysicianList();
            }

            List<string> physicianIds = new List<string>();
            Guid findADoctor = new Guid(Settings.GetSetting("MedTouch.DataImport.WebService.HomeFindADoctorItemGUID"));
            Item physicianRoot = WebserviceItemHelper.TryGetItemFromGuid(findADoctor);

            if (physicianRoot != null)
            {
                string query = string.Format
                (
                    "fast:{0}//*[@@templatename='{1}' and @__Updated >='{2}']", 
                    physicianRoot.Paths.FullPath,
                    Settings.GetSetting("MedTouch.DataImport.WebService.GenericItemFieldNamePhysician"), 
                    DateUtil.ToIsoDate(updatedSince)
                );

                foreach (Item physician in WebserviceItemHelper.TryGetItemsFromMaster(query))
                {
                    physicianIds.Add(physician.ID.ToString());
                }
            }

            LogPhysicianTotal(physicianIds);
            return physicianIds;
        }

        private void LogPhysicianTotal<T>(IEnumerable<T> physicians)
        {
            Utility.log(string.Format("physicians total: {0}", physicians.Count()));
        }

        [WebMethod]
        public void PhysicianImportWrapper(string physicianId)
        {
            WebserviceItemHelper.PhysicianImportWrapper(physicianId);
        }

        [WebMethod]
        public void AddToPublishingQueue()
        {
            List<string> guidsNeedToBePublished = new List<string>() {
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalPhysicianDirectoryDegreeTitleFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalPhysicianDirectoryDepartmentsFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalPhysicianDirectoryEducationTypesFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalPhysicianDirectoryLanguagesFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalPhysicianDirectoryPositionFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalSpecialtiesFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalLocationsFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalTaxonomyFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.InsurancesGUID"),
				Settings.GetSetting("MedTouch.DataImport.WebService.HomeFindADoctorItemGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.AffiliationsItemGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.ProviderTypesItemGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.SpecialtiesItemGUID")
            };

            foreach (var gid in guidsNeedToBePublished)
            {
                Item itemToPublish = WebserviceItemHelper.TryGetItemFromGuid(new Guid(gid));
                if (itemToPublish != null)
                {
                    Console.WriteLine(String.Format("Adding Item To Publish queue: {0}", itemToPublish.Name));
					WebserviceItemHelper.PublishItem(itemToPublish, true);
                }
            }
            
        }
        //just for debugging and testing!
        [WebMethod]
        public void DeleteAllImportItems()
        {

            //this is just a down and dirty implementation -- we will look into batching item field updates later and using the
            //itemhelper updateitem method

            List<string> guidsunderwhichtodelete = new List<string>() {
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalPhysicianDirectoryDegreeTitleFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalPhysicianDirectoryDepartmentsFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalPhysicianDirectoryEducationTypesFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalPhysicianDirectoryLanguagesFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalPhysicianDirectoryPositionFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalSpecialtiesFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalLocationsFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.GlobalTaxonomyFolderGUID"),
                Settings.GetSetting("MedTouch.DataImport.WebService.InsurancesGUID")
            };

            using (new SecurityDisabler())
            {
                guidsunderwhichtodelete.ForEach(guid => WebserviceItemHelper.TryGetItemFromGuid(Guid.Parse(guid)).DeleteChildren());
            }

            Item physicianRoot =
                WebserviceItemHelper.TryGetItemFromGuid(new Guid(Settings.GetSetting("MedTouch.DataImport.WebService.HomeFindADoctorItemGUID")));

            if (physicianRoot != null)
            {
                WebserviceItemHelper.TryRemoveImportedItems(physicianRoot);
            }
        }


        [WebMethod]
        public Guid GetGuidFromPath(string path)
        {
            return WebserviceItemHelper.TryGetItemByPath(path).ID.ToGuid();
        }

        [WebMethod]
        public string GetPhysicianImagePathAfterUpload(string physicianImageFolderPath, string strItemName, string webPath)
        {
            return WebserviceItemHelper.UploadPhysicianImapge(physicianImageFolderPath, strItemName, webPath);
        }
    }
}
