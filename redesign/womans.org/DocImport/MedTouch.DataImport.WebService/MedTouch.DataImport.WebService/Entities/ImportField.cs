﻿
using MedTouch.Common.Helpers.Serialization.Entities;
using System.Runtime.Serialization;

namespace MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice.Webservice.Entities
{
    public class ImportField
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Value { get; set; }
        
        public override string ToString()
        {
            return SerializableEntityWrapper<ImportField>.Create(this).ToJsonString();
        }

        public static ImportField JsonToObjectOfType(string json)
        {
            return SerializableEntityWrapper<ImportField>.JsonToObjectOfType(json);
        }
    }
}
