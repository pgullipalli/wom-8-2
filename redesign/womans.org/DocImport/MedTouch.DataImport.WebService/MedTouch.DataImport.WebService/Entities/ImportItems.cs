﻿
using System.Collections.Generic;
using System.Runtime.Serialization;
using MedTouch.Common.Helpers.Serialization.Entities;

namespace MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice.Webservice.Entities
{
    public class ImportItems
    {
        [DataMember]
        public string FieldName { get; set; }

        [DataMember]
        public List<ImportItem> Items { get; set; }

        public override string ToString()
        {
            return SerializableEntityWrapper<ImportItems>.Create(this).ToJsonString();
        }

        public static ImportItems JsonToObjectOfType(string json)
        {
            return SerializableEntityWrapper<ImportItems>.JsonToObjectOfType(json);
        }
    }
}
