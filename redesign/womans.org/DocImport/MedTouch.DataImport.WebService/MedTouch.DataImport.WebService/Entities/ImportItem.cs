﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MedTouch.Common.Helpers.Serialization.Entities;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice.Webservice.Entities
{
    public class ImportItem
    {
        private static readonly Database Master = Factory.GetDatabase("master");

        [IgnoreDataMember]
        public TemplateItem TemplateItem 
        { 
            get
            {
                return TryGetItem(TemplatePath);
            }
        }

        [DataMember]
        public string TemplatePath { get; set; }

        [DataMember]
        public string ParentPath { get; set; }

        [DataMember]
        public string Name { get; set; }

        [IgnoreDataMember]
        public Item ParentSitecoreItem
        {
            get
            {
                return TryGetItem(ParentPath);
            }
        }

        [IgnoreDataMember]
        public Item SitecoreItem
        {
            get
            {
                return TryGetItem(GetSitecoreItemPath());
            }
        }

        [IgnoreDataMember]
        public string SitecoreItemName
        {
            get
            {
                return GetSitecoreItemName();
            }
        }

        [DataMember]
        public List<ImportField> Fields { get; set; }

        private string GetSitecoreItemName()
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                return Name;
            }

            return ItemUtil.ProposeValidItemName(Name);
        }

        private string GetSitecoreItemPath()
        {
            string itemName = GetSitecoreItemName();

            if (string.IsNullOrWhiteSpace(ParentPath) || string.IsNullOrWhiteSpace(itemName))
            {
                return string.Empty;
            }

            return string.Concat(ParentPath, "/", itemName);
        }

        private static TemplateItem TryGetTemplateItem(string uri)
        {
            Item item = TryGetItem(uri);
            if (item == null)
            {
                return null;
            }

            return item;
        }

        private static Item TryGetItem(string uri)
        {
            try
            {
                return Master.GetItem(uri);
            }
            catch (Exception)
            {
            }

            return null;
        }

        public override string ToString()
        {
            return SerializableEntityWrapper<ImportItem>.Create(this).ToJsonString();
        }

        public static ImportItem JsonToObjectOfType(string json)
        {
            return SerializableEntityWrapper<ImportItem>.JsonToObjectOfType(json);
        }
    }
}
