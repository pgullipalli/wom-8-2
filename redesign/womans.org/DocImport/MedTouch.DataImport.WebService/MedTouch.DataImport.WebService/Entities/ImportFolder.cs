﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MedTouch.Common.Helpers.Serialization.Entities;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice.Webservice.Entities
{
    public class ImportFolder
    {
        [DataMember]
        public ImportItem Folder { get; set; }

        [DataMember]
        public List<ImportItem> Items { get; set; }

        public override string ToString()
        {
            return SerializableEntityWrapper<ImportFolder>.Create(this).ToJsonString();
        }

        public static ImportFolder JsonToObjectOfType(string json)
        {
            return SerializableEntityWrapper<ImportFolder>.JsonToObjectOfType(json);
        }
    }
}
