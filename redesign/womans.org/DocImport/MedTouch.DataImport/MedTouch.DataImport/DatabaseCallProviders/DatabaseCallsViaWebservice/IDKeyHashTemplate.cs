﻿using System;

namespace MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice
{
    public class IDKeyHashTemplate
    {
        public Guid ID { get; set; }
        public string Key { get; set; }
        public string Hash { get; set; }
        public string Template { get; set; }

        public IDKeyHashTemplate(Guid id, string key, string hash, string template)
        {
            ID = id;
            Key = key;
            Hash = hash;
            Template = template;
        }
    }
}
