﻿using System;
using System.Collections.Generic;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;
using MedTouch.DataImport.Sync.Contracts.SlaveToMaster;
using MedTouch.DataImport._Class.Global;
using Newtonsoft.Json;
using System.Diagnostics;

//read this error: http://stackoverflow.com/questions/352654/could-not-find-default-endpoint-element
//"This error can arise if you are calling the service in a class library and calling the class library from another project."

//TODO should i do error handling here to catch webservice errors?

namespace MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice
{
    public class DatabaseWebserviceClient : IDatabaseCallProvider
    {

        private static volatile DatabaseWebserviceClient instance;
        private static object syncRoot = new Object();

        private MedTouch.DataImport.InteractWithSitecore.InteractWithSitecoreSoapClient Webservice;

        private DatabaseWebserviceClient() 
        {
            Webservice = new MedTouch.DataImport.InteractWithSitecore.InteractWithSitecoreSoapClient();  
        }

        public static DatabaseWebserviceClient Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new DatabaseWebserviceClient();
                    }
                }

                return instance;
            }
        }

        public Dictionary<ContextSensitiveKeyTemplateTuple, HashAndID> GetChildren(Guid parentID)
        {
            Console.WriteLine("Get Children for " + parentID.ToString());
            Dictionary<ContextSensitiveKeyTemplateTuple, HashAndID> items = new Dictionary<ContextSensitiveKeyTemplateTuple, HashAndID>();

            //TODO: What to do about (if we have a lot of child items to report): CommunicationException - 
            //The maximum message size quota for incoming messages (65536) has been exceeded. To increase the quota, 
            //use the MaxReceivedMessageSize property on the appropriate binding element.

            var itemKeyHashTemplateTuples = Webservice.GetChildren(parentID, true);


            //going to return a list of pipe dilimeted strings where the first field is the id, the second is the hash, the third is the key

            foreach (var itemKeyHashTemplate in itemKeyHashTemplateTuples) 
            {

                var item = new HashAndID(itemKeyHashTemplate.Hash, itemKeyHashTemplate.ID);  //we should create a special data type for this transaction to represent the data contract

                item.ID = itemKeyHashTemplate.ID; //this id will be used to make future updates to fields on the item
                item.Hash = itemKeyHashTemplate.Hash; //the hash will be used to determine whether or not we should update the field
               
                //TODO: what if key is empty? -- how to enforce this -- should we delete these items?
                //this logic really shoudlnt happen here i dont think.... -- it should just be this class' job to ferry the data back and forth, not modify it
                //TODO: What to do if 2 items have the same key (would occur in the case of duplicate keys in the Sitecore DB) -- we will get an (ArgumentException) item already added 
                if (itemKeyHashTemplate.Key != String.Empty)
                    items.Add(new ContextSensitiveKeyTemplateTuple(itemKeyHashTemplate.Key, itemKeyHashTemplate.Template), item);

            }

            return items;

        }

        public Guid GetGuidOfSingleChildOfTemplateWhereNameEquals(Guid contextParentID, string templateName, string name, InteractWithSitecore.InsertIfDoesntExist insertIfDoesntExist)
        {
            //Console.WriteLine("GetGuidOfSingleChildOfTemplateWhereNameEquals");
            return Webservice.GetGuidOfSingleChildOfTemplateWhereNameEquals(contextParentID, templateName, name, insertIfDoesntExist);

        }

        public string GetPhysicianImagePathAfterUpload(string physicianImageFolderPath, string strItemName, string webPath)
        {
            return Webservice.GetPhysicianImagePathAfterUpload(physicianImageFolderPath, strItemName, webPath);
        }

        public List<string> GetPhysiciansWithChildLocationFolder()
        {
            return Webservice.GetPhysiciansWithChildLocationFolder();
        }

        public List<string> GetPhysicianList()
        {
            return Webservice.GetPhysicianList();
        }

        public List<string> GetPhysicianListUpdatedSince(DateTime updatedSince)
        {
            return Webservice.GetPhysicianListUpdatedSince(updatedSince);
        }

        public void PhysicianImportWrapper(string physicianId)
        {
            Webservice.PhysicianImportWrapper(physicianId);
        }
        public void AddToPublishingQueue()
        {
            Webservice.AddToPublishingQueue();
        }
        public Guid GetGuidFromPath(string path) 
        {
            Console.WriteLine("Get Guid From Path " + path);
            Utility.log(string.Format("Get Guid From Path:{0}", path));
            return Webservice.GetGuidFromPath(path);
        }


        public void DeleteItem(Guid id)
        {
            Console.WriteLine(string.Format("Delete Item:{0}", id));
            Utility.log(string.Format("Delete Item:{0}", id));
            Webservice.DeleteItem(id);         
        }

        public Guid InsertItem(InsertableItem insertableItem, Guid contextParentID)
        {
            Console.WriteLine(String.Format("Insert Item: {0}-{1}", insertableItem.Name, insertableItem.Key));    
            Utility.log(string .Format("InsertItem:{0} | {1} | {2} | {3}", insertableItem.Name, insertableItem.Template, insertableItem.Key, contextParentID));
            return Webservice.InsertItem(insertableItem.Name, insertableItem.Template, insertableItem.Key, contextParentID);
        }


        public Guid InsertItemUsingBranch(InsertableItem insertableItem, Guid contextParentID)
        {
            Console.WriteLine(String.Format("Insert Item Using Branch: {0}-{1}", insertableItem.Name, insertableItem.Key));
            Utility.log(String.Format("Insert Item Using Branch: {0}-{1}", insertableItem.Name, insertableItem.Key));
            return Webservice.InsertItemUsingBranch(insertableItem.Name, insertableItem.BranchTemplateToInsert, insertableItem.Key, contextParentID);
        }

        //TODO: what if a value is null --- should we have some guarantee that before we call this method its not null?
        public void UpdateItem(Guid id, InsertableItem insertableItem)
        {
            
            Console.WriteLine(String.Format("Update Item: {0}|{1}", insertableItem.Name, insertableItem.Key));
            Utility.log(String.Format("Update Item: {0}|{1}", insertableItem.Name, insertableItem.Key));

            //Console.WriteLine("Updating Item: " + id + " over webservice");
            Stopwatch sw = Stopwatch.StartNew();

            Dictionary<string, string> fieldValuePairs = new Dictionary<string, string>();
            insertableItem.FieldnameValuePairs
                .ForEach(fieldnameValuePair =>  
                    fieldValuePairs.Add(fieldnameValuePair.Fieldname, fieldnameValuePair.Value));

            //HASH!
            fieldValuePairs.Add("AllUpdateableFieldsHash", insertableItem.Hash);

            string json = JsonConvert.SerializeObject(fieldValuePairs, Formatting.Indented);

            Webservice.UpdateAllFieldsOnItem(id, json);
            sw.Stop();
            //Console.WriteLine("Item updated - " + sw.Elapsed.TotalSeconds + " - " + insertableItem.Name);
        }

        //public void UpdateCloningDestinationForPhysicians(List<string> physiciansIds)
        //{
        //    foreach(string physicianId in physiciansIds)
        //    {
        //        Guid physicianGuid = new Guid(physicianId);
        //        Webservice.UpdateCloningDestination(physicianGuid);
        //    }
        //}
    }
}
