﻿using System;
using System.Collections.Generic;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;
using MedTouch.DataImport.Sync.Contracts.SlaveToMaster;


namespace MedTouch.DataImport.DatabaseCallProviders
{
    public interface IDatabaseCallProvider
    {
        Dictionary<ContextSensitiveKeyTemplateTuple, HashAndID> GetChildren(Guid parentID);
        void DeleteItem(Guid id);
        Guid InsertItem(InsertableItem i, Guid contextParentID);
        Guid InsertItemUsingBranch(InsertableItem i, Guid contextParentID);
        void UpdateItem(Guid itemID, InsertableItem i); 
        Guid GetGuidFromPath(string path);
        Guid GetGuidOfSingleChildOfTemplateWhereNameEquals(Guid contextParentID, string templateName, string name, InteractWithSitecore.InsertIfDoesntExist insertIfDoesntExist);

    }
}
