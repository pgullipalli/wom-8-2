﻿using System.Collections.Generic;
using System.Linq;

//http://programanddesign.com/cs/subtracting-sets/

namespace MedTouch.DataImport.HashsetExtensions
{
    public static class HashsetExtensions
    {
        public static bool IsAny<T>(this List<T> data)
        {
            return data != null && data.Any();
        }

        public static HashSet<T> ToSet<T>(this IEnumerable<T> collection)
        {
            return new HashSet<T>(collection);
        }

        public static HashSet<T> Subtract<T>(this HashSet<T> set, IEnumerable<T> other)
        {
            var clone = set.ToSet();
            clone.ExceptWith(other);
            return clone;
        }

    }
}


   