﻿using MedTouch.DataImport.Sync;

namespace MedTouch.DataImport.Parser
{
    public interface IParser
    {
        RootItem parse(string filename, string rootItemPath);
    }
}
