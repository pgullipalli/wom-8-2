﻿using System;

namespace MedTouch.DataImport.DataValidationAndExceptionHandling
{
    public class InvalidKeyException : Exception
    {
        public InvalidKeyException(string error) : base(error) { }
    }
}
