﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using MedTouch.DataImport.DataValidationAndExceptionHandling;
using MedTouch.DataImport.Hash;
using MedTouch.DataImport.SitecoreFieldModels;
using MedTouch.DataImport.Sync.Contracts.SlaveToMaster;

namespace MedTouch.DataImport.Sync.Contracts.MasterToSlave
{

    //we need these fields in order to be able to insert an Item into the DB
    //it is possible to insert an item into the DB with a path to the parent, but that would add unecessary logic to this class
    //if you know the path to the parent, you can do a webservice lookup to get the corresponding guid
    //we use the get method to help enforce the data contract

    //the reason why we have insertableItem and IInsertable is that Iinsertable cannot validate the output of the functions such as GetName() etc.
    //abstract class isn't an option for the same reason
    //we create an InsertableItem from an objcet that implements Insert

    public class InsertableItem
    {
        //we can do our validation and massaging here...
        //we ensure that objects have a hash and that its not empty or null here

        public InsertableItem(string name, string template, string branchTemplateToInsert, string key, string interveningParentFolder, Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> children, List<FieldnameValuePair> fieldnameValuePairs)
        {
            Name = name;
            Template = template;
            BranchTemplateToInsert = branchTemplateToInsert;
            Key = key;
            Children = children;
            FieldnameValuePairs = fieldnameValuePairs;
            InterveningParentFolder = interveningParentFolder;
        }

        private string _Name;

        public string Name {
            get
            {
                if (String.IsNullOrWhiteSpace(_Name))
                {
                    throw new Exception("Property either wasn't set or was set to an empty string/null");
                }

                return Utilities.GeneralizeName(_Name);
            }
            set
            {
                _Name = value;
            }
        }

        private string _Template;

        public string Template
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_Template))
                {
                    throw new Exception("Property either wasn't set or was set to an empty string/null");
                }

                return _Template;
            }
            set
            {
                _Template = value;
            }
        }
        

        public string Hash
        {
            get //[will contain GUIDs if the key is made up of reference fields]
            {
                if (FieldnameValuePairs == null)
                {
                    throw new Exception("Cannot generate hash from a null list of fieldname value pairs");
                }
                return HashGenerator.BuildMD5HashFromFieldnameValuePairs(FieldnameValuePairs);
            }

        }

        private Regex InterveningParentFolderRegex = new Regex("^[/]?(?<folder>[^/]*)[/]?$");

        private string _InterveningParentFolder;

        public string InterveningParentFolder
        {
            get
            {
                if (_InterveningParentFolder == null)
                {
                    throw new Exception("Property wasn't set or was set to null");
                }
               
                else
                {
                    return _InterveningParentFolder;
                }

            }
            set
            {
                if (value == String.Empty)
                {
                    _InterveningParentFolder = String.Empty;
                }
                else if (value == null)
                {
                    throw new Exception("Property was set to null");
                }
                else if (InterveningParentFolderRegex.Match(value).Length > 0)
                {
                    _InterveningParentFolder = InterveningParentFolderRegex.Match(value).Groups["folder"].Value;
                }
                else
                {
                    throw new Exception("Invalid folder name format. Folder can only have slashes at begning and end, or none at all.");
                }
                
            }
        }

        private string _Key;

       

        public string Key 
        {
            get
            {
                return _Key;
            }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new InvalidKeyException("Property was set to an empty string/null");
                }
                _Key = value;
            }
        }

        private string _BranchTemplateToInsert;

        public string BranchTemplateToInsert
        {
            get
            {
                //no need to check for null/empty, as program logic will interpret an empty string/null
                //as no branch template, and a non empty string as a path to a branch template
                return _BranchTemplateToInsert;
            }
            set
            {
                _BranchTemplateToInsert = value;
            }
        }
        

        private List<FieldnameValuePair> _FieldnameValuePairs;

        public List<FieldnameValuePair> FieldnameValuePairs
        {
            get
            {
                if (_FieldnameValuePairs == null || _FieldnameValuePairs.Contains(null))
                {
                    throw new Exception("Property wasn't set or was set to null or one of the FieldnameValuePairs was null."); //we could even check if one of the fieldnames or values was null or empty string here...look into this
                }

                return _FieldnameValuePairs;
            }
            set
            {
                _FieldnameValuePairs = value;
            }
        }

        private Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> _Children;

        public Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> Children
        {
            get
            {
                return _Children;
            }
            set
            {
                _Children = value;
            }
        }

        public bool IsDeltaDeleteEligible { get; set; }

        public bool ShouldDelete { get; set; }
    }
}
