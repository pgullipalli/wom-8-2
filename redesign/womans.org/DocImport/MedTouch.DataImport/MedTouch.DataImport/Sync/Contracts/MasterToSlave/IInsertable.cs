﻿namespace MedTouch.DataImport.Sync.Contracts.MasterToSlave
{
    public interface IInsertable
    {
        string GetName();
        string GetTemplate();
        string GetContextSensitiveKey();
        string GetInterveningParentFolder();
        string GetBranchTemplateToInsert(); //path to the branch template that is used to on insert - empty string means it doesn't exist
        bool IsDeltaDeleteEligible();
        bool ShouldDelete { get; set; }
        //TODO: what's missing is GetFieldValuePairs, GetHash -- these are done thru reflection, hence they aren't defined in the interface
        //well for now gethash is but it wont be later since that is also dependant on reflection
    }
}
