﻿using System.Collections.Generic;
using System.ComponentModel;

namespace MedTouch.DataImport.Sync.Contracts.MasterToSlave {
    /// <summary>
    /// Given a list of IInsertables, find all of the properties of type IInsertable,
    /// and return a list of the IInsertable objects those properties reference
    /// </summary>
    public class IInsertableChildrenListBuilder
    {
        /// <summary>
        /// Returns all children that implement the iInsertable interface
        /// </summary>
        public static List<IInsertable> BuildListOfIInsertableChildren(object o)
        {
            List<IInsertable> iInsertableChildren = new List<IInsertable>();

            //Nice to know that GetProperties() should also return properties from a base class
            //though it wont return properties from an interface (we dont care about getting properties from an interface in this case, just from a base class)
            //see: http://stackoverflow.com/questions/4031267/typedescriptor-doesnt-return-members-from-inherited-interfaces


            //Take all children, of all classes and return all of them in a single array
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(o))
            {
                IInsertable iInsertableScalarChild = property.GetValue(o) as IInsertable;

                if (iInsertableScalarChild != null)
                {
                    iInsertableChildren.Add(iInsertableScalarChild);
                    continue;
                }

                IInsertable[] iInsertableArrayChild = property.GetValue(o) as IInsertable[];
                if (iInsertableArrayChild != null)
                {
                    foreach (var iInsertableArrayChildElement in iInsertableArrayChild)
                    {
                        if (iInsertableArrayChildElement != null)
                        {
                            iInsertableChildren.Add(iInsertableArrayChildElement);
                        }

                    }

                }

            }

            return iInsertableChildren;
        }
    }
}
