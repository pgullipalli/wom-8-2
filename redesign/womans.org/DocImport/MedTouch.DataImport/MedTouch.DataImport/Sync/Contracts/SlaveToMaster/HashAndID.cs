﻿using System;

namespace MedTouch.DataImport.Sync.Contracts.SlaveToMaster
{
    public class HashAndID 
    {
        public string Hash { get; set; }
        public Guid ID { get; set; }

        public HashAndID(string hash, Guid id)
        {
            Hash = hash;
            ID = id;
        }

        public HashAndID(Guid id)
        {
            Hash = null;
            ID = id;
        }

        public Guid GetID() 
        {
            return this.ID;
        }

    }
}
