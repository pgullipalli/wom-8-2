﻿using System;

namespace MedTouch.DataImport.Sync.Contracts.SlaveToMaster
{
    public class ContextSensitiveKeyTemplateTuple : Tuple<string,string>
    {
        public string Key;
        public string Template;
        
        public ContextSensitiveKeyTemplateTuple(string key, string template) : base(key, template)
        {
            Key = key;
            Template = template;
        }

    }
}
