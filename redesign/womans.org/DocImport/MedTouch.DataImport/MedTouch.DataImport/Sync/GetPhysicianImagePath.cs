﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice;

namespace MedTouch.DataImport.Sync
{
    public class GetPhysicianImagePath
    {
        public string GetPhysicianImagePathViaWebService(string physicianImageFolderPath, string strItemName, string webPath)
        {
            return DatabaseWebserviceClient.Instance.GetPhysicianImagePathAfterUpload( physicianImageFolderPath,  strItemName,  webPath);
        }
    }
}
