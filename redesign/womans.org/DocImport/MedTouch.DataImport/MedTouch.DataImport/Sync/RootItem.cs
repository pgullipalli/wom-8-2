﻿using System.Collections.Generic;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;

namespace MedTouch.DataImport.Sync
{
    /// <summary>
    /// We assume that the root item already exists in the content tree and will not be synced itself, its descendants will be synced
    /// </summary>
    public class RootItem
    {
        public List<IInsertable> Children { get; set; }
        public string Path { get; set; }
    }
}
