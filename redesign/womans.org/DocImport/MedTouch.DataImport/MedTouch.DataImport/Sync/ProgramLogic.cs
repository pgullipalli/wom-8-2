﻿using System;
using System.Collections.Generic;
using System.Linq;
using MedTouch.DataImport.DataValidationAndExceptionHandling;
using MedTouch.DataImport.DatabaseCallProviders;
using MedTouch.DataImport.DatabaseCallProviders.DatabaseCallsViaWebservice;
using MedTouch.DataImport.SitecoreFieldModels;
using MedTouch.DataImport.SitecoreFieldModels.ReferenceFields;
using MedTouch.DataImport.SitecoreFieldModels.ReferenceFields.ValueToGuidResolutionAndSync;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;
using MedTouch.DataImport.Sync.Contracts.SlaveToMaster;
using MedTouch.DataImport.Sync.Mappings;
using MedTouch.DataImport._Class.Global;
using System.Diagnostics;
//using Sitecore.Diagnostics;

namespace MedTouch.DataImport.Sync
{
    ///The key we use on the client side is Tuple<ContextParentItemPath,Item.Key,Template>
    ///We use that key to do lookups by field in the DB and then to sync those lookups with items on the client
    ///To do DB lookups we need to pass that Tuple as well as a map provides the DB webservice with the fields nessary for forming a key given a certain template
    ///After that sync occurs, we simply use Guid
 
    public class ProgramLogic
    {
        public bool IsFullImport { get; set; }
        
        public DateTime UpdatedSince { get; set; }

        public ProgramLogic()
        {
        }

        public ProgramLogic(bool isFullImport)
        {
            SetIsFullImport(isFullImport);
        }

        private void SetIsFullImport(bool isFullImport)
        {
            IsFullImport = isFullImport;
        }

        private void SyncReferenceFieldTranslator(ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer, DatabaseWebserviceClient webserviceCallProvider, List<IInsertable> iInsertables)
        {
            referenceFieldValueToGuidResolverAndSyncer.Sync(webserviceCallProvider, iInsertables);
            
        }

        //We support one level of nesting

        private ReferenceFieldValueToGuidResolverAndSyncer BuildReferenceFieldTranslatorForItemAndChildrenAndSyncDatasources(List<IInsertable> iInsertables, DatabaseWebserviceClient webserviceCallProvider)
        {
            ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer = new ReferenceFieldValueToGuidResolverAndSyncer();

            iInsertables
                .ForEach(iInsertable =>                      
                    BuildReferenceFieldValueToGuidResolverAndSyncer(referenceFieldValueToGuidResolverAndSyncer, iInsertable)
                );

            SyncReferenceFieldTranslator(referenceFieldValueToGuidResolverAndSyncer, webserviceCallProvider, iInsertables);

 
            return referenceFieldValueToGuidResolverAndSyncer;
        }

        private static ReferenceFieldValueToGuidResolverAndSyncer BuildReferenceFieldValueToGuidResolverAndSyncer(List<IInsertable> iInsertables)
        {
            ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer = new ReferenceFieldValueToGuidResolverAndSyncer();

            foreach (var iInsertable in iInsertables)
            {
                BuildReferenceFieldValueToGuidResolverAndSyncer(referenceFieldValueToGuidResolverAndSyncer, iInsertable);
            }

            return referenceFieldValueToGuidResolverAndSyncer;

        }

        private static void BuildReferenceFieldValueToGuidResolverAndSyncer(ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer, IInsertable iInsertable)
        {
            var datasourceAndReferenceFieldValuePairs = ReferenceFieldDatasourcePathValuePairBuilder.GetReferenceFieldValuesForTranslationAndSync(iInsertable);

            foreach (var datasourceAndReferenceFieldValuePair in datasourceAndReferenceFieldValuePairs)
            {
                var datasourcePath = datasourceAndReferenceFieldValuePair.Fieldname;
                var referenceFieldValuePair = datasourceAndReferenceFieldValuePair.Value;


                referenceFieldValueToGuidResolverAndSyncer.Add(datasourcePath, referenceFieldValuePair);
            }

            foreach (var iInsertableChild in IInsertableChildrenListBuilder.BuildListOfIInsertableChildren(iInsertable))
            {
                BuildReferenceFieldValueToGuidResolverAndSyncer(referenceFieldValueToGuidResolverAndSyncer, iInsertableChild);
            }
        }

        public void RunImportWrapper(out int physiciansCount)
        {
            List<string> physiciansIds = null;
            physiciansIds = DatabaseWebserviceClient.Instance.GetPhysicianList();

            physiciansCount = physiciansIds.Count;
            int counter = 0;
            Console.WriteLine(string.Format("PhysicianImport@Total of imported physicians: {0}", physiciansCount));
            Utility.log(string.Format("PhysicianImport@Total of imported physicians: {0}", physiciansCount));
            foreach (string physiciansId in physiciansIds)
            {
                counter++;

                Console.WriteLine(String.Format("Start Updating Physician #: {0} of {1}", counter, physiciansCount));
                Utility.log(String.Format("Start Updating Physician #: {0} of {1} - guid {2}", counter, physiciansCount, physiciansId));
                Stopwatch sw = Stopwatch.StartNew();
                //Console.WriteLine(String.Format("Updating Clone Destinations, locations, and offices for physician: {0}", physiciansId));                
                //Utility.log(String.Format("Updating Clone Destinations, locations, and offices for physician: {0}", physiciansId));  
                try
                {

                    DatabaseWebserviceClient.Instance.PhysicianImportWrapper(physiciansId);
                }
                catch (Exception e)
                {

                    Console.WriteLine(String.Format("Unable to update physician: {0}", physiciansId));
                    Utility.log(String.Format("Unable to update physician: {0}", physiciansId));
                }
                sw.Stop();


                Console.WriteLine(String.Format("End Updating Physician #: {0} of {1}, total time {2}", counter, physiciansCount, sw.Elapsed.TotalSeconds));
                Utility.log(String.Format("End Updating Physician #: {0} of {1}, total time {2}", counter, physiciansCount, sw.Elapsed.TotalSeconds));
                Console.WriteLine("");
            }
        }

        //TODO: IInsertable only gets us so far, it won't verify that the annotations used for reference fields are used properly
        //- that's why gethash and getkeyvaluepairs arent defined -- if we want to verify that the annotations are used properly 
        //we can create a wrapper class which takes an IIdentifiable, verifies that the annotations are good, and creates 
        //Insertable Items from that -- or we just say its up to the programmer to make sure the annotationsa are used properly
        public void run(RootItem rootItem) 
        {
            //if(ConfigurationManager.AppSettings["medtouch.bata..import.logging.enabled"].Equals("true"))
            //    Log.Info("PhysicianImport@BuildReferenceFieldTranslatorForItemAndChildrenAndSyncDatasources", "INFO");

            Stopwatch sw = Stopwatch.StartNew();
            Console.WriteLine("Build ReferenceFieldValueToGuidResolverAndSyncer");
            ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer = BuildReferenceFieldTranslatorForItemAndChildrenAndSyncDatasources(rootItem.Children, DatabaseWebserviceClient.Instance);
            Utility.log("PhysicianImport@BuildInsertableItemsAndTheirChildren");
            sw.Stop();            
            Console.WriteLine("Build ReferenceFieldValueToGuidResolverAndSyncer Complete (" + sw.Elapsed.TotalSeconds + ")");

            //do the same thing for locations
            //pass locations into the insertable items creation

            sw = Stopwatch.StartNew();
            Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableItems = BuildInsertableItemsAndTheirChildren(rootItem.Children, referenceFieldValueToGuidResolverAndSyncer);
            sw.Stop();
            Console.WriteLine("BuildInsertableItemsAndTheirChildren Complete (" + sw.Elapsed.TotalSeconds + ")");

            sw = Stopwatch.StartNew();
            Guid iInsertableContextRootNodeGuid = DatabaseWebserviceClient.Instance.GetGuidFromPath(rootItem.Path);
            sw.Stop();
            Console.WriteLine("BuildInsertableItemsAndTheirChildren Complete (" + sw.Elapsed.TotalSeconds + ")");


            Utility.log("PhysicianImport@SyncChildrenOfIInsertableContextRootItem");
            sw = Stopwatch.StartNew();
            SyncChildrenOfIInsertableContextRootItem(insertableItems, iInsertableContextRootNodeGuid);
            sw.Stop();
            Console.WriteLine("SyncChildrenOfIInsertableContextRootItem Complete (" + sw.Elapsed.TotalSeconds + ")");

            //We handle this by calling web services to handle physician one by one to above web service time out.

            List<string> physiciansIds = null;
            if (IsFullImport)
                physiciansIds = DatabaseWebserviceClient.Instance.GetPhysicianList();
            else
                physiciansIds = DatabaseWebserviceClient.Instance.GetPhysicianListUpdatedSince(UpdatedSince);

            //if (physiciansIds != null && physiciansIds.Any())
            //{
            //    DatabaseWebserviceClient.Instance.UpdateCloningDestinationForPhysicians(physiciansIds);
            //}

            int counter = 0;
            Console.WriteLine(string.Format("PhysicianImport@Total of imported physicians: {0}", physiciansIds.Count));
            Utility.log(string.Format("PhysicianImport@Total of imported physicians: {0}",physiciansIds.Count));
            foreach (string physiciansId in physiciansIds)
            {
                counter++;

                Console.WriteLine(String.Format("Start Updating Physician #: {0} of {1}", counter, physiciansIds.Count));
                Utility.log(String.Format("Start Updating Physician #: {0} of {1}", counter, physiciansIds.Count));

                Console.WriteLine(String.Format("Updating Clone Destinations, locations, and offices for physician: {0}", physiciansId));                
                Utility.log(String.Format("Updating Clone Destinations, locations, and offices for physician: {0}", physiciansId)); 
               
                DatabaseWebserviceClient.Instance.PhysicianImportWrapper(physiciansId);

                Console.WriteLine(String.Format("End Updating Physician #: {0} of {1}", counter, physiciansIds.Count));
                Utility.log(String.Format("End Updating Physician #: {0} of {1}", counter, physiciansIds.Count));
            }

            Utility.log("AddToPublishingQueue");
            //DatabaseWebserviceClient.Instance.AddToPublishingQueue();
        }


        private void SyncChildrenOfIInsertableContextRootItem(Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableItems, Guid iInsertableContextRootNodeGuid)
        {
            Console.WriteLine("SyncChildrenOfIInsertableContextRootItem: " + iInsertableContextRootNodeGuid.ToString());
            List<ContextSensitiveKeyTemplateTupleToIDMapping> keyIDPairs = null;
            if (AreDeltaDeleteEligible(insertableItems))
            {
                insertableItems = DeleteShouldDeleteItems(DatabaseWebserviceClient.Instance, insertableItems, DatabaseWebserviceClient.Instance.GetChildren(iInsertableContextRootNodeGuid));

                keyIDPairs =
                    Syncer.SyncChildrenOfIInsertableContextRootItem(
                            insertableItems,
                            iInsertableContextRootNodeGuid, //has to be guid because path is NOT unique (2 physicians could have the same name for example)
                            DatabaseWebserviceClient.Instance,
                            Syncer.HowToDealWithUnusedItemsOnSlave.Keep //this policy does not apply to the reference items, they are handled differently
                );
            }
            else
            {
                keyIDPairs =
                    Syncer.SyncChildrenOfIInsertableContextRootItem(
                            insertableItems,
                            iInsertableContextRootNodeGuid, //has to be guid because path is NOT unique (2 physicians could have the same name for example)
                            DatabaseWebserviceClient.Instance,
                            Syncer.HowToDealWithUnusedItemsOnSlave.Discard //this policy does not apply to the reference items, they are handled differently
                );
            }
            
            
            List<ContextSensitiveKeyTemplateTupleToIDMapping> templateTupleList = keyIDPairs
                .ToList()
                .Where(contextSensitiveKeyTemplateTupleToIDMapping =>
                    insertableItems[contextSensitiveKeyTemplateTupleToIDMapping.ContextSensitiveKeyTemplateTuple].Children != null &&
                    insertableItems[contextSensitiveKeyTemplateTupleToIDMapping.ContextSensitiveKeyTemplateTuple].Children.Keys.Any()) //this makes sure there is at least 1 element in the list, though that element could be null and it will return true
                .ToList();

                foreach (ContextSensitiveKeyTemplateTupleToIDMapping contextSensitiveKeyTemplateTupleToIDMapping in templateTupleList) {
                    SyncChildrenOfIInsertableContextRootItem(
                        insertableItems[contextSensitiveKeyTemplateTupleToIDMapping.ContextSensitiveKeyTemplateTuple].Children, //TODO: this could be null or empty list...what to do then.
                        contextSensitiveKeyTemplateTupleToIDMapping.ID);  
                }
        }

        private bool AreDeltaDeleteEligible(Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableItems)
        {
            if (IsFullImport)
            {
                return false;
            }

            foreach (ContextSensitiveKeyTemplateTuple key in insertableItems.Keys)
            {
                InsertableItem insertableItem = insertableItems[key];
                if (!insertableItem.IsDeltaDeleteEligible)
                {
                    return false;
                }
            }

            return true;
        }

        private Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> DeleteShouldDeleteItems(IDatabaseCallProvider slaveCallProvider, Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableItems, Dictionary<ContextSensitiveKeyTemplateTuple, HashAndID> itemsInSitecore)
        {
            Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> notDeletedInsertableItems = new Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem>();
            var items = (from key in insertableItems.Keys
                                                   let insertableItem = insertableItems[key]
                                                   select new
                                                       {
                                                           Key = key,
                                                           InsertableItem = insertableItem,
                                                           ShouldDelete = insertableItem.IsDeltaDeleteEligible && insertableItem.ShouldDelete
                                                       }).ToList();

            foreach (var item in items)
            {
                HashAndID hashAndID;
                if (item.ShouldDelete && itemsInSitecore.TryGetValue(item.Key, out hashAndID))
                {
                        slaveCallProvider.DeleteItem(hashAndID.ID);
                        Utility.log(String.Format("Deleted physician (Sitecore ID: {0}, Key: {1})", hashAndID.ID, item.InsertableItem.Key));
                }
                else if (!item.ShouldDelete)
                {
                    notDeletedInsertableItems.Add(item.Key, item.InsertableItem);
                }
            }

            return notDeletedInsertableItems;
        }

        //call to handle unlimited levels of nesting

        private static Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> BuildInsertableLocationItemsAndTheirChildren(List<IInsertable> iInsertables, ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer)
        {
            if (iInsertables == null || iInsertables.Count() == 0) //recursive base case
            {
                return null;
            }

            if (iInsertables.Where(iInsertable => String.IsNullOrWhiteSpace(iInsertable.GetContextSensitiveKey())).Any()) 
            {
                Console.WriteLine(String.Format("Context sensitive key is null/empty, skipping")); //if we wanna get the parent, we gotta go up in the stack trace
                return null;
                //throw new Exception("Context sensitive key is null/empty"); //how should we handle this?
            }

            Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableItems = new Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem>();

            //TODO: the problem here is duplicate keys...what to do if we encounter duplicate keys
            //TODO: this is where we should try to catch as many exceptions as possible

            foreach (var iInsertable in iInsertables) {
                try
                {
                    if(iInsertable.ToString().Contains("MedTouch.Beta.Physicians.Import.Location"))
                    {
                        var insertableItem = new InsertableItem(
                             iInsertable.GetName(),
                             iInsertable.GetTemplate(),
                             iInsertable.GetBranchTemplateToInsert(),
                             iInsertable.GetContextSensitiveKey(),
                             iInsertable.GetInterveningParentFolder(),
                             BuildInsertableLocationItemsAndTheirChildren(IInsertableChildrenListBuilder.BuildListOfIInsertableChildren(iInsertable), referenceFieldValueToGuidResolverAndSyncer), //we call this 2wice, once here, and once in BuildReferenceFieldTranslatorForItemAndChildrenAndSyncDatasources
                             FieldnameValuePairBuilder.BuildFieldnameValuePairs(iInsertable, referenceFieldValueToGuidResolverAndSyncer)
                             );

                        insertableItem.IsDeltaDeleteEligible = iInsertable.IsDeltaDeleteEligible();
                        insertableItem.ShouldDelete = iInsertable.ShouldDelete;
                        insertableItems.Add(new ContextSensitiveKeyTemplateTuple(iInsertable.GetContextSensitiveKey(), iInsertable.GetTemplate()), insertableItem);
                    }
                }
                catch (InvalidKeyException)
                {
                    System.Console.WriteLine("Invalid Key"); //TODO: catch different types of errors + report them differntly -- the idea is to do the field validation in InsertableItem and throw a different error based on which field is being accessed (i.e. InvalidKeyError ...)
                }
                catch (ArgumentException)
                {
                    System.Console.WriteLine(String.Format("An item with the same key has already been added {0}", iInsertable.GetName())); //TODO: better error msg
                }
                catch
                {
                    System.Console.WriteLine("An error has occured in creating an insertableItem");
                }
            }
    
            return insertableItems;
        }
        private static Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> BuildInsertableItemsAndTheirChildren(List<IInsertable> iInsertables, ReferenceFieldValueToGuidResolverAndSyncer referenceFieldValueToGuidResolverAndSyncer)
        {
            if (iInsertables == null || iInsertables.Count() == 0) //recursive base case
            {
                return null;
            }

            if (iInsertables.Where(iInsertable => String.IsNullOrWhiteSpace(iInsertable.GetContextSensitiveKey())).Any()) 
            {
                Console.WriteLine(String.Format("Context sensitive key is null/empty, skipping")); //if we wanna get the parent, we gotta go up in the stack trace
                return null;
                //throw new Exception("Context sensitive key is null/empty"); //how should we handle this?
            }

            Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> insertableItems = new Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem>();

            //TODO: the problem here is duplicate keys...what to do if we encounter duplicate keys
            //TODO: this is where we should try to catch as many exceptions as possible

            foreach (var iInsertable in iInsertables) {
                try
                {

                    var insertableItem = new InsertableItem(
                         iInsertable.GetName(),
                         iInsertable.GetTemplate(),
                         iInsertable.GetBranchTemplateToInsert(),
                         iInsertable.GetContextSensitiveKey(),
                         iInsertable.GetInterveningParentFolder(),
                         BuildInsertableItemsAndTheirChildren(IInsertableChildrenListBuilder.BuildListOfIInsertableChildren(iInsertable), referenceFieldValueToGuidResolverAndSyncer), //we call this 2wice, once here, and once in BuildReferenceFieldTranslatorForItemAndChildrenAndSyncDatasources
                         FieldnameValuePairBuilder.BuildFieldnameValuePairs(iInsertable, referenceFieldValueToGuidResolverAndSyncer)
                         );

                    insertableItem.IsDeltaDeleteEligible = iInsertable.IsDeltaDeleteEligible();
                    insertableItem.ShouldDelete = iInsertable.ShouldDelete;
                    insertableItems.Add(new ContextSensitiveKeyTemplateTuple(iInsertable.GetContextSensitiveKey(), iInsertable.GetTemplate()), insertableItem);
                }
                catch (InvalidKeyException)
                {
                    System.Console.WriteLine("Invalid Key"); //TODO: catch different types of errors + report them differntly -- the idea is to do the field validation in InsertableItem and throw a different error based on which field is being accessed (i.e. InvalidKeyError ...)
                }
                catch (ArgumentException)
                {
                    System.Console.WriteLine(String.Format("An item with the same key has already been added {0}", iInsertable.GetName())); //TODO: better error msg
                }
                catch
                {
                    System.Console.WriteLine("An error has occured in creating an insertableItem");
                }
            }
    
            return insertableItems;
        }

        
    }

}
