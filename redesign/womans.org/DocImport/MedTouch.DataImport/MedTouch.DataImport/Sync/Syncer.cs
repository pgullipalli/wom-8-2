﻿using System;
using System.Collections.Generic;
using System.Linq;
using MedTouch.DataImport.DatabaseCallProviders;
using MedTouch.DataImport.HashsetExtensions;
using MedTouch.DataImport.InteractWithSitecore;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;
using MedTouch.DataImport.Sync.Contracts.SlaveToMaster;
using MedTouch.DataImport.Sync.Mappings;
using MedTouch.DataImport._Class.Global;
using System.Diagnostics;

namespace MedTouch.DataImport.Sync
{
    public class Syncer
    {

        public enum HowToDealWithUnusedItemsOnSlave
        {
            Keep,
            Discard
        }

        public static List<ContextSensitiveKeyTemplateTupleToIDMapping> SyncChildrenOfIInsertableContextRootItem(Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> masterItemsByContextSensitiveKeyTemplateTuple, Guid slaveContextParentGuid, IDatabaseCallProvider slaveCallProvider, HowToDealWithUnusedItemsOnSlave howToDealWithUnusedItemsOnSlave)
        {
            Console.WriteLine("Syncer.SyncChildrenOfIInsertableContextRootItem: " + slaveContextParentGuid.ToString() + "-" + howToDealWithUnusedItemsOnSlave);
            Dictionary<ContextSensitiveKeyTemplateTuple, HashAndID> slaveHashAndIDByContextSensitiveKeyTemplateTuple = slaveCallProvider.GetChildren(slaveContextParentGuid);

            Console.WriteLine("InsertItemsThatAreOnMasterButNotOnSlave: " + slaveContextParentGuid.ToString() + "-" + howToDealWithUnusedItemsOnSlave);
            Stopwatch sw = Stopwatch.StartNew();
            InsertItemsThatAreOnMasterButNotOnSlave(slaveCallProvider, slaveContextParentGuid, masterItemsByContextSensitiveKeyTemplateTuple, slaveHashAndIDByContextSensitiveKeyTemplateTuple);
            sw.Stop();
            Console.WriteLine("InsertItemsThatAreOnMasterButNotOnSlave Complete: " + sw.Elapsed.TotalSeconds + " seconds");
            
           // HydrateMasterItemsWithGuidAndHashInformationFromSlaveItems(masterItemsByContextSensitiveKeyTemplateTuple, slaveHashAndIDByContextSensitiveKeyTemplateTuple);

            if (howToDealWithUnusedItemsOnSlave == HowToDealWithUnusedItemsOnSlave.Discard)
            {
                RemoveItemsOnSlave(slaveCallProvider, masterItemsByContextSensitiveKeyTemplateTuple, slaveHashAndIDByContextSensitiveKeyTemplateTuple);
            }

            Console.WriteLine("SelectivelyUpdateItemsOnSlave: " + slaveContextParentGuid.ToString() + "-" + howToDealWithUnusedItemsOnSlave);
            sw = Stopwatch.StartNew();
            SelectivelyUpdateItemsOnSlave(slaveCallProvider, masterItemsByContextSensitiveKeyTemplateTuple, slaveHashAndIDByContextSensitiveKeyTemplateTuple);
            sw.Stop();
            Console.WriteLine("SelectivelyUpdateItemsOnSlave Complete: " + sw.Elapsed.TotalSeconds + " seconds");

            return masterItemsByContextSensitiveKeyTemplateTuple
                .Keys
                .Select(keyTemplateTuple =>
                    new ContextSensitiveKeyTemplateTupleToIDMapping(keyTemplateTuple, slaveHashAndIDByContextSensitiveKeyTemplateTuple[keyTemplateTuple].ID))
                .ToList();

        }

        private static void SelectivelyUpdateItemsOnSlave(IDatabaseCallProvider slaveCallProvider, Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> masterItemsByContextSensitiveKeyTemplateTuple, Dictionary<ContextSensitiveKeyTemplateTuple, HashAndID> slaveHashAndIDByContextSensitiveKeyTemplateTuple)
        {


            List <ContextSensitiveKeyTemplateTuple> keysList = masterItemsByContextSensitiveKeyTemplateTuple
                                           .Keys
                                           .Where(contextSensitiveKeyTemplateTuple => !masterItemsByContextSensitiveKeyTemplateTuple[contextSensitiveKeyTemplateTuple].Hash.Equals(slaveHashAndIDByContextSensitiveKeyTemplateTuple[contextSensitiveKeyTemplateTuple].Hash))
                                           .ToList();

            foreach (ContextSensitiveKeyTemplateTuple contextSensitiveKeyTemplateTuple in keysList)
            {
                slaveCallProvider.UpdateItem(slaveHashAndIDByContextSensitiveKeyTemplateTuple[contextSensitiveKeyTemplateTuple].ID, masterItemsByContextSensitiveKeyTemplateTuple[contextSensitiveKeyTemplateTuple]);
            }

        }

        public static void InsertItemsThatAreOnMasterButNotOnSlave(IDatabaseCallProvider slaveCallProvider, Guid slaveContextParentGuid, Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> masterItemsByContextSensitiveKeyTemplateTuple, Dictionary<ContextSensitiveKeyTemplateTuple, HashAndID> slaveHashAndIDByContextSensitiveKeyTemplateTuple)
        {
            var ContextSensitiveKeyTemplateTuplesOfItemsOnMasterButNotOnSlave =
                                                        new HashSet<ContextSensitiveKeyTemplateTuple>(masterItemsByContextSensitiveKeyTemplateTuple.Keys)
                                                       .Subtract(slaveHashAndIDByContextSensitiveKeyTemplateTuple.Keys)
                                                       .ToList();
            
            foreach (var ContextSensitiveKeyTemplateTupleOfItemsOnMasterButNotOnSlave in ContextSensitiveKeyTemplateTuplesOfItemsOnMasterButNotOnSlave)
            {
                var masterItem = masterItemsByContextSensitiveKeyTemplateTuple[ContextSensitiveKeyTemplateTupleOfItemsOnMasterButNotOnSlave];
                Guid id;

                var slaveActualParentGuid = slaveContextParentGuid;

                if (!String.IsNullOrWhiteSpace(masterItem.InterveningParentFolder))
                {
                    string templateName = string.Empty;
                    if (slaveContextParentGuid.ToString().ToLower().Equals(Constants.GenericItemPathGUID.PHYSICIAN_DIRECTORY,StringComparison.OrdinalIgnoreCase))
                    {
                        slaveActualParentGuid = slaveCallProvider.GetGuidOfSingleChildOfTemplateWhereNameEquals(slaveContextParentGuid, 
                           Constants.GenericItemTemplatePath.MODULES_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_PREDEFINED_FOLDER_PHYSICIAN_FOLDER, masterItem.InterveningParentFolder.ToUpper(), InsertIfDoesntExist.YES);
                    }
                    else
                    {
                        slaveActualParentGuid = slaveCallProvider.GetGuidOfSingleChildOfTemplateWhereNameEquals(slaveContextParentGuid, Constants.GenericItemTemplatePath.COMMON_FOLDER, 
                            masterItem.InterveningParentFolder, InsertIfDoesntExist.YES);
                    }
                }


                if (!String.IsNullOrEmpty(masterItem.BranchTemplateToInsert))
                {
                    id = slaveCallProvider.InsertItemUsingBranch(masterItem, slaveActualParentGuid);
                }
                else
                {
                    id = slaveCallProvider.InsertItem(masterItem, slaveActualParentGuid);
                }
                slaveHashAndIDByContextSensitiveKeyTemplateTuple[ContextSensitiveKeyTemplateTupleOfItemsOnMasterButNotOnSlave] = new HashAndID(id);              
            }
            
        }

        public static void RemoveItemsOnSlave(IDatabaseCallProvider slaveCallProvider, Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> masterItemsByContextSensitiveKeyTemplateTuple, Dictionary<ContextSensitiveKeyTemplateTuple, HashAndID> slaveHashAndIDByContextSensitiveKeyTemplateTuple)
        {

            new HashSet<ContextSensitiveKeyTemplateTuple>(slaveHashAndIDByContextSensitiveKeyTemplateTuple.Keys)
                                                        .Subtract(masterItemsByContextSensitiveKeyTemplateTuple.Keys)
                                                        .ToList()
                                                        .ForEach(key => slaveCallProvider.DeleteItem(slaveHashAndIDByContextSensitiveKeyTemplateTuple[key].ID));
        }

       
    }
}
