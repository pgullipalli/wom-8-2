﻿using System;
using MedTouch.DataImport.Sync.Contracts.SlaveToMaster;

namespace MedTouch.DataImport.Sync.Mappings
{
    //TODO: Should this be Key_IDTemplatePair
    public class ContextSensitiveKeyTemplateTupleToIDMapping
    {
        public ContextSensitiveKeyTemplateTupleToIDMapping(ContextSensitiveKeyTemplateTuple contextSensitiveKeyTemplateTuple, Guid id)
        {
            ContextSensitiveKeyTemplateTuple = contextSensitiveKeyTemplateTuple;
            ID = id;
        }

        public ContextSensitiveKeyTemplateTuple ContextSensitiveKeyTemplateTuple { get; set; }
        public Guid ID { get; set; }
    }
}
