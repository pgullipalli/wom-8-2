﻿namespace MedTouch.DataImport._Class.Global
{
    /// <summary>
    /// This is the constants will be used for import
    /// This is not designed to be changed by client
    /// If the value need to be changed by client please update to retrieve value from appsetting.
    /// ex:
    /// return ConfigurationManager.AppSettings[key name goes here]
    /// </summary>
    class Constants
    {
        public static class GenericItemPathGUID
        {
            public static string GLOBAL_PHYSICIAN_DIRECTORY_DEGREE_TITLES
            {
                get { return "A728A328-6AC9-49A9-BB5A-C7B0DB3CE734"; } 
            }
            public static string GLOBAL_PHYSICIAN_DIRECTORY_DEPARTMENTS
            {
                get { return "E80E1350-81F6-4965-8E34-35C68C28FD80"; }
            }
            public static string GLOBAL_PHYSICIAN_DIRECTORY_EDUCATION_TYPE
            {
                get { return "94FD383C-4254-4253-A0B2-C027C389F6A1"; }
            }
            //public static string GLOBAL_PHYSICIAN_DIRECTORY_GENDERS
            //{
            //    get { return "175C53E9-9310-480F-9C75-A14D87224A41"; }
            //}
            public static string GLOBAL_PHYSICIAN_DIRECTORY_INSURANCES
            {
                get { return "45B51A0F-0511-4DA4-B216-E4D5B87FD249"; }
            }
            public static string GLOBAL_PHYSICIAN_DIRECTORY_LANGUAGES
            {
                get { return "D1B796F0-96DD-4252-912A-67AEE4530FBA"; } 
            }
            public static string GLOBAL_PHYSICIAN_DIRECTORY_POSITION_TITLES
            {
                get { return "81349F23-4615-42AA-BFE8-BA07EE2219DD"; }
            }
            public static string GLOBAL_PHYSICIAN_DIRECTORY_PRACTICES
            {
                get { return "C92491AE-E39F-4D69-9A53-A5ED02663786"; }
            }
            public static string GLOBAL_TAXONOMY
            {
                get { return "EBD269B4-B320-4C79-9005-653F68B3A0B1"; }
            }
            public static string GLOBAL_TAXONOMY_AFFILIATIONS
            {
                get { return "E7E5DBFB-A749-4EAC-A66F-D7806C1F64F0"; }
            }
            public static string GLOBAL_SPECIALTIES
            {
                get { return "70C68350-2BFF-46E7-A86D-2186B4EFB1E1"; }
            }
            public static string GLOBAL_LOCATIONS
            {
                get { return "00D7B53A-C479-4F47-89AF-B3F9AB6FA942"; }
            }
            public static string PHYSICIAN_DIRECTORY
            {
                get { return "E0EB83E0-1193-46C7-89A2-37C7474BD7D5"; }
                //get { return "C84CB772-F820-47E2-B628-5C11AAA33A2B"; } // For LCL ENV.
            }
            //public static string GLOBAL_PHYSICIAN_DIRECTORY_AFFILIATIONS
            //{
            //    //get { return "63280A28-3BF3-4206-8D16-468D947081DC"; }
            //    get { return "E7E5DBFB-A749-4EAC-A66F-D7806C1F64F0"; }
            //}
            public static string GLOBAL_PHYSICIAN_DIRECTORY_PROVIDER_TYPES
            {
                get { return "D65E5676-0ED4-4D3A-B718-50DC55CE4D4C"; }
            }
            //public static string GLOBAL_PHYSICIAN_DIRECTORY_CLINICAL_INTERESTS
            //{
            //    get { return "D6266FB6-23E3-4DF6-9843-6BB19D79EA67"; }
            //}
            public static string GLOBAL_PHYSICIAN_DIRECTORY_MEDICAL_GROUPS
            {
                get { return "7E583A64-6036-4241-B372-10983764D91A"; }
            }
        }
        public static class GenericConstant
        {
            public static string MEDTOUCH_BETA_PHYSICIANS_IMPORT_LOCATION
            {
                get { return "MedTouch.Beta.Physicians.Import.Location"; }
            }            
            public static string GLOBAL_LOCATIONS
            {
                get { return "/sitecore/content/Global/Locations"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_SPECIALTIES
            {
                get { return "/sitecore/content/Global/List Manager/Specialties"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_AFFILIATIONS
            {
                get { return "/sitecore/content/Global/List Manager/Affiliations"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_TAXONOMY
            {
                get { return "/sitecore/content/Global/Taxonomy"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_DEGREE_TITLES
            {
                get { return "/sitecore/content/Global/Physician Directory/Degree Titles"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_DEPARTMENTS
            {
                get { return "/sitecore/content/Global/List Manager/Departments"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_LANGUAGES
            {
                get { return "/sitecore/content/Global/Physician Directory/Languages"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_NAME
            {
                get { return "Name"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_SPECIALTY_NAME
            {
                get { return "Specialty Name"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_DEGREE_TITLE_NAME
            {
                get { return "Degree Title Name"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_DEPARTMENT_NAME
            {
                get { return "Department Name"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_LANGUAGE_NAME
            {
                get { return "Language Name"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_ITEM_FILED_NAME_TAXONOMY_TERM
            {
                get { return "Taxonomy Term"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_AFFILIATION_NAME
            {
                get { return "Affiliation Name"; }
            }
            //public static string GLOBAL_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_AFFILIATIONS
            //{
            //    //get { return "/sitecore/content/Global/Physician Directory/Affiliations"; }
            //    get { return "/sitecore/content/Global/List Manager/Affiliations"; }
            //}
            public static string GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_AFFILIATION_CODE
            {
                get { return "Affiliation Code"; }
            }
            public static string GLOBAL_CONTENT_GLOABL_ITEM_FILED_NAME_TAXONOMY_ID
            {
                get { return "Taxonomy ID"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_POSITION_TITLES
            {
                get { return "/sitecore/content/Global/Physician Directory/Position Titles"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_PROVIDER_TYPES
            {
                get { return "/sitecore/content/Global/Physician Directory/Provider Types"; }
            }
            public static string GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_PROVIDER_TYPE_CODE
            {
                get { return "Provider Type Code"; }
            }
            //public static string GLOBAL_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_CLINICAL_INTERESTS
            //{
            //    get { return "/sitecore/content/Global/Physician Directory/Clinical Interests"; }
            //}
            public static string GLOBAL_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_MEDICAL_GROUPS
            {
                get { return "/sitecore/content/Global/Physician Directory/Medical Groups"; }
            }
        }
        public static class GenericItemTemplatePath
        {
            public static string MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_DEGREE_TITLE
            {
                get { return "Modules/Physician Directory/Component Templates/Degree Title"; }
            }
            public static string MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_DEMPARTMENT
            {
                get { return "Global/Component Templates/Department"; }
            }
            public static string MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_EDUCATION_TYPE
            {
                get { return "Modules/Physician Directory/Component Templates/Education Type"; }
            }
            //public static string MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_GENDER
            //{
            //    get { return "Modules/Physician Directory/Component Templates/Gender"; }
            //}
            public static string MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_INSURANCE
            {
                get { return "Global/Component Templates/Insurance"; }
            }
            public static string MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_LANGUAGE
            {
                get { return "Global/Component Templates/Language"; }
            }
            public static string MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_POSITION_TITLE
            {
                get { return "Modules/Physician Directory/Component Templates/Position Title"; }
            }
            public static string MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_PRACTICE
            {
                get { return "Modules/Physician Directory/Component Templates/Practice"; }
            }
            public static string GLOBAL_COMPONENT_TEMPLATES_TAXONOMY_TERM
            {
                get { return "Global/Component Templates/Taxonomy Term"; }
            }
            public static string GLOBAL_COMPONENT_TEMPLATES_SPECIALTY
            {
                get { return "Global/Component Templates/Specialty"; }
            }
            public static string GLOBAL_COMPONENT_TEMPLATES_LOCATION
            {
                get { return "Global/Page Templates/Location"; }
            }
            public static string MODULES_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_PREDEFINED_FOLDER_PHYSICIAN_FOLDER
            {
                get { return "Modules/Physician Directory/Component Templates/Predefined Folders/Physician Folder"; }
            }
            //public static string MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_AFFILIATIONS
            //{
            //    get { return "Modules/Physician Directory/Component Templates/Affiliation"; }
            //}
            public static string GLOBAL_COMPONENT_TEMPLATES_AFFILIATION
            {
                get { return "Global/Component Templates/Affiliation"; }
            }
            public static string MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_PROVIDER_TYPE
            {
                get { return "Modules/Physician Directory/Component Templates/Provider Type"; }
            }
            //public static string MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_CLINICAL_INTERESTS
            //{
            //    get { return "Modules/Physician Directory/Component Templates/Clinical Interest"; }
            //}
            public static string MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_MEDICAL_GROUPS
            {
                get { return "Modules/Physician Directory/Component Templates/Medical Group"; }
            }
            public static string COMMON_FOLDER
            {
                get { return "common/Folder"; }
            }
            public static string SITECORE_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_LANGUAGES
            {
                get { return "/sitecore/content/Global/List Manager/Languages"; }
            }
            public static string SITECORE_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_DEPARTMENTS
            {
                get { return "/sitecore/content/Global/List Manager/Departments"; }
            }
            public static string SITECORE_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_DEGREE_TITLES
            {
                get { return "/sitecore/content/Global/Physician Directory/Degree Titles"; }
            }
            public static string SITECORE_CONTENT_GLOBAL_SPECIALTIES
            {
                get { return "/sitecore/content/Global/List Manager/Specialties"; }
            }
            public static string SITECORE_CONTENT_GLOBAL_AFFILIATIONS
            {
                get { return "/sitecore/content/Global/List Manager/Affiliations"; }
            }
            public static string SITECORE_CONTENT_GLOBAL_TAXONOMY
            {
                get { return "/sitecore/content/Global/Taxonomy"; }
            }
            public static string SITECORE_CONTENT_GLOBAL_PROVIDER_TYPES
            {
                get { return "/sitecore/content/Global/Physician Directory/Provider Types"; }
            }
            //public static string SITECORE_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_CLINICAL_INTERESTS
            //{
            //    get { return "/sitecore/content/Global/Physician Directory/Clinical Interests"; }
            //}
            public static string SITECORE_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_MEDICAL_GROUPS
            {
                get { return "/sitecore/content/Global/Physician Directory/Medical Groups"; }
            }
        }

    }
}
