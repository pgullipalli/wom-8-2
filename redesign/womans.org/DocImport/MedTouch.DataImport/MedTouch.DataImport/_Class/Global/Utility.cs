﻿using System;
using System.Configuration;
using System.IO;

namespace MedTouch.DataImport._Class.Global
{
    public class Utility
    {
        /// <summary>
        /// Logs the specified log message.
        /// </summary>
        /// <param name="logMessage">The log message.</param>
        public static void log(string logMessage)
        {
            if (ConfigurationManager.AppSettings["medtouch.bata.import.logging.enabled"].Equals("true"))
            {
                string logFilePath = ConfigurationManager.AppSettings["medtouch.bata.import.logging.filepath"] + "log_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                if (!string.IsNullOrEmpty(logFilePath))
                {
                    using (StreamWriter w = File.AppendText(logFilePath))
                    {
                        w.Write("\r\nLog Entry : ");
                        w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                        w.WriteLine("{0}", logMessage);
                        w.Flush();
                    }
                }
            }
        }

    }
}
