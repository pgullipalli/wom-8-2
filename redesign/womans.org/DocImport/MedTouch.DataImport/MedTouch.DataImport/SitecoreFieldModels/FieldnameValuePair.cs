﻿namespace MedTouch.DataImport.SitecoreFieldModels
{

    public class FieldnameValuePair
    {
        public string Fieldname { get; set; }
        public string Value { get; set; }

        public FieldnameValuePair()
        {

        }

        public FieldnameValuePair(string fieldname, string value)
        {
            Fieldname = fieldname;
            Value = value;
        }


    }
    

}
