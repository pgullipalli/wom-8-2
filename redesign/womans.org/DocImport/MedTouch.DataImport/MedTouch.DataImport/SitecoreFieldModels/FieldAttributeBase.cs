﻿namespace MedTouch.DataImport.SitecoreFieldModels
{
    public abstract class FieldAttributeBase : System.Attribute
    {
        public string FieldName { get; set; }

        public FieldAttributeBase(string fieldName)
        {

            FieldName = fieldName;
        }
    }
}
