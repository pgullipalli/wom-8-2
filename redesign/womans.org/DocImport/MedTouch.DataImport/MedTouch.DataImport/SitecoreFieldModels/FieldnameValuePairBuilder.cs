﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using MedTouch.DataImport.SitecoreFieldModels.ReferenceFields;
using MedTouch.DataImport.SitecoreFieldModels.ReferenceFields.ValueToGuidResolutionAndSync;
using MedTouch.DataImport.SitecoreFieldModels.ValueFields;

namespace MedTouch.DataImport.SitecoreFieldModels
{
    /// <summary>
    /// Builds lists of properties and their values, given a properly formed and annotated class.
    /// Use the ReferenceFieldAttribute to mark fields as Sitecore reference fields (i.e. fields that store guids / pipe dilimeted guids)
    /// Make sure you are using properties (specifcy get and set to do so) and not fields
    /// Make sure those properties are public
    /// see: http://www.java2s.com/Code/CSharp/Reflection/GetsthespecifiedattributefromthePropertyDescriptor.htm
    /// </summary>
    public class FieldnameValuePairBuilder
    {

        //TODO: behavior if field isn't present in XML
        //TODO: behavior if field value is null
        //TODO: behavior if field value is empty string --> make the field empty- duh
        //Hao: The above TOBO behavior should be catched in xml parser not here.

        //TODO: IMPORTANT these methods need to be HEAVILY TESTED
        //TODO: if referenceFieldTranslator is not pass (i.e. = null), then we dont translate the values to guids [we do this when we build the hash]
               

        public static List<FieldnameValuePair> BuildFieldnameValuePairs(object o, ReferenceFieldValueToGuidResolverAndSyncer referenceFieldTranslator)
        {
            List<FieldnameValuePair> fieldValuePairs = new List<FieldnameValuePair>();

            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(o))
            {

                FieldAttributeBase fieldAttribute = FindFieldAttributeBaseAttribute(property);

                if (fieldAttribute != null)
                {
                    var propertyValue = property.GetValue(o);

                    if (propertyValue == null) //if it's not set at all, empty string
                    {
                        fieldValuePairs.Add(new FieldnameValuePair(fieldAttribute.FieldName, String.Empty));
                    }
                    
                    else if (propertyValue.GetType().IsArray)
                    {
                        if (fieldAttribute as ReferenceFieldAttribute != null) 
                        {
                            //this case really needs to be tested
                            //TODO: what do we do if we have an array that has null elements in it? -- if theya re all null, it should just return an empty string (OR IN SITECORE DOES IT HAVE TO BE AN EMPTY GUID?)-- we dont want to return pipes next to eachother that would be bad
                            fieldValuePairs.Add(
                                new FieldnameValuePair(
                                    fieldAttribute.FieldName, 
                                    referenceFieldTranslator
                                        .TranslateValuesToPipeDelimitedGuids(
                                            ((ReferenceFieldAttribute)fieldAttribute).DatasourcePath, 
                                            ((object[])propertyValue)
                                                .Where(obj => obj != null && !String.IsNullOrWhiteSpace(obj.ToString()))
                                                .Select(obj => obj.ToString())
                                                .ToArray()
                                        )
                                )
                            );
                        }
                        else if (fieldAttribute as ValueFieldAttribute != null) //multi list value field
                        {
                            throw new Exception("Mullist value fields are not supported - TODO: find out if sitecore supports them");
                        }
                        else 
                        {
                             throw new Exception("Fields other than valuefield and referncefield are not supported");
                        }
                    }

                    else if (propertyValue.GetType().IsPrimitive || propertyValue.GetType() == typeof(String))
                    {
                        if (fieldAttribute as ValueFieldAttribute != null)
                        {
                            fieldValuePairs.Add(new FieldnameValuePair(fieldAttribute.FieldName, propertyValue.ToString()));
                        }
                        else if (referenceFieldTranslator != null && fieldAttribute as ReferenceFieldAttribute != null)
                        {
                            fieldValuePairs.Add(
                                new FieldnameValuePair(
                                    fieldAttribute.FieldName,
                                    referenceFieldTranslator
                                        .TranslateValueToGuid(
                                            ((ReferenceFieldAttribute)fieldAttribute).DatasourcePath,
                                            propertyValue.ToString()
                                        ).ToString()
                                )
                           );
                        }
                        

                    }
                    else
                    {
                        throw new Exception("Invalid property type -- at this point only arrays and primitives are supported -- no support for any collections for example");
                    }

                }

            }

            return fieldValuePairs;


        }

        private static FieldAttributeBase FindFieldAttributeBaseAttribute(PropertyDescriptor property)
        {
            FieldAttributeBase fieldAttributeBaseAttribute = null;

            foreach (Attribute attribute in property.Attributes)
            {
                fieldAttributeBaseAttribute = attribute as FieldAttributeBase;

                if (fieldAttributeBaseAttribute != null)
                {
                    return fieldAttributeBaseAttribute;
                }
            }

            return null;
        }



        


    }
    
}
