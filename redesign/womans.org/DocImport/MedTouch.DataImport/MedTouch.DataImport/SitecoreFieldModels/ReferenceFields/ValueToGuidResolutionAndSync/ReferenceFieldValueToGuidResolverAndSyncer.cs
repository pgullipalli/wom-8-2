﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using MedTouch.DataImport.DatabaseCallProviders;
using MedTouch.DataImport.Sync;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;
using MedTouch.DataImport.Sync.Contracts.SlaveToMaster;
using MedTouch.DataImport._Class.Global;

namespace MedTouch.DataImport.SitecoreFieldModels.ReferenceFields.ValueToGuidResolutionAndSync
{
    /// <summary>
    /// Given a value of a reference field, find its Guid
    /// </summary>

    public class ReferenceFieldValueToGuidResolverAndSyncer
    {
        private Dictionary<string, Dictionary<string, Guid>> DatasourcePathToValueGuidMap;

        public ReferenceFieldValueToGuidResolverAndSyncer()
        {
            DatasourcePathToValueGuidMap = new Dictionary<string, Dictionary<string, Guid>>();
        }

        public void Add(string datasourcePath, string value)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                return;
            }

            if (!DatasourcePathToValueGuidMap.ContainsKey(datasourcePath))
            {
                DatasourcePathToValueGuidMap[datasourcePath] = new Dictionary<string, Guid>();
            }

            if (!DatasourcePathToValueGuidMap[datasourcePath].ContainsKey(value))
            {
                DatasourcePathToValueGuidMap[datasourcePath].Add(value, Guid.Empty);
            }

        }

        //public Guid TranslateValueToGuid(string datasourcePath, string value)
        //{
        //    try
        //    {
        //        return DatasourcePathToValueGuidMap[datasourcePath][value];
        //    }
        //    catch
        //    {
        //        return Guid.Empty; //TODO: or should this be Guid.Empty.ToString()?
        //    }
        //}

        public string TranslateValueToGuid(string datasourcePath, string value)
        {
            try
            {
                return DatasourcePathToValueGuidMap[datasourcePath][value].ToString("B");
            }
            catch
            {
                return string.Empty; //TODO: or should this be Guid.Empty.ToString()?
            }
        }

        public string TranslateValuesToPipeDelimitedGuids(string datasourcePath, string[] values)
        {
            try
            {
                return String.Join("|", values
                    .Where(value => !String.IsNullOrWhiteSpace(value))
                    .Select(value => TranslateValueToGuid(datasourcePath, value)));
            }
            catch
            {
                return String.Empty; //TODO: or should this be Guid.Empty.ToString()
            }
        }

        public void Sync(IDatabaseCallProvider callProvider, List<IInsertable> iInsertables)
        {
            foreach (var datasourcePath in DatasourcePathToValueGuidMap.Keys) 
            {
                Sync(datasourcePath, callProvider, iInsertables);
            }
        }
        public string GetInsertableItemTemplatePath(string datasourcePath, IDatabaseCallProvider callProvider)
        {
            string itemTemplatePath = string.Empty;
            //TODO: Hao no hard code guid or path. Need to move to app settings
            string parentItemGuid = callProvider.GetGuidFromPath(datasourcePath).ToString().ToUpper();
            if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_PHYSICIAN_DIRECTORY_DEGREE_TITLES))
            {
                itemTemplatePath = Constants.GenericItemTemplatePath.MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_DEGREE_TITLE;
            }
            else if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_PHYSICIAN_DIRECTORY_DEPARTMENTS))
            {
                itemTemplatePath = Constants.GenericItemTemplatePath.MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_DEMPARTMENT;
            }
            else if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_PHYSICIAN_DIRECTORY_EDUCATION_TYPE))
            {
                itemTemplatePath = Constants.GenericItemTemplatePath.MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_EDUCATION_TYPE;
            }
            else if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_PHYSICIAN_DIRECTORY_INSURANCES))
            {
                itemTemplatePath = Constants.GenericItemTemplatePath.MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_INSURANCE;
            }
            else if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_PHYSICIAN_DIRECTORY_LANGUAGES))
            {
                itemTemplatePath = Constants.GenericItemTemplatePath.MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_LANGUAGE;
            }
            else if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_PHYSICIAN_DIRECTORY_POSITION_TITLES))
            {
                itemTemplatePath = Constants.GenericItemTemplatePath.MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_POSITION_TITLE;
            }
            else if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_PHYSICIAN_DIRECTORY_PRACTICES))
            {
                itemTemplatePath = Constants.GenericItemTemplatePath.MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_PRACTICE;
            }
            else if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_TAXONOMY))
            {
                itemTemplatePath = Constants.GenericItemTemplatePath.GLOBAL_COMPONENT_TEMPLATES_TAXONOMY_TERM;
            }
            else if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_SPECIALTIES))
            {
                itemTemplatePath = Constants.GenericItemTemplatePath.GLOBAL_COMPONENT_TEMPLATES_SPECIALTY;
            }
            else if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_TAXONOMY_AFFILIATIONS))
            {
                itemTemplatePath = Constants.GenericItemTemplatePath.GLOBAL_COMPONENT_TEMPLATES_AFFILIATION;
            }
            //else if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_PHYSICIAN_DIRECTORY_AFFILIATIONS))
            //{
            //    itemTemplatePath = Constants.GenericItemTemplatePath.GLOBAL_COMPONENT_TEMPLATES_AFFILIATION;
            //}
            else if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_PHYSICIAN_DIRECTORY_PROVIDER_TYPES))
            {
                itemTemplatePath = Constants.GenericItemTemplatePath.MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_PROVIDER_TYPE;
            }
            //else if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_PHYSICIAN_DIRECTORY_CLINICAL_INTERESTS))
            //{
            //    itemTemplatePath = Constants.GenericItemTemplatePath.MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_CLINICAL_INTERESTS;
            //}
            else if (parentItemGuid.Equals(Constants.GenericItemPathGUID.GLOBAL_PHYSICIAN_DIRECTORY_MEDICAL_GROUPS))
            {
                itemTemplatePath = Constants.GenericItemTemplatePath.MODULE_PHYSICIAN_DIRECTORY_COMPONENT_TEMPLATES_MEDICAL_GROUPS;
            }
            return itemTemplatePath;
        }


        private static FieldAttributeBase FindFieldAttributeBaseAttribute(PropertyDescriptor property)
        {
            FieldAttributeBase fieldAttributeBaseAttribute = null;

            foreach (Attribute attribute in property.Attributes)
            {
                fieldAttributeBaseAttribute = attribute as FieldAttributeBase;

                if (fieldAttributeBaseAttribute != null)
                {
                    return fieldAttributeBaseAttribute;
                }
            }

            return null;
        }

        public List<FieldnameValuePair> getLocationFieldnameValuePair(List<IInsertable> iInsertables, string locationName)
        {
            List<FieldnameValuePair> fieldValuePairs = new List<FieldnameValuePair>();

            foreach (var insertable in iInsertables)
            {
                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(insertable))
                {
                    FieldAttributeBase fieldAttribute = FindFieldAttributeBaseAttribute(property);

                    if (fieldAttribute != null)
                    {
                        var propertyValue = property.GetValue(insertable);

                        if (propertyValue != null && propertyValue.GetType().IsArray)
                        {
                            //find location object
                            foreach (object arrayItem in (object[]) propertyValue)
                            {
                                if (arrayItem.ToString().Contains(Constants.GenericConstant.MEDTOUCH_BETA_PHYSICIANS_IMPORT_LOCATION))
                                {
                                    ArrayList arrlstTemp = new ArrayList((object[]) propertyValue);
                                    //get location object
                                    foreach (var locationItem in arrlstTemp)
                                    {
                                        PropertyInfo[] properties = locationItem.GetType().GetProperties();
                                        foreach (PropertyInfo pi in properties)
                                        {
                                            if (pi.GetValue(locationItem, null) != null)
                                            {
                                                if (pi.GetValue(locationItem, null).ToString().Equals(locationName))
                                                {
                                                    foreach (var i in arrlstTemp)
                                                    {
                                                        PropertyInfo[] p = i.GetType().GetProperties();
                                                        foreach (PropertyInfo f in p)
                                                        {
                                                            if (f.GetValue(locationItem, null) != null)
                                                            {
                                                                fieldValuePairs.Add(new FieldnameValuePair(f.Name,
                                                                                                           f.GetValue(
                                                                                                               locationItem,
                                                                                                               null).
                                                                                                               ToString()));
                                                            }
                                                            else
                                                            {
                                                                fieldValuePairs.Add(new FieldnameValuePair(f.Name,
                                                                                                           string.Empty));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return fieldValuePairs;
        }

        public void Sync(string datasourcePath, IDatabaseCallProvider callProvider, List<IInsertable> iInsertables)
        {
            Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem> referenceItems = new Dictionary<ContextSensitiveKeyTemplateTuple, InsertableItem>();
            
            string template = GetInsertableItemTemplatePath(datasourcePath, callProvider);

            if(!string.Equals(template, string.Empty))
            {
                string filedName = Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_NAME;

                if (datasourcePath.Equals(Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_SPECIALTIES))
                {
                    filedName = Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_SPECIALTY_NAME;
                }
                else if (datasourcePath.Equals(Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_DEGREE_TITLES))
                {
                    filedName = Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_DEGREE_TITLE_NAME;
                }
                else if (datasourcePath.Equals(Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_DEPARTMENTS))
                {
                    filedName = Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_DEPARTMENT_NAME;
                }
                else if (datasourcePath.Equals(Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_LANGUAGES))
                {
                    filedName = Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_LANGUAGE_NAME;
                }
                else if (datasourcePath.Equals(Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_AFFILIATIONS))
                {
                    filedName = Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_ITEM_FIELD_NAME_AFFILIATION_NAME;
                }
                else if (datasourcePath.Equals(Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_TAXONOMY))
                {
                    filedName = Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_ITEM_FILED_NAME_TAXONOMY_TERM;
                }

                bool isStandardTemplage = true;
                string templateKeyName = string.Empty;
                if (datasourcePath.Contains(Constants.GenericItemTemplatePath.SITECORE_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_LANGUAGES))
                {
                    templateKeyName = "Language";
                    isStandardTemplage = false;
                }
                else if (datasourcePath.Contains(Constants.GenericItemTemplatePath.SITECORE_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_DEPARTMENTS))
                {
                    templateKeyName = "Department";
                    isStandardTemplage = false;
                }
                else if (datasourcePath.Contains(Constants.GenericItemTemplatePath.SITECORE_CONTENT_GLOBAL_PHYSICIAN_DIRECTORY_DEGREE_TITLES))
                {
                    templateKeyName = "Degree Title";
                    isStandardTemplage = false;
                }
                else if (datasourcePath.Contains(Constants.GenericItemTemplatePath.SITECORE_CONTENT_GLOBAL_SPECIALTIES))
                {
                    templateKeyName = "Specialty";
                    isStandardTemplage = false;
                }
                else if (datasourcePath.Contains(Constants.GenericItemTemplatePath.SITECORE_CONTENT_GLOBAL_AFFILIATIONS))
                {
                    templateKeyName = "Affiliation";
                    isStandardTemplage = false;
                }
                else if (datasourcePath.Contains(Constants.GenericItemTemplatePath.SITECORE_CONTENT_GLOBAL_TAXONOMY))
                {
                    templateKeyName = "Taxonomy Term";
                    isStandardTemplage = false;
                }
                else if (datasourcePath.Contains(Constants.GenericItemTemplatePath.SITECORE_CONTENT_GLOBAL_PROVIDER_TYPES))
                {
                    templateKeyName = "Provider Type ";
                    isStandardTemplage = false;
                }
                else
                {
                    isStandardTemplage = true;
                }

                if(isStandardTemplage)
                {
                    DatasourcePathToValueGuidMap[datasourcePath]
                        .Keys
                        .ToList()
                        .ForEach(value => referenceItems.Add
                                (
                                new ContextSensitiveKeyTemplateTuple(value, template),
                                new InsertableItem(value, template, null, value, String.Empty, null, new List<FieldnameValuePair> { new FieldnameValuePair(filedName, value) })
                                )
                            ); 
                }
                else
                {
                    if (templateKeyName.Equals("Taxonomy Term", StringComparison.CurrentCultureIgnoreCase))
                    {
                        DatasourcePathToValueGuidMap[datasourcePath]
                            .Keys
                            .ToList()
                            .ForEach(value => referenceItems.Add(
                                new ContextSensitiveKeyTemplateTuple(value, template),
                                new InsertableItem(value.Split('|').First(), template, null, value.Split('|').First(), String.Empty, null, new List<FieldnameValuePair>
                                        {
                                            new FieldnameValuePair(Constants.GenericConstant.GLOBAL_CONTENT_GLOBAL_ITEM_FILED_NAME_TAXONOMY_TERM, value.Split('|').First()), 
                                            new FieldnameValuePair(Constants.GenericConstant.GLOBAL_CONTENT_GLOABL_ITEM_FILED_NAME_TAXONOMY_ID, value.Split('|').Last())
                                        }
                                    ))
                            );
                    }
                    else
                    {
                        DatasourcePathToValueGuidMap[datasourcePath]
                            .Keys
                            .ToList()
                            .ForEach(value => referenceItems.Add(
                                new ContextSensitiveKeyTemplateTuple(value, template),
                                new InsertableItem(value.Split('|').First(), template, null, value.Split('|').First(), String.Empty, null, new List<FieldnameValuePair>
                                        {
                                            new FieldnameValuePair(templateKeyName + " Name", value.Split('|').First()), 
                                            new FieldnameValuePair(templateKeyName + " Code", value.Split('|').Last())
                                        }
                                    ))
                            );
                    }
                }
                var parentItemGuid = callProvider.GetGuidFromPath(datasourcePath);

                var keyIDpairs = Syncer.SyncChildrenOfIInsertableContextRootItem(
                        referenceItems, 
                        parentItemGuid, 
                        callProvider, 
                        Syncer.HowToDealWithUnusedItemsOnSlave.Keep
                        );

                keyIDpairs
                    .Select(contextSensitiveKeyTemplateTuple_IdMapping => 
                        new {
                            ContextSensitiveKey = contextSensitiveKeyTemplateTuple_IdMapping.ContextSensitiveKeyTemplateTuple.Key, 
                            ID = contextSensitiveKeyTemplateTuple_IdMapping.ID
                        })
                    .ToList()
                    .ForEach(contextSensitiveKeyIDPair =>
                        DatasourcePathToValueGuidMap[datasourcePath][contextSensitiveKeyIDPair.ContextSensitiveKey] = contextSensitiveKeyIDPair.ID
                    );
                }
                else
                {
                    Console.WriteLine("Template is empty.");
                }
        }
    }
}
