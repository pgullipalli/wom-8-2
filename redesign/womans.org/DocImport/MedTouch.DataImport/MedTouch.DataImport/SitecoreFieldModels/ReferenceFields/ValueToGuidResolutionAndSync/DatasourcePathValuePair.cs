﻿using System.Collections.Generic;

namespace MedTouch.DataImport.SitecoreFieldModels.ReferenceFields.ValueToGuidResolutionAndSync
{

    public class DatasourcePathValuePair
    {
        public string DatasourcePath { get; set; }
        public string Value { get; set; }

        public DatasourcePathValuePair()
        {

        }

        public DatasourcePathValuePair(string fieldname, string value)
        {
            DatasourcePath = fieldname;
            Value = value;
        }

        public static List<DatasourcePathValuePair> GenerateSinglePairList(string datasourcePath, string value)
        {
            return new List<DatasourcePathValuePair>() { new DatasourcePathValuePair(datasourcePath, value) };
        }
    }


}
