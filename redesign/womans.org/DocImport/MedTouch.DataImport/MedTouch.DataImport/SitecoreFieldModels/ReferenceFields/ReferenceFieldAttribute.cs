﻿//http://msdn.microsoft.com/en-us/library/aa288454%28v=vs.71%29.aspx

namespace MedTouch.DataImport.SitecoreFieldModels.ReferenceFields
{
    public class ReferenceFieldAttribute : FieldAttributeBase
    {
        //public static HashSet<string> AllDatasourcePaths = new HashSet<string>(); --no longer going with static - going to use reflection to get this data -- try to stay away from statics

        public string DatasourcePath {get;set;}
        //public string FieldName { get; set; }   --they all use the same field name so this is unecessary

        //The constructor doesn't seem to get called until we start parsing the XML document (and accessing the properties with these attributes I think)
        //So I think we have to actually access the property or call the method that this attribute is tied to in order for the constructor to be called

        //it is possible that we pass the same datasourcePath to 2 properties but with different fieldnames (though i think this would only happen by mistake) --
        //TODO: should we check for that?

        public ReferenceFieldAttribute(string fieldName, string datasourcePath) : base(fieldName)
        {

            DatasourcePath = datasourcePath;
            //ieldName = fieldName; -- they all use the same fieldname so this is unecesary
            //AllDatasourcePaths.Add(datasourcePath); //another way to do this would be to recursively drill down through the properties of physician and any other complex type looking at their attributes
        }

        

    }
}
