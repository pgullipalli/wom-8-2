﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;

namespace MedTouch.DataImport.SitecoreFieldModels.ReferenceFields
{
    /// <summary>
    /// Uses reflection to build a dictionary 
    /// </summary>
    public class ReferenceFieldDatasourcePathValuePairBuilder
    {
        public static List<FieldnameValuePair> GetReferenceFieldValuesForTranslationAndSync(object o)
        {

            List<FieldnameValuePair> referenceFieldParentPathValuePairs = new List<FieldnameValuePair>();

            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(o))
            {

                ReferenceFieldAttribute referenceFieldAttribute = FindReferenceFieldAttribute(property);

                if (referenceFieldAttribute != null)
                {
                    var propertyValue = property.GetValue(o);

                    if (propertyValue == null)
                    {
                        continue;
                    }

                        else if (propertyValue.GetType().IsArray)
                        {
                            foreach (var arrayItem in (object[])propertyValue)
                            {
                                if (arrayItem.ToString().Contains("MedTouch.Beta.Physicians.Import.Location"))
                                {
                                    Type t = arrayItem.GetType();
                                    PropertyInfo p = t.GetProperty("Location_Name");
                                    referenceFieldParentPathValuePairs.Add(new FieldnameValuePair(referenceFieldAttribute.DatasourcePath, p.GetValue(arrayItem, null).ToString()));
                                }
                                else
                                {
                                    if (!String.IsNullOrWhiteSpace(arrayItem.ToString()))
                                    {
                                        referenceFieldParentPathValuePairs.Add(new FieldnameValuePair(referenceFieldAttribute.DatasourcePath, arrayItem.ToString()));
                                    }
                                
                                }
                            }
                    }

                    else if (propertyValue.GetType().IsPrimitive || propertyValue.GetType() == typeof(String))
                    {
                        if (!String.IsNullOrWhiteSpace(propertyValue.ToString()))
                        {
                            referenceFieldParentPathValuePairs.Add(new FieldnameValuePair(referenceFieldAttribute.DatasourcePath, propertyValue.ToString()));
                        }

                    }
                    else
                    {
                        throw new Exception("Invalid property type -- at this point only arrays and primitives are supported -- no support for any collections for example");
                    }

                }

            }

            return referenceFieldParentPathValuePairs;
        }
        
        private static ReferenceFieldAttribute FindReferenceFieldAttribute(PropertyDescriptor property)
        {
            ReferenceFieldAttribute referenceFieldAttribute = null;

            foreach (Attribute attribute in property.Attributes)
            {
                referenceFieldAttribute = attribute as ReferenceFieldAttribute;

                if (referenceFieldAttribute != null)
                {
                    return referenceFieldAttribute;
                }
            }

            return null;
        }


    }
}
