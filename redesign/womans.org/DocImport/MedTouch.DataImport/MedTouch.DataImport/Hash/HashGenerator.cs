﻿using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using MedTouch.DataImport.SitecoreFieldModels;

namespace MedTouch.DataImport.Hash
{
    public class HashGenerator
    {
        public static string BuildMD5HashFromFieldnameValuePairs(List<FieldnameValuePair> fieldnameValuePairs)
        {

            System.Xml.Serialization.XmlSerializer xmlserializer = new System.Xml.Serialization.XmlSerializer(fieldnameValuePairs.GetType());

            StringBuilder hashString = new StringBuilder();

            using (MemoryStream stream = new MemoryStream())
            {
                xmlserializer.Serialize(stream, fieldnameValuePairs);

                foreach (byte b in new MD5CryptoServiceProvider().ComputeHash(stream.ToArray()))
                {
                    hashString.Append(b.ToString("X2"));
                }

            }

            return hashString.ToString();


        }

        public static string BuildMD5HashFromString(string s)
        {
            byte[] bs = new MD5CryptoServiceProvider().ComputeHash(System.Text.Encoding.UTF8.GetBytes(s));
            System.Text.StringBuilder hashString = new System.Text.StringBuilder();
            foreach (byte b in bs)
            {
                hashString.Append(b.ToString("x2"));
            }

            return hashString.ToString();
        }
    }
}
