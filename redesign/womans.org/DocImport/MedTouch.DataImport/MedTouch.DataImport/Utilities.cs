﻿using System;
using System.Text.RegularExpressions;

namespace MedTouch.DataImport
{
    public class Utilities
    {


        public static string GeneralizeName(string name)
        {
            string newName = Regex.Replace(name, @"\.", String.Empty); //remove periods (ex: M.D. becomes MD);
            newName = Regex.Replace(newName, @"[^\w]", " "); // replace special characters with space
            newName = Regex.Replace(newName, @"\s+", " "); // replace multiple space with a single space            
            newName = Regex.Replace(newName, @"^[ \t]+|[ \t]+$", ""); // remove leading and trailing space

            return newName; //the toLower() and dashes will be provided by URL helper
        }


       
        
    }
}
