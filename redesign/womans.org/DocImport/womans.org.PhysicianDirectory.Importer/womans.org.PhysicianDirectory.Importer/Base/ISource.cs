﻿
namespace MedTouch.PhysicianDirectory.Import
{
    public interface ISource<T>
    {
        T Source { get; set; }
    }
}
