﻿
namespace MedTouch.PhysicianDirectory.Import.Utilities.Serialization
{
    public interface IJsonSerializer<T>
    {
        string Serialize(T data);
        T Deserialize(string json);
    }
}
