﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using MedTouch.PhysicianDirectory.Import.Utilities.Serialization.DTO;

namespace MedTouch.PhysicianDirectory.Import.Utilities.Serialization
{
    /// <summary>
    /// Mediator object for XmlSerializer with Generics support
    /// </summary>
    /// <typeparam name="T">object type -- Note: the type of object used must be serializable</typeparam>
    public class XmlSerializer<T> : IXmlSerializer<T>
    {
        #region Members

        private static bool DefaultErrorLoggingEnabled = true;
        private static XmlSerializer DefaultXmlSerializer = new XmlSerializer(typeof(T));

        #endregion 

        #region Properties

        protected XmlSerializer Serializer { get; set; }
        protected bool ErrorLoggingEnabled { get; set; }

        #endregion

        #region Constructors

        protected XmlSerializer()
            : this(DefaultErrorLoggingEnabled)
        {
        }

        protected XmlSerializer(XmlSerializer xmlSerializer)
            : this(xmlSerializer, DefaultErrorLoggingEnabled)
        {
        }

        protected XmlSerializer(bool errorLoggingEnabled)
            : this(DefaultXmlSerializer, errorLoggingEnabled)
        {
        }

        protected XmlSerializer(XmlSerializer xmlSerializer, bool errorLoggingEnabled)
        {
            SetSerializer(xmlSerializer);
            SetErrorLoggingEnabled(errorLoggingEnabled);
        }

        #endregion

        #region Methods

        protected void SetSerializer(XmlSerializer xmlSerializer)
        {
            Serializer = xmlSerializer;
        }

        protected void SetErrorLoggingEnabled(bool errorLoggingEnabled)
        {
            ErrorLoggingEnabled = errorLoggingEnabled;
        }

        /// <summary>
        /// Loads an xml string representation of the given object into the provider Stream
        /// </summary>
        /// <param name="stream">the stream</param>
        /// <param name="data">the object to serialize</param>
        public void Serialize(Stream stream, T data)
        {
            Serializer.Serialize(stream, data);
        }

        /// <summary>
        /// Creates an xml string representation of the given object
        /// </summary>
        /// <param name="data">the object to serialize</param>
        /// <returns>an xml string representation of the given object</returns>
        /// <remarks>An empty string will be returned if an exception is encountered</remarks>
        public string Serialize(T data)
        {
            XmlSerializeParameters<T> xmlSerializeParameters = new XmlSerializeParameters<T> { Data = data };
            return Serialize(xmlSerializeParameters);
        }

        /// <summary>
        /// Creates an xml string representation of the given object
        /// </summary>
        /// <param name="data">the object to serialize</param>
        /// <param name="xmlSerializeParameters">Settings for xml generation/param>
        /// <returns>an xml string representation of the given object</returns>
        /// <remarks>An empty string will be returned if an exception is encountered</remarks>
        public string Serialize(XmlSerializeParameters<T> xmlSerializeParameters)
        {
            StringBuilder stringBuilder = new StringBuilder();

            try
            {
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    OmitXmlDeclaration = xmlSerializeParameters.OmitXmlDeclaration,
                    Indent = xmlSerializeParameters.IndentElements
                };
                
                using (XmlWriter xmlWriter = XmlWriter.Create(stringBuilder, settings))
                {
                    if (xmlSerializeParameters.XmlSerializerNamespaces != null)
                    {
                        Serializer.Serialize(xmlWriter, xmlSerializeParameters.Data, xmlSerializeParameters.XmlSerializerNamespaces); 
                    }
                    else
                    {
                        Serializer.Serialize(xmlWriter, xmlSerializeParameters.Data);    
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("XmlSerializer Exception:  Cannot serialize object", ex);
            }

            return stringBuilder.ToString();
        }

        private static bool IsDefaultInstanceOfType(T data)
        {
            if (typeof(T).IsValueType)
                return default(T).Equals(data);

            return data == null;
        }

        /// <summary>
        /// Creates an object from the given xml string
        /// </summary>
        /// <param name="xml">string representation of xml</param>
        /// <returns>an object of the specified type</returns>
        /// <remarks>The default value of the specified type will be returned if an exception is encountered</remarks>
        public T Deserialize(string xml)
        {
            T data = default(T);

            try
            {
                using (StringReader stringReader = new StringReader(xml))
                {
                    data = (T)Serializer.Deserialize(stringReader);
                }
            }
            catch (Exception ex)
            {
                LogError("XmlSerializer Exception:  Cannot deserialize object", ex);
            }

            return data;
        }

        private void LogError(string message, Exception exception)
        {
            if (ErrorLoggingEnabled)
            {
                
            }
        }

        public static IXmlSerializer<T> Create()
        {
            return new XmlSerializer<T>();
        }

        public static IXmlSerializer<T> Create(XmlSerializer xmlSerializer)
        {
            return new XmlSerializer<T>(xmlSerializer);
        }

        public static IXmlSerializer<T> Create(bool errorLoggingEnabled)
        {
            return new XmlSerializer<T>(errorLoggingEnabled);
        }

        public static IXmlSerializer<T> Create(XmlSerializer xmlSerializer, bool errorLoggingEnabled)
        {
            return new XmlSerializer<T>(xmlSerializer, errorLoggingEnabled);
        }

        #endregion
    }
}