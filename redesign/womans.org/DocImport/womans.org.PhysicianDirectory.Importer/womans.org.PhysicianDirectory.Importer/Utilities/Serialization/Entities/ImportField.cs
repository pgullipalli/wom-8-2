﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.Serialization;
using MedTouch.PhysicianDirectory.Import.Utilities.Serialization.Entities;

namespace womans.org.PhysicianDirectory.Import.Utilities.Serialization.Entities
{
    public class ImportField
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Value { get; set; }
        
        public override string ToString()
        {
            return SerializableEntityWrapper<ImportField>.Create(this).ToJsonString();
        }

        public static ImportField JsonToObjectOfType(string json)
        {
            return SerializableEntityWrapper<ImportField>.JsonToObjectOfType(json);
        }
    }
}
