﻿
namespace MedTouch.PhysicianDirectory.Import.Utilities.Serialization.Entities
{
    public interface IXmlEntity
    {
        string ToXmlString();
        string ToXmlString(bool indentElements);
    }
}
