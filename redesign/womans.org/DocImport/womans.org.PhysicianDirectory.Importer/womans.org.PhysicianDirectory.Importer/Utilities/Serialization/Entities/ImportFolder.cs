﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using MedTouch.PhysicianDirectory.Import.Utilities.Serialization.Entities;

namespace womans.org.PhysicianDirectory.Import.Utilities.Serialization.Entities
{
    public class ImportFolder
    {
        [DataMember]
        public ImportItem Folder { get; set; }

        [DataMember]
        public List<ImportItem> Items { get; set; }

        public override string ToString()
        {
            return SerializableEntityWrapper<ImportFolder>.Create(this).ToJsonString();
        }

        public static ImportFolder JsonToObjectOfType(string json)
        {
            return SerializableEntityWrapper<ImportFolder>.JsonToObjectOfType(json);
        }
    }
}
