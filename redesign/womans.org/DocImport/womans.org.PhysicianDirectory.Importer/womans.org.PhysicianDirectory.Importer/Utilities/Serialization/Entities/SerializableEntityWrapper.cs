﻿
namespace MedTouch.PhysicianDirectory.Import.Utilities.Serialization.Entities
{
    public class SerializableEntityWrapper<T> : SerializableEntity<T>, ISerializableEntityWrapper
    {
        private T ObjectToSerialize { get; set; }

        private SerializableEntityWrapper(T objectToSerialize)
        {
            SetObjectToSerialize(objectToSerialize);
        }

        public void SetObjectToSerialize(object objectToSerialize)
        {
            AssertObjectToSerialize(objectToSerialize);
            SetObjectToSerialize((T)objectToSerialize);
        }

        private void SetObjectToSerialize(T objectToSerialize)
        {
            ObjectToSerialize = objectToSerialize;
        }

        private void AssertObjectToSerialize(object objectToSerialize)
        {
        }

        private static bool IsNotNullOrDefault(object objectToSerialize)
        {
            return !object.Equals(objectToSerialize, default(T));
        }

        protected override T GetCurrentObject()
        {
            return ObjectToSerialize;
        }

        public static T XmlToObjectOfType(string xml)
        {
            return SerializableEntity<T>.XmlToObjectOfType(xml);
        }

        public static T JsonToObjectOfType(string json)
        {
            return SerializableEntity<T>.JsonToObjectOfType(json);
        }

        public static ISerializableEntityWrapper Create(T objectToSerialize)
        {
            return new SerializableEntityWrapper<T>(objectToSerialize);
        }
    }
}
