﻿
namespace MedTouch.PhysicianDirectory.Import.Utilities.Serialization.Entities
{
    public interface IJsonEntity
    {
        string ToJsonString();
    }
}
