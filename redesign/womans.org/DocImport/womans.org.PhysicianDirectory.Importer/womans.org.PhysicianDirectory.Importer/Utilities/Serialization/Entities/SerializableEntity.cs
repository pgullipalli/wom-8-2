﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

using MedTouch.PhysicianDirectory.Import.Utilities.Serialization.DTO;

namespace MedTouch.PhysicianDirectory.Import.Utilities.Serialization.Entities
{
    [DataContract]
    public abstract class SerializableEntity<T> : ISerializableEntity
    {
        #region Members

        protected static IXmlSerializer<T> XmlSerializer = XmlSerializer<T>.Create();
        protected static IJsonSerializer<T> JsonSerializer = JsonSerializer<T>.Create();

        private static XmlSerializerNamespaces _xmlSerializerNamespaces = GetEmptyXmlSerializerNamespaces();

        #endregion

        #region Methods

        protected abstract T GetCurrentObject();

        public object GetObject()
        {
            return GetCurrentObject();
        }

        /// <summary>
        /// Converts the current instance into a string of XML
        /// </summary>
        /// <returns>a string of XML</returns>
        /// <remarks>if an exception is encountered, an empty string will be returned</remarks>
        public string ToXmlString()
        {
            return ToXmlString(false);
        }

        public string ToXmlString(bool indentElements)
        {
            XmlSerializeParameters<T> xmlSerializeParameters = new XmlSerializeParameters<T>
            {
                Data = GetCurrentObject(),
                IndentElements = indentElements,
                XmlSerializerNamespaces = _xmlSerializerNamespaces,
                OmitXmlDeclaration = true
            };

            return XmlSerializer.Serialize(xmlSerializeParameters);
        }

        private static XmlSerializerNamespaces GetEmptyXmlSerializerNamespaces()
        {
            XmlSerializerNamespaces xmlSerializerNamespaces = new XmlSerializerNamespaces();
            xmlSerializerNamespaces.Add(string.Empty, string.Empty);
            return xmlSerializerNamespaces;
        }

        public string ToJsonString()
        {
            return JsonSerializer.Serialize(GetCurrentObject());
        }

        public static T XmlToObjectOfType(string xml)
        {
            return XmlSerializer.Deserialize(xml);
        }

        public static T JsonToObjectOfType(string json)
        {
            return JsonSerializer.Deserialize(json);
        }

        #endregion
    }
}