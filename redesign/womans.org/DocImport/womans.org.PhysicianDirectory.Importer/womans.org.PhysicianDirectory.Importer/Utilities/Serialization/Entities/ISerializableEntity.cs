﻿
namespace MedTouch.PhysicianDirectory.Import.Utilities.Serialization.Entities
{
    public interface ISerializableEntity : IXmlEntity, IJsonEntity
    {
        object GetObject();
    }
}
