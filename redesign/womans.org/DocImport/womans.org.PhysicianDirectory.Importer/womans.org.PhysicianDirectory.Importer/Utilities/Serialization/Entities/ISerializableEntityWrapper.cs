﻿
namespace MedTouch.PhysicianDirectory.Import.Utilities.Serialization.Entities
{
    public interface ISerializableEntityWrapper : ISerializableEntity
    {
        void SetObjectToSerialize(object objectToSerialize);
    }
}
