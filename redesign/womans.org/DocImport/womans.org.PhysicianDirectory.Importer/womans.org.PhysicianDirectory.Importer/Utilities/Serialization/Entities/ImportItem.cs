﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using MedTouch.PhysicianDirectory.Import.Utilities.Serialization.Entities;

namespace womans.org.PhysicianDirectory.Import.Utilities.Serialization.Entities
{
    public class ImportItem
    {
        [DataMember]
        public string TemplatePath { get; set; }

        [DataMember]
        public string ParentPath { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<ImportField> Fields { get; set; }

        public override string ToString()
        {
            return SerializableEntityWrapper<ImportItem>.Create(this).ToJsonString();
        }

        public static ImportItem JsonToObjectOfType(string json)
        {
            return SerializableEntityWrapper<ImportItem>.JsonToObjectOfType(json);
        }
    }
}
