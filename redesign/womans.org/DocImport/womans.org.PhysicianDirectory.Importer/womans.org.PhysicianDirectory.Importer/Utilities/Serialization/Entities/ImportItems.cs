﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using MedTouch.PhysicianDirectory.Import.Utilities.Serialization.Entities;

namespace womans.org.PhysicianDirectory.Import.Utilities.Serialization.Entities
{
    public class ImportItems
    {
        [DataMember]
        public string FieldName { get; set; }

        [DataMember]
        public List<ImportItem> Items { get; set; }

        public override string ToString()
        {
            return SerializableEntityWrapper<ImportItems>.Create(this).ToJsonString();
        }

        public static ImportItems JsonToObjectOfType(string json)
        {
            return SerializableEntityWrapper<ImportItems>.JsonToObjectOfType(json);
        }
    }
}
