﻿using System.Xml.Serialization;

namespace MedTouch.PhysicianDirectory.Import.Utilities.Serialization.DTO
{
    public class XmlSerializeParameters<T>
    {
        public T Data { get; set; }
        public XmlSerializerNamespaces XmlSerializerNamespaces { get; set; }
        public bool IndentElements { get; set; }
        public bool OmitXmlDeclaration { get; set; }
    }
}
