﻿using System.IO;

using MedTouch.PhysicianDirectory.Import.Utilities.Serialization.DTO;

namespace MedTouch.PhysicianDirectory.Import.Utilities.Serialization
{
    public interface IXmlSerializer<T>
    {
        void Serialize(Stream stream, T data);
        string Serialize(T data);
        string Serialize(XmlSerializeParameters<T> xmlSerializeParameters);
        T Deserialize(string xml);
    }
}
