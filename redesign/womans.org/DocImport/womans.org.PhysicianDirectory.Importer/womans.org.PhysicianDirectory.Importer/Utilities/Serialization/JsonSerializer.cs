﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace MedTouch.PhysicianDirectory.Import.Utilities.Serialization
{
    public class JsonSerializer<T> : IJsonSerializer<T>
    {
        private const bool DefaultErrorLoggingEnabled= true;
        private static XmlObjectSerializer DefaultJsonSerializer = new DataContractJsonSerializer(typeof(T));

        protected XmlObjectSerializer Serializer { get; set; }
        protected bool ErrorLoggingEnabled { get; set; }

        #region Constructors

        protected JsonSerializer()
            : this(DefaultJsonSerializer)
        {
        }

        protected JsonSerializer(XmlObjectSerializer jsonSerializer)
            : this(jsonSerializer, DefaultErrorLoggingEnabled)
        {
        }

        protected JsonSerializer(bool errorLoggingEnabled)
            : this(DefaultJsonSerializer, errorLoggingEnabled)
        {
        }

        protected JsonSerializer(XmlObjectSerializer jsonSerializer, bool errorLoggingEnabled)
        {
            SetSerializer(jsonSerializer);
            SetErrorLoggingEnabled(errorLoggingEnabled);
        }
       
        #endregion

        #region Methods

        protected void SetSerializer(XmlObjectSerializer jsonSerializer)
        {
            Serializer = jsonSerializer;
        }

        protected void SetErrorLoggingEnabled(bool errorLoggingEnabled)
        {
            ErrorLoggingEnabled = errorLoggingEnabled;
        }

        public string Serialize(T data)
        {
            string json = string.Empty;

            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Serializer.WriteObject(memoryStream, data);
                    json = Encoding.Default.GetString(memoryStream.ToArray());
                }
            }
            catch (Exception ex)
            {
                LogError(this.ToString(), ex);
            }

            return json;
        }

        public T Deserialize(string json)
        {
            T data = default(T);

            try
            {
                byte[] bytes = Encoding.Default.GetBytes(json);

                using (MemoryStream memoryStream = new MemoryStream(bytes))
                {
                    data = (T)Serializer.ReadObject(memoryStream);
                }
            }
            catch (Exception ex)
            {
                LogError(this.ToString(), ex);
            }

            return data;
        }

        private void LogError(string message, Exception exception)
        {
            if (ErrorLoggingEnabled)
            {
                
            }
        }

        private static bool IsDefaultInstanceOfType(T data)
        {
            if (typeof(T).IsValueType)
                return default(T).Equals(data);

            return data == null;
        }

        public static IJsonSerializer<T> Create()
        {
            return new JsonSerializer<T>();
        }

        public static IJsonSerializer<T> Create(XmlObjectSerializer jsonSerializer)
        {
            return new JsonSerializer<T>(jsonSerializer);
        }

        public static IJsonSerializer<T> Create(bool errorLoggingEnabled)
        {
            return new JsonSerializer<T>(errorLoggingEnabled);
        }

        public static IJsonSerializer<T> Create(XmlObjectSerializer jsonSerializer, bool errorLoggingEnabled)
        {
            return new JsonSerializer<T>(jsonSerializer, errorLoggingEnabled);
        }

        #endregion
    }
}
