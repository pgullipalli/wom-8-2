﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MedTouch.PhysicianDirectory.Import.Utilities.Validation
{
    public class Assertor : IAssertor
    {
        private Assertor()
        {
        }

        public void ArgumentCondition(bool condition, string argumentName, string message)
        {
            if (!condition)
            {
                message =  "An argument condition was false.";

                if (argumentName != null)
                {
                    throw new ArgumentException(message, argumentName);
                }
                throw new ArgumentException(message);
            }
        }

        public void ArgumentNotNull(object argument, string argumentName)
        {
            if (argument == null)
            {
                if (argumentName != null)
                {
                    throw new ArgumentNullException(argumentName);
                }
                throw new ArgumentNullException();
            }
        }

        public void ArgumentNotNull(object argument, Func<string> getArgumentName)
        {
            if (argument == null)
            {
                string paramName = getArgumentName();
                if (paramName != null)
                {
                    throw new ArgumentNullException(paramName);
                }
                throw new ArgumentNullException();
            }
        }

        public void ArgumentNotNullOrEmpty(Guid? argument, string argumentName)
        {
            ArgumentNotNull(argument, argumentName);
            if (!argument.HasValue || argument.Value == Guid.Empty)
            {
                throw new ArgumentException("Null Guids are not allowed.");
            }
        }

        public void ArgumentNotNullOrEmpty(string argument, string argumentName)
        {
            if (string.IsNullOrEmpty(argument))
            {
                if (argument == null)
                {
                    if (argumentName != null)
                    {
                        throw new ArgumentNullException(argumentName, "Null ids are not allowed.");
                    }
                    throw new ArgumentNullException();
                }
                if (argumentName != null)
                {
                    throw new ArgumentException("Empty strings are not allowed.", argumentName);
                }
                throw new ArgumentException("Empty strings are not allowed.");
            }
        }

        public void ArgumentNotNullOrEmpty(string argument, Func<string> getArgumentName)
        {
            if (string.IsNullOrEmpty(argument))
            {
                string paramName = getArgumentName();
                if (argument == null)
                {
                    if (paramName != null)
                    {
                        throw new ArgumentNullException(paramName, "Null ids are not allowed.");
                    }
                    throw new ArgumentNullException();
                }
                if (paramName != null)
                {
                    throw new ArgumentException("Empty strings are not allowed.", paramName);
                }
                throw new ArgumentException("Empty strings are not allowed.");
            }
        }

        public void AreEqual(int value1, int value2, string message)
        {
            if (value1 != value2)
            {
                throw new Exception(message);
            }
        }

        public void AreEqual(int value1, int value2, string format, params object[] args)
        {
            if (value1 != value2)
            {
                string message = string.Format(format, args);
                AreEqual(value1, value2, message);
            }
        }

        public void AreEqual(string value1, string value2, string message)
        {
            if ((value1 == null) && (value2 != null))
            {
                throw new Exception(message);
            }
            if ((value1 != null) && (value2 == null))
            {
                throw new Exception(message);
            }
            if (((value1 != null) && (value2 != null)) && (value1.Length != value2.Length))
            {
                throw new Exception(message);
            }
            if (value1 != value2)
            {
                throw new Exception(message);
            }
        }

        public void AreEqual(string value1, string value2, string format, params object[] args)
        {
            if (value1 != value2)
            {
                string message = string.Format(format, args);
                AreEqual(value1, value2, message);
            }
        }

        public void IsFalse(bool condition, string message)
        {
            ArgumentNotNull(message, "message");
            if (condition)
            {
                throw new InvalidOperationException(message);
            }
        }

        public void IsFalse(bool condition, Func<string> getMessage)
        {
            if (condition)
            {
                string message = getMessage();
                if (message != null)
                {
                    throw new InvalidOperationException(message);
                }
                throw new InvalidOperationException();
            }
        }

        public void IsFalse(bool condition, string format, params object[] args)
        {
            if (condition)
            {
                string message = string.Format(format, args);
                IsFalse(condition, message);
            }
        }

        public void IsNotNull(object value, string message)
        {
            if (value == null)
            {
                throw new InvalidOperationException(message);
            }
        }

        public void IsNotNull(object value, string format, params object[] args)
        {
            if (value == null)
            {
                string message = string.Format(format, args);
                IsNotNull(value, message);
            }
        }

        public void IsNotNull(object value, Type type)
        {
            if (value == null)
            {
                IsNotNull(value, type, string.Empty, new object[0]);
            }
        }

        public void IsNotNull(object value, Type type, string format, params object[] args)
        {
            if (value == null)
            {
                string message = string.Format(format, args);
                IsNotNull(value, message);
            }
        }

        public void IsNotNullOrEmpty(string value, string message)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new InvalidOperationException(message);
            }
        }

        public void IsNotNullOrEmpty(string value, string format, params object[] args)
        {
            if (string.IsNullOrEmpty(value))
            {
                string message = string.Format(format, args);
                IsNotNullOrEmpty(value, message);
            }
        }

        public void IsNull(object value, string message)
        {
            if (value != null)
            {
                throw new InvalidOperationException(message);
            }
        }

        public void IsNull(object value, string format, params object[] args)
        {
            if (value != null)
            {
                string message = string.Format(format, args);
                IsNull(value, message);
            }
        }

        public void IsTrue(bool condition, string message)
        {
            if (!condition)
            {
                throw new InvalidOperationException(message);
            }
        }

        public void IsTrue(bool condition, Func<string> getMessage)
        {
            if (!condition)
            {
                string message = getMessage();
                if (message != null)
                {
                    throw new InvalidOperationException(message);
                }
                throw new InvalidOperationException();
            }
        }

        public void IsTrue(bool condition, string format, params object[] args)
        {
            if (!condition)
            {
                string message = string.Format(format, args);
                IsTrue(false, message);
            }
        }

        public void ReflectionObjectCreated(object obj, string typeName)
        {
            if (obj == null)
            {
                throw new Exception(string.Format("The type '{0}' could not be created.", typeName));
            }
        }

        public void Required(object obj, string message)
        {
            if (obj == null)
            {
                throw new Exception(message);
            }
        }

        public void Required(object obj, string format, params object[] args)
        {
            if (obj == null)
            {
                string message = string.Format(format, args);
                Required(obj, message);
            }
        }

        public T ResultNotNull<T>(T result, string message) where T : class
        {
            ArgumentNotNullOrEmpty(message, "message");
            IsNotNull(result, message);
            return result;
        }

        public T ResultNotNull<T>(T result) where T : class
        {
            return ResultNotNull<T>(result, "Post condition failed");
        }

        public static IAssertor CreateNewAssertor()
        {
            return new Assertor();
        }
    }
}
