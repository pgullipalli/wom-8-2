﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedTouch.PhysicianDirectory.Import.Utilities.Validation
{
    public interface IAssertor
    {
        void ArgumentCondition(bool condition, string argumentName, string message);

        void ArgumentNotNull(object argument, string argumentName);

        void ArgumentNotNull(object argument, Func<string> getArgumentName);

        void ArgumentNotNullOrEmpty(Guid? argument, string argumentName);

        void ArgumentNotNullOrEmpty(string argument, string argumentName);

        void ArgumentNotNullOrEmpty(string argument, Func<string> getArgumentName);

        void AreEqual(int value1, int value2, string message);

        void AreEqual(int value1, int value2, string format, params object[] args);

        void AreEqual(string value1, string value2, string message);

        void AreEqual(string value1, string value2, string format, params object[] args);

        void IsFalse(bool condition, string message);

        void IsFalse(bool condition, Func<string> getMessage);

        void IsFalse(bool condition, string format, params object[] args);

        void IsNotNull(object value, string message);

        void IsNotNull(object value, string format, params object[] args);

        void IsNotNull(object value, Type type);

        void IsNotNull(object value, Type type, string format, params object[] args);

        void IsNotNullOrEmpty(string value, string message);

        void IsNotNullOrEmpty(string value, string format, params object[] args);

        void IsNull(object value, string message);

        void IsNull(object value, string format, params object[] args);

        void IsTrue(bool condition, string message);

        void IsTrue(bool condition, Func<string> getMessage);

        void IsTrue(bool condition, string format, params object[] args);

        void ReflectionObjectCreated(object obj, string typeName);

        void Required(object obj, string message);

        void Required(object obj, string format, params object[] args);

        T ResultNotNull<T>(T result, string message) where T : class;

        T ResultNotNull<T>(T result) where T : class;
    }
}
