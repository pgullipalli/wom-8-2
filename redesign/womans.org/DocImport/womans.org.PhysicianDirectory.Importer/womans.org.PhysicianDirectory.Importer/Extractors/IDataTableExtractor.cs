﻿using System.Data;
using System.IO;

namespace MedTouch.PhysicianDirectory.Import.Extractors
{
    public interface IDataTableExtractor : IExtractor<FileSystemInfo, DataTable>
    {
    }
}
