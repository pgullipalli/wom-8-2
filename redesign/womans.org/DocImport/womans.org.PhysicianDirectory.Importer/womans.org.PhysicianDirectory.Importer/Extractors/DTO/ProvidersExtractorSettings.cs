﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using womans.org.PhysicianDirectory.Import.Extractors.Models;

namespace womans.org.PhysicianDirectory.Import.Extractors.DTO
{
    public class ProvidersExtractorSettings
    {
        public string ProviderSource { get; set; }
        public IEnumerable<Affiliation> Affiliations { get; set; }
        public IEnumerable<Language> Languages { get; set; }
        public IEnumerable<Practice> Practices { get; set; }
        public IEnumerable<ProviderStatus> ProviderStatuses { get; set; }
        public IEnumerable<ProviderType> ProviderTypes { get; set; }
        public IEnumerable<Specialty> Specialties { get; set; }
    }
}
