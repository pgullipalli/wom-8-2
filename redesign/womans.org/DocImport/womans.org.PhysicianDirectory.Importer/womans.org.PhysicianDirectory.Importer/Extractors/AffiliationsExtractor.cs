﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MedTouch.PhysicianDirectory.Import.Extractors;
using womans.org.PhysicianDirectory.Import.Extractors.Models;

namespace womans.org.PhysicianDirectory.Import.Extractors
{
    public class AffiliationsExtractor : XmlExtractor<Affiliation>
    {
        private AffiliationsExtractor() 
            : base()
        {
        }

        private AffiliationsExtractor(string source)
            : base(source)
        {
        }

        protected override IEnumerable<Affiliation> ExtractModelInstances(XElement root)
        {
            return (from affiliation in GetDescendants(root, "Affiliation")
                    let id = GetChildValue(affiliation, "ID")
                     let name = GetChildValue(affiliation, "Name")
                     let providerCount = GetChildValue(affiliation, "ProviderCount")
                    where !string.IsNullOrWhiteSpace(id)
                           && !string.IsNullOrWhiteSpace(name)
                           && !string.IsNullOrWhiteSpace(providerCount)
                    select new Affiliation
                        {
                            ID = id,
                            Name = name,
                            ProviderCount = providerCount
                        }).ToList();
        }

        public static XmlExtractor<Affiliation> CreateNewAffiliationsExtractor()
        {
            return new AffiliationsExtractor();
        }

        public static XmlExtractor<Affiliation> CreateNewAffiliationsExtractor(string source)
        {
            return new AffiliationsExtractor(source);
        }
    }
}
