﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using MedTouch.PhysicianDirectory.Import.Extractors;
using womans.org.PhysicianDirectory.Import.Extractors.Models;

namespace womans.org.PhysicianDirectory.Import.Extractors
{
    public class WomansCsvExtractor : CsvExtractor
    {
        private const string Pipe = "|";

        public WomansCsvExtractor() : base()
        {
        }

        public WomansCsvExtractor(FileSystemInfo source)
            : base(source)
        {
        }

        public override DataTable Extract()
        {
            DataTable rawData = base.Extract();
            DataTable formattedData = CreateNewDataTable();

            foreach (DataRow physician in rawData.Rows)
            {
                ImportPhysician(physician, formattedData);
            }
            
            return formattedData;
        }

        private static void ImportPhysician(DataRow physician, DataTable formattedData)
        {
            DataRow formattedPhysician = formattedData.NewRow();

            /// Physician Fields   

            formattedPhysician[CsvFields.FirstName] = physician[CsvFields.FirstName].ToString().Trim();

            formattedPhysician[CsvFields.MiddleInitial] = physician[CsvFields.MiddleInitial].ToString().Trim();

            formattedPhysician[CsvFields.LastName] = physician[CsvFields.LastName].ToString().Trim();

            string degreeTitle = physician[CsvFields.DegreeTitles].ToString().Trim();
            if (!string.IsNullOrWhiteSpace(degreeTitle))
            {
                List<string> degreeTitlesArray = new List<string>();
                degreeTitlesArray.Add(degreeTitle);
                formattedPhysician[CsvFields.DegreeTitles] = degreeTitlesArray;    
            }

            List<string> languagesSpokenArray = new List<string>();
            //languagesSpokenArray.Add("English");
            string languagesSpokenField = physician[CsvFields.LanguagesSpoken].ToString().Trim();
            List<string> languagesSpokenFieldSplit = languagesSpokenField.Split(';').ToList();
            foreach (string languageSpoken in languagesSpokenFieldSplit)
            {
                if (!string.IsNullOrWhiteSpace(languageSpoken))
                {
                    languagesSpokenArray.Add(languageSpoken.Trim());
                }
            }
            formattedPhysician[CsvFields.LanguagesSpoken] = languagesSpokenArray;

            formattedPhysician[CsvFields.InternalHospitalID] = physician[CsvFields.InternalHospitalID].ToString().Trim();

            formattedPhysician[CsvFields.Website] = physician[CsvFields.Website].ToString().Trim();

            //we get multiple rows when there are multiple specialties, so just return this as a string
            //List<string> specialtyArray = new List<string>();
            //specialtyArray.Add(physician[CsvFields.Specialties].ToString().Trim());
            formattedPhysician[CsvFields.Specialties] = physician[CsvFields.Specialties].ToString().Trim();


            //TODO: Populate Board Certs properly
            StringBuilder certificationValue = new StringBuilder();
            string certification1 = physician[CsvFields.CertificationFields.Certifications1].ToString().Trim();
            string certification2 = physician[CsvFields.CertificationFields.Certifications2].ToString().Trim();
            string certification3 = physician[CsvFields.CertificationFields.Certifications3].ToString().Trim();
            if (!string.IsNullOrWhiteSpace(certification1) || !string.IsNullOrWhiteSpace(certification2) ||
                !string.IsNullOrWhiteSpace(certification3))
            {
                certificationValue.AppendLine("<ul>");
                if (!string.IsNullOrWhiteSpace(certification1))
                    certificationValue.AppendLine(string.Format("<li>{0}</li>", certification1));
                if (!string.IsNullOrWhiteSpace(certification2))
                    certificationValue.AppendLine(string.Format("<li>{0}</li>", certification2));
                if (!string.IsNullOrWhiteSpace(certification3))
                    certificationValue.AppendLine(string.Format("<li>{0}</li>", certification3));
                certificationValue.AppendLine("</ul>");
                formattedPhysician[CsvFields.Certifications] = certificationValue.ToString();
            }
            else
            {
                formattedPhysician[CsvFields.Certifications] = string.Empty;
            }

            

            
            //if (specialtyArray.Count > 0)
            //{
            //    formattedPhysician[CsvFields.Certifications] = certificationValue.ToString();
            //}
            //else
            //{
            //    formattedPhysician[CsvFields.Certifications] = string.Empty;
            //}            

            /// Education Fields
            //formattedPhysician[CsvFields.Education.EducationType] = physician[CsvFields.Education.EducationType].ToString().Trim();
            //formattedPhysician[CsvFields.Education.InstitutionName] = physician[CsvFields.Education.InstitutionName].ToString().Trim();

            /// Internship Fields
            //List<string> internshipArray = new List<string>();
            //for (int i = 1; i <= InternshipFieldsCount; i++)
            //{
            //    string internshipInstitutionName = physician[string.Format(InternshipFormat, i)].ToString().Trim();
            //    if (!string.IsNullOrWhiteSpace(internshipInstitutionName))
            //    {
            //        internshipArray.Add(internshipInstitutionName);
            //    }
            //}
            //formattedPhysician[CsvFields.Internship.InstitutionNames] = internshipArray;

            /// Residency Fields
            //List<string> residencyArray = new List<string>();
            //for (int i = 1; i <= ResidencyFieldsCount; i++)
            //{
            //    string residencyInstitutionName = physician[string.Format(ResidencyFormat, i)].ToString().Trim();
            //    if (!string.IsNullOrWhiteSpace(residencyInstitutionName))
            //    {
            //        residencyArray.Add(residencyInstitutionName);
            //    }
            //}
            //formattedPhysician[CsvFields.Residency.InstitutionNames] = residencyArray;

            /// Fellowship Fields
            //List<string> fellowshipArray = new List<string>();
            //for (int i = 1; i <= FellowshipFieldsCount; i++)
            //{
            //    string fellowshipInstitutionName = physician[string.Format(FellowshipFormat, i)].ToString().Trim();
            //    if (!string.IsNullOrWhiteSpace(fellowshipInstitutionName))
            //    {
            //        fellowshipArray.Add(fellowshipInstitutionName);
            //    }
            //}
            //formattedPhysician[CsvFields.Fellowship.InstitutionNames] = fellowshipArray;

            /// Office Fields
            List<CsvOffice> officeArray = new List<CsvOffice>();
            string officeName = physician[CsvFields.OfficeFields.OfficeName1].ToString().Trim();
            if (string.IsNullOrWhiteSpace(officeName))
            {
                string firstNameValue = physician[CsvFields.FirstName].ToString().Trim();
                string middleInitialValue = physician[CsvFields.MiddleInitial].ToString().Trim();
                string lastNameValue = physician[CsvFields.LastName].ToString().Trim();
                //string suffixValue = physician[CsvFields.Suffix].ToString().Trim();
                string degreeTitleValue = physician[CsvFields.DegreeTitles].ToString().Trim();                    
                if (!string.IsNullOrWhiteSpace(firstNameValue))
                {
                    officeName = firstNameValue;
                }
                if (!string.IsNullOrWhiteSpace(middleInitialValue))
                {
                    officeName = string.Format("{0} {1}", officeName, middleInitialValue);
                }
                if (!string.IsNullOrWhiteSpace(lastNameValue))
                {
                    officeName = string.Format("{0} {1}", officeName, lastNameValue);
                }
                //if (!string.IsNullOrWhiteSpace(suffixValue))
                //{
                //    officeName = string.Format("{0} {1}", officeName, suffixValue);
                //}
                if (!string.IsNullOrWhiteSpace(degreeTitleValue))
                {
                    officeName = string.Format("{0}, {1}", officeName, degreeTitleValue);
                }
            }

                CsvLocation csvLocation = new CsvLocation();
                csvLocation.LocationName = officeName;
                csvLocation.Address1 = physician[CsvFields.OfficeFields.Address11].ToString().Trim();
                csvLocation.Address2 = physician[CsvFields.OfficeFields.Address12].ToString().Trim();
                csvLocation.City = physician[CsvFields.OfficeFields.City1].ToString().Trim();
                csvLocation.State = physician[CsvFields.OfficeFields.State1].ToString().Trim();
                csvLocation.Zip = physician[CsvFields.OfficeFields.Zip1].ToString().Trim();

                CsvOffice csvOffice = new CsvOffice();              
                csvOffice.OfficeName = officeName;
                csvOffice.Location = csvLocation;
                csvOffice.OfficePhone = physician[CsvFields.OfficeFields.OfficePhone1].ToString().Trim();
                csvOffice.OfficeFax = physician[CsvFields.OfficeFields.OfficeFax1].ToString().Trim();
                csvOffice.OfficeAnswering = physician[CsvFields.OfficeFields.Answering1].ToString().Trim();

                //string acceptNewPatientValue = physician[string.Format(AcceptNewPatientFormat, i)].ToString().Trim();
                //if (string.Equals(acceptNewPatientValue, "Referrable", StringComparison.CurrentCultureIgnoreCase))
                //{
                //    csvOffice.AcceptingNewPatients = "1";
                //}
                //else
                //{
                //    csvOffice.AcceptingNewPatients = "0";
                //}
                //if (i == 1)
                //{
                //    csvOffice.PrimaryOffice = "1";
                //}
                //else
                //{
                //    csvOffice.PrimaryOffice = "0";
                //}

                if (!csvOffice.IsEmpty)
                {
                    officeArray.Add(csvOffice);
                }
            
            formattedPhysician[CsvFields.Offices] = officeArray;


            formattedData.Rows.Add(formattedPhysician);
        }

        private DataTable CreateNewDataTable()
        {
            DataTable table = new DataTable();

            foreach (var field in typeof(CsvFields).GetFields())
            {
                var fieldName = field.GetValue(null);
                table.Columns.Add(fieldName.ToString(), typeof(object));
            }

            //foreach (var field in typeof(CsvFields.Education).GetFields())
            //{
            //    var fieldName = field.GetValue(null);
            //    table.Columns.Add(fieldName.ToString(), typeof(object));
            //}

            //foreach (var field in typeof(CsvFields.Internship).GetFields())
            //{
            //    var fieldName = field.GetValue(null);
            //    table.Columns.Add(fieldName.ToString(), typeof(object));
            //}

            //foreach (var field in typeof(CsvFields.Residency).GetFields())
            //{
            //    var fieldName = field.GetValue(null);
            //    table.Columns.Add(fieldName.ToString(), typeof(object));
            //}

            //foreach (var field in typeof(CsvFields.Fellowship).GetFields())
            //{
            //    var fieldName = field.GetValue(null);
            //    table.Columns.Add(fieldName.ToString(), typeof(object));
            //}

            return table;
        }

        public static IDataTableExtractor CreateNewWomansCsvExtractor()
        {
            return new WomansCsvExtractor();
        }

        public static IDataTableExtractor CreateNewWomansCsvExtractor(FileSystemInfo source)
        {
            return new WomansCsvExtractor(source);
        }
    }
}
