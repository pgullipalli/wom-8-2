﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MedTouch.PhysicianDirectory.Import.Extractors;
using womans.org.PhysicianDirectory.Import.Extractors.DTO;
using womans.org.PhysicianDirectory.Import.Extractors.Models;

namespace womans.org.PhysicianDirectory.Import.Extractors
{
    public class ProvidersExtractor : XmlExtractor<Provider>
    {
        private ProvidersExtractorSettings Settings { get; set; }

        private ProvidersExtractor(ProvidersExtractorSettings settings)
            : base(settings.ProviderSource)
        {
            SetSettings(settings);
        }

        private void SetSettings(ProvidersExtractorSettings settings)
        {
            AssertSettings(settings);
            Settings = settings;
        }

        private void AssertSettings(ProvidersExtractorSettings settings)
        {
            Assertor.ArgumentNotNull(settings.Affiliations, "settings.Affiliations");
            Assertor.ArgumentNotNull(settings.Languages, "settings.Languages");
            Assertor.ArgumentNotNull(settings.Practices, "settings.Practices");
            Assertor.ArgumentNotNull(settings.ProviderStatuses, "settings.ProviderStatuses");
            Assertor.ArgumentNotNull(settings.ProviderTypes, "settings.ProviderTypes");
            Assertor.ArgumentNotNull(settings.Specialties, "settings.Specialties");
        }

        protected override IEnumerable<Provider> ExtractModelInstances(XElement root)
        {
            List<Provider> providers = (from provider in GetDescendants(root, "Provider")
                                        let id = GetChildValue(provider, "ID")
                                        let lastModified = GetChildValue(provider, "lastModified")
                                        let isDeletedString = GetChildValue(provider, "IsDeleted")
                                        let isDeleted = string.Equals(isDeletedString, "Yes", StringComparison.CurrentCultureIgnoreCase)
                                        let deletedDate = GetChildValue(provider, "DeletedDate")
                                        let lastName = GetChildValue(provider, "LastName")
                                        let firstName = GetChildValue(provider, "FirstName")
                                        let middleName = GetChildValue(provider, "MiddleName")
                                        let generationalSuffix = GetChildValue(provider, "GenerationalSuffix")
                                        let medicalSuffixOrDegrees = GetChildValue(provider, "MedicalSuffixOrDegrees")
                                        let photoURL = GetChildValue(provider, "PhotoURL")
                                        let gender = GetChildValue(provider, "Gender")
                                        let npiNumber = GetChildValue(provider, "NPINumber")
                                        let bio = GetChildValue(provider, "Bio")
                                        let providerType = Find(Settings.ProviderTypes, pt => string.Equals(pt.ID, GetChildValue(GetChild(provider, "ProviderType"), "ID"), StringComparison.CurrentCultureIgnoreCase))
                                        let providerStatus = Find(Settings.ProviderStatuses, ps => string.Equals(ps.ID, GetChildValue(GetChild(provider, "ProviderStatus"), "ID"), StringComparison.CurrentCultureIgnoreCase))
                                        let displayPublicly = GetChildValue(provider, "DisplayPublicly")
                                        let displayInPDF = GetChildValue(provider, "DisplayInPDF")
                                        let showPhotoPublicly = GetChildValue(provider, "ShowPhotoPublicly")
                                        let acceptingNewPatients = GetChildValue(provider, "AcceptingNewPatients")
                                        let emails = GetModelInstances(GetChildren(provider, "Email"), CreateNewEmail)
                                        let languages = FindAll(Settings.Languages, l => GetChildrenValues(GetChildren(provider, "Language"), "ID").Contains(l.ID))
                                        let affiliations = FindAll(Settings.Affiliations, a => GetChildrenValues(GetChildren(provider, "Affiliation"), "NameID").Contains(a.ID))
                                        let specialties = FindAll(Settings.Specialties, l => GetChildrenValues(GetChildren(provider, "Specialty"), "ID").Contains(l.ID))
                                        let professionalOrAcademicTitles = GetModelInstances(GetChildren(provider, "ProfessionalOrAcademicTitle"), CreateNewProfessionalOrAcademicTitle)
                                        let customTrueFalses = GetModelInstances(GetChildren(provider, "CustomTrueFalse"), CreateNewCustomTrueFalse)
                                        let customFreeTexts = GetModelInstances(GetChildren(provider, "CustomFreeText"), CreateNewCustomFreeText)
                                        let practices = FindAll(Settings.Practices, l => GetChildrenValues(GetChildren(provider, "Practice"), "ID").Contains(l.ID))
                                        let publications = GetModelInstances(GetChildren(provider, "Publication"), CreateNewPublication)
                                        let videos = GetModelInstances(GetChildren(provider, "Video"), CreateNewVideo)
                                    where !string.IsNullOrWhiteSpace(id) 
                                    && string.Equals(displayPublicly, "Yes", StringComparison.CurrentCultureIgnoreCase)
                    select new Provider
                    {
                        ID = id,
                        LastModified = lastModified,
                        IsDeleted = isDeleted,
                        DeletedDate = deletedDate,
                        LastName = lastName,
                        FirstName = firstName,
                        MiddleName = middleName,
                        GenerationalSuffix = generationalSuffix,
                        MedicalSuffixOrDegrees = medicalSuffixOrDegrees,
                        PhotoURL = photoURL,
                        Gender = gender,
                        NPINumber = npiNumber,
                        Bio = bio,
                        ProviderType = providerType,
                        ProviderStatus = providerStatus,
                        DisplayPublicly = displayPublicly,
                        DisplayInPDF = displayInPDF,
                        ShowPhotoPublicly = showPhotoPublicly,
                        AcceptingNewPatients = acceptingNewPatients,
                        Emails = emails,
                        Languages = languages,
                        Affiliations = affiliations,
                        Specialties = specialties,
                        ProfessionalOrAcademicTitles = professionalOrAcademicTitles,
                        CustomTrueFalses = customTrueFalses,
                        CustomFreeTexts = customFreeTexts,
                        Practices = practices,
                        Publications = publications,
                        Videos = videos
                    }).ToList();

            return providers;
        }

        private IEnumerable<T> GetModelInstances<T>(IEnumerable<XElement> elements, Func<XElement, T> factoryMethod) where T : class
        {
            Assertor.ArgumentNotNull(factoryMethod, "factoryMethod");
            if (elements == null)
            {
                return new List<T>();
            }

            return (from element in elements
                    let instance = factoryMethod(element)
                    where instance != null
                    select instance).ToList();
        }
        
        private Email CreateNewEmail(XElement emailElement)
        {
            if (emailElement == null)
            {
                return null;
            }

            Email email = new Email
            {
                Type = GetChildValue(emailElement, "Type"),
                EmailAddress = GetChildValue(emailElement, "EmailAddress"),
                DisplayPublicly = GetChildValue(emailElement, "DisplayPublicly")
            };

            bool hasValue = !string.IsNullOrWhiteSpace(email.Type)
                            || !string.IsNullOrWhiteSpace(email.EmailAddress)
                            || !string.IsNullOrWhiteSpace(email.DisplayPublicly);

            if (hasValue)
            {
                return email;
            }

            return null;
        }

        private CustomFreeText CreateNewCustomFreeText(XElement customFreeTextElement)
        {
            if (customFreeTextElement == null)
            {
                return null;
            }

            CustomFreeText customFreeText = new CustomFreeText
            {
	            TypeID = GetChildValue(customFreeTextElement, "TypeID"),
	            Type = GetChildValue(customFreeTextElement, "Type"),
	            Value = GetChildValue(customFreeTextElement, "Value"),
            };

            bool hasValue = !string.IsNullOrWhiteSpace(customFreeText.TypeID)
                            || !string.IsNullOrWhiteSpace(customFreeText.Type)
                            || !string.IsNullOrWhiteSpace(customFreeText.Value);

            if (hasValue)
            {
                return customFreeText;
            }

            return null;
        }

        private CustomTrueFalse CreateNewCustomTrueFalse(XElement customTrueFalseElement)
        {
	        if (customTrueFalseElement == null)
	        {
		        return null;
	        }

	        CustomTrueFalse customTrueFalse = new CustomTrueFalse
	        {
		        TypeID = GetChildValue(customTrueFalseElement, "TypeID"),
		        Type = GetChildValue(customTrueFalseElement, "Type"),
		        Value = GetChildValue(customTrueFalseElement, "Value"),
	        };

	        bool hasValue = !string.IsNullOrWhiteSpace(customTrueFalse.TypeID)
					        || !string.IsNullOrWhiteSpace(customTrueFalse.Type)
					        || !string.IsNullOrWhiteSpace(customTrueFalse.Value);

	        if (hasValue)
	        {
		        return customTrueFalse;
	        }

	        return null;
        }

        private ProfessionalOrAcademicTitle CreateNewProfessionalOrAcademicTitle(XElement professionalOrAcademicTitleElement)
        {
            if (professionalOrAcademicTitleElement == null)
            {
                return null;
            }

            ProfessionalOrAcademicTitle professionalOrAcademicTitle = new ProfessionalOrAcademicTitle
            {
                Name = GetChildValue(professionalOrAcademicTitleElement, "Name"),
                SortOrder = GetChildValue(professionalOrAcademicTitleElement, "SortOrder"),
            };

            bool hasValue = !string.IsNullOrWhiteSpace(professionalOrAcademicTitle.Name)
                            || !string.IsNullOrWhiteSpace(professionalOrAcademicTitle.SortOrder);

            if (hasValue)
            {
                return professionalOrAcademicTitle;
            }

            return null;
        }

        private Publication CreateNewPublication(XElement publicationElement)
        {
            if (publicationElement == null)
            {
                return null;
            }

            string sortOrderString = GetChildValue(publicationElement, "SortOrder");
            int sortOrder = 0;

            int.TryParse(GetChildValue(publicationElement, "SortOrder"), out sortOrder);
            
            Publication publication = new Publication
            {
                FullCitation = GetChildValue(publicationElement, "FullCitation"),
                IsPeerReviewed = GetChildValue(publicationElement, "IsPeerReviewed"),
                SortOrder = sortOrder,
                Type = GetChildValue(publicationElement, "Type")
            };

            bool hasValue = !string.IsNullOrWhiteSpace(publication.FullCitation)
                            || !string.IsNullOrWhiteSpace(publication.IsPeerReviewed)
                            || !string.IsNullOrWhiteSpace(sortOrderString)
                            || !string.IsNullOrWhiteSpace(publication.Type);

            if (hasValue)
            {
                return publication;
            }

            return null;
        }

        private Video CreateNewVideo(XElement videoElement)
        {
            if (videoElement == null)
            {
                return null;
            }

            Video video = new Video
            {
                Name = GetChildValue(videoElement, "Name"),
                URL = GetChildValue(videoElement, "URL"),
                DisplayPublicly = GetChildValue(videoElement, "DisplayPublicly"),
                VideoSource = GetChildValue(videoElement, "VideoSource"),
                OriginalCode = GetChildValue(videoElement, "OriginalCode")
            };

            bool hasValue = !string.IsNullOrWhiteSpace(video.Name)
                            || !string.IsNullOrWhiteSpace(video.URL)
                            || !string.IsNullOrWhiteSpace(video.DisplayPublicly)
                            || !string.IsNullOrWhiteSpace(video.VideoSource)
                            || !string.IsNullOrWhiteSpace(video.OriginalCode);

            if (hasValue)
            {
                return video;
            }

            return null;
        }

        private T Find<T>(IEnumerable<T> instances, Func<T, bool>  predicate) where T : class
        {
            return FindAll(instances, predicate).FirstOrDefault();
        }

        private IEnumerable<T> FindAll<T>(IEnumerable<T> instances, Func<T, bool> predicate) where T : class
        {
            if (instances == null)
            {
                return new List<T>();
            }

            return instances.Where(instance => predicate(instance)).ToList();
        }

        public static XmlExtractor<Provider> CreateNewProvidersExtractor(ProvidersExtractorSettings settings)
        {
            return new ProvidersExtractor(settings);
        }
    }
}
