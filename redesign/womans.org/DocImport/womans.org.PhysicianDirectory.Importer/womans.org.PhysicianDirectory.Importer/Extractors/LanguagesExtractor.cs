﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MedTouch.PhysicianDirectory.Import.Extractors;
using womans.org.PhysicianDirectory.Import.Extractors.Models;

namespace womans.org.PhysicianDirectory.Import.Extractors
{
    public class LanguagesExtractor : XmlExtractor<Language>
    {
        private LanguagesExtractor()
            : base()
        {
        }

        private LanguagesExtractor(string source)
            : base(source)
        {
        }

        protected override IEnumerable<Language> ExtractModelInstances(XElement root)
        {
            return (from language in GetDescendants(root, "Language")
                    let id = GetChildValue(language, "ID")
                    let name = GetChildValue(language, "Name")
                    let providerCount = GetChildValue(language, "ProviderCount")
                    where !string.IsNullOrWhiteSpace(id)
                           && !string.IsNullOrWhiteSpace(name)
                           && !string.IsNullOrWhiteSpace(providerCount)
                    select new Language
                    {
                        ID = id,
                        Name = name,
                        ProviderCount = providerCount
                    }).ToList();
        }

        public static XmlExtractor<Language> CreateNewLanguagesExtractor()
        {
            return new LanguagesExtractor();
        }

        public static XmlExtractor<Language> CreateNewLanguagesExtractor(string source)
        {
            return new LanguagesExtractor(source);
        }
    }
}
