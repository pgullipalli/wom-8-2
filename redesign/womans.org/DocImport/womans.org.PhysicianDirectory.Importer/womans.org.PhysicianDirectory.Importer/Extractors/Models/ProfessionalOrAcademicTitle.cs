﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class ProfessionalOrAcademicTitle
    {
        public string Name { get; set; }

        public string SortOrder { get; set; }
    }
}
