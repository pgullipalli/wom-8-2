﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class CustomTrueFalse
    {
        public string TypeID { get; set; }

        public string Type { get; set; }

        public string Value { get; set; }
    }
}
