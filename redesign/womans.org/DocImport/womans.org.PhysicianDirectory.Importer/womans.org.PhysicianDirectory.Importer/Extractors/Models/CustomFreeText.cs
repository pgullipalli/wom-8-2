﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class CustomFreeText
    {
        public string TypeID { get; set; }

		public string Type { get; set; }

		public string Value { get; set; }
    }
}
