﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class Provider
    {
        public string ID { get; set; }

        public string LastModified { get; set; }

        public bool IsDeleted { get; set; }

        public string DeletedDate { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string FirstName { get; set; }

        public string GenerationalSuffix { get; set; }

        public string MedicalSuffixOrDegrees { get; set; }
        
        public string PhotoURL { get; set; }

        public string Gender { get; set; }

        public string NPINumber { get; set; }

        public string Bio { get; set; }

        public ProviderType ProviderType { get; set; }

        public ProviderStatus ProviderStatus { get; set; }

        public string DisplayPublicly { get; set; }

        public string DisplayInPDF { get; set; }

        public string ShowPhotoPublicly { get; set; }

        public string AcceptingNewPatients { get; set; }

        public IEnumerable<Email> Emails { get; set; }

        public IEnumerable<Affiliation> Affiliations { get; set; }

        public IEnumerable<Language> Languages { get; set; }

        public IEnumerable<Specialty> Specialties { get; set; }

        public IEnumerable<ProfessionalOrAcademicTitle> ProfessionalOrAcademicTitles { get; set; }

        public IEnumerable<CustomTrueFalse> CustomTrueFalses { get; set; }

        public IEnumerable<CustomFreeText> CustomFreeTexts { get; set; }

        public IEnumerable<Practice> Practices { get; set; }

        public IEnumerable<Publication> Publications { get; set; }

        public IEnumerable<Video> Videos { get; set; }
    }
}
