﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models.Maps
{
    public class AffilationsToLocationsMap : IMap<string, string>
    {
        private static object lockObject = new object();

        private static volatile IMap<string, string> _Current;
        public static IMap<string, string> Current
        {
            get
            {
                if (_Current == null)
                {
                    lock (lockObject)
                    {
                        if (_Current == null)
                        {
                            _Current = new AffilationsToLocationsMap();
                        }
                    }
                }

                return _Current;
            }
        }

        private IDictionary<string, string> AffilationsToLocationsLookup { get; set; }

        private AffilationsToLocationsMap()
        {
            AffilationsToLocationsLookup = CreateNewLookup();
        }

        public bool HasKey(string affiliationName)
        {
            return AffilationsToLocationsLookup.ContainsKey(affiliationName);
        }

        public string this[string affiliationName]
        {
            get
            {
                if (!HasKey(affiliationName))
                {
                    return null;
                }
                
                return AffilationsToLocationsLookup[affiliationName];
            }
        }

        private static IDictionary<string, string> CreateNewLookup()
        {
            IDictionary<string, string> lookup = new Dictionary<string, string>();
            lookup.Add("Providence Hood River Memorial Hospital", "{A1430077-89CF-4505-9A0A-BEC94D95EC3E}");
            lookup.Add("Providence Medford Medical Center", "{177926A3-0EA3-4BC9-90CC-81361796E0E0}");
            lookup.Add("Providence Milwaukie Hospital", "{FE342B5C-F980-4DB0-A401-C48A826424A1}");
            lookup.Add("Providence Newberg Medical Center", "{F8035608-8FE6-4142-993A-DE141FC16861}");
            lookup.Add("Providence Portland Medical Center", "{7722DD72-786F-4BAE-A44B-15C08F89607B}");
            lookup.Add("Providence Seaside Hospital", "{D36C610F-02D5-4481-965C-1C52B18A0920}");
            lookup.Add("Providence St. Vincent Medical Center", "{53517D86-8184-41D4-92CA-ABDA2B43DC96}");
            lookup.Add("Providence Willamette Falls Medical Center", "{0920AC62-276A-4208-AD07-39E93EAE7CA1}");
            return lookup;
        }
    }
}
