﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models.Maps
{
    public interface IMap<TKey, TValue>
    {
        bool HasKey(TKey key);

        TValue this[TKey key] { get; }
    }
}
