﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class CsvSpecialty : IComparable<CsvSpecialty>
    {
        public string SpecialtyName { get; set; }

        public bool BoardCertified { get; set; }

        public int CompareTo(CsvSpecialty other)
        {
            return this.BoardCertified.CompareTo(other.BoardCertified);
        }
    }
}
