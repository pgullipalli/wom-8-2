﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Linq;

using MedTouch.PhysicianDirectory.Import;
using MedTouch.PhysicianDirectory.Import.Utilities.Validation;

namespace womans.org.PhysicianDirectory.Import.Extractors.Utilities
{
    public class XmlOperations : IXmlOperations
    {
        private static readonly IAssertor Assertor = MedTouch.PhysicianDirectory.Import.Utilities.Validation.Assertor.CreateNewAssertor();

        public XElement Source { get; set; }

        protected XmlOperations()
        {
        }

        protected XmlOperations(XElement source)
        {
            Source = source;
        }

        public string GetValue()
        {
            if (Source == null)
            {
                return string.Empty;
            }

            return Source.Value;
        }

        public void SaveXml(string filePath)
        {
            if (Source == null)
            {
                return;
            }    

            Source.Save(filePath);
        }

        public string GetAttributeValue(string attributeName)
        {
            Assertor.ArgumentNotNullOrEmpty(attributeName, "attributeName");
            if (Source == null)
            {
                return string.Empty;
            }

            XAttribute attribute = Source.Attribute(attributeName);
            if (attribute == null)
            {
                return string.Empty;
            }

            return attribute.Value;
        }
        
        public string GetChildValue(string childName)
        {
            Assertor.ArgumentNotNullOrEmpty(childName, "childName");
            if (Source == null)
            {
                return string.Empty;
            }

            XElement childElement = GetChild(childName);
            if (childElement == null)
            {
                return string.Empty;
            }
            return childElement.Value;
        }

        public XElement GetChild(string childName)
        {
            Assertor.ArgumentNotNullOrEmpty(childName, "childName");
            if (Source == null)
            {
                return null;
            }

            XElement child = GetChildren(childName).FirstOrDefault();
            if (child == null)
            {
                return null;
            }

            return child;
        }

        public IEnumerable<string> GetChildrenValues(string childName)
        {
            Assertor.ArgumentNotNullOrEmpty(childName, "childName");
            if (Source == null)
            {
                return new List<string>();
            }


            return (from child in GetChildren(childName)
                    where !string.IsNullOrWhiteSpace(child.Value)
                    select child.Value.Trim()).ToList();
        }

        public IEnumerable<string> GetChildrenValues(IEnumerable<XElement> elements, string childName)
        {
            Assertor.ArgumentNotNullOrEmpty(childName, "childName");
            if (elements == null)
            {
                return new List<string>();
            }

            List<string> values = new List<string>();

            foreach (XElement element in elements)
            {
                List<string> childrenValues = (from child in GetChildren(element, childName)
                                               where !string.IsNullOrWhiteSpace(child.Value)
                                               select child.Value.Trim()).ToList();
    
                if (childrenValues.Any())
                {
                    values.AddRange(childrenValues);
                }
            }

            return values.Distinct();
        }

        public IEnumerable<XElement> GetChildren(string childName)
        {
            return GetChildren(Source, childName);
        }

        private static IEnumerable<XElement> GetChildren(XElement element, string childName)
        {
            Assertor.ArgumentNotNullOrEmpty(childName, "childName");
            if (element == null)
            {
                return new List<XElement>();
            }

            return element.Elements(childName);
        }

        public string GetDescendantValue(string descendantName)
        {
            return GetDescendantsValue(descendantName).FirstOrDefault();
        }

        public IEnumerable<string> GetDescendantsValue(string descendantName)
        {
            Assertor.ArgumentNotNullOrEmpty(descendantName, "descendantName");
            if (Source == null)
            {
                return new List<string>();
            }

            return (from descendant in GetDescendants(descendantName)
                     where !string.IsNullOrWhiteSpace(descendant.Value)
                     select descendant.Value.Trim()).ToList();
        }

        public IEnumerable<XElement> GetDescendants(string descendantName)
        {
            Assertor.ArgumentNotNullOrEmpty(descendantName, "descendantName");
            if (Source == null)
            {
                return new List<XElement>();
            }

            return Source.Descendants(descendantName).ToList();
        }

        public static IXmlOperations CreateNewXmlOperations()
        {
            return new XmlOperations();
        }

        public static IXmlOperations CreateNewXmlOperations(XElement source)
        {
            return new XmlOperations(source);
        }
    }
}
