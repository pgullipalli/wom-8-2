﻿using System.Collections.Generic;
using System.Xml.Linq;
using MedTouch.PhysicianDirectory.Import;

namespace womans.org.PhysicianDirectory.Import.Extractors.Utilities
{
    public interface IXmlOperations : ISource<XElement>
    {
        void SaveXml(string filePath);

        string GetValue();

        string GetAttributeValue(string attributeName);

        string GetChildValue(string childName);

        XElement GetChild(string childName);

        IEnumerable<string> GetChildrenValues(string childName);

        IEnumerable<string> GetChildrenValues(IEnumerable<XElement> elements, string childName);

        IEnumerable<XElement> GetChildren(string childName);

        string GetDescendantValue(string descendantName);

        IEnumerable<string> GetDescendantsValue(string descendantName);

        IEnumerable<XElement> GetDescendants(string descendantName);
    }
}
