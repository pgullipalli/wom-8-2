﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MedTouch.PhysicianDirectory.Import.Utilities.Serialization.Entities;
using womans.org.PhysicianDirectory.Import.Utilities.Serialization.Entities;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class ProviderType
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public string ProviderCount { get; set; }

        public bool HasData()
        {
            return !string.IsNullOrWhiteSpace(ID)
                    || !string.IsNullOrWhiteSpace(Name)
                    || !string.IsNullOrWhiteSpace(ProviderCount);
        }
    }
}
