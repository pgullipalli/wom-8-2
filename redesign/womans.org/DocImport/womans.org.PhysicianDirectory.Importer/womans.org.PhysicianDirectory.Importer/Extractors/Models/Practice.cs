﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class Practice
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public IEnumerable<ContactNumber> ContactNumbers { get; set; }

        public Address Address { get; set; }
    }
}

