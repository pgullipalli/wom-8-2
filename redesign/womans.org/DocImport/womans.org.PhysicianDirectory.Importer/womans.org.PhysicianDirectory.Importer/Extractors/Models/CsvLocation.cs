﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class CsvLocation
    {
        public string LocationName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public bool IsEmpty
        {
            get
            {
                bool result = false;
                
                if (string.IsNullOrWhiteSpace(this.Address1) &&
                    string.IsNullOrWhiteSpace(this.Address2) &&
                    string.IsNullOrWhiteSpace(this.City) &&
                    string.IsNullOrWhiteSpace(this.State) && 
                    string.IsNullOrWhiteSpace(this.Zip))
                {
                    result = true;
                }

                return result;
            }
        }
    }
}
