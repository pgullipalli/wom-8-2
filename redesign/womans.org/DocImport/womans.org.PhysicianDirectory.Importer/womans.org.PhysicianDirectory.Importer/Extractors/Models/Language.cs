﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class Language
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public string ProviderCount { get; set; }
    }
}
