﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class ContactNumber
    {
        public string Type { get; set; }

        public string AreaCode { get; set; }

        public string Number { get; set; }

        public string FormattedAsUSStandard { get; set; }

        public string DisplayPublicly { get; set; }
    }
}
