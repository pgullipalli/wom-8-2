﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class Email
    {
        public string Type { get; set; }

        public string EmailAddress { get; set; }

        public string DisplayPublicly { get; set; }
    }
}
