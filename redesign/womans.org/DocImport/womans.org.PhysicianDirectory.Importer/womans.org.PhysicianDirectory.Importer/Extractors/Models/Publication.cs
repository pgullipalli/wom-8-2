﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class Publication
    {
        public string FullCitation { get; set; }

        public string IsPeerReviewed { get; set; }

        public int SortOrder { get; set; }

        public string Type { get; set; }
    }
}
