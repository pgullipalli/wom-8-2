﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class CsvOffice
    {
        public CsvLocation Location { get; set; }

        public string OfficeName { get; set; }

        public string OfficePhone { get; set; }

        public string OfficeFax { get; set; }

        public string OfficeAnswering { get; set; }

        public string AcceptingNewPatients { get; set; }

        public string PrimaryOffice { get; set; }

        public bool IsEmpty
        {
            get
            {
                bool result = false;

                if (this.Location.IsEmpty &&
                    string.IsNullOrWhiteSpace(this.OfficePhone) &&
                    string.IsNullOrWhiteSpace(this.OfficeFax))
                {
                    result = true;
                }

                return result;
            }
        }
    }
}
