﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class Address
    {
        public string Line1 { get; set; }
        
        public string Line2 { get; set; }
        
        public string City { get; set; }
        
        public string State { get; set; }
        
        public string Zip { get; set; }
        
        public string Latitude { get; set; }
        
        public string Longitude { get; set; }
        
        public string Type { get; set; }

        public string OnPublicTransportation { get; set; }

        public string HandicapAccessible { get; set; }

        public string HasWeekendHours { get; set; }

        public string HasEveningHours { get; set; }
    }
}
