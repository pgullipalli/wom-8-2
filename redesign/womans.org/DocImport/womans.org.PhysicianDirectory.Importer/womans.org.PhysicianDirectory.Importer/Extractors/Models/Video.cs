﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors.Models
{
    public class Video
    {
        public string Name { get; set; }

        public string URL { get; set; }

        public string DisplayPublicly { get; set; }

        public string VideoSource { get; set; }

        public string OriginalCode { get; set; }
    }
}
