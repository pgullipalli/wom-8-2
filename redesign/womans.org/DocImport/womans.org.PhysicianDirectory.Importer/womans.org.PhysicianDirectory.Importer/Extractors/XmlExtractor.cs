﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MedTouch.PhysicianDirectory.Import.Extractors;
using MedTouch.PhysicianDirectory.Import.Utilities.Validation;
using womans.org.PhysicianDirectory.Import.Extractors.Utilities;

namespace womans.org.PhysicianDirectory.Import.Extractors
{
    public abstract class XmlExtractor<TResult> : IExtractor<string, IEnumerable<TResult>>, IFileSaver
    {
        protected static readonly IAssertor Assertor = MedTouch.PhysicianDirectory.Import.Utilities.Validation.Assertor.CreateNewAssertor();

        protected IXmlOperations XmlOperations { get; set; }

        private string _Source;
        public string Source
        {
            get
            {
                return _Source;
            }
            set
            {
                SetSource(value);
            }
        }


        private XDocument _XmlDocument;
        private XDocument XmlDocument
        {
            get
            {
                if (_XmlDocument == null && !string.IsNullOrWhiteSpace(Source))
                {
                    _XmlDocument = XDocument.Load(Source);
                }

                return _XmlDocument;
            }
        }

        protected XmlExtractor()
            : this(Utilities.XmlOperations.CreateNewXmlOperations())
        {
        }

        protected XmlExtractor(string source)
            : this(Utilities.XmlOperations.CreateNewXmlOperations(), source)
        {
        }

        protected XmlExtractor(IXmlOperations xmlOperations)
        {
            SetXmlOperations(xmlOperations);
        }

        protected XmlExtractor(IXmlOperations xmlOperations, string source)
        {
            SetXmlOperations(xmlOperations);
            Source = source;
        }

        protected virtual void SetSource(string source)
        {
            Assertor.ArgumentNotNullOrEmpty(source, "Source");
            _Source = source;
        }

        protected virtual void SetXmlOperations(IXmlOperations xmlOperations)
        {
            Assertor.ArgumentNotNull(xmlOperations, "xmlOperations");
            XmlOperations = xmlOperations;
        }

        public virtual IEnumerable<TResult> Extract()
        {
            Assertor.ArgumentNotNullOrEmpty(Source, "Source");
            Assertor.IsNotNull(XmlDocument, "XmlDocument cannot be null!");
            Assertor.IsNotNull(XmlDocument.Root, "XmlDocument.Root cannot be null!");
            return ExtractModelInstances(XmlDocument.Root);
        }

        protected abstract IEnumerable<TResult> ExtractModelInstances(XElement root);

        protected string GetValue(XElement element)
        {
            XmlOperations.Source = element;
            return XmlOperations.GetValue();
        }

        protected string GetAttributeValue(XElement element, string attributeName)
        {
            XmlOperations.Source = element;
            return XmlOperations.GetAttributeValue(attributeName);
        }

        protected string GetChildValue(XElement element, string childName)
        {
            XmlOperations.Source = element;
            return XmlOperations.GetChildValue(childName);
        }

        protected XElement GetChild(XElement element, string childName)
        {
            XmlOperations.Source = element;
            return XmlOperations.GetChild(childName);
        }

        protected IEnumerable<string> GetChildrenValues(IEnumerable<XElement> elements, string childName)
        {
            return XmlOperations.GetChildrenValues(elements, childName);
        }

        protected IEnumerable<string> GetChildrenValues(XElement element, string childName)
        {
            XmlOperations.Source = element;
            return XmlOperations.GetChildrenValues(childName);
        }

        protected IEnumerable<XElement> GetChildren(XElement element, string childName)
        {
            XmlOperations.Source = element;
            return XmlOperations.GetChildren(childName);
        }

        protected string GetDescendantValue(XElement element, string descendantName)
        {
            XmlOperations.Source = element;
            return XmlOperations.GetDescendantValue(descendantName);
        }

        protected IEnumerable<string> GetDescendantsValue(XElement element, string descendantName)
        {
            XmlOperations.Source = element;
            return XmlOperations.GetDescendantsValue(descendantName);
        }

        protected IEnumerable<XElement> GetDescendants(XElement element, string descendantName)
        {
            XmlOperations.Source = element;
            return XmlOperations.GetDescendants(descendantName);
        }

        public void Save(string filePath)
        {
            XmlOperations.Source = XmlDocument.Root;
            XmlOperations.SaveXml(filePath);
        }
    }
}
