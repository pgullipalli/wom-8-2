﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using LumenWorks.Framework.IO.Csv;
using MedTouch.PhysicianDirectory.Import.Utilities.Validation;
using womans.org.PhysicianDirectory.Import.Extractors;

namespace MedTouch.PhysicianDirectory.Import.Extractors
{
    public abstract class CsvExtractor : IDataTableExtractor
    {
        protected static readonly IAssertor Assertor = MedTouch.PhysicianDirectory.Import.Utilities.Validation.Assertor.CreateNewAssertor();

        private FileSystemInfo _Source;
        public FileSystemInfo Source
        {
            get
            {
                return _Source;
            }
            set
            {
                AssertSource(value);
                _Source = value;
            }
        }

        protected CsvExtractor()
        {
        }

        protected CsvExtractor(FileSystemInfo source)
        {
            SetSource(source);
        }

        protected virtual void SetSource(FileSystemInfo source)
        {
            Source = source;
        }

        protected virtual void AssertSource(FileSystemInfo source)
        {
            Assertor.ArgumentNotNull(source, "Source");
            Assertor.ArgumentNotNullOrEmpty(source.FullName, "Source.FullName");
        }

        public virtual DataTable Extract()
        {
            DataTable dataTable = CreateNewDataTable();
            using (var csv = new CsvReader(new StreamReader(Source.FullName), true))
            {
                var fieldCount = csv.FieldCount;
                var headers = csv.GetFieldHeaders();
                while (csv.ReadNextRecord())
                {
                    DataRow dataRow = dataTable.NewRow();

                    for (var i = 0; i < fieldCount; i++)
                    {
                        if (dataRow.Table.Columns.Contains(headers[i]))
                        {
                            dataRow[headers[i]] = csv[i];
                        }
                    }

                    dataTable.Rows.Add(dataRow);
                }
            }

            return dataTable;
        }

        private DataTable CreateNewDataTable()
        {
            DataTable table = new DataTable();

            foreach (var field in typeof(CsvFields).GetFields())
            {
                var fieldName = field.GetValue(null);
                table.Columns.Add(fieldName.ToString(), typeof(object));
            }

            foreach (var field in typeof(CsvFields.CertificationFields).GetFields())
            {
                var fieldName = field.GetValue(null);
                table.Columns.Add(fieldName.ToString(), typeof(object));
            }

            foreach (var field in typeof(CsvFields.OfficeFields).GetFields())
            {
                var fieldName = field.GetValue(null);
                table.Columns.Add(fieldName.ToString(), typeof(object));
            }

            return table;
        }
    }
}
