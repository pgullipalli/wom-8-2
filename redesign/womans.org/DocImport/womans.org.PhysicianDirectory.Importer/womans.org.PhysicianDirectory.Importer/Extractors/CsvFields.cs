﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace womans.org.PhysicianDirectory.Import.Extractors
{
    public static class CsvFields
    {

        public const string FirstName = "firstname";

        public const string MiddleInitial = "middle";

        public const string LastName = "lastname";

        public const string DegreeTitles = "degree";

        public const string LanguagesSpoken = "languages";

        public const string InternalHospitalID = "Med Prof";

        public const string Website = "OfficeWebSite";

        public const string Specialties = "specialty";

        public const string Offices = "Offices";

        public const string Certifications = "Certifications";

        public static class CertificationFields
        {
            public const string Certifications1 = "boardcert1";

            public const string Certifications2 = "boardcert2";

            public const string Certifications3 = "boeardcert3";
        }

        public static class OfficeFields
        {
            /// OfficeName#
            public const string OfficeName1 = "officename";

            /// Address#.1
            public const string Address11 = "address1";

            /// Address#.2
            public const string Address12 = "address2";

            /// City#
            public const string City1 = "citya";

            /// State#
            public const string State1 = "statea";

            /// Zip#
            public const string Zip1 = "zipa";

            /// OfficePhone#
            public const string OfficePhone1 = "phone";

            /// OfficeFax#
            public const string OfficeFax1 = "fax";

            /// Answering#
            public const string Answering1 = "answering";
        }
    }
}
