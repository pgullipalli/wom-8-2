﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MedTouch.PhysicianDirectory.Import.Extractors;
using womans.org.PhysicianDirectory.Import.Extractors.Models;

namespace womans.org.PhysicianDirectory.Import.Extractors
{
    public class ProviderStatusesExtractor : XmlExtractor<ProviderStatus>
    {
        private ProviderStatusesExtractor() 
            : base()
        {
        }

        private ProviderStatusesExtractor(string source)
            : base(source)
        {
        }

        protected override IEnumerable<ProviderStatus> ExtractModelInstances(XElement root)
        {
            return (from providerStatus in GetDescendants(root, "ProviderStatus")
                        let id = GetChildValue(providerStatus, "ID")
                        let name = GetChildValue(providerStatus, "Name")
                        let providerCount = GetChildValue(providerStatus, "ProviderCount")
                    where !string.IsNullOrWhiteSpace(id)
                           && !string.IsNullOrWhiteSpace(name)
                           && !string.IsNullOrWhiteSpace(providerCount)
                    select new ProviderStatus
                     {
                         ID = id,
                         Name = name,
                         ProviderCount = providerCount
                     }).ToList();
        }

        public static XmlExtractor<ProviderStatus> CreateNewProviderStatusesExtractor()
        {
            return new ProviderStatusesExtractor();
        }

        public static XmlExtractor<ProviderStatus> CreateNewProviderStatusesExtractor(string source)
        {
            return new ProviderStatusesExtractor(source);
        }
    }
}
