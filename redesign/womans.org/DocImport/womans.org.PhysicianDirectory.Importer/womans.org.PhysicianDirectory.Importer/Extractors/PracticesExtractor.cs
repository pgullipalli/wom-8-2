﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MedTouch.PhysicianDirectory.Import.Extractors;
using womans.org.PhysicianDirectory.Import.Extractors.Models;

namespace womans.org.PhysicianDirectory.Import.Extractors
{
    public class PracticesExtractor : XmlExtractor<Practice>
    {
        private PracticesExtractor()
            : base()
        {
        }

        private PracticesExtractor(string source)
            : base(source)
        {
        }

        protected override IEnumerable<Practice> ExtractModelInstances(XElement root)
        {
            return (from practice in GetDescendants(root, "Practice")
                    let id = GetChildValue(practice, "ID")
                    let name = GetChildValue(practice, "Name")
                    let type = GetChildValue(practice, "Type")
                    let contactNumbers = GetContactNumbers(GetChildren(practice, "ContactNumber"))
                    let address = GetAddress(GetChild(practice, "Address"))
                    where !string.IsNullOrWhiteSpace(id)
                    select new Practice
                     {
                         ID = id,
                         Name = name,
                         Type = type,
                         ContactNumbers = contactNumbers,
                         Address = address
                     }).ToList();
        }

        private IEnumerable<ContactNumber> GetContactNumbers(IEnumerable<XElement> contactNumberElements)
        {
            if (contactNumberElements == null || !contactNumberElements.Any())
            {
                return new List<ContactNumber>();
            }

            return (from contactNumberElement in contactNumberElements
                    let contactNumber = GetContactNumber(contactNumberElement)
                    where contactNumber != null
                    select contactNumber).ToList();
        }

        private ContactNumber GetContactNumber(XElement contactNumberElement)
        {
            if (contactNumberElement == null)
            {
                return null;
            }

            ContactNumber contactNumber = new ContactNumber
            {
                Type = GetChildValue(contactNumberElement, "Type"),
                AreaCode = GetChildValue(contactNumberElement, "AreaCode"),
                Number = GetChildValue(contactNumberElement, "Number"),
                FormattedAsUSStandard = GetChildValue(contactNumberElement, "FormattedAsUSStandard"),
                DisplayPublicly = GetChildValue(contactNumberElement, "DisplayPublicly")
            };

            bool hasValue = !string.IsNullOrWhiteSpace(contactNumber.Type)
                                || !string.IsNullOrWhiteSpace(contactNumber.AreaCode)
                                || !string.IsNullOrWhiteSpace(contactNumber.Number)
                                || !string.IsNullOrWhiteSpace(contactNumber.FormattedAsUSStandard)
                                || !string.IsNullOrWhiteSpace(contactNumber.DisplayPublicly);

            if (hasValue)
            {
                return contactNumber;
            }

            return null;
        }

        private Address GetAddress(XElement addressElement)
        {
            Address address = new Address
            {
                Line1 = GetChildValue(addressElement, "Line1"),
                Line2 = GetChildValue(addressElement, "Line2"),
                City = GetChildValue(addressElement, "City"),
                State = GetChildValue(addressElement, "State"),
                Zip = GetChildValue(addressElement, "Zip"),
                Latitude = GetChildValue(addressElement, "Latitude"),
                Longitude = GetChildValue(addressElement, "Longitude"),
                Type = GetChildValue(addressElement, "Type"),
                OnPublicTransportation = GetChildValue(addressElement, "OnPublicTransportation"),
                HandicapAccessible = GetChildValue(addressElement, "HandicapAccessible"),
                HasWeekendHours = GetChildValue(addressElement, "HasWeekendHours"),
                HasEveningHours = GetChildValue(addressElement, "HasEveningHours")
            };

            bool hasValue = !string.IsNullOrWhiteSpace(address.Line1)
                                || !string.IsNullOrWhiteSpace(address.Line2)
                                || !string.IsNullOrWhiteSpace(address.City)
                                || !string.IsNullOrWhiteSpace(address.State)
                                || !string.IsNullOrWhiteSpace(address.Zip)
                                || !string.IsNullOrWhiteSpace(address.Latitude)
                                || !string.IsNullOrWhiteSpace(address.Longitude)
                                || !string.IsNullOrWhiteSpace(address.Type)
                                || !string.IsNullOrWhiteSpace(address.OnPublicTransportation)
                                || !string.IsNullOrWhiteSpace(address.HandicapAccessible)
                                || !string.IsNullOrWhiteSpace(address.HasWeekendHours)
                                || !string.IsNullOrWhiteSpace(address.HasEveningHours);

            if (hasValue)
            {
                return address;
            }

            return null;
        }

        public static XmlExtractor<Practice> CreateNewPracticesExtractor()
        {
            return new PracticesExtractor();
        }

        public static XmlExtractor<Practice> CreateNewPracticesExtractor(string source)
        {
            return new PracticesExtractor(source);
        }
    }
}
