﻿namespace MedTouch.PhysicianDirectory.Import.Extractors
{
    public interface IExtractor<TSource, TResult> : ISource<TSource>
    {
        TResult Extract();
    }
}
