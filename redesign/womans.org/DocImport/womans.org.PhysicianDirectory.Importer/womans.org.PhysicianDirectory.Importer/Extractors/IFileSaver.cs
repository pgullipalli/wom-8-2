﻿namespace MedTouch.PhysicianDirectory.Import.Extractors
{
    public interface IFileSaver
    {
        void Save(string filePath);
    }
}
