﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MedTouch.PhysicianDirectory.Import.Extractors;
using womans.org.PhysicianDirectory.Import.Extractors.Models;

namespace womans.org.PhysicianDirectory.Import.Extractors
{
    public class SpecialtiesExtractor : XmlExtractor<Specialty>
    {
        private SpecialtiesExtractor()
            : base()
        {
        }

        private SpecialtiesExtractor(string source)
            : base(source)
        {
        }

        protected override IEnumerable<Specialty> ExtractModelInstances(XElement root)
        {
            return (from specialty in GetDescendants(root, "Specialty")
				    let id = GetChildValue(specialty, "ID")
                    let name = GetChildValue(specialty, "Name")
                    let includeInPublicSearches = GetChildValue(specialty, "IncludeInPublicSearches")
                    let isHospitalBased = GetChildValue(specialty, "IsHospitalBased")
                    let providerCount = GetChildValue(specialty, "ProviderCount")
			        where !string.IsNullOrWhiteSpace(id)
				           && !string.IsNullOrWhiteSpace(name)
				           && !string.IsNullOrWhiteSpace(providerCount)
			        select new Specialty
			         {
				         ID = id,
                         Name = name,
                         IncludeInPublicSearches = includeInPublicSearches,
                         IsHospitalBased = isHospitalBased,
				         ProviderCount = providerCount
			         }).ToList();
        }

        public static XmlExtractor<Specialty> CreateNewSpecialtiesExtractor()
        {
            return new SpecialtiesExtractor();
        }

        public static XmlExtractor<Specialty> CreateNewSpecialtiesExtractor(string source)
        {
            return new SpecialtiesExtractor(source);
        }
    }
}
