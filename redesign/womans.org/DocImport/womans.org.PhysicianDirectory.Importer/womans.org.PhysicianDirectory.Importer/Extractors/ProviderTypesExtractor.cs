﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MedTouch.PhysicianDirectory.Import.Extractors;
using womans.org.PhysicianDirectory.Import.Extractors.Models;

namespace womans.org.PhysicianDirectory.Import.Extractors
{
    public class ProviderTypesExtractor : XmlExtractor<ProviderType>
    {
        private ProviderTypesExtractor()
            : base()
        {
        }

        private ProviderTypesExtractor(string source)
            : base(source)
        {
        }

        protected override IEnumerable<ProviderType> ExtractModelInstances(XElement root)
        {
            return (from providerType in GetDescendants(root, "ProviderType")
                    let id = GetChildValue(providerType, "ID")
                    let name = GetChildValue(providerType, "Name")
                    let providerCount = GetChildValue(providerType, "ProviderCount")
                    where !string.IsNullOrWhiteSpace(id)
                           && !string.IsNullOrWhiteSpace(name)
                           && !string.IsNullOrWhiteSpace(providerCount)
                    select new ProviderType
                    {
                        ID = id,
                        Name = name,
                        ProviderCount = providerCount
                    }).ToList();
        }

        public static XmlExtractor<ProviderType> CreateNewProviderTypesExtractor()
        {
            return new ProviderTypesExtractor();
        }

        public static XmlExtractor<ProviderType> CreateNewProviderTypesExtractor(string source)
        {
            return new ProviderTypesExtractor(source);
        }
    }
}
