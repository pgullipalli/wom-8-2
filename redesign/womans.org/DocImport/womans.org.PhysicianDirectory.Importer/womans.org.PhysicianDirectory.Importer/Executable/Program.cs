﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using MedTouch.DataImport.Sync;
using womans.org.PhysicianDirectory.Import.Parsers;


namespace MedTouch.PhysicianDirectory.Import.Executable
{
    class Program
    {
        public static string log = string.Empty;

        static void Main(string[] args)
        {
            string physicianXmlFilePath = ConfigurationManager.AppSettings["medtouch.physician.import.xml.filepath"];
            string physicianXmlPrefix = ConfigurationManager.AppSettings["medtouch.physician.import.prefix"];
            //var physicianxmlfilepath = physicianXmlFilePath + physicianXmlPrefix + DateTime.Now.ToString("MMddyyyy") + ".csv";
            var physicianxmlfilepath = physicianXmlFilePath + physicianXmlPrefix  + ".csv";
            import(physicianxmlfilepath, IsDeltaMode(args));
        }

        private static bool IsDeltaMode(IEnumerable<string> args)
        {
            if (args == null || args.Count() < 1)
            {
                return false;
            }

            return args.FirstOrDefault(arg => string.Equals(arg, "delta=true", StringComparison.CurrentCultureIgnoreCase)) != null;
        }

        private static void import(string physicianxmlfilepath, bool isDeltaMode)
        {
            string emailMessage = string.Empty;
            DateTime? lastRunDate = null;
            bool isFullImport;

            if (isDeltaMode)
            {
                lastRunDate = GetLastDateRun();
                if (lastRunDate.HasValue)
                {
                    isFullImport = false;    
                }
                else
                {
                    isFullImport = true;
                }
            }
            else
            {
                isFullImport = true;
            }

            log = "Physician Import START";

            if (!File.Exists(physicianxmlfilepath))
            {
                //send email that file is not found
                emailMessage += string.Format(" {0} {1} - Failed to run the import", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString()) + System.Environment.NewLine;
                emailMessage += string.Format("\r\nXML Import provider file was not found for: {0}", physicianxmlfilepath);
                SendEmailNotification(emailMessage);
                return;
            }
            else
            {
                FileInfo f = new FileInfo(physicianxmlfilepath);
                long fileLength = f.Length;
                if (fileLength <= 0)
                {
                    //send email
                    emailMessage += string.Format(" {0} {1} - Failed to run the import", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString()) + System.Environment.NewLine;
                    emailMessage += string.Format("\r\nXML Import provider file for {0} had a file size of 0 ", physicianxmlfilepath);
                    SendEmailNotification(emailMessage);
                    return;
                }
            }

            var physicianParser = new ParsePhysicianInfo(lastRunDate, isFullImport);

            //var physicians = physicianParser.parse();
            var physicians = physicianParser.parse(physicianxmlfilepath);
			if(physicians.Count > 0)
			{
                var programLogic = new MedTouch.DataImport.PhysicianDirectory.Sync.ProgramLogic(isFullImport);
			    programLogic.UpdatedSince = DateTime.Now;
				RootItem rootItem = new RootItem();
				rootItem.Path = ConfigurationManager.AppSettings["medtouch.physician.import.find.doctor.root"];
				rootItem.Children = physicians;

				log += "Calling programLogic.run [START]";
				log += string.Format("There are total of {0} physicians in the xml.", physicians.Count);
				emailMessage += string.Format("{0} {1}- Import process started\r\n", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
				Stopwatch sw = Stopwatch.StartNew();
			    DateTime now = DateTime.Now;
				programLogic.run(rootItem);
				sw.Stop();

                LogLastRunDate(now);
                
				log += "Calling programLogic.run [END]";
				log += String.Format("Total Time Spent: {0:00}:{1:00}:{2:00} ", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds);
				emailMessage += string.Format(" {0} {1} - Import process finished", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString()) + System.Environment.NewLine;
				emailMessage += "\r\nSummary:" + System.Environment.NewLine;
				emailMessage += string.Format("Number of physicians imported/updated: {0} ", physicians.Count) + System.Environment.NewLine;
				emailMessage += String.Format("Time processed: {0:00}:{1:00}:{2:00} Log file: ", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds) + System.Environment.NewLine;
				string logFilePath = ConfigurationManager.AppSettings["medtouch.bata.import.logging.filepath"] + "log_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
				emailMessage += logFilePath;
                
				LogToFile(log);
				SendEmailNotification(emailMessage);
				//System.Console.Write("Press any key to continue...");
				//System.Console.Read();
			}
			else
			{
				//send email xml doesn't not contain the correct xml format
				emailMessage += string.Format(" {0} {1} - Failed to run the import", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString()) + System.Environment.NewLine;
				emailMessage += string.Format("\r\nXML Import provider file for {0} had 0 node count", physicianxmlfilepath);
				SendEmailNotification(emailMessage);
				return;
			}
        }

        private static DateTime? GetLastDateRun()
        {
            string filePath = GetLastRunDateFilePath();
            if (string.IsNullOrWhiteSpace(filePath) || !File.Exists(filePath))
            {
                return null;
            }

            IEnumerable<string> lines = File.ReadAllLines(filePath);
            if (lines == null || !lines.Any())
            {
                return null;
            }

            DateTime dateLastRun = DateTime.MinValue;
            if (!DateTime.TryParse(string.Join(string.Empty, lines), out dateLastRun))
            {
                return null;
            }

            return dateLastRun;
        }

        public static void LogToFile(string logMessage)
        {
            if (ConfigurationManager.AppSettings["medtouch.bata.import.logging.enabled"].Equals("true"))
            {
                string logFilePath = ConfigurationManager.AppSettings["medtouch.bata.import.logging.filepath"] + "log_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";

                if (!string.IsNullOrEmpty(logFilePath))
                {
                    using (StreamWriter w = File.AppendText(logFilePath))
                    {
                        w.Write("\r\nLog Entry : ");
                        w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                        w.WriteLine("{0}", logMessage);
                        w.Flush();
                        w.Close();
                    }
                }
            }
        }

        private static void LogLastRunDate(DateTime lastRunDate)
        {
            string filePath = GetLastRunDateFilePath();
            if (!string.IsNullOrEmpty(filePath))
            {
                using (StreamWriter w = File.CreateText(filePath))
                {
                    w.Write("{0}", lastRunDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    w.Flush();
                    w.Close();
                }
            }
        }
        
        private static string GetLastRunDateFilePath()
        {
            return ConfigurationManager.AppSettings["medtouch.physician.import.datelastrun.filepath"];
        }

        /// <summary>
        /// Sends the email notification.
        /// </summary>
        public static void SendEmailNotification(string message)
        {
            try
            {
                SendEmail(message);
            }
            catch (Exception)
            {
                log += "Exception at SendEmailNOtification.";
            }

        }

        public static void SendEmail(string strMessage)
        {
            MailAddress from = new MailAddress(ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.from"]);
            string[] toAddress = ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.to"].Split(';');
            foreach (string emailTo in toAddress)
            {
                MailAddress to = new MailAddress(emailTo);
                MailMessage message = new MailMessage(from, to);
                message.Subject = ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.subject"] + string.Format("{0}/{1}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString());
                message.Body = strMessage;

                SmtpClient client = new SmtpClient();
                client.Host = ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.server"];
                client.Port = Int32.Parse(ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.server.port"]);
                NetworkCredential credentials = new NetworkCredential(
                    ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.server.user.name"],
                    ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.server.password"]);
                client.Credentials = credentials;

                try
                {
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    log = "Error on sending notification email.";
                }
            }
        }
    }
}
