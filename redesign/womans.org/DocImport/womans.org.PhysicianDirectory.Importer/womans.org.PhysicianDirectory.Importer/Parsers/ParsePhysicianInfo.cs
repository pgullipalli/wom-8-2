﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using MedTouch.DataImport.Parser;
using MedTouch.DataImport.Sync;
using MedTouch.DataImport.Sync.Contracts.MasterToSlave;
using MedTouch.PhysicianDirectory.DataImport;

using womans.org.PhysicianDirectory.Import.Extractors;
using womans.org.PhysicianDirectory.Import.Extractors.DTO;
using womans.org.PhysicianDirectory.Import.Extractors.Models;
using womans.org.PhysicianDirectory.Import.Extractors.Models.Maps;
using womans.org.PhysicianDirectory.Import.Utilities.Serialization.Entities;
using MedTouch.PhysicianDirectory.Import.Extractors;
using System.Data;
using System.Text;
using MedTouch.DataImport._Class.Global;

namespace womans.org.PhysicianDirectory.Import.Parsers
{
    public class ParsePhysicianInfo : IParser
    {
        private const string Pipe = "|";
        private const string Space = " ";
        private const string Comma = ",";
        private const string ParagraphTag = "<p>";
        private const string ParagraphFormat = "<p>{0}</p>";

        private static readonly string MedialibraryPath = ConfigurationManager.AppSettings["medtouch.physician.import.medialibrary.path"];
        private static readonly string ImageServerBaseUrl = ConfigurationManager.AppSettings["medtouch.physician.import.image.server.base.url"];

        private DateTime? LastRun { get; set; }

        private bool IsFullImport { get; set; }

        public ParsePhysicianInfo()
        {
        }

        public ParsePhysicianInfo(DateTime? lastRun, bool isFullImport)
        {
            LastRun = lastRun;
            IsFullImport = isFullImport;
        }
        
        private static readonly string DefaultPhysicianPhotoImageHtml = string.Format
        (
            "<image mediaid=\"{0}\" mediapath=\"{1}\" src=\"{2}\" />",
            ConfigurationManager.AppSettings["medtouch.physician.import.medialibrary.DefaultPhysicianPhotoMediaId"],
            ConfigurationManager.AppSettings["medtouch.physician.import.medialibrary.DefaultPhysicianPhotoMediaPath"],
            ConfigurationManager.AppSettings["medtouch.physician.import.medialibrary.DefaultPhysicianPhotoMediaSrc"]
        );

        public List<IInsertable> parse(string path)
        {
            List<Physician> physicians = new List<Physician>();

            IDataTableExtractor physciansDataTableExtractor = CreateNewCsvExtractor(path);
            DataTable physiciansDataTable = physciansDataTableExtractor.Extract();

            foreach (DataRow physician in physiciansDataTable.Rows)
            {
                string internalHospitalID = physician[CsvFields.InternalHospitalID].ToString();
                if (string.IsNullOrWhiteSpace(internalHospitalID))
                    continue;

                Physician p = physicians.FirstOrDefault(doctor => string.Equals(doctor.Internal_Hospital_ID, internalHospitalID, StringComparison.CurrentCultureIgnoreCase));
                bool found = p != null;

                if (!found)
                {
                    p = new Physician();

                    p.First_Name = physician[CsvFields.FirstName].ToString();
                    p.Middle_Initial = physician[CsvFields.MiddleInitial].ToString();
                    p.Last_Name = physician[CsvFields.LastName].ToString();
                    string urlFormat =
                        "<link linktype=\"external\" url=\"{0}\" anchor=\"\" target=\"_blank\" />";
                    if (!string.IsNullOrWhiteSpace(physician[CsvFields.Website].ToString()))
                        p.Website = string.Format(urlFormat, physician[CsvFields.Website].ToString());

                    if (!physician[CsvFields.DegreeTitles].Equals(System.DBNull.Value))
                    {
                        string[] degreeTitles = ((List<string>) physician[CsvFields.DegreeTitles]).ToArray();
                        if (degreeTitles != null)
                        {
                            p.Degree_Title = degreeTitles;
                        }
                    }

                    if (!physician[CsvFields.LanguagesSpoken].Equals(System.DBNull.Value))
                    {
                        string[] languages = ((List<string>) physician[CsvFields.LanguagesSpoken]).ToArray();
                        if (languages != null)
                        {
                            p.Languages_Spoken = languages;
                        }
                    }

                    p.Internal_Hospital_ID = internalHospitalID;
                    string[] specialties = new string[0];
                    if (!string.IsNullOrWhiteSpace(physician[CsvFields.Specialties].ToString()))
                    {
                        specialties = new string[] {physician[CsvFields.Specialties].ToString()};
                    }
                    p.Specialties = specialties;


                    p.Certifications = physician[CsvFields.Certifications].ToString();

                    //string headline = string.Empty;
                    //if (!string.IsNullOrWhiteSpace(p.First_Name))
                    //{
                    //    headline = p.First_Name;
                    //}
                    //if (!string.IsNullOrWhiteSpace(p.Middle_Initial))
                    //{
                    //    headline = string.Format("{0} {1}", headline, p.Middle_Initial);
                    //}
                    //if (!string.IsNullOrWhiteSpace(p.Last_Name))
                    //{
                    //    headline = string.Format("{0} {1}", headline, p.Last_Name);
                    //}
                    //if (!string.IsNullOrWhiteSpace(p.Suffix))
                    //{
                    //    headline = string.Format("{0} {1}", headline, p.Suffix);
                    //}
                    //if (p.Degree_Title != null && p.Degree_Title.Count() > 0 && !string.IsNullOrWhiteSpace(p.Degree_Title[0]))
                    //{
                    //    headline = string.Format("{0}, {1}", headline, p.Degree_Title[0]);
                    //}
                    //p.Headline = headline;

                    /// Education
                    //string educationType = physician[CsvFields.Education.EducationType].ToString();
                    //string institutionName = physician[CsvFields.Education.InstitutionName].ToString();
                    //if (!string.IsNullOrWhiteSpace(educationType) && !string.IsNullOrWhiteSpace(institutionName))
                    //{
                    //    p.Education = new[]
                    //            {
                    //                new Education
                    //                {
                    //                    Education_Type = educationType,
                    //                    Institution_Name = institutionName
                    //                }
                    //            };
                    //}

                    /// Internship                    
                    //if (!physician[CsvFields.Internship.InstitutionNames].Equals(System.DBNull.Value))
                    //{
                    //    List<string> internships = physician[CsvFields.Internship.InstitutionNames] as List<string>;
                    //    if (internships != null && internships.Any())
                    //    {
                    //        p.Internship = new Internship[internships.Count];
                    //        for (int i=0; i<internships.Count; i++)
                    //        {
                    //            Internship internshipObject = new Internship { Institution_Name = internships[i] };
                    //            p.Internship[i] = internshipObject;
                    //        }
                    //    }
                    //}

                    /// Residency
                    //if (!physician[CsvFields.Residency.InstitutionNames].Equals(System.DBNull.Value))
                    //{
                    //    List<string> residencies = physician[CsvFields.Residency.InstitutionNames] as List<string>;
                    //    if (residencies != null && residencies.Any())
                    //    {
                    //        p.Residency = new Residency[residencies.Count];
                    //        for (int i = 0; i < residencies.Count; i++)
                    //        {
                    //            Residency residencyObject = new Residency { Institution_Name = residencies[i] };
                    //            p.Residency[i] = residencyObject;
                    //        }
                    //    }
                    //}

                    /// Fellowship
                    //if (!physician[CsvFields.Fellowship.InstitutionNames].Equals(System.DBNull.Value))
                    //{
                    //    List<string> fellowships = physician[CsvFields.Fellowship.InstitutionNames] as List<string>;
                    //    if (fellowships != null && fellowships.Any())
                    //    {
                    //        p.Fellowship = new Fellowship[fellowships.Count];
                    //        for (int i = 0; i < fellowships.Count; i++)
                    //        {
                    //            Fellowship fellowshipObject = new Fellowship { Institution_Name = fellowships[i] };
                    //            p.Fellowship[i] = fellowshipObject;
                    //        }
                    //    }
                    //}

                    /// Office
                    if (!physician[CsvFields.Offices].Equals(System.DBNull.Value))
                    {
                        List<CsvOffice> offices = physician[CsvFields.Offices] as List<CsvOffice>;
                        if (offices != null && offices.Any())
                        {
                            p.LocationList = new Location[offices.Count];
                            p.PhysicianOffice = new PhysicianOffice[offices.Count];
                            for (int i = 0; i < offices.Count; i++)
                            {
                                /// Office Location
                                Location location = new Location
                                {
                                    Headline = offices[i].Location.LocationName,
                                    Navigation_Title = offices[i].Location.LocationName,
                                    Browser_Title = offices[i].Location.LocationName,
                                    Location_Name = offices[i].Location.LocationName,
                                    Address_1 = offices[i].Location.Address1,
                                    Address_2 = offices[i].Location.Address2,
                                    City = offices[i].Location.City,
                                    State = offices[i].Location.State.ToUpper(),
                                    DisplayState = offices[i].Location.State.ToUpper(),
                                    Zip = offices[i].Location.Zip
                                };
                                string officeId =
                                    string.Format("{0} {1} {2} {3} {4}", location.Location_Name, location.Address_1,
                                        location.Address_2, location.City, location.Zip).Trim();
                                if (officeId.Length > 100)
                                {
                                    officeId = officeId.Substring(0, 100);
                                }
                                location.Location_ID = officeId;

                                p.LocationList[i] = location;

                                /// Office
                                PhysicianOffice physicianOffice = new PhysicianOffice
                                {
                                    Office_Id = officeId,
                                    Location = location.GetContextSensitiveKey(),
                                    Office_Name = offices[i].OfficeName,
                                    Office_Phone = offices[i].OfficePhone,
                                    Office_Fax = offices[i].OfficeFax,
                                    Answering_Phone = offices[i].OfficeAnswering
                                };
                                p.PhysicianOffice[i] = physicianOffice;
                            }

                        }
                    }

                }
                else
                {
                    //if we got the same physician again, see if we need to add another specialty
                    if (!physician[CsvFields.Specialties].Equals(System.DBNull.Value) && !p.Specialties.Contains(physician[CsvFields.Specialties].ToString()))
                    {
                        List<string> newSpecialties = p.Specialties.ToList();
                        newSpecialties.Add(physician[CsvFields.Specialties].ToString());
                        p.Specialties = newSpecialties.ToArray();
                    }
                }

                if (!found)
                {
                    physicians.Add(p);
                }
            }

            //put them in alpha order, not necesary, but nice
            physicians.Sort(delegate(Physician p1, Physician p2) { 
                string p1FullName = p1.Last_Name + " " + p1.First_Name + " " + p1.Middle_Initial;
                string p2FullName = p2.Last_Name + " " + p2.First_Name + " " + p2.Middle_Initial;
                return p1FullName.CompareTo(p2FullName); 
            });


            Console.WriteLine("Total Physicians Parsed: " + physicians.Count);
            Utility.log("Total Physician Parsed:" + physicians.Count);

            return physicians.Cast<IInsertable>().ToList();
        }

        //public List<IInsertable> parse()
        //{
        //    List<IInsertable> physicians = new List<IInsertable>();

        //    ProvidersExtractorSettings settings = new ProvidersExtractorSettings
        //    {
        //        ProviderSource = GetProviderSourceUrl(),
        //        Affiliations  = AffiliationsExtractor.CreateNewAffiliationsExtractor(ConfigurationManager.AppSettings["medtouch.physician.import.affiliations.url"]).Extract(),
        //        Languages = LanguagesExtractor.CreateNewLanguagesExtractor(ConfigurationManager.AppSettings["medtouch.physician.import.languages.url"]).Extract(),
        //        Practices = PracticesExtractor.CreateNewPracticesExtractor(ConfigurationManager.AppSettings["medtouch.physician.import.practices.url"]).Extract(),
        //        ProviderStatuses = ProviderStatusesExtractor.CreateNewProviderStatusesExtractor(ConfigurationManager.AppSettings["medtouch.physician.import.providerstatuses.url"]).Extract(),
        //        ProviderTypes = ProviderTypesExtractor.CreateNewProviderTypesExtractor(ConfigurationManager.AppSettings["medtouch.physician.import.providertypes.url"]).Extract(),
        //        Specialties = SpecialtiesExtractor.CreateNewSpecialtiesExtractor(ConfigurationManager.AppSettings["medtouch.physician.import.specialties.url"]).Extract()
        //    };

        //    XmlExtractor<Provider> providersExtractor = ProvidersExtractor.CreateNewProvidersExtractor(settings);
        //    IEnumerable<Provider> providers = providersExtractor.Extract();
        //    foreach (Provider provider in providers)
        //    {
        //        var p = new Physician();

        //        IList<string> headline = new List<string>();
        //        headline.Add(provider.FirstName);
        //        headline.Add(Space);
        //        headline.Add(provider.LastName);
                
        //        if (!string.IsNullOrWhiteSpace(provider.MedicalSuffixOrDegrees) && headline.Any())
        //        {
        //            headline.Add(Comma);
        //        }

        //        headline.Add(provider.MedicalSuffixOrDegrees);

        //        p.Headline = string.Join(string.Empty, headline).Trim();
        //        p.Content_Copy = ReplaceVariables(provider.Bio);

        //        p.First_Name = provider.FirstName;
        //        p.Middle_Initial = provider.MiddleName;
        //        p.Last_Name = provider.LastName;
        //        p.Suffix = provider.GenerationalSuffix;
        //        p.Gender = provider.Gender;

        //        bool canImportPhoto = !provider.IsDeleted
        //                              && string.Equals(provider.ShowPhotoPublicly, "yes", StringComparison.CurrentCultureIgnoreCase)
        //                              && !string.IsNullOrWhiteSpace(provider.PhotoURL);

        //        if (canImportPhoto)
        //        {
        //            try
        //            {
        //                var getPhysicianImagePath = new GetPhysicianImagePath();
        //                string imageValue = getPhysicianImagePath.GetPhysicianImagePathViaWebService
        //                (
        //                    string.Concat(MedialibraryPath, p.Last_Name[0]),
        //                    string.Concat(p.First_Name, Space, p.Middle_Initial, Space, p.Last_Name).Replace('.', ' '),
        //                    provider.PhotoURL
        //                );

        //                if (!string.IsNullOrEmpty(imageValue))
        //                {
        //                    p.Photo = imageValue;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Console.WriteLine("Unable to load image for " + p.Internal_Hospital_ID + " (" + provider.PhotoURL + ")");
        //            }
        //        }

        //        p.Degree_Title = provider.MedicalSuffixOrDegrees.Split(Comma.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList().Select(degree => degree.Trim()).ToArray();
        //        if (provider.Languages.Any())
        //        {
        //            //p.Languages_Spoken = new ImportItems
        //            //    {
        //            //        FieldName = "Languages Spoken",
        //            //        Items = (from language in provider.Languages
        //            //                 let nameField = new ImportField
        //            //                     {
        //            //                         Name = "Language Name",
        //            //                         Value = language.Name
        //            //                     }
        //            //                 let codeField = new ImportField
        //            //                     {
        //            //                         Name = "Language Code",
        //            //                         Value = language.ID
        //            //                     }
        //            //                 where !string.IsNullOrWhiteSpace(nameField.Name)
        //            //                 select new ImportItem
        //            //                     {
        //            //                         TemplatePath = "/sitecore/templates/Global/Component Templates/Language",
        //            //                         ParentPath = "/sitecore/content/Global/List Manager/Languages",
        //            //                         Name = language.Name,
        //            //                         Fields = new List<ImportField>(new[] {nameField, codeField})
        //            //                     }).ToList()
        //            //    }.ToString();
        //        }
        //        else
        //        {
        //            //p.Languages_Spoken = new ImportItems
        //            //    {
        //            //        FieldName = "Languages Spoken",
        //            //        Items = new List<ImportItem>()
        //            //    }.ToString();
        //        }

        //        if (provider.Affiliations.Any())
        //        {
        //            List<string> affiliations = (from affil in provider.Affiliations
        //                                         where AffilationsToLocationsMap.Current.HasKey(affil.Name)
        //                                         select AffilationsToLocationsMap.Current[affil.Name]).ToList();

        //            //p.Affiliations = string.Join(Pipe, affiliations);
        //        }

        //        p.Internal_Hospital_ID = provider.ID;

        //        if (string.Equals(provider.AcceptingNewPatients, "yes", StringComparison.CurrentCultureIgnoreCase))
        //        {
        //            p.Accepting_New_Patients = "1";
        //        }
        //        else if (string.Equals(provider.AcceptingNewPatients, "no", StringComparison.CurrentCultureIgnoreCase))
        //        {
        //            p.Accepting_New_Patients = "0";
        //        }

        //        p.NPI_Number = provider.NPINumber;

        //        if (provider.ProviderType != null)
        //        {
        //            p.ProviderTypes = new ImportItems
        //            {
        //                FieldName  = "Provider Types",
        //                Items  = new List<ImportItem> 
        //                (
        //                    new [] 
        //                    { 
        //                        new ImportItem
        //                        {
        //                            TemplatePath = "/sitecore/templates/Modules/Physician Directory/Component Templates/Provider Type",
        //                            ParentPath = "/sitecore/content/Global/Physician Directory/Provider Types",
        //                            Name = provider.ProviderType.Name,
        //                            Fields  = new List<ImportField>
        //                            (
        //                                new[] 
        //                                {
        //                                    new ImportField
        //                                    {
        //                                        Name  = "Provider Type Name",
        //                                        Value  = provider.ProviderType.Name        
        //                                    },
        //                                    new ImportField
        //                                    {
        //                                        Name  = "Provider Type Code",
        //                                        Value  = provider.ProviderType.ID
        //                                    }
        //                                }
        //                            )
        //                        }
        //                    } 
        //                )
        //            }.ToString();
        //        }
        //        else
        //        {
        //            p.ProviderTypes = new ImportItems
        //                {
        //                    FieldName = "Provider Types",
        //                    Items = new List<ImportItem>()
        //                }.ToString();
        //        }

        //        if (string.Equals(provider.DisplayInPDF, "yes", StringComparison.CurrentCultureIgnoreCase))
        //        {
        //            p.Display_In_PDF = "1";
        //        }
        //        else if (string.Equals(provider.DisplayInPDF, "no", StringComparison.CurrentCultureIgnoreCase))
        //        {
        //            p.Display_In_PDF = "0";    
        //        }

        //        CustomTrueFalse hasProvidenceMyChart = provider.CustomTrueFalses.FirstOrDefault(ctf => string.Equals(ctf.Type, "Has Providence MyChart?", StringComparison.CurrentCultureIgnoreCase));
        //        if (hasProvidenceMyChart != null && string.Equals(hasProvidenceMyChart.Value, "yes", StringComparison.CurrentCultureIgnoreCase))
        //        {
        //            p.Has_Providence_MyChart = "1";
        //        }
        //        else
        //        {
        //            p.Has_Providence_MyChart = "0";
        //        }

        //        //p.Specialties = new ImportItems
        //        //{
        //        //    FieldName = "Specialties",
        //        //    Items = (from specialty in provider.Specialties
        //        //             where ConvertYesNoToBoolean(specialty.IncludeInPublicSearches)
        //        //             let nameField = new ImportField
        //        //             {
        //        //                 Name = "Specialty Name",
        //        //                 Value = specialty.Name
        //        //             }

        //        //             let codeField = new ImportField
        //        //             {
        //        //                 Name = "Specialty Code",
        //        //                 Value = specialty.ID
        //        //             }
        //        //             where !string.IsNullOrWhiteSpace(nameField.Name)
        //        //             select new ImportItem
        //        //             {
        //        //                 TemplatePath = "/sitecore/templates/Global/Component Templates/Specialty",
        //        //                 ParentPath = "/sitecore/content/Global/List Manager/Specialties",
        //        //                 Name = specialty.Name,
        //        //                 Fields = new List<ImportField>(new[] { nameField, codeField })
        //        //             }).ToList()
        //        //}.ToString();

        //        CustomFreeText areaOfExpertise = provider.CustomFreeTexts.FirstOrDefault(cft => string.Equals(cft.Type, "AreaOfExpertise", StringComparison.CurrentCultureIgnoreCase));
        //        if (areaOfExpertise != null && !string.IsNullOrWhiteSpace(areaOfExpertise.Value))
        //        {
        //            if(areaOfExpertise.Value.IndexOf(ParagraphTag) < 0)
        //            {
        //                p.Expertise = string.Format(ParagraphFormat, areaOfExpertise.Value);
        //            }
        //            else
        //            {
        //                p.Expertise = areaOfExpertise.Value;
        //            }
        //        }

        //        if (provider.ProfessionalOrAcademicTitles.Any())
        //        {
        //            p.Position_Title = provider.ProfessionalOrAcademicTitles.Select(title => title.Name).ToArray();
        //        }

        //        CustomFreeText educationalBackGround = provider.CustomFreeTexts.FirstOrDefault(cft => string.Equals(cft.Type, "EducationalBackGround", StringComparison.CurrentCultureIgnoreCase));
        //        if (educationalBackGround != null)
        //        {
        //            p.Educational_Background = ReplaceVariables(educationalBackGround.Value);
        //        }

        //        if (provider.Publications.Any())
        //        {
        //            p.Publications = ReplaceVariables
        //            (
        //                string.Join
        //                (
        //                    string.Empty,
        //                    (from publication in provider.Publications
        //                     orderby publication.SortOrder
        //                     select publication.FullCitation).ToList()
        //                )
        //            );
        //        }
                
        //        IEnumerable<Video> videos = (from video in provider.Videos
        //                     select video).ToList();
							 
        //        if (videos.Any())
        //        {
        //            ImportFolder importFolder = new ImportFolder();
        //            importFolder.Folder = new ImportItem
        //            {
        //                Name = "Videos",
        //                Fields = new List<ImportField>(),
        //                TemplatePath = "/sitecore/templates/Common/Folder",
        //            };

   
        //            importFolder.Items = new List<ImportItem>();
   
        //            foreach (Video video in videos)
        //            {
        //                importFolder.Items.Add
        //                (
        //                   new ImportItem
        //                   {
        //                       TemplatePath = "/sitecore/templates/Modules/Physician Directory/Component Templates/Video",
        //                       Name = video.Name,
        //                       Fields = new List<ImportField>
        //                       (
        //                           new[]
        //                            {
        //                                new ImportField
        //                                {
        //                                    Name = "Name",
        //                                    Value = video.Name
        //                                },
        //                                new ImportField
        //                                {
        //                                    Name = "Url",
        //                                    Value = video.URL
        //                                },
        //                                new ImportField
        //                                {
        //                                    Name = "VideoSource",
        //                                    Value = video.VideoSource
        //                                },
        //                                new ImportField
        //                                {
        //                                    Name = "OriginalCode",
        //                                    Value = video.OriginalCode
        //                                },
        //                                new ImportField
        //                                {
        //                                    Name = "Featured",
        //                                    Value = ConvertYesNoToCheckBoxFieldValue(video.DisplayPublicly)
        //                                }
        //                            }
        //                       )
        //                   }
        //                );
        //            }

        //            p.Videos = importFolder.ToString();
        //        }

        //        List<Location> locations = new List<Location>();
        //        List<PhysicianOffice> offices = new List<PhysicianOffice>();
        //        foreach (Practice practice in provider.Practices)
        //        {
        //            Location location = new Location();

        //            if (practice.Address != null)
        //            {
        //                location.Address_1 = practice.Address.Line1;
        //                location.Address_2 = practice.Address.Line2;
        //                location.City = practice.Address.City;
        //                location.State = practice.Address.State;
        //                location.Zip = practice.Address.Zip;    
        //            }
                    
        //            ContactNumber mainPhone = practice.ContactNumbers.FirstOrDefault(cn => string.Equals(cn.Type, "Main", StringComparison.CurrentCultureIgnoreCase));
        //            if (mainPhone != null)
        //            {
        //                location.Main_Phone = FormatContactNumber(mainPhone);
        //            }

        //            ContactNumber mainFax = practice.ContactNumbers.FirstOrDefault(cn => string.Equals(cn.Type, "Fax", StringComparison.CurrentCultureIgnoreCase));
        //            if (mainFax != null)
        //            {
        //                location.Main_Fax = FormatContactNumber(mainFax);
        //            }

        //            location.Show_In_Location_Listings = "0";
        //            location.Location_Name = practice.Name;
        //            location.Location_ID = string.Format("{0} {1} {2} {3} {4}", location.Location_Name, location.Address_1, location.Address_2, location.City, location.Zip).Trim();

        //            locations.Add(location);

        //            PhysicianOffice office = new PhysicianOffice();
        //            office.Office_Name = location.Location_Name;
        //            //office.Location = location.Location_Name;
        //            office.Office_Id = location.Location_ID;

        //            if (practice.Address != null)
        //            {
        //                office.Has_Evening_Hours = ConvertYesNoToCheckBoxFieldValue(practice.Address.HasEveningHours);
        //                office.Has_Weekend_Hours = ConvertYesNoToCheckBoxFieldValue(practice.Address.HasWeekendHours);
        //            }

        //            offices.Add(office);
        //        }

        //        //p.Locations = locations.ToArray();
        //        p.PhysicianOffice = offices.ToArray();
        //        p.ShouldDelete = provider.IsDeleted;

        //        bool shouldAdd = !(provider.IsDeleted && IsFullImport);
        //        if (shouldAdd)
        //        {
        //            physicians.Add(p);
        //        }
        //    }

        //    SaveProviderExtractorXml(providersExtractor);
        //    return physicians;
        //}

        private static string FormatContent(string content)
        {            
            string updatedContent = ReplaceVariables(content);
            if (string.IsNullOrWhiteSpace(updatedContent))
            {
                return updatedContent;
            }

            StringBuilder sbResult = new StringBuilder();

            string[] contentParagraphs = updatedContent.Split(new string[]{"    ", "\n\r", "\n", "\r"}, StringSplitOptions.RemoveEmptyEntries);
            foreach (string paragraph in contentParagraphs)
            {
                if (!string.IsNullOrWhiteSpace(paragraph))
                {
                    sbResult.AppendLine(string.Format("<p>{0}</p>", paragraph.Trim()));
                }
            }

            return sbResult.ToString();
        }

        private static string ReplaceVariables(string content)
        {
            if (string.IsNullOrWhiteSpace(content))
            {
                return content;
            }

            //content = Regex.Replace(content, "&", "&amp;", RegexOptions.IgnoreCase);
            content = Regex.Replace(content, "{Line Break}", string.Empty, RegexOptions.IgnoreCase);
            return content;
        }

        private static string FormatContactNumber(ContactNumber contactNumber)
        {
            if (contactNumber == null)
            {
                return string.Empty;
            }
            string numberNoAreaCode = FormatPhoneNumberNoAreaCode(contactNumber.Number);
            if (string.IsNullOrWhiteSpace(contactNumber.AreaCode) && !string.IsNullOrWhiteSpace(numberNoAreaCode))
            {
                return numberNoAreaCode;
            }

            if (!string.IsNullOrWhiteSpace(contactNumber.AreaCode) && string.IsNullOrWhiteSpace(numberNoAreaCode))
            {
                return numberNoAreaCode;
            }

            return string.Join("-", new[] { contactNumber.AreaCode, FormatPhoneNumberNoAreaCode(contactNumber.Number) });
        }

        private static string FormatPhoneNumberNoAreaCode(string number)
        {
            if (string.IsNullOrWhiteSpace(number))
            {
                return string.Empty;
            }

            if (number.Length != 7)
            {
                return number;
            }

            return string.Concat(number.Substring(0, 3), "-", number.Substring(3, 4));
        }

        private static string ConvertYesNoToCheckBoxFieldValue(string polarity)
        {
            if (string.IsNullOrWhiteSpace(polarity))
            {
                return polarity;
            }

            if (string.Equals(polarity, "yes", StringComparison.CurrentCultureIgnoreCase))
            {
                return "1";
            }
            if (string.Equals(polarity, "no", StringComparison.CurrentCultureIgnoreCase))
            {
                return "0";
            }

            return polarity;
        }

        private static bool ConvertYesNoToBoolean(string polarity)
        {
            if (string.Equals(polarity, "yes", StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }

            return false;
        }

        public RootItem parse(string filename, string rootItemPath)
        {
            throw new NotImplementedException();
        }

        private string GetProviderSourceUrl()
        {
            if (!LastRun.HasValue)
            {
                return ConfigurationManager.AppSettings["medtouch.physician.import.providers.url"];
            }

            return string.Format(ConfigurationManager.AppSettings["medtouch.physician.import.providers.delta.url.format"], LastRun.Value.ToString("yyyy-MM-dd HH:mm:ss"));
        }

        private static void SaveProviderExtractorXml(XmlExtractor<Provider> extractor)
        {
            string format = ConfigurationManager.AppSettings["medtouch.physician.import.xmlsource.filepath.format"];
            if (string.IsNullOrWhiteSpace(format))
            {
                return;
            }

            string filePath = string.Format(format, DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss"));
            SaveExtractorXml(extractor, filePath);
        }

        private static void SaveExtractorXml<T>(XmlExtractor<T> extractor, string filePath)
        {
            if (extractor == null || string.IsNullOrWhiteSpace(filePath))
            {
                return;
            }

            extractor.Save(filePath);
        }

        public static IDataTableExtractor CreateNewCsvExtractor(string path)
        {
            return new WomansCsvExtractor(new FileInfo(path));
        }
    }
}
