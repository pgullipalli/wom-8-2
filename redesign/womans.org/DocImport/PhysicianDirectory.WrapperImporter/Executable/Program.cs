﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MedTouch.DataImport.Sync;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Net;

namespace PhysicianDirectory.WrapperImporter.Executable
{
    class Program
    {
        public static string log = string.Empty;

        public static void Main(string[] args)
        {
            ImportWrapper();
        }

        private static void ImportWrapper()
        {
            int physiciansCount = 0;
            string emailMessage = string.Empty;

            var programLogic = new ProgramLogic(true);
            programLogic.UpdatedSince = DateTime.Now;

            log += "Calling programLogic.RunImportWrapper [START]";
            //log += string.Format("There are total of {0} physicians in the xml.", physicians.Count);
            emailMessage += string.Format("{0} {1}- Import process started\r\n", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
            Stopwatch sw = Stopwatch.StartNew();
            DateTime now = DateTime.Now;
            programLogic.RunImportWrapper(out physiciansCount);
            sw.Stop();

            LogLastRunDate(now);

            log += "Calling programLogic.RunImportWrapper [END]";
            log += String.Format("Total Time Spent: {0:00}:{1:00}:{2:00} ", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds);
            emailMessage += string.Format(" {0} {1} - Import process finished", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString()) + System.Environment.NewLine;
            emailMessage += "\r\nSummary:" + System.Environment.NewLine;
            emailMessage += string.Format("Number of physicians imported/updated: {0} ", physiciansCount) + System.Environment.NewLine;
            emailMessage += String.Format("Time processed: {0:00}:{1:00}:{2:00} Log file: ", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds) + System.Environment.NewLine;
            string logFilePath = ConfigurationManager.AppSettings["medtouch.bata.import.logging.filepath"] + "log_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            emailMessage += logFilePath;

            LogToFile(log);
            SendEmailNotification(emailMessage);
        }

        public static void LogToFile(string logMessage)
        {
            if (ConfigurationManager.AppSettings["medtouch.bata.import.logging.enabled"].Equals("true"))
            {
                string logFilePath = ConfigurationManager.AppSettings["medtouch.bata.import.logging.filepath"] + "log_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";

                if (!string.IsNullOrEmpty(logFilePath))
                {
                    using (StreamWriter w = File.AppendText(logFilePath))
                    {
                        w.Write("\r\nLog Entry : ");
                        w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                        w.WriteLine("{0}", logMessage);
                        w.Flush();
                        w.Close();
                    }
                }
            }
        }

        private static void LogLastRunDate(DateTime lastRunDate)
        {
            string filePath = GetLastRunDateFilePath();
            if (!string.IsNullOrEmpty(filePath))
            {
                using (StreamWriter w = File.CreateText(filePath))
                {
                    w.Write("{0} - {1}", lastRunDate.ToString("yyyy-MM-dd HH:mm:ss"), "By ImportWrapper executable.");
                    w.Flush();
                    w.Close();
                }
            }
        }

        private static string GetLastRunDateFilePath()
        {
            return ConfigurationManager.AppSettings["medtouch.physician.import.datelastrun.filepath"];
        }

        /// <summary>
        /// Sends the email notification.
        /// </summary>
        public static void SendEmailNotification(string message)
        {
            try
            {
                SendEmail(message);
            }
            catch (Exception ex)
            {
                log += string.Format("Exception at SendEmailNOtification.\r\nExcpetion Message: {0}\r\nStack Trace: {1}", ex.Message, ex.StackTrace);
            }

        }

        public static void SendEmail(string strMessage)
        {
            MailAddress from = new MailAddress(ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.from"]);
            string[] toAddress = ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.to"].Split(';');
            foreach (string emailTo in toAddress)
            {
                MailAddress to = new MailAddress(emailTo);
                MailMessage message = new MailMessage(from, to);
                message.Subject = ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.subject"] + string.Format("{0}/{1}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString());
                message.Body = strMessage;

                SmtpClient client = new SmtpClient();
                client.Host = ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.server"];
                client.Port = Int32.Parse(ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.server.port"]);
                NetworkCredential credentials = new NetworkCredential(
                    ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.server.user.name"],
                    ConfigurationManager.AppSettings["medtouch.bata.import.logging.mail.server.password"]);
                client.Credentials = credentials;

                try
                {
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    log += string.Format("Error on sending notification email.\r\nExcpetion Message: {0}\r\nStack Trace: {1}", ex.Message, ex.StackTrace);
                }
            }
        }
    }
}
