﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Two Column Wide Left Layout Frame =============================================-->
<div class="main-content grid">

        <sc:Placeholder ID="plcSection" key="Section Panel" runat="server" />

<div class="wide-left">
		<div class="mobile-left-col"></div>
    <!--===== CONTENT COLUMN =====-->
    <section class="nine columns">
        <sc:Placeholder ID="Placeholder2" key="Content Panel" runat="server" />
    </section>
    <!--===== CONTENT COLUMN =====-->

    <!--===== RIGHT COLUMN =====-->
    <aside class="three columns rightside">
        <sc:Placeholder ID="Placeholder1" key="Right Panel" runat="server" />
    </aside>
    <!--===== RIGHT COLUMN =====-->
    
    <!--===== BOTTOM COLUMN =====-->
    <sc:Placeholder ID="Placeholder34" key="Bottom Panel" runat="server"/>
    <!--===== BOTTOM COLUMN =====-->
</div>
</div>
<!--============================================= MedTouch.Base: Common: Two Column Wide Left Layout Frame =============================================-->
