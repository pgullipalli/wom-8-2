﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Two Column Wide Right Layout Frame =============================================-->
<div class="main-content grid">
    <div class="wide-right">
        <!--===== LEFT COLUMN =====-->
        <aside class="three columns">
		    <sc:Placeholder ID="Placeholder21" key="Left Panel" runat="server"/>
        </aside>
        <!--===== LEFT COLUMN =====-->

        <!--===== CONTENT COLUMN =====-->
        <section class="nine columns">
		    <sc:Placeholder ID="Placeholder22" key="Content Panel" runat="server"/>
        </section>
        <!--===== CONTENT COLUMN =====-->
    
        <!--===== BOTTOM COLUMN =====-->
        <sc:Placeholder ID="Placeholder34" key="Bottom Panel" runat="server"/>
        <!--===== BOTTOM COLUMN =====-->
    </div>
</div>
<!--============================================= /MedTouch.Base: Common: Two Column Wide Right Layout Frame =============================================-->