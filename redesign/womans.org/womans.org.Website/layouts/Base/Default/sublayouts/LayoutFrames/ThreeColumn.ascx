﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Three Columns Layout Frame =============================================-->
<div class="main-content grid">
   	<div class="interior-page">
        <!--===== LEFT COLUMN =====-->
        <aside class="three columns">
		    <sc:Placeholder ID="Placeholder31" key="Left Panel" runat="server"/>
            <div class="mobile-left-col"></div>
        </aside>
        <!--===== LEFT COLUMN =====-->

        <!--===== CONTENT COLUMN =====-->
        <section class="six columns">
		    <sc:Placeholder ID="Placeholder32" key="Content Panel" runat="server"/>
        </section>
        <!--===== CONTENT COLUMN =====-->

        <!--===== RIGHT COLUMN =====-->
        <aside class="three columns rightside"> 
	        <sc:Placeholder ID="Placeholder33" key="Right Panel" runat="server"/>
        </aside>
        <!--===== RIGHT COLUMN =====-->
    
        <!--===== BOTTOM COLUMN =====-->
        <sc:Placeholder ID="Placeholder34" key="Bottom Panel" runat="server"/>
        <!--===== BOTTOM COLUMN =====-->
    </div>
</div>
<!--============================================= /MedTouch.Base: Common: Three Columns Layout Frame =============================================-->