﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: One Column Layout Frame =============================================-->
<div class="main-content grid">
    <section class="column col-1">
        <sc:Placeholder ID="Placeholder11" key="Content Panel" runat="server"/>
    </section>
    <!--===== BOTTOM COLUMN =====-->
    <sc:Placeholder ID="Placeholder34" key="Bottom Panel" runat="server"/>
    <!--===== BOTTOM COLUMN =====-->
</div>
<!--============================================= /MedTouch.Base: Common: One Column Layout Frame =============================================-->