﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"  
    Inherits="Layouts.Homepagehero.LandingSliderFieldSublayout" Codebehind="LandingSlider.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Landing Slider =============================================--> 
<asp:PlaceHolder runat="server" ID="phSlider">
<section class="hero home grid">
	<div class="inner">
		<asp:Panel ID="pnlFrontEndSlides" runat="server" CssClass="mtslide" Visible="false">
            <ul class="slides">
				<asp:Repeater ID="rptFrontEndSlides" runat="server" OnItemDataBound="rptSlide_ItemDataBound">
					<ItemTemplate>
                        <li>
                            <asp:Literal runat="server" ID="litSlideImage" Visible="false"></asp:Literal>
                            <sc:Image runat="server" ID="scSlideImage" Visible="false" />
                            <asp:Panel ID="pnlCore" runat="server" CssClass="core bottom">
                                <div class="slide-content">
							        <h1><sc:Text runat="server" ID="scTextTitle" Field="Slide Title" /></h1>
							        <div class="copy-left">
								        <sc:Text ID="scTextContent" runat="server" Field="Slide Content" />
                                        <sc:Link ID="scLinkReadMore" runat="server" Field="Slide Link" />
							        </div>
							    </div>
						    </asp:Panel>						 
						</li>
					</ItemTemplate>
				</asp:Repeater>
			</ul>
        </asp:Panel>	
	</div>
</section>
</asp:PlaceHolder>
<!--============================================= /MedTouch.Base: Common: Landing Slider =============================================-->