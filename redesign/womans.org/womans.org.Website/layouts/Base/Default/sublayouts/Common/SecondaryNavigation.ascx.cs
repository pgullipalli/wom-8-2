﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.UI;
using womans.org.BusinessLogic.Helpers;
using Womans.CustomItems.PageTemplates.Common;

namespace womans.org.Website.layouts.Base.Default.sublayouts.Common
{
    public partial class SecondaryNavigation : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            rptSecondaryNav.DataSource = Navigation.GetSecondaryNavigation();
            rptSecondaryNav.DataBind();

            rptTabletMenu.DataSource = Navigation.GetSecondaryNavigation();
            rptTabletMenu.DataBind();

            if (Sitecore.Context.Item.TemplateID.ToString() == LandingPageItem.TemplateId)
            {
                divServicesMenu.Attributes.Add("class", "services-menu lp grid hide-on-phones");
                divServicesMenuTablet.Attributes.Add("class", "services-menu lp grid tablet-display");
            }
        }
    }
}