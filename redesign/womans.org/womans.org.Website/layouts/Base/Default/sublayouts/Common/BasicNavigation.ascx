﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.Base.layouts.modules.Base.BasicnavigationSublayout" CodeBehind="BasicNavigation.ascx.cs" %>
<!--============================================= MedTouch.Base: Common: Basic Navigation =============================================-->
<ul>
    <asp:Repeater ID="rptNav" runat="server" OnItemDataBound="rptNav_OnItemDataBound">
        <ItemTemplate>
            <li>
                <asp:HyperLink ID="hplNav" runat="server" />
            </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>
<!--============================================= /MedTouch.Base: Common: Basic Navigation =============================================--> 
