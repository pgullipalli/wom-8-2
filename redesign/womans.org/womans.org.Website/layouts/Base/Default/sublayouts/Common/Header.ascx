﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"  Inherits="womans.org.Website.layouts.Base.Default.sublayouts.Common.HeaderSublayout" Codebehind="Header.ascx.cs" %>
<%@ Import Namespace="MedTouch.Common.Helpers" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Header =============================================--> 

<div class="right-utility-A">
    <div class="text-resizer">
        <a class="zoom_out resetFont" href="">A</a>
        <a class="zoom_in increaseFont" href="">A</a>
        <a class="zoom_big increaseFont-more" href="">A</a>
    </div>
    <ul>
        <li>
            <a href="#" onclick=" window.print() " class="print-button">Print</a>
        </li>
        <li>
            <a href="http://www.addthis.com/bookmark.php" class="email-button addthis_button share">Email</a>
        </li>
    </ul>
</div>

<div class="right-utility-B">
    <sc:sublayout id="slSocial" runat="server" path="/layouts/womans/sublayouts/common/social.ascx" />
</div>
	
<header role="banner">				
    <!-- quick links panel -->
    <sc:sublayout id="slquicklinks" runat="server" path="/layouts/womans/sublayouts/common/quicklinks.ascx" />

    <section class="top grid">
		
        <!-- Mobile CODE: added .mobile-menu -->
        <div class="mobile-dropdown-nav">
            <ul>
                <li class="mobile-menu-button">
                    <a href="#">
                        <span></span>
                        <asp:Literal runat="server" ID="litMenuTitle"/>
                    </a>
                </li>
                <li class="mobile-search-button">
                    <a href="#">
                        <span></span>
                        Search
                    </a>
                </li>
                <li class="mobile-call-button">
                    <a href="tel:+12259271300">
                        <span></span>
                        Call
                    </a>
                </li>
            </ul>
        </div>
        
        <!-- /Mobile CODE: -->


        <!-- Header Logo -->
        <asp:Panel runat="server" ID="pnlLogo" CssClass="logo" Visible="False">
            <asp:HyperLink runat="server" ID="hplLogo">
                <asp:PlaceHolder runat="server" ID="plHomeLogo">
                    <sc:Image runat="server" ID="scHeaderHighResLogo" CssClass="screen-logo" MaxWidth="200" Field="Site Logo" />
                    <sc:Image runat="server" ID="scHeaderLogo" CssClass="ie-print" Field="Site Logo Fallback" />
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="plLogoOverride">
                    <sc:Image runat="server" ID="scHeaderHighResLogoOverride" CssClass="screen-logo foundation-logo" MaxWidth="200" Field="Site Logo Override" Width="200" CssStyle="margin-bottom: -20px;" />
                    <sc:Image runat="server" ID="scHeaderLogoOverride" CssClass="ie-print foundation-logo" Field="Site Logo Fallback Override" />
                </asp:PlaceHolder>
            </asp:HyperLink>
        </asp:Panel>
        <!-- /Header Logo -->

        
        <%--<a class="donate" href="#">Donate</a>--%>
        <sc:Link runat="server" ID="scLinkDonate" Field="Donate Url" CssClass="donate"></sc:Link>
        <div class="header-utility">
            <div class="header-utility-contents">
                <table>
                    <tr>
                        <!-- <td>
                                 <a class="donate" href="#">Donate</a>
                             </td> -->
                        <td>
                            <div class="search-table-wrapper">
                                <table>
                                    <tr>
                                        <td>
                                            <a class="quick-links" href="#">
                                                <%= CultureHelper.GetDictionaryTranslation("Common.QuickLinks.Label") %>
                                                <span class="arrow"></span>
                                            </a>
                                            <sc:sublayout id="scBasicSiteSearch" runat="server" path="/layouts/modules/SiteSearch/default/BasicSiteSearchBox.ascx" Parameters="Search Results Page={BE6F4231-8D94-476E-A3A9-930AB47EED37}"/>
                                        </td>
                                    </tr>
										
                                </table>
                            </div>
                        </td>
                        <td>
                            <a class="main-search" href="#"></a>
                        </td>
                    </tr>
                </table>
            </div>
				

        </div>
		


        <!-- Header Nav -->
        <sc:Sublayout runat="server" ID="slPrimaryNavigation" Path="/layouts/Base/Default/sublayouts/Common/PrimaryNavigation.ascx"/>
        <!-- /Header Nav -->

        <!-- Mobile CODE: added .mobile-menu -->
        <asp:Panel runat="server" ID="pnlInThisSection" Visible="False">
            <section class="mobile-menu visible-mobile">
                <a href="#" class="toggle2"><span>In This Section</span></a>
            </section>
        </asp:Panel>
        <!-- / END Mobile CODE -->

    </section>
    <sc:Placeholder runat="server" ID="slider" Key="slider" />
</header>
<sc:Sublayout runat="server" ID="slNotificationbox" Path="/layouts/Base/Default/sublayouts/Common/NotificationBox.ascx"/>	


<sc:Placeholder runat="server" ID="grid" Key="grid" />
		
<!--============================================= /MedTouch.Base: Common: Header =============================================-->