﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Carousel.ascx.cs" 
    Inherits="MedTouch.Base.layouts.Base.Default.sublayouts.Common.CaseSlider" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Carousel =============================================-->
<asp:Placeholder ID="phCarousel" runat="server" Visible="false">
    <section class="grid">
        
        <section class="mtc crsl-items" data-navigation="nav-01">
            <div class="slides crsl-wrap">
                <asp:Repeater ID="rptCarousel" runat="server" OnItemDataBound="rptCarousel_ItemDataBound">
                    <ItemTemplate>

                    <div class="crsl-item">
                        <section class="feat-box">
                            <sc:link id="scCalloutLink" runat="server" field="Callout Link" Visible="false">
                                <sc:Image id="scCalloutImage" runat="server" field="Callout Image" />
                            </sc:link>
                            <div id="divVideo" runat="server" class="video" visible="false">        
                                <a id="aCaseLink" runat="server" name="modal">
                                    <sc:Image id="scThumbImage" runat="server" field="Thumbnail" />
                                    <span style="background-image: url('/assets/images/pd-video-overlay.png'); background-scale: cover;
                                        position: absolute; width: 120px; height: 100px; top: 25%; left: 25%;"></span>
                                </a>
                            </div>
                            <h4><sc:Text runat="server" ID="scTxtCalloutTitle" Field="Callout Title" /></h4>
                            <sc:Text runat="server" ID="scTxtCalloutDescription" Field="Callout Description" />
                        </section>
                    </div>

                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </section>
        <div id="nav-01" class="crsl-nav">
	        <a href="#" class="previous"><asp:Literal ID="litPreviousLink" runat="server" /></a>
	        <a href="#" class="next"><asp:Literal ID="litNextLink" runat="server" /></a>
	    </div>
    </section>
</asp:Placeholder>
<!--============================================= /MedTouch.Base: Common: Carousel =============================================-->
