﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"  Inherits="MedTouch.Base.layouts.Base.Default.sublayouts.Common.ImagebannerSublayout" 
Codebehind="~/layouts/Base/Default/sublayouts/Common/ImageBanner.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Image Banner =============================================--> 
<asp:PlaceHolder runat="server" ID="phImageBanner" Visible="false">
    <article>
        <div class="inner-banner">
	        <sc:Image runat="server" ID="scImgBanner" Field="Interior Banner" Parameters="responsive=1"/>
        </div>
    </article>
</asp:PlaceHolder>
<!--============================================= /MedTouch.Base: Common: Image Banner =============================================--> 