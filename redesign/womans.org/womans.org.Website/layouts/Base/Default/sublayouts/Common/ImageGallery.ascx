﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageGallery.ascx.cs"
    Inherits="MedTouch.Base.layouts.Base.Default.sublayouts.Common.ImageGallery" %>
<!--============================================= MedTouch.Base: Common: Image Gallery =============================================-->
<asp:Panel ID="pnlImageGallery" runat="server" Visible="false" CssClass="module-photo-gallery">
    <div class="stage">
        <div class="image-wrap">
        </div>
        <div class="image-overlay">
            <div class="image-count">
                <span class="current"></span>of <span class="total"></span>
            </div>
            <div class="image-details">
                <h6 class="image-title">
                </h6>
                <div class="image-desc">
                </div>
            </div>
        </div>
    </div>
    <asp:Repeater ID="rptImageGallery" runat="server" OnItemDataBound="rptImageGallery_OnItemDataBound">
        <HeaderTemplate>
            <div class="thumbs">
        </HeaderTemplate>
        <ItemTemplate>
            <img src="#" id="imgThumbnail" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
            </div>
        </FooterTemplate>
    </asp:Repeater>
</asp:Panel>
<!--============================================= MedTouch.Base: Common: Image Gallery =============================================-->
