﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.Base.layouts.modules.Base.NavigationdropdownSublayout" CodeBehind="NavigationDropdown.ascx.cs" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Navigation Dropdown =============================================--> 

<ul>
    <asp:Repeater ID="rptNav" runat="server" OnItemDataBound="rptNav_OnItemDataBound">
        <ItemTemplate>
            <li>
                <sc:Link ID="scNav" runat="server" Field="Destination" />
            </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>
<!--============================================= /MedTouch.Base: Common: Navigation Dropdown =============================================--> 
