﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
            Inherits="MedTouch.Base.layouts.modules.Base.ImagecalloutSublayout" CodeBehind="~/layouts/Base/Default/sublayouts/Common/ImageCallout.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<!--============================================= MedTouch.Base: Common: Image Callout =============================================--> 
<asp:Panel ID="pnlCallout" runat="server" CssClass="callout">
    <h4>
        <span><sc:Text runat="server" ID="scTxtCalloutTitle" Field="Callout Header"/></span>
    </h4>
    <div class="feature-story-callout">
        <sc:Image id="scImage" runat="server" field="Callout Image" MaxWidth="250" />
        <div class="feature-story-title">
        
            <sc:link id="scLink" runat="server" field="Callout Link">
                <sc:Text runat="server" ID="scTxtCalloutDes" Field="Callout Text"/>
            </sc:link>
        </div>
    </div>
</asp:Panel>
<!--============================================= /MedTouch.Base: Common: Image Callout =============================================-->