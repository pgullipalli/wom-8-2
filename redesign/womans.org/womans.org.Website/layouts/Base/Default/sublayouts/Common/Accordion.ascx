﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Accordion.ascx.cs" Inherits="MedTouch.Base.layouts.modules.Base.Accordion" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Accordion =============================================-->
<%--<article>--%>
    <asp:Panel ID="pnlEditAccordion" Visible="false" runat="server">
        <sc:EditFrame ID="editLinks" runat="server" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Accordion">
            <h3>
                Edit Accordion</h3>
        </sc:EditFrame>
    </asp:Panel>
    
    <asp:Panel runat="server" ID="pnlAccordion" CssClass="accordion" Visible="False">
        <asp:Repeater runat="server" ID="rptAccordion">
            <ItemTemplate>
                <h3>
                    <a href="#"><%#Eval("key") %></a>
                </h3>
                <div class="accordian_copy"><%#Eval("value") %></div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Literal ID="litAccordion" runat="server" />
    </asp:Panel>
<%--</article>--%>
<!--============================================= /MedTouch.Base: Common: Accordion =============================================-->
