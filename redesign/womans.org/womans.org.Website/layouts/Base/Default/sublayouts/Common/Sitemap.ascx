﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.Base.layouts.modules.Base.SitemapSublayout" CodeBehind="Sitemap.ascx.cs" %>
<!--============================================= MedTouch.Base: Common: Sitemap =============================================--> 
<div class="sitemap">
    <asp:Literal ID="litSitemap" runat="server" />
</div>
<!--============================================= /MedTouch.Base: Common: Sitemap =============================================--> 
