﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using MedTouch.Common.Controls.Navigation;
using MedTouch.Common.Helpers;
using Sitecore.Data.Items;
using Sitecore.Links;
using MedTouch.Common.UI;
using Sitecore.Collections;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using System.Web;
using System.Web.UI.HtmlControls;

namespace womans.org.Website.layouts.Base.Default.sublayouts.Common
{
    public partial class PrimaryNavigation : BaseSublayout
    {
        private void Page_Load(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif
                BindControls();

#if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
#endif
        }

        private void BindControls()
        {
            Item navItem = ItemHelper.GetSingleItemFromReferenceField(ItemHelper.GetStartItem(), "Primary Navigation");
            if (navItem != null)
            {
                ChildList childList = navItem.GetChildren();
                if (childList.Any())
                {
                    rptPrimaryNav.DataSource = childList;
                    rptPrimaryNav.DataBind();
                }
                else
                {
                    rptPrimaryNav.Visible = false;
                }
            }
        }

        protected void rptPrimaryNav_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Item item = e.Item.DataItem as Item;
                Literal litNav = e.Item.FindControl("litNav") as Literal;
                HtmlAnchor lnkNav = e.Item.FindControl("lnkNav") as HtmlAnchor;
               

                if (lnkNav != null && litNav != null)
                {
                    
                    LinkField destinationField = item.Fields["Destination"];

                    lnkNav.HRef = Sitecore.Links.LinkManager.GetItemUrl(destinationField.TargetItem);

                    string overrideNavTitle = ItemHelper.GetFieldRawValue(item, "Override Navigation Title");


                    if (destinationField.IsInternal)
                    {
                        Item destinationItem = destinationField.TargetItem;
                        litNav.Text = ItemHelper.GetFieldRawValueOrDefault(destinationItem, "Navigation Title", "[Navigation Title]");
                        if (ContextItem.ID.Equals(destinationItem.ID) || ContextItem.Axes.GetAncestors().Any(i => i.ID.Equals(destinationItem.ID)))
                        {
                            HtmlControl liNav = (HtmlControl)e.Item.FindControl("liNav");
                            if (liNav != null)
                            {
                                liNav.Attributes.Add("class", "active");
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(overrideNavTitle))
                    {
                        litNav.Text = Server.HtmlEncode(overrideNavTitle);
                    }

                  
                }
            }

        }
    }
}