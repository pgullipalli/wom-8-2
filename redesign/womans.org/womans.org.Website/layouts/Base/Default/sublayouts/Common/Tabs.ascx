﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Tabs.ascx.cs" 
    Inherits="MedTouch.Base.layouts.modules.Base.Tabs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Tabs =============================================--> 
<%--<article>--%>
    <asp:Panel ID="pnlEditTabs" Visible="false" runat="server">
        <sc:EditFrame ID="editLinks" runat="server" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Tabs">
            <h3>
                Edit Tabs</h3>
        </sc:EditFrame>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlTabs" CssClass="responsive-tabs" Visible="False">
        <asp:Repeater runat="server" ID="rptTabs">
            <ItemTemplate>
                <h2><%#Eval("key") %></h2><div><%#Eval("value") %></div>
            </ItemTemplate>
        </asp:Repeater>
    </asp:Panel>
<%--</article>--%>
<!--============================================= /MedTouch.Base: Common: Tabs =============================================--> 