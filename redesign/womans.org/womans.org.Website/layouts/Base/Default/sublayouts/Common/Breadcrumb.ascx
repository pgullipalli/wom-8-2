﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.Base.layouts.modules.Base.BreadcrumbSublayout" CodeBehind="Breadcrumb.ascx.cs" %>
<!--============================================= MedTouch.Base: Common: Breadcrumb =============================================--> 
<div class="grid">
	<div class="breadcrumb-wrapper hide-on-mobile">
		<div class="breadcrumb">
			<a href="/" class="home">Home</a>
				<span class="separator"> &gt; </span>
				
			<asp:Repeater ID="rptBreadcrumb" runat="server" OnItemDataBound="rptBreadcrumb_ItemDataBound">
				<ItemTemplate>
					<asp:HyperLink ID="hlBreadcrumb" runat="server" /><asp:Literal ID="litBreadcrumb" runat="server" />
				</ItemTemplate>
				<SeparatorTemplate>
					<span class="separator"><asp:Literal ID="litSeparator" runat="server" /></span>
				</SeparatorTemplate>
			</asp:Repeater>
		</div>
	</div>
</div>
<!--============================================= /MedTouch.Base: Common: Breadcrumb =============================================--> 