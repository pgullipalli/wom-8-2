﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.Base.layouts.modules.Base.NotificationboxSublayout" CodeBehind="NotificationBox.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Notification Box =============================================--> 
<asp:Placeholder ID="phNotificationBox" runat="server" Visible="false">
    <section class="alert-box">
	    <div class="grid">
	        <sc:Text runat="server" ID="scNotificationText" DisableWebEditing="true"/>
	    </div>
    </section>
</asp:Placeholder>
<!--============================================= /MedTouch.Base: Common: Notification Box =============================================--> 
