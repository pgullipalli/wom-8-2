﻿namespace womans.org.Website.layouts.Base.Default.sublayouts.Common
{
    #region

    using System;
    using System.Linq;

    using MedTouch.Common.Helpers;
    using MedTouch.Common.UI;

    using Sitecore.Links;

    #endregion

    /// <summary>
    ///     Summary description for HeaderSublayout
    /// </summary>
    public partial class HeaderSublayout : BaseSublayout
    {
        #region Fields

        private readonly string _fieldName_header_browser_title = "Browser Title";

        private string dictionaryMenuTitleLabel;

        #endregion

        #region Properties

        protected string DictionaryMenuTitleLabel
        {
            get
            {
                if (this.dictionaryMenuTitleLabel == null)
                {
                    this.dictionaryMenuTitleLabel =
                        CultureHelper.GetDictionaryTranslation("Modules.Base.Default.Common.Header.MenuTitle");
                }

                return this.dictionaryMenuTitleLabel;
            }
        }

        #endregion

        #region Methods

        private void BindControl()
        {
            this.litMenuTitle.Text = this.DictionaryMenuTitleLabel;
            var isLogoOverride = false;
            var itm = this.ContextItem;
            //page level override
            if (this.scHeaderHighResLogoOverride != null && this.scHeaderLogoOverride != null)
            {
                var strLogoSvg = ItemHelper.GetFieldRawValue(itm, this.scHeaderHighResLogoOverride.Field);
                var strLogoPng = ItemHelper.GetFieldRawValue(itm, this.scHeaderLogoOverride.Field);
                if (!string.IsNullOrEmpty(strLogoPng) && !string.IsNullOrEmpty(strLogoSvg))
                {
                    this.scHeaderHighResLogoOverride.Item = itm;
                    this.scHeaderLogoOverride.Item = itm;
                    this.pnlLogo.Visible = true;
                    isLogoOverride = true;
                    this.plHomeLogo.Visible = false;
                }
            }
            if (!isLogoOverride)
            {
                itm = ItemHelper.GetStartItem();
                this.plLogoOverride.Visible = false;
                if (this.scHeaderHighResLogo != null && this.scHeaderLogo != null)
                {
                    var strLogoSvg = ItemHelper.GetFieldRawValue(itm, this.scHeaderHighResLogo.Field);
                    var strLogoPng = ItemHelper.GetFieldRawValue(itm, this.scHeaderLogo.Field);
                    if (!string.IsNullOrEmpty(strLogoPng) && !string.IsNullOrEmpty(strLogoSvg))
                    {
                        this.scHeaderHighResLogo.Item = itm;
                        this.scHeaderLogo.Item = itm;
                        this.pnlLogo.Visible = true;
                    }
                }

            }
            if (this.pnlLogo.Visible)
            {
                this.hplLogo.NavigateUrl = LinkManager.GetItemUrl(ItemHelper.GetStartItem());
                var strBrowserTitle = ItemHelper.GetFieldHtmlValue(itm, this._fieldName_header_browser_title);
                if (!string.IsNullOrWhiteSpace(strBrowserTitle))
                {
                    this.hplLogo.ToolTip = strBrowserTitle;
                }
            }

            this.scLinkDonate.Item = ItemHelper.GetStartItem();
            this.scLinkDonate.Text = CultureHelper.GetDictionaryTranslation("Womans.Common.DonateLinkLabel");
            this.ContextItem.Visualization.GetRenderings(Sitecore.Context.Device, false).ToList().ForEach(
                r =>
                    {
                        if (r.RenderingItem != null && r.RenderingItem.InnerItem != null
                            && r.RenderingItem.InnerItem.TemplateName.ToLower().Equals("sublayout")
                            && (r.RenderingItem.InnerItem.Name.Equals("Proximity Navigation") || r.RenderingItem.InnerItem.Name.Equals("Section")))
                        {
                            if (this.ContextItem.GetChildren().Any() || this.IsParentHasAValidProximityNavi())
                            {
                                this.pnlInThisSection.Visible = true;
                            }
                        }
                    });
        }

        private bool IsParentHasAValidProximityNavi()
        {
            var returnVal = false;

            this.ContextItem.Parent.Visualization.GetRenderings(Sitecore.Context.Device, false).ToList().ForEach(
                r =>
                    {
                        if (r.RenderingItem != null && r.RenderingItem.InnerItem != null
                            && r.RenderingItem.InnerItem.TemplateName.ToLower().Equals("sublayout")
                            && r.RenderingItem.InnerItem.Name.Equals("Proximity Navigation"))
                        {
                            returnVal = true;
                        }
                    });
            return returnVal;
        }

        private void Page_Load(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif
            this.BindControl();
#if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
#endif
        }

        #endregion
    }
}