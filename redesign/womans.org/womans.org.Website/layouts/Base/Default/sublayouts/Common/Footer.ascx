﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Footer.ascx.cs" Inherits="womans.org.Website.layouts.Base.Default.sublayouts.Common.Footer" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="wffm" Namespace="Sitecore.Form.Core.Renderings" Assembly="Sitecore.Forms.Core" %>


<style>

footer .scfSingleLineTextLabel
{
    display:none !important;
}

footer  .scfEmailLabel
{
    display:none !important;
}

footer .scfSingleLineTextBox
{
    margin-bottom: 5px !important;
    width: 100% !important;
}

footer .scfEmailTextBox
{
    margin-bottom: 5px !important;
    width: 100% !important;
}

footer .scfValidationSummary
{
    <%--display:none !important;--%>
    color:White !important;
}

footer .scfValidator
{
    color:White !important;
}

footer .scfSubmitButton 
{
     font-size: 0.9em;
    text-transform: uppercase;
    font-family: 'Open Sans', sans-serif;
    font-weight: 400;
    border: 0px;
    color: white;
     transition: .25s;
    -moz-transition: .25s;
    -webkit-transtion: .25s;
    -o-transition: .25s;
    -ms-transition: .25s;background-color: #464646;
    border: 1px solid #464646;
    float:right;
}

footer .scfSubmitButton:hover,
input[type="submit"].black:hover,
button.black:hover{
    background-color: #4e4e4e;
}

</style>


<!--============================================= MedTouch.Base: Common: Footer =============================================--> 
<footer>

		<div class="back-to-top grid">
			<a href="#top"></a>
		</div>
		<div class="latest-tweet grid">
				<!-- Placeholder, twitter will generate latest tweet here. -->
                <asp:Literal runat="server" ID="litTwitter"></asp:Literal>
		</div>
		<div class="footer-header grid">
			<div class="eight columns newsletter">
				<div class="border-wrapper">
					<div class="seven columns">
                        <asp:Literal runat="server" ID="litNewsletterIntro"></asp:Literal>
					</div>
					<div class="five columns signup">
                        <wffm:FormRender ID="FormRender1" FormID="{E182B8B8-29CC-4F76-B8C6-F092A1C1BC88}" runat="server"/>
					</div>
				</div>
			</div>
			<div class="four columns footer-donate">
				<div class="border-wrapper">
                    <asp:Literal runat="server" ID="litSupportWomansIntro"></asp:Literal>
                    
				</div>
			</div>
			
		</div>
		<div class="footer-main grid">
				<div class="four columns connect-with-womans">
                    <asp:Literal runat="server" ID="litFooterColumn1"></asp:Literal>

                    <div class="footer-content">
                    <sc:sublayout id="slSocial" runat="server" path="/layouts/womans/sublayouts/common/social.ascx" />
                    </div>
                </div>		
		    
                <div class="four columns womans-footer-nav">
                    <asp:Literal runat="server" ID="litFooterColumn2"></asp:Literal>
                </div>		
		
				<div class="four columns locations-and-maps">   
                    <asp:Literal runat="server" ID="litFooterColumn3"></asp:Literal>	
				</div>
            </div>
      
	
	
        <div class="footer-utility-links">
             <asp:Literal runat="server" ID="litFooterColumn4"></asp:Literal>
        <br />
        
        <ul>
            <li>
                <span><asp:Literal runat="server" ID="litCopyrightText"></asp:Literal></span>
            </li>
             <asp:Literal runat="server" ID="litFooterColumn5"></asp:Literal>
        </ul>

		</div>
	<sc:Image id="scFooterImage" runat="server" field="Footer Image" MaxWidth="250" />
	
    
</footer>
<!--============================================= /MedTouch.Base: Common: Footer =============================================--> 
