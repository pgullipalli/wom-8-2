﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrimaryNavigation.ascx.cs" Inherits="womans.org.Website.layouts.Base.Default.sublayouts.Common.PrimaryNavigation" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Primary Navigation =============================================--> 
<%--<script type="text/javascript">
    $(document).ready(function () {
        $(".top-nav li").first().addClass("first");
        $(".top-nav li").last().addClass("last");
    });
</script>--%>
<nav class="main-nav" role="navigation">
    <div class="grid">
        <ul class="twelve columns spread">  
            <asp:Repeater ID="rptPrimaryNav" runat="server" OnItemDataBound="rptPrimaryNav_OnItemDataBound">
                <ItemTemplate>
                   <li runat="server" ID="liNav">
                        <a runat="server" id="lnkNav"><asp:Literal ID="litNav" runat="server" /></a>
                        <sc:Link ID="scNav" runat="server" Field="Destination"></sc:Link>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</nav>
<!--============================================= /MedTouch.Base: Common: Primary Navigation =============================================-->