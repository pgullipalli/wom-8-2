﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace womans.org.Website.layouts.Base.Default.sublayouts.Common {
    
    
    public partial class HeaderSublayout {
        
        /// <summary>
        /// slSocial control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Sublayout slSocial;
        
        /// <summary>
        /// slquicklinks control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Sublayout slquicklinks;
        
        /// <summary>
        /// litMenuTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litMenuTitle;
        
        /// <summary>
        /// pnlLogo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlLogo;
        
        /// <summary>
        /// hplLogo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink hplLogo;
        
        /// <summary>
        /// plHomeLogo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder plHomeLogo;
        
        /// <summary>
        /// scHeaderHighResLogo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Image scHeaderHighResLogo;
        
        /// <summary>
        /// scHeaderLogo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Image scHeaderLogo;
        
        /// <summary>
        /// plLogoOverride control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder plLogoOverride;
        
        /// <summary>
        /// scHeaderHighResLogoOverride control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Image scHeaderHighResLogoOverride;
        
        /// <summary>
        /// scHeaderLogoOverride control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Image scHeaderLogoOverride;
        
        /// <summary>
        /// scLinkDonate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link scLinkDonate;
        
        /// <summary>
        /// scBasicSiteSearch control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Sublayout scBasicSiteSearch;
        
        /// <summary>
        /// slPrimaryNavigation control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Sublayout slPrimaryNavigation;
        
        /// <summary>
        /// pnlInThisSection control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlInThisSection;
        
        /// <summary>
        /// slider control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Placeholder slider;
        
        /// <summary>
        /// slNotificationbox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Sublayout slNotificationbox;
        
        /// <summary>
        /// grid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Placeholder grid;
    }
}
