﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.Base.layouts.modules.Base.VideocalloutSublayout" CodeBehind="VideoCallout.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Video Callout =============================================--> 
<asp:Panel ID="pnlCallout" runat="server" CssClass="callout">
    <div class="video">        
        <a id="aVideoLink" runat="server" name="modal">
            <sc:Image id="scImage" runat="server" field="Callout Image" />
            <span style="background-image: url('/assets/images/pd-video-overlay.png'); background-scale: cover;
                position: absolute; width: 120px; height: 100px; top: 25%; left: 25%;"></span>
        </a>
    </div>
    <h4><sc:Text runat="server" ID="scTxtTitle" Field="Callout Title" /></h4>
    <sc:Text runat="server" ID="scTxtDescription" Field="Callout Description" />
    <sc:link id="scLink" runat="server" field="Callout Link" CssClass="callout-button read-arrow" />
</asp:Panel>
<!--============================================= /MedTouch.Base: Common: Video Callout =============================================-->