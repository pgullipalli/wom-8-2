﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Forms.Core.Crm;
using Womans.CustomItems.PageTemplates.Common;
using MedTouch.Common.Helpers;

namespace womans.org.Website.layouts.Base.Default.sublayouts.Common
{
    public partial class ContentDisplay : MedTouch.Base.layouts.modules.Base.ContentdisplaySublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         
            if (base.ContextItem.TemplateID.ToString() == TwoColumnWideLeftGenericPageItem.TemplateId)
            {
                plcHeadline.Visible = false;
            }
            
            string strSubHeadline = ItemHelper.GetFieldRawValue(Sitecore.Context.Item, "Sub Headline");
            if (string.IsNullOrWhiteSpace(strSubHeadline))
            {
                h2SubHeadline.Visible = false;
            }
            else
            {
                h2SubHeadline.Visible = true;
            }
        }
    }
}