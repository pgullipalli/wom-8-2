﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"  Inherits="MedTouch.Base.layouts.Base.Default.sublayouts.Common.SitetoolSublayout" 
Codebehind="~/layouts/Base/Default/sublayouts/Common/SiteTool.ascx.cs" %>
<!--============================================= MedTouch.Base: Common: Site Tool =============================================--> 
<div class="tool-block">
	<div class="site-tools">					
		<ul class="tools-nav">	
			<li class="aaa"><a href="javascript:window.print()" class="print">Print</a><span class="text-size">Print</span></li>
						
				<li class="aaa">												
					<a class="addthis_button share" href="http://www.addthis.com/bookmark.php">
					<span class="st_sharethis_custom">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
					</a>
					<span class="text-size">Share</span>
					<script type="text/javascript">									var addthis_config = { "data_track_addressbar": false };</script>
					<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f79a6b70d002cd8"></script>								
				</li>
						
			<li class="aaa"><span class="text-size">Text Size:</span> <a href="#" class="zoom_out resetFont">A</a> | <a href="#" class="zoom_in increaseFont">A</a> | <a href="#" class="zoom_big increaseFont-more">A</a></li>
		</ul>
	</div>
</div>
<!--============================================= MedTouch.Base: Common: Site Tool =============================================--> 
