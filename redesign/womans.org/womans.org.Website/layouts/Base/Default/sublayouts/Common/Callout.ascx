﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.Base.layouts.modules.Base.CalloutSublayout" CodeBehind="Callout.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Callout =============================================--> 
<asp:Panel ID="pnlCallout" runat="server" CssClass="callout">
    <section class="inner">
		<div class="gray-bg">
			<h4><sc:text id="scTitle" runat="server" field="Callout Title" /></h4>
			<sc:text id="scCopy" runat="server" field="Callout Copy" />
			<sc:link id="scLink" runat="server" field="Callout Link" CssClass="callout-button read-arrow" />
		</div>				
	</section>
</asp:Panel>
<!--============================================= /MedTouch.Base: Common: Callout =============================================--> 