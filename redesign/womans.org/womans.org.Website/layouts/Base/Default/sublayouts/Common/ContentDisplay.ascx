﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentDisplay.ascx.cs" Inherits="womans.org.Website.layouts.Base.Default.sublayouts.Common.ContentDisplay" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Common: Content Display =============================================--> 
<article>
    <asp:Panel ID="pnlPropEdit" Visible="false" runat="server">
        <sc:EditFrame ID="editLinks" runat="server" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Content Properties">
            <h3>
                Edit Content Properties</h3>
        </sc:EditFrame>
    </asp:Panel>
    <asp:PlaceHolder ID="phHeadline" runat="server">
        <asp:Placeholder runat="server" ID="plcHeadline">
        <h1>
            <sc:Text ID="scHeadline" runat="server" Field="Headline" />
        </h1>
		</asp:Placeholder>

		<h2 id="h2SubHeadline" runat="server"><sc:Text ID="scIntroduction" runat="server" Field="Sub Headline" /></h2>
		
    </asp:PlaceHolder>
    <sc:Text ID="scContentCopy" runat="server" Field="Content Copy" />


    <sc:sublayout id="slAccordion" runat="server" path="/layouts/base/default/sublayouts/common/accordion.ascx" />

    <sc:sublayout id="slTab" runat="server" path="/layouts/base/default/sublayouts/common/tabs.ascx" />



    
</article>
<!--============================================= /MedTouch.Base: Common: Content Display =============================================--> 