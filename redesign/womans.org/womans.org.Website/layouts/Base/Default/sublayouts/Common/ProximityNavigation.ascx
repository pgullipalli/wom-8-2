﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProximityNavigation.ascx.cs" 
    Inherits="MedTouch.Base.layouts.modules.Base.ProximityNavigationSublayout" %>
<!--============================================= MedTouch.Base: Common: Proximity Navigation =============================================-->
<!-- Mobile CODE: added .mobile-menu -->
<div class="its">
    <a href="#" class="prox"><span class="icon"></span>In This Section</a>
</div>

<nav class="left-nav nav-collapse collapse" role="navigation">
    <asp:Panel ID="pnlNavTitle" runat="server" CssClass="leftnav-title" Visible="false">
        <asp:HyperLink ID="hlNavTitle" runat="server" />
    </asp:Panel>
    <asp:Literal ID="litNav" runat="server" />
</nav>
<!--============================================= /MedTouch.Base: Common: Proximity Navigation =============================================-->