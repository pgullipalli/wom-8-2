﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using System.Text;
using System.Security.Cryptography;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace womans.org.Website.layouts.Base.Default.sublayouts.Common
{
    public partial class Footer : Layouts.Footer.FooterSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Item itm = MedTouch.Common.Helpers.ItemHelper.GetStartItem();
            litSupportWomansIntro.Text = FieldRenderer.Render(itm, "Support Womans Intro");
            litNewsletterIntro.Text = FieldRenderer.Render(itm, "Newsletter Intro");

            litFooterColumn3.Text = FieldRenderer.Render(itm, "Footer Column 3");
            litFooterColumn1.Text = FieldRenderer.Render(itm, "Footer Column 1");
            litFooterColumn2.Text = FieldRenderer.Render(itm, "Footer Column 2");
            litFooterColumn4.Text = FieldRenderer.Render(itm, "Footer Column 4");
            litFooterColumn5.Text = FieldRenderer.Render(itm, "Footer Column 5");
            litCopyrightText.Text = FieldRenderer.Render(itm, "Copyright Text");
            
            try
            {
                int tweetCount = string.IsNullOrEmpty(ItemHelper.GetFieldRawValue(itm, "Tweet Count")) ? 1 : Convert.ToInt32(ItemHelper.GetFieldRawValue(itm, "Tweet Count"));
                if (!string.IsNullOrEmpty(FieldRenderer.Render(itm, "OAuth Consumer Key")) && !string.IsNullOrEmpty(FieldRenderer.Render(itm, "OAuth Consumer Secret")) && !string.IsNullOrEmpty(FieldRenderer.Render(itm, "OAuth Token")) && !string.IsNullOrEmpty(FieldRenderer.Render(itm, "OAuth Token Secret")))
                {
                    litTwitter.Text = buildTweetFromJson(getTwitter(), tweetCount);
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Footer: Unable to load Twitter timeline", ex, this);
                litTwitter.Visible = false;

            }
        }

        protected void btnDonate_Click(object sender, EventArgs e)
        {

        }

        protected string getTwitter()
        {
            string twitter = string.Empty;

            if (Cache["Twitter"] == null)
            {
                Item twitterVars = MedTouch.Common.Helpers.ItemHelper.GetStartItem();
                var twitterName = FieldRenderer.Render(twitterVars, "Twitter Account Name");
                string resource_url = "https://api.twitter.com/1.1/statuses/user_timeline.json"; //string.Empty; //Fill in with string to woman's twitter feed
                string q = string.Empty; //fill in with query string
                Cache.Insert("Twitter", findUserTwitter(resource_url, q), null, DateTime.Now.AddMinutes(60), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return Cache["Twitter"].ToString();
        }

        private string findUserTwitter(string resource_url, string q)
        {

            // oauth application keys
            //TODO: Set these up as config values
            Item twitterVars = MedTouch.Common.Helpers.ItemHelper.GetStartItem();
            
            var oauth_token = ItemHelper.GetFieldRawValue(twitterVars, "OAuth Token"); //"insert here...";
            var oauth_token_secret = ItemHelper.GetFieldRawValue(twitterVars, "OAuth Token Secret"); //"insert here...";
            var oauth_consumer_key = ItemHelper.GetFieldRawValue(twitterVars, "OAuth Consumer Key"); // = "insert here...";
            var oauth_consumer_secret = ItemHelper.GetFieldRawValue(twitterVars, "OAuth Consumer Secret"); // = "insert here...";

            // oauth implementation details
            var oauth_version = "1.0";
            var oauth_signature_method = "HMAC-SHA1";

            // unique request details
            var oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            var timeSpan = DateTime.UtcNow
                - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();


            // create oauth signature
            var baseFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                            "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&q={6}";

            var baseString = string.Format(baseFormat,
                                        oauth_consumer_key,
                                        oauth_nonce,
                                        oauth_signature_method,
                                        oauth_timestamp,
                                        oauth_token,
                                        oauth_version,
                                        Uri.EscapeDataString(q)
                                        );

            baseString = string.Concat("GET&", Uri.EscapeDataString(resource_url), "&", Uri.EscapeDataString(baseString));

            var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret),
                                    "&", Uri.EscapeDataString(oauth_token_secret));

            string oauth_signature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oauth_signature = Convert.ToBase64String(
                    hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }

            // create the request header
            var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                               "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                               "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
                               "oauth_version=\"{6}\"";

            var authHeader = string.Format(headerFormat,
                                    Uri.EscapeDataString(oauth_nonce),
                                    Uri.EscapeDataString(oauth_signature_method),
                                    Uri.EscapeDataString(oauth_timestamp),
                                    Uri.EscapeDataString(oauth_consumer_key),
                                    Uri.EscapeDataString(oauth_token),
                                    Uri.EscapeDataString(oauth_signature),
                                    Uri.EscapeDataString(oauth_version)
                            );



            ServicePointManager.Expect100Continue = false;

            // make the request
            var postBody = "q="+Uri.EscapeDataString(q);//
            resource_url += "?" + postBody;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
            request.Headers.Add("Authorization", authHeader);
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            var response = (HttpWebResponse)request.GetResponse();
            var reader = new StreamReader(response.GetResponseStream());
            var objText = reader.ReadToEnd();
            return objText;/**/
            string html = "";
            /*
            try
            {
                JArray jsonDat = JArray.Parse(objText);
                for (int x = 0; x < jsonDat.Count(); x++)
                {
                    html += jsonDat[x]["text"].ToString();
                }
                return html;
            }
            catch (Exception twit_error)
            {
                return html + twit_error.ToString();
            }
             * */
        }

        private string buildTweetFromJson(string twitterData,int tweetCount)
        {
            StringBuilder tweets = new StringBuilder();
            try
            {
                JArray jsonDat = JArray.Parse(twitterData);
                for (int x = 0; x < tweetCount; x++)
                {
                    string tweet = jsonDat[x]["text"].ToString();
                    string tweetwURL = TweetParser.ParseURL(tweet); //Regex.Replace(tweet, @"(http(s)?://)?([\w-]+\.)+[\w-]+(/\S\w[\w- ;,./?%&=]\S*)?", "<a href=\"$1\" target='_blank'>$1</a>");
                    string tweetwHash = TweetParser.ParseHashtag(tweetwURL);//Regex.Replace(tweetwURL, "(#)((?:[A-Za-z0-9-_]*))", "<a href='http://www.twitter.com/hashtag/$1' target='_blank'>$1</a>"); ;
                    string tweetwUser = TweetParser.ParseUsername(tweetwHash);
                    tweets.Append("<div><p>"+tweetwUser+"</p></div>");
                }
            }
            catch (Exception twit_error)
            {
                //tweets.Append("<div>" + twit_error.Message + "</div>");
            }
            return tweets.ToString();
        }

        public static class TweetParser
        {
            public static string ParseURL(string s)
            {
                return Regex.Replace(s, @"(http(s)?://)?([\w-]+\.)+[\w-]+(/\S\w[\w- ;,./?%&=]\S*)?", new MatchEvaluator(TweetParser.URL));
            }
            public static string ParseUsername(string s)
            {
                return Regex.Replace(s, "(@)((?:[A-Za-z0-9-_]*))", new MatchEvaluator(TweetParser.Username));
            }
            public static string ParseHashtag(string s)
            {
                return Regex.Replace(s, "(#)((?:[A-Za-z0-9-_]*))", new MatchEvaluator(TweetParser.Hashtag));
            }
            private static string Hashtag(Match m)
            {
                string x = m.ToString();
                string tag = x.Replace("#", "%23");
                return "<a href='http://twitter.com/search?q="+tag+"' target='_blank'>" + x+"</a>";
            }
            private static string Username(Match m)
            {
                string x = m.ToString();
                string username = x.Replace("@", "");
                return "<a href='http://twitter.com/" + username+"' target='_blank'>@"+username+"</a>";
            }
            private static string URL(Match m)
            {
                bool endLink = m.ToString().Contains("\"");
                string x = m.ToString();
                return endLink ? "<a href='" + x.Replace("\"", "") + "' target='_blank'>" + x.Replace("\"", "") + "</a>\"" : "<a href='" + x + "' target='_blank'>" + x + "</a>";
            }
        }
    }
}