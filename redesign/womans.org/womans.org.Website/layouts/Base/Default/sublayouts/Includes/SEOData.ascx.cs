﻿using System;
using System.Web.UI.HtmlControls;
using MedTouch.Common.Helpers;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Sitecore.Web.UI.WebControls;

namespace womans.org.Website.layouts.Base
{

    /// <summary>
    /// Summary description for SeodataSublayout
    /// </summary>
    public partial class SeodataSublayout : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif

            BindControls();

#if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
#endif
        }

        private void BindControls()
        {
            if (!IsPostBack)
            {
                LinkField redirect = Sitecore.Context.Item.Fields["Redirect Link"];
                if (redirect != null && !String.IsNullOrEmpty(redirect.Value))
                {
                    switch (redirect.LinkType)
                    {
                        case "internal":
                            Response.Redirect(LinkManager.GetItemUrl(redirect.TargetItem), false);
                            break;
                        case "external":
                            Response.Redirect(redirect.Url, false);
                            break;
                        case "media":
                            MediaItem media = new MediaItem(redirect.TargetItem);
                            Response.Redirect(Sitecore.StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(media)), false);
                            break;
                        //case "mailto":
                        //    Response.Redirect(redirect.Url, false);
                        //    break;

                    }
                }
            }

            pnlSEOEdit.Visible = Sitecore.Context.PageMode.IsPageEditor;
            Instantiate();
        }

        private void Instantiate()
        {
            var contextItem = Sitecore.Context.Item;
            if (contextItem != null)
            {
                string browserTitle = contextItem["Browser Title"];
                string metaDescription = contextItem["Meta Description"];
                CheckboxField noFollowField = (CheckboxField)contextItem.Fields["Meta Robots NOFOLLOW"];
                CheckboxField noIndexField = (CheckboxField)contextItem.Fields["Meta Robots NOINDEX"];
                ReferenceField canonicalField = (ReferenceField)contextItem.Fields["Canonical URL"];

                if (browserTitle != null)
                {
                    string browserTitleFormat = ItemHelper.GetFieldRawValue(ItemHelper.GetStartItem(), "Browser Title Format");
                    htTitle.Text = Server.HtmlEncode(string.IsNullOrEmpty(browserTitleFormat)
                                                            ? browserTitle
                                                            : string.Format(browserTitleFormat, browserTitle));
                    hmTitle.Content = Server.HtmlEncode(string.IsNullOrEmpty(browserTitleFormat)
                                                            ? browserTitle
                                                            : string.Format(browserTitleFormat, browserTitle));
                    hmTitle.Name = "title";
                    hmTitle.Visible = true;
                }

                if (!string.IsNullOrWhiteSpace(metaDescription))
                {
                    hmDescription.Name = "description";
                    hmDescription.Content = metaDescription;
                    hmDescription.Visible = true;
                }

                if (noIndexField != null && noFollowField != null)
                {
                    if (noIndexField.Checked && noFollowField.Checked)
                    {
                        //litMetaRobots.Text = "<meta name=\"robots\" content=\"NOINDEX, NOFOLLOW\" />";
                        hmRobots.Name = "robots";
                        hmRobots.Content = "NOINDEX, NOFOLLOW";
                        hmRobots.Visible = true;
                    }
                    else if (!noIndexField.Checked && noFollowField.Checked)
                    {
                        //litMetaRobots.Text = "<meta name=\"robots\" content=\"INDEX, NOFOLLOW\" />";
                        hmRobots.Name = "robots";
                        hmRobots.Content = "INDEX, NOFOLLOW";
                        hmRobots.Visible = true;
                    }
                    else if (noIndexField.Checked && !noFollowField.Checked)
                    {
                        //litMetaRobots.Text = "<meta name=\"robots\" content=\"NOINDEX, FOLLOW\" />";
                        hmRobots.Name = "robots";
                        hmRobots.Content = "NOINDEX, FOLLOW";
                        hmRobots.Visible = true;
                    }
                }

                var url = Request.Url;
                string absolutePath = (canonicalField != null && canonicalField.TargetItem != null ?
                                    LinkManager.GetItemUrl(canonicalField.TargetItem) :
                                    url.AbsolutePath) ?? string.Empty;
                absolutePath = absolutePath.Trim('/');
                if (!string.IsNullOrEmpty(absolutePath))
                    absolutePath = "/" + absolutePath;
                litCanonical.Text = string.Format("<link rel=\"canonical\" href=\"{0}://{1}{2}\" />",
                                                      url.Scheme, url.Host.TrimEnd('/'), absolutePath);
            }
        }
    }
}