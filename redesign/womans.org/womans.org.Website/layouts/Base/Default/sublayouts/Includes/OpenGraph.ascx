﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.Base.layouts.modules.Base.OpenGraphSublayout" CodeBehind="OpenGraph.ascx.cs" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI.HtmlControls" Assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<!--============================================= MedTouch.Base: Include: Open Graph =============================================-->

    <asp:Literal runat="server" ID="litTwitterCard" Visible="false"></asp:Literal>
    <asp:Literal runat="server" ID="litTwitterSite" Visible="false"></asp:Literal>
    <asp:Literal runat="server" ID="litTwitterCreator" Visible="false"></asp:Literal>
    
    <asp:Literal runat="server" ID="litOGTitle" Visible="false"></asp:Literal>
    <asp:Literal runat="server" ID="litOGType" Visible="false"></asp:Literal>
    <asp:Literal runat="server" ID="litOGDescription" Visible="false"></asp:Literal>
    <asp:Literal runat="server" ID="litOGUrl" Visible="false"></asp:Literal>
    <asp:Literal runat="server" ID="litOGSiteName" Visible="false"></asp:Literal>
    <asp:Literal runat="server" ID="litOGImage" Visible="false"></asp:Literal>    

<!--============================================= /MedTouch.Base: Include: Open Graph =============================================-->



