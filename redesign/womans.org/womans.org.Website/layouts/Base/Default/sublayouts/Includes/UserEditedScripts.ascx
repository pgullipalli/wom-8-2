﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserEditedScripts.ascx.cs"
    Inherits="MedTouch.Base.layouts.Base.Default.sublayouts.Includes.UserEditedScripts" %>
<!--============================================= MedTouch.Base: Include: User Edited Script =============================================-->
<asp:Placeholder runat="server" ID="phScript" Visible="false">
    <asp:Literal runat="server" ID="ltrlScript"></asp:Literal>
</asp:Placeholder>
<!--============================================= /MedTouch.Base: Include: User Edited Script =============================================-->
