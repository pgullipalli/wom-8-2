﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.Base.layouts.modules.Base.SeodataSublayout" CodeBehind="SEOData.ascx.cs" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI.HtmlControls" Assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--============================================= MedTouch.Base: Include: SEO Data ========================================-->
<asp:Panel ID="pnlSEOEdit" Visible="false" runat="server">
    <sc:EditFrame ID="editLinks" runat="server" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/SEO Data">
        <span class="page-editor-edit-button">[Edit SEO Data]</span>
    </sc:EditFrame>
</asp:Panel>

<asp:HtmlTitle ID="htTitle" runat="server"></asp:HtmlTitle>
<asp:HtmlMeta ID="hmTitle" runat="server" Visible="false" />
<asp:HtmlMeta ID="hmKeywords" runat="server" Visible="false" />
<asp:HtmlMeta ID="hmDescription" runat="server" Visible="false" />
<asp:HtmlMeta ID="hmRobots" runat="server" Visible="false" />
<asp:HtmlMeta ID="hmCopyright" runat="server" Visible="false" />
<asp:Literal ID="litCanonical" runat="server" />
<!--============================================= /MedTouch.Base: Include: SEO Data ========================================-->