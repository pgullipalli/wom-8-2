﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalUtilityNavigation.ascx.cs"
    Inherits="MedTouch.Base.usercontrols.modules.Base.GlobalUtilityNavigation" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<asp:Repeater ID="rptGlobalUtilNav" runat="server" OnItemDataBound="rptGlobalUtilNav_OnItemDataBound">
    <HeaderTemplate>
        <aside class="tools">
            <ul>
    </HeaderTemplate>
    <ItemTemplate>
                <li>
                    <sc:Link ID="scNav" runat="server" Field="Destination">
                        <asp:Literal ID="litNav" runat="server" />
                    </sc:Link>
                </li>
    </ItemTemplate>
    <FooterTemplate>
            </ul>
        </aside>
    </FooterTemplate>
</asp:Repeater>

