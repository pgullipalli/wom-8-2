﻿<%@ Import Namespace="Sitecore.Analytics" %>

<%@ Page Language="c#" Inherits="System.Web.UI.Page" CodePage="65001" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head runat="server">

<!-- GOOGLE FONTS -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
    
    <sc:sublayout id="slSeoData" runat="server" path="/layouts/Base/Default/sublayouts/Includes/SeoData.ascx" />   

	
	<meta name="google-site-verification" content="0cAq1Rtvm4CpM1rXKh9yLVUomUAcQc0-d4NuAJfz0_o" />



	
	<!-- Mobile Device Scale/Zoom -->	
    <!--meta name="viewport" content="width=device-width"-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

   <meta name="Copyright" content="">
	
	<!-- Mobile Device Scale/Zoom -->	
    <!--meta name="viewport" content="width=device-width"-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <!-- Fav Icons -->
	<link rel="shortcut icon" href="/assets/images/favicon.ico?v=2">
	<%--<link rel="apple-touch-icon" href="/assets/images/apple-touch-icon.png">--%>

	<!-- Stylesheets -->
    <link rel="stylesheet" href="/assets/css/styles.css">
	<link rel="stylesheet" href="/assets/css/modules.css">	
	<link rel="stylesheet" href="/assets/css/mobile.css">	
	

	<!--<script src="/assets/js/prefixfree.min.js"></script>-->
        <!--[if lt IE 9]>
           <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>	
        <![endif]-->

	
     <%--<sc:sublayout id="slOpenGraph" runat="server" path="/layouts/Base/Default/sublayouts/Includes/OpenGraph.ascx" />--%>   

    <sc:visitoridentification runat="server" />


    <sc:sublayout id="slHeadScripts" runat="server" path="/layouts/Base/Default/sublayouts/Includes/UserEditedScripts.ascx" parameters="Field=Head Script" />
</head>
<body runat="server" id="PageBody">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NPP246"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NPP246');</script>
<!-- End Google Tag Manager -->

    <sc:sublayout id="slOpenBodyScripts" runat="server" path="/layouts/Base/Default/sublayouts/Includes/UserEditedScripts.ascx" parameters="Field=Open Body Script" />
    <form method="post" runat="server" id="mainform">    
        <asp:ScriptManager ID="spListScriptManager" runat="server" />

        <sc:placeholder ID="Placeholder1" key="Top" runat="server" />

        <sc:placeholder ID="Placeholder2" key="Main" runat="server" />

        <sc:placeholder ID="Placeholder3" key="Bottom" runat="server" />

        
    </form>
    
	<script src="/assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>		
	<!-- Grab Google CDN jQuery. fall back to local if necessary -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>	    !window.jQuery && document.write('<script src="/assets/js/jquery-1.9.1.min.js"><\/script>')</script>
	<!-- Grab Google CDN jQuery UI. fall back to local if necessary -->
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script>	    !window.jQuery.ui && document.write('<script src="/assets/js/jquery-ui-1.10.4.min.js"><\/script>')</script>
    <script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>
	<!-- Local Scripts -->
    <script src="/assets/js/wffmscript.js"></script>
	<script src="/assets/js/plugins.js"></script>
	<script src="/assets/js/tipsy.js"></script>
	<script src="/assets/js/script.js"></script>
    
    
    <sc:sublayout id="slCloseBodyScripts" runat="server" path="/layouts/Base/Default/sublayouts/Includes/UserEditedScripts.ascx"  parameters="Field=Close Body Script"/>
    
</body>
</html>