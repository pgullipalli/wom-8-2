﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpcomingEventsCallOut.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.calendar.UpcomingEventsCallOut" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: Calendar: Upcoming Session Events =============================-->
<asp:Panel ID="pnlListing" runat="server" CssClass="" >
    <div class="callout-wrapper">
        <div class="callout">
        <h4>
			<span><asp:Literal ID="litUpcomingEventsHeader" runat="server" /></span>
		</h4>
        <asp:Repeater ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_OnItemDataBound">
            <ItemTemplate>
			    <div class="callout-event">
                    <div class="event-header">
                        <asp:HyperLink ID="hlItem" runat="server" />
                    </div>
                    <p class="callout-event-preview">
                        <asp:Literal ID="litTeaser" runat="server" />
                        <asp:HyperLink ID="hlMoreLink" CssClass="read-more" runat="server" />
                    </p>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Button ID="btnViewMore" runat="server" CssClass="default-button pink inset" Visible="false"/>
        </div>
        </div>
</asp:Panel>
<!--==================== /Module: Calendar: Upcoming Session Events =============================-->