﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="UpcomingEventsHome.ascx.cs"
    Inherits="womans.org.Website.layouts.womans.sublayouts.calendar.UpcomingEventsHome" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: Calendar: Upcoming Session Events =============================-->
<h2 class="home-accordion-handle">
    <asp:Literal ID="litUpcomingEventsHeader" runat="server" />
    <span class="toggle-icon"></span>
</h2>
<asp:Panel ID="pnlListing" runat="server" CssClass="home-accordion-content" >
    <asp:Repeater ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_OnItemDataBound">
        <HeaderTemplate>
            <div class="callout-wrapper">
                <div class="callout">
        </HeaderTemplate>
        <ItemTemplate>
			<div class="callout-event">
                <div class="event-header">
                    <asp:HyperLink ID="hlItem" runat="server" />
                </div>
                <p class="callout-event-preview">
                    <asp:Literal ID="litTeaser" runat="server" />
                    <asp:HyperLink ID="hlMoreLink" CssClass="read-more" runat="server" />
                </p>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            <%--<asp:Button ID="btnViewMore" runat="server" CssClass="default-button pink inset" Visible="false"/>--%>
            <a id="aViewMore" runat="server" class="default-button pink inset" Visible="false" />
            </div>
            </div>
        </FooterTemplate>
    </asp:Repeater>
</asp:Panel>
<!--==================== /Module: Calendar: Upcoming Session Events =============================-->