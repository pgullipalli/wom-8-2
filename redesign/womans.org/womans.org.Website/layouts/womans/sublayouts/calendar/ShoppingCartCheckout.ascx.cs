﻿#region

using Cart = womans.org.BusinessLogic.Helpers.WomansCart;

#endregion

namespace womans.org.Website.layouts.womans.sublayouts.calendar
{
    #region

    using System;

    using MedTouch.Calendar.Helpers;
    using MedTouch.Common.Helpers;

    using Sitecore.Links;

    using global::womans.org.BusinessLogic.Helpers;

    using Sitecore.Data.Items;

    #endregion

    public partial class ShoppingCartCheckout : MedTouch.Calendar.layouts.modules.Calendar.Default.ShoppingCartCheckout
    {
        #region Fields

        private string dictionaryFitnessMemberError;

        private string dictionaryFitnessMemberLabel;

        private string dictionaryFitnessMemberWatermark;

        #endregion

        #region Properties

        protected string DictionaryFitnessMemberError
        {
            get
            {
                if (this.dictionaryFitnessMemberError == null)
                {
                    this.dictionaryFitnessMemberError =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.Calendar.ShoppingCartCheckout.FitnessMemberError");
                }
                return this.dictionaryFitnessMemberError;
            }
        }

        protected Item RenderingParamSearchPage
        {
            get
            {
                return this.GetSingleReferenceProperty("EventSearchPage");
            }
        }

        protected string DictionaryFitnessMemberLabel
        {
            get
            {
                if (this.dictionaryFitnessMemberLabel == null)
                {
                    this.dictionaryFitnessMemberLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.Calendar.ShoppingCartCheckout.FitnessMemberLabel");
                }
                return this.dictionaryFitnessMemberLabel;
            }
        }

        protected string DictionaryFitnessMemberWatermark
        {
            get
            {
                if (this.dictionaryFitnessMemberWatermark == null)
                {
                    this.dictionaryFitnessMemberWatermark =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.Calendar.ShoppingCartCheckout.FitnessMemberWatermark");
                }
                return this.dictionaryFitnessMemberWatermark;
            }
        }

        #endregion

        #region Methods

        protected override void BindControl()
        {
            // Bind base shopping cart controls
            base.BindControl();

            // If any fitness club pricing, show fitness member number entry
            var cart = ModuleHelper.ShoppingCart as Cart;
            var bRequireFitnessClub = cart.HasFitnessPricing;
            this.pnlAddFitnessMemberEntry.Visible = bRequireFitnessClub;
            this.btnRegisterFitnessClub.Visible = bRequireFitnessClub;
            this.hlRegister.Visible = !bRequireFitnessClub;
            this.hlAddClasses.NavigateUrl = ItemHelper.GetItemUrl(RenderingParamSearchPage);
            this.hlAddClasses.Text =
                CultureHelper.GetDictionaryTranslation("Modules.Calendar.ShoppingCartCheckout.AddClassesLabel");
            if (bRequireFitnessClub)
            {
                this.btnRegisterFitnessClub.Text = this.DictionaryRegisterLink;
                this.litFitnessMember.Text = this.DictionaryFitnessMemberLabel;
                this.litErrFitnessClub.Text = this.DictionaryFitnessMemberError;
                if (!this.IsPostBack)
                {
                    if (!string.IsNullOrEmpty(cart.FitnessMemberNumber))
                    {
                        this.txtFitnessMember.Text = cart.FitnessMemberNumber;
                    }
                    else
                    {
                        UIHelper.SetTextboxWatermark(this.txtFitnessMember, "", this.DictionaryFitnessMemberWatermark);
                    }
                }
            }
        }

        protected void btnRegisterFitnessClub_Click(object sender, EventArgs e)
        {
            // validate fitness club membership before continuing to registration form
            if (CalendarHelper.CheckFitnessMember(this.txtFitnessMember.Text))
            {
                var cart = ModuleHelper.ShoppingCart as WomansCart;
                cart.FitnessMemberNumber = this.txtFitnessMember.Text;
                var paymentPageURL = this.hlRegister.NavigateUrl;
                if (!string.IsNullOrEmpty(paymentPageURL))
                {
                    this.Response.Redirect(paymentPageURL);
                }
                // shouldn't happen - but, just in case
                var this_url = LinkManager.GetItemUrl(Sitecore.Context.Item);
                this.Response.Redirect(this_url);
            }
            // Invalid fitness club #
            this.pnlFitnessMemberError.Visible = true;
        }

     
        

        #endregion

        protected void btnAddClasses_OnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}