﻿using MedTouch.Calendar.Helpers.ObjectModel;
using Sitecore;

namespace womans.org.Website.layouts.womans.sublayouts.calendar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using MedTouch.Calendar.Helpers;
    using MedTouch.Calendar.Search;
    using MedTouch.Common.Helpers;
    using MedTouch.Common.UI;
    using Sitecore.Data.Items;

    public partial class UpcomingEventsCallOut : BaseSublayout
    {
        #region Fields
        // Default Value of UpcomingEvent Control.
        private const int DefaultNumberOfDisplayPages = 1;
        private const string DefaultCurrentPage = "1";
        private string viewAllUrl = string.Empty;

        private int? renderingParamMaxTeaserCharacters;
        private int? renderingParamNumberOfDisplayedItems;
        private bool? renderingParamRecursiveTaxonomy;
        private bool? renderingParamShowTitleAsLink;
        private bool? renderingParamShowLinkToDetailPage;
        private Item renderingParamViewAllLink;
        #endregion

        #region Dictionaries
        protected readonly string DictionaryUpcomingEventsLabel = CultureHelper.GetDictionaryTranslation("Modules.Calendar.UpcomingEventsCallout.UpcomingEventsLabel");
        protected readonly string DictionaryReadMoreLink = CultureHelper.GetDictionaryTranslation("Modules.Calendar.UpcomingEventsCallout.ReadMoreLink");
        protected readonly string DictionaryViewMore = CultureHelper.GetDictionaryTranslation("Modules.Calendar.UpcomingEventsCallout.ViewMore");
        #endregion Dictionaries

        #region RenderingParameters

        protected int RenderingParamNumberOfDisplayedItems
        {
            get
            {
                if (!this.renderingParamNumberOfDisplayedItems.HasValue)
                {
                    this.renderingParamNumberOfDisplayedItems = this.GetIntProperty("Number of Displayed Items");
                }

                return this.renderingParamNumberOfDisplayedItems.GetValueOrDefault(1);
            }
        }

        protected int RenderingParamMaxTeaserCharacters
        {
            get
            {
                if (!this.renderingParamMaxTeaserCharacters.HasValue)
                {
                    this.renderingParamMaxTeaserCharacters = this.GetIntProperty("Max Teaser Characters");
                }

                return this.renderingParamMaxTeaserCharacters.GetValueOrDefault(100);
            }
        }

        protected bool RenderingParamShowTitleAsLink
        {
            get
            {
                if (!this.renderingParamShowTitleAsLink.HasValue)
                {
                    this.renderingParamShowTitleAsLink = this.GetBooleanProperty("Show Title as Link");
                }

                return this.renderingParamShowTitleAsLink.GetValueOrDefault(true);
            }
        }

        protected bool RenderingParamShowLinkToDetailPage
        {
            get
            {
                if (!this.renderingParamShowLinkToDetailPage.HasValue)
                {
                    this.renderingParamShowLinkToDetailPage = this.GetBooleanProperty("Show Link to Detail Page");
                }

                return this.renderingParamShowLinkToDetailPage.GetValueOrDefault(true);
            }
        }

        protected bool RenderingParamRecursiveTaxonomy
        {
            get
            {
                if (!this.renderingParamRecursiveTaxonomy.HasValue)
                {
                    this.renderingParamRecursiveTaxonomy = this.GetBooleanProperty("RecursiveTaxonomy");
                }

                return this.renderingParamRecursiveTaxonomy.GetValueOrDefault(true);
            }
        }

        protected Item RenderingParamViewAllLink
        {
            get
            {
                if (this.renderingParamViewAllLink == null)
                {
                    this.renderingParamViewAllLink = this.GetSingleReferenceProperty("ViewAllLink");
                }

                return this.renderingParamViewAllLink;
            }
        }
        #endregion Properties

        protected void Page_Load(object sender, EventArgs e)
        {
            #if (!DEBUG)
            try
            {
            #endif
                Initialize();
            #if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
            #endif
        }

        protected override void Bind()
        {
            if (ContextItem == null)
            {
                return;
            }

            string taxonomyItems = ItemHelper.GetShortIds(ContextItem, "Taxonomy Terms");
            string taxonomy = SitecoreSearchHelper.GetTagTaxonomyGuidsFromShortIds(taxonomyItems, this.RenderingParamRecursiveTaxonomy);

            string specialtyItems = ItemHelper.GetFieldRawValue(ContextItem, "Specialties");
            string locationItems = ItemHelper.GetFieldRawValue(ContextItem, "Locations");

            var searchResultsItem = ModuleHelper.GetSearchResultPageItem(RenderingParamViewAllLink);
            bool displayAllContent = false;

            // Types of events to list: Dated (default) or Ongoing
            EventListType eventListType =
                Request.QueryString[ModuleHelper.Config.QueryString.GetEventTypeQuerystring()] == "Ongoing"
                    ? EventListType.Ongoing
                    : EventListType.Dated;
            //https://medtouch.attask-ondemand.com/task/view?ID=53ff325800af32ab990c261135be2eaf
            //The ongoing events shouldn't show on the home page or any "upcoming" events. 

            // Search Param
            var eventSearchParam = new CalendarSearchParam()
                {
                    Keyword = string.Empty,
                    Category = string.Empty,
                    DateFrom = DateTime.Now.ToString(ModuleHelper.Config.DefaultDateFormat()),
                    DateTo = string.Empty,
                    SearchType = CalendarSearchType.EventSession,
                    ListType = eventListType,
                    IncludeOngoing = false,//do not include ongoing events regardless it has a start date or not.
                    Locations = locationItems,
                    Specialties = specialtyItems,
                    Taxonomy = taxonomy,
                    IsTaxonomyRecursive = this.RenderingParamRecursiveTaxonomy
                };

            List<Item> items = new List<Item>();

            int totalCount = 0;
            int startItem = 0;
            int endItem = 0;

            // Search
            ModuleHelper.CalendarSearch(
                eventSearchParam,
                DefaultCurrentPage,
                RenderingParamNumberOfDisplayedItems,
                DefaultNumberOfDisplayPages,
                out startItem,
                out endItem,
                out totalCount,
                out items);

            if (!items.Any())
            {
                // Whet not suppress, return all Event Items
                eventSearchParam.Locations = string.Empty;
                eventSearchParam.Specialties = string.Empty;
                eventSearchParam.Taxonomy = string.Empty;

                // Search
                ModuleHelper.CalendarSearch(
                    eventSearchParam,
                    DefaultCurrentPage,
                    RenderingParamNumberOfDisplayedItems,
                    DefaultNumberOfDisplayPages,
                    out startItem,
                    out endItem,
                    out totalCount,
                    out items);

                displayAllContent = true;
            }

            if (items.Any() && searchResultsItem != null)
            {
                // Setup "View All" link if all items aren't displayed & 
                if (searchResultsItem != null && totalCount > RenderingParamNumberOfDisplayedItems)
                {
                    if (displayAllContent)
                    {
                        viewAllUrl = BaseModuleHelper.GetSearchResultsUrl(string.Empty, string.Empty, string.Empty, false, searchResultsItem);
                    }
                    else
                    {
                        string strSpecialties = ItemHelper.GetItemNamesFromItemGuids(specialtyItems);
                        string strLocations = ItemHelper.GetItemNamesFromItemGuids(locationItems);
                        viewAllUrl = BaseModuleHelper.GetSearchResultsUrl(
                            strSpecialties,
                            strLocations,
                            taxonomyItems,
                            RenderingParamRecursiveTaxonomy,
                            searchResultsItem);
                    }
                }
            }

            // Bind Label
            litUpcomingEventsHeader.Text = DictionaryUpcomingEventsLabel;

            // Bind UpcomingEvent
            lvSearchResults.DataSource = items;
            lvSearchResults.DataBind();
        }

        protected void lvSearchResults_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Button btnViewMore = e.Item.FindControl("btnViewMore") as Button;
                if (btnViewMore != null && !string.IsNullOrEmpty(viewAllUrl))
                {
                    btnViewMore.PostBackUrl = viewAllUrl;
                    btnViewMore.Text = DictionaryViewMore;
                    btnViewMore.Visible = true;
                }
            }

            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

            Item item = e.Item.DataItem as Item;
            if (item == null) return;

            Item eventTopicItem = ModuleHelper.GetEventTopicFromEventSession(item);
            string strEventTopicLink = ModuleHelper.GetEventTopicLinkUrl(eventTopicItem, item);

            // Title
            HyperLink hlItem = e.Item.FindControl("hlItem") as HyperLink;
            string strHeadline = ItemHelper.GetFieldHtmlValue(eventTopicItem, ItemMapper.EventTopic.FieldName.Headline);
            //https://medtouch.attask-ondemand.com/task/view?ID=53ff325800af32ab990c261135be2eaf
            //string strDate = RegistrationHelper.GetSessionDateTimeString(item, false, "dd<br/>MMM");
            DateTime startDate;
            string strDate = string.Empty;
            DateTime? sitecoreDateField1 = ModuleHelper.GetSitecoreDateField(item, EventSession.FieldName.SessionStartDate);
            if (sitecoreDateField1.HasValue)
            {
                startDate = sitecoreDateField1.Value;
                strDate = startDate.ToString("dd<br/>MMM");
            }

            
            if (hlItem != null)
            {
                if(!string.IsNullOrEmpty(strDate))
                    hlItem.Text = string.Format("<span class='event-date'>{0}</span>{1}", strDate, strHeadline);
                else
                    hlItem.Text = string.Format("<span class='event-date'></span>{0}", strHeadline);

                hlItem.NavigateUrl = strEventTopicLink;
            }

            // Teaser
            Literal litTeaser = e.Item.FindControl("litTeaser") as Literal;
            string strTeaser = ItemHelper.GetItemTeaser(eventTopicItem, this.RenderingParamMaxTeaserCharacters);
            //https://medtouch.attask-ondemand.com/task/view?ID=53ff325800af32ab990c261135be2eaf
            strTeaser = StringUtil.Clip(strTeaser, this.RenderingParamMaxTeaserCharacters, true); 
            if (litTeaser != null && !string.IsNullOrEmpty(strTeaser))
            {
                litTeaser.Visible = true;
                litTeaser.Text = strTeaser;
            }

            // Read More
            HyperLink hlMoreLink = e.Item.FindControl("hlMoreLink") as HyperLink;
            if (hlMoreLink != null)
            {
                hlMoreLink.Text = DictionaryReadMoreLink;
                hlMoreLink.NavigateUrl = strEventTopicLink;
                hlMoreLink.Visible = true;
            }
        }
    }
}