﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
using MedTouch.News.Helpers;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace womans.org.Website.layouts.womans.sublayouts.news
{

    public partial class HomeLatestNewsSublayout : BaseSublayout
    {
        protected string FieldTaxonomyTerms = "Taxonomy Terms";

        private int _numberOfDisplayedItems = 1;
        private int _maxTeaserCharacters = 100;
        private string _dateFormat = "d";
        private bool _showLinkToDetail = true;
        private bool _resultTitleAsLink = true;
        private bool _showThumbnail = false;
        private string _readMoreText = "Read More";

        private void Page_Load(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif

            if (!IsPostBack)
            {
                Initialize();
                Search();
            }

#if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
#endif
        }

        private void Initialize()
        {



            _numberOfDisplayedItems = GetIntProperty("Number of Displayed Items");
            _maxTeaserCharacters = GetIntProperty("Max Teaser Characters");
            _dateFormat = GetProperty("Date Format");
            _showThumbnail = GetBooleanProperty("Show Thumbnail");

            _resultTitleAsLink = GetBooleanProperty("Show Title as Link");
            _showLinkToDetail = GetBooleanProperty("Show Link to Detail Page");

            _readMoreText = CultureHelper.GetDictionaryTranslation("Modules.News.RelatedNews.ReadMoreLink");
        }

        private void Search()
        {
            List<Item> items = new List<Item>();
            bool displayAllContent = false;

            int totalCount = 0;
            int startItem = 0;
            int endItem = 0;

            string taxonomyItems = ItemHelper.GetShortIds(ContextItem, FieldTaxonomyTerms);
            string specialtyItems = ItemHelper.GetFieldRawValue(ContextItem, "Specialties");
            string locationItems = ItemHelper.GetFieldRawValue(ContextItem, "Locations");
            string templateFilter = ModuleHelper.NewsTemplateGuid();
            bool isTaxonomyRecursive = GetBooleanProperty("Recursive Taxonomy");
            bool suppress = GetBooleanProperty("Suppress When Nothing Matches");
            Item searchResultsItem = GetSingleReferenceProperty("View All Link");
            if (searchResultsItem == null)
            {
                searchResultsItem = ItemHelper.GetItemFromReferenceField(ItemHelper.GetStartItem(), "News Search Results Page");
            }

            // Create Search Param
            var searchParam = new RelatedContentHelper.RelatedContentHelperSearchParam
            {
                ContextItem = ContextItem,
                TaxonomyItems = taxonomyItems,
                IsRecursive = isTaxonomyRecursive,
                SpecialtyItems = specialtyItems,
                LocationItems = locationItems,
                TemplateFilter = templateFilter
            };

            // Create SortParam . Sort by Latest to Oldest
            var sortOptions = new RelatedContentHelper.SortParam
            {
                FieldName = "News Date",
                RelatedContentSortOption = RelatedContentHelper.RelatedContentSortOption.OrderByDescending
            };

            // Search
            RelatedContentHelper.RelatedContentSearch(searchParam, sortOptions, "1",
                                                      _numberOfDisplayedItems, 1, out startItem, out endItem,
                                                      out totalCount, out items);


            if (!items.Any() && !suppress)
            {
                displayAllContent = true;

                //Create SearchParam
                var suppressSearchParam = new RelatedContentHelper.RelatedContentHelperSearchParam
                {
                    ContextItem = ContextItem,
                    TaxonomyItems = string.Empty,
                    IsRecursive = isTaxonomyRecursive,
                    SpecialtyItems = string.Empty,
                    LocationItems = string.Empty,
                    TemplateFilter = templateFilter
                };

                RelatedContentHelper.RelatedContentSearch(suppressSearchParam, sortOptions, "1",
                                                          _numberOfDisplayedItems, 1, out startItem, out endItem,
                                                          out totalCount, out items);
            }

            if (items.Any())
            {
                if (_numberOfDisplayedItems < totalCount)
                {
                    string resultUrl = string.Empty;
                    if (displayAllContent)
                    {
                        resultUrl = BaseModuleHelper.GetSearchResultsUrl(string.Empty, string.Empty, string.Empty, false, searchResultsItem);
                    }
                    else
                    {
                        string strSpecialties = ItemHelper.GetItemNamesFromItemGuids(specialtyItems);
                        string strLocations = ItemHelper.GetItemNamesFromItemGuids(locationItems);

                        resultUrl = BaseModuleHelper.GetSearchResultsUrl(strSpecialties, strLocations, taxonomyItems, isTaxonomyRecursive, searchResultsItem);
                    }
                    

                    if (this.btnViewAll != null && !string.IsNullOrEmpty(resultUrl))
                    {
                        btnViewAll.PostBackUrl = resultUrl;
                        btnViewAll.Text = CultureHelper.GetDictionaryTranslation("Womans.HomeLatestNews.ViewAll"); ;
                        btnViewAll.Visible = true;
                    }

                    pnlViewAll.Visible = true;
                }
            }
            else
            {
                //phControlHeader.Visible = false;
            }

            //Bind Label
            //litRelatedNews.Text = CultureHelper.GetDictionaryTranslation("Modules.News.RelatedNews.RelatedNewsLabel");
            litLatestNews.Text = CultureHelper.GetDictionaryTranslation(
                           "Womans.HomeLatestNews.LatestNewsTitle");

            //Bind Related News
            lvSearchResults.DataSource = items;
            lvSearchResults.DataBind();
        }

        protected void lvSearchResults_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            Item item = (Item)e.Item.DataItem;

            HyperLink hlItem = (HyperLink)e.Item.FindControl("hlItem");
            Literal litItem = (Literal)e.Item.FindControl("litItem");
            Literal litDate = (Literal)e.Item.FindControl("litDate");
            Literal litTeaser = (Literal)e.Item.FindControl("litTeaser");
            HyperLink hlMoreLink = (HyperLink)e.Item.FindControl("hlMoreLink");
            Literal litButtonText = (Literal)e.Item.FindControl("litButtonText");
            
            Panel pnlDate = (Panel)e.Item.FindControl("pnlDate");
            
            Sitecore.Web.UI.WebControls.Image scThumbnail = (Sitecore.Web.UI.WebControls.Image)e.Item.FindControl("scThumbnail");

            if (item != null)
            {
                string strItemLink = LinkManager.GetItemUrl(item);
                if (hlItem != null && _resultTitleAsLink)
                {
                    hlItem.Text = ItemHelper.GetFieldHtmlValue(item, "Headline");
                    hlItem.NavigateUrl = strItemLink;
                    if (litItem != null) litItem.Visible = false;
                }
                else if (litItem != null)
                {
                    litItem.Text = ItemHelper.GetFieldHtmlValue(item, "Headline");
                    if (hlItem != null) hlItem.Visible = false;
                }

                if (litDate != null && pnlDate != null)
                {
                    DateField dateField = item.Fields["News Date"];
                    if (dateField != null && !string.IsNullOrEmpty(dateField.Value))
                    {
                        litDate.Text = dateField.DateTime.ToString(_dateFormat);
                        pnlDate.Visible = true;
                    }
                }

                if (litTeaser != null)
                {
                    string strTeaser = ItemHelper.GetItemTeaser(item, _maxTeaserCharacters);
                    if (!string.IsNullOrEmpty(strTeaser))
                    {
                        litTeaser.Text = strTeaser;
                        litTeaser.Visible = true;
                    }
                }

                if (hlMoreLink != null)
                {
                    //hlMoreLink.Text = _readMoreText;
                    hlMoreLink.NavigateUrl = strItemLink;
                    hlMoreLink.Visible = true;
                }

                if (litButtonText != null)
                {
                    litButtonText.Text = _readMoreText;
                    litButtonText.Visible = true;
                }

                if (scThumbnail != null && _showThumbnail)
                {
                    if (!string.IsNullOrEmpty(ItemHelper.GetFieldRawValue(item, "News Image")))
                    {
                        scThumbnail.Item = item;
                        scThumbnail.Visible = true;
                    }
                }
            }
        }
    }

}


