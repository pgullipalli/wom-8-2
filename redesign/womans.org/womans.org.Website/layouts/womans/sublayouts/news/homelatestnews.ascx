﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="homelatestnews.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.news.HomeLatestNewsSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<!--==================== Women: News: Latest News Home Page (CUSTOM) =============================-->
<h2 class="home-accordion-handle">
    
		   			<asp:Literal ID="litLatestNews" runat="server"></asp:Literal>
		   			<span class="toggle-icon"></span>
		   		</h2>
		   		<div class="home-accordion-content">
			   		<div class="callout home-callout">
						<asp:ListView runat="server" ID="lvSearchResults" OnItemDataBound="lvSearchResults_ItemDataBound">
                            <ItemTemplate>
                                <div class="callout-event">
							        <asp:HyperLink runat="server" ID="hlMoreLink">
								        <%--<sc:Image ID="scThumbnail" Field="News Image" runat="server" MaxWidth="40" Visible="false"/>--%>
								        <span class="news-title"><asp:Literal ID="litItem" runat="server" /></span>
								        <%--<span class="news-teaser"><asp:Literal ID="litTeaser" runat="server" Visible="false" /></span>
                                        <span class="read-more"><asp:Literal runat="server" ID="litButtonText" Visible="false"></asp:Literal></span>--%>
							        </asp:HyperLink>
        			            </div>
                            </ItemTemplate>
                        </asp:ListView>
                        
                        
                       
				</div>
                 <asp:Panel ID="pnlViewAll" runat="server" Visible="false" CssClass="module-nw-view-all">
                    <asp:Button ID="btnViewAll" runat="server" CssClass="default-button pink inset" Visible="true"/>
                </asp:Panel>
            </div>
<!--==================== /Women: News: Latest News Home Page (CUSTOM) =============================-->   

