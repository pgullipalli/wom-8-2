﻿using System;

using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
using MedTouch.PhysicianDirectory.Helpers;
using MedTouch.PhysicianDirectory.Helpers.Links;
using MedTouch.PhysicianDirectory.Search;

using Sitecore.Data.Items;
using Sitecore.Links;

namespace womans.org.Website.layouts.womans.sublayouts.physiciandirectory
{
    public partial class physicianquicksearch : MedTouch.PhysicianDirectory.layouts.modules.PhysicianDirectory.PhysicianquicksearchSublayout
    {
        //protected override void BindSpecialtyDropdown()
        //{
        //    if (!this.ddlSpecialty.Enabled || !this.ddlSpecialty.Visible)
        //    {
        //        return;
        //    }

        //    string strQuerystringSpecialty = Request.QueryString[BaseModuleHelper.SpecialtiesQuerystring()];

        //    ModuleHelper.BindPhysicianFilterDropdown(
        //        ddlSpecialty,
        //        strQuerystringSpecialty,
        //        CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianSearch.AllSpecialtiesText"),
        //        "{F13CD9DF-B762-4E72-8706-5D325EB1F223}",
        //        "{A7D5609C-6FF0-432F-B6D6-5836199EED92}",
        //        "Specialty Name");

        //    ModuleHelper.BindSpecialtyDropdown(this.ddlSpecialty, strQuerystringSpecialty);
        //}

        //private void SearchSubmitted()
        //{
        //    string lastname = this.txtLastName.Text.Trim();
        //    if (lastname.Equals(this.DictionarySearchByLastNameWatermark, StringComparison.OrdinalIgnoreCase))
        //    {
        //        lastname = string.Empty;
        //    }

        //    string specialties = this.ddlSpecialty.SelectedValue;

        //    if (!string.IsNullOrEmpty(lastname) || !string.IsNullOrEmpty(specialties))
        //    {
        //        var physicianSearchParam = new PhysicianSearchParam()
        //        {
        //            LastName = lastname,
        //            Specialties = specialties,
        //        };

        //        ILinkHelper linkHelper = ModuleHelper.GetLinkHelper();

        //        linkHelper.SubmitSearch(
        //            physicianSearchParam, ModuleHelper.GetSearchResultPageItem(this.RenderingParamSearchResultsPage));
        //    }
        //    else
        //    {
        //        this.litError.Text = this.DictionaryCriteriaRequired;
        //        this.pnlError.Visible = true;
        //    }
        //}
    }
}