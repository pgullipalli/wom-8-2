﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="physicianlisting.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.physiciandirectory.physicianlisting" %>

<%@ Import Namespace="MedTouch.Common.Helpers" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="MedTouch" TagName="PaginationList" Src="/layouts/modules/PhysicianDirectory/default/PhysicianPagination.ascx" %>

<!--==================== Module: Physician Directory: Physician Listing =============================-->
<asp:Panel ID="pnlResults" class="module-pd-results" runat="server">
    <asp:Panel ID="pnlCurrentSearchMessage" runat="server" Visible="false">
        <h3>
            <asp:Literal ID="litCurrentSearch" runat="server" /></h3>
    </asp:Panel>
    <MedTouch:PaginationList ID="ucPagingTop" runat="server" DisplayInfo="true" />
    <asp:Panel ID="pnlListing" runat="server" CssClass="listing grid">
        <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_ItemDataBound">
            <ItemTemplate>
                <asp:Panel ID="pnlResult" runat="server" CssClass="listing-item">
                    <div class="three columns">
                        <asp:Panel ID="pnlImage" runat="server" Visible="false" CssClass="module-pd-thumbnail core-thumbnail">
                            <asp:HyperLink ID="hlImage" runat="server">
                                <sc:Image ID="scImage" runat="server" MaxWidth="100" Field="Photo" />
                            </asp:HyperLink>
                        </asp:Panel>
                        <asp:Panel ID="pnlReadMore" runat="server" Visible="false" CssClass="listing-item-more-link">
                            <asp:HyperLink ID="hlMoreLink" runat="server" CssClass="button" />
                        </asp:Panel>
                    </div>
                    <div class="nine columns">
                        <div class="module-pd-listing-info twelve columns">
                            <h3>
                                <asp:HyperLink ID="hlItem" runat="server" /><asp:Literal ID="litItem" runat="server" />
                            </h3>
                            <asp:Panel ID="pnlSpecialties" runat="server" Visible="false" CssClass="module-pd-specialty-list">
                                <asp:Literal ID="litSpecialties" runat="server" />
                                <div class="new-patients">
                                <asp:Panel runat="server" ID="pnlAppointment">
                                    <span></span>
                                    <asp:Literal runat="server" ID="litMakeAppointment" Visible="false"></asp:Literal><p><%=this.MakeAnAppointmentText %></p>
                                </asp:Panel>
                                </div>
                            </asp:Panel>

                        </div>
                        
                        <sc:Sublayout ID="slPhysicianOffices" Path="/layouts/modules/PhysicianDirectory/default/PhysicianOfficesLite.ascx"
                            runat="server" />
                    </div>
                </asp:Panel>
            </ItemTemplate>
            <EmptyDataTemplate>
                <%=this.DictionaryNoResultsMessage %></EmptyDataTemplate>
        </asp:ListView>
    </asp:Panel>
    <MedTouch:PaginationList ID="ucPagingBottom" runat="server" DisplayInfo="true" />
</asp:Panel>
<!--==================== /Module: Physician Directory: Physician Listing =============================-->