﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
using MedTouch.PhysicianDirectory.Helpers;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using MedTouch.PhysicianDirectory.Search;

namespace womans.org.Website.layouts.womans.sublayouts.physiciandirectory
{
    public partial class physicianlisting : BaseSublayout
    {
        #region Fields
        private string dictionaryCurrentSearchMessage;
        private string dictionaryNoResultsMessage;
        private string dictionaryReadMoreLink;
        private string querystringPageValue;
        private string dictionaryMakeAnAppointmentText;
        private string querystringPageKey;
        private int? renderingParamNumberOfDisplayedItems;
        private int? renderingParamNumberOfDisplayedPages;
        private int? renderingParamMaxTeaserCharacters;
        private bool? renderingParamShowTitleAsLink;
        private bool? renderingParamShowLinkToDetailPage;
        private bool? renderingParamAccurateRangeSearch;
        #endregion Fields

        #region protected Properties
        protected string DictionaryCurrentSearchMessage
        {
            get
            {
                if (this.dictionaryCurrentSearchMessage == null)
                {
                    this.dictionaryCurrentSearchMessage =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianListing.CurrentSearchMessage");
                }

                return this.dictionaryCurrentSearchMessage;
            }
        }

        protected string MakeAnAppointmentText
        {
            get
            {
                if (this.dictionaryMakeAnAppointmentText == null)
                {
                    this.dictionaryMakeAnAppointmentText =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.MakeAnAppointmentText");
                }

                return this.dictionaryMakeAnAppointmentText;
            }
        }

        public string DictionaryNoResultsMessage
        {
            get
            {
                if (this.dictionaryNoResultsMessage == null)
                {
                    this.dictionaryNoResultsMessage =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianListing.NoResultsMessage");
                }

                return this.dictionaryNoResultsMessage;
            }
        }

        protected string DictionaryReadMoreLink
        {
            get
            {
                if (this.dictionaryReadMoreLink == null)
                {
                    this.dictionaryReadMoreLink =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianListing.ReadMoreLink");
                }

                return this.dictionaryReadMoreLink;
            }
        }

        protected string QuerystringPageKey
        {
            get
            {
                if (this.querystringPageKey == null)
                {
                    this.querystringPageKey = ModuleHelper.PageQuerystring();
                }

                return this.querystringPageKey;
            }
        }

        protected string QuerystringPageValue
        {
            get
            {
                if (this.querystringPageValue == null)
                {
                    this.querystringPageValue = Request.QueryString[this.QuerystringPageKey];

                    if (string.IsNullOrEmpty(this.querystringPageValue))
                    {
                        this.querystringPageValue = "1";
                    }
                }

                return this.querystringPageValue;
            }
        }

        protected int RenderingParamNumberOfDisplayedItems
        {
            get
            {
                if (!this.renderingParamNumberOfDisplayedItems.HasValue)
                {
                    this.renderingParamNumberOfDisplayedItems = this.GetIntProperty("Number of Displayed Items");
                }

                return this.renderingParamNumberOfDisplayedItems.GetValueOrDefault(1);
            }
        }

        protected int RenderingParamNumberOfDisplayedPages
        {
            get
            {
                if (!this.renderingParamNumberOfDisplayedPages.HasValue)
                {
                    this.renderingParamNumberOfDisplayedPages = this.GetIntProperty("Number of Displayed Pages");
                }

                return this.renderingParamNumberOfDisplayedPages.GetValueOrDefault(1);
            }
        }

        protected int RenderingParamMaxTeaserCharacters
        {
            get
            {
                if (!this.renderingParamMaxTeaserCharacters.HasValue)
                {
                    this.renderingParamMaxTeaserCharacters = this.GetIntProperty("Max Teaser Characters");
                }

                return this.renderingParamMaxTeaserCharacters.GetValueOrDefault(100);
            }
        }

        protected bool RenderingParamShowTitleAsLink
        {
            get
            {
                if (!this.renderingParamShowTitleAsLink.HasValue)
                {
                    this.renderingParamShowTitleAsLink = this.GetBooleanProperty("Show Title as Link");
                }

                return this.renderingParamShowTitleAsLink.GetValueOrDefault(true);
            }
        }

        protected bool RenderingParamShowLinkToDetailPage
        {
            get
            {
                if (!this.renderingParamShowLinkToDetailPage.HasValue)
                {
                    this.renderingParamShowLinkToDetailPage = this.GetBooleanProperty("Show Link to Detail Page");
                }

                return this.renderingParamShowLinkToDetailPage.GetValueOrDefault(true);
            }
        }

        protected bool RenderingParamAccurateRangeSearch
        {
            get
            {
                if (!this.renderingParamAccurateRangeSearch.HasValue)
                {
                    this.renderingParamAccurateRangeSearch = this.GetBooleanProperty("AccurateRangeSearch");
                }

                return this.renderingParamAccurateRangeSearch.GetValueOrDefault(true);
            }
        }
        #endregion protected Properties

        private void Page_Load(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif
            this.BindControls();
#if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
#endif
        }

        protected void BindControls()
        {
            this.BindPaging();
            this.BindSearchResult();
        }

        protected void BindPaging()
        {
            this.ucPagingTop.NumberOfDisplayedItem = this.RenderingParamNumberOfDisplayedItems;
            this.ucPagingTop.NumberOfDisplayedPage = this.RenderingParamNumberOfDisplayedPages;

            this.ucPagingBottom.NumberOfDisplayedItem = this.RenderingParamNumberOfDisplayedItems;
            this.ucPagingBottom.NumberOfDisplayedPage = this.RenderingParamNumberOfDisplayedPages;
        }

        protected void BindSearchResult()
        {
            List<Item> items = new List<Item>();
            int totalCount = 0;
            int startItem = 0;
            int endItem = 0;

            if (this.ContextItem.TemplateName.Equals("Physician Folder", StringComparison.OrdinalIgnoreCase))
            {

            }
            else
            {
                string keyword = this.Request.QueryString[ModuleHelper.KeywordQuerystring()];
                string firstname = this.Request.QueryString[ModuleHelper.FirstNameQuerystring()];
                string lastname = this.Request.QueryString[ModuleHelper.LastNameQuerystring()];
                string firstInitLastName = this.Request.QueryString[ModuleHelper.FirstInitLastNameQuerystring()];
                string fullname = this.Request.QueryString[ModuleHelper.FullNameQuerystring()];
                string languages = this.Request.QueryString[ModuleHelper.LanguagesQuerystring()];
                string gender = this.Request.QueryString[ModuleHelper.GenderQuerystring()];
                string zip = this.Request.QueryString[ModuleHelper.ZipQuerystring()];
                string range = this.Request.QueryString[BaseModuleHelper.RangeQuerystring()];
                string departments = this.Request.QueryString[ModuleHelper.DepartmentsQuerystring()];
                string cities = this.Request.QueryString[ModuleHelper.CityQuerystring()];

                string taxonomy = this.Request.QueryString[BaseModuleHelper.TaxonomyQuerystring()];
                string taxonomyRecursive = this.Request.QueryString[BaseModuleHelper.TaxonomyRecursivelySearchQuerystring()];
                bool isTaxonomyRecursive;
                if (!bool.TryParse(taxonomyRecursive, out isTaxonomyRecursive))
                {
                    isTaxonomyRecursive = true;
                }

                string locations = this.Request.QueryString[BaseModuleHelper.LocationsQuerystring()];
                string specialties = this.Request.QueryString[BaseModuleHelper.SpecialtiesQuerystring()];
                string services = Request.QueryString[BaseModuleHelper.ServicesQuerystring()];

                // Sort by distance if zip/range is not empty, otherwise sortby last name.
                PhysicianSortParam sortParam = new PhysicianSortParam(
                        ItemMapper.Physician.FieldName.LastName, PhysicianSortByOption.OrderByDistance);

                var physicianSearchParam = new PhysicianSearchParam()
                {
                    Keyword = keyword,
                    FirstName = firstname,
                    LastName = lastname,
                    FirstInitialLastName = firstInitLastName,
                    FullName = fullname,
                    Specialties = specialties,
                    Languages = languages,
                    Gender = gender,
                    Zip = zip,
                    Range = range,
                    Locations = locations,
                    Departments = departments,
                    Cities = cities,
                    AccurateRangeSearch = this.RenderingParamAccurateRangeSearch,
                    Taxonomies = taxonomy,
                    IsTaxonomyRecursive = isTaxonomyRecursive,
                    Services = services,
                    SortParam = sortParam
                };

                var physicianSearcher = ModuleHelper.GetPhysicianSearcher();

                physicianSearcher.Search(
                    physicianSearchParam,
                    this.QuerystringPageValue,
                    this.RenderingParamNumberOfDisplayedItems,
                    this.RenderingParamNumberOfDisplayedPages,
                    out startItem,
                    out endItem,
                    out totalCount,
                    out items);

                if (!string.IsNullOrEmpty(keyword))
                {
                    string currentSearchMessage = this.DictionaryCurrentSearchMessage;
                    if (!string.IsNullOrEmpty(currentSearchMessage) && currentSearchMessage.Contains("{0}"))
                    {
                        this.litCurrentSearch.Text = string.Format(currentSearchMessage, "<span class=\"module-search-keyword\">" + keyword + "</span>");
                        this.pnlCurrentSearchMessage.Visible = true;
                    }
                }
            }

            MedTouch.Pagination.Helpers.ModuleHelper.SetPagination(
                this.ucPagingTop,
                totalCount,
                startItem,
                endItem,
                this.RenderingParamNumberOfDisplayedItems,
                this.RenderingParamNumberOfDisplayedPages,
                this.QuerystringPageKey);

            MedTouch.Pagination.Helpers.ModuleHelper.SetPagination(
                this.ucPagingBottom,
                totalCount,
                startItem,
                endItem,
                this.RenderingParamNumberOfDisplayedItems,
                this.RenderingParamNumberOfDisplayedPages,
                this.QuerystringPageKey);

            this.lvSearchResults.DataSource = items;
            this.lvSearchResults.DataBind();
        }

        protected void lvSearchResults_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            Item item = e.Item.DataItem as Item;

            if (item == null)
            {
                return;
            }

            string strItemLink = ItemHelper.GetItemUrl(item);
            string strDisplayName = ModuleHelper.GetPhysicianDisplayName(item);
            string strImage = ItemHelper.GetFieldRawValue(item, ItemMapper.Physician.FieldName.Photo);

            HyperLink hlItem = e.Item.FindControl("hlItem") as HyperLink;
            Literal litItem = e.Item.FindControl("litItem") as Literal;
            if (hlItem != null && this.RenderingParamShowTitleAsLink)
            {
                hlItem.Text = strDisplayName;
                hlItem.NavigateUrl = strItemLink;
                if (litItem != null)
                {
                    litItem.Visible = false;
                }
            }
            else if (litItem != null)
            {
                litItem.Text = strDisplayName;
                if (hlItem != null)
                {
                    hlItem.Visible = false;
                }
            }

            HyperLink hlMoreLink = e.Item.FindControl("hlMoreLink") as HyperLink;
            Panel pnlReadMore = e.Item.FindControl("pnlReadMore") as Panel;
            if (hlMoreLink != null && pnlReadMore != null && this.RenderingParamShowLinkToDetailPage)
            {
                hlMoreLink.Text = this.DictionaryReadMoreLink;
                hlMoreLink.NavigateUrl = strItemLink;
                pnlReadMore.Visible = true;
            }

            Panel pnlImage = e.Item.FindControl("pnlImage") as Panel;
            HyperLink hlImage = e.Item.FindControl("hlImage") as HyperLink;
            Sitecore.Web.UI.WebControls.Image scImage = e.Item.FindControl("scImage") as Sitecore.Web.UI.WebControls.Image;
            if (!string.IsNullOrEmpty(strImage) && scImage != null && hlImage != null && pnlImage != null)
            {
                scImage.Item = item;
                hlImage.NavigateUrl = strItemLink;
                pnlImage.Visible = true;
            }

            Sublayout slPhysicianOffices = (Sublayout)e.Item.FindControl("slPhysicianOffices");
            if (slPhysicianOffices != null)
            {
                string param = this.RenderingParamAccurateRangeSearch ? "AccurateRangeSearch=1" : "AccurateRangeSearch=0";
                slPhysicianOffices.Parameters = param;
                slPhysicianOffices.DataSource = item.ID.ToString();
            }

            Panel pnlSpecialties = (Panel)e.Item.FindControl("pnlSpecialties");
            Literal litSpecialties = (Literal)e.Item.FindControl("litSpecialties");
            if (pnlSpecialties != null && litSpecialties != null)
            {
                ModuleHelper.ManageToShowRelatedItems(
                    litSpecialties,
                    pnlSpecialties,
                    ", ",
                    item,
                    "Specialties",
                    "Specialty Name",
                    ModuleHelper.DISPLAY_TYPE_UNORDERED_LIST);
            }

            List<Item> relatedItems = ItemHelper.GetItemsFromMultilistField("Specialties", item);
            foreach (Item itm in relatedItems)
            {
                CheckboxField cf = itm.Fields["Allow to Make an Appointment"];
                if (cf != null)
                {
                    if (cf.Checked)
                    {
                        Literal litMakeAppointment = (Literal)e.Item.FindControl("litMakeAppointment");
                        litMakeAppointment.Visible = true;
                    }
                }
            }

        }
    }
}