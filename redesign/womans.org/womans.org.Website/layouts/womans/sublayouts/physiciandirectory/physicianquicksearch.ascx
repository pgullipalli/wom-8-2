﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="physicianquicksearch.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.physiciandirectory.physicianquicksearch" %>

<!--==================== Module: Physician Directory: Physician Quick Search =============================-->
<asp:Panel ID="pnlControlWrapper" runat="server" DefaultButton="btnSubmit" CssClass="module-pd-search-again core-search-again collapse-for-mobile">
    <div class="reg-callout grid">
        <asp:PlaceHolder runat="server" ID="phControlHeader">
            <h3 class="module-heading"><asp:Label ID="lblPhysicianSearch" runat="server" /></h3>
        </asp:PlaceHolder>
        <div class="module-alphabet-list-sm alpha-list columns">
            <ul class="module-alphabet-list">
            </ul>
        </div>

        <div class="search-form">
        <asp:Panel ID="pnlLastName" runat="server" CssClass="twelve columns">
            <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName" CssClass="label" />
            <asp:TextBox ID="txtLastName" runat="server" CssClass="textbox" />
        </asp:Panel>
        <asp:Panel ID="pnlSpecialty" runat="server" CssClass="twelve columns">
            <asp:Label ID="lblSpecialty" runat="server" AssociatedControlID="ddlSpecialty" CssClass="label" />
            <div class="selectbox">
            <asp:DropDownList ID="ddlSpecialty" runat="server" CssClass="selectboxdiv" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlAdvSearch" runat="server" Visible="false">
            <asp:Hyperlink ID="hlAdvSearch" runat="server" />
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="false" CssClass="errortext"><asp:Literal ID="litError" runat="server" /></asp:Panel>

        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="button" />
        <asp:ImageButton ID="ibSubmit" runat="server" OnClick="ibSubmit_Click" Visible="false" />
        <asp:LinkButton ID="lbSubmit" runat="server" Visible="false" OnClick="lbSubmit_Click" />

        </div>
    </div>
</asp:Panel>


<!--==================== /Module: Physician Directory: Physician Quick Search =============================-->
