﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="logo.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.common.logo" %>

<div class="logo">
				<a title="Woman's" href="/">
                    <asp:Literal runat="server" ID="litLogo"></asp:Literal>
				    <asp:Literal runat="server" ID="litLogoFallback"></asp:Literal>
				</a>
			</div>