﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="featurecallouts.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.common.featurecallouts" %>




			<div class="grid features hide-on-mobile">
				<!-- ################################## DESTOP ONLY ################################## -->
				
                <asp:Repeater runat="server" ID="rptFeatures">
                    <ItemTemplate>
                        
                        <div class="three columns">
					<div class="feature-image-wrapper">
						<a href='<%# Eval("Link.Url") %>'>
							<%# Eval("Image.Rendered") %>
						</a>
					</div>
					<div class="feature-content">
						<div class="inner">
							<h4>
								<span><%# Eval("Headline.Rendered") %></span>
							</h4>
							<p>
								<a href='<%# Eval("Link.Url") %>'><%# Eval("Content.Rendered") %></a>
							</p>
						</div>
					</div>
				</div>
    
                    </ItemTemplate>
                
                </asp:Repeater>
                

				<!-- ################################## END DESKTOP ONLY ##################################-->

			</div>

				<!-- ################################## MOBILE ONLY SLIDER ##################################-->
				
				<div class="owl-carousel hide-on-desktop" >

                <asp:Repeater runat="server" ID="rptFeatureMobile">
                    <ItemTemplate>
                    <div>
				  	<div>
					  	<div class="feature-image-wrapper">
							<a href='<%# Eval("Link.Url") %>'>
								<%# Eval("Image.Rendered") %>
							</a>
						</div>
						<div class="feature-content">
							<div class="inner">
								<h4>
									<span><%# Eval("Headline.Rendered") %></span>
								</h4>
								<p>
									<a href='<%# Eval("Link.Url") %>'><%# Eval("Content.Rendered") %></a>
								</p>								
							</div>
						</div>
					</div>
				  </div>
                    </ItemTemplate>
                </asp:Repeater>
				 

				</div>

				<!-- ################################## END MOBILE ONLY SLIDER ##################################-->