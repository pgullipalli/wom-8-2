﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using womans.org.BusinessLogic.Helpers;

namespace womans.org.Website.layouts.womans.sublayouts.common
{
    public partial class homepageslider : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            rptIcons.DataSource = Navigation.GetSecondaryHomePageNavigation();
            rptIcons.DataBind();
        }
    }
}