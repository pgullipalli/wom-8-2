﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="callout.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.common.callout" %>
<div class="callout-wrapper">
<div class="callout">
    <h4><span>
            <asp:Literal runat="server" ID="litHeadline"></asp:Literal>
    </span></h4>
    <div class="feature-story-callout" style="border-top:0px;">
        <asp:Literal runat="server" ID="litContent"></asp:Literal>
        <h3>
            <asp:HyperLink runat="server" ID="hlLink"></asp:HyperLink>
        </h3>
    </div>
</div>
</div>