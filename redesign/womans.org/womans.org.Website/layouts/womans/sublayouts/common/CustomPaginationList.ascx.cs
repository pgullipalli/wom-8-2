﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Pagination._usercontrols.pagination;
using MedTouch.Common.Helpers;
using System.Web.UI.HtmlControls;

namespace womans.org.Website.layouts.womans.sublayouts.common
{
    public partial class CustomPaginationList : PaginationList
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BindControl();
        }

        private void BindControl()
        {
            pnlPaginationInfo.Visible = DisplayInfo;

            if (TotalCount > 0)
            {
                // paging information
                int totalPage = 0;
                int startItem = 0;
                int endItem = 0;
                int currentPage = 1;
                int startPage = 1;
                int endPage = 1;

                string strCurrentPage = Request.QueryString[PageQuerystring];
                MedTouch.Common.Controls.Navigation.Pagination.GetPaginationData(strCurrentPage, TotalCount,
                                                                                 NumberOfDisplayedItem,
                                                                                 NumberOfDisplayedPage,
                                                                                 out totalPage, out startItem,
                                                                                 out endItem, out currentPage,
                                                                                 out startPage,
                                                                                 out endPage);

                if (DisplayInfo)
                {
                    string strThisPage = currentPage.ToString(CultureInfo.InvariantCulture);
                    string strTotalPage = totalPage.ToString(CultureInfo.InvariantCulture);
                    string strTotalItems = TotalCount.ToString(CultureInfo.InvariantCulture);
                    string strDisplayedItems = "1";
                    if (TotalCount != 1)
                    {
                        string strStart = startItem.ToString(CultureInfo.InvariantCulture);
                        string strEnd = (endItem > TotalCount)
                                            ? TotalCount.ToString(CultureInfo.InvariantCulture)
                                            : endItem.ToString(CultureInfo.InvariantCulture);

                        if (strStart.Equals(strEnd))
                            strDisplayedItems = strStart;
                        else
                            strDisplayedItems = string.Format("{0} - {1}", startItem, (endItem > TotalCount)
                                                                                                     ? TotalCount.ToString(CultureInfo.InvariantCulture)
                                                                                                     : endItem.ToString(CultureInfo.InvariantCulture));
                    }

                    litInfo.Text = MedTouch.Pagination.Helpers.ModuleHelper.BuildPaginationInfoString(strThisPage, strTotalPage, strDisplayedItems, strTotalItems);
                }

                if (totalPage > 1)
                {
                    // prev & next
                    if (currentPage == 1)
                    {
                        liPrevious.Attributes["class"] = "arrow no-link";
                        liNext.Attributes["class"] = "arrow";
                    }
                    if (currentPage != 1)
                    {
                        hlPrevious.NavigateUrl = (currentPage - 1 == 1)
                            ? UrlHelper.RemoveQueryString(PageQuerystring)
                            : UrlHelper.AppendQueryString(PageQuerystring,
                                (currentPage - 1).ToString(CultureInfo.InvariantCulture));
                    }
                    if (currentPage != totalPage)
                    {
                        hlNext.NavigateUrl = UrlHelper.AppendQueryString(PageQuerystring,
                            (currentPage + 1).ToString(CultureInfo.InvariantCulture));
                    }
                    if (currentPage == totalPage)
                    {
                        liPrevious.Attributes["class"] = "arrow";
                        liNext.Attributes["class"] = "arrow no-link";
                    }

                    // page range
                    for (int i = startPage; i <= endPage; i++)
                    {
                        HyperLink hlPage = new HyperLink();
                        hlPage.Text = i.ToString(CultureInfo.InvariantCulture);
                        hlPage.NavigateUrl = (i == 1)
                            ? UrlHelper.RemoveQueryString(PageQuerystring)
                            : UrlHelper.AppendQueryString(PageQuerystring, i.ToString(CultureInfo.InvariantCulture));

                        HtmlGenericControl liControl = new HtmlGenericControl("li");
                        liControl.Controls.Add(hlPage);

                        if (currentPage == i)
                        {
                            liControl.Attributes["class"] = "active";
                        }

                        phPageRange.Controls.Add(liControl);
                    }

                    if (!string.IsNullOrEmpty(ActivePreviousButtonImage)
                        && !string.IsNullOrEmpty(InactivePreviousButtonImage)
                        && !string.IsNullOrEmpty(ActiveNextButtonImage)
                        && !string.IsNullOrEmpty(InactiveNextButtonImage))
                    {
                        liNext.Visible = false;
                        liPrevious.Visible = false;
                    }
                    else
                    {
                        hlNext.Text = CultureHelper.GetDictionaryTranslation("Modules.Base.Pagination.Next");
                        hlPrevious.Text = CultureHelper.GetDictionaryTranslation("Modules.Base.Pagination.Previous");
                    }
                }
                else
                {
                    pnlPaginationNav.Visible = false;
                }

                // Set control properties so that these are available for callers
                StartItem = startItem;
                EndItem = endItem;
            }
            else
            {
                pnlPagination.Visible = false;
            }
        }
    }
}