﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using Womans.CustomItems.PageTemplates.Homepage;
using Sitecore.Data.Items;
using Womans.CustomItems.ComponentTemplates;
using womans.org.BusinessLogic.Helpers;


namespace womans.org.Website.layouts.womans.sublayouts.common
{
    public partial class quicklinks : System.Web.UI.UserControl
    {
        protected bool isFirst = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            //rptNav.DataSource = Navigation.GetPrimaryNavigation();
            //rptNav.DataBind();

            Item itm = ItemHelper.GetStartItem();
            HomepageItem home = itm;
            QuickLinksNavigationItem item = home.SiteSettings.QuickLinksRoot.Item;

            imgQuickLinks.ImageUrl = item.Image.MediaUrl;
            litTitle.Text = item.Heading.Rendered;
            litContent.Text = item.ContentCopy.Rendered;
            hlLink.NavigateUrl = item.Link.Url;
            hlLink.Text = item.LinkText.Raw;

            rptQuicklinks.DataSource = NavigationSection.GetQuickLinks();
            rptQuicklinks.DataBind();
        }

        protected List<Navigation> GetPrimaryNavigation()
        {
            if (isFirst)
            {
                isFirst = false;
                return Navigation.GetPrimaryNavigation();
            }
            else
            {
                return new List<Navigation>();
            }
        }
    }
}