﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
using Sitecore.Shell.Applications.ContentEditor;
using DateTime = System.DateTime;

namespace womans.org.Website.layouts.womans.sublayouts.common
{
    public partial class DueDateCalculator : BaseSublayout
    {
        #region Variables
        private readonly string _strDictionaryBabysHeartbeatLabel = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.BabysHeartbeatLabel");
        private readonly string _strDictionaryConceptionDateLabel = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.ConceptionDateLabel");
        private readonly string _strDictionaryCycleLengthLabel = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.CycleLengthLabel");
        private readonly string _strDictionaryDueDateLabel = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.DueDateLabel");
        private readonly string _strDictionaryFetalAgeLabel = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.FetalAgeLabel");
        private readonly string _strDictionaryHavingBoyGirlLabel = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.HavingBoyGirlLabel");
        private readonly string _strDictionaryInfoText = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.InfoText");
        private readonly string _strDictionaryMenstrualPeriodError = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.MenstrualPeriodError");
        private readonly string _strDictionaryMenstrualPeriodLabel = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.MenstrualPeriodLabel");
        private readonly string _strDictionaryMenstrualPeriodPlaceHolder = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.MenstrualPeriodPlaceHolder");
        private readonly string _strDictionaryPositivePregnancyTestLabel = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.PositivePregnancyTestLabel");
        private readonly string _strDictionaryRemarksLabel = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.RemarksLabel");
        private readonly string _strDictionaryRiskiestPeriodWasOverLabel = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.RiskiestPeriodWasOverLabel");
        private readonly string _strDictionaryTitleLabel = CultureHelper.GetDictionaryTranslation("Womans.DueDateCalculator.TitleLabel");

        private int? _renderingParamCycleLengthStartOption;
        private int? _renderingParamCycleLengthEndOption;
        #endregion Variables

        #region Properties
        protected int RenderingParamCycleLengthStartOption
        {
            get
            {
                if (!_renderingParamCycleLengthStartOption.HasValue)
                {
                    _renderingParamCycleLengthStartOption = GetIntProperty("Cycle Length Start Option");
                    if (_renderingParamCycleLengthStartOption.Value == 0)
                    {
                        _renderingParamCycleLengthStartOption = 15;
                    }
                }
                return _renderingParamCycleLengthStartOption.Value;
            }
        }

        protected int RenderingParamCycleLengthEndOption
        {
            get
            {
                if (!_renderingParamCycleLengthEndOption.HasValue)
                {
                    _renderingParamCycleLengthEndOption = GetIntProperty("Cycle Length End Option");
                    if (_renderingParamCycleLengthEndOption.Value == 0)
                    {
                        _renderingParamCycleLengthEndOption = 45;
                    }
                }
                return _renderingParamCycleLengthEndOption.Value;
            }
        }
        #endregion Properties

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            #if (!DEBUG)
            try
            {
            #endif

            Initialize();
            if (!IsPostBack)
            {
                BindControl();
            }

            #if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
            #endif
        }

        protected void lbCalculatePregnancy_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtMenstrualPeriod.Text))
            {
                spanError.Visible = true;
                pnlResultPregnancyCalculation.Visible = false;
                return;
            }

            DateTime lastMenstrualPeriodDate;
            if (!DateTime.TryParse(txtMenstrualPeriod.Text, out lastMenstrualPeriodDate))
            {
                spanError.Visible = true;
                pnlResultPregnancyCalculation.Visible = false;
                return;
            }

            /***** M. Abuyasin: Business Logic based on Client JavaScript functions 
             * See AtTask: https://medtouch.attask-ondemand.com/task/view?ID=54072c82009099b28adcb24021d02c72 
             * *****/
            var menstrualCycleLength = int.Parse(ddlCycleLength.SelectedValue);
            const int lutealPhaseLength = 14; // defaults to 14

            // sets ovulation date to menstrual date + cycle days - luteal days
            var conceptionDate = new DateTime(lastMenstrualPeriodDate.Ticks).Date;
            conceptionDate = conceptionDate.AddDays(menstrualCycleLength);
            conceptionDate = conceptionDate.AddDays(-lutealPhaseLength);
            litConceptionDateValue.Text = conceptionDate.ToString("MMMM dd, yyyy");

            // sets due date to ovulation date plus 266 days
            var dueDate = new DateTime(conceptionDate.Ticks);
            dueDate = dueDate.AddDays(266);
            litDueDateValue.Text = dueDate.ToString("MMMM dd, yyyy");

            // sets fetal age to 14 + 266 (pregnancy time) - time left
            var fetalAgeTimeSpan = dueDate.Subtract(DateTime.Now.Date);
            var fetalAge = 14 + 266 - fetalAgeTimeSpan.Days;
            var numFetalWeeks = (fetalAge / 7); // sets weeks to whole number of weeks
            var numFetalDays = (fetalAge % 7); // sets days to the whole number remainder

            // fetal age message, automatically includes 's' on week and day if necessary
            var fetalAgeString = numFetalWeeks + " week" + (numFetalWeeks > 1 ? "s" : "") + ", " + numFetalDays + " day" + (numFetalDays > 1 ? "s" : "");
            litFetalAgeValue.Text = fetalAgeString;

            // 4 weeks pregnant
            var pregnancyTestDate = new DateTime(conceptionDate.Ticks);
            pregnancyTestDate = pregnancyTestDate.AddDays(14);
            litPositivePregnancyTestValue.Text = pregnancyTestDate.ToString("MMMM dd, yyyy");

            var firstHeartBeatDate = new DateTime(conceptionDate.Ticks);
	        firstHeartBeatDate = firstHeartBeatDate.AddDays(56);
            litBabysHeartbeatValue.Text = firstHeartBeatDate.ToString("MMMM dd, yyyy");

            var riskiestPeriodOverDate = new DateTime(conceptionDate.Ticks);
            riskiestPeriodOverDate = riskiestPeriodOverDate.AddDays(84);
            litRiskiestPeriodWasOverValue.Text = riskiestPeriodOverDate.ToString("MMMM dd, yyyy");

            var babySexKnownDate = new DateTime(conceptionDate.Ticks);
            babySexKnownDate = babySexKnownDate.AddDays(98);
            litHavingBoyGirlValue.Text = babySexKnownDate.ToString("MMMM dd, yyyy");
            /***** M. Abuyasin: Business Logic based on Client JavaScript functions *****/

            spanError.Visible = false;
            pnlResultPregnancyCalculation.Visible = true;
        }
        #endregion Events

        #region Methods
        protected override void Initialize()
        {
            litTitle.Text = _strDictionaryTitleLabel;
            litInfoText.Text = _strDictionaryInfoText;
            litMenstrualPeriodLabel.Text = _strDictionaryMenstrualPeriodLabel;
            litMenstrualPeriodError.Text = _strDictionaryMenstrualPeriodError;
            litCycleLengthLabel.Text = _strDictionaryCycleLengthLabel;
            litDueDateLabel.Text = _strDictionaryDueDateLabel;
            litConceptionDateLabel.Text = _strDictionaryConceptionDateLabel;
            litFetalAgeLabel.Text = _strDictionaryFetalAgeLabel;
            litPositivePregnancyTestLabel.Text = _strDictionaryPositivePregnancyTestLabel;
            litBabysHeartbeatLabel.Text = _strDictionaryBabysHeartbeatLabel;
            litRiskiestPeriodWasOverLabel.Text = _strDictionaryRiskiestPeriodWasOverLabel;
            litHavingBoyGirlLabel.Text = _strDictionaryHavingBoyGirlLabel;
            litRemarksLabel.Text = _strDictionaryRemarksLabel;
        }

        protected void BindControl()
        {
            // Bind 'txtMenstrualPeriod' control with its placeholder
            UIHelper.SetTextboxWatermarkPlaceholder(txtMenstrualPeriod, string.Empty, _strDictionaryMenstrualPeriodPlaceHolder);

            // Bind 'ddlCycleLength' with its source list
            BindCycleLengthList();
        }

        private void BindCycleLengthList()
        {
            ddlCycleLength.Items.Clear();
            for (var i = RenderingParamCycleLengthStartOption; i <= RenderingParamCycleLengthEndOption; i++)
            {
                var li = new ListItem(i.ToString(CultureInfo.InvariantCulture), i.ToString(CultureInfo.InvariantCulture));
                if (i == RenderingParamCycleLengthStartOption)
                    li.Selected = true;

                ddlCycleLength.Items.Add(li);
            }
        }
        #endregion Methods
    }
}