﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Womans.CustomItems.ComponentTemplates;
using Womans.CustomItems.BaseTemplates;
using MedTouch.Common.UI;
using Womans.CustomItems.PageTemplates.Common;

namespace womans.org.Website.layouts.womans.sublayouts.common
{
    public partial class featurecallouts : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            LandingPageItem page = Sitecore.Context.Item;
            if (page != null)
            {
                List<FooterFeatureItem> features = new List<FooterFeatureItem>();
                foreach (var f in page.FooterFeatures.ListItems)
                {
                    FooterFeatureItem feature = f;
                    features.Add(feature);
                }

                rptFeatureMobile.DataSource = features;
                rptFeatureMobile.DataBind();

                rptFeatures.DataSource = features;
                rptFeatures.DataBind();
            }
            
        }
    }
}