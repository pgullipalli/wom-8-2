﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Womans.CustomItems.ComponentTemplates;
using MedTouch.Common.UI;

namespace womans.org.Website.layouts.womans.sublayouts.common
{
    public partial class callout : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            if (!string.IsNullOrEmpty(DataSource))
            {

                CalloutItem i = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(DataSource));
                if (i != null)
                {
                    litHeadline.Text = i.CalloutTitle.Rendered;
                    litContent.Text = i.CalloutCopy.Rendered;
                    hlLink.NavigateUrl = i.CalloutLink.Url;
                    hlLink.Text = i.CalloutLinkButtonText.Rendered;
                }

            }
        }
    }
}