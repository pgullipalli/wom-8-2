﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.UI;
using Womans.CustomItems.ComponentTemplates;

namespace womans.org.Website.layouts.womans.sublayouts.common
{
    public partial class patientstory : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            if (!string.IsNullOrWhiteSpace(DataSource))
            {
                PatientStoryItem i = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(DataSource));
                if (i != null)
                {
                    litHeadline.Text = i.Headline.Rendered;
                    litQuote.Text = i.Quote.Raw;
                    litImage.Text = i.Image.Rendered;
                    litName.Text = i.NameandPlace.Raw;

                    pnlWrapper.Visible = true;
                }
            }
        }
    }
}