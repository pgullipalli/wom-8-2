﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using Sitecore.Collections;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using womans.org.BusinessLogic.Helpers;

namespace womans.org.Website.layouts.womans.sublayouts.common
{
    public partial class social : System.Web.UI.UserControl
    {
        private int itemCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;


            Item navItem = ItemHelper.GetSingleItemFromReferenceField(ItemHelper.GetStartItem(), "Social Root");
   
            //rptSocial.DataSource = Navigation.GetSocial();
            if (navItem != null)
            {
                ChildList childList = navItem.GetChildren();
                if (childList.Any())
                {
                    itemCount = childList.Count;
                    rptSocial.DataSource = childList;
                    rptSocial.DataBind();
                }
            }
        }

        protected void rptSocial_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Item item = e.Item.DataItem as Item;
                Link scNav = e.Item.FindControl("scNav") as Link;
                Literal litLiOpen = e.Item.FindControl("litLiOpen") as Literal;
                Literal litLiClose = e.Item.FindControl("litLiClose") as Literal;
                Text scTxtTitle = e.Item.FindControl("scTxtTitle") as Text;


                if (scNav != null && scTxtTitle != null && litLiOpen!= null && litLiClose!=  null)
                {
                    scNav.Item = item;
                    scTxtTitle.Item = item;
                    litLiOpen.Text = "<li class=\"" + ItemHelper.GetFieldRawValue(item,"CSS Class") + "\">";
                    litLiClose.Text = "</li>";
                }

                if (e.Item.ItemIndex == itemCount -1)
                {
                    scNav.Attributes.Add("class", "last");
                }
            }
        }
    }
}