﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.UI;
using Womans.CustomItems.ComponentTemplates;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;

namespace womans.org.Website.layouts.womans.sublayouts.common
{
    public partial class slidercallout : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            if (!string.IsNullOrEmpty(DataSource))
            {
                SliderCalloutItem i = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(DataSource));
                if (i != null)
                {
                    litTitle.Text = i.CalloutTitle.Rendered;
                    //litCalloutCopy.Text = i.CalloutCopy.Rendered;
                    litCalloutButton.Text = i.CalloutLinkButtonText.Raw;
                    //link.HRef = i.CalloutLink.Url;
                    litCalloutSliderCaption.Text = i.CalloutSliderCaption.Raw;
                    litCalloutButton.Text = i.CalloutCopy.Rendered;

                    //List<string> images = new List<string>();
                    var images = new Dictionary<string, string>();
                    foreach (var image in i.CalloutSliderImages.ListItems)
                    {
                        var media = (Sitecore.Data.Items.MediaItem)(image);
                        var desc = string.IsNullOrEmpty(media.Title) ? "" : media.Title;
                        images.Add(MediaManager.GetMediaUrl(media),desc);
                    }

                    rptSlides.DataSource = images;
                    rptSlides.DataBind();
                }
            }
        }
    }
}