﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="homepageslider.ascx.cs"
    Inherits="womans.org.Website.layouts.womans.sublayouts.common.homepageslider" %>

<div class="homepage-services-slider-full-width">
    <ul>
        <asp:Repeater runat="server" ID="rptIcons">
        <ItemTemplate>
            <li class='<%# Eval("CSSClass.Rendered") %>'><a href='<%# Eval("LinkToSelf") %>'><span class="popup-content">
                <%# Eval("HoverImage.Rendered") %>
                <span class="popup-text">
                    <%# Eval("HoverContentCopy.Rendered") %></span>
                <button class="default-button black" type="button">
                    <%# Eval("HoverButtonCopy.Rendered") %></button>
            </span><span class="icon"></span>
                <%# Eval("OverrideNavigationTitle.Rendered") %>
            </a></li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>
