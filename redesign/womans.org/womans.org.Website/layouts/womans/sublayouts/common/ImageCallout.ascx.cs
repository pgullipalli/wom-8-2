﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Womans.CustomItems.ComponentTemplates;
using MedTouch.Common.UI;

namespace womans.org.Website.layouts.womans.sublayouts.common
{
    public partial class ImageCallout : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            if (!string.IsNullOrEmpty(DataSource))
            {
                ImageCalloutItem i = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(DataSource));
                if (i != null)
                {
                    litHeading.Text = i.CalloutHeader.Rendered;
                    litText.Text = i.CalloutText.Rendered;
                    litImage.Text = i.CalloutImage.Rendered;
                    string url = i.CalloutLink.Url;
                    hrefImage.HRef = url;
                    hrefText.HRef = url;
                }
            }
 
        }
    }
}