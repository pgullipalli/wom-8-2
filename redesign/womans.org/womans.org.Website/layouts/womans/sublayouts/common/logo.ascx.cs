﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using MedTouch.Common.Helpers;
using Womans.CustomItems.PageTemplates.Homepage;

namespace womans.org.Website.layouts.womans.sublayouts.common
{
    public partial class logo : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            Item itm = ItemHelper.GetStartItem();
            HomepageItem home = itm;
            litLogo.Text = home.SiteSettings.SiteLogo.Rendered.Replace(">"," class=\"screen-logo\">");
            litLogoFallback.Text = home.SiteSettings.SiteLogoFallback.Rendered.Replace(">", " class=\"ie-print\">");

        }
    }
}