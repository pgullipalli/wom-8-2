﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecondaryNavigation.ascx.cs"
    Inherits="womans.org.Website.layouts.Base.Default.sublayouts.Common.SecondaryNavigation" %>
<!--============================================= Womans: Common: Secondary Navigation Landing Page =============================================-->
<!-- navigation hidden -->
<div id="divServicesMenu" runat="server" class="services-menu lp grid hide-on-phones">
    <ul>
        <asp:Repeater ID="rptSecondaryNav" runat="server">
            <ItemTemplate>
                    <li class='<%# Eval("CssClass") %>'><a href="<%# Eval("LinkToSelf") %>" original-title='<%# Eval("NavigationTitle") %>'>
                        <%# Eval("NavigationTitle") %></a></li>
                
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <div class="services-menu-toggle toggle-button">
    </div>
</div>
<div id="divServicesMenuTablet" runat="server" class="services-menu lp grid tablet-display">
    <ul>
        <asp:Repeater runat="server" ID="rptTabletMenu">
            <ItemTemplate>
                <li class='<%# Eval("CssClass") %>'><a href="<%# Eval("LinkToSelf") %>" original-title='<%# Eval("NavigationTitle") %>'>
                    <%# Eval("NavigationTitle") %></a></li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <div class="services-menu-toggle tablet">
    </div>
</div>

<!--============================================= /Womans: Common: Secondary Navigation Landing Page =============================================-->
