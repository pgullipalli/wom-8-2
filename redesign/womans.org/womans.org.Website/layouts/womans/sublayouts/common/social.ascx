﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="social.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.common.social" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

    
		<ul class="social-links">
            <asp:Repeater runat="server" ID="rptSocial" OnItemDataBound="rptSocial_OnItemDataBound">
                <ItemTemplate>
                      <asp:Literal ID="litLiOpen" runat="server" />
                        <sc:Link ID="scNav" runat="server" Field="Destination" Visible="True">
                           <sc:Text runat="server" ID="scTxtTitle" Field="Override Navigation Title"/>
                        </sc:Link>
                    <asp:Literal ID="litLiClose" runat="server" />
                </ItemTemplate>
            </asp:Repeater>
		</ul>
