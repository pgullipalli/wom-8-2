﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomPaginationList.ascx.cs" 
    Inherits="womans.org.Website.layouts.womans.sublayouts.common.CustomPaginationList" %>
<!--==================== Womans: Custom: Pagination List =============================-->
    <asp:Panel ID="pnlPagination" runat="server" CssClass="module-pg-wrapper">
        <asp:Panel ID="pnlPaginationNav" runat="server" CssClass="module-pg-nav">
			<ul class="pagination">
			    <li id="liPrevious" runat="server" class="arrow"><asp:HyperLink ID="hlPrevious" runat="server" /></li>
                <asp:PlaceHolder ID="phPageRange" runat="server" />
			    <li id="liNext" runat="server" class="arrow"><asp:HyperLink ID="hlNext" runat="server" /></li>
			</ul>			
		</asp:Panel>
     	<asp:Panel ID="pnlPaginationInfo" runat="server" CssClass="module-pg-info">
            <asp:literal ID="litInfo" runat="server" />
        </asp:Panel>
	</asp:Panel>
<!--==================== /Womans: Custom: Pagination List =============================-->