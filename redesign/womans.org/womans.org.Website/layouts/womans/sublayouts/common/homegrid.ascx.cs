﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using Womans.CustomItems.PageTemplates.Homepage;
using Sitecore.Data.Items;
using Womans.CustomItems.ComponentTemplates;
using womans.org.BusinessLogic.Helpers;


namespace womans.org.Website.layouts.womans.sublayouts.common
{
    public partial class homegrid : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            Item itm = ItemHelper.GetStartItem();

            HomepageItem home = itm;
            List<HomepageSlideItem> slides = new List<HomepageSlideItem>();

            foreach (var item in home.SiteSettings.HomepageSliders.ListItems)
            {
                HomepageSlideItem s = item;
                slides.Add(s);
            }

            rptSlides.DataSource = slides;
            rptSlides.DataBind();

            rptNavigation.DataSource = Navigation.GetSecondaryNavigation();
            rptNavigation.DataBind();
        }

        protected void rptSlides_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HomepageSlideItem hsItem = (HomepageSlideItem) e.Item.DataItem;
                Panel pnlCore = (Panel) e.Item.FindControl("pnlCore");
                if (hsItem == null || pnlCore == null)
                    return;

                string strSlideTitle = hsItem.SlideTitle.Rendered;
                string strSlideContent = hsItem.SlideContent.Rendered;
                if (string.IsNullOrWhiteSpace(strSlideTitle) && string.IsNullOrWhiteSpace(strSlideContent))
                {
                    pnlCore.Visible = false;
                }
                else
                {
                    pnlCore.Visible = true;
                }
            }
        }
    }
}