﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageCallout.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.common.ImageCallout" %>

<div class="callout-wrapper">
<div class="callout">
		<h4><span><asp:Literal runat="server" ID="litHeading"></asp:Literal></span></h4>

		<div class="feature-story-callout">
			<a runat="server" id="hrefImage" href="#">
				<asp:Literal runat="server" ID="litImage"></asp:Literal>
			</a>
			<p class="feature-story-title">
				<a runat="server" id="hrefText" href="#"><asp:Literal runat="server" ID="litText"></asp:Literal></a>
			</p>
		</div>
	</div>
    </div>