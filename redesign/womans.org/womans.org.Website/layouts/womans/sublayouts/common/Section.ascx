﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Section.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.common.Section" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="headline">
		<table>
			<tr>
				<td class="headline-main-heading">
					<div>
						<h1>
                            <sc:Text ID="scHeadline" runat="server" Field="Headline" /></h1>
                    </div>
				</td>
				<td>
					<div class="headline-nav">
					    <!-- M. Abuyasin Update: 09/16/2014: AtTask#106408: WL Template no longer correct on style guide or insert template option  -->
					    <sc:sublayout id="slProximityNavigation" runat="server" path="/layouts/Base/Default/sublayouts/Common/ProximityNavigation.ascx" Parameters="NavigationLevelCSSPrefix=nav-item%20level-&ActiveStateCSS=on&CurrentStateCSS=in" />
			    		<!-- Mobile CODE: added .mobile-menu -->
<%--<div class="its">
    <a href="#" class="prox"><span class="icon"></span>
     <sc:Text ID="scHeadlineMobile" runat="server" Field="Headline" /></a>
</div>
<nav class="left-nav nav-collapse collapse" role="navigation">
	<asp:Repeater runat="server" ID="rptNavigation">
        <ItemTemplate>
            <div class="level-1 nav-item"><a href='<%# Eval("LinkToSelf") %>'><%# Eval("NavigationTitle") %></a></div>
        </ItemTemplate>
    </asp:Repeater>
</nav>		--%>
                    <!-- M. Abuyasin Update: 09/16/2014: AtTask#106408: WL Template no longer correct on style guide or insert template option  -->
			    	</div>
				</td>
			</tr>
		</table>
		
		
	</div>


