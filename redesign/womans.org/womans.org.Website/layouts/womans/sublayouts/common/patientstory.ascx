﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="patientstory.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.common.patientstory" %>

<asp:Panel ID="pnlWrapper" runat="server" CssClass="callout-wrapper" Visible="False">
    <div class="callout">
		<h4>
			<span><asp:Literal runat="server" ID="litHeadline"></asp:Literal></span>
		</h4>
		
		<div class="grid">
			<div class="patient-stories">
				<div class="six columns patient-stories-callout-image">
					<asp:Literal runat='server' ID="litImage"></asp:Literal>
				</div>	

				<div class="six columns">
					<p class="patient-story-text"><asp:Literal runat="server" ID="litQuote"></asp:Literal></p>
					<p class="patient-name"><asp:Literal runat="server" ID="litName"></asp:Literal></p>
				</div>	
			</div>
		</div>
	</div></asp:Panel>