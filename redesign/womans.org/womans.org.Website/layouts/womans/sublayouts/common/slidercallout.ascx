﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="slidercallout.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.common.slidercallout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
	<h2><asp:Literal runat="server" ID="litTitle"></asp:Literal></h2>
	<div class="homepage-callout-slider">
		<div>
			<asp:Repeater runat="server" ID="rptSlides">
                <ItemTemplate>
                    <div>
                        <img src='<%# Eval("key") %>' description='<%# Eval("value") %>'/>
					</div>
				</ItemTemplate>
            </asp:Repeater>
		</div>					
	</div>
	<div class="slider-caption">
		<p><asp:Literal runat="server" ID="litCalloutSliderCaption"></asp:Literal></p>
		<button class="default-button red pinterest-carousel" type="button">Pin It</button>
	</div>
	<div class="pinterest-info">
		<asp:Literal runat="server" ID="litCalloutCopy"></asp:Literal>

        <asp:Literal runat="server" ID="litCalloutButton"></asp:Literal>
	</div>



