﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DueDateCalculator.ascx.cs" 
    Inherits="womans.org.Website.layouts.womans.sublayouts.common.DueDateCalculator" %>
<!--==================== Womans: Common: Due Date Calculator =============================-->
<style type="text/css">
    /*form pages*/
    #frmPregnancyCalculator .intro 
	{ 
		font-size:12px; line-height:150%;
		border-bottom: none; 
		margin:0px; 
		padding:15px 20px 20px 20px; 
		float:left; 
		width:415px; 
	} 
	#frmPregnancyCalculator 
	{
		font-size:10px; 
		color:#80858E; 
		line-height:110%;
	} 
	#frmPregnancyCalculator .unit 
	{
		float:left; 
		width:100%; 
		padding-bottom:16px;
	} 
	#frmPregnancyCalculator .section 
	{ 
		font-size:14px; 
		line-height:100%; 
		color:#6f7876; 
		background:#e8eceb; 
		padding:6px; 
		margin-bottom:15px; 
		float:left; 
		width:456px; 
	} 
	#frmPregnancyCalculator label.title 
	{
		font-size:12px; 
		color: #666; 
		font-weight:bold; 
		line-height:110%; 
		text-align:left; 
		padding:0px 0px 12px 0px; 
		float:left; 
		width:100%;
	} 
	#frmPregnancyCalculator label.descrip 
	{
		font-size:11px; 
		color:#80858E; 
		line-height:125%; 
		text-align:right; 
		padding-right:10px; 
		float:left; 
		width:200px;
	} 
	#frmPregnancyCalculator .singleField 
	{ 
		float:left; 
		width:100%; 
		padding-bottom:15px; 
		font-size:11px; 
		line-height:125%;
	} 
	#frmPregnancyCalculator .info 
	{ 
		font-size:12px; 
		line-height:110%; 
		float:left; 
		width:100%; 
		padding:15px 0px 8px 0px;
	} 
	#frmPregnancyCalculator input, #frmPregnancyCalculator select 
	{
		font-size:13px; 
		line-height:100%; 
		color:#535a66; 
		border:1px solid #CCC; 
		height:20px;
	} 
	#frmPregnancyCalculator .divider 
	{
		margin:5px 0px 15px 0px; 
		width:415px; 
		border-top:1px dotted #cccccc; 
		float:left;
	} 
	#resultPregnancyCalculation 
	{
		display:none;
		width:100%;
		float:left;
	}
	.accent02 
	{
        color: #e3369c;
    }
</style>
<h1><asp:Literal ID="litTitle" runat="server" /></h1>
<asp:Panel ID="pnlFormPregnancyCalculator" name="frmPregnancyCalculator" runat="server">
    <div class="info">
        <asp:Literal ID="litInfoText" runat="server" />
    </div>
    <div class="divider"></div>
    <div class="unit">
        <div class="singleField">
            <label for="<%= txtMenstrualPeriod.ClientID %>" class="descrip"><asp:Literal ID="litMenstrualPeriodLabel" runat="server" /></label>
            <asp:TextBox ID="txtMenstrualPeriod" runat="server" name="lastMenstrualPeriod" AutoCompleteType="None" type="date" placeholder="(MM/DD/YYYY)" style="width: 150px;" CssClass="dp_input"></asp:TextBox>
            <br/>
            <span ID="spanError" runat="server" style="color: red" Visible="false">
                <asp:Literal ID="litMenstrualPeriodError" runat="server" />
            </span>
        </div>
        <div class="singleField">
            <label for="<%= ddlCycleLength.ClientID %>" class="descrip"><asp:Literal ID="litCycleLengthLabel" runat="server" /></label>
            <asp:DropDownList ID="ddlCycleLength" runat="server" name="menstrualCycleLength" style="width: 150px;"/>
        </div>
    </div>
    <div class="unit">
		<div class="singleField">
		    <label class="descrip"></label>
		    <asp:LinkButton ID="lbCalculatePregnancy" runat="server" 
                OnClick="lbCalculatePregnancy_Click">
		        <img src="/assets/images/btnFormCalculate.gif" alt="Calculate" id="btnCalculate" width="103" height="17" name="btnCalculate" style="border-width: 0px; border-style: solid;" />
		    </asp:LinkButton>
        </div>
    </div>
    <asp:Panel ID="pnlResultPregnancyCalculation" runat="server" Visible="false">
        <div class="divider"></div>
        <div class="unit">
			<div class="singleField">
			    <label class="descrip"><asp:Literal ID="litDueDateLabel" runat="server" /></label>
				<span id="resultDueDate" class="accent02"><asp:Literal ID="litDueDateValue" runat="server" /></span>
			</div>
			<div class="singleField">
				<label class="descrip"><asp:Literal ID="litConceptionDateLabel" runat="server" /></label>
				<span id="resultConceptionDate" class="accent02"><asp:Literal ID="litConceptionDateValue" runat="server" /></span>
			</div>
			<div class="singleField">
				<label class="descrip"><asp:Literal ID="litFetalAgeLabel" runat="server" /></label>
				<span id="resultFetalAge" class="accent02"><asp:Literal ID="litFetalAgeValue" runat="server" /></span>
			</div>
			<div class="singleField">
				<label class="descrip"><asp:Literal ID="litPositivePregnancyTestLabel" runat="server" /></label>
				<span id="resultPregnancyTestDate" class="accent02"><asp:Literal ID="litPositivePregnancyTestValue" runat="server" /></span>
			</div>
			<div class="singleField">
				<label class="descrip"><asp:Literal ID="litBabysHeartbeatLabel" runat="server" /></label>
				<span id="resultFirstHeartBeatDate" class="accent02"><asp:Literal ID="litBabysHeartbeatValue" runat="server" /></span>
			</div>
			<div class="singleField">
				<label class="descrip"><asp:Literal ID="litRiskiestPeriodWasOverLabel" runat="server" /></label>
				<span id="resultRiskiestPeriodOverDate" class="accent02"><asp:Literal ID="litRiskiestPeriodWasOverValue" runat="server" /></span>
			</div>
			<div class="singleField">
				<label class="descrip"><asp:Literal ID="litHavingBoyGirlLabel" runat="server" /></label>
				<span id="resultBabySexKnownDate" class="accent02"><asp:Literal ID="litHavingBoyGirlValue" runat="server" /></span>
			</div>
			<div class="singleField">
				<br />
				<asp:Literal ID="litRemarksLabel" runat="server" />
			</div>
		</div>
    </asp:Panel>
</asp:Panel>
<!--==================== /Womans: Common: Due Date Calculator =============================-->
