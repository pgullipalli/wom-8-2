﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="homegrid.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.common.homegrid" %>
<section class="hero home grid">
				<div class="decoration"></div>
				<div class="inner">
					<div class="mtslide">
					  <ul class="slides">
						
                      <asp:Repeater runat="server" ID="rptSlides" OnItemDataBound="rptSlides_ItemDataBound">
                        <ItemTemplate>
                            <li>	
                            <%# Eval("SlideImage.Rendered") %>	 
						  <%--<div class="core">--%>
                          <asp:Panel ID="pnlCore" runat="server" CssClass="core">
							  <div class="slide-content">
								<h1>
									<a href='<%# Eval("SlideLink.Url") %>'>
										<%# Eval("SlideTitle.Rendered") %>
                                        <span><%# Eval("SlideContent.Rendered") %></span>
										<span class="arrow"></span>
									</a>
								</h1>
								<div class="copy-left">
									<%# Eval("SlideLink.Rendered") %>
								</div>
							  </div>
                            </asp:Panel>
						  <%--</div>--%>
						</li>
                        </ItemTemplate>
                      </asp:Repeater>
                      </ul>
					</div>		
                      
                      
						
					

					<div class="homepage-services-slider-mobile">
						<div class="owl-carousel-homepage">
							<ul>
                                <asp:Repeater runat="server" ID="rptNavigation">
                                    <ItemTemplate>
                                        <li class='<%# Eval("CssClass") %>'>
									<a href='<%# Eval("LinkToSelf") %>'>
										<span class="icon"></span>
										<%# Eval("NavigationTitle") %>
									</a>
								</li>
								    
                                    </ItemTemplate>
                                </asp:Repeater>
								
							</ul>
						</div>					
					</div>
				</div>		
                
			</section>


            <section class="main-content">
		<div class="grid home-content">
		   <div class="four columns homepage-callout-slider-wrapper">
		   		<sc:placeholder ID="leftpanel" key="Left Panel" runat="server"/>
		   </div>
		   <div class="four columns center-callout-col">
		   		<sc:placeholder ID="contentpanel" key="Content Panel" runat="server"/>
		   </div>
		   <div class="four columns">
		   		<sc:placeholder ID="rightpanel" key="Right Panel" runat="server"/>
		   </div>

		   <div class="homepage-mobile-container"></div>
		</div>
        </section>
	