﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="bodysearch.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.common.bodysearch" %>
<script type="text/javascript">
    function ispagenotfound(e, t) {

        if (!(document.URL.indexOf("search-results") > -1)) {
            if (e.which === 13) {
                e.preventDefault();
                var url = "/search-results/?keyword=" + t.value;
                window.location = url + "";
                return;
            }
            ;
        }
        ;

    }
</script>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSubmit">
    <asp:TextBox ID="txtKeyword" runat="server" CssClass="main-search-input" onkeydown="ispagenotfound(event, this)" />
    <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="black button"  />
    <asp:Panel ID="pnlError" runat="server" Visible="false" CssClass="errortext">
        <asp:Literal ID="litError" runat="server" />
    </asp:Panel>
</asp:Panel>