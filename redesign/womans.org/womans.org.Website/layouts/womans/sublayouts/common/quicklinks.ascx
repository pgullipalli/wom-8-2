﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="quicklinks.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.common.quicklinks" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<section class="dropdown-panel">
			<div class="grid">
				<div class="eight columns quick-links-nav">
			   <sc:Sublayout id="scBasicSiteSearch" runat="server" path="/layouts/modules/SiteSearch/default/BasicSiteSearchBoxMobile.ascx" Parameters="Search Results Page={BE6F4231-8D94-476E-A3A9-930AB47EED37}"/>
					<div class="inner">
							
                            <asp:Repeater runat="server" ID="rptQuicklinks">
                                <ItemTemplate>
                                    <div class="three columns">

                                            <asp:Repeater runat="server" ID="rptHomeNav" Visible="<%# this.isFirst %>" DataSource="<%# GetPrimaryNavigation() %>">
                                                <HeaderTemplate>
                                                    <div class="home-nav">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <a href="<%# Eval("LinkToSelf") %>"><%# Eval("NavigationTitle") %></a>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </div>
                                                </FooterTemplate>
                                            </asp:Repeater>
								            
                                        <h4 class="accordion-handle">
								        <%# Eval("SectionTitle") %>
								<span class="toggle-icon"></span>
							</h4>
							<ul>
                                <asp:Repeater runat="server" ID="rptSubNav" DataSource=<%# Eval("SubNav") %>>
                                    <ItemTemplate>
                                        <li><a href="<%# Eval("LinkToSelf") %>"><%# Eval("NavigationTitle") %></a></li>    
                                    </ItemTemplate>
                                </asp:Repeater>

							</ul>
                                </div>
                                </ItemTemplate>
                            </asp:Repeater>


						</div>
				</div>
				<div class="four columns dropdown-feature">
					<div class="inner">
                        <asp:Image runat="server" ID="imgQuickLinks" CssClass="dropdown-feature-image" />
                        
						<h4><asp:Literal runat="server" ID="litTitle"></asp:Literal></h4>
						<p>
							<asp:Literal runat="server" ID="litContent"></asp:Literal>
							<asp:HyperLink runat="server" ID="hlLink"></asp:HyperLink>
						</p>
					</div>
				</div>
			</div>
            
		</section>

        