﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using MedTouch.SiteSearch.Helpers;
using MedTouch.Common.UI.Base;
using MedTouch.Common.UI;
using MedTouch.Common.Helpers;

namespace womans.org.Website.layouts.womans.sublayouts.common
{
    public partial class bodysearch : BaseSublayout
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialize();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string keyword = txtKeyword.Text.Trim();
            if (!string.IsNullOrEmpty(keyword) && !keyword.Equals(CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.BasicSiteSearchBox.SearchBoxWatermark"), StringComparison.OrdinalIgnoreCase))
            {
                Item searchResultsPage = ModuleHelper.GetSearchResultPage(GetSingleReferenceProperty("Search Results Page"));
                ModuleHelper.SubmitSearch(keyword, string.Empty, string.Empty, string.Empty, string.Empty, false, string.Empty, searchResultsPage);
            }
            else
            {
                litError.Text = CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.BasicSiteSearchBox.KeywordRequired");
                pnlError.Visible = true;
            }
        }

        private void Initialize()
        {
            this.btnSubmit.Text = CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.BasicSiteSearchBox.SearchButtonText");
            UIHelper.SetTextboxWatermark(this.txtKeyword, "", CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.BasicSiteSearchBox.SearchBoxWatermark"));
        }
    }
}