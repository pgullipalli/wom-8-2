﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Womans.CustomItems.BaseTemplates;

namespace womans.org.Website.layouts.womans.sublayouts.layoutframes
{
    public partial class two_column_wide_right : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            PageDataItem page = Sitecore.Context.Item;
            litContent.Text = page.ContentCopy.Rendered;
            litHeading.Text = page.Headline.Rendered;
            litSubTitle.Text = page.Introduction.Rendered;
        }
    }
}