﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls;
using MedTouch.Common.UI;

namespace womans.org.Website.layouts.womans.sublayouts.layoutframes
{
    using System.Web.UI.HtmlControls;

    using MedTouch.Common.Helpers;

    using Sitecore.Data.Items;

    public partial class OneColumnLanding : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindControls();
            }
        }

        private void BindControls()
        {
            var item = ContextItem;
            if (item != null)
            {
                var pageBg = ItemHelper.GetMediaUrlFromFileField(item, "Page Background Image");
                if (!string.IsNullOrEmpty(pageBg))
                {
					var body = ((HtmlGenericControl)Page.FindControl("PageBody"));
                    body.Style["background"] = "url('" + pageBg + "')";
					body.Style["height"] = "auto";
                }

                List<Item> jumpItems;
                ItemHelper.TryGetItemsFromMultilistField(item, "Jump Items", out jumpItems);
                rptJumpItems.DataSource = jumpItems;
                rptJumpItems.DataBind();

                var middleBgImageUrl = ItemHelper.GetMediaUrlFromFileField(item, "Middle Background Image");
                if (middleBgImageUrl != null)
                {
                    pnlMiddleContent.Style["background-image"] = "url('" + middleBgImageUrl + "')";
                }

                var bottomBgImageUrl = ItemHelper.GetMediaUrlFromFileField(item, "Bottom Background Image");
                if (bottomBgImageUrl != null)
                {
                    pnlBottomContent.Style["background-image"] = "url('" + bottomBgImageUrl + "')";
                }
            }
        }
    }
}