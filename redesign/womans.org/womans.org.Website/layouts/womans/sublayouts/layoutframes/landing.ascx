﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="landing.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.layoutframes.landing" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<section class="main-content grid landing-page">
        
        <img class="landing-image" runat="server" id="imgBackground" src="/assets/images/girl-in-grass.jpg">
	    
        <div class="main-quote-mobile-wrapper"></div>
		<div class="grid">
		   <div class="quote">
		   		<div class="main-quote-desktop-wrapper">
			   		<div class="main-quote">
			   			<p>
			   			    <sc:Text runat="server" ID="scTextQuote" Field="Quote"/>
			   			</p>
			   		</div>

			   	</div>
		   </div>

		   <div class="landing-jumps">
		   		<div class="main-heading-wrapper">		   		
		   			<h1><asp:Literal runat="server" ID="litHeading"></asp:Literal></h1>
				</div>
		   		<div class="grid">
		   			<div class="landing-jump-wrapper">
						<div class="landing-jump left">
						  <div class="landing-jump-image-wrapper">
						  	<a runat="server" id="hlTopLeftImage" href="#">
						  		<asp:Literal runat="server" ID="litImageTopLeft"></asp:Literal>
						  	</a>
						  </div>
						  <p>
                            <asp:HyperLink runat="server" ID="hlTopLeft"></asp:HyperLink>
						  </p>
						  <a href="#" class="hover-overlay hide-on-phones" runat="server" id="hlOverlayTopLeft">
						  	<span class="overlay-heading"><asp:Literal runat="server" ID="litHeadingTopLeftOverlay"></asp:Literal></span>
						 	<span class="overlay-description"><asp:Literal runat="server" ID="litContentTopLeftOverlay"></asp:Literal></span>					  	
						 	<span class="default-button black"><asp:Literal runat="server" ID="litButtonTopLeft"></asp:Literal></span>
						  </a>
						</div>
						<div class="landing-jump">
							<div class="landing-jump-image-wrapper">
						  		<a runat="server" id="hlTopRightImage" href="#">
						  			<asp:Literal runat="server" ID="litImageTopRight"></asp:Literal>
						  		</a>
						  	</div>					  	
						  	<p>
						  		<asp:HyperLink runat="server" ID="hlTopRight"></asp:HyperLink>
						  	</p>
						  	<a href="#" class="hover-overlay hide-on-phones" runat="server" id="hlOverlayTopRight">
							  	<span class="overlay-heading"><asp:Literal runat="server" ID="litHeadingTopRightOverlay"></asp:Literal></span>
							 	<span class="overlay-description"><asp:Literal runat="server" ID="litContentTopRightOverlay"></asp:Literal></span>					  	
							 	<span class="default-button black"><asp:Literal runat="server" ID="litButtonTopRight"></asp:Literal></span>
							</a>
						</div>
					</div>
				</div>	
				<div class="grid">
					<div class="landing-jump-wrapper">
						<div class="landing-jump left">
						  <div class="landing-jump-image-wrapper">
						  	<a runat="server" id="hlBottomLeftImage" href="#">
						  		<asp:Literal runat="server" ID="litImageBottomLeft"></asp:Literal>
						  	</a>
						  </div>
						  <p>
						  	<asp:HyperLink runat="server" ID="hlBottomLeft"></asp:HyperLink>
						  </p>
						  <a href="#" class="hover-overlay hide-on-phones" runat="server" id="hlOverlayBottomLeft">
						  	<span class="overlay-heading"><asp:Literal runat="server" ID="litHeadingBottomLeftOverlay"></asp:Literal></span>
						 	<span class="overlay-description"><asp:Literal runat="server" ID="litContentBottomLeftOverlay"></asp:Literal></span>					  	
						 	<span class="default-button black"><asp:Literal runat="server" ID="litButtonBottomLeft"></asp:Literal></span>
						  </a>
						</div>
						<div class="landing-jump">
						  <div class="landing-jump-image-wrapper">
						  	<a runat="server" id="hlBottomRightImage" href="#">
						  		<asp:Literal runat="server" ID="litImageBottomRight"></asp:Literal>
						  	</a>
						  </div>
						  <p>
						  	<asp:HyperLink runat="server" ID="hlBottomRight"></asp:HyperLink>
						  </p>
						  <a href="#" class="hover-overlay hide-on-phones" runat="server" id="hlOverlayBottomRight">
						  	<span class="overlay-heading"><asp:Literal runat="server" ID="litHeadingBottomRightOverlay"></asp:Literal></span>
						 	<span class="overlay-description"><asp:Literal runat="server" ID="litContentBottomRightOverlay"></asp:Literal></span>					  	
						 	<span class="default-button black"><asp:Literal runat="server" ID="litButtonBottomRight"></asp:Literal></span>
						  </a>
						</div>
					</div>
				</div>

				<div class="grid">
					<div class="six columns">

					</div>

					<div class="six columns toggle">
						<div>
							<p class="all-services-toggle">
								<span></span>
								<asp:Literal runat="server" ID="litExpandTitle"></asp:Literal>
							</p>
						</div>
					</div>
				</div>	
		   </div>
		   <div style="clear:both"></div>
		</div>	
		<div class="grid dropdown-content">
			<div class="nine columns">
				<div class="header1"></div>
				<h2><asp:Literal runat="server" ID="litSubTitle"></asp:Literal></h2>
				
                <asp:Literal runat="server" ID="litContent"></asp:Literal>
                
                <sc:sublayout id="slAccordion" runat="server" path="/layouts/base/default/sublayouts/common/Accordion.ascx" />
                <sc:sublayout id="slLandingTab" runat="server" path="/layouts/base/default/sublayouts/common/tabs.ascx" />
			</div>
			<div class="three columns">
                <!-- Callouts -->

                <div class="highlight-box right">
					<h2><asp:Literal runat="server" ID="litPintHeader"></asp:Literal></h2>
					<p><asp:Literal runat="server" ID="litPinkContent"></asp:Literal></p>
					<h3>
						<a id="hlPinkLink" runat="server"><asp:Literal runat="server" ID="litPinkLink"></asp:Literal></a>
					</h3>
				</div>

                <sc:Placeholder runat="server" id="RightPanel" Key="Right Panel"></sc:Placeholder>
			</div>

            <sc:sublayout id="slFeatureCallouts" runat="server" path="/layouts/womans/sublayouts/common/featurecallouts.ascx" />

		</div>	
	</section>	

