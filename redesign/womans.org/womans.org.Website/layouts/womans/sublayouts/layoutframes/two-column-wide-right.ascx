﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="two-column-wide-right.ascx.cs"
    Inherits="womans.org.Website.layouts.womans.sublayouts.layoutframes.two_column_wide_right" %>



    <sc:sublayout id="slheader" runat="server" path="/layouts/womans/sublayouts/common/navigation.ascx" />

<!-- main content -->
<div class="grid">
    <sc:placeholder runat="server" id="Breadcrumb" key="Breadcrumb"></sc:placeholder>
</div>
<div class="main-content grid">
    <sc:placeholder runat="server" id="SectionPanel" key="Section Panel"></sc:placeholder>
    <div class="wide-right">
        <div class="mobile-right-col">
        </div>
        <aside class="three columns">
		                    <sc:Placeholder runat="server" id="LeftPanel" Key="Left Panel"></sc:Placeholder>

		    </aside>
        <section class="nine columns">

<div class="content">   
	        <article>
                <sc:placeholder runat="server" id="section" key="Section"></sc:placeholder>
           
                <h1><asp:Literal runat="server" ID="litHeading"></asp:Literal></h1>
                <asp:Literal runat="server" ID="litSubTitle"></asp:Literal>
                <asp:Literal runat="server" ID="litContent"></asp:Literal>
            </article>

            <sc:Placeholder runat="server" id="ContentPanel" Key="Content Panel"></sc:Placeholder>

            			<div class="callout-wrapper">
	<div class="callout">
		<sc:Placeholder runat="server" id="calloutLeft" key="calloutLeft" ></sc:Placeholder>
	</div>
	 
	<div class="callout">
		<sc:Placeholder runat="server" id="calloutMiddle" key="calloutMiddle" ></sc:Placeholder>
	</div>

	<div class="callout">
		<sc:Placeholder runat="server" id="calloutRight" key="calloutRight" ></sc:Placeholder>
	</div>
</div>




            </div>


		</section>
    </div>
</div>
