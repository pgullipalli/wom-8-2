﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="home.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.layoutframes.home" %>
<section class="main-content">
		<div class="grid home-content">
		   
           		<sc:Placeholder runat="server" id="columnone" key="columnone"></sc:Placeholder>
		   
           <div class="four columns center-callout-col">
                 <sc:Placeholder runat="server" id="columntwo" key="columntwo"></sc:Placeholder>
		   </div>
		   <div class="four columns">
		   		<sc:Placeholder runat="server" id="columnthree" key="columnthree"></sc:Placeholder>
		   </div>

		   <div class="homepage-mobile-container"></div>
		</div>
	</section>
