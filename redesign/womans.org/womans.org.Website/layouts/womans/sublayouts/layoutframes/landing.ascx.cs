﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Womans.CustomItems.PageTemplates.Common;
using Womans.CustomItems.BaseTemplates;
using Womans.CustomItems.ComponentTemplates;
using MedTouch.Common.UI;

namespace womans.org.Website.layouts.womans.sublayouts.layoutframes
{
    public partial class landing : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            PageDataItem page = Sitecore.Context.Item;
            //litImage.Text = page.Image.Rendered;
            litHeading.Text = page.Headline.Rendered;
            //litSubTitle.Text = page.Introduction.Rendered;
            litSubTitle.Text = page.SubHeadline.Rendered;
            
            LandingCTAItem cta = Sitecore.Context.Item;
            
            litContentTopLeftOverlay.Text = cta.BodyTextTopLeft.Rendered;
            litContentTopRightOverlay.Text = cta.BodyTextTopRight.Rendered;
            litContentBottomLeftOverlay.Text = cta.BodyTextBottomLeft.Rendered;
            litContentBottomRightOverlay.Text = cta.BodyTextBottomRight.Rendered;

            litImageTopLeft.Text = cta.BackgroundImageTopLeft.Rendered;
            litImageTopRight.Text = cta.BackgroundImageTopRight.Rendered;
            litImageBottomLeft.Text = cta.BackgroundImageBottomLeft.Rendered;
            litImageBottomRight.Text = cta.BackgroundImageBottomRight.Rendered;

            litHeadingBottomLeftOverlay.Text = cta.HeadingBottomLeft.Rendered;
            litHeadingBottomRightOverlay.Text = cta.HeadingBottomRight.Rendered;
            litHeadingTopLeftOverlay.Text = cta.HeadingTopLeft.Rendered;
            litHeadingTopRightOverlay.Text = cta.HeadingTopRight.Rendered;

            litButtonBottomLeft.Text = cta.ButtonTextBottomLeft.Rendered;
            litButtonBottomRight.Text = cta.ButtonTextBottomRight.Rendered;
            litButtonTopRight.Text = cta.ButtonTextTopRight.Rendered;
            litButtonTopLeft.Text = cta.ButtonTextTopLeft.Rendered;

            hlTopLeft.NavigateUrl = cta.LinkTopLeft.Url;
            hlTopLeft.Text = cta.HeadingTopLeft.Rendered;
            hlTopLeftImage.HRef = cta.LinkTopLeft.Url;
            hlOverlayTopLeft.HRef = cta.LinkTopLeft.Url;

            hlTopRight.NavigateUrl = cta.LinkTopRight.Url;
            hlTopRight.Text = cta.HeadingTopRight.Text;
            hlTopRightImage.HRef = cta.LinkTopRight.Url;
            hlOverlayTopRight.HRef = cta.LinkTopRight.Url;

            hlBottomLeft.NavigateUrl = cta.LinkBottomLeft.Url;
            hlBottomLeft.Text = cta.HeadingBottomLeft.Rendered;
            hlBottomLeftImage.HRef = cta.LinkBottomLeft.Url;
            hlOverlayBottomLeft.HRef = cta.LinkBottomLeft.Url;

            hlBottomRight.NavigateUrl = cta.LinkBottomRight.Url;
            hlBottomRight.Text = cta.HeadingBottomRight.Rendered;
            hlBottomRightImage.HRef = cta.LinkBottomRight.Url;
            hlOverlayBottomRight.HRef = cta.LinkBottomRight.Url;

            litContent.Text = page.ContentCopy.Rendered;
            
            LandingPageItem lPage = Sitecore.Context.Item;
            litExpandTitle.Text = lPage.ExpandTitle.Rendered;
            scTextQuote.Item = lPage;

            imgBackground.Src = lPage.BackgroundImage.MediaUrl;

            litPinkContent.Text = lPage.PinkCalloutBody.Rendered;
            litPintHeader.Text = lPage.PinkCalloutHeader.Rendered;
            litPinkLink.Text = lPage.PinkCalloutLinkText.Rendered;
            hlPinkLink.HRef = lPage.PinkCalloutLink.Url;

        }
    }
}