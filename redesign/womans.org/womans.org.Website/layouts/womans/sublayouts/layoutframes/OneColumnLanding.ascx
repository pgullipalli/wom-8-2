﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OneColumnLanding.ascx.cs"
    Inherits="womans.org.Website.layouts.womans.sublayouts.layoutframes.OneColumnLanding" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<section class="main-content grid landing-pageb">
    <div class="content-copy nine columns">
        <sc:FieldRenderer runat="server" FieldName="Heading" />
        <asp:Repeater runat="server" id="rptJumpItems">
            <HeaderTemplate>
                <div class="twelve columns">
            </HeaderTemplate>
            <ItemTemplate>
                <div class="core-list">
                    <sc:Link runat="server" Item="<%# Container.DataItem %>" Field="Link">
                        <div class="copy">
                            <div class="copy-image">
                                <sc:Image runat="server" Item="<%# Container.DataItem %>" Field="Image" CssClass="left" />
                            </div>
                            <div class="copy-headline">
                                <sc:FieldRenderer runat="server" Item="<%# Container.DataItem %>" FieldName="Heading" EnclosingTag="h2" />
                            </div>
                        </div>
                        <div class="copy-overlay">
                            <div class="copy-overlay-image"><sc:Image runat="server" Item="<%# Container.DataItem %>" Field="Image Overlay" CssClass="left" /></div>
                            <div class="copy-overlay-text"><sc:FieldRenderer runat="server" Item="<%# Container.DataItem %>" FieldName="Body" /></div>
                        </div>
                    </sc:Link>
                </div>
            </ItemTemplate>
            <FooterTemplate>
                </div>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <!--BGIMAGE------------------------->
    <div class="landing-fpo">
        <sc:Image runat="server" Field="Image" />
    </div>
    <!----------------------------PTA----------------------------->
    <asp:Panel runat="server" ID="pnlMiddleContent" CssClass="pta twelve columns">
        <div class="copy eight columns">
            <div class="content">
                <sc:FieldRenderer runat="server" FieldName="Middle Body" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlBottomContent" CssClass="pta twelve columns">
        <div class="copyb ten columns">
            <div class="six columns">
                <sc:FieldRenderer runat="server" FieldName="Bottom Left Content" />
            </div>
            <div class="six columns">
                <div class="content">
                    <sc:FieldRenderer runat="server" FieldName="Bottom Right Content" />
                </div>
            </div>
        </div>
    </asp:Panel>
</section>