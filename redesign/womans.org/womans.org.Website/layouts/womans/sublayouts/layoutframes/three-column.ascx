﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="three-column.ascx.cs" Inherits="womans.org.Website.layouts.womans.sublayouts.layoutframes.three_column" %>


<!-- Mobile CODE: added .mobile-menu -->
<div class="its">
    <a href="#" class="prox"><span class="icon"></span>In This Section</a>
</div>
<nav class="left-nav nav-collapse collapse" role="navigation">
	<div class="leftnav-title"><a href="#">Your Birth Experience</a></div>
	<div class="level-1 nav-item"><a href="#">Types of Deliveries</a></div>
	<div class="expanded"><div class="level-1 nav-item on"><a href="#">Labor Options</a></div>
		<div class="level-2 active nav-item">Cesarean Birth</div>
		<div class="level-2 nav-item"><a href="#">Natural Birthing Options</a></div>
		<div class="level-2 nav-item"><a href="#">Vaginal Delivery</a></div>
		<div class="level-2 nav-item"><a href="#">Scheduled Induction</a></div>
	</div>
	<div class="level-1 nav-item"><a href="#">Labor and Delivery</a></div>
	<div class="level-1 nav-item"><a href="#">Managing Your Pain</a></div>
	<div class="level-1 nav-item"><a href="#">Amenities</a></div>
</nav>		


<!-- Content -->

<article>
	
    <h1><asp:Literal runat="server" ID="litHeading"></asp:Literal></h1>
	
    <asp:Literal runat="server" ID="litSubTitle"></asp:Literal>

    <asp:Literal runat="server" ID="litContent"></asp:Literal>

	
	
	<div class="highlight-box right">
		<h2>Contact Us Today</h2>
		<p>We offer breast cancer detection and treatment in three convenient locations.</p>
		<h3>
			<a href="tel:+2252157981">225-215-7981</a>
		</h3>
	</div>
	
							
</article>



<!-- Callouts -->

<div class="callout-wrapper">
	<div class="callout">
		<h3 class="module-header">Upcoming Events</h3>
		
		<div class="callout-event">
			<div class="event-header">
				<a href="#">
					<span class="event-date">22 Jan</span>
					Woman's Ideal Protein Information
				</a>
			</div>

			<p class="callout-event-preview">Hinc disputationi ei nam, mei etudoctus tamquam suscipit te. Enim mediocritatem vel eine, in qui falli minimum. <a href="#" class="read-more">Read More</a> </p>
		</div>

		<div class="callout-event">
			<div class="event-header">
				<a href="#">
					<span class="event-date">22 Jan</span>
					Woman's Ideal Protein Information
				</a>
			</div>

			<p class="callout-event-preview">Hinc disputationi ei nam, mei etudoctus tamquam suscipit te. Enim mediocritatem vel eine, in qui falli minimum. <a href="#" class="read-more">Read More</a> </p>
		</div>

		<button type="button" class="default-button black inset">View All Events</button>

	</div>
			
	 
	<div class="callout">
		<h3 class="module-header">Patient Stories</h3>
		
		<div class="grid">
			<div class="patient-stories">
				<div class="six columns patient-stories-callout-image">
					<img src="/assets/images/woman-ribbon.png">
				</div>	

				<div class="six columns">
					<p class="patient-story-text">We knew this was a winning combination from the moment we started working with Woman's. This is a dream come true.</p>
					<p class="patient-name">Susan Allen, <br>Cedar Rapids, IA</p>
				</div>	
			</div>
		</div>
	</div>

	<div class="callout">
		<h3 class="module-header">Feature Story</h3>

		<div class="feature-story-callout">
			<a href="#">
				<img src="/assets/images/landing-jump.jpg" style="width: 100%;">
			</a>
			<p class="feature-story-title">
				<a href="#">I am newly diagnosed</a>
			</p>
		</div>
	</div>
</div>