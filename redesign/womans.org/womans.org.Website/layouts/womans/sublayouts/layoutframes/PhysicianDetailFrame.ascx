﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="main-content grid">
    <h1><sc:Text ID="scHeadline" runat="server" Field="Headline" /></h1>
    <div class="wide-left">
	    <div class="mobile-left-col"></div>
	    <section class="nine columns">
            <sc:Placeholder runat="server" id="ContentPanel" Key="Content Panel"></sc:Placeholder>
        </section>	
		
        <aside class="three columns">
            <div class="callout-wrapper">
		        <div class="callout">
                    <sc:Placeholder runat="server" id="RightPanel" Key="Right Panel"></sc:Placeholder>
                </div>
            </div>
        </aside>	
    </div>
</div>

