﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LogoImage.ascx.cs" Inherits="womans.org.Website.layouts.EmailCampaign.LogoImage" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>

<!-- ===== Top Image ===== -->
<sc:Link ID="scLink" runat="server" Field="Link" >
    <sc:Image ID="scImage" runat="server" Field="Image" CssClass="image_fix screen-logo" MaxWidth="175" MaxHeight="130" border="0" />
</sc:Link>
<!-- ===== Top Image ===== -->
