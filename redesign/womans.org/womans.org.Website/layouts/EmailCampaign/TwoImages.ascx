﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TwoImages.ascx.cs" Inherits="womans.org.Website.layouts.EmailCampaign.TwoImages" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>

<!-- ===== Two Images ===== -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: arial, sans-serif; color: #666;">
	<tr>
		<td style="padding: 25px 10px 25px 0; border-bottom: 1px solid #CCC;" class="half">
			<table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif; width: 290px;" class="col-1">
			    <tr>
				    <td style="padding: 0;">
				        <sc:Link runat="server" runat="server" ID="lnkImage1" Field="Link Left"><sc:Image runat="server" ID="imgField1" Field="Image Left" MaxWidth="290" MaxHeight="210" border="0" /></sc:Link>
				    </td>
			    </tr>
			    <tr>
				    <td bgcolor="#ec2b8c" style="padding: 0 15px; font-family: arial, sans-serif;">
					    <h2 style="color: #ffffff;"><sc:Text runat="server" ID="txtHeadline1" Field="Headline Left" /></h2>
				    </td>
			    </tr>
			    <tr>
				    <td style="padding: 20px 0 0 0; font-family: arial, sans-serif; border-bottom: 0;">
				        <sc:Text runat="server" ID="txtContent1" Field="Content Left" />
					</td>				    
                </tr>
				<tr>
				    <td style="font-family: arial, sans-serif; display: block; background-color: #ec2b8c;  border-radius: 5px; padding: 8px; width: 80px; text-align: center;" class="viewmore">
					    <sc:Link runat="server" ID="lnkButton1" Field="Link Left" style="text-transform: uppercase; color: #ffffff; font-size: 12px; text-decoration: none">
						    <asp:Literal ID="litReadMore1" runat="server" />
					    </sc:Link>
				    </td>
			    </tr>
			</table>
		</td>
		<td style="padding: 25px 0 25px 10px; border-bottom: 1px solid #CCC;" class="half">
			<table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif; width: 290px;" class="col-1">
			    <tr>
				    <td style="padding: 0;">
                        <sc:Link runat="server" ID="lnkImage2" Field="Link Right"><sc:Image runat="server" ID="imgField2" Field="Image Right" border="0" MaxWidth="290" MaxHeight="210" /></sc:Link>
                    </td>
			    </tr>
			    <tr>
				    <td bgcolor="#ec2b8c" style="padding: 0 15px; font-family: arial, sans-serif;">
				        <h2 style="color: #ffffff;"><sc:Text runat="server" ID="txtHeadline2" Field="Headline Right" /></h2>
				    </td>
			    </tr>
			    <tr>
				    <td style="padding: 20px 0 0 0; font-family: arial, sans-serif; border-bottom: 0;">
				        <sc:Text runat="server" ID="txtContent2" Field="Content Right" />
                    </td>
				</tr>
				<tr>
				    <td style="font-family: arial, sans-serif; display: block; background-color: #ec2b8c;  border-radius: 5px; padding: 8px; width: 80px; text-align: center;" class="viewmore">
					    <sc:Link runat="server" ID="lnkButton2" Field="Link Right" style="text-transform: uppercase; color: #ffffff; font-size: 12px; text-decoration: none">
						    <asp:Literal ID="litReadMore2" runat="server" />
                        </sc:Link>
				    </td>
			    </tr>
			</table>
		</td>					
	</tr>
</table>
<!-- ===== Two Images ===== -->
