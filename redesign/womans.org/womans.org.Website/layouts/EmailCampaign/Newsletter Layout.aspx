﻿<%@ Page Language="c#" Inherits="System.Web.UI.Page" CodePage="65001" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Woman's eNewsletter</title>
	<style type="text/css">
		#outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
		
		body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
		
		/* Prevent Webkit and Windows Mobile platforms from changing default font sizes.*/ 
		.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */  
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		
		/* Forces Hotmail to display normal line spacing. */ 
		#backgroundTable {margin:0; padding:0; width:100%; line-height: 100% !important;}
		/* End reset */

		/* Some sensible defaults for images
		Bring inline: Yes. */
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
		a img {border:none;} 
		.image_fix {display:block;}

		/* Yahoo paragraph fix
		Bring inline: Yes. */
		p {margin: 1em 0;}

		/* Hotmail header color reset
		Bring inline: Yes. */
		h1, h2, h3, h4, h5, h6 {}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
		color: red !important; /* Preferably not the same color as the normal header link color.  There is limited support for psuedo classes in email clients, this was added just for good measure. */
		}

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
		color: purple !important; /* Preferably not the same color as the normal header link color. There is limited support for psuedo classes in email clients, this was added just for good measure. */
		}
		
		td > * { vertical-align : middle;}

		/* Outlook 07, 10 Padding issue fix
		Bring inline: No.*/
		table td {border-collapse: collapse;}
		.ExternalClass * {line-height: 100%} 

		/* Remove spacing around Outlook 07, 10 tables
		Bring inline: Yes */
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }

		/* Styling your links has become much simpler with the new Yahoo.  In fact, it falls in line with the main credo of styling in email and make sure to bring your styles inline.  Your link colors will be uniform across clients when brought inline.
		Bring inline: Yes. */
		a {color: orange;}
		
		/*STYLE CLIENT SETUP */
		.logo{text-align: left;padding: 15px 0 10px 0;}
		#eNewsletter{width: 600px; margin: 0 auto; font-family: arial, sans-serif; color: #666;}

		body, tfoot{margin: 0; padding: 0;}
		
		p{font-size: 13px; line-height: 20px; margin-top: 0; margin-right: 15px; color: #787878; font-family: arial, sans-serif;}

		h2{margin-top: 10px; margin-bottom: 15px; font-weight: normal; color: #353535; font-family: arial, sans-serif; padding: 5px; line-height: 1.3em;}
		h3{font-size: 13px; font-weight: bold; font-family: arial, sans-serif;padding: 5px;}

		a{color: #ec2b8c; font-weight: bold; display: inline-block;}

		ul{padding: 0;}

		table{position: relative;}
		
		td{padding: 10px 0px 10px 0px; vertical-align: top;}

		#eNewsletter td td{border-bottom: 0px;}

		.important-links a{color: #ec2b8c; text-decoration: none; font-size: 13px;}

		.image-header{background: #ec2b8c; padding: 15px; color: white !important; margin: -4px 0px 25px 0px;}
		
		viewmore {text-transform: uppercase; color: #ffffff; display: block; padding: 8px; background-color: #ec2b8c; width: 30%;font-size: 12px; border-radius: 5px; text-align: center}

		.social-links{overflow: hidden;}
		.social-links td{padding-left: 3px;}
		/*.social-links a{display: block;background-color: #464646;width: 40px;height: 40px;}*/
		.social-links a:hover{background-color: #ec2b8c !important;}


		tfoot{text-align: center;}
		tfoot ul{display: inline-block;font-size: 12px;}
		tfoot td > div{display: block; overflow: hidden; text-align: center;}
		tfoot p{padding-right: 0px; font-size: 12px;}
		.view-email{position: absolute; top: 10px; right: 10px; padding-right: 0;}
		.view-email a{color: #666; font-weight: normal; font-size: 11px;}
		
		/* Latest News Image style */
		/*.news-image-thumbnail{float: left; margin: 0 10px 5px 0;}*/


		/***************************************************
		****************************************************
		MOBILE TARGETING
		****************************************************
		***************************************************/
		@media only screen and (max-width: 600px) {
			/* Part one of controlling phone number linking for mobile. */
			a[href^="tel"], a[href^="sms"] {
						text-decoration: none;
						color: blue; /* or whatever your want */
						pointer-events: none;
						cursor: default;
					}

			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
						text-decoration: default;
						color: orange !important;
						pointer-events: auto;
						cursor: default;
					}
					
					
			body {overflow-x: hidden;}		
			h1, h2, h3 {line-height: initial;}
			.col-1, .col-2, .col-3, .col-4 {width: 100% !important; display: block;}
			
			h2 {line-height: 1.3em !important;}
			
			#backgroundTable {width: 95% !important; margin: 0 auto;}
			#backgroundTable td, #backgroundTable td table {width: 100%; max-width: 600px; padding-left: 0 !important;}			
			.two-thirds, .half {width: 100% !important; padding-left: 0 !important; display: block; padding: 10px 0 !important;}
			.half-no-wrap {width: 50% !important;}
			.two-thirds-no-wrap {width: 50% !important;}
			.half-no-wrap img {width: 100% !important; height: auto;}
			.two-thirds-no-wrap img {width: 100% !important; height: auto;}
			#backgroundTable table.news-header td{ width: auto;}		
			#social {display: none !important;}
			#header-logo {width: 100% !important; text-align: center;}
			#eNewsletter {max-width: 600px; width: 100% !important;}	
			#hero {width: 100% !important; display: block;}
			#hero img {width: 100%; height: auto;}
			.half {display: block; width: 53%; border-bottom: 0 !important; }
			.half h2 {line-height: initial !important;}
			.viewmore {width: 30% !important;}
			.news-teaser {display: initial !important}
			
			.callout-event {padding-bottom: 10px;}
			.six {padding-left: 10px !important;}

		}
		@media only screen and (max-width: 400px) {
		.half {width: auto; padding-top: 0 !important;}
		.half img{width: 100%; height: auto;}
		h2 {font-size: 1.3em;}
		}
		
		@media only screen and (max-device-width: 480px) {
			table[class=table] { 
				width:100% !important;
			}
		}

		/* More Specific Targeting */

		@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
		/* You guessed it, ipad (tablets, smaller screens, etc) */
			/* repeating for the ipad */
			a[href^="tel"], a[href^="sms"] {
						text-decoration: none;
						color: blue; /* or whatever your want */
						pointer-events: none;
						cursor: default;
					}

			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
						text-decoration: default;
						color: orange !important;
						pointer-events: auto;
						cursor: default;
					}
		}

		@media only screen and (-webkit-min-device-pixel-ratio: 2) {
		/* Put your iPhone 4g styles in here */ 
		}

		/* Android targeting */
		@media only screen and (-webkit-device-pixel-ratio:.75){
		/* Put CSS for low density (ldpi) Android layouts in here */
		}
		@media only screen and (-webkit-device-pixel-ratio:1){
		/* Put CSS for medium density (mdpi) Android layouts in here */
		}
		@media only screen and (-webkit-device-pixel-ratio:1.5){
		/* Put CSS for high density (hdpi) Android layouts in here */
		}
		/* end Android targeting */

	</style>

	<!-- Targeting Windows Mobile -->
	<!--[if IEMobile 7]>
	<style type="text/css">
	
	</style>
	<![endif]-->   

	<!-- ***********************************************
	****************************************************
	END MOBILE TARGETING
	****************************************************
	************************************************ -->

	<!--[if gte mso 9]>
		<style>
		/* Target Outlook 2007 and 2010 */
		@font-face {
			font-family: Arial, Helvetica, sans-serif;
		}
		</style>
	<![endif]-->
</head>

<body>
    <form id="frmMain" runat="server">
        <!-- ===== Start of wrapper table ===== -->            
        <table cellpadding="0" cellspacing="0" border="0" width="100%" id="backgroundTable">
            <tr>
	            <td valign="top" width="600" style="border-bottom: 0; padding-bottom: 0"> 
	            <!-- ===== Email CORE ===== -->
        
                <sc:Placeholder ID="nlHeader" Key="nl-header" runat="server" />

                <sc:Placeholder ID="nlBody" Key="nl-body" runat="server" />

                <sc:Placeholder ID="nlFooter" Key="nl-footer" runat="server" />
        

                <!-- ===== Email CORE ===== -->
	            </td>
            </tr>
        </table>  
        <!-- ===== End of wrapper table ===== -->
    </form>
</body>
</html>

