﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="womans.org.Website.layouts.EmailCampaign.Header" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>

<!-- ===== Header ===== -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: arial, sans-serif; color: #666;">
	<tr>
		<td valign="top" width="300" style="border-bottom: 0;" id="header-logo">
			<sc:Placeholder ID="nltopimage" Key="nl-top-image" runat="server" />
		</td>
		<td valign="top"  width="300" style="border-bottom: 0;" id="social">
			<table class="social-links" cellpadding="0" cellspacing="0" border="0" width="300" style="font-family: arial, sans-serif;">
				<tr>
					<td colspan="6" style="text-align: right; padding-bottom: 40px; vertical-align: middle; ont-family: arial, sans-serif;" valign="middle">
					    <a href="#" style="color: #787878;font-size: 11px;" runat="server" id="hrefBrowser"><asp:Literal runat="server" ID="litBrowser"></asp:Literal></a>
                    </td>
				</tr>
				<tr>
					<td class="facebook-link" valign="middle" align="center" width="40" height="40" bgcolor="#464646" style="padding: 0; border-right: 3px solid #FFF; vertical-align: middle;">
                        <sc:Link ID="scLinkFacebook" runat="server" Field="Facebook Link" style="display: block; padding-bottom: 14px; padding-top: 15px; background-color: #464646;">
                            <sc:Image ID="scImageFacebook" Field="Facebook Icon" runat="server" MaxWidth="8" MaxHeight="16" border="0" style="vertical-align: middle; display:block;" />
                        </sc:Link>
					</td>
					<td class="twitter-link" align="center" valign="middle" width="40" height="40" bgcolor="#464646" style="padding: 0; border-right: 3px solid #FFF; vertical-align: middle;">
                        <sc:Link ID="scLinkTwitter" runat="server" Field="Twitter Link" style="display: block; padding-bottom: 14px; padding-top: 15px; background-color: #464646;">
                            <sc:Image ID="scImageTwitter" Field="Twitter Icon" runat="server" MaxWidth="24" MaxHeight="16" border="0" style="vertical-align: middle; display:block; " />
                        </sc:Link>
					</td>
					<td class="youtube-link" valign="middle" align="center" width="40" height="40" bgcolor="#464646" style="padding: 0; border-right: 3px solid #FFF; vertical-align: middle;">
                        <sc:Link ID="scLinkYoutube" runat="server" Field="Youtube Link" style="display: block; padding-bottom: 14px; padding-top: 15px; background-color: #464646;">
                            <sc:Image ID="scImageYoutube" Field="Youtube Icon" runat="server" MaxWidth="18" MaxHeight="16" border="0" style="vertical-align: middle; display:block;" />
                        </sc:Link>
					</td>
					<td class="wp-link" align="center" valign="middle" width="40" height="40" bgcolor="#464646" style="padding: 0; border-right: 3px solid #FFF; vertical-align: middle;">
                        <sc:Link ID="scLinkBlog" runat="server" Field="Blog Link" style="display: block;  padding-bottom: 12px; padding-top: 12px; background-color: #464646;">
                            <sc:Image ID="scImageBlog" Field="Blog Icon" runat="server" MaxWidth="21" MaxHeight="21" border="0" style="vertical-align: middle; display:block;" />
                        </sc:Link>
					</td>
					<td class="pinterest-link" valign="middle" align="center" width="40" height="40" bgcolor="#464646" style="padding: 0; border-right: 3px solid #FFF; vertical-align: middle;">
                        <sc:Link ID="scLinkPinterest" runat="server" Field="Pinterest Link" style="display: block; padding-bottom: 14px; padding-top: 15px; background-color: #464646;">
                            <sc:Image ID="scImagePinterest" Field="Pinterest Icon" runat="server" MaxWidth="13" MaxHeight="16" border="0" style="vertical-align: middle; display:block;" />
                        </sc:Link>
					</td>
					<td class="instagram-link" valign="middle" align="center" width="40" height="40" bgcolor="#464646" style="padding: 0; border-right: 3px solid #FFF; vertical-align: middle;">
                        <sc:Link ID="scLinkInstagram" runat="server" Field="Instagram Link" style="display: block; padding-bottom: 14px; padding-top: 16px;background-color: #464646;">
                            <sc:Image ID="scImageInstagram" Field="Instagram Icon" runat="server" MaxWidth="16" MaxHeight="15" border="0" style="vertical-align: middle; display:block;" />
                        </sc:Link>
					</td>
				</tr>
			</table>					
		</td>
	</tr>
</table>
<!-- ===== Header ===== -->