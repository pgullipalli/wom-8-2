﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.UI;
using Womans.CustomItems.Messages.InnerContent.Newsletter;

namespace womans.org.Website.layouts.EmailCampaign
{
    public partial class TwoColumnThreeQuarterWidth : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            ThreeQuarterWidthColumnItem currItem = base.ContextItem;

            if (currItem != null)
            {
                txtColumn1.Item = currItem;
                txtColumn2.Item = currItem;

            }

        }
    }
}