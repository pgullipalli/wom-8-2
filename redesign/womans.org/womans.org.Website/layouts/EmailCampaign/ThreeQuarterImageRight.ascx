﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ThreeQuarterImageRight.ascx.cs" Inherits="womans.org.Website.layouts.EmailCampaign.ThreeQuarterImageRight" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>

<!-- ===== Three Quarter Image Right ===== -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: arial, sans-serif; color: #666;">					
	<tr>
		<td style="border-bottom: 1px solid #CCC;">
			<table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;">
				<tr>
					<td style="padding: 25px 10px 25px 0; width: 75%;">
						<h2><sc:Text ID="scTextTitle" runat="server" Field="Title" /></h2>
						<p>
						    <sc:Text ID="scTextContent" runat="server" Field="Content" />
						    <sc:Link ID="scLink" runat="server" Field="Link" style="color: #EC2B8C;" />
						</p>
					</td>
					<td style="padding: 25px 0 25px 10px; font-family: arial, sans-serif;" class="three-fourth">
					    <sc:Image ID="scImage" runat="server" Field="Image" MaxWidth="130" MaxHeight="210" />
					</td>
				</tr>
			</table>
		</td>			
	</tr>
</table>
<!-- ===== Three Quarter Image Right ===== -->
						