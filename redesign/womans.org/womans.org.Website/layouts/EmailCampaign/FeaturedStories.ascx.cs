﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.UI;
using womans.org.BusinessLogic.Messages.InnerContent.Newsletter;

namespace womans.org.Website.layouts.EmailCampaign
{
    public partial class FeaturedStories : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            TwoImagesTwoLinksItem currentItem = base.ContextItem;

            if (currentItem != null)
            {
                scTextTitle.Item = currentItem;

                scLinkLeft.Item = currentItem;
                scImageLeft.Item = currentItem;
                scLinkLeft1.Item = currentItem;

                scLinkRight.Item = currentItem;
                scImageRight.Item = currentItem;
                scLinkRight1.Item = currentItem;
            }
        }
    }
}