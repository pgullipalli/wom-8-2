﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.UI;
using Womans.CustomItems.Messages.InnerContent.Newsletter;

namespace womans.org.Website.layouts.EmailCampaign
{
    public partial class TwoImages : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            HalfWidthImageBothItem currItem = base.ContextItem;

            imgField1.Item = currItem;
            imgField2.Item = currItem;

            txtHeadline1.Item = currItem;
            txtHeadline2.Item = currItem;

            txtContent1.Item = currItem;
            txtContent2.Item = currItem;

            lnkImage1.Item = currItem;
            lnkImage2.Item = currItem;

            litReadMore1.Text = "Read More";
            litReadMore2.Text = "Read More";

            lnkButton1.Item = currItem;
            lnkButton2.Item = currItem;
        }
    }
}