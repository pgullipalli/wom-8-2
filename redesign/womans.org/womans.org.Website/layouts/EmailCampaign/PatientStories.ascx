﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PatientStories.ascx.cs" 
    Inherits="womans.org.Website.layouts.EmailCampaign.PatientStories" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<!-- ===== Patient Stories ===== -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: arial, sans-serif; color: #666;">				
	<tr>
		<td style="padding: 25px 0; border-bottom: 1px solid #CCC;">
			<table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;">
				<tr>
					<td style="padding: 0px; font-family: arial, sans-serif;">
						<h2 style="margin: 0; padding: 10px 0;"><sc:Text ID="scTextTitle" runat="server" Field="Title" /></h2>
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;">
				<tr>
					<td style="border-bottom: 1px solid #CCC; font-family: arial, sans-serif; width: 290px;" class="half">
						<table class="patient-stories" cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="width: 30%; padding-bottom: 0; padding-right: 10px;" class="six columns patient-stories-callout-image">
									<sc:Image ID="scImageLeft" runat="server" Field="Image Left" MaxWidth="125" MaxHeight="165" />
								</td>
								<td style="width: 65%; padding-bottom: 0; padding-left: 10px;" class="six columns">
									<sc:Text ID="scTextContentLeft" runat="server" Field="Content Left" />
								</td>
							</tr>
						</table>
					</td>
					<td style="border-bottom: 1px solid #CCC; font-family: arial, sans-serif; width: 290px;" class="half">
						<table class="patient-stories" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td style="width: 30%; padding-bottom: 0; padding-right: 10px;" class="six columns patient-stories-callout-image">
									<sc:Image ID="scImageRight" runat="server" Field="Image Right" MaxWidth="125" MaxHeight="165" />
								</td>
								<td style="width: 65%; padding-bottom: 0; padding-left: 10px;" class="six columns">
									<sc:Text ID="scTextContentRight" runat="server" Field="Content Right" />
								</td>
							</tr>
						</table>
					</td>
				</tr>							
			</table>
		</td>
	</tr>								
</table>
<!-- ===== Patient Stories ===== -->