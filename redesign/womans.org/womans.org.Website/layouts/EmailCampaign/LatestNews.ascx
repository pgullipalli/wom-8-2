﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="LatestNews.ascx.cs" Inherits="womans.org.Website.layouts.EmailCampaign.LatestNews" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<!-- ===== Latest News ===== -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: arial, sans-serif; color: #666;">				
	<tr>
		<td style="padding: 25px 0; border-bottom: 1px solid #CCC;">
		    <table id="tablePropEdit" runat="server" Visible="false" cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;">
				<tr>
					<td style="font-family: arial, sans-serif; display: block; background-color: #ec2b8c;  border-radius: 5px; padding: 8px; text-align: center;" class="viewmore">
					    <sc:EditFrame ID="editLinks" runat="server" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Newsletter Tagging Properties">
						    <h3 style="margin: 0; padding: 10px 0; color: #ffffff;">Edit Tagging Properties</h3>
                        </sc:EditFrame>
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;">
				<tr>
					<td style="padding: 0px; font-family: arial, sans-serif;">
					    <h2 style="margin: 0; padding: 10px 0;"><sc:Text runat="server" ID="txtHeadline" Field="Title" /></h2>
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;">
				<tr>
                    <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_ItemDataBound">
                        <ItemTemplate>
                            <td style="width: 200px; padding-bottom: 0; font-family: arial, sans-serif;" class="col-1">
						        <div class="callout-event">
						            <%--<asp:Hyperlink runat="server" ID="lnkNewsItem" style="text-decoration: none; color: #EC2B8C; font-weight: bold;">--%>
						                <table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;" class="news-header">
						                    <tr>
						                        <td id="tdThumbnail" runat="server" style="padding-right: 10px;">
						                            <sc:Image ID="scThumbnail" Field="News Image" runat="server" MaxWidth="50" MaxHeight="50" CssClass="news-image-thumbnail" DisableWebEditing="True" />
                                                </td>
                                                <td style="padding-right: 10px;">
                                                    <asp:HyperLink ID="lnkNewsItemTitle" runat="server" style="text-decoration: none; color: #EC2B8C; font-weight: bold;"><span class="news-title" style="display: block; margin-bottom: 10px; color:#EC2B8C;"><asp:Literal ID="litItem" runat="server" /></span></asp:HyperLink>
                                                </td>
						                    </tr>
                                        </table>							            
								        <p>								            
								            <span class="news-teaser" style="color: #787878; font-size: 13px; font-weight: normal; display: block; clear: both;"><asp:Literal ID="litTeaser" runat="server" /></span>
								            <asp:HyperLink ID="lnkNewsItemReadMore" runat="server" style="text-decoration: none; color: #EC2B8C; font-weight: bold;"><span class="read-more" style="font-size: 13px; font-weight: normal; color:#EC2B8C;"><asp:Literal ID="litMoreLink" runat="server" /></span></asp:HyperLink>
									    </p>
							        <%--</asp:Hyperlink>--%>
						        </div>
					        </td>	
                        </ItemTemplate>
                    </asp:ListView>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!-- ===== Latest News ===== -->