﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TwoThirdImageLeft.ascx.cs" Inherits="womans.org.Website.layouts.EmailCampaign.TwoThirdImageLeft" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>

<!-- ===== Two Third Image Left ===== -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: arial, sans-serif; color: #666;">					
	<tr>
		<td style="border-bottom: 1px solid #CCC;">
			<table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;">
				<tr>
					<td style="padding: 25px 10px 25px 0; font-family: arial, sans-serif; width: 33%;" class="two-thirds-no-wrap">
					    <sc:Image ID="scImage" runat="server" Field="Image" MaxWidth="200" MaxHeight="210" />
					</td>
					<td style="padding: 25px 0 25px 10px;" class="two-thirds-no-wrap">
						<h2><sc:Text ID="scTextTitle" runat="server" Field="Title" /></h2>
						<p>
						    <sc:Text ID="scTextContent" runat="server" Field="Content" />
						    <sc:Link ID="scLink" runat="server" Field="Link" style="color: #EC2B8C;" />
						</p>
					</td>
				</tr>
			</table>
		</td>			
	</tr>
</table>
<!-- ===== Two Third Image Left ===== -->						