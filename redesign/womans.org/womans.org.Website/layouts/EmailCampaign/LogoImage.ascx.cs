﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.UI;
using Womans.CustomItems.Messages.InnerContent.Newsletter;

namespace womans.org.Website.layouts.EmailCampaign
{
    public partial class LogoImage : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            LinkWithImageItem currentItem = base.ContextItem;
            if (currentItem != null)
            {
                scLink.Item = currentItem;
                scImage.Item = currentItem;
            }
        }
    }
}