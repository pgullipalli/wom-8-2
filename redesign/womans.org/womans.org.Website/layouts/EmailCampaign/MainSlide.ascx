﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainSlide.ascx.cs" Inherits="womans.org.Website.layouts.EmailCampaign.MainSlide" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>

<!-- ===== Main Slide ===== -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: arial, sans-serif; color: #666;">
    <tr>
	    <td style="padding-bottom: 25px; border-bottom: 1px solid #CCC;" id="hero">
		    <table cellpadding="0" cellspacing="0" border="0" width="600" style="font-family: arial, sans-serif; width: 100%;">
		        <tr>
		            <td style="padding: 0;">
		                <sc:Link runat="server" Field="Link" ID="lnkSlide"><sc:image ID="imgSlide" runat="server" style="vertical-align: top;" border="0" Field="Image" MaxWidth="600" MaxHeight="250" /></sc:Link>
		            </td>
		        </tr>
		        <tr>
			        <td bgcolor="#ec2b8c" style="padding: 0 15px; font-family: arial, sans-serif;">
			            <h2 style="color: #ffffff;"><font face="Arial"><sc:Text ID="txtHeadline" runat="server" Field="Headline" /></font></h2>
			        </td>
		        </tr>
		    </table>
	    </td>
    </tr>
</table>
<!-- ===== Main Slide ===== -->            
            