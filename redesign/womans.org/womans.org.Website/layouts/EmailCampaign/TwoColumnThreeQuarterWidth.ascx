﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TwoColumnThreeQuarterWidth.ascx.cs" Inherits="womans.org.Website.layouts.EmailCampaign.TwoColumnThreeQuarterWidth" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>

<!-- ===== Three Quarter Width Column ===== -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: arial, sans-serif; color: #666;">		
	<tr>
		<td style="padding-top: 10px; padding-bottom: 25px; border-bottom: 1px solid #CCC;">
			<table cellpadding="0" cellspacing="0" border="0" width="600" style="font-family: arial, sans-serif;">
				<tr>
					<td style="width: 60%; padding-top:0; padding-bottom: 0; padding-right: 10px; font-family: arial, sans-serif; border-bottom: 0;" class="col-2">
						<sc:Text runat="server" id="txtColumn1" Field="Content Column 1" />
					</td>

					<td style="width: 35%; font-family: arial, sans-serif; border-bottom: 0;" align="left" class="col-3">
					    <sc:Text runat="server" id="txtColumn2" Field="Content Column 2" />                   
					</td>
				</tr>
			</table>						
		</td>
	</tr>
</table>
<!-- ===== Three Quarter Width Column ===== -->