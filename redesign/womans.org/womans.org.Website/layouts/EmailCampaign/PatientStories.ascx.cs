﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.UI;
using womans.org.BusinessLogic.Messages.InnerContent.Newsletter;

namespace womans.org.Website.layouts.EmailCampaign
{
    public partial class PatientStories : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            TwoImagesTwoContentsItem currentItem = base.ContextItem;

            if (currentItem != null)
            {
                scTextTitle.Item = currentItem;

                scImageLeft.Item = currentItem;
                scTextContentLeft.Item = currentItem;

                scImageRight.Item = currentItem;
                scTextContentRight.Item = currentItem;
            }
        }
    }
}