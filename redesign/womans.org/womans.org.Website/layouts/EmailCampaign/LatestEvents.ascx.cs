﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using MedTouch.Calendar.Helpers;
using MedTouch.Calendar.Search;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
//using MedTouch.News.Helpers;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Express;
using Sitecore.Links;
using womans.org.BusinessLogic.Messages.InnerContent.Newsletter;

namespace womans.org.Website.layouts.EmailCampaign
{
    public partial class LatestEvents : BaseSublayout
    {
        #region Dictionaries        
        protected readonly string DictionaryReadMoreLink = CultureHelper.GetDictionaryTranslation("EmailCampaign.Newsletter.NewsletterLatestEvents.ReadMoreLink");
        #endregion Dictionaries

        #region Fields
        // Default Value of RelatedEvent Control.
        private const int DefaultNumberOfDisplayPages = 1;
        private const int DefaultNumberOfDisplayedItems = 2;
        private const string DefaultCurrentPage = "1";

        private int? renderingParamMaxTeaserCharacters;
        private int? renderingParamNumberOfDisplayedItems;
        private bool? renderingParamRecursiveTaxonomy;
        private bool? renderingParamShowThumbnail;
        private string renderingParamDateFormat;
        private bool? renderingParamShowTitleAsLink;
        private bool? renderingParamShowLinkToDetailPage;
        private bool? renderingParamSuppressWhenNothingMatches;
        private bool? renderingParamIncludeOngoing;
        private Item renderingParamViewAllLink;
        #endregion

        #region RenderingParameters
        protected int RenderingParamNumberOfDisplayedItems
        {
            get
            {
                if (!this.renderingParamNumberOfDisplayedItems.HasValue)
                {
                    this.renderingParamNumberOfDisplayedItems = this.GetIntProperty("Number of Displayed Items");
                }

                return this.renderingParamNumberOfDisplayedItems.GetValueOrDefault(1);
            }
        }

        protected int RenderingParamMaxTeaserCharacters
        {
            get
            {
                if (!this.renderingParamMaxTeaserCharacters.HasValue)
                {
                    this.renderingParamMaxTeaserCharacters = this.GetIntProperty("Max Teaser Characters");
                }

                return this.renderingParamMaxTeaserCharacters.GetValueOrDefault(100);
            }
        }

        protected bool RenderingParamShowTitleAsLink
        {
            get
            {
                if (!this.renderingParamShowTitleAsLink.HasValue)
                {
                    this.renderingParamShowTitleAsLink = this.GetBooleanProperty("Show Title as Link");
                }

                return this.renderingParamShowTitleAsLink.GetValueOrDefault(true);
            }
        }

        protected bool RenderingParamShowLinkToDetailPage
        {
            get
            {
                if (!this.renderingParamShowLinkToDetailPage.HasValue)
                {
                    this.renderingParamShowLinkToDetailPage = this.GetBooleanProperty("Show Link to Detail Page");
                }

                return this.renderingParamShowLinkToDetailPage.GetValueOrDefault(true);
            }
        }

        protected bool RenderingParamRecursiveTaxonomy
        {
            get
            {
                if (!this.renderingParamRecursiveTaxonomy.HasValue)
                {
                    this.renderingParamRecursiveTaxonomy = this.GetBooleanProperty("RecursiveTaxonomy");
                }

                return this.renderingParamRecursiveTaxonomy.GetValueOrDefault(true);
            }
        }

        protected bool RenderingParamShowThumbnail
        {
            get
            {
                if (!this.renderingParamShowThumbnail.HasValue)
                {
                    this.renderingParamShowThumbnail = this.GetBooleanProperty("ShowThumbnail");
                }

                return this.renderingParamShowThumbnail.GetValueOrDefault(false);
            }
        }

        protected string RenderingParamDateFormat
        {
            get
            {
                if (string.IsNullOrEmpty(this.renderingParamDateFormat))
                {
                    this.renderingParamDateFormat = this.GetProperty("DateFormat") ?? "";
                }

                return this.renderingParamDateFormat;
            }
        }

        protected bool RenderingParamSuppressWhenNothingMatches
        {
            get
            {
                if (!this.renderingParamSuppressWhenNothingMatches.HasValue)
                {
                    this.renderingParamSuppressWhenNothingMatches = this.GetBooleanProperty("SuppressWhenNothingMatches");
                }

                return this.renderingParamSuppressWhenNothingMatches.GetValueOrDefault(true);
            }
        }

        protected bool RenderingParamIncludeOngoing
        {
            get
            {
                if (!this.renderingParamIncludeOngoing.HasValue)
                {
                    this.renderingParamIncludeOngoing = this.GetBooleanProperty("IncludeOngoing");
                }

                return this.renderingParamIncludeOngoing.GetValueOrDefault(true);
            }
        }

        protected Item RenderingParamViewAllLink
        {
            get
            {
                if (this.renderingParamViewAllLink == null)
                {
                    this.renderingParamViewAllLink = this.GetSingleReferenceProperty("ViewAllLink");
                }

                return this.renderingParamViewAllLink;
            }
        }
        #endregion Properties

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;
            
            tablePropEdit.Visible = Sitecore.Context.PageMode.IsPageEditor;
            editLinks.DataSource = base.ContextItem.ID.ToString();

            RelatedContentItem currentItem = base.ContextItem;
            if (currentItem != null)
            {
                txtHeadline.Item = currentItem;                
            }

            Bind();
        }

        protected void rptRelatedEvents_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem && e.Item.ItemType != ListItemType.Item) 
                return;

            Item sessionItem = e.Item.DataItem as Item;
            if (sessionItem == null) 
                return;

            Item eventTopicItem = ModuleHelper.GetEventTopicFromEventSession(sessionItem);
            string strEventTopicLink = ModuleHelper.GetEventTopicLinkUrl(eventTopicItem, sessionItem);

            HyperLink hlEventTitle = (HyperLink) e.Item.FindControl("hlEventTitle");
            Literal litSessionDate = (Literal) e.Item.FindControl("litSessionDate");
            Literal litEventTitle = (Literal) e.Item.FindControl("litEventTitle");
            Literal litTeaser = (Literal) e.Item.FindControl("litTeaser");
            HyperLink hlReadMore = (HyperLink) e.Item.FindControl("hlReadMore");

            // Title
            string strHeadline = ItemHelper.GetFieldHtmlValue(eventTopicItem, ItemMapper.EventTopic.FieldName.Headline);
            if (hlEventTitle != null && litEventTitle != null)
            {
                hlEventTitle.NavigateUrl = strEventTopicLink;
                litEventTitle.Text = strHeadline;
                if (litSessionDate != null)
                {
                    litSessionDate.Text = RegistrationHelper.GetSessionDateTimeString(sessionItem, false, RenderingParamDateFormat);
                }
            }

            // Teaser
            string strTeaser = ItemHelper.GetItemTeaser(eventTopicItem, this.RenderingParamMaxTeaserCharacters);
            if (litTeaser != null && hlReadMore != null && !string.IsNullOrEmpty(strTeaser))
            {
                litTeaser.Text = strTeaser;
                hlReadMore.Text = DictionaryReadMoreLink;
                hlReadMore.NavigateUrl = strEventTopicLink;
            }
        }

        protected override void Bind()
        {
            if (ContextItem == null)
            {
                return;
            }

            string taxonomyItems = ItemHelper.GetShortIds(ContextItem, "Taxonomy Terms");
            string taxonomy = SitecoreSearchHelper.GetTagTaxonomyGuidsFromShortIds(taxonomyItems, this.RenderingParamRecursiveTaxonomy);

            string specialtyItems = ItemHelper.GetFieldRawValue(ContextItem, "Specialties");
            string locationItems = ItemHelper.GetFieldRawValue(ContextItem, "Locations");

            var searchResultsItem = ModuleHelper.GetSearchResultPageItem(RenderingParamViewAllLink);

            // Types of events to list: Dated (default) or Ongoing
            EventListType eventListType =
                Request.QueryString[ModuleHelper.Config.QueryString.GetEventTypeQuerystring()] == "Ongoing"
                    ? EventListType.Ongoing
                    : EventListType.Dated;

            // Search Param
            var eventSearchParam = new CalendarSearchParam()
            {
                Keyword = string.Empty,
                Category = string.Empty,
                DateFrom = DateTime.Now.ToString(ModuleHelper.Config.DefaultDateFormat()),
                DateTo = string.Empty,
                SearchType = CalendarSearchType.EventSession,
                ListType = eventListType,
                IncludeOngoing = RenderingParamIncludeOngoing,
                Locations = locationItems,
                Specialties = specialtyItems,
                Taxonomy = taxonomy,
                IsTaxonomyRecursive = this.RenderingParamRecursiveTaxonomy
            };

            List<Item> items = new List<Item>();
            bool displayAllContent = false;

            int totalCount = 0;
            int startItem = 0;
            int endItem = 0;

            // Search
            ModuleHelper.CalendarSearch(
                eventSearchParam,
                DefaultCurrentPage,
                DefaultNumberOfDisplayedItems,
                DefaultNumberOfDisplayPages,
                out startItem,
                out endItem,
                out totalCount,
                out items);

            if (!items.Any() && !this.RenderingParamSuppressWhenNothingMatches)
            {
                // Whet not suppress, return all Event Items
                eventSearchParam.Locations = string.Empty;
                eventSearchParam.Specialties = string.Empty;
                eventSearchParam.Taxonomy = string.Empty;

                // Search
                ModuleHelper.CalendarSearch(
                    eventSearchParam,
                    DefaultCurrentPage,
                    DefaultNumberOfDisplayedItems,
                    DefaultNumberOfDisplayPages,
                    out startItem,
                    out endItem,
                    out totalCount,
                    out items);

                displayAllContent = true;
            }

            //if (items.Any())
            //{
            //    if (searchResultsItem != null)
            //    {

            //        // Setup base "view all" link
            //        string strSpecialties = ItemHelper.GetItemNamesFromItemGuids(specialtyItems);
            //        string strLocations = ItemHelper.GetItemNamesFromItemGuids(locationItems);
            //        string viewAllUrl = string.Empty;
            //        if (displayAllContent)
            //        {
            //            viewAllUrl = BaseModuleHelper.GetSearchResultsUrl(
            //                string.Empty,
            //                string.Empty,
            //                string.Empty,
            //                false,
            //                searchResultsItem);
            //        }
            //        else
            //        {
            //            viewAllUrl = BaseModuleHelper.GetSearchResultsUrl(
            //                strSpecialties,
            //                strLocations,
            //                taxonomyItems,
            //                RenderingParamRecursiveTaxonomy,
            //                searchResultsItem);
            //        }

            //        //// "View All" link only if all items aren't displayed
            //        //if (totalCount > RenderingParamNumberOfDisplayedItems)
            //        //{
            //        //    pnlViewAll.Visible = true;
            //        //    hlViewAll.NavigateUrl = viewAllUrl;
            //        //    hlViewAll.Text = DictionaryViewAll;
            //        //    hlViewAll.Visible = true;
            //        //}

            //        // "View Ongoing" link if there are any ongoing items that meet criteria
            //        // First: find out if there ARE any ongoing items to show in the list before showing the View Ongoing link
            //        eventSearchParam.ListType = EventListType.Ongoing;
            //        CalendarSearcher calendarSearcher = new CalendarSearcher();
            //        var ongoingItems = calendarSearcher.ExecuteSearch(eventSearchParam);
            //        if (ongoingItems.Count > 0)
            //        {
            //            //pnlViewAll.Visible = true;
            //            //hlViewOngoing.NavigateUrl = viewAllUrl + (viewAllUrl.Contains("?") ? "&" : "?")
            //            //                            + ModuleHelper.Config.QueryString.GetEventTypeQuerystring()
            //            //                            + "=Ongoing";
            //            //hlViewOngoing.Text = DictionaryViewOngoing;
            //            //hlViewOngoing.Visible = true;
            //        }
            //    }
            //}
            //else
            //{
            //    phRelatedEvents.Visible = false;
            //}

            // Bind Label
            //lblRelatedEventsHeader.Text = DictionaryRelatedEventsLabel;

            // Bind RelatedEvent
            rptRelatedEvents.DataSource = items.Take(2);
            rptRelatedEvents.DataBind();
        }
    }
}