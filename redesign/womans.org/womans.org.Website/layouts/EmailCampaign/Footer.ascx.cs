﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.UI;
using Womans.CustomItems.Messages.InnerContent;

namespace womans.org.Website.layouts.EmailCampaign
{
    public partial class Footer : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            FooterItem currentItem = base.ContextItem;
            if (currentItem != null)
            {
                scTextFooterLinks.Item = currentItem;
                scTextCopyrightText.Item = currentItem;
            }
        }
    }
}