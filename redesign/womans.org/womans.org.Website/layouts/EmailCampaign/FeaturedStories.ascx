﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeaturedStories.ascx.cs" 
    Inherits="womans.org.Website.layouts.EmailCampaign.FeaturedStories" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<!-- ===== Featured Stories ===== -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: arial, sans-serif; color: #666;">	
	<tbody>
        <tr>
		    <td style="padding: 25px 0; border-bottom: 1px solid #CCC;">
			    <table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;">
				    <tbody>
				        <tr>
					        <td style="padding: 0px; font-family: arial, sans-serif;">
					            <h2 style="margin: 0; padding: 10px 0;"><sc:Text ID="scTextTitle" runat="server" Field="Title" /></h2>
					        </td>
				        </tr>
			        </tbody>
                </table>						
			    <table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;">
				    <tbody>
				        <tr>
					        <td style="padding: 25px 10px 25px 0;" class="half">
						        <table class="patient-stories" cellpadding="0" cellspacing="0" border="0" width="100%">
							        <tbody>
							            <tr>
								            <td style="width: 30%; padding-bottom: 0;" class="six columns patient-stories-callout-image">
									            <div class="feature-story-callout">
										            <sc:Link ID="scLinkLeft" runat="server" Field="Link Left">
										                <sc:Image ID="scImageLeft" runat="server" Field="Image Left" MaxWidth="287" MaxHeight="216" />
										            </sc:Link>
										            <p class="feature-story-title" style="margin-bottom: 0;">
										                <sc:Link  ID="scLinkLeft1" runat="server" Field="Link Left" style="padding: 0; text-align: center; display: block; color:#EC2B8C;" />
										            </p>
									            </div>
								            </td>	
							            </tr>
						            </tbody>
                                </table>
					        </td>
					        <td style="padding: 25px 0 25px 10px;" class="half">
						        <table class="patient-stories" cellpadding="0" cellspacing="0" border="0" width="100%">
							        <tbody>
							            <tr>
								            <td style="width: 30%; padding-bottom: 0;" class="six columns patient-stories-callout-image">
									            <div class="feature-story-callout">
										            <sc:Link ID="scLinkRight" runat="server" Field="Link Right">
										                <sc:Image ID="scImageRight" runat="server" Field="Image Right" MaxWidth="287" MaxHeight="216" />
										            </sc:Link>
										            <p class="feature-story-title" style="margin-bottom: 0;">
										                <sc:Link ID="scLinkRight1" runat="server" Field="Link Right" style="padding: 0; text-align: center; display: block; color:#EC2B8C;" />
										            </p>
									            </div>
								            </td>	
							            </tr>
						            </tbody>
                                </table>
					        </td>
				        </tr>							
			        </tbody>
                </table>												
		    </td>
	    </tr>
    </tbody>
</table>
<!-- ===== Featured Stories ===== -->