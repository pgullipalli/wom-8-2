﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HalfWidthImageRight.ascx.cs" Inherits="womans.org.Website.layouts.EmailCampaign.HalfWidthHeaderLeft" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>

<!-- ===== Half Width Image Right ===== -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: arial, sans-serif; color: #666;">				
	<tr>
		<td style="border-bottom: 1px solid #CCC;">
			<table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;">
				<tr>
					<td style="padding: 25px 10px 25px 0; font-family: arial, sans-serif;" class="half-no-wrap">
					    <sc:Text runat="server" ID="txtContent" Field="Content"/>
					</td>
					<td style="padding: 25px 0 25px 10px;" class="half-no-wrap">
					    <sc:Link runat="server" ID="lnkImage" Field="Link"><sc:Image runat="server" ID="imgField" Field="Image" MaxWidth="290" MaxHeight="210"/></sc:Link>
					</td>
				</tr>							
			</table>
		</td>
	</tr>
</table>
<!-- ===== Half Width Image Right ===== -->

