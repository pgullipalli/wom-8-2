﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.UI;
using Womans.CustomItems.Messages.InnerContent.Newsletter;

namespace womans.org.Website.layouts.EmailCampaign
{
    public partial class Header : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            HeaderItem currentItem = base.ContextItem;

            if (currentItem != null)
            {
                litBrowser.Text = currentItem.LinkText.Rendered;
                if (currentItem.LinktoSelf.Checked)
                {
                    hrefBrowser.HRef = Sitecore.Links.LinkManager.GetItemUrl(Sitecore.Context.Item);
                }
                else
                {
                    try
                    {
                        hrefBrowser.HRef = currentItem.Destination.Url;
                    }
                    catch
                    {
                        hrefBrowser.HRef = Sitecore.Links.LinkManager.GetItemUrl(Sitecore.Context.Item);
                    }
                }

                scLinkTwitter.Item = currentItem;
                scImageTwitter.Item = currentItem;
                scLinkBlog.Item = currentItem;
                scImageBlog.Item = currentItem;
                scLinkFacebook.Item = currentItem;
                scImageFacebook.Item = currentItem;
                scLinkPinterest.Item = currentItem;
                scImagePinterest.Item = currentItem;
                scLinkYoutube.Item = currentItem;
                scImageYoutube.Item = currentItem;
                scLinkInstagram.Item = currentItem;
                scImageInstagram.Item = currentItem;
            }

        }
    }

}