﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LatestEvents.ascx.cs" Inherits="womans.org.Website.layouts.EmailCampaign.LatestEvents" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!-- ===== Latest Events ===== -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: arial, sans-serif; color: #666;">	
	<tr>
		<td style="padding: 25px 0; border-bottom: 1px solid #CCC;">
            <table id="tablePropEdit" runat="server" Visible="false" cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;">
				<tr>
					<td style="font-family: arial, sans-serif; display: block; background-color: #ec2b8c;  border-radius: 5px; padding: 8px; text-align: center;" class="viewmore">
					    <sc:EditFrame ID="editLinks" runat="server" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Newsletter Tagging Properties">
						    <h3 style="margin: 0; padding: 10px 0; color: #ffffff;">Edit Tagging Properties</h3>
                        </sc:EditFrame>
					</td>
				</tr>
			</table>            
            <table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;">
				<tr>
					<td style="padding: 0px; font-family: arial, sans-serif;">
						<h2 style="margin: 0; padding: 10px 0;"><sc:Text runat="server" ID="txtHeadline" Field="Title" /></h2>
					</td>
				</tr>
			</table>						
			<table cellpadding="0" cellspacing="0" border="0" style="font-family: arial, sans-serif;">
				<tr>
				    <asp:Repeater ID="rptRelatedEvents" runat="server" 
                        OnItemDataBound="rptRelatedEvents_ItemDataBound">
				        <ItemTemplate>
				            <td style="padding: 25px 10px 25px 0; width: 290px; border-bottom: 0 !important;" class="half">
								<div class="callout-event">
									<div class="event-header">
									    <asp:HyperLink ID="hlEventTitle" runat="server" style="text-decoration: none; margin-bottom: 15px; color: #EC2B8C; font-weight: bold;">
									        <span class="event-date" style="color:#464646; display:block; margin-botton: 10px; display: block;"><asp:Literal ID="litSessionDate" runat="server" /></span>
									        <asp:Literal ID="litEventTitle" runat="server" />
									    </asp:HyperLink>										
									</div>
									<p class="callout-event-preview" style="margin-bottom: 0;">
									    <asp:Literal ID="litTeaser" runat="server" />
									    <asp:HyperLink ID="hlReadMore" runat="server" style="color: #EC2B8C;" />
									</p>
								</div>
							</td>
				        </ItemTemplate>				                               
				    </asp:Repeater>
				</tr>
			</table>
		</td>
	</tr>				
</table>
<!-- ===== Latest Events ===== -->
