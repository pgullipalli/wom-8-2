﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Express;
using Womans.CustomItems.Messages.InnerContent.Newsletter;
using MedTouch.Common.UI;

namespace womans.org.Website.layouts.EmailCampaign
{
    public partial class MainSlide : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            MainSectionItem currItem = base.ContextItem;
            if (currItem != null)
            {
                imgSlide.Item = currItem;
                txtHeadline.Item = currItem;
                lnkSlide.Item = currItem;
                //litImage.Text = currItem.Image.Rendered;
                //litHeadline.Text = currItem.Headline.Rendered;
            }
        }
    }
}