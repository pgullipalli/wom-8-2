﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="womans.org.Website.layouts.EmailCampaign.Footer" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>

<!-- ===== Footer ===== -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: arial, sans-serif; color: #666;">				
	<tr>
		<td style="border-bottom: none; padding: 0">
			<table cellpadding="0" cellspacing="0" border="0" style="background: #dddcdc; text-align: center; width:100%;">
				<tr>
					<td style="padding: 30px 0; font-family: arial, sans-serif;" class="col-1">						
                        <sc:Text ID="scTextFooterLinks" runat="server" Field="Footer Links"/>						
						<p style="color: #666;"><sc:Text ID="scTextCopyrightText" runat="server" Field="Copyright Text"/></p>
					</td>
				</tr>
			</table>            
		</td>
	</tr>				
</table>
<!-- ===== Footer ===== -->