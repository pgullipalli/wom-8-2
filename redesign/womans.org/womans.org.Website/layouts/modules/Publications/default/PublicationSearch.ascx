﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
            Inherits="womans.org.Website.layouts.modules.Publications.PublicationSearch" CodeBehind="PublicationSearch.ascx.cs" %>
<!--==================== Module: Publications: Publication Search =============================-->
<asp:PlaceHolder runat="server" ID="phControlHeader">
    <h3><asp:Label ID="lblPublicationSearch" runat="server" /></h3>
</asp:PlaceHolder>
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSubmit" CssClass="module-pb-search core-search grid">
    <asp:Panel ID="pnlKeyword" runat="server" CssClass="six columns">
        <asp:Label ID="lblKeyword" runat="server" AssociatedControlID="txtKeyword" CssClass="label" />
        <asp:TextBox ID="txtKeyword" runat="server" CssClass="textbox" placeholder="Enter Keyword(s)" />
    </asp:Panel>
    <asp:PlaceHolder runat="server" Visible="False" ID="pnlHideUnused">
        <asp:Panel ID="pnlDate" runat="server" CssClass="six columns">
            <asp:Label ID="lblDate" runat="server" AssociatedControlID="txtDateFrom" CssClass="label" />
            <div class="grid">
                <asp:TextBox ID="txtDateFrom" runat="server" AutoCompleteType="None" type="date" placeholder="Date From" CssClass="dp_input six columns textbox" />
                <asp:TextBox ID="txtDateTo" runat="server" AutoCompleteType="None" type="date" placeholder="Date To" CssClass="dp_input six columns textbox" />
            </div>
        </asp:Panel>
    </asp:PlaceHolder>
    <div class="twelve columns  search-option-submit">
        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="button pink" />
        <asp:ImageButton ID="ibSubmit" runat="server" OnClick="ibSubmit_Click" Visible="false" />
        <asp:LinkButton ID="lbSubmit" runat="server" Visible="false" OnClick="lbSubmit_Click" />
    </div>
</asp:Panel>


<asp:Panel ID="pnlCategory" runat="server" CssClass="six columns" Visible="false">
    <asp:Label ID="lblCategory" runat="server" AssociatedControlID="ddlCategory" CssClass="label" />
    <asp:DropDownList ID="ddlCategory" runat="server" CssClass="styled" />
</asp:Panel>


<!--==================== /Module: Publications: Publication Search =============================-->