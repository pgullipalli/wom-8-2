﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using MedTouch.Publications.Helpers.UI;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace womans.org.Website.layouts.modules.Publications
{
    public partial class RelatedPublications : PublicationsSublayout
	{
        protected string FieldTaxonomyTerms = "Taxonomy Terms";

        private int _numberOfDisplayedItems = 1;
        private int _maxTeaserCharacters = 100;
        private string _dateFormat = "d";
        private bool _showLinkToDetail = true;
        private bool _resultTitleAsLink = true;
        private bool _showThumbnail = false;
        private string _readMoreText = "Read More";
        private string _sortBy = "publication date";

        protected override void Bind()
        {
            GetData();
            Search();
        }

        private void GetData()
        {
            GetPropertyData();
            GetDictionaryData();
        }

        private void GetPropertyData()
        {
            _numberOfDisplayedItems = GetIntProperty("Number of Displayed Items");
            _maxTeaserCharacters = GetIntProperty("Max Teaser Characters");
            _dateFormat = GetProperty("Date Format");
            _showThumbnail = GetBooleanProperty("Show Thumbnail");
            _sortBy = GetProperty("Sort By");

            _resultTitleAsLink = GetBooleanProperty("Show Title as Link");
            _showLinkToDetail = GetBooleanProperty("Show Link to Detail Page");

        }

        private void GetDictionaryData()
        {
            _readMoreText = CultureHelper.GetDictionaryTranslation("Modules.Publications.RelatedPublications.ReadMoreLink");
        }

        private void Search()
        {
            List<Item> items = new List<Item>();
            bool allContent = false;

            int totalCount = 0;
            int startItem = 0;
            int endItem = 0;

            string taxonomyItems = ItemHelper.GetShortIds(ContextItem, FieldTaxonomyTerms);
            string specialtyItems = ItemHelper.GetFieldRawValue(ContextItem, "Specialties");
            string locationItems = ItemHelper.GetFieldRawValue(ContextItem, "Locations");
            string templateFilter = ModuleHelper.PublicationTemplateGuid;
            bool isTaxonomyRecursive = GetBooleanProperty("Recursive Taxonomy");
            bool suppress = GetBooleanProperty("Suppress When Nothing Matches");
            Item searchResultsItem = GetSingleReferenceProperty("View All Link");
            if (searchResultsItem == null)
            {
                searchResultsItem = ItemHelper.GetItemFromReferenceField(ItemHelper.GetStartItem(), "Publication Search Results Page");
            }

            //Create Search Param
            var searchParam = new RelatedContentHelper.RelatedContentHelperSearchParam
            {
                ContextItem = ContextItem,
                TaxonomyItems = taxonomyItems,
                IsRecursive = isTaxonomyRecursive,
                SpecialtyItems = specialtyItems,
                LocationItems = locationItems,
                TemplateFilter = templateFilter
            };

            //Create SortParam . Sort by Latest to Oldest
            RelatedContentHelper.SortParam sortOptions = new RelatedContentHelper.SortParam
            {
                FieldName = _sortBy,
                OrderByAscending = false
            };

            //Search
            RelatedContentHelper.RelatedContentSearch(searchParam, sortOptions, "1",
                                                      _numberOfDisplayedItems, 1, out startItem, out endItem,
                                                      out totalCount, out items);


            if (!items.Any() && !suppress)
            {
                //Create SearchParam
                var suppressSearchParam = new RelatedContentHelper.RelatedContentHelperSearchParam
                {
                    ContextItem = ContextItem,
                    TaxonomyItems = string.Empty,
                    IsRecursive = isTaxonomyRecursive,
                    SpecialtyItems = string.Empty,
                    LocationItems = string.Empty,
                    TemplateFilter = templateFilter
                };

                RelatedContentHelper.RelatedContentSearch(suppressSearchParam, sortOptions, "1",
                                                          _numberOfDisplayedItems, 1, out startItem, out endItem,
                                                          out totalCount, out items);

                allContent = true;
            }

            if (items.Any())
            {
                if (_numberOfDisplayedItems < totalCount)
                {
                    string resultUrl = string.Empty;
                    if (allContent)
                    {
                        resultUrl = BaseModuleHelper.GetSearchResultsUrl(string.Empty, string.Empty, string.Empty, false, searchResultsItem);
                    }
                    else
                    {
                        string strSpecialties = ItemHelper.GetItemNamesFromItemGuids(specialtyItems);
                        string strLocations = ItemHelper.GetItemNamesFromItemGuids(locationItems);

                        resultUrl = BaseModuleHelper.GetSearchResultsUrl(strSpecialties, strLocations, taxonomyItems, isTaxonomyRecursive, searchResultsItem);
                    }

                    hlViewAll.NavigateUrl = resultUrl;
                    hlViewAll.Text = CultureHelper.GetDictionaryTranslation("Modules.Publications.RelatedPublications.ViewAll");
                    pnlViewAll.Visible = true;
                }
            }
            else
            {
                phControlHeader.Visible = false;
            }

            //Bind Label
            litRelatedPublications.Text = CultureHelper.GetDictionaryTranslation("Modules.Publications.RelatedPublications.RelatedPublicationsLabel");

            //Bind Related Publication
            lvSearchResults.DataSource = items;
            lvSearchResults.DataBind();
        }

        protected void lvSearchResults_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            Item item = (Item)e.Item.DataItem;

            HyperLink hlItem = (HyperLink)e.Item.FindControl("hlItem");
            Literal litItem = (Literal)e.Item.FindControl("litItem");
            Literal litDate = (Literal)e.Item.FindControl("litDate");
            Literal litTeaser = (Literal)e.Item.FindControl("litTeaser");
            Panel pnlTeaser = e.Item.FindControl("pnlTeaser") as Panel;
            HyperLink hlMoreLink = (HyperLink)e.Item.FindControl("hlMoreLink");
            Panel pnlMoreLink = (Panel)e.Item.FindControl("pnlMoreLink");
            Panel pnlThumbnail = (Panel)e.Item.FindControl("pnlThumbnail");
            Panel pnlDate = (Panel)e.Item.FindControl("pnlDate");
            Sitecore.Web.UI.WebControls.Image scThumbnail = (Sitecore.Web.UI.WebControls.Image)e.Item.FindControl("scThumbnail");
            HyperLink hlImage = (HyperLink)e.Item.FindControl("hlImage");

            if (item != null)
            {
                string strItemLink = LinkManager.GetItemUrl(item);
                if (hlItem != null && _resultTitleAsLink)
                {
                    hlItem.Text = ItemHelper.GetFieldHtmlValue(item, "Headline");
                    hlItem.NavigateUrl = strItemLink;
                    if (litItem != null) litItem.Visible = false;
                }
                else if (litItem != null)
                {
                    litItem.Text = ItemHelper.GetFieldHtmlValue(item, "Headline");
                    if (hlItem != null) hlItem.Visible = false;
                }

                //if (litDate != null && pnlDate != null)
                //{
                //    DateField dateField = item.Fields["Publication Date"];
                //    if (dateField != null && !string.IsNullOrEmpty(dateField.Value))
                //    {
                //        litDate.Text = dateField.DateTime.ToString(_dateFormat);
                //        pnlDate.Visible = true;
                //    }
                //}

                if (pnlTeaser != null && litTeaser != null)
                {
                    litTeaser.Text = ItemHelper.GetItemTeaser(item, _maxTeaserCharacters);
                    pnlTeaser.Visible = !string.IsNullOrEmpty(litTeaser.Text.Trim());
                }

                if (hlMoreLink != null && pnlMoreLink != null && _showLinkToDetail)
                {
                    hlMoreLink.Text = _readMoreText;
                    hlMoreLink.NavigateUrl = strItemLink;
                    pnlMoreLink.Visible = true;
                }

                if (pnlThumbnail != null && scThumbnail != null && hlImage != null && _showThumbnail)
                {
                    if (!string.IsNullOrEmpty(ItemHelper.GetFieldRawValue(item, "Cover Thumbnail")))
                    {
                        scThumbnail.Item = item;
                        hlImage.NavigateUrl = strItemLink;
                        pnlThumbnail.Visible = true;
                    }
                }
            }
        }
	}
}