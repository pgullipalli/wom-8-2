﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.Publications.layouts.modules.Publications.PublicationdetailSublayout"
    CodeBehind="PublicationDetail.ascx.cs" %>
<%@ Import Namespace="MedTouch.Common.Helpers" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: Publications: Publication Detail =============================-->
<div class="module-pb-detail">
    <div class="return">
        <a href="javascript:history.back()"><%= CultureHelper.GetDictionaryTranslation("Modules.Publications.PublicationSearch.ReturnToSearchLabel")%></a>
    </div>
    <h1>
        <sc:Text ID="scHeadline" Field="Headline" runat="server" />
    </h1>
    <div class="grid">
        <asp:Panel ID="pnlThumbnail" CssClass="two columns" runat="server" Visible="false">
            <sc:Image ID="scThumbnail" Field="Cover Thumbnail" runat="server" />
        </asp:Panel>
        <asp:Panel class="ten columns" ID="pnlDate" runat="server" Visible="false">
            <div><sc:Date ID="scDate" runat="server" Field="Publication Date" /></div>
        
        <p><sc:Text ID="scContentCopy" Field="Content Copy" runat="server" /></p>
        <asp:HyperLink CssClass="selected" ID="hlFile" runat="server" />

        </asp:Panel>
    </div>
</div>
<!--==================== /Module: Publications: Publication Detail =============================-->
