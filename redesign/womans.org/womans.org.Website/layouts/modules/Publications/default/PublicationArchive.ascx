﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.Publications.layouts.modules.Publications.PublicationarchiveSublayout" CodeBehind="PublicationArchive.ascx.cs" %>
<!--==================== Module: Publications: Publications Archive =============================-->
<asp:Panel ID="pnlYears" runat="server" CssClass="module-pb-archive core-archive callout collapse-for-mobile">
    <div class="reg-callout grid">
        <asp:PlaceHolder runat="server" ID="phControlHeader">
            <h3 class="module-heading"><asp:Literal ID="litPublicationsArchive" runat="server" /></h3>
        </asp:PlaceHolder>
        <asp:BulletedList ID="blYears" runat="server" DisplayMode="HyperLink" CssClass="module-pb-filters-year core-list" />
    </div>
</asp:Panel>
<!--==================== Module: Publications: Publications Archive =============================-->
