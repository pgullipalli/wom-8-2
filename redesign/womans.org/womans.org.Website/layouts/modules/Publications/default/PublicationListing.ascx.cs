﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using MedTouch.Pagination._usercontrols.pagination;
using MedTouch.Pagination.Helpers.Utilities;
using MedTouch.Pagination.Helpers.Utilities.Base;
using MedTouch.Pagination.Helpers.Utilities.DTO;
using MedTouch.Publications.Helpers.UI;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace womans.org.Website.layouts.modules.Publications
{
	public partial class PublicationListing : PublicationsSublayout
	{
        private readonly string _strQuerystringLocations = BaseModuleHelper.LocationsQuerystring();
        private readonly string _strQuerystringSpecialties = BaseModuleHelper.SpecialtiesQuerystring();
        private readonly string _strQuerystringTaxonomy = BaseModuleHelper.TaxonomyQuerystring();
        private readonly string _strQuerystringTaxonomyRecursive = BaseModuleHelper.TaxonomyRecursivelySearchQuerystring();

        private int _numberOfDisplayedItems = 1;
        private int _numberOfDisplayedPages = 1;
        private int _maxTeaserCharacters = 100;
        private bool _showThumbnail = false;
        private string _dateFormat = "d";
        private string _sortBy = "publication date";
        private bool _showLinkToDetail = true;
        private bool _resultTitleAsLink = true;
        private string _readMoreText = "Read More";

        protected override void Bind()
        {
            GetPropertyData();
            BindUserControls();
            Search();
        }

        private void GetPropertyData()
        {
            _numberOfDisplayedItems = GetIntProperty("Number of Displayed Items");
            _numberOfDisplayedPages = GetIntProperty("Number of Displayed Pages");
            _showThumbnail = GetBooleanProperty("Show Thumbnail");
            _maxTeaserCharacters = GetIntProperty("Max Teaser Characters");
            _dateFormat = GetProperty("Date Format");
            _sortBy = GetProperty("Sort By");
        }

        private void BindUserControls()
        {
            ucPagingTop.NumberOfDisplayedItem = _numberOfDisplayedItems;
            ucPagingTop.NumberOfDisplayedPage = _numberOfDisplayedPages;
            ucPagingBottom.NumberOfDisplayedItem = _numberOfDisplayedItems;
            ucPagingBottom.NumberOfDisplayedPage = _numberOfDisplayedPages;
        }

        private void Search()
        {
            string page = Request.QueryString[ModuleHelper.PageQuerystring];
            string specialties = Request.QueryString[_strQuerystringSpecialties];
            string locations = Request.QueryString[_strQuerystringLocations];
            string taxonomy = Request.QueryString[_strQuerystringTaxonomy];
            string taxonomyRecursive = Request.QueryString[_strQuerystringTaxonomyRecursive];

            List<Item> items = new List<Item>();

            if (string.IsNullOrEmpty(page))
                page = "1";

            int totalCount = 0;
            int startItem = 0;
            int endItem = 0;

            bool isTaxonomyRecursive;
            if (!bool.TryParse(taxonomyRecursive, out isTaxonomyRecursive))
                isTaxonomyRecursive = true;

            if (ContextItem.TemplateName.Equals("Year", StringComparison.OrdinalIgnoreCase))
            {
                int intYear = DateTime.Now.Year;
                int.TryParse(ContextItem.Name, out intYear);

                string dateFrom = new DateTime(intYear, 1, 1).ToString("d");
                string dateTo = new DateTime(intYear, 12, 31).ToString("d");

                ModuleHelper.PublicationSearch(string.Empty, string.Empty, dateFrom, dateTo, string.Empty, string.Empty, string.Empty, true, page,
                                        _numberOfDisplayedItems,
                                        _numberOfDisplayedPages, out startItem, out endItem,
                                        out totalCount, out items, _sortBy);
            }
            else if (ContextItem.TemplateName.Equals("Month", StringComparison.OrdinalIgnoreCase))
            {
                int intYear = DateTime.Now.Year;
                int intMonth = DateTime.Now.Month;

                int.TryParse(ContextItem.Name, out intMonth);
                int.TryParse(ContextItem.Parent.Name, out intYear);

                DateTime dateFrom = new DateTime(intYear, intMonth, 1);
                DateTime dateTo = dateFrom.AddMonths(1).AddDays(-1);
                string strDateFrom = dateFrom.ToString("d");
                string strDateTo = dateTo.ToString("d");

                ModuleHelper.PublicationSearch(string.Empty, string.Empty, strDateFrom, strDateTo, string.Empty, string.Empty, string.Empty, true, page,
                                        _numberOfDisplayedItems,
                                        _numberOfDisplayedPages, out startItem, out endItem,
                                        out totalCount, out items, _sortBy);
            }
            else
            {
                string keyword = Request.QueryString[ModuleHelper.KeywordQuerystring];
                string category = Request.QueryString[ModuleHelper.CategoryQuerystring];
                string dateFrom = Request.QueryString[ModuleHelper.DateFromQuerystring];
                string dateTo = Request.QueryString[ModuleHelper.DateToQuerystring];

                Item categoryFolderItem = GetSingleReferenceProperty("Publication Category Folder");

                string categoryFilter = ModuleHelper.GetPublicationCategoryGuidFromName(category, categoryFolderItem);

                ModuleHelper.PublicationSearch(keyword, categoryFilter, dateFrom, dateTo, specialties, locations, taxonomy, isTaxonomyRecursive, page,
                                        _numberOfDisplayedItems,
                                        _numberOfDisplayedPages, out startItem, out endItem,
                                        out totalCount, out items, _sortBy);

                SetCurrentSearchMessage(keyword);
            }

            BindPaginationControls(totalCount);

            bool canLoadData = items.Any();
            GetPropertyDataIfCanLoad(canLoadData);

            BindListView(items);
        }

        private void BindPaginationControls(int totalCount)
        {
            if (totalCount > 0)
            {
                IPaginationHelper paginationHelper = GetPaginationHelper(totalCount);
                paginationHelper.BindPaginationControls();
            }
        }

        private IPaginationHelper GetPaginationHelper(int totalCount)
        {
            PaginationDataParameters paginationDataParameters = GetPaginationDataParameters(totalCount);
            return PaginationHelper.Create(paginationDataParameters);
        }

        private PaginationDataParameters GetPaginationDataParameters(int totalCount)
        {
            return new PaginationDataParameters
            {
                TotalCount = totalCount,
                NumberOfDisplayedItems = _numberOfDisplayedItems,
                NumberOfDisplayedPages = _numberOfDisplayedPages,
                PageQueryStringParameterName = ModuleHelper.PageQuerystring,
                PaginationControls = new PaginationList[] { ucPagingTop, ucPagingBottom }
            };
        }
        private void SetCurrentSearchMessage(string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
            {
                string currentSearchMessage =
                    CultureHelper.GetDictionaryTranslation("Modules.Publications.PublicationListing.CurrentSearchMessage");
                if (!string.IsNullOrEmpty(currentSearchMessage) && currentSearchMessage.Contains("{0}"))
                {
                    litCurrentSearch.Text = string.Format(currentSearchMessage, "<span class=\"module-search-keyword\">" + keyword + "</span>");
                    pnlCurrentSearchMessage.Visible = true;
                }
            }
        }

        private void GetPropertyDataIfCanLoad(bool canLoadData)
        {
            if (canLoadData)
            {
                _resultTitleAsLink = GetBooleanProperty("Show Title as Link");
                _showLinkToDetail = GetBooleanProperty("Show Link to Detail Page");
                _readMoreText = CultureHelper.GetDictionaryTranslation("Modules.Publications.PublicationListing.ReadMoreLink");
            }
        }

        private void BindListView(IEnumerable<Item> results)
        {
            lvSearchResults.DataSource = results;
            lvSearchResults.DataBind();
        }

        protected void lvSearchResults_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            Item item = (Item)e.Item.DataItem;

            HyperLink hlItem = (HyperLink)e.Item.FindControl("hlItem");
            Literal litItem = (Literal)e.Item.FindControl("litItem");
            //Literal litDate = (Literal)e.Item.FindControl("litDate");
            Literal litTeaser = (Literal)e.Item.FindControl("litTeaser");
            Panel pnlTeaser = e.Item.FindControl("pnlTeaser") as Panel;
            HyperLink hlMoreLink = (HyperLink)e.Item.FindControl("hlMoreLink");
            Panel pnlReadMore = (Panel)e.Item.FindControl("pnlReadMore");
            Panel pnlThumbnail = (Panel)e.Item.FindControl("pnlThumbnail");
            //Panel pnlDate = (Panel)e.Item.FindControl("pnlDate");
            Sitecore.Web.UI.WebControls.Image scThumbnail = (Sitecore.Web.UI.WebControls.Image)e.Item.FindControl("scThumbnail");
            HyperLink hlImage = (HyperLink)e.Item.FindControl("hlImage");

            if (item != null)
            {
                string strItemLink = LinkManager.GetItemUrl(item);

                if (hlItem != null && _resultTitleAsLink)
                {
                    hlItem.Text = ItemHelper.GetFieldHtmlValue(item, "Headline");
                    hlItem.NavigateUrl = strItemLink;
                    if (litItem != null) litItem.Visible = false;
                }
                else if (litItem != null)
                {
                    litItem.Text = ItemHelper.GetFieldHtmlValue(item, "Headline");
                    if (hlItem != null) hlItem.Visible = false;
                }

                //if (litDate != null && pnlDate != null)
                //{
                //    DateField dateField = item.Fields["Publication Date"];
                //    if (dateField != null && !string.IsNullOrEmpty(dateField.Value))
                //    {
                //        litDate.Text = dateField.DateTime.ToString(_dateFormat);
                //        pnlDate.Visible = true;
                //    }
                //}

                if (pnlTeaser != null && litTeaser != null)
                {
                    litTeaser.Text = ItemHelper.GetItemTeaser(item, _maxTeaserCharacters);
                    pnlTeaser.Visible = !string.IsNullOrEmpty(litTeaser.Text.Trim());
                }

                if (hlMoreLink != null && pnlReadMore != null && _showLinkToDetail)
                {
                    hlMoreLink.Text = _readMoreText;
                    hlMoreLink.NavigateUrl = strItemLink;
                    pnlReadMore.Visible = true;
                }

                if (pnlThumbnail != null && scThumbnail != null && hlImage != null && _showThumbnail)
                {
                    if (!string.IsNullOrEmpty(ItemHelper.GetFieldRawValue(item, "Cover Thumbnail")))
                    {
                        scThumbnail.Item = item;
                        hlImage.NavigateUrl = strItemLink;
                        pnlThumbnail.Visible = true;
                    }
                }
            }
        }
	}
}