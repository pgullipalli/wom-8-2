﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
            Inherits="womans.org.Website.layouts.modules.Publications.PublicationSearch" CodeBehind="PublicationSearch.ascx.cs" %>
<%@ Import Namespace="MedTouch.Common.Helpers" %>

<!--==================== Module: Publications: Publication Quick Search =============================-->
<%--<script type="text/javascript">
    function ResetAllFields() {
        $("#<%= txtKeyword.ClientID %>").val('');
        $("#<%= txtDateFrom.ClientID %>").val('');
        $("#<%= txtDateTo.ClientID %>").val('');
        $("#<%= ddlCategory.ClientID %>").val('');
        $(".out").empty();
    }
    $(document).ready(function () {
        
    });
</script>--%>
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSubmit" CssClass="module-pb-search core-search callout collapse-for-mobile">
    <div class="reg-callout">
        <asp:PlaceHolder runat="server" ID="phControlHeader">
            <h3>
                <%= CultureHelper.GetDictionaryTranslation("Modules.Publications.PublicationSearch.PublicationQuickSearchLabel") %>
                <asp:PlaceHolder runat="server" Visible="False" ID="plHideLable">
                    <asp:Label ID="lblPublicationSearch" runat="server" />    
                </asp:PlaceHolder>
                
            </h3>
        </asp:PlaceHolder>
        <asp:Panel ID="pnlKeyword" runat="server" CssClass="twelve columns">
            <asp:Label ID="lblKeyword" runat="server" AssociatedControlID="txtKeyword" CssClass="label" />
            <asp:TextBox ID="txtKeyword" runat="server" CssClass="textbox" />
        </asp:Panel>

        <asp:Panel ID="pnlCategory" runat="server" CssClass="twelve columns">
            <asp:Label ID="lblCategory" runat="server" AssociatedControlID="ddlCategory" CssClass="label" />
            <div class="selectbox">
                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="selectboxdiv" />
                <div class="out"></div>
            </div>
        </asp:Panel>
        <asp:PlaceHolder runat="server" Visible="False" ID="pnlHideUnused">
            <asp:Panel ID="pnlDate" runat="server" CssClass="twelve columns">
                <asp:Label ID="lblDate" runat="server" AssociatedControlID="txtDateFrom" CssClass="label" />
                <asp:TextBox ID="txtDateFrom" type="date" placeholder="Date From" runat="server" AutoCompleteType="None" CssClass="dp_input twelve columns" />
                <asp:TextBox ID="txtDateTo" type="date" placeholder="Date To" runat="server" AutoCompleteType="None" CssClass="dp_input twelve columns" />
            </asp:Panel>
        </asp:PlaceHolder>
        <div class="twelve columns">
            <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="button pink" />
            <asp:ImageButton ID="ibSubmit" runat="server" OnClick="ibSubmit_Click" Visible="false" />
            <asp:LinkButton ID="lbSubmit" runat="server" Visible="false" OnClick="lbSubmit_Click" />
            <%--<input type="button" id="btnReset" value="Reset" onclick="ResetAllFields();" style="display: inline !important;" class="button"/>--%>
            <input type="reset" name="main_1$leftpanel_0$btnSubmit" value="Reset" class="button reset">
        </div>
    </div>
</asp:Panel>
<!--==================== /Module: Publications: Publication Quick Search =============================-->