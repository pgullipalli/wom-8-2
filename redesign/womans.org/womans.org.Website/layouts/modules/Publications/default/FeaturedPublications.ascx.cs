﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using MedTouch.Publications.Helpers.UI;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace womans.org.Website.layouts.modules.Publications
{
    public partial class FeaturedPublications : PublicationsSublayout
	{
        private int _maxTeaserCharacters = 100;
        private string _dateFormat = "d";
        private bool _showThumbnail = false;
        private bool _showLinkToDetail = true;
        private bool _resultTitleAsLink = true;
        private string _readMoreText = "Read More";

        protected override void Bind()
        {
            GetPropertyData();
            BindLiterals();
            BindViewAllControls();
            List();
        }

        private void GetPropertyData()
        {
            _maxTeaserCharacters = GetIntProperty("Max Teaser Characters");
            _showThumbnail = GetBooleanProperty("Show Thumbnail");
            _dateFormat = GetProperty("Date Format");
        }

        private void BindLiterals()
        {
            litFeaturedPublications.Text = CultureHelper.GetDictionaryTranslation("Modules.Publications.FeaturedPublications.FeaturedPublicationsLabel");
        }

        private void BindViewAllControls()
        {
            Item viewAllItem = GetSingleReferenceProperty("View All Link");

            if (viewAllItem == null)
            {
                viewAllItem = ItemHelper.GetItemFromReferenceField(ItemHelper.GetStartItem(), "Publication Search Results Page");
            }

            if (viewAllItem != null)
            {
                hlViewAll.NavigateUrl = LinkManager.GetItemUrl(viewAllItem);
                hlViewAll.Text = CultureHelper.GetDictionaryTranslation("Modules.Publications.FeaturedPublications.ViewAll");
                pnlViewAll.Visible = true;
            }
        }

        private void List()
        {
            List<Item> items = GetReferenceProperty("Selected Publications");

            if (items.Any())
            {
                _resultTitleAsLink = GetBooleanProperty("Show Title as Link");
                _showLinkToDetail = GetBooleanProperty("Show Link to Detail Page");
                _readMoreText = CultureHelper.GetDictionaryTranslation("Modules.Publications.FeaturedPublications.ReadMoreLink");
            }
            else
            {
                phControlHeader.Visible = false;
                pnlViewAll.Visible = false;
            }

            lvSearchResults.DataSource = items;
            lvSearchResults.DataBind();
        }

        protected void lvSearchResults_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            Item item = (Item)e.Item.DataItem;

            HyperLink hlItem = (HyperLink)e.Item.FindControl("hlItem");
            Literal litItem = (Literal)e.Item.FindControl("litItem");
            //Literal litDate = (Literal)e.Item.FindControl("litDate");
            Literal litTeaser = (Literal)e.Item.FindControl("litTeaser");
            Panel pnlTeaser = e.Item.FindControl("pnlTeaser") as Panel;
            HyperLink hlMoreLink = (HyperLink)e.Item.FindControl("hlMoreLink");
            Panel pnlThumbnail = (Panel)e.Item.FindControl("pnlThumbnail");
            Panel pnlReadMore = (Panel)e.Item.FindControl("pnlReadMore");
            //Panel pnlDate = (Panel)e.Item.FindControl("pnlDate");
            Sitecore.Web.UI.WebControls.Image scThumbnail = (Sitecore.Web.UI.WebControls.Image)e.Item.FindControl("scThumbnail");
            HyperLink hlImage = (HyperLink)e.Item.FindControl("hlImage");

            if (item != null)
            {
                string strItemLink = LinkManager.GetItemUrl(item);

                if (hlItem != null && _resultTitleAsLink)
                {
                    hlItem.Text = ItemHelper.GetFieldHtmlValue(item, "Headline");
                    hlItem.NavigateUrl = strItemLink;
                    if (litItem != null) litItem.Visible = false;
                }
                else if (litItem != null)
                {
                    litItem.Text = ItemHelper.GetFieldHtmlValue(item, "Headline");
                    if (hlItem != null) hlItem.Visible = false;
                }

                //if (litDate != null && pnlDate != null)
                //{
                //    DateField dateField = item.Fields["Publication Date"];
                //    if (dateField != null && !string.IsNullOrEmpty(dateField.Value))
                //    {
                //        litDate.Text = dateField.DateTime.ToString(_dateFormat);
                //        pnlDate.Visible = true;
                //    }
                //}

                if (pnlTeaser != null && litTeaser != null)
                {
                    litTeaser.Text = ItemHelper.GetItemTeaser(item, _maxTeaserCharacters);
                    pnlTeaser.Visible = !string.IsNullOrEmpty(litTeaser.Text.Trim());
                }

                if (hlMoreLink != null && pnlReadMore != null && _showLinkToDetail)
                {
                    hlMoreLink.Text = _readMoreText;
                    hlMoreLink.NavigateUrl = strItemLink;
                    pnlReadMore.Visible = true;
                }

                if (pnlThumbnail != null && scThumbnail != null && hlImage != null && _showThumbnail)
                {
                    if (!string.IsNullOrEmpty(ItemHelper.GetFieldRawValue(item, "Cover Thumbnail")))
                    {
                        scThumbnail.Item = item;
                        hlImage.NavigateUrl = strItemLink;
                        pnlThumbnail.Visible = true;
                    }
                }
            }
        }
	}
}