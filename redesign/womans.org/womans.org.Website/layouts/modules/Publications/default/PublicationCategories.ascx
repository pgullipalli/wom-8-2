﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.Publications.layouts.modules.Publications.PublicationcategoriesSublayout" CodeBehind="PublicationCategories.ascx.cs" %>
<!--==================== Module: Publications: Publication Categories =============================-->
<asp:Panel ID="pnlCategories" runat="server" CssClass="module-pb-categories callout collapse-for-mobile">
    <div class="reg-callout grid">
        <asp:PlaceHolder runat="server" ID="phControlHeader">
            <h3 class="module-heading"><asp:Literal ID="litPublicationCategories" runat="server" /></h3>
        </asp:PlaceHolder>
        <asp:BulletedList ID="blCategory" runat="server" DisplayMode="HyperLink" CssClass="module-pb-filters-list core-list" />
    </div>
</asp:Panel>
<!--==================== /Module: Publications: Publication Categories =============================-->
