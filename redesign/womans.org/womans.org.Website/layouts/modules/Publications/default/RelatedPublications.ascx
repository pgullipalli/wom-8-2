﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="womans.org.Website.layouts.modules.Publications.RelatedPublications"
    CodeBehind="RelatedPublications.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: Publications: Related Publications =============================-->
<asp:Panel runat="server" ID="pnlRelatedPublications" CssClass="module-pb-related core-related callout collapse-for-mobile">
    <div class="reg-callout grid">
        <asp:PlaceHolder ID="phControlHeader" runat="server">
            <h3 class="module-heading"><asp:Label ID="litRelatedPublications" runat="server" /></h3>
        </asp:PlaceHolder>
        <asp:Panel ID="pnlListing" runat="server" CssClass="listing">
            <ul class="core-list">
            <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_ItemDataBound" >
                <ItemTemplate>
                    <li class="core-li grid">

                        <asp:Panel ID="pnlThumbnail" runat="server" Visible="false" CssClass="list-item-image">
                                <asp:HyperLink ID="hlImage" runat="server" Target="_blank"><sc:Image ID="scThumbnail" Field="Cover Thumbnail" runat="server" MaxWidth="100" /></asp:HyperLink>
                        </asp:Panel>

                        <asp:Panel ID="pnlResult" runat="server" CssClass="list-item-copy">
                            <h5><asp:HyperLink ID="hlItem" runat="server" Target="_blank" /><asp:Literal ID="litItem" runat="server" /></h5>
                            <%--<asp:Panel ID="pnlDate" runat="server" CssClass="date" Visible="false">
                                <asp:Literal ID="litDate" runat="server" />
                            </asp:Panel>--%>
                            <asp:Panel ID="pnlTeaser" runat="server" Visible="false" CssClass="list-item-teaser">
                                <asp:Literal ID="litTeaser" runat="server" />
                            </asp:Panel>
                            <asp:Panel ID="pnlReadMore" runat="server" Visible="false" CssClass="list-item-links">
                                <asp:HyperLink ID="hlMoreLink" CssClass="read-more" runat="server" Target="_blank" />
                            </asp:Panel>
                        </asp:Panel>
                    </li>
                </ItemTemplate>
            </asp:ListView>
            </ul>
        </asp:Panel>
        <asp:Panel ID="pnlViewAll" runat="server" Visible="false" CssClass="module-pb-view-all">
            <asp:HyperLink ID="hlViewAll" runat="server" />
        </asp:Panel>
    </div>
</asp:Panel>
<!--==================== /Module: Publications: Related Publications =============================-->
