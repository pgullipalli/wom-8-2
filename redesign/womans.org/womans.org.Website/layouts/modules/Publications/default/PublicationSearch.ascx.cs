﻿using System;
using System.Collections.Generic;

using Sitecore.Data.Items;

using MedTouch.Common.Helpers;
using MedTouch.Publications.Helpers.UI;

namespace womans.org.Website.layouts.modules.Publications
{
    /// <summary>
    /// Summary description for PublicationsearchSublayout
    /// </summary>
    public partial class PublicationSearch : PublicationsSublayout
    {
        protected override void Bind()
        {
            BindControl();
        }

        private void BindControl()
        {
            string keyword = Request.QueryString[ModuleHelper.KeywordQuerystring];
            string dateFrom = Request.QueryString[ModuleHelper.DateFromQuerystring];
            string dateTo = Request.QueryString[ModuleHelper.DateToQuerystring];

            string strKeywordLabel = CultureHelper.GetDictionaryTranslation("Modules.Publications.PublicationSearch.SearchByKeywordLabel");
            string strCategoryLabel = CultureHelper.GetDictionaryTranslation("Modules.Publications.PublicationSearch.SearchByCategoryLabel");
            string strDateLabel = CultureHelper.GetDictionaryTranslation("Modules.Publications.PublicationSearch.SearchByDateLabel");

            lblKeyword.Text = strKeywordLabel;
            lblCategory.Text = strCategoryLabel;
            lblDate.Text = strDateLabel;

            lblKeyword.Visible = !string.IsNullOrEmpty(strKeywordLabel);
            lblCategory.Visible = !string.IsNullOrEmpty(strCategoryLabel);

            UIHelper.SetTextboxWatermarkPlaceholder(txtKeyword, keyword,
                                         CultureHelper.GetDictionaryTranslation(
                                             "Modules.Publications.PublicationSearch.SearchBoxWatermark"));

            bool allowSearchByDate = GetBooleanProperty("Allow Search by Date");
            if (allowSearchByDate)
            {
                UIHelper.SetTextboxWatermarkPlaceholder(txtDateFrom, dateFrom,
                                             CultureHelper.GetDictionaryTranslation(
                                                 "Modules.Publications.PublicationSearch.DateFromWatermark"));
                UIHelper.SetTextboxWatermarkPlaceholder(txtDateTo, dateTo,
                                             CultureHelper.GetDictionaryTranslation(
                                                 "Modules.Publications.PublicationSearch.DateToWatermark"));

                lblDate.Visible = !string.IsNullOrEmpty(strDateLabel);
            }
            else
            {
                pnlDate.Visible = false;
            }

            btnSubmit.Text =
                CultureHelper.GetDictionaryTranslation("Modules.Publications.PublicationSearch.SearchButtonText");
            lblPublicationSearch.Text =
                CultureHelper.GetDictionaryTranslation("Modules.Publications.PublicationSearch.PublicationSearchLabel");

            BindCategoryDropdown();
        }

        private void BindCategoryDropdown()
        {
            string category = Request.QueryString[ModuleHelper.CategoryQuerystring];
            Item categoryFolderItem = GetSingleReferenceProperty("Publication Category Folder");
            List<Item> availableCategories = ModuleHelper.GetAvailablePublicationCategories(categoryFolderItem);
            ModuleHelper.BindPublicationCategoryDropdown(ddlCategory, category, availableCategories, pnlCategory);
        }

        protected void ibSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            SearchSubmitted();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SearchSubmitted();
        }

        protected void lbSubmit_Click(object sender, EventArgs e)
        {
            SearchSubmitted();
        }

        protected virtual void SearchSubmitted()
        {
            string searchBoxWatermark = CultureHelper.GetDictionaryTranslation("Modules.Publications.PublicationSearch.SearchBoxWatermark");
            string dateFromWatermark = CultureHelper.GetDictionaryTranslation("Modules.Publications.PublicationSearch.DateFromWatermark");
            string dateToWatermark = CultureHelper.GetDictionaryTranslation("Modules.Publications.PublicationSearch.DateToWatermark");

            string keyword = txtKeyword.Text.Trim();
            if (keyword.Equals(searchBoxWatermark, StringComparison.OrdinalIgnoreCase))
                keyword = string.Empty;

            string dateFrom =
                txtDateFrom.Text.Trim();
            if (dateFrom.Equals(dateFromWatermark, StringComparison.OrdinalIgnoreCase))
                dateFrom = string.Empty;

            string dateTo = txtDateTo.Text.Trim();
            if (dateTo.Equals(dateToWatermark, StringComparison.OrdinalIgnoreCase))
                dateTo = string.Empty;

            Item searchResultsPage = GetSingleReferenceProperty("Search Results Page");
            if (searchResultsPage == null)
            {
                searchResultsPage = ItemHelper.GetItemFromReferenceField(ItemHelper.GetStartItem(), "Publication Search Results Page");
            }
            ModuleHelper.SubmitSearch(keyword, ddlCategory.SelectedValue, string.Empty, dateFrom,
                                         dateTo, searchResultsPage);
        }
    }
}