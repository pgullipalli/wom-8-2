﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="womans.org.Website.layouts.modules.Publications.LatestPublications"
    CodeBehind="LatestPublications.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: Publications: Latest Publications =============================-->
<asp:Panel runat="server" ID="pnlLatestPublications" CssClass="module-pb-latest reg-callout">
    <asp:PlaceHolder ID="phControlHeader" runat="server">
        <h3 class="module-header">
            <asp:Label ID="litLatestPublications" runat="server" /></h3>
    </asp:PlaceHolder>
    <asp:Panel ID="pnlListing" runat="server" CssClass="listing">
        <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_ItemDataBound">
            <ItemTemplate>
                <asp:Panel ID="pnlResult" runat="server" CssClass="listing-item grid">
                    
                    <div class="four columns">
                        <asp:Panel ID="pnlThumbnail" runat="server" Visible="false" CssClass="module-thumbnail">
                            <asp:HyperLink ID="hlImage" runat="server" Target="_blank"><sc:Image ID="scThumbnail" Field="Cover Thumbnail" runat="server" MaxWidth="100" /></asp:HyperLink>
                        </asp:Panel>
                    </div>

                    <div class="eight columns">
                        <h5><asp:HyperLink ID="hlItem" runat="server" Target="_blank" /><asp:Literal ID="litItem" runat="server" /></h5>                    
                        <%--<asp:Panel ID="pnlDate" runat="server" CssClass="module-pb-date" Visible="false">
                            <asp:Literal ID="litDate" runat="server" /></asp:Panel>--%>                    
                        <asp:Panel ID="pnlTeaser" runat="server" Visible="false" CssClass="listing-item-teaser">
                            <asp:Literal ID="litTeaser" runat="server" />
                        </asp:Panel>                    
                        <asp:Panel ID="pnlReadMore" runat="server" Visible="false" CssClass="listing-item-more-link">
                            <asp:HyperLink ID="hlMoreLink" runat="server" Target="_blank" />
                        </asp:Panel>
                    </div>
                </asp:Panel>
            </ItemTemplate>
        </asp:ListView>
    </asp:Panel>
    <asp:Panel ID="pnlViewAll" runat="server" Visible="false" CssClass="module-pb-view-all">
        <asp:HyperLink ID="hlViewAll" runat="server" />
    </asp:Panel>
</asp:Panel>
<!--==================== /Module: Publications: Latest Publications =============================-->