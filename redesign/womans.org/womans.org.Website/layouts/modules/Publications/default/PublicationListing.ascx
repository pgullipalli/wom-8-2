﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="womans.org.Website.layouts.modules.Publications.PublicationListing" CodeBehind="PublicationListing.ascx.cs" %>
<%@ Register TagPrefix="MedTouch" TagName="PaginationList" Src="/layouts/modules/Publications/default/PublicationPagination.ascx" %>
<%@ Import Namespace="MedTouch.Common.Helpers" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: Publications: Publication Listing =============================-->
<asp:Panel ID="pnlResults" CssClass="module-pb-results" runat="server">
    <asp:Panel ID="pnlCurrentSearchMessage" runat="server" Visible="false">
        <h3>
            <asp:Literal ID="litCurrentSearch" runat="server" /></h3>
    </asp:Panel>
    <MedTouch:PaginationList ID="ucPagingTop" runat="server" DisplayInfo="true" />
    <asp:Panel ID="pnlListing" runat="server" CssClass="listing">
        <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_ItemDataBound">
            <ItemTemplate>
                <asp:Panel ID="pnlResult" runat="server" CssClass="listing-item results">
                    <asp:Panel ID="pnlThumbnail" runat="server" Visible="false" CssClass="two columns">
                        <asp:HyperLink ID="hlImage" runat="server" Target="_blank"><sc:Image ID="scThumbnail" Field="Cover Thumbnail" runat="server" MaxWidth="100" /></asp:HyperLink>
                    </asp:Panel>
                    <div class="ten columns">
                        <h4><asp:HyperLink ID="hlItem" runat="server" Target="_blank" /><asp:Literal ID="litItem" runat="server" /></h4>
                        <%--<asp:Panel ID="pnlDate" runat="server" CssClass="module-date">
                            <asp:Literal ID="litDate" runat="server" />
                        </asp:Panel> --%>                   
                        <asp:Panel ID="pnlTeaser" runat="server" Visible="false" CssClass="listing-item-teaser">
                            <asp:Literal ID="litTeaser" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="pnlMoreLink" runat="server" Visible="false" CssClass="listing-item-more-link">
                            <asp:HyperLink ID="hlMoreLink" runat="server" Target="_blank" />
                        </asp:Panel>
                    </div>
                </asp:Panel>
            </ItemTemplate>
            <EmptyDataTemplate>
                <%=CultureHelper.GetDictionaryTranslation("Modules.Publications.PublicationListing.NoResultsMessage") %></EmptyDataTemplate>
        </asp:ListView>
    </asp:Panel>
    <MedTouch:PaginationList ID="ucPagingBottom" runat="server" DisplayInfo="true" />
</asp:Panel>
<!--==================== /Module: Publications: Publication Listing =============================-->
