﻿namespace womans.org.Website.layouts.modules.Calendar
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using MedTouch.Calendar.Helpers;
    using MedTouch.Calendar.Helpers.ObjectModel;
    using MedTouch.Calendar.Helpers.ShoppingCart;
    using MedTouch.Calendar.Search;
    using MedTouch.Common.Helpers;
    using MedTouch.Common.Helpers.Logging;
    using MedTouch.Common.Helpers.Logging.Base;
    using MedTouch.Common.Helpers.Storage;
    using MedTouch.Common.UI;

    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;
    using Sitecore.Diagnostics;
    using Sitecore.Links;
    using Sitecore.Sharedsource.Data.Comparers.ItemComparers;
    using Sitecore.Web;
    using Sitecore.Web.UI.WebControls;

    using global::womans.org.BusinessLogic.Helpers;

    using MedTouch.Calendar.UI.EventHandler.SaveAction;

    using Image = Sitecore.Web.UI.WebControls.Image;

    #endregion

    public partial class EventTopicOfferDates : BaseSublayout
    {
        #region Static Fields

        private static readonly ILogger logger = Logger.Create();

        #endregion

        #region Fields

        protected readonly string DictionaryAddToCalendarLink =
            CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventTopicOfferDates.AddToCalendarLink");

        protected readonly string DictionaryAddToCartError =
            CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventTopicOfferDates.AddToCartError");

        protected readonly string DictionaryAddToCartLink =
            CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventTopicOfferDates.AddToCartLink");

        protected readonly string DictionaryAddToCartSuccess =
            CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventTopicOfferDates.AddToCartSuccess");

        protected readonly string DictionaryAvailabilityLabel =
            CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventTopicOfferDates.AvailabilityLabel");

        protected readonly string DictionaryClassFullLabel =
            CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventTopicOfferDates.ClassFullLabel");

        protected readonly string DictionaryInstructorEmailLabel =
            CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventTopicOfferDates.LabelInstructorEmail");

        protected readonly string DictionaryLabelInstructors =
            CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventTopicOfferDates.LabelInstructors");

        protected readonly string DictionaryLabelMaps =
            CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventTopicOfferDates.LabelMaps");

        protected readonly string DictionaryLabelOfferDates =
            CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventTopicOfferDates.LabelOfferDates");

        protected readonly string DictionaryLabelRegisterLink =
            CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventTopicOfferDates.RegisterLink");

        protected readonly string DictionaryRemoveFromCartLink =
            CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventTopicOfferDates.RemoveFromCartLink");

        protected readonly string DictionaryWaitlistLink =
            CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventTopicOfferDates.WaitlistLink");

        protected readonly string RenderingParamDatesOfferedDateFormat = "DatesOfferedDateFormat";

        private string _datesOfferedDateFormat;

        private string _eventSessionKey;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Returns string version of start/end datetime span
        ///     All Calendar components that show session dates/times should use this method
        ///     for consistent date span handling.
        ///     Example-1: June 27, 2013 3:00PM – 5:00PM
        ///     Example-2: June 27 – June 28, 2013 3:00PM - 5:00PM
        ///     Example-3: June 27 - June 28, 2013 (if timeFormat not provided)
        ///     Example-4: Ongoing June 27 - 28 (if isOngoing AND startdate)
        ///     Example-5: Ongoing (if isOngoing, no start date)
        /// </summary>
        /// <param name="sessionItem"></param>
        /// <param name="bShowTime"></param>
        /// <param name="dateFormat"></param>
        /// <param name="timeFormat"></param>
        /// <returns></returns>
        public static string GetSessionDateTimeString(
            Item sessionItem, bool bShowTime = true, string dateFormat = "", string timeFormat = "")
        {
            var dateTimeString =
                CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventSessionListing.NonDateEventMessage");
            if (sessionItem != null)
            {
                try
                {
                    var startDate = ModuleHelper.GetSitecoreDateField(
                        sessionItem, EventSession.FieldName.SessionStartDate);
                    var endDate = ModuleHelper.GetSitecoreDateField(sessionItem, EventSession.FieldName.SessionEndDate);
                    var isOngoing = (ItemHelper.GetFieldRawValue(sessionItem, EventSession.FieldName.IsOngoing) == "1");

                    //Hide start / end date per Penni's request
                    //Penni West: Ongoing - has no start/end date
                    //https://medtouch.attask-ondemand.com/task/view?ID=545abe3b0017123fbe575eadcdaf2a26

                    if (isOngoing)
                    {
                        return String.Format(
                            "<span class=\"date\">{0}</span>",
                            CultureHelper.GetDictionaryTranslation(
                                "Modules.Calendar.EventSessionListing.OngoingEventMessage"));
                    }

                    if (startDate.HasValue || isOngoing)
                    {
                        var strOngoing = ""; // assume not an onging event
                        if (isOngoing)
                        {
                            strOngoing = String.Format(
                                "<span class=\"date\">{0}</span>",
                                CultureHelper.GetDictionaryTranslation(
                                    "Modules.Calendar.EventSessionListing.OngoingEventMessage"));
                        }

                        if (startDate.HasValue)
                        {
                            var sessionStartDate = startDate.Value;
                            var sessionEndDate = endDate.HasValue ? endDate.Value : startDate.Value;

                            if (string.IsNullOrEmpty(dateFormat))
                            {
                                dateFormat = "MM/dd/yyyy";
                            }
                            if (bShowTime && string.IsNullOrEmpty(timeFormat))
                            {
                                timeFormat = "h:mm tt";
                            }

                            // If 'Start Date' and 'End Date' are on the same Day use Example-1, 3, or 4 Format 
                            if (sessionStartDate.Year == sessionEndDate.Year
                                && sessionStartDate.Month == sessionEndDate.Month
                                && sessionStartDate.Day == sessionEndDate.Day)
                            {
                                //if (!bShowTime || isOngoing)
                                //    dateTimeString = String.Format("<span class=\"date\">{0}{1} {2}</span>",
                                //        strOngoing,
                                //        sessionStartDate.ToString(dateFormat), 
                                //        sessionStartDate.ToString(timeFormat));
                                //else
                                dateTimeString = String.Format(
                                    "<span class=\"date\">{0}{1} {2}</span>{2} - {3}",
                                    strOngoing,
                                    sessionStartDate.ToString(dateFormat),
                                    sessionStartDate.ToString(timeFormat),
                                    sessionEndDate.ToString(timeFormat));
                            }

                                // Else use Example-2, 3, or 4 Format
                            else
                            {
                                if (!bShowTime || isOngoing)
                                {
                                    dateTimeString = String.Format(
                                        "<span class=\"date\">{0}{1} {2}</span>{3} - {4}",
                                        strOngoing,
                                        sessionStartDate.ToString(dateFormat),
                                        sessionStartDate.ToString(timeFormat),
                                        sessionStartDate.ToString(timeFormat),
                                        sessionEndDate.ToString(timeFormat));
                                }
                                else
                                {
                                    dateTimeString =
                                        String.Format(
                                            "<span class=\"date\">{0}{1} {2}</span> {3} {2} - {4} {5}",
                                            strOngoing,
                                            sessionStartDate.ToString(dateFormat),
                                            sessionStartDate.ToString(timeFormat),
                                            sessionStartDate.ToString(dateFormat),
                                            sessionEndDate.ToString(dateFormat),
                                            sessionEndDate.ToString(timeFormat));
                                }
                            }
                            return dateTimeString;
                        }

                        // no start date & ongoing: Use Example 5 format
                        return strOngoing;
                    }
                }
                catch (Exception ex)
                {
                    logger.Info(
                        "Error getting start or end date field in event session: " + sessionItem.ID,
                        HttpContext.Current.Session);
                    return dateTimeString;
                }
            }

            // fallback - invalid session item OR not ongoing and no start date: Assume non-date event (usually empty string)
            return dateTimeString;
        }

        /// <summary>
        ///     Return List of payment methods as sitecore listitems (for a dropdown list)
        /// </summary>
        /// <param name="ddlPaymentMethods">Dropdownlist to populate</param>
        /// <param name="validPmtMethods">List of payment method Items supported for this event</param>
        /// <param name="curStatus">Current payment method to be selected in list</param>
        /// <returns></returns>
        public static void PopulatePaymentMethodsList(
            DropDownList ddlPaymentMethods, List<Item> validPmtMethods, string sessionID)
        {
            ddlPaymentMethods.Items.Clear();
            foreach (var pmtItem in validPmtMethods)
            {
                var pm = new PaymentMethod(pmtItem);
                if (pm.ShowInList)
                {
                    if (!string.IsNullOrEmpty(pm.Description))
                    {
                        var newItem = new ListItem
                                          {
                                              Text = pm.Description,
                                              Value =
                                                  string.Format(
                                                      "{0}?{1}={2}&{3}={4}",
                                                      ItemHelper.GetItemUrl(pm.RegistrationFormPage),
                                                      ModuleHelper.Config.QueryString
                                                                  .GetEventSessionIDQuerystring(),
                                                      sessionID,
                                                      ModuleHelper.Config.QueryString
                                                                  .GetPaymentMethodIDQuerystring(),
                                                      pmtItem.ID.ToShortID())
                                          };
                        ddlPaymentMethods.Items.Add(newItem);
                    }
                }
            }
            if (ddlPaymentMethods.Items.Count > 0)
            {
                var watermarkItem = new ListItem { Text = "Select Payment Method", Value = "", Selected = true };
                ddlPaymentMethods.Items.Insert(0, watermarkItem);
            }
        }

        #endregion
        protected Item RenderingParamShoppingCartPage
        {
            get
            {
                    return this.GetSingleReferenceProperty("ShoppingCartPage");
            }
        }
        #region Methods

        protected void BindControl()
        {
            if (this.ContextItem == null)
            {
                return;
            }

            var calendarSearcher = new CalendarSearcher();
            //List<Item> eventSessionItems = calendarSearcher.GetEventSessionItems(ContextItem.ID.ToString());
            //List<Item> eventSessionItems = ModuleHelper.GetFutureSessions(ContextItem);
            var eventSessionItems = CalendarHelper.GetFutureSessions(this.ContextItem);
            var comparer = new FieldValueComparer(EventSession.FieldName.SessionStartDate);
            eventSessionItems.Sort(comparer);

            if (!eventSessionItems.Any())
            {
                this.pnlOfferDates.Visible = false;
            }
            else
            {
                // Offer Dates Label
                if (this.phOfferDatesHeader != null && this.litOfferDatesHeader != null)
                {
                    this.phOfferDatesHeader.Visible = true;
                    this.litOfferDatesHeader.Text = this.DictionaryLabelOfferDates;
                }

                // Offer Dates Listing.
                this.lvOfferDates.DataSource = eventSessionItems;
                this.lvOfferDates.DataBind();
            }

            var dateFormat = this.GetProperty("DateFormat");
        }

        protected override void Initialize()
        {
            // Load Rendering Parameter
            this._datesOfferedDateFormat = this.GetProperty(this.RenderingParamDatesOfferedDateFormat) ?? "";
            this._eventSessionKey =
                this.Request.QueryString[ModuleHelper.Config.QueryString.GetEventSessionKeyQuerystring()];
        }

        protected void Page_Load(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif
            this.Initialize();
            if (!this.IsPostBack)
            {
                this.BindControl();
            }
#if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
#endif
        }

        ////#if !PREMIUM
        //        protected void lbRegister_OnItemCommand(object sender, ListViewCommandEventArgs e)
        //        {
        //            string sessionID = e.CommandName;
        //            ListViewDataItem item = (ListViewDataItem)e.Item;
        //            DropDownList ddlPaymentMethod = e.Item.FindControl("ddlPaymentMethod") as DropDownList;
        //            Panel pnlRegisterPaymentType = e.Item.FindControl("pnlRegisterPaymentType") as Panel;
        //            LinkButton lbRegister = item.FindControl("lbRegister") as LinkButton;
        //            if (item == null || ddlPaymentMethod == null || pnlRegisterPaymentType == null || lbRegister == null)
        //                return;
        //            pnlRegisterPaymentType.Visible = true;
        //        }
        ////#endif

        protected void ddlPaymentMethod_SelectedIndexChanged(Object sender, EventArgs e)
        {
            ////#if !PREMIUM
            ////            DropDownList ddlPaymentMethod = sender as DropDownList;
            ////            if (ddlPaymentMethod != null)
            ////                WebUtil.Redirect(ddlPaymentMethod.SelectedValue);
            ////#endif
        }

        //#if PREMIUM
        protected void lbRegister_OnItemCommand(object sender, ListViewCommandEventArgs e)
        {
            var sessionID = e.CommandName;
            var item = (ListViewDataItem)e.Item;
            if (item == null)
            {
                return;
            }
            var lbAddToCart = item.FindControl("lbAddRemove") as LinkButton;
            if (lbAddToCart == null)
            {
                return;
            }

            // Add vs. Remove button based on whether item already in cart
            var curCart = ModuleHelper.ShoppingCart;
            Assert.IsNotNull(curCart, "Add to Cart: Shopping cart null.");
            var cartSession = curCart.FindCartSession(sessionID);

            //Remove item from cart & make it addable again
            if (cartSession != null)
            {
                curCart.RemoveFromCart(sessionID);
                lbAddToCart.Text = this.IsWaitlistSession(sessionID)
                                       ? this.DictionaryWaitlistLink
                                       : this.DictionaryAddToCartLink;
                return;
            }

            // Add item to Cart and change button to Remove
            // first be sure to close any additional form panels that might be open - add to cart clicked, but no continue
            foreach (var lvitm in this.lvOfferDates.Items)
            {
                var pnlOpenForm = lvitm.FindControl("pnlAdditionalInfo") as Panel;
                if (pnlOpenForm != null)
                {
                    pnlOpenForm.Visible = false;
                }
            }
            // determine whether topic for this session:
            //  a) has additional form with custom fields, and
            //  b) if that form hasn't already been filled in.
            var sessionItem = ItemHelper.GetItemFromGuid(sessionID);
            Assert.IsNotNull(sessionItem, "Add to Cart: Invalid session item.");
            var topicItem = ModuleHelper.GetEventTopicFromEventSession(sessionItem);
            Assert.IsNotNull(topicItem, "Add to Cart: Invalid topic for selected session.");
            var cartTopic = curCart.FindCartTopic(topicItem.ID.ToString());
            Assert.IsNotNull(topicItem, "Add to Cart: Invalid topic for selected session.");
            var customFormItem = ItemHelper.GetItemFromReferenceField(
                topicItem, ItemMapper.EventTopic.FieldName.AdditionalForm);
            Item checkoutPageItem = RegistrationHelper.GetShoppingCartCheckoutPageItem(this.RenderingParamShoppingCartPage);
            if (!cartTopic.CustomFields.Any() && customFormItem != null)
            {
                // Show custom form in iFrame - use skeleton page (CustomFormContainerPage) containing layout (CustomFormLayout) with a single placeholder
                // for the form, which is placed dynamically (based on topic's custom form ID) by a custom version of WFFM FormRender: 
                // See: /sitecore/layout/Renderings/Modules/Web Forms for Marketers/Event Custom Form
                var siteStartItem = ItemHelper.GetStartItem();
                Assert.IsNotNull(siteStartItem, "Add to Cart: Invalid site start item.");
                var customFormContainerItem = ItemHelper.GetItemFromReferenceField(
                    siteStartItem, ItemMapper.CalendarModuleSiteSettings.FieldName.CustomFormContainerPage);
                
                Assert.IsNotNull(customFormContainerItem, "Add to Cart: Invalid custom form container page.");
                var customFormContainerURL = LinkManager.GetItemUrl(customFormContainerItem);
                int customFormHeight;
                if (
                    !int.TryParse(
                        ItemHelper.GetFieldRawValue(topicItem, ItemMapper.EventTopic.FieldName.AdditionalFormHeight),
                        out customFormHeight))
                {
                    customFormHeight = 300;
                }
                var customFormInfo = new CustomFormData.CustomFormInfo
                                         {
                                             CustomFormID = customFormItem.ID.ToString(),
                                             SessionID = sessionID,
                                             ReturnURL = WebUtil.GetRawUrl() + "?checkoutredirect=" + ItemHelper.GetItemUrl(checkoutPageItem),
                                             bWaitlistReg =
                                                 (lbAddToCart.Text
                                                  == this.DictionaryWaitlistLink),
                                             bAddToCartError = false,
                                             bRedirectMode = false,
                                             bInlineForm = false
                                         };
                var storageMedium = SessionStorageMedium.Create(HttpContext.Current.Session);
                storageMedium.Save(CustomFormData.CustomFormInfoName, customFormInfo);

                // Preferred custom form processing: Show the form inline on the current page (below the "add to cart" button) 
                var pnlAdditionalInfo = item.FindControl("pnlAdditionalInfo") as Panel;
                Assert.IsNotNull(pnlAdditionalInfo, "Add to Cart: Panel for iframe missing from page.");
                var ifrCustomForm = item.FindControl("ifrCustomForm") as HtmlGenericControl;
                if (pnlAdditionalInfo != null && ifrCustomForm != null)
                {
                    ifrCustomForm.Attributes.Add("src", customFormContainerURL);
                    ifrCustomForm.Attributes.Add("style", "height: " + customFormHeight + "px");
                    pnlAdditionalInfo.Visible = true;
                    return;
                }
                // Alternative custom form processing: redirect to full-page form if iframe panel not used/found
                customFormInfo.bInlineForm = true;
                storageMedium.Save(CustomFormData.CustomFormInfoName, customFormInfo);
                this.Response.Redirect(customFormContainerURL);
            }

            // No Custom form - simply add the item to the cart
            var bAdded = curCart.AddToCart(sessionID, (lbAddToCart.Text == this.DictionaryWaitlistLink));
            lbAddToCart.Text = this.DictionaryRemoveFromCartLink;
            var hdnError = item.FindControl("hdnAddToCartError") as HiddenField;
            if (hdnError != null)
            {
                hdnError.Value = !bAdded ? this.DictionaryAddToCartError : string.Empty;
            }
            var pnlErrorMessage = item.FindControl("pnlErrorMessage") as Panel;
            if (pnlErrorMessage != null)
            {
                pnlErrorMessage.Visible = !bAdded;
            }

            

            Response.Redirect(ItemHelper.GetItemUrl(checkoutPageItem));
        }

        protected void lvInstructors_OnItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem)
            {
                return;
            }

            var item = e.Item.DataItem as Item;
            if (item == null)
            {
                return;
            }
            var pnlInstructorName = e.Item.FindControl("pnlInstructorName") as Panel;
            var pnlInstructorInfo = e.Item.FindControl("pnlInstructorInfo") as Panel;
            if (pnlInstructorName == null || pnlInstructorInfo == null)
            {
                return;
            }

            // Instructor name
            var litInstructorName = e.Item.FindControl("litInstructorName") as Literal;
            var litInstructorNameOnly = e.Item.FindControl("litInstructorNameOnly") as Literal;
            var strInstructorName = ItemHelper.GetFieldRawValue(item, EventInstructor.FieldName.InstructorName);
            if (litInstructorName != null && !string.IsNullOrEmpty(strInstructorName))
            {
                litInstructorName.Text = strInstructorName;
                litInstructorNameOnly.Text = strInstructorName;
            }

            // Photo
            var pnlThumbnail = e.Item.FindControl("pnlThumbnail") as Panel;
            var scThumbnail = e.Item.FindControl("scThumbnail") as Image;
            if (pnlThumbnail != null && scThumbnail != null)
            {
                if (!string.IsNullOrEmpty(ItemHelper.GetFieldRawValue(item, EventInstructor.FieldName.Photo)))
                {
                    scThumbnail.Item = item;
                    pnlThumbnail.Visible = true;
                }
            }

            // Professional Title
            var pnlTitle = e.Item.FindControl("pnlTitle") as Panel;
            var litTitle = e.Item.FindControl("litTitle") as Literal;
            var strTitle = ItemHelper.GetFieldRawValue(item, EventInstructor.FieldName.ProfessionalTitle);
            if (pnlTitle != null && litTitle != null && !string.IsNullOrEmpty(strTitle))
            {
                pnlTitle.Visible = true;
                litTitle.Text = strTitle;
            }

            // Further description
            var pnlDescription = e.Item.FindControl("pnlDescription") as Panel;
            var litDescription = e.Item.FindControl("litDescription") as Literal;
            var strDescription = ItemHelper.GetFieldRawValue(item, EventInstructor.FieldName.Description);
            if (pnlDescription != null && litDescription != null && !string.IsNullOrEmpty(strDescription))
            {
                pnlDescription.Visible = true;
                litDescription.Text = strDescription;
            }

            // Contact Phone
            var pnlPhone = e.Item.FindControl("pnlPhone") as Panel;
            var litPhone = e.Item.FindControl("litPhone") as Literal;
            var strPhone = ItemHelper.GetFieldRawValue(item, EventInstructor.FieldName.ContactPhone);
            if (pnlPhone != null && litPhone != null && !string.IsNullOrEmpty(strPhone))
            {
                pnlPhone.Visible = true;
                litPhone.Text = strPhone;
            }

            // Contact Email
            var pnlEmail = e.Item.FindControl("pnlEmail") as Panel;
            var hlEmail = e.Item.FindControl("hlEmail") as HyperLink;
            var strEmail = ItemHelper.GetFieldRawValue(item, EventInstructor.FieldName.ContactEmail);
            if (!string.IsNullOrEmpty(strEmail) && hlEmail != null && pnlEmail != null)
            {
                pnlEmail.Visible = true;
                hlEmail.Text = this.DictionaryInstructorEmailLabel;
                hlEmail.NavigateUrl = "mailto:" + strEmail;
            }

            if (!pnlThumbnail.Visible && !pnlTitle.Visible && !pnlDescription.Visible && !pnlPhone.Visible
                && !pnlEmail.Visible)
            {
                pnlInstructorName.Visible = true;
            }
            else
            {
                pnlInstructorInfo.Visible = true;
            }
        }

        protected void lvOfferDatess_OnItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem)
            {
                return;
            }

            var item = e.Item.DataItem as Item;
            if (item == null)
            {
                return;
            }

            // Highlight item
            var pnlItem = e.Item.FindControl("pnlItem") as Panel;
            if (pnlItem != null && item.Key.Equals(this._eventSessionKey))
            {
                var baseclasses = pnlItem.Attributes["class"];
                pnlItem.Attributes["class"] = baseclasses + " highlight";
            }

            // LEFT CONTENT

            // Date
            var pnlDate = e.Item.FindControl("pnlDate") as Panel;
            var litSessionDateTime = e.Item.FindControl("litSessionDateTime") as Literal;
            litSessionDateTime.Text = GetSessionDateTimeString(item, true, this._datesOfferedDateFormat);
            pnlDate.Visible = (!string.IsNullOrEmpty(litSessionDateTime.Text));

            // AttendingDateInfo
            var pnlAttendingDateInfo = e.Item.FindControl("pnlAttendingDateInfo") as Panel;
            var litAttendingDateInfo = e.Item.FindControl("litAttendingDateInfo") as Literal;
            var strAttendingDateInfo = ItemHelper.GetFieldRawValue(
                item, EventSession.FieldName.AttendingDatesInformation);
            if (!string.IsNullOrEmpty(strAttendingDateInfo) && pnlAttendingDateInfo != null
                && litAttendingDateInfo != null)
            {
                pnlAttendingDateInfo.Visible = true;
                litAttendingDateInfo.Text = strAttendingDateInfo;
            }

            // Location Info
            var pnlLocation = e.Item.FindControl("pnlLocation") as Panel;
            var locationItem = ItemHelper.GetItemFromReferenceField(item, EventSession.FieldName.EventLocation);
            if (locationItem != null)
            {
                var strAddress1 = ItemHelper.GetFieldRawValue(locationItem, EventLocation.FieldName.Address1);
                var strAddress2 = ItemHelper.GetFieldRawValue(locationItem, EventLocation.FieldName.Address2);
                var strCity = ItemHelper.GetFieldRawValue(locationItem, EventLocation.FieldName.City);
                var strState = ItemHelper.GetFieldRawValueFromReferencedItem(
                    locationItem, EventLocation.FieldName.State, "Abbreviation");
                var strZip = ItemHelper.GetFieldRawValue(locationItem, EventLocation.FieldName.ZipCode);
                var strLocationOnelineAddress = StringHelper.GetAddressString(
                    strAddress1, strAddress2, strCity, strState, strZip, true);
                var strLocationName = ItemHelper.GetFieldRawValue(locationItem, EventLocation.FieldName.LocationName);
                var strLocationURL = string.Empty;
                LinkField locLink = locationItem.Fields[EventLocation.FieldName.LocationLink];
                if (locLink != null)
                {
                    ItemHelper.GetGeneralLinkUrl(locLink, out strLocationURL);
                }
                if ((!string.IsNullOrEmpty(strLocationOnelineAddress) || !string.IsNullOrEmpty(strLocationName))
                    && pnlLocation != null)
                {
                    pnlLocation.Visible = true;

                    // Location Name
                    var pnlLocationName = e.Item.FindControl("pnlLocationName") as Panel;
                    var litLocationName = e.Item.FindControl("litLocationName") as Literal;
                    var scLocationLink = e.Item.FindControl("scLocationLink") as Link;

                    if (!string.IsNullOrEmpty(strLocationName) && pnlLocationName != null)
                    {
                        pnlLocationName.Visible = true;
                        if (scLocationLink != null && !string.IsNullOrEmpty(strLocationURL))
                        {
                            scLocationLink.Item = locationItem;
                            scLocationLink.Text = strLocationName;
                            scLocationLink.Visible = true;
                        }
                        else if (litLocationName != null)
                        {
                            litLocationName.Text = strLocationName;
                            litLocationName.Visible = true;
                        }
                    }

                    var spnEventLocation = e.Item.FindControl("spnEventLocation") as HtmlGenericControl;
                    if (spnEventLocation != null)
                    {
                        spnEventLocation.InnerText = strLocationName + ", "
                                                     + StringHelper.GetAddressString(
                                                         strAddress1, strAddress2, strCity, strState, strZip, true);
                    }

                    // Location Address
                    var pnlLocationAddress = e.Item.FindControl("pnlLocationAddress") as Panel;
                    var litLocationAddress = e.Item.FindControl("litLocationAddress") as Literal;
                    if (!string.IsNullOrEmpty(strLocationOnelineAddress) && pnlLocationAddress != null
                        && litLocationAddress != null)
                    {
                        pnlLocationAddress.Visible = true;
                        // (Generate Multi Line address)
                        litLocationAddress.Text = StringHelper.GetAddressString(
                            strAddress1, strAddress2, strCity, strState, strZip, false);
                    }

                    // Location Map and Direction
                    var pnlMapsAndDirection = e.Item.FindControl("pnlMapsAndDirection") as Panel;
                    var hlMapsAndDirection = e.Item.FindControl("hlMapsAndDirection") as HyperLink;
                    if (pnlMapsAndDirection != null && hlMapsAndDirection != null)
                    {
                        hlMapsAndDirection.Text = this.DictionaryLabelMaps;
                        hlMapsAndDirection.NavigateUrl = MapHelper.GetMapLink(strLocationOnelineAddress);
                        pnlMapsAndDirection.Visible = true;
                        hlMapsAndDirection.Visible = true;
                    }
                }
            }

            // Add Event to Calendar: uses http://js.addthisevent.com
            var eventTopicItem = ModuleHelper.GetEventTopicFromEventSession(item);
            var isOngoingEvent = (ItemHelper.GetFieldRawValue(item, EventSession.FieldName.IsOngoing) == "1");
            var startDate = ModuleHelper.GetSitecoreDateField(item, EventSession.FieldName.SessionStartDate);
            if (!isOngoingEvent && startDate.HasValue && eventTopicItem != null)
            {
                var pnlAddToCalendar = e.Item.FindControl("pnlAddToCalendar") as Panel;
                pnlAddToCalendar.Visible = true;
                var anAddToCalendar = e.Item.FindControl("anAddToCalendar") as HtmlAnchor;
                anAddToCalendar.HRef = WebUtil.GetRawUrl();

                var spnEventStartDate = e.Item.FindControl("spnEventStartDate") as HtmlGenericControl;
                if (spnEventStartDate != null)
                {
                    spnEventStartDate.InnerText = (startDate.HasValue) ? startDate.Value.ToString() : "";
                }

                var endDate = ModuleHelper.GetSitecoreDateField(item, EventSession.FieldName.SessionEndDate);
                var spnEventEndDate = e.Item.FindControl("spnEventEndDate") as HtmlGenericControl;
                if (spnEventEndDate != null)
                {
                    spnEventEndDate.InnerText = (endDate.HasValue) ? endDate.ToString() : "";
                }

                var spnZoneCode = e.Item.FindControl("spnZoneCode") as HtmlGenericControl;
                if (spnZoneCode != null)
                {
                    var siteStartItem = ItemHelper.GetStartItem();
                    spnZoneCode.InnerText = ItemHelper.GetFieldRawValue(
                        siteStartItem, ItemMapper.CalendarModuleSiteSettings.FieldName.AddToCalendarZoneCode);
                    // "15" = Eastern US time zone; see http://addthisevent.com/ for list of zone codes
                }

                var spnAllDayEvent = e.Item.FindControl("spnAllDayEvent") as HtmlGenericControl;
                spnAllDayEvent.InnerText = "false";
                // this is always false since we only allow addtocalendar for events with a start date, change for ongoing?
                //spnAllDayEvent.InnerText = (startDate.HasValue || endDate.HasValue) ? "false" : "true";

                var ltAddToCalendar = e.Item.FindControl("ltAddToCalendar") as Literal;
                if (ltAddToCalendar != null)
                {
                    ltAddToCalendar.Text =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.Calendar.EventTopicOfferDates.AddToCalendarLink");
                }

                var spnOrganizer = e.Item.FindControl("spnOrganizer") as HtmlGenericControl;
                if (spnOrganizer != null)
                {
                    spnOrganizer.InnerText = ItemHelper.GetFieldRawValue(
                        eventTopicItem, ItemMapper.EventTopic.FieldName.EventContactName);
                }

                var spnOrganizerEmail = e.Item.FindControl("spnOrganizerEmail") as HtmlGenericControl;
                if (spnOrganizerEmail != null)
                {
                    spnOrganizerEmail.InnerText = ItemHelper.GetFieldRawValue(
                        eventTopicItem, ItemMapper.EventTopic.FieldName.EventContactEmail);
                }

                var spnEventSummary = e.Item.FindControl("spnEventSummary") as HtmlGenericControl;
                if (spnEventSummary != null)
                {
                    spnEventSummary.InnerText = ItemHelper.GetFieldRawValue(
                        eventTopicItem, ItemMapper.EventTopic.FieldName.Headline);
                }
                //string strEventTopicLink = ModuleHelper.GetEventTopicLinkUrl(eventTopicItem, item);

                var spnEvenDescription = e.Item.FindControl("spnEventDescription") as HtmlGenericControl;
                if (spnEvenDescription != null)
                {
                    spnEvenDescription.InnerHtml =
                        StringHelper.StripHtmlTags(ItemHelper.GetItemTeaser(eventTopicItem, 50));
                    // GetItemTeaser strips HTML, but adds surrounding <p>, so need to strip
                }
            }

            // RIGHT CONTENT

            var pnlRegister = e.Item.FindControl("pnlRegister") as Panel;
            if (pnlRegister != null)
            {
                //#if PREMIUM
                // Add to Cart
                var pnlAddRemove = e.Item.FindControl("pnlAddRemove") as Panel;
                var lbAddRemove = e.Item.FindControl("lbAddRemove") as LinkButton;

                // Add vs. Remove button based on whether item already in cart
                var cartSession = ModuleHelper.ShoppingCart.FindCartSession(item.ID.ToString());

                if (pnlAddRemove != null && lbAddRemove != null && RegistrationHelper.IsAllowedRegistration(item))
                {
                    pnlRegister.Visible = true;
                    pnlAddRemove.Visible = true;
                    var pnlErrorMessage = e.Item.FindControl("pnlErrorMessage") as Panel;
                    var litErrorMessage = e.Item.FindControl("litErrorMessage") as Literal;
                    if (pnlErrorMessage != null && litErrorMessage != null)
                    {
                        pnlErrorMessage.Visible = false;
                        litErrorMessage.Text = this.DictionaryAddToCartError;
                    }
                    lbAddRemove.CommandName = item.ID.ToString();
                    lbAddRemove.CommandArgument = e.Item.DataItemIndex.ToString();
                    if (cartSession != null)
                    {
                        lbAddRemove.Text = this.DictionaryRemoveFromCartLink;
                    }
                    else
                    {
                        lbAddRemove.Text = this.IsWaitlistSession(item.ID.ToString())
                                               ? this.DictionaryWaitlistLink
                                               : this.DictionaryAddToCartLink;
                    }
                }
            }

            // Capacity: Seats available or Class Full (-1 return from GetSeats => no fixed capacity, display nothing)
            var pnlCapacity = e.Item.FindControl("pnlCapacity") as Panel;
            var seatsAvailable = RegistrationHelper.GetSessionSeatsAvailable(item);

            if (pnlCapacity != null)
            {
                if (isOngoingEvent || seatsAvailable == -1)
                {
                    pnlRegister.Visible = true;
                }
                else
                {
                    if (seatsAvailable >= 0 && !this.IsSameDayRegistrationTimeEnded(item))
                    {
                        var litCapacityInfo = e.Item.FindControl("litCapacityInfo") as Literal;
                        if (litCapacityInfo != null)
                        {
                            pnlRegister.Visible = true;
                            pnlCapacity.Visible = true;
                            litCapacityInfo.Text = (seatsAvailable == 0
                                                        ? this.DictionaryClassFullLabel
                                                        : string.Format(
                                                            "{0}: {1:d}", this.DictionaryAvailabilityLabel, seatsAvailable));
                        }
                    }
                    else
                    {
                        pnlRegister.Visible = false;
                    }
                }
            }





            // Instructors
            var pnlInstructors = e.Item.FindControl("pnlInstructors") as Panel;
            var lvInstructors = e.Item.FindControl("lvInstructors") as ListView;
            if (pnlInstructors != null && lvInstructors != null)
            {
                var instructorItems = ItemHelper.GetItemsFromMultilistField(EventSession.FieldName.Instructors, item);
                if (instructorItems.Any())
                {
                    pnlInstructors.Visible = true;
                    lvInstructors.DataSource = instructorItems;
                    lvInstructors.DataBind();
                }
            }
        }

        private bool IsSameDayRegistrationTimeEnded(Item eventSessionItem)
        {
            var sessionRegistrationEndTime = ModuleHelper.GetSitecoreDateField(
                eventSessionItem, EventSession.FieldName.EndDate);
            if (sessionRegistrationEndTime.HasValue)
            {
                if (Convert.ToDateTime(DateTime.Now) > Convert.ToDateTime(sessionRegistrationEndTime))
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsWaitlistSession(string inSessionID)
        {
            var enrollType = RegistrationHelper.CheckRegistrationCapacity(inSessionID);
            return (enrollType == eEventCapacity.WaitlistAvailable);
        }

        #endregion

        //#endif
    }
}