﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventTopicThumbnail.ascx.cs" Inherits="womans.org.Website.layouts.modules.Calendar.EventTopicThumbnail" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<sc:Image runat="server" ID="scImgThumbnail" Field="Event Thumbnail" CssClass="right"/>