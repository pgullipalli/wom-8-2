﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="UpcomingSessionEvents.ascx.cs"
    Inherits="womans.org.Website.layouts.modules.Calendar.UpcomingSessionEvents" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: Calendar: Upcoming Session Events =============================-->
<asp:Panel runat="server" ID="phUpcomingEvents" CssClass="module-ce-session-upcoming  module-ce-upcoming">
    <div class="grid reg-callout">
    <asp:PlaceHolder ID="phUpcomingEventsHeader" runat="server">
        <h3 class="module-heading"><asp:Label ID="lblUpcomingEventsHeader" runat="server" /></h3>
    </asp:PlaceHolder>
    <asp:Panel ID="pnlListing" runat="server" CssClass="listing-wrap twelve columns">
        <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_OnItemDataBound">
            <LayoutTemplate>
                <ul class="core-list">
                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                </ul>
            </LayoutTemplate>
            <ItemTemplate>
				<li class="core-li grid">
                    <h5 class="list-item-header">
                        <asp:HyperLink ID="hlItem" runat="server" />
                        <asp:Literal ID="litItem" runat="server" />
                    </h5>
                    <asp:Panel ID="pnlThumbnail" runat="server" CssClass="list-item-image" Visible="false">
                        <!-- Leave these at max. pixels thumbnail can ever be (i.e, in the main content panel) 
                        CSS will resize smaller if needed -->
                        <sc:Image ID="scThumbnail" Field="Event Thumbnail" MaxHeight="100" MaxWidth="100" runat="server" />
                    </asp:Panel>
			        <div class="list-item-copy">
                        <asp:Panel ID="pnlTeaser" runat="server" Visible="false" CssClass="list-item-teaser">
                            <asp:Literal ID="litTeaser" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="pnlDate" runat="server" CssClass="module-date" Visible="false">
                            <asp:Literal runat="server" ID="litSessionDateTime"></asp:Literal>
                        </asp:Panel>
                        <asp:Panel ID="pnlReadMore" runat="server" CssClass="list-item-links" Visible="false">
                            <asp:HyperLink ID="hlMoreLink" runat="server" Visible="False" />
                            <asp:Label ID="lblSeperator" runat="server" Visible="False">|</asp:Label>
                            <asp:HyperLink ID="hlRegister" runat="server" Visible="False" />
                        </asp:Panel>
                    </div>
                </li>
            </ItemTemplate>
        </asp:ListView>
    </asp:Panel>
    <asp:Panel ID="pnlViewAll" runat="server" Visible="false" CssClass="module-ce-view-all core-view-all twelve columns">
        <asp:HyperLink ID="hlViewAll" runat="server" Visible="false" />
        <asp:HyperLink ID="hlViewOngoing" runat="server" Visible="false" />
    </asp:Panel>
    </div>
</asp:Panel>
<!--==================== /Module: Calendar: Upcoming Session Events =============================-->