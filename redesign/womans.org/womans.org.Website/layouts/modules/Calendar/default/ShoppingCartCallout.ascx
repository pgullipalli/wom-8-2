﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartCallout.ascx.cs" 
    Inherits="MedTouch.Calendar.layouts.modules.Calendar.Default.ShoppingCartCallout" %>
<!--==================== Module: Calendar: Shopping Cart Summary =============================-->
<asp:Panel ID="pnlShoppingCart" runat="server" CssClass="callout-wrapper module-ce-session-shoppingcart module-ce-session-shoppingcart-summary">
    <div class="callout">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:PlaceHolder runat="server" ID="phControlHeader">
                            <h3 class="module-header"><asp:Label ID="lblShoppingCartHeader" runat="server" /></h3>
                </asp:PlaceHolder>
                <div class="grid reg-callout">
                    <asp:Panel runat="server" ID="pnlCartSummary" CssClass="cart-summary twelve columns" >
                        <p><asp:Label ID="lblCartSummary" runat="server" /></p>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlCartActions" Visible="false" CssClass="cart-actions grid twelve columns">
                        <asp:Panel ID="pnlEmpty" runat="server" CssClass="cart-edit six columns">
                            <asp:HyperLink ID="hlEditCart" runat="server" CssClass="button pink"/>
                        </asp:Panel>
                        <asp:Panel ID="pnlCheckout" runat="server" CssClass="cart-checkout six columns" >
                            <asp:HyperLink ID="hlCheckout" runat="server" CssClass="button pink"/>
                        </asp:Panel>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Panel>

<!--==================== /Module: Calendar: Shopping Cart Summary =============================-->
