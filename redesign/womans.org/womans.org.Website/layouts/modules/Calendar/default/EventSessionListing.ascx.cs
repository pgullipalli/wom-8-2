﻿namespace womans.org.Website.layouts.modules.Calendar.@default
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;

    using global::womans.org.BusinessLogic.Calendar;

    using MedTouch.Calendar.Helpers;
    using MedTouch.Calendar.Search;
    using MedTouch.Common.Helpers;
    using MedTouch.Common.UI;

    using Sitecore.Data.Items;

    public partial class EventSessionListing : BaseSublayout
    {
        #region Dictionaries
        protected readonly string DictionaryNoResultsMessage = CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventSessionListing.NoResultsMessage");
        protected readonly string DictionaryReadMoreLink = CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventSessionListing.ReadMoreLink");
        protected readonly string DictionaryRegisterLink = CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventSessionListing.RegisterLink");
        protected readonly string DictionaryViewOngoing = CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventSessionListing.ViewOngoing");
        protected readonly string DictionaryViewDated = CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventSessionListing.ViewDated");
        #endregion Dictionaries

        #region Properties
        private readonly string _strQuerystringPage = ModuleHelper.Config.QueryString.GetPageQuerystring();

        private string _dateFormat = "";
        private bool _includeOngoing = false;

        private int _numberOfDisplayedItems = 1;
        private int _numberOfDisplayedPages = 1;
        private int _maxTeaserCharacters = 100;
        private bool _showLinkToDetail = true;
        private bool _resultTitleAsLink = true;
        private string _readMoreText = "Read More";

        #endregion Properties

        #region RenderingParameters
        protected readonly string RenderingParamDateFormat = "DateFormat";
        protected readonly string RenderingParamIncludeOngoing = "IncludeOngoing";

        protected readonly string RenderingParamNumberOfDisplayedItems = "Number of Displayed Items";
        protected readonly string RenderingParamNumberOfDisplayedPages = "Number of Displayed Pages";
        protected readonly string RenderingParamMaxTeaserCharacters = "Max Teaser Characters";
        protected readonly string RenderingParamShowTitleAsLink = "Show Title as Link";
        protected readonly string RenderingParamShowLinkToDetailPage = "Show Link to Detail Page";

        #endregion RenderingParameters

        protected void Page_Load(object sender, EventArgs e)
        {
            #if (!DEBUG)
            try
            {
            #endif
                //No check post back here to support submission from other controls
                this.Initialize();
                this.Search();

            #if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
            #endif
        }

        protected override void Initialize()
        {
            // Rendering Parameter
            this._dateFormat = this.GetProperty(this.RenderingParamDateFormat) ?? "";
            this._includeOngoing = this.GetBooleanProperty(this.RenderingParamIncludeOngoing);
            this._numberOfDisplayedItems = this.GetIntProperty(this.RenderingParamNumberOfDisplayedItems);
            this._numberOfDisplayedPages = this.GetIntProperty(this.RenderingParamNumberOfDisplayedPages);
            this._maxTeaserCharacters = this.GetIntProperty(this.RenderingParamMaxTeaserCharacters);
            this._resultTitleAsLink = this.GetBooleanProperty(this.RenderingParamShowTitleAsLink);
            this._showLinkToDetail = this.GetBooleanProperty(this.RenderingParamShowLinkToDetailPage);

            this._readMoreText = this.DictionaryReadMoreLink;

            // Paging Control
            ucPagingTop.NumberOfDisplayedItem = this._numberOfDisplayedItems;
            ucPagingTop.NumberOfDisplayedPage = this._numberOfDisplayedPages;
            ucPagingBottom.NumberOfDisplayedItem = this._numberOfDisplayedItems;
            ucPagingBottom.NumberOfDisplayedPage = this._numberOfDisplayedPages;
        }

        protected void Search()
        {
            string page = this.Request.QueryString[this._strQuerystringPage];

            List<Item> items = new List<Item>();

            if (string.IsNullOrEmpty(page))
                page = "1";

            int totalCount = 0;
            int startItem = 0;
            int endItem = 0;

            string keyword = this.Request.QueryString[ModuleHelper.Config.QueryString.GetKeywordQuerystring()];
            string category = this.Request.QueryString[ModuleHelper.Config.QueryString.GetCategoryQuerystring()];
            string instructor = this.Request.QueryString[ModuleHelper.Config.QueryString.GetInstructorQuerystring()];
            string dateFrom = this.Request.QueryString[ModuleHelper.Config.QueryString.GetDateFromQuerystring()];
            string dateTo = this.Request.QueryString[ModuleHelper.Config.QueryString.GetDateToQuerystring()];

            // Taggings
            string specialties = this.Request.QueryString[BaseModuleHelper.SpecialtiesQuerystring()];
            string locations = this.Request.QueryString[BaseModuleHelper.LocationsQuerystring()];
            string taxonomy = this.Request.QueryString[BaseModuleHelper.TaxonomyQuerystring()];
            string taxonomyRecursive = this.Request.QueryString[BaseModuleHelper.TaxonomyRecursivelySearchQuerystring()];

            // Types of events to list: Dated (default) or Ongoing
            EventListType eventListType =
                this.Request.QueryString[ModuleHelper.Config.QueryString.GetEventTypeQuerystring()] == "Ongoing"
                    ? EventListType.Ongoing
                    : EventListType.Dated;

            bool isTaxonomyRecursive;
            if (!bool.TryParse(taxonomyRecursive, out isTaxonomyRecursive))
                isTaxonomyRecursive = true;

            // Prepare View Ongoing/View Dated link (see below)
            var SearchPageItem = ModuleHelper.GetSearchResultPageItem(Sitecore.Context.Item);
            string viewAllUrl = string.Empty;
            if (SearchPageItem != null)
            {
                string strSpecialties = specialties;
                string strLocations = locations;
                string taxonomyItems = ItemHelper.GetShortIds(this.ContextItem, "Taxonomy Terms");
                viewAllUrl = BaseModuleHelper.GetSearchResultsUrl(
                    strSpecialties,
                    strLocations,
                    taxonomyItems,
                    isTaxonomyRecursive,
                    SearchPageItem);
            }


            //Prepare Tagging
            specialties = SitecoreSearchHelper.GetTagSpecialtyGuidsFromNames(specialties);
            locations = SitecoreSearchHelper.GetTagLocationGuidsFromNames(locations);
            taxonomy = SitecoreSearchHelper.GetTagTaxonomyGuidsFromShortIds(taxonomy, isTaxonomyRecursive);

            //Search Param
            var eventSearchParam = new CalendarSearchParam()
            {
                Keyword = keyword,
                Category = category,
                Instructor = instructor,
                DateFrom = dateFrom,
                DateTo = dateTo,
                SearchType = CalendarSearchType.EventSession,
                ListType = eventListType,
                IncludeOngoing = this._includeOngoing,
                Locations = locations,
                Specialties = specialties,
                Taxonomy = taxonomy,
                IsTaxonomyRecursive = isTaxonomyRecursive
            };

            // Search
            WomansModuleHelper.CalendarSearch(eventSearchParam, page,
                this._numberOfDisplayedItems,
                this._numberOfDisplayedPages,true, out startItem, out endItem,
                out totalCount, out items);


            // Setup pagination Param
            MedTouch.Pagination.Helpers.ModuleHelper.SetPagination(ucPagingTop, totalCount, startItem, endItem,
                this._numberOfDisplayedItems, this._numberOfDisplayedPages,
                this._strQuerystringPage);

            MedTouch.Pagination.Helpers.ModuleHelper.SetPagination(ucPagingBottom, totalCount, startItem, endItem,
                this._numberOfDisplayedItems, this._numberOfDisplayedPages,
                this._strQuerystringPage);

            // Bind Result
            lvSearchResults.DataSource = items;
            lvSearchResults.DataBind();

            // Initialize "View Dated"/"View Ongoing" link if search page is available (see above)
            if (!string.IsNullOrEmpty(viewAllUrl))
            {
                // First: Need to find out if there ARE any alternate-view items to show in the list before showing the link
                if (eventListType == EventListType.Ongoing)
                {
                    hlViewOngoing.Text = this.DictionaryViewDated;
                    eventSearchParam.ListType = EventListType.Dated;
                }
                else
                {
                    hlViewOngoing.Text = this.DictionaryViewOngoing;
                    eventSearchParam.ListType = EventListType.Ongoing;
                }
                CalendarSearcher calendarSearcher = new CalendarSearcher();
                var altItems = calendarSearcher.ExecuteSearch(eventSearchParam);
                if (altItems.Count > 0)
                {
                    // Second: add/remove ongoing querystring to link if needed
                    hlViewOngoing.NavigateUrl = (eventListType == EventListType.Ongoing
                        ? UrlHelper.RemoveQueryString(ModuleHelper.Config.QueryString.GetEventTypeQuerystring())
                        : UrlHelper.AppendQueryString(ModuleHelper.Config.QueryString.GetEventTypeQuerystring(),
                            "Ongoing"));
                    pnlViewOngoing.Visible = true;
                }
            }
        }

        protected void lvSearchResults_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem)
                return;

            Item item = e.Item.DataItem as Item;

            if (item != null)
            {
                Item eventTopicItem = ModuleHelper.GetEventTopicFromEventSession(item);
                string strEventTopicLink = ModuleHelper.GetEventTopicLinkUrl(eventTopicItem, item);

                // Photo from event Topic item. (Parent Item)
                Panel pnlImage = e.Item.FindControl("pnlImage") as Panel;
                Sitecore.Web.UI.WebControls.Image scImage = e.Item.FindControl("scImage") as Sitecore.Web.UI.WebControls.Image;
                HyperLink hlImage = e.Item.FindControl("hlImage") as HyperLink;
                string strImage = ItemHelper.GetFieldRawValue(eventTopicItem, ItemMapper.EventTopic.FieldName.EventThumbnail);
                if (!string.IsNullOrEmpty(strImage) && scImage != null && hlImage != null && pnlImage != null)
                {
                    scImage.Item = eventTopicItem;
                    hlImage.NavigateUrl = strEventTopicLink;
                    pnlImage.Visible = true;
                }

                // Headline
                Literal litItem = e.Item.FindControl("litItem") as Literal;
                HyperLink hlItem = e.Item.FindControl("hlItem") as HyperLink;
                string strDisplayName = ItemHelper.GetFieldRawValue(eventTopicItem, ItemMapper.EventTopic.FieldName.Headline);
                if (hlItem != null && this._resultTitleAsLink)
                {
                    hlItem.Text = strDisplayName;
                    hlItem.NavigateUrl = strEventTopicLink;
                    if (litItem != null) litItem.Visible = false;
                }
                else if (litItem != null)
                {
                    litItem.Text = strDisplayName;
                    if (hlItem != null) hlItem.Visible = false;
                }

                // Teaser
                Panel pnlTeaser = (Panel)e.Item.FindControl("pnlTeaser");
                Literal litTeaser = e.Item.FindControl("litTeaser") as Literal;
                string strTeaser = ItemHelper.GetItemTeaser(eventTopicItem, this._maxTeaserCharacters);
                if (pnlTeaser != null && litTeaser != null && !string.IsNullOrEmpty(strTeaser)) {
                    pnlTeaser.Visible = true;
                    litTeaser.Text = strTeaser;
                }

                // Instructors
                Panel pnlInstructors = (Panel)e.Item.FindControl("pnlInstructors");
                Literal litInstructorsLabel = e.Item.FindControl("litInstructorsLabel") as Literal;
                Literal litInstructors = e.Item.FindControl("litInstructors") as Literal;
                string strInstructors = RegistrationHelper.GetSessionInstructorList(item);
                if (pnlInstructors != null && litInstructors != null && litInstructorsLabel != null && !string.IsNullOrEmpty(strInstructors))
                {
                    litInstructorsLabel.Text = CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventSessionListing.LabelInstructors");
                    litInstructors.Text = strInstructors;
                    pnlInstructors.Visible = true;
                }

                // Date
                var pnlDate = e.Item.FindControl("pnlDate") as Panel;
                var litSessionDateTime = e.Item.FindControl("litSessionDateTime") as Literal;
                litSessionDateTime.Text = RegistrationHelper.GetSessionDateTimeString(item, true, this._dateFormat);
                pnlDate.Visible = (!string.IsNullOrEmpty(litSessionDateTime.Text));

                // Read More
                Panel pnlReadMore = e.Item.FindControl("pnlReadMore") as Panel;
                HyperLink hlMoreLink = e.Item.FindControl("hlMoreLink") as HyperLink;
                if (hlMoreLink != null && pnlReadMore != null && this._showLinkToDetail)
                {
                    hlMoreLink.Text = this._readMoreText;
                    hlMoreLink.NavigateUrl = strEventTopicLink;
                    hlMoreLink.Visible = true;
                    pnlReadMore.Visible = true;
                }
            }
        }
    }
}