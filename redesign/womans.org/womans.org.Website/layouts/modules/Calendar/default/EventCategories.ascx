﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventCategories.ascx.cs"
    Inherits="MedTouch.Calendar.layouts.modules.Calendar.EventCategories" %>
<!--==================== Module: Calendar: Event Categories =============================-->
<asp:Panel ID="pnlBrowseByCategory" runat="server" CssClass="module-ce-categories collapse-for-mobile">
    <div class="reg-callout grid">
        <asp:PlaceHolder runat="server" ID="phControlHeader" Visible="False">
            <h3 class="module-heading"><asp:Literal ID="lblBrowseByCategoryTitle" runat="server" Visible="False"/></h3>
        </asp:PlaceHolder>
        <asp:Repeater ID="rptCategories" runat="server" OnItemDataBound="rptCategories_OnItemDataBound">
            <HeaderTemplate>
                <ul class="module-ce-filters-list">
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <asp:HyperLink ID="hlCategory" runat="server" />
                </li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>
<!--==================== /Module: Calendar: Event Categories =============================-->
