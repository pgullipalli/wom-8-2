﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartCheckout.ascx.cs" 
    Inherits="MedTouch.Calendar.layouts.modules.Calendar.Default.ShoppingCartCheckout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: Calendar: Shopping Cart =============================-->
<div class="cart-header-wrap grid">
    <h1 class="module-ce-checkout-header nine columns"><sc:Text ID="scHeadline" runat="server" Field="Headline" /></h1>
    <asp:Panel ID="pnlEmpty" runat="server" CssClass="cart-empty three columns">
        <asp:LinkButton ID="lbEmpty" runat="server" onclick="lbEmpty_Click" />
    </asp:Panel>
</div>
<asp:Panel ID="pnlShoppingCart" runat="server" CssClass="module-ce-session-shoppingcart grid">
    <asp:Panel ID="pnlCartContent" runat="server" CssClass="cart-contents twelve columns grid">
        <div class="twelve columns">
            <asp:Literal ID="litEmptyMessage" runat="server" Visible="false" />
            <asp:ListView ID="lvShoppingCartEvents" runat="server" OnItemDataBound="lvShoppingCartEvents_OnItemDataBound">
                <LayoutTemplate>
                    <ul class="cart-listing">
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <li class="cart-item">
                        <div class="item-summary">
                            <h5 class="name">
                                <asp:HyperLink ID="hlItem" runat="server" />
                                <asp:Literal runat="server" ID="litItem" />
                            </h5>
                        </div>                    
                        <asp:Panel ID="pnlShoppingCartSessions" runat="server" Visible="false">
                            <asp:ListView ID="lvShoppingCartSessions" runat="server" OnItemDataBound="lvShoppingCartSessions_OnItemDataBound">
                                <ItemTemplate>
                                    <asp:Panel ID="pnlResult" runat="server" CssClass="item-details">
                                        <asp:Panel ID="pnlRemove" runat="server" CssClass="item-actions-remove"  Visible="false">
                                            <asp:LinkButton ID="lbRemove" runat="server" OnCommand="lbRemove_OnCommand" />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlDateTime" runat="server" CssClass="desc" Visible="false">
                                            <asp:Literal ID="litDateTime" runat="server" />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlFee" runat="server" CssClass="item-price" Visible="false">
                                            <asp:Literal ID="litFee" runat="server" />
                                        </asp:Panel>
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:ListView>
                        </asp:Panel>
                    </li>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlCartActions" CssClass="cart-actions twelve columns grid" Visible="false">
        <asp:Panel runat="server" ID="pnlAddPromo" CssClass="cart-promos six columns grid first" Visible="false">
		    <div class="add-promo-link twelve columns">
			    <a href="#"><asp:Literal ID="litPromoCodeReveal" runat="server" /></a>
		    </div>
            <asp:Panel ID="pnlPromoCodeEntry" runat="server" CssClass="add-promo-panel twelve columns grid" DefaultButton="btnApplyPromoCode" >
                <asp:HiddenField runat="server" ID="hdnPromoCodesAvail" ClientIDMode="Static" />
                <asp:Label ID="lbPromoCode" runat="server" AssociatedControlID="txtPromoCode" />
                <asp:TextBox ID="txtPromoCode" runat="server" CssClass="textbox promo-input nine columns first" />
                <asp:Button ID="btnApplyPromoCode" runat="server" CssClass="button pink default_button" OnClick="btnApplyPromoCode_Click" />
			    <span class="error" data-reason="blank" style="display:none;"><asp:Literal ID="litErrBlankPromocode" runat="server" /></span>
			    <span class="error" data-reason="invalid" style="display:none;"><asp:Literal ID="litErrInvalidPromocode" runat="server" /></span>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlSummary" Visible="false" CssClass="cart-calculations six columns grid">
            <asp:Panel ID="pnlShowPromos" runat="server" Visible="false">
                <div class="cart-subtotal twelve columns">
                    <h5>
                        <asp:Label ID="lblSubtotalLabel" runat="server"/>
                        <asp:Literal ID="litSubtotalValue" runat="server" />
                    </h5>                
                </div>
                <div class="cart-promos twelve columns" >
                    <div class="applied-promos">
                        <asp:ListView ID="lvShoppingCartPromos" runat="server" OnItemDataBound="lvShoppingCartPromos_OnItemDataBound">
                            <ItemTemplate>
                                <div>
                                    <asp:Literal ID="litPromoCode" runat="server" />
                                    <span><asp:Literal runat="server" ID="litPromoAmt"></asp:Literal></span>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlTotal" CssClass="cart-total twelve columns" >
                <h5>
                    <asp:Label ID="lblTotalLabel" runat="server"/>
                    <asp:Literal ID="litTotalValue" runat="server" />
                </h5>
            </asp:Panel>
        </asp:Panel>
	    <div class="cart-buttons twelve columns grid">
            <asp:Panel ID="pnlRegister" runat="server" CssClass="cart-register six columns push_six">
                <asp:HyperLink ID="hlRegister" runat="server" CssClass="button"/>
            </asp:Panel>
        </div>
    </asp:Panel>
</asp:Panel>
        
<!--==================== /Module: Calendar: Shopping Cart =============================-->
