﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomFormLayout.aspx.cs" Inherits="MedTouch.Calendar.layouts.modules.Calendar.CustomFormLayout" %>

<%@ OutputCache Location="None" VaryByParam="none" %>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head id="Head1" runat="server">
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
    <sc:sublayout id="slSeoData" runat="server" path="/layouts/Base/Default/sublayouts/Includes/SeoData.ascx" />   
	<!--meta name="google-site-verification" content=""-->
	<!-- Mobile Device Scale/Zoom -->	
    <!--meta name="viewport" content="width=device-width"-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <!-- Fav Icons -->
	<!--<link rel="shortcut icon" href="/assets/mtmc/images/favicon.ico">-->
	<link rel="apple-touch-icon" href="/assets/mtmc/images/apple-touch-icon.png">
	<!-- Stylesheets -->
    <link rel="stylesheet" href="/assets/mtmc/css/styles.css">
	<link rel="stylesheet" href="/assets/mtmc/css/modules.css">	

	<!--<script src="/assets/js/prefixfree.min.js"></script>-->
        <!--[if lt IE 9]>
           <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>	
        <![endif]-->
</head>
<body class="<%= MedTouch.Base.BLL.Helpers.LayoutHelper.GetBodyClass() %>" >
    <sc:sublayout id="slOpenBodyScripts" runat="server" path="/layouts/Base/Default/sublayouts/Includes/UserEditedScripts.ascx" parameters="Field=Open Body Script" />
    <form method="post" runat="server" id="Form2" >
        <div class="module-ce-customform core-customform">
            <sc:placeholder ID="Placeholder2" key="Main" runat="server" />
        </div>
    </form>
    
	<script src="/assets/mtmc/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>		
	<!-- Grab Google CDN jQuery. fall back to local if necessary -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>	    !window.jQuery && document.write('<script src="/assets/mtmc/js/jquery-1.9.1.js"><\/script>')</script>
	<!-- Grab Google CDN jQuery UI. fall back to local if necessary -->
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script>	    !window.jQuery.ui && document.write('<script src="/assets/mtmc/js/jquery-ui-1.10.4.min.js"><\/script>')</script>
	<!-- Local Scripts -->
	<script src="/assets/mtmc/js/plugins.js"></script>
	<script src="/assets/mtmc/js/script.js"></script>
    <sc:sublayout id="slCloseBodyScripts" runat="server" path="/layouts/Base/Default/sublayouts/Includes/UserEditedScripts.ascx"  parameters="Field=Close Body Script"/>
 
    <!-- With new responsive design HTML, this script below need to move to the bottom of \layouts\Base\Default\layouts\BrowserLayout.aspx -->
    <script src="/sitecore modules/Web/Web Forms for Marketers/Control/wffmmtcustomscript.js" type="text/javascript"></script> 
</body>