﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MedTouch.Common.UI;

namespace womans.org.Website.layouts.modules.Calendar
{
	public partial class EventTopicThumbnail : BaseSublayout
	{
        private void Page_Load(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif

            Initiate();

#if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
#endif

        }

        private void Initiate()
        {
 
        }
	}
}