﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PDFEventRosterListingCustomFormOne.ascx.cs" Inherits="womans.org.Website.layouts.modules.Calendar.PDFEventRosterListingCustomFormOne" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<div class="listing">
    <asp:Panel runat="server" ID="pnlEventTopicHeadline" Style="text-align: center">
        <h1>
            <asp:Literal runat="server" ID="eventTopicHeadline"></asp:Literal>
        </h1>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlSessionDateTime" Style="text-align: center">
        <h3>
            <asp:Literal runat="server" ID="sessionDateTime"></asp:Literal>
        </h3>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLocation">
        <table style="page-break-inside: avoid">
            <tr>
                <td class="col-left" style="width: 50%">
                    <asp:Panel runat="server" ID="pnlLocationLabel">
                        <h4>
                            <asp:Literal runat="server" ID="litLocationLabel"></asp:Literal>
                        </h4>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlLocationName">
                        <asp:Literal runat="server" ID="litLocationName"></asp:Literal>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlLocationAddressOne">
                        <asp:Literal runat="server" ID="litLocationAddressOne"></asp:Literal>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlLocationAddressTwo">
                        <asp:Literal runat="server" ID="litLocationAddressTwo"></asp:Literal>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlLocationCityStateZip">
                        <asp:Literal runat="server" ID="litLocationCityStateZip"></asp:Literal>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlInstructors">
        <table>
            <tr>
                <td class="col-left" style="width: 50%">
                    <asp:Panel runat="server" ID="pnlInstructorsLabel">
                        <h4>
                            <asp:Literal runat="server" ID="litInstructorsLabel"></asp:Literal>
                        </h4>
                    </asp:Panel>
                    <asp:Repeater runat="server" ID="rptInstructors">
                        <ItemTemplate>
                            <asp:Panel runat="server" ID="pnlInstructorName">
                                <%# Eval("InstructorName") %>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <div class="hr"></div>
    <asp:ListView ID="lvEventRoster" runat="server" OnItemDataBound="lvEventRoster_ItemDataBound">
        <ItemTemplate>
            <table>
                <tr>
                    <td class="col-left" style="width: 50%">
                        <asp:Panel ID="pnlName" runat="server">
                            <h4>
                                <asp:Literal ID="litLastName" runat="server" />
                                ,&nbsp;
                                <asp:Literal ID="litFirstName" runat="server" />
                            </h4>
                        </asp:Panel>
                        <asp:Panel ID="pnlPhone" runat="server" Visible="false">
                            <asp:Literal ID="litPhone" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="pnlEmail" runat="server" Visible="false">
                            <asp:Literal ID="litEmail" runat="server" />
                        </asp:Panel>
                    </td>
                    <td class="col-right" style="width: 50%">
                        <asp:Panel ID="pnlDueDate" runat="server" Visible="false">
                            <asp:Literal ID="litDueDateLabel" runat="server" />
                            <asp:Literal ID="litDueDate" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="pnlPartnerName" runat="server" Visible="false">
                            <asp:Literal ID="litPartnerNameLabel" runat="server" />
                            <asp:Literal ID="litPartnerName" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="pnlPracticeOrPhysicianName" runat="server" Visible="false">
                            <asp:Literal ID="litPracticeOrPhysicianNameLabel" runat="server" />
                            <asp:Literal ID="litPracticeOrPhysicianName" runat="server" />
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <div class="hr"></div>
        </ItemTemplate>
    </asp:ListView>
</div>
