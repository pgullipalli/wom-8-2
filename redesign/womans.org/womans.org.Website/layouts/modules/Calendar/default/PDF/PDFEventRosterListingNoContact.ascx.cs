﻿namespace womans.org.Website.layouts.modules.Calendar
{
    #region

    using System;
    using System.Linq;
    using System.Web.UI.WebControls;

    using MedTouch.Calendar.Entities;
    using MedTouch.Calendar.Helpers;
    using MedTouch.Calendar.Helpers.ObjectModel;
    using MedTouch.Common.Helpers;
    using MedTouch.Common.UI;

    using Sitecore.Data;
    using Sitecore.Diagnostics;
    using Sitecore.Form.Web.UI.Controls;
    using Sitecore.StringExtensions;

    #endregion

    /// <summary>
    ///     The pdf event roster listing.
    /// </summary>
    public partial class PDFEventRosterListingNoContact : BaseSublayout
    {
        #region Fields

        /// <summary>
        ///     The _custom d bcontext.
        /// </summary>
        private CustomEntities customDbContext;

        /// <summary>
        ///     The _event session guid.
        /// </summary>
        private Guid eventSessionGuid;

        /// <summary>
        ///     The _event session id.
        /// </summary>
        private string eventSessionId;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the event session id.
        /// </summary>
        public string EventSessionId
        {
            get
            {
                if (string.IsNullOrEmpty(this.eventSessionId))
                {
                    this.eventSessionGuid = Guid.Empty;
                    if (this.Request.QueryString[ModuleHelper.Config.QueryString.GetEventSessionIDQuerystring()] != null)
                    {
                        this.eventSessionId =
                            this.Request.QueryString[ModuleHelper.Config.QueryString.GetEventSessionIDQuerystring()];
                        Guid.TryParse(this.eventSessionId, out this.eventSessionGuid);
                    }
                    else
                    {
                        this.eventSessionId = string.Empty;
                        this.eventSessionGuid = Guid.Empty;
                    }
                }

                return this.eventSessionId;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the custom d bcontext.
        /// </summary>
        protected CustomEntities CustomDBcontext
        {
            get
            {
                if (this.customDbContext == null)
                {
                    this.customDbContext = new CustomEntities();
                }

                return this.customDbContext;
            }
        }

        #endregion

        // private string _dateFormat = "d";

        // protected readonly string RenderingParamDateFormat = "DateFormat";

        #region Methods

        /// <summary>
        ///     The bind.
        /// </summary>
        protected override void Bind()
        {
            Assert.IsNotNullOrEmpty(this.EventSessionId, "Missing Event Session ID for printer friendly roster");
            Assert.IsFalse(this.eventSessionGuid == Guid.Empty, "Invalid Event Session ID for printer friendly roster");
            this.BindHeader();
            this.BindRoster();
        }

        /// <summary>
        ///     The bind roster.
        /// </summary>
        protected void BindRoster()
        {
            var evtHelper = ModuleHelper.CustomRegistrationHelper;
            var eventRegistration =
                evtHelper.GetEventRegistrations(this.eventSessionGuid)
                         .Where(evt => EventRegistrationStatus.GetStatusCountAsEnrolled(evt.RegistrationStatus))
                         .OrderBy(evt => evt.Lastname_Secure)
                         .ToList();
            this.lvEventRoster.DataSource = eventRegistration;
            this.lvEventRoster.DataBind();
        }

        /// <summary>
        ///     The lv event roster_ item data bound.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="e">
        ///     The e.
        /// </param>
        protected void lvEventRoster_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var litLastName = (Literal)e.Item.FindControl("litLastName");
            var litFirstName = (Literal)e.Item.FindControl("litFirstName");
            var pnlAddress1 = (Panel)e.Item.FindControl("pnlAddress1");
            var litAddress1 = (Literal)e.Item.FindControl("litAddress1");
            var pnlAddress2 = (Panel)e.Item.FindControl("pnlAddress2");
            var litAddress2 = (Literal)e.Item.FindControl("litAddress2");
            var pnlAddress3 = (Panel)e.Item.FindControl("pnlAddress3");
            var litAddress3 = (Literal)e.Item.FindControl("litAddress3");
            var pnlPhone = (Panel)e.Item.FindControl("pnlPhone");
            var litPhone = (Literal)e.Item.FindControl("litPhone");
            var pnlEmail = (Panel)e.Item.FindControl("pnlEmail");
            var litEmail = (Literal)e.Item.FindControl("litEmail");
            var litPmtMethodLabel = (Literal)e.Item.FindControl("litPmtMethodLabel");
            var litPmtMethod = (Literal)e.Item.FindControl("litPmtMethod");
            var litStatusLabel = (Literal)e.Item.FindControl("litStatusLabel");
            var litStatus = (Literal)e.Item.FindControl("litStatus");
            var litDateLabel = (Literal)e.Item.FindControl("litDateLabel");
            var litDate = (Literal)e.Item.FindControl("litDate");

            var evtReg = e.Item.DataItem as ICalendar_EventRegistration;
            litLastName.Text = evtReg.Lastname_Secure;
            litFirstName.Text = evtReg.Firstname_Secure;

            if (pnlAddress1 != null && litAddress1 != null && !string.IsNullOrEmpty(evtReg.Address_Secure))
            {
                litAddress1.Text = evtReg.Address_Secure;
                pnlAddress1.Visible = true;
            }

            if (pnlAddress2 != null && litAddress2 != null && !string.IsNullOrEmpty(evtReg.Address2_Secure))
            {
                litAddress2.Text = evtReg.Address2_Secure;
                pnlAddress2.Visible = true;
            }

            if (pnlAddress3 != null && litAddress3 != null)
            {
                var addr3 = evtReg.City_Secure;
                if (!string.IsNullOrEmpty(evtReg.State_Secure))
                {
                    addr3 = string.IsNullOrEmpty(addr3) ? evtReg.State_Secure : addr3 + ", " + evtReg.State_Secure;
                }

                addr3 += "&nbsp;&nbsp;" + evtReg.ZipCode_Secure;

                litAddress3.Text = addr3;
                pnlAddress3.Visible = true;
            }

            if (pnlPhone != null && litPhone != null && !string.IsNullOrEmpty(evtReg.PhoneNumber_Secure))
            {
                litPhone.Text = evtReg.PhoneNumber_Secure;
                pnlPhone.Visible = true;
            }

            if (pnlEmail != null && litEmail != null && !string.IsNullOrEmpty(evtReg.Email_Secure))
            {
                litEmail.Text = evtReg.Email_Secure;
                pnlEmail.Visible = true;
            }

            litPmtMethodLabel.Text =
                CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventRoster.PaymentMethodLabel");
            litPmtMethod.Text = evtReg.PaymentMethod;

            litStatusLabel.Text = CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventRoster.StatusLabel");
            litStatus.Text = EventRegistrationStatus.GetStatusString(evtReg.RegistrationStatus);

            litDateLabel.Text = CultureHelper.GetDictionaryTranslation(
                "Modules.Calendar.EventRoster.SubmittedDateLabel");
            litDate.Text = evtReg.Created.ToShortDateString();
        }

        /// <summary>
        ///     The set literal value.
        /// </summary>
        /// <param name="literal">
        ///     The literal.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        private static void SetLiteralValue(Literal literal, string value)
        {
            if (literal != null && !value.IsNullOrEmpty())
            {
                literal.Text = value;
                literal.Visible = true;
            }
        }

        /// <summary>
        ///     The bind header.
        /// </summary>
        private void BindHeader()
        {
            var id = new ID(this.EventSessionId);
            var item = Sitecore.Context.Database.GetItem(id);
            var eventSession = new EventSession(item);

            this.SetSessionMainValues(eventSession);
            this.SetLocationValues(eventSession);
            this.SetInstructorValues(eventSession);
        }

        /// <summary>
        ///     The set instructor values.
        /// </summary>
        /// <param name="eventSession">
        ///     The event session.
        /// </param>
        private void SetInstructorValues(EventSession eventSession)
        {
            var pnlInstructors = (Panel)this.FindControl("pnlInstructors");
            if (pnlInstructors != null)
            {
                var instructorsLabel = (Literal)this.FindControl("litInstructorsLabel");
                var pnlInstructorsLabel = (Panel)this.FindControl("pnlInstructorsLabel");
                var instructors = (Repeater)this.FindControl("rptInstructors");

                if (pnlInstructorsLabel != null)
                {
                    SetLiteralValue(
                        instructorsLabel,
                        CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventRoster.InstructorsLabel"));
                }

                instructors.DataSource = eventSession.Instructors;
                instructors.DataBind();
            }
        }

        /// <summary>
        /// The set location values.
        /// </summary>
        /// <param name="eventSession">The event session.</param>
        private void SetLocationValues(EventSession eventSession)
        {
            var pnlLocation = (Panel)this.FindControl("pnlLocation");
            if (pnlLocation != null && eventSession.EventLocation != null)
            {
                var locationLabel = (Literal)this.FindControl("litLocationLabel");
                var locationName = (Literal)this.FindControl("litLocationName");
                var locationAddressOne = (Literal)this.FindControl("litLocationAddressOne");
                var locationAddressTwo = (Literal)this.FindControl("litLocationAddressTwo");
                var locationAddressCityStateZip = (Literal)this.FindControl("litLocationCityStateZip");

                var pnlLocationLabel = (Panel)this.FindControl("pnlLocationLabel");
                var pnlLocationName = (Panel)this.FindControl("pnlLocationName");
                var pnlLocationAddressOne = (Panel)this.FindControl("pnlLocationAddressOne");
                var pnlLocationAddressTwo = (Panel)this.FindControl("pnlLocationAddressTwo");
                var pnlLocationAddressCityStateZip = (Panel)this.FindControl("pnlLocationCityStateZip");

                if (pnlLocationLabel != null)
                {
                    SetLiteralValue(
                        locationLabel,
                        CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventRoster.LocationLabel"));
                }

                if (pnlLocationName != null)
                {
                    SetLiteralValue(locationName, eventSession.EventLocation.LocationName);
                }

                if (pnlLocationAddressOne != null)
                {
                    SetLiteralValue(locationAddressOne, eventSession.EventLocation.Address1);
                }

                if (pnlLocationAddressTwo != null)
                {
                    SetLiteralValue(locationAddressTwo, eventSession.EventLocation.Address2);
                }

                if (pnlLocationAddressCityStateZip != null)
                {
                    var locName = string.Empty;
                    var zipCode = string.Empty;
                    var abbreviation = string.Empty;

                    //if (eventSession.EventLocation.LocationName != null)
                    //{
                    //    locName = eventSession.EventLocation.LocationName;
                    //}
                    if (eventSession.EventLocation.City != null)
                    {
                        locName = eventSession.EventLocation.City;
                    }

                    if (eventSession.EventLocation.ZipCode != null)
                    {
                        zipCode = eventSession.EventLocation.ZipCode;
                    }

                    if (eventSession.EventLocation.LocationState != null
                        && eventSession.EventLocation.LocationState.Abbreviation != null)
                    {
                        abbreviation = eventSession.EventLocation.LocationState.Abbreviation;
                    }
                    if (!string.IsNullOrEmpty(locName))
                    {
                        SetLiteralValue( locationAddressCityStateZip, string.Format("{0}, {1} {2}", locName, abbreviation, zipCode));
                    }
                }
            }
        }

        /// <summary>
        ///     The set session main values.
        /// </summary>
        /// <param name="eventSession">
        ///     The event session.
        /// </param>
        private void SetSessionMainValues(EventSession eventSession)
        {
            var eventTopicHeadline = (Literal)this.FindControl("eventTopicHeadline");
            var sessionDates = (Literal)this.FindControl("sessionDateTime");

            var pnlEventTopicHeadline = (Panel)this.FindControl("pnlEventTopicHeadline");
            var pnlSessionDate = (Panel)this.FindControl("pnlSessionDateTime");

            var ongoing = eventSession.IsOngoing
                              ? CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventRoster.OngoingEventText")
                              : string.Empty;

            if (pnlEventTopicHeadline != null)
            {
                var parentEvent = ItemHelper.GetFirstAncestorByTemplate(eventSession, "Event Topic");
                var topicHeadline = FieldRenderer.Render(parentEvent, ItemMapper.EventTopic.FieldName.Headline);
                SetLiteralValue(eventTopicHeadline, topicHeadline);
            }

            if (pnlSessionDate != null)
            {
                if (eventSession.SessionStartDateTime != null && eventSession.SessionEndDateTime != null)
                {
                    SetLiteralValue(
                        sessionDates,
                        ongoing.IsNullOrEmpty()
                            ? string.Format(
                                "{0} - {1} {2} - {3}",
                                eventSession.SessionStartDateTime.Value.ToShortDateString(),
                                eventSession.SessionEndDateTime.Value.ToShortDateString(),
                                eventSession.SessionStartDateTime.Value.ToShortTimeString(),
                                eventSession.SessionEndDateTime.Value.ToShortTimeString())
                            : ongoing);
                }
            }
        }

        #endregion
    }
}