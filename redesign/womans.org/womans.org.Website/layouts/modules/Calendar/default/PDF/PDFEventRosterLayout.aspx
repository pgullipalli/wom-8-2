﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" %>
<%@ OutputCache Location="None" VaryByParam="none" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<html>
<head>
    <title>Session Roster</title>
    <link href="/assets/PDF/PDFCalendarStyle.css" rel="stylesheet" />
    <sc:VisitorIdentification runat="server" />
</head>
<body>
    <form method="post" runat="server" id="mainform">

        <div class="wrapper">
            <sc:placeholder Key="Main" runat="server" />
        </div>
    </form>
</body>
</html>
