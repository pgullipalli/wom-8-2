﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="EventTopicOfferDates.ascx.cs"
            Inherits="womans.org.Website.layouts.modules.Calendar.EventTopicOfferDates" CompilerOptions="/d:BASIC"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<asp:Panel ID="pnlOfferDates" runat="server" CssClass="module-ce-offerdates listing twelve columns grid">
    <asp:PlaceHolder ID="phOfferDatesHeader" runat="server" Visible="False">
        <h3>
            <asp:Literal ID="litOfferDatesHeader" runat="server" />
        </h3>
    </asp:PlaceHolder>
    <asp:ListView ID="lvOfferDates" runat="server" OnItemDataBound="lvOfferDatess_OnItemDataBound" OnItemCommand="lbRegister_OnItemCommand">
        <ItemTemplate>
            <asp:Panel ID="pnlItem" runat="server" class="module-ce-offerdates-item list-item twelve columns grid highlight">
                <asp:Panel ID="pnlLeftContent" runat="server" CssClass="event-lpanel six columns grid">
                    <asp:Panel ID="pnlDate" runat="server" Visible="false" CssClass="module-date twelve columns">
                        <asp:Literal runat="server" ID="litSessionDateTime"></asp:Literal>
                    </asp:Panel>
                    <asp:Panel ID="pnlAttendingDateInfo" runat="server" Visible="false" CssClass="event-attendinginfo twelve columns">
                        <asp:Literal ID="litAttendingDateInfo" runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="pnlLocation" runat="server" Visible="False" CssClass="cal-location-name twelve columns">
                        <asp:Panel ID="pnlLocationName" runat="server" Visible="false">
                            <asp:Literal ID="litLocationName" runat="server" Visible = "false" />
                            <sc:Link ID="scLocationLink" field="Location Link" runat="server" Visible="false"/>
                        </asp:Panel>
                        <asp:Panel ID="pnlLocationAddress" runat="server" Visible="false">
                            <asp:Literal ID="litLocationAddress" runat="server" />
                        </asp:Panel>
                        <asp:PlaceHolder runat="server" Visible="False" ID="hideByRequest">
                            <asp:Panel ID="pnlMapsAndDirection" runat="server" Visible="false">
                                <asp:HyperLink ID="hlMapsAndDirection" runat="server" Target="_blank" Visible="false" />
                            </asp:Panel>
                        </asp:PlaceHolder>
                    </asp:Panel>
                    <asp:Panel ID="pnlAddToCalendar" runat="server" CssClass="module-ce-addthisevent twelve columns" Visible = "false" >
                        <a id="anAddToCalendar" runat="server" href="" title="Add to Calendar" class="addthisevent">
                            <asp:Literal runat="server" ID="ltAddToCalendar"></asp:Literal>
                            <span class="_start" runat="server" id="spnEventStartDate"></span>
                            <span class="_end" runat="server" id="spnEventEndDate"></span>
                            <span class="_zonecode" runat="server" id="spnZoneCode"></span> 
                            <span class="_summary" runat="server" id="spnEventSummary"></span>
                            <span class="_description" runat="server" id="spnEventDescription"></span>
                            <span class="_location" runat="server" id="spnEventLocation"></span>
                            <span class="_organizer" runat="server" id="spnOrganizer"></span>
                            <span class="_organizer_email" runat="server" id="spnOrganizerEmail"></span>
                            <span class="_all_day_event" runat="server" id="spnAllDayEvent"></span> 
                            <span class="_date_format">MM/DD/YYYY</span> 
                        </a>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlRightContent" runat="server" CssClass="event-rpanel six columns grid">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlErrorMessage" runat="server" Visible="false" CssClass="system-message twelve columns centered">
                                <asp:Literal ID="litErrorMessage" runat="server"  />
                            </asp:Panel>
                            <asp:Panel ID="pnlRegister" runat="server" Visible="false" CssClass="event-register twelve columns">
                                <asp:Panel runat="server" ID="pnlRegisterButton" class="event-button" Visible="false">
                                    <asp:HyperLink ID="hlRegister" runat="server" CssClass="button" Visible="false" />
                                    <asp:LinkButton ID="lbRegister" runat="server" CssClass="button" Visible="false" /> 
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlRegisterPaymentType" Visible="false" CssClass="twelve columns">
                                    <div class="event-payment-method">
                                        <asp:DropDownList ID="ddlPaymentMethod" runat="server" OnSelectedIndexChanged="ddlPaymentMethod_SelectedIndexChanged" AutoPostBack="true" EnableViewState="true" />
                                    </div>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlAddRemove" class="event-button" Visible="false">
                                    <asp:LinkButton ID="lbAddRemove" runat="server" CssClass="button pink" /> 
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlCapacity" CssClass="event-availability" Visible="false">
                                    <asp:Literal ID="litCapacityInfo" runat="server" />
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlAdditionalInfo" Visible="false" CssClass="main-content grid" style="text-align: left" >
                                    <iframe runat="server" ID="ifrCustomForm" scrolling="no" seamless="seamless" />
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:Panel runat="server" ID="pnlInstructors" Visible="false" CssClass="module-ce-instructors-listing twelve columns grid">
                        <asp:Panel ID="pnlInstructorsTitle" runat="server" >
                            <h5><%= this.DictionaryLabelInstructors %></h5>
                        </asp:Panel>
                        <asp:ListView ID="lvInstructors" runat="server" OnItemDataBound="lvInstructors_OnItemDataBound" >
                            <ItemTemplate>
                                <asp:Panel runat="server" ID="pnlInstructorName" Visible="false" CssClass="module-instructor twelve columns">
                                    <asp:Literal ID="litInstructorNameOnly" runat="server" />
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlInstructorInfo" Visible="false" CssClass="toggle-hide-show twelve columns">
                                    <a href="javascript:void(0);">
                                        <span>+</span>&nbsp;<asp:Literal ID="litInstructorName" runat="server" />
                                    </a>
                                </asp:Panel>
                                <asp:Panel ID="pnlResult" runat="server" CssClass="instructor-item hide-onload twelve columns" >
                                    <asp:Panel ID="pnlThumbnail" runat="server" Visible="false" CssClass="thumbnail">
                                        <sc:Image ID="scThumbnail" Field="Photo" runat="server" MaxWidth="50" MaxHeight="50" />
                                    </asp:Panel>
                                    <div class="results">
                                        <asp:Panel ID="pnlTitle" runat="server" Visible="false">
                                            <asp:Literal ID="litTitle" runat="server" />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlDescription" runat="server" Visible="false">
                                            <asp:Literal ID="litDescription" runat="server" />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlPhone" runat="server" Visible="false">
                                            <asp:Literal ID="litPhone" runat="server" />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlEmail" runat="server" Visible="false">
                                            <asp:HyperLink ID="hlEmail" runat="server" />
                                        </asp:Panel>
                                    </div>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:ListView>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </ItemTemplate>
    </asp:ListView>
</asp:Panel>
<!-- AddThisEvent -->
<script type="text/javascript" src="http://js.addthisevent.com/atemay.js"> </script>
<!-- AddThisEvent Settings -->
<script type="text/javascript">
    addthisevent.settings({
        license: "aao8iuet5zp9iqw5sm9z",
        mouse: false,
        css: true,
        outlook: { show: true, text: "Outlook Calendar" },
        google: { show: true, text: "Google Calendar" },
        yahoo: { show: true, text: "Yahoo Calendar" },
        hotmail: { show: true, text: "Hotmail Calendar" },
        ical: { show: true, text: "iCal Calendar" },
        facebook: { show: true, text: "Facebook Event" },
        dropdown: { order: "google,ical,outlook" },
        callback: ""
    });
</script>