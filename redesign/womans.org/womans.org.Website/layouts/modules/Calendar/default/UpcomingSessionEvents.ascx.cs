﻿


namespace womans.org.Website.layouts.modules.Calendar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;

    using global::womans.org.BusinessLogic.Calendar;

    using MedTouch.Calendar.Helpers;
    using MedTouch.Calendar.Search;
    using MedTouch.Common.Helpers;
    using MedTouch.Common.UI;
    using Sitecore.Data.Items;

    public partial class UpcomingSessionEvents : BaseSublayout
    {
        #region Fields
        // Default Value of UpcomingEvent Control.
        private const int DefaultNumberOfDisplayPages = 1;
        private const string DefaultCurrentPage = "1";

        private int? renderingParamMaxTeaserCharacters;
        private int? renderingParamNumberOfDisplayedItems;
        private bool? renderingParamRecursiveTaxonomy;
        private bool? renderingParamShowThumbnail;
        private string renderingParamDateFormat;
        private bool? renderingParamShowTitleAsLink;
        private bool? renderingParamShowLinkToDetailPage;
        private bool? renderingParamSuppressWhenNothingMatches;
        private bool? renderingParamIncludeOngoing;
        private Item renderingParamViewAllLink;
        #endregion

        #region Dictionaries
        protected readonly string DictionaryUpcomingEventsLabel = CultureHelper.GetDictionaryTranslation("Modules.Calendar.UpcomingSessionEvents.UpcomingEventsLabel");
        protected readonly string DictionaryReadMoreLink = CultureHelper.GetDictionaryTranslation("Modules.Calendar.UpcomingSessionEvents.ReadMoreLink");
        protected readonly string DictionaryViewAll = CultureHelper.GetDictionaryTranslation("Modules.Calendar.UpcomingSessionEvents.ViewAll");
        protected readonly string DictionaryRegisterLink = CultureHelper.GetDictionaryTranslation("Modules.Calendar.UpcomingSessionEvents.RegisterLink");
        protected readonly string DictionaryViewOngoing = CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventSessionListing.ViewOngoing");
        #endregion Dictionaries

        #region RenderingParameters

        protected int RenderingParamNumberOfDisplayedItems
        {
            get
            {
                if (!this.renderingParamNumberOfDisplayedItems.HasValue)
                {
                    this.renderingParamNumberOfDisplayedItems = this.GetIntProperty("Number of Displayed Items");
                }

                return this.renderingParamNumberOfDisplayedItems.GetValueOrDefault(1);
            }
        }

        protected int RenderingParamMaxTeaserCharacters
        {
            get
            {
                if (!this.renderingParamMaxTeaserCharacters.HasValue)
                {
                    this.renderingParamMaxTeaserCharacters = this.GetIntProperty("Max Teaser Characters");
                }

                return this.renderingParamMaxTeaserCharacters.GetValueOrDefault(100);
            }
        }

        protected bool RenderingParamShowTitleAsLink
        {
            get
            {
                if (!this.renderingParamShowTitleAsLink.HasValue)
                {
                    this.renderingParamShowTitleAsLink = this.GetBooleanProperty("Show Title as Link");
                }

                return this.renderingParamShowTitleAsLink.GetValueOrDefault(true);
            }
        }

        protected bool RenderingParamShowLinkToDetailPage
        {
            get
            {
                if (!this.renderingParamShowLinkToDetailPage.HasValue)
                {
                    this.renderingParamShowLinkToDetailPage = this.GetBooleanProperty("Show Link to Detail Page");
                }

                return this.renderingParamShowLinkToDetailPage.GetValueOrDefault(true);
            }
        }

        protected bool RenderingParamRecursiveTaxonomy
        {
            get
            {
                if (!this.renderingParamRecursiveTaxonomy.HasValue)
                {
                    this.renderingParamRecursiveTaxonomy = this.GetBooleanProperty("RecursiveTaxonomy");
                }

                return this.renderingParamRecursiveTaxonomy.GetValueOrDefault(true);
            }
        }

        protected bool RenderingParamShowThumbnail
        {
            get
            {
                if (!this.renderingParamShowThumbnail.HasValue)
                {
                    this.renderingParamShowThumbnail = this.GetBooleanProperty("ShowThumbnail");
                }

                return this.renderingParamShowThumbnail.GetValueOrDefault(false);
            }
        }

        protected string RenderingParamDateFormat
        {
            get
            {
                if (string.IsNullOrEmpty(this.renderingParamDateFormat))
                {
                    this.renderingParamDateFormat = this.GetProperty("DateFormat") ?? "";
                }

                return this.renderingParamDateFormat;
            }
        }

        protected bool RenderingParamSuppressWhenNothingMatches
        {
            get
            {
                if (!this.renderingParamSuppressWhenNothingMatches.HasValue)
                {
                    this.renderingParamSuppressWhenNothingMatches = this.GetBooleanProperty("SuppressWhenNothingMatches");
                }

                return this.renderingParamSuppressWhenNothingMatches.GetValueOrDefault(true);
            }
        }

        protected bool RenderingParamIncludeOngoing {
            get {
                if (!this.renderingParamIncludeOngoing.HasValue) {
                    this.renderingParamIncludeOngoing = this.GetBooleanProperty("IncludeOngoing");
                }

                return this.renderingParamIncludeOngoing.GetValueOrDefault(true);
            }
        }

        protected Item RenderingParamViewAllLink
        {
            get
            {
                if (this.renderingParamViewAllLink == null)
                {
                    this.renderingParamViewAllLink = this.GetSingleReferenceProperty("ViewAllLink");
                }

                return this.renderingParamViewAllLink;
            }
        }
        #endregion Properties

        protected void Page_Load(object sender, EventArgs e)
        {
            #if (!DEBUG)
            try
            {
            #endif
                Initialize();
            #if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
            #endif
        }

        protected override void Bind()
        {
            if (ContextItem == null)
            {
                return;
            }

            string taxonomyItems = ItemHelper.GetShortIds(ContextItem, "Taxonomy Terms");
            string taxonomy = SitecoreSearchHelper.GetTagTaxonomyGuidsFromShortIds(taxonomyItems, this.RenderingParamRecursiveTaxonomy);

            string specialtyItems = ItemHelper.GetFieldRawValue(ContextItem, "Specialties");
            string locationItems = ItemHelper.GetFieldRawValue(ContextItem, "Locations");

            var searchResultsItem = ModuleHelper.GetSearchResultPageItem(RenderingParamViewAllLink);

            // Types of events to list: Dated (default) or Ongoing
            EventListType eventListType =
                Request.QueryString[ModuleHelper.Config.QueryString.GetEventTypeQuerystring()] == "Ongoing"
                    ? EventListType.Ongoing
                    : EventListType.Dated;

            //https://medtouch.attask-ondemand.com/task/view?ID=53ff325800af32ab990c261135be2eaf
            //The ongoing events shouldn't show on the home page or any "upcoming" events. 

            // Search Param
            var eventSearchParam = new CalendarSearchParam()
                {
                    Keyword = string.Empty,
                    Category = string.Empty,
                    DateFrom = DateTime.Now.ToString(ModuleHelper.Config.DefaultDateFormat()),
                    DateTo = string.Empty,
                    SearchType = CalendarSearchType.EventSession,
                    ListType = eventListType,
                    //IncludeOngoing = RenderingParamIncludeOngoing,
                    IncludeOngoing = false,
                    Locations = locationItems,
                    Specialties = specialtyItems,
                    Taxonomy = taxonomy,
                    IsTaxonomyRecursive = this.RenderingParamRecursiveTaxonomy
                };

            List<Item> items = new List<Item>();
            bool displayAllContent = false;

            int totalCount = 0;
            int startItem = 0;
            int endItem = 0;

            // Search
            WomansModuleHelper.CalendarSearch(
                eventSearchParam,
                DefaultCurrentPage,
                RenderingParamNumberOfDisplayedItems,
                DefaultNumberOfDisplayPages,
                true,
                out startItem,
                out endItem,
                out totalCount,
                out items);

            if (!items.Any() && !RenderingParamSuppressWhenNothingMatches)
            {
                // Whet not suppress, return all Event Items
                eventSearchParam.Locations = string.Empty;
                eventSearchParam.Specialties = string.Empty;
                eventSearchParam.Taxonomy = string.Empty;

                // Search
                WomansModuleHelper.CalendarSearch(
                    eventSearchParam,
                    DefaultCurrentPage,
                    RenderingParamNumberOfDisplayedItems,
                    DefaultNumberOfDisplayPages,
                    true,
                    out startItem,
                    out endItem,
                    out totalCount,
                    out items);

                displayAllContent = true;
            }

            if (items.Any())
            {
                if (searchResultsItem != null)
                {

                    // Setup base "view all" link
                    string strSpecialties = ItemHelper.GetItemNamesFromItemGuids(specialtyItems);
                    string strLocations = ItemHelper.GetItemNamesFromItemGuids(locationItems);
                    string viewAllUrl = string.Empty;
                    if (displayAllContent)
                    {
                        viewAllUrl = BaseModuleHelper.GetSearchResultsUrl(
                            string.Empty, 
                            string.Empty, 
                            string.Empty, 
                            false, 
                            searchResultsItem);
                    }
                    else
                    {
                        viewAllUrl = BaseModuleHelper.GetSearchResultsUrl(
                            strSpecialties,
                            strLocations,
                            taxonomyItems,
                            RenderingParamRecursiveTaxonomy,
                            searchResultsItem);
                    }

                    // "View All" link only if all items aren't displayed
                    if (totalCount > RenderingParamNumberOfDisplayedItems)
                    {
                        pnlViewAll.Visible = true;
                        hlViewAll.NavigateUrl = viewAllUrl;
                        hlViewAll.Text = DictionaryViewAll;
                        hlViewAll.Visible = true;
                    }

                    // "View Ongoing" link if there are any ongoing items that meet criteria
                    // First: find out if there ARE any ongoing items to show in the list before showing the View Ongoing link
                    eventSearchParam.ListType = EventListType.Ongoing;
                    CalendarSearcher calendarSearcher = new CalendarSearcher();
                    var ongoingItems = calendarSearcher.ExecuteSearch(eventSearchParam);
                    if (ongoingItems.Count > 0)
                    {
                        pnlViewAll.Visible = true;
                        hlViewOngoing.NavigateUrl = viewAllUrl + (viewAllUrl.Contains("?") ? "&" : "?")
                                                    + ModuleHelper.Config.QueryString.GetEventTypeQuerystring()
                                                    + "=Ongoing";
                        hlViewOngoing.Text = DictionaryViewOngoing;
                        hlViewOngoing.Visible = true;
                    }
                }
            }
            else
            {
                phUpcomingEvents.Visible = false;
            }


            // Bind Label
            lblUpcomingEventsHeader.Text = DictionaryUpcomingEventsLabel;

            // Bind UpcomingEvent
            lvSearchResults.DataSource = items;
            lvSearchResults.DataBind();
        }

        protected void lvSearchResults_OnItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            Item item = e.Item.DataItem as Item;
            if (item == null) return;

            Item eventTopicItem = ModuleHelper.GetEventTopicFromEventSession(item);
            string strEventTopicLink = ModuleHelper.GetEventTopicLinkUrl(eventTopicItem, item);

            // Title
            HyperLink hlItem = e.Item.FindControl("hlItem") as HyperLink;
            Literal litItem = e.Item.FindControl("litItem") as Literal;
            string strHeadline = ItemHelper.GetFieldHtmlValue(eventTopicItem, ItemMapper.EventTopic.FieldName.Headline);
            if (hlItem != null && this.RenderingParamShowTitleAsLink)
            {
                hlItem.Text = strHeadline;
                hlItem.NavigateUrl = strEventTopicLink;
                if (litItem != null) litItem.Visible = false;
            }
            else if (litItem != null)
            {
                litItem.Text = strHeadline;
                if (hlItem != null) hlItem.Visible = false;
            }

            // Thumbnail from Event Topic
            Panel pnlThumbnail = e.Item.FindControl("pnlThumbnail") as Panel;
            Sitecore.Web.UI.WebControls.Image scThumbnail = e.Item.FindControl("scThumbnail") as Sitecore.Web.UI.WebControls.Image;
            string strImage = ItemHelper.GetFieldRawValue(eventTopicItem, ItemMapper.EventTopic.FieldName.EventThumbnail);
            if (pnlThumbnail != null && scThumbnail != null && this.RenderingParamShowThumbnail &&
               !string.IsNullOrEmpty(strImage))
            {
                scThumbnail.Item = eventTopicItem;
                pnlThumbnail.Visible = true;
            }

            // Teaser
            Panel pnlTeaser = (Panel)e.Item.FindControl("pnlTeaser");
            Literal litTeaser = e.Item.FindControl("litTeaser") as Literal;
            string strTeaser = ItemHelper.GetItemTeaser(eventTopicItem, this.RenderingParamMaxTeaserCharacters);
            if (pnlTeaser != null && litTeaser != null && !string.IsNullOrEmpty(strTeaser))
            {
                pnlTeaser.Visible = true;
                litTeaser.Text = strTeaser;
            }

            // Date
            var pnlDate = e.Item.FindControl("pnlDate") as Panel;
            var litSessionDateTime = e.Item.FindControl("litSessionDateTime") as Literal;
            litSessionDateTime.Text = RegistrationHelper.GetSessionDateTimeString(item, true, RenderingParamDateFormat);
            pnlDate.Visible = (!string.IsNullOrEmpty(litSessionDateTime.Text));

            // Read More
            Panel pnlReadMore = e.Item.FindControl("pnlReadMore") as Panel;
            HyperLink hlMoreLink = e.Item.FindControl("hlMoreLink") as HyperLink;
            if (hlMoreLink != null && pnlReadMore != null && this.RenderingParamShowLinkToDetailPage)
            {
                hlMoreLink.Text = this.DictionaryReadMoreLink;
                hlMoreLink.NavigateUrl = strEventTopicLink;
                hlMoreLink.Visible = true;
                pnlReadMore.Visible = true;
            }
        }
    }
}