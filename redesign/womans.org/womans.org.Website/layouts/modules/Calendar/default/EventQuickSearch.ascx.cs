﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MedTouch.Calendar.layouts.modules.Calendar;
using MedTouch.Common.Helpers;

namespace womans.org.Website.layouts.modules.Calendar
{
    public partial class EventQuickSearch : EventSearch
	{
        protected readonly string DictionaryEventQuickSearchLabel = CultureHelper.GetDictionaryTranslation("Modules.Calendar.EventSearch.EventQuickSearchLabelNew");

        protected override void Initialize()
        {
            base.Initialize();
            lblEventSearch.Text = DictionaryEventQuickSearchLabel;
            phControlHeader.Visible = (!string.IsNullOrWhiteSpace(DictionaryEventQuickSearchLabel));
        }
	}
}