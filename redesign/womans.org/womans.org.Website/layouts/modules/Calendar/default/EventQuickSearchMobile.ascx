﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="EventQuickSearch.ascx.cs"
    Inherits="womans.org.Website.layouts.modules.Calendar.EventQuickSearch" %>
<!--==================== Womans: Calendar: Event Quick Search =============================-->

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSubmit" CssClass="module-ce-search core-search left-mobile-search">
<div class="grid reg-callout">
        <asp:PlaceHolder runat="server" ID="phControlHeader">
            <h3 class="module-header"><asp:Label ID="lblEventSearch" runat="server" /></h3>
        </asp:PlaceHolder>
        <asp:Panel ID="pnlKeyword" runat="server" CssClass="twelve columns">
            <asp:Label ID="lblKeyword" runat="server" AssociatedControlID="txtKeyword" CssClass="label" />
            <asp:TextBox ID="txtKeyword" runat="server" CssClass="textbox" />
        </asp:Panel>
        <asp:Panel ID="pnlCategory" runat="server" CssClass="twelve columns">
            <asp:Label ID="lblCategory" runat="server" AssociatedControlID="ddlCategory" CssClass="label" />
			<div class="selectbox">  
                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="selectboxdiv" />
				<div class="out"></div>
			</div>			
        </asp:Panel>
        <asp:PlaceHolder runat="server" Visible="False" ID="hideByRequest">
            <asp:Panel ID="pnlInstructor" runat="server" CssClass="twelve columns">
                <asp:Label ID="lblInstructor" runat="server" AssociatedControlID="ddlInstructor" CssClass="label" />
			    <div class="selectbox">  
                    <asp:DropDownList ID="ddlInstructor" runat="server" CssClass="selectboxdiv" />
				    <div class="out"></div>
			    </div>			
            </asp:Panel>
        </asp:PlaceHolder>
        <asp:Panel ID="pnlDate" runat="server" CssClass="twelve columns grid">
            <asp:Label ID="lblDate" runat="server" AssociatedControlID="txtDateFrom" CssClass="label" />
			<label class="noshow">Date From</label>
            <asp:TextBox ID="txtDateFrom" type="date" runat="server" CssClass="dp_input twelve columns date-from" />
			<label class="noshow">Date To</label>
            <asp:TextBox ID="txtDateTo" type="date" runat="server" CssClass="dp_input twelve columns date-to" />
        </asp:Panel>
        <asp:Panel ID="pnlError" runat="server" Visible="false" CssClass="twelve columns errortext">
            <asp:Literal ID="litError" runat="server" />
        </asp:Panel>
        <asp:Panel ID="pnlSubmit" runat="server" CssClass="twelve columns">
            <%--<input type="button" id="btnReset" type="button" value="reset" class="button reset-button"/>--%>
            <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" ValidationGroup="vldGrpEventSearch" CssClass="button pink default_button"/>
            <asp:ImageButton ID="ibSubmit" runat="server" OnClick="ibSubmit_Click" Visible="false" ValidationGroup="vldGrpEventSearch" />
            <asp:LinkButton ID="lbSubmit" runat="server" Visible="false" OnClick="lbSubmit_Click" ValidationGroup="vldGrpEventSearch" />
            <asp:Button ID="btnReset" runat="server" CssClass="button reset-button default_button" />
        </asp:Panel>
    </div>
</asp:Panel>
<!--==================== /Womans: Calendar: Event Quick Search =============================-->