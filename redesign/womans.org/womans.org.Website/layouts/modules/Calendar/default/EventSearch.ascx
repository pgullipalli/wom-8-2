﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventSearch.ascx.cs"
            Inherits="MedTouch.Calendar.layouts.modules.Calendar.EventSearch" %>
<!--==================== Module: Calendar: Event Search =============================-->
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSubmit" CssClass="module-ce-search core-search">
    <div class="grid">
        <asp:PlaceHolder runat="server" ID="hideTitle" Visible="False">
            <asp:PlaceHolder runat="server" ID="phControlHeader">
                <h4><asp:Label ID="lblEventSearch" runat="server" /></h4>
            </asp:PlaceHolder>
        </asp:PlaceHolder>
        <asp:Panel ID="pnlKeyword" runat="server" CssClass="six columns">
            <asp:Label ID="lblKeyword" runat="server" AssociatedControlID="txtKeyword" CssClass="label"/>
            <asp:TextBox ID="txtKeyword" runat="server" CssClass="textbox" />
        </asp:Panel>
        <asp:Panel ID="pnlCategory" runat="server" CssClass="six columns">
            <asp:Label ID="lblCategory" runat="server" AssociatedControlID="ddlCategory" CssClass="label" />
            <div class="selectbox">  
                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="selectboxdiv" />
                <div class="out"></div>
            </div>
        </asp:Panel>
        <asp:PlaceHolder runat="server" ID="pnlHideUnUsed" Visible="False">
            <asp:PlaceHolder runat="server" Visible="False" ID="hideByRequest">
                <asp:Panel ID="pnlInstructor" runat="server" CssClass="six columns">
                    <asp:Label ID="lblInstructor" runat="server" AssociatedControlID="ddlInstructor" CssClass="label" />
                    <div class="selectbox">  
                        <asp:DropDownList ID="ddlInstructor" runat="server" CssClass="selectboxdiv" />
                        <div class="out"></div>
                    </div>
                </asp:Panel>
            </asp:PlaceHolder>
        </asp:PlaceHolder>
        <asp:Panel ID="pnlDate" runat="server" CssClass="six columns">
            <asp:Label ID="lblDate" runat="server" AssociatedControlID="txtDateFrom" CssClass="label" />
            <div class="grid">
                <asp:TextBox ID="txtDateFrom" type="date" runat="server" AutoCompleteType="None" CssClass="dp_input six columns textbox" />
                <asp:TextBox ID="txtDateTo" type="date" runat="server" AutoCompleteType="None" CssClass="dp_input six columns textbox" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlError" runat="server" Visible="false" CssClass="twelve columns errortext">
            <asp:Literal ID="litError" runat="server" />
        </asp:Panel>
        <div class="twelve columns search-option-submit">
            <!-- RESET button here for EventQuickSearch.ascx, which is using this sublayouts backend code -->
            <asp:Button ID="btnReset" runat="server" ValidationGroup="vldGrpEventSearch" CssClass="button" Visible = "false"/>
            <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" ValidationGroup="vldGrpEventSearch" CssClass="default_button pink button"/>
            <asp:ImageButton ID="ibSubmit" runat="server" OnClick="ibSubmit_Click" Visible="false" ValidationGroup="vldGrpEventSearch" />
            <asp:LinkButton ID="lbSubmit" runat="server" Visible="false" OnClick="lbSubmit_Click" ValidationGroup="vldGrpEventSearch" />
        </div>
    </div>
</asp:Panel>
<!--==================== /Module: Calendar: Event Search =============================-->