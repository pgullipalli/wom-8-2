﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventDayPicker.ascx.cs"
    Inherits="MedTouch.Calendar.layouts.modules.Calendar.EventDayPicker" %>
<!--==================== Module: Calendar: Event Day Picker =============================-->

<asp:Panel id="pnlEventDayPicker" runat="server" CssClass="module-ce-eventdaypicker core-daypicker collapse-for-mobile" >
	<div class="reg-callout grid">
	    <!-- TO-DO - get this from dictionary -->
		<h3 class="module-heading">Browse by Date</h3>	
	    <div class="module-ce-daypicker twelve columns">
            <!-- Do Not Remove These Hidden Fields -->
            <asp:HiddenField ID="hdnResultPageURL" ClientIDMode="Static" runat="server" Value="" />
            <asp:HiddenField ID="hdnEventsDays" ClientIDMode="Static" runat="server" Value="" />
        </div>
    </div>
</asp:Panel>
<!--==================== /Module: Calendar: Event Day Picker =============================-->