﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventTopicListing.ascx.cs"
    Inherits="MedTouch.Calendar.layouts.modules.Calendar.EventTopicListing" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="MedTouch" TagName="PaginationList" Src="/layouts/modules/Calendar/default/CalendarPagination.ascx" %>
<!--==================== Module: Calendar: Event Topic Listing =============================-->
<asp:Panel ID="pnlResults" runat="server" CssClass="module-ce-topic-results module-ce-results core-results">
    <MedTouch:PaginationList ID="ucPagingTop" runat="server" DisplayInfo="true" />
    <asp:Panel ID="pnlViewOngoing" runat="server" Visible="false" CssClass="module-ce-view-ongoing core-view-ongoing twelve columns">
        <asp:HyperLink ID="hlViewOngoing" runat="server" />
    </asp:Panel>
    <asp:Panel ID="pnlListing" runat="server" CssClass="grid">
		<div class="listing-wrap twelve columns">
            <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_ItemDataBound">
                <LayoutTemplate>
                    <ul class="core-list">
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <li class="core-li grid">
                        <h5 class="list-item-header">
                            <asp:HyperLink ID="hlItem" runat="server" />
                            <asp:Literal ID="litItem" runat="server" />
                        </h5>
                        <asp:Panel ID="pnlImage" runat="server" CssClass="list-item-image" Visible="false">
                            <asp:HyperLink ID="hlImage" runat="server">
                                <!-- Leave these at max. pixels thumbnail can ever be (i.e, in the main content panel) 
                                CSS will resize smaller if needed -->
                                <sc:Image ID="scImage" runat="server" MaxWidth="100" MaxHeight="100" Field="Event Thumbnail" />
                            </asp:HyperLink>
                        </asp:Panel>
			            <div class="list-item-copy">
                            <asp:Panel ID="pnlTeaser" runat="server" CssClass="list-item-teaser" Visible="false" >
                                <asp:Literal ID="litTeaser" runat="server" />
                            </asp:Panel>
                            <asp:Panel ID="pnlReadMore" runat="server" CssClass="list-item-link" Visible="false">
                                <asp:HyperLink ID="hlMoreLink" runat="server" />
                            </asp:Panel>
                        </div>
                    </li>
                </ItemTemplate>
                <EmptyDataTemplate>
                    <div class="search-no-results">
                        <%=DictionaryNoResultsMessage%>
                    </div>
                </EmptyDataTemplate>
            </asp:ListView>
        </div>
    </asp:Panel>
    <MedTouch:PaginationList ID="ucPagingBottom" runat="server" DisplayInfo="true" />
</asp:Panel>
<!--==================== /Module: Calendar: Event Topic Listing =============================-->