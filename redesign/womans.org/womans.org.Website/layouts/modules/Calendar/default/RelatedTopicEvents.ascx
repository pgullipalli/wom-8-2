﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RelatedTopicEvents.ascx.cs"
    Inherits="MedTouch.Calendar.layouts.modules.Calendar.RelatedTopicEvents" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: Calendar: Related Topic Events =============================-->
<asp:Panel runat="server" ID="phRelatedEvents" CssClass="module-ce-topic-related module-ce-related collapse-for-mobile">
    <div class="grid reg-callout">
        <asp:PlaceHolder ID="phRelatedEventsHeader" runat="server">
            <h3 class="module-heading"><asp:Label ID="lblRelatedEventsHeader" runat="server" /></h3>
        </asp:PlaceHolder>
        <asp:Panel ID="pnlListing" runat="server" CssClass="listing-wrap twelve columns">
            <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_OnItemDataBound">
                <LayoutTemplate>
                    <ul class="core-list">
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <li class="core-li grid">
                        <h5 class="list-item-header">
                            <asp:HyperLink ID="hlItem" runat="server" />
                            <asp:Literal ID="litItem" runat="server" />
                        </h5>
                        <asp:Panel ID="pnlThumbnail" runat="server" CssClass="list-item-image" Visible="false">
                            <!-- Leave these at max. pixels thumbnail can ever be (i.e, in the main content panel) 
                            CSS will resize smaller if needed -->
                            <sc:image id="scThumbnail" field="Event Thumbnail" maxheight="100" maxwidth="100" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="pnlResult" runat="server" CssClass="list-item-copy">
                            <asp:Panel ID="pnlTeaser" runat="server" Visible="false" CssClass="list-item-teaser">
                                <asp:Literal ID="litTeaser" runat="server" />
                            </asp:Panel>
                            <asp:Panel ID="pnlMoreLink" runat="server" Visible="false"  CssClass="list-item-links">
                                <asp:HyperLink ID="hlMoreLink" runat="server" />
                            </asp:Panel>
                        </asp:Panel>
                    </li>
                </ItemTemplate>
            </asp:ListView>
        </asp:Panel>
        <asp:Panel ID="pnlViewAll" runat="server" Visible="false" CssClass="module-ce-view-all module-view-all twelve columns">
            <asp:HyperLink ID="hlViewAll" runat="server" Visible="false" />
            <asp:HyperLink ID="hlViewOngoing" runat="server" Visible="false" />
        </asp:Panel>
    </div>
</asp:Panel>
<!--==================== Module: Calendar: Related Topic Events =============================-->