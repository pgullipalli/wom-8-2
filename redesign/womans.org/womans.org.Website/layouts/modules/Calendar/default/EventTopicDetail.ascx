﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventTopicDetail.ascx.cs"
    Inherits="MedTouch.Calendar.layouts.modules.Calendar.EventTopicDetail" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: Calendar: Event Topic Detail =============================-->
<asp:Panel runat="server" CssClass="module-ce-topic-profile grid">

    <asp:Panel ID="pnlHeadline" runat="server" Visible="false" CssClass="twelve columns">
        <h1><sc:Text ID="scTextHeadline" runat="server" Field="Headline" /></h1>
    </asp:Panel>
    <asp:Panel ID="pnlContentCopy" runat="server" Visible="false" CssClass="twelve columns">
            <sc:Sublayout ID="slPhysicianTabs" Path="/layouts/modules/Calendar/default/EventTopicThumbnail.ascx" runat="server" />
        <sc:Text ID="sctxtContentCopy" runat="server" Field="Content Copy" />
    </asp:Panel>
    <asp:Panel ID="pnlContact" runat="server" Visible="False" CssClass="module-ce-contact twelve columns">
        <h3><asp:Literal ID="litContactLabel" runat="server" /></h3>
        <asp:Panel ID="pnlContactName" runat="server" Visible="false">
            <sc:Text ID="sctxtContactName" runat="server" Field="Event Contact Name" />
        </asp:Panel>
        <asp:Panel ID="pnlContactPhone" runat="server" Visible="false">
            <sc:Text ID="sctxtContactPhone" runat="server" Field="Event Contact Phone" />
        </asp:Panel>
        <asp:Panel ID="pnlContactEmail" runat="server" Visible="false">
            <asp:HyperLink ID="hlContactEmail" runat="server" />
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlClassReviews" runat="server" Visible="False" CssClass="module-ce-reviews twelve columns">
        <h3><asp:Literal ID="litClassReviewsLabel" runat="server" /></h3>
        <sc:Text ID="sctxtClassReviews" runat="server" Field="Class Reviews" />
    </asp:Panel>
    <asp:Panel ID="pnlRegisterInstruction" runat="server" Visible="false" CssClass="module-ce-register twelve columns" >
        <h3><asp:Literal ID="litRegisterInstructionLabel" runat="server" /></h3>
        <sc:Text ID="sctxtRegisterInstruction" runat="server" Field="Registration Instruction" />
    </asp:Panel>
    <asp:Panel ID="pnlFee" runat="server" Visible="false" CssClass="module-ce-fee twelve columns grid">
        <h3><asp:Literal ID="litFeesLabel" runat="server" /></h3>
        <asp:Panel runat="server" ID="pnlFixedFee" CssClass="fixed-fee" Visible = "true" >
            <asp:Literal ID="litFee" runat="server" />
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlFeeType" CssClass="fee-type" Visible="false">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:DropDownList ID="ddlFeeType" runat="server" OnSelectedIndexChanged="ddlFeeType_SelectedIndexChanged" AutoPostBack="true" EnableViewState="true" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </asp:Panel>
    <sc:Sublayout ID="slOfferDates" Path="/layouts/modules/Calendar/default/EventTopicOfferDates.ascx" runat="server" />
</asp:Panel>
<!--==================== /Module: Calendar: Event Topic Detail =============================-->