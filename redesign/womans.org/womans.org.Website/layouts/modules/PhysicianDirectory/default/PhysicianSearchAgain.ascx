﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="womans.org.Website.layouts.modules.PhysicianDirectory.PhysiciansearchagainSublayout"
    CodeBehind="PhysicianSearchAgain.ascx.cs" %>
<%@ Import Namespace="MedTouch.Common.Helpers" %>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
<%--<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/excite-bike/jquery-ui.css" />--%>
<!--==================== Module: Physician Directory: Physician Search Again =============================-->
<script type="text/javascript">
    //Validate Zip and Range - Code need to stay on this page.
    function SearchAgain_ValidateZipAndRange(src, args) {
        var dRange = document.getElementById('<%=ddlRange.ClientID %>');
        var dzip = document.getElementById('<%=txtZip.ClientID %>');
        var watermarkZip = '<%=this.DictionarySearchByZipWaterMark %>';
        if (dRange.selectedIndex > 0 && dzip.value == watermarkZip) {
            args.IsValid = false;
        } else {
            args.IsValid = true;
        }
    }
//    function ResetAllFields() {
//        $("#<%= txtKeyword.ClientID %>").val('');
//        $("#<%= txtFirstName.ClientID %>").val('');
//        $("#<%= txtLastName.ClientID %>").val('');
//        $("#<%= txtNameAutoComplete.ClientID %>").val('');
//        $("#<%= txtZip.ClientID %>").val('');

//        $("#<%= ddlSpecialty.ClientID %>").val('');
//        $("#<%= ddlDepartment.ClientID %>").val('');
//        $("#<%= ddlLanguage.ClientID %>").val('');
//        $("#<%= ddlGender.ClientID %>").val('');
//        $("#<%= ddlCity.ClientID %>").val('');
//        $("#<%= ddlRange.ClientID %>").val('');
//        $("#<%= ddlLocations.ClientID %>").val('');

//        $(".out").empty();
//    }

//    $(document).ready(function () {
//        $("#btnReset").val("<%=DictionaryResetButtonText %>");

//        var termTemplate = "<span class='ui-autocomplete-term'>%s</span>";
//        $("#<%= txtNameAutoComplete.ClientID %>").autocomplete({
//            source: namesList,
//            open: function (e, ui) {
//                var acData = $(this).data('autocomplete');
//                acData
//				.menu
//				.element
//				.find('a')
//				.each(function () {
//				    var me = $(this);
//				    var regex = new RegExp(acData.term, "gi");
//				    me.html(me.text().replace(regex, function (matched) {
//				        return termTemplate.replace('%s', matched);
//				    }));
//				});
//            }
//        }).focus(function (event, ui) {
//            event.preventDefault();
//            if ($(this).val() == '') {
//                $(this).val('');
//            }
//            return false;
//        });

//    });
</script>
<asp:Panel ID="pnlControlWrapper" runat="server" DefaultButton="btnSubmit" CssClass="module-pd-search-again core-search-again collapse-for-mobile"> <!-- CssClass="module-pd-search-again callout  twelve columns grid" -->
    <div class="reg-callout grid">
        <asp:PlaceHolder runat="server" ID="phControlHeader">
            <h3 class="module-heading">
                <asp:Label ID="lblPhysicianSearchAgain" runat="server" /></h3>
        </asp:PlaceHolder>
        <asp:Panel ID="pnlAlpha" runat="server" CssClass="module-alphabet-list-sm">
            <asp:BulletedList ID="blAtoZ" runat="server" DisplayMode="HyperLink" CssClass="module-alphabet-list" />
            <div class="clear">
            </div>
        </asp:Panel>

        
        <script type="text/javascript">
            $(document).ready(function () {

                $("#<%=blAtoZ.ClientID%>").each(function () {
                    $(this).find('li').each(function () {
                        $(this).addClass('one columns');
                    });
                });
            });
        </script>

        <asp:Panel ID="pnlKeyword" runat="server" CssClass="search-option" Visible="false">
            <asp:Label ID="lblKeyword" runat="server" AssociatedControlID="txtKeyword" CssClass="label" />
            <asp:TextBox ID="txtKeyword" runat="server" CssClass="textbox" />
        </asp:Panel>
        <asp:Panel ID="pnlNameAutoComplete" runat="server" CssClass="twelve columns">
            <asp:Label ID="lblNameAutoComplete" runat="server" AssociatedControlID="txtNameAutoComplete" CssClass="label" />
            <asp:TextBox ID="txtNameAutoComplete" runat="server" CssClass="textbox autocomplete" />
        </asp:Panel>
        <asp:Panel ID="pnlFirstName" runat="server" CssClass="search-option hide-onload" Visible="false">
            <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName" CssClass="label" />
            <asp:TextBox ID="txtFirstName" runat="server" CssClass="textbox" />
        </asp:Panel>
        <asp:Panel ID="pnlLastName" runat="server" CssClass="search-option" Visible="false">
            <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName" CssClass="label" />
            <asp:TextBox ID="txtLastName" runat="server" CssClass="textbox" />
        </asp:Panel>
        <asp:Panel ID="pnlSpecialty" runat="server" CssClass="twelve columns">
            <asp:Label ID="lblSpecialty" runat="server" AssociatedControlID="ddlSpecialty" CssClass="label" />
            <div class="selectbox">
                <asp:DropDownList ID="ddlSpecialty" runat="server" CssClass="selectboxdiv" DataTextField="Text" DataValueField="Value" />
                <div class="out"></div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDepartment" runat="server" CssClass="search-option hide-onload" Visible="false">
            <asp:Label ID="lblDepartment" runat="server" AssociatedControlID="ddlDepartment"
                CssClass="label" />
            <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="styled" />
        </asp:Panel>
        <asp:Panel ID="pnlLanguage" runat="server" CssClass="search-option hide-onload" Visible="false">
            <asp:Label ID="lblLanguage" runat="server" AssociatedControlID="ddlLanguage" CssClass="label" />
            <asp:DropDownList ID="ddlLanguage" runat="server" CssClass="styled" />
        </asp:Panel>
        <asp:Panel ID="pnlGender" runat="server" CssClass="twelve columns">
            <asp:Label ID="lblGender" runat="server" AssociatedControlID="ddlGender" CssClass="label" />
            <div class="selectbox">
                <asp:DropDownList ID="ddlGender" runat="server" CssClass="selectboxdiv" />
                <div class="out"></div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlCity" runat="server" CssClass="twelve columns">
            <asp:Label ID="lblCity" runat="server" AssociatedControlID="ddlCity" CssClass="label" />
            <div class="selectbox">
                <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectboxdiv" />
                <div class="out"></div>
            </div>
        </asp:Panel>

        



        <asp:Panel ID="pnlLocations" runat="server" CssClass="twelve columns">
            <asp:Label ID="lblLocations" runat="server" AssociatedControlID="ddlLocations"
                CssClass="label" />
            <div class="selectbox">
                <asp:DropDownList ID="ddlLocations" runat="server" CssClass="selectboxdiv" />
                <div class="out"></div>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlZip" runat="server" CssClass="search-option hide-onload" Visible="false">
            <asp:Label ID="lblZip" runat="server" AssociatedControlID="txtZip" CssClass="label" />
            <asp:TextBox ID="txtZip" runat="server" CssClass="textbox" />
            <asp:RegularExpressionValidator ID="regexVldZip" ControlToValidate="txtZip" Display="dynamic"
                runat="server" CssClass="errortext" ValidationGroup="vldGrpSearch" />
            <asp:CustomValidator ID="ctmVldZipRange" runat="server" ClientValidationFunction="Search_ValidateZipAndRange"
                CssClass="errortext" Display="Dynamic" ValidationGroup="vldGrpSearch" />
        </asp:Panel>
        <asp:Panel ID="pnlRange" runat="server" CssClass="search-option hide-onload" Visible="false">
            <asp:Label ID="lblRange" runat="server" AssociatedControlID="ddlRange" CssClass="label" />
            <asp:DropDownList ID="ddlRange" runat="server" CssClass="styled" />
        </asp:Panel>
        <asp:Panel ID="pnlError" runat="server" Visible="false" CssClass="errortext">
            <asp:Literal ID="litError" runat="server" /></asp:Panel>

        <asp:Panel ID="pnlButtons" runat="server" CssClass="twelve columns">
            <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" ValidationGroup="vldGrpSearchAgain"
                CssClass="button default-button pink" />
            <%--<input type="button" id="btnReset" onclick="ResetAllFields();" />--%>
            <input type="reset" name="main_1$leftpanel_0$btnSubmit" value="Reset" class="button reset">	
            <asp:ImageButton ID="ibSubmit" runat="server" OnClick="ibSubmit_Click" Visible="false"
                ValidationGroup="vldGrpSearchAgain" />
            <asp:LinkButton ID="lbSubmit" runat="server" Visible="false" OnClick="lbSubmit_Click"
                ValidationGroup="vldGrpSearchAgain" />
        </asp:Panel>
    </div>
</asp:Panel>
<!--==================== /Module: Physician Directory: Physician Search Again =============================-->
