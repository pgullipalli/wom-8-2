﻿
namespace MedTouch.PhysicianDirectory.layouts.modules.PhysicianDirectory
{
    using System;

    using MedTouch.Common.Helpers;
    using MedTouch.Common.UI;
    using MedTouch.PhysicianDirectory.Helpers;
    using MedTouch.PhysicianDirectory.Helpers.Links;
    using MedTouch.PhysicianDirectory.Search;

    using Sitecore.Data.Items;
    using Sitecore.Links;

    /// <summary>
    /// Summary description for PhysicianbasicsearchSublayout
    /// </summary>
    public partial class PhysicianquicksearchSublayout : BaseSublayout
    {
        #region Fields
        private string dictionarySearchByLastNameLabel;
        private string dictionarySearchByLastNameWatermark;
        private string dictionarySearchBySpecialtyLabel;
        private string dictionaryControlHeaderLabel;
        private string dictionarySearchButtonText;
        private string dictionaryAdvancedSearchLinkText;
        private string dictionaryCriteriaRequired;
        private Item renderingParamSearchResultsPage;
        private Item renderingParamAdvancedSearchPage;
        #endregion Fields

        #region properties

        protected string DictionaryControlHeaderLabel
        {
            get
            {
                if (this.dictionaryControlHeaderLabel == null)
                {
                    this.dictionaryControlHeaderLabel =
                        CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianQuickSearch.PhysicianQuickSearchLabel");
                }

                return this.dictionaryControlHeaderLabel;
            }
        }

        protected string DictionarySearchByLastNameLabel
        {
            get
            {
                if (this.dictionarySearchByLastNameLabel == null)
                {
                    this.dictionarySearchByLastNameLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianQuickSearch.LastNameLabel");
                }

                return this.dictionarySearchByLastNameLabel;
            }
        }

        protected string DictionarySearchByLastNameWatermark
        {
            get
            {
                if (this.dictionarySearchByLastNameWatermark == null)
                {
                    this.dictionarySearchByLastNameWatermark =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianQuickSearch.LastNameWatermark");
                }

                return this.dictionarySearchByLastNameWatermark;
            }
        }

        protected string DictionarySearchBySpecialtyLabel
        {
            get
            {
                if (this.dictionarySearchBySpecialtyLabel == null)
                {
                    this.dictionarySearchBySpecialtyLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianQuickSearch.SpecialtyLabel");
                }

                return this.dictionarySearchBySpecialtyLabel;
            }
        }

        protected string DictionarySearchButtonText
        {
            get
            {
                if (this.dictionarySearchButtonText == null)
                {
                    this.dictionarySearchButtonText =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianQuickSearch.SearchButtonText");
                }

                return this.dictionarySearchButtonText;
            }
        }

        protected string DictionaryAdvancedSearchLinkText
        {
            get
            {
                if (this.dictionaryAdvancedSearchLinkText == null)
                {
                    this.dictionaryAdvancedSearchLinkText =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianQuickSearch.AdvancedSearchLinkText");
                }

                return this.dictionaryAdvancedSearchLinkText;
            }
        }

        protected string DictionaryCriteriaRequired
        {
            get
            {
                if (this.dictionaryCriteriaRequired == null)
                {
                    this.dictionaryCriteriaRequired =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianQuickSearch.CriteriaRequired");
                }

                return this.dictionaryCriteriaRequired;
            }
        }

        protected Item RenderingParamSearchResultsPage
        {
            get
            {
                if (this.renderingParamSearchResultsPage == null)
                {
                    this.renderingParamSearchResultsPage = this.GetSingleReferenceProperty("Search Results Page");
                }

                return this.renderingParamSearchResultsPage;
            }
        }

        protected Item RenderingParamAdvancedSearchPage
        {
            get
            {
                if (this.renderingParamAdvancedSearchPage == null)
                {
                    this.renderingParamAdvancedSearchPage = this.GetSingleReferenceProperty("Advanced Search Page");
                }

                return this.renderingParamAdvancedSearchPage;
            }
        }
        #endregion properties

        protected override void Bind()
        {
            this.lblLastName.Text = this.DictionarySearchByLastNameLabel;
            this.lblLastName.Visible = !string.IsNullOrEmpty(this.DictionarySearchByLastNameLabel);
            UIHelper.SetTextboxWatermark(this.txtLastName, string.Empty, this.DictionarySearchByLastNameWatermark);

            this.lblSpecialty.Text = this.DictionarySearchBySpecialtyLabel;
            this.lblSpecialty.Visible = !string.IsNullOrEmpty(this.DictionarySearchBySpecialtyLabel);
            this.BindSpecialtyDropdown();

            this.btnSubmit.Text = this.DictionarySearchButtonText;
        }

        private void BindSpecialtyDropdown()
        {
            if (!this.ddlSpecialty.Enabled || !this.ddlSpecialty.Visible)
            {
                return;
            }

            string strQuerystringSpecialty = Request.QueryString[BaseModuleHelper.SpecialtiesQuerystring()];
            ModuleHelper.BindSpecialtyDropdown(this.ddlSpecialty, strQuerystringSpecialty);
        }

        protected void ibSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            this.SearchSubmitted();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            this.SearchSubmitted();
        }

        protected void lbSubmit_Click(object sender, EventArgs e)
        {
            this.SearchSubmitted();
        }

        private void SearchSubmitted()
        {
            string lastname = this.txtLastName.Text.Trim();
            if (lastname.Equals(this.DictionarySearchByLastNameWatermark, StringComparison.OrdinalIgnoreCase))
            {
                lastname = string.Empty;
            }

            string specialties = this.ddlSpecialty.SelectedValue;

            if (!string.IsNullOrEmpty(lastname) || !string.IsNullOrEmpty(specialties))
            {
                var physicianSearchParam = new PhysicianSearchParam()
                {
                    LastName = lastname,
                    Specialties = specialties,
                };

                ILinkHelper linkHelper = ModuleHelper.GetLinkHelper();

                linkHelper.SubmitSearch(
                    physicianSearchParam, ModuleHelper.GetSearchResultPageItem(this.RenderingParamSearchResultsPage));
            }
            else
            {
                this.litError.Text = this.DictionaryCriteriaRequired;
                this.pnlError.Visible = true;
            }
        }
    }
}