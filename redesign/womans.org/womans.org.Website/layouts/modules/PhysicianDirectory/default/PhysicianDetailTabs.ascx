﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PhysicianDetailTabs.ascx.cs" Inherits="womans.org.Website.layouts.modules.PhysicianDirectory.PhysicianDetailTabs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<!--------==========================TABS JQUERY==============================-->
	
 <asp:Panel id="tabs" runat="server" ClientIDMode="Static" CssClass="module-pd-tabs responsive-tabs">
    <asp:Literal ID="litTab1" runat="server" />
    <asp:Panel ID="pnlOverview" runat="server" ClientIDMode="Static">
        <asp:Panel ID="pnlTabSpecialties" runat="server" Visible="false">
            <div class="module-pd-tab-label">
                <h2><asp:Literal ID="litSpecialties" runat="server" /></h2></div>                
            <div class="module-pd-attribute">
                <asp:Literal ID="litTabSpecialties" runat="server" />
            </div>
            <div class="clear">
            </div>
        </asp:Panel>
 
         <asp:Panel ID="pnlPhysicianGroups" runat="server" Visible="false">
            <div class="module-pd-tab-label">
                <h2><asp:Literal ID="litPhysicianGroups" runat="server" /></h2></div>                
            <sc:Sublayout ID="slPhysicianOffices" Path="/layouts/modules/PhysicianDirectory/default/PhysicianOfficesTab.ascx"
                            runat="server" />
            <div class="clear">
            </div>
        </asp:Panel>           

        
        <!-- WEBSITE -->
        <asp:Panel ID="pnlWebsite" runat="server" Visible="false">
            <div class="module-pd-tab-label">
                <h2><asp:Literal ID="litWebsite" runat="server" /></h2></div>                
              <%--<div class="module-pd-attribute">--%>
                <p><sc:Link runat="server" ID="scWebsite" Field="Website" /></p>
            <%--</div>  
            <div class="clear">
            </div>--%>
        </asp:Panel> 

        <asp:Panel ID="pnlTabLanguages" runat="server" Visible="false">
            <div class="module-pd-tab-label">
                <h2><asp:Literal ID="litLanguages" runat="server" /></h2></div>
            <div class="module-pd-attribute">
                <asp:Literal ID="litTabLanguages" runat="server" />
            </div>
            <div class="clear">
            </div>
        </asp:Panel>
                
    </asp:Panel>
    
    <asp:Literal ID="litTab2" runat="server" />
    <asp:Panel runat="server" ID="pnlCreds">
         <asp:Panel ID="pnlTabCertifications" runat="server" Visible="false">
            <div class="module-pd-tab-label">
                <h2><asp:Literal ID="litCertifications" runat="server" /></h2></div>
            <div class="module-pd-attribute">
                <sc:Text ID="scTextCert" Field="Certifications" runat="server" /></div>
            <div class="clear">
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlTabLicenses" runat="server" Visible="false">
            <div class="module-pd-tab-label">
                <h2><asp:Literal ID="litLicenses" runat="server" /></h2></div>
            <div class="module-pd-attribute">
                <sc:Text ID="scTextLicenses" Field="Licenses" runat="server" /></div>
            <div class="clear">
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlPositionTitles" runat="server" Visible="false">
            <div class="module-pd-tab-label">
                <h2><asp:Literal ID="litPositionTitles" runat="server" /></h2></div>
            <div class="module-pd-attribute">
               <asp:Literal ID="litPositionTitleValue" runat="server" />
            </div>
            <div class="clear">
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlTabExpertise" runat="server" Visible="false">
            <div class="module-pd-tab-label">
                <h2><asp:Literal ID="litExpertise" runat="server" /></h2></div>
            <div class="module-pd-attribute">
                <sc:Text ID="scTextExpertise" Field="Expertise" runat="server" /></div>
            <div class="clear"></div>
        </asp:Panel>
    </asp:Panel>
    
    <asp:Literal ID="litTab3" runat="server" />
    <asp:Panel runat="server" ID="pnlEducation">
        <asp:Panel ID="pnlTabEducation" runat="server" Visible="false">
            <div class="module-pd-tab-label">
                <h2><asp:Literal ID="litEducation" runat="server" /></h2></div>
            <div class="module-pd-attribute">
                <asp:Repeater ID="rptTabEducation" runat="server" OnItemDataBound="rptTabEducation_ItemDataBound">
                    <HeaderTemplate><ul></HeaderTemplate>
                    <ItemTemplate>
                        <li><asp:Literal ID="litEduItem" runat="server" /></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </asp:Repeater>
            </div>
            <div class="clear">
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlTabInternship" runat="server" Visible="false">
            <div class="module-pd-tab-label">
                <h2><asp:Literal ID="litInternship" runat="server" /></h2></div>
            <div class="module-pd-attribute">
                <asp:Repeater ID="rptTabInternship" runat="server" OnItemDataBound="rptTabEducation_ItemDataBound">
                    <HeaderTemplate><ul></HeaderTemplate>
                    <ItemTemplate>
                        <li><asp:Literal ID="litEduItem" runat="server" /></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </asp:Repeater>
            </div>
            <div class="clear">
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlTabResidency" runat="server" Visible="false">
            <div class="module-pd-tab-label">
                <h2><asp:Literal ID="litResidency" runat="server" /></h2></div>
            <div class="module-pd-attribute">
                <asp:Repeater ID="rptTabResidency" runat="server" OnItemDataBound="rptTabEducation_ItemDataBound">
                    <HeaderTemplate><ul></HeaderTemplate>
                    <ItemTemplate>
                        <li><asp:Literal ID="litEduItem" runat="server" /></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </asp:Repeater>
            </div>
            <div class="clear">
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlTabFellowship" runat="server" Visible="false">
            <div class="module-pd-tab-label">
               <h2> <asp:Literal ID="litFellowship" runat="server" /></h2></div>
            <div class="module-pd-attribute">
                <asp:Repeater ID="rptTabFellowship" runat="server" OnItemDataBound="rptTabEducation_ItemDataBound">
                    <HeaderTemplate><ul></HeaderTemplate>
                    <ItemTemplate>
                        <li><asp:Literal ID="litEduItem" runat="server" /></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </asp:Repeater>
            </div>
            <div class="clear">
            </div>
        </asp:Panel> 
        

    </asp:Panel>

    <asp:Literal ID="litTab4" runat="server" />
    <asp:Panel ID="pnlAchievements" runat="server" ClientIDMode="Static">
        <asp:Panel ID="pnlTabAwards" runat="server" Visible="false">
            <div class="module-pd-tab-label">
                <h2><asp:Literal ID="litAwards" runat="server" /></h2></div>
            <div class="module-pd-attribute">
                <sc:Text ID="scTextAwards" Field="Special Interests" runat="server" /></div>
            <div class="clear">
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlTabPublications" runat="server" Visible="false">
            <div class="module-pd-tab-label">
               <h2> <asp:Literal ID="litPublications" runat="server" /></h2></div>
            <div class="module-pd-attribute">
                <sc:Text ID="scTextPublications" Field="Publications" runat="server" /></div>
            <div class="clear">
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlTabCommunity" runat="server" Visible="false">
            <div class="module-pd-tab-label">
                <h2><asp:Literal ID="litCommunity" runat="server" /></h2></div>
            <div class="module-pd-attribute">
                <sc:Text ID="scTextCommunity" Field="Community" runat="server" /></div>
            <div class="clear">
            </div>
        </asp:Panel>
    </asp:Panel>

</asp:Panel>

<!--==================== /Module: Physician Directory: Physician Detail =============================-->
