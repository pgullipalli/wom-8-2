﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
using MedTouch.PhysicianDirectory.Helpers;
using MedTouch.PhysicianDirectory.Helpers.Links;
using MedTouch.PhysicianDirectory.Search;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using womans.org.BusinessLogic.Helpers;
using womans.org.BusinessLogic.PhysicianDirectory.Search;


namespace womans.org.Website.layouts.modules.PhysicianDirectory
{
    public partial class PhysicianSearch : BaseSublayout
    {
        #region Fields
        private string dictionarySearchByKeywordLabel;
        private string dictionarySearchByKeywordWatermark;
        private string dictionarySearchByFirstNameLabel;
        private string dictionarySearchByFirstNameWatermark;
        private string dictionarySearchByFullNameLabel;
        private string dictionarySearchByFullNameWaterMark;
        private string dictionarySearchByLastNameLabel;
        private string dictionarySearchByLastNameWatermark;
        private string dictionarySearchBySpecialtyLabel;
        private string dictionarySearchByDepartmentLabel;
        private string dictionarySearchByLanguageLabel;
        private string dictionarySearchByGenderLabel;
        private string dictionarySearchByRangeLabel;
        private string dictionarySearchByZipLabel;
        private string dictionarySearchByZipInvalidLabel;
        private string dictionarySearchByZipWaterMark;
        private string dictionarySearchByZipRangeRequiredLabel;
        private string dictionarySearchByCityLabel;
        private string dictionarySearchByPhysicianGroupLabel;

        private string dictionaryCriteriaRequired;

        private string dictionarySearchButtonText;
        private string dictionaryControlHeaderLabel;
        private string dictionaryAdvancedSearchLinkText;
        private Item renderingParamSearchResultsPage;
        #endregion Fields

        #region properties

        protected string DictionaryControlHeaderLabel
        {
            get
            {
                if (this.dictionaryControlHeaderLabel == null)
                {
                    this.dictionaryControlHeaderLabel =
                        CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianSearch.PhysicianSearchLabel");
                }

                return this.dictionaryControlHeaderLabel;
            }
        }

        protected string DictionarySearchByPhysicianGroupLabel
        {
            get
            {
                if (this.dictionarySearchByPhysicianGroupLabel == null)
                {
                    this.dictionarySearchByPhysicianGroupLabel =
                        CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianSearch.PhysicianGroupLabel");
                }

                return this.dictionarySearchByPhysicianGroupLabel;
            }
        }

        protected string DictionarySearchByKeywordLabel
        {
            get
            {
                if (this.dictionarySearchByKeywordLabel == null)
                {
                    this.dictionarySearchByKeywordLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.KeywordLabel");
                }

                return this.dictionarySearchByKeywordLabel;
            }
        }

        protected string DictionarySearchByKeywordWatermark
        {
            get
            {
                if (this.dictionarySearchByKeywordWatermark == null)
                {
                    this.dictionarySearchByKeywordWatermark =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.KeywordWatermark");
                }

                return this.dictionarySearchByKeywordWatermark;
            }
        }

        protected string DictionarySearchByFirstNameLabel
        {
            get
            {
                if (this.dictionarySearchByFirstNameLabel == null)
                {
                    this.dictionarySearchByFirstNameLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.FirstNameLabel");
                }

                return this.dictionarySearchByFirstNameLabel;
            }
        }

        protected string DictionarySearchByFirstNameWatermark
        {
            get
            {
                if (this.dictionarySearchByFirstNameWatermark == null)
                {
                    this.dictionarySearchByFirstNameWatermark =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.FirstNameWatermark");
                }

                return this.dictionarySearchByFirstNameWatermark;
            }
        }

        protected string DictionarySearchByFullNameLabel
        {
            get
            {
                if (this.dictionarySearchByFullNameLabel == null)
                {
                    this.dictionarySearchByFullNameLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.NameLabel");
                }

                return this.dictionarySearchByFullNameLabel;
            }
        }

        protected string DictionarySearchByFullNameWaterMark
        {
            get
            {
                if (this.dictionarySearchByFullNameWaterMark == null)
                {
                    this.dictionarySearchByFullNameWaterMark =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.NameWatermark");
                }

                return this.dictionarySearchByFullNameWaterMark;
            }
        }

        protected string DictionarySearchByLastNameLabel
        {
            get
            {
                if (this.dictionarySearchByLastNameLabel == null)
                {
                    this.dictionarySearchByLastNameLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.LastNameLabel");
                }

                return this.dictionarySearchByLastNameLabel;
            }
        }

        protected string DictionarySearchByLastNameWatermark
        {
            get
            {
                if (this.dictionarySearchByLastNameWatermark == null)
                {
                    this.dictionarySearchByLastNameWatermark =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.LastNameWatermark");
                }

                return this.dictionarySearchByLastNameWatermark;
            }
        }

        protected string DictionarySearchByCityLabel
        {
            get
            {
                if (this.dictionarySearchByCityLabel == null)
                {
                    this.dictionarySearchByCityLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.CityLabel");
                }

                return this.dictionarySearchByCityLabel;
            }
        }

        protected string DictionarySearchByDepartmentLabel
        {
            get
            {
                if (this.dictionarySearchByDepartmentLabel == null)
                {
                    this.dictionarySearchByDepartmentLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.DepartmentLabel");
                }

                return this.dictionarySearchByDepartmentLabel;
            }
        }

        protected string DictionarySearchByLanguageLabel
        {
            get
            {
                if (this.dictionarySearchByLanguageLabel == null)
                {
                    this.dictionarySearchByLanguageLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.LanguageLabel");
                }

                return this.dictionarySearchByLanguageLabel;
            }
        }

        protected string DictionarySearchByGenderLabel
        {
            get
            {
                if (this.dictionarySearchByGenderLabel == null)
                {
                    this.dictionarySearchByGenderLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.GenderLabel");
                }

                return this.dictionarySearchByGenderLabel;
            }
        }

        protected string DictionarySearchByRangeLabel
        {
            get
            {
                if (this.dictionarySearchByRangeLabel == null)
                {
                    this.dictionarySearchByRangeLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.RangeLabel");
                }

                return this.dictionarySearchByRangeLabel;
            }
        }

        protected string DictionarySearchBySpecialtyLabel
        {
            get
            {
                if (this.dictionarySearchBySpecialtyLabel == null)
                {
                    this.dictionarySearchBySpecialtyLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.SpecialtyLabel");
                }

                return this.dictionarySearchBySpecialtyLabel;
            }
        }

        protected string DictionarySearchByZipLabel
        {
            get
            {
                if (this.dictionarySearchByZipLabel == null)
                {
                    this.dictionarySearchByZipLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.ZipLabel");
                }

                return this.dictionarySearchByZipLabel;
            }
        }

        protected string DictionarySearchByZipInvalidLabel
        {
            get
            {
                if (this.dictionarySearchByZipInvalidLabel == null)
                {
                    this.dictionarySearchByZipInvalidLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.ZipInvalidLabel");
                }

                return this.dictionarySearchByZipInvalidLabel;
            }
        }

        protected string DictionarySearchByZipWaterMark
        {
            get
            {
                if (this.dictionarySearchByZipWaterMark == null)
                {
                    this.dictionarySearchByZipWaterMark =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.ZipWatermark");
                }

                return this.dictionarySearchByZipWaterMark;
            }
        }

        protected string DictionarySearchByZipRangeRequiredLabel
        {
            get
            {
                if (this.dictionarySearchByZipRangeRequiredLabel == null)
                {
                    this.dictionarySearchByZipRangeRequiredLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.ZipRangeRequiredLabel");
                }

                return this.dictionarySearchByZipRangeRequiredLabel;
            }
        }

        protected string DictionaryCriteriaRequired
        {
            get
            {
                if (this.dictionaryCriteriaRequired == null)
                {
                    this.dictionaryCriteriaRequired =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.CriteriaRequired");
                }

                return this.dictionaryCriteriaRequired;
            }
        }

        protected string DictionarySearchButtonText
        {
            get
            {
                if (this.dictionarySearchButtonText == null)
                {
                    this.dictionarySearchButtonText =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.SearchButtonText");
                }

                return this.dictionarySearchButtonText;
            }
        }

        protected string DictionaryAdvancedSearchLinkText
        {
            get
            {
                if (this.dictionaryAdvancedSearchLinkText == null)
                {
                    this.dictionaryAdvancedSearchLinkText =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.AdvancedSearchLinkText");
                }

                return this.dictionaryAdvancedSearchLinkText;
            }
        }

        protected Item RenderingParamSearchResultsPage
        {
            get
            {
                if (this.renderingParamSearchResultsPage == null)
                {
                    this.renderingParamSearchResultsPage = this.GetSingleReferenceProperty("Search Results Page");
                }

                return this.renderingParamSearchResultsPage;
            }
        }

        public static string SpecialtySearchFolderName
        {
            get 
            {
                return "{A7D5609C-6FF0-432F-B6D6-5836199EED92}";
            }
        }

        public static string SpecialtySearchTemplateGuid
        {
            get
            {
                return "{F13CD9DF-B762-4E72-8706-5D325EB1F223}";
            }
        }

        #endregion properties

        protected override void Bind()
        {
            this.litPhysicianSearch.Text = this.DictionaryControlHeaderLabel;
            this.phControlHeader.Visible = !string.IsNullOrEmpty(this.DictionaryControlHeaderLabel);

            this.lblKeyword.Text = this.DictionarySearchByKeywordLabel;
            this.lblKeyword.Visible = !string.IsNullOrEmpty(this.DictionarySearchByKeywordLabel);
            UIHelper.SetTextboxWatermark(this.txtKeyword, string.Empty, this.DictionarySearchByKeywordWatermark);

            this.lblFirstName.Text = this.DictionarySearchByFirstNameLabel;
            this.lblFirstName.Visible = !string.IsNullOrEmpty(this.DictionarySearchByFirstNameLabel);
            UIHelper.SetTextboxWatermark(this.txtFirstName, string.Empty, this.DictionarySearchByFirstNameWatermark);

            this.lblNameAutoComplete.Text = this.DictionarySearchByFullNameLabel;
            this.lblNameAutoComplete.Visible = !string.IsNullOrEmpty(this.DictionarySearchByFullNameLabel);
            UIHelper.SetTextboxWatermarkPlaceholder(this.txtNameAutoComplete, string.Empty, this.DictionarySearchByFullNameWaterMark);
            this.BindAutoCompleteControl();

            this.lblLastName.Text = this.DictionarySearchByLastNameLabel;
            this.lblLastName.Visible = !string.IsNullOrEmpty(this.DictionarySearchByLastNameLabel);
            UIHelper.SetTextboxWatermark(this.txtLastName, string.Empty, this.DictionarySearchByLastNameWatermark);

            this.lblSpecialty.Text = this.DictionarySearchBySpecialtyLabel;
            this.lblSpecialty.Visible = !string.IsNullOrEmpty(this.DictionarySearchBySpecialtyLabel);
            this.BindSpecialtyDropdown();

            this.lblDepartment.Text = this.DictionarySearchByDepartmentLabel;
            this.lblDepartment.Visible = !string.IsNullOrEmpty(this.DictionarySearchByDepartmentLabel);
            this.BindDepartmentDropdown();

            this.lblPhysicianGroup.Text = this.DictionarySearchByPhysicianGroupLabel;
            this.lblPhysicianGroup.Visible = !string.IsNullOrEmpty(this.DictionarySearchByPhysicianGroupLabel);
            this.BindPhysicianGroupDropDown();

            this.lblLanguage.Text = this.DictionarySearchByLanguageLabel;
            this.lblLanguage.Visible = !string.IsNullOrEmpty(this.DictionarySearchByLanguageLabel);
            this.BindLanguageDropdown();

            this.lblGender.Text = this.DictionarySearchByGenderLabel;
            this.lblGender.Visible = !string.IsNullOrEmpty(this.DictionarySearchByGenderLabel);
            this.BindGenderDropdown();

            this.lblZip.Text = this.DictionarySearchByZipLabel;
            this.lblZip.Visible = !string.IsNullOrEmpty(this.DictionarySearchByZipLabel);
            UIHelper.SetTextboxWatermark(this.txtZip, string.Empty, this.DictionarySearchByZipWaterMark);
            this.BindZipValidationControl();

            this.lblRange.Text = this.DictionarySearchByRangeLabel;
            this.lblRange.Visible = !string.IsNullOrEmpty(this.DictionarySearchByRangeLabel);
            this.BindRangeDropdown();
            // If Rang is Visible, Zipcode need to be Visible.
            if (this.lblRange.Visible)
            {
                this.lblZip.Visible = true;
            }

            this.lblCity.Text = this.DictionarySearchByCityLabel;
            this.lblCity.Visible = !string.IsNullOrEmpty(this.DictionarySearchByCityLabel);
            this.BindCityDropDown();

            this.btnSubmit.Text = this.DictionarySearchButtonText;
            this.lbSubmit.Text = this.DictionarySearchButtonText;
        }

        protected void BindDepartmentDropdown()
        {
            if (!this.ddlDepartment.Enabled || !this.ddlDepartment.Visible)
            {
                return;
            }

            // Check Deparment Tag. If this is set , hide department
            string strDepartments = ItemHelper.GetItemNamesFromItemGuids(ItemHelper.GetFieldRawValue(ContextItem, "Departments"));
            if (string.IsNullOrEmpty(strDepartments))
            {
                string strQuerystringPractice = Request.QueryString[ModuleHelper.DepartmentsQuerystring()];
                ModuleHelper.BindDepartmentDropdown(this.ddlDepartment, strQuerystringPractice);
            }
            else
            {
                this.pnlSpecialty.Visible = false;
            }
        }

        protected void BindPhysicianGroupDropDown()
        {
            if (!this.ddlPhysicianGroup.Enabled || !this.ddlPhysicianGroup.Visible)
            {
                return;
            }

            // Check Locations Tag. 
            string strLocations = ItemHelper.GetItemNamesFromItemGuids(ItemHelper.GetFieldRawValue(ContextItem, "Locations"));
            if (string.IsNullOrEmpty(strLocations))
            {
                //string strQuerystringPhysGroup = this.Request.QueryString[BaseModuleHelper.LocationsQuerystring()];
                //ModuleHelper.BindPhysicianFilterDropdown(this.ddlPhysicianGroup, strQuerystringPhysGroup, CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianSearch.AllLocationsText"), "{BFE5CC2B-F045-4A05-8B9A-1BC4B5F49CB6}", "{21931248-664D-4B33-A3E8-426B8694D7EB}", "Headline");
                string strQuerystringPhysGroup = this.Request.QueryString[PhysicianHelper.GroupQuerystring()];
                PhysicianHelper.BindGroupDropDown(this.ddlPhysicianGroup, strQuerystringPhysGroup);
            }
            else
            {
                this.pnlPhysicianGroup.Visible = false;
            }


        }

        protected void BindSpecialtyDropdown()
        {
            if (!this.ddlSpecialty.Enabled || !this.ddlSpecialty.Visible)
            {
                return;
            }

            string strQuerystringSpecialty = Request.QueryString[BaseModuleHelper.SpecialtiesQuerystring()];
            PhysicianHelper.BindSpecialtySearchDropDown(this.ddlSpecialty, strQuerystringSpecialty);

            //Item item = Sitecore.Context.Database.GetItem(SpecialtySearchFolderName);
            //ListItemCollection listItems = new ListItemCollection();

            //if (item != null)
            //{
            //    foreach (Item itm in item.GetChildren())
            //    {
            //        string specialties = string.Empty;
            //        if (itm.TemplateID.ToString() == SpecialtySearchTemplateGuid)
            //        {
            //            MultilistField mfSpecialties = (MultilistField)(itm.Fields["Specialties"]);
            //            foreach (Item i in mfSpecialties.GetItems())
            //            {
            //                if (string.IsNullOrWhiteSpace(specialties))
            //                {
            //                    specialties = i.Name;
            //                }
            //                else
            //                {
            //                    specialties += "|" + i.Key;
            //                }
            //            }

            //            ListItem li = new ListItem(itm.Fields["Specialty Name"].Value, specialties);
            //            listItems.Add(li);

            //        }
            //    }
            //}

            //ListItem listItem = new ListItem(CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianSearch.AllSpecialtiesText"), "");
            //listItems.Insert(0, listItem);

            //ddlSpecialty.DataSource = listItems;
            //ddlSpecialty.DataBind();

        }

        protected void BindLanguageDropdown()
        {
            if (!this.ddlLanguage.Enabled || !this.ddlLanguage.Visible)
            {
                return;
            }

            string strQuerystringLanguage = Request.QueryString[ModuleHelper.LanguagesQuerystring()];
            ModuleHelper.BindLanguageDropdown(this.ddlLanguage, strQuerystringLanguage);
        }

        protected void BindGenderDropdown()
        {
            if (!this.ddlGender.Enabled || !this.ddlGender.Visible)
            {
                return;
            }

            string strQuerystringGender = Request.QueryString[ModuleHelper.GenderQuerystring()];
            ModuleHelper.BindGenderDropdown(this.ddlGender, strQuerystringGender);
        }

        protected void BindRangeDropdown()
        {
            string strLocations = ItemHelper.GetItemNamesFromItemGuids(ItemHelper.GetFieldRawValue(ContextItem, ItemMapper.Physician.FieldName.Locations));
            if (string.IsNullOrEmpty(strLocations))
            {
                string strQuerystringRange = Request.QueryString[BaseModuleHelper.RangeQuerystring()];
                ModuleHelper.BindRangeDropdown(this.ddlRange, strQuerystringRange);
            }
            else
            {
                this.pnlRange.Visible = false;
                this.pnlZip.Visible = false;
            }
        }

        protected void BindCityDropDown()
        {
            if (!this.ddlCity.Enabled || !this.ddlCity.Visible)
            {
                return;
            }

            string strQuerystringCity = Request.QueryString[ModuleHelper.CityQuerystring()];
            ModuleHelper.BindCityDropdown(this.ddlCity, strQuerystringCity);
        }

        protected void BindZipValidationControl()
        {
            // Zip Regex Control
            this.regexVldZip.ErrorMessage = this.DictionarySearchByZipInvalidLabel;
            this.regexVldZip.ValidationExpression = StringHelper.BuildZipRegexPattern(this.DictionarySearchByZipWaterMark);

            // Zip and Range Validation
            this.ctmVldZipRange.ErrorMessage = this.DictionarySearchByZipRangeRequiredLabel;
        }

        protected void BindAutoCompleteControl()
        {
            if (!this.txtNameAutoComplete.Enabled || !this.txtNameAutoComplete.Visible)
            {
                return;
            }

            // Serialize autocomplete control
            List<string> namesList = ModuleHelper.BuildPredictiveSearch();

            if (namesList.Count > 0)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var serializedNames = js.Serialize(namesList);

                string serializedListText = "var wordlist=" + serializedNames + ";";
                Page.ClientScript.RegisterClientScriptBlock(
                    this.GetType(), "registerserializedlist", serializedListText, true);
            }
        }

        protected void ibSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            this.SearchSubmitted();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            this.SearchSubmitted();
        }

        protected void lbSubmit_Click(object sender, EventArgs e)
        {
            this.SearchSubmitted();
        }

        protected virtual void SearchSubmitted()
        {
            string firstname = this.txtFirstName.Text.Trim();
            if (firstname.Equals(this.DictionarySearchByFirstNameWatermark, StringComparison.OrdinalIgnoreCase))
            {
                firstname = string.Empty;
            }

            string lastname = this.txtLastName.Text.Trim();
            if (lastname.Equals(this.DictionarySearchByLastNameWatermark, StringComparison.OrdinalIgnoreCase))
            {
                lastname = string.Empty;
            }

            string zip = this.txtZip.Text.Trim();
            if (zip.Equals(this.DictionarySearchByZipWaterMark, StringComparison.OrdinalIgnoreCase))
            {
                zip = string.Empty;
            }

            string fullName = this.txtNameAutoComplete.Text.Trim();
            if (fullName.Equals(this.DictionarySearchByFullNameWaterMark, StringComparison.OrdinalIgnoreCase))
            {
                fullName = string.Empty;
            }

            string taxonomy = this.txtKeyword.Text.Trim();
            if (taxonomy.Equals(this.DictionarySearchByKeywordWatermark, StringComparison.OrdinalIgnoreCase))
            {
                taxonomy = string.Empty;
            }

            //gets all of the specialties
            string strSpecialties = ItemHelper.GetItemNamesFromItemGuids(ItemHelper.GetFieldRawValue(ContextItem, ItemMapper.Physician.FieldName.Specialties));

            string specialties = string.IsNullOrEmpty(strSpecialties) ? this.ddlSpecialty.SelectedValue : strSpecialties;
            //string strLocations = ItemHelper.GetItemNamesFromItemGuids(ItemHelper.GetFieldRawValue(ContextItem, ItemMapper.Physician.FieldName.Locations));
            //string locations = string.IsNullOrEmpty(strLocations) ? this.ddlPhysicianGroup.SelectedValue : strLocations;
            string group = this.ddlPhysicianGroup.SelectedValue;

            string languages = this.ddlLanguage.SelectedValue;
            string departments = this.ddlDepartment.SelectedValue;
            string gender = this.ddlGender.SelectedValue;
            string range = this.ddlRange.SelectedValue;
            string cities = this.ddlCity.SelectedValue;

            //if (!string.IsNullOrEmpty(locations))
            if (!string.IsNullOrEmpty(group))
            {
                zip = string.Empty;
                range = string.Empty;
            }

            Item searchResultsPage = ModuleHelper.GetSearchResultPageItem(this.RenderingParamSearchResultsPage);

            //var physicianSearchParam = new PhysicianSearchParam()
            //{
            //    Keyword = string.Empty,
            //    Taxonomies = taxonomy,
            //    FirstName = firstname,
            //    LastName = lastname,
            //    FullName = fullName,
            //    Specialties = specialties,
            //    Departments = departments,
            //    Languages = languages,
            //    Gender = gender,
            //    Zip = zip,
            //    Range = range,
            //    Locations = locations,
            //    Cities = cities
            //};
            var physicianSearchParam = new WomansPhysicianSearchParam()
            {
                Keyword = string.Empty,
                Taxonomies = taxonomy,
                FirstName = firstname,
                LastName = lastname,
                FullName = fullName,
                Specialties = specialties,
                Departments = departments,
                Languages = languages,
                Gender = gender,
                Zip = zip,
                Range = range,
                Group = group,
                Cities = cities
            };

            //ILinkHelper linkHelper = ModuleHelper.GetLinkHelper();
            ILinkHelper linkHelper = PhysicianHelper.GetLinkHelper();

            linkHelper.SubmitSearch(physicianSearchParam, searchResultsPage);
        }
    }
}