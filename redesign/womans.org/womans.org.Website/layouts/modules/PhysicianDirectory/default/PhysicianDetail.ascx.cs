﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
using MedTouch.PhysicianDirectory.Helpers;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;

namespace womans.org.Website.layouts.modules.PhysicianDirectory
{

    /// <summary>
    /// Summary description for PhysiciandetailSublayout
    /// </summary>
    public partial class PhysicianDetail : BaseSublayout
    {
        private void Page_Load(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif

            Initiate();

#if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
#endif

        }

        private void Initiate()
        {
            //slPhysicianImage.DataSource = ContextItem.ID.ToString();
            //slPhysicianTitle.DataSource = ContextItem.ID.ToString();
            //slPhysicianInfo.DataSource = ContextItem.ID.ToString();
            //slPhysicianOffices.DataSource = ContextItem.ID.ToString();
            slPhysicianTabs.DataSource = ContextItem.ID.ToString();


            bool acceptsPatients = false;
            Sitecore.Data.Fields.MultilistField ml = ContextItem.Fields["Specialties"];
            foreach (var i in ml.GetItems())
            {
                if (i.Fields["Allow to Make An Appointment"].Value == "1")
                {
                    acceptsPatients = true;
                    break;
                }
            }

            if (acceptsPatients)
            {
                pnlMakeAppointment.Visible = true;
                litNewPatientsMessage.Text = CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearch.MakeAnAppointmentText");
            }
 
        }

        protected void btnPrintPDF_Click(object sender, EventArgs e)
        {
            Download();
        }

        private void Download()
        {
            string headerText = GetProperty("PDF Header");
            string footerText = GetProperty("PDF Footer");

            string url = Request.Url.ToString();
            url += Request.Url.ToString().Contains("?") ? "&PDF=true" : "?PDF=true"; // Add Parameter PDF=true to call the PDF device layout rather than the default device layout
            HTMLPDFHelper.ConvertToPDF(url, ContextItem.Name, HTMLPDFHelper.enmPDFTarget.PDFAsAttachment, headerText, footerText);
        }
    }
}