﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
using MedTouch.PhysicianDirectory.Helpers;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;

namespace womans.org.Website.layouts.modules.PhysicianDirectory
{
    /// <summary>
    /// Summary description for PhysiciandetailtabsSublayout
    /// </summary>
    public partial class PhysicianDetailTabs : BaseSublayout
    {
        private void Page_Load(object sender, EventArgs e)
        {

#if (!DEBUG)
            try
            {
#endif

            BuildTabs();

#if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
#endif

        }

        private void BuildTabs()
        {
            StringBuilder sbTabs = new StringBuilder();

            BuildOverviewTab(sbTabs);
            BuildCredentialsTab(sbTabs);
            BuildEducationTab(sbTabs);
            BuildAchievementsTab(sbTabs);
            //BuildMediaTab(sbTabs);
            //BuildInsuranceTab(sbTabs);

            //if (sbTabs.Length > 0)
            //{
            //    litTabs.Text = string.Format("<ul>{0}</ul>", sbTabs.ToString());
            //}
            //else
            //{
            //    litTabs.Visible = false;
            //}
        }

        private void BindEducation(ref bool showThisTab, ModuleHelper.EducationCategory eduCat, Literal litTab, Repeater rptTab, Panel pnlTab)
        {
            List<Item> educationItems = new List<Item>();
            string label = string.Empty;
            switch (eduCat)
            {
                    case ModuleHelper.EducationCategory.Education:
                    educationItems = ModuleHelper.GetPhysicianEducationItems(ContextItem);
                    label = CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.LabelEducation");
                    break;
                    case ModuleHelper.EducationCategory.Internship:
                    educationItems = ModuleHelper.GetPhysicianInternshipItems(ContextItem);
                    label = CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.LabelInternship");
                    break;
                    case ModuleHelper.EducationCategory.Residency:
                    educationItems = ModuleHelper.GetPhysicianResidencyItems(ContextItem);
                    label = CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.LabelResidency");
                    break;
                    case ModuleHelper.EducationCategory.Fellowship:
                    educationItems = ModuleHelper.GetPhysicianFellowshipItems(ContextItem);
                    label = CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.LabelFellowship");
                    break;
            }

            if (educationItems.Any() && litTab != null && rptTab != null && pnlTab != null)
            {
                litTab.Text = label;
                rptTab.DataSource = educationItems;
                rptTab.DataBind();
                rptTab.Visible = true;
                pnlTab.Visible = true;
                showThisTab = true;
            }
        }
        
        private void BuildOverviewTab(StringBuilder sb)
        {
            bool showThisTab = false;

            ModuleHelper.ManageToShowRelatedItemsOnTab(ref showThisTab, litSpecialties, "Modules.PhysicianDirectory.PhysicianDetailTabs.LabelSpecialties", litTabSpecialties, ", ", pnlTabSpecialties, ContextItem, "Specialties", "Specialty Name", ModuleHelper.DISPLAY_TYPE_UNORDERED_LIST);
            
            ModuleHelper.ManageToShowRelatedItemsOnTab(ref showThisTab, litLanguages, "Modules.PhysicianDirectory.PhysicianDetailTabs.LabelLanguages", litTabLanguages, "<br/>", pnlTabLanguages, ContextItem, "Languages Spoken", "Language Name", ModuleHelper.DISPLAY_TYPE_UNORDERED_LIST);

            List<Item> officeItems = ModuleHelper.GetPhysicianOfficeItems(ContextItem);
            if (officeItems.Any())
            {
                litPhysicianGroups.Text = CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.LabelPhysicianGroups");
                pnlPhysicianGroups.Visible = true;
            }
            
            if (!string.IsNullOrWhiteSpace(ContextItem["Website"])) {
                litWebsite.Text = CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.LabelWebsite");
                scWebsite.Item = ContextItem;
                if (string.IsNullOrWhiteSpace(scWebsite.Text))
                {
                    string strLinkText = string.Empty;
                    LinkField lfWebsite = (LinkField)ContextItem.Fields["Website"];
                    if (lfWebsite != null)
                    {
                        if (string.IsNullOrWhiteSpace(lfWebsite.Text))
                        {
                            strLinkText = lfWebsite.Url;
                        }
                        else
                        {
                            strLinkText = lfWebsite.Text;
                        }
                    }
                    scWebsite.Text = strLinkText;
                }

                pnlWebsite.Visible = true;
            }

            
            // show tab if something on the tab is available
            pnlOverview.Visible = showThisTab;
            if (showThisTab)
            {
                litTab1.Text = "<h2>" + CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.TabOverview") + "</h2>";
                //sb.Append(string.Format("<li><a href=\"{0}\">{1}</a></li>", "#pnlOverview", CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.TabOverview")));
            }
        }

        private void BuildCredentialsTab(StringBuilder sb)
        {
            bool showThisTab = false;

            ModuleHelper.ManageToShowRichTextFieldOnTab(ref showThisTab, litCertifications, "Modules.PhysicianDirectory.PhysicianDetailTabs.LabelCertifications", scTextCert, pnlTabCertifications, ContextItem);
            ModuleHelper.ManageToShowRichTextFieldOnTab(ref showThisTab, litLicenses, "Modules.PhysicianDirectory.PhysicianDetailTabs.LabelLicenses", scTextLicenses, pnlTabLicenses, ContextItem);
            ModuleHelper.ManageToShowRelatedItemsOnTab(ref showThisTab, litPositionTitles, "Modules.PhysicianDirectory.PhysicianDetailTabs.LabelPositionTitles", litPositionTitleValue, "<br/>", pnlPositionTitles, ContextItem, "Position Titles", "Name", ModuleHelper.DISPLAY_TYPE_UNORDERED_LIST);

            ModuleHelper.ManageToShowRichTextFieldOnTab(ref showThisTab, litExpertise, "Modules.PhysicianDirectory.PhysicianDetailTabs.LabelExpertise", scTextExpertise, pnlTabExpertise, ContextItem);

            pnlCreds.Visible = showThisTab;
            if (showThisTab)
            {
                litTab2.Text = "<h2>" + CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.TabCredentials") + "</h2>";
                //sb.Append(string.Format("<li><a href=\"{0}\">{1}</a></li>", "#pnlOverview", CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.TabOverview")));
            }

        }

        private void BuildEducationTab(StringBuilder sb)
        {
            bool showThisTab = false;
            BindEducation(ref showThisTab, ModuleHelper.EducationCategory.Education, litEducation, rptTabEducation, pnlTabEducation);
            BindEducation(ref showThisTab, ModuleHelper.EducationCategory.Internship, litInternship, rptTabInternship, pnlTabInternship);
            BindEducation(ref showThisTab, ModuleHelper.EducationCategory.Residency, litResidency, rptTabResidency, pnlTabResidency);
            BindEducation(ref showThisTab, ModuleHelper.EducationCategory.Fellowship, litFellowship, rptTabFellowship, pnlTabFellowship);


            pnlEducation.Visible = showThisTab;
            if (showThisTab)
            {
                litTab3.Text = "<h2>" + CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.TabEducation") + "</h2>";
                //sb.Append(string.Format("<li><a href=\"{0}\">{1}</a></li>", "#pnlOverview", CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.TabOverview")));
            }
        }

        private void BuildAchievementsTab(StringBuilder sb)
        {
            bool showThisTab = false;

            ModuleHelper.ManageToShowRichTextFieldOnTab(ref showThisTab, litAwards, "Modules.PhysicianDirectory.PhysicianDetailTabs.LabelAwards", scTextAwards, pnlTabAwards, ContextItem);
            ModuleHelper.ManageToShowRichTextFieldOnTab(ref showThisTab, litPublications, "Modules.PhysicianDirectory.PhysicianDetailTabs.LabelPublications", scTextPublications, pnlTabPublications, ContextItem);
            ModuleHelper.ManageToShowRichTextFieldOnTab(ref showThisTab, litCommunity, "Modules.PhysicianDirectory.PhysicianDetailTabs.LabelCommunity", scTextCommunity, pnlTabCommunity, ContextItem);

            // show tab if something on the tab is available
            pnlAchievements.Visible = showThisTab;
            
            if (showThisTab)
            {
                litTab4.Text = "<h2>" + CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.TabAchievements") + "</h2>";
            //    sb.Append(string.Format("<li><a href=\"{0}\">{1}</a></li>", "#pnlAchievements", CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.TabAchievements")));
            }
        }

        private void BuildMediaTab(StringBuilder sb)
        {
            //bool showThisTab = false;

            //ModuleHelper.ManageToShowRichTextFieldOnTab(ref showThisTab, null, string.Empty, scTextMedia, pnlTabMedia, ContextItem);

            //// show tab if something on the tab is available
            //pnlMedia.Visible = showThisTab;
            //if (showThisTab)
            //{
            //    litTab3.Text = "<h2>" + CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.TabMedia") + "</h2>";
            //    //sb.Append(string.Format("<li><a href=\"{0}\">{1}</a></li>", "#pnlMedia", CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.TabMedia")));
            //}
        }

        private void BuildInsuranceTab(StringBuilder sb)
        {
            //bool showThisTab = false;

            //ModuleHelper.ManageToShowRelatedItemsOnTab(
            //    ref showThisTab,
            //    litInsurance,
            //    "Modules.PhysicianDirectory.PhysicianDetailTabs.LabelInsurance",
            //    litTabInsurance,
            //    "<br/>",
            //    pnlTabInsuranceAccepted,
            //    ContextItem,
            //    ItemMapper.Physician.FieldName.InsuranceAccepted,
            //    "Name",
            //    ModuleHelper.DISPLAY_TYPE_UNORDERED_LIST);

            //// show tab if something on the tab is available
            //pnlInsurance.Visible = showThisTab;
            //if (showThisTab)
            //{
            //    litTab4.Text = "<h2>" + CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.TabInsurance") + "</h2>";
            //    //sb.Append(string.Format("<li><a href=\"{0}\">{1}</a></li>", "#pnlInsurance", CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.TabInsurance")));
            //}
        }

        private void BindServices(ref bool showThisTab,  Literal litTab, Repeater rptTab, Panel pnlTab)
        {
            //List<Item> serviceItems = new List<Item>();
            //string label = string.Empty;
            //label = CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianDetailTabs.LabelFellowship");

            //if (serviceItems.Any() && litTab != null && rptTab != null && pnlTab != null)
            //{
            //    litTab.Text = label;
            //    rptTab.DataSource = serviceItems;
            //    rptTab.DataBind();
            //    rptTab.Visible = true;
            //    pnlTab.Visible = true;
            //    showThisTab = true;
            //}
        }

        protected void rptTabEducation_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Item item = (Item) e.Item.DataItem;
                Literal litEduItem = (Literal) e.Item.FindControl("litEduItem");

                if (item != null && litEduItem != null)
                {
                    string strYear = ItemHelper.GetFieldHtmlValue(item, "Year");
                    string strInstitution = ItemHelper.GetFieldHtmlValue(item, "Institution Name");
                    string strEduType = ItemHelper.GetFieldHtmlValueFromReferencedItem(item, "Education Type", "Name");

                    string strEdu = string.Empty;
                    if (!string.IsNullOrEmpty(strInstitution))
                        strEdu += string.IsNullOrEmpty(strEdu) ? strInstitution : ", " + strInstitution;
                    if (!string.IsNullOrEmpty(strYear))
                        strEdu += string.IsNullOrEmpty(strEdu) ? strYear : ", " + strYear;
                    //if (!string.IsNullOrEmpty(strEduType))
                    //    strEdu += string.IsNullOrEmpty(strEdu) ? strEduType : ", " + strEduType;

                    litEduItem.Text = strEdu;
                }
            }
        }

        protected void rptRelatedItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Item item = (Item) e.Item.DataItem;
                Text scTextItem = (Text) e.Item.FindControl("scTextItem");

                if (item != null && scTextItem != null)
                {
                    scTextItem.Item = item;
                }
            }
        }

        protected void rptServices_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //check if it is coming from Item or Alternating Item template
            if (e.Item.ItemType != ListItemType.Item &&
                e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            Item item = e.Item.DataItem as Item;
            if (item != null)
            {
                HyperLink hlService = e.Item.FindControl("hlService") as HyperLink;

                if (hlService != null)
                {
                    hlService.Text = ItemHelper.GetFieldHtmlValue(item, "Name");
                    string navUrl = string.Empty;
                    ItemHelper.GetGeneralLinkUrl(item.Fields["Link"], out navUrl);
                    hlService.NavigateUrl = navUrl;

                    if (string.IsNullOrEmpty(navUrl))
                        hlService.Enabled = false;
                }
            }
        }
    }
}