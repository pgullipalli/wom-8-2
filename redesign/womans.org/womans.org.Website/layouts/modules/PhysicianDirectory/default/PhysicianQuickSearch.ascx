﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.PhysicianDirectory.layouts.modules.PhysicianDirectory.PhysicianquicksearchSublayout" CodeBehind="PhysicianQuickSearch.ascx.cs" %>
<!--==================== Module: Physician Directory: Physician Quick Search =============================-->


<asp:Panel ID="pnlControlWrapper" runat="server" DefaultButton="btnSubmit" CssClass="module-pd-search-again core-search-again collapse-for-mobile" >
    <div class="mobile-callout-header">Filter Your Results</div>
    <div class="reg callout grid">
    <h3>Filter Your Results</h3>

     <asp:Panel ID="pnlLastName" runat="server" CssClass="six columns">
            <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName" CssClass="label" />
            <asp:TextBox ID="txtLastName" runat="server" placholder="Enter a Name" CssClass="textbox" />
        </asp:Panel>
        <asp:Panel ID="pnlSpecialty" runat="server" CssClass="six columns">
            <asp:Label ID="lblSpecialty" runat="server" AssociatedControlID="ddlSpecialty" CssClass="label" />
            <div class="selectbox">
            <asp:DropDownList ID="ddlSpecialty" runat="server" CssClass="selectboxdiv" />
            <div class="out"></div>
            </div>
        </asp:Panel>
    
    <div class="grid twelve columns toggle-hide-show"> 
		<a href="#"><span class="show">Advanced Search +</span></a>
	</div>

        <asp:Panel ID="pnlAdvSearch" runat="server" Visible="false">
            <asp:Hyperlink ID="hlAdvSearch" runat="server" />
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="false" CssClass="errortext"><asp:Literal ID="litError" runat="server" /></asp:Panel>

	<section class="form-toggle">
		<div class="six columns">
		  <label class="label">Language</label>
		  <div class="selectbox">  
			  <select class="selectboxdiv">
				<option value="">Select a Language</option>
				<option value="english">English</option>
				<option value="spanish">Spanish</option>
			  </select>
			  <div class="out"></div>
		  </div>		
		</div>
		<div class="six columns">
			<label class="label">Gender</label>
			<div class="selectbox">  
				  <select class="selectboxdiv">
					<option value="">Select a Gender</option>
					<option value="male">Male</option>
					<option value="female">Female</option>
				  </select>
				  <div class="out"></div>
			</div>		
		</div>
		<div class="six columns">
			<label class="label">City</label>
			<div class="selectbox">  
				<select class="selectboxdiv">
				<option value="">Select a City</option>
				<option value="Cambridge , MA">Cambridge , MA</option>
				<option value="Hiawatha, IA">Hiawatha, IA</option>
				<option value="Old Katy, TX">Old Katy, TX</option>
				</select>
				<div class="out"></div>
			</div>			
		</div>
	</section> 


	
	<div class="search-option-submit twelve columns">
        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="button default-button black" />
	</div>   
		
</div>


</asp:Panel>

   
<!--==================== /Module: Physician Directory: Physician Quick Search =============================-->
