﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DirectoryPDF.ascx.cs" Inherits="MedTouch.PhysicianDirectory.layouts.modules.PhysicianDirectory.DirectoryPDF" %>
<!--==================== Module: Physician Directory: Directory PDF =============================-->
<asp:Panel ID="pnlDownloadPDF" runat="server" CssClass="module-pd-directorypdf right">
    <span class="pdf-icon"></span>
    <asp:linkbutton ID="lbDownload" runat="server" OnClick="lbDownload_Click" CssClass="pdf-link" />
    <asp:ImageButton ID="ibDownload" runat="server" Visible="false" OnClick="ibDownload_Click" />
    <asp:Button ID="btnDownload" runat="server" Visible="false" OnClick="btnDownload_Click" />
</asp:Panel>
<div class="clear"></div>
<!--==================== /Module: Physician Directory: Directory PDF =============================-->