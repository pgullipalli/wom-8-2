﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.PhysicianDirectory.layouts.modules.PhysicianDirectory.PhysicianinfoSublayout"
    CodeBehind="PhysicianInfo.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<asp:Panel runat="server" ID="pnlInfo" CssClass="module-pd-info">
    <asp:Panel ID="pnlInfoSpecialties" runat="server" Visible="false" CssClass="module-pd-specialty-list">
        <asp:Literal ID="litInfoSpecialties" runat="server" />
    </asp:Panel>
    <asp:Panel ID="pnlInfoPositions" runat="server" Visible="false">
        <asp:Literal ID="litInfoPositions" runat="server" />
    </asp:Panel>
    <asp:Panel ID="pnlWebsite" runat="server" Visible="false">
        <sc:link id="scLinkPhysicianWebsite" field="Website" runat="server" />
    </asp:Panel>
    <asp:Panel ID="pnlProfile" runat="server" Visible="false">
    
        <asp:Hyperlink ID="hlProfile" runat="server" />
    </asp:Panel>
    <div class="clear"></div>
</asp:Panel>