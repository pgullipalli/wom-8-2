﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PhysicianDetail.ascx.cs" Inherits="womans.org.Website.layouts.modules.PhysicianDirectory.PhysicianDetail" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: Physician Directory: Physician Detail =============================-->


<h1>
    <sc:Text ID="scHeadline" runat="server" Field="Headline" />
</h1>

<asp:Panel runat="server" ID="pnlDetail" CssClass="module-pd-profile core-profile">

    <asp:Panel ID="pnlInfo" runat="server" CssClass="module-pd-profile-top grid">
        
        <asp:Panel ID="pnlMakeAppointment" runat="server" CssClass="new-patients" Visible="false">
        	<span></span>
            <p><asp:Literal runat="server" ID="litNewPatientsMessage" /></p>
        </asp:Panel>
        <h2>
            <sc:Text ID="scIntroduction" runat="server" Field="Sub Headline" />
        </h2>
        <sc:Text ID="scContentCopy" runat="server" Field="Content Copy" />
        
    </asp:Panel>
   
    <sc:Sublayout ID="slPhysicianTabs" Path="/layouts/modules/PhysicianDirectory/default/PhysicianDetailTabs.ascx"
            runat="server" />
</asp:Panel>


<!--==================== /Module: Physician Directory: Physician Detail =============================-->
