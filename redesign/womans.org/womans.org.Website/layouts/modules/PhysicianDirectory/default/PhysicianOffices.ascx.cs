﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
using MedTouch.PhysicianDirectory.DAL;
using MedTouch.PhysicianDirectory.Helpers;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;

namespace womans.org.Website.layouts.modules.PhysicianDirectory
{
    using Sitecore.Sharedsource.Data.Comparers.ItemComparers;

    /// <summary>
    /// Summary description for PhysicianofficesSublayout
    /// </summary>
    public partial class PhysicianofficesSublayout : BaseSublayout
    {
        #region properties
        private bool AccurateRangeSearch
        {
            get { return GetBooleanProperty("AccurateRangeSearch"); }
        }

        private Point ZipPoint { get; set; }
        private string ZipQueryString { get { return Request.QueryString[ModuleHelper.ZipQuerystring()]; } }
        private string RangeQueryString { get { return Request.QueryString[BaseModuleHelper.RangeQuerystring()]; } }
        #endregion properties

        private void Page_Load(object sender, EventArgs e)
        {

#if (!DEBUG)
            try
            {
#endif
            Initialize();
            BindOffices();

#if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
#endif

        }

        private void Initialize()
        {
            if (!string.IsNullOrEmpty(ZipQueryString))
            {
                //Get GeoLocation of Zipcode
                GeoLocation foundGeoLocation = GeoLocationDAL.GetGeolocation(ZipQueryString);
                if (foundGeoLocation != null)
                {
                    ZipPoint = new Point(foundGeoLocation.Latitude, foundGeoLocation.Longitude);
                }
            }
        }

        private void BindOffices()
        {
            List<Item> officeItems = ModuleHelper.GetPhysicianOfficeItems(ContextItem);
            if (officeItems.Any())
            {
                // Sort
                var sortItems = officeItems
                    .OrderByDescending(x => ItemHelper.GetFieldRawValue(x, "Primary Office"))
                    .ThenBy(x => ItemHelper.GetFieldRawValue(x, "Office Name"))
                    .ToList();

                rptOffices.DataSource = sortItems;
                rptOffices.DataBind();
                rptOffices.Visible = true;
            }
        }

        protected void rptOffices_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Item item = (Item)e.Item.DataItem;
                Literal litOfficeName = (Literal)e.Item.FindControl("litOfficeName");
                Label lblPrimary = (Label)e.Item.FindControl("lblPrimary");
                Literal litAddress = (Literal)e.Item.FindControl("litAddress");
                Literal litPhoneLabel = (Literal)e.Item.FindControl("litPhoneLabel");
                Literal litPhone = (Literal)e.Item.FindControl("litPhone");
                Literal litFaxLabel = (Literal)e.Item.FindControl("litFaxLabel");
                Literal litFax = (Literal)e.Item.FindControl("litFax");
                Literal litEmailLabel = (Literal)e.Item.FindControl("litEmailLabel");
                Literal litAcceptingPatients = (Literal)e.Item.FindControl("litAcceptingPatients");
                Literal litOfficeHours = (Literal)e.Item.FindControl("litOfficeHours");
                Literal litDistance = (Literal)e.Item.FindControl("litDistance");
                Literal litDistanceLabel = (Literal)e.Item.FindControl("litDistanceLabel");
                HyperLink hlEmail = (HyperLink)e.Item.FindControl("hlEmail");
                Label lblSeparator = (Label)e.Item.FindControl("lblSeparator");
                HyperLink hlMap = (HyperLink)e.Item.FindControl("hlMap");
                Link scLinkOfficeWebsite = (Link)e.Item.FindControl("scLinkOfficeWebsite");
                Panel pnlOfficeName = (Panel)e.Item.FindControl("pnlOfficeName");
                Panel pnlAddress = (Panel)e.Item.FindControl("pnlAddress");
                Panel pnlPhone = (Panel)e.Item.FindControl("pnlPhone");
                Panel pnlFax = (Panel)e.Item.FindControl("pnlFax");
                Panel pnlEmail = (Panel)e.Item.FindControl("pnlEmail");
                Panel pnlOfficeLink = (Panel)e.Item.FindControl("pnlOfficeLink");
                Panel pnlAcceptingPatients = (Panel)e.Item.FindControl("pnlAcceptingPatients");
                Panel pnlOfficeHours = (Panel)e.Item.FindControl("pnlOfficeHours");
                Panel pnlDistance = (Panel)e.Item.FindControl("pnlDistance");

                if (item != null)
                {
                    string strOfficeName = ItemHelper.GetFieldHtmlValue(item, "Office Name");
                    string strPhone = ItemHelper.GetFieldHtmlValue(item, "Office Phone");
                    string strFax = ItemHelper.GetFieldHtmlValue(item, "Office Fax");
                    string strEmail = ItemHelper.GetFieldRawValue(item, "Email Address");
                    string strAcceptingPatients = ItemHelper.GetFieldRawValue(item, "Accepting New Patients");
                    string strOfficeHours = ItemHelper.GetFieldRawValue(item, "Office Hours");

                    Item locationItem = ItemHelper.GetSingleItemFromReferenceField(item, "Location");
                    string strLocationName = ItemHelper.GetFieldHtmlValue(locationItem, "Location Name");
                    string strAddress1 = ItemHelper.GetFieldHtmlValue(locationItem, "Address 1");
                    string strAddress2 = ItemHelper.GetFieldHtmlValue(locationItem, "Address 2");
                    string strCity = ItemHelper.GetFieldHtmlValue(locationItem, "City");
                    string strZip = ItemHelper.GetFieldHtmlValue(locationItem, "Zip");
                    string strMainPhone = ItemHelper.GetFieldHtmlValue(locationItem, "Main Phone");
                    string strMainFax = ItemHelper.GetFieldHtmlValue(locationItem, "Main Fax");
                    string strState = ItemHelper.GetFieldHtmlValueFromReferencedItem(locationItem, "State", "Abbreviation");

                    string strPrimary = ItemHelper.GetFieldRawValue(item, "Primary Office");


                    string strLabel =
    CultureHelper.GetDictionaryTranslation(
        "Modules.PhysicianDirectory.PhysicianOffices.OfficeLocationsLabel");
                    Literal litOfficesTitle = (Literal)e.Item.FindControl("litOfficesTitle");
                    Panel pnlOfficesTitle = (Panel)e.Item.FindControl("pnlOfficesTitle");
                    if (!string.IsNullOrEmpty(strLabel) && litOfficesTitle != null && pnlOfficesTitle != null)
                    {
                        litOfficesTitle.Text = strLabel;
                        pnlOfficesTitle.Visible = true;
                    }


                    // use Office Name if available, otherwise, use Location Name
                    if (litOfficeName != null &&
                        (!string.IsNullOrEmpty(strLocationName) || !string.IsNullOrEmpty(strOfficeName)))
                    {
                        litOfficeName.Text = string.IsNullOrEmpty(strOfficeName) ? strLocationName : strOfficeName;
                        if (strPrimary == "1" && lblPrimary != null)
                            lblPrimary.Text =
                                CultureHelper.GetDictionaryTranslation(
                                    "Modules.PhysicianDirectory.PhysicianOffices.Primary");
                        pnlOfficeName.Visible = true;
                    }

                    // use Office Phone if available, otherwise, use Main Phone
                    if (litPhone != null && litPhoneLabel != null &&
                        (!string.IsNullOrEmpty(strPhone) || !string.IsNullOrEmpty(strMainPhone)))
                    {
                        litPhone.Text = string.IsNullOrEmpty(strPhone) ? strMainPhone : strPhone;
                        litPhoneLabel.Text =
                            CultureHelper.GetDictionaryTranslation(
                                "Modules.PhysicianDirectory.PhysicianOffices.LabelPhone");
                        pnlPhone.Visible = true;
                    }

                    // use Office Fax if available, otherwise, use Main Fax
                    if (litFax != null && litFaxLabel != null &&
                        (!string.IsNullOrEmpty(strFax) || !string.IsNullOrEmpty(strMainFax)))
                    {
                        litFax.Text = string.IsNullOrEmpty(strFax) ? strMainFax : strFax;
                        litFaxLabel.Text =
                            CultureHelper.GetDictionaryTranslation(
                                "Modules.PhysicianDirectory.PhysicianOffices.LabelFax");
                        pnlFax.Visible = true;
                    }

                    if (hlEmail != null && litEmailLabel != null && !string.IsNullOrEmpty(strEmail))
                    {
                        hlEmail.Text = strEmail;
                        hlEmail.NavigateUrl = "mailto:" + strEmail;
                        litEmailLabel.Text =
                            CultureHelper.GetDictionaryTranslation(
                                "Modules.PhysicianDirectory.PhysicianOffices.LabelEmail");
                        pnlEmail.Visible = true;
                    }

                    if (litAddress != null &&
                        (!string.IsNullOrEmpty(strAddress1) || !string.IsNullOrEmpty(strAddress2) ||
                         !string.IsNullOrEmpty(strCity) || !string.IsNullOrEmpty(strState) ||
                         !string.IsNullOrEmpty(strZip)))
                    {
                        litAddress.Text = StringHelper.GetAddressString(strAddress1, strAddress2, strCity, strState,
                                                                        strZip, false);
                        pnlAddress.Visible = true;

                        if (hlMap != null)
                        {
                            string oneLineAddress = StringHelper.GetAddressString(strAddress1, string.Empty, strCity,
                                                                                  strState, strZip, true);
                            hlMap.NavigateUrl = MapHelper.GetMapLink(oneLineAddress);
                            hlMap.Text =
                                CultureHelper.GetDictionaryTranslation(
                                    "Modules.PhysicianDirectory.PhysicianOffices.LinkOfficeMap");
                            hlMap.Visible = true;
                            pnlOfficeLink.Visible = true;
                        }
                    }

                    if (pnlAcceptingPatients != null && litAcceptingPatients != null && strAcceptingPatients == "1")
                    {
                        litAcceptingPatients.Text =
                            CultureHelper.GetDictionaryTranslation(
                                "Modules.PhysicianDirectory.PhysicianOffices.AcceptingNewPatients");
                        pnlAcceptingPatients.Visible = true;
                    }

                    if (pnlOfficeHours != null && litOfficeHours != null && !string.IsNullOrEmpty(strOfficeHours))
                    {
                        litOfficeHours.Text = strOfficeHours;
                        pnlOfficeHours.Visible = true;
                    }

                    //Display distance only on Accurate Search and search by Zip and Range.
                    if (pnlDistance != null && litDistance != null && !string.IsNullOrEmpty(ZipQueryString) &&
                        !string.IsNullOrEmpty(RangeQueryString) && AccurateRangeSearch && ZipPoint != null)
                    {
                        //Get Office Location.
                        double latitude, longitude, rangeSearch;
                        Double.TryParse(ItemHelper.GetFieldRawValue(locationItem, "Latitude"), out latitude);
                        Double.TryParse(ItemHelper.GetFieldRawValue(locationItem, "Longitude"), out longitude);
                        Double.TryParse(RangeQueryString, out rangeSearch);
                        var officeLocation = new Point(latitude, longitude);

                        //Calculate Distance
                        double distance = GoogleMapHelper.CalcDistance(officeLocation, ZipPoint,
                                                                       GoogleMapHelper.GeoCodeCalcMeasurement.Miles);

                        //Create Distance Text.
                        string distanceTxt;
                        if (distance > rangeSearch)
                        {
                            distanceTxt = CultureHelper.GetDictionaryTranslation(
                                "Modules.PhysicianDirectory.PhysicianOffices.LabelDistanceOutOfRange");
                        }
                        else
                        {
                            distanceTxt = string.Format("{0} {1}",
                                                        Math.Round(distance, 2).ToString(CultureInfo.InvariantCulture),
                                                        CultureHelper.GetDictionaryTranslation(
                                                            "Modules.PhysicianDirectory.PhysicianOffices.LabelDistanceUnit"));
                        }

                        litDistanceLabel.Text = CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianOffices.LabelDistance");
                        litDistance.Text = distanceTxt;
                        pnlDistance.Visible = true;
                    }

                    if (scLinkOfficeWebsite != null)
                    {
                        string strWebsite = ItemHelper.GetFieldRawValue(locationItem, ItemMapper.Location.FieldName.Website);
                        if (!string.IsNullOrEmpty(strWebsite))
                        {
                            scLinkOfficeWebsite.Item = locationItem;
                            scLinkOfficeWebsite.Text =
                                CultureHelper.GetDictionaryTranslation(
                                    "Modules.PhysicianDirectory.PhysicianOffices.LinkOfficeWebsite");
                            scLinkOfficeWebsite.Visible = true;
                            pnlOfficeLink.Visible = true;
                        }
                    }

                    if (lblSeparator != null && hlMap != null && scLinkOfficeWebsite != null)
                        lblSeparator.Visible = !string.IsNullOrEmpty(hlMap.Text) &&
                                               !string.IsNullOrEmpty(scLinkOfficeWebsite.Text);
                }
            }
            else if (e.Item.ItemType == ListItemType.Header)
            {

            }
        }
    }
}