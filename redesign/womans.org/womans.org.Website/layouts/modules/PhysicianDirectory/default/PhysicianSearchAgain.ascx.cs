﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
using MedTouch.PhysicianDirectory.Helpers;
using MedTouch.PhysicianDirectory.Helpers.Links;
using MedTouch.PhysicianDirectory.Search;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using womans.org.BusinessLogic.Helpers;
using womans.org.BusinessLogic.PhysicianDirectory.Search;

namespace womans.org.Website.layouts.modules.PhysicianDirectory
{


    /// <summary>
    /// Summary description for PhysiciansearchagainSublayout
    /// </summary>
    public partial class PhysiciansearchagainSublayout : BaseSublayout
    {
        #region Fields
        private string dictionarySearchByKeywordLabel;
        private string dictionarySearchByKeywordWatermark;
        private string dictionarySearchByFirstNameLabel;
        private string dictionarySearchByFirstNameWatermark;
        private string dictionarySearchByFullNameLabel;
        private string dictionarySearchByFullNameWaterMark;
        private string dictionarySearchByLastNameLabel;
        private string dictionarySearchByLastNameWatermark;
        private string dictionarySearchBySpecialtyLabel;
        private string dictionarySearchByDepartmentLabel;
        private string dictionarySearchByLanguageLabel;
        private string dictionarySearchByGenderLabel;
        private string dictionarySearchByRangeLabel;
        private string dictionarySearchByZipLabel;
        private string dictionarySearchByZipInvalidLabel;
        private string dictionarySearchByZipWaterMark;
        private string dictionarySearchByZipRangeRequiredLabel;
        private string dictionarySearchByCityLabel;

        private string dictionaryCriteriaRequired;

        private string dictionarySearchButtonText;
        private string dictionaryControlHeaderLabel;
        private string dictionaryResetButtonText;

        private string dictionarySearchByPhysicianGroupLabel;

        private Item renderingParamSearchResultsPage;
        #endregion Fields

        #region properties
        protected string DictionaryControlHeaderLabel
        {
            get
            {
                if (this.dictionaryControlHeaderLabel == null)
                {
                    this.dictionaryControlHeaderLabel =
                        CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianSearchAgain.PhysicianSearchAgainLabel");
                }

                return this.dictionaryControlHeaderLabel;
            }
        }

        public static string SpecialtySearchFolderName
        {
            get
            {
                return "{A7D5609C-6FF0-432F-B6D6-5836199EED92}";
            }
        }

        public static string SpecialtySearchTemplateGuid
        {
            get
            {
                return "{F13CD9DF-B762-4E72-8706-5D325EB1F223}";
            }
        }

        protected string DictionarySearchByPhysicianGroupLabel
        {
            get
            {
                if (this.dictionarySearchByPhysicianGroupLabel == null)
                {
                    this.dictionarySearchByPhysicianGroupLabel =
                        CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianSearch.PhysicianGroupLabel");
                }

                return this.dictionarySearchByPhysicianGroupLabel;
            }
        }

        protected string DictionarySearchByKeywordLabel
        {
            get
            {
                if (this.dictionarySearchByKeywordLabel == null)
                {
                    this.dictionarySearchByKeywordLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.KeywordLabel");
                }

                return this.dictionarySearchByKeywordLabel;
            }
        }

        protected string DictionarySearchByKeywordWatermark
        {
            get
            {
                if (this.dictionarySearchByKeywordWatermark == null)
                {
                    this.dictionarySearchByKeywordWatermark =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.KeywordWatermark");
                }

                return this.dictionarySearchByKeywordWatermark;
            }
        }

        protected string DictionarySearchByFirstNameLabel
        {
            get
            {
                if (this.dictionarySearchByFirstNameLabel == null)
                {
                    this.dictionarySearchByFirstNameLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.FirstNameLabel");
                }

                return this.dictionarySearchByFirstNameLabel;
            }
        }

        protected string DictionarySearchByFirstNameWatermark
        {
            get
            {
                if (this.dictionarySearchByFirstNameWatermark == null)
                {
                    this.dictionarySearchByFirstNameWatermark =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.FirstNameWatermark");
                }

                return this.dictionarySearchByFirstNameWatermark;
            }
        }

        protected string DictionarySearchByFullNameLabel
        {
            get
            {
                if (this.dictionarySearchByFullNameLabel == null)
                {
                    this.dictionarySearchByFullNameLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.NameLabel");
                }

                return this.dictionarySearchByFullNameLabel;
            }
        }

        protected string DictionarySearchByFullNameWaterMark
        {
            get
            {
                if (this.dictionarySearchByFullNameWaterMark == null)
                {
                    this.dictionarySearchByFullNameWaterMark =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.NameWatermark");
                }

                return this.dictionarySearchByFullNameWaterMark;
            }
        }

        protected string DictionarySearchByLastNameLabel
        {
            get
            {
                if (this.dictionarySearchByLastNameLabel == null)
                {
                    this.dictionarySearchByLastNameLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.LastNameLabel");
                }

                return this.dictionarySearchByLastNameLabel;
            }
        }

        protected string DictionarySearchByLastNameWatermark
        {
            get
            {
                if (this.dictionarySearchByLastNameWatermark == null)
                {
                    this.dictionarySearchByLastNameWatermark =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.LastNameWatermark");
                }

                return this.dictionarySearchByLastNameWatermark;
            }
        }

        protected string DictionarySearchByCityLabel
        {
            get
            {
                if (this.dictionarySearchByCityLabel == null)
                {
                    this.dictionarySearchByCityLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.CityLabel");
                }

                return this.dictionarySearchByCityLabel;
            }
        }

        protected string DictionarySearchByDepartmentLabel
        {
            get
            {
                if (this.dictionarySearchByDepartmentLabel == null)
                {
                    this.dictionarySearchByDepartmentLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.DepartmentLabel");
                }

                return this.dictionarySearchByDepartmentLabel;
            }
        }

        protected string DictionarySearchByLanguageLabel
        {
            get
            {
                if (this.dictionarySearchByLanguageLabel == null)
                {
                    this.dictionarySearchByLanguageLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.LanguageLabel");
                }

                return this.dictionarySearchByLanguageLabel;
            }
        }

        protected string DictionarySearchByGenderLabel
        {
            get
            {
                if (this.dictionarySearchByGenderLabel == null)
                {
                    this.dictionarySearchByGenderLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.GenderLabel");
                }

                return this.dictionarySearchByGenderLabel;
            }
        }

        protected string DictionarySearchByRangeLabel
        {
            get
            {
                if (this.dictionarySearchByRangeLabel == null)
                {
                    this.dictionarySearchByRangeLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.RangeLabel");
                }

                return this.dictionarySearchByRangeLabel;
            }
        }

        protected string DictionarySearchBySpecialtyLabel
        {
            get
            {
                if (this.dictionarySearchBySpecialtyLabel == null)
                {
                    this.dictionarySearchBySpecialtyLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.SpecialtyLabel");
                }

                return this.dictionarySearchBySpecialtyLabel;
            }
        }

        protected string DictionarySearchByZipLabel
        {
            get
            {
                if (this.dictionarySearchByZipLabel == null)
                {
                    this.dictionarySearchByZipLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.ZipLabel");
                }

                return this.dictionarySearchByZipLabel;
            }
        }

        protected string DictionarySearchByZipInvalidLabel
        {
            get
            {
                if (this.dictionarySearchByZipInvalidLabel == null)
                {
                    this.dictionarySearchByZipInvalidLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.ZipInvalidLabel");
                }

                return this.dictionarySearchByZipInvalidLabel;
            }
        }

        protected string DictionarySearchByZipWaterMark
        {
            get
            {
                if (this.dictionarySearchByZipWaterMark == null)
                {
                    this.dictionarySearchByZipWaterMark =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.ZipWatermark");
                }

                return this.dictionarySearchByZipWaterMark;
            }
        }

        protected string DictionarySearchByZipRangeRequiredLabel
        {
            get
            {
                if (this.dictionarySearchByZipRangeRequiredLabel == null)
                {
                    this.dictionarySearchByZipRangeRequiredLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.ZipRangeRequiredLabel");
                }

                return this.dictionarySearchByZipRangeRequiredLabel;
            }
        }

        protected string DictionaryCriteriaRequired
        {
            get
            {
                if (this.dictionaryCriteriaRequired == null)
                {
                    this.dictionaryCriteriaRequired =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.CriteriaRequired");
                }

                return this.dictionaryCriteriaRequired;
            }
        }

        protected string DictionarySearchButtonText
        {
            get
            {
                if (this.dictionarySearchButtonText == null)
                {
                    this.dictionarySearchButtonText =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.SearchButtonText");
                }

                return this.dictionarySearchButtonText;
            }
        }

        protected string DictionaryResetButtonText
        {
            get
            {
                if (this.dictionaryResetButtonText == null)
                {
                    this.dictionaryResetButtonText =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianSearchAgain.ResetButtonText");
                }

                return this.dictionaryResetButtonText;
            }
        }

        protected Item RenderingParamSearchResultsPage
        {
            get
            {
                if (this.renderingParamSearchResultsPage == null)
                {
                    this.renderingParamSearchResultsPage = this.GetSingleReferenceProperty("Search Results Page");
                }

                return this.renderingParamSearchResultsPage;
            }
        }
        #endregion properties

        protected override void Bind()
        {
            this.lblPhysicianSearchAgain.Text = this.DictionaryControlHeaderLabel;
            this.phControlHeader.Visible = !string.IsNullOrEmpty(this.DictionaryControlHeaderLabel);

            this.lblKeyword.Text = this.DictionarySearchByKeywordLabel;
            this.lblKeyword.Visible = !string.IsNullOrEmpty(this.DictionarySearchByKeywordLabel);
            string keyword = this.Request.QueryString[ModuleHelper.KeywordQuerystring()];
            UIHelper.SetTextboxWatermark(this.txtKeyword, keyword, this.DictionarySearchByKeywordWatermark);

            this.lblNameAutoComplete.Text = this.DictionarySearchByFullNameLabel;
            this.lblNameAutoComplete.Visible = !string.IsNullOrEmpty(this.DictionarySearchByFullNameLabel);
            UIHelper.SetTextboxWatermarkPlaceholder(this.txtNameAutoComplete, string.Empty, this.DictionarySearchByFullNameWaterMark);
            this.BindAutoCompleteControl();

            this.lblFirstName.Text = this.DictionarySearchByFirstNameLabel;
            this.lblFirstName.Visible = !string.IsNullOrEmpty(this.DictionarySearchByFirstNameLabel);
            string firstname = this.Request.QueryString[ModuleHelper.FirstNameQuerystring()];
            UIHelper.SetTextboxWatermark(this.txtFirstName, firstname, this.DictionarySearchByFirstNameWatermark);

            this.lblLastName.Text = this.DictionarySearchByLastNameLabel;
            this.lblLastName.Visible = !string.IsNullOrEmpty(this.DictionarySearchByLastNameLabel);
            string lastname = this.Request.QueryString[ModuleHelper.LastNameQuerystring()];
            UIHelper.SetTextboxWatermark(this.txtLastName, lastname, this.DictionarySearchByLastNameWatermark);

            this.lblSpecialty.Text = this.DictionarySearchBySpecialtyLabel;
            this.lblSpecialty.Visible = !string.IsNullOrEmpty(this.DictionarySearchBySpecialtyLabel);
            this.BindSpecialtyDropdown();

            this.lblLocations.Text = this.DictionarySearchByPhysicianGroupLabel;
            this.lblLocations.Visible = !string.IsNullOrEmpty(this.DictionarySearchByPhysicianGroupLabel);
            this.BindLocationsDropDown();

            this.lblDepartment.Text = this.DictionarySearchByDepartmentLabel;
            this.lblDepartment.Visible = !string.IsNullOrEmpty(this.DictionarySearchByDepartmentLabel);
            this.BindDepartmentDropdown();

            this.lblLanguage.Text = this.DictionarySearchByLanguageLabel;
            this.lblLanguage.Visible = !string.IsNullOrEmpty(this.DictionarySearchByLanguageLabel);
            this.BindLanguageDropdown();

            this.lblGender.Text = this.DictionarySearchByGenderLabel;
            this.lblGender.Visible = !string.IsNullOrEmpty(this.DictionarySearchByGenderLabel);
            this.BindGenderDropdown();

            this.lblZip.Text = this.DictionarySearchByZipLabel;
            this.lblZip.Visible = !string.IsNullOrEmpty(this.DictionarySearchByZipLabel);
            string zip = this.Request.QueryString[ModuleHelper.ZipQuerystring()];
            UIHelper.SetTextboxWatermark(this.txtZip, zip, this.DictionarySearchByZipWaterMark);
            this.BindZipValidationControl();

            this.lblRange.Text = this.DictionarySearchByRangeLabel;
            this.lblRange.Visible = !string.IsNullOrEmpty(this.DictionarySearchByRangeLabel);
            this.BindRangeDropdown();
            // If Rang is Visible, Zipcode need to be Visible.
            if (this.lblRange.Visible)
            {
                this.lblZip.Visible = true;
            }

            this.lblCity.Text = this.DictionarySearchByCityLabel;
            this.lblCity.Visible = !string.IsNullOrEmpty(this.DictionarySearchByCityLabel);
            this.BindCityDropDown();

            this.btnSubmit.Text = this.DictionarySearchButtonText;
            this.lbSubmit.Text = this.DictionarySearchButtonText;

            this.BindAtoZList();
        }

        protected void BindAtoZList()
        {
            if (!this.blAtoZ.Enabled || !this.blAtoZ.Visible)
            {
                return;
            }

            Item searchResultsPage =
                ModuleHelper.GetSearchResultPageItem(this.RenderingParamSearchResultsPage);

            string firstInitiLastName = this.Request.QueryString[ModuleHelper.FirstInitLastNameQuerystring()];

            if (searchResultsPage != null)
            {
                ModuleHelper.BindAlphabeticalList(this.blAtoZ, firstInitiLastName, searchResultsPage);
            }
        }

        protected void BindSpecialtyDropdown()
        {
            if (!this.ddlSpecialty.Enabled || !this.ddlSpecialty.Visible)
            {
                return;
            }

            string strQuerystringSpecialty = Request.QueryString[BaseModuleHelper.SpecialtiesQuerystring()];
            PhysicianHelper.BindSpecialtySearchDropDown(this.ddlSpecialty, strQuerystringSpecialty);

            //Item item = Sitecore.Context.Database.GetItem(SpecialtySearchFolderName);
            //ListItemCollection listItems = new ListItemCollection();
            //int counter = 1;
            //int selectedIndex = 0;

            //if (item != null)
            //{
            //    foreach (Item itm in item.GetChildren())
            //    {
            //        string specialties = string.Empty;
            //        if (itm.TemplateID.ToString() == SpecialtySearchTemplateGuid)
            //        {
            //            MultilistField mfSpecialties = (MultilistField)(itm.Fields["Specialties"]);
            //            foreach (Item i in mfSpecialties.GetItems())
            //            {
            //                if (string.IsNullOrWhiteSpace(specialties))
            //                {
            //                    specialties = i.Name;
            //                }
            //                else
            //                {
            //                    specialties += "|" + i.Name;
            //                }
            //            }

            //            ListItem li = new ListItem(itm.Fields["Specialty Name"].Value, specialties);
            //            listItems.Add(li);
            //            if (li.Value == strQuerystringSpecialty)
            //            {
            //                selectedIndex = counter;
            //            }
            //            counter++;
            //        }
            //    }
            //}

            //ListItem listItem = new ListItem(CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianSearch.AllSpecialtiesText"), "");
            //listItems.Insert(0, listItem);

            //ddlSpecialty.DataSource = listItems;
            //ddlSpecialty.DataBind();
            //ddlSpecialty.SelectedIndex = selectedIndex;
        }

        protected void BindDepartmentDropdown()
        {
            if (!this.ddlDepartment.Enabled || !this.ddlDepartment.Visible)
            {
                return;
            }

            string strQuerystringDepartment = Request.QueryString[ModuleHelper.DepartmentsQuerystring()];
            ModuleHelper.BindDepartmentDropdown(this.ddlDepartment, strQuerystringDepartment);
        }

        protected void BindLocationsDropDown()
        {
            if (!this.ddlLocations.Enabled || !this.ddlLocations.Visible)
            {
                return;
            }

            // Check Locations Tag. 
            string strLocations = ItemHelper.GetItemNamesFromItemGuids(ItemHelper.GetFieldRawValue(ContextItem, "Locations"));
            if (string.IsNullOrEmpty(strLocations))
            {
                //string strQuerystringPhysGroup = this.Request.QueryString[BaseModuleHelper.LocationsQuerystring()];
                //ModuleHelper.BindPhysicianFilterDropdown(this.ddlLocations, strQuerystringPhysGroup, CultureHelper.GetDictionaryTranslation("Modules.PhysicianDirectory.PhysicianSearch.AllLocationsText"), "{BFE5CC2B-F045-4A05-8B9A-1BC4B5F49CB6}", "{21931248-664D-4B33-A3E8-426B8694D7EB}", "Headline");
                string strQuerystringPhysGroup = this.Request.QueryString[PhysicianHelper.GroupQuerystring()];
                PhysicianHelper.BindGroupDropDown(this.ddlLocations, strQuerystringPhysGroup);
            }
            else
            {
                this.pnlLocations.Visible = false;
            }
        }

        protected void BindLanguageDropdown()
        {
            if (!this.ddlLanguage.Enabled || !this.ddlLanguage.Visible)
            {
                return;
            }

            string strQuerystringLanguage = Request.QueryString[ModuleHelper.LanguagesQuerystring()];
            ModuleHelper.BindLanguageDropdown(this.ddlLanguage, strQuerystringLanguage);
        }

        protected void BindGenderDropdown()
        {
            if (!this.ddlGender.Enabled || !this.ddlGender.Visible)
            {
                return;
            }

            string strQuerystringGender = Request.QueryString[ModuleHelper.GenderQuerystring()];
            ModuleHelper.BindGenderDropdown(this.ddlGender, strQuerystringGender);
        }

        protected void BindRangeDropdown()
        {
            if (!this.ddlRange.Enabled || !this.ddlRange.Visible)
            {
                return;
            }

            string strQuerystringRange = Request.QueryString[BaseModuleHelper.RangeQuerystring()];
            ModuleHelper.BindRangeDropdown(this.ddlRange, strQuerystringRange);
        }

        protected void BindCityDropDown()
        {
            if (!this.ddlCity.Enabled || !this.ddlCity.Visible)
            {
                return;
            }

            string strQuerystringCity = Request.QueryString[ModuleHelper.CityQuerystring()];
            ModuleHelper.BindCityDropdown(this.ddlCity, strQuerystringCity);
        }

        protected void BindZipValidationControl()
        {
            // Zip Regex Control
            this.regexVldZip.ErrorMessage = this.DictionarySearchByZipInvalidLabel;
            this.regexVldZip.ValidationExpression = StringHelper.BuildZipRegexPattern(this.DictionarySearchByZipWaterMark);

            // Zip and Range Validation
            this.ctmVldZipRange.ErrorMessage = this.DictionarySearchByZipRangeRequiredLabel;
        }

        protected void BindAutoCompleteControl()
        {
            if (!this.txtNameAutoComplete.Enabled || !this.txtNameAutoComplete.Visible)
            {
                return;
            }

            // Serialize autocomplete control
            List<string> namesList = ModuleHelper.BuildPredictiveSearch();

            if (namesList.Count > 0)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var serializedNames = js.Serialize(namesList);

                string serializedListText = "var wordlist=" + serializedNames + ";";
                Page.ClientScript.RegisterClientScriptBlock(
                    this.GetType(), "registerserializedlist", serializedListText, true);
            }

            string strQuerystringFullName = Request.QueryString[ModuleHelper.FullNameQuerystring()];
            if (!string.IsNullOrEmpty(strQuerystringFullName))
            {
                this.txtNameAutoComplete.Text = strQuerystringFullName;
            }
        }

        protected void ibSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            this.SearchSubmitted();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            this.SearchSubmitted();
        }

        protected void lbSubmit_Click(object sender, EventArgs e)
        {
            this.SearchSubmitted();
        }

        protected virtual void SearchSubmitted()
        {
            string firstname = this.txtFirstName.Text.Trim();
            if (firstname.Equals(this.DictionarySearchByFirstNameWatermark, StringComparison.OrdinalIgnoreCase))
            {
                firstname = string.Empty;
            }

            string lastname = this.txtLastName.Text.Trim();
            if (lastname.Equals(this.DictionarySearchByLastNameWatermark, StringComparison.OrdinalIgnoreCase))
            {
                lastname = string.Empty;
            }

            string zip = this.txtZip.Text.Trim();
            if (zip.Equals(this.DictionarySearchByZipWaterMark, StringComparison.OrdinalIgnoreCase))
            {
                zip = string.Empty;
            }

            string fullName = this.txtNameAutoComplete.Text.Trim();
            if (fullName.Equals(this.DictionarySearchByFullNameWaterMark, StringComparison.OrdinalIgnoreCase))
            {
                fullName = string.Empty;
            }

            string taxonomy = this.txtKeyword.Text.Trim();
            if (taxonomy.Equals(this.DictionarySearchByKeywordWatermark, StringComparison.OrdinalIgnoreCase))
            {
                taxonomy = string.Empty;
            }

            string specialties = this.ddlSpecialty.SelectedValue;
            string departments = this.ddlDepartment.SelectedValue;
            string languages = this.ddlLanguage.SelectedValue;
            string gender = this.ddlGender.SelectedValue;
            string range = this.ddlRange.SelectedValue;
            string cities = this.ddlCity.SelectedValue;
            //string locations = this.ddlLocations.SelectedValue;
            string group = this.ddlLocations.SelectedValue;

            if (specialties != string.Empty)
            {
                Sitecore.Diagnostics.Log.Error("Specialties: " + specialties, new Exception());
            }

            //if (!string.IsNullOrEmpty(taxonomy) || !string.IsNullOrEmpty(firstname) || !string.IsNullOrEmpty(lastname)
            //    || !string.IsNullOrEmpty(specialties) || !string.IsNullOrEmpty(languages) || !string.IsNullOrEmpty(gender)
            //    || !string.IsNullOrEmpty(zip) || !string.IsNullOrEmpty(range) || !string.IsNullOrEmpty(departments) 
            //    || !string.IsNullOrEmpty(cities) || !string.IsNullOrEmpty(fullName))
            //{
                Item searchResultsPage = ModuleHelper.GetSearchResultPageItem(this.RenderingParamSearchResultsPage);

                //var physicianSearchParam = new PhysicianSearchParam()
                //{
                //    Keyword = string.Empty,
                //    Taxonomies = taxonomy,
                //    FirstName = firstname,
                //    LastName = lastname,
                //    Zip = zip,
                //    FullName = fullName,
                //    Specialties = specialties,
                //    Departments = departments,
                //    Languages = languages,
                //    Gender = gender,
                //    Range = range,
                //    Cities = cities,
                //    Locations = locations
                //};
                var physicianSearchParam = new WomansPhysicianSearchParam()
                {
                    Keyword = string.Empty,
                    Taxonomies = taxonomy,
                    FirstName = firstname,
                    LastName = lastname,
                    FullName = fullName,
                    Specialties = specialties,
                    Departments = departments,
                    Languages = languages,
                    Gender = gender,
                    Zip = zip,
                    Range = range,
                    Group = group,
                    Cities = cities
                };

                //ILinkHelper linkHelper = ModuleHelper.GetLinkHelper();
                ILinkHelper linkHelper = PhysicianHelper.GetLinkHelper();

                linkHelper.SubmitSearch(physicianSearchParam, searchResultsPage);
            //}
            //else
            //{
            //    litError.Text = _criteriaRequiredTxt;
            //    pnlError.Visible = true;
            //}
        }
    }
}