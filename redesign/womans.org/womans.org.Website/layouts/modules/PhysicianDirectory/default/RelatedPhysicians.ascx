﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.PhysicianDirectory.layouts.modules.PhysicianDirectory.RelatedphysiciansSublayout"
    CodeBehind="RelatedPhysicians.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: Physician Directory: Related Physicians =============================-->
<asp:Panel runat="server" ID="pnlControlWrapper" CssClass="module-pd-related callout collapse-for-mobile">
    <div class="reg-callout grid">
        <asp:PlaceHolder ID="phControlHeader" runat="server">
            <h3 class="module-heading"><asp:Literal ID="lblControlHeader" runat="server" /></h3>
        </asp:PlaceHolder>
        <asp:Panel ID="pnlListing" runat="server" CssClass="listing">
            <ul class="core-list">
                <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_ItemDataBound">
                    <ItemTemplate>

                    <li class="core-li grid">
    
                            <asp:Panel ID="pnlThumbnail" runat="server" Visible="false" CssClass="list-item-image">
                                <sc:Image ID="scThumbnail" Field="Photo" runat="server" MaxWidth="75" MaxHeight="75" />
                            </asp:Panel>
                            <div class="list-item-copy">
                                <h5><asp:HyperLink ID="hlItem" runat="server" /><asp:Literal ID="litItem" runat="server" /></h5>
                                <asp:Panel ID="pnlSpecialties" runat="server" Visible="false">
                                    <asp:Literal ID="litSpecialties" runat="server" />
                                </asp:Panel>
                                <asp:Panel ID="pnlMoreLink" runat="server" Visible="false" CssClass="listing-item-more-link">
                                    <asp:HyperLink ID="hlMoreLink" runat="server" />
                                </asp:Panel>
                            </div>
                    </li>
                    </ItemTemplate>
                </asp:ListView>
            </ul>
        </asp:Panel>
        <asp:Panel ID="pnlViewAll" runat="server" Visible="false" CssClass="module-pd-view-all">
            <asp:HyperLink ID="hlViewAll" runat="server" />
        </asp:Panel>
    </div>
</asp:Panel>
<!--==================== /Module: Physician Directory: Related Physicians =============================-->