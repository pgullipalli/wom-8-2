﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhysicianSearch.ascx.cs" Inherits="womans.org.Website.layouts.modules.PhysicianDirectory.PhysicianSearch" %>

<%@ Import Namespace="MedTouch.Common.Helpers" %>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
<%--<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/excite-bike/jquery-ui.css" />--%>
<!--==================== Module: Physician Directory: Physician Search =============================-->

<asp:Panel ID="pnlControlWrapper" runat="server" DefaultButton="btnSubmit" CssClass="module-pd-search core-search twelve columns grid">
    <asp:PlaceHolder runat="server" ID="phControlHeader">
       <h3>
            <asp:Literal ID="litPhysicianSearch" runat="server" /></h3>
    </asp:PlaceHolder>
    <asp:Panel ID="pnlKeyword" runat="server" CssClass="search-option" Visible="false">
        <asp:Label ID="lblKeyword" runat="server" AssociatedControlID="txtKeyword" CssClass="label" />
        <asp:TextBox ID="txtKeyword" runat="server" CssClass="search textbox" />
    </asp:Panel>
    <asp:Panel ID="pnlNameAutoComplete" runat="server" CssClass="six columns">
        <asp:Label ID="lblNameAutoComplete" runat="server" AssociatedControlID="txtNameAutoComplete" CssClass="label" />
        <asp:TextBox ID="txtNameAutoComplete" runat="server" CssClass="textbox autocomplete" />
    </asp:Panel>
    <asp:Panel ID="pnlSpecialty" runat="server" CssClass="six columns">
        <asp:Label ID="lblSpecialty" runat="server" AssociatedControlID="ddlSpecialty" CssClass="label" />
        <div class="selectbox">
        <asp:DropDownList ID="ddlSpecialty" runat="server" CssClass="selectboxdiv" DataValueField="value" DataTextField="Text" />
        <div class="out"></div>

        </div>
    </asp:Panel>

    <section class="form-toggle">

    <asp:Panel ID="pnlGender" runat="server" CssClass="six columns">
        <asp:Label ID="lblGender" runat="server" AssociatedControlID="ddlGender" CssClass="label" />
        <div class="selectbox">
        <asp:DropDownList ID="ddlGender" runat="server" CssClass="selectboxdiv" />
        <div class="out"></div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlCity" runat="server" CssClass="six columns">
        <asp:Label ID="lblCity" runat="server" AssociatedControlID="ddlCity" CssClass="label" />
        <div class="selectbox">
        <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectboxdiv" />
        <div class="out"></div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlPhysicianGroup" runat="server" CssClass="six columns" Visible="true">
        <asp:Label ID="lblPhysicianGroup" runat="server" AssociatedControlID="ddlPhysicianGroup" CssClass="label" />
        <div class="selectbox">
        <asp:DropDownList ID="ddlPhysicianGroup" runat="server" CssClass="selectboxdiv" />
        <div class="out"></div>
        </div>
    </asp:Panel>



    <asp:Panel ID="pnlLastName" runat="server" CssClass="search-option hide-onload" Visible="false">
        <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName" CssClass="label" />
        <asp:TextBox ID="txtLastName" runat="server" CssClass="textbox" />
    </asp:Panel>
    <asp:Panel ID="pnlFirstName" runat="server" CssClass="search-option hide-onload" Visible="false">
        <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName" CssClass="label" />
        <asp:TextBox ID="txtFirstName" runat="server" CssClass="textbox" />
    </asp:Panel>
    <asp:Panel ID="pnlDepartment" runat="server" CssClass="search-option hide-onload" Visible="false">
        <asp:Label ID="lblDepartment" runat="server" AssociatedControlID="ddlDepartment"
            CssClass="label" />
        <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="styled" />
    </asp:Panel>
    <asp:Panel ID="pnlLanguage" runat="server" CssClass="search-option hide-onload" Visible="false">
        <asp:Label ID="lblLanguage" runat="server" AssociatedControlID="ddlLanguage" CssClass="label" />
        <asp:DropDownList ID="ddlLanguage" runat="server" CssClass="styled" />
    </asp:Panel>
    <asp:Panel ID="pnlZip" runat="server" CssClass="search-option hide-onload" Visible="false">
        <div class="half">
            <asp:Label ID="lblZip" runat="server" AssociatedControlID="txtZip" CssClass="label" />
            <asp:TextBox ID="txtZip" runat="server" CssClass="textbox" />
            <asp:RegularExpressionValidator ID="regexVldZip" ControlToValidate="txtZip" Display="dynamic"
                runat="server" CssClass="errortext" ValidationGroup="vldGrpSearch" />
            <asp:CustomValidator ID="ctmVldZipRange" runat="server" ClientValidationFunction="Search_ValidateZipAndRange"
                CssClass="errortext" Display="Dynamic" ValidationGroup="vldGrpSearch" />
        </div>
        <asp:Panel ID="pnlRange" runat="server" CssClass="half right hide-onload">
            <asp:Label ID="lblRange" runat="server" AssociatedControlID="ddlRange" CssClass="label" />
            <asp:DropDownList ID="ddlRange" runat="server" CssClass="styled" />
            <div class="out"></div>
        </asp:Panel>
    </asp:Panel>
    </section>

    <div class="grid twelve columns toggle-hide-show"> 
		<a href="#"><span class="show">Advanced Search +</span></a>
	</div>	


    <asp:Panel ID="pnlError" runat="server" Visible="false" CssClass="errortext">
        <asp:Literal ID="litError" runat="server" /></asp:Panel>


    <div class="search-option-submit twelve columns">

    <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" ValidationGroup="vldGrpSearch"
        CssClass="button default-button pink" />
    <asp:ImageButton ID="ibSubmit" runat="server" OnClick="ibSubmit_Click" Visible="false"
        ValidationGroup="vldGrpSearch" />
    <asp:LinkButton ID="lbSubmit" runat="server" Visible="false" OnClick="lbSubmit_Click"
        ValidationGroup="vldGrpSearch" />
        </div>
</asp:Panel>
<!--==================== Module: Physician Directory: Physician Search =============================-->
