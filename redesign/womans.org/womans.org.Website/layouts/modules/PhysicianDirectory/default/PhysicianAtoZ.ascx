﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.PhysicianDirectory.layouts.modules.PhysicianDirectory.PhysicianatozSublayout" CodeBehind="PhysicianAtoZ.ascx.cs" %>
<!--==================== Module: Physician Directory: Physician A to Z =============================-->
<asp:Panel ID="pnlAtoZ" runat="server" CssClass="module-pd-az core-az grid">
    <asp:PlaceHolder runat="server" ID="phControlHeader">
        <h3><asp:Literal ID="litPhysicianAtoZLabel" runat="server" /></h3>
    </asp:PlaceHolder>
    <asp:BulletedList ID="blAtoZ" runat="server" DisplayMode="HyperLink" CssClass="module-alphabet-list" />
    <div class="clear"></div>
</asp:Panel>

<script type="text/javascript">
    $(document).ready(function () {

        $("#<%=blAtoZ.ClientID%>").each(function () {
            $(this).find('li').each(function ()
            {   
                $(this).addClass('one columns'); });
        });
    });
    </script>
<!--==================== /Module: Physician Directory: Physician A to Z =============================-->