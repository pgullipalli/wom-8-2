﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.PhysicianDirectory.layouts.modules.PhysicianDirectory.PhysiciantitleSublayout"
    CodeBehind="PhysicianTitle.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<h1>
    <asp:Literal ID="litName" runat="server" />
</h1>
