﻿using System;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;

namespace womans.org.Website.layouts.modules.PhysicianDirectory
{
    using MedTouch.PhysicianDirectory.Helpers;

    /// <summary>
    /// Summary description for PhysicianimageSublayout
    /// </summary>
    public partial class PhysicianimageSublayout : BaseSublayout
    {
        private void Page_Load(object sender, EventArgs e)
        {

#if (!DEBUG)
            try
            {
#endif

            Initiate();

#if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
#endif

        }


        private void Initiate()
        {
            if (!string.IsNullOrEmpty(ItemHelper.GetFieldRawValue(ContextItem, ItemMapper.Physician.FieldName.Photo)))
            {
                scThumbnail.Item = ContextItem;
                pnlThumbnail.Visible = true;
            }

            if (!string.IsNullOrEmpty(ItemHelper.GetFieldRawValue(ContextItem, "Media Tab Content")))
            {
                scVideo.Item = ContextItem;
                scVideo.Visible = true;
                pnlVideo.Visible = true;

            }

            if (!string.IsNullOrEmpty(ItemHelper.GetFieldRawValue(ContextItem, "Profile")))
            {

                hlProfile.NavigateUrl = ItemHelper.GetMediaUrlFromFileField(ContextItem, "Profile");
                hlProfile.Text = CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianDetail.ProfileLinkLabel");
                hlProfile.Target = "_blank";
                pnlProfileButton.Visible = true;
            }


        }


    }
}