﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits=" MedTouch.PhysicianDirectory.layouts.modules.PhysicianDirectory.DownloadpdfSublayout"
    CodeBehind="DownloadPDF.ascx.cs" %>
<!--==================== Module: Physician Directory: Download PDF =============================-->
<asp:Panel ID="pnlDownloadPDF" runat="server" CssClass="module-pd-downloadpdf right">
    <asp:linkbutton ID="lbDownload" runat="server" OnClick="lbDownload_Click" CssClass="pdf-link" />
    <asp:ImageButton ID="ibDownload" runat="server" Visible="false" OnClick="ibDownload_Click" />
    <asp:Button ID="btnDownload" runat="server" Visible="false" OnClick="btnDownload_Click" />
</asp:Panel>
<div class="clear"></div>
<!--==================== /Module: Physician Directory: Download PDF =============================-->