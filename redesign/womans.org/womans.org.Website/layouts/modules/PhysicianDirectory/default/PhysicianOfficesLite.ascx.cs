﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
using MedTouch.PhysicianDirectory.DAL;
using MedTouch.PhysicianDirectory.Helpers;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;

namespace womans.org.Website.layouts.modules.PhysicianDirectory
{


    /// <summary>
    /// Summary description for PhysicianofficesSublayout
    /// </summary>
    public partial class PhysicianofficesSublayoutLite : BaseSublayout
    {
        #region properties

        private bool AccurateRangeSearch
        {
            get { return GetBooleanProperty("AccurateRangeSearch"); }
        }

        private Point ZipPoint { get; set; }

        private string ZipQueryString
        {
            get { return Request.QueryString[ModuleHelper.ZipQuerystring()]; }
        }

        private string RangeQueryString
        {
            get { return Request.QueryString[BaseModuleHelper.RangeQuerystring()]; }
        }

        #endregion properties

        private void Page_Load(object sender, EventArgs e)
        {

#if (!DEBUG)
            try
            {
#endif
            Initialize();
            BindOffices();

#if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
#endif

        }

        private void Initialize()
        {
            if (!string.IsNullOrEmpty(ZipQueryString))
            {
                //Get GeoLocation of Zipcode
                GeoLocation foundGeoLocation = GeoLocationDAL.GetGeolocation(ZipQueryString);
                if (foundGeoLocation != null)
                {
                    ZipPoint = new Point(foundGeoLocation.Latitude, foundGeoLocation.Longitude);
                }
            }
        }

        private void BindOffices()
        {
            List<Item> officeItems = ModuleHelper.GetPhysicianOfficeItems(ContextItem);
            if (officeItems.Any())
            {
                // Sort
                var sortItems = officeItems
                    .OrderByDescending(x => ItemHelper.GetFieldRawValue(x, "Primary Office"))
                    .ThenBy(x => ItemHelper.GetFieldRawValue(x, "Office Name"))
                    .ToList();

                rptOffices.DataSource = sortItems;
                rptOffices.DataBind();
                rptOffices.Visible = true;
            }
        }

        protected void rptOffices_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Item item = (Item) e.Item.DataItem;
                Literal litOfficeName = (Literal) e.Item.FindControl("litOfficeName");
                Literal litPhone = (Literal) e.Item.FindControl("litPhone");


                if (item != null)
                {
                    string strOfficeName = ItemHelper.GetFieldHtmlValue(item, "Office Name");
                    string strPhone = ItemHelper.GetFieldHtmlValue(item, "Office Phone");


                    Item locationItem = ItemHelper.GetSingleItemFromReferenceField(item, "Location");
                    string strLocationName = ItemHelper.GetFieldHtmlValue(locationItem, "Location Name");
                    string strMainPhone = ItemHelper.GetFieldHtmlValue(locationItem, "Main Phone");
                    string strMainFax = ItemHelper.GetFieldHtmlValue(locationItem, "Main Fax");

                    string strPrimary = ItemHelper.GetFieldRawValue(item, "Primary Office");

                    // use Office Name if available, otherwise, use Location Name
                    if (litOfficeName != null &&
                        (!string.IsNullOrEmpty(strLocationName) || !string.IsNullOrEmpty(strOfficeName)))
                    {
                        litOfficeName.Text = string.IsNullOrEmpty(strOfficeName) ? strLocationName : strOfficeName;
                    }

                    //// use Office Phone if available, otherwise, use Main Phone
                    //if (litPhone != null && litPhoneLabel != null &&
                    //    (!string.IsNullOrEmpty(strPhone) || !string.IsNullOrEmpty(strMainPhone)))
                    //{
                    //    litPhone.Text = string.IsNullOrEmpty(strPhone) ? strMainPhone : strPhone;
                    //    litPhoneLabel.Text =
                    //        CultureHelper.GetDictionaryTranslation(
                    //            "Modules.PhysicianDirectory.PhysicianOffices.LabelPhone");
                    //    pnlPhone.Visible = true;
                    //}

                }
                else if (e.Item.ItemType == ListItemType.Header)
                {
                    string strLabel =
                        CultureHelper.GetDictionaryTranslation(
                            "Modules.PhysicianDirectory.PhysicianOffices.OfficeLocationsLabel");
                    Literal litOfficesTitle = (Literal) e.Item.FindControl("litOfficesTitle");
                    Panel pnlOfficesTitle = (Panel) e.Item.FindControl("pnlOfficesTitle");
                    if (!string.IsNullOrEmpty(strLabel) && litOfficesTitle != null && pnlOfficesTitle != null)
                    {
                        litOfficesTitle.Text = strLabel;
                        pnlOfficesTitle.Visible = true;
                    }
                }
            }
        }
    }
}