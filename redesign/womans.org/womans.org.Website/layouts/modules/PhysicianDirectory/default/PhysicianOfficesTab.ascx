﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="womans.org.Website.layouts.modules.PhysicianDirectory.PhysicianofficesSublayoutLite"
    CodeBehind="PhysicianOfficesLite.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<asp:Panel runat="server" ID="pnlOffices" CssClass="grid">
    <div class="module-pd-office-listing-tab">
        <ul>
    <asp:Repeater ID="rptOffices" runat="server" OnItemDataBound="rptOffices_ItemDataBound"
        Visible="false">
        <ItemTemplate>
                <li><asp:Literal ID="litOfficeName" runat="server" /></li>        
        </ItemTemplate>
    </asp:Repeater>
    </ul>
    </div>
</asp:Panel>
