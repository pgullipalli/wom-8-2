﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="womans.org.Website.layouts.modules.PhysicianDirectory.PhysicianimageSublayout" CodeBehind="PhysicianImage.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<!--==================== Module: Physician Directory: Physician Image =============================-->
<%--<div class="callout">--%>
			<asp:Panel ID="pnlThumbnail" runat="server" CssClass="physician-profile-pic" Visible="false">
                <sc:Image ID="scThumbnail" Field="Photo" runat="server" MaxWidth="200" MaxHeight="200" CssClass="profile-pic" />
            </asp:Panel>
            
            <asp:Panel runat="server" ID="pnlVideo" CssClass="physician-profile-vid" Visible="false">
                <sc:Text runat="server" ID="scVideo" Field="Media Tab Content" />
            </asp:Panel>
            
            <asp:Panel runat="server" ID="pnlProfileButton" CssClass="address-callout" Visible="false">
                <asp:HyperLink runat="server" ID="hlProfile"></asp:HyperLink>
                
            </asp:Panel>

            
<%--		</div>--%>
<!--==================== /Module: Physician Directory: Physician Image =============================-->