﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PhysicianOffices.ascx.cs" Inherits=" womans.org.Website.layouts.modules.PhysicianDirectory.PhysicianofficesSublayout" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<!--==================== Module: Physician Directory: Physician Offices =============================-->



<asp:Repeater ID="rptOffices" runat="server" OnItemDataBound="rptOffices_ItemDataBound"
    Visible="false">
    <HeaderTemplate>

    </HeaderTemplate>
    <ItemTemplate> 
    <asp:Panel runat="server" ID="pnlOffices" CssClass="callout">
            <asp:Panel ID="pnlOfficesTitle" runat="server" Visible="false">
            <div class="physician-contact callout">
                <h4><span><asp:Literal ID="litOfficesTitle" runat="server" /></span></h4>
        </asp:Panel>
        <div class="module-pd-offices">
             
<%--            <div class="location-map">
					<img src="/assets/images/map.jpg">
			</div>  
--%>            <div class="address-callout">

                <asp:Panel ID="pnlAddress" runat="server" Visible="false">
                    <address>
                        <asp:Panel ID="pnlOfficeName" runat="server" Visible="false">
                            <asp:Literal ID="litOfficeName" runat="server" /></><br/>
                        </asp:Panel>
                           <asp:Literal ID="litAddress" runat="server" />
                    </address>
                </asp:Panel>
                <asp:Panel ID="pnlPhone" runat="server" Visible="false">
                    <p><asp:Literal ID="litPhoneLabel" runat="server" />
                    <asp:Literal ID="litPhone" runat="server" /></p>
                </asp:Panel>
                <asp:Panel ID="pnlFax" runat="server" Visible="false">
                    <p><asp:Literal ID="litFaxLabel" runat="server" />
                    <asp:Literal ID="litFax" runat="server" /></p>
                </asp:Panel>
                
                <asp:Panel ID="pnlOfficeLink" runat="server" Visible="false"></asp:Panel>
                <asp:HyperLink ID="hlMap" runat="server" Target="_blank" Visible="false" CssClass="button black" />
            </div>
        </div>
    </div>
<div class="clear"></div>
</asp:Panel>
    </ItemTemplate>
    
    <FooterTemplate>

    </FooterTemplate>
</asp:Repeater>



<!--==================== /Module: Physician Directory: Physician Offices =============================-->
