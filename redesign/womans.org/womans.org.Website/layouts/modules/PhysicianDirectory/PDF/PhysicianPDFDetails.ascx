﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhysicianPDFDetails.ascx.cs" Inherits="MedTouch.PhysicianDirectory.layouts.modules.PhysicianDirectory.PDF.PhysicianPDFDetails" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: Physician Directory: Physician PDF Detail =============================-->
<asp:Panel runat="server" ID="pnlDetail" CssClass="module-pd-profile">
    <sc:Sublayout ID="slREeturnToSearchLink" Path="/layouts/Base/Default/sublayouts/common/ReturnToSearchLink.ascx" runat="server" />
    <asp:Panel ID="pnlInfo" runat="server" CssClass="module-pd-profile-top">
        <sc:Sublayout ID="slPhysicianImage" Path="/layouts/modules/PhysicianDirectory/default/PhysicianImage.ascx" runat="server" />
        <sc:Sublayout ID="slPhysicianTitle" Path="/layouts/modules/PhysicianDirectory/default/PhysicianTitle.ascx" runat="server" />
        <sc:Sublayout ID="slPhysicianInfo" Path="/layouts/modules/PhysicianDirectory/default/PhysicianInfo.ascx" runat="server" />
        <sc:Sublayout ID="slPhysicianOffices" Path="/layouts/modules/PhysicianDirectory/default/PhysicianOffices.ascx" runat="server" />
    </asp:Panel>
    <sc:Sublayout ID="slPhysicianTabs" Path="/layouts/modules/PhysicianDirectory/default/PhysicianDetailTabs.ascx" runat="server" />
</asp:Panel>
<!--==================== /Module: Physician Directory: Physician PDF Detail =============================-->