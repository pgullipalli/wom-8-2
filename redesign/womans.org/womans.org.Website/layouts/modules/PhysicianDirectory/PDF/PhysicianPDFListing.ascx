﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhysicianPDFListing.ascx.cs" Inherits="MedTouch.PhysicianDirectory.layouts.modules.PhysicianDirectory.PhysicianPDFListing" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<p class="cover" style="page-break-after:always;">
    <span class="document-name"><asp:Literal ID="litOrgName" runat="server" /></span><br/>            
    <span class="document-title">Physician Directory</span><br/>
    <span class="document-caption">This directory is current as of <%=DateTime.Now.ToString("MMMM dd, yyyy") %></span>

</p>

<div class="listing">
    <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_ItemDataBound">
        <ItemTemplate>
            <table style="page-break-inside:avoid;">
                <tr>
                    <td rowspan="2" class="col-photo">
                        <asp:Image ID="imgSpacer" runat="server" ImageUrl="/sitecore/images/blank.gif" Height="1" />
                        <asp:Panel ID="pnlImage" runat="server" class="doc-photo" Visible="false"><asp:Image ID="imgPhoto" runat="server" /></asp:Panel>
                    </td>
                    <td colspan="2" class="col-name">
                        <h3>
                            <asp:Literal ID="litItem" runat="server" />
                        </h3>
                        <asp:Panel ID="pnlPositionTitles" runat="server" Cssclass="doc-position"><asp:Literal id="litPositionTitles" runat="server" /></asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="col-location">
                        <sc:Sublayout ID="slPhysicianOffices" Path="/layouts/modules/PhysicianDirectory/PDF/PhysicianOfficesPDF.ascx" runat="server" />
                    </td>
                    <td class="col-info">
                        <asp:Panel ID="pnlSpecialties" runat="server" Visible="false">
                            <h4>
                                <asp:Literal ID="litSpecialtiesLabel" runat="server" /></h4>                
                            <div class="content">
                                <asp:Literal ID="litSpecialties" runat="server" />
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlTabEducation" runat="server" Visible="false">
                            <h4>
                                <asp:Literal ID="litEducation" runat="server" /></h4>
                            <div class="content">
                                <asp:Repeater ID="rptTabEducation" runat="server" OnItemDataBound="rptTabEducation_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:Literal ID="litEduItem" runat="server" />
                                    </ItemTemplate>
                                    <SeparatorTemplate>
                                        <br />
                                    </SeparatorTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlTabInternship" runat="server" Visible="false">
                            <h4>
                                <asp:Literal ID="litInternship" runat="server" /></h4>
                            <div class="content">
                                <asp:Repeater ID="rptTabInternship" runat="server" OnItemDataBound="rptTabEducation_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:Literal ID="litEduItem" runat="server" />
                                    </ItemTemplate>
                                    <SeparatorTemplate>
                                        <br />
                                    </SeparatorTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlTabResidency" runat="server" Visible="false">
                            <h4>
                                <asp:Literal ID="litResidency" runat="server" /></h4>
                            <div class="content">
                                <asp:Repeater ID="rptTabResidency" runat="server" OnItemDataBound="rptTabEducation_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:Literal ID="litEduItem" runat="server" />
                                    </ItemTemplate>
                                    <SeparatorTemplate>
                                        <br />
                                    </SeparatorTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlTabFellowship" runat="server" Visible="false">
                            <h4>
                                <asp:Literal ID="litFellowship" runat="server" /></h4>
                            <div class="content">
                                <asp:Repeater ID="rptTabFellowship" runat="server" OnItemDataBound="rptTabEducation_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:Literal ID="litEduItem" runat="server" />
                                    </ItemTemplate>
                                    <SeparatorTemplate>
                                        <br />
                                    </SeparatorTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <div class="hr"></div>
        </ItemTemplate>
    </asp:ListView>
</div>