﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.PhysicianDirectory.layouts.modules.PhysicianDirectory.PhysicianofficesSublayout"
    CodeBehind="~/layouts/modules/PhysicianDirectory/default/PhysicianOffices.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<asp:Repeater ID="rptOffices" runat="server" OnItemDataBound="rptOffices_ItemDataBound"
    Visible="false">
    <ItemTemplate>
        <div class="location">
            <asp:Panel ID="pnlOfficeName" runat="server" Visible="false">
                <h4>
                    <asp:Literal ID="litOfficeName" runat="server" /></h4>
                <asp:Label ID="lblPrimary" runat="server" AssociatedControlID="litOfficeName" />
            </asp:Panel>
            <asp:Panel ID="pnlAddress" runat="server" Visible="false">
                <asp:Literal ID="litAddress" runat="server" />
            </asp:Panel>
            <asp:Panel ID="pnlPhone" runat="server" Visible="false">
                <b>
                    <asp:Literal ID="litPhoneLabel" runat="server" /></b>
                <asp:Literal ID="litPhone" runat="server" />
            </asp:Panel>
            <asp:Panel ID="pnlFax" runat="server" Visible="false">
                <b>
                    <asp:Literal ID="litFaxLabel" runat="server" /></b>
                <asp:Literal ID="litFax" runat="server" />
            </asp:Panel>
            <asp:Panel ID="pnlEmail" runat="server" Visible="false">
                <b>
                    <asp:Literal ID="litEmailLabel" runat="server" /></b>
                <asp:HyperLink ID="hlEmail" runat="server" />
            </asp:Panel>
        </div>
    </ItemTemplate>
</asp:Repeater>
