﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.SiteSearch.layouts.modules.SiteSearch.BasicSearchResultsSublayout"
    CodeBehind="BasicSearchResults.ascx.cs" %>
<%@ Import Namespace="MedTouch.Common.Helpers" %>
<%@ Register TagPrefix="MedTouch" TagName="PaginationList" Src="/layouts/modules/SiteSearch/default/SearchResultsPagination.ascx" %>
<!--==================== Module: Site Search: Basic Search Results =============================-->
<asp:Panel ID="pnlSearch" runat="server" CssClass="module-ss-results article">
    <asp:Panel ID="pnlCurrentSearchMessage" runat="server" Visible="false" CssClass="module-ss-results-message">
        <h3>
            <asp:Literal ID="litCurrentSearch" runat="server" /></h3>
    </asp:Panel>
    <MedTouch:PaginationList ID="ucPagingTop" runat="server" DisplayInfo="true" />
    <asp:Panel ID="pnlListing" runat="server" CssClass="listing">
        <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_ItemDataBound">
            <ItemTemplate>
                <asp:Panel ID="pnlResult" runat="server" CssClass="listing-item">
                    <h4>
                        <asp:HyperLink ID="hlItem" runat="server" /><asp:Literal ID="litItem" runat="server" /></h4>
                    <asp:Panel ID="pnlTeaser" runat="server" Visible="false" CssClass="listing-item-teaser"><asp:Literal ID="litTeaser" runat="server" /></asp:Panel>
                    <asp:Panel ID="pnlMoreLink" runat="server" Visible="false" CssClass="listing-item-more-link">
                        <asp:HyperLink ID="hlMoreLink" runat="server" />
                    </asp:Panel>
                </asp:Panel>
            </ItemTemplate>
            <EmptyDataTemplate>
                <%=CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.BasicSearchResults.NoResultsMessage") %></EmptyDataTemplate>
        </asp:ListView>
    </asp:Panel>
    <MedTouch:PaginationList ID="ucPagingBottom" runat="server" DisplayInfo="true" />
</asp:Panel>
<!--==================== /Module: Site Search: Basic Search Results =============================-->