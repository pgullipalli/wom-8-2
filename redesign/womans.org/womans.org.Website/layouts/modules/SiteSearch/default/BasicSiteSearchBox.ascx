﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="womans.org.Website.layouts.modules.SiteSearch.BasicSiteSearchBox" CodeBehind="BasicSiteSearchBox.ascx.cs" %>
<!--==================== Module: Site Search: Basic Site Search Box =============================-->

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSubmit" CssClass="main-search-input-wrapper">
    <asp:Panel ID="pnlKeyword" runat="server" CssClass="search-option" Visible="False">
        <asp:Label ID="lblKeyword" runat="server" AssociatedControlID="txtKeyword" />
    </asp:Panel>
        
        <asp:TextBox ID="txtKeyword" runat="server" CssClass="main-search-input" onkeydown="ispagenotfound(event, this)" />
        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="main-search-submit black button" />
        <asp:ImageButton ID="ibSubmit" runat="server" Visible="false" OnClick="ibSubmit_Click" />
        <asp:LinkButton ID="lbSubmit" runat="server" Visible="false" OnClick="lbSubmit_Click" />
    
    <asp:Panel ID="pnlError" runat="server" Visible="false" CssClass="errortext"><asp:Literal ID="litError" runat="server" /></asp:Panel>

</asp:Panel>
<!--==================== /Module: Site Search: Basic Site Search Box =============================-->