﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="womans.org.Website.layouts.modules.SiteSearch.BasicSiteSearchBox" CodeBehind="BasicSiteSearchBox.ascx.cs" %>
    <%@ Import Namespace="MedTouch.Common.Helpers" %>




<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSubmit" CssClass="mobile-search-wrapper">
<div class="mobile-search-inner">
	<table>
		<tr>
			<td>
                <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible="False">
                    <asp:Panel ID="pnlKeyword" runat="server" CssClass="search-option" Visible="False">
                        <asp:Label ID="lblKeyword" runat="server" AssociatedControlID="txtKeyword" />
                    </asp:Panel>
                </asp:PlaceHolder>
                <asp:TextBox ID="txtKeyword" runat="server" />
			</td>
			<td>
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="black button" />
                <asp:PlaceHolder ID="PlaceHolder2" runat="server" Visible="False">
                    <asp:ImageButton ID="ibSubmit" runat="server" Visible="false" OnClick="ibSubmit_Click" />
                    <asp:LinkButton ID="lbSubmit" runat="server" Visible="false" OnClick="lbSubmit_Click" />
                </asp:PlaceHolder>
			</td>
		</tr>
	</table>
</div>
    <asp:Panel ID="pnlError" runat="server" Visible="false" CssClass="errortext"><asp:Literal ID="litError" runat="server" /></asp:Panel>

</asp:Panel>
