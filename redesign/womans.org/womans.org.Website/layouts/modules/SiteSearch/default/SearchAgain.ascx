﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="womans.org.Website.layouts.modules.SiteSearch.SearchAgain"
    CodeBehind="SearchAgain.ascx.cs" %>
<!--==================== Module: Site Search: Search Again =============================-->
<asp:Panel ID="pnlFilter" runat="server" DefaultButton="btnSubmit" CssClass="module-ss-search-again core-search-again core-search grid">
    <asp:Panel ID="pnlKeyword" runat="server" CssClass="six columns">
        <asp:Label ID="lblKeyword" runat="server" AssociatedControlID="txtKeyword" />
        <asp:TextBox ID="txtKeyword" runat="server" CssClass="textbox" /></asp:Panel>
    <asp:Panel ID="pnlCategory" runat="server" CssClass="six columns">
        <asp:Label ID="lblCategory" runat="server" AssociatedControlID="ddlCategory" />
        <div class="selectbox">
            <asp:DropDownList ID="ddlCategory" runat="server" CssClass="selectboxdiv" />
            <div class="out"></div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlError" runat="server" Visible="false" CssClass="errortext">
        <asp:Literal ID="litError" runat="server" /></asp:Panel>
    <div class="twelve columns">
        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="button pink default-button" />
        <asp:ImageButton ID="ibSubmit" runat="server" Visible="false" OnClick="ibSubmit_Click" />
        <asp:LinkButton ID="lbSubmit" runat="server" Visible="false" OnClick="lbSubmit_Click" />
    </div>
</asp:Panel>
<!--==================== /Module: Site Search: Search Again =============================-->