﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.SiteSearch.layouts.modules.SiteSearch.SearchfilterSublayout" CodeBehind="SearchFilter.ascx.cs" %>
<!--==================== Module: Site Search: Search Filters =============================-->
<asp:Panel ID="pnlFilter" runat="server" CssClass="module-ss-filters core-filters callout collapse-for-mobile">
    <div class="reg-callout grid">
        <asp:PlaceHolder runat="server" ID="phControlHeader">
            <h3 class="module-heading"><asp:Literal ID="litSearchFilter" runat="server" /></h3>
        </asp:PlaceHolder>
        <asp:BulletedList ID="blFilter" runat="server" DisplayMode="HyperLink" CssClass="module-ss-filters-list core-list" />
    </div>
</asp:Panel>
<!--==================== /Module: Site Search: Search Filters =============================-->

