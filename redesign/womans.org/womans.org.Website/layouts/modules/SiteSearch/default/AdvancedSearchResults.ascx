﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.SiteSearch.layouts.modules.SiteSearch.AdvancedsearchresultsSublayout"
    CodeBehind="AdvancedSearchResults.ascx.cs" %>
<%@ Import Namespace="MedTouch.Common.Helpers" %>
<%@ Register TagPrefix="MedTouch" TagName="PaginationList" Src="/layouts/modules/SiteSearch/default/SearchResultsPagination.ascx" %>
<!--==================== Module: Site Search: Advanced Search Results =============================-->
<asp:Panel ID="pnlSearch" runat="server" CssClass="module-ss-results module-core-results grid">
    <asp:Panel ID="pnlCurrentSearchMessage" runat="server" Visible="false" CssClass="module-ss-results-message twelve columns">
        <h3><asp:Literal ID="litCurrentSearch" runat="server" /></h3>
    </asp:Panel>
    <MedTouch:PaginationList ID="ucPagingTop" runat="server" DisplayInfo="true" />
    <asp:Panel ID="pnlListing" runat="server" CssClass="listing grid">
        <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_ItemDataBound">
            <ItemTemplate>
                <asp:Panel ID="pnlResult" runat="server" CssClass="twelve columns">
                    <h4>
                        <asp:HyperLink ID="hlItem" runat="server" /><asp:Literal ID="litItem" runat="server" /></h4>
                    <asp:Panel ID="pnlTeaser" runat="server" Visible="false" CssClass="listing-item-teaser"><asp:Literal ID="litTeaser" runat="server" /></asp:Panel>
                    <asp:Panel ID="pnlMoreLink" runat="server" Visible="false" CssClass="listing-item-more-link">
                        <asp:HyperLink ID="hlMoreLink" runat="server" />
                    </asp:Panel>
                </asp:Panel>
            </ItemTemplate>
            <EmptyDataTemplate>
                <%=CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.AdvancedSearchResults.NoResultsMessage") %></EmptyDataTemplate>
        </asp:ListView>
    </asp:Panel>
    <MedTouch:PaginationList ID="ucPagingBottom" runat="server" DisplayInfo="true" />
</asp:Panel>
<!--==================== /Module: Site Search: Advanced Search Results =============================-->