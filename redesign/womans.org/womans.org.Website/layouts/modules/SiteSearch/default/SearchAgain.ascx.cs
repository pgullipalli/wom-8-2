﻿using System;
using System.Collections.Generic;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
using MedTouch.SiteSearch.Helpers;
using Sitecore.Data.Items;

namespace womans.org.Website.layouts.modules.SiteSearch
{
    public partial class SearchAgain : BaseSublayout
	{
        #region properties
        private readonly string _keywordWatermark = CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.SearchAgain.SearchBoxWatermark");
        private readonly string _criteriaRequiredTxt = CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.SearchAgain.CriteriaRequired");

        private readonly string _strQuerystringKeyword = ModuleHelper.KeywordQuerystring();
        private readonly string _strQuerystringCategory = ModuleHelper.CategoryQuerystring();
        private string _searchButtonText = string.Empty;
        #endregion properties

        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialize();
        }

        private void Initialize()
        {
            string keyword = Request.QueryString[_strQuerystringKeyword];
            string strKeywordLabel = CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.SearchAgain.SearchByKeywordLabel");
            lblKeyword.Text = strKeywordLabel;
            lblKeyword.Visible = !string.IsNullOrEmpty(strKeywordLabel);
            UIHelper.SetTextboxWatermarkPlaceholder(txtKeyword, keyword, _keywordWatermark);

            string strCategoryLabel = CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.SearchAgain.SearchByCategoryLabel");
            lblCategory.Text = strCategoryLabel;
            lblCategory.Visible = !string.IsNullOrEmpty(strCategoryLabel);

            BindFilterDropdown();

            _searchButtonText = CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.SearchAgain.SearchButtonText");
            btnSubmit.Text = _searchButtonText;
        }

        private void BindFilterDropdown()
        {
            string category = Request.QueryString[_strQuerystringCategory];
            Item searchFilterFolder = GetSingleReferenceProperty("Search Filter Folder");
            List<Item> categories = ModuleHelper.GetAllFilters(searchFilterFolder);
            ModuleHelper.BindSearchFilterDropdown(ddlCategory, category, categories, pnlCategory);
        }

        private void SearchSubmitted()
        {
            string keyword = txtKeyword.Text.Trim();

            if (keyword.Equals(_keywordWatermark, StringComparison.OrdinalIgnoreCase))
            {
                keyword = string.Empty;
            }

            string category = ddlCategory.SelectedValue;

            string searchPathID = string.Empty;
            if (!string.IsNullOrEmpty(category))
            {
                var categoryFilterItem = ModuleHelper.GetSearchFilterCategoryItem(category);
                if (categoryFilterItem != null)
                {
                    searchPathID = ItemHelper.GetFieldRawValue(categoryFilterItem, "By Path");
                }
            }

            if (!string.IsNullOrEmpty(keyword) || !string.IsNullOrEmpty(category))
            {
                Item searchResultsPage = ModuleHelper.GetSearchResultPage(GetSingleReferenceProperty("Search Results Page"));
                ModuleHelper.SubmitSearch(keyword, category, string.Empty, string.Empty, string.Empty, false, searchPathID, searchResultsPage);
            }
            else
            {
                litError.Text = _criteriaRequiredTxt;
                pnlError.Visible = true;
            }
        }

        protected void ibSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            SearchSubmitted();
        }

        protected void lbSubmit_Click(object sender, EventArgs e)
        {
            SearchSubmitted();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SearchSubmitted();
        }
	}
}