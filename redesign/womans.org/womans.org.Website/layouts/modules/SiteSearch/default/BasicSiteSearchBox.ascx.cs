﻿using System;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
using MedTouch.SiteSearch.Helpers;
using Sitecore.Data.Items;

namespace womans.org.Website.layouts.modules.SiteSearch
{
    public partial class BasicSiteSearchBox : BaseSublayout
	{
        private string _searchBoxWatermark = string.Empty;
        private string _searchButtonText = string.Empty;

        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialize();
        }

        private void SearchSubmitted()
        {
            _searchBoxWatermark = CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.BasicSiteSearchBox.SearchBoxWatermark");
            string keyword = txtKeyword.Text.Trim();
            if (!string.IsNullOrEmpty(keyword) && !keyword.Equals(_searchBoxWatermark, StringComparison.OrdinalIgnoreCase))
            {
                Item searchResultsPage = ModuleHelper.GetSearchResultPage(GetSingleReferenceProperty("Search Results Page"));
                ModuleHelper.SubmitSearch(keyword, string.Empty, string.Empty, string.Empty, string.Empty, false, string.Empty, searchResultsPage);
            }
            else
            {
                litError.Text = CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.BasicSiteSearchBox.KeywordRequired");
                pnlError.Visible = true;
                pnlSearch.Attributes.Add("style","display:block;");
            }
        }

        protected void ibSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            SearchSubmitted();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SearchSubmitted();
        }

        protected void lbSubmit_Click(object sender, EventArgs e)
        {
            SearchSubmitted();
        }

        private void Initialize()
        {
            string strKeywordLabel = CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.BasicSiteSearchBox.SearchByKeywordLabel");
            lblKeyword.Text = strKeywordLabel;
            lblKeyword.Visible = !string.IsNullOrEmpty(strKeywordLabel);

            _searchButtonText = CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.BasicSiteSearchBox.SearchButtonText");
            btnSubmit.Text = _searchButtonText;

            UIHelper.SetTextboxWatermarkPlaceholder(txtKeyword, "",
                             CultureHelper.GetDictionaryTranslation("Modules.SiteSearch.BasicSiteSearchBox.SearchBoxWatermark"));
        }
	}
}