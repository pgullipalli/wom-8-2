﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="womans.org.Website.layouts.modules.News.NewsSearch" CodeBehind="NewsSearch.ascx.cs" %>
<!--==================== Module: News: News Quick Search =============================-->
<%--<script type="text/javascript">
function ResetAllFields() {
        ResetField($("#<%= txtKeyword.ClientID %>"));
        ResetField($("#<%= txtDateFrom.ClientID %>"));
        ResetField($("#<%= txtDateTo.ClientID %>"));

        ResetField($("#<%= ddlCategory.ClientID %>"));
    }
    $(document).ready(function () {
        $("#btnReset").val("<%= _resetButtonTxt %>");
    });
</script>--%>
<style type="text/css">
    .panel-buttons .button {
        display: inline !important;
    }
</style>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSubmit" CssClass="module-nw-quick-search core-quick-search core-search callout collapse-for-mobile">
    <div class="reg-callout grid">
    <asp:PlaceHolder runat="server" ID="phControlHeader">
    <h3 class="module-heading">
        <asp:Literal ID="litNewsSearch" runat="server" /></h3>
    </asp:PlaceHolder>
    <asp:Panel ID="pnlKeyword" runat="server" CssClass="twelve columns">
        <asp:Label ID="lblKeyword" runat="server" AssociatedControlID="txtKeyword" CssClass="label" />
        <asp:TextBox ID="txtKeyword" runat="server" CssClass="textbox"/></asp:Panel>
    <asp:Panel ID="pnlCategory" runat="server" CssClass="twelve columns">
        <asp:Label ID="lblCategory" runat="server" AssociatedControlID="ddlCategory" CssClass="label" />
        <div class="selectbox">
        <asp:DropDownList ID="ddlCategory" runat="server" CssClass="selectboxdiv" />
        <div class="out"></div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDate" runat="server" CssClass="twelve columns grid">
        <asp:Label ID="lblDate" runat="server" AssociatedControlID="txtDateFrom" CssClass="label" />
        <asp:TextBox ID="txtDateFrom" type="date" runat="server" CssClass="dp_input twelve columns" />
        <asp:TextBox ID="txtDateTo" type="date" runat="server" CssClass="dp_input twelve columns" />
    </asp:Panel>
    <asp:Panel runat="server" CssClass="twelve columns">
        <%--<input type="button" id="btnReset" onclick="ResetAllFields();" class="button" style="display:none;"/>--%>
        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="button default_button pink" />
        <asp:ImageButton ID="ibSubmit" runat="server" OnClick="ibSubmit_Click" Visible="false" />
        <asp:LinkButton ID="lbSubmit" runat="server" Visible="false" OnClick="lbSubmit_Click" />
        <input type="reset" name="main_1$leftpanel_0$btnSubmit" value="Reset" class="button reset">
    </asp:Panel>
    
    </div>
</asp:Panel>
<!--==================== /Module: News: News Quick Search =============================-->