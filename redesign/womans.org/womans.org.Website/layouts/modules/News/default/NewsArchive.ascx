﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.News.layouts.modules.News.NewsarchiveSublayout" CodeBehind="NewsArchive.ascx.cs" %>
<!--==================== Module: News: News Archive =============================-->
<asp:Panel ID="pnlYears" runat="server" CssClass="module-nw-year callout collapse-for-mobile">
    <div class="">
    <div class="reg-callout">
        <asp:PlaceHolder runat="server" ID="phControlHeader">
        <h3><asp:Literal ID="litNewsArchive" runat="server" /></h3></asp:PlaceHolder>
        <asp:BulletedList ID="blYears" runat="server" DisplayMode="HyperLink" CssClass="module-nw-filters-year" />
        </div>
    </div>
</asp:Panel>
<!--==================== /Module: News: News Archive =============================-->