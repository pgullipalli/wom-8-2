﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.News.layouts.modules.News.FeaturednewsSublayout" CodeBehind="FeaturedNews.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: News: Featured News =============================-->

<asp:Panel runat="server" ID="pnlFeaturedNews" CssClass="module-nw-feature grid reg-callout">
    <asp:PlaceHolder ID="phControlHeader" runat="server">
        <h3 class="module-header">
            <asp:Literal ID="litFeaturedNews" runat="server" /></h3>
    </asp:PlaceHolder>
    <asp:Panel ID="pnlListing" runat="server" CssClass="listing twelve columns">
        <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_ItemDataBound">
            <ItemTemplate>
                <asp:Panel ID="pnlResult" runat="server" CssClass="listing-item">
                    <asp:Panel ID="pnlThumbnail" runat="server" Visible="false" CssClass="module-thumbnail">
                        <sc:Image ID="scThumbnail" Field="News Image" runat="server" MaxWidth="100"/>
                    </asp:Panel>
                    <div class="teaser-copy">
                        <h5><asp:HyperLink ID="hlItem" runat="server" /><asp:Literal ID="litItem" runat="server" /></h5>
                        <asp:Panel ID="pnlDate" runat="server" CssClass="date" Visible="false">
                            <asp:Literal ID="litDate" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="pnlTeaser" runat="server" Visible="false" CssClass="listing-item-teaser">
                            <asp:Literal ID="litTeaser" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="pnlMoreLink" runat="server" Visible="false" CssClass="listing-item-more-link">
                            <asp:HyperLink ID="hlMoreLink" runat="server" />
                        </asp:Panel>
                    </div>
                </asp:Panel>
            </ItemTemplate>
        </asp:ListView>
    </asp:Panel>
    <asp:Panel ID="pnlViewAll" runat="server" Visible="false" CssClass="module-nw-view-all">
        <asp:HyperLink ID="hlViewAll" runat="server" /></asp:Panel>
</asp:Panel>
<!--==================== /Module: News: Featured News =============================-->
