﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.News.layouts.modules.News.RelatednewsSublayout" CodeBehind="RelatedNews.ascx.cs" %>

<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: News: Related News =============================-->
<asp:Panel runat="server" ID="pnlRelatedNews" CssClass="module-nw-related core-related callout collapse-for-mobile">
    <div class="reg-callout grid">
        <asp:PlaceHolder ID="phControlHeader" runat="server">
            <h3 class="module-heading"><asp:Literal ID="litRelatedNews" runat="server" /></h3>
        </asp:PlaceHolder>
        <asp:Panel runat="server" ID="pnlListing" >
        <ul class="core-list">
            <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_ItemDataBound" >
                <ItemTemplate>
                    <li class="grid">
                        <asp:Panel ID="pnlThumbnail" runat="server" Visible="false" CssClass="module-thumbnail">
                            <sc:Image ID="scThumbnail" Field="News Image" runat="server" MaxWidth="50" MaxHeight="50" />
                        </asp:Panel>
                                                  
                        <asp:Panel ID="pnlResult" runat="server" CssClass="teaser-copy">
                            <h5 class="list-item-header"><asp:HyperLink ID="hlItem" runat="server" /><asp:Literal ID="litItem" runat="server" /></h5>
                            <asp:Panel ID="pnlDate" runat="server" CssClass="date" Visible="false">
                                <asp:Literal ID="litDate" runat="server" />
                            </asp:Panel>                            
                            <asp:Panel ID="pnlTeaser" runat="server" Visible="false" CssClass="list-item-teaser">
                                <asp:Literal ID="litTeaser" runat="server" />
                            </asp:Panel>
                            <asp:Panel ID="pnlMoreLink" runat="server" Visible="false" CssClass="listing-item-more-link">
                                <asp:HyperLink ID="hlMoreLink" runat="server" />
                            </asp:Panel>                            
                        </asp:Panel>
                       
                        
                       
                    </li>
                    
                   
                </ItemTemplate>
            </asp:ListView>
        </ul>
        </asp:Panel>
        <asp:Panel ID="pnlViewAll" runat="server" Visible="false" CssClass="module-view-all">
            <asp:HyperLink ID="hlViewAll" runat="server" />
        </asp:Panel>
    </div>
</asp:Panel>
<!--==================== /Module: News: Related News =============================-->