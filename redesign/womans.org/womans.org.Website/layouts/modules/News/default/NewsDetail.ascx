﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.News.layouts.modules.News.NewsdetailSublayout" CodeBehind="NewsDetail.ascx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!--==================== Module: News: News Detail =============================-->
<div class="module-nw-detail article">

   <sc:Sublayout ID="slREeturnToSearchLink" Path="/layouts/Base/Default/sublayouts/common/ReturnToSearchLink.ascx"
        runat="server" Visible="true" />

    <h1><sc:Text ID="scHeadline" Field="Headline" runat="server" /></h1>
    <asp:Panel ID="pnlDate" runat="server" Visible="false" CssClass="date">
        <sc:Date ID="scDate" runat="server" Field="News Date" Format="MMMM dd, yyyy" />
    </asp:Panel>
    <asp:Panel ID="pnlThumbnail" runat="server" Visible="false" CssClass="listing-pic right">
        <sc:Image ID="scThumbnail" Field="News Image" runat="server" />
    </asp:Panel>
    <sc:Text ID="scContentCopy" Field="Content Copy" runat="server" />
</div>
<!--==================== /Module: News: News Detail =============================-->