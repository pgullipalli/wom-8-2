﻿using System;
using System.Collections.Generic;
using MedTouch.Common.Helpers;
using MedTouch.Common.UI;
using MedTouch.News.Helpers;
using Sitecore.Data.Items;

namespace womans.org.Website.layouts.modules.News
{
    public partial class NewsSearch : BaseSublayout
	{
        private readonly string _strQuerystringKeyword = ModuleHelper.KeywordQuerystring();
        private readonly string _strQuerystringCategory = ModuleHelper.CategoryQuerystring();
        private readonly string _strQuerystringDateFrom = ModuleHelper.DateFromQuerystring();
        private readonly string _strQuerystringDateTo = ModuleHelper.DateToQuerystring();
        protected readonly string _resetButtonTxt = CultureHelper.GetDictionaryTranslation("Modules.News.NewsSearch.ResetButtonText");

        private void Page_Load(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif

            if (!IsPostBack)
            {
                Initialize();
            }

#if (!DEBUG)
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(this.ToString(), ex, this);
            }
#endif
        }

        private void Initialize()
        {
            string keyword = Request.QueryString[_strQuerystringKeyword];
            string dateFrom = Request.QueryString[_strQuerystringDateFrom];
            string dateTo = Request.QueryString[_strQuerystringDateTo];

            string strKeywordLabel = CultureHelper.GetDictionaryTranslation("Modules.News.NewsSearch.SearchByKeywordLabel");
            string strCategoryLabel = CultureHelper.GetDictionaryTranslation("Modules.News.NewsSearch.SearchByCategoryLabel");
            string strDateLabel = CultureHelper.GetDictionaryTranslation("Modules.News.NewsSearch.SearchByDateLabel");

            lblKeyword.Text = strKeywordLabel;
            lblCategory.Text = strCategoryLabel;
            lblDate.Text = strDateLabel;

            lblKeyword.Visible = !string.IsNullOrEmpty(strKeywordLabel);
            lblCategory.Visible = !string.IsNullOrEmpty(strCategoryLabel);

            UIHelper.SetTextboxWatermarkPlaceholder(txtKeyword, keyword,
                                         CultureHelper.GetDictionaryTranslation("Modules.News.NewsSearch.SearchBoxWatermark"));

            bool allowSearchByDate = GetBooleanProperty("Allow Search by Date");
            if (allowSearchByDate)
            {
                UIHelper.SetTextboxWatermarkPlaceholder(txtDateFrom, dateFrom,
                                             CultureHelper.GetDictionaryTranslation(
                                                 "Modules.News.NewsSearch.DateFromWatermark"));
                UIHelper.SetTextboxWatermarkPlaceholder(txtDateTo, dateTo,
                                             CultureHelper.GetDictionaryTranslation(
                                                 "Modules.News.NewsSearch.DateToWatermark"));
                lblDate.Visible = !string.IsNullOrEmpty(strDateLabel);
            }
            else
            {
                pnlDate.Visible = false;
            }

            btnSubmit.Text = CultureHelper.GetDictionaryTranslation("Modules.News.NewsSearch.SearchButtonText");
            litNewsSearch.Text = CultureHelper.GetDictionaryTranslation("Modules.News.NewsSearch.NewsSearchLabel");
            phControlHeader.Visible = !(string.IsNullOrEmpty(litNewsSearch.Text));

            BindCategoryDropdown();
        }

        private void BindCategoryDropdown()
        {
            string category = Request.QueryString[_strQuerystringCategory];
            Item categoryFolderItem = GetSingleReferenceProperty("News Category Folder");
            List<Item> availableCategories = ModuleHelper.GetAvailableNewsCategories(categoryFolderItem);
            ModuleHelper.BindNewsCategoryDropdown(ddlCategory, category, availableCategories, pnlCategory);
        }

        protected void ibSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            SearchSubmitted();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SearchSubmitted();
        }

        protected void lbSubmit_Click(object sender, EventArgs e)
        {
            SearchSubmitted();
        }

        protected virtual void SearchSubmitted()
        {
            string searchBoxWatermark = CultureHelper.GetDictionaryTranslation("Modules.News.NewsSearch.SearchBoxWatermark");
            string dateFromWatermark = CultureHelper.GetDictionaryTranslation("Modules.News.NewsSearch.DateFromWatermark");
            string dateToWatermark = CultureHelper.GetDictionaryTranslation("Modules.News.NewsSearch.DateToWatermark");

            string keyword = txtKeyword.Text.Trim();
            if (keyword.Equals(searchBoxWatermark, StringComparison.OrdinalIgnoreCase))
                keyword = string.Empty;

            string dateFrom = txtDateFrom.Text.Trim();
            if (dateFrom.Equals(dateFromWatermark, StringComparison.OrdinalIgnoreCase))
                dateFrom = string.Empty;

            string dateTo = txtDateTo.Text.Trim();
            if (dateTo.Equals(dateToWatermark, StringComparison.OrdinalIgnoreCase))
                dateTo = string.Empty;

            Item searchResultsPage = GetSingleReferenceProperty("Search Results Page");
            if (searchResultsPage == null)
            {
                searchResultsPage = ItemHelper.GetItemFromReferenceField(ItemHelper.GetStartItem(), "News Search Results Page");
            }
            ModuleHelper.SubmitSearch(keyword, ddlCategory.SelectedValue, string.Empty, dateFrom,
                                         dateTo, searchResultsPage);
        }
	}
}