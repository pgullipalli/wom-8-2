﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.News.layouts.modules.News.NewscategoriesSublayout" CodeBehind="NewsCategories.ascx.cs" %>
<!--==================== Module: News: News Categories =============================-->
<asp:Panel ID="pnlCategories" runat="server" CssClass="module-nw-categories core-categories callout collapse-for-mobile">
    <div class="reg-callout grid">
        <asp:PlaceHolder runat="server" ID="phControlHeader">
        <h3 class="module-heading">
            <asp:Literal ID="litNewsCategories" runat="server" /></h3></asp:PlaceHolder>

        <asp:BulletedList ID="blCategory" runat="server" DisplayMode="HyperLink" CssClass="module-nw-filters-list core-list" />
    </div>
</asp:Panel>
<!--==================== /Module: News: News Categories =============================-->