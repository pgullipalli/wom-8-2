﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="MedTouch.News.layouts.modules.News.NewslistingSublayout" CodeBehind="NewsListing.ascx.cs" %>
<%@ Import Namespace="MedTouch.Common.Helpers" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="MedTouch" TagName="PaginationList" Src="/layouts/modules/News/default/NewsPagination.ascx" %>
<!--==================== Module: News: News Listing =============================-->
<asp:Panel ID="pnlResults" CssClass="module-nw-results article" runat="server">
    <asp:Panel ID="pnlCurrentSearchMessage" runat="server" Visible="false">
        <h3><asp:Literal ID="litCurrentSearch" runat="server" /></h3>
    </asp:Panel>
    <MedTouch:PaginationList ID="ucPagingTop" runat="server" DisplayInfo="true" />
    <asp:Panel ID="pnlListing" runat="server" CssClass="listing">
        <asp:ListView ID="lvSearchResults" runat="server" OnItemDataBound="lvSearchResults_ItemDataBound">
            <ItemTemplate>
                <asp:Panel ID="pnlResult" runat="server" CssClass="listing-item">
                    <h4><asp:HyperLink ID="hlItem" runat="server" /><asp:Literal ID="litItem" runat="server" /></h4>
                    <asp:Panel ID="pnlThumbnail" runat="server" Visible="false" CssClass="module-thumbnail two columns">
                        <sc:Image ID="scThumbnail" Field="News Image" runat="server" MaxWidth="100" />
                    </asp:Panel>
                    <div class="teaser-copy ten columns">
                        <asp:Panel ID="pnlDate" runat="server" CssClass="module-date">
                            <asp:Literal ID="litDate" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="pnlTeaser" runat="server" Visible="false" CssClass="listing-item-teaser">
                            <asp:Literal ID="litTeaser" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="pnlMoreLink" runat="server" Visible="false" CssClass="listing-item-more-link">
                            <asp:HyperLink ID="hlMoreLink" runat="server" />
                        </asp:Panel>
                    </div>
                </asp:Panel>
            </ItemTemplate>
            <EmptyDataTemplate>
                <%=CultureHelper.GetDictionaryTranslation("Modules.News.NewsListing.NoResultsMessage") %></EmptyDataTemplate>
        </asp:ListView>
    </asp:Panel>
    <MedTouch:PaginationList ID="ucPagingBottom" runat="server" DisplayInfo="true" />
</asp:Panel>
<!--==================== /Module: News: News Listing =============================-->