﻿<%@ Control Language="c#" AutoEventWireup="True" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="womans.org.Website.layouts.modules.News.NewsSearch" CodeBehind="NewsSearch.ascx.cs" %>
<!--==================== Module: News: News Search =============================-->
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSubmit" CssClass="module-nw-search core-search twelve columns grid">
    <asp:Panel ID="pnlControlHeader" runat="server" Visible="False">
        <asp:PlaceHolder runat="server" ID="phControlHeader" >
            <h3><asp:Literal ID="litNewsSearch" runat="server" /></h3>
        </asp:PlaceHolder>
    </asp:Panel>
    <asp:Panel ID="pnlKeyword" runat="server" CssClass="six columns">
        <asp:Label ID="lblKeyword" runat="server" AssociatedControlID="txtKeyword" CssClass="label" />
        <asp:TextBox ID="txtKeyword" runat="server" CssClass="textbox"/>
    </asp:Panel>
    <asp:Panel ID="pnlCategory" runat="server" CssClass="six columns">
        <asp:Label ID="lblCategory" runat="server" AssociatedControlID="ddlCategory" CssClass="label" />
        <div class="selectbox">
        <asp:DropDownList ID="ddlCategory" runat="server" CssClass="selectboxdiv" />
        <div class="out"></div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDate" runat="server" CssClass="six columns">
        <asp:Label ID="lblDate" runat="server" AssociatedControlID="txtDateFrom" CssClass="label" />
        <div class="grid">
        <asp:TextBox type="date" ID="txtDateFrom" runat="server" CssClass="dp_input six columns"/>
        <asp:TextBox type="date" ID="txtDateTo" runat="server" CssClass="dp_input six columns" />
        </div>
    </asp:Panel>
    <div class="search-option-submit twelve columns">
        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="button pink default_button" />
        <asp:ImageButton ID="ibSubmit" runat="server" OnClick="ibSubmit_Click" Visible="false" />
        <asp:LinkButton ID="lbSubmit" runat="server" Visible="false" OnClick="lbSubmit_Click" />
    </div>
</asp:Panel>
<!--==================== /Module: News: News Search =============================-->