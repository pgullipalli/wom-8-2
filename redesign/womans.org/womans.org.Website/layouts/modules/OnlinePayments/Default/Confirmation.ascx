﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Confirmation.ascx.cs" Inherits="MedTouch.OnlinePayments.layouts.modules.OnlinePayments.Default.Confirmation" %>
<asp:Panel ID="pnlConfirmation" Cssclass="module-op-confirmation-listing" runat="server">
    <asp:Repeater ID="rptConfirmation" runat="server">
        <ItemTemplate>
                <asp:Panel ID="pnlFormField" CssClass="listing-item" Visible='<%# Eval("HasValue") %>' runat="server">
                    <asp:Label 
                        ID="lblFieldName" 
                        CssClass="listing-item-field-name"
                        runat="server">
                       <%# Eval("Name") %>:&nbsp;
                    </asp:Label>
                    <asp:Label 
                        ID="lblFieldValue" 
                        CssClass="listing-item-field-value"
                        Text='<%# Eval("Value") %>' 
                        runat="server" />
                </asp:Panel>
        </ItemTemplate>
    </asp:Repeater>
</asp:Panel>