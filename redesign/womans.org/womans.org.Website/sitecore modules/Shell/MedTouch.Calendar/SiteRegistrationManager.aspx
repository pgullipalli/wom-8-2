﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="SiteRegistrationManager.aspx.cs" Inherits="MedTouch.Calendar.sitecore_modules.Shell.MedTouch.Calendar.SiteRegistrationManager" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sitewide Registration Search</title>
    
    <script type="text/javascript" src="/assets/js/jquery.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.dataTables.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/dataTables.bootstrap.css">

    <script type="text/javascript" language="javascript" src="/assets/js/dataTables.bootstrap.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {

            var oTable = $("#grid").dataTable({ "bLengthChange": true, "bPaginate": true})
						
                        
			} );
           
      

           
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div style="padding:20px;">

<table id="grid" class="table table-striped table-bordered" cellspacing="0" width="100%" style="font-size: 10px;">
    <thead>
        <tr>
            <th>Edit</th>
            <th>Event</th>
            <th>Session</th>
            <th>First&nbsp;Name</th>
            <th>Last&nbsp;Name</th>
            <th>Email</th>
            <th>Status</th>
            <th>Fee</th>
            <th>Fee&nbsp;Type</th>
        </tr>
    </thead>
    <tbody>
    <asp:Repeater runat="server" ID="rptRegistrations">
        <ItemTemplate>
            <tr>
                <td><a target="_parent" href="setTransactionId.aspx?id=<%# Eval("SessionId") %>&transid=<%# Eval("TransactionId") %>" >Edit</a></td>
                <td><%# Eval("TopicName") %></td>
                <td><%# Eval("SessionName") %></td>
                <td><%# Eval("FirstName") %></td>
                <td><%# Eval("LastName") %></td>
                <td><%# Eval("Email") %></td>
                <td><%# Eval("Status") %></td>
                <td><%# Eval("Fee") %></td>
                <td><%# Eval("FeeType") %></td>
            </tr>
           
            
        </ItemTemplate>
    </asp:Repeater>
    </tbody>
</table>

       
    </div>
    
   
    </form>
</body>
</html>
