﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetTransactionId.aspx.cs" Inherits="MedTouch.Calendar.sitecore_modules.Shell.MedTouch.Calendar.SetTransactionId" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script>
        
        function redirect()
        {
            var guid = '<%= Request.QueryString["Id"].ToString() %>';
            var url = '/sitecore/shell/applications/content manager/default.aspx?id={' + guid + '}&lang=en&fo={' + guid + '}';
            window.location.href = url;
        }
    
    </script>
</head>
<body onload="redirect()">
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
