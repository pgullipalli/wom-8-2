﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecurringPrototype.aspx.cs"
    Inherits="MedTouch.Calendar.layouts.RecurringPrototype" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <style type="text/css">
        .wrapper
        {
            border: grey 1px dotted;
        }
        .right
        {
            float: right;
        }
        .left
        {
            float: left;
        }
    </style>
    <form id="form1" runat="server">
    <div>
        <div class="wrapper">
            <div class="left">
                <asp:RadioButton runat="server" Text="Weekly Recurrence" GroupName="RecurrenceType"
                    ID="rdbWeeklyRecurrence" />
                of every
                <asp:TextBox runat="server" ID="txtWeeklyFrequency" Text="1" />
                week(s)
            </div>
            <div class="right">
                <asp:CheckBox runat="server" ID="chkSunday" Text="Sunday" />
                <asp:CheckBox runat="server" ID="chkMonday" Text="Monday" />
                <asp:CheckBox runat="server" ID="chkTuesday" Text="Tuesday" />
                <asp:CheckBox runat="server" ID="chkWednesday" Text="Wednesday" />
                <asp:CheckBox runat="server" ID="chkThursday" Text="Thursday" />
                <asp:CheckBox runat="server" ID="chkFriday" Text="Friday" />
                <asp:CheckBox runat="server" ID="chkSaturday" Text="Saturday" />
            </div>
            <div style="clear: both;">
            </div>
        </div>
        <div class="wrapper">
            <div class="left">
                <asp:RadioButton ID="rdbMonthlyRecurrence" runat="server" Text="Monthly Recurrence"
                    GroupName="RecurrenceType" />
                of every
                <asp:TextBox runat="server" ID="txtMonthlyFrequency" Text="1" />
                month(s)
            </div>
            <div class="right">
                <div>
                    <asp:RadioButton ID="rdbMonthlyDayOfMonth" runat="server" Text="Day " GroupName="RecurrenceMonthlyType" />
                    <asp:TextBox runat="server" ID="txtDayOfMonth" />
                    of month(s).
                </div>
                <div>
                    <asp:RadioButton ID="rdbMonthlyDayOfWeek" runat="server" Text="the" GroupName="RecurrenceMonthlyType" />
                    <asp:DropDownList ID="ddlWeekNumber" runat="server" />
                    <asp:DropDownList ID="ddlDayOfWeek" runat="server" />
                </div>
            </div>
            <div style="clear: both;">
            </div>
        </div>
    </div>
    <div class="wrapper">
        <div>
            Range of recurrence</div>
        <div>
            start :
            <asp:Calendar ID="dpStartDate" runat="server" />
        </div>
        <div>
            <div>
                <asp:RadioButton ID="rdbEndRepeat" runat="server" Text="Repeat" GroupName="repeatGroup" />
                <asp:TextBox runat="server" ID="txtRepeat" Text="1" />Times</div>
            <div>
                <asp:RadioButton ID="rdbEndDate" runat="server" Text="End Date" GroupName="repeatGroup" />
                <asp:Calendar ID="dpEndDate" runat="server" />
            </div>
        </div>
    </div>
    <asp:Button runat="server" ID="btnSubmit" OnClick="btnSubmit_Click" Text="Submit" />
    <div>
        <asp:Literal runat="server" ID="litResult" />
    </div>
    </form>
</body>
</html>
