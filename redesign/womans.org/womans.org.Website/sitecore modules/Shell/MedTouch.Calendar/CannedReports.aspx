﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CannedReports.aspx.cs" Inherits="MedTouch.Calendar.sitecore_modules.Shell.MedTouch.Calendar.RegistrationReports" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Reports</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/dataTables.bootstrap.css" />
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE"/>
    <meta http-equiv="EXPIRES" content="0"/>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
            <scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" 
                Name="Telerik.Web.UI.Common.Core.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" 
                Name="Telerik.Web.UI.Common.jQuery.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" 
                Name="Telerik.Web.UI.Common.jQueryInclude.js">
            </asp:ScriptReference>
        </scripts>
        </telerik:RadScriptManager>
        <div>
        </div>
        <div style="padding: 20px">
            <div>
                <asp:Label runat="server" ID="lblDateFrom" Text="Date From: "></asp:Label>
                <telerik:RadDatePicker runat="server" ID="datePickFromDate"></telerik:RadDatePicker>
                <asp:Label runat="server" ID="lblDateTo" Text="Date To: "></asp:Label>
                <telerik:RadDatePicker runat="server" ID="datePickToDate"></telerik:RadDatePicker>
                <asp:Label runat="server" ID="lblReportType" Text="Report Type: "></asp:Label>
                <asp:DropDownList runat="server" ID="ddlReportType" />
                <asp:Button runat="server" ID="btnGetReport" Text="Get Report" OnClick="btnGetReport_OnClick" />
            </div>
            <div>
                <p>
                    <asp:Label runat="server" ID="lblMessage" Visible="false"></asp:Label></p>
            </div>
        </div>
    </form>
</body>
</html>
