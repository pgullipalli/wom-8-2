﻿namespace womans.org.Website.sitecore_modules.Web.MedTouch_Utilities
{
    #region

    using System;
    using System.Globalization;
    using System.Linq;
    using System.Web.Security;
    using System.Web.UI;

    using Sitecore.Diagnostics;
    using Sitecore.Security.Accounts;

    #endregion

    public partial class UpdateEcmUsers : Page
    {
        #region Public Methods and Operators

        public void DeleteUser(User user)
        {
            try
            {
                user.Delete();
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error Deleting User {0}; Source:{1}", ex.Message, ex.Source), this);
            }
        }

        public void EditUser(User existingUser, string domain)
        {
            this.Response.Write(string.Format("Checking User... {0}<br/>", existingUser.Profile.UserName));
            //claire_dot_m_dot_ojeh_at_gmail_dot_com ==>Claire M Ojeh
            //janetrh1964_at_yahoo_dot_com ==>janetrh1964

            var userName = string.Empty;
            var userNameBase =
                existingUser.Profile.UserName.Replace(existingUser.Domain.ToString(), "")
                            .Replace("\\", "")
                            .Replace("_at_", "@")
                            .Replace("_dot_", " ");
            if (userNameBase.Length > userNameBase.IndexOf("@", StringComparison.Ordinal) + 1)
            {
                userName =
                    userNameBase.Substring(0, userNameBase.LastIndexOf("@", StringComparison.Ordinal) + 1)
                                .Replace("@", " ");
            }
            userName = string.Format(@"{0}\{1}", domain, userName);
            var newPassword = Membership.GeneratePassword(10, 3);

            if (Sitecore.Security.Accounts.User.Exists(userName))
            {
                this.Response.Write(string.Format("Unable to update: UserName ({0}) already exist!!<br/>", userName));
                return;
            }

            var email = existingUser.Profile.Email;
            //update email address
            if (string.IsNullOrEmpty(existingUser.Profile.Email))
            {
                email = existingUser.Profile.ProfileUser.LocalName.Replace("_at_", "@").Replace("_dot_", ".");
            }

            //create new user
            Membership.CreateUser(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(userName), newPassword, email);

            try
            {
                //copy over existing roles
                var newUser = Sitecore.Security.Accounts.User.FromName(userName, true);
                this.Response.Write(string.Format("Added New User: {0}<br/>", newUser.Profile.UserName));
                existingUser.Roles.ToList().ForEach(
                    r =>
                        {
                            this.Response.Write(string.Format("Copying Roles: {0}<br/>", r.ToString()));
                            newUser.Roles.Add(r);
                        });

                //copy over existing full name if exist
                if (!string.IsNullOrEmpty(existingUser.Profile.FullName))
                {
                    newUser.Profile.FullName = existingUser.Profile.FullName;
                }

                this.Response.Write(string.Format("Deleting User: {0}<br/>", existingUser.Profile.UserName));
                this.DeleteUser(existingUser);
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error Message: {0}; Source:{1}", ex.Message, ex.Source), this);
            }
        }

        #endregion

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            this.UpdateUser();
        }

        private void PrintOutUserInfo(User u)
        {
            this.Response.Write(
                string.Format("{0} | {1} | {2} | {3} <br/>", u.Profile.UserName, u.Domain, u.Profile.Email, u.Name));
        }

        private void UpdateUser()
        {
            UserManager.GetUsers()
                       .Where(
                           u =>
                           u.Domain.ToString().Contains("extranet") || u.Domain.ToString().Contains("Emailcampaign"))
                       .ToList()
                       .ForEach(
                           i =>
                               {
                                   if (i.Profile.UserName.Contains("_at_") && i.Profile.UserName.Contains("_dot_"))
                                   {
                                       this.EditUser(i, "Emailcampaign");
                                   }
                               });
        }

        #endregion
    }
}