﻿namespace womans.org.Website.sitecore_modules.Web.MedTouch_Utilities
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Web.UI;

    using MedTouch.Common.Helpers;

    using Sitecore.Data.Items;
    using Sitecore.Tasks;

    using womans.org.BusinessLogic.Helpers.Tasks;

    #endregion

    public partial class TestEventReminderEmail : Page
    {
        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            var task = new SendReminderEmailsTask();
            var startItem = ItemHelper.GetItemByPath("/sitecore/content/Home");
            var items = new List<Item>();
            items.Add(startItem);

            var cmdItem =
                new CommandItem(ItemHelper.GetItemByPath("/sitecore/system/Tasks/Schedules/CalendarSendReminderEmails"));
            var schItem =
                new ScheduleItem(
                    ItemHelper.GetItemByPath("/sitecore/system/Tasks/Schedules/CalendarSendReminderEmails"));

            task.SendEventReminderEmails(items.ToArray(), cmdItem, schItem);
        }

        #endregion
    }
}