<%@ Page Language="C#" AutoEventWireup="true" Inherits="Sitecore.SharedSource.Search.sitecore.DemoPage5"
   CodeBehind="DemoPage5.aspx.cs" MasterPageFile="~/sitecore modules/Web/ShareSourceSearchDemo/Demo.Master" %>

<asp:Content ContentPlaceHolderID="mainPH" runat="server">
   <h3>
      Search Parameters:</h3>
   Date Range 1:
   <asp:TextBox ID="FieldName1TextBox" runat="server" />
   Start:
   <asp:TextBox ID="StartDate1TextBox" runat="server" />
   End:
   <asp:TextBox ID="EndDate1TextBox" runat="server" />
   <br />
   Date Range 2:
   <asp:TextBox ID="FieldName2TextBox" runat="server" />
   Start:
   <asp:TextBox ID="StartDate2TextBox" runat="server" />
   End:
   <asp:TextBox ID="EndDate2TextBox" runat="server" />
</asp:Content>
