﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="~/UI/WFFM/EventRegistrationShoppingCartFields.cs" 
    Inherits="MedTouch.Calendar.UI.WFFM.EventRegistrationShoppingCartFields" %>
<%@ Register TagPrefix="wfmcustom" Namespace="Sitecore.Form.Web.UI.Controls" Assembly="Sitecore.Forms.Custom" %>
<style type="text/css">
    /* Inline Css */
    .CartTotalBorder
    {
        clear: left;
        text-align: left;
        display: block;
        margin: 5px 0px;
        width: 100%;
        vertical-align: top;
    }
    
    .CartTotalTitleLabel
    {
        padding: 3px 0px;
        width: 30%;
        display: block;
        float: left;
    }
    
    .CartTotalLabel
    {
        padding: 0px 2px 0px 0px;
        width: 60%;
        display: block;
        float: left;
    }
</style>
<asp:Panel ID="pnlCartTotal" CssClass="CartTotalBorder" runat="server">
    <wfmcustom:Label ID="wfmCartTotalTitle" CssClass="CartTotalTitleLabel" runat="server"
        AssociatedControlID="lbCartTotal" Text="Cart Total" />
    <asp:Label ID="lbCartTotal" CssClass="CartTotalLabel" runat="server" />
</asp:Panel>