﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="~/UI/WFFM/PaymentMethodFields.cs"
    Inherits="MedTouch.Calendar.UI.WFFM.PaymentMethodFields" %>
<%@ Register TagPrefix="wfmcustom" Namespace="Sitecore.Form.Web.UI.Controls" Assembly="Sitecore.Forms.Custom" %>

<asp:Panel ID="pnlPaymentDescription" runat="server" CssClass="scfSingleLineTextBorder">
    <asp:Label ID="lbPaymentMethodID" runat="server" ReadOnly="True" Visible="False" />
    <asp:Label ID="lbPaymentInformation" runat="server" CssClass="scfSingleLineTextLabel" />
</asp:Panel>
