﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="~/UI/WFFM/EventSessionIDField.cs"
    Inherits="MedTouch.Calendar.UI.WFFM.EventSessionIDField" %>
<%@ Register TagPrefix="wfmcustom" Namespace="Sitecore.Form.Web.UI.Controls" Assembly="Sitecore.Forms.Custom" %>
<asp:Panel ID="pnlEventSessionID" runat="server" >
    <asp:Label ID="lbEventSessionID" runat="server" ReadOnly="True" Visible="False" />
</asp:Panel>
