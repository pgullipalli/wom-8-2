﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="~/UI/WFFM/EventRegistrationAmountField.cs" 
    Inherits="MedTouch.Calendar.UI.WFFM.EventRegistrationAmountField" %>
<%@ Register TagPrefix="wfmcustom" Namespace="Sitecore.Form.Web.UI.Controls" Assembly="Sitecore.Forms.Custom" %>
<style type="text/css">
    /* Inline Css */
    .EventAmountBorder
    {
        clear: left;
        text-align: left;
        display: block;
        margin: 5px 0px;
        width: 100%;
        vertical-align: top;
    }
    
    .EventAmountTitleLabel
    {
        padding: 3px 0px;
        width: 30%;
        display: block;
        float: left;
    }
    
    .EventAmountLabel
    {
        padding: 0px 2px 0px 0px;
        width: 60%;
        display: block;
        float: left;
    }
</style>
<asp:Panel ID="pnlEventRegistrationAmount" CssClass="EventAmountBorder" runat="server">
    <wfmcustom:Label ID="wfmAmountTitle" CssClass="EventAmountTitleLabel" runat="server"
        AssociatedControlID="lbAmountValue" Text="Total" />
    <asp:Label ID="lbAmountValue" CssClass="EventAmountLabel" runat="server" />
</asp:Panel>