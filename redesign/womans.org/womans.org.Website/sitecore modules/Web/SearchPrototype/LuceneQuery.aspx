﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sitecore modules/Web/SearchPrototype/Demo.Master"
    AutoEventWireup="true" CodeBehind="LuceneQuery.aspx.cs" Inherits="MedTouch.SiteSearch.Prototype.Scripts.LuceneQuery" %>

<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
    <h2>
        Direct Lucene Query based on Lucene query syntax
    </h2>
    <p>
        <a href="http://lucene.apache.org/core/3_6_0/queryparsersyntax.html"> Lucene Query Syntax</a>
        The current list special characters are+ - && || ! ( ) { } [ ] ^ " ~ * ? : \ <br/>
        To escape these character use the \ before the character. For example to search for (1+1):2 use the query:
        \(1\+1\)\:2
    </p>
    <p>
        Index name and Root item input fields are not being used.
    </p>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="mainPH" runat="server">
    <div>
        Lucene Query:
        <asp:TextBox ID="txtQuery" Width="500px" Height="400px" TextMode="MultiLine" runat="server" Text="+_template:bfe5cc2bf0454a058b9a1bc4b5f49cb6 +_path:532166548920452fa371754f24b0c83e +_language:en +location\ name:medical +show\ in\ location\ listings:1" /></div>
</asp:Content>
