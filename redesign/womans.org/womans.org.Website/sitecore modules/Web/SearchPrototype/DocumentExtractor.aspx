﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="MedTouch.SiteSearch.sitecore_modules.Web.SearchPrototype.DocumentExtractor" %>

<%@ Assembly Name="Sitecore.Kernel" %>
<%@ Assembly Name="MedTouch.Common" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Sitecore.Diagnostics" %>
<%@ Import Namespace="Sitecore.Data" %>
<%@ Import Namespace="Sitecore.SharedSource.Search.Utilities" %>
<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        string guid = Request.QueryString["id"];
        if (!IsPostBack && !string.IsNullOrEmpty(guid))
        {
            MediaItem documentItem = Sitecore.Configuration.Factory.GetDatabase("master").Items[guid];

            string result = ExtractMediaItem(documentItem);
            litResult.Text = result;
        }
    }

    protected string ExtractMediaItem(MediaItem mediaItem)
    {
        string pdfContents = string.Empty;

        if (mediaItem != null)
        {
            try
            {
                using (var stream = mediaItem.GetMediaStream())
                {
                    TextExtractor textExtractor = new TextExtractor();
                    pdfContents = textExtractor.ParseStream(stream);
                }
            }
            catch (Exception ex)
            {
                string message =
                    string.Format(
                        "Sitecore.SharedSource.Search.DynamicFields: PdfContentField: Cannot parse Media Content: Media path|{0}: File Path|{1}",
                        mediaItem.Path, mediaItem.FilePath);
                Log.Warn(message, ex);
            }
        }

        return pdfContents;
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        MediaItem documentItem = Sitecore.Configuration.Factory.GetDatabase("master").GetItem(txtPath.Text);

        string guid = documentItem.ID.ToGuid().ToString();
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path) + "?id=" + guid);
    }

</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <asp:Literal ID="Literal2" runat="server">Media Item Path</asp:Literal>
            <asp:TextBox runat="server" ID="txtPath" Text="/sitecore/media library/Files/installation_guide_sc65"
                Width="1000" />
        </div>
        <div>
            <asp:Button runat="server" ID="btnOk" Text="Execute" OnClick="btnOk_Click" />
        </div>
        <div>
            <asp:Literal runat="server" ID="litResult"/>
        </div>
    </div>
    </form>
</body>
</html>
