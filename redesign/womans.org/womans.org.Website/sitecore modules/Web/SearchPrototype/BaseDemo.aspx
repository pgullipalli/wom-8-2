﻿<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/sitecore modules/Web/SearchPrototype/Demo.Master"
    Inherits="MedTouch.SiteSearch.Prototype.Scripts.BaseDemo" CodeBehind="BaseDemo.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
    <h2>
        Base Demo
    </h2>
    <p>
        full text query (same as SiteSearch box).
    </p>
</asp:Content>
