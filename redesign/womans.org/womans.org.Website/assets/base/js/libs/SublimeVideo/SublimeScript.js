﻿/** http://docs.sublimevideo.net/playlists **//** jQuery version **/
var SublimeVideo = SublimeVideo || { playlists: {} };
jQuery(document).ready(function () {
    // A SublimeVideoPlaylist instance can takes some options:  
    //  - autoplayOnPageLoad: whether or not to autoplay the playlist on page load. Note that if you set it to true,  
    //    you should remove the 'sublime' class from the first video in the playlist.  
    //  - loadNext: whether or not to load the next item in the playlist once a video playback ends  
    //  - autoplayNext: whether or not to autoplay the next item in the playlist once a video playback ends  
    //  - loop: whether or not to loop the entire playlist once the last video playback ends
    var playlistOptions = { autoplayOnPageLoad: false, loadNext: true, autoplayNext: true, loop: false };
    // Automatically instantiate all the playlists in the page
    jQuery('.sv_playlist').each(function (i, el) { SublimeVideo.playlists[el.id] = new SublimeVideoPlaylist(el.id, playlistOptions); });
});
var SublimeVideoPlaylist = function (playlistWrapperId, options) {
    if (!jQuery("#" + playlistWrapperId)) return;
    this.options = options;
    this.playlistWrapperId = playlistWrapperId;
    this.firstVideoId = jQuery("#" + this.playlistWrapperId + " video").attr("id");
    this.setupObservers();
    this.updateActiveVideo(this.firstVideoId);
};
jQuery.extend(SublimeVideoPlaylist.prototype, { setupObservers: function () {
    var that = this;
    jQuery("#" + this.playlistWrapperId + " li").each(function () {
        jQuery(this).click(function (event) {
            event.preventDefault();
            if (!jQuery(this).hasClass("active")) {
                that.clickOnThumbnail(jQuery(this).attr("id"), that.options['autoplayNext']);
            }
        });
    });
},
    reset: function () {
        // Hide the current active video
        jQuery("#" + this.playlistWrapperId + " .video_wrap.active").removeClass("active");
        // Get current active video and unprepare it
        // we could have called sublimevideo.unprepare() without any arguments, but this is faster
        sublimevideo.unprepare(jQuery("#" + this.activeVideoId)[0]);
        // Deselect its thumbnail
        this.deselectThumbnail(this.activeVideoId);
    },
    clickOnThumbnail: function (thumbnailId, autoplay) {
        this.updateActiveVideo(thumbnailId.replace(/^thumbnail_/, ""));
        if (autoplay) { // And finally, prepare and play it if autoplay is true
            sublimevideo.prepareAndPlay(jQuery("#" + this.activeVideoId)[0]);
        } else { // Or only prepare it if autoplay is false
            sublimevideo.prepare(jQuery("#" + this.activeVideoId)[0]);
        }
    }, selectThumbnail: function (videoId) { jQuery("#thumbnail_" + videoId).addClass("active"); }, deselectThumbnail: function (videoId) { jQuery("#thumbnail_" + videoId).removeClass("active"); }, updateActiveVideo: function (videoId) {
        // Basically undo all the stuff and bring it back to the point before js kicked in
        if (this.activeVideoId) this.reset();
        // Set the new active video
        this.activeVideoId = videoId;
        // And show the video
        this.showActiveVideo();
    }, showActiveVideo: function () {
        // Select its thumbnail
        this.selectThumbnail(this.activeVideoId);
        // Show it
        jQuery("#" + this.activeVideoId).parent().addClass("active");
    },
    handleAutoNext: function (newThumbnailId) {
        this.clickOnThumbnail(newThumbnailId, this.options['autoplayNext']);
    }
});

sublimevideo.ready(function () {
    for (var playlistId in SublimeVideo.playlists) {
        SublimeVideo.playlists[playlistId].clickOnThumbnail(SublimeVideo.playlists[playlistId].activeVideoId, SublimeVideo.playlists[playlistId].options['autoplayOnPageLoad']);
    }
    sublimevideo.onEnd(function (sv) {
        var matches = sv.element.id.match(/^video([0-9]+)$/);
        if (matches !== undefined) {
            var playlistId = jQuery(sv.element).parents('.sv_playlist').attr("id");
            var nextThumbnail = jQuery("#thumbnail_" + sv.element.id).next("li");
            if (nextThumbnail.length > 0) {
                if (SublimeVideo.playlists[playlistId].options['loadNext']) {
                    SublimeVideo.playlists[playlistId].handleAutoNext(nextThumbnail.attr("id"));
                }
            }
            else if (SublimeVideo.playlists[playlistId].options['loop']) {
                SublimeVideo.playlists[playlistId].updateActiveVideo(SublimeVideo.playlists[playlistId].firstVideoId);
                SublimeVideo.playlists[playlistId].handleAutoNext(SublimeVideo.playlists[playlistId].activeVideoId);
            }
            else {
                sublimevideo.stop();
            }
        }
    });
});